<?php 
    /**
    * REPORT ALL DATA CALL HISTORY MONTH TO DATE
    * @author mbuh
    * -- 
    * @param start_date = Y-m-01 , end_date = Y-m-30/31
    * @return CSV FILE
    */
    ## construct ##

    $con = mysqli_connect("192.168.0.11","enigma","enigma","enigmahsbcdb");
    //init notif before exported file
    echo date('H:i:s') . " Create File CSV ... wait gan\n";
        
    //define today
    // $date = date("Y-m-d");    
    $date = '2019-03-01';    
    $sql = 
    "SELECT d.deb_id AS cust_id,d.deb_name,DATE_FORMAT(d.deb_wo_date,'%d-%m-%Y') AS wo_date,
                d.deb_perm_msg_coll,CONCAT(\"'\",s.Recipient),DATE_FORMAT(s.SendDate,'%d-%m-%Y %H:%i:%s') AS senddate,ss.StatusCode,
                s.Message,a.id AS agent,b.id AS tl,DATE_FORMAT(d.deb_created_ts, '%d-%m-%Y') AS tglupload,
                    CASE
                        WHEN s.TemplateId = 13 THEN 'Fresh WO (+5)'
                        WHEN s.TemplateId = 15 THEN 'Fresh WO (+20)'
                        WHEN s.TemplateId = 14 THEN 'Fresh WO (+30)'
                        WHEN s.TemplateId = 17 THEN 'Fresh WO (+53)'
                        WHEN s.TemplateId = 18 THEN 'Fresh WO (+75)'
                        WHEN s.TemplateId = 23 THEN 'Fresh WO (+100)'
                        WHEN s.TemplateId = 19 THEN 'Fresh WO (+150)'
                        WHEN s.TemplateId = 20 THEN 'Fresh WO (+175)'                       
                    END AS SMStype                      
                FROM t_gn_debitur d
                INNER JOIN serv_sms_outbox s ON d.deb_id=s.MasterId
                INNER JOIN t_tx_agent a ON d.deb_update_by_user=a.UserId
                INNER JOIN t_tx_agent b ON a.tl_id=b.UserId
                INNER JOIN serv_sms_status ss ON s.SmsStatus=ss.StatusId
                WHERE 1=1
                AND s.TemplateId IN (13,15,14,17,18,23,19,20)
                AND s.SendDate >= '".$date." 00:00:00'
                AND s.SendDate <=  '".$date." 23:00:00'
                GROUP BY d.deb_id";

    // echo $sql; exit;
    //set query
    $qry =mysqli_query($con,$sql);
    // Sample data. This can be fetched from mysql too
    $data = array();
    while ($row_data = mysqli_fetch_assoc($qry)) {
        $data[] = $row_data;
        echo date('H:i:s') . " lagi proses nih gan .............+++..+++..+++\n";
        echo date('H:i:s') . " ++............++\n";
        echo date('H:i:s') . " SABAR ++............++\n";
    }
    
    echo date('H:i:s') ." Lagi Dinamain filenya .......\n";
    //filename
    $fileName = "ReportSMSDaily".date("Ymd"). ".csv";
    header('Content-type: text/csv');
    
    // do not cache the file
    header('Pragma: no-cache');
    header('Expires: 0');
    echo date('H:i:s'). " File nya udah di path gan sabar bentar lagi............\n"; 
    //tentukan foldernya 
    $path   = '/opt/enigma/webapps/hsbc/Export/';

    //cek apakah sudah ada direktorinya 
    //jika tdk ada bkin baru
    if(!is_dir($path)) {
        mkdir($path, 0777, TRUE);
    }

    $fp = fopen($path.$fileName.'', "w");
    fputcsv($fp, array('Customerid','DEBITUR_NAME','WO_DATE','SEGMENT','RECEPIENT','SEND_DATE','SMS_STATUS','MESSAGE','DC','TL','UPLOAD_DATE','Template',));
    // output each row of the data
    foreach ($data as $row )
    {
        fputcsv($fp, $row);
    }
    
    echo date('H:i:s') . "udah selesai gan coba dilihat ......\n";
    echo "FILENAME = ".$fileName."\n";
    echo "PATH INFORMATION = ".$path."\n";
    echo "#DemocrazyISSyirk\n";
    echo "#setuju\n";
    fclose($fp);
    exit();