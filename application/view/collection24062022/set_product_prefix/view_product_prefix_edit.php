<?php
preg_match_all('!\d+!', $data['PrefixChar'], $matches);
$PrefixCode = substr($data['PrefixChar'], 0, (strlen($data['PrefixChar'])-strlen($matches[0][0])) )
?>

<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
 <fieldset class="corner" style="background-color:white;margin:3px;">
 <form name="frmEditPrefix">
	<?php echo form()->hidden('PrefixNumberId',null,$data['PrefixNumberId']);?>
	<legend class="icon-application">&nbsp;&nbsp;&nbsp;Edit Product Prefix </legend>
	<table cellpadding="4px;">
	<tr>
		<td class="text_header">* Product </td>
		<td><?php echo form() -> combo('result_head_level','select long',$Product, $data['ProductId'],null,array('disabled'=>true));?></td>
	</tr>
	<tr>
		<td class="text_header">* Prefix Code</td>
		<td><?php echo form() -> input('result_code','input_text long', $PrefixCode,null,array('style'=>'height:19px;'));?></td>
	</tr>
	<tr>
		<td class="text_header">* Prefix Length</td>
		<td><?php echo form() -> input('result_name','input_text long', $data['PrefixLength'], null,array('style'=>'height:19px;'));?></td>
	</tr>
	<tr>
		<td class="text_header">Form Input</td>
		<td><?php echo form() -> combo('form_input','select long',array());?></td>
	</tr>
	<tr>
		<td class="text_header">Form Edit</td>
		<td><?php echo form() -> combo('form_edit','select long',NULL);?></td>
	</tr>
					
	<tr>
		<td class="text_header">Status</td>
		<td><?php echo form() -> combo('status_active','select long',$Status, $data['PrefixFlagStatus']);?></td>
	</tr>
	<tr>
		<td class="text_header">&nbsp;</td>
		<td><input type="button" class="update button" onclick="Ext.DOM.UpdatePrefix();" value="Update"></td>
	</tr>
	</table>
	</form>
	</fieldset>
</div>