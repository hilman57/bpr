<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
 <fieldset class="corner" style="background-color:white;margin:3px;">
 <legend class="icon-application">&nbsp;&nbsp;&nbsp;Add Product Prefix </legend>			
	<table cellpadding="4px;">
		<tr>
			<td class="text_header">* Product </td>
			<td><?php echo form() -> combo('result_head_level','select long',$Product );?></td>
		</tr>
		<tr>
			<td class="text_header">* Method</td>
			<td><?php echo form() -> combo('result_method','input_text long', $Method );?></td>
		</tr>
		<tr>
			<td class="text_header">* Code</td>
			<td><?php echo form() -> input('result_code','input_text long', null, null,array('style'=>'height:19px;'));?></td>
		</tr>
		
		<tr>
			<td class="text_header">* Length</td>
			<td><?php echo form() -> input('result_length','input_text long', null, null,array('style'=>'height:19px;'));?></td>
		</tr>
		<tr>
			<td class="text_header">Form Input</td>
			<td><?php echo form() -> combo('form_input','select long',$AddForm);?></td>
		</tr>
		<tr>
			<td class="text_header">Form Edit</td>
			<td><?php echo form() -> combo('form_edit','select long',$EditForm);?></td>
		</tr>
		<tr>
			<td class="text_header">Status</td>
			<td><?php echo form() -> combo('status_active','select long',$Status);?></td>
		</tr>
		<tr>
			<td class="text_header">&nbsp;</td>
			<td><input type="button" class="save button" onclick="Ext.DOM.savePrefix();" value="Save"></td>
		</tr>
	</table>
</fieldset>			
</div>