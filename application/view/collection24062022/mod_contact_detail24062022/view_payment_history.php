<?php 
/*
 * @ pack : payment history history 
 */
 
$objPH = (object)null;
if( class_exists('M_PaymentHistory') ) 
{
  $objPH =&M_PaymentHistory::Instance();
}

$listPH = $objPH->_get_payment_detail($Customer['deb_id']);
// print_r($listPH);
$Month = $objPH->_get_select_month();
$contentPH = array();
if(is_array($listPH) AND count($listPH)>0)
{
	foreach ($listPH as $index => $value)
	{
		$expl_index = explode('-',$index);
		
		$contentPH[$expl_index[0]][$expl_index[1]]= $value;
		
	}
}



/*
 * @ pack : create function return x 
 */
 
if(!function_exists('_class_exist'))
{
  function _class_exist($Year=NULL, $month=0 ) 
 {
   $conds = 0;
   $arrYears = explode('-', $Year);
   if( is_array($arrYears) ){
	  if( $arrYears[1] == $month ){
		  $conds++;
	  }
   }
   
   return $conds;   
 }
} 

/*
 * @ pack : create div layout 
 */
if(!function_exists('_class_html'))
{
 function _class_html( $conds )
{
	$array_html = array(
	  1 => "<div class='assign' style='color:green;'>Pay</div>",	
	  0 => "<div class='close' style='color:red;'>No Pay</div>"
	);
	 
	return  $array_html[$conds];
 }
 
} 
 
 
?>

<fieldset class='corner' style='margin-top:0px;margin-bottom:0px;'>
<legend class='icon-customers' id='payment-history-detail'>&nbsp;&nbsp;Payment History</legend>
<div id="content-payment-history-detail">	
	
</div>

<!-- Payment history list -->
<div id="content-payment-history-list">	
 <table border=0 cellspacing=1 width='99%'>
	<thead>
		<tr height='28'>
			<th class="ui-corner-top ui-state-default first" width="5%" nowrap>&nbsp;No.</th>
			<th class="ui-corner-top ui-state-default middle center" width="10%" nowrap>&nbsp;Pay Date</th>
			<th class="ui-corner-top ui-state-default middle center" width="30%" nowrap>&nbsp;CH Name</th>
			<th class="ui-corner-top ui-state-default middle center" width="20%" nowrap>&nbsp;Agent ID</th>
			<th class="ui-corner-top ui-state-default middle center" width="20%" nowrap>&nbsp;Payment</th>
			<th class="ui-corner-top ui-state-default middle center" width="20%" nowrap>&nbsp;Delete By</th>
			<th class="ui-corner-top ui-state-default middle center" width="20%" nowrap>&nbsp;Update Date</th>
			<th class="ui-corner-top ui-state-default middle center" width="20%" nowrap>&nbsp;Action</th>
		</tr>
	</thead>
	<tbody id="PaymentHistoryList"></tbody>
 </table>	
</div>

</fieldset>