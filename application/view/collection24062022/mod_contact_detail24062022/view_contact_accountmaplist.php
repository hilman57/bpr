<!-- start : acount maping ====================== -->
<div id="tabs_list_account_map" style="background-color:#FFFFFF;overflow:auto;">
<fieldset class='corner'>
<legend class='icon-customers'>&nbsp;&nbsp;Account List </legend>
<table border=0 align="left" cellspacing=1 width="100%">
   <thead>
	 <tr height='24'>
		<th class="ui-corner-top ui-state-default first" WIDTH="2%" nowrap>&nbsp;No.</td>
		<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;ID Number.</td>
		<th class="ui-corner-top ui-state-default first" WIDTH="10%" nowrap>&nbsp;Account Number</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Class Card</td>
		<!-- t h class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Data Type</t d --->
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Amout WO</td>
	 </tr>
   </thead>
  <tbody id="list_account_content_map">
  <?php $no = 0; ?>
  <?php if(is_array($AccOthers) ) { 
	foreach( $AccOthers as $rows ) :  
		$bgcolor = ( ($no%2)==1 ?"#FFFFFF":"#FFFEEE");
	?>
		<tr class="onselect" bgcolor="<?php echo $bgcolor;?>" style="cursor:pointer;"
			onclick="Ext.DOM.DetailMapping('<?php echo $rows['AccountNo'];?>');" >
			<td class="content-first center"><?php echo ($no+1);?></td>
			<td class="content-middle left"><?php echo ($rows['CardNo']? $rows['CardNo'] : "-");?></td>
			<td class="content-middle left"><?php echo ($rows['AccountNo']?$rows['AccountNo'] : "-") ;?></td>
			<td class="content-middle left"><?php echo ($rows['ClassCard']? $rows['ClassCard'] : "-");?></td>
			<!-- t d class="content-lasted left"><?#php echo ($rows['DataFrom']? $rows['DataFrom'] : "-");?></t d -->
			<td class="content-lasted left"><?php echo ($rows['AmountWO']? $rows['AmountWO'] : "-");?></td>
		</tr>	
  <?php $no++; endforeach;?>
  <?php } ?>
  </tbody>
 </table>
</fieldset> 
</div>