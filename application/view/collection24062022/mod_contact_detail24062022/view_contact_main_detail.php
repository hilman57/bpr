<?php 
__(javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/accounting.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/views/EUI_Contact.js', 'eui_'=>'1.0.0', 'time'=>time())
	))
);
?>

<?php $this ->load ->view('mod_contact_detail/view_contact_javascript_devel');?>

<div class="contact_detail">

<?php echo form()->hidden('ControllerId',NULL,_get_post("ControllerId"));?>
<?php echo form()->hidden('CustomerId',NULL,_get_post("CustomerId"));?>
<?php echo form()->hidden('LastCallPhoneNumber',NULL,$Dropdown['COLL_CUSTOMER_PHN']);?>
<?php echo form()->hidden('IsAksessAll',NULL,$Dropdown['COLL_AKSESS_ALL']);?>
<?php echo form()->hidden('CustomerAccountNo',NULL,$Customer['deb_acct_no']);?>
<?php echo form()->hidden('BucketRandomId',NULL,$BucketRandomId);?>

<script>
 var AutoMaximum = function() 
{
  var tabs_height = $("#main_content").height();	
  $('#tabs_list').mytab().tabs();  
  $('#tabs_list').mytab().tabs("option", "selected", 0);
  $("#tabs_list").mytab().close({}, false);
  $("#tabs_list").css({'margin-left' : '5px' });
  /*
	$("#tabs_list").tabs();
	$("#tabs_list").tabs('select',0);
	$("#tabs_list").css({'margin-left' : '5px' });
	*/
	$("#tabs_list-2,#tabs_list-3,#tabs_list-4,#tabs_list-5").css({
		height : (tabs_height -(tabs_height/8))
	})
}

 $('document').ready(function(){
	AutoMaximum();
 });
	 
	
 $("#main_content").resize(function(){
	AutoMaximum();
});

</script>
<style>
table{ padding:0px;}
.corner table td {
	padding:1px;
}
.ui-tabs .ui-tabs-nav li a { 
	padding: 0.2em 1em; 
}
.ui-tabs .ui-tabs-panel { 
padding: 0em 1.4em; 
}

</style>
<div id="tabs_list">

	<ul>
		<li class="ui-tab-li-first"><a href="#tabs_list-1">Account Information</a></li>
		<li class="ui-tab-li-middle"><a href="#tabs_list-2">Account Mapping</a></li>
		<li class="ui-tab-li-middle"><a href="#tabs_list-3">Payment History</a></li>
		<li class="ui-tab-li-middle"><a href="#tabs_list-4">SMS Inbox</a></li>
		<li class="ui-tab-li-lasted"><a href="#tabs_list-5">SMS Outbox</a></li>
	</ul>
	
	<!--
	<ul>
		<li><a href="#tabs_list-1">Account Information</a></li>
		<li><a href="#tabs_list-2">Account Mapping</a></li>
		<li><a href="#tabs_list-3">Payment History</a></li>
		<li><a href="#tabs_list-4">SMS Inbox</a></li>
		<li><a href="#tabs_list-5">SMS Outbox</a></li>
	</ul>-->
	
	<!-- load :: policy data -->
	<div id="tabs_list-1" style="background-color:#FFFFFF;overflow:auto;"> 
		<?php $this ->load->view('mod_contact_detail/view_contact_information');?>
	</div>
	
	<!-- load ::personal data -->
	<div id="tabs_list-2" style="background-color:#FFFFFF;overflow:auto;">
		<div id="first-content-map">
			<?php $this ->load->view('mod_contact_detail/view_contact_accountmaplist');?>
		</div>
		<div id="two-content-map">
			<?php $this ->load->view('mod_contact_detail/view_contact_accountmap');?>
		</div>
	</div>
	
	<div id="tabs_list-3" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_payment_history');?> 
	</div>
	
	<div id="tabs_list-4" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_contact_smsinbox');?> 
	</div>
	<div id="tabs_list-5" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_contact_smsoutbox');?> 
	</div>
</div>

<div id="Prospect_Pop" title="Prospect Note" style="display:none;width:400px;">
  	<div class="ui-widget-form-table-compact" style="width:99%;">
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell ui-widget-content-top " style="width:100%;">
				<fieldset class="corner" style="border-radius:3px;margin:0px;">
					<legend class="edit-users-x"> <span style="margin-left:8px;">Form Prospect</span></legend>
					<form name="frmProspectNote">
					<div class="ui-widget-form-table-compact">
						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">* Note</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left"><?php echo form()->textarea('text_prospect_notes','textarea', null,null, array("style"=> "width:400px;height:50px;") );?></div>
						</div>						
						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption"></div>
							<div class="ui-widget-form-cell center"></div>
							<div class="ui-widget-form-cell left">
								<?php echo form()->button('btnsavenote', 'button save', "&nbsp;Save&nbsp;&nbsp;", array("click" => "Ext.DOM.SaveProspect();") );?>
							</div>
						</div>
					</div>
					</form>
					
				</fieldset>
			</div>
			
		</div>
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell ui-widget-content-top " style="width:100%">
				<fieldset class="corner" style="border-radius:3px;margin:0px;">
				<legend class="icon-menulist"> <span style="margin-left:8px;">List Data</span></legend>	
					<div id="ui-widget-content-debitur-page" style="margin-top:-5px;"></div>
				</fieldset>
			</div>			
		</div>
	</div>
 </div>
 
</div>	
<div id="WindowUserDialog" > </div>