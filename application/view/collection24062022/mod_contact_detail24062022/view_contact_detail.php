<?php 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 * 
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 * @ example    : get by fields ID / Campaign ID
 */

$conn =& get_instance();
if( $conn ) 
{
 $conn->db->select('Field_Id');
 $conn->db->from('t_gn_field_campaign');
 $conn->db->where('CampaignId', $Customer['deb_cmpaign_id']);
 $conn->db->where('Field_Active',1);
 $conn->db->order_by('Field_Id','ASC');
 // echo $conn->db->_get_var_dump();
 
 
 foreach( $conn->db->get() -> result_assoc() as $rows )
 { 
	if( $Flexible =& _fldFlexibleLayout($rows['Field_Id']))
	{
		$Flexible -> _setTables('t_gn_debitur'); // rcsorce data
		//sementara hanya bisa 1x join
		$Flexible -> _setJoinSelect(array("CONCAT(t_gn_debitur_additional_address.ALAMAT,', RT. ',
								t_gn_debitur_additional_address.NO_RT,' RW. ',
								t_gn_debitur_additional_address.NO_RW,', KEL.',
								t_gn_debitur_additional_address.KEL_NAME,', KEC. ',
								t_gn_debitur_additional_address.KEC_NAME,', KAB. ',
								t_gn_debitur_additional_address.KAB_NAME,', PROV. ',
								t_gn_debitur_additional_address.PROP_NAME) AS AdditionalAddress",
								"CONCAT(t_gn_debitur_slik_address.Debitur_Alamat,', KEL.',
								t_gn_debitur_slik_address.Debitur_Kelurahan,', KEC. ',
								t_gn_debitur_slik_address.Debitur_Kecamatan,', ',
								t_gn_debitur_slik_address.Debitur_KabKotaKet,', KodePos. ',
								t_gn_debitur_slik_address.Debitur_KodePos) AS SlikAddress"));
		$Flexible -> _setTableJoin(array('t_gn_debitur_additional_address','t_gn_debitur_slik_address')); // rcsorce data
		// $Flexible -> _setTableJoinConds('deb_acct_no = t_gn_debitur_slik_address.deb_cardno'); // rcsorce data
		$Flexible -> _setTableJoinConds(array('deb_acct_no = t_gn_debitur_additional_address.Accno','deb_acct_no = t_gn_debitur_slik_address.deb_cardno')); // rcsorce data
		
		$Flexible -> _setCustomerId(array('deb_id' => $Customer['deb_id'] )); // set conditional array();
		$Flexible -> _Compile();
	}
 }
 
}	
// END OFF
?>
<!-- start phone detail --->
<?php $this->load->view("mod_contact_detail/view_contact_phone");?>

<?php $this->load->view("mod_contact_detail/view_contact_callhistory");?>
<!-- start phone detail --->

