<?php 
if(is_array($payment)) {
 echo "<table cellspacing='1' width='100%' align='left'>
	<thead>
		<tr>
			<th class='ui-corner-top ui-state-default first' height='19px'>No</th>
			<th class='ui-corner-top ui-state-default middle' height='19px'>Due Date</th>
			<th class='ui-corner-top ui-state-default lasted'>Payment Amount</th>
		</tr> 
	</thead>";
	
  $num = 0;	$no = 1;
  foreach( $payment as $tanggal => $average ) { 
	$color  = ( $num%2!=0? '#FFFFFF':'#FFFEEE'); 
	echo "
		<tbody>
			<tr bgcolor='{$color}'>
				<td class='content-first center'>{$no}</td>
				<td class='content-first center'>{$tanggal}</td>
				<td class='content-middle center'>". _getCurrency($average)."</td>
			</tr>
		</tbody>";
	$num++;	$no++;
  }	
 echo "<table>";	
} 
?>