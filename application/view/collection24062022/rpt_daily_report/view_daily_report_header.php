<?php 
 $UI = & get_instance();
  
?>

<!DOCTYPE html>
<html>
<head>
	<title>Enigma :: <?php echo $title; ?></title>
	<meta charset="utf-8">
	<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.grid.css?time=<?php echo time();?>" />
	<style>
	<!--
		div.navigation { }
		div.navigation ul{margin-left:-45px;font-family:Arial;font-size:16px;}
		div.navigation li{float:left;list-style-type:none;padding:4px 8px 4px 8px;margin-bottom:20px;margin-top:10px;color:#053077;}
		div.navigation li.first{border-left:0px solid #ddd;}
		div.navigation li.middle{border-left:1px solid #d1ccca;}
		div.navigation li.lasted{border-left:0px solid #ddd;}
		div.navigation li:hover{cursor:pointer;color:#e2ccc7;}
		p.clear{clear:both;border-bottom:1px solid #dddddd;} 
		h4 {margin-bottom:5px;margin-top:10px;color:#053077;padding:4px;}
	-->
	</style>
</head>
<body>
<div class='navigation'>
  <ul>
	<li class='first'><b>Daily</b></li>
	<li class='middle'><?php echo $title ?></li>
	<li class='middle'>Interval</li>
	<li class='lasted'>
		<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>
		<?php echo date('d/m/Y', strtotime( $UI->M_ReportDailyReport->start_date()));?>
		<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>
		<?php echo date('d/m/Y', strtotime( $UI->M_ReportDailyReport->end_date()));?>
	</li>
  </ul>
</div>
<p class='clear'></p>
