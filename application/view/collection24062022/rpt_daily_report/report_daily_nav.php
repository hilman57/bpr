<?php if(!_have_get_session('UserId')) exit('Expired Session ID '); ?>

<script>

Ext.document().ready(function(){
  Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
});

$('.date').datepicker({
	showOn : 'button', 
	buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	dateFormat : 'dd-mm-yy',
	readonly:true,
	onSelect	: function(date){
		if(typeof(date) =='string'){
			var x = date.split('-');
			var retur = x[2]+"-"+x[1]+"-"+x[0];
			if(new Date(retur) > new Date()) {
				Ext.Cmp($(this).attr('id')).setValue('');
			}
		}
	}
});



// Ext.DOM.ShowReport HTML

Ext.DOM.ShowReport = function() {
	var param = [];
		param['mode'] = "HTML";
		param['report_type'] = Ext.Cmp('report_type').getValue();
	 	
	Ext.Window({
		url : Ext.DOM.INDEX +"/ReportDailyReport/ShowReport/",
		param : Ext.Join([Ext.Serialize("frmReportDaily").getElement(), param ]).object()
	}).newtab();
}


/* excel MODEL **/

Ext.DOM.ShowExcel = function() {
	var param = [];
		param['mode'] = "EXCEL";
		param['report_type'] = Ext.Cmp('report_type').getValue();
	 	
	Ext.Window({
		url : Ext.DOM.INDEX +"/ReportDailyReport/ShowExcel/",
		param : Ext.Join([Ext.Serialize("frmReportDaily").getElement(), param ]).object()
	}).newtab();
}

</script>

<fieldset class="corner">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Order Report</span></legend>

<div id="panel-main-content">

<form name="frmReportDaily" id="frmReportDaily">
<div style='margin-top:20px;margin-bottom:20px;width:500px;'>
	<table border=0 cellpadding='8' cellspacing=8>
		<tr>
			<td class='text_caption  bottom'> Report Group </td>
			<td><?php echo form()->combo('report_group','select long', $ReportGroup);?></td>
		</tr>
		
		<tr>
			<td class='text_caption bottom'> Report Type </td>
			<td>
				<fieldset style='padding-left:0px;width:100px;background-color:#fffcfd;border:1px solid #eeeeee;resize:both;'>
					<legend style="background-color:#fffcfd;"> <a href="javascript:void(0);" onclick="Ext.Cmp('report_type').setChecked();">#ALL</a></legend>
					<div style='line-height:24px;padding-left:6px;width:300px;border:0px solid #eeeeee;overflow: auto;resize:both;'>
						<?php foreach( $ReportType as $k => $name ) : ?>
							<?php echo form()->checkbox('report_type', null, $k);?> <?php echo $name; ?><br>
						<?php endforeach; ?>	
					</div>	
				</fieldset>
			</td>
		</tr>
		
		<tr>
			<td class='text_caption  bottom'> Interval </td>
			<td>
				<?php echo form()->input('start_date','input_text date', null);?> &nbsp;-&nbsp;
				<?php echo form()->input('end_date','input_text date', null);?> 
			</td>
		</tr>
		<tr>
			<td ></td>
			<td>
				<?php echo form()->button('submit_report','page-go button', 'Submit',array("click"=>"Ext.DOM.ShowReport();"));?>
				<?php echo form()->button('submit_report','excel button', 'Export', array("click"=>"Ext.DOM.ShowExcel();") );?>
			</td>
		</tr>
	</table>
	</div>
</form>	
</div>
</fieldset>
