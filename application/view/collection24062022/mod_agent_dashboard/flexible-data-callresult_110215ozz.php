<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;Completed</legend>
	<table border=0 align="left" cellspacing=1 cellspacing="1px">
		<tr>
			<th class="font-standars ui-corner-top ui-state-default first" rowspan="2">&nbsp;Call Status</td>
			<th class="font-standars ui-corner-top ui-state-default first center" colspan="2">&nbsp;Summary</td>
			<th class="font-standars ui-corner-top ui-state-default first center" rowspan="2">&nbsp;Action</td>
		</tr>
		<tr>
			<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Customer</td>
			<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Policy</td>
		</tr>
	<?php 
	$spanData = "<img src=\"". base_url() ."library/gambar/icon/zoom.png\">";
	$no= 0;
	foreach( $CallResultData['Completed'] as $key => $rows ) { 
		$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	?>	
	<tr CLASS="onselect" bgcolor="  <?php echo $color;?>">
		<td class="content-first left" WIDTH="12%"><div class="text-content left-text"><?php echo ucfirst(strtolower($rows['CallReasonDesc'])); ?></div></td>
		<td class="content-middle center" WIDTH="12%"><div class="text-content center-text"><?php echo $rows['Jumlah']; ?></div></td>
		<td class="content-middle center" WIDTH="12%"><div class="text-content center-text"><?php echo $PolicyData['Completed'][$rows['CallReasonId']]['Jumlah']; ?></div></td>
		<td class="content-lasted center" WIDTH="12%">
				<span style="cursor:pointer;" onclick="javascript:ShowByResult('<?php echo $rows['CallReasonId'];?>');"><?php echo $spanData;?></span>
				
		</td>
	</tr>	
	<?php
		$total_rows +=$rows['Jumlah'];
		$total_policies +=$PolicyData['Completed'][$rows['CallReasonId']]['Jumlah'];
		$no++;
	} ?>	
	
	<tr>
		<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=1>&nbsp;<b>Total</b></td>
		<?php else : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=1>&nbsp;<b>Total</b></td>
		<?php endif; ?>
		<th class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<?php echo $total_rows; ?>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;<?php echo $total_policies; ?>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;<b></b>&nbsp;</td>
	</tr>
	</table>
</fieldset>

<fieldset class="corner" style="margin-top:10px;">
	<legend class="icon-customers">&nbsp;&nbsp;Pending</legend>
	<table border=0 align="left" cellspacing=1 cellspacing="1px">
		<tr>
			<th class="font-standars ui-corner-top ui-state-default first" rowspan="2">&nbsp;Call Status</td>
			<th class="font-standars ui-corner-top ui-state-default first center" colspan="2">&nbsp;Summary</td>
			<th class="font-standars ui-corner-top ui-state-default first center" rowspan="2">&nbsp;Action</td>
		</tr>
		<tr>
			<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Customer</td>
			<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Policy</td>
		</tr>
	<?php 
	$spanData = "<img src=\"". base_url() ."library/gambar/icon/zoom.png\">";
	$no= 0;
	foreach( $CallResultData['Uncompleted'] as $key => $rows ) { 
		$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	?>	
	<tr CLASS="onselect" bgcolor="  <?php echo $color;?>">
		<td class="content-first left" WIDTH="12%"><div class="text-content left-text"><?php echo ucfirst(strtolower($rows['CallReasonDesc'])); ?></div></td>
		<td class="content-middle center" WIDTH="12%"><div class="text-content center-text"><?php echo $rows['Jumlah']; ?></div></td>
		<td class="content-middle center" WIDTH="12%"><div class="text-content center-text"><?php echo $PolicyData['Uncompleted'][$rows['CallReasonId']]['Jumlah']; ?></div></td>
		<td class="content-lasted center" WIDTH="12%">
				<span style="cursor:pointer;" onclick="javascript:ShowByResult('<?php echo $rows['CallReasonId'];?>');"><?php echo $spanData;?></span>
				
		</td>
	</tr>	
	<?php
		$total_rows2 +=$rows['Jumlah'];
		$total_policies2 +=$PolicyData['Uncompleted'][$rows['CallReasonId']]['Jumlah'];
		$no++;
	} ?>
	
	<tr>
		<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=1>&nbsp;<b>Total</b></td>
		<?php else : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=1>&nbsp;<b>Total</b></td>
		<?php endif; ?>
		<th class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<?php echo $total_rows2; ?>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;<?php echo $total_policies2; ?>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;<b></b>&nbsp;</td>
	</tr>
	</table>
</fieldset>