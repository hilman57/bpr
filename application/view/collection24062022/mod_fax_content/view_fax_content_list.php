<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center"><span class="header_order">&nbsp;#</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order">&nbsp;No</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.OutboxStatusTime" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Date</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.OutboxFaxNumber" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Destination</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="b.DataOrigFilename" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Content</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.OutboxFaxFile" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Fax File</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.OutboxQueueTime" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Queue Date</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.OutboxCreatedBy" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Create By</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.OutboxStatus" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Status</span></th>	  
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order center" id ="a.OutboxPageTotal" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;# Page</span></th>	
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows ) { 
  $color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); 
  $FAX_FILES = preg_replace('/\.tiff/i','.pdf',$rows['OutboxFaxFile']); // convert PDF
 ?>
<tr class="onselect" bgcolor="<?php __($color); ?>">
	<td class="content-first"><?php __(form()->checkbox('OutboxId',null, $rows['OutboxId']));?></td>
  <td class="content-middle center"><?php __($no); ?></td>
	<td class="content-middle left"><?php __(($rows['OutboxStatusTime']?$rows['OutboxStatusTime']:'-')); ?></td>
	<td class="content-middle left"><?php __(($rows['OutboxFaxNumber']?$rows['OutboxFaxNumber']:'-')); ?></td>
	<td class="content-middle left"><?php __(($rows['DataOrigFilename']?$rows['DataOrigFilename']:'TEXT')); ?></td>
	<td class="content-middle left"><?php __(($rows['OutboxFaxFile']? $FAX_FILES :'-')); ?></td>
	<td class="content-middle left"><?php __(($rows['OutboxQueueTime']?$rows['OutboxQueueTime']:'-')); ?></td>
	<td class="content-middle left"><?php __(($rows['OutboxCreatedBy']?strtoupper($rows['OutboxCreatedBy']):'-')); ?></td>
	<td class="content-middle left"><?php __(($rows['OutboxStatus']?$fax_status[$rows['OutboxStatus']]:'-')); ?></td>
  <td class="content-lasted center"><?php __($rows['OutboxPageSent'].'/'.$rows['OutboxPageTotal']); ?></td>
</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>


