<script>
// get Event 
$(function(){
	Ext.Cmp('ActiveUserLevel').listener({
		onChange : function( e ){
			Ext.Ajax({
				url : Ext.DOM.INDEX+"/ModActiveMenu/MenuByUserLevel/",
				method : 'GET',
				param :{
					UserLevelId : ( $(this).val()=='' ? 0 : $(this).val() )
				}	
			}).load('div_ActiveMenuId');
		}
	});
	
Ext.DOM.MenuActiveDetail = function() {	
	Ext.Ajax
	({
		url : Ext.DOM.INDEX+"/ModActiveMenu/DetailMenuActive/",
		method : 'POST',
		param :{
			ActiveMenuId : Ext.Cmp('ActiveMenuId').getValue()
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function(response){
				if( response.success ){
				  (function( $obj ){ $obj.setValue(response.data.file_name); $obj.disabled(true); $obj.addClass('form-required');})(new Ext.Cmp('ActiveMenuName'));
				  (function( $obj ){ $obj.setValue(response.data.menu); $obj.disabled(true); $obj.addClass('form-required');})(new Ext.Cmp('ActiveMenuTitle'));
				} else {
				  (function( $obj ){ $obj.setValue(""); $obj.disabled(false); $obj.removeClass('form-required'); })( new Ext.Cmp('ActiveMenuName') );
				  (function( $obj ){ $obj.setValue(""); $obj.disabled(false); $obj.removeClass('form-required'); })( new Ext.Cmp('ActiveMenuTitle') );
			   }
			});
		}
	}).post();		
}

});
</script>
<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
<fieldset class="corner" style="background-color:white;margin:3px;">
 <legend class="icon-application">&nbsp;&nbsp;&nbsp;Add Active Menu</legend>	
 <form name="AdditionalPhoneType">
	<table cellpadding="6px;">
<?php	
	foreach( $FieldName as $key => $label ) 
	{
		if(!in_array($label, $HTML['primary'])) 
		{
		
			// input 
			
			if( in_array($label, $HTML['input']) )
			{
				echo "<tr>
						<td class=\"text_caption\">* " . ucfirst($TitleLabel[$label]) . " </td>
						<td>" . form()->input($label,'input_text long') . " </td>
					</tr> ";
			}	
			
			// input 
			
			if( in_array($label, $HTML['combo']) )
			{
				echo "<tr>
						<td class=\"text_caption\">* " . ucfirst($TitleLabel[$label]) . " </td>
						<td><span id=\"div_$label\">" . form()->combo($label,'select long', $HTML['option'][$label]) . "</span></td>
					</tr> ";
			}	
		}	
	} 
	
?>	
<tr>
	<td class="text_caption">&nbsp;</td>
	<td>
		<input type="button" class="save button" onclick="Ext.DOM.Save();" value="Save">
		<input type="button" class="close button" onclick="Ext.Cmp('top-panel').setText('');" value="Close">
	</td>
	</tr>
</table>
</form>
</fieldset>
</div>