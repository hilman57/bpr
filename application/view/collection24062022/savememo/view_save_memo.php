<!DOCTYPE html>
<html>
<head>
	<title>Memo</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />
	<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
	<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script> 
	<script src="<?php echo base_url('tinymce/tinymce.min.js') ?>"></script>
	<script type="application/javascript">
    tinymce.init({
            selector: "textarea",
            plugins: [
                    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor codesample"
            ],
 
            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft codesample",
 
            menubar: false,
            toolbar_items_size: 'small',
 
            style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
 
            templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
            ]
    }); 
    </script>
	<style>
		.info {
			background-color: #e7f3fe;
			border-left: 6px solid #2196F3;
		}
	</style>
</head>
<body>
	<div id="result_content_add" class="box-shadowx" style="margin-top:10px;">
		<fieldset class="corner" style="background-color:white;margin:3px;">
			<legend class="icon-application">&nbsp;&nbsp;&nbsp;Add Memo </legend>
			<form name="frmMemo" method="post">
				<!--<form action="<?php //echo base_url('index.php/SaveMemo/_Save'); ?>" method="post">-->
					<table cellpadding="6px;">
						<tr>
							<td class='font-standars text_caption bottom'>Comment :</td>
							<td><input type="hidden" id="NoteId" name="NoteId" value="<?php echo $Notes['NoteId'];?>"></td>
							<td><textarea id="Note" name="Note"><?php echo $Notes['Note']; ?></textarea></td>
						</tr>
						<tr>
							<td>
								<button type="submit" name="submitForm" value="formSave">SAVE</button><td>
								<!--<input type="button" class="button close" onclick="if(confirm('Do you want to exit from this session?')){window.close()};" value="Exit"  >-->
							</td>
						</tr>
						<tr>
							<div class="info">
								<p><strong>Info!</strong> *Please Press F5 if Your Notes not loaded</p>
							</div> 
						</td>
					</table>
			</form>
		</fieldset>
	</div>
</body>
</html>