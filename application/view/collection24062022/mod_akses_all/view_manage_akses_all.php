<?php
/* @ def 	 : view upload manual
 * 
 * @ param	 : sesion  
 * @ package : bucket data 
 */
 
?>
<div class="box-shadow" style="padding:10px;">
<fieldset class='corner'>

<legend class="icon-campaign">&nbsp;&nbsp;Distribute Data</legend>	
<div style="border-left:0px solid #dddddd;text-align:top;margin-top:-10px;">
	<table align="left" border=0 cellpadding="5px">
		<tr>
			<td class="left text_caption bottom" style='height:24px;'># Assign Type &nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><?php echo form() -> combo('Assign_Type','select long', (isset($Assign_Type) ? $Assign_Type : null), null, array('change'=>'getAssignTo();')); ?></td>
		</tr>
		
		<tr>
			<td class="left text_caption bottom" style='height:24px;'># Assign To&nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><span id="Assign-level"><?php echo form() -> combo('Assign_To','select long'); ?></td></span>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td class="left bottom" style='padding-left:12px;' colspan=2> 
				<input type="button" class="save button" onclick="Ext.DOM.Assign();" value="Assign">
				<input type="button" class="save button" onclick="Ext.DOM.Reset();" value="Reset">
				<input type="button" class="save button" onclick="Ext.DOM.AssignAll();" value="Assign All">
				<input type="button" class="save button" onclick="Ext.DOM.ResetAll();" value="Reset All">
				<input type="button" class="close button" onclick="Ext.DOM.ClosePanel();" value="Close">
			</td>
		</tr>
	</table>
</div>
</fieldset>
</div>