<?php echo javascript(); ?>

<script type="text/javascript">

Ext.document('document').ready( function(){

/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */
 
Ext.DOM.StaffAvailable = function(){
 Ext.Ajax
 ({
	url 	: Ext.DOM.INDEX+'/QtyStaffGroup/StaffAvailable/',
	method 	: 'GET',
	param 	: {
		time : Ext.Date().getDuration()
		}	
	}).load('content-agent-staff-available');
 }

// loader  :
Ext.DOM.StaffAvailable();
	
/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */
 
Ext.DOM.StaffByGroup = function(){
 Ext.Ajax
 ({
	url 	: Ext.DOM.INDEX+'/QtyStaffGroup/StaffByGroup/',
	method 	: 'GET',
	param 	: {
		time : Ext.Date().getDuration()
		}	
	}).load('content-agent-staff-group');
}

// loader  :
Ext.DOM.StaffByGroup();


/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param :QualityStaffId
 * @ param : -
 */

Ext.DOM.AddAvailableGroup = function(){
Ext.Ajax
 ({
	url 	: Ext.DOM.INDEX+'/QtyStaffGroup/AddAvailableSkill/',
	method 	: 'POST',
	param 	: {
		time : Ext.Date().getDuration(),
		QualityStaffId : Ext.Cmp('QualityStaffId').getChecked()
	},

	ERROR : function(e){
		Ext.Util(e).proc(function(items){
			if( items.success){
				Ext.Msg("Add Quality Skill").Success();
				Ext.DOM.StaffByGroup();	
			}
			else{
				Ext.Msg("Add Quality Skill").Failed();
			}
		});
	}		
  }).post();
}

/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Quality_Group_Id
 * @ param : Quality_Skill_Id
 */

 Ext.DOM.AssignQualityGroup = function(){
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX+'/QtyStaffGroup/AssignQualitySkill/',
		method 	: 'POST',
		param 	: {
			time : Ext.Date().getDuration(),
			Quality_Group_Id : Ext.Cmp('Quality_Group_Id').getChecked(),
			Quality_Skill_Id : Ext.Cmp('Quality_Skill_Id').getValue(),
		},

		ERROR : function(e){
			Ext.Util(e).proc(function(items){
				if( items.success){
					Ext.Msg("Assign Quality Skill").Success();
					Ext.DOM.StaffByGroup();	
				}
				else{
					Ext.Msg("Assign Quality Skill").Failed();
				}
			});
		}		
  }).post();
 }
 
 /* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Quality_Group_Id
 * @ param : -
 */
 
 Ext.DOM.RemoveQualityGroup = function(){
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX+'/QtyStaffGroup/RemoveQualitySkill/',
		method 	: 'POST',
		param 	: {
			time : Ext.Date().getDuration(),
			Quality_Group_Id : Ext.Cmp('Quality_Group_Id').getChecked()
		},

		ERROR : function(e){
			Ext.Util(e).proc(function(items){
				if( items.success){
					Ext.Msg("Remove Quality Skill").Success();
					Ext.DOM.StaffByGroup();	
				}
				else{
					Ext.Msg("Remove Quality Skill").Failed();
				}
			});
		}		
  }).post();
 }

 /* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Quality_Group_Id
 * @ param : -
 */
 
 Ext.DOM.EmptyQualityGroup = function(){
  Ext.Ajax ({
		url 	: Ext.DOM.INDEX+'/QtyStaffGroup/ClearQualitySkill/',
		method 	: 'POST',
		param 	: {
			time : Ext.Date().getDuration(),
			Quality_Group_Id : Ext.Cmp('Quality_Group_Id').getChecked()
		},

		ERROR : function(e){
			Ext.Util(e).proc(function(items){
				if( items.success){
					Ext.Msg("Clear Quality Skill").Success();
					Ext.DOM.StaffByGroup();	
				}
				else{
					Ext.Msg("Clear Quality Skill").Failed();
				}
			});
		}		
  }).post();
 }

 /* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */


});

</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;Quality Staff Available</legend>	
	<div id="content-agent-staff-available"> </div>
</fieldset>


<fieldset class="corner" style='margin-top:20px;'> 
	<legend class="icon-customers">&nbsp;&nbsp;Quality Staff Group</legend>	
	<div id="content-agent-staff-group"> </div>
</fieldset>