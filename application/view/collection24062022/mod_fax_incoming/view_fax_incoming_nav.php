<?php 
echo javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time())
));
?>

<script type="text/javascript">
/* create object **/
Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 
var Reason = [];

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* catch of requeet accep browser **/
var datas = {
	faxnumber 	: '<?php echo _get_post('faxnumber');?>',
	agent_id 	: '<?php echo _get_post('agent_id');?>',
	//agent_group : '<?php echo _get_post('agent_group');?>',
	start_time 	: '<?php echo _get_post('start_time');?>',
	end_time 	: '<?php echo _get_post('end_time');?>',
	order_by	: '<?php echo _get_post('order_by');?>',
	type		: '<?php echo _get_post('type');?>'
}
			
/* assign navigation filter **/

var navigation = {
	custnav : Ext.DOM.INDEX +'/FaxIncoming/index/',
	custlist : Ext.DOM.INDEX +'/FaxIncoming/content/'
}
		
/* assign show list content **/
		
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/* function searching customers **/
	
var searchCustomer = function()
{
	Ext.EQuery.construct(navigation,Ext.Join([Ext.Serialize('frmSearch').getElement()]).object() );
	Ext.EQuery.postContent();
}

// simple clear under form document

Ext.DOM.Clear = function(){
	Ext.Serialize('frmSearch').Clear();
}

// Download 

Ext.DOM.Download = function(){
  var InboxId = Ext.Cmp('InboxId').getChecked();
  if( InboxId.length > 0 ) 
  {
		Ext.Window({ 
			url : Ext.DOM.INDEX +'/FaxIncoming/Download/', 
			param : {
				InboxId : InboxId
			}	
		}).newtab();
  }
  else{
	Ext.Msg("No selected rows ").Info();
  }
}



// show fax 

Ext.DOM.ShowFax = function()
{
var InboxId = Ext.Cmp('InboxId').getChecked();
if( InboxId.length > 0 )
{
	Ext.Window
	({
		url   : Ext.DOM.INDEX +'/FaxIncoming/ViewFaxFile/',
		width : ( Ext.query(window).width()/3), height: Ext.query(window).height(),
		left  : Ext.query(window).width(),
		param : {
			time: Ext.Date().getTime(),
			InboxId: Ext.Cmp('InboxId').getChecked()
		}
  }).popup();
 }
else{
		Ext.Msg('Please select rows ').Info();
	}
}


/* 
 * @ def : play
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
$(function(){
	$('#toolbars').extToolbars
	({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Search'],['Clear'],['Show'],['Download']],
		extMenu   : [['searchCustomer'],['Clear'],['ShowFax'],['Download']],
		extIcon   : [['zoom.png'], ['cancel.png'],['page_white_magnify.png'],['page_white_put.png']],
		extText   : true,
		extInput  : true,
		extOption : [{
			render : 5,
			type   : 'label',
			id     : 'voice-list-wait', 	
			name   : 'voice-list-wait',
			label :'<span style="color:#dddddd;">-</span>'
		}]
	});
	
	$('.box').datepicker({
		showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly: true, 
		dateFormat:'dd-mm-yy',readonly:true
	});
});



</script>

<!-- start : content -->
<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
	
	<div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
		<form name="frmSearch">
		<table cellpadding="3px;" border=0>
		<tr>
			<td class="text_caption bottom"> Fax Number</td>
			<td class='bottom'><?php echo form() -> input('faxnumber','input_text long',_get_post('faxnumber')); ?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"> Is Read</td>
			<td class='bottom'><?php echo form() -> combo('agent_id','select long',array('1'=>'READ','0' => 'UNREAD'),_get_post('agent_id')); ?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"> Interval </td>
			<td class='bottom'>
			<?php echo form() -> input('start_time','input_text box',_get_post('start_time')); ?> - 
			<?php echo form() -> input('end_time','input_text box',_get_post('end_time')); ?> </td>
		</tr>
		
		</table>
		</form>
	</div>
	</div>
	
	<div id="toolbars"></div>
	<div id="play_panel"></div>
	<div id="recording_panel" class="box-shadow">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	