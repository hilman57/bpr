<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.DataId" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;No</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.InboxSenderNo" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Fax Number</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.InboxFaxFile" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;File Name</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.InboxReceiveTime" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Receive Date</span></th>    
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.InboxPageReceived" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Page Receive</span></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.InboxPageReceived" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Dispatch Status</span></th>	
		
		
		
		<th nowrap class="font-standars ui-corner-top ui-state-default first center"><span class="header_order" id ="a.InboxReadStatus" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Status</span></th>	
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows ){ 
	$FAX_FILES= preg_replace('/\.tiff/i','.pdf',$rows['InboxFaxFile']); // convert PDF
  $color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); ?>
<tr class="onselect" bgcolor="<?php __($color); ?>">
	<td class="content-first"><?php __(form()->radio('InboxId',null, $rows['InboxId']));?></td>
	<td class="content-middle"><?php __($no); ?></td>
	<td class="content-middle"><?php __($rows['InboxSenderNo']); ?></td>
	<td class="content-middle"><?php __($FAX_FILES); ?></td>
	<td class="content-middle"><?php __($rows['InboxReceiveTime']); ?></td>
	<td class="content-middle center"><?php __($rows['InboxPageReceived']); ?></td>
	<td class="content-middle center"><?php __(($rows['DispatchStatus']?'YES':'NO')); ?></td>
	<td class="content-lasted center"><?php __(($rows['InboxReadStatus'] ? 'READ' : 'UNREAD')); ?></td>
	
</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>


