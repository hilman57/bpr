<script>
/* Ext.DOM.UploadFax **/

Ext.DOM.UploadFax  = function(){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/FaxContent/UploadFax/',
		file 	: 'file_fax',
		method 	: 'POST',
		param 	: {
			time : Ext.Date().getTime()
		},
		complete : function(e){
			console.log(e);
		}
	 }).upload();
}

Ext.DOM.CloseFax = function(){
	Ext.Cmp("panel-call-center").setText('');
}


</script>
<div>
	<table cellspacing=2 cellpadding=2 >
		<tr>
			<td class="text_caption bottom"> File Name </td>
			<td class="bottom"><?php __(form()->upload('file_fax')); ?></td>
		</tr>
		<tr height=28>
			<td class="text_caption bottom" colspan="2"><span id="loading-title-ajax"></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td>
				<?php __(form()->button('file_fax_submit','button upload','Upload', array('click'=>'Ext.DOM.UploadFax();') )); ?> 
				<?php __(form()->button('file_fax', 'button close','Close',array('click'=>'Ext.DOM.CloseFax();'))); ?>
			</td>
		</tr>
	</table>
</div>
