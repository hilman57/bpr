<?php echo javascript(); ?>
<script type="text/javascript">
	// @ pack : Hidden --------------------------------------------------------

	Ext.document('document').ready(function () {
		Ext.Cmp('legend_title').setText(Ext.System.view_file_name());
	});

	// @ pack : Hidden --------------------------------------------------------

	var datas = {
		order_by: '<?php echo _get_post('
		order_by ');?>',
		type: '<?php echo _get_post('
		type ');?>',
		keywords: '<?php echo _get_post('
		keywords ');?>'
	}

	// @ pack : upload data listener 

	Ext.DOM.UploadData = function () {
		Ext.Ajax({
			url: Ext.DOM.INDEX + '/MgtBucket/ManualUpload/',
			param: {
				time: Ext.Date().getDuration(),
				templatetype: 1
			}
		}).load("span_top_nav");
	}

	Ext.DOM.TemplateRank = function () {
		Ext.Ajax({
			url: Ext.DOM.INDEX + '/MgtBucket/TemplateRank/',
			param: {
				time: Ext.Date().getDuration(),
				templatetype: 1
			}
		}).load("span_top_nav");
	}

	// @ pack : upload data listener 

	Ext.DOM.Upload = function () {
		// alert('haloo')
		// return false
		Ext.Cmp("loading-image").setText("<img src=" + Ext.DOM.LIBRARY +
			"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
		Ext.Ajax({
			url: Ext.DOM.INDEX + '/MgtBucket/UploadBucketQuest/',
			method: 'POST',
			param: {
				TemplateId: Ext.Cmp('upload_template').getValue(),
				CampaignId: Ext.Cmp('upload_campaign').getValue()
			},
			complete: function (fn) {
				var ERR = eval(fn.target.DONE);
				try {
					if (ERR) {
						var CALLBACK = JSON.parse(fn.target.responseText);
						if ((typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null)) {
							var msg = "\n";
							for (var i in CALLBACK.mesages) {
								msg += "Failed : " + CALLBACK.mesages[i].N + ",\nSuccess : " + CALLBACK
									.mesages[i].Y + "\n Blacklist : " + CALLBACK.mesages[i].B +
									",\nDuplicate : " + CALLBACK.mesages[i].D + "\nDuplicate(s) : " +
									CALLBACK.mesages[i].X + "\n";
							}
							Ext.Msg(msg).Error();
						} else {
							Ext.Msg(CALLBACK.mesages).Info();
						}

						Ext.Cmp("loading-image").setText('');
					}
				} catch (e) {
					alert(e);
				}
			}
		}).upload()
	}


	// @ pack : upload data listener 

	Ext.DOM.ClosePanel = function () {
		Ext.Cmp('span_top_nav').setText("");
	}


	// @ pack : Hidden -------------------------------------------------------- UploadRank

	Ext.query(function () {
		Ext.query('#toolbars').extToolbars({
			extUrl: Ext.DOM.LIBRARY + '/gambar/icon',
			extTitle: [
				['Export'],
				['Upload Data'],
				['Search File :'],
				['Search']
			],
			extMenu: [
				['SaveExcel'],
				['UploadData'],
				[''],
				['Search']
			],
			extIcon: [
				['database_save.png'],
				['database_go.png'],
				[''],
				['zoom.png']
			],
			extText: true,
			extInput: true,
			extOption: [{
				render: 3,
				type: 'text',
				id: 'akeywords',
				name: 'akeywords',
				value: '<?php echo _get_post('
				keywords ');?>',
				width: 200
			}]
		});

		// @ pack : Hidden --------------------------------------------------------
		Ext.Cmp("akeywords").setFocus();

	});

	// @ pack : Hidden --------------------------------------------------------
	Ext.Cmp("akeywords").listener({
		onKeyUp: function (evt) {
			if (evt.keyCode == 13) {
				Ext.DOM.Search();
			}
		}
	})

	// @ pack : Hidden --------------------------------------------------------

	Ext.EQuery.TotalPage = '<?php __($page->_get_total_page()); ?>';
	Ext.EQuery.TotalRecord = '<?php __($page->_get_total_record()); ?>';

	// @ pack : Hidden --------------------------------------------------------

	Ext.DOM.navigation = {
		// custnav: Ext.DOM.INDEX + '/Uploadquestioner/index',
		custlist: Ext.DOM.INDEX + '/Uploadquestioner/Content'
	};


	// @ pack : Hidden --------------------------------------------------------

	// Ext.EQuery.construct(Ext.DOM.navigation, datas)
	Ext.EQuery.postContentList();

	// @ pack : Hidden --------------------------------------------------------

	// Ext.DOM.Search = function () {
	// 	Ext.EQuery.construct(Ext.DOM.navigation, {
	// 		keywords: Ext.Cmp("akeywords").getValue()
	// 	});

	// 	Ext.EQuery.postContent();
	// }

	// @ pack : Hidden --------------------------------------------------------

	Ext.DOM.Hidden = function () {
		var UploadId = Ext.Cmp('ftp_upload_id').getValue();
		if (Ext.Cmp('ftp_upload_id').empty()) {
			Ext.Msg("Please select a row(s)").Info();
		} else {
			Ext.Ajax({
				url: Ext.DOM.INDEX + '/Uploadquestioner/hidden/',
				method: 'POST',
				param: {
					FTP_UploadId: UploadId,
					Active: 0
				},
				ERROR: function (e) {
					Ext.Util(e).proc(function (hidden) {
						if (hidden.success) {
							Ext.Msg("Hidden rows").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Hidden rows").Failed();
						}
					});
				}
			}).post();
		}

	}

	// @ pack : Delete --------------------------------------------------------

	Ext.DOM.Delete = function () {
		var UploadId = Ext.Cmp('ftp_upload_id').getValue();
		if (Ext.Cmp('ftp_upload_id').empty()) {
			Ext.Msg("Please select a row(s)").Info();
		} else {
			if (Ext.Msg("Do you want to deleted this row(s) ").Confirm()) {
				Ext.Ajax({
					url: Ext.DOM.INDEX + '/Uploadquestioner/Delete/',
					method: 'POST',
					param: {
						FTP_UploadId: UploadId
					},
					ERROR: function (e) {
						Ext.Util(e).proc(function (Delete) {
							if (Delete.success) {
								Ext.Msg("Delete rows").Success();
								Ext.EQuery.postContent();
							} else {
								Ext.Msg("Delete rows").Failed();
							}
						});
					}
				}).post();
			}
		}

	}

	// @ pack : SaveExcel --------------------------------------------------------

	Ext.DOM.Show = function () {

		var UploadId = Ext.Cmp('ftp_upload_id').getValue();
		if (Ext.Cmp('ftp_upload_id').empty()) {
			Ext.Msg("Please select a row(s)").Info();
		} else {
			Ext.Ajax({
				url: Ext.DOM.INDEX + '/Uploadquestioner/hidden/',
				method: 'POST',
				param: {
					FTP_UploadId: UploadId,
					Active: 1
				},
				ERROR: function (e) {
					Ext.Util(e).proc(function (hidden) {
						if (hidden.success) {
							Ext.Msg("Show rows").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Show rows").Failed();
						}
					});
				}
			}).post();
		}

	}

	// @ pack : SaveExcel --------------------------------------------------------

	Ext.DOM.SaveExcel = function () {
		var ChecklistId = Ext.Cmp('ftp_upload_id').getValue();
		if (ChecklistId != '') {
			if (Ext.Msg("Do you wan to download this file ?")) {
				Ext.Window({
					url: Ext.DOM.INDEX + "/Uploadquestioner/SaveExcel/",
					param: {
						Ftp_upload_id: ChecklistId
					}
				}).newtab();
			}
		} else {
			Ext.Msg("Please select a row(s)").Info();
		}
	}

	// @ pack : SaveExcel --------------------------------------------------------


	var CheckDuplicateData = function () {
		var ChecklistId = doJava.checkedValue('ftp_upload_id');
		if (ChecklistId != '') {
			doJava.File = '../class/class.ftpupload.info.php';
			doJava.Params = {
				action: 'check_duplicate',
				Ftp_upload_id: ChecklistId
			}
			window.open(doJava.File + '?' + doJava.ArrVal())
		}
	}
	// --------------------------------------------------------
</script>

<fieldset class="corner">

	<legend class="icon-campaign">&nbsp;&nbsp;Upload Data </legend>
	<div style="border-left:0px solid #dddddd;text-align:top;margin-top:-10px;">
		<table align="left" border="0" cellpadding="5px">
			<tbody>
				<tr>
					<td class="left text_caption bottom" style="height:24px;"># Template &nbsp;</td>
					<td class="center text_caption bottom">:</td>
					<td class="left bottom"> <select type="combo" name="upload_template" id="upload_template"
							class="select long">
							<option value=""> --choose --</option>
							<option value="31">UPLOAD_QUESTIONER</option>
						</select></td>
				</tr>



				<tr>
					<td class="left text_caption bottom" style="height:24px;"># File location&nbsp;</td>
					<td class="center text_caption bottom">:</td>
					<td class="left bottom">
						<form action="javascript:void(0);" method="post" enctype="multipart/form-data">
							<div class="browse">
								<input type="file" name="fileToupload[]" id="fileToupload">
							</div>
						</form>
					</td>
					<!--<td class="left bottom"> </td>-->
				</tr>
				<tr>
					<td class="left text_caption bottom" style="height:24px;">&nbsp;</td>
					<td class="center text_caption bottom">&nbsp;</td>
					<td class="left bottom"><span id="loading-image"></span></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="left bottom" style="padding-left:12px;" colspan="2">
						<input type="button" class="save button" onclick="Ext.DOM.Upload();" value="Upload">
						<input type="button" class="close button" onclick="Ext.DOM.ClosePanel();" value="Close">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</fieldset>