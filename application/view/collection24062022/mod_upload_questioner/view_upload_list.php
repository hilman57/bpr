<fieldset class="corner">

	<legend class="icon-campaign">&nbsp;&nbsp;Upload Data </legend>
	<div style="border-left:0px solid #dddddd;text-align:top;margin-top:-10px;">
		<table align="left" border="0" cellpadding="5px">
			<tbody>
				<tr>
					<td class="left text_caption bottom" style="height:24px;"># Template &nbsp;</td>
					<td class="center text_caption bottom">:</td>
					<td class="left bottom"> <select type="combo" name="upload_template" id="upload_template"
							class="select long">
							<option value=""> --choose --</option>
							<option value="31">UPLOAD_QUESTIONER</option>
						</select></td>
				</tr>



				<tr>
					<td class="left text_caption bottom" style="height:24px;"># File location&nbsp;</td>
					<td class="center text_caption bottom">:</td>
					<td class="left bottom">
						<form action="javascript:void(0);" method="post" enctype="multipart/form-data">
							<div class="browse">
								<input type="file" name="fileToupload[]" id="fileToupload">
							</div>
						</form>
					</td>
					<!--<td class="left bottom"> </td>-->
				</tr>
				<tr>
					<td class="left text_caption bottom" style="height:24px;">&nbsp;</td>
					<td class="center text_caption bottom">&nbsp;</td>
					<td class="left bottom"><span id="loading-image"></span></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="left bottom" style="padding-left:12px;" colspan="2">
						<input type="button" class="save button" onclick="Ext.DOM.Upload();" value="Upload">
						<input type="button" class="close button" onclick="Ext.DOM.ClosePanel();" value="Close">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</fieldset>