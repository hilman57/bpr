<fieldset  class='corner' style='margin:8px 15px 0px 10px;'>
	<legend class='icon-menulist'>&nbsp;&nbsp;Add Work Project </legend>
	<div>
	<form name="WorkFormTemplate">
		<table>
			<tr>
				<td class='text_caption bottom'>* Work Project Code</td>
				<td><?php __(form()->input('ProjectCode','select long')); ?></td>
				
			</tr>
			
			<tr>
				<td class='text_caption bottom'>* Work Project Name </td>
				<td><?php __(form()->input('ProjectName','select long')); ?></td>
			</tr>
			<tr>
				<td class='text_caption bottom'>* Work Project Status</td>
				<td><?php __(form()->combo('ProjectFlags','select long', array('0' => 'Not Active','1' => 'Active'))); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>&nbsp;</td>
				<td>
					<?php __(form()->button('FtpSaveButton','save button', 'Save', array("click" =>"Ext.DOM.WorkSaveButton();"))); ?>
					<?php __(form()->button('FtpCloseButton','close button', 'Cancel', array("click" =>"Ext.Cmp('span_top_nav').setText('');"))); ?>
				
					</td>
			</tr>
		</table>
		</form>
	</div>
</fieldset>