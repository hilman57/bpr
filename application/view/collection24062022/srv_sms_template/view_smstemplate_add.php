<div id="result_content_add" class="box-shadowx" style="margin-top:10px;">
<fieldset class="corner" style="background-color:white;margin:3px;">
<legend class="icon-application">&nbsp;&nbsp;&nbsp;Add SMS Template </legend>	
<form name="frmAddTemplateSMS">
<table cellpadding="6px;">
<?php	
  foreach( $FieldName as $key => $label ) 
	{
		if(!in_array($label, $html['primary'])) 
		{	
		
			// input 
			if( in_array($label, $html['input']) ) {
				echo "<tr>
						<td class=\"text_caption bottom\"># " . ucfirst($labels[$label]) . "&nbsp;:</td>
						<td>" . form() -> input($label,'input_text long', $object[$label]) . " </td>
					</tr> ";
			}	
			
			// input 
			
			if( in_array($label, $html['combo']) )
			{
				echo "<tr>
						<td class=\"text_caption bottom\"># " . ucfirst($labels[$label]) . "&nbsp;:</td>
						<td>" . form() -> combo($label,'select long', $dropdown[$label], $object[$label]) . " </td>
					</tr> ";
			}

			if( in_array($label, $html['textarea']) )
			{
				echo "<tr>
						<td class=\"text_caption\"># " . ucfirst($labels[$label]) . "&nbsp;:</td>
						<td>" . form() -> textarea($label,'textarea long', $object[$label], null, array("style"=>"text-align:justify;width:350px;height:100px;") ) . " </td>
					</tr> ";
			}		
		}
		// hidden edit Mode
		else
		{
			echo form() -> hidden($label,null, $object[$label]);	
		}	
	} 
	
	echo "<tr>
			<td class=\"text_caption\">&nbsp;</td>
			<td>"
				. form() ->button('button', 'update button', 'Save', array("click" => "Ext.DOM.Save();")) .
				  form() ->button('button', 'close button', 'Close', array("click" => "Ext.Cmp('top-panel').setText('');")) .
			"</td>
		 </tr> ";
?>	
</table>
 </form>
</fieldset>
</div>