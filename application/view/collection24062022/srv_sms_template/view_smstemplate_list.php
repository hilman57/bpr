<?php  
/** set label style on selected **/
if(!function_exists('word_wrap')){
  function word_wrap($text = null , $int = 120 )
 {
	return wordwrap($text, $int,"\r\n");		
  }
}

/** get label header attribute **/

page_set_style("background-color", "#FFFCCC");
page_set_style("font-color", "#022706");
page_set_style("font-size", "12px");
page_set_style("line-height", "22px");
page_set_style("text-align", "justify");
page_set_style("padding","4px");

/** get label header attribute **/

page_set_align('GroupTplName', 'left');
page_set_align('SmsTplSubject', 'left');
page_set_align('SmsTplContent', 'left');
page_set_align('SmsTplFlags', 'center');

/** get label header attribute **/

$call_user_func = array('SmsTplContent'=>'word_wrap', 'BalanceAffterPay'=>'_getCurrency');
$call_link_wrap = array('GroupTplName'=> 'nowrap','SmsTplSubject'=> 'nowrap');
$call_link_witdh = array('GroupTplName'=>'20%','SmsTplSubject'=>'20%', 'SmsTplContent'=>'50%','SmsTplFlags'=>'20%');
$call_link_style = array('SmsTplContent'=>'text-align:justify;padding:4px;line-height:22px;font-size:12px;color:#013162;');

/** get label header attribute **/

$labels  =& page_labels();
$primary =& page_primary();
$align   =& page_get_align();
 
?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('<?php __($primary);?>').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No.</th>	
		<?php foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); ?>
		<th nowrap class="font-standars ui-corner-top ui-state-default" align="<?php echo $align[$Field]; ?>"> 
		<span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach($page->result_assoc() as $rows) 
{
  $color= ($no%2!=0?'#FAFFF9':'#FFFFFF');
?>
	<tr class="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first center"> <?php echo form()->checkbox($primary,null,$rows[$primary], array("change"=>"Ext.Cmp('$primary').oneChecked(this);") );?></td>
		<td class="content-middle center"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) :  
			$color=&page_column($Field);  ?>
			<td class="content-middle " <?php echo $color; ?> 
				width="<?php echo $call_link_witdh[$Field];?>" 
				align="<?php echo $align[$Field]; ?>" 
				<?php echo $call_link_wrap[$Field]; ?>
				style="<?php echo $call_link_style[$Field];?>" >
				<?php echo page_call_function($Field, $rows, $call_user_func); ?>
			</td>
		<?php endforeach;?>
	</tr>
</tbody>
<?php
	$no++; 
};
?>
</table>
	
<!-- END OF FILE  -->
<!-- location : // ../application/layout/user/view_user_list.php -->