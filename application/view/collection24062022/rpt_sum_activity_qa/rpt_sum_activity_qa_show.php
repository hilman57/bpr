<html>
<head>
	<?php $this-> load -> view('rpt_sum_activity_qa/rpt_sum_activity_qa_style'); ?>
<title>Summary QA Activity</title>
</head>

<body>

	<table border=0 cellpadding=0 cellspacing=0 width=1170 class=xl9723788 style='border-collapse:collapse;table-layout:fixed;width:878pt'>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl9923788 width=19 style='height:12.75pt;width:14pt'><a name="RANGE!A1:I37">&nbsp;</a></td>
			<td class=xl10023788 width=122 style='width:92pt'>&nbsp;</td>
			<td class=xl10023788 width=17 style='width:13pt'>&nbsp;</td>
			<td class=xl10023788 width=195 style='width:146pt'>&nbsp;</td>
			<td class=xl10023788 width=124 style='width:93pt'>&nbsp;</td>
			<td class=xl10023788 width=104 style='width:78pt'>&nbsp;</td>
			<td class=xl10023788 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl10023788 width=464 style='width:348pt'>&nbsp;</td>
			<td rowspan=20 class=xl15423788 width=19 style='width:14pt'>&nbsp;</td>
		</tr>
		<tr height=27 style='height:20.25pt'>
			<td height=27 class=xl10123788 style='height:20.25pt'>&nbsp;</td>
			<td colspan=7 class=xl10423788>SUMMARY REPORT QA</td>
		</tr>
		<tr height=27 style='height:20.25pt'>
			<td height=27 class=xl10123788 style='height:20.25pt'>&nbsp;</td>
			<td class=xl10323788 colspan=2>Welcome Call Team</td>
			<td class=xl10423788>&nbsp;</td>
			<td class=xl10423788>&nbsp;</td>
			<td class=xl10423788>&nbsp;</td>
			<td class=xl10423788>&nbsp;</td>
			<td class=xl10423788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl10123788 style='height:15.0pt'>&nbsp;</td>
			<td class=xl10623788>Periode<span style='mso-spacerun:yes'> </span></td>
			<td class=xl10723788>:</td>
			<td class=xl10823788>Oct 2014</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
		</tr>
		<tr class=xl9723788 height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl11023788>Leader</td>
			<td class=xl10723788>:</td>
			<td class=xl11123788>Putri Handayani</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
		</tr>
		<tr class=xl9723788 height=18 style='height:13.5pt'>
			<td height=18 class=xl10123788 style='height:13.5pt'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl11123788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
		</tr>
		<tr class=xl9723788 height=23 style='mso-height-source:userset;height:17.25pt'>
			<td height=23 class=xl10123788 style='height:17.25pt'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td colspan=3 class=xl15523788 style='border-right:1.0pt solid black'>Monthly</td>
			<td rowspan=2 class=xl15823788 style='border-bottom:1.0pt solid black'>Keterangan</td>
		</tr>
		<tr class=xl9823788 height=23 style='mso-height-source:userset;height:17.25pt'>
			<td height=23 class=xl11223788 style='height:17.25pt'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td align=left valign=top></td>
			<td class=xl11323788 style='border-top:none'># of score QA</td>
			<td class=xl11423788 style='border-top:none;border-left:none'># of sample QA</td>
			<td class=xl11523788 style='border-top:none;border-left:none'>Score Rata-rata</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl11623788>Welcome Call</td>
			<td class=xl11723788>&nbsp;</td>
			<td class=xl11823788 width=195 style='border-left:none;width:146pt'>Triliyana Siregar (Lia)</td>
			<td class=xl11923788 style='border-left:none'>0</td>
			<td class=xl11923788 style='border-left:none'>0</td>
			<td class=xl12023788 align=center style='border-left:none'>#DIV/0!</td>
			<td class=xl12123788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12223788>&nbsp;</td>
			<td class=xl12323788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Al Izzah Chan (Ezza)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Dian Priyanti (Didi)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl13023788 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Dwinta Nadyastari (Dita)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl13023788 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Enny Qibtya (Enny)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl13023788 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Elisabeth YuliAryanti (Elis)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl13023788 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Irma Novy Yanti (Irma)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Laila Fitri (Laila)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Linsar Carnelia TB (Linsar)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Marlina (Marlin)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Mega Permata Sari (Mega)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Sujono Manansang (Jon's)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl10223788>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Maria Magdalena A (Maria)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl10223788>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Finda Utari (Finda)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl10223788>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=17 style='height:12.75pt'>
			<td height=17 class=xl11223788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl12823788>&nbsp;</td>
			<td class=xl12923788>&nbsp;</td>
			<td class=xl12423788 width=195 style='border-top:none;border-left:none;width:146pt'>Leny Ariyani (Leny)</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12523788 style='border-top:none;border-left:none'>0</td>
			<td class=xl12623788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl12723788 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl10223788>&nbsp;</td>
		</tr>
		<tr class=xl9823788 height=18 style='height:13.5pt'>
			<td height=18 class=xl11223788 style='height:13.5pt'>&nbsp;</td>
			<td class=xl13123788>&nbsp;</td>
			<td class=xl13223788>&nbsp;</td>
			<td class=xl13323788 width=195 style='border-top:none;border-left:none;width:146pt'>TBA</td>
			<td class=xl13423788 style='border-top:none;border-left:none'>0</td>
			<td class=xl13423788 style='border-top:none;border-left:none'>0</td>
			<td class=xl13523788 align=center style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl13623788 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl10223788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl13723788 width=195 style='width:146pt'></td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl13923788>SUMMARY</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14023788>QA Welcome Call</td>
			<td class=xl14023788>&nbsp;</td>
			<td class=xl14123788>&nbsp;</td>
			<td class=xl14223788>0</td>
			<td class=xl14223788>0</td>
			<td class=xl14323788 align=center>#DIV/0!</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14423788 style='border-top:none'># of sample QA</td>
			<td class=xl14423788 style='border-top:none'>&nbsp;</td>
			<td class=xl14423788 style='border-top:none'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14523788>BCA</td>
			<td class=xl14523788>:</td>
			<td class=xl14523788 align=right>0</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14623788 style='border-top:none'>CIMB</td>
			<td class=xl14623788 style='border-top:none'>:</td>
			<td class=xl14523788 align=right>0</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14623788 style='border-top:none'>Bancass</td>
			<td class=xl14623788 style='border-top:none'>:</td>
			<td class=xl14523788 align=right>0</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14623788 style='border-top:none'>Agency</td>
			<td class=xl14623788 style='border-top:none'>:</td>
			<td class=xl14523788 align=right>0</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl14723788 style='border-top:none'>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14823788># CCO survey/day</td>
			<td class=xl14923788>:</td>
			<td class=xl14923788 align=right>0</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=17 style='height:12.75pt'>
			<td height=17 class=xl10123788 style='height:12.75pt'>&nbsp;</td>
			<td class=xl14823788>&nbsp;</td>
			<td class=xl14823788>&nbsp;</td>
			<td class=xl15023788 align=center>#DIV/0!</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10523788>&nbsp;</td>
			<td class=xl10923788>&nbsp;</td>
			<td class=xl13823788>&nbsp;</td>
		</tr>
		<tr height=18 style='height:13.5pt'>
			<td height=18 class=xl15123788 style='height:13.5pt'>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15223788>&nbsp;</td>
			<td class=xl15323788>&nbsp;</td>
		</tr>
	</table>
</body>
</html>