<html>
<head>
	<?php $this -> load -> view('rpt_call_monthly/rpt_call_monthly_style'); ?>
<title>Welcome Call Monthly Report</title>
</head>
<body>
	<table border=0 cellpadding=0 cellspacing=0 width=1935 class=xl9730510 style='border-collapse:collapse;table-layout:fixed;width:1451pt'>
	<tr class=xl15030510 height=24 style='mso-height-source:userset;height:18.0pt'>
		<td height=24 class=xl15230510 dir=LTR width=403 style='height:18.0pt;width:302pt'>Welcome Call Monthly Report</td>
		<td class=xl15030510 width=120 style='width:90pt'>&nbsp;</td>
		<td class=xl15030510 width=24 style='width:18pt'>&nbsp;</td>
		<td class=xl15030510 width=84 style='width:63pt'>&nbsp;</td>
		<td class=xl15030510 width=120 style='width:90pt'>&nbsp;</td>
		<td class=xl15030510 width=24 style='width:18pt'>&nbsp;</td>
		<td class=xl15030510 width=84 style='width:63pt'>&nbsp;</td>
		<td class=xl15030510 width=120 style='width:90pt'>&nbsp;</td>
		<td class=xl15030510 width=24 style='width:18pt'>&nbsp;</td>
		<td class=xl15030510 width=84 style='width:63pt'>&nbsp;</td>
		<td class=xl15030510 width=120 style='width:90pt'>&nbsp;</td>
		<td class=xl15030510 width=24 style='width:18pt'>&nbsp;</td>
		<td class=xl15030510 width=84 style='width:63pt'>&nbsp;</td>
		<td class=xl15030510 width=120 style='width:90pt'>&nbsp;</td>
		<td class=xl15030510 width=24 style='width:18pt'>&nbsp;</td>
		<td class=xl15030510 width=84 style='width:63pt'>&nbsp;</td>
		<td class=xl15030510 width=120 style='width:90pt'>&nbsp;</td>
		<td class=xl15030510 width=24 style='width:18pt'>&nbsp;</td>
		<td class=xl15030510 width=84 style='width:63pt'>&nbsp;</td>
		<td class=xl15030510 width=36 style='width:27pt'>&nbsp;</td>
		<td class=xl15030510 width=64 style='width:48pt'>&nbsp;</td>
		<td class=xl15030510 width=64 style='width:48pt'>&nbsp;</td>
	</tr>
	<tr class=xl15030510 height=24 style='mso-height-source:userset;height:18.0pt'>
		<td height=24 class=xl15330510 dir=LTR width=403 style='height:18.0pt;width:302pt'>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
	</tr>
	<tr class=xl9830510 height=31 style='height:23.25pt'>
		<td height=31 class=xl15130510 style='height:23.25pt'>&nbsp;</td>
		<td colspan=9 class=xl19030510>&nbsp;</td>
		<td colspan=3 class=xl19030510>&nbsp;</td>
		<td colspan=3 class=xl21330510>&nbsp;</td>
		<td class=xl15130510>&nbsp;</td>
		<td class=xl15130510>&nbsp;</td>
		<td class=xl15130510>&nbsp;</td>
		<td class=xl15130510>&nbsp;</td>
		<td class=xl15130510>&nbsp;</td>
		<td class=xl15130510>&nbsp;</td>
	</tr>
	<tr class=xl9930510 height=20 style='height:15.0pt'>
		<td height=20 class=xl15030510 style='height:15.0pt'>&nbsp;</td>
		<td colspan=3 class=xl16730510>BCA<span style='mso-spacerun:yes'> </span></td>
		<td colspan=3 class=xl16830510 style='border-left:none'>CIMB</td>
		<td colspan=3 class=xl19130510 style='border-left:none'>Other Bancass</td>
		<td colspan=3 class=xl19230510 style='border-left:none'>Agency</td>
		<td colspan=3 class=xl21430510 style='border-left:none'>Temporell</td>
		<td colspan=3 rowspan=2 class=xl20030510 style='border-right:.5pt solid black'>ALL</td>
		<td class=xl15430510>&nbsp;</td>
		<td rowspan=3 class=xl19530510 style='border-bottom:.5pt solid black'>BCA</td>
		<td rowspan=3 class=xl19530510 style='border-bottom:1.0pt solid black'>Others</td>
	</tr>
	<tr class=xl9930510 height=20 style='height:15.0pt'>
		<td height=20 class=xl10030510 dir=LTR width=403 style='height:15.0pt;width:302pt'>August - 2014</td>
		<td colspan=3 class=xl16530510>WCDS1 WC11411001</td>
		<td colspan=3 class=xl16630510 style='border-left:none'>WCDS2 WC11411001</td>
		<td colspan=3 class=xl19830510 style='border-left:none'>WCDS3 WC11411001</td>
		<td colspan=3 class=xl19930510 style='border-left:none'>WCDS4 WC11411001</td>
		<td colspan=3 class=xl21530510 style='border-left:none'>WCDS1 WC01410001</td>
		<td class=xl15430510>&nbsp;</td>
	</tr>
	<tr height=21 style='height:15.75pt'>
		<td height=21 class=xl10130510 dir=LTR width=403 style='height:15.75pt;width:302pt'>-ALL-</td>
		<td colspan=2 class=xl16330510 dir=LTR width=144 style='border-left:none;width:108pt'>Amount</td>
		<td class=xl10230510 dir=LTR width=84 style='width:63pt'>%</td>
		<td colspan=2 class=xl16330510 dir=LTR width=144 style='border-left:none;width:108pt'>Amount</td>
		<td class=xl10230510 dir=LTR width=84 style='width:63pt'>%</td>
		<td colspan=2 class=xl16330510 dir=LTR width=144 style='border-left:none;width:108pt'>Amount</td>
		<td class=xl10230510 dir=LTR width=84 style='width:63pt'>%</td>
		<td colspan=2 class=xl16330510 dir=LTR width=144 style='border-left:none;width:108pt'>Amount</td>
		<td class=xl10230510 dir=LTR width=84 style='width:63pt'>%</td>
		<td colspan=2 class=xl16330510 dir=LTR width=144 style='border-left:none;width:108pt'>Amount</td>
		<td class=xl10230510 dir=LTR width=84 style='width:63pt'>%</td>
		<td colspan=2 class=xl16330510 dir=LTR width=144 style='border-left:none;width:108pt'>Amount</td>
		<td class=xl10330510 dir=LTR width=84 style='width:63pt'>%</td>
		<td class=xl15030510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl10530510 dir=LTR width=403 style='height:15.0pt;width:302pt'>1. In Progress</td>
		<td colspan=2 class=xl17130510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='width:63pt'>0.00%</td>
		<td colspan=2 class=xl17130510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='width:63pt'>0.00%</td>
		<td colspan=2 class=xl17130510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='width:63pt'>0.00%</td>
		<td colspan=2 class=xl17130510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='width:63pt'>0.00%</td>
		<td colspan=2 class=xl17130510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='width:63pt'>0.00%</td>
		<td colspan=2 class=xl17130510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='width:63pt'>0.00%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right>0</td>
		<td class=xl10930510 align=right>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl10530510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>2. Untouched</td>
		<td colspan=2 class=xl17030510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td colspan=2 class=xl17030510 dir=LTR width=144 style='border-left:none;+width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td colspan=2 class=xl17030510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td colspan=2 class=xl17030510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td colspan=2 class=xl17030510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td colspan=2 class=xl17030510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right style='border-top:none'>0</td>
		<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl10530510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>3.Conducted/Attempt/Dialed(A+B+C)</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>4,538</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>54</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>144</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>1,228</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>1,228</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>10,269</td>
		<td class=xl10730510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right style='border-top:none'>4538</td>
		<td class=xl10930510 align=right style='border-top:none'>1426</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl10530510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>Total (1+2+3)</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>4,538</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>54</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>144</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>1,228</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>1,228</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16930510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>10,269</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right style='border-top:none'>4538</td>
		<td class=xl10930510 align=right style='border-top:none'>1426</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11030510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>A. Connected &amp; Answered (I+II) - CLEAN DATA</td>
		<td colspan=2 class=xl17430510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>3,890</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>85.72%</td>
		<td colspan=2 class=xl17430510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>43</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>79.63%</td>
		<td colspan=2 class=xl17430510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>121</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>84.03%</td>
		<td colspan=2 class=xl17430510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>1,023</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>83.31%</td>
		<td colspan=2 class=xl17430510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>1,023</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>83.31%</td>
		<td colspan=2 class=xl17430510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>8,617</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>83.91%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl11230510 align=right style='border-top:none'>3890</td>
		<td class=xl11330510 align=right style='border-top:none'>1187</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11430510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>I. Contacted (Ia+Ib)</td>
		<td colspan=2 class=xl17230510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>3,748</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>82.59%</td>
		<td colspan=2 class=xl17230510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>41</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>75.93%</td>
		<td colspan=2 class=xl17230510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>109</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>75.69%</td>
		<td colspan=2 class=xl17230510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>958</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>78.01%</td>
		<td colspan=2 class=xl17230510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>958</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>78.01%</td>
		<td colspan=2 class=xl17230510 dir=LTR align=right width=144 style='border-left:none;width:108pt'>8,100</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>78.88%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl11630510 align=right style='border-top:none'>3748</td>
		<td class=xl11730510 align=right style='border-top:none'>1108</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11830510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>Ia. Surveyed</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>3,596</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>95.94%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>41</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>100.00%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>108</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>99.08%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>922</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>96.24%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>922</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>96.24%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>7,804</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>96.35%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl14830510 align=right style='border-top:none'>3596</td>
		<td class=xl14930510 align=right style='border-top:none'>1071</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11830510 dir=LTR width=403 style='height:15.0pt;border-top:none;width:302pt'>Ib. No Interest to Talk</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>152</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>4.06%</td>
		<td colspan=2 class=xl17930510 dir=LTR width=144 style='border-left:none;width:108pt'>&nbsp;</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.00%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>1</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>0.92%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>36</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>3.76%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>36</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>3.76%</td>
		<td colspan=2 class=xl17730510 dir=LTR width=144 style='border-left:none;width:108pt'>296</td>
		<td class=xl11930510 dir=LTR align=right width=84 style='border-top:none;width:63pt'>3.65%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl14830510 align=right style='border-top:none'>152</td>
		<td class=xl14930510 align=right style='border-top:none'>37</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11430510 dir=LTR width=403 style='height:15.0pt;
		border-top:none;width:302pt'><span style='mso-spacerun:yes'>     </span>II.
		Non Contacted</td>
		<td colspan=2 class=xl17630510 dir=LTR width=144 style='border-left:none;
		width:108pt'>142</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>3.65%</td>
		<td colspan=2 class=xl17630510 dir=LTR width=144 style='border-left:none;
		width:108pt'>2</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>4.65%</td>
		<td colspan=2 class=xl17630510 dir=LTR width=144 style='border-left:none;
		width:108pt'>12</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>9.92%</td>
		<td colspan=2 class=xl17630510 dir=LTR width=144 style='border-left:none;
		width:108pt'>65</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.35%</td>
		<td colspan=2 class=xl17630510 dir=LTR width=144 style='border-left:none;
		width:108pt'>65</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.35%</td>
		<td colspan=2 class=xl17630510 dir=LTR width=144 style='border-left:none;
		width:108pt'>517</td>
		<td class=xl11530510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.00%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl11630510 align=right style='border-top:none'>142</td>
		<td class=xl11730510 align=right style='border-top:none'>79</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11030510 dir=LTR width=403 style='height:15.0pt;
		border-top:none;width:302pt'>B. Connected &amp; Not Answered</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>360</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>7.93%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>9</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>16.67%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>16</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>11.11%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>82</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.68%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>82</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.68%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>942</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>9.17%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl11230510 align=right style='border-top:none'>360</td>
		<td class=xl11330510 align=right style='border-top:none'>107</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl11030510 dir=LTR width=403 style='height:15.0pt;
		border-top:none;width:302pt'>C. Not Connected - UNCLEAN DATA</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>288</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.35%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>2</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>3.70%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>7</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>4.86%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>123</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>10.02%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>123</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>10.02%</td>
		<td colspan=2 class=xl18030510 dir=LTR width=144 style='border-left:none;
		width:108pt'>710</td>
		<td class=xl11130510 dir=LTR align=right width=84 style='border-top:none;
		width:63pt'>6.91%</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl11230510 align=right style='border-top:none'>288</td>
		<td class=xl11330510 align=right style='border-top:none'>132</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
		border-top:none;width:302pt'>Busy</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>27</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>1</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>1</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>19</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>19</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>102</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right style='border-top:none'>27</td>
		<td class=xl10930510 align=right style='border-top:none'>21</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
		border-top:none;width:302pt'>Dead Tone</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>155</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>1</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>5</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>59</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>59</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>384</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right style='border-top:none'>155</td>
		<td class=xl10930510 align=right style='border-top:none'>65</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
		<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
		border-top:none;width:302pt'>Left Message/Voice Mail</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>68</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl18130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>&nbsp;</td>
		<td class=xl12230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl20730510 dir=LTR width=144 style='border-left:none;
		width:108pt'>&nbsp;</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>26</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>26</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
		width:108pt'>137</td>
		<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
		<td class=xl15030510>&nbsp;</td>
		<td class=xl10830510 align=right style='border-top:none'>68</td>
		<td class=xl10930510 align=right style='border-top:none'>26</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Return List</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>27</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>1</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>18</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>18</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>67</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>27</td>
	<td class=xl10930510 align=right style='border-top:none'>19</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Wrong Number</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>11</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>1</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>1</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>20</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>11</td>
	<td class=xl10930510 align=right style='border-top:none'>1</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl11030510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>D. In Progress</td>
	<td colspan=2 class=xl18330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12030510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12030510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12030510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12030510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12030510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl12030510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl11230510 align=right style='border-top:none'>0</td>
	<td class=xl11330510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Busy</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Call Back Later</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Can Not Be Reached</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Dead Tone</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Left Message/Voice Mail</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>No Answer</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl12330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Wrong Number</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>&nbsp;</td>
	<td class=xl10730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl10830510 align=right style='border-top:none'>0</td>
	<td class=xl10930510 align=right style='border-top:none'>0</td>
	</tr>
	<tr class=xl12930510 height=24 style='mso-height-source:userset;height:18.0pt'>
	<td height=24 class=xl12430510 style='height:18.0pt'>Contacted Ratio
	(Successful Contacted to All data)<span style='mso-spacerun:yes'> </span></td>
	<td class=xl12530510 style='border-top:none'>85.72%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>79.63%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>84.03%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>83.31%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>83.31%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>83.91%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl12730510>85.72%</td>
	<td class=xl12830510>83.24%</td>
	</tr>
	<tr class=xl12930510 height=24 style='mso-height-source:userset;height:18.0pt'>
	<td height=24 class=xl13030510 style='height:18.0pt;border-top:none'>Successful
	Surveyed to Contacted rate<span style='mso-spacerun:yes'> </span></td>
	<td class=xl12530510 style='border-top:none'>79.24%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>75.93%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>75.00%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>75.08%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>75.08%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl12530510 style='border-top:none'>76.00%</td>
	<td class=xl12530510 style='border-top:none'>&nbsp;</td>
	<td class=xl12630510 style='border-top:none'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13130510 style='border-top:none'>79.24%</td>
	<td class=xl13230510 style='border-top:none'>75.11%</td>
	</tr>
	<tr class=xl15030510 height=20 style='height:15.0pt'>
	<td height=20 class=xl15030510 style='height:15.0pt'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15530510 style='border-top:none'>&nbsp;</td>
	<td class=xl15630510 style='border-top:none'>&nbsp;</td>
	<td class=xl15630510 style='border-top:none'>&nbsp;</td>
	<td class=xl15130510>&nbsp;</td>
	<td class=xl15130510>&nbsp;</td>
	<td class=xl15130510>&nbsp;</td>
	</tr>
	<tr class=xl15130510 height=21 style='height:15.75pt'>
	<td height=21 class=xl15730510 style='height:15.75pt'>Summary from
	Questionnaire<span style='mso-spacerun:yes'> </span></td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl15930510>&nbsp;</td>
	<td class=xl15830510>&nbsp;</td>
	<td class=xl16030510>&nbsp;</td>
	<td class=xl15130510>&nbsp;</td>
	<td class=xl15130510>&nbsp;</td>
	<td class=xl15130510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl14630510 dir=LTR width=403 style='height:15.0pt;
	width:302pt'>Q01 = 0</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>908</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>15</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>10</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>190</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>190</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>2,018</td>
	<td class=xl14730510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q01 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>2,688</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>26</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>98</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>732</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>732</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>5,786</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q01 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>41</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>108</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>7,804</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q02 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>1,085</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>8</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>11</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>216</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>216</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>2,008</td>
	<td class=xl13430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q02 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>2,511</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>33</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>97</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>706</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>706</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>5,796</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q02 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>41</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>108</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>7,804</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q03 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>969</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>8</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>26</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>230</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>230</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>2,098</td>
	<td class=xl13430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q03 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>2,627</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>33</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>82</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>692</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>692</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>5,706</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q03 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>41</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>108</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>7,804</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q04 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>44</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>47</td>
	<td class=xl13430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q04 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,552</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,325</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q04 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,372</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q05 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>71</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>76</td>
	<td class=xl13430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q05 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,525</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,296</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q05 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,372</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q06 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>105</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>112</td>
	<td class=xl13430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q06 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,491</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,260</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q06 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,372</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q07 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>136</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>145</td>
	<td class=xl13430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q07 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>23</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>25</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q07 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,437</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>41</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>108</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>7,634</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13630510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18830510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl13730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18830510 dir=LTR width=144 style='border-left:none;
	width:108pt'>41</td>
	<td class=xl13730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18830510 dir=LTR width=144 style='border-left:none;
	width:108pt'>108</td>
	<td class=xl13730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18830510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl13730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18830510 dir=LTR width=144 style='border-left:none;
	width:108pt'>922</td>
	<td class=xl13730510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl21130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>7,804</td>
	<td class=xl13830510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q08 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q08 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	<td class=xl13930510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl13330510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q08 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,372</td>
	<td class=xl13430510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl14030510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>3,596</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>4,372</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='mso-height-source:userset;height:15.0pt'>
	<td height=20 class=xl14330510 dir=LTR width=403 style='height:15.0pt;
	width:302pt'>Q09 = 0</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18430510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10430510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='mso-height-source:userset;height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q09 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl9830510></td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='mso-height-source:userset;height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q09 = 2</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl9830510></td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl14030510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14230510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='mso-height-source:userset;height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	width:302pt'>Q10 = 0</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl9830510></td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='mso-height-source:userset;height:15.0pt'>
	<td height=20 class=xl12130510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q10 = 1</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl16130510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl10630510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl9830510></td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='mso-height-source:userset;height:15.0pt'>
	<td height=20 class=xl14430510 dir=LTR width=403 style='height:15.0pt;
	border-top:none;width:302pt'>Q10 = 2</td>
	<td colspan=2 class=xl19330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14530510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl19330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14530510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl19330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14530510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl19330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14530510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl19330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14530510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl19330510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14530510 dir=LTR width=84 style='border-top:none;width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl9830510></td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	<td height=20 class=xl14030510 dir=LTR width=403 style='height:15.0pt;
	width:302pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl18630510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14130510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td colspan=2 class=xl20930510 dir=LTR width=144 style='border-left:none;
	width:108pt'>0</td>
	<td class=xl14230510 dir=LTR width=84 style='width:63pt'>&nbsp;</td>
	<td class=xl15030510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	<td class=xl13530510>&nbsp;</td>
	</tr>
	<![if supportMisalignedColumns]>
	<tr height=0 style='display:none'>
	<td width=403 style='width:302pt'></td>
	<td width=120 style='width:90pt'></td>
	<td width=24 style='width:18pt'></td>
	<td width=84 style='width:63pt'></td>
	<td width=120 style='width:90pt'></td>
	<td width=24 style='width:18pt'></td>
	<td width=84 style='width:63pt'></td>
	<td width=120 style='width:90pt'></td>
	<td width=24 style='width:18pt'></td>
	<td width=84 style='width:63pt'></td>
	<td width=120 style='width:90pt'></td>
	<td width=24 style='width:18pt'></td>
	<td width=84 style='width:63pt'></td>
	<td width=120 style='width:90pt'></td>
	<td width=24 style='width:18pt'></td>
	<td width=84 style='width:63pt'></td>
	<td width=120 style='width:90pt'></td>
	<td width=24 style='width:18pt'></td>
	<td width=84 style='width:63pt'></td>
	<td width=36 style='width:27pt'></td>
	<td width=64 style='width:48pt'></td>
	<td width=64 style='width:48pt'></td>
	</tr>
	<![endif]>
	</table>
</body>
</html>