<?php __(javascript(array(array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time())))); ?>
<script>	
/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
Ext.DOM.ShowReport = function() {
  var param = []; 	
  var TITLE = "Welcome Call Monthly Report :: from "+ Ext.Cmp('start_date').getValue()+" to "+
			   Ext.Cmp('end_date').getValue();
  
  param['mode'] = 'html';
  
  if(Ext.Cmp('start_date').empty()){ 
	Ext.Msg("Start of date is empty ").Info(); }
  else if(Ext.Cmp('end_date').empty() ){
	Ext.Msg("End of date is empty ").Info(); }
  else 
  {	
	  Ext.Dialog('panelx', {
			ID		: 'panelx1',
			type	: 'ajax/html',
			title	: TITLE,
			url 	: Ext.DOM.INDEX +'/RptCallMonthly/ShowReport/',
			param 	: Ext.Join([ Ext.Serialize("frm_monthly_report").getReportElement()]).object(),
			style 	: {
				width  		: $(window).width(),
				height 		: $('#main_content').height(),
				scrolling 	: 1, 
				resize 		: 0,
				close		: 1,
				minimize	: 1,
				top			: 50,
				left		: 2,
				right		: 2
			}	
		}).open();
	}	
}

/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
Ext.DOM.ShowExcel = function() {
	Ext.Window
	({
		url 	: Ext.DOM.INDEX +'/RptCallMonthly/ShowExcel/',
		param 	: Ext.Join([ Ext.Serialize("frm_monthly_report").getReportElement()]).object()
		
	}).newtab();
}
/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 Ext.query('.date').datepicker
 ({
	showOn		 	: 'button', 
	buttonImage 	: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	changeYear 		: true,
	changeMonth 	: true,
	dateFormat 		: 'dd-mm-yy',
	readonly		:true,
	onSelect : function(date)
	{
		if(typeof(date) =='string'){
			var x = date.split('-');
			var retur = x[2]+"-"+x[1]+"-"+x[0];
			if(new Date(retur) > new Date()) {
				Ext.Cmp($(this).attr('id')).setValue('');
			}
		}
	}
});

</script>

<?php 
$UI =& get_instance();
$UI->load->model(array('M_RptCallMonthly') );

// get data 
$config =& $UI->M_RptCallMonthly->_getCampaignByWorkProject();
$CampaignName = array();

if(is_array($config)) 
	foreach( $config as $id => $rows ) 
{
	$CampaignName[$id] = $rows['name']; 			
}

	
?>
<fieldset class="corner" style='margin-top:0px;'>
<legend class="icon-menulist">&nbsp;&nbsp;Monthly Report </legend>
<div>
<form name="frm_monthly_report">
	<table cellpadding='4' cellspacing=4>
		
		<tr>
			<td class="text_caption bottom">Report Type : </td>
			<td class='bottom'><?php __(form()->combo('report_type','select long', 
					array('call_monthly_report' => 'Monthly Report', 'summary_from_question'=>'Summary from Questionnaire')));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Campaign ID :</td>
			<td class='bottom'><?php __(form()->listcombo('CampaignId','select long', $CampaignName));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Issued Date :</td>
			<td class='bottom'>
				<?php __(form()->input('start_date','input_text box date'));?> &nbsp-
				<?php __(form()->input('end_date','input_text box date'));?>
			</td>
		</tr>
		
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td class='bottom'>
				<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
				<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
			</td>
		</tr>
	</table>
</form>	
</div>
</fieldset>