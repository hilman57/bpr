<?php
/*
 * ! source html 
 *
 */

$start_date = date("Y-m", strtotime( $this->URI->_get_post('start_date')));
$end_date = date("Y-m", strtotime( $this->URI->_get_post('end_date')));

function percent_data( $data )
{
	if( !is_null($data) )
	{
		if((int)$data >=0 )
			return $data." %";
		else
			return '-';
		
	}	
	else
		return '-';
} 


// then set UI 

$UI=& get_instance(); 
$UI->load->model(array('M_RptCallMonthly'));
$config = $UI->M_RptCallMonthly->_get_config();

while(true) 
{ 

$estart_date = $start_date;
$dates = explode('-', $estart_date); 
$bln = $dates[0] .'-'. $dates[1];
	
__("<table cellspacing=1 cellpadding=0 style=\"border-bottom:0px solid red;margin-left:0px;margin-bottom:20px;\" width=\"80%\">
	<tr height=22 bgcolor=\"#E25C77\" style=\"color:white;\">
		<td rowspan=\"2\" style=\"padding:4px;text-align:center;\">
			<div style=\"font-size:14px;color:white;font-weight:bold;\">Issued Monthly -  ". NameMonth((INT)$dates[1]) ." ".  $dates[0] ."</div>
		</td> ");
		if(is_array($data_campaign))
		foreach($data_campaign as $CampaignId => $rows )
		{
			__("<td colspan=\"2\" align=\"center\">	
				<div style=\"width:75px;\">
					<b>{$rows['name']}</b>
				</div>
				</td>");
		}
		__("<td colspan=\"2\" rowspan=\"1\" align=\"center\" ><b>ALL</b></td>");
	__("</tr>"); 
	
	
__("<tr height=22 bgcolor=\"#E25C77\" style=\"color:white;\">");

		if(is_array($data_campaign))
		foreach($data_campaign as $CampaignId => $rows )
		{
			__("<td style=\"padding:4px;text-align:center;\"><b>Amount</b></td>");
			__("<td style=\"padding:4px;text-align:center;\"><b>Rate</b></td>");
		}
		
		__("<td style=\"padding:4px;text-align:center;\"><b>Amount</b></td>");
		__("<td style=\"padding:4px;text-align:center;\"><b>Rate</b></td>");
		
	__("</tr>"); 
 
 
 
 $no = 0;
 foreach( $config as $title => $rows )
 {
	$no++;
	$color = ($no%2?'#FFFEEE':'#ffffff');
 
  if(is_array($rows['child']) )
  {
	$child = $rows['child'];
	
	__("<tr height=\"24\" style=\"color:#7e009d;border:1px solid #7e009d;background-color:FFFCCC;\">");
	__("<td class=\"content-first\" style=\"padding:4px;text-align:left;\">&nbsp;<b>{$rows['no']}{$rows['title']}</b></td>");
 
	$total_all_size_data = 0;  
	if(is_array($data_campaign)){
	 foreach($data_campaign as $CampaignId => $row )
	 {
	 
		$total_data = $data_size[$estart_date][$CampaignId][$rows['code']];
		$rate_size_code = $data_size[$estart_date][$CampaignId][$rows['call_rate_func']];
		
		$total_all_size_data += $total_data; 
		__("<td class=\"content-middle\" align=\"right\">".($total_data?$total_data:0)."&nbsp;</td>");
		__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $rate_size_code ). "&nbsp;</td>");
	 }
	}
	__("<td class=\"content-middle\" align=\"right\">{$total_all_size_data}&nbsp;</td>");
	__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $data_size[$estart_date][$rows['call_rate_func']] ) ."&nbsp;</td>");
	__("</tr>");
	
	
	if(is_array( $child) )
	foreach( $child as $k => $r )
	{
	  __("<tr height=\"24\" style=\"color:#7e009d;border:1px solid #7e009d;background-color:{$color}\">");
	  __("<td class=\"content-first\" style=\"padding-left:8px;text-align:left;\">&nbsp;{$r['no']}&nbsp;.&nbsp;{$r['title']}</td>");
	 
	 $total_all_size_data = 0; 
	 if(is_array($data_campaign))
	 foreach($data_campaign as $CampaignId => $row )
			{
				$total_data = $data_size[$estart_date][$CampaignId][$r['code']];
				$rate_size_code = $data_size[$estart_date][$CampaignId][$r['call_rate_func']];
				$total_all_size_data+=$total_data;
				
				__("<td class=\"content-middle\" align=\"right\">". ( $total_data ? $total_data : 0 ) ."&nbsp;</td>");
				__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $rate_size_code ). "&nbsp;</td>");
			}
			
		__("<td class=\"content-middle\" align=\"right\">". $total_all_size_data . "&nbsp;</td>");
		__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $data_size[$r['call_rate_func']] ) ."&nbsp;</td>");	
		__("</tr>");
		
		$tree_code = $r['child'];
		
		if( is_array($tree_code) ) 
			foreach($tree_code as $x => $y )
		{
			__("<tr height=\"24\" style=\"color:#7e009d;border:1px solid #7e009d;background-color:{$color}\">");
			__("<td class=\"content-first\" style=\"padding-left:20px;text-align:left;\">&nbsp;{$y['no']}&nbsp;.&nbsp;{$y['title']}</td>");
			
			$total_all_size_data = 0;
			
			if(is_array($data_campaign))
				foreach($data_campaign as $CampaignId => $row ) 
				{
					$total_data = $data_size[$estart_date][$CampaignId][$y['code']];
					$rate_size_code = $data_size[$estart_date][$CampaignId][$y['call_rate_func']];
					$total_all_size_data+= $total_data;
					
					__("<td class=\"content-middle\" align=\"right\">". ( $total_data ? $total_data : 0 ) ."&nbsp;</td>");
					__("<td class=\"content-middle\" align=\"right\" >". call_user_func("percent_data", $rate_size_code ). "&nbsp;</td>");
				}
				
			__("<td class=\"content-middle\" align=\"right\">". $total_all_size_data . "&nbsp;</td>");
			__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $data_size[$estart_date][$y['call_rate_func']] ) ."&nbsp;</td>");		
			__("</tr>");
			
		}	
		
	
	}
	
  }
  else
  {
  
	__("<tr height=\"24\" style=\"color:#7e009d;border:1px solid #7e009d;background-color:{$color}\">");
	__("<td class=\"content-first\" style=\"padding:4px;text-align:left;\">&nbsp;<b>{$rows['no']}{$rows['title']}</b></td>");
 
	$total_all_size_data = 0;
	if(is_array($data_campaign))
	foreach($data_campaign as $CampaignId => $row )
	{
		$call_user_func = ( isset($rows['call_user_func']) ? $rows['call_user_func'] : "" );
		$total_size_code = $data_size[$estart_date][$CampaignId][$rows['code']];
		$rate_size_code = $data_size[$estart_date][$CampaignId][$rows['call_rate_func']];
		
		$total_all_size_data+=$total_size_code;
		
		if( $call_user_func!='' ) {
			__("<td class=\"content-middle\" align=\"right\">". call_user_func($rows['call_user_func'], $total_size_code) ."&nbsp;</td>");
			__("<td class=\"content-middle\" align=\"right\">{$rate_size_code}&nbsp;</td>");
		}
		else{
			__("<td class=\"content-middle\" align=\"right\">". ( $total_size_code ? $total_size_code : 0 )."&nbsp;</td>");
			__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $rate_size_code ). "&nbsp;</td>");	
		}
	}

	if( $call_user_func!='' ){
		__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $total_all_size_data ) ."&nbsp;</td>");
	}
	else{
		__("<td class=\"content-middle\" align=\"right\">". $total_all_size_data ."&nbsp;</td>");
	}
	
	__("<td class=\"content-middle\" align=\"right\">". call_user_func("percent_data", $data_size[$estart_date][$rows['call_rate_func']] ) . "&nbsp;</td>");	
	
	__("</tr>");
  
  }
  
 }	
 
 __("</table>");

 if ( $start_date == $end_date ) break;
		$start_date = nextMonth($start_date);
		
 }
//print_r($data_campaign);
 
Excel()->HTML_Excel(date('YmdHi')."_Call_Monthly_Report.xls");

?>