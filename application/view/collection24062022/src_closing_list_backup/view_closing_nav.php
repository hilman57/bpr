<?php

	echo javascript();
?>
<script type="text/javascript">

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText("Sales Submited");
 }); 
 	
	
var Reason = [];

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* catch of requeet accep browser **/
var datas = {
	cust_name 		: '<?php echo _get_post('cust_name');?>',
	gender	 		: '<?php echo _get_post('gender');?>',
	card_type 		: '<?php echo _get_post('card_type');?>',
	order_by 		: '<?php echo _get_post('order_by');?>',
	type	 		: '<?php echo _get_post('type');?>',
	user_id 		: '<?php echo _get_post('user_id');?>',
	verify_status 	: '<?php echo _get_post('verify_status');?>',
}
			
	/* assign navigation filter **/
		
var navigation = 
{
	custnav	 : Ext.DOM.INDEX +'/SrcCustomerClosing/index/',
	custlist : Ext.DOM.INDEX +'/SrcCustomerClosing/Content/'
}
		
/* assign show list content **/

Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();
		
/* creete object javaclass **/
//doJava.File = '../class/class.src.customers.php' 
		
		var defaultPanel = function(){
			if( doJava.destroy() ){
				doJava.Method = 'POST',
				doJava.Params = { 
					action		:'tpl_onready', 
					//cust_number : datas.cust_number,
					cust_name 	: datas.cust_name, 
					gender		: datas.gender,
					card_type	: datas.card_type
					//cust_dob 	: datas.cust_dob, 
					//home_phone  : datas.home_phone, 
					//office_phone: datas.office_phone,
					//mobile_phone: datas.mobile_phone,  
					//campaign_id : datas.campaign_id, 
					//call_result : datas.call_result,  
					//user_id 	: datas.user_id
				}
				doJava.Load('span_top_nav');	
			}
		} 
		
	
/* function searching customers **/
	
var searchCustomer = function(){
    
	Ext.EQuery.construct(navigation,{
		cust_name : Ext.Cmp('cust_name').getValue(),
		gender : Ext.Cmp('gender').getValue(),
		card_type : Ext.Cmp('card_type').getValue(),
		verify_status : Ext.Cmp('verify_status').getValue()
	});
	
	Ext.EQuery.postContent()
}
		
/* function clear searching form **/
	
var resetSeacrh = function()
{
	if( doJava.destroy() ){
		doJava.init = [
								['cust_number'], ['cust_name'],
								['cust_dob'], ['home_phone'],
								['office_phone'], ['mobile_phone'],
								['campaign_id'], ['call_result'],
								['user_id']
							  ]
				doJava.setValue('');	
			}
}
 Ext.DOM.gotoCallCustomer = function()
 {
	var CustomerId  = Ext.Cmp('chk_cust_call').getChecked();
	if( CustomerId!='')
	{	
		if( CustomerId.length == 1 ) {
			Ext.EQuery.Ajax
			({
				url 	: Ext.DOM.INDEX +'/SrcCustomerClosing/ContactDetail/',
				method  : 'GET',
				param 	: {
					CustomerId : CustomerId,
					ControllerId : Ext.DOM.INDEX +'/SrcCustomerClosing/index/', 
					
				}
			});
		}
		else{ Ext.Msg("Select One Customers !").Info(); }			
	}
	else{ Ext.Msg("No Customers Selected !").Info(); }	
 }

	/* memanggil Jquery plug in */
	
		$(function(){
			
			
			$('#toolbars').extToolbars({
				extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
				extTitle  : [['Search'],['Go to Call '],['Clear']],
				extMenu   : [['searchCustomer'],['gotoCallCustomer'],['resetSeacrh']],
				extIcon   : [['zoom.png'],['telephone_go.png'],['cancel.png']],
				extText   : true,
				extInput  : true,
				extOption : [{
						render : 4,
						type   : 'combo',
						header : 'Call Reason ',
						id     : 'v_result_customers', 	
						name   : 'v_result_customers',
						triger : '',
						store  : Reason
					}]
			});
		});
		
		
</script>
<fieldset class="corner">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
	<div id="span_top_nav">					
	<div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
		<table cellpadding="3px;">
			<tr>
				<td class="text_caption"> Customer Name</td>
				<td><?php echo form() -> input('cust_name','input_text', _get_post('cust_name'));?></td>
				<td class="text_caption"> Gender</td>
				<td><?php echo form() -> combo('gender', 'select long', $GenderId, _get_post('gender')) ?></td>
				<td class="text_caption"> Card Type</td>
				<td><?php echo form() -> combo('card_type', 'select long', $CardType, _get_post('card_type')) ?> </td>
			</tr>
			<tr>
				<td class="text_caption"> Campaign Name</td>
				<td><?php echo form() -> combo('campaign_name','select long',$CampaignId, _get_post('campaign_name'));?></td>
				<td class="text_caption"> Verify Status</td>
				<td><?php echo form() -> combo('verify_status','select long',$ResultQuality, _get_post('verify_status'));?></td>
				<td class="text_caption">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
	</div>
	<div id="toolbars"></div>
	<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	