<table width="100%" class="custom-grid" cellspacing="0">
<thead>
	<tr height="20" > 
		<th nowrap class="custom-grid th-first">&nbsp;<b style='color:#608ba9;'>#</b></th>	
		<th nowrap class="custom-grid th-middle" align='left'>&nbsp;<b style='color:#608ba9;'>No</b></th>			
		<th nowrap class="custom-grid th-middle" align='left'>&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CustomerFirstName');"><b style='color:#608ba9;'>Cust Name</b></span></th>   
        <th nowrap class="custom-grid th-middle">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('g.AproveName');"><b style='color:#608ba9;'>Verify Status</b></span></th>
		<th nowrap class="custom-grid th-middle">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CustomerRejectedDate');"><b style='color:#608ba9;'>Verify Date</b></span></th>
		<th nowrap class="custom-grid th-lasted">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CustomerUpdatedTs');"><b style='color:#608ba9;'>Last Call Date</b></span></th>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows )
{ 
 $color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
 $QualityId = ( in_array($rows['CallReasonQue'],$BackLevel) ? '' : array('disabled' => $QualityId ) );	
?>
	<tr class="onselect" onclick=" Ext.DOM.gotoCallCustomer('<?php __($rows['CustomerId']);?>');">
		<td class="content-first"><?php echo form()->checkbox('chk_cust_call',null,$rows['CustomerId'],null,$QualityId); ?> </td>
		<td class="content-middle"><?php  echo $no; ?></td>
		<td class="content-middle" nowrap><?php echo $rows['CustomerFirstName']; ?></td>
		<td class="content-middle" style='color:green;text-align:center;'><?php echo ( $rows['AproveName']?$rows['AproveName'] : 'New Closing' ); ?></td>
		<td class="content-middle" style='color:green;text-align:center;'><?php echo ( $rows['CustomerRejectedDate']?$rows['CustomerRejectedDate'] : '-'); ?></td>
		<td class="content-lasted" style='color:green;text-align:center;'><?php echo ( $rows['CustomerUpdatedTs'] ? $rows['CustomerUpdatedTs'] : null); ?></td>
	</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>


