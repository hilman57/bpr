<?php 
echo javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/views/EUI_Contact.js', 'eui_'=>'1.0.0', 'time'=>time())));?>
	
<?php $this ->load ->view('src_closing_list/view_contact_javascript');?>

<!-- detail content -->

<fieldset class="corner"> 
<legend class="icon-customers"> &nbsp;&nbsp;Contact Detail </legend>
<div id="toolbars" class="contact"></div>

<div class="contact_detail" style="margin-left:-8px;">
<?php echo form()->hidden('ControllerId',NULL,_get_post("ControllerId"));?>
<table width="100%" border=0>
		<tr>
			<td  width="70%" valign="top">
				<?php $this ->load ->view('src_closing_list/view_contact_default_detail');?>
				<?php $this ->load ->view('src_closing_list/view_contact_history_detail');?>
			</td>
			<td  width="20%" rowspan="2" valign="top">
				<?php $this ->load ->view('src_closing_list/view_contact_phone_detail');?>
			</td>
		</tr>
	</table>
	<div id="WindowUserDialog" >
</div>
</fieldset>	
