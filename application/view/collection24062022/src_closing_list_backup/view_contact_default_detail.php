<fieldset class="corner" style="margin-top:-3px;"> 
<legend class="icon-menulist"> &nbsp;&nbsp;Customer Information</legend> 
<div id="contact_default_info" class="box-shadow box-left-top">
<form name="frmInfoCustomer">
<?php echo form()->hidden('CustomerId',NULL, $Customers['CustomerId']);?>
<?php echo form()->hidden('CustomerNumber',NULL, $Customers['CustomerNumber']);?>



<table width="99%" align="center" cellpadding="2px" cellspacing="4px">
	<tr>
		<td nowrap class="text_caption">Customer Name</td>
		<td><?php echo form() -> input('CustomerFirstName','input_text long',$Customers['CustomerFirstName'],NULL,1);?> </td>
		<td class="text_caption" nowrap>Card Type </td>
		<td><?php echo form() -> combo('CardTypeId','input_text long',$Combo['CardType'],$Customers['CardTypeId'],NULL,1); ?></td>
		<td class="text_caption" nowrap>Address3</td>
		<td><?php echo form() -> input('CustomerAddressLine3','input_text long',$Customers['CustomerAddressLine3'],NULL,1); ?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap >Gender</td>
		<td><?php echo form() -> combo('GenderId','input_text long',$Combo['Gender'], $Customers['GenderId'],NULL,1);?></td>
		<td class="text_caption" nowrap>Address 1</td>
		<td><?php echo form() -> input('CustomerAddressLine1','input_text long',$Customers['CustomerAddressLine1'],NULL,1);?></td>
		<td class="text_caption" nowrap>City  </td>
		<td><?php echo form() -> input('CustomerCity','input_text long',$Customers['CustomerCity'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap >DOB </td>
		<td><?php echo form() -> input('CustomerDOB','input_text long',$Customers['CustomerDOB'],NULL,1);?></td>
		<td class="text_caption" nowrap>Address 2</td>
		<td><?php echo form() -> input('CustomerAddressLine2','input_text long',$Customers['CustomerAddressLine2'],NULL,1);?></td>
		<td class="text_caption" nowrap>ZIP code</td>
		<td><?php echo form() -> input('CustomerZipCode','input_text long',$Customers['CustomerZipCode'],NULL,1);?></td>
	</tr>
</table>	
</form>
</div>
</fieldset>	
<?php $this ->load ->view('src_closing_list/view_customer_info');?>