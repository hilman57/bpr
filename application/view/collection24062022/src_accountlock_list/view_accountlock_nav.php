<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	lock_account_status : "<?php echo _get_exist_session('lock_account_status');?>",
	lock_agent_id 		: "<?php echo _get_exist_session('lock_agent_id');?>",
	lock_call_status 	: "<?php echo _get_exist_session('lock_call_status');?>",
	lock_campaign_id 	: "<?php echo _get_exist_session('lock_campaign_id');?>",
	lock_cust_id 		: "<?php echo _get_exist_session('lock_cust_id');?>",
	lock_cust_name 		: "<?php echo _get_exist_session('lock_cust_name');?>",
	lock_recsource 		: "<?php echo _get_exist_session('lock_recsource');?>",
	lock_start_amountwo : "<?php echo _get_exist_session('lock_start_amountwo');?>",
 	lock_end_amountwo 	: "<?php echo _get_exist_session('lock_end_amountwo');?>",
	lock_start_date 	: "<?php echo _get_exist_session('lock_start_date');?>",
	lock_end_date 		: "<?php echo _get_exist_session('lock_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/ModAccountLock/index/',
	custlist : Ext.DOM.INDEX+'/ModAccountLock/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('flwCustomers').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('flwCustomers').Clear();
	Ext.DOM.searchCustomer();
}

Ext.DOM.SetUnlockAccountAll = function(){
	if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_LEADER){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }
 
/* @ pack : handling data ---- */
 
 if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_INBOUND){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }
 
/* @ pack : handling data ---- */

  if( Ext.DOM.handling ==  Ext.DOM.USER_LEVEL_AGENT){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }

/* @ pack : handling data ---- */

 // if( CustomerId.length ==0 ){
	// Ext.Msg("Please select rows ").Info();
	// return false;
 // }
 
 Ext.Ajax
({
	url 	: Ext.DOM.INDEX+'/ModAccountLock/SetUnlockAccountAll',
	method 	: 'POST',
	param 	: { },
	ERROR	: function(e) {
	  Ext.Util(e).proc(function(response){
		  console.log(response.success);
		if( response.success ){
			Ext.Msg('Unlock Data ( '+ response.success +' )').Success();
			Ext.DOM.searchCustomer();
			
		} else {
			Ext.Msg('Unlock Data ( '+ response.success +' )').Failed();
		}
	  });	
   }	
}).post();

}


Ext.DOM.SetUnlockAccount = function() {
 
var CustomerId = Ext.Cmp('CustomerId').getValue();
 
/* @ pack : handling data ---- */
 
 if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_LEADER){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }
 
/* @ pack : handling data ---- */
 
 if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_INBOUND){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }
 
/* @ pack : handling data ---- */

  if( Ext.DOM.handling ==  Ext.DOM.USER_LEVEL_AGENT){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }

/* @ pack : handling data ---- */

 if( CustomerId.length ==0 ){
	Ext.Msg("Please select rows ").Info();
	return false;
 }
 
/* @ pack : then execute ---- */

Ext.Ajax
({
	url 	: Ext.DOM.INDEX+'/ModAccountLock/SetUnlockAccount',
	method 	: 'POST',
	param 	: { 
		CustomerId : CustomerId
	},
	ERROR	: function(e) {
	  Ext.Util(e).proc(function(response){
		if( response.success ){
			Ext.Msg('Unlock Data ( '+ response.success +' )').Success();
			Ext.DOM.searchCustomer();
			
		} else {
			Ext.Msg('Unlock Data ( '+ response.success +' )').Failed();
		}
	  });	
   }	
}).post();
	
}

// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Unlock Account'],['Unlock Account All']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['SetUnlockAccount'],['SetUnlockAccountAll']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['key_delete.png'],['key_delete.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="flwCustomers">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('lock_campaign_id','select auto', $CampaignId, _get_exist_session('lock_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('lock_agent_id','select auto', $Deskoll, _get_exist_session('lock_agent_id'));?></td>
			<td class="text_caption bottom"> # Last Call date &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('lock_start_date','input_text date', _get_exist_session('lock_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('lock_end_date','input_text date', _get_exist_session('lock_end_date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('lock_cust_id','input_text long', _get_exist_session('lock_cust_id'));?></td>
			<td class="text_caption bottom"> # Account Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('lock_account_status','select auto', $AccountStatus, _get_exist_session('lock_account_status'));?></td>
			<td class="text_caption bottom"> # Recsource &nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('lock_recsource','input_text long', _get_exist_session('lock_recsource'));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('lock_cust_name','input_text long', _get_exist_session('lock_cust_name'));?></td>
			<td class="text_caption bottom"> # Last Call Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('lock_call_status','select auto', $LastCallStatus, _get_exist_session('lock_call_status'));?></td>
			<td class="text_caption bottom"> # Amount WO&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('lock_start_amountwo','input_text box', _get_exist_session('lock_start_amountwo') );?>
				&nbsp; <span>to</span>&nbsp;	
				<?php echo form()->input('lock_end_amountwo','input_text box', _get_exist_session('lock_end_amountwo') );?>
			</td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->