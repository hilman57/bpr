<?php echo javascript(); ?>
<script type="text/javascript"> 

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';
	
	
var totals_pages 	= Ext.EQuery.TotalPage;
var totals_records 	= Ext.EQuery.TotalRecord;

var query_list_data = ''; 	//<#?php echo mysql_escape_string($db -> Pages -> _get_query());?>';

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 	
var getListCampaign = ( function()
{
  return ( 
	Ext.Ajax({
			url : Ext.DOM.INDEX +'/MgtBucket/getCampaignName/',
			method :'GET',
			param :{
				action:'get_campaign'
			}
		}).json()
	)
})(); 

var getListATM = ( function()
{
  return ( 
	Ext.Ajax({
			url : Ext.DOM.INDEX +'/MgtBucket/getATMName/',
			method :'GET',
			param :{
				action:'getATMName'
			}
		}).json()
	)
})(); 

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 	
$(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Back'],['Find'],[],[],[],[],['By CheckList'],['By Amount'],['Upload Data']],
		extMenu  :[['backtohome'],['FindBucket'],[],[],[],[],['Ext.DOM.Process'],['Ext.DOM.ProcessAmountATM'],['UploadManual']],
		extIcon  :[['house.png'],['find.png'],[],[],[],[],['drive_disk.png'],['drive_disk.png'],['database_go.png']],		
		extText  :true,
		extInput :true,
		extOption:[{
						render	: 5,
						header	: '#Product ',
						type	: 'combo',
						id		: 'combo_filter_campaign', 	
						name	: 'combo_filter_campaign',
						value	: '',
						store	: [getListCampaign], 
						triger	: '',
						width	: 110
					},{
						render	: 6,
						header	: '# TL ',
						type	: 'combo',
						id		: 'combo_filter_atm', 	
						name	: 'combo_filter_atm',
						value	: '',
						store	: [getListATM], 
						triger	: '',
						width	: 90
					},{
						render	: 2,
						value   : totals_records,
						type	: 'text',
						id		: 'find_sum_dta', 
						name	: 'find_sum_dta',
						width	: 60
						
					},{
						render	: 4,
						value   : 0,
						type	: 'text',
						id		: 'text_sum_asg', 
						name	: 'text_sum_asg',
						width	: 50
						
					},{
						render	: 3,
						label	: '<span style="color:#234777;"> Assign Data : </span>',
						type	: 'label',
						id		: 'text_label_dta', 
						name	: 'text_label_dta',
						width	: 80 
					}]
			});
			
	Ext.Cmp('find_sum_dta').disabled(true);
	$('#start_date,#end_date').datepicker({showOn: 'button', buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', buttonImageOnly: true, dateFormat:'dd-mm-yy'});	
});


// @ pack : clear content 

Ext.DOM.ClosePanel = function() {
	Ext.Cmp('panel-content').setText("");
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 
Ext.DOM.UploadManual = function(){
	Ext.Ajax({	
		url	  : Ext.DOM.INDEX +'/MgtBucket/ManualUpload',
		param : { 
			time : Ext.Date().getDuration()
		}
	}).load("panel-content");
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
Ext.DOM.Upload = function()
{
	Ext.Cmp("loading-image").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/MgtBucket/UploadBucket/',
		method 	: 'POST',
		param	: {
			TemplateId : Ext.Cmp('upload_template').getValue(),
			CampaignId : Ext.Cmp('upload_campaign').getValue()
		},
		complete : function(fn){
			var ERR = eval(fn.target.DONE);
			try {	
				if( ERR )
				{
					var CALLBACK = JSON.parse(fn.target.responseText);
					if( (typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null) ){
						var msg = "\n";
						for(var i in  CALLBACK.mesages ){
							msg += "Failed : "+ CALLBACK.mesages[i].N +",\nSuccess : "+CALLBACK.mesages[i].Y +"\n Blacklist : "+ CALLBACK.mesages[i].B +",\nDuplicate : "+  CALLBACK.mesages[i].D  +"\nDuplicate(s) : "+CALLBACK.mesages[i].X+"\n";
						}	
						Ext.Msg(msg).Error();
					}
					else{
						Ext.Msg(CALLBACK.mesages).Info();
					}	
					
					Ext.Cmp("loading-image").setText('');
				}	
			}
			catch(e){ alert(e); }
		}
	}).upload()
 }
 

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */

 Ext.DOM.ProcessByATMChecked = function()
{
	var atm = Ext.Cmp('combo_filter_atm').getValue();
	var ftp_list_id = Ext.Cmp('ftp_list_id').getValue();
	var campaign_id = Ext.Cmp('combo_filter_campaign').getValue();
	if( ftp_list_id =='' ) { alert('Please select a rows !'); return false; }
	else if( campaign_id=='') { alert('Please select a campaign !'); return false; }
	else if( atm=='') { alert('Please select User ATM !'); return false; }
	else 
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/MgtBucket/SaveByCheckedATM/',
			method  : 'POST',
			param   : {
				ftp_list_id : ftp_list_id,
				campaign_id : campaign_id,
				atm : atm
			},
			ERROR : function( fn ) {
				try
				{
					var ERR = JSON.parse( fn.target.responseText ); 
					Ext.Msg("Assign Data, Sucess : "+ERR.mesages._success +" , Duplicate :"+ERR.mesages._duplicate).Info();
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			} 
		}).post();
	}
}
 
Ext.DOM.ProcessAmount = function()
 {
	
	var amount_size = Ext.Cmp('find_sum_dta').getValue(),
		amount_assign = Ext.Cmp('text_sum_asg').getValue(),
		campaign_name = Ext.Cmp('combo_filter_campaign').getValue(),
		fileupload = Ext.Cmp('file_upload_id').getValue(),
		assign_status = Ext.Cmp('assign_data').getValue(),
		end_date	= Ext.Cmp('start_date').getValue(),
		start_date	= Ext.Cmp('start_date').getValue();
	
	if (fileupload==''){ 
		Ext.Msg("Please select file type").Info(); }
	else if (campaign_name==''){ 
		Ext.Msg("Please select campaign").Info();}
	else if( parseInt(amount_assign) < 1 ) { 
		Ext.Msg('Assign data can\'t zero').Info();}
	else
	{
		if( parseInt(amount_size) >= parseInt(amount_assign) )
		{
		 var ERROR = Ext.Ajax ({
				url 	: Ext.DOM.INDEX+'/MgtBucket/saveByAmount/',
				method  : 'POST',
				param 	: {
					amount_size   : amount_size,
					amount_assign : amount_assign,
					campaign_name : campaign_name,
					fileupload	  : fileupload,
					assign_status : assign_status,
					start_date 	  : start_date, 
					end_date	  :	end_date
				},
				ERROR : function(fn){
					try
					{
						var ERR = JSON.parse( fn.target.responseText ); 
						Ext.Msg("Assign Data, Sucess : "+ERR.mesages._success +" , Duplicate :"+ERR.mesages._duplicate).Info();
						Ext.EQuery.postContent();
					}
					catch(e){
						Ext.Msg(e).Error();
					}
				}
				
			}).post();
		}
		else{
			Ext.Msg("Assign Data").Failed();
		}
	}
 }

 Ext.DOM.ProcessAmountATM = function()
 {
	
	var atm = Ext.Cmp('combo_filter_atm').getValue(),
		amount_size = Ext.Cmp('find_sum_dta').getValue(),
		amount_assign = Ext.Cmp('text_sum_asg').getValue(),
		campaign_name = Ext.Cmp('combo_filter_campaign').getValue(),
		fileupload = Ext.Cmp('file_upload_id').getValue(),
		assign_status = Ext.Cmp('assign_data').getValue(),
		end_date	= Ext.Cmp('start_date').getValue(),
		start_date	= Ext.Cmp('start_date').getValue();
	
	if (fileupload==''){ 
		Ext.Msg("Please select file type").Info(); }
	else if (campaign_name==''){ 
		Ext.Msg("Please select campaign").Info();}
	else if (atm==''){ 
		Ext.Msg("Please select User ATM").Info();}
	else if( parseInt(amount_assign) < 1 ) { 
		Ext.Msg('Assign data can\'t zero').Info();}
	else
	{
		if( parseInt(amount_size) >= parseInt(amount_assign) )
		{
		 var ERROR = Ext.Ajax ({
				url 	: Ext.DOM.INDEX+'/MgtBucket/saveByAmountATM/',
				method  : 'POST',
				param 	: {
					amount_size   : amount_size,
					amount_assign : amount_assign,
					campaign_name : campaign_name,
					fileupload	  : fileupload,
					assign_status : assign_status,
					start_date 	  : start_date, 
					end_date	  :	end_date,
					atm     	  : atm
				},
				ERROR : function(fn){
					try
					{
						var ERR = JSON.parse( fn.target.responseText ); 
						Ext.Msg("Assign Data, Sucess : "+ERR.mesages._success +" , Duplicate :"+ERR.mesages._duplicate).Info();
					}
					catch(e){
						Ext.Msg(e).Error();
					}
				}
				
			}).post();
		}
		else{
			Ext.Msg("Assign Data").Failed();
		}
	}
 }
 
/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 	
Ext.DOM.Process = function()
{
	var ftp_list_id = Ext.Cmp('ftp_list_id').getValue();
	var campaign_id = Ext.Cmp('combo_filter_campaign').getValue();
	if( ftp_list_id =='' ) { alert('Please select a rows !'); return false; }
	else if( campaign_id=='') { alert('Please select a campaign !'); return false; }
	else 
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/MgtBucket/SaveByChecked/',
			method  : 'POST',
			param   : {
				ftp_list_id : ftp_list_id,
				campaign_id : campaign_id
			},
			ERROR : function( fn ) {
				try
				{
					var ERR = JSON.parse( fn.target.responseText ); 
					Ext.Msg("Assign Data, Sucess : "+ERR.mesages._success +" , Duplicate :"+ERR.mesages._duplicate).Info();
					Ext.EQuery.postContent();
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			} 
		}).post();
	}
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 
var Deleted = function()
{
	var ftp_list_id = Ext.Cmp('ftp_list_id').getValue();
	if( ftp_list_id =='' ) { alert('Please select a rows !'); return false; }
	else if( ftp_list_id.length<1 ){ alert('Please select a rows !'); return false; }
	else
	{
		Ext.Ajax({
			url 	: Ext.DOM.SYSTEM+'/controller/EUI_Controll.bucket.php',
			method 	: 'POST',
			param 	: {
				action : 'deleted_to_ftpbucket',
				ftp_list_id : ftp_list_id
			},
			ERROR : function( e ){
				try { 
					var  ERROR = JSON.parse(e.target.responseText );
					if( ERROR.result ){
						alert("Succes, Deleted FTP Bucket data with ( "+ERROR.totals_success+" ) rows data !");
						$('#main_content').load('act_ftp_bucket_nav.php');
					}
					else{
						alert("Failed, Deleted FTP Bucket data with ( "+ERROR.totals_success+" ) rows data !");
						return false;
					}
				}
				catch(e){ alert(e); }
			}
			
		}).post();
	}
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
Ext.DOM.backtohome = function()
{
	if( confirm('Do you want to back campaign setup ?')){
		Ext.Ajax({ url : Ext.DOM.INDEX+'/SetCampaign/', method :'GET', param : {}}).load("main_content");
	}
	else
		return false;
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 	
var datas=
{ 
	order_by 	: '<?php echo _get_post('order_by');?>',
	type	 	: '<?php echo _get_post('type');?>',
	work_branch : '<?php echo _get_post('work_branch');?>',
	city 		: '<?php echo _get_post('city');?>',
	card_type 	: '<?php echo _get_post('card_type');?>',
	start_date 	: '<?php echo _get_post('start_date');?>',
	end_date 	: '<?php echo _get_post('end_date');?>',
	assign_data : '<?php echo _get_post('assign_data');?>',
	file_upload : '<?php echo _get_post('file_upload');?>',
	file_product_id : '<?php echo _get_post('file_product_id');?>'
}
	
/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 		
Ext.DOM.navigation = 
{
	custnav	 : Ext.DOM.INDEX+'/MgtBucket/index/',
	custlist : Ext.DOM.INDEX+'/MgtBucket/Content/'
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 
 Ext.EQuery.construct(Ext.DOM.navigation,datas)
 Ext.EQuery.postContentList();
		
/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 
Ext.DOM.FindBucket = function()
{
	Ext.EQuery.construct( navigation, {
		work_branch : Ext.Cmp('work_branch').getChecked(),
		assign_data : Ext.Cmp('assign_data').getValue(),
		start_date 	: Ext.Cmp('start_date').getValue(),
		end_date 	: Ext.Cmp('end_date').getValue(),
		file_upload : Ext.Cmp('file_upload_id').getValue(),
		file_product_id: Ext.Cmp('file_product_id').getValue()	
	});
	Ext.EQuery.postContent();
}		
			
</script>	
<fieldset class="corner">
	<legend class="icon-campaign">&nbsp;&nbsp;Bucket Data</legend>	
	<div class="box-shadowx" style="border:1px solid #ddd;margin-bottom:8px;margin-top:8px;margin-right:5px;margin-left:5px;">
		<table cellspacing='6px' style="margin:6px" border=0 width='100%'>
			<tr>
				<td valign="top" width='50%'>
				<!-- center content box -->
				<div id="center-box">
					<table cellspacing='3px'>
						<tr>
							<td valign="top" class="left text_caption bottom"># File Name</td>
							<td valign="top" class="text_caption bottom">:</td>
							<td valign="top" class="left bottom"><?php echo form() -> combo('file_upload_id','select',$Filename,$this ->URI->_get_post('file_upload'),null );?></td>
						</tr>
						<tr>
							<td valign="top" class="left text_caption bottom"># Product </td>
							<td valign="top" class="text_caption bottom">:</td>
							<td valign="top" class="left bottom"><?php echo form()->combo('file_product_id','select',$ProductId,$this ->URI->_get_post('file_product_id'),null);?></td>
						</tr>
						<tr>
							<td class="left text_caption bottom" valign='top'># Upload Date</td>
							<td class="left text_caption bottom" valign='top'>:</td>
							<td class="left bottom" valign="top"><?php echo form() -> input('start_date','input_text date',$this ->URI->_get_post('start_date'));?>&nbsp;-&nbsp;<?php echo form() -> input('end_date','input_text date',$this ->URI->_get_post('end_date'));?></td>
						</tr>
						<tr>
							<td class="text_caption bottom" valign='top'># Assign Status</td>
							<td class="text_caption bottom" valign='top'>:</td>
							<td class="left bottom" valign="top"><?php echo form()->combo('assign_data','select long',array(0=>'Not Assign',1=>'Assign'),($this ->URI-> _get_have_post('assign_data')?$this ->URI->_get_post('assign_data'):null)); ?></td>
						</tr>
					</table>
				</div>
				<!-- center stop content box -->
				</td width='50%'>
				<td valign="top">
					<div id="right-box"> 
						<div id="panel-content"></div>
					</div>
				</td>
			</tr>	
		</table>
	</div>
	<div id="toolbars"></div>
	<div class="box-shadow" style="background-color:#FFFFFF;margin-top:10px;">	
	<div class="content_table" id="content_table"></div>
	<div id="pager"></div>
	</div>	
</fieldset>	