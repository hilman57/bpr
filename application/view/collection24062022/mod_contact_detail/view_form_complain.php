<?php echo base_window_header("Form Complain"); ?>
<style> 
.ui-chrome{ border-radius:3px;}
.ui-chrome:hover{ border:1px solid #FF4321;}
.ui-context-chrome { border-radius:4px;height:22px;}
</style>
<script>
// efine url 
 
 var URL = function () 
{
	return window.opener.Ext.DOM.INDEX;
}
var phoneblock = function()
{
	return window.opener.BLOCK_PHONE;
}
Ext.DOM.SaveComplain = function()
{
	if(phoneblock()=="")
	{
		alert("Please block some phone");
		return false;
	}
	var FrmComplain = Ext.Serialize('frmComplain');
	var VarComplain = new Array();
	
	VarComplain['CustomerId'] = Ext.Cmp("DebiturId").getValue();
	VarComplain['PhoneBlock'] = phoneblock();
	Ext.Ajax
	({
		url 	: URL()+"/ModSaveActivity/save_complain_note/",
		method 	: "POST",
		param 	: Ext.Join([
					FrmComplain.getElement() ,VarComplain
				]).object(),
		ERROR :function( e ) {
			Ext.Util(e).proc(function( response ){
				if( response.success ){
					Ext.Msg("Complain").Success();
					Ext.Serialize('frmComplain').Clear();
				} else {
					Ext.Msg("Complain").Failed();
				}
			});
		}
		
	}).post();
};
</script>
</head>
<body style="margin:10px 5px 5px 5px;">
	<fieldset class="corner" style="border-radius:3px;margin-top:10px;">
		<legend class="edit-users-x"><span style="margin-left:8px;">Complain</span></legend>
		<form name="frmComplain">
		<?php echo form()->hidden('DebiturId',null, $DEBITUR);?>
		<?php echo "\r\n";?>
		<div class="ui-widget-form-table-compact">
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Source Complain</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->combo('source_complain', 'select long',$SOURCE_COMPLAIN );?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Type of Cases</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->combo('jenis_kasus', 'select long',$KASUS_KOMPLEN );?></div>
			</div>
			
			<!-- <div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Last blocked phone</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php //echo form()->checkbox("cb_ptp_reminder",null,1); ?></div>
			</div> -->
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Action taken</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->textarea('text_action_taken','textarea', null, null, array("style"=> "width:300px;height:70px;") );?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Result</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->textarea('text_result','textarea', null, null, array("style"=> "width:300px;height:70px;") );?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption"></div>
				<div class="ui-widget-form-cell center"></div>
				<div class="ui-widget-form-cell left">
					<?php echo form()->button('btnshow', 'button save', "Save", array("click" => "Ext.DOM.SaveComplain();") );?>
				</div>
			</div>
			
		</div>
		</form>
		
	</fieldset>
</body>
</html>