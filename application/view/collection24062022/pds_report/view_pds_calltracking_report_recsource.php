<?php
	$this->load->view('pds_report/view_call_tracking_style');
?>

<title>PDS Report</title>
PDS Report <br> Date <?=$param['start_date']?> to <?=$param['end_date']?> <br>
Printed By: <?=_get_session('Username')?> <br>
Print Date: <?=date('m/d/Y H:i:s')?><p></p>

<?php
	foreach($recsource as $CampaignCode => $Recsource){
		echo "<p>$CampaignCode</p>";
?>
<table border=0 cellpadding=1 cellspacing=1 style='border-collapse:collapse;'>
	<tr>
		<td class=xl66 rowspan=2>Recsource</td>
		<td class=xl66 rowspan=2>Data Size</td>
		<td class=xl66 rowspan=2>Utilize</td>
		<?php
			$colspan1 = count($getCallInitiatedHeader);
			echo "<td class=xl66 colspan=$colspan1>Call Initiated</td>";
			
			$colspan2 = count($getStatusContacted);
			echo "<td class=xl66 colspan=$colspan2>Contacted</td>";
		?>
		<td class=xl66 rowspan=2>Total Contacted</td>
		<td class=xl66 rowspan=2>Uncontacted</td>
	</tr>
	<tr>
		<?php
			foreach($getCallInitiatedHeader as $Id => $Head){
				echo "<td class=xl66>".$Head."</td>";
			}

			foreach($getStatusContacted as $Desc => $Id){
				echo "<td class=xl66>".$Desc."</td>";
			}
		?>
	</tr>
	<?php
	// echo "<pre>";
	// print_r($Recsource);
	// echo "</pre>";
	foreach($Recsource as $Recs => $Rows){
		echo "<tr calss=x174>";
			echo "<td class=xl71>".$Recs."</td>";
			echo "<td class=xl71>".($getDataSize[$CampaignCode][$Recs]['DataSize']?$getDataSize[$CampaignCode][$Recs]['DataSize']:0)."</td>";
			
			foreach($getStatusContacted as $CallReasonDesc => $Id){
				$totalDataConPerReason[$CampaignCode][$Recs] += ($getDataCallTracking[$CampaignCode][$Recs][$CallReasonDesc]?$getDataCallTracking[$CampaignCode][$Recs][$CallReasonDesc]:0);
			}
			foreach($getStatusUncontacted as $CallReasonDesc => $Id){
				$dataUnconPerReason[$CampaignCode][$Recs] = ($getDataCallTracking[$CampaignCode][$Recs][$CallReasonDesc]?$getDataCallTracking[$CampaignCode][$Recs][$CallReasonDesc]:0);
			}
			$DataUtilize[$CampaignCode][$Recs] = $totalDataConPerReason[$CampaignCode][$Recs]+$dataUnconPerReason[$CampaignCode][$Recs];
			// kolom Utilize
			echo "<td class=xl71>".$DataUtilize[$CampaignCode][$Recs]."</td>";
			
			foreach($getStatusContacted as $CallReasonDesc => $Id){
				$InitiateContact[$CampaignCode][$Recs] += ($getDataCallInitiate[$CampaignCode][$Recs][$CallReasonDesc]?$getDataCallInitiate[$CampaignCode][$Recs][$CallReasonDesc]:0);
			}
			foreach($getStatusUncontacted as $CallReasonDesc => $Id){
				$InitiateUncontact[$CampaignCode][$Recs] += ($getDataCallInitiate[$CampaignCode][$Recs][$CallReasonDesc]?$getDataCallInitiate[$CampaignCode][$Recs][$CallReasonDesc]:0);
			}
			
			//kolom-kolom Call Initiated
			echo "<td class=xl71>".$InitiateContact[$CampaignCode][$Recs]."</td>";
			echo "<td class=xl71>".round(($InitiateContact[$CampaignCode][$Recs]/$DataUtilize[$CampaignCode][$Recs]),1)."</td>";
			echo "<td class=xl71>".$InitiateUncontact[$CampaignCode][$Recs]."</td>";
			echo "<td class=xl71>".round(($InitiateUncontact[$CampaignCode][$Recs]/$DataUtilize[$CampaignCode][$Recs]),1)."</td>";
			
			foreach($getStatusContacted as $CallReasonDesc => $Id){
				$dataConPerReason[$CampaignCode][$Recs] = ($getDataCallTracking[$CampaignCode][$Recs][$CallReasonDesc]?$getDataCallTracking[$CampaignCode][$Recs][$CallReasonDesc]:0);
				$totalContacted[$CampaignCode][$CallReasonDesc] += $dataConPerReason[$CampaignCode][$Recs];
				echo "<td class=xl71>".$dataConPerReason[$CampaignCode][$Recs]."</td>";
			}
			// kolom Total Contacted
			echo "<td class=xl71>".$totalDataConPerReason[$CampaignCode][$Recs]."</td>";
			// kolom Uncontacted
			echo "<td class=xl71>".$dataUnconPerReason[$CampaignCode][$Recs]."</td>";
		
		$Sum_DataSize[$CampaignCode]		+=$getDataSize[$CampaignCode][$Recs]['DataSize'];
		$Sum_DataUtilize[$CampaignCode]		+=$DataUtilize[$CampaignCode][$Recs];
		$Sum_DataContacted[$CampaignCode]	+=$totalDataConPerReason[$CampaignCode][$Recs];
		$Sum_DataUncontacted[$CampaignCode]	+=$dataUnconPerReason[$CampaignCode][$Recs];
		
		$Sum_InitiateContact[$CampaignCode]		+=$InitiateContact[$CampaignCode][$Recs];
		$Sum_InitiateUncontact[$CampaignCode]	+=$InitiateUncontact[$CampaignCode][$Recs];
		
		echo "</tr>";
	}
	?>
	<tr>
		<td class=xl66>Total</td>
		<td class=xl66><?php echo $Sum_DataSize[$CampaignCode]; ?></td>
		<td class=xl66><?php echo $Sum_DataUtilize[$CampaignCode]; ?></td>
		<td class=xl66><?php echo $Sum_InitiateContact[$CampaignCode]; ?></td>
		<td class=xl66><?php echo round(($Sum_InitiateContact[$CampaignCode]/$Sum_DataUtilize[$CampaignCode]),1); ?></td>
		<td class=xl66><?php echo $Sum_InitiateUncontact[$CampaignCode]; ?></td>
		<td class=xl66><?php echo round(($Sum_InitiateUncontact[$CampaignCode]/$Sum_DataUtilize[$CampaignCode]),1); ?></td>
		<?php
		foreach($getStatusContacted as $CallReasonDesc => $Id){
			echo "<td class=xl66>".$totalContacted[$CampaignCode][$CallReasonDesc]."</td>";
		}
		?>
		<td class=xl66><?php echo $Sum_DataContacted[$CampaignCode]; ?></td>
		<td class=xl66><?php echo $Sum_DataUncontacted[$CampaignCode]; ?></td>
	</tr>
</table>

<?php
	}
?>