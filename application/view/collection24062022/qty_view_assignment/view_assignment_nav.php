<?php echo javascript(); ?>
<script type="text/javascript">
Ext.document('document').ready(function(){

/* 
 * @ def : handle Title every page controller 
 * ---------------------------------------
 * 
 * @ param : namespace < Ext > ( object )
 * @ param : -
 */
 
Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
	
/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : -
 * @ param : -
 */
 
 Ext.DOM.ViewPanelLeft = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/QualityAssignment/FilterAssignment/',
		method : 'GET',
		param :{ 
			time : Ext.Date().getDuration()
		}	
	}).load("hader-panel-content-left");
 }
 
// loader :  
Ext.DOM.ViewPanelLeft(); 
 
/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : -
 * @ param : -
 */
Ext.DOM.DetailData = function( VAR ){
  Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/QualityAssignment/'+ VAR.controller +'/',
		method 	: 'POST',
		param 	: {
			CampaignId  : Ext.Cmp('CampaignId').getValue(),
			start_date  : Ext.Cmp('start_date').getValue(),
			end_date 	: Ext.Cmp('end_date').getValue(),
			start_page  : VAR.page
		}	
	}).load('result_content_combo');
} 
	
	
Ext.DOM.SelectedPage = function( page ){
	Ext.DOM.DetailData({
		controller : 'ShowDataByChecked',
		page : page
	});
}	
/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : -
 * @ param : -
 */
 
 Ext.DOM.ViewPanelRight = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/QualityAssignment/ContentFilter/',
		method : 'GET',
		param :{ 
			time : Ext.Date().getDuration()
		}	
	}).load("hader-panel-content-right");
 }
 
// loader :  
Ext.DOM.ViewPanelRight(); 
  

/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */
 
Ext.DOM.DetailAssignment = function( sel ){
	if( sel.value == 'CHECKED' ){
		Ext.DOM.DetailData({ controller : 'ShowDataByChecked', page : 0 });
	}
	
	if( sel.value == 'AMOUNT' ){
		Ext.DOM.DetailData({ controller : 'ShowDataByAmount', page : 0 });
	}	
}
 
  

/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */

 
Ext.DOM.AssignByChecked = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/QualityAssignment/AssignByChecked/',
		method : 'POST',
		param : {
			AssignId : Ext.Cmp('AssignId').getChecked(),
			QualityStaffId : Ext.Cmp('QualityStaffId').getChecked()
		},
		
		ERROR : function(e){
			Ext.Util(e).proc(function(items){
				if( items.success ){
					Ext.Msg("Assign Data").Success();
					Ext.DOM.DetailData({ 
						controller : 'ShowDataByChecked', 
						page : 0 
					});
				}
				else{
					Ext.Msg("Assign Data").Failed();
				}
			});
		}
	}).post();
}
  
  
 

/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */

 
Ext.DOM.AssignByAmount = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/QualityAssignment/AssignByAmount/',
		method : 'POST',
		param : {
			AssignSizeData : Ext.Cmp('view_size_amount').getValue(),
			QualityStaffId : Ext.Cmp('QualityStaffId').getChecked(),
			CampaignId  : Ext.Cmp('CampaignId').getValue(),
			start_date  : Ext.Cmp('start_date').getValue(),
			end_date 	: Ext.Cmp('end_date').getValue(),
		},
		
		ERROR : function(e){
			Ext.Util(e).proc(function(items){
				if( items.success ){
					Ext.Msg("Assign Data").Success();
					Ext.DOM.DetailData({ 
						controller : 'ShowDataByAmount', 
						page : 0 
					});
				}
				else{
					Ext.Msg("Assign Data").Failed();
				}
			});
		}
	}).post();
}
    

/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */

 
Ext.DOM.AssignData = function()
{
	var select_type = Ext.Cmp('submit_filter').getValue();
	if( Ext.Cmp('submit_filter').empty()!=true ) 
	{
		switch( select_type )
		{
			case 'CHECKED' : Ext.DOM.AssignByChecked();  break;	
			case 'AMOUNT'  : Ext.DOM.AssignByAmount();   break;	
		}	
	}
	else{
		Ext.Msg("Please select Method").Info();
	}	
}

 
/* 
 * @ def : Ext.DOM.StaffAvailable 
 * ---------------------------------------
 * 
 * @ param : Unit Test
 * @ param : Unit Test
 */

 $('.date').datepicker({showOn: 'button', buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', buttonImageOnly: true, dateFormat:'dd-mm-yy',readonly:true});
 
});
</script>
<!-- start : content -->
<fieldset class="corner">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span> </legend>	
	<div id="header-panel-content-top"> 
		<div id="hader-panel-content-left" style='float:left'>&nbsp;</div>
		<div id="hader-panel-content-right" style='float:left;margin-left:8px;'>&nbsp;</div>
	</div>
</fieldset>	