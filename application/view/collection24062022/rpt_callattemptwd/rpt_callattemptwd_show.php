<html>
<head>
	<?php
		$Month = date("F - Y");
		
		$this -> load -> view('rpt_callattemptwd/rpt_callattemptwd_style');
	?>
</head>
<title>Welcome Call - Call Attempt & WD Finished</title>
<body>
	<table border=0 cellpadding=0 cellspacing=0 width=492 class=xl9730232 style='border-collapse:collapse;table-layout:fixed;width:370pt'>
		<tr height=36 style='mso-height-source:userset;height:27.0pt'>
			<td colspan=9 height=36 class=xl11130232 dir=LTR width=432 style='height:27.0pt;width:325pt'>Welcome Call - Call Attempt &amp; WD Finished</td>
			<td class=xl9730232 width=60 style='width:45pt'></td>
		</tr>
		<tr height=36 style='mso-height-source:userset;height:27.0pt'>
			<td colspan=10 height=36 class=xl10130232 dir=LTR width=348 style='height:27.0pt;width:262pt'><?php echo $Month; ?></td>
		</tr>
		
		<tr height=20 style='height:15.0pt'>
			<td colspan=2 height=20 class=xl9830232 dir=LTR width=96 style='height:15.0pt;width:72pt'>A. Call Attempt</td>
			<td colspan=2 class=xl9830232 dir=LTR width=96 style='border-left:none;width:73pt'>Amount</td>
			<td class=xl9830232 dir=LTR width=60 style='border-left:none;width:45pt'>%</td>
		</tr>
		
		<?php
		
		for($x=1;$x<=9;$x++)
		{
			$Percentage = 0;
			
			$Amount = ($call_attempt[$x]['Amount']?$call_attempt[$x]['Amount']:0);
			$Total = ($call_attempt[$x]['Total']?$call_attempt[$x]['Total']:0);
			$Percentage = (($Amount / $Total) * 100);
			
			?>
		<tr height=20 style='height:15.0pt'>
			<td colspan=2 height=20 class=xl10330232 dir=LTR width=96 style='height:15.0pt;width:72pt'><?php echo $x; ?></td>
			<td colspan=2 class=xl10430232 dir=LTR width=96 style='border-left:none;width:73pt'><?php echo number_format($Amount); ?></td>
			<td class=xl9930232 dir=LTR align=right width=60 style='border-top:none;border-left:none;width:45pt'><?php echo round($Percentage,2); ?> %</td>
		</tr>
			<?php
			$sAmount += $Amount;
		}
		?>
		
		<tr height=20 style='height:15.0pt'>
			<td colspan=2 height=20 class=xl10530232 dir=LTR align=left width=96 style='height:15.0pt;width:72pt'>Total</td>
			<td colspan=2 class=xl10630232 dir=LTR align=right width=96 style='border-left:none;width:73pt'><?php echo $sAmount; ?></td>
		</tr>
		
		<tr height=18 style='mso-height-source:userset;height:13.5pt'>
			<td height=18 class=xl9730232 style='height:13.5pt' colspan="10"></td>
		</tr>
		
		<tr height=24 style='mso-height-source:userset;height:18.0pt'>
			<td colspan=6 height=24 class=xl10730232 dir=LTR align=left width=264 style='height:18.0pt;width:199pt'>B. Time to Call(Connected &amp; Answered)</td>
			<td colspan=2 class=xl10930232 dir=LTR width=144 style='border-left:none; width:108pt'>Amount</td>
			<td colspan=2 class=xl9830232 dir=LTR width=84 style='border-left:none;width:63pt'>%</td>
		</tr>
		<?php
			for($s_i=8; $s_i<=22; $s_i++)
			{
				$Percentage = 0;
				
				$s_h = (strlen($s_i)==1)?"0".$s_i:$s_i;
				$AmountHour = ($call_time[$s_i]['Amount']?$call_time[$s_i]['Amount']:0);
				$TotalHour = ($call_time[$s_i]['Total']?$call_time[$s_i]['Total']:0);
				$PercentageHour = (($AmountHour / $TotalHour) * 100);
				
			?>
			
			<tr height=24 style='mso-height-source:userset;height:18.0pt'>
				<td colspan=6 height=24 class=xl10330232 dir=LTR width=264 style='height:18.0pt;width:199pt'><?php echo $s_h.":00 - ".$s_h ?>:59</td>
				<td colspan=2 class=xl10430232 dir=LTR width=144 style='border-left:none;width:108pt'><?php echo $AmountHour; ?></td>
				<td colspan=2 class=xl9930232 dir=LTR align=right width=84 style='border-left:none;width:63pt'><?php echo round($PercentageHour,2); ?>%</td>
			</tr>
			
			<?php
				$sAmountHour += $AmountHour;
			}
		?>
		
		<tr height=24 style='mso-height-source:userset;height:18.0pt'>
			<td colspan=6 height=20 class=xl10530232 dir=LTR align=left width=96 style='height:15.0pt;width:72pt'>Total</td>
			<td colspan=2 class=xl10630232 dir=LTR align=right width=96 style='border-left:none;width:73pt'><?php echo $sAmountHour; ?></td>
		</tr>
		
		<tr height=18 style='mso-height-source:userset;height:13.5pt'>
			<td height=18 class=xl9730232 style='height:13.5pt' colspan="10"></td>
		</tr>
		
		<tr height=24 style='mso-height-source:userset;height:18.0pt'>
			<td colspan=6 height=24 class=xl10730232 dir=LTR align=left width=264 style='height:18.0pt;width:199pt'>C. WD Finished (All status completed)</td>
			<td colspan=2 class=xl10930232 dir=LTR width=144 style='border-left:none;width:108pt'>Total</td>
			<td colspan=2 class=xl9830232 dir=LTR width=84 style='border-left:none;width:63pt'>%</td>
		</tr>
		
		<?php
		
		for($y=0;$y<=9;$y++)
		{
			$WDercentage = 0;
			
			$WDAmount = ($wd_finished[$y]['Amount']?$wd_finished[$y]['Amount']:0);
			$WDTotal = ($wd_finished[$y]['Total']?$wd_finished[$y]['Total']:0);
			$WDercentage = (($WDAmount / $WDTotal) * 100);
			
			?>
		<tr height=24 style='mso-height-source:userset;height:18.0pt'>
			<td colspan=6 height=24 class=xl11030232 dir=LTR align=right width=264 style='height:18.0pt;width:199pt'><?php echo $y; ?></td>
			<td colspan=2 class=xl10430232 dir=LTR width=144 style='border-left:none;width:108pt'><?php echo $WDAmount; ?></td>
			<td colspan=2 class=xl9930232 dir=LTR align=right width=84 style='border-left:none;width:63pt'><?php echo round($WDercentage,2); ?> %</td>
		</tr>
		<?php
			$sWDAmount += $WDAmount;
		}
		?>
		
		
		<tr height=24 style='mso-height-source:userset;height:18.0pt'>
			<td colspan=6 height=20 class=xl10530232 dir=LTR align=left width=96 style='height:15.0pt;width:72pt'>Total</td>
			<td colspan=2 height=20 class=xl10530232 dir=LTR align=left width=96 style='height:15.0pt;width:72pt'><?php echo $sWDAmount; ?></td>
		</tr>
	</table>
</body>
</html>