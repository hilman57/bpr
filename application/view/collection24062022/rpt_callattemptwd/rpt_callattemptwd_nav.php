<form name="frmReport">
<script>	
	Ext.DOM.ShowReport=function()
	{
		if(Ext.Cmp('start_date').getValue()=='' && Ext.Cmp('end_date').getValue()=='')
		{
			alert('Please Choose Interval!');
		}
		else
		{
			Ext.Window
			(
				{
					url 	: Ext.DOM.INDEX +'/CallAttemptWD/ShowReport/',
					param	: 	{
									start_date	: Ext.Cmp('start_date').getValue(),
									end_date	: Ext.Cmp('end_date').getValue()
								}
				}
			).newtab();
		}
	}
	
	Ext.DOM.ShowExcel = function()
	{
		Ext.Window
		(
			{
				url 	: Ext.DOM.INDEX +'/CallAttemptWD/ShowExcel/',
				param 	:	{ 
								start_date	: Ext.Cmp('start_date').getValue(),
								end_date	: Ext.Cmp('end_date').getValue()
							}
			}
		).newtab();
	}
	
	Ext.query('.date').datepicker({
			showOn : 'button', 
			buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
			buttonImageOnly	: true, 
			dateFormat : 'dd-mm-yy',
			readonly:true,
			onSelect	: function(date){
				if(typeof(date) =='string'){
					var x = date.split('-');
					var retur = x[2]+"-"+x[1]+"-"+x[0];
					if(new Date(retur) > new Date()) {
						Ext.Cmp($(this).attr('id')).setValue('');
					}
				}
			}
		});
</script>
<fieldset class="corner" style='margin-top:0px;'>
<legend class="icon-menulist">&nbsp;&nbsp;Call Attempt & WD Report </legend>
<div>
	<table cellpadding='4' cellspacing=4>
		<tr>
			<td class="text_caption bottom">Interval </td>
			<td class='bottom'>
				<?php __(form()->input('start_date','input_text box date'));?> &nbsp-
				<?php __(form()->input('end_date','input_text box date'));?>
			</td>
		</tr>
		
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td class='bottom'>
				<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
				<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
			</td>
		</tr>
	</table>
</div>
</fieldset>
</form>