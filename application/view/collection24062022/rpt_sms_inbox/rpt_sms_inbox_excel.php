<!DOCTYPE html>
<html>
<head>
	<style>
		html {
			font-family: Trebuchet MS,Arial,sans-serif;
			font-size: 12px;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td {
			padding: 5px;
			position: center;
		}
		#color {
			background-color : #00FFFF;
		}
	</style>
<title>Report SMS Inbox</title>
</head>
<body>
	<h1>Report SMS Inbox</h1>
	<!-- <h4>START DATE : <?php //echo $_REQUEST['start_date']; ?></h4>
	<h4>END DATA   : <?php //echo $_REQUEST['end_date']; ?></h4> -->
	<?php 
		// $date = str_replace('/', '-', $date);
		$start =  date('Y-m-d', strtotime($_REQUEST['start_date']));
		$end   =  date('Y-m-d', strtotime($_REQUEST['end_date']));

		$sql = "SELECT IF(b.deb_id IS NULL,0,b.deb_id) AS CustomerId,
				IF(b.deb_acct_no IS NULL,'Unknown',b.deb_acct_no) AS CustomerNumber,
				IF(b.deb_name IS NULL,'Unknown',b.deb_name) AS CustomerName,
				a.TextMessage AS Message, b.deb_wo_date as WoDate, a.Sender AS PhNumber,
				IF(a.IsRead=0,'New Message','Read') AS ReadStatus,
				a.RecvDate AS RecvDate 
				FROM ( serv_sms_inbox a ) 
				LEFT JOIN t_gn_debitur b ON a.MasterId = b.deb_id 
				LEFT JOIN t_gn_assignment c ON b.deb_id = c.CustomerId 
				WHERE 1=1 and a.RecvDate >='".$start." 00:00:00' and a.RecvDate <= '".$end." 23:59:59' ";
		// echo "<pre>".$sql."</pre>";
		$query =  @mysql_query($sql);
	?>
	<table>
			<tr>
				<th id="color">No.</th>
				<th id="color">CustomerId</th>
				<th id="color">CustomerName</th>
				<th id="color" width="600">Message</th>
				<th id="color">WO Date</th>
				<th id="color">PhNumber</th>
				<th id="color">Message Status</th>
				<th id="color">Reply Date</th>
			</tr>
			<?php 
				$no = 1;
				while( $rows = @mysql_fetch_array($query)){
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $rows['CustomerId']; ?></td>
				<td><?php echo $rows['CustomerName']; ?></td>
				<td><?php echo $rows['Message']; ?></td>
				<td><?php echo $rows['WoDate']; ?></td>
				<td><?php echo $rows['PhNumber']; ?></td>
				<td><?php echo $rows['ReadStatus']; ?></td>
				<td><?php echo $rows['RecvDate']; ?></td>
			</tr>
			<?php
				$no++;
				}
			?>
	</table> <br><br><br>
	<!-- <footer>
		&copy;DIDIGANTENG
	</footer> -->
</body>
</html>