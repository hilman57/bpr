<script>
$(function(){
 var date = new Date();
	$("#SentDate").datepicker ({
		showOn : 'button',  
		buttonImage	: Ext.DOM.LIBRARY +'/gambar/calendar.gif', 
		buttonImageOnly: true,
		dateFormat	:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange:date.getFullYear()+':3000'
	});
});

</script>
<form name="frmEditSetSMS">
<div style="margin:5px;">
 <fieldset class="corner">
 <legend class="icon-application"> &nbsp;&nbsp;&nbsp;Add</legend>
	<?php echo form()->hidden('SRV_Data_Id',null,$isi['SRV_Data_Id']); ?>
		<table border=0 cellspacing="6">
			<tr>
				<td class="text_caption"> Template </td> 
				<td><?php echo form()->combo('template','select long',$Template,$isi['SRV_Data_TemplateId']); ?></td>
				<td class="text_caption"> Data Status </td> 
				<td><?php echo form()->combo('data_status','select long',$DataStatus,$isi['SRV_Data_Status']); ?></td>
				<td class="text_caption"> Date Sent </td> 
				<td><?php echo form()->input('SentDate','input_text',$isi['SRV_Data_SentDate']); ?></td>
			</tr>		
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="button" class="save button" value="Save" onclick="Ext.DOM.UpdateSet();">
					<input type="button" class="close button" value="Close" onclick="Ext.Cmp('top_header').setText('');">
				</td>
			</tr>
		</table>

</fieldset>
	</div>	
</form>	