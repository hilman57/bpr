<?php echo base_window_header("Form Complain"); ?>
<style> 
.ui-chrome{ border-radius:3px;}
.ui-chrome:hover{ border:1px solid #FF4321;}
.ui-context-chrome { border-radius:4px;height:22px;}
</style>
<script>
// efine url 
 
 var URL = function () 
{
	return window.opener.Ext.DOM.INDEX;
}

Ext.DOM.EditComplain = function()
{
	var FrmComplain = Ext.Serialize('frmComplain');
	var VarComplain = new Array();
	Ext.Ajax
	({
		url 	: URL()+"/MgtComplainDebitur/update_complain_note/",
		method 	: "POST",
		param 	: Ext.Join([
					FrmComplain.getElement()
				]).object(),
		ERROR :function( e ) {
			Ext.Util(e).proc(function( response ){
				if( response.success ){
					Ext.Msg("Update Complain").Success();
					window.opener.Ext.DOM.searchCustomer();
				} else {
					Ext.Msg("Update Complain").Failed();
				}
			});
		}
		
	}).post();
};
</script>
</head>
<body style="margin:10px 5px 5px 5px;">
	<fieldset class="corner" style="border-radius:3px;margin-top:10px;">
		<legend class="edit-users-x"><span style="margin-left:8px;">Complain</span></legend>
		<form name="frmComplain">
		<?php echo form()->hidden('ComplainId',null, $IdComplain);?>
		<?php echo "\r\n";?>
		<div class="ui-widget-form-table-compact">
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Product</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left text_caption"><?php echo ( isset($COMPLAIN['CampaignDesc']) ? $COMPLAIN['CampaignDesc'] : "-") ?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Customer Name</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left text_caption"><?php echo ( isset($COMPLAIN['CustomerName']) ? $COMPLAIN['CustomerName'] : "-") ?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Customer ID</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left text_caption"><?php echo ( isset($COMPLAIN['AccountNumber']) ? $COMPLAIN['AccountNumber'] : "-") ?></div>
			</div>
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Phone Blocked</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left text_caption"><?php echo ( isset($COMPLAIN['PhoneBlock']) ? $COMPLAIN['PhoneBlock'] : "-") ?></div>
			</div>
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Deskol</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left text_caption"><?php echo ( isset($COMPLAIN['deskol']) ? $COMPLAIN['deskol'] : "-") ?></div>
			</div>
			
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Source Complain</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->combo('source_complain', 'select long',$SOURCE_COMPLAIN, ( isset($COMPLAIN['id_source_complain']) ? $COMPLAIN['id_source_complain'] : null) );?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Type of Cases</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->combo('jenis_kasus', 'select long',$KASUS_KOMPLEN,( isset($COMPLAIN['id_kasus_komplen']) ? $COMPLAIN['id_kasus_komplen'] : null));?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Action taken</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->textarea('text_action_taken','textarea', ( isset($COMPLAIN['action_taken']) ? $COMPLAIN['action_taken'] : null), null, array("style"=> "width:300px;height:70px;") );?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Result</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->textarea('text_result','textarea', ( isset($COMPLAIN['result']) ? $COMPLAIN['result'] : null), null, array("style"=> "width:300px;height:70px;") );?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Response Complain</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->combo('respon', 'select long',$RESPON,( isset($COMPLAIN['id_respon_review']) ? $COMPLAIN['id_respon_review'] : null));?></div>
			</div>
			
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption"></div>
				<div class="ui-widget-form-cell center"></div>
				<div class="ui-widget-form-cell left">
					<?php echo form()->button('btnshow', 'button save', "Update", array("click" => "Ext.DOM.EditComplain();") );?>
					<?php echo form()->button('btnshow', 'button close', "&nbsp;Exit&nbsp;&nbsp;&nbsp;", array("click" => "ExitComplain();"));?>
				</div>
			</div>
			
		</div>
		</form>
		
	</fieldset>
</body>
</html>