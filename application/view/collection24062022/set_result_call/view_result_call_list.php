<?php ?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%" align="center">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('chk_result').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">No</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonCode" onclick="Ext.EQuery.orderBy(this.id);">Result ID </b></span></th>        
        <th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonDesc" onclick="Ext.EQuery.orderBy(this.id);">Result Name </b></span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonCategoryId" onclick="Ext.EQuery.orderBy(this.id);">Result Category</b></span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonLevel" onclick="Ext.EQuery.orderBy(this.id);">Level</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonContactedFlag" onclick="Ext.EQuery.orderBy(this.id);">Is Contacted</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonEvent" onclick="Ext.EQuery.orderBy(this.id);">Apply as Trigger Form</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonLater" onclick="Ext.EQuery.orderBy(this.id);">Apply as Call Later</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonNoNeed" onclick="Ext.EQuery.orderBy(this.id);">Not Interested</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonStatusFlag" onclick="Ext.EQuery.orderBy(this.id);">Result Status</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" id ="a.CallReasonOrder" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Order</a></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle lasted"><span class="header_order" id ="a.CallWorkProjectId" onclick="Ext.EQuery.orderBy(this.id);">&nbsp;Work Project</a></th>
	</tr>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
			<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
				<td class="content-first"><input type="checkbox" value="<?php echo $rows['CallReasonId']; ?>" name="chk_result" id="chk_result"></td>
				<td class="content-middle"><?php echo $no ?></td>
				<td class="content-middle"><?php echo $rows['CallReasonCode']; ?></td>
				<td class="content-middle"><?php echo $rows['CallReasonDesc']; ?></td>
				<td class="content-middle"><?php echo $rows['CallReasonCategoryCode']; ?></td>
				<td class="content-middle"><?php echo $rows['CallReasonLevel']; ?></td>
				<td class="content-middle"><?php echo ($rows['CallReasonContactedFlag']?'Yes':'No'); ?></td>
				<td class="content-middle"><?php echo $rows['triger'];?></td>
				<td class="content-middle"><?php echo $rows['calllater'];?></td>
				<td class="content-middle"><?php echo $rows['notinterest'];?></td>
				<td class="content-middle"><?php echo $rows['statusResult'];?></td>
				<td class="content-middle center"><?php echo $rows['CallReasonOrder'];?></td>
				<td class="content-lasted left"><?php echo $rows['ProjectName'];?></td>
				
				
				
			</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>



