<?php

$dateFrom = $_REQUEST['dateFrom'];
$dateTo 	= $_REQUEST['dateTo'];
$rptGroup = $_REQUEST['rptGroup'];
$rptAgents= $_REQUEST['rptAgents'];
$paid 	= $_REQUEST['Phour'];

//$paid = $Phour;
//echo $rptGroup;
//print_r($rptAgents);


/* reconstruct date */
$sdates = explode("-", $start_date);
//echo "<br>".$sdates[0];
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];

/* reconstruct date 
$sdates = explode("/", $dateFrom);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("/", $dateTo);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];*/

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){

	echo "End date before start date";
	exit;
}

function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}

	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];

	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);

	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}

	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;

	return $yyyy."-".$mm."-".$dd;
}

function printNote(){
	echo "note:<br>".
			 "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">".
	     "</table>";
}

// ------------------------------------

if( !function_exists('month' ) )
{
	function month( $date = null )
{
		if( is_null($date) ){
			return null;
		}	
		
		$explode_mk_time = date('Y-m', strtotime($date) );
		return $explode_mk_time;
	}
}


// ----------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------

 function showReport($start_date, $end_date, $GroupCallCenter, $AgentId)
{
	
  global $paid;
  
  $start_periode = month( $start_date );
  $end_periode = month( $end_date );
  
 // -------------- show table ------------------------------------------------------  
		echo '<table class="data" border=1 style="border-collapse: collapse">';
		echo '<tr>'
			 .'<td rowspan= "3" class="head" style="vertical-align:middle;">&nbsp;NO</td>'
		     .'<td rowspan= "3" class="head" style="vertical-align:middle;">&nbsp;Coll Name</td>'
		     .'<td rowspan= "3" class="head" style="vertical-align:middle;">&nbsp;Paid Hours</td>'
		     .'<td class="head" align="center" colspan="6">RAW DATA</td>'
		     .'<td class="head" align="center" colspan="3">PRODUCTION</td>'
		     .'<td class="head" align="center" colspan="3">CONVERSION</td>'
			 .'<td class="head" align="center" colspan="2">OUTCOMES</td>'
		     .'</tr>';
		echo '<tr>'
		     //.'<td >&nbsp;</td>'
		     //.'<td class="head">Coll Name</td>'
			 //.'<td class="head">Paid Hours</td>'
			 .'<td class="head">Dialer Hours</td>'
			 .'<td class="head"># RPCs</td>'
		     .'<td class="head"># KP+BP</td>'
		     .'<td class="head"># PK</td>'
		     .'<td class="head"># BP</td>'
		     .'<td class="head">IDR Collected</td>'
		     .'<td class="head">Util</td>'
		     .'<td class="head">RPC/OP Hr </td>'
		     .'<td class="head">RPC / Pd</td>'
		     .'<td class="head">% PTP</td>'
		     .'<td class="head">% PK</td>'
		     .'<td class="head">Avg IDR</td>'
		     .'<td class="head">CEV</td>'
		     .'<td class="head">EVPH</td>'
		     .'</tr>';
		echo '<tr>'
			// .'<td class="head">NO</td>'
		    // .'<td class="head">&nbsp;</td>'
			//.'<td class="head">&nbsp;</td>'
			 .'<td class="head">&nbsp;</td>'
			 .'<td class="head">&nbsp;</td>'
		     .'<td class="head">&nbsp;</td>'
		     .'<td class="head">&nbsp;</td>'
		     .'<td class="head">&nbsp;</td>'
		     .'<td class="head">&nbsp;</td>'
		     .'<td class="head">(Dscp)</td>'
		     .'<td class="head">(Speed)</td>'
		     .'<td class="head">(PROD)</td>'
		     .'<td class="head">(Say Pay)</td>'
		     .'<td class="head">(Do Pay)</td>'
		     .'<td class="head">(Nego Pay)</td>'
		     .'<td class="head">(SKILL)</td>'
		     .'<td class="head">(We Got)</td>'
		     .'</tr>';

		/*if($group){
			$fromTbl = ", cc_agent b ";
			//periksa apakah ada agent yang dipilih
			if(count($agents) and $agents[0]){
				$agentlist = implode(",", $agents);
				$cond = 'AND a.userid = b.userid AND b.id IN ('.$agentlist.')';
			}else{
				$cond = 'AND a.userid = b.userid AND b.agent_group ='.$group;
			}
		}*/
		
		if($GroupCallCenter){
			$fromTbl = ", cc_agent b ";
			//periksa apakah ada agent yang dipilih
			if(count($AgentId) and $AgentId[0]){
			
				$agentlist = implode(",", $AgentId);
				$cond = "AND a.userid = b.userid AND b.id IN (".$agentlist.")";
			}else{
				$cond = 'AND a.userid = b.userid AND b.agent_group ='.$GroupCallCenter;
			}
		}
		
	
			   
		//$agent_data = array();
		// Get data size and volume
		
		/*
		$sql = "select a.userid, a.paidhours, a.dialer_hours, a.rpc, a.ptp, a.payment, a.bp,
				a.collected, c.description  
				from  t_gn_sdr a $fromTbl 
				LEFT OUTER JOIN cc_agent b ON b.`id` = a.`agent_id`				
				LEFT OUTER JOIN cc_agent_group c on b.agent_group = c.id 
				where a.userid=b.userid and b.spv_id IN(1,0)
				/*AND b.agent_group = c.id  */
		
		/*	
				AND yearmonth>='{$start_periode}'
				AND yearmonth<='{$end_periode}' $cond
				order by description";
	    */
		$sql = "SELECT a.userid, a.paidhours, a.dialer_hours, a.rpc, a.ptp, a.payment, a.bp, a.collected, c.description 
				FROM t_gn_sdr a  
				LEFT OUTER JOIN cc_agent b ON b.`id` = a.`agent_id`
				LEFT OUTER JOIN cc_agent_group c ON b.agent_group = c.id 
				LEFT OUTER JOIN t_tx_agent d ON b.userid=d.id
				where b.spv_id IN(1,0) 
				AND yearmonth>='{$start_periode}'
				AND yearmonth<='{$end_periode}' ";
				
				if( count($agentlist) > 0  ){
					$sql .= "AND d.UserId IN( $agentlist ) ";
				}
				
				$sql .= " order by description ";
				
		//echo $sql;		
		
		/*$sql = "select a.`userid`, a.`paidhours`, a.`dialer_hours`, a.`rpc`, a.`ptp`, a.`payment`, a.`bp`,
				a.`collected`, c.description  
					from `coll_sdr` as a, cc_agent as b, cc_agent_group as c 
				where a.userid=b.userid and b.spv_id=1 and b.agent_group = c.id and `yearmonth`='".$periode."' $cond
				order by description";*/
		
		/* v2
		$sql = "select a.`userid`, a.`dialer_hours`, a.`rpc`, a.`ptp`, a.`payment`, a.`bp`,
				a.`collected`
					from `coll_sdr` as a, cc_agent as b 
				where a.userid=b.userid and b.spv_id=1 and `yearmonth`='".$periode."' $cond
				order by userid";
				*/
		//$paid = $Phour;		
				/*
				$sql = "select a.`userid`, a.`paidhours`, a.`dialer_hours`, a.`rpc`, a.`ptp`, a.`payment`, a.`bp`,
				a.`collected` 
					from `coll_sdr` as a,`cc_agent` as b
				where `yearmonth`='".$periode."' and a.userid=b.userid and b.agent_group='".$group."'
				order by userid";*/
		$res = @mysql_query($sql);
		//echo $sql."<br>";
		
		
		$tpaid=0;
		$tdial=0;
		$trpc=0;
		$tptp=0;
		$tpayment=0;
		$tbp=0;
		$tcollected=0;
		$tutil=0;
		$tspeed=0;
		$tprod=0;
		$tprosenptp=0;
		$tprosenpk=0;
		$tnego=0;
		$tcev=0;
		$tgot=0;
        $k=0;
		$cnt = 0;
		$dcr = "UNKNOWN";
	
		
		
		while ($row = @mysql_fetch_array($res)) {
	
			if ($paid != '') {
				$paidx = $paid;
			} else {
				$paidx = $row['paidhours'];
			}
					// $paid=$row['paidhours'];    E H
					 $dial=$row['dialer_hours']/3600;
					 $rpc=$row['rpc'];
					 $ptp=$row['ptp'];
					 $payment=$row['payment'];
					 $bp=$row['bp'];
					 $collected=$row['collected'];
					// $util=$dial/$row['paidhours'];  EH
					$util=$dial/$paidx;
					 $speed=$rpc/$dial;
					// $prod=($row['dialer_hours']/$row['paidhours'])*($row['rpc']/$row['dialer_hours']);  EH
					$prod=($row['dialer_hours']/$paidx)*($row['rpc']/$row['dialer_hours']);
					 $prosenptp=$row['ptp']/$row['rpc'];
					 $prosenpk=$row['payment']/$row['ptp'];
					 $nego=$row['collected']/$row['payment'];
					 //$cev=($row['ptp']/$row['rpc'])*($row['collected']/$row['ptp'])*($row['collected']/$row['payment']);
					 $cev=$prosenptp*$prosenpk*$nego;
					 $got=$prod*$cev;
					$tpaid+=$paidx;
					$tdial+=$dial;
					$trpc+=$rpc;
					$tptp+=$ptp;
					$tpayment+=$payment;
					$tbp+=$bp;
					$tcollected+=$collected;
					$tutil+=$util;
					$tspeed+=$speed;
					$tprod+=$prod;
					$tprosenptp+=$prosenptp;
					$tprosenpk+=$prosenpk;
					$tnego+=$nego;
					$tcev+=$cev;
					$tgot+=$got;
					
						if($dcr != $row['description']){
				
				if($cnt>0){
					echo '<tr>';
					echo '<td colspan="2" align="right"><strong>Sub Total: </strong>&nbsp;</td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtpaid,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtdial,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtrpc,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtptp,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtpayment,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtbp,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtcollected,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtutil*100,2,",",".").' %</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtspeed,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtprod,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtprosenptp*100,2,",",".").' %</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtprosenpk*100,2,",",".").' %</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtnego,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtcev,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtgot,2,",",".").'</strong></td>';
					echo '</tr>';
				}
				$dcr = $row['description'];
				echo '<tr>';
				echo '<td colspan="17" class="head2"><strong>'.$dcr.'</strong>&nbsp;</td>';
				echo '</tr>';
				//subtotal
		$subtpaid=0;
		$subtdial=0;
		$subtrpc=0;
		$subtptp=0;
		$subtpayment=0;
		$subtbp=0;
		$subtcollected=0;
		$subtutil=0;
		$subtspeed=0;
		$subtprod=0;
		$subtprosenptp=0;
		$subtprosenpk=0;
		$subtnego=0;
		$subtcev=0;
        $subtgot=0;
			}
			
					
					
					$cnt++;
					echo "<tr>";
					echo 
							'<td align="right">'.$cnt.'</td>'
							 .'<td align="right">'.$row['userid'].'</td>'
							 .'<td align="right">'.$paidx.'</td>'
							 .'<td align="right">'.number_format($dial,2,",",".").'</td>'
							 .'<td align="right">'.$rpc.'</td>'
							 .'<td align="right">'.$ptp.'</td>'
							 .'<td align="right">'.$payment.'</td>'
							 .'<td align="right">'.$bp.'</td>'
							 .'<td align="right">'.number_format($collected,2,",",".").'</td>'
							 .'<td align="right">'.number_format($util*100,2,",",".").' %</td>'
							 .'<td align="right">'.number_format($speed,2,",",".").'</td>'
							 .'<td align="right">'.number_format($prod,2,",",".").'</td>'
							 .'<td align="right">'.number_format($prosenptp*100,2,",",".").' %</td>'
							 .'<td align="right">'.number_format($prosenpk*100,2,",",".").' %</td>'
							 .'<td align="right">'.number_format($nego,2,",",".").'</td>'
							 .'<td align="right">'.number_format($cev,2,",",".").'</td>'
							 .'<td align="right">'.number_format($got,2,",",".").'</td>';
					echo '</tr>';
          $k++;
		  //subtotal
					$subtpaid+=$paidx;
					$subtdial+=$dial;
					$subtrpc+=$rpc;
					$subtptp+=$ptp;
					$subtpayment+=$payment;
					$subtbp+=$bp;
					$subtcollected+=$collected;
					$subtutil+=$util;
					$subtspeed+=$speed;
					$subtprod+=$prod;
					$subtprosenptp+=$prosenptp;
					$subtprosenpk+=$prosenpk;
					$subtnego+=$nego;
					$subtcev+=$cev;
					$subtgot+=$got; 
		 }//end while

		
		if($cnt>0){
					echo '<tr>';
					echo '<td colspan="2" align="right"><strong>Sub Total: </strong>&nbsp;</td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtpaid,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtdial,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtrpc,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtptp,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtpayment,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtbp,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtcollected,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtutil*100,2,",",".").' %</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtspeed,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtprod,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtprosenptp*100,2,",",".").' %</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtprosenpk*100,2,",",".").' %</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtnego,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtcev,2,",",".").'</strong></td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtgot,2,",",".").'</strong></td>';
					echo '</tr>';
				}


			echo "<tr>";
			echo 
					 '<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head">Totals</td>'
					 .'<td class="head" align="right">'.number_format($tpaid,2,",",".").'</td>'
					 .'<td class="head" align="right">'.number_format($tdial,2,",",".").'</td>'
					 .'<td class="head" align="right">'.number_format($trpc,2,",",".").'</td>'
					 .'<td class="head" align="right">'.number_format($tptp,2,",",".").'</td>'
					 .'<td class="head" align="right">'.number_format($tpayment,2,",",".").'</td>'
					 .'<td class="head" align="right">'.number_format($tbp,2,",",".").'</td>'
					 .'<td class="head" align="right">'.number_format($tcollected,2,",",".").'</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					 .'<td class="head" align="right">&nbsp;</td>'
					  .'<td class="head" align="right">&nbsp;</td>';
			echo '</tr>';

			
		

		echo "<tr>";
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">Tm Avg</td>';
		echo '<td class="head"  align="right" >'.number_format($tpaid/$k,2,",",".").'</td>';
		echo '<td class="head"  align="right">'.number_format($tdial/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($trpc/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tptp/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tpayment/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tbp/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tcollected/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format(100*($tutil/$k),2,",",".").' %</td>';
		echo '<td class="head" align="right">'.number_format($tspeed/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tprod/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format(100*($tprosenptp/$k),2,",",".").' %</td>';
		echo '<td class="head" align="right">'.number_format(100*($tprosenpk/$k),2,",",".").' %</td>';
		echo '<td class="head" align="right">'.number_format($tnego/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tcev/$k,2,",",".").'</td>';
		echo '<td class="head" align="right">'.number_format($tgot/$k,2,",",".").'</td>';
		echo '</tr>';
		echo '</table><br>';


	//save to report data $start_date, $end_date, $group, $agents to session
	
	$_SESSION['xl_start_date'] 	= $start_date;
	$_SESSION['xl_end_date'] 		= $end_date;
	$_SESSION['xl_group'] 			= $GroupCallCenter;
	$_SESSION['xl_agents'] 			= $AgentId;	
	$_SESSION['paid'] 			= $paid;	


}
?>
<html>
	<head>
		<title>
			enigmaColection Report - Skill Development Report
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}
			-->
			</style>
	</head>
<body>
	<h1><u><?
	switch($rptIntervalMode){
		case "hourly":
			echo "Hourly ";
			break;
		case "daily":
			echo "Daily ";
			break;
	}
		?>Skill Development Report</u></h1>
      
	<?php
	//echo $periode;
	echo "<h2>Periode ${start_date} to ${end_date}</h2>";
   $GroupCallCenter = 1;
	echo "<hr size=1>";
	showReport($start_date, $end_date, $GroupCallCenter, $AgentId, $paidx);
    $_SESSION['xl_rep_title'] 	= "Periode ${sdates[1]}-${sdates[2]}";
	printNote();
	?>
     <a href="?action=excel&rptType=<?=$rptType;?>&transid=<?=$transid;?>">Save as excel</a>
</body>
</html>
