<?php echo javascript(); ?>
<script type="text/javascript">

	
/**
 ** javscript prototype system
 ** version v.0.1
 **/

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
	
var datas = 
	{
		keywords : '',  //'<#?php echo $_REQUEST['keywords'];?>',
		order_by : '', //'<#?php echo $_REQUEST['order_by'];?>',
		type 	 : '' //'<#?php echo $_REQUEST['type'];?>'
	}
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
$('#toolbars').extToolbars({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Enable'],['Disable'] ,['Add'],['Edit'],['Delete'],[],['Search']],
		extMenu  :[['EnableUserLayout'],['DisableUserLayout'],['addUserLayout'],['EditLayout'],['deleteWork'],[],['searchWork']],
		extIcon  :[['accept.png'],['cancel.png'], ['add.png'],['calendar_edit.png'],['delete.png'],[],['zoom.png']],
		extText  :true,
		extInput :true,
		extOption: [{
					 render:5,
					 type:'text',
					 id:'v_result', 	
					 name:'v_result',
					 value: datas.keywords,
					 width:200
					}]
	});
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 Ext.DOM._content_page = {
	custnav  : Ext.DOM.INDEX+'/SysUserLayout/index',
	custlist : Ext.DOM.INDEX+'/SysUserLayout/Content'			
 }	
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/

 Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
 Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
	var searchWork = function(){
		var keywords = doJava.dom('v_result').value;
		var datas = {
			keywords : keywords
		}
		
		Ext.EQuery.construct( Ext.DOM._content_page,datas)
		Ext.EQuery.postContent();
	}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
	Ext.EQuery.construct(Ext.DOM._content_page, datas )
	Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 var UpdateData = function()
	{
		var BranchId =  doJava.dom('BranchId').value;
		var BranchCode =  doJava.dom('BranchCode').value;
		var BranchName =  doJava.dom('BranchName').value;
		var BranchContact = doJava.dom('BranchContact').value; 
		var BranchAddress =  doJava.dom('BranchAddress').value;
		var BranchManager = doJava.dom('BranchManager').value;
		var BranchEmail = doJava.dom('BranchEmail').value;
		if( BranchCode =='' ) { alert('Please input BranchCode..!'); return false;}
		else if( BranchName =='' ) { alert('Please input BranchName..!'); return false;}
		else
		{
			doJava.File = '../class/class.work.area.php';
			doJava.Params = { 
				action : 'update_work_area',
				BranchId : BranchId,
				BranchCode : BranchCode,
				BranchName : BranchName,
				BranchContact : BranchContact, 
				BranchAddress : BranchAddress,
				BranchManager : BranchManager,
				BranchEmail : BranchEmail
			}
			
			var error = doJava.eJson();
			if( error.result ){
				alert('Success, Update Branch Data..!');
				extendsJQuery.postContent();	
			}
			else{
				alert('Failed, Update Branch Data..!');
			}
		}
	}		

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 
Ext.DOM.SaveLayout=function(){
	
 if(Ext.Cmp('UserThemes').empty()){
	Ext.Msg('Layout Themes is empty').Info();
	Ext.Cmp('UserThemes').setFocus();
 }
 else if( Ext.Cmp('UserLayout').empty()){
	Ext.Msg('Layout Name is empty').Info();
	Ext.Cmp('UserLayout').setFocus();
 }
 else if( Ext.Cmp('UserGroup').empty()){
	Ext.Msg('User Group is empty').Info();
	Ext.Cmp('UserGroup').setFocus();
 }
 else
	{	
		Ext.Ajax
		({
			url : Ext.DOM.INDEX +'/SysUserLayout/SaveLayout/',
			method : 'POST',
			param :{
				UserThemes : Ext.Cmp('UserThemes').getValue(),
				UserLayout : Ext.Cmp('UserLayout').getValue(),
				UserGroup  : Ext.Cmp('UserGroup').getValue()
			},
			ERROR : function(fn){
				var ERR = JSON.parse(fn.target.responseText);
				if(ERR.success){
					Ext.Msg("Save Layout").Success();
					Ext.EQuery.construct(Ext.DOM._content_page, datas )
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Save Layout").Failed();
				}
			}
		}).post();
	}
}
 
/* disbaled work ***/

Ext.DOM.DisableUserLayout = function()
{
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/SysUserLayout/SetLayout/',
		method : 'POST',
		param :{
			SetLayout : 0,
			LayoutId : Ext.Cmp('LayoutId').getValue()		
		},
		ERROR : function(fn){
			var ERR = JSON.parse(fn.target.responseText);
			if(ERR.success){
				Ext.Msg("Disable Layout").Success();
				Ext.EQuery.construct(Ext.DOM._content_page, datas )
				Ext.EQuery.postContent();
			}
			else{
				Ext.Msg("Disable Layout").Failed();
			}
		}
	}).post();
} 	
/* disbaled work ***/

Ext.DOM.EnableUserLayout = function()
{
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/SysUserLayout/SetLayout/',
		method : 'POST',
		param :{
			SetLayout : 1,
			LayoutId : Ext.Cmp('LayoutId').getValue()	
		},
		ERROR : function(fn){
			var ERR = JSON.parse(fn.target.responseText);
			if(ERR.success){
				Ext.Msg("Enable Layout").Success();
				Ext.EQuery.construct(Ext.DOM._content_page, datas )
				Ext.EQuery.postContent();
			}
			else{
				Ext.Msg("Enable Layout").Failed();
			}
		}
	}).post();
}
/* editWork **/

Ext.DOM.EditLayout = function(){
	Ext.Ajax
	({
		url    : Ext.DOM.INDEX+'/SysUserLayout/EditUserLayout/',
		method :'GET',
		param  :{
			LayoutId : Ext.Cmp('LayoutId').getValue()
		}
	}).load('span_top_nav');
}

Ext.DOM.UpdateLayout = function(){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SysUserLayout/UpdateLayout/',
		method 	: 'POST',
		param 	: {
			LayoutId   : Ext.Cmp('LayoutId').getValue(),
			UserThemes : Ext.Cmp('UserThemes').getValue(),
			UserLayout : Ext.Cmp('UserLayout').getValue(),
			UserGroup  : Ext.Cmp('UserGroup').getValue()
		},
		ERROR : function(fn){
			var ERR = JSON.parse(fn.target.responseText);
			if(ERR.success){
				Ext.Msg("Update Layout").Success();
				Ext.EQuery.construct(Ext.DOM._content_page, datas )
				Ext.EQuery.postContent();
			}
			else{
				Ext.Msg("Update Layout").Failed();
			}
		}
	}).post();
}



	
Ext.DOM.addUserLayout = function()
{
	Ext.Ajax
	({
		url    : Ext.DOM.INDEX+'/SysUserLayout/AddUserLayout/',
		method :'GET',
		param  :{
			act : 'add-layout-user'
		}
	}).load('span_top_nav');
	
}
	
/* cancelWork *************/	
Ext.DOM.cancelLayout = function()
{
	//Ext.Cmp('span_top_nav').setText('');
}	
</script>

<!-- ///////////////////////////////////////////////////////////////////////////-->
<!-- ///////////////////////////////////////////////////////////////////////////-->
<!-- start : content -->

	<fieldset class="corner">
		<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span> </legend>	
			<div id="toolbars"></div>
			<div id="span_top_nav" style="margin:5px;"></div>
			<div id="customer_panel" class="box-shadow">
				<div class="content_table" style="background-color:#FFFFFF;"></div>
				<div id="pager"></div>
			</div>
	</fieldset>	
		
<!-- stop : content -->
<!-- ///////////////////////////////////////////////////////////////////////////-->
<!-- ///////////////////////////////////////////////////////////////////////////-->