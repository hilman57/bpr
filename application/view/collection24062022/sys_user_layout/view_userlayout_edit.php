<fieldset class="corner">
	<legend class="icon-application">&nbsp;&nbsp; Update User Layout</legend>
	<div class="box-shadow" style="margin-bottom:10px;">
		<input type="hidden" id="LayoutId" name="LayoutId" value="<?php echo $LayoutData['Id'];?>"/>
		<table cellpadding="8px" cellspacing="8px">
			<tr>
				<td class="text-caption">Layout Themes </td>
				<td class=""><?php echo form() -> combo('UserThemes','select long',$UserThemes,$LayoutData['Themes']);?></td>
			</tr>
			<tr>
				<td class="text-caption">Layout Name</td>
				<td class=""><?php echo form() -> combo('UserLayout','select long', $UserLayout,$LayoutData['LayoutId']);?></td>
			</tr>
			<tr>
				<td class="text-caption">User Group</td>
				<td class=""><?php echo form() -> combo('UserGroup','select long',$UserGroup,$LayoutData['GroupId']);?></td>
			</tr>
			<tr>
				<td class="text-caption"></td>
				<td class="">
					<input type="button" class="update button" onclick="Ext.DOM.UpdateLayout();" value="Update">
					<input type="button" class="close button" onclick="Ext.Cmp('span_top_nav').setText('');" value="Close">
				</td>
			</tr>
	</div>
</fieldset>