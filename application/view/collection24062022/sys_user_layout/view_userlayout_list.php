<?php ?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%">&nbsp;<a href="javascript:void(0);" style="text-decoration:none;" onclick="Ext.Cmp('LayoutId').setChecked();">#</a></th>	
		<th class="font-standars ui-corner-top ui-state-default first center" width="5%"  align="center">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('a.BranchId');" title="Order ASC/DESC">No</span></th>	
		<th class="font-standars ui-corner-top ui-state-default first center" width="8%"  align="left">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('b.name');" title="Order ASC/DESC">User Group</span></th>		
		<th class="font-standars ui-corner-top ui-state-default first center" width="12%" align="left">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('c.Name');" title="Order ASC/DESC">Layout Name.</span></th>    
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%" align="left">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('a.BranchManager');" title="Order ASC/DESC">Layout Themes.</span></th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%" align="left">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('a.BranchContact');" title="Order ASC/DESC">User Create</span></th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%" align="left">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('a.BranchName');" title="Order ASC/DESC">Create date.</span></th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%" align="left">&nbsp;<span class="header_order" onclick="javscript:Ext.EQuery.orderBy('a.BranchEmail');" title="Order ASC/DESC">Status.</span></th>
	</tr>
</thead>	
<tbody>
<?php

 $no  = $num;
 foreach( $page -> result_assoc() as $rows )
 { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
			<tr class="onselect" bgcolor="<?php echo $color; ?>">
				<td class="content-first" ><?php echo form() -> checkbox('LayoutId',NULL,$rows['Id'], NULL, NULL,0);?></td>
				<td class="content-middle" align="center"><?php echo $no; ?></td>
				<td class="content-middle" align="left"><?php echo ($rows['GroupName']?$rows['GroupName']:'-'); ?></td>
				<td class="content-middle" ><?php echo ($rows['LayoutName']?$rows['LayoutName']:'-');?></td>
				<td class="content-middle" ><?php echo ($rows['Themes']?$rows['Themes']:'-');?></td>
				<td class="content-middle" ><?php echo ($rows['CreatedDateTs']?$rows['CreatedDateTs']:'-');?></td>
				<td class="content-middle" ><div class="wraptext"><?php echo ($rows['UserName']?$rows['UserName']:'-');?></div></td>
				<td class="content-lasted" align="justify" nowrap><?php echo ($rows['Flags']?'Active':'Not Active');?></td>
			</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>