<?php echo javascript(); ?>
<script type="text/javascript">

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
Ext.DOM.IsCall = function( CustomerId ){
  return( 
	Ext.Ajax({
		url    : Ext.DOM.INDEX +'/ModContactDetail/IsCall/',
		method : 'GET',
		param  : {
			CustomerId : CustomerId
		}
	}).json()
  );
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
Ext.DOM.DetailData = function( CustomerId )
{
	console.log(CustomerId);
	var ControllerId = Ext.DOM.INDEX +'/MonAgentActivity/index/';
	if( Ext.Msg("Do you want show detail ?").Confirm() ) 
	{
		Ext.ActiveMenu().NotActive();
		Ext.EQuery.Ajax 
		({
			url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
			method  : 'GET',
			param 	: {
				CustomerId : CustomerId,
				ControllerId : ControllerId
			}
		});
		
	 }
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
Ext.DOM.HangupAgent = function( AgentId )
{
	console.log(AgentId);
	if( Ext.Msg("Do you want Hangup Call?").Confirm() ) 
	{
		Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX +'/MonAgentActivity/AgentHangup/',
			method  : 'POST',
			param 	: {
				AgentId  : AgentId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(drop){
					drop.success;
				});
			}
				
		}).post();
	 }
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
Ext.DOM.StoreActivity = function()
{
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/MonAgentActivity/Store/',
		method  : 'POST',
		param 	: {
			time : Ext.Date().getDuration(),
			AgentStatus : Ext.Cmp('UserAgentStatus').getValue()
		},
		ERROR 	: function(e){
			Ext.Util(e).proc(function(data)
			{
				for( var i in data ) {
					if( typeof(data[i].Status)=='string' ) {
						Ext.Cmp(data[i].UserId+'-name').setAttribute('style',data[i].Styles);
					}
					if( typeof(data[i].Status)=='string' ) {
						Ext.Cmp(data[i].UserId+'-agentstatus').setText(data[i].Status);
						Ext.Cmp(data[i].UserId+'-agentstatus').setAttribute('style',data[i].Styles);
					}
					if( typeof(data[i].Duration)=='string'){
						Ext.Cmp(data[i].UserId+'-time').setText(data[i].Duration);
						Ext.Cmp(data[i].UserId+'-time').setAttribute('style',data[i].Styles);
					}
					if( typeof(data[i].Extension)=='string'){
						Ext.Cmp(data[i].UserId+'-ext').setText(data[i].Extension);
						Ext.Cmp(data[i].UserId+'-ext').setAttribute('style',data[i].Styles);
					}
					if( typeof(data[i].ExtStatus)=='string') {
						Ext.Cmp(data[i].UserId+'-extstatus').setText(data[i].ExtStatus);	
						Ext.Cmp(data[i].UserId+'-extstatus').setAttribute('style',data[i].Styles);
					}
					if( typeof(data[i].Data)=='string') {
						Ext.Cmp(data[i].UserId+'-data').setText(data[i].Data);
						Ext.Cmp(data[i].UserId+'-data').setAttribute('style',data[i].Styles);
					}
					if( typeof(data[i].Action)=='string') {
						Ext.Cmp(data[i].UserId+'-spy').setText(data[i].Action);
						Ext.Cmp(data[i].UserId+'-spy').setAttribute('style',data[i].Styles);
					}
				}	
			});
		}
	}).post();
		
	Ext.DOM.setTimeOutId = setTimeout(function(){
		Ext.DOM.StoreActivity();
	},1000);
}


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
Ext.DOM.ActivityAgent = function(Order)
{
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/MonAgentActivity/Content/',
		param : {
			Order : Order,
			time : Ext.Date().getDuration(),
			AgentStatus : Ext.Cmp('UserAgentStatus').getValue()
		}	
	}).load("content-activity");
	
	Ext.DOM.StoreActivity();	
} 


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
Ext.DOM.SpyAgent = function( ExtSite, ExtSrc ) 
{

  if( Ext.Msg("Do you want to listen ?").Confirm() ) {
	Ext.Ajax 
	({
		url 	: Ext.DOM.INDEX +'/MonAgentActivity/SpyAgent/',
		method  : 'POST',
		param 	: {
			FromExtension  : ExtSite,
			ToExtension	 : ExtSrc
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(spy){
				spy.success;
			});
		}
			
	}).post();
  }	
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

Ext.DOM.CoachAgent = function( ExtSite, ExtSrc ) 
{
  if( Ext.Msg("Do you want to listen ?").Confirm() ) {
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MonAgentActivity/CoachAgent/',
		method  : 'POST',
		param 	: {
			FromExtension  : ExtSite,
			ToExtension	 : ExtSrc
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(spy){
				spy.success;
			});
		}
			
	}).post();
  }	
}


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
	Ext.DOM.ActivityAgent('DESC');
 });
 
 
Ext.document('document').ready( function(){ 
Ext.DOM.Reload = function(data){ 
	window.clearInterval(Ext.DOM.setTimeOutId);
	window.Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/MonAgentActivity/',
		param : {
			AgentStatus : data
		}
	}).load('main_content');
	
}
 });
</script> 	
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
	<div>
		<table>
			<tr>
				<td class='text_caption bottom'>Caller Status &nbsp;:&nbsp;</td>
				<td><?php __(form()->combo('UserAgentStatus', 'select long', 
					array("NOL" => "Logout", '1' => 'Ready', 2=>'Not Ready', '3'=>'ACW', 4=>'Busy') , _get_post('AgentStatus'), array("change"=> "Ext.DOM.Reload(this.value);")));?> </td>
			</tr>
		</table>
	</div>
	<div id="content-activity"></iframe>
</fieldset>