<?php

// Ubah format tanggal ke english form
$dNewDate = strtotime($start_date1);
$dNewDate2 = strtotime($start_date2);
$start_date1 = date("Y-m-d", $dNewDate);
$start_date2 = date("Y-m-d", $dNewDate2);
$start_date="";

// Buat interval untuk cek harian
while ($start_date1<=$start_date2) {
	if ($start_date1==$start_date2) {	
		$start_date .=$start_date1;
	} else {
		$start_date .=$start_date1."|";
	}
	$date = strtotime("+1 day", strtotime($start_date1));
	$start_date1 = date("Y-m-d", $date);
}

// Ubah format tanggal ke english form
$dNewDate = strtotime($end_date1);
$dNewDate2 = strtotime($end_date2);
$end_date1 = date("Y-m-d", $dNewDate);
$end_date2 = date("Y-m-d", $dNewDate2);
$end_date="";


// Buat interval untuk cek harian
while ($end_date1<=$end_date2) {
	if ($end_date1==$end_date2) {	
		$end_date .=$end_date1;
	} else {
		$end_date .=$end_date1."|";
	}
	$date = strtotime("+1 day", strtotime($end_date1));
	$end_date1 = date("Y-m-d", $date);
}
$sdates = explode("|", $start_date);
$edates = explode("|", $end_date);

// Dapatkan bulan tarik tanggal
$getMonth1 = explode("-", $sdates[0]);
$month1 = $getMonth1[1];
$getMonth2 = explode("-", $edates[0]);
$month2 = $getMonth2[1];

// Konversi bulan ke nama bulan
$monthName1 = date('F', mktime(0, 0, 0, $month1, 10));
$monthName2 = date('F', mktime(0, 0, 0, $month2, 10));

// Dapatkan tahun tarik tanggal
$year1 = $getMonth1[0];
$year2 = $getMonth2[0];

function showReport($monthName1, $monthName2, $year1, $year2, $lstart_date, $lend_date)
{
	echo "<table class=\"data\" border=1 style=\"border-collapse: collapse\" width=\"80%\" align=\"center\"><tr>"
		     ."<td class=\"head\">Period 1</td>"
		     ."<td class=\"head\">Inventory</td>"
		     ."<td class=\"head\">Balance</td>"
		     ."<td class=\"head\">Attempt</td>"
		     ."<td class=\"head\">Intensity</td>"
		     ."<td class=\"head\">RPC</td>"
			 ."<td class=\"head\">%RPC to attempt</td>"
		     ."<td class=\"head\">PTP</td>"
		     ."<td class=\"head\">%PTP to attempt</td>"
			 ."<td class=\"head\">Jml PTP</td>"
			 ."<td class=\"head\">Payment</td>"
		     ."<td class=\"head\">ACC</td>"	
		     ."<td class=\"head\">SR Acc</td>"		     
		     ."</tr>";

	$dataMonth1 = null;

	$array = implode("','",$lstart_date); // Explode array untuk kondisi where in

	// Query select data
	$sql = "SELECT MAX(a.inventory) as inventory, MAX(a.balance) as balance, SUM(a.attempt) as attempt, SUM(a.rpc) as rpc, SUM(a.ptp) as ptp, SUM(a.jml_ptp)  as jml_ptp, SUM(a.payment) as payment, SUM(a.acc) as acc FROM t_gn_inventory_log a 
			WHERE DATE_FORMAT(a.log_ts, '%Y-%m-%d') IN ('".$array."')";

	$res = @mysql_query($sql);
	
	$row = @mysql_fetch_array($res);
	$dataMonth1['inventory'] = $row['inventory'];
	$dataMonth1['balance'] = $row['balance'];
	$dataMonth1['attempt'] = $row['attempt'];
	$dataMonth1['rpc'] = $row['rpc'];
	$dataMonth1['ptp'] = $row['ptp'];
	$dataMonth1['jml_ptp'] = $row['jml_ptp'];
	$dataMonth1['payment'] = $row['payment'];
	$dataMonth1['acc'] = $row['acc'];
	
	echo "<tr>";
	echo '<td nowrap>'.$monthName1.' '.$year1.'</td>'
			 .'<td nowrap>'.$dataMonth1['inventory'].'</td>'
			 .'<td class="text">'.$dataMonth1['balance'].'</td>'
			 .'<td>'.$dataMonth1['attempt'].'</td>'
			 .'<td>'.number_format($dataMonth1['attempt']/$dataMonth1['inventory']*100,3).'</td>'
			 .'<td>'.$dataMonth1['rpc'].'</td>'
			 .'<td>'.number_format($dataMonth1['rpc']/$dataMonth1['attempt']*100,3).'%</td>'
			 .'<td>'.$dataMonth1['ptp'].'</td>'
			 .'<td>'.number_format($dataMonth1['ptp']/$dataMonth1['attempt']*100,3).'%</td>'
			 .'<td>'.$dataMonth1['jml_ptp'].'</td>'
			 .'<td>'.$dataMonth1['payment'].'</td>'
			 .'<td>'.$dataMonth1['acc'].'</td>'
			 .'<td>'.number_format($dataMonth1['acc']/$dataMonth1['inventory']*100,3).'%</td>';
	echo "</tr>";
	echo "</table><br>";

	echo "<table class=\"data\" border=1 style=\"border-collapse: collapse\" width=\"80%\" align=\"center\"><tr>"
		     ."<td class=\"head\">Period 2</td>"
		     ."<td class=\"head\">Inventory</td>"
		     ."<td class=\"head\">Balance</td>"
		     ."<td class=\"head\">Attempt</td>"
		     ."<td class=\"head\">Intensity</td>"
		     ."<td class=\"head\">RPC</td>"
			 ."<td class=\"head\">%RPC to attempt</td>"
		     ."<td class=\"head\">PTP</td>"
		     ."<td class=\"head\">%PTP to attempt</td>"
			 ."<td class=\"head\">Jml PTP</td>"
			 ."<td class=\"head\">Payment</td>"
		     ."<td class=\"head\">ACC</td>"	
		     ."<td class=\"head\">SR Acc</td>"		     
		     ."</tr>";

	$dataMonth2 = null;

	$array = implode("','",$lend_date); // Explode array untuk kondisi where in

	// Query select data
	$sql = "SELECT MAX(a.inventory) as inventory, MAX(a.balance) as balance, SUM(a.attempt)  as attempt, SUM(a.rpc) as rpc, SUM(a.ptp) as ptp, SUM(a.jml_ptp)  as jml_ptp, SUM(a.payment) as payment, SUM(a.acc) as acc FROM t_gn_inventory_log a 
			WHERE DATE_FORMAT(a.log_ts, '%Y-%m-%d') IN ('".$array."')";
	
	$res = @mysql_query($sql);
	
	$row = @mysql_fetch_array($res);
	$dataMonth2['inventory'] = $row['inventory'];
	$dataMonth2['balance'] = $row['balance'];
	$dataMonth2['attempt'] = $row['attempt'];
	$dataMonth2['rpc'] = $row['rpc'];
	$dataMonth2['ptp'] = $row['ptp'];
	$dataMonth2['jml_ptp'] = $row['jml_ptp'];
	$dataMonth2['payment'] = $row['payment'];
	$dataMonth2['acc'] = $row['acc'];
	
	echo "<tr>";
	echo '<td nowrap>'.$monthName2.' '.$year2.'</td>'
			 .'<td nowrap>'.$dataMonth2['inventory'].'</td>'
			 .'<td class="text">'.$dataMonth2['balance'].'</td>'
			 .'<td>'.$dataMonth2['attempt'].'</td>'
			 .'<td>'.number_format($dataMonth2['attempt']/$dataMonth2['inventory']*100,3).'</td>'
			 .'<td>'.$dataMonth2['rpc'].'</td>'
			 .'<td>'.number_format($dataMonth2['rpc']/$dataMonth2['attempt']*100,3).'%</td>'
			 .'<td>'.$dataMonth2['ptp'].'</td>'
			 .'<td>'.number_format($dataMonth2['ptp']/$dataMonth2['attempt']*100,3).'%</td>'
			 .'<td>'.$dataMonth2['jml_ptp'].'</td>'
			 .'<td>'.$dataMonth2['payment'].'</td>'
			 .'<td>'.$dataMonth2['acc'].'</td>'
			 .'<td>'.number_format($dataMonth2['acc']/$dataMonth2['inventory']*100,3).'%</td>';;
	echo "</tr>";
	echo "</table><br>";


}


 function showHeaders($month1, $month2, $year1, $year2) 
 {
	echo "<div class=\"center\">".
		 "<p class=\"normal font-size22\">Inventory Report</p>".
		 "<p class=\"normal font-size14\">Periode : ". $month1 ." ". $year1 ." and ". $month2 ." ". $year2 ."</p>".
		 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
	"</div>";
}


function printNote($sdate, $edate){
	echo "<p style=\"margin-left:10%;\">Note:</p>".
		  "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-left:10%;\">";    
	echo "<tr>";
	echo '<td nowrap>Periode 1 (Tanggal) : ';
	foreach($sdate as $date){
		$getDate = explode("-", $date);
		echo $getDate[2].", ";
	};
	echo "</td>";
	echo "</tr>";
	echo "<tr>";
	echo '<td nowrap>Periode 2 (Tanggal) : ';
	foreach($edate as $date){
		$getDate = explode("-", $date);
		echo $getDate[2].", ";
	};
	echo "</td>";
	echo "</tr>";
	   "</table>";
}
?>

<html>
	<head>
		<title>
			Enigma Collection Report - Inventory Report
		</title>
		<style>
		.text{
		mso-number-format:"\@";/*force text*/
	}
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	vertical-align:middle;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}


.left { text-align:left;}
.right{ text-align:right;}
.center{ text-align:center;}
.font-size22 { font-size:22px; color:#000;}
.font-size20 { font-size:20px; color:#000;}
.font-size18 { font-size:18px; color:#000;}
.font-size16 { font-size:16px; color:#000;}
.font-size14 { font-size:16px; color:#000;}

p.normal  { line-height:6px;}

			</style>
	</head>
<body>
	<?php showHeaders($monthName1,$monthName2, $year1, $year2);?>
	<?php showReport($monthName1,$monthName2, $year1, $year2, $sdates, $edates); ?>
	<?php printNote($sdates, $edates); ?>
</body>
</html>