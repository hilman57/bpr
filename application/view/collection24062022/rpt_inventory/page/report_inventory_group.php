<fieldset class="corner" style='margin-top:px;'>
<legend class="icon-menulist">&nbsp;&nbsp;User Filter </legend>
<form name="frmReport">
<div>
	<table cellpadding='4' cellspacing=4>
		
		<tr>
			<td class="text_caption bottom">Interval 1</td>
			<td >:</td>
			<td class='bottom'> <?php echo form()->input('start_date1','input_text box date');?> &nbsp- <?php echo form()->input('start_date2','input_text box date');?> </td>
		</tr>
		<tr>
			<td class="text_caption bottom">Interval 2</td>
			<td >:</td>
			<td class='bottom'> <?php echo form()->input('end_date1','input_text box date');?> &nbsp- <?php echo form()->input('end_date2','input_text box date');?> </td>
		</tr>
		
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td >&nbsp;</td>
			<td class='bottom'>
				<?php echo form()->button('','page-go button','Show',array("click"=>"new ShowReport();") );?>
				<?php echo form()->button('','excel button','Export',array("click"=>"new ShowExcel();") );?>
			</td>
		</tr>	
	</table>
</div>
</form>
</fieldset>