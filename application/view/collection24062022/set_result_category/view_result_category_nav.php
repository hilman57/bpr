<?php echo javascript(); ?>
<script type="text/javascript">
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();

/**
 ** javscript prototype system
 ** version v.0.1
 **/

 var datas= {
	keywords : '<?php echo _get_post('keywords'); ?>',
	order_by : '<?php echo _get_post('order_by'); ?>',
	type 	 : '<?php echo _get_post('type'); ?>'
 }
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
  
$(function(){
	
	$('#toolbars').extToolbars
	({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Enable'],['Disable'] ,['Add'],['Edit'],['Delete'],['Cancel'],[],['Search']],
		extMenu   : [['EnableResult'],['DisableResult'],['AddCategory'],['EditResult'],['DeleteCategory'],['CancelResult'],[],['SearchCategory']],
		extIcon   : [['accept.png'],['cancel.png'], ['add.png'],['calendar_edit.png'],['delete.png'],['cancel.png'],[],['zoom.png']],
		extText   : true,
		extInput  : true,
		extOption : [{
						render	: 6,
						type	: 'text',
						id		: 'CategoryName', 	
						name	: 'CategoryName',
						value	: datas.keywords,
						width	: 200
					}]
	});
});

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.EQuery.TotalPage   = <?php echo $page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo $page -> _get_total_record(); ?>;

/**
 ** javscript prototype system
 ** version v.0.1
 **/
		
var navigation = {
			custnav : Ext.DOM.INDEX +'/SetResultCategory/index/',
			custlist : Ext.DOM.INDEX +'/SetResultCategory/Content/'
		}
		
/* assign show list content **/
		
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();


Ext.DOM.SearchCategory = function(){
	Ext.EQuery.construct(navigation,{
		keywords : Ext.Cmp("CategoryName").getValue()
	});
	Ext.EQuery.postContent();
	
}
/* assign show list content **/
// assign show list content
		
Ext.DOM.CancelResult=function(){
	Ext.Cmp('span_top_nav').setText('');
}


/* assign show list content **/
// assign show list content

Ext.DOM.AddCategory = function(){
	// alert('waw');
	Ext.Ajax
	({
		url  : Ext.DOM.INDEX +'/SetResultCategory/AddView/',
		method : 'GET',
		param : {
			duration : Ext.Date().getDuration()
		}
	}).load('span_top_nav');
}

/* assign show list content **/		
/* edit category ****/	

Ext.DOM.EditResult = function()
{
	if( Ext.Cmp('chk_category').empty() ){ 
		Ext.Msg("Please select rows ").Info(); }
	else 
	{
		if( Ext.Cmp('chk_category').getValue().length > 1 ) { 
			Ext.Msg("Please select a rows ").Info(); }
		else
		{
			Ext.Ajax
			({
				url  	: Ext.DOM.INDEX +'/SetResultCategory/EditView/',
				method 	: 'GET',
				param 	: {
					duration 	: Ext.Date().getDuration(),
					CategoryId 	: Ext.Cmp('chk_category').getValue()
				}
			}).load('span_top_nav');
		}	
	}	
	
}

/* assign show list content **/		
/* * delete **/	
		
Ext.DOM.DeleteCategory = function()
{
 if( CategoryId = Ext.Cmp('chk_category').empty()!=true ) {
	if( Ext.Msg("Do you want delete this rows ").Confirm() )
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SetResultCategory/Delete/',
			method 	: 'POST',
			param 	: { 
				CategoryId : Ext.Cmp('chk_category').getValue(), 
				Active	: 0,
			},
			ERROR 	: function(fn)
			{
				try
				{
					var ERR = JSON.parse(fn.target.responseText);
					if( ERR.success ){
						Ext.Msg("Delete Result Category ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Delete Result Category ").Failed();
					}
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			}
		}).post();
	}	
 }
 else{ Ext.Msg("Please select a row!").Info(); }
}
	

/* assign show list content **/
/* DisableResult **/
Ext.DOM.DisableResult=function()
{
 var CategoryId = Ext.Cmp('chk_category').getValue();
 if( CategoryId !='') 
 {
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SetResultCategory/SetActive/',
		method 	: 'POST',
		param 	: { 
			CategoryId : CategoryId, 
			Active	: 0,
		},
		ERROR 	: function(fn)
		{
			try{
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Disable Result Category ").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Disable Result Category ").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();
 }
 else{ Ext.Msg("Please select a row!").Info(); }
 
}

/* assign show list content **/		
/* enable **/
Ext.DOM.EnableResult=function()
{
 var CategoryId = Ext.Cmp('chk_category').getValue();
 if( CategoryId !='') 
 {
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SetResultCategory/SetActive/',
		method 	: 'POST',
		param 	: { 
			CategoryId : CategoryId, 
			Active	: 1,
		},
		ERROR 	: function(fn)
		{
			try{
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Enable Result Category ").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Enable Result Category ").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();
 }
 else{ Ext.Msg("Please select a row!").Info(); }
 
}
		

/* assign show list content **/
// Ext.DOM.UpdateCatgory
		
Ext.DOM.UpdateCategory = function()
{
	var CallReasonCategoryId    = Ext.Cmp('CallReasonCategoryId').getValue(),
		CallReasonCategoryCode 	= Ext.Cmp('CallReasonCategoryCode').getValue(), 
		CallReasonCategoryName 	= Ext.Cmp('CallReasonCategoryName').getValue(), 
		CallReasonInterest 		= Ext.Cmp('CallReasonInterest').getValue(), 
		CallReasonCategoryFlags = Ext.Cmp('CallReasonCategoryFlags').getValue(), 
		CallReasonCategoryOrder = Ext.Cmp('CallReasonCategoryOrder').getValue(), 
		CallOutboundGoalsId 	= Ext.Cmp('CallOutboundGoalsId').getValue();
	
	if( Ext.Cmp('CallOutboundGoalsId').empty()){ Ext.Msg("Call Type is empty").Info();}
	else if( Ext.Cmp('CallReasonCategoryCode').empty()){ Ext.Msg("Category Code is empty").Info();	}	
	else if( Ext.Cmp('CallReasonCategoryName').empty() ){ Ext.Msg("Category Name is empty").Info(); }
	else
	{
		Ext.Ajax ({
			url 	: Ext.DOM.INDEX +'/SetResultCategory/UpdateCategory/',
			method 	: 'POST',
			param 	: { 
				CallReasonCategoryId	: CallReasonCategoryId,
				CallReasonCategoryCode 	: CallReasonCategoryCode,
				CallReasonCategoryName 	: CallReasonCategoryName, 
				CallReasonInterest 		: CallReasonInterest, 
				CallReasonCategoryFlags : CallReasonCategoryFlags, 
				CallReasonCategoryOrder : CallReasonCategoryOrder,
				CallOutboundGoalsId 	: CallOutboundGoalsId
			},
			ERROR 	: function(fn) {
				try {
					var ERR = JSON.parse(fn.target.responseText);
					if( ERR.success ){
						Ext.Msg("Update Result Category ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Update Result Category ").Failed();
					}
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			}
		}).post();
	}	
}
		

/* assign show list content **/
// Ext.DOM.SaveCatgory

Ext.DOM.SaveCatgory = function()
{
	var CallReasonCategoryCode 	= Ext.Cmp('CallReasonCategoryCode').getValue(), 
		CallReasonCategoryName 	= Ext.Cmp('CallReasonCategoryName').getValue(), 
		CallReasonInterest 		= Ext.Cmp('CallReasonInterest').getValue(), 
		CallReasonCategoryFlags = Ext.Cmp('CallReasonCategoryFlags').getValue(), 
		CallReasonCategoryOrder = Ext.Cmp('CallReasonCategoryOrder').getValue(), 
		CallOutboundGoalsId 	= Ext.Cmp('CallOutboundGoalsId').getValue();
	
	if( Ext.Cmp('CallOutboundGoalsId').empty()){ Ext.Msg("Call Type is empty").Info();}
	else if( Ext.Cmp('CallReasonCategoryCode').empty()){ Ext.Msg("Category Code is empty").Info();	}	
	else if( Ext.Cmp('CallReasonCategoryName').empty() ){ Ext.Msg("Category Name is empty").Info(); }
	else
	{
		Ext.Ajax ({
			url 	: Ext.DOM.INDEX +'/SetResultCategory/SaveCategory/',
			method 	: 'POST',
			param 	: { 
				CallReasonCategoryCode 	: CallReasonCategoryCode,
				CallReasonCategoryName 	: CallReasonCategoryName, 
				CallReasonInterest 		: CallReasonInterest, 
				CallReasonCategoryFlags : CallReasonCategoryFlags, 
				CallReasonCategoryOrder : CallReasonCategoryOrder,
				CallOutboundGoalsId 	: CallOutboundGoalsId
			},
			ERROR 	: function(fn) {
				try {
					var ERR = JSON.parse(fn.target.responseText);
					if( ERR.success ){
						Ext.Msg("Save Result Category ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Save Result Category ").Failed();
					}
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			}
		}).post();
	}
}
</script>
<!-- start : content -->
<fieldset class="corner">
	<legend class="icon-callresult">&nbsp;&nbsp;<span id="legend_title"></span> </legend>	
		<div id="toolbars"></div>
		<div id="span_top_nav"></div>
		<div class="content_table"></div>
		<div id="pager"></div>
		<div id="ViewCmp"></div>
	</fieldset>	
<!-- stop : content -->
	
	
	