<?php ?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('chk_category').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" align="center">&nbsp;No</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" align="left">&nbsp;<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.CallReasonCategoryCode');">Result Code</span> </th>        
        <th nowrap class="font-standars ui-corner-top ui-state-default first center" align="left">&nbsp;<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.CallReasonCategoryName');">Result Name</span> </th>
		 <th nowrap class="font-standars ui-corner-top ui-state-default first center" align="center">&nbsp;<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.CallReasonInterest');">Interest</span> </th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.CallReasonCategoryOrder');">Order</span> </th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('b.Name');">Call Type</span> </th>
		<th nowrap width="15%" align="center" class="font-standars ui-corner-top ui-state-default first center">&nbsp;<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.CallReasonCategoryFlags');">Status</span> </th>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
	<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"><input type="checkbox" value="<?php echo $rows['CallReasonCategoryId']; ?>" name="chk_category" id="chk_category"></td>
		<td class="content-middle" align="center"><?php echo $no ?></td>
		<td class="content-middle"><?php echo $rows['CallReasonCategoryCode']; ?></td>
		<td class="content-middle"><?php echo $rows['CallReasonCategoryName']; ?></td>
		<td class="content-middle" align="center"><?php echo ($rows['CallReasonInterest']?'YES' :'NO'); ?></td>
		<td class="content-middle" align="center"><?php echo $rows['CallReasonCategoryOrder']; ?></td>
		<td class="content-middle" align="center"><?php echo $rows['Name']; ?></td>
		<td align="center" class="content-lasted"><?php echo ($rows['CallReasonCategoryFlags']?'Active':'Not Active');?></td>
	</tr>	
</tbody>
<?php
	$no++;
  };
?>
</table>



