<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
<fieldset class="corner" style="background-color:white;margin:3px;">
 <legend class="icon-application">&nbsp;&nbsp;&nbsp;Edit Category Result </legend>	
	<?php echo form()->hidden('CallReasonCategoryId',null,$Data['CallReasonCategoryId']); ?>
	<table cellpadding="6px;">
	<tr>
		<td class="text_caption">* Call Type </td>
		<td><?php echo form() -> combo('CallOutboundGoalsId','input_text long',$CallType,$Data['CallOutboundGoalsId']);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Interest</td>
		<td><?php echo form() -> combo('CallReasonInterest','input_text long',$Interest,$Data['CallReasonInterest']);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Category Code</td>
		<td><?php echo form() -> input('CallReasonCategoryCode','input_text long',$Data['CallReasonCategoryCode']);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Category Name</td>
		<td><?php echo form() -> input('CallReasonCategoryName','input_text long',$Data['CallReasonCategoryName']);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Status</td>
		<td><?php echo form() -> combo('CallReasonCategoryFlags','select long', array(0=>'Not Active',1=>'Active'),$Data['CallReasonCategoryFlags']);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Order </td>
		<td><?php echo form() -> combo('CallReasonCategoryOrder','select long', $OrderId , $Data['CallReasonCategoryOrder']);?></td>
	</tr>
	<tr>
		<td class="text_caption">&nbsp;</td>
		<td><input type="button" class="update button" onclick="Ext.DOM.UpdateCategory();" value="Update"></td>
	</tr>
</table>
</fieldset>
</div>