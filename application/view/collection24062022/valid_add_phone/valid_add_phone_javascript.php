<?php __(javascript()); ?>
<script type="text/javascript">

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/ValidAddPhone/index/',
	custlist : Ext.DOM.INDEX+'/ValidAddPhone/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation)
Ext.EQuery.postContentList();

Ext.DOM.Approve = function()
{
	var Error = ( 
					Ext.Ajax({
						url		: Ext.DOM.INDEX+'/ValidAddPhone/ApproveNumber',
						method	: 'POST',
						param	: {
							ApproveID : Ext.Cmp('chk_valid_id').getChecked()
						}
					}).json()
				);
		
	if( Error.success)
	{ 
		alert("Success, Enable Menu");
		Ext.EQuery.postContent();
	}
	else{
		alert("Failed, Enabled Menu");
	}
}

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Approve']],
		extMenu  : [['search'],['resetSeacrh'],['Approve']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['accept.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('validNumber').Clear();
	Ext.DOM.search();
}

Ext.DOM.search = function() {
	var param = Ext.Serialize('validNumber').getElement();
	Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	Ext.EQuery.postContent();
}
	
</script>