<fieldset class="corner" onkeydown="enterSearch(event);">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span>Request Valid Number List</legend>
	<div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
		<form name="validNumber">
			<table cellspacing="0">
				<tr>
					<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
					<td class="bottom"><?php echo form()-> input('keys_cust_id','input_text long', _get_exist_session('keys_cust_id'));?></td>
					<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
					<td class="bottom"><?php echo form()->input('keys_cust_name','input_text long', _get_exist_session('keys_cust_name'));?></td>
					<td class="text_caption bottom"> # Phone Number&nbsp;:</td>
					<td class="bottom"><?php echo form()->input('keys_phone_number','input_text long', _get_exist_session('keys_phone_number'));?></td>
					<td class="text_caption bottom"> # Phone Type Number&nbsp;:</td>
					<td class="bottom"><?php echo form()->combo('keys_phone_type','select long', array());?></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="toolbars" style="margin:5px;"></div>
	<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
		<div class="content_table" style="margin:-4px;"></div>
		<div id="pager"></div>
	</div>
</fieldset>

<?php $this -> load -> view('valid_add_phone/valid_add_phone_javascript') ?>