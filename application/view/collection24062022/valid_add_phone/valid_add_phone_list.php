<style>
	.wraptext{color:green;font-size:11px;padding:3px;width:120px;}
	.wraptext:hover{color:blue;}
</style>
<table width="100%" class="custom-grid" cellspacing="0">
	<thead>
		<tr height="20"> 
			<th nowrap class="font-standars ui-corner-top ui-state-default first center" >&nbsp;<a href="javascript:void(0);" onclick="doJava.checkedAll('chk_cust_call');">#</a></th>	
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No</th>			
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('e.CampaignName');">Customer Name</span></th>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('e.CampaignName');">Customer ID</span></th>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('c.CustomerFirstName');">Phone Number</span></th>     
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('c.CustomerFirstName');">Phone Type Number</span></th>     
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('h.AproveName');">Valid Status</span></th>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('e.full_name');">Agent Name</span></th>
			<th nowrap class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('b.PolicySalesDate');">Valid Date</span></th>
			<th nowrap class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;<span class="header_order" onclick="extendsJQuery.orderBy('b.PolicySalesDate');">Approve Date</span></th>
		</tr>
	</thead>	
	<tbody>
	<?php
		$no = $num;
		foreach( $page -> result_assoc() as $rows ) 
		{
			// echo "<pre>";
			// print_r($page);
			// echo "</pre>";
			$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	?>
		<tr class="onselect" bgcolor="<?php echo $color;?>">
			<td class="content-first">
				<input type="checkbox" value="<?php  echo $rows['ValidID']; ?>" name="chk_valid_id" name="chk_valid_id">
			</td>
			<td class="content-middle" align="center"><?php  echo $no; ?></td>
			<td class="content-middle"><div style="width:100px;"><?php  echo $rows['deb_name']; ?></div></td>
			<td class="content-middle"><div style="width:100px;"><?php  echo $rows['deb_cardno']; ?></div></td>
			<td class="content-middle" nowrap align="center"><?php echo $rows['PhoneNumber']; ?></td>
			<td class="content-middle" nowrap align="center"><?php echo $rows['adp_name']; ?></td>
			<td class="content-middle" nowrap align="center"><span style="color:#b14a06;font-weight:bold;"><?php echo ($rows['valid_status']?$rows['valid_status']:'-'); ?></span></td>
			<td class="content-middle" nowrap align="center"><?php echo $rows['agent_name']; ?></td>
			<td class="content-lasted" nowrap align="center"><?php echo $rows['ValidDate']; ?></td>
			<td class="content-lasted" nowrap align="center"><?php echo $rows['ValidApproveDate']; ?></td>
		</tr>
	<?php
			$no++;
		}
	?>
	</tbody>
</table>