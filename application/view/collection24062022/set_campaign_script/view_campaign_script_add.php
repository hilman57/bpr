<fieldset class="corner" style="margin:8px;">
	<legend class="pdf">&nbsp;&nbsp; Add Campaign Script</legend>	
	<div class="content-panel box-shadow">
		<table cellspacing="10">
			<tr> 
				<td class="text_caption">* Campaign Name </td>
				<td><?php echo form()->combo("CampaignId","select ",$CampaignName);?></td>
			</tr>
			
			<tr> 
				<td class="text_caption">* File Name </td>
				<td><?php echo form()->upload('ScriptFileName');?></td>
			</tr>
			
			<tr> 
				<td class="text_caption">* Title</td>
				<td><?php echo form()->input("Description","select long");?></td>
			</tr>
			
			<tr> 
				<td class="text_caption">* Status </td>
				<td><?php echo form()->combo("ScriptFlagStatus","select long",$Active);?></td>
			</tr>
			
			<tr> 
				<td class="text_caption">&nbsp;</td>
				<td>
					<input type="button" class="upload button" onclick="Ext.DOM.UploadScript();" value="Upload">
					<input type="button" class="close button" onclick="Ext.Cmp('span_top_nav').setText('');" value="Close">
				</td>
			</tr>
			
			
		</table>
	</div>
</fieldset>