<?php echo javascript(); ?>
<script type="text/javascript">

var OUTBOUND = 2;

// @ pack : upload data listener 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
// @ pack : upload data listener 

var status_campaign = '<?php echo $this -> URI -> _get_post("status_campaign"); ?>';

// @ pack : upload data listener 

var datas= { 
	status_campaign	: status_campaign,
	order_by  : '<?php echo $this -> URI -> _get_post('order_by');?>',
	type : '<?php echo $this -> URI -> _get_post('type');?>'
}

function ExportDeleted() 
{
	var CampaignId = Ext.Cmp('check_list_cmp').getValue();
	Ext.Window
	({
		url : Ext.DOM.INDEX +'/SetCampaign/ExportDeleted/',
		param :{
			CampaignId : CampaignId
		}	
	}).newtab();
		
}
// @ pack : upload data listener 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Detail Data'],['Export'],['Export Deleted']],
		extMenu  :[['ShowRowData'],['ExportExcel'],['ExportDeleted']],
		extIcon  :[['table_go.png'],['page_white_excel.png'],['page_white_excel.png']],
		extText  :true,
		extInput :true,
		extOption:[{
			render	: 1,
			header	: 'Filter ',
			type	: 'combo',
			id		: 'combo_filter_campaign', 	
			name	: 'combo_filter_campaign',
			value	: status_campaign,
			store	: [{'2':'All'},{'1':'Active'},{'0':'Not Active'}],
			triger	: '',
			width	: 200
		}]
	});	
});

// @ pack : upload data listener 

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : upload data listener 

var navigation = {
	custnav	 : Ext.DOM.INDEX+'/SetCampaign/index/',
	custlist : Ext.DOM.INDEX+'/SetCampaign/Content/',
}
	
// @ pack : upload data listener 

Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();


// @ pack : upload data listener 

Ext.DOM.ExportExcel = function() {
	var CampaignId = Ext.Cmp('check_list_cmp').getValue();
	if( CampaignId!='' )
	{
		// console.log(CampaignId.length);
		// var ArrCampaign =  CampaignId.split(",");
		if( CampaignId.length === 1 )
		{
			Ext.Window
			({
				url : Ext.DOM.INDEX +'/SetCampaign/Export/',
				param :{
					CampaignId : CampaignId
				}	
			}).newtab();
		}
		else
		{
			Ext.Msg('Please Select one Campaign').Info();
		}
	}
	else{
		Ext.Msg('Please Select Campaign').Info();
	}	
}
		
// @ pack : upload data listener 

Ext.DOM.ShowRowData = function() {
 var CampaignId = Ext.Cmp('check_list_cmp').getValue();
 if( CampaignId!='' ) {
		Ext.Window ({
			url : Ext.DOM.INDEX +'/SetCampaign/View/',
			param :{
				CampaignId : CampaignId
			}	
		}).newtab();
	}
	else{
		Ext.Msg('Please Select Campaign').Info();
	}	
}


// @ pack : upload data listener 

Ext.DOM.viewCampaign = function(){
	var status_campaign = Ext.Cmp('combo_filter_campaign').getValue();
	if( status_campaign ) {
		datas = { status_campaign: status_campaign }
		Ext.EQuery.construct(navigation,datas)
		Ext.EQuery.postContent();
	}	
}

</script>