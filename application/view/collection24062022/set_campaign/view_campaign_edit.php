<!-- start : layout add campaign -->
<script>
$(function(){
 var date = new Date();
	$("#ExpiredDate").datepicker
	({
		showOn : 'button',  
		buttonImage	: Ext.DOM.LIBRARY +'/gambar/calendar.gif', 
		buttonImageOnly: true,
		dateFormat	:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange:date.getFullYear()+':3000'
	});

	$("#StartDate").datepicker
	({
		showOn : 'button',  
		buttonImage	: Ext.DOM.LIBRARY +'/gambar/calendar.gif', 
		buttonImageOnly: true,
		dateFormat	:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange:date.getFullYear()+':3000'
	});
});
</script>		
<form name="frm_add_campaign">
<div class="box-shadow" style="padding:10px;">
 <fieldset class="corner">
 <legend class="icon-application"> &nbsp;&nbsp;&nbsp;Edit Campaigns </legend>
 <table border=0 width="80%" align="left" cellpadding="3px" style="margin-top:-10px;">
 <tr>
	<td valign="top" align="left">
		<table cellspacing="6" align="left" border="0">
			
			<tr style="display:none;">
				<td class="text_caption" style="display:none;">* Campaign ID. </td>
				<td style="display:none;"></td>
			</tr>
			
			<tr style="display:yes;">
				<td class="text_caption bottom" nowrap>* Campaign Number. </td>
				<td>
				<?php echo form() -> hidden("CampaignId",null, $Campaign['CampaignId']); ?>
				<?php echo form() -> input("CampaignNumber","select long", $Campaign['CampaignNumber'], NULL, array('disabled' => true,'style'=>'color:#dddddd;')); ?></td>
				<td class="text_caption bottom" nowrap>* Campaign Name</td>
				<td>  <?php echo form() -> input('CampaignName', 'input_text long', $Campaign['CampaignName']); ?></td>
				
				<td style = "display:none"> 
					<?php echo form() -> combo('DirectMethod', 'input_text long', $Method ,1,array('change'=>'getMethod(this);'),array('length'=> '12', 'disabled' => true)); ?>
				</td>
			</tr>
			<tr style="display:yes;">
				<td class="text_caption bottom" style="display:yes;">* Campaign Code. </td>
				<td><?php echo form() -> input("CampaignCode","select long", $Campaign['CampaignCode'], NULL, null); ?></td>
				<td class="text_caption bottom" nowrap>Campaign Desc</td>
				<td>  <?php echo form() -> input('CampaignDesc', 'input_text long', $Campaign['CampaignDesc']); ?></td>
			</tr>
			
			<tr style="display:none";>
				<td class="text_caption" >* Campaign Type</td>
				<td><?php echo form() -> combo("CampaignTypeId"); ?></td>
			</tr>			
			<tr style="display:none;">
				<td class="text_caption" style="display:none";>* Re-Upload Campaign</td>
				<td style="display:none;"><?php echo form() -> combo("ReUpload","select"); ?></td>
			</tr>
			<tr>
				<td class="text_caption bottom" valign="top">* Work Project</td>
				<td valign='top'> <?php echo form() -> combo('ProjectId', 'select long',$Utility->_get_work_project(),$Campaign['ProjectId'], NULL); ?> </td>
				<td class="text_caption bottom">* Build Type</td>
				<td colspan="1" class="text_caption left" nowrap> <?php echo form() -> combo('BuildTypeId', 'select long',$Utility->BuildType(), $Campaign['BuildTypeId'], null, array('disabled'=>true)); ?></td>
			</tr>
			
			<tr>
				<td class="text_caption bottom">* Campaign Interval</td>
				<td  class="text_caption"><?php echo form() -> input('StartDate', 'input_text box',$this -> EUI_Tools ->_date_indonesia($Campaign['CampaignStartDate'])); ?> to <?php echo form() -> input('ExpiredDate', 'input_text box',$this -> EUI_Tools ->_date_indonesia($Campaign['CampaignEndDate'])); ?></td>
				<td class="text_caption bottom">Method</td>
				<td style = "display:yes"> <?php echo form() -> combo('DirectMethod', 'input_text long', $Method ,$Campaign['DirectMethod'], array('change'=>'getMethod(this);'),array('length'=> '12', 'disabled' => true)); ?></td>
			</tr>
			
			
			<tr>
				<td class="text_caption bottom" nowrap>* Outbound / Inbound</td>
				<td><?php echo form() -> combo("OutboundGoalsId","select long",$OutboundGoals,$Campaign['OutboundGoalsId'],array('change'=>'getDirect(this);'), array('disabled'=>true)); ?></td>
				<td class="text_caption bottom" valign="top">* Action</td>
				<td valign="top"> <?php echo form() -> combo('DirectAction', 'input_text long',$Action,$Campaign['DirectAction'], null, array('length'=> '12', 'disabled' => true)); ?></td>
			</tr>
			
			<tr>
				<td class="text_caption bottom" valign="top" nowrap>* Source Campaign</td>
				<td valign="top"> <?php echo form() -> combo('AvailCampaignId', 'input_text long', $Avail,null,null,array('length'=> '12', 'disabled' => true)); ?></td>
				
			</tr>
			
			<tr>
				<td class="text_caption bottom">* Status Active</td>
				<td><?php echo form() -> combo("StatusActive","select long",array('1'=>'Active','0'=>'Not Active'),$Campaign['CampaignStatusFlag']); ?></td>
			</tr>

			<tr style="display:none;">
				<td class="text_caption" >* Date Extends</td>
				<td><?php echo form() -> input('ExtendsDate', 'input_text box'); ?></td>
			</tr>
			<tr style="display:none;">
				<td class="text_caption">* Re-Upload Reason</td>
				<td><?php echo form() -> combo('ReUploadReason'); ?></td>
			</tr>
				
			<tr>
				<td align="right" colspan=4>
					<input type="button" class="save button" onclick="Ext.DOM.SetUpdate();" value="Update">
					<input type="button" class="close button" onclick="Ext.Cmp('span_top_nav').setText('');" value="Close">
				</td>
			</tr>
		</table>
	</td>
	</tr>

</table>

</fieldset>
</div>

</form>