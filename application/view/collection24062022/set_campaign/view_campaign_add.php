<!-- start : layout add campaign -->
<script>
$(function(){
 var date = new Date();
	$("#ExpiredDate").datepicker
	({
		showOn : 'button',  
		buttonImage	: Ext.DOM.LIBRARY +'/gambar/calendar.gif', 
		buttonImageOnly: true,
		dateFormat	:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange:date.getFullYear()+':3000'
	});

	$("#StartDate").datepicker ({
		showOn : 'button',  
		buttonImage	: Ext.DOM.LIBRARY +'/gambar/calendar.gif', 
		buttonImageOnly: true,
		dateFormat	:'dd-mm-yy',changeMonth: true,changeYear: true,yearRange:date.getFullYear()+':3000'
	});
});

</script>		
<form name="frm_add_campaign">
<div class="box-shadow" style="padding:10px;">
 <fieldset class="corner">
 <legend class="icon-application"> &nbsp;&nbsp;&nbsp;Add Campaign </legend>
 <table border=0 width="70%" align="left" cellpadding="3px" style="margin-top:-10px;">
 <tr>
	<td valign="top" align="left">
		<table cellspacing="4" align="left" border="0" width='100%'>
			
			<tr style="display:none;">
				<td class="text_caption bottom" style="display:none;">* Campaign ID. </td>
				<td style="display:none;"><?php echo form() -> combo("CampaignId"); ?></td>
			</tr>
			
			<tr style="display:yes;">
				<td class="text_caption bottom" nowrap>* Campaign Number. </td>
				<td class="bottom"><?php echo form() -> input("CampaignNumber","select long", $Utility -> _get_campaign_code_number(), NULL, array('disabled' => true,'style'=>'color:#dddddd;')); ?></td>
				<td class="text_caption bottom" nowrap>* Campaign Name</td>
				<td class="bottom"> <?php echo form() -> input('CampaignName', 'input_text long'); ?></td>
			</tr>	
			<tr>	
				<td class="text_caption bottom" nowrap>* Campaign Code. </td>
				<td class="bottom"><?php echo form() -> input('CampaignCode','input_text long'); ?></td>
				<td class="text_caption bottom" nowrap>Campaign Desc</td>
				<td class="bottom"> <?php echo form() -> input('CampaignDesc', 'input_text long'); ?></td>
			</tr>
			
			<tr style="display:none";>
				<td class="text_caption bottom" >* Campaign Type</td>
				<td><?php echo form() -> combo("CampaignTypeId"); ?></td>
			</tr>
								
			<tr style="display:none">
				<td class="text_caption bottom" style="display:none;"> Built Type</td>
				<td style="display:none"><?php echo form() -> combo("BuiltTypeIdxx","select", $Utility->BuildType() ); ?></td>
			</tr>
								
			<tr style="display:none;">
				<td class="text_caption bottom" style="display:none";>* Re-Upload Campaign</td>
				<td style="display:none;"><?php echo form() -> combo("ReUpload","select"); ?></td>
			</tr>
			
			<tr>
				<td class="text_caption bottom" valign="top">* Work Project</td>
				<td valign='top'> <?php echo form() -> combo('ProjectId', 'select long',$Utility->_get_work_project(),NULL, NULL); ?> </td>
				<td class="text_caption bottom">* Build Type</td>
				<td colspan="1" class="text_caption left" nowrap> <?php echo form() -> combo('BuildTypeId', 'select long',$Utility->BuildType(), 1, null, array('disabled'=>true)); ?></td>
			</tr>
			
			<tr>
				<td class="text_caption bottom">* Campaign Interval</td>
				<td colspan="1" class="text_caption left" nowrap><?php echo form() -> input('StartDate', 'input_text box'); ?> to <?php echo form() -> input('ExpiredDate', 'input_text box'); ?></td>
				<td class="text_caption bottom">Method</td>
				<td style = "display:yes"> <?php echo form() -> combo('DirectMethod', 'input_text long', $Method ,2, array('change'=>'getMethod(this);'),array('length'=> '12', 'disabled' => true)); ?></td>
			
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>* Outbound / Inbound</td>
				<td><?php echo form() -> combo("OutboundGoalsId","select long",$OutboundGoals,2,array('change'=>'getDirect(this);'), array('disabled'=>true)); ?></td>
				<td class="text_caption bottom" valign="top">* Action</td>
				<td valign="top"> <?php echo form() -> combo('DirectAction', 'input_text long',$Action,1, null, array('length'=> '12', 'disabled' => true)); ?></td>
			</tr>
			
			<tr>
				<td class="text_caption bottom" valign="top" nowrap>* Source Campaign</td>
				<td valign="top"> <?php echo form() -> combo('AvailCampaignId', 'input_text long', $Avail,null,null,array('length'=> '12', 'disabled' => true)); ?></td>
				
			</tr>
			
			<tr>
				<td class="text_caption bottom">* Status Active</td>
				<td><?php echo form() -> combo("StatusActive","select long",array('1'=>'Active','0'=>'Not Active'),1); ?></td>
			</tr>

			<tr style="display:none;">
				<td class="text_caption" >* Date Extends</td>
				<td><?php echo form() -> input('ExtendsDate', 'input_text box'); ?></td>
			</tr>
			
			<tr style="display:none;">
				<td class="text_caption">* Re-Upload Reason</td>
				<td><?php echo form() -> combo('ReUploadReason'); ?></td>
			</tr>
			
		</table>
	</td>
	</tr>
	<tr>
		<td  align="right">
			
			<input type="button" class="save button" onclick="Ext.DOM.saveCmpUpload();" value="Save">
			<input type="button" class="close button" onclick="Ext.Cmp('span_top_nav').setText('');" value="Close">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
</table>

</fieldset>
</div>


</form>