<?php
// ini_set("memory_limit", "2048M");
ini_set('memory_limit', -1); 
set_time_limit(3000);
define('LINES', 5);

/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
$CampaignId = _get_array_post('CampaignId');
$this->load->helper(array('EUI_ExcelWorksheet','EUI_ExcelWorkbookBig'));   
$set_rows_headers  = array(
	'debiturid'				=> array('field'=>'deb_id'),
	'cardno'				=> array('field'=>'deb_cardno'),
	'custno'				=> array('field'=>'deb_acct_no'),
	'accountno'				=> array('field'=>'deb_acct_no'),
	'idno'					=> array('field'=>'deb_id_number'),
	'nbrcards'				=> array('field'=>'deb_no_card'),
	'fullname'				=> array('field'=>'deb_name'),
	'mothername'			=> array('field'=>'deb_mother_name'),
	'dateofbirth'			=> array('field'=>'deb_dob'),
	'homeaddress'			=> array('field'=>'deb_add_hm'),
	'billingaddress'		=> array('field'=>'deb_billing_add'),
	'officeaddress'			=> array('field'=>'deb_add_off'),
	'homezip'				=> array('field'=>'deb_zip_h'),
	'homephone'				=> array('field'=>'deb_home_no1'),
	'homephone2'			=> array('field'=>NULL),
	'mobilephone'			=> array('field'=>'deb_mobileno1'),
	'mobilephone2'			=> array('field'=>NULL),
	'otheraddress'			=> array('field'=>NULL),
	'otheraddress2'			=> array('field'=>NULL),
	'officephone'			=> array('field'=>'deb_office_ph1'),
	'officefax'				=> array('field'=>NULL),
	'ecname'				=> array('field'=>'deb_ec_name_f'),
	'ecaddress'				=> array('field'=>'deb_ec_add_f'),
	'ecphone'				=> array('field'=>'deb_ec_phone_f'),
	'ecmobile'				=> array('field'=>'deb_ec_mobile_f'),
	'ecrelationship'		=> array('field'=>NULL),
	'branch'				=> array('field'=>NULL),
	'area'					=> array('field'=>'deb_region'),
	'opendate'				=> array('field'=>'deb_open_date'),
	'clss_entry'			=> array('field'=>'deb_resource'),
	'creditlimit'			=> array('field'=>'deb_limit'),
	'currentbalance'		=> array('field'=>'deb_wo_amount'),
	'feeandcharge'			=> array('field'=>'deb_fees'),
	'interest'				=> array('field'=>'deb_interest'),
	'principalbalance'		=> array('field'=>'deb_principal'),
	'lastpaymentdate'		=> array('field'=>'deb_last_paydate'),
	'lastpaymentamount'		=> array('field'=>'deb_last_pay'),
	'currentpaymentdue'		=> array('field'=>NULL),
	'bpcount'				=> array('field'=>'deb_bp'),
	'ptpcount'				=> array('field'=>'ptpcounts'),
	'wo_date'				=> array('field'=>'deb_b_d'),
	'wo_amount'				=> array('field'=>'deb_wo_amount'),
	'wo_open'				=> array('field'=>NULL),
	'wo_lpd'				=> array('field'=>NULL),
	'curr_coll_id'			=> array('field'=>'deb_update_by_user'),
	'dlq'					=> array('field'=>'dlq'),
	'perm_message'			=> array('field'=>'deb_perm_msg'),
	'perm_date'				=> array('field'=>'deb_perm_msg_date'),
	'accstatus'				=> array('field'=>'CallReasonDesc'),
	'accstatusdate'			=> array('field'=>'deb_call_activity_datets'),
	'prevaccstatus'			=> array('field'=>'prevac'),
	'prevaccstatusdate'		=> array('field'=>NULL),
	'lastresponse'			=> array('field'=>NULL),
	'contacthistoryid'		=> array('field'=>'bucket_agent'),
	'agent'					=> array('field'=>'deb_agent'),
	'note'					=> array('field'=>'note'),
	'reminder'				=> array('field'=>'deb_reminder'),
	'uploadbatch'			=> array('field'=>'uploadbatch'),
	'ptp_discount'			=> array('field'=>'ptp_discount'),
	'ptp_amount'			=> array('field'=>'ptpAmount'),
	'ptp_date'				=> array('field'=>'ptpDate'),
	'ptp_channel'			=> array('field'=>'ptpChannel'),
	'ssv_no'				=> array('field'=>'deb_ssv_no'),
	'callstatus'			=> array('field'=>'prevac'),
	'calldate'				=> array('field'=>'deb_call_activity_datets'),
	'prevcallstatus'		=> array('field'=>NULL),
	'infoptp'				=> array('field'=>'infoptp'),
	'prevcalldate'			=> array('field'=>NULL),
	'tenor'					=> array('field'=>'ptpTenor'),
	'addhomephone'			=> array('field'=>'addhomephone'),
	'addhomephone2'			=> array('field'=>'addhomephone2'),
	'addmobilephone'		=> array('field'=>'addmobilephone'),
	'addmobilephone2'		=> array('field'=>'addmobilephone2'),
	'addofficephone'		=> array('field'=>'addofficephone'),
	'addofficephone2'		=> array('field'=>'addofficephone2'),
	'addfax'				=> array('field'=>'addfax'),
	'addfax2'				=> array('field'=>'addfax2'),
	'swap_count'			=> array('field'=>'deb_swap_count'),
	'sms_count'				=> array('field'=>'deb_sms_count'),
	'other_agent'			=> array('field'=>'other_agent'),
	'other_card_number' 	=> array('field'=>'other_card_number'),
	'other_accstatus'		=> array('field'=>'other_accstatus'),
	'contacttype'			=> array('field'=>'deb_contact_type'),
	'rpc'					=> array('field'=>'deb_rpc'),
	'spc_with'				=> array('field'=>'deb_spc'),
	'namahomephone1'		=> array('field'=>NULL),
	'relativehomephone1'	=> array('field'=>NULL),
	'namahomephone2'		=> array('field'=>NULL),
	'relativehomephone2'	=> array('field'=>NULL),
	'namaofficephone1'		=> array('field'=>NULL),
	'relativeofficephone1'	=> array('field'=>NULL),
	'namaofficephone2'		=> array('field'=>NULL),
	'relativeofficephone2'	=> array('field'=>NULL),
	'namatlpphone1'			=> array('field'=>NULL),
	'relativetlpphone1'		=> array('field'=>NULL),
	'namatlpphone2'			=> array('field'=>NULL),
	'relativetlpphone2'		=> array('field'=>NULL),
	'namamobilephone1'		=> array('field'=>NULL),
	'relativemobilephone1'	=> array('field'=>NULL),
	'namamobilephone2'		=> array('field'=>NULL),
	'relativemobilephone2'	=> array('field'=>NULL),
	'keep_data'				=> array('field'=>'deb_is_kept'),
	'afterpay'				=> array('field'=>'deb_afterpay'),
	'attempt_call'			=> array('field'=>'attempt_call'),
	'isblock'				=> array('field'=>'deb_is_lock'),
	'bal_afterpay'			=> array('field'=>'deb_bal_afterpay'),
	'pri_afterpay'			=> array('field'=>'deb_pri_afterpay'),
	'tenor_value'			=> array('field'=>NULL),
	'tenor_amnt'			=> array('field'=>NULL),
	'tenor_dates'			=> array('field'=>NULL),
	'flag'					=> array('field'=>'flag'),
	'current_lock'			=> array('field'=>NULL),
	'lock_modul'			=> array('field'=>NULL),
	'cpa_count'				=> array('field'=>NULL),
	'lunas_flag'			=> array('field'=>NULL),
	'last_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'max_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'acc_mapping'			=> array('field'=>'acc_mapping'),
	'class_mapping'			=> array('field'=>NULL),
	'accstatus_name'		=> array('field'=>'accstatus_name'),
	'SPC_DESC'				=> array('field'=>'SPC_DESC'),
	'RPC_DESC'				=> array('field'=>'RPC_DESC'),
	'other_ch_office'		=> array('field'=>'other_ch_office'),
	'other_ch_home'			=> array('field'=>'other_ch_home'),
	'other_ch_mobile1'		=> array('field'=>'other_ch_mobile1'),
	'other_ch_mobile2'	 	=> array('field'=>'other_ch_mobile2'),
	'family'				=> array('field'=>'family'),
	'neighbour'				=> array('field'=>'neighbour'),
	'rel_person'			=> array('field'=>'rel_person'),
	'other_ec'				=> array('field'=>'other_ec'),
	'AdditionalAddress'		=> array('field'=>'AdditionalAddress')
);
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
function EventInfoPTP( $code = "" ){
	$arr_info_ptp = array(101=> "LUNAS", 102=>"CICILAN");
	return ( isset($arr_info_ptp[$code]) ? $arr_info_ptp[$code] : "");
}

// EventUploadBatch 
function EventUploadBatch( $date = "" ){
	if( !is_null($date) ){
		return date('YmdHis', strtotime($date));
	}
	return "";
}

function StringFormat2($string = null) {
	$stringy = preg_replace("/[^A-Za-z0-9 ]/", '', $string);
	$strings = StringFormat($stringy);
	
	return $strings;
}

/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
// buang karakter aneh di telpon: 
function StringFormat($string = null){
	$data = @explode("\r\n", $string);
	$valstr = null;
	
	//percobaan 
	if(count($data)==1 ) {
		$valstr = (string)$string; 
	}
	else if( count($data)> 1){
		$values = array();
		foreach($data as $i => $val ){
			$val = trim($val);
			if(!strlen($val)) continue;
			else if(strlen($val)){
				$values[] = trim($val);
			}
		}
		// return :
		$string = @implode(" ", $values);
		$valstr = (string)$string;
	}
	
	// explode space 
	$array = explode("\t\r\s", $valstr);
	if( !count($array)) return null;
	
	$arrayStr = array(); 
	foreach($array as $key => $valdata ){
		$valdata = trim($valdata);
		if( strlen($valdata)>0){
			$arrayStr[] = (string)$valdata;
		}
	}
	// return data :
	// EventEscapeNewline($arrayStr)
	$strings = (string)implode(" ",$arrayStr);
	// return (string)implode(" ",$arrayStr);
	$strangs = EventEscapeNewline($strings);
	return $strangs;
	
	/*
	// test fixed bug removel line enter / 2 enter fuck :
	// coba aja ini bro ?
	$data = @explode("[\r\n\t\n\r\s'-:.+,;?=_/]/", $string);
	$values = array();
	foreach($data as $i => $val ){
		if( !strlen($val)){
			continue;
		}
		$values[] = trim($val);
	}
	
	$string = @implode(" ", $values);
	
	
	//new_line, Enter, tab, kutip 
	// $stringVal = str_replace(array("\t", "\n", "\n\r", "'"),  array(" ", " ", " ", ""), $string);
	// $stringVal = trim(preg_replace('/\s+/', ' ', $string));
	$text = preg_replace("/[\r\n\t\n\r\s'-:.+,;?=_/><!}{*&]+/", " ", $string);
	$stringVal = preg_replace('/[^A-Za-z0-9\-]/', ' ', $text);
	return (string)$stringVal;
	
	//return (string)$string;
	*/
	
}
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
// ambil hanya number 
function NumberFormat($string = null){
	// ambil hanya number : 
	// $stringVal = str_replace(array("\t", "\n", "\n\r", "'", " ", "-", ":", ".", "+"),  
							 // array("", "", "", "","", "","", "", ""), $string);
	// $stringVal = trim(preg_replace('/\s+/', ' ', $string));
	
	/* $text = preg_replace("/[\r\n\t\n\r\s'-:.+,;?=_/><!}{*&]+/", " ", $string);
	$stringVal = preg_replace('/[^A-Za-z0-9\-]/', ' ', $text);
	return (string)$stringVal; */
	
	return preg_replace("/[^0-9 ]/", '', $string);
}

/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
// EventEscapeNewline nggak usah di pake :
function EventEscapeNewline( $string = null ){
	$strin = preg_replace("/[\n\r\t]/","",$string);
	return preg_replace("/[^A-Za-z0-9,._\-\/\:\n\r\s]/", '', $strin);
}

/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
 
if(!function_exists('EventCallStatusCode') ) {
 function EventCallStatusCode( $Code = NULL )  {
	$UI =& get_instance();
	$UI->db->reset_select();
	$UI->db->select("a.CallReasonDesc as Code", FALSE);
	$UI->db->from("t_lk_account_status a");
	$UI->db->where("a.CallReasonCode", $Code);
	$qry = $UI->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
	
 }
 
}

/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
if(!function_exists('EventSpcDesc') ) {
 function EventSpcDesc( $Code = NULL )  {
	$UI =& get_instance();
	$UI->db->reset_select();
	$UI->db->select("a.spc_description as Code");
	$UI->db->from("t_lk_speach_with a");
	$UI->db->where("a.spc_code", $Code);
	$qry = $UI->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
 }
 
}

/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
if(!function_exists('EventUploadDate') ) {
 function EventUploadDate( $Code = NULL )  {
	$UI =& get_instance();
	$UI->db->reset_select();
	$UI->db->select("a.FTP_UploadDateTs as Code");
	$UI->db->from("t_gn_upload_report_ftp a");
	$UI->db->where("a.FTP_UploadId", $Code);
	$qry = $UI->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
	
 }
 
}


/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
if(!function_exists('EventIsRPC') )  {
	function EventIsRPC( $code  = 0 ) {
		return ( $code ? "YES" : "NO");
	}
}	
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
 if(!function_exists('CallEvent') ) {
	 function CallEvent( $key = null, $value=null )  {
		// jika ada field yang belum di definisikan silahkan tambahkan sendiri : 
		$resultArray = array( 
		
		// string format :
			'accstatus' 		=> 'EventCallStatusCode',  
			'prevaccstatus' 	=> 'EventCallStatusCode', 
			'isrpc' 			=> 'EventIsRPC',
			'spc_with' 			=> 'EventSpcDesc',
			'infoptp' 			=> 'EventInfoPTP',
			'uploadbatch' 		=> 'EventUploadBatch',
			'note'				=> 'StringFormat2',
			'Notes' 			=> 'StringFormat2',
			'deb_reminder' 		=> 'StringFormat2',
			'cardno'			=> 'StringFormat2',
			'custno'			=> 'StringFormat2',
			'accountno'			=> 'StringFormat2',
			'idno'				=> 'StringFormat2',
			'fullname'			=> 'StringFormat2',
			'deb_name'			=> 'StringFormat2',
			'mothername'		=> 'StringFormat2',
			
			'homeaddress'		=> 'StringFormat2',
			'billingaddress'	=> 'StringFormat2',
			'officeaddress'		=> 'StringFormat2',
			'deb_add_hm'		=> 'StringFormat2',
			'deb_billing_add'	=> 'StringFormat2',
			'deb_add_off'		=> 'StringFormat2',
			
			'otheraddress'		=> 'StringFormat2',
			'otheraddress2'		=> 'StringFormat2',
			'ecname'			=> 'StringFormat2',
			'ecaddress'			=> 'StringFormat2',
			'perm_message'		=> 'StringFormat2',
			'reminder'			=> 'StringFormat2',
			'RPC_DESC'			=> 'StringFormat2',
			//number only :
			'homephone'			=> 'NumberFormat',
			'homephone2'		=> 'NumberFormat',
			'mobilephone'		=> 'NumberFormat',
			'mobilephone2'		=> 'NumberFormat',
			'officephone'		=> 'NumberFormat',
			'officefax'			=> 'NumberFormat',
			'ecphone'			=> 'NumberFormat',
			'ecmobile'			=> 'NumberFormat',
			'addhomephone'		=> 'NumberFormat',
			'addhomephone2'		=> 'NumberFormat',
			'addmobilephone'	=> 'NumberFormat',
			'addmobilephone2'	=> 'NumberFormat',
			'addofficephone'	=> 'NumberFormat',
			'addofficephone2'	=> 'NumberFormat',
			
			// new line 
			'deb_cardno'		=>	'StringFormat2',
			'deb_acct_no'		=>	'StringFormat2',
			'deb_acct_no'		=>	'StringFormat2',
			'deb_id_number'		=>	'StringFormat2',
			'deb_no_card'		=>	'StringFormat2',
			'deb_name'			=>	'StringFormat2',
			'deb_mother_name'	=>	'StringFormat2',
			'deb_dob'			=>	'StringFormat2',
			'deb_add_hm'		=>	'StringFormat2',
			'deb_billing_add'	=>	'StringFormat2',
			'deb_add_off'		=>	'StringFormat2',
			'deb_zip_h'			=>	'StringFormat2',
			'deb_home_no1'		=>	'StringFormat2',
			'deb_mobileno1'		=>	'StringFormat2',
			'deb_office_ph1'	=>	'StringFormat2',
			'deb_ec_name_f'		=>	'StringFormat2',
			'deb_ec_add_f'		=>	'StringFormat2',
			'deb_ec_phone_f'	=>	'StringFormat2',
			'deb_ec_mobile_f'	=>	'StringFormat2',
			'deb_region'		=>	'StringFormat2',
			'deb_open_date'		=>	'StringFormat2',
			'deb_resource'		=>	'StringFormat2',
			// 'deb_limit'			=>	'StringFormat2',
			'deb_fees'			=>	'StringFormat2',
			'deb_interest'		=>	'StringFormat2',
			// 'deb_principal'		=>	'StringFormat2',
			'deb_last_paydate'	=>	'StringFormat2',
			// 'deb_last_pay'		=>	'StringFormat2',
			'deb_bp'			=>	'StringFormat2',
			'ptpcounts'			=>	'StringFormat2',
			'deb_b_d'			=>	'StringFormat2',
			// 'deb_wo_amount'		=>	'StringFormat2',
			'deb_update_by_user'=>	'StringFormat2',
			'deb_perm_msg'		=>	'StringFormat2',
			'deb_perm_msg_date'	=>	'StringFormat2',
			'deb_call_status_code'		=> 'StringFormat2',
			'CallReasonDesc'			=> 'StringFormat2',
			'deb_prev_call_status_code'	=> 'StringFormat2',
			'prevac'			=> 'StringFormat2',
			'deb_agent'			=> 'StringFormat2',
			'deb_reminder'		=> 'StringFormat2',
			'deb_swap_count'	=> 'StringFormat2',
			'deb_sms_count'		=> 'StringFormat2',
			'deb_contact_type'	=> 'StringFormat2',
			'deb_rpc'			=> 'StringFormat2',
			'deb_rpc`'			=> 'StringFormat2',
			'deb_spc`'			=> 'StringFormat2',
			'deb_is_kept'		=> 'StringFormat2',
			// 'deb_afterpay'		=> 'StringFormat2',
			'deb_is_lock'		=> 'StringFormat2',
			// 'deb_bal_afterpay'	=> 'StringFormat2',
			// 'deb_pri_afterpay'	=> 'StringFormat2',
			'deb_last_swap_ts'	=> 'StringFormat2',
			'deb_call_activity_datets'	=> 'StringFormat2',
			
			// 'addhomephone'		=> 
			// 'addhomephone2'		=> 
			// 'addmobilephone'	=> 
			// 'addmobilephone2'	=> 
			// 'addofficephone'	=> 
			// 'addofficephone2'	=> 
			'other_agent'		=> 'StringFormat2',
			'other_card_number'	=> 'StringFormat2',
			'other_accstatus'	=> 'StringFormat2',
			'addfax'			=> 'StringFormat2',
			'addfax2'			=> 'StringFormat2',
			// 'uploadbatch'		=> 'StringFormat2',
			'deb_ssv_no'		=> 'StringFormat2',
			'dlq'				=> 'StringFormat2',
			'flag'				=> 'StringFormat2',
			'accstatus_name'	=> 'StringFormat2',
			'SPC_DESC'			=> 'StringFormat2',
			'RPC_DESC'			=> 'StringFormat2',
			// 'infoptp'			=> 'StringFormat2',
			'acc_mapping'		=> 'StringFormat2',
			'attempt_call'		=> 'StringFormat2',
			'note'				=> 'StringFormat2',
			'ptpDate'			=> 'StringFormat2',
			// 'ptpAmount'			=> 'StringFormat2',
			'ptpChannel'		=> 'StringFormat2',
			'ptpTenor'			=> 'StringFormat2',
			// 'ptp_discount'		=> 'StringFormat2',
			'other_ch_office'	=> 'StringFormat2',
			'other_ch_home'		=> 'StringFormat2',
			'other_ch_mobile1'	=> 'StringFormat2',
			'other_ch_mobile2'	=> 'StringFormat2',
			'family'			=> 'StringFormat2',
			'neighbour'			=> 'StringFormat2',
			'rel_person'		=> 'StringFormat2',
			'other_ec'			=> 'StringFormat2',
			'bucket_agent'		=> 'StringFormat2',
			'AdditionalAddress'	=> 'StringFormat2'
			);
			
		
		// jika process ini tidak terdefinis :
		$rootMaps = array_keys($resultArray);
		if(!in_array($key,$rootMaps)){
			return $value;
		} 
		// if have on define :
		else if(in_array($key,$rootMaps)){
			$function = (isset($resultArray[$key]) ? $resultArray[$key] : null );
			if(function_exists($function)){
				return @call_user_func_array($function, array($value));
			}
			// no have function :
			return $value;
		}
		// default of return :
		return (string)$value;	
	}
 }
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */ 
 
 function HapusKarakterAneh( $string = null ){
	 if( is_null($string) ){
		 return '';
	 }
	return preg_replace("/[^A-Za-z0-9,._\-\/\:]/", '', $string);
 }
 
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
 $file_name = $campaign."-alldata-".date('Ymd')."-".date('His').".xls";
 $BASE_DIR_NAME = dirname (__FILE__);
 $BASE_EXCEL_FILE_NAME = "${BASE_DIR_NAME}/${file_name}";

 $workbook =& new writeexcel_workbookbig($BASE_EXCEL_FILE_NAME);
 $worksheet =& $workbook->addworksheet();
		
/* pack header format every file **/
	 
 $header_format =& $workbook->addformat();
 $header_format ->set_bold();
 $header_format->set_size(10);
 $header_format->set_color('white');
 $header_format->set_align('left');
 $header_format->set_align('vcenter');
 $header_format->set_pattern();
 $header_format->set_fg_color('black');
 $content_fmt   = $workbook->addformat();
 $content_fmt->set_text_wrap();
 
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
 $num = 0; // default num from 0 to (n);
 
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */

 $ar_header = $set_rows_headers; //( isset( $result[0] ) ? $result[0] : array() );
 $col_head = 0;
 if(is_array($ar_header )) 
 foreach($ar_header as $label => $name) {
	$worksheet->write_string($num, $col_head, $label, $header_format );
	$col_head++; 	
 }

 $num = $num+1; 
 
/**
 * @ object		create header name of file to process 
 * @ package 	 view
 * @ params 	 Content line write
 */
 $UI =& get_instance();
 $_conds = array();
 
$cmp_in=''; 
if(is_array($CampaignId) 
	 &&(count($CampaignId)> 0)) {
		$cmp_in = $CampaignId[0];
 }
 
function get_deblist( $UI, $cmp_in ) {
	// $sql = " select deb_id from t_gn_debitur a inner join t_gn_assignment g on a.deb_id = g.CustomerId ";
	$UI=$UI;
	$UI->db->reset_select();
	$UI->db->select("deb_id");
	$UI->db->from("t_gn_debitur a");
	$UI->db->join("t_gn_assignment b","a.deb_id=b.CustomerId", "INNER");
	
	// check campaign ID :
	 if( $cmp_in ) {
		$UI->db->where("a.deb_cmpaign_id",$cmp_in);
	 }
	 
	$counter=0;
	$line=array();
	$data = array();
	
	 $qry = $UI->db->get(); 
	 if($qry &&($qry->num_rows()> 0)) 
	 foreach($qry->result_assoc() as $value) {
		$counter++;
		array_push( $line , $value["deb_id"] );
		if( ($counter % LINES) == 0 ) { // sudah mencapai 25
			$data[] = $line; // remove koma di awal string
			$line = array();
		}
	 }
	 
	 if($counter % LINES != 0){
		 $data[] = $line;
	 }
	return $data;
}   

$line_ar = get_deblist($UI, $cmp_in);
 // echo"<pre>";print_r($line_ar);echo"</pre>"; exit();
$n=$num;
$w=$worksheet;
$cn=0;

// echo count($line_ar);
// write data per 5 row
foreach($line_ar as $linear) {
	// if($cn>0 && $cn <= 3137){
		$r = raw_data_perline($linear, $n, $w, $UI, $set_rows_headers, $cmp_in);
	// $r = raw_data_perline($line_ar[759], $n, $w, $UI, $set_rows_headers, $cmp_in);
		$n = $r['n'];
		$w = $r['w'];
	// }
	$cn++;
	// if($cn==760)
		// break;
}
 
function raw_data_perline($line=array(), $num, $work, $UI, $set_rows_headers, $cmp_in) {
	$UI = $UI;
	 $UI->db->reset_select();
	 $UI->db->select("
		a.deb_id,
		a.deb_cardno,
		a.deb_acct_no,
		a.deb_acct_no,
		a.deb_id_number,
		a.deb_no_card,
		a.deb_name,
		a.deb_mother_name,
		a.deb_dob,
		a.deb_add_hm,
		a.deb_billing_add,
		a.deb_add_off,
		a.deb_zip_h,
		a.deb_home_no1,
		a.deb_mobileno1,
		a.deb_office_ph1,
		a.deb_ec_name_f,
		a.deb_ec_add_f,
		a.deb_ec_phone_f,
		a.deb_ec_mobile_f,
		a.deb_region,
		a.deb_open_date,
		a.deb_resource,
		a.deb_limit,
		a.deb_fees,
		a.deb_interest,
		a.deb_principal,
		a.deb_last_paydate,
		a.deb_last_pay,
		a.deb_bp,
		(select count(ptps.ptp_id) from t_tx_ptp ptps where ptps.deb_id = a.deb_id group by ptps.deb_id) as ptpcounts,
		a.deb_b_d,
		a.deb_wo_amount,
		a.deb_update_by_user,
		a.deb_perm_msg,
		a.deb_perm_msg_date,
		a.deb_call_status_code,
		b.CallReasonDesc,
		a.deb_prev_call_status_code,
		(select az.CallReasonDesc from t_lk_account_status az where az.CallReasonCode = a.deb_prev_call_status_code) as prevac,
		a.deb_agent,
		a.deb_reminder,
		a.deb_swap_count,
		a.deb_sms_count,
		a.deb_contact_type,
		a.deb_rpc,
		(select spc.rpc_name from t_lk_remote_place spc where spc.rpc_code = a.deb_rpc) as `deb_rpc`,
		(select spc.spc_name from t_lk_speach_with spc where spc.spc_code = a.deb_spc) as `deb_spc`,
		a.deb_is_kept,
		a.deb_afterpay,
		a.deb_is_lock,
		a.deb_bal_afterpay,
		a.deb_pri_afterpay,
		a.deb_last_swap_ts,
		a.deb_call_activity_datets,
		a.addhomephone as addhomephone,
		a.addhomephone2 as addhomephone2,
		a.addmobilephone as addmobilephone,
		a.addmobilephone2 as addmobilephone2,
		a.addofficephone as addofficephone,
		a.addofficephone2 as addofficephone2,
		a.other_agent as other_agent,
		a.other_card_number as other_card_number,
		a.other_accstatus as other_accstatus,
		a.addfax as addfax,
		a.addfax2 as addfax2,
		a.deb_created_ts as uploadbatch,
		a.deb_ssv_no as deb_ssv_no,
		'0' as dlq,
		'1' as flag,
		b.CallReasonDesc as accstatus_name, 
		d.spc_description as SPC_DESC, 
		e.rpc_description as RPC_DESC, 
		IF((select ptp.ptp_type from t_tx_ptp ptp where ptp.ptp_account = a.deb_acct_no and ptp.ptp_type<>0 limit 0,1) IS NULL,a.infoptp,NULL) as infoptp,
		IF(a.acc_mapping<>'', a.acc_mapping, (select group_concat(am.Card) from t_gn_account_mapping am where am.CardNo=a.deb_acct_no)) as acc_mapping,
		i.deb_attempt as attempt_call,
		j.CallHistoryNotes as note,
		(select ptp.ptp_date from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpDate,
		(select ptp.ptp_amount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpAmount,
		(select pych.pch_name from t_tx_ptp ptp
			left join t_lk_payment_channel pych ON ptp.ptp_chanel = pych.pch_code
			where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpChannel,
		(select ptp.ptp_tenor from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpTenor,
		(select ptp.ptp_discount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptp_discount,
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=103) as other_ch_office, 
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=101) as other_ch_home, 
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=102) as other_ch_mobile1, 
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=109) as other_ch_mobile2, 
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=104) as family, 
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=105) as neighbour,
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=107) as rel_person, 
		(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id  AND c.addphone_type=108) as other_ec,
		h.code_user as bucket_agent,
		CONCAT(adrs.ALAMAT,', RT. ',
								adrs.NO_RT,' RW. ',
								adrs.NO_RW,', KEL.',
								adrs.KEL_NAME,', KEC. ',
								adrs.KEC_NAME,', KAB. ',
								adrs.KAB_NAME,', PROV. ',
								adrs.PROP_NAME) AS AdditionalAddress", FALSE);
		
	 $UI->db->from("t_gn_debitur a");
	 $UI->db->join("t_lk_account_status b ","a.deb_call_status_code=b.CallReasonCode", "LEFT");
	 $UI->db->join("t_lk_speach_with d "," a.deb_spc=d.spc_code", "LEFT");
	 $UI->db->join("t_lk_remote_place e "," a.deb_rpc=e.rpc_code","LEFT");
	 $UI->db->join("t_gn_campaign f", "a.deb_cmpaign_id=f.CampaignId", "LEFT");
	 $UI->db->join("t_gn_assignment g", "a.deb_id = g.CustomerId", "INNER");
	 $UI->db->join("t_tx_agent h", "g.AssignSelerId = h.UserId", "LEFT");
	 $UI->db->join("call_history_min i", "a.deb_id = i.deb_id", "LEFT");
	 $UI->db->join("t_gn_callhistory j", "i.Last_CallhistoryId=j.CallHistoryId", "LEFT");
	 $UI->db->join("t_gn_debitur_additional_address adrs", "a.deb_acct_no=adrs.Accno", "LEFT");
	 
	 
	 // check campaign ID :
	 if( $cmp_in ) {
		$UI->db->where("a.deb_cmpaign_id",$cmp_in);
	 }
	 if($line) {
		$UI->db->where_in("a.deb_id",$line);
	 }
	 
	 
	 $qry = $UI->db->get(); 
	 $hed = array_keys($set_rows_headers);
	 
	 $num = $num;
	 $no = 1;
	 
	//$rowset = array();
	 if($qry &&($qry->num_rows()> 0)) 
	 foreach($qry->result_assoc() as $key => $value) {
		// print_r($no++);

		
		$val = array();
		$mSource = $set_rows_headers;
		
		// then check validation of array:
		if(is_array($hed))
		foreach($hed as $i => $vals){
			$mArray  = $mSource[$vals];
			if(!is_null($mArray['field'])){	
				$field = $mArray['field'];
				// formating data before save to excel :
				$valueData = CallEvent($field, $value[$field]);
				$work->write_string($num, $i, $valueData);
				
		
			} 
		 }
						// print_r( $value[$field] );
			// die();
		 $num++;
	 }
	 
	 return array("n"=>$num, "w"=>$work);
	 
} 


/* 

 
// process query dat to database : 
 $UI->db->reset_select();
 $UI->db->select("
	a.deb_id,
	a.deb_cardno,
	a.deb_acct_no,
	a.deb_acct_no,
	a.deb_id_number,
	a.deb_no_card,
	a.deb_name,
	a.deb_mother_name,
	a.deb_dob,
	a.deb_add_hm,
	a.deb_billing_add,
	a.deb_add_off,
	a.deb_zip_h,
	a.deb_home_no1,
	a.deb_mobileno1,
	a.deb_office_ph1,
	a.deb_ec_name_f,
	a.deb_ec_add_f,
	a.deb_ec_phone_f,
	a.deb_ec_mobile_f,
	a.deb_region,
	a.deb_open_date,
	a.deb_resource,
	a.deb_limit,
	a.deb_fees,
	a.deb_interest,
	a.deb_principal,
	a.deb_last_paydate,
	a.deb_last_pay,
	a.deb_bp,
	(select count(ptps.ptp_id) from t_tx_ptp ptps where ptps.deb_id = a.deb_id group by ptps.deb_id) as ptpcounts,
	a.deb_b_d,
	a.deb_wo_amount,
	a.deb_update_by_user,
	a.deb_perm_msg,
	a.deb_perm_msg_date,
	a.deb_call_status_code,
	b.CallReasonDesc,
	a.deb_prev_call_status_code,
	(select az.CallReasonDesc from t_lk_account_status az where az.CallReasonCode = a.deb_prev_call_status_code) as prevac,
	a.deb_agent,
	a.deb_reminder,
	a.deb_swap_count,
	a.deb_sms_count,
	a.deb_contact_type,
	a.deb_rpc,
	(select spc.rpc_name from t_lk_remote_place spc where spc.rpc_code = a.deb_rpc) as `deb_rpc`,
	(select spc.spc_name from t_lk_speach_with spc where spc.spc_code = a.deb_spc) as `deb_spc`,
	a.deb_is_kept,
	a.deb_afterpay,
	a.deb_is_lock,
	a.deb_bal_afterpay,
	a.deb_pri_afterpay,
	a.deb_last_swap_ts,
	a.deb_call_activity_datets,
	a.addhomephone as addhomephone,
	a.addhomephone2 as addhomephone2,
	a.addmobilephone as addmobilephone,
	a.addmobilephone2 as addmobilephone2,
	a.addofficephone as addofficephone,
	a.addofficephone2 as addofficephone2,
	a.other_agent as other_agent,
	a.other_card_number as other_card_number,
	a.other_accstatus as other_accstatus,
	a.addfax as addfax,
	a.addfax2 as addfax2,
	a.deb_created_ts as uploadbatch,
	a.deb_ssv_no as deb_ssv_no,
	'0' as dlq,
	'1' as flag,
	b.CallReasonDesc as accstatus_name, 
	d.spc_description as SPC_DESC, 
	e.rpc_description as RPC_DESC, 
	IF((select ptp.ptp_type from t_tx_ptp ptp where ptp.ptp_account = a.deb_acct_no and ptp.ptp_type<>0 limit 0,1) IS NULL,a.infoptp,NULL) as infoptp,
	IF(a.acc_mapping<>'', a.acc_mapping, (select group_concat(am.Card) from t_gn_account_mapping am where am.CardNo=a.deb_acct_no)) as acc_mapping,
	i.deb_attempt as attempt_call,
	j.CallHistoryNotes as note,
	(select ptp.ptp_date from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpDate,
	(select ptp.ptp_amount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpAmount,
	(select pych.pch_name from t_tx_ptp ptp
		left join t_lk_payment_channel pych ON ptp.ptp_chanel = pych.pch_code
		where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpChannel,
	(select ptp.ptp_tenor from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpTenor,
	(select ptp.ptp_discount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptp_discount,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=103) as other_ch_office, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=101) as other_ch_home, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=102) as other_ch_mobile1, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=109) as other_ch_mobile2, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=104) as family, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=105) as neighbour,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=107) as rel_person, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id  AND c.addphone_type=108) as other_ec,
	h.code_user as bucket_agent", FALSE);
	
 $UI->db->from("t_gn_debitur a");
 $UI->db->join("t_lk_account_status b ","a.deb_call_status_code=b.CallReasonCode", "LEFT");
 $UI->db->join("t_lk_speach_with d "," a.deb_spc=d.spc_code", "LEFT");
 $UI->db->join("t_lk_remote_place e "," a.deb_rpc=e.rpc_code","LEFT");
 $UI->db->join("t_gn_campaign f", "a.deb_cmpaign_id=f.CampaignId", "LEFT");
 $UI->db->join("t_gn_assignment g", "a.deb_id = g.CustomerId", "INNER");
 $UI->db->join("t_tx_agent h", "g.AssignSelerId = h.UserId", "LEFT");
 $UI->db->join("call_history_min i", "a.deb_id = i.deb_id", "LEFT");
 $UI->db->join("t_gn_callhistory j", "i.Last_CallhistoryId=j.CallHistoryId", "LEFT");
 
 
 // check campaign ID :
 if(is_array($CampaignId) 
 &&(count($CampaignId)> 0)) {
	$UI->db->where_in("a.deb_cmpaign_id",array_map('intval', $CampaignId));
 }
 
 // $UI->db->limit(18000);
 // use limit for test 
 //$UI->db->limit(1000);
 
 // value of debug :
 // echo $this->db->_get_var_dump();
 // exit();

 $qry = $UI->db->get(); 
 $hed = array_keys($set_rows_headers);
 $num = $num;
 $no = 1;
 
//$rowset = array();
 if($qry &&($qry->num_rows()> 0)) 
 foreach($qry->result_assoc() as $key => $value) {
	// print_r($no++);
	$val = array();
	$mSource = $set_rows_headers;
	
	// then check validation of array:
	if(is_array($hed))
	foreach($hed as $i => $vals){
		$mArray  = $mSource[$vals];
		if(!is_null($mArray['field'])){	
			$field = $mArray['field'];
			// formating data before save to excel :
			$valueData = CallEvent($field, $value[$field]);
			$worksheet->write_string($num, $i, $valueData);
		} 
	 }
	 $num++;
 }
 */
 
 
// download excel:
$workbook->close(); // end book 
if(file_exists($BASE_EXCEL_FILE_NAME)) {
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel; charset=utf-16");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	
?>