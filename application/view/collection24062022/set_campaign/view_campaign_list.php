
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;#</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No</th>   
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CampaignCode');" title="Order ASC/DESC">Campaign Code</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CampaignId');" title="Order ASC/DESC">Campaign Number</span></th>
        <th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CampaignName');" title="Order ASC/DESC">Campaign Name</span></th>
        <th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CampaignDesc');" title="Order ASC/DESC">Campaign Desc</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CampaignDesc');" title="Order ASC/DESC">Work Project</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('f.Description');" title="Order ASC/DESC">Call Type</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('a.CampaignEndDate');" title="Order ASC/DESC">Expiration Date</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Data Size</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;Status Active</th>
	</tr>
</thead>	
<tbody>
<?php
$no  = $num;
foreach( $page -> result_assoc() as $rows )
{ 
 ?>
	<tr class="onselect">
		<td class="content-first"><input type="checkbox" name="check_list_cmp" id="check_list_cmp" value="<?php echo $rows['CampaignId']; ?>"></td>
		<td class="content-middle center"><?php echo $no; ?></td>
		<td class="content-middle"><b><?php echo (isset($rows['CampaignCode'])?$rows['CampaignCode']:''); ?></b></td>
		<td class="content-middle"><?php echo (isset($rows['CampaignNumber'])?$rows['CampaignNumber']:''); ?></td>
		<td class="content-middle"><?php echo (isset($rows['CampaignName'])?$rows['CampaignName']:'-'); ?></td>
		<td class="content-middle"><?php echo (isset($rows['CampaignDesc'])?$rows['CampaignDesc']:'-'); ?></td>
		<td class="content-middle"><?php echo (isset($rows['ProjectName'])?$rows['ProjectName']:'-'); ?></td>
		<td class="content-middle"><?php echo (isset($rows['CallTypeName'])?$rows['CallTypeName']:'-'); ?></td>
		<td class="content-middle center"><?php echo $this -> EUI_Tools -> _date_indonesia((isset($rows['CampaignEndDate'])?$rows['CampaignEndDate']:'')); ?></td>
		<td class="content-lasted center" style="text-align:right;padding-right:8px;"><?php echo ($rows['DataSize'] ? $rows['DataSize'] : 0 ); ?></td>
		<td class="content-lasted center"><?php echo $rows['CmpStatus']; ?></td>
	</tr>			
</tbody>
<?php
		$no++;
  };
?>
</table>