<table cellspacing=1 width='99%'>
<thead>
	<tr height="27">
		<th class="ui-corner-top ui-state-default first" width='2%'>No</th>
		<th class="ui-corner-top ui-state-default middle left" width='8%'>Destination</th>
		<th class="ui-corner-top ui-state-default middle left" width='40%'>Message</th>
		<th class="ui-corner-top ui-state-default middle left" >&nbsp;User Create</th>
		<th class="ui-corner-top ui-state-default middle center" >&nbsp;Create Date</th>
		<th class="ui-corner-top ui-state-default middle" >Send Date</th>
		<th class="ui-corner-top ui-state-default lasted left" >Sent Status</th>
	</tr>
</thead>
	
<tbody>	
<?php
if( is_array($content_pages) ) { 
 $no = $start_page+1;
 foreach( $content_pages as $num => $rows )
{
 
// @ pack : of list color 

 $back_color = ( $num%2!=0 ? '#FFFFFF' :'FFFEEE');
 $call_date = date('d-m-Y H:i:s', strtotime($rows[CallHistoryCreatedTs]));

// @ pack : of content table 

 echo " <tr bgcolor='{$back_color}' class='onselect'>
		<td class='content-first' nowrap>{$no}</td>	
		<td class='content-middle' nowrap>&nbsp;". _getPhoneNumber($rows['Recipient'])."</td>
		<td class='content-middle'><div class='text-content left-text'>". $rows['Message'] ."</div></td>
		<td class='content-middle' nowrap>&nbsp;". $rows['full_name']  ."</td>
		<td class='content-middle center' nowrap>&nbsp;". _getDateTime($rows['RequestTs']) ."</td>
		<td class='content-middle center'>&nbsp;". _getDateTime($rows['SentDate']) ."</td>
		<td class='content-middle left'>&nbsp;". $rows['StatusDesc'] ."</td>
		</tr>";
	$no++;	
 }	
 
}

/* @ pack : -------------------------------------------------------
 * @ pack : # get list off page #----------------------------------
 * @ pack : -------------------------------------------------------
 */

 $max_page = 10;
 
// @ pack : start html  

 $_li_create = " <div class='page-web-voice' style='margin-left:-5px;margin-top:2px;border-top:0px solid #FFFEEE;'><ul>";
 
// @ pack : list 
 
 $start =(int)(!$select_pages ? 1: ((($select_pages%$max_page ==0) ? ($select_pages/$max_page) : intval($select_pages/$max_page)+1)-1)*$max_page+1);
 $end   =(int)((($start+$max_page-1)<=$total_pages) ? ($start+$max_page-1) : $total_pages );
	
// @ pack : like here 

 if( $select_pages > 1) 
 {
	$post = (int)(($select_pages)-1);
	
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsOutbox('1');\" ><a href=\"javascript:void(0);\">&lt;&lt;</a></li>";
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsOutbox('$post');\" ><a href=\"javascript:void(0);\">&lt;</a></li>";
 }

// @ pack : check its 

 if($start>$max_page){
	$_li_create.="<li cclass=\"page-web-voice-normal\"  onclick=\"Ext.DOM.SmsOutbox('" .($start-1) . "');\" ><a href=\"javascript:void(0);\">...</a></li>";
 }

// @ pack : check its 
 
 for( $p = $start; $p<=$end; $p++)
 { 
	if( $p == $select_pages ){ 
		$_li_create .="<li class=\"page-web-voice-current\" id=\"{$selpages[$p]}\" onclick=\"Ext.DOM.SmsOutbox('{$p}');\"> <a href=\"javascript:void(0);\">{$p}</a></li>";
	 } else {
		$_li_create .=" <li class=\"page-web-voice-normal\" id=\"{{$selpages[$p]}}\" onclick=\"Ext.DOM.SmsOutbox('{$p}');\"><a href=\"javascript:void(0);\">{$p}</a></li>";
	}
 }

// @ pack : check its 
  
 if($end<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsOutbox('".($end+1) ."');\"><a href=\"javascript:void(0);\" >...</a></li>";
 }

// @ pack : check its 
 
 if($select_pages<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsOutbox('". ($select_pages+1) ."');\"><a href=\"javascript:void(0);\" title=\"Next page\">&gt;</a></li>";
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsOutbox('". ($total_pages)."');\"><a href=\"javascript:void(0);\" title=\"Last page\">&gt;&gt;</a></li>";
 }
		
// @ pack : check its 
	
 $_li_create .="</ul></div>";
 
 if( $total_pages > 1){	
	 echo "<tr>
			<td colspan='4'>{$_li_create}</td>
			<td style='color:red;'>Record (s)&nbsp;: <span class='input_text' style='padding:2px;'>{$total_records}&nbsp;</span></td>
		 </tr>	";
 }	 
	
?>
</tbody>
</table>