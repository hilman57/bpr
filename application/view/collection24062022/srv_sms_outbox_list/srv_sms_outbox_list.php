<?php 
if(!function_exists('_setCutWord') ){
	function _setCutWord($message = "" ) {
		return "<p style='line-height:20px;text-align:justify;'>". wordwrap($message, 75, "<br>") ."</p>";
	}
}
// @ pack :  set align #------------------------------

 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');

// @ pack :  set align #------------------------------

 $labels =& page_labels();
 $primary =& page_primary();

// @ pack :  set align #------------------------------

 page_set_align('CustomerNumber', 'left');
 page_set_align('CustomerName', 'left');
 page_set_align('Message', 'left');
 page_set_align('PhNumber', 'left');
 page_set_align('SentDate', 'center');
 page_set_align('SendDate', 'center');
 

// @ pack : set currency column in here 

 $call_user_func = array(
			'SentDate' =>'_getDateTime',
			'PhNumber' => '_getPhoneNumber',
			'SentDate' => '_getDateTime',
			'SendDate' => '_getDateTime',
			'Message'  => '_setCutWord');
			
 $call_link_user = array('CustomerName','CustomerNumber');
 

// @ pack : color 
 
 $nowrap = array('SentDate'=>'nowrap','StatusDesc'=>'nowrap','SendDate' => 'nowrap');
 $align =& page_get_align();
 
?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); if($Field != 'CallReasonId') { ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php  } endforeach;  ?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows ) { 
 $color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); ?>
 <tr class="onselect" bgcolor="<?php echo $color; ?>">
	<td class="content-first center"> <?php __($no);?></td>
	<?php foreach( $labels as $Field => $LabelName ) : $color =& page_column($Field);    ?>
	<td class="content-middle" <?php __($color) ?>  align="<?php __($align[$Field]); ?>" <?php __($nowrap[$Field]);?>>
		<?php if( in_array( $Field, $call_link_user ) ) : ?>
		<a href="javascript:void(0);" style="color:#289c05;text-decoration:none;" onclick="Ext.DOM.CallCustomer('<?php __($rows[$primary]); ?>');">  <?php __($rows[$Field]); ?></a>
		<?php else : ?>
		<?php __(page_call_function($Field, $rows, $call_user_func) ); ?>
		<?php endif; ?>	
	</td>
	<?php endforeach;?>
 </tr>	
</tbody>
<?php
	$no++;
}

?>
</table>


