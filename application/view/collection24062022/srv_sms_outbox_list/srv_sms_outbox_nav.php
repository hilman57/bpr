<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	outbox_campaign_id	: "<?php echo _get_exist_session('outbox_campaign_id');?>",
	outbox_cust_id		: "<?php echo _get_exist_session('outbox_cust_id');?>",
	outbox_cust_name	: "<?php echo _get_exist_session('outbox_cust_name');?>",
	outbox_phone_num	: "<?php echo _get_exist_session('outbox_phone_num');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/SMSOutbox/index/',
	custlist : Ext.DOM.INDEX+'/SMSOutbox/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('sms_outbox').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('sms_outbox').Clear();
	Ext.DOM.searchCustomer();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!=0) 
 {	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/SMSOutbox/index/', 
		}
  });
 } else { 
	Ext.Msg("Unknown Customers Selected !").Info(); 
 }
 
}

// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear']],
		extMenu  : [['searchCustomer'],['resetSeacrh']],
		extIcon  : [['zoom.png'],['zoom_out.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		changeYear:true,
		changeMonth:true,
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>
<style>input.input_text{border-radius:3px;}select.select{border-radius:3px;}</style>
<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="sms_outbox">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('outbox_campaign_id','select auto', $CampaignId, _get_exist_session('outbox_campaign_id'));?></td>
			
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('outbox_cust_id','input_text long', _get_exist_session('outbox_cust_id'));?></td>
			
			<td class="text_caption bottom"> # Send Date&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('outbox_start_sentdate','input_text date', _get_exist_session('outbox_start_sentdate'));?>
				<?php echo form()->input('outbox_end_sentdate','input_text date', _get_exist_session('outbox_end_sentdate'));?>
			</td>
			
		</tr>
		<tr>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('outbox_cust_name','input_text long', _get_exist_session('outbox_cust_name'));?></td>
			
			<td class="text_caption bottom"> # Phone Number&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('outbox_phone_num','input_text long', _get_exist_session('outbox_phone_num'));?></td>
			
			<td class="text_caption bottom"> # SMS Status&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('outbox_sms_status','select long',array( 2=> 'SENDING',3=>'PENDING', 4=>'FAILED'), _get_exist_session('outbox_sms_status'));?></td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->