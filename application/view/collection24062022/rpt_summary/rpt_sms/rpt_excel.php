<!DOCTYPE html>
<html>
<head>
	<style>
		html {
			font-family: Trebuchet MS,Arial,sans-serif;
			font-size: 12px;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td {
			padding: 5px;
		}
		#color {
			background-color : #00FFFF;
		}
	</style>
<title>Summary Report SMS BLAST</title>
</head>
<body>
	<h1>SUMMARY REPORT SMS BLAST</h1>
	<!-- <h4>START DATE : <?php //echo //$_REQUEST['start_date']; ?></h4>
	<h4>END DATA   : <?php //echo //$_REQUEST['end_date']; ?></h4> -->
	<?php 
		$sql = "
			SELECT
				count(if(so.TemplateId = 13 and so.SmsStatus = 2, so.TemplateId, null)) as wo5_success,
				count(if(so.TemplateId = 13 and so.SmsStatus = 3, so.TemplateId, null)) as wo5_queueu,
				count(if(so.TemplateId = 13 and so.SmsStatus = 4, so.TemplateId, null)) as wo5_failed,
				count(if(so.TemplateId = 13 and so.SmsStatus = 1, so.TemplateId, null)) as wo5_others,
				count(if(so.TemplateId = 15 and so.SmsStatus = 2, so.TemplateId, null)) as wo20_success,
				count(if(so.TemplateId = 15 and so.SmsStatus = 3, so.TemplateId, null)) as wo20_queueu,
				count(if(so.TemplateId = 15 and so.SmsStatus = 4, so.TemplateId, null)) as wo20_failed,
				count(if(so.TemplateId = 15 and so.SmsStatus = 1, so.TemplateId, null)) as wo20_others,
				count(if(so.TemplateId = 14 and so.SmsStatus = 2, so.TemplateId, null)) as wo30_success,
				count(if(so.TemplateId = 14 and so.SmsStatus = 3, so.TemplateId, null)) as wo30_queueu,
				count(if(so.TemplateId = 14 and so.SmsStatus = 4, so.TemplateId, null)) as wo30_failed,
				count(if(so.TemplateId = 14 and so.SmsStatus = 1, so.TemplateId, null)) as wo30_others,
				count(if(so.TemplateId = 22 and so.SmsStatus = 2, so.TemplateId, null)) as wo40_success,
				count(if(so.TemplateId = 22 and so.SmsStatus = 3, so.TemplateId, null)) as wo40_queueu,
				count(if(so.TemplateId = 22 and so.SmsStatus = 4, so.TemplateId, null)) as wo40_failed,
				count(if(so.TemplateId = 22 and so.SmsStatus = 1, so.TemplateId, null)) as wo40_others,
				count(if(so.TemplateId = 17 and so.SmsStatus = 2, so.TemplateId, null)) as wo53_success,
				count(if(so.TemplateId = 17 and so.SmsStatus = 3, so.TemplateId, null)) as wo53_queueu,
				count(if(so.TemplateId = 17 and so.SmsStatus = 4, so.TemplateId, null)) as wo53_failed,
				count(if(so.TemplateId = 17 and so.SmsStatus = 1, so.TemplateId, null)) as wo53_others,
				count(if(so.TemplateId = 18 and so.SmsStatus = 2, so.TemplateId, null)) as wo75_success,
				count(if(so.TemplateId = 18 and so.SmsStatus = 3, so.TemplateId, null)) as wo75_queueu,
				count(if(so.TemplateId = 18 and so.SmsStatus = 4, so.TemplateId, null)) as wo75_failed,
				count(if(so.TemplateId = 18 and so.SmsStatus = 1, so.TemplateId, null)) as wo75_others,
				count(if(so.TemplateId = 23 and so.SmsStatus = 2, so.TemplateId, null)) as wo100_success,
				count(if(so.TemplateId = 23 and so.SmsStatus = 3, so.TemplateId, null)) as wo100_queueu,
				count(if(so.TemplateId = 23 and so.SmsStatus = 4, so.TemplateId, null)) as wo100_failed,
				count(if(so.TemplateId = 23 and so.SmsStatus = 1, so.TemplateId, null)) as wo100_others,
				count(if(so.TemplateId = 19 and so.SmsStatus = 2, so.TemplateId, null)) as wo150_success,
				count(if(so.TemplateId = 19 and so.SmsStatus = 3, so.TemplateId, null)) as wo150_queueu,
				count(if(so.TemplateId = 19 and so.SmsStatus = 4, so.TemplateId, null)) as wo150_failed,
				count(if(so.TemplateId = 19 and so.SmsStatus = 1, so.TemplateId, null)) as wo150_others,
				count(if(so.TemplateId = 20 and so.SmsStatus = 2, so.TemplateId, null)) as wo175_success,
				count(if(so.TemplateId = 20 and so.SmsStatus = 3, so.TemplateId, null)) as wo175_queueu,
				count(if(so.TemplateId = 20 and so.SmsStatus = 4, so.TemplateId, null)) as wo175_failed,
				count(if(so.TemplateId = 20 and so.SmsStatus = 1, so.TemplateId, null)) as wo175_others,
				count(if(so.TemplateId = 29 and so.SmsStatus = 2, so.TemplateId, null)) as pop4_success,
				count(if(so.TemplateId = 29 and so.SmsStatus = 3, so.TemplateId, null)) as pop4_queueu,
				count(if(so.TemplateId = 29 and so.SmsStatus = 4, so.TemplateId, null)) as pop4_failed,
				count(if(so.TemplateId = 29 and so.SmsStatus = 1, so.TemplateId, null)) as pop4_others,
				count(if(so.TemplateId = 30 and so.SmsStatus = 2, so.TemplateId , null)) as bp4_success,
				count(if(so.TemplateId = 30 and so.SmsStatus = 3, so.TemplateId , null)) as bp4_queueu,
				count(if(so.TemplateId = 30 and so.SmsStatus = 4, so.TemplateId , null)) as bp4_failed,
				count(if(so.TemplateId = 30 and so.SmsStatus = 1, so.TemplateId , null)) as bp4_others,
				count(if(so.TemplateId = 31 and so.SmsStatus = 2, so.TemplateId, null)) as pop25_success,
				count(if(so.TemplateId = 31 and so.SmsStatus = 3, so.TemplateId, null)) as pop25_queueu,
				count(if(so.TemplateId = 31 and so.SmsStatus = 4, so.TemplateId, null)) as pop25_failed,
				count(if(so.TemplateId = 31 and so.SmsStatus = 1, so.TemplateId, null)) as pop25_others
			FROM serv_sms_outbox so 
			WHERE 
				so.SendDate >='".$_REQUEST['start_date']."' 
				and so.SendDate <= '".$_REQUEST['end_date']."'
			";
		$query =  @mysql_query($sql);
	?>
	<table>
			<tr>
				<th id="color">DATE</th>
				<th id="color">SMS Type</th>
				<th id="color">Unit</th>
				<th id="color">Success</th>
				<th id="color">Queueu</th>
				<th id="color">Failed</th>
				<th id="color">Others</th>
				<th id="color">Total</th>
			</tr>
			<?php 
				while( $rows = @mysql_fetch_array($query) ) :
					$total5 	= ($rows['wo5_success'] + $rows['wo5_queueu']) + ($rows['wo5_failed'] + $rows['wo5_others']); 
					$total20 	= ($rows['wo20_success'] + $rows['wo20_queueu']) + ($rows['wo20_failed'] + $rows['wo20_others']); 
					$total30 	= ($rows['wo30_success'] + $rows['wo30_queueu']) + ($rows['wo30_failed'] + $rows['wo30_others']);
					$total40 	= ($rows['wo40_success'] + $rows['wo40_queueu']) + ($rows['wo40_failed'] + $rows['wo40_others']); 
					$total53 	= ($rows['wo53_success'] + $rows['wo53_queueu']) + ($rows['wo53_failed'] + $rows['wo53_others']); 
					$total75 	= ($rows['wo75_success'] + $rows['wo75_queueu']) + ($rows['wo75_failed'] + $rows['wo75_others']); 
					$total100 	= ($rows['wo100_success'] + $rows['wo100_queueu']) + ($rows['wo100_failed'] + $rows['wo100_others']); 
					$total150 	= ($rows['wo150_success'] + $rows['wo150_queueu']) + ($rows['wo150_failed'] + $rows['wo150_others']); 
					$total175 	= ($rows['wo175_success'] + $rows['wo175_queueu']) + ($rows['wo175_failed'] + $rows['wo175_others']); 
					$totalp4    = ($rows['pop4_success'] + $rows['pop4_queueu']) + ($rows['pop4_failed'] + $rows['pop4_others']); 
					$totalbp 	= ($rows['bp4_success'] + $rows['bp4_queueu']) + ($rows['bp4_failed'] + $rows['bp4_others']); 
					$totalp25 	= ($rows['pop25_success'] + $rows['pop25_queueu']) + ($rows['pop25_failed'] + $rows['pop25_others']); 
					// $tot_foot5 += $total5;
					// $tot_foot20 += $total20;
					// $tot_foot30 += $total30;
					// $tot_foot40 += $total40;
					// $tot_foot53 += $total53;
					// $tot_foot75 += $total75;
					// $tot_foot100 += $total100;
					// $tot_foot150 += $total153;
					// $tot_foot175 += $total175;
					// $tot_footp4 += $totalp4;
					// $tot_footbp += $totalbp;
					// $tot_footpp25 += $totalp25;
				echo "<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+5)</td>
					<td>RIT 2</td>
					<td>".$rows['wo5_success']."</td>
					<td>".$rows['wo5_queueu']."</td>
					<td>".$rows['wo5_failed']."</td>
					<td>".$rows['wo5_others']."</td>
					<td>".$total5."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+20)</td>
					<td>RIT 2</td>
					<td>".$rows['wo20_success']."</td>
					<td>".$rows['wo20_queueu']."</td>
					<td>".$rows['wo20_failed']."</td>
					<td>".$rows['wo20_others']."</td>
					<td>".$total20."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+30)</td>
					<td>RIT 2</td>
					<td>".$rows['wo30_success']."</td>
					<td>".$rows['wo30_queueu']."</td>
					<td>".$rows['wo30_failed']."</td>
					<td>".$rows['wo30_others']."</td>
					<td>".$total30."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+40)</td>
					<td>RIT 2</td>
					<td>".$rows['wo40_success']."</td>
					<td>".$rows['wo40_queueu']."</td>
					<td>".$rows['wo40_failed']."</td>
					<td>".$rows['wo40_others']."</td>
					<td>".$total40."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+53)</td>
					<td>RIT 2</td>
					<td>".$rows['wo53_success']."</td>
					<td>".$rows['wo53_queueu']."</td>
					<td>".$rows['wo53_failed']."</td>
					<td>".$rows['wo53_others']."</td>
					<td>".$total53."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+75)</td>
					<td>RIT 2</td>
					<td>".$rows['wo75_success']."</td>
					<td>".$rows['wo75_queueu']."</td>
					<td>".$rows['wo75_failed']."</td>
					<td>".$rows['wo75_others']."</td>
					<td>".$total75."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+100)</td>
					<td>RIT 2</td>
					<td>".$rows['wo100_success']."</td>
					<td>".$rows['wo100_queueu']."</td>
					<td>".$rows['wo100_failed']."</td>
					<td>".$rows['wo100_others']."</td>
					<td>".$total100."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+150)</td>
					<td>RIT 2</td>
					<td>".$rows['wo150_success']."</td>
					<td>".$rows['wo150_queueu']."</td>
					<td>".$rows['wo150_failed']."</td>
					<td>".$rows['wo150_others']."</td>
					<td>".$total150."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Fresh WO (+175)</td>
					<td>RIT 2</td>
					<td>".$rows['wo175_success']."</td>
					<td>".$rows['wo175_queueu']."</td>
					<td>".$rows['wo175_failed']."</td>
					<td>".$rows['wo175_others']."</td>
					<td>".$total175."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Calendar Date (on 4th)</td>
					<td>RIT 2</td>
					<td>".$rows['pop4_success']."</td>
					<td>".$rows['pop4_queueu']."</td>
					<td>".$rows['pop4_failed']."</td>
					<td>".$rows['pop4_others']."</td>
					<td>".$totalp4."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Calendar Date (on 15th)</td>
					<td>RIT 2</td>
					<td>".$rows['bp4_success']."</td>
					<td>".$rows['bp4_queueu']."</td>
					<td>".$rows['bp4_failed']."</td>
					<td>".$rows['bp4_others']."</td>
					<td>".$totalbp."</td>
				</tr>
				<tr>
					<td>".$_REQUEST['start_date']." - ".$_REQUEST['end_date']."</td>
					<td>Calendar Date (on 24th)</td>
					<td>RIT 2</td>
					<td>".$rows['pop25_success']."</td>
					<td>".$rows['pop25_queueu']."</td>
					<td>".$rows['pop25_failed']."</td>
					<td>".$rows['pop25_others']."</td>
					<td>".$totalp25."</td>
				</tr>";
			endwhile;
			?>
	</table> <br><br><br>
	<!-- <footer>
		&copy;DIDIGANTENG
	</footer> -->
</body>
</html>