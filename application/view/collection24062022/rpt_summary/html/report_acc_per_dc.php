
<html>
<head>
		<title>
			Enigma Collection Report - Review Customer per DC
		</title>
		<style>
			<!--
				body, td, input, select, textarea {
					font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
				}
				a img {
					border: 0;
				}
				a.hover {
					text-decoration: none;
				}
				a.hover:hover {
					text-decoration: underline;
				}
				form {
					margin: 0;
					padding: 0;
				}
				table.data {
					border-style: solid;
					border-width: 1;
					border-color: silver;
					background-color: #ECF1FB;
					background-image: url(bgtablebox.jpg);
					background-position: bottom;
					background-repeat: repeat-x;
				}
				table.data th {
					padding: 3;
				}
				table.data td {
					padding: 0 6 0 6;
				}
				table.data td, table.data th {
					font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
					vertical-align: top;
				}
				table.data th {
					background-color: 3565AF;
					color: white;
					font-weight: normal;
					vertical-align: top;
					text-align: left;
				}
				table.data th a, table.data th a:visited {
					font-weight: normal;
					color: #CCFFFF;
				}
				table.data td.head {
					background-color: AABBFF;
					vertical-align:middle;
				}
				input, textarea {
				}
				input.button, button.button, span.button, div.button {
					border-style: solid;
					border-width: 1;
					border-color: 6666AA;
					background-image: url(bgbutt.jpg);
					background-repeat: repeat-x;
					background-position: center;
					font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
					font-weight: normal;
				}
				span.button a, div.button a {
					color: #0F31BB;
				}
				table.subdata th {
					font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
					color: #637dde;
					padding: 0 5 0 0;
					text-align: left;
				}


				.left { text-align:left;}
				.right{ text-align:right;}
				.center{ text-align:center;}
				.font-size22 { font-size:22px; color:#000;}
				.font-size20 { font-size:20px; color:#000;}
				.font-size18 { font-size:18px; color:#000;}
				.font-size16 { font-size:16px; color:#000;} 
				.font-size14 { font-size:16px; color:#000;} 

				p.normal  { line-height:6px;}

			-->
			</style>
	</head>
<body>
<?php

// --------------- function save URL  ------------------
function save_url_excel()
 {
	$out = new EUI_Object(_get_all_request() );
	$save_url_excel = url_controller() ."/ShowExcel?
		start_date={$out->get_value('start_date')}
		&report_title={$out->get_value('report_title')} 
		&end_date={$out->get_value('end_date')}
		&report_type={$out->get_value('report_type')}
		&report_group={$out->get_value('report_group')}
		&user_tl={$out->get_value('user_tl')}
		&user_agent={$out->get_value('user_agent')}
		&report_mode={$out->get_value('report_mode')}";
	return $save_url_excel;
 }	
 
$headers = array(
	'agent'=> "Nama DC",
	'Account_No'=> "Account No",
	'customer_name'=> "Customer Name",
	'CallReasonDesc'=> "Status",
	'tanggal'=> "Tanggal"
);
$cols_convert = array(
'proposal_date'=>'_getDateIndonesia',
'create_date'=>'_getDateIndonesia',
'open_date'=>'_getDateIndonesia',
'approval_date'=>'_getDateTime',
'outstanding_byspv'=>'_getCurrency',
'discountamo_byspv'=>'_getCurrency',
'totalpayment_byspv'=>'_getCurrency',
'downpayment_byspv'=>'_getCurrency',
'deb_principal'=>'_getCurrency',
'futurepay_byspv'=>'_getCurrency'
);

	echo "<div class=\"center\">".
		 "<p class=\"normal font-size22\">Report - Review Customer per DC</p>".
		 "<p class=\"normal font-size14\">Periode :". _get_post("start_date") ." to ". _get_post("end_date") ."</p>".
		 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
	"</div>";
	echo '<table class="data" border="1" style="border-collapse:collapse;" cellpadding="3px">';
	echo '<tr>';
	echo '<td class="head">NO.</td>';
	foreach($headers as $column => $name)
	{
		echo '<td class="head">' . $name .' </td>';
	}
	echo '</tr>';
	
	$no=1;
	
	foreach($acc_dc as $rows => $content)
	{
		echo '<tr>';
		echo '<td >'.$no.'</td>';
		foreach($headers as $column => $name)
		{
		// var_dump(in_array($column,array_keys($cols_convert)));
			if(in_array($column,array_keys($cols_convert)))
			{
				$result = call_user_func($cols_convert[$column],$content[$column]);
				echo '<td>'. $result. '</td>';
			}
			else
			{
				echo '<td>'.$content[$column]. '</td>';
			}
			
			
		}
		echo '</tr>';
		$no++;
	}
	echo '<tr><td colspan="'. (count($headers)+1) . '">Summary : '.($no-1).' Record(s)</td></tr>';
	echo "</table>";
	// echo"<pre>";
	// print_r($cpa_lunas);
	// echo"</pre>";
?>
<center>
	<a href="<?php echo save_url_excel();?>">Save as excel</a>
</center>
</body>
</html>	