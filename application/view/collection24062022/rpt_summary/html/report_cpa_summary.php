<?php

/*mysql_connect("192.168.0.11","enigma","enigma");
mysql_select_db("enigmacollectdb");*/

/* reconstruct date */
$sdates = explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];

$_SESSION['xl_start_date'] 	= $start_date;
$_SESSION['xl_end_date'] 		= $end_date;
$_SESSION['xl_rep_title'] 	= "Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}";

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}


// --------------- function save URL  ------------------
function save_url_excel()
 {
	$out = new EUI_Object(_get_all_request() );
	$save_url_excel = url_controller() ."/ShowExcel?
		start_date={$out->get_value('start_date')}
		&report_title={$out->get_value('report_title')} 
		&end_date={$out->get_value('end_date')}
		&report_type={$out->get_value('report_type')}
		&report_group={$out->get_value('report_group')}
		&user_tl={$out->get_value('user_tl')}
		&user_agent={$out->get_value('user_agent')}
		&report_mode={$out->get_value('report_mode')}";
	return $save_url_excel;
 }	
// -----------------------------------------------------------------------------------

 function showHeaders() 
 {
	echo "<div class=\"center\">".
		 "<p class=\"normal font-size22\">Report - Summary CPA</p>".
		 "<p class=\"normal font-size16\">Report Mode : Summary</p>".
		 "<p class=\"normal font-size14\">Periode :". _get_post("start_date") ." to ". _get_post("end_date") ."</p>".
		 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
	"</div>";
}

// -----------------------------------------------------------------------------------


function showReport($start_date,$end_date){
	global $project,$agent_id,$listagent;	
	$time_start = microtime(true);
	 echo '<table class="data" border="1" style="border-collapse:collapse;" cellpadding="3px">'
		    .'<tr>'
			.'<td class="head">No.</td>'
			.'<td class="head">Region</td>'
			.'<td class="head">Proposal Date</td>'
			.'<td class="head">Ref. No</td>'
			.'<td class="head">Product</td>'
			.'<td class="head">Arrangement</td>'
			.'<td class="head">Customer Number</td>'
			.'<td class="head">Full Name</td>'
			.'<td class="head">Open Date</td>'
    		.'<td class="head">Cycle DLQ</td>'
			.'<td class="head">Card Status</td>'
    		.'<td class="head">Reponsible Negotiator</td>'
			.'<td class="head">Placement</td>' 
			.'<td class="head">Agency Name</td>'
			.'<td class="head">Outstanding Balance</td>' 
			.'<td class="head">Total Payment</td>' 
			.'<td class="head">Down Payment</td>' 
			.'<td class="head">Future Payment</td>' 
			.'<td class="head">Payment Periode</td>' 
			.'<td class="head">Principal</td>'
			.'<td class="head">Charges Fee</td>'
			.'<td class="head">Discount Amount</td>'
			.'<td class="head">+/- from O/S balance (%)</td>'
			.'<td class="head">+/- from Principal (%)</td>'
			.'<td class="head">Ocupation</td>'
			.'<td class="head">Reason</td>'
			.'<td class="head">No. of Other Delinquen Debt</td>'
			.'<td class="head">Payment Handled By</td>'
			.'<td class="head">Mapping Acount</td>'
			.'<td class="head">Justification</td>'
			.'<td class="head">Exception Level</td>'
			.'<td class="head">Authority</td>'
			.'<td class="head">Approver Name</td>'
			.'<td class="head">Balance tobe Writen off</td>'
			.'<td class="head">Barcode</td>'
			.'<td class="head">Created Date</td>'
			.'<td class="head">Printed Date</td>'
			.'</tr>';

 		$sql="SELECT b.region,
		b.create_date as 'Proposal Date',
		'D' as 'Ref. No.', 
		'Product=formula',
		'Arrangement=formula',
		a.deb_acct_no,
		a.deb_name,
		a.deb_open_date,
		'cycle Dlq = 8',
		b.card_status,
		b.agent_id as 'Responsible Negotiator',
		b.placement,
		a.deb_wo_amount as 'Outstanding balance', 
		'Total Payment=getSummaryPay()',
		b.downpayment_byspv,'Total Pay - Down Pay' as 'Future Pay',
		b.payment_periode,
		a.deb_principal,
		a.deb_pri_afterpay,
		a.deb_bal_afterpay,
		'Out_Bal - Princ' as 'Charges Fee',
		'Out-Bal = Tot_Pay' as 'Disc_amount',
		'+/- from O/S balance (%)',
		'+/- from Principal (%)',
		b.occupation,
		b.reason,
		b.pay_handled_by,
		b.justification,
		b.create_date,
		b.printed_date,
		b.principle_byspv, 
		b.outstanding_byspv, 
		b.totalpayment_byspv,
		b.downpayment_byspv, 
		b.charges_byspv, 
		b.from_princ
		FROM t_gn_debitur a, t_gn_discount_trx b where a.deb_acct_no=b.deb_acctno
		AND b.create_date >= '$start_date 00:00:00'
		AND b.create_date <= '$end_date 23:00:00'";
		//echo $sql;
		$qry=@mysql_query($sql);
		$col=array();
		$n=1;
		$p=array();
		while($res=@mysql_fetch_array($qry)){
		    $p=$res["UPLOAD_ID"];
			$prj=explode("_",$p);
			$create_date=explode(" ",$res["Proposal Date"]);
			$project=$prj[3];
			$col["region"][$n]=$res["region"];
			$col["Proposal Date"][$n]=$create_date[0];
			$col["Ref. No."][$n]=$res["Ref. No."];
			$col["Product=formula"][$n]=update_product($res["deb_acct_no"]);
			$col["Arrangement=formula"][$n]=update_arrangement($res["Ref. No."]);
			$col["deb_acct_no"][$n]=$res["deb_acct_no"];
			$col["deb_name"][$n]=$res["deb_name"];
			$col["deb_open_date"][$n]=$res["deb_open_date"];
			$col["cycle Dlq = 8"][$n]="8";
			$col["card_status"][$n]=$res["card_status"];
			$col["Responsible Negotiator"][$n]=$res["Responsible Negotiator"];
			$col["placement"][$n] =$res["placement"];
			$col["Agency Name"][$n]="AIA";
			$col["Outstanding balance"][$n]=$res["outstanding_byspv"];
			$col["Total Payment"][$n]=$res["totalpayment_byspv"];
			$col["downpayment_byspv"][$n]	=$res["downpayment_byspv"];
			$col["Future Pay"][$n]	=$res["totalpayment_byspv"]-$res["downpayment_byspv"];
			$col["payment_periode"][$n]	=$res["payment_periode"];
			$col["deb_principal"][$n]	=$res["deb_principal"];
			$col["Charges Fee"][$n]	=$res["outstanding_byspv"]-$res["principle_byspv"];
			$col["Disc_amount"][$n]	=$res["outstanding_byspv"]-$res["totalpayment_byspv"];
			$col["+/- from O/S balance (%)"][$n]=in_min_value($col["Disc_amount"][$n]/$res["outstanding_byspv"]);
			$col["+/- from Principal (%)"][$n]	=in_pls_value(($col["Total Payment"][$n]-$res["principle_byspv"])/$res["principle_byspv"]);
			$col["Ocupation"][$n]	=$res["occupation"];
			$col["Reason"][$n]	=$res["reason"];
			$col["PaymentHandledBy"][$n]=$res["pay_handled_by"];
			$col["justification"][$n]	=$res["justification"];
			$col["create_date"][$n]	=$res["create_date"];
			$col["printed_date"][$n]	=$res["printed_date"];
			$col["exception_level"][$n] = exception_levels($col["Disc_amount"][$n]);
			$col["getCols"][$n] = getCols(exception_levels($col["Disc_amount"][$n]));
			$col["from_princ"][$n] = $res["from_princ"];
		$n++;
		}
		
		for($m=1; $m<$n; $m++){
		   echo  '<tr>'
				.'<td>'.$m.'</td>'
				.'<td>'.$col["region"][$m].'</td>'
				.'<td>'.$col["Proposal Date"][$m].'</td>'
				.'<td>'.$col["Ref. No."][$m].'</td>'
				.'<td>'.$col["Product=formula"][$m].'</td>'
				.'<td>'.$col["Arrangement=formula"][$m].'</td>'
				.'<td>'.$col["deb_acct_no"][$m].'</td>'
				.'<td>'.$col["deb_name"][$m].'</td>'
				.'<td>'.$col["deb_open_date"][$m].'</td>'
				.'<td>'.$col["cycle Dlq = 8"][$m].'</td>'
				.'<td>'.$col["card_status"][$m].'</td>'
				.'<td>'.$col["Responsible Negotiator"][$m].'</td>'
				.'<td>'.$col["placement"][$m].'</td>'
				.'<td>'.$col["Agency Name"][$m].'</td>'
				.'<td>'.currency($col["Outstanding balance"][$m]).'</td>'
				.'<td>'.currency($col["Total Payment"][$m]).'</td>'
				.'<td>'.currency($col["downpayment_byspv"][$m]).'</td>'
				.'<td>'.currency($col["Future Pay"][$m]).'</td>'
				.'<td>'.$col["payment_periode"][$m].'</td>'
				.'<td>'.currency($col["deb_principal"][$m]).'</td>'
				.'<td>'.currency($col["Charges Fee"][$m]).'</td>'
				.'<td>'.currency($col["Disc_amount"][$m]).'</td>'
				.'<td>'.$col["+/- from O/S balance (%)"][$m].'</td>'
				.'<td>'.$col["+/- from Principal (%)"][$m].'</td>'
				.'<td>'.$col["Ocupation"][$m].'</td>'
				.'<td>'.$col["Reason"][$m].'</td>'
				.'<td>'.">=3".'</td>'
				.'<td>'.$col["PaymentHandledBy"][$m].'</td>'
				.'<td>'."N".'</td>'
				.'<td>'.$col["justification"][$m].'</td>'
				.'<td>'.$col["exception_level"][$m].'</td>'
				.'<td>'.$col["getCols"][$m].'</td>'
				.'<td>'.(($col["payment_periode"][$m] == ">6" || $col["from_princ"][$m] <= "-0.4")?"Approval by Head of Collection":"-").'</td>'
				.'<td>'.currency($col["Charges Fee"][$m]).'</td>'
				.'<td>'."".'</td>'
				.'<td>'.$col["create_date"][$m].'</td>'
				.'<td>'.$col["printed_date"][$m].'</td>'
				.'</tr>';
		 }
		 echo    '<tr><td colspan="37">Summary : '.($m-1).' Records</td></tr>'
			    .'</table>';
}


		
function update_product($deb_acct_no){
	$cfno = $deb_acct_no;
	$str = substr($deb_acct_no,0,1);
	$str2 = substr($deb_acct_no,0,3);
	$prod = "";
	
	if($str == '2'){
		$prod = "CF";
	}else if($str == '4'){
		$prod = "CARD";
	}else if($str == '5'){
		$prod = "CARD";
	}else if($str2 == '080'){
		$prod = "PIL";
	}else if($str2 == '001'){
		$prod = "PIL";
	}else if($str2 == '071'){
		$prod = "GRF";
	}else if($str2 == '073'){
		$prod = "GRF";
	}else{
		$prod = "-";
	}
	
	return $prod;
}

function update_arrangement($reff){
	$argmt = "";
	if($reff == 'D'){
		$argmt = "SETTLEMENT";
	}else if($reff == 'R'){
		$argmt = "RESCHEDULE";
	}else if($reff == 'X'){
		$argmt = "PAID-OFF";
	}else{
		$argmt = "-";
	}
	return $argmt;
}

function getSummaryPay($deb_acct_no=''){
	$sql ="select sum(a.ptp_amount) as afterpay from t_tx_ptp a
			left join t_gn_debitur b on b.deb_id =  a.deb_id
			where b.deb_acct_no ='$deb_acct_no'";
	$qry = @mysql_query($sql);
	if( $qry && ($row = mysql_fetch_assoc($qry))){
		return $row['afterpay'];
	}	
}

function currency($currency=''){
			return number_format($currency,0,',','.');
	}

function in_min_value($integer_min=''){
	return (number_format($integer_min,4, '.','') * (-1)*100).'%';
}

function in_pls_value($integer_min=''){
	return (number_format($integer_min,4, '.','') *100).'%';
}
function exception_levels($disc=''){
	$excpt = "";
	
	if($disc <= 5000000){
		$excpt = "1";
	}else if($disc <= 10000000){
		$excpt = "2";
	}else if($disc <= 20000000){
		$excpt = "3";
	}else if($disc <= 30000000){
		$excpt = "4";
	}else if($disc <= 50000000){
		$excpt = "5";
	}else if($disc <= 100000000){
		$excpt = "6";
	}else if($disc <= 2300000000){
		$excpt = "7";
	}else{
		$excpt = "-";
	}
	
	return $excpt;
}

function getCols($int){
		if( empty($int)) return false;
		else{
			$iont = array(
				1=>'Coll Spv',
				2=>'Coll Band 6',
				3=>'Coll Band 5',
				4=>'Coll Band 4',
				5=>'Head Of Coll',
				6=>'Coll of CCC',
				7=>'Coll of CRM');
			return $iont[$int];	
		}
}

?>
<html>
<head>
		<title>
			Enigma Collection Report - CPA Summarys Discount
		</title>
	<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
	vertical-align:middle;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align:middle;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	vertical-align:middle;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}


.left { text-align:left;}
.right{ text-align:right;}
.center{ text-align:center;}
.font-size22 { font-size:22px; color:#000;}
.font-size20 { font-size:20px; color:#000;}
.font-size18 { font-size:18px; color:#000;}
.font-size16 { font-size:16px; color:#000;} 
.font-size14 { font-size:16px; color:#000;} 

p.normal  { line-height:6px;}

			-->
			</style>
	</head>
<body>
	<?php showHeaders(); ?>
	<?php showReport($start_date,$end_date);?>
	<center>
		<a href="<?php echo save_url_excel();?>">Save as excel</a>
	</center>
	
</body>
</html>