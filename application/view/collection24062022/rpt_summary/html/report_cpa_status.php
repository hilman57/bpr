<?php

/*mysql_connect("192.168.0.11","enigma","enigma");
mysql_select_db("enigmacollectdb");*/
	
/*$dateFrom	= $_REQUEST['dateFrom'];
$dateTo		= $_REQUEST['dateTo'];
$rptGroup	= $_REQUEST['rptGroup'];
$rptAgents	= $_REQUEST['rptAgents'];

/* reconstruct date */
$sdates		= explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates		= explode("-", $end_date);
$end_date	= $edates[2]."-".$edates[1]."-".$edates[0];
$_SESSION['xl_start_date'] 	= $start_date;
$_SESSION['xl_end_date'] 	= $end_date;
$_SESSION['xl_rep_title'] 	= "Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}";

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}

// --------------- function save URL  ------------------
function save_url_excel()
 {
	$out = new EUI_Object(_get_all_request() );
	$save_url_excel = url_controller() ."/ShowExcel?
		start_date={$out->get_value('start_date')}
		&report_title={$out->get_value('report_title')} 
		&end_date={$out->get_value('end_date')}
		&report_type={$out->get_value('report_type')}
		&report_group={$out->get_value('report_group')}
		&user_tl={$out->get_value('user_tl')}
		&user_agent={$out->get_value('user_agent')}
		&report_mode={$out->get_value('report_mode')}";
	return $save_url_excel;
 }	
// -----------------------------------------------------------------------------------

 function showHeaders() 
 {
	echo "<div class=\"center\">".
		 "<p class=\"normal font-size22\">Report - CPA Status Report</p>".
		 "<p class=\"normal font-size16\">Report Mode : Summary</p>".
		 "<p class=\"normal font-size14\">Periode :". _get_post("start_date") ." to ". _get_post("end_date") ."</p>".
		 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
	"</div>";
}

// -----------------------------------------------------------------------------------

function showReport($start_date,$end_date){
	global $project,$agent_id,$listagent;	
	$time_start = microtime(true);
		echo '<table class="data" border="1" style="border-collapse:collapse;" width="99%" align="center" cellpadding="3px">'
		    .'<tr>'
			.'<td class="head">No.</td>'
			.'<td class="head">Product</td>'
			.'<td class="head">Customer Number</td>'
			.'<td class="head">Full Name</td>'
			.'<td class="head">Account Status</td>'
    		.'<td class="head">Tenor</td>'
			.'<td class="head">Create Date</td>'
    		.'<td class="head">Disc. Balance</td>'
			.'<td class="head">Disc. Principal</td>' 
			.'<td class="head">Payment</td>'
			.'<td class="head">Deal Payment</td>'
			.'<td class="head">Agent ID</td>' 
			.'<td class="head">Status CPA</td>'
			.'</tr>';

 		$sqls = "SELECT 	
		e.CampaignName as Product, 
		a.deb_id,a.deb_acct_no,
		 a.deb_name ,
		 a.deb_call_status_code,
		 c.CallReasonDesc ,
		 a.deb_prev_call_status_code,
		 d.CallReasonDesc,
		 a.deb_wo_amount,
		 b.create_date,
		 b.from_balance,
		 b.from_princ,
		 b.cpa_status,
		 b.agent_id,
		 b.expired_date,
		 b.approval_date,
		 b.payment_periode,
		 b.totalpayment_byspv,
		 b.aggrement,
		 b.cpa_status
	  	 FROM t_gn_debitur a
		 LEFT JOIN t_lk_account_status c ON a.deb_call_status_code = c.CallReasonCode
		 LEFT JOIN t_gn_discount_trx b ON a.deb_acct_no=b.deb_acctno
		 LEFT JOIN t_lk_account_status d ON a.deb_prev_call_status_code = d.CallReasonCode		
		 left join t_gn_campaign e on a.deb_cmpaign_id  =e.CampaignId
		WHERE
		b.create_date >= '$start_date 00:00:00' 
		AND b.create_date <= '$end_date 23:00:00' 
		AND b.aggrement= 105
		and b.cpa_status <> ''
		AND b.approval_date is not null";
		/*
		b.aggrement = 101 AND b.approval_date is not null
		AND b.cpa_status in ('BATAL','LUNAS')";
		*/
		$qrys=@mysql_query($sqls);
		//echo $sqls;
		$col=array();
		$n=1;
		//$res=mysql_fetch_assoc($qrys);
		while($res=@mysql_fetch_array($qrys)){
			$col["product_id"][$n] = $res["Product"];
			$col["deb_id"][$n] = $res["deb_id"];
			$col["deb_acct_no"][$n]=$res["deb_acct_no"];
			$col["deb_name"][$n]=$res["deb_name"];
			$col["accstat"][$n]=$res["CallReasonDesc"];
			$col["payment_periode"][$n]=$res["payment_periode"];
			$col["create_date"][$n]=$res["create_date"];
			$col['from_balance'][$n]=$res['from_balance']*100;
			$col["from_princ"][$n]=$res["from_princ"]*100;
			$col["totalpayment_byspv"][$n]=$res["totalpayment_byspv"];
			$col["totalpayment_byhst"][$n]=get_payment_history($col["deb_id"][$n],$col["create_date"][$n],$res["expired_date"]);
			$col["agent_id"][$n]=$res["agent_id"];
			$col["cpa_status"][$n]=$res["cpa_status"];
		$n++;
		}

		for($m=1; $m<$n; $m++){
		   echo  '<tr>'
				.'<td>'.$m.'</td>'
				.'<td>'.$col["product_id"][$m].'</td>'
				.'<td>'.$col["deb_acct_no"][$m].'</td>'
				.'<td>'.$col["deb_name"][$m].'</td>'
				.'<td>'.$col["accstat"][$m].'</td>'
				.'<td>'.$col["payment_periode"][$m].'</td>'
				.'<td>'.$col["create_date"][$m].'</td>'
				.'<td align="right">-'.$col["from_balance"][$m].'%</td>'
				.'<td align="right">'.$col["from_princ"][$m].'%</td>'
				.'<td align="right">'.currency($col["totalpayment_byhst"][$m]).'</td>'
				.'<td align="right">'.currency($col["totalpayment_byspv"][$m]).'</td>'
				.'<td>'.$col["agent_id"][$m].'</td>'
				.'<td>'.$col["cpa_status"][$m].'</td>'
				.'</tr>';
		 }
		 echo    '<tr><td colspan="12">Summary : '.($m-1).' Records</td></tr>'
			    .'</table>';
}


		
	function get_payment_history($custno="",$create_date="",$expire_date="")
		{
			$key = substr($custno,0,1);
			//if($key=="5" || $key=="4")
			//{
				$sql="	SELECT sum(a.pay_amount) as payment
						FROM t_gn_payment a 
						WHERE a.deb_id = '$custno' AND pay_date >='$create_date' AND pay_date <='$expire_date'";
						
				//echo $sql;		
				$res=mysql_fetch_assoc(mysql_query($sql));
				
				$get_payment_history=$res['payment'];
			//}
			/*else if($key=="0" || $key=="2")
			{
				$sql="	SELECT sum(a.pay_amount) as payment
						FROM t_gn_payment a 
						WHERE a.deb_id = '$custno' AND pay_date >='$create_date' AND pay_date <='$expire_date'";
				$res=mysql_fetch_assoc(mysql_query($sql));
				$get_payment_history=$res['payment'];
			}
			*/
			return $get_payment_history;
			//return $sql;
		}
function update_product($custno){
	$cfno = $custno;
	$str = substr($custno,0,1);
	$str2 = substr($custno,0,3);
	$prod = "";
	
	if($str == '2'){
		$prod = "CF";
	}else if($str == '4'){
		$prod = "CARD";
	}else if($str == '5'){
		$prod = "CARD";
	}else if($str2 == '080'){
		$prod = "PIL";
	}else if($str2 == '001'){
		$prod = "PIL";
	}else if($str2 == '071'){
		$prod = "GRF";
	}else if($str2 == '073'){
		$prod = "GRF";
	}else{
		$prod = "-";
	}
	
	return $prod;
}

function update_arrangement($reff){
	$argmt = "";
	if($reff == 'D'){
		$argmt = "SETTLEMENT";
	}else if($reff == 'R'){
		$argmt = "RESCHEDULE";
	}else if($reff == 'X'){
		$argmt = "PAID-OFF";
	}else{
		$argmt = "-";
	}
	return $argmt;
}

function getSummaryPay($custno=''){
	$sql ="select sum(a.amount) as afterpay from coll_ptp_history a
			where a.custno='$custno'";
	$qry = @mysql_query($sql);
	if( $qry && ($row = mysql_fetch_assoc($qry))){
		return $row['afterpay'];
	}	
}

function currency($currency=''){
			return number_format($currency,0,',','.');
	}

function in_min_value($integer_min=''){
	return (number_format($integer_min,4, '.','') * (-1)*100).'%';
}

function exception_levels($disc=''){
	$excpt = "";
	
	if($disc <= 5000000){
		$excpt = "1";
	}else if($disc <= 10000000){
		$excpt = "2";
	}else if($disc <= 20000000){
		$excpt = "3";
	}else if($disc <= 30000000){
		$excpt = "4";
	}else if($disc <= 50000000){
		$excpt = "5";
	}else if($disc <= 100000000){
		$excpt = "6";
	}else if($disc <= 2300000000){
		$excpt = "7";
	}else{
		$excpt = "-";
	}
	
	return $excpt;
}

?>
<html>
<head>
		<title>
			Enigma Collection Report - CPA Status Report
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	vertical-align:middle;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}


.left { text-align:left;}
.right{ text-align:right;}
.center{ text-align:center;}
.font-size22 { font-size:22px; color:#000;}
.font-size20 { font-size:20px; color:#000;}
.font-size18 { font-size:18px; color:#000;}
.font-size16 { font-size:16px; color:#000;} 
.font-size14 { font-size:16px; color:#000;} 

p.normal  { line-height:6px;}

			-->
			</style>
	</head>
<body>
	<?php showHeaders();?>
	<?php showReport($start_date,$end_date); ?>
	<center>
	<a href="<?php echo save_url_excel();?>">Save as excel</a>
	</center>
</body>
</html>