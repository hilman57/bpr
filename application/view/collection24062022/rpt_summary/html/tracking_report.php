<?php

Global $start_microtime;
Global $gReportSession;

// ---------------------------------------------------------
$sdates = explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){

	echo "End date before start date";
	exit;
}


 function Export()
{
	
}


function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}

	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];

	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);

	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}

	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;

	return $yyyy."-".$mm."-".$dd;
}

function printNote(){
	echo "Note:<br>".
			 "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">".
	     "</table>";
}

//--------------------------------------------------------------------------
/*
 * @ package 		ShowExcel
 */
 
 function showTl()
 {
	$arr_tl = array(); 
	$sql = " select a.UserId, a.id, a.full_name 
			 from t_tx_agent a 
			 where a.handling_type = 13 ";
			 
	$rs = @mysql_query($sql);
	 while( $row =@ mysql_fetch_array( $rs ) ) {
		$arr_tl[$row['UserId']] = join(" - ", array($row['id'], $row['full_name']));
	}	
	
	return $arr_tl;
 }
 
  //--------------------------------------------------------------------------
/*
 * @ package 		ShowExcel
 */
 
 function showAgentName( $AgentId = 0  )
{
	$arr_agent = array(); 
	$sql = sprintf(" select a.UserId, a.id, a.full_name 
			 from t_tx_agent a 
			 where a.UserId='%d'", $AgentId);
			 
	$rs = @mysql_query($sql);
	 while( $row =@ mysql_fetch_array( $rs ) ) {
		$arr_agent[$row['UserId']] = join(" - ", array($row['id'], $row['full_name']));
	}	
	return ( isset( $arr_agent[$AgentId] ) ?  $arr_agent[$AgentId] : "-");
 }

 //--------------------------------------------------------------------------
/*
 * @ package 		ShowExcel
 */
 
 function showAgent( $AgentId = 0  ) {
	 
	$arr_agent = array(); 
	$sql = " select a.UserId, a.id, a.full_name 
			 from t_tx_agent a 
			 where a.handling_type = 4 ";
			 
	$rs = @mysql_query($sql);
	 while( $row =@ mysql_fetch_array( $rs ) ) {
		$arr_agent[$row['UserId']] = join(" - ", array($row['id'], $row['full_name']));
	}	
	return $arr_agent;
 }
 
// -----------------------------------------------------------------------------------

 function showHeaders() 
 {
	
	$Group = array('report_group_per_tl' => 'Group Per TL', 'report_group_per_deskoll' => 'Group Per Agent');
	$Mode = array('summary' =>'Summary', 'detail' => 'Detail');
	$out = new EUI_Object(_get_all_request());
	
	$mode_report = $Group[$out->get_value('report_group')];
	$group_report = $Mode[$out->get_value('report_mode')];
	
	echo "<div class=\"center\">".
		 "<p class=\"normal font-size22\">Report - Data Tracking</p>".
		 "<p class=\"normal font-size18\">Report Group : {$mode_report}</p>".
		 "<p class=\"normal font-size16\">Report Mode : {$group_report}</p>".
		 "<p class=\"normal font-size14\">Periode :". _get_post("start_date") ." to ". _get_post("end_date") ."</p>".
		 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
	"</div>";
}
 
 
 //--------------------------------------------------------------------------
/*
 * @ package 		ShowExcel
 */
 
function showHeaderDetailTL( $header = "TL Name")
{
	
  echo '<table class="data" border=1 style="border-collapse: collapse">'
			.'<tr>'
		     .'<td class="head" rowspan="2">No.</td>'
		     .'<td class="head" rowspan="2">'.$header.'</td>'
		     .'<td class="head" rowspan="2">Datasize</td>'
		     .'<td class="head" rowspan="2">Volume</td>'
		     .'<td class="head" rowspan="2">Data Utilized</td>'
		     .'<td class="head" rowspan="2">Volume Utilized</td>'
		     .'<td class="head" rowspan="2">Call Initiated</td>'
		     .'<td class="head" rowspan="2">% Utilized</td>'
		     .'<td class="head" colspan="6" align="center">PTP</td>'
		     .'<td class="head" colspan="2" align="center">VALID</td>'
		     
			 //.'<td class="head" colspan="5" align="center">SKIP</td>'
			 
		     .'<td class="head" colspan="2" align="center">PR-</td>'
		     .'<td class="head" colspan="2" align="center">ON-NEGO </td>'
		     .'<td class="head" align="center">OS-</td>'
			 
			 
			 .'<td class="head" colspan="2" align="center">DECEASE</td>'
			 .'<td class="head" colspan="2" align="center">MOVE</td>'
			 .'<td class="head" colspan="2" align="center">NBP</td>'
			 .'<td class="head" colspan="2" align="center">BUSY</td>'
			 
			 
			 .'<td class="head" colspan="2" align="center">COMPLAIN</td>'
			 .'<td class="head" rowspan="2" align="center">TOTAL REACH</td>'
			 .'<td class="head" rowspan="2" align="center">(%)REACH</td>'
		     .'</tr>';
			 
		echo '<tr>'
		     .'<td class="head" align="center">PTP NEW</td>'
			 .'<td class="head" align="center">PTP POP</td>'
			 .'<td class="head" align="center" nowrap>PTP <br>PAID OFF</td>'
			 //.'<td class="head" align="center">SP</td>'
			 .'<td class="head" align="center">PTP BP</td>'
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
		    //.'<td class="head" align="center">MV</td>'
		    //.'<td class="head" align="center">WN</td>'
		    //.'<td class="head" align="center">NK</td>'
			//.'<td class="head" align="center">TOTAL</td>'
			//.'<td class="head" align="center">(%)</td>'
		     
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 .'<td class="head" align="center">TOTAL OS </td>'
			 
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%) </td>'

			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%) </td>'
			 
		     .'<td class="head" align="center">TOTAL COMPLAIN</td>'
		     .'<td class="head" align="center">(%) COMPLAIN </td>'
		   .'</tr>';
			 
}

//--------------------------------------------------------------------------
/*
 * @ package 		ShowExcel
 */
 
function showHeaderSummaryTL( $header = "TL Name" ){
	
  echo '<table class="data" border=1 style="border-collapse: collapse">'
			.'<tr>'
		     .'<td class="head" rowspan="2">No.</td>'
		     .'<td class="head" rowspan="2">'.$header .'</td>'
		     .'<td class="head" rowspan="2">Datasize</td>'
		     .'<td class="head" rowspan="2">Volume</td>'
		     .'<td class="head" rowspan="2">Data Utilized</td>'
		     .'<td class="head" rowspan="2">Volume Utilized</td>'
		     .'<td class="head" rowspan="2">Call Initiated</td>'
		     .'<td class="head" rowspan="2">% Utilized</td>'
		     .'<td class="head" colspan="6" align="center">PTP</td>'
		     .'<td class="head" colspan="2" align="center">VALID</td>'
		     // .'<td class="head" colspan="5" align="center">SKIP</td>'
			 
		     .'<td class="head" colspan="2" align="center">PR-</td>'
		     .'<td class="head" colspan="2" align="center">ON-NEGO </td>'
		     .'<td class="head" align="center">OS-</td>'
			 
			 .'<td class="head" colspan="2" align="center">DECEASE</td>'
			 .'<td class="head" colspan="2" align="center">MOVE</td>'
			 .'<td class="head" colspan="2" align="center">NBP</td>'
			 .'<td class="head" colspan="2" align="center">BUSY</td>'
			 
			 .'<td class="head" colspan="2" align="center">COMPLAIN</td>'
			 .'<td class="head" rowspan="2" align="center">TOTAL REACH</td>'
			 .'<td class="head" rowspan="2" align="center">(%)REACH</td>'
		     .'</tr>';
			 
		echo '<tr>'
		     .'<td class="head" align="center">PTP NEW</td>'
			 .'<td class="head" align="center">PTP POP</td>'
			 .'<td class="head" align="center" nowrap>PTP <br>PAID OFF</td>'
		//	 .'<td class="head" align="center">SP</td>'
			 .'<td class="head" align="center">PTP BP</td>'
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
		     
			 // .'<td class="head" align="center">MV</td>'
		     // .'<td class="head" align="center">WN</td>'
		     // .'<td class="head" align="center">NK</td>'
			 // .'<td class="head" align="center">TOTAL</td>'
			 // .'<td class="head" align="center">(%)</td>'
			 
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 .'<td class="head" align="center">TOTAL OS </td>'
			 
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%) </td>'

			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
			 .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%) </td>'
			 
		     .'<td class="head" align="center">TOTAL COMPLAIN</td>'
		     .'<td class="head" align="center">(%) COMPLAIN </td>'
		   .'</tr>';
			 
}


//--------------------------------------------------------------------------
/*
 * @ package 		ShowExcel
 */
 
function ShowReport()
{
  
  $start_microtime = strtotime(date('Y-m-d H:i:s'));
  
  $out = new EUI_Object( _get_all_request() );
  if( $out->get_value('report_group') =='report_group_per_deskoll' ){
	ShowSummaryReportAgent();	
  }
	
  else if( $out->get_value('report_group') =='report_group_per_tl' ) 
  {
	 switch( $out->get_value('report_mode') ) {
		case 'summary' : ShowSummaryReportByTL(); break;
		case 'detail'  : ShowDetailReportByTL(); break;				
		default :
				ShowSummaryReportByTL();
		break;
			
	}
  }	
  
  $end_microtime = strtotime(date('Y-m-d H:i:s'));
  $total_execute_time = (($end_microtime)- ($start_microtime));
  printf("Execute time : %s (s)", $total_execute_time);
}

//--------------------------------------------------------------------------
/*
 * @ package 		ShowSummaryReportAgent
 */
 
 function ShowSummaryReportAgent() 
{ 
	
  $out 			= new EUI_Object( _get_all_request() );
  $start_now 	= date('Y-m-d');
  $Agent 		= $out->get_array_value('user_agent');
  $start_date  	= $out->get_value('start_date','_getDateEnglish');
  $end_date 	= $out->get_value('end_date','_getDateEnglish');
  $product 		= ( $out->get_array_value('product') ? $out->get_value('product')  : null );
  $ArTL  		= showAgent();
  
  showHeaderSummaryTL("Agent Name");
  
// ------------- on data product  filter data ---------
  
  $ProductId = ( is_null($product) ? "" : sprintf("AND b.deb_cmpaign_id IN (%d)", $product) );
  $ProductId2 = ( is_null($product) ? "" : sprintf("AND a.deb_cmpaign_id IN (%d)", $product) );

// -------------------------------------------------------------
  
 $agent_data = array();
	
// Get data size and volume

 $sql = sprintf("
			SELECT  c.UserId as agent, count(*) as datasize, 
				SUM(a.deb_amount_wo) AS datavol, c.tl_id
			from t_gn_debitur a USE INDEX (unik)
			inner join cc_agent b on a.deb_agent=b.userid
			inner join t_tx_agent c on b.userid=c.id
			WHERE 1=1 %s
			group by agent ", $ProductId2);

 printf("<!-- <pre>%s</pre><hr> -->", $sql);
	// -------------- echo $sql ----------------------;		 
		
		$res = @mysql_query($sql);
		 while ($row = @mysql_fetch_array($res)) 
		{
			$data = array();
			$agent = $row['agent'];
			$data['datasize']+= $row['datasize'];
			$data['datavol']+= $row['datavol'];
			$agent_data[$agent] = $data;
		}
	
// --------------- Get data utilization ----------------------------------------
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT a.CustomerId) AS u_data, 
				SUM(DISTINCT b.deb_amount_wo) AS u_vol
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId=b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' %s
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
		$sql2 = sprintf("SELECT 
				c.tl_id AS tl, 
				c.UserId as agent,
				COUNT(DISTINCT a.CustomerId) AS u_data, 
				SUM(DISTINCT b.deb_amount_wo) AS u_vol
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId=b.deb_id
			INNER JOIN t_tx_agent c on b.deb_agent=c.id
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );
			
		$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
		//printf("data utilize : <pre>%s</pre><hr>", $sql);
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) 
		{			
			$agent = $row['agent'];
			$agent_data[$agent]['u_data']+= $row['u_data'];
			$agent_data[$agent]['u_vol']+= $row['u_vol'];
		}
		
		
	// ----- Get Call Initiated on cc_call_session -------------------------------------------
	
		$sql = sprintf( 
				"select 
					count(*) as agent_dial, 
				c.UserId as agent 
				from cc_call_session a ,cc_agent b, t_tx_agent c  
				where a.agent_id = b.id and b.userid=c.id  
				and a.start_time >= '%s 00:00:00' 
				and a.start_time <= '%s 23:59:59' 
				group by agent", $start_date, $end_date );
		
	//	printf("<pre>%s</pre><hr>", $sql);	
	
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res))  {
			$agent = $row['agent'];
			$agent_data[$agent]['agent_dial']+= $row['agent_dial'];
		}
		
	// -------------- get PTP New -------------------------------
		$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_new
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id 
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=106 %s
			GROUP BY  tl, agent", $start_date, $end_date, $ProductId);
			
		$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_new
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id 
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND e.CallReasonCode=106 %s
			GROUP BY tl, agent",$start_now, $start_now, $ProductId );	
			
		$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
		
	   
		// printf("<!-- <pre>%s</pre><hr> -->", $sql);	
		
		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['agent'];
			$agent_data[$agent]['ptp_new']+= $row['ptp_new'];
		}
		
	// ------- get data PTP - POP (107)
	
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_pop
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >='%s 00:00:00' 
				AND a.CallHistoryCreatedTs <='%s 23:59:59' 
				AND a.CallAccountStatus = 107 %s
			GROUP BY  tl, agent", $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_pop
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >='%s 00:00:00' 
				AND a.CallHistoryCreatedTs <='%s 23:59:59' 
				AND a.CallAccountStatus = 107 %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
	
	//printf("<pre>%s</pre><hr>", $sql);	
	 $res = @mysql_query($sql);
	 while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['ptp_pop']+= $row['ptp_pop'];
	 }
		
	// ------- get data PTP - PAID (109)
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_paid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus = 109 %s
			GROUP BY tl, agent", $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_paid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus = 109 %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );   
	//printf("<pre>%s</pre><hr>", $sql);	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['ptp_paid']+= $row['ptp_paid'];
	}
	
	// ------- get data SETTLE - PAYMENT (SP) (0)	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_sp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >= '%s 00:00:00' 
				AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
				AND a.CallAccountStatus = '0' %s
			GROUP BY tl, agent",  $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_sp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >= '%s 00:00:00' 
				AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
				AND a.CallAccountStatus = '0' %s
			GROUP BY tl, agent",  $start_now, $start_now, $ProductId );		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );   
	//printf("<pre>%s</pre><hr>", $sql);	
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent =$row['agent'];
		$agent_data[$agent]['ptp_sp'] += $row['ptp_sp'];
	}
		
	// ------- get data PTP-BP  (108)	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_bp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=108 %s
			GROUP BY tl, agent",  $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_bp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=108 %s
			GROUP BY tl, agent",  $start_now, $start_now, $ProductId );		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );   
	//printf("<pre>%s</pre><hr>", $sql);	
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) {
			$agent =$row['agent'];
			$agent_data[$agent]['ptp_bp'] += $row['ptp_bp'];
		}
		
// ------------- data valid ( VALID )------------------
	
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_valid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=103 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_valid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=103 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );		
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );	
	// printf("<!-- <pre>%s</pre><hr> -->", $sql);	
		$res = @mysql_query($sql);
		if($res)
			while ($row = @mysql_fetch_array($res)) 
		{
			$agent = $row['agent'];
			$agent_data[$agent]['data_valid']+= $row['data_valid'];
		}	
		
		
	// -------------- 111 < MV >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_mv
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = '111' %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_mv
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = '111' %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['agent'];
			$agent_data[$agent]['data_mv']+= $row['data_mv'];
		}
					
					
	// -------------- 113 < WN >/BUSY LINE	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_wn
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=113 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_wn
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=113 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);
			
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_wn']+= $row['data_wn'];
	}
	
	
	// -------------- 112 < NBP >	

	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_nbp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 112 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_nbp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 112 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_nbp']+= $row['data_nbp'];
	 }
	 
	// -------------- 105 < NEGO >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_ng
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=105 %s  
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_ng
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=105 %s  
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_ng']+= $row['data_ng'];
	}
	
	// -------------- 102 < OS >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_os
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=102 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_os
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=102 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_os']+= $row['data_os'];
	}

		
	// -------------- 114 < COMPLAIN >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_com
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=114 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_com
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=114 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
			 
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
//	printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_com']+= $row['data_com'];
	}
	
	
	// -------------- 104 < OP-ON PROSPECT >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_op
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=104 %s 
		GROUP BY tl, agent", $start_date, $end_date, $ProductId );
		
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_op
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=104 %s 
		GROUP BY tl, agent", $start_now, $start_now, $ProductId);	
		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_op']+= $row['data_op'];
	}	
		
	// -------------- 110 < DECEASE >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_dcs
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 110 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_dcs
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 110 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
//	printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['agent'];
		$agent_data[$agent]['data_dcs']+= $row['data_dcs'];
	}
	
	// ========= default  =
	
		$cnt				= 0;
		$totDataSize 		= 0;
		$totDataVol 		= 0;
		$totDataUtil 		= 0;
		$totVolUtil 		= 0;
		$totAgentDial 		= 0;
		$totVolPtpNew 		= 0;
		$totVolPtpPop 		= 0;
		$totVolPtpPaid 		= 0;
		$totVolPtpSp 		= 0;
		$totVolPtpBp 		= 0;
		$totDataValid    	= 0;
		$totDataMV			= 0;
		$totDataNK			= 0;
		$totDataWN			= 0;
		$totTotalSkip		= 0;
		$totTotalPR			= 0;
		$totTotalNego		= 0;
		$totTotalOS			= 0;
		$totTotalComplain   = 0;
		$totVolTotalPtp  	= 0;
		$totContacted		= 0;
		$totTotalReach		= 0;
		$totTotalDecease  	= 0; 
		$totTotalMV  		= 0; 
		$totTotalNBP  		= 0; 
		$totTotalBusy  		= 0; 
	 
 if( is_array($Agent) ) 
	 foreach( $Agent as $AgentId ) 
{
	//printf( "my name is : %s<br>", $ArTL[$AgentId]);
	
	$cnt++;
	
	
	$vDataSize 		= ($agent_data[$AgentId]['datasize'] ? $agent_data[$AgentId]['datasize'] : 0);
	$vDataVol 		= ($agent_data[$AgentId]['datavol'] ? $agent_data[$AgentId]['datavol'] : 0);
	$vDataUtil 		= ($agent_data[$AgentId]['u_data'] ? $agent_data[$AgentId]['u_data'] : 0);
	$vVolUtil 		= ($agent_data[$AgentId]['u_vol'] ? $agent_data[$AgentId]['u_vol'] : 0);
	$vAgentDial 	= ($agent_data[$AgentId]['agent_dial'] ? $agent_data[$AgentId]['agent_dial'] : 0);
	$vVolPtpNew 	= ($agent_data[$AgentId]['ptp_new'] ? $agent_data[$AgentId]['ptp_new'] : 0);
	$vVolPtpPop 	= ($agent_data[$AgentId]['ptp_pop'] ? $agent_data[$AgentId]['ptp_pop'] : 0);
	$vVolPtpPaid 	= ($agent_data[$AgentId]['ptp_paid'] ? $agent_data[$AgentId]['ptp_paid'] : 0);
	$vVolPtpSp 		= ($agent_data[$AgentId]['ptp_sp'] ? $agent_data[$AgentId]['ptp_sp'] : 0);
	$vVolPtpBp 		= ($agent_data[$AgentId]['ptp_bp'] ? $agent_data[$AgentId]['ptp_bp'] : 0);
	$vDataValid     = (($agent_data[$AgentId]['data_valid'] ? $agent_data[$AgentId]['data_valid'] : 0)+
					  ($agent_data[$AgentId]['data_nk'] ? $agent_data[$AgentId]['data_nk'] : 0));
	
	$vDataMV		= ($agent_data[$AgentId]['data_mv'] ? $agent_data[$AgentId]['data_mv'] : 0);
	$vDataNBP		= ($agent_data[$AgentId]['data_nbp'] ? $agent_data[$AgentId]['data_nbp'] : 0);
	$vDataBusy		= ($agent_data[$AgentId]['data_wn'] ? $agent_data[$AgentId]['data_wn'] : 0);
	$vDataDecease	= ($agent_data[$AgentId]['data_dcs'] ? $agent_data[$AgentId]['data_dcs'] : 0);
	
	$vTotalSkip		= ($vDataMV+$vDataNBP+$vDataBusy);
	$vTotalPR		= ($agent_data[$AgentId]['data_op'] ? $agent_data[$AgentId]['data_op'] : 0);
	$vTotalNego		= ($agent_data[$AgentId]['data_ng'] ? $agent_data[$AgentId]['data_ng'] : 0);
	$vTotalOS		= ($agent_data[$AgentId]['data_os'] ? $agent_data[$AgentId]['data_os'] : 0);
	$vTotalComplain = ($agent_data[$AgentId]['data_com'] ? $agent_data[$AgentId]['data_com'] : 0);
	$vVolTotalPtp  	= ($vVolPtpNew+$vVolPtpPop+$vVolPtpPaid+$vVolPtpSp+$vVolPtpBp);
	$vContacted		= ($vTotalNego);
	$vTotalReach	= ($vVolTotalPtp+$vContacted+$vDataValid);	
	
	$persenUtil 	= ($vDataSize?(($vDataUtil *100)/$vDataSize):0);
	$persenPtp 		= ($vDataUtil?(($vVolTotalPtp/$vDataUtil)*100) : 0);
	$persenValid 	= ($vDataUtil?(($vDataValid/$vDataUtil)*100) : 0);
	$persenSkip 	= ($vDataUtil?(($vTotalSkip/$vDataUtil)*100) : 0);
	$persenPR		= ($vDataUtil?(($vTotalPR/$vDataUtil)*100) : 0);
	$persenNego		= ($vDataUtil?(($vTotalNego/$vDataUtil)*100) : 0);
	$persenOS		= ($vDataUtil?(($vTotalOS/$vDataUtil)*100) : 0);
	$persenComplain = ($vDataUtil?(($vTotalComplain/$vDataUtil)*100) : 0);
	$persenReach 	= ($vDataUtil?(($vTotalReach/$vDataUtil)*100) : 0);
	
	$persenDecease	= ($vDataUtil?(($vDataDecease/$vDataUtil)*100) : 0);
	$persenNBP 		= ($vDataUtil?(($vDataNBP/$vDataUtil)*100) : 0);
	$persenMV		= ($vDataUtil?(($vDataMV/$vDataUtil)*100) : 0);
	$persenBusy		= ($vDataUtil?(($vDataBusy/$vDataUtil)*100) : 0);
	
	
	
	echo "<tr>";
		echo '<td nowrap>'. $cnt .'</td>';
		
		printf('<td nowrap>%s</td>', ( $ArTL[$AgentId] ? $ArTL[$AgentId] : sprintf("<b>%s</b>", showAgentName( $AgentId))));
			
		echo '<td align="right">'.number_format($vDataSize).'</td>'
			.'<td align="right">'.number_format($vDataVol).'</td>'
			.'<td align="right">'.number_format($vDataUtil).'</td>'
			.'<td align="right">'.number_format($vVolUtil).'</td>'
			.'<td align="right">'.number_format($vAgentDial).'</td>';
			
		printf ('<td  align="right">%.2f</td>', $persenUtil);
		
		echo  '<td  align="right">'.number_format($agent_data[$AgentId]['ptp_new']).'</td>' 
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_pop']).'</td>'
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_paid']).'</td>'
			// .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_sp']).'</td>'
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_bp']).'</td>'
			 .'<td align="right">'.number_format($vVolTotalPtp).'</td>';
			 
		printf ('<td  align="right">%.2f</td>', $persenPtp);
		
		echo '<td align="right">'.number_format($vDataValid).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenValid);
		
		/*
		echo  '<td align="right">'.number_format($vDataMV).'</td>'
			 .'<td align="right">'.number_format($vDataNK).'</td>'
			 .'<td align="right">'.number_format($vDataWN).'</td>'
			 .'<td align="right">'.number_format($vTotalSkip).'</td>';
			 */
		//printf ('<td  align="right">%.2f</td>', $persenSkip);
		echo '<td align="right">'.number_format($vTotalPR).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenPR);
		echo '<td align="right">'.number_format($vTotalNego).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenNego);
		echo '<td align="right">'.number_format($vTotalOS).'</td>';
		
		
		echo '<td align="right">'.number_format($vDataDecease).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenDecease);
		
		echo '<td align="right">'.number_format($vDataMV).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenMV);
		
		echo '<td align="right">'.number_format($vDataNBP).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenNBP);
		
		echo '<td align="right">'.number_format($vTotalBusy).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenBusy);

		echo '<td align="right">'.number_format($vTotalComplain).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenComplain);
		
		echo '<td align="right">'.number_format($vTotalReach).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenReach);
		echo '</tr>';
		
		$totDataSize 		+= $vDataSize;
		$totDataVol 		+= $vDataVol;
		$totDataUtil 		+= $vDataUtil;
		$totVolUtil 		+= $vVolUtil;
		$totAgentDial 		+= $vAgentDial;
		$totVolPtpNew 		+= $vVolPtpNew;
		$totVolPtpPop 		+= $vVolPtpPop;
		$totVolPtpPaid 		+= $vVolPtpPaid;
		$totVolPtpSp 		+= $vVolPtpSp;
		$totVolPtpBp 		+= $vVolPtpBp;
		$totDataValid   	+= $vDataValid;
		//$totDataMV			+= $vDataMV;
		//$totDataNK			+= $vDataNK;
		//$totDataWN			+= $vDataWN;
		$totTotalSkip		+= $vTotalSkip;
		$totTotalPR			+= $vTotalPR;
		$totTotalNego		+= $vTotalNego;
		$totTotalOS			+= $vTotalOS;
		$totTotalComplain  	+= $vTotalComplain;
		$totVolTotalPtp  	+= $vVolTotalPtp;
		$totContacted		+= $vContacted;
		$totTotalReach		+= $vTotalReach;
		
		$totTotalDecease  	+= $vDataDecease;
		$totTotalMV  		+= $vDataMV;
		$totTotalNBP  		+= $vDataNBP;
		$totTotalBusy  		+= $vTotalBusy;
		
	}
	
	$totPersenUtil 		= ($totDataSize?(($totDataUtil *100)/$totDataSize):0);
	$totPersenPtp 		= ($totDataUtil?(($totVolTotalPtp/$totDataUtil)*100) : 0);
	$totPersenValid 	= ($totDataUtil?(($totDataValid/$totDataUtil)*100) : 0);
	$totPersenSkip 		= ($totDataUtil?(($totTotalSkip/$totDataUtil)*100) : 0);
	$totPersenPR		= ($totDataUtil?(($totTotalPR/$totDataUtil)*100) : 0);
	$totPersenNego		= ($totDataUtil?(($totTotalNego/$totDataUtil)*100) : 0);
	$totPersenOS		= ($totDataUtil?(($totTotalOS/$totDataUtil)*100) : 0);
	$totPersenComplain  = ($totDataUtil?(($totTotalComplain/$totDataUtil)*100) : 0);
	$totPersenReach 	= ($totDataUtil?(($totTotalReach/$totDataUtil)*100) : 0);
	
	$totPersenDecease  	+= ($totDataUtil?(($totTotalDecease/$totDataUtil)*100) : 0);
	$totPersentMV  		+= ($totDataUtil?(($totTotalMV/$totDataUtil)*100) : 0);
	$totPersentNBP  	+= ($totDataUtil?(($totTotalNBP/$totDataUtil)*100) : 0);
	$totPersenBusy  	+= ($totDataUtil?(($totTotalBusy/$totDataUtil)*100) : 0);
		
		
	
	echo "<tr>";
		echo '<td class="head"  align="center" colspan=2 nowrap ><b>TOTAL</b></td>'
			.'<td class="head"  align="right">'.number_format($totDataSize).'</td>'
			.'<td class="head"  align="right">'.number_format($totDataVol).'</td>'
			.'<td class="head"  align="right">'.number_format($totDataUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($totVolUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($totAgentDial).'</td>';
			
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenUtil);
		
		echo  '<td  class="head"  align="right">'.number_format($totVolPtpNew).'</td>' 
			 .'<td class="head"  align="right">'.number_format($totVolPtpPop).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolPtpPaid).'</td>'
			// .'<td class="head"  align="right">'.number_format($totVolPtpSp).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolPtpBp).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolTotalPtp).'</td>';
			 
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenPtp);
		echo '<td class="head"  align="right">'.number_format($totDataValid).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenValid);
		
		// echo  '<td class="head"  align="right">'.number_format($totDataMV).'</td>'
		// .'<td class="head"  align="right">'.number_format($totDataNK).'</td>'
		// .'<td class="head"  align="right">'.number_format($totDataWN).'</td>'
		// .'<td class="head"  align="right">'.number_format($totTotalSkip).'</td>';
		//printf ('<td  class="head"  align="right">%.2f</td>', $totPersenSkip);
		
		echo '<td class="head"  align="right">'.number_format($totTotalPR).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenPR);
		echo '<td class="head"  align="right">'.number_format($totTotalNego).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenNego);
		echo '<td class="head"  align="right">'.number_format($totTotalOS).'</td>';
		
		echo '<td class="head"  align="right">'.number_format($totTotalDecease).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenDecease);
		
		echo '<td class="head"  align="right">'.number_format($totTotalMV).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersentMV);
		
		echo '<td class="head"  align="right">'.number_format($totTotalNBP).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersentNBP);
		
		echo '<td class="head"  align="right">'.number_format($totTotalBusy).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenBusy);
		

		echo '<td class="head"  align="right">'.number_format($totTotalComplain).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenComplain);
		
		echo '<td class="head"  align="right">'.number_format($totTotalReach).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenReach);
		
		echo '</tr></table><br>';
}

//--------------------------------------------------------------------------
/*
 * @ package 		ShowSummaryReportByTL
 */
 
function ShowSummaryReportByTL()
{ 
  
  $out 			= new EUI_Object( _get_all_request() );
  $start_now 	= date('Y-m-d');
  $Agent 		= $out->get_array_value('user_tl');
  $start_date   = $out->get_value('start_date','_getDateEnglish');
  $end_date 	= $out->get_value('end_date','_getDateEnglish');
  $product 		= $out->get_value('product');
  $ArTL  		= showTl();
  
// ----------- if ----------------------------------------------
 $flags = false;
 if ( strtotime( $end_date ) >= strtotime( $start_now ) ){
	$flags = true;
 }
 
 
 // ------------- on data product  filter data ---------
  
  $ProductId = ( is_null($product) ? "" : sprintf("AND b.deb_cmpaign_id IN (%d)", $product) );
  $ProductId2 = ( is_null($product) ? "" : sprintf("AND a.deb_cmpaign_id IN (%d)", $product) );

  showHeaderSummaryTL("TL Name");
  
 // Get data size and volume ------------------------------------
	$agent_data = array();
	
	$sql = sprintf("SELECT  
				c.tl_id as agent,
				COUNT(*) as datasize, 
				SUM(a.deb_amount_wo) AS datavol 
			FROM t_gn_debitur a USE INDEX (unik)
			INNER JOIN cc_agent b on a.deb_agent=b.userid
			INNER JOIN t_tx_agent c on b.userid=c.id
			WHERE 1=1 %s
			GROUP BY agent",  $ProductId2);
			
	
	//printf("<pre>%s</pre><hr>", $sql);

	// -------------- echo $sql ----------------------;		 
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$data = array();
		$agent = $row['agent'];
		$data['datasize'] += $row['datasize'];
		$data['datavol'] += $row['datavol'];
		$agent_data[$agent] = $data;
	}
	
	// --------------- Get data utilization ----------------------------------------
	 $sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT a.CustomerId) AS u_data, 
				SUM(DISTINCT b.deb_amount_wo) AS u_vol
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId=b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' %s
			GROUP BY  tl, agent", $start_date, $end_date, $ProductId );
			
		$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT a.CustomerId) AS u_data, 
				SUM(DISTINCT b.deb_amount_wo) AS u_vol
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId=b.deb_id
			ILEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' %s
			GROUP BY  tl, agent", $start_now, $start_now, $ProductId );
			
		$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
		//printf("<pre>%s</pre><hr>", $sql);
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['tl'];
			$agent_data[$agent]['u_data']+= $row['u_data'];
			$agent_data[$agent]['u_vol'] += $row['u_vol'];
		}
	
	// Get Call Initiated -------------------------------------------
	
		$sql = sprintf( 
				"select 
					count(*) as agent_dial, 
				c.tl_id as agent 
				from cc_call_session a ,cc_agent b, t_tx_agent c  
				where a.agent_id = b.id and b.userid=c.id  
				and a.start_time >= '%s 00:00:00' 
				and a.start_time <= '%s 23:59:59' 
				group by agent", $start_date, $end_date );
		
	//	printf("<pre>%s</pre><hr>", $sql);	
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res))  {
			$agent = $row['agent'];
			$agent_data[$agent]['agent_dial']+= $row['agent_dial'];
		}
		
	// -------------- get PTP New -------------------------------
		$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_new
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id 
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=106 %s
			GROUP BY  tl, agent", $start_date, $end_date, $ProductId);
			
		$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_new
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id 
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND e.CallReasonCode=106 %s
			GROUP BY  tl, agent",$start_now, $start_now, $ProductId );	
			
		$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
		
	   
		//printf("<pre>%s</pre><hr>", $sql);	
		
		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['tl'];
			$agent_data[$agent]['ptp_new']+= $row['ptp_new'];
		}
		
	// ------- get data PTP - POP (107)
	
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_pop
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >='%s 00:00:00' 
				AND a.CallHistoryCreatedTs <='%s 23:59:59' 
				AND a.CallAccountStatus = 107 %s
			GROUP BY  tl, agent", $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_pop
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >='%s 00:00:00' 
				AND a.CallHistoryCreatedTs <='%s 23:59:59' 
				AND a.CallAccountStatus = 107 %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
	
	//printf("<pre>%s</pre><hr>", $sql);	
	 $res = @mysql_query($sql);
	 while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['ptp_pop']+= $row['ptp_pop'];
	 }
		
	// ------- get data PTP - PAID (109)
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_paid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus = 109 %s
			GROUP BY tl, agent", $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_paid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus = 109 %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );   
	//printf("<pre>%s</pre><hr>", $sql);	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['ptp_paid']+= $row['ptp_paid'];
	}
	
	// ------- get data SETTLE - PAYMENT (SP) (0)	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_sp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >= '%s 00:00:00' 
				AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
				AND a.CallAccountStatus = '0' %s
			GROUP BY tl, agent",  $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_sp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
				AND a.CallHistoryCreatedTs >= '%s 00:00:00' 
				AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
				AND a.CallAccountStatus = '0' %s
			GROUP BY tl, agent",  $start_now, $start_now, $ProductId );		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );   
	//printf("<pre>%s</pre><hr>", $sql);	
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent =$row['tl'];
		$agent_data[$agent]['ptp_sp'] += $row['ptp_sp'];
	}
		
	// ------- get data PTP-BP  (108)	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_bp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=108 %s
			GROUP BY tl, agent",  $start_date, $end_date, $ProductId);
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_bp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE 1=1 
			AND a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=108 %s
			GROUP BY tl, agent",  $start_now, $start_now, $ProductId );		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );   
	//printf("<pre>%s</pre><hr>", $sql);	
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) {
			$agent =$row['tl'];
			$agent_data[$agent]['ptp_bp'] += $row['ptp_bp'];
		}
		
// ------------- data valid ( VALID )------------------
	
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_valid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=103 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_valid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=103 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );		
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
		$res = @mysql_query($sql);
		if($res)
			while ($row = @mysql_fetch_array($res)) 
		{
			$agent = $row['tl'];
			$agent_data[$agent]['data_valid']+= $row['data_valid'];
		}	
		
		
	// -------------- 111 < MV >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_mv
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = '111' %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_mv
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = '111' %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['tl'];
			$agent_data[$agent]['data_mv']+= $row['data_mv'];
		}
					
					
	// -------------- 113 < WN >/BUSY LINE	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_wn
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=113 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_wn
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=113 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);
			
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_wn']+= $row['data_wn'];
	}
	
	
	// -------------- 112 < NBP >	

	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_nbp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 112 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_nbp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 112 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_nbp']+= $row['data_nbp'];
	 }
	 
	// -------------- 105 < NEGO >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_ng
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=105 %s  
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_ng
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=105 %s  
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_ng']+= $row['data_ng'];
	}
	
	// -------------- 102 < OS >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_os
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=102 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_os
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=102 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_os']+= $row['data_os'];
	}

		
	// -------------- 114 < COMPLAIN >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_com
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=114 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_com
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=114 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
			 
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
//	printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_com']+= $row['data_com'];
	}
	
	
	// -------------- 104 < OP-ON PROSPECT >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_op
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=104 %s 
		GROUP BY tl, agent", $start_date, $end_date, $ProductId );
		
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_op
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=104 %s 
		GROUP BY tl, agent", $start_now, $start_now, $ProductId);	
		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_op']+= $row['data_op'];
	}	
		
	// -------------- 110 < DECEASE >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_dcs
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 110 %s 
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId as agent, 
				COUNT(DISTINCT b.deb_id) AS data_dcs
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 110 %s 
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$agent = $row['tl'];
		$agent_data[$agent]['data_dcs']+= $row['data_dcs'];
	}
	
		$cnt = 0;
		$totData = 0;
		$totVol = 0;
		$totUData = 0;
		$totUVol = 0;
		$totCall = 0;
		$totPOP = 0;
		$totRESP = 0;
		$totSP = 0;
		$totRP = 0;
		$totOP = 0;
		$totBP = 0;
		$group = -1;
		$tl = "";
		$subTotData = 0;
		$subTotVol  = 0;
		$subTotUData = 0;
		$subTotUVol = 0;
		$subTotCall = 0;
		$subTotPop	= 0;
		$subTotPop2	= 0;
		$subTotPop3	= 0;
		$subTotPop4	= 0;
		$subTotResp	= 0;
		$subTotSp	= 0;
		$subTotRp	= 0;
		$subTotOp	= 0;
		$subTotBp	= 0;
		$subTotRpBpOp 	= 0;
		$subTotPtpdata 	= 0;
		$subTotNadata	= 0;
		$subTotNbpdata	= 0;
		$subTotValid	= 0;
		$subTotMvdata	= 0;
		$subTotWndata	= 0;
		$subTotNkdata	= 0;
		$subTotSkip		= 0;
		$subTotReach	= 0;	
		
	// echo "<pre>";
		// print_r($agent_data);
	// echo "</pre>";
	 
 if( is_array($Agent) ) 
	 foreach( $Agent as $AgentId ) 
{
	$cnt++;
	
	$vDataSize 		= ($agent_data[$AgentId]['datasize'] ? $agent_data[$AgentId]['datasize'] : 0);
	$vDataVol 		= ($agent_data[$AgentId]['datavol'] ? $agent_data[$AgentId]['datavol'] : 0);
	$vDataUtil 		= ($agent_data[$AgentId]['u_data'] ? $agent_data[$AgentId]['u_data'] : 0);
	$vVolUtil 		= ($agent_data[$AgentId]['u_vol'] ? $agent_data[$AgentId]['u_vol'] : 0);
	$vAgentDial 	= ($agent_data[$AgentId]['agent_dial'] ? $agent_data[$AgentId]['agent_dial'] : 0);
	$vVolPtpNew 	= ($agent_data[$AgentId]['ptp_new'] ? $agent_data[$AgentId]['ptp_new'] : 0);
	$vVolPtpPop 	= ($agent_data[$AgentId]['ptp_pop'] ? $agent_data[$AgentId]['ptp_pop'] : 0);
	$vVolPtpPaid 	= ($agent_data[$AgentId]['ptp_paid'] ? $agent_data[$AgentId]['ptp_paid'] : 0);
	$vVolPtpSp 		= ($agent_data[$AgentId]['ptp_sp'] ? $agent_data[$AgentId]['ptp_sp'] : 0);
	$vVolPtpBp 		= ($agent_data[$AgentId]['ptp_bp'] ? $agent_data[$AgentId]['ptp_bp'] : 0);
	$vDataValid     = (($agent_data[$AgentId]['data_valid'] ? $agent_data[$AgentId]['data_valid'] : 0)+
					  ($agent_data[$AgentId]['data_nk'] ? $agent_data[$AgentId]['data_nk'] : 0));		
	
	$vDataMV		= ($agent_data[$AgentId]['data_mv'] ? $agent_data[$AgentId]['data_mv'] : 0);
	$vDataNBP		= ($agent_data[$AgentId]['data_nbp'] ? $agent_data[$AgentId]['data_nbp'] : 0);
	$vDataBusy		= ($agent_data[$AgentId]['data_wn'] ? $agent_data[$AgentId]['data_wn'] : 0);	
	// $vDataDecease	= 0; //($agent_data[$AgentId]['data_wn'] ? $agent_data[$AgentId]['data_wn'] : 0);
	$vDataDecease	= ($agent_data[$AgentId]['data_dcs'] ? $agent_data[$AgentId]['data_dcs'] : 0);	
		
	$vTotalPR		= ($agent_data[$AgentId]['data_op'] ? $agent_data[$AgentId]['data_op'] : 0);
	$vTotalNego		= ($agent_data[$AgentId]['data_ng'] ? $agent_data[$AgentId]['data_ng'] : 0);
	$vTotalOS		= ($agent_data[$AgentId]['data_os'] ? $agent_data[$AgentId]['data_os'] : 0);
	$vTotalComplain = ($agent_data[$AgentId]['data_com'] ? $agent_data[$AgentId]['data_com'] : 0);
	$vVolTotalPtp  	= ($vVolPtpNew+$vVolPtpPop+$vVolPtpPaid+$vVolPtpSp+$vVolPtpBp);
	$vContacted		= ($vTotalNego);
	$vTotalReach	= ($vVolTotalPtp+$vContacted+$vDataValid);	
		
	
	$persenUtil 	= ($vDataSize?(($vDataUtil *100)/$vDataSize):0);
	$persenPtp 		= ($vDataUtil?(($vVolTotalPtp/$vDataUtil)*100) : 0);
	$persenValid 	= ($vDataUtil?(($vDataValid/$vDataUtil)*100) : 0);
	$persenSkip 	= ($vDataUtil?(($vTotalSkip/$vDataUtil)*100) : 0);
	$persenPR		= ($vDataUtil?(($vTotalPR/$vDataUtil)*100) : 0);
	$persenNego		= ($vDataUtil?(($vTotalNego/$vDataUtil)*100) : 0);
	$persenOS		= ($vDataUtil?(($vTotalOS/$vDataUtil)*100) : 0);
	$persenComplain = ($vDataUtil?(($vTotalComplain/$vDataUtil)*100) : 0);
	$persenReach 	= ($vDataUtil?(($vTotalReach/$vDataUtil)*100) : 0);
	$persenDecease	= ($vDataUtil?(($vDataDecease/$vDataUtil)*100) : 0);
	$persenNBP 		= ($vDataUtil?(($vDataNBP/$vDataUtil)*100) : 0);
	$persenMV		= ($vDataUtil?(($vDataMV/$vDataUtil)*100) : 0);
	$persenBusy		= ($vDataUtil?(($vDataBusy/$vDataUtil)*100) : 0);
	
	echo "<tr>";
		echo '<td nowrap>'. $cnt .'</td>'
			.'<td nowrap>'.$ArTL[$AgentId].'</td>'
			.'<td align="right">'.number_format($vDataSize).'</td>'
			.'<td align="right">'.number_format($vDataVol).'</td>'
			.'<td align="right">'.number_format($vDataUtil).'</td>'
			.'<td align="right">'.number_format($vVolUtil).'</td>'
			.'<td align="right">'.number_format($vAgentDial).'</td>';
			
		printf ('<td  align="right">%.2f</td>', $persenUtil);
		
		echo  '<td  align="right">'.number_format($agent_data[$AgentId]['ptp_new']).'</td>' 
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_pop']).'</td>'
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_paid']).'</td>'
			 //.'<td align="right">'.number_format($agent_data[$AgentId]['ptp_sp']).'</td>'
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_bp']).'</td>'
			 .'<td align="right">'.number_format($vVolTotalPtp).'</td>';
			 
		printf ('<td  align="right">%.2f</td>', $persenPtp);
		echo '<td align="right">'.number_format($vDataValid).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenValid);
		
		// echo  '<td align="right">'.number_format($vDataMV).'</td>'
			 // .'<td align="right">'.number_format($vDataNK).'</td>'
			 // .'<td align="right">'.number_format($vDataWN).'</td>'
			 
			 // .'<td align="right">'.number_format($vTotalSkip).'</td>';
		// printf ('<td  align="right">%.2f</td>', $persenSkip);
		
		echo '<td align="right">'.number_format($vTotalPR).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenPR);
		
		echo '<td align="right">'.number_format($vTotalNego).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenNego);
		printf('<td align="right">%s</td>', number_format($vTotalOS));
		
		echo '<td align="right">'.number_format($vDataDecease).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenDecease);
		
		echo '<td align="right">'.number_format($vDataMV).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenMV);
		
		echo '<td align="right">'.number_format($vDataNBP).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenNBP);
		
		echo '<td align="right">'.number_format($vTotalBusy).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenBusy);
		
		echo '<td align="right">'.number_format($vTotalComplain).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenComplain);
		
		echo '<td align="right">'.number_format($vTotalReach).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenReach);
		echo '</tr>';
		
		
		// --------------- on total bottom ----------------------------------------
		$totDataSize 		+= $vDataSize;
		$totDataVol 		+= $vDataVol;
		$totDataUtil 		+= $vDataUtil;
		$totVolUtil 		+= $vVolUtil;
		$totAgentDial 		+= $vAgentDial;
		$totVolPtpNew 		+= $vVolPtpNew;
		$totVolPtpPop 		+= $vVolPtpPop;
		$totVolPtpPaid 		+= $vVolPtpPaid;
		$totVolPtpSp 		+= $vVolPtpSp;
		$totVolPtpBp 		+= $vVolPtpBp;
		$totDataValid   	+= $vDataValid;
		$totTotalSkip		+= $vTotalSkip;
		$totTotalPR			+= $vTotalPR;
		$totTotalNego		+= $vTotalNego;
		$totTotalOS			+= $vTotalOS;
		$totTotalComplain  	+= $vTotalComplain;
		$totVolTotalPtp  	+= $vVolTotalPtp;
		$totContacted		+= $vContacted;
		$totTotalReach		+= $vTotalReach;
		
		$totTotalDecease  	+= $vDataDecease;
		$totTotalMV  		+= $vDataMV;
		$totTotalNBP  		+= $vDataNBP;
		$totTotalBusy  		+= $vDataBusy;
		
		// $TotDataSize += $vDataSize;
		// $TotDataVol += $vDataVol;
		// $TotDataUtil += $vDataUtil;
		// $TotVolUtil += $vVolUtil;
		// $TotAgentDial += $vAgentDial;
		// $TotpersenUtil 	= ($TotDataSize?(($TotDataUtil *100)/$TotDataSize):0);
		
		// $TotVolPtpNew += $vVolPtpNew;
		// $TotVolPtpPop += $vVolPtpPop;
		// $TotVolPtpPaid += $vVolPtpPaid;
		// $TotVolPtpSp += $vVolPtpSp;
		// $TotVolPtpBp += $vVolPtpBp;
		// $TotDataValid += $vDataValid;
		// $TotVolTotalPtp	= ($TotVolPtpNew+$TotVolPtpPop+$TotVolPtpPaid+$TotVolPtpSp+$TotVolPtpBp);
		// $TotpersenPtp 	= ($TotDataUtil?(($TotVolTotalPtp/$TotDataUtil)*100) : 0);
		// $TotpersenValid = ($TotDataUtil?(($TotVolTotalPtp/$TotDataUtil)*100) : 0);
	
	}
	 // =================== total on bootom =================
    $totPersenUtil 		= ($totDataSize?(($totDataUtil *100)/$totDataSize):0);
	$totPersenPtp 		= ($totDataUtil?(($totVolTotalPtp/$totDataUtil)*100) : 0);
	$totPersenValid 	= ($totDataUtil?(($totDataValid/$totDataUtil)*100) : 0);
	$totPersenSkip 		= ($totDataUtil?(($totTotalSkip/$totDataUtil)*100) : 0);
	$totPersenPR		= ($totDataUtil?(($totTotalPR/$totDataUtil)*100) : 0);
	$totPersenNego		= ($totDataUtil?(($totTotalNego/$totDataUtil)*100) : 0);
	$totPersenOS		= ($totDataUtil?(($totTotalOS/$totDataUtil)*100) : 0);
	$totPersenComplain  = ($totDataUtil?(($totTotalComplain/$totDataUtil)*100) : 0);
	$totPersenReach 	= ($totDataUtil?(($totTotalReach/$totDataUtil)*100) : 0);
	
	$totPersenDecease  	+= ($totDataUtil?(($totTotalDecease/$totDataUtil)*100) : 0);
	$totPersentMV  		+= ($totDataUtil?(($totTotalMV/$totDataUtil)*100) : 0);
	$totPersentNBP  	+= ($totDataUtil?(($totTotalNBP/$totDataUtil)*100) : 0);
	$totPersenBusy  	+= ($totDataUtil?(($totTotalBusy/$totDataUtil)*100) : 0);
	
	echo "<tr>";
		echo '<td class="head"  align="center" colspan=2 nowrap ><b>TOTAL</b></td>'
			.'<td class="head"  align="right">'.number_format($totDataSize).'</td>'
			.'<td class="head"  align="right">'.number_format($totDataVol).'</td>'
			.'<td class="head"  align="right">'.number_format($totDataUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($totVolUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($totAgentDial).'</td>';
			
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenUtil);
		
		echo  '<td  class="head"  align="right">'.number_format($totVolPtpNew).'</td>' 
			 .'<td class="head"  align="right">'.number_format($totVolPtpPop).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolPtpPaid).'</td>'
			// .'<td class="head"  align="right">'.number_format($totVolPtpSp).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolPtpBp).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolTotalPtp).'</td>';
			 
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenPtp);
		echo '<td class="head"  align="right">'.number_format($totDataValid).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenValid);
		/*
		echo  '<td class="head"  align="right">'.number_format($totDataMV).'</td>'
			 .'<td class="head"  align="right">'.number_format($totDataNK).'</td>'
			 .'<td class="head"  align="right">'.number_format($totDataWN).'</td>'
			 .'<td class="head"  align="right">'.number_format($totTotalSkip).'</td>';
			*/
			
		//printf ('<td  class="head"  align="right">%.2f</td>', $totPersenSkip);
		echo '<td class="head"  align="right">'.number_format($totTotalPR).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenPR);
		echo '<td class="head"  align="right">'.number_format($totTotalNego).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenNego);
		echo '<td class="head"  align="right">'.number_format($totTotalOS).'</td>';
		
		
		echo '<td class="head"  align="right">'.number_format($totTotalDecease).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenDecease);
		
		echo '<td class="head"  align="right">'.number_format($totTotalMV).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersentMV);
		
		echo '<td class="head"  align="right">'.number_format($totTotalNBP).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersentNBP);
		
		echo '<td class="head"  align="right">'.number_format($totTotalBusy).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenBusy);
		
		
		echo '<td class="head"  align="right">'.number_format($totTotalComplain).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenComplain);
		
		echo '<td class="head"  align="right">'.number_format($totTotalReach).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenReach);
		echo '</tr></table><br>';
 		

}


// ------------- show detail By LeaderId 

function showContentByTL( $LeaderId  = 0, $agent_data = array() )
{	


	$sql = "select a.UserId, a.full_name, a.id from t_tx_agent a where a.tl_id = $LeaderId 
			AND a.handling_type IN(4,13)
			order by a.handling_type ASC, a.id ASC";
	//echo $sql;		
	$qry = mysql_query($sql);
	$num_sec = 1;
	 while( $row = mysql_fetch_array($qry) )
	{
		$AgentId 		= $row['UserId'];
		$vDataSize 		= ($agent_data[$AgentId]['datasize'] ? $agent_data[$AgentId]['datasize'] : 0);
		$vDataVol 		= ($agent_data[$AgentId]['datavol'] ? $agent_data[$AgentId]['datavol'] : 0);
		$vDataUtil 		= ($agent_data[$AgentId]['u_data'] ? $agent_data[$AgentId]['u_data'] : 0);
		$vVolUtil 		= ($agent_data[$AgentId]['u_vol'] ? $agent_data[$AgentId]['u_vol'] : 0);
		$vAgentDial 	= ($agent_data[$AgentId]['agent_dial'] ? $agent_data[$AgentId]['agent_dial'] : 0);
		$vVolPtpNew 	= ($agent_data[$AgentId]['ptp_new'] ? $agent_data[$AgentId]['ptp_new'] : 0);
		$vVolPtpPop 	= ($agent_data[$AgentId]['ptp_pop'] ? $agent_data[$AgentId]['ptp_pop'] : 0);
		$vVolPtpPaid 	= ($agent_data[$AgentId]['ptp_paid'] ? $agent_data[$AgentId]['ptp_paid'] : 0);
		$vVolPtpSp 		= ($agent_data[$AgentId]['ptp_sp'] ? $agent_data[$AgentId]['ptp_sp'] : 0);
		$vVolPtpBp 		= ($agent_data[$AgentId]['ptp_bp'] ? $agent_data[$AgentId]['ptp_bp'] : 0);
		$vDataValid     = (($agent_data[$AgentId]['data_valid'] ? $agent_data[$AgentId]['data_valid'] : 0)+
						  ($agent_data[$AgentId]['data_nk'] ? $agent_data[$AgentId]['data_nk'] : 0));
		
		
		$vDataMV		= ($agent_data[$AgentId]['data_mv'] ? $agent_data[$AgentId]['data_mv'] : 0);
		$vDataNBP		= ($agent_data[$AgentId]['data_nbp'] ? $agent_data[$AgentId]['data_nbp'] : 0);
		$vDataBusy		= ($agent_data[$AgentId]['data_wn'] ? $agent_data[$AgentId]['data_wn'] : 0);	
		$vDataDecease	= 0; //($agent_data[$AgentId]['data_wn'] ? $agent_data[$AgentId]['data_wn'] : 0);	
		
		$vTotalSkip		= ($vDataMV+$vDataNBP+$vDataBusy);
		
		$vTotalPR		= ($agent_data[$AgentId]['data_op'] ? $agent_data[$AgentId]['data_op'] : 0);
		$vTotalNego		= ($agent_data[$AgentId]['data_ng'] ? $agent_data[$AgentId]['data_ng'] : 0);
		$vTotalOS		= ($agent_data[$AgentId]['data_os'] ? $agent_data[$AgentId]['data_os'] : 0);
		$vTotalComplain = ($agent_data[$AgentId]['data_com'] ? $agent_data[$AgentId]['data_com'] : 0);
		$vVolTotalPtp  	= ($vVolPtpNew+$vVolPtpPop+$vVolPtpPaid+$vVolPtpSp+$vVolPtpBp);
		$vContacted		= ($vTotalNego);
		$vTotalReach	= ($vVolTotalPtp+$vContacted+$vDataValid);	
		
		$persenUtil 	= ($vDataSize?(($vDataUtil *100)/$vDataSize):0);
		$persenPtp 		= ($vDataUtil?(($vVolTotalPtp/$vDataUtil)*100) : 0);
		$persenValid 	= ($vDataUtil?(($vDataValid/$vDataUtil)*100) : 0);
		$persenSkip 	= ($vDataUtil?(($vTotalSkip/$vDataUtil)*100) : 0);
		$persenPR		= ($vDataUtil?(($vTotalPR/$vDataUtil)*100) : 0);
		$persenNego		= ($vDataUtil?(($vTotalNego/$vDataUtil)*100) : 0);
		$persenOS		= ($vDataUtil?(($vTotalOS/$vDataUtil)*100) : 0);
		$persenComplain = ($vDataUtil?(($vTotalComplain/$vDataUtil)*100) : 0);
		$persenReach 	= ($vDataUtil?(($vTotalReach/$vDataUtil)*100) : 0);
		
		$persenDecease	= ($vDataUtil?(($vDataDecease/$vDataUtil)*100) : 0);
		$persenNBP 		= ($vDataUtil?(($vDataNBP/$vDataUtil)*100) : 0);
		$persenMV		= ($vDataUtil?(($vDataMV/$vDataUtil)*100) : 0);
		$persenBusy		= ($vDataUtil?(($vDataBusy/$vDataUtil)*100) : 0);
		
		
		
		echo "<tr>";
		echo '<td nowrap>'. $num_sec .'</td>'
			.'<td nowrap>'.$row['id'].' - '. $row['full_name'].'</td>'
			.'<td align="right">'.number_format($vDataSize).'</td>'
			.'<td align="right">'.number_format($vDataVol).'</td>'
			.'<td align="right">'.number_format($vDataUtil).'</td>'
			.'<td align="right">'.number_format($vVolUtil).'</td>'
			.'<td align="right">'.number_format($vAgentDial).'</td>';
			
		printf ('<td  align="right">%.2f</td>', $persenUtil);
		
		echo  '<td  align="right">'.number_format($agent_data[$AgentId]['ptp_new']).'</td>' 
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_pop']).'</td>'
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_paid']).'</td>'
			// .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_sp']).'</td>'
			 .'<td align="right">'.number_format($agent_data[$AgentId]['ptp_bp']).'</td>'
			 .'<td align="right">'.number_format($vVolTotalPtp).'</td>';
			 
		printf ('<td  align="right">%.2f</td>', $persenPtp);
		echo '<td align="right">'.number_format($vDataValid).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenValid);
		
		// echo  '<td align="right">'.number_format($vDataMV).'</td>'
			 // .'<td align="right">'.number_format($vDataNK).'</td>'
			 // .'<td align="right">'.number_format($vDataWN).'</td>'
			 // .'<td align="right">'.number_format($vTotalSkip).'</td>';
		// printf ('<td  align="right">%.2f</td>', $persenSkip);
		
		echo '<td align="right">'.number_format($vTotalPR).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenPR);
		echo '<td align="right">'.number_format($vTotalNego).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenNego);
		echo '<td align="right">'.number_format($vTotalOS).'</td>';
		
		echo '<td align="right">'.number_format($vDataDecease).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenDecease);
		
		echo '<td align="right">'.number_format($vDataMV).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenMV);
		
		echo '<td align="right">'.number_format($vDataNBP).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenNBP);
		
		echo '<td align="right">'.number_format($vTotalBusy).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenBusy);
		
		echo '<td align="right">'.number_format($vTotalComplain).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenComplain);
		
		echo '<td align="right">'.number_format($vTotalReach).'</td>';
		printf ('<td  align="right">%.2f</td>', $persenReach);
		echo '</tr>';
		
		$num_sec++;
	}
}

//--------------------------------------------------------------------------
/*
 * @ package 		ShowDetailReportByTL
 */
 
function SQL_Union($flgs, $sql ){
	if( $flgs ){
		return sprintf("UNION %s", $sql); 
	}
	return "";
} 
 

//--------------------------------------------------------------------------
/*
 * @ package 		ShowDetailReportByTL
 */
 
function ShowDetailReportByTL()
{
	
  $out = new EUI_Object( _get_all_request() );
  
// --------- date var data report --------------
  
  $Tl 			= $out->get_array_value('user_tl');
  $start_now 	= date('Y-m-d');
  $start_date  	= $out->get_value('start_date','_getDateEnglish');
  $end_date 	= $out->get_value('end_date','_getDateEnglish');
  $product 		= ($out->get_value('product') ? $out->get_value('product') : null );
  $ArTL  		= showTl();
  
  showHeaderDetailTL("Agent Name");
  $agent_data = array();
  $leader_data = array();
  
  
 // ----------- if ----------------------------------------------
 $flags = false;
 if ( strtotime( $end_date ) >= strtotime( $start_now ) ){
	$flags = true;
 }
 
 // ------------- on data product  filter data ---------
  
  $ProductId = ( is_null($product) ? "" : sprintf("AND b.deb_cmpaign_id IN (%d)", $product) );
  $ProductId2 = ( is_null($product) ? "" : sprintf("AND a.deb_cmpaign_id IN (%d)", $product) );
 
 // Get data size and volume ------------------------------------
 
	$sql = sprintf("SELECT  
				c.tl_id as tl, 
				c.UserId as agent,
				COUNT(*) as datasize, 
				SUM(a.deb_amount_wo) AS datavol 
			FROM t_gn_debitur a USE INDEX (unik)
			INNER JOIN cc_agent b on a.deb_agent=b.userid
			INNER JOIN t_tx_agent c on b.userid=c.id
			WHERE 1=1 %s
			GROUP BY tl, agent",  $ProductId2);
			
	//printf("<pre>%s</pre><hr>", $sql);
	
	// -------------- echo $sql ----------------------;		 
		
		$res = @mysql_query($sql);
		 while ($row = @mysql_fetch_array($res)) 
		{
			$leader_data[$row['tl']]['datasize']+= $row['datasize'];
			$leader_data[$row['tl']]['datavol']+= $row['datavol'];
			$agent_data[$row['tl']][$row['agent']]['datasize']+= $row['datasize'];
			$agent_data[$row['tl']][$row['agent']]['datavol']+= $row['datavol'];
		}

	// --------------- Get data utilization ----------------------------------------
	
		$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT a.CustomerId) AS u_data, 
				SUM(DISTINCT b.deb_amount_wo) AS u_vol
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId=b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' %s
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
		$sql2 = sprintf("SELECT 
				c.tl_id AS tl, 
				c.UserId as agent,
				COUNT(DISTINCT a.CustomerId) AS u_data, 
				SUM(DISTINCT b.deb_amount_wo) AS u_vol
			FROM t_gn_debitur b USE INDEX (unik)
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId=b.deb_id
			INNER JOIN t_tx_agent c on b.deb_agent=c.id
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId );
			
		$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
		//printf("data utilize : <pre>%s</pre><hr>", $sql);
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) 
		{			
			$leader_data[$row['tl']]['u_data']+= $row['u_data'];
			$leader_data[$row['tl']]['u_vol']+= $row['u_vol'];
			$agent_data[$row['tl']][$row['agent']]['u_data']+= $row['u_data'];
			$agent_data[$row['tl']][$row['agent']]['u_vol']+= $row['u_vol'];
		}
		
	// Get Call Initiated -----------------------------------------------
	
		$sql = sprintf( 
				"select count(*) as agent_dial, c.tl_id as tl, c.UserId as agent
				from cc_call_session a ,cc_agent b, t_tx_agent c  
				where a.agent_id = b.id and b.userid=c.id  
				and a.start_time >= '%s 00:00:00' 
				and a.start_time <= '%s 23:59:59' 
				group by tl, agent", $start_date, $end_date );
				
	
		//printf("<pre>%s</pre>", $sql);	
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) 
		{
			$leader_data[$row['tl']]['agent_dial']+= $row['agent_dial'];
			$agent_data[$row['tl']][$row['agent']]['agent_dial']+= $row['agent_dial'];
		}
		
		
	// ------------------------------ PTP NEW 106 ---------------------------------
		$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl,
				f.UserId as agent,	
				COUNT(DISTINCT b.deb_id) AS ptp_new
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus = 106
			%s  GROUP BY tl, agent", 
			$start_date, $end_date, $ProductId );
			
		$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl,
				f.UserId as agent,	
				COUNT(DISTINCT b.deb_id) AS ptp_new
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus = 106
			%s  GROUP BY tl, agent", 
			$start_now, $start_now, $ProductId);
			
		$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
		
	 //printf("<pre>%s</pre><hr>", $sql);
	  
	   
	  $res = @mysql_query($sql);
		 if( $res ) 
			 while ($row = @mysql_fetch_array($res)) 
		{
			$leader_data[$row['tl']]['ptp_new']+= $row['ptp_new'];
			$agent_data[$row['tl']][$row['agent']]['ptp_new']+= $row['ptp_new'];
		}
		
	// ------- get data PTP - POP (107) -------------------------------------
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_pop
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=107 %s
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_pop
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=107 %s
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
	
	//printf("<pre>%s</pre><hr>", $sql);	
	
		$res = @mysql_query($sql);
		if( $res ) 
			 while ($row = @mysql_fetch_array($res)) 
		{
			
			$leader_data[$row['tl']]['ptp_pop']+= $row['ptp_pop'];
			$agent_data[$row['tl']][$row['agent']]['ptp_pop']+= $row['ptp_pop'];
		}
		
	// ------- get data PTP - PAID (109)
	$sql1= sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_paid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=109 %s
			GROUP BY tl, agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_paid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=109 %s
			GROUP BY tl, agent", $start_now, $start_now, $ProductId);		
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	if( $res ) 
		while ($row = @mysql_fetch_array($res)) 
	{
		$leader_data[$row['tl']]['ptp_paid']+= $row['ptp_paid'];
		$agent_data[$row['tl']][$row['agent']]['ptp_paid']+= $row['ptp_paid'];
	}
		
	// ------- get data SETTLE - PAYMENT (SP) (0)	
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_sp
			FROM t_gn_debitur b
			LEFT OUTER
			JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN cc_agent d ON b.deb_agent=d.userid
			LEFT JOIN t_tx_agent f on d.userid = f.id
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=0 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId as agent,
				COUNT(DISTINCT b.deb_id) AS ptp_sp
			FROM t_gn_debitur b
			LEFT OUTER
			JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN cc_agent d ON b.deb_agent=d.userid
			LEFT JOIN t_tx_agent f on d.userid = f.id
			WHERE 1=1 
			AND a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=0 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );
	
	//printf("<pre>%s</pre><hr>", $sql);	
	$res = @mysql_query($sql);
	if( $res ) 
		while ($row = @mysql_fetch_array($res)) 
	{
		$leader_data[$row['tl']]['ptp_sp']+= $row['ptp_sp'];
		$agent_data[$row['tl']][$row['agent']]['ptp_sp']+= $row['ptp_sp'];
	}
		
	// ------- get data PTP-BP  (108)	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_bp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus =108 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS ptp_bp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus =108 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId );		
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);		
	
	$res = @mysql_query($sql);
	if( $res ) 
		while ($row = @mysql_fetch_array($res))  
	{
		$leader_data[$row['tl']]['ptp_bp']+= $row['ptp_bp'];
		$agent_data[$row['tl']][$row['agent']]['ptp_bp']+= $row['ptp_bp'];
	}
	
	// ------------- data valid ------------------
	
	$sql1 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_valid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=103 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_valid
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=103 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId );		
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
		$res = @mysql_query($sql);
		if($res)
			while ($row = @mysql_fetch_array($res)) 
		{
			$leader_data[$row['tl']]['data_valid']+= $row['data_valid'];
			$agent_data[$row['tl']][$row['agent']]['data_valid']+= $row['data_valid'];
		}	
		
		
	// -------------- 111 < MV >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_mv
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = '111' %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_mv
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = '111' %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
		$res = @mysql_query($sql);
		while ($row = @mysql_fetch_array($res)) {
			$leader_data[$row['tl']]['data_mv']+= $row['data_mv'];
			$agent_data[$row['tl']][$row['agent']]['data_mv']+= $row['data_mv'];
		}
					
					
	// -------------- 113 < WN >/BUSY LINE	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_wn
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=113 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_wn
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=113 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);
			
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_wn']+= $row['data_wn'];
		$agent_data[$row['tl']][$row['agent']]['data_wn']+= $row['data_wn'];
	}
	
	
	// -------------- 112 < NBP >	

	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_nbp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 112 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_nbp
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 112 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_nbp']+= $row['data_nbp'];
		$agent_data[$row['tl']][$row['agent']]['data_nbp']+= $row['data_nbp'];
	 }
	 
	// -------------- 105 < NEGO >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_ng
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=105 %s  
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_ng
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=105 %s  
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_ng']+= $row['data_ng'];
		$agent_data[$row['tl']][$row['agent']]['data_ng']+= $row['data_ng'];
	}
	
	// -------------- 102 < OS >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_os
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=102 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_os
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs>='%s 00:00:00' 
			AND a.CallHistoryCreatedTs<='%s 23:59:59' 
			AND a.CallAccountStatus=102 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_os']+= $row['data_os'];
		$agent_data[$row['tl']][$row['agent']]['data_os']+= $row['data_os'];
	}

		
	// -------------- 114 < COMPLAIN >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_com
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=114 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_com
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >='%s 00:00:00' 
			AND a.CallHistoryCreatedTs <='%s 23:59:59' 
			AND a.CallAccountStatus=114 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);		
			 
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
//	printf("<pre>%s</pre><hr>", $sql);
		
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_com']+= $row['data_com'];
		$agent_data[$row['tl']][$row['agent']]['data_com']+= $row['data_com'];
	}
	
	
	// -------------- 104 < OP-ON PROSPECT >	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_op
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=104 %s 
		GROUP BY tl,agent", $start_date, $end_date, $ProductId );
		
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_op
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus=104 %s 
		GROUP BY tl,agent", $start_now, $start_now, $ProductId);	
		
	   
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
	//printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_op']+= $row['data_op'];
		$agent_data[$row['tl']][$row['agent']]['data_op']+= $row['data_op'];
	}	
		
	// -------------- 110 < DECEASE >	
	
	$sql1 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_dcs
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 110 %s 
			GROUP BY tl,agent", $start_date, $end_date, $ProductId );
			
	$sql2 = sprintf("
			SELECT 
				f.tl_id AS tl, 
				f.UserId AS agent, 
				COUNT(DISTINCT b.deb_id) AS data_dcs
			FROM t_gn_debitur b
			LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId = b.deb_id
			LEFT JOIN t_tx_agent f ON f.userid = a.CreatedById
			WHERE a.CallHistoryCreatedTs >= '%s 00:00:00' 
			AND a.CallHistoryCreatedTs <= '%s 23:59:59' 
			AND a.CallAccountStatus = 110 %s 
			GROUP BY tl,agent", $start_now, $start_now, $ProductId);
			
	$sql = sprintf("%s %s", $sql1, SQL_Union($flags, $sql2 ) );		
//	printf("<pre>%s</pre><hr>", $sql);
	
	$res = @mysql_query($sql);
	while ($row = @mysql_fetch_array($res)) {
		$leader_data[$row['tl']]['data_dcs']+= $row['data_dcs'];
		$agent_data[$row['tl']][$row['agent']]['data_dcs']+= $row['data_dcs'];
	}
	
	// ========= default  ================	
	
		$cnt				= 0;
		$totDataSize 		= 0;
		$totDataVol 		= 0;
		$totDataUtil 		= 0;
		$totVolUtil 		= 0;
		$totAgentDial 		= 0;
		$totVolPtpNew 		= 0;
		$totVolPtpPop 		= 0;
		$totVolPtpPaid 		= 0;
		$totVolPtpSp 		= 0;
		$totVolPtpBp 		= 0;
		$totDataValid    	= 0;
		$totDataMV			= 0;
		$totDataNK			= 0;
		$totDataWN			= 0;
		$totTotalSkip		= 0;
		$totTotalPR			= 0;
		$totTotalNego		= 0;
		$totTotalOS			= 0;
		$totTotalComplain   = 0;
		$totVolTotalPtp  	= 0;
		$totContacted		= 0;
		$totTotalReach		= 0;
		
		$totTotalDecease  	= 0; 
		$totTotalMV  		= 0; 
		$totTotalNBP  		= 0; 
		$totTotalBusy  		= 0; 
		
	
 if( is_array($Tl) ) 
	 foreach( $Tl as $LeaderId ) 
{
	$cnt++;
	
	showContentByTL( $LeaderId ,$agent_data[$LeaderId]);
	
	
	$vDataSize 		= ($leader_data[$LeaderId]['datasize'] ? $leader_data[$LeaderId]['datasize'] : 0);
	$vDataVol 		= ($leader_data[$LeaderId]['datavol'] ? $leader_data[$LeaderId]['datavol'] : 0);
	$vDataUtil 		= ($leader_data[$LeaderId]['u_data'] ? $leader_data[$LeaderId]['u_data'] : 0);
	$vVolUtil 		= ($leader_data[$LeaderId]['u_vol'] ? $leader_data[$LeaderId]['u_vol'] : 0);
	$vAgentDial 	= ($leader_data[$LeaderId]['agent_dial'] ? $leader_data[$LeaderId]['agent_dial'] : 0);
	$vVolPtpNew 	= ($leader_data[$LeaderId]['ptp_new'] ? $leader_data[$LeaderId]['ptp_new'] : 0);
	$vVolPtpPop 	= ($leader_data[$LeaderId]['ptp_pop'] ? $leader_data[$LeaderId]['ptp_pop'] : 0);
	$vVolPtpPaid 	= ($leader_data[$LeaderId]['ptp_paid'] ? $leader_data[$LeaderId]['ptp_paid'] : 0);
	$vVolPtpSp 		= ($leader_data[$LeaderId]['ptp_sp'] ? $leader_data[$LeaderId]['ptp_sp'] : 0);
	$vVolPtpBp 		= ($leader_data[$LeaderId]['ptp_bp'] ? $leader_data[$LeaderId]['ptp_bp'] : 0);
	$vDataValid     = (($leader_data[$LeaderId]['data_valid'] ? $leader_data[$LeaderId]['data_valid'] : 0)+
					  ($leader_data[$LeaderId]['data_nbp'] ? $leader_data[$LeaderId]['data_nbp'] : 0));
	
	$vDataMV		= ($leader_data[$LeaderId]['data_mv'] ? $leader_data[$LeaderId]['data_mv'] : 0);
	$vDataNBP		= ($leader_data[$LeaderId]['data_nbp'] ? $leader_data[$LeaderId]['data_nbp'] : 0);
	$vDataBusy		= ($leader_data[$LeaderId]['data_wn'] ? $leader_data[$LeaderId]['data_wn'] : 0);
	$vDataDecease	= ($leader_data[$LeaderId]['data_dcs'] ? $leader_data[$LeaderId]['data_dcs'] : 0);
	
	
	$vTotalSkip		= ($vDataMV+$vDataNBP+$vDataBusy);
	$vTotalPR		= ($leader_data[$LeaderId]['data_op'] ? $leader_data[$LeaderId]['data_op'] : 0);
	$vTotalNego		= ($leader_data[$LeaderId]['data_ng'] ? $leader_data[$LeaderId]['data_ng'] : 0);
	$vTotalOS		= ($leader_data[$LeaderId]['data_os'] ? $leader_data[$LeaderId]['data_os'] : 0);
	$vTotalComplain = ($leader_data[$LeaderId]['data_com'] ? $leader_data[$LeaderId]['data_com'] : 0);
	$vVolTotalPtp  	= ($vVolPtpNew+$vVolPtpPop+$vVolPtpPaid+$vVolPtpSp+$vVolPtpBp);
	$vContacted		= ($vTotalNego);
	$vTotalReach	= ($vVolTotalPtp+$vContacted+$vDataValid);	
	
	$persenUtil 	= ($vDataSize?(($vDataUtil *100)/$vDataSize):0);
	$persenPtp 		= ($vDataUtil?(($vVolTotalPtp/$vDataUtil)*100) : 0);
	$persenValid 	= ($vDataUtil?(($vDataValid/$vDataUtil)*100) : 0);
	$persenSkip 	= ($vDataUtil?(($vTotalSkip/$vDataUtil)*100) : 0);
	$persenPR		= ($vDataUtil?(($vTotalPR/$vDataUtil)*100) : 0);
	$persenNego		= ($vDataUtil?(($vTotalNego/$vDataUtil)*100) : 0);
	$persenOS		= ($vDataUtil?(($vTotalOS/$vDataUtil)*100) : 0);
	$persenComplain = ($vDataUtil?(($vTotalComplain/$vDataUtil)*100) : 0);
	$persenReach 	= ($vDataUtil?(($vTotalReach/$vDataUtil)*100) : 0);
	
	$persenDecease	= ($vDataUtil?(($vDataDecease/$vDataUtil)*100) : 0);
	$persenNBP 		= ($vDataUtil?(($vDataNBP/$vDataUtil)*100) : 0);
	$persenMV		= ($vDataUtil?(($vDataMV/$vDataUtil)*100) : 0);
	$persenBusy		= ($vDataUtil?(($vDataBusy/$vDataUtil)*100) : 0);
	
	
	echo "<tr>";
		echo '<td class="head"  align="right">&nbsp;</td>'
			.'<td class="head"  align="left" nowrap ><b>'.$ArTL[$LeaderId] .'</b></td>'
			.'<td class="head"  align="right">'.number_format($vDataSize).'</td>'
			.'<td class="head"  align="right">'.number_format($vDataVol).'</td>'
			.'<td class="head"  align="right">'.number_format($vDataUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($vVolUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($vAgentDial).'</td>';
			
		printf ('<td  class="head"  align="right">%.2f</td>', $persenUtil);
		
		echo '<td  class="head"  align="right">'.number_format($leader_data[$LeaderId]['ptp_new']).'</td>' 
			 .'<td class="head"  align="right">'.number_format($leader_data[$LeaderId]['ptp_pop']).'</td>'
			 .'<td class="head"  align="right">'.number_format($leader_data[$LeaderId]['ptp_paid']).'</td>'
			// .'<td class="head"  align="right">'.number_format($leader_data[$LeaderId]['ptp_sp']).'</td>'
			 .'<td class="head"  align="right">'.number_format($leader_data[$LeaderId]['ptp_bp']).'</td>'
			 .'<td class="head"  align="right">'.number_format($vVolTotalPtp).'</td>';
			 
		printf ('<td  class="head"  align="right">%.2f</td>', $persenPtp);
		echo '<td class="head"  align="right">'.number_format($vDataValid).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenValid);
		
		// echo  '<td class="head"  align="right">'.number_format($vDataMV).'</td>'
		//	 .'<td class="head"  align="right">'.number_format($vDataNK).'</td>'
		//	 .'<td class="head"  align="right">'.number_format($vDataWN).'</td>'
		//	 .'<td class="head"  align="right">'.number_format($vTotalSkip).'</td>';
		//printf ('<td  class="head"  align="right">%.2f</td>', $persenSkip);
		
		echo '<td class="head"  align="right">'.number_format($vTotalPR).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenPR);
		echo '<td class="head"  align="right">'.number_format($vTotalNego).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenNego);
		echo '<td class="head"  align="right">'.number_format($vTotalOS).'</td>';
		
		echo '<td class="head"  align="right">'.number_format($vDataDecease).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenDecease);
		
		echo '<td class="head"  align="right">'.number_format($vDataMV).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenMV);
		
		echo '<td class="head"  align="right">'.number_format($vDataNBP).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenNBP);
		
		echo '<td class="head"  align="right">'.number_format($vDataBusy).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenBusy);
		
		echo '<td class="head"  align="right">'.number_format($vTotalComplain).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenComplain);
		
		echo '<td class="head"  align="right">'.number_format($vTotalReach).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $persenReach);
		echo '</tr>';
		
		
	// --------------- on total bottom ----------------------------------------
		$totDataSize 		+= $vDataSize;
		$totDataVol 		+= $vDataVol;
		$totDataUtil 		+= $vDataUtil;
		$totVolUtil 		+= $vVolUtil;
		$totAgentDial 		+= $vAgentDial;
		$totVolPtpNew 		+= $vVolPtpNew;
		$totVolPtpPop 		+= $vVolPtpPop;
		$totVolPtpPaid 		+= $vVolPtpPaid;
		$totVolPtpSp 		+= $vVolPtpSp;
		$totVolPtpBp 		+= $vVolPtpBp;
		$totDataValid   	+= $vDataValid;
		$totTotalSkip		+= $vTotalSkip;
		$totTotalPR			+= $vTotalPR;
		$totTotalNego		+= $vTotalNego;
		$totTotalOS			+= $vTotalOS;
		$totTotalComplain  	+= $vTotalComplain;
		$totVolTotalPtp  	+= $vVolTotalPtp;
		$totContacted		+= $vContacted;
		$totTotalReach		+= $vTotalReach;
		
		$totTotalDecease  	+= $vDataDecease;
		$totTotalMV  		+= $vDataMV;
		$totTotalNBP  		+= $vDataNBP;
		$totTotalBusy  		+= $vDataBusy;
		
		
  }	
  
  
  // =================== total on bootom =================
    $totPersenUtil 		= ($totDataSize?(($totDataUtil *100)/$totDataSize):0);
	$totPersenPtp 		= ($totDataUtil?(($totVolTotalPtp/$totDataUtil)*100) : 0);
	$totPersenValid 	= ($totDataUtil?(($totDataValid/$totDataUtil)*100) : 0);
	$totPersenSkip 		= ($totDataUtil?(($totTotalSkip/$totDataUtil)*100) : 0);
	$totPersenPR		= ($totDataUtil?(($totTotalPR/$totDataUtil)*100) : 0);
	$totPersenNego		= ($totDataUtil?(($totTotalNego/$totDataUtil)*100) : 0);
	$totPersenOS		= ($totDataUtil?(($totTotalOS/$totDataUtil)*100) : 0);
	$totPersenComplain  = ($totDataUtil?(($totTotalComplain/$totDataUtil)*100) : 0);
	$totPersenReach 	= ($totDataUtil?(($totTotalReach/$totDataUtil)*100) : 0);
	
	$totPersenDecease  	+= ($totDataUtil?(($totTotalDecease/$totDataUtil)*100) : 0);
	$totPersentMV  		+= ($totDataUtil?(($totTotalMV/$totDataUtil)*100) : 0);
	$totPersentNBP  	+= ($totDataUtil?(($totTotalNBP/$totDataUtil)*100) : 0);
	$totPersenBusy  	+= ($totDataUtil?(($totTotalBusy/$totDataUtil)*100) : 0);
	
	echo "<tr>";
		echo '<td class="head"  align="center" colspan=2 nowrap ><b>TOTAL</b></td>'
			.'<td class="head"  align="right">'.number_format($totDataSize).'</td>'
			.'<td class="head"  align="right">'.number_format($totDataVol).'</td>'
			.'<td class="head"  align="right">'.number_format($totDataUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($totVolUtil).'</td>'
			.'<td class="head"  align="right">'.number_format($totAgentDial).'</td>';
			
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenUtil);
		
		echo  '<td  class="head"  align="right">'.number_format($totVolPtpNew).'</td>' 
			 .'<td class="head"  align="right">'.number_format($totVolPtpPop).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolPtpPaid).'</td>'
			// .'<td class="head"  align="right">'.number_format($totVolPtpSp).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolPtpBp).'</td>'
			 .'<td class="head"  align="right">'.number_format($totVolTotalPtp).'</td>';
			 
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenPtp);
		echo '<td class="head"  align="right">'.number_format($totDataValid).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenValid);
		/*
		echo  '<td class="head"  align="right">'.number_format($totDataMV).'</td>'
			 .'<td class="head"  align="right">'.number_format($totDataNK).'</td>'
			 .'<td class="head"  align="right">'.number_format($totDataWN).'</td>'
			 .'<td class="head"  align="right">'.number_format($totTotalSkip).'</td>';
			*/
			
		//printf ('<td  class="head"  align="right">%.2f</td>', $totPersenSkip);
		echo '<td class="head"  align="right">'.number_format($totTotalPR).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenPR);
		echo '<td class="head"  align="right">'.number_format($totTotalNego).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenNego);
		echo '<td class="head"  align="right">'.number_format($totTotalOS).'</td>';
		
		
		echo '<td class="head"  align="right">'.number_format($totTotalDecease).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenDecease);
		
		echo '<td class="head"  align="right">'.number_format($totTotalMV).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersentMV);
		
		echo '<td class="head"  align="right">'.number_format($totTotalNBP).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersentNBP);
		
		echo '<td class="head"  align="right">'.number_format($totTotalBusy).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenBusy);
		
		
		echo '<td class="head"  align="right">'.number_format($totTotalComplain).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenComplain);
		
		echo '<td class="head"  align="right">'.number_format($totTotalReach).'</td>';
		printf ('<td  class="head"  align="right">%.2f</td>', $totPersenReach);
		echo '</tr></table><br>';
		
	
	
}


//--------------------------------------------------------------------------
/*
 * @ package 		ShowDetailReportByTL
 */
function showReportc($start_date, $end_date, $group, $agents){

		echo '<table class="data" border=1 style="border-collapse: collapse">';
		echo '<tr>'
		     .'<td class="head" rowspan="2">No.</td>'
		     .'<td class="head" rowspan="2">DCR Name</td>'
		     .'<td class="head" rowspan="2">Datasize</td>'
		     .'<td class="head" rowspan="2">Volume</td>'
		     .'<td class="head" rowspan="2">Data Utilized</td>'
		     .'<td class="head" rowspan="2">Volume Utilized</td>'
		     .'<td class="head" rowspan="2">Call Initiated</td>'
		     .'<td class="head" rowspan="2">% Utilized</td>'
			 
		     .'<td class="head" colspan="7" align="center">PTP</td>'
		     .'<td class="head" colspan="2" align="center">VALID</td>'
		     .'<td class="head" colspan="5" align="center">SKIP</td>'
			 
		     .'<td class="head" colspan="2" align="center">PR-</td>'
		     .'<td class="head" colspan="2" align="center">ON-NEGO </td>'
			 
		     .'<td class="head" align="center">OS-</td>'
			 .'<td class="head" colspan="2" align="center">COMPLAIN</td>'
			 
			 .'<td class="head" rowspan="2" align="center">TOTAL REACH</td>'
			 .'<td class="head" rowspan="2" align="center">(%)REACH</td>'
		     
			 .'</tr>';
			 
		echo '<tr>'
		     .'<td class="head" align="center">PTP NEW</td>'
			 .'<td class="head" align="center">PTP POP</td>'
			 .'<td class="head" align="center" nowrap>PTP <br>PAID OFF</td>'
			 .'<td class="head" align="center">SP</td>'
			 .'<td class="head" align="center">PTP BP</td>'
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
		     .'<td class="head" align="center">MV</td>'
		     .'<td class="head" align="center">WN</td>'
		     .'<td class="head" align="center">NK</td>'
			 .'<td class="head" align="center">TOTAL</td>'
			 .'<td class="head" align="center">(%)</td>'
			 
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
			 
		     .'<td class="head" align="center">TOTAL</td>'
		     .'<td class="head" align="center">(%)</td>'
		     
			 .'<td class="head" align="center">TOTAL OS </td>'
			 
		     .'<td class="head" align="center">TOTAL COMPLAIN</td>'
		     .'<td class="head" align="center">(%) COMPLAIN </td>'
			 
		     //.'<td class="head">Total Skip</td>'
		    // .'<td class="head">% Skip</td>'
		     .'</tr>';

		
		if($group || $agents){
			$fromTbl = ", cc_agent d ";
			//periksa apakah ada agent yang dipilih
			if(count($agents) and $agents[0]){
				$agentlist = implode(",", $agents);
				$cond = ' AND b.deb_agent = d.userid AND d.id IN ('.$agentlist.') 
						  AND d.occupancy>0 AND d.spv_id IN(0, 1) ';
			}else{
				$cond = " AND b.deb_agent = d.userid 
						  AND d.occupancy>0 
						  AND d.spv_id = 1 
						  AND d.agent_group ='".$group."' ";
			}
			
			if(count($agents) and $agents[0]){
				$agentlist = implode(",", $agents);
				$condInit = ' AND d.id IN ('.$agentlist.') AND d.occupancy>0 AND d.spv_id IN(1,2)';
			}else{
				$condInit = " AND d.occupancy>0 
							  AND d.spv_id IN(1,2) 
							  AND d.agent_group ='".$group."' ";
			}
		}
		
		
		
		$agent_data = array();
		// Get data size and volume
		
		$sql = " SELECT b.deb_agent as agent , COUNT(*) AS datasize, SUM(b.deb_wo_amount) AS datavol, d.agent_group, d.occupancy ".
		       " FROM t_gn_debitur b, cc_agent d ".
		       " WHERE b.deb_agent = d.userid ".
		       " $cond ".
		       " GROUP BY b.deb_agent 
			     ORDER BY d.agent_group, b.deb_agent ";
				 
	// -------------- echo $sql ----------------------;		 
		
		$res = @mysql_query($sql);
		 while ($row = @mysql_fetch_array($res)) 
		{
			$data = array();
			$agent = $row['agent'];
			$data['group'] = $row['agent_group'];
			$data['occupancy']= $row['occupancy'];
			$data['datasize'] = $row['datasize'];
			$data['datavol'] 	= $row['datavol'];
			$agent_data[$agent] = $data;
		}

	// Get data utilization		       
		$sql = "SELECT  b.deb_agent as agent, COUNT(DISTINCT a.CustomerId) AS u_data, SUM(DISTINCT b.deb_amount_wo) AS u_vol
				FROM t_gn_debitur b 
				LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId=b.deb_id $fromTbl 
				WHERE 
					a.CallHistoryCallDate >= '$start_date 00:00:00'
					AND a.CallHistoryCallDate <= '$end_date 23:59:59'
					$cond
					GROUP BY b.deb_agent";		       

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['u_data'] = $row['u_data'];
			$agent_data[$agent]['u_vol'] 	= $row['u_vol'];
		}

		// Get Call Initiated
		$sql = "SELECT a.agent_id, d.userid, COUNT(*) AS agent_dial ".
		       "FROM cc_call_session a , cc_agent d    ".
		       "WHERE ".
		       "a.agent_id = d.id AND a.start_time >= '$start_date 00:00:00' ".
		       "AND a.start_time <= '$end_date 23:59:59' and d.spv_id IN(1,2) $condInit ".
		       "GROUP BY a.agent_id";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= trim($row['userid']);
			$agent_data[$agent]['agent_dial'] = $row['agent_dial'];
			//echo $row['userid']." -> ".$row['agent_dial']." -> ".$agent_data[$agent]['agent_dial']."<br>";
		}

		// Get POP yang di pake cuma POP 1 aja , yang Lain Gak DI pake .
		
		// $sql = " SELECT b.deb_agent as agent, COUNT(DISTINCT a.CustomerId ) AS pop_data  ".
		       // " FROM t_gn_debitur b ".
			   // " LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id $fromTbl ".
		       // " WHERE ".
		       // " a.CallHistoryCallDate >= '$start_date 00:00:00' ".
		       // " AND a.CallHistoryCallDate <= '$end_date 23:59:59' ".
		       // " AND a.CallAccountStatus = 7 $cond ".
		       // " GROUP BY b.deb_agent";
			   
		$sql = "SELECT b.deb_agent AS agent, 
				COUNT(DISTINCT b.deb_id) AS pop_data
				FROM t_gn_debitur b
				LEFT OUTER JOIN t_gn_callhistory_report a ON a.CustomerId = b.deb_id
				LEFT JOIN cc_agent d on b.deb_agent=d.userid
				LEFT JOIN t_lk_account_status e on a.CallAccountStatus=e.CallReasonId WHERE 1=1 ".
				" AND a.CallHistoryCallDate >= '$start_date 00:00:00' ".
		        " AND a.CallHistoryCallDate <= '$end_date 23:59:59' ".
			    " AND e.CallReasonCode = '107' $cond ".
			    " GROUP BY b.deb_agent ";

	   

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['agent'];
			$agent_data[$agent]['pop_data'] = $row['pop_data'];
		}
		
	
		//getting pop2
	$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS pop2_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND b.accstatus = 15 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['pop2_data'] = $row['pop2_data'];
		}
		//getting pop3
	$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS pop3_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND b.accstatus = 16 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['pop3_data'] = $row['pop3_data'];
		}
		
		//getting pop4
	$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS pop4_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND b.accstatus = 17 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);

		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['pop4_data'] = $row['pop4_data'];
		}

		// Get RE-SP
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS resp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 13 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['resp_data'] = $row['resp_data'];
		}

		// Get SP
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS sp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 9 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['sp_data'] = $row['sp_data'];
		}

		//OP 	-> 1
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS op_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 1 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['op_data'] = $row['op_data'];
		}

		//RP	-> 8
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS rp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 8 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['rp_data'] = $row['rp_data'];
		}

		//BP	-> 2
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS bp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 2 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['bp_data'] = $row['bp_data'];
		}

		//PTP	-> 4
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS ptp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 4 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['ptp_data'] = $row['ptp_data'];
		}

		//NA 	-> 3
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS na_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 3 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['na_data'] = $row['na_data'];
		}

		//NBP	-> 10
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS nbp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 10 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['nbp_data'] = $row['nbp_data'];
		}

		//MV 	-> 11
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS mv_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 11 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['mv_data'] = $row['mv_data'];
		}

		//WN	-> 5
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS wn_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 5 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['wn_data'] = $row['wn_data'];
		}

		//NK	-> 6
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS nk_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 6 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['nk_data'] = $row['nk_data'];
		}


		$cnt = 0;
		$totData = 0;
		$totVol = 0;
		$totUData = 0;
		$totUVol = 0;
		$totCall = 0;
		$totPOP = 0;
		$totRESP = 0;
		$totSP = 0;
		$totRP = 0;
		$totOP = 0;
		$totBP = 0;
		$group = -1;
		$tl = "";
		$subTotData = 0;
		$subTotVol  = 0;
		$subTotUData = 0;
		$subTotUVol = 0;
		$subTotCall = 0;
		$subTotPop	= 0;
		$subTotPop2	= 0;
		$subTotPop3	= 0;
		$subTotPop4	= 0;
		$subTotResp	= 0;
		$subTotSp	= 0;
		$subTotRp	= 0;
		$subTotOp	= 0;
		$subTotBp	= 0;
		$subTotRpBpOp 	= 0;
		$subTotPtpdata 	= 0;
		$subTotNadata	= 0;
		$subTotNbpdata	= 0;
		$subTotValid	= 0;
		$subTotMvdata	= 0;
		$subTotWndata	= 0;
		$subTotNkdata	= 0;
		$subTotSkip		= 0;
		$subTotReach	= 0;	
		foreach($agent_data as $agent => $data){

			if($group != $data['group']){
				if($cnt>0){
					$persenUtil = $subTotData?(($subTotUData *100)/ $subTotData):0;
					echo "<tr>";
					echo '<td class="head" nowrap>&nbsp</td>'
							 .'<td class="head" ><b>'.$tl.'<b></td>'
							 .'<td class="head" align="right">'.number_format($subTotData).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotVol).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotUData).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotUVol).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotCall).'</td>';
					printf ('<td class="head" align="right">%.2f</td>', $persenUtil);
					echo '<td class="head" align="right">'.number_format($subTotPop).'</td>' 
							 .'<td class="head" align="right">'.number_format($subTotPop2).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotPop3).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotPop4).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotResp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotSp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotRp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotOp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotBp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotRpBpOp).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotPtpdata).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotNadata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotNbpdata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotValid).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotMvdata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotWndata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotNkdata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotSkip).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotReach).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>';
					echo '</tr>';

					$tl = "";
					$subTotData = 0;
					$subTotVol  = 0;
					$subTotUData = 0;
					$subTotUVol = 0;
					$subTotCall = 0;
					$subTotPop	= 0;
					$subTotPop2	= 0;
					$subTotPop3	= 0;
					$subTotPop4	= 0;
					$subTotResp	= 0;
					$subTotSp	= 0;
					$subTotRp	= 0;
					$subTotOp	= 0;
					$subTotBp	= 0;
					$subTotRpBpOp 	= 0;
					$subTotPtpdata 	= 0;
					$subTotNadata	= 0;
					$subTotNbpdata	= 0;
					$subTotValid	= 0;
					$subTotMvdata	= 0;
					$subTotWndata	= 0;
					$subTotNkdata	= 0;
					$subTotSkip		= 0;
					$subTotReach	= 0;
				}
				$group = $data['group'];

			}

			$cnt++;
			if($data['occupancy']==4)
				$tl = $agent;
			$persenUtil = $data['datasize']?(($data['u_data'] *100)/ $data['datasize']):0;

			$Contacted 	= $data['rp_data']+$data['op_data']+$data['bp_data'];
			$Valid			= $data['na_data']+$data['nbp_data'];
			$Skip				= $data['mv_data']+$data['wn_data']+$data['nk_data'];
			$Reach			= $Contacted + $data['ptp_data'] + $data['na_data'];
			$perContacted	=	$data['u_data']?(($Contacted *100)/ $data['u_data']):0;
			$perPTP			=	$data['u_data']?((($data['ptp_data']+$data['pop_data']) *100)/ $data['u_data']):0;
			$perValid		=	$data['u_data']?(($Valid *100)/ $data['u_data']):0;
			$perSkip		=	$data['u_data']?(($Skip *100)/ $data['u_data']):0;
			$perReach		=	$data['u_data']?(($Reach *100)/ $data['u_data']):0;
			echo "<tr>";
			echo '<td nowrap>'.$cnt.'</td>'
					 .'<td>'.$agent.'</td>'
					 .'<td align="right">'.$data['datasize'].'</td>'
					 .'<td align="right">'.number_format($data['datavol']).'</td>'
					 .'<td align="right">'.$data['u_data'].'</td>'
					 .'<td align="right">'.number_format($data['u_vol']).'</td>'
					 .'<td align="right">'.number_format($data['agent_dial']).'</td>';
			printf ('<td align="right">%.2f</td>', $persenUtil);
			echo '<td align="right">'.number_format($data['pop_data']).'</td>'
					 //.'<td align="right">'.number_format($data['pop1_data']).'</td>'
					 .'<td align="right">'.number_format($data['pop2_data']).'</td>'
					 .'<td align="right">'.number_format($data['pop3_data']).'</td>'
					 .'<td align="right">'.number_format($data['pop4_data']).'</td>'
					 .'<td align="right">'.number_format($data['resp_data']).'</td>'
					 .'<td align="right">'.number_format($data['sp_data']).'</td>'
					 .'<td align="right">'.number_format($data['rp_data']).'</td>'
					 .'<td align="right">'.number_format($data['op_data']).'</td>'
					 .'<td align="right">'.number_format($data['bp_data']).'</td>'
					 .'<td align="right">'.number_format($Contacted).'</td>';
			printf ('<td align="right">%.2f</td>', $perContacted);
			echo '<td align="right">'.number_format($data['ptp_data']).'</td>';
			printf ('<td align="right">%.2f</td>', $perPTP);
			echo '<td align="right">'.number_format($data['na_data']).'</td>'
					 .'<td align="right">'.number_format($data['nbp_data']).'</td>'
					 .'<td align="right">'.number_format($Valid).'</td>';
			printf ('<td align="right">%.2f</td>', $perValid);
			echo '<td align="right">'.number_format($data['mv_data']).'</td>'
					 .'<td align="right">'.number_format($data['wn_data']).'</td>'
					 .'<td align="right">'.number_format($data['nk_data']).'</td>'
					 .'<td align="right">'.number_format($Skip).'</td>';
			printf ('<td align="right">%.2f</td>', $perSkip);
			echo '<td align="right">'.number_format($Reach).'</td>';
			printf ('<td align="right">%.2f</td>', $perReach);
			echo '</tr>';

			$totData 		+= $data['datasize'];
			$subTotData 	+= $data['datasize'];
			$totVol  		+= $data['datavol'];
			$subTotVol  	+= $data['datavol'];
			$totUData 		+= $data['u_data'];
			$subTotUData 	+= $data['u_data'];
			$totUVol 		+= $data['u_vol'];
			$subTotUVol 	+= $data['u_vol'];
			$totCall 		+= $data['agent_dial'];
			$subTotCall 	+= $data['agent_dial'];
			$totPop 		+= $data['pop_data'];
			$subTotPop		+= $data['pop_data'];
			$totPop2 		+= $data['pop2_data'];
			$subTotPop2		+= $data['pop2_data'];
			$totPop3 		+= $data['pop3_data'];
			$subTotPop3		+= $data['pop3_data'];
			$totPop4		+= $data['pop4_data'];
			$subTotPop4		+= $data['pop4_data'];
			$totResp   		+= $data['resp_data'];
			$subTotResp		+= $data['resp_data'];
			$totSp   		+= $data['sp_data'];
			$subTotSp		+= $data['sp_data'];
			$totRp   		+= $data['rp_data'];
			$subTotRp		+= $data['rp_data'];
			$totOp   		+= $data['op_data'];
			$subTotOp		+= $data['op_data'];
			$totBp   		+= $data['bp_data'];
			$subTotBp		+= $data['bp_data'];
			$totRpBpOp		+= $Contacted;
			$subTotRpBpOp	+= $Contacted;
			$totPtpdata		+= $data['ptp_data'];
			$subTotPtpdata	+= $data['ptp_data'];
			$totNadata		+= $data['na_data'];
			$subTotNadata	+= $data['na_data'];
			$totNbpdata		+= $data['nbp_data'];
			$subTotNbpdata	+= $data['nbp_data'];
			$totValid		+= $Valid;
			$subTotValid	+= $Valid;
			$totMvdata		+= $data['mv_data'];
			$subTotMvdata	+= $data['mv_data'];
			$totWndata		+= $data['wn_data'];
			$subTotWndata	+= $data['wn_data'];
			$totNkdata		+= $data['nk_data'];
			$subTotNkdata	+= $data['nk_data'];
			$totSkip		+= $Skip;
			$subTotSkip		+= $Skip;
			$totReach		+= $Reach;
			$subTotReach	+= $Reach;

		}

		$persenUtil = $totData?(($totUData *100)/ $totData):0;
		echo "<td class=\"head\"></td>"
		     ."<td class=\"head\">TOTAL </td>"
		     .'<td class="head" align="right">'.$totData.'</td>'
		     .'<td class="head" align="right">'.number_format($totVol).'</td>'
		     .'<td class="head" align="right">'.$totUData.'</td>'
		     .'<td class="head" align="right">'.number_format($totUVol).'</td>'
		     .'<td class="head" align="right">'.number_format($totCall).'</td>';
		printf ('<td class="head" align="right">%.2f</td>', $persenUtil);
		//echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totPop).'</td>';
		echo '<td class="head">'.number_format($totPop2).'</td>';
		echo '<td class="head">'.number_format($totPop3).'</td>';
		echo '<td class="head">'.number_format($totPop4).'</td>';
		echo '<td class="head">'.number_format($totResp).'</td>';
		echo '<td class="head">'.number_format($totSp).'</td>';
		echo '<td class="head">'.number_format($totRp).'</td>';
		echo '<td class="head">'.number_format($totOp).'</td>';
		echo '<td class="head">'.number_format($totBp).'</td>';
		echo '<td class="head">'.number_format($totRpBpOp).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totPtpdata).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totNadata).'</td>';
		echo '<td class="head">'.number_format($totNbpdata).'</td>';
		echo '<td class="head">'.number_format($totValid).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totMvdata).'</td>';
		echo '<td class="head">'.number_format($totWndata).'</td>';
		echo '<td class="head">'.number_format($totNkdata).'</td>';
		echo '<td class="head">'.number_format($totSkip).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totReach).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '</table><br>';


}
?>
<html>
	<head>
		<title>
			enigmaColection Report - Tracking
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	vertical-align:middle;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}


.left { text-align:left;}
.right{ text-align:right;}
.center{ text-align:center;}
.font-size22 { font-size:22px; color:#000;}
.font-size20 { font-size:20px; color:#000;}
.font-size18 { font-size:18px; color:#000;}
.font-size16 { font-size:16px; color:#000;} 
.font-size14 { font-size:16px; color:#000;} 

p.normal  { line-height:6px;}

			-->
			</style>
	</head>
<body>
	<?php showHeaders();?>
	<?php ShowReport(); ?>
</body>

</html>