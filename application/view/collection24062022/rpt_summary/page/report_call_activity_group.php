<fieldset class="corner" style='margin-top:px;'>
<legend class="icon-menulist">&nbsp;&nbsp;User Filter </legend>
<form name="frmReport">
<div>
	<table cellpadding='4' cellspacing=4>
		<tr>
			<td class="text_caption bottom">Select Report </td>
			<td >:</td>
			<td class='bottom'><?php echo form()->combo('report_type','select auto', ReportType(), null, array('change' => 'ShowReportType();' ) );?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Report Group </td>
			<td >:</td>
			<td class='bottom'><?php echo form()->combo('report_group','select auto', ReportGroup());?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Product</td>
			<td >:</td>
			<td class='bottom'><?php echo form()->combo('product','select auto', Product_Outbound());?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Team Leader </td>
			<td >:</td>
			<td class='bottom' id="content-tl"><?php echo form()->combo('user_tl','select auto', Teamleader(), _get_session('UserId'), array('change' =>'ShowAgentPerTL();'));?></td>
		</tr>
		
		
		<tr>
			<td class="text_caption bottom">Deskoll</td>
			<td >:</td>
			<td class='bottom' id="content-deskoll"><?php echo form()->combo('user_agent','select auto',Agent());?></td>
		</tr>
		
		
		<tr>
			<td class="text_caption bottom">Interval </td>
			<td >:</td>
			<td class='bottom'> <?php echo form()->input('start_date','input_text box date');?> &nbsp- <?php echo form()->input('end_date','input_text box date');?> </td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Mode</td>
			<td >:</td>
			<td ><?php echo form()->combo('report_mode','select auto',  ReportMode());?></td>
		</tr>
		
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td >&nbsp;</td>
			<td class='bottom'>
				<?php echo form()->button('','page-go button','Show',array("click"=>"new ShowReport();") );?>
				<?php echo form()->button('','excel button','Export',array("click"=>"new ShowExcel();") );?>
			</td>
		</tr>	
	</table>
</div>
</form>
</fieldset>