<?php

global $paymentChannels;
global $BASE_EXCEL_FILE_NAME;
	$paymentChannels = array(0 => "Unknown", "ATM Bank Lain", "Teller Bank Lain", "HSBC", "POS",
								  "Pickup", "ATM Mandiri", "Teller Mandiri", "ATM BCA", "Teller BCA", "RTGS",
								  "Teller", "ATM Transfer", "ATM Pembayaran", "HSBC");

$file_name = "report_ptp_".date('Ymd')."_".date('His').".xls";
$BASE_DIR_NAME = dirname (__FILE__);
$BASE_EXCEL_FILE_NAME = "${BASE_DIR_NAME}/${file_name}";
		 
/* reconstruct date */
$sdates = explode("-", $start_date);
//echo "<br>".$sdates[0];
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];


if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}


/** start # omens : untuk menghitung amount wo stelah di bayar ( amount_balance after pay ) ****/

function getPchDay(){
	$sql = "select * from t_lk_payment_channel"; //where pch_flags = 1";
	$qry = @mysql_query($sql);
		while($row = mysql_fetch_assoc($qry)){
			$pch[$row['pch_code']] = $row['pch_day'];
		}
	return $pch;
}

function isPay($custno=''){
		$sql = "select count(a.paymenthistoryid) as isPay 
				from coll_payment_history a where a.custno='$custno'";
			
		$qry = @mysql_query($sql);
		$row = mysql_fetch_assoc($qry);
		if( $row['isPay'] >0 ): return true;
			else:
				return false;
			endif;	
}



function calculAfterPay($custno='', $amount_wo=0)
{
	$amount_after_pay = 0;
	$sql = "select sum(a.amount) as afterpay from coll_payment_history a where a.custno='$custno'";
	$qry = @mysql_query($sql);
	$row = mysql_fetch_assoc($qry);
	
	if( isPay($custno) ):
		if( $amount_wo!=0) : $amount_after_pay = (($amount_wo)-($row['afterpay'])); endif;
	else: $amount_after_pay = $amount_wo;  endif;	
	return $amount_after_pay;
}

/** stop # omens : untuk menghitung amount wo stelah di bayar ( amount_balance after pay ) ****/


function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}
	
	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}  


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];
	
	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);
	
	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}
	
	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;
	
	return $yyyy."-".$mm."-".$dd;
}


// -----------------------------------------------------------------------------------


 

function showReport($vstart_date, $vend_date, $GroupCallCenter, $vAgentId,$vTlId){	
	global $paymentChannels;
	global $BASE_EXCEL_FILE_NAME;
		  
		// var_dump($BASE_EXCEL_FILE_NAME);
		/*
		 * @ def		create header name of file to process 
		 *
		 * @ package 	 view
		 * @ params 	 Content line write
		 */

		 $workbook =& new writeexcel_workbookbig($BASE_EXCEL_FILE_NAME);
		 $worksheet =& $workbook->addworksheet();
				
		/* pack header format every file **/
			 
		 $header_format =& $workbook->addformat();
		 $header_format ->set_bold();
		 $header_format->set_size(10);
		 $header_format->set_color('white');
		 $header_format->set_align('left');
		 $header_format->set_align('vcenter');
		 $header_format->set_pattern();
		 $header_format->set_fg_color('black');
 
		 $num = 0; // default num from 0 to (n);
 
		/*
		 * @ def		called name of template download
		 *
		 * @ package 	 view
		 * @ params 	 Content line write
		 */
		$ar_header = array(
			"No.",
			"Tgl Jatuh Tempo",
			"CH Name",
			"Card No",
			"DCR Name",
			"Info PTP",
		    "AmountWo",
		   "Payment Channel",
			"Efective Date",
		   "Promise to Pay"	
		);
		$col_head = 0;
		if(is_array( $ar_header ))
			foreach( $ar_header as $label => $name)
		{
			$worksheet->write_string($num, $col_head, $name, $header_format );
			$col_head++; 	
		 }

		$num = $num+1;
		
		if($GroupCallCenter){
			$fromTbl = ", cc_agent d ";
			
			if(count($vAgentId) and $vAgentId[0]){
			
				$agentlist = implode(",", $vAgentId);
				$cond = "AND b.deb_agent = d.userid AND d.id IN (".$agentlist.")";
			}else{
				$cond = 'AND b.deb_agent = d.userid AND d.agent_group ='.$GroupCallCenter;
			}
		}
		
		$sub = "SELECT date_format(py.pay_created_ts, '%d-%m-%Y') as pay_created_ts 
				FROM t_gn_payment py  WHERE py.pay_id = (
				select MAX(pd.pay_id) from t_gn_payment pd 
				where pd.deb_id = a.deb_id
				and pd.pay_dc_id = e.UserId )";
						
		$sql = "SELECT 
		
				b.deb_agent , a.ptp_date, b.deb_name, 
				b.deb_cardno,
				a.deb_id, a.ptp_type,c.info_ptp_name, 
				b.deb_wo_amount, a.ptp_amount ,
				a.ptp_chanel, b.deb_bal_afterpay,($sub) as pay_created_ts
				FROM t_tx_ptp a 
				INNER JOIN t_gn_debitur b ON a.deb_id = b.deb_id
				LEFT JOIN t_lk_info_ptp c on a.ptp_type = c.info_ptp_code 
				INNER JOIN cc_agent d on b.deb_agent=d.userid
				INNER JOIN t_tx_agent e on d.userid=e.id
				
				WHERE  a.ptp_date >= '$vstart_date 00:00:00' 
		        AND a.ptp_date <= '$vend_date 23:59:59' 
				AND a.is_delete = 0
				$cond
		        ORDER BY b.deb_agent ,a.ptp_date";

		$res = @mysql_query($sql);
		$cnt = 0;
		$dcr = "UNKNOWN";		
		$subtotal = 0;
		(int) $grandtotal = 0;

		while ($row = @mysql_fetch_array($res)) {
			$pchDay = getPchDay();
			if(array_key_exists ( $row['ptp_chanel'] , $pchDay )){
				$interval = $pchDay[$row['ptp_chanel']];
				$Duedate = strtotime($row['ptp_date']);
				$EffectiveDate = date('Y-m-d', strtotime($interval.' day', $Duedate));
			}
			$cnt++;
			
			$worksheet->write_string($num, 0, $cnt );
			$worksheet->write_string($num, 1, $row['ptp_date'] );
			$worksheet->write_string($num, 2, $row['deb_name'] );
			$worksheet->write_string($num, 3, $row['deb_cardno'] );
			$worksheet->write_string($num, 4, strtoupper($row['deb_agent']) );
			$worksheet->write_string($num, 5, (($row['ptp_type']=='101')?'LUNAS':(($row['ptp_type']=='102')?'CICILAN':'')) );
			$worksheet->write_number($num, 6, $row['deb_bal_afterpay'] );
			$worksheet->write_string($num, 7, $paymentChannels[$row['ptp_chanel']] );
			$worksheet->write_string($num, 8, $EffectiveDate );
			$worksheet->write_number($num, 9, $row['ptp_amount'] );
			
			$num++;
			
			
			$subtotal += $row['ptp_amount'];
			$grandtotal += $row['ptp_amount'];
			
		}
		$worksheet->write_string($num, 8, "Grand Total: " );
		$worksheet->write_string($num, 9, $grandtotal );
	
	$_SESSION['xl_start_date'] 	= $start_date;
	$_SESSION['xl_end_date'] 		= $end_date;
	$_SESSION['xl_group'] 			= $group;
	$_SESSION['xl_agents'] 			= $agents;	
	
		$workbook->close(); // end book 
}

showReport($start_date, $end_date, $GroupCallCenter, $AgentId,$TlId);
// --------------- download excel ---------------------------
// var_dump(file_exists($BASE_EXCEL_FILE_NAME));
if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel; charset=utf-16");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	
	
?>