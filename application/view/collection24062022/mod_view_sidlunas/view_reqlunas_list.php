<?php 
// @ pack :  set align #------------------------------

 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');

// @ pack :  set align #------------------------------

 $labels =& page_labels();
 $primary =& page_primary();

// @ pack :  set align #------------------------------

 page_set_align('AccountNumber', 'left');
 page_set_align('CustomerName', 'left');
 page_set_align('AprvByDeskoll', 'left');
 page_set_align('AccountStatus', 'left');
 page_set_align('CallStatus', 'left');
 page_set_align('RequestName', 'left');
 page_set_align('CPAStatus', 'left');
 page_set_align('CreateDate', 'center');
 page_set_align('LastCallDate', 'center');
 page_set_align('UpdateDate', 'center');
 page_set_align('ReqByDeskoll', 'center');
 page_set_align('AmountWO', 'right');


// @ pack : set currency column in here 
 $align=&page_get_align(); 
 $call_user_func = array('AmountWO'=>'_getCurrency', 'BalanceAffterPay'=>'_getCurrency','UpdateDate'=>'_getDateTime');
 $call_link_user = array();
 $sid_status = array(1=>'SID Required', 2=>'SID Complete');

?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<a href="javascript:void(0);" style="color:red;" onclick="Ext.Cmp('<?php __($primary);?>').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); if($Field != 'CallReasonId') { ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle <?php __($align[$Field]); ?>"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php  } endforeach;  ?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows ) { 
 $color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); ?>
 <tr class="onselect" bgcolor="<?php echo $color; ?>">
	<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary],array("change"=>"Ext.Cmp(this.id).oneChecked(this);"), NULL ); ?>
	<td class="content-middle center"> <?php __($no);?></td>
	<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field);  ?>
	<td class="content-middle" <?php __($color) ?>  align="<?php __($align[$Field]); ?>">
		<?php if( in_array( $Field, $call_link_user ) ) : ?>
		<a href="javascript:void(0);" style="color:#289c05;text-decoration:none;" onclick="Ext.DOM.CallCustomer('<?php __($rows[$primary]); ?>');">  <?php __($rows[$Field]); ?></a>
		<?php else : ?>
			<?php if($Field=="SIDStatus"){
				echo $sid_status[$rows[$Field]];
			} else{
				echo (page_call_function($Field, $rows, $call_user_func) );
			} ?>
		<?php //echo $rows; // __(page_call_function($Field, $rows, $call_user_func) ); ?>
		<?php endif; ?>	
	</td>
	<?php endforeach;?>
 </tr>	
</tbody>
<?php
	$no++;
}

?>
</table>


