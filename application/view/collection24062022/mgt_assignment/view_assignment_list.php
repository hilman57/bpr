<?php ?>
<table class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th width="2%" rowspan="2"  nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('chk_cmp').setChecked();">#</a></th>		
		<th width="2%" rowspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;No</th>
		<!-- <th width="10%" rowspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="a.CampaignNumber" onclick="Ext.EQuery.orderBy(this.id);">Campaign ID.</th>      -->
		<th width="8%" rowspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="a.CampaignNumber" onclick="Ext.EQuery.orderBy(this.id);">Campaign Code.</th>     
		<th width="18%" rowspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="a.CampaignName" onclick="Ext.EQuery.orderBy(this.id);">Campaign Name.</th> 
		<!-- <th width="10%" rowspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="b.Description" onclick="Ext.EQuery.orderBy(this.id);">Call Type.</th>  -->
		<!--<th width="20%" colspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Data Size.</th>-->
		<th width="20%" colspan="2" nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;New Data (All)</th>    
		<th width="20%" colspan="2" nowrap class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;New Data (Today)</th>    
	</tr>
	<tr height="24">
		<th width="10%" nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Customer</th>
		<th width="10%" nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Policy</th>
		<th width="10%" nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Customer</th>
		<th width="10%" nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Policy</th>
	</tr>
</thead>	
<tbody>
<?php
$no  = $num;
foreach( $page -> result_assoc() as $rows )
{ 
	$CampaignId = $rows['CampaignId'];
	
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); ?>
	<tr class="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"><input type="checkbox" value="<?php echo $rows['CampaignId']; ?>" id="chk_cmp" name="chk_cmp" onchange="Ext.Cmp('chk_cmp').oneChecked(this);"></td>
		<td class="content-middle"><?php echo $no; ?></td>
		<!-- <td class="content-middle"><?php //echo $rows['CampaignNumber'];?></td> -->
		<td class="content-middle"><?php echo $rows['CampaignCode'];?></td>
		<td class="content-middle"><b style="color:green;"><?php echo $rows['CampaignName'];?></b></td>
		<!-- <td class="content-middle"><?php //echo $rows['Description'];?></b></td> -->
		<!--<td class="content-middle" align="center"><X?php echo $Model -> _get_size_privileges($rows['CampaignId'],1); ?></td>
		<td class="content-middle" align="center"><X?php echo $Model -> _get_policy_size($rows['CampaignId'],1); ?></td>-->
		<td class="content-middle" align="center"><?php echo $Model -> _get_count_privileges($rows['CampaignId'],1); ?></td>
		<td class="content-lasted" align="center"><?php echo $Model -> _get_policy_utilize_size($rows['CampaignId'],1); ?></td>
		<td class="content-middle" align="center"><?php echo $Model -> _get_today_privileges($rows['CampaignId'],1); ?></td>
		<td class="content-lasted" align="center"><?php echo $Model -> _get_policy_today($rows['CampaignId'],1); ?></td>
	</tr>			
</tbody>
<?php $no++; }; ?>
</table>