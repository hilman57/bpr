<?php
/* loader HTML **/
$this->load->helper('EUI_Html');
?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $website['_web_title'];?> :: <?php echo ucfirst(base_layout());?> Layout</title>
<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script> 
<script>
<!-- start : script --->
var layout = function(){
	$('#container').css({ 'width' : ($(window).width()-10), 'height' : ($(window).height()-15), 'overflow': 'auto' });
}
		
$(document).ready(function(){ layout(); });
Ext.document(document).resize(function(){ layout(); })

<!-- stop: script --->

</script>
</head>
<?php
// echo "<pre>";
// print_r($view_rows_PerCampaign);
// echo "</pre>";
__(body('start'));
__('<div class="content_table" id="container">');
	__("<fieldset class='corner' style='margin-bottom:10px;margin-top:10px;'>");
	__("<legend class='edit-users-x'></legend>");
	__("<table width='100%' class='custom-grid' cellspacing='1'>");
		__("<tr>
			<th class='font-standars ui-state-default first center' rowspan='3'>NO </th>
			<th class='font-standars ui-state-default middle center' rowspan='3'>&nbsp;Callers</th>
			<th class='font-standars ui-state-default middle center' colspan='4'>&nbsp;$CampaignCode</th>
		</tr>");
		__("<tr>
			<th class='font-standars ui-state-default middle center' colspan='2'>&nbsp;Total Policy</th>
			<th class='font-standars ui-state-default middle center' colspan='2'>&nbsp;Total Customer</th>
		</tr>");
		__("<tr>
			<th class='font-standars ui-state-default middle center'>&nbsp;New Data</th>
			<th class='font-standars ui-state-default middle center'>&nbsp;Pending Data</th>
			<th class='font-standars ui-state-default middle center'>&nbsp;New Data</th>
			<th class='font-standars ui-state-default middle center'>&nbsp;Pending Data</th>
		</tr>");
	$num = 1;
foreach($UserId as $id => $user){
	if(in_array($id, $VarsUserId)){

		$tot_new_data = 0;
		$tot_ped_data = 0;
		
			$color = ($num%2!=0?'#FFFFFF':'#FFFEEE');
			__("<tr bgcolor='$color'>
					<td class='content-first center'>". $num ."</td>
					<td class='content-middle left'>&nbsp;". $UserId[$id]."</td>
					<td class='content-middle right'>&nbsp;". ( $view_rows_PPerCampaign[$id]['new_data'] ? $view_rows_PPerCampaign[$id]['new_data'] : 0 )."</td>
					<td class='content-middle right'>". ( $view_rows_PPerCampaign[$id]['ped_data'] ? $view_rows_PPerCampaign[$id]['ped_data']:0 ) ."&nbsp;</td>
					<td class='content-middle right'>&nbsp;". ( $view_rows_CPerCampaign[$id]['new_data'] ? $view_rows_CPerCampaign[$id]['new_data'] : 0 )."</td>
					<td class='content-middle right'>". ( $view_rows_CPerCampaign[$id]['ped_data'] ? $view_rows_CPerCampaign[$id]['ped_data']:0 ) ."&nbsp;</td>
					
			  </tr>"
			 );	
			 
			 $totals_policy += $rows['new_data'];
			 $totals_customer += $rows['new_data'];
			 $num++;	

	}
}
	__("</table>");
	__("</fieldset>");
	__('</div>');
__(body('stop'));
__(html('stop'));
?>
