<?php 
/** set label style on selected **/
page_background_color('#FFFCCC');
page_font_color('#8a1b08');

/** get label header attribute **/

$labels =& page_labels();
$primary =& page_primary();

/** rider get lable ****/
?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?>
	</tr>
</thead>
<tbody>
<?php

// get phone type 
 
 $UI =& get_instance();
 $UI->load->model('M_Combo');
 
 // testting 

$no = $num;
foreach( $page -> result_assoc() as $rows )
{ 
	$sphone[$rows[$primary]]= $UI->M_Combo->CallPhoneType($rows[$primary]);
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>
	<tr class="onselect" bgcolor="<?php echo $color;?>" >
		<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary],array('click' => 'Ext.DOM.gotoCallCustomer();'), null); ?>
		<td class="content-middle"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) : $color=&page_column($Field);  ?>
		<?php if( $Field=='CallPhoneType') : ?>
			<td class="content-middle" <?php __($color) ?> ><?php echo ( $sphone[$rows[$Field]]?$sphone[$rows[$Field]]:'-' ); ?></td>
		<?php else :?>	
			<td class="content-middle" <?php __($color) ?> ><?php echo ( $rows[$Field]?$rows[$Field]:'-' ); ?></td>
		<?php endif; ?>	
		<?php endforeach; ?>
	</tr>
</tbody>
<?php
	$no++;
}

?>
</table>


