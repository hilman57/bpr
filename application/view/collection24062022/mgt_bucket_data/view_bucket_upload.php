<?php
/* @ def 	 : view upload manual
 * 
 * @ param	 : sesion  
 * @ package : bucket data 
 */
 
?>
<div class="box-shadow" style="padding:10px;">
<fieldset class='corner'>

<legend class="icon-campaign">&nbsp;&nbsp;Upload Data </legend>	
<div style="border-left:0px solid #dddddd;text-align:top;margin-top:-10px;">
	<table align="left" border=0 cellpadding="5px">
		<tr>
			<td class="left text_caption bottom" style='height:24px;'># Template &nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<!-- <td class="left bottom"><?php //echo form() -> combo('upload_template','select long', (isset($Template) ? $Template : null)); ?></td> -->
			<td class="left bottom"> 
				<select type="combo" name="upload_template" id="upload_template" class="select long">
					<option value=""> --choose --</option>
					<option value="4">UPLOAD_NEW_CARD</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="left text_caption bottom" style='height:24px;'># Kartu&nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><?php echo form() -> combo('upload_campaign','select long', (isset($CampaignId) ? $CampaignId : null)); ?></td>
		</tr>
		
		<tr>
			<td class="left text_caption bottom" style='height:24px;'># File location&nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><?php echo form() -> upload('fileToupload'); ?></td>
			<!--<td class="left bottom"> <?php //echo form()->checkbox('FlagDesc','','1'); ?></td>-->
		</tr>
		<tr>
			<td class="left text_caption bottom" style='height:24px;'>&nbsp;</td>
			<td class="center text_caption bottom">&nbsp;</td>	
			<td class="left bottom"><span id="loading-image"></span></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td class="left bottom" style='padding-left:12px;' colspan=2> 
				<input type="button" class="save button" onclick="Ext.DOM.Upload();" value="Upload">
				<input type="button" class="close button" onclick="Ext.DOM.ClosePanel();" value="Close">
			</td>
		</tr>
	</table>
</div>
</fieldset>
</div>