<?php 
/*
 * @ def 		: view_content_policydata data load by Policy ID 
 * -----------------------------------------
 * 
 * @ params  	: $ Policy ID 
 * @ example    : -
 */
 // echo "<pre>";
 // print_r($PolicyData);
 // echo "</pre>";
 ?>
 
<form name="frmInfoPolicy">
<table width="99%" align="center" cellpadding="2px" cellspacing="4px">
	<tr>
		<td nowrap class="text_caption">Policy Number</td>
		<td><?php echo form() -> input('PolicyNumber','input_text long',$PolicyData['PolicyNumber'],NULL,1);?> </td>
		<td class="text_caption" nowrap >Product Name</td>
		<td><?php echo form() -> input('PolicyProductName','input_text long',$PolicyData['PolicyProductName'],NULL,1);?></td>
		<td class="text_caption" nowrap >Product Category</td>
		<td><?php echo form() -> input('PolicyProductCategory','input_text long',$PolicyData['PolicyProductCategory'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap >Product Type</td>
		<td><?php echo form() -> input('PolicyProductType','input_text long',$PolicyData['PolicyProductType'],NULL,1);?></td>
		<td class="text_caption" nowrap >Product Type Detail</td>
		<td><?php echo form() -> input('PolicyProductTypeDetail','input_text long',$PolicyData['PolicyProductTypeDetail'],NULL,1);?></td>
		<td class="text_caption" nowrap >DC Name</td>
		<td><?php echo form() -> input('Nama_Dc','input_text long',$PolicyData['Nama_Dc'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap>Branch</td>
		<td><?php echo form() -> input('Branch_Name','input_text long',$PolicyData['Branch_Name'],NULL,1); ?></td>
		<td class="text_caption" nowrap>District Name</td>
		<td><?php echo form() -> input('CustomerCountry','input_text long',$PolicyData['CustomerCountry'],NULL,1); ?></td>
		<td class="text_caption" nowrap>Policy Iss Date</td>
		<td><?php echo form() -> input('PolicyIssDate','input_text long',$PolicyData['PolicyIssDate'],NULL,1); ?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap>Bill Frequency</td>
		<td><?php echo form() -> input('PaymentMode','input_text long',$PolicyData['PaymentModeAs'],NULL,1);?></td>
		<td class="text_caption" nowrap>Premium</td>
		<td><?php echo form() -> input('PolicyPremi','input_text long',$PolicyData['PolicyPremi'],NULL,1);?></td>
		<td class="text_caption" nowrap>Currency</td>
		<td><?php echo form() -> input('Currency','input_text long',$PolicyData['Currency'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap>Billing Bank Code</td>
		<td><?php echo form() -> input('Bank_Issuer','input_text long',$PolicyData['Bank_Issuer'],NULL,1);?></td>
		<td class="text_caption" nowrap>Major Product</td>
		<td><?php echo form() -> input('PolicyMajorProduct','input_text long',$PolicyData['PolicyMajorProduct'],NULL,1);?></td>
		<td class="text_caption" nowrap>Product Code</td>
		<td><?php echo form() -> input('PolicyProductCode','input_text long',$PolicyData['PolicyProductCode'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap>Risk Common Date</td>
		<td><?php echo form() -> input('PolicyRCD','input_text long',$PolicyData['PolicyRCD'],NULL,1);?></td>
		<td class="text_caption" nowrap>Grace Periode</td>
		<td><?php echo form() -> input('PolicySisaGracePeriod','input_text long',$PolicyData['PolicySisaGracePeriod'],NULL,1);?></td>
		<td class="text_caption" nowrap>Payment Frequency</td>
		<td><?php echo form() -> input('PaymentMode','input_text long',$PolicyData['PaymentMode'],NULL,1);?></td>
	</tr>

	<tr>
		<td class="text_caption" nowrap>Premium</td>
		<td><?php echo form() -> input('PolicyPremi','input_text long',$PolicyData['PolicyPremi'],NULL,1);?></td>
		<td class="text_caption" nowrap>Payment Type</td>
		<td><?php echo form() -> input('PolicyPaymentType','input_text long',$PolicyData['PolicyPaymentType'],NULL,1);?></td>
		
	</tr>
</table>	
</form>