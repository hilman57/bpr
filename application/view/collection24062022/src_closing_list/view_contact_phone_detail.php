<?php

/** 
 * @ def : grid of content policy by customer ID 
 * ----------------------------------------------------------------
 
 * @ param  : $ CustomerId 
 * @ refer  : $ Pilcy ID     
 * 
 */
?>

<fieldset class="corner" style="margin-top:-3px;"> 
	<legend class="icon-menulist"> &nbsp;&nbsp; Call Activity</legend> 
<div style="overflow:auto;margin-top:3px;" class="activity-content box-shadow">
<input type="hidden" name="CallingNumber" id="CallingNumber" 
value="<?php __($Customers['CustomerMobilePhoneNum']);?>">
<form name="frmActivityCall">
<?php echo form()->hidden('QualityStatus',NULL,$Customers['CallReasonQue']);?>
<table class="activity-table" cellpadding="0px;" border=0 cellspacing="1px">
	<tr>
		<td nowrap class="text_caption">Phone</td>
		<td nowrap id="phone_primary_number"><?php echo form()->combo('PhoneNumber','select long', $Phones,$Customers['CustomerMobilePhoneNum'],
		array("change" =>"Ext.Cmp('CallingNumber').setValue(this.value);") ); ?></td>
	</tr>	
	<tr>
		<td nowrap class="text_caption">Add Phone</td>
		<td nowrap id="phone_additional_number"><?php echo form()->combo('AddPhoneNumber','select long',$AddPhone, null, 
		array("change" =>"Ext.Cmp('CallingNumber').setValue(this.value);")); ?></td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td>
			<img class="image-calls" src="<?php echo base_url(); ?>/library/gambar/PhoneCall.png" width="35px" height="35px" style="cursor:pointer;" title="Dial..." onclick="dialCustomer();">
			<img class="image-hangup" src="<?php echo base_url(); ?>/library/gambar/HangUp.png" width="35px" height="35px" style="cursor:pointer;" title="Hangup..." onclick="hangupCustomer();">
		</td>
	</tr>	
	<tr>
		<td class="text_caption" valign="top">Call Status </td>
		<td> <?php echo form()->combo('CallStatus','select long', $CallCategoryId,$Customers['CallReasonCategoryId'],array('change'=>"getCallReasultId(this);")); ?></td>
	</tr>	
	<tr>
		<td class="text_caption">Call Result</td>
		<td id="contact_reason_text">
			<span id="DivCallResultId">
				<?php echo form()->combo('CallResult','select long',$CallResultId, $Customers['CallReasonId'],array('change'=>'getEventSale(this);')); ?>
			</span>
		</td>
	</tr>	
	<tr>
		<td class="text_caption" valign=top>Call Later </td>
		<td> 
			<?php echo form()->input('date_call_later','input_text box date'); ?>&nbsp;
			<?php echo form()->input('hour_call_later','input_text kotak',null, null, array('style'=>'margin-top:2px;')); ?> :
			<?php echo form()->input('minute_call_later','input_text kotak',null, null, array('style'=>'margin-top:2px;'));?>
		</td>
	</tr>	
	<tr>
		<td class="text_caption">&nbsp;Policy Number</td>
		<td ><?php __(form()->combo('ProductForm','select',$PolicyNumber, null, array('change'=>'Ext.DOM.EventFromPolicy(this);'),array('disabled' => true)));?></td>
	</tr>	
	<tr>
		<td class="text_caption">&nbsp;Call Complete</td>
		<td nowrap><?php __(form()->checkbox('CallComplete',null,1));?></td>
	</tr>	
	<tr>
		<td class="text_caption">Note</td>
		<td align="center"> 
			<textarea id="call_remarks" name="call_remarks" onkeyup="this.value=this.value.toUpperCase();" style="height:100px;background-color:#fbf9f7;color:#000000;font-family:Arial;font-size:11px;border:1px solid #dddddd;width:200px;"></textarea>
		</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td align="center">
			<input type="button" class="button save" value='Save' id="ButtonUserSave" onclick="saveActivity();">
			<input type="button" class="button close" value='Cancel' id="ButtonUserCancel" onclick="CancelActivity();">
		</td>
	</tr>
	</table>	
	</form>
	</div>	
</fieldset>	