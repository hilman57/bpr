<fieldset class="corner" style="margin-top:-3px;"> 
<legend class="icon-menulist"> &nbsp;&nbsp;Customer Information</legend> 
	<script>
		Ext.query(function(){
			Ext.query("#tabs_list" ).tabs();
		});
	</script>
	<div id="tabs_list" class="corner" class="history-tabs">
		<ul>
			<li><a href="#tabs_list-1">Policy Data</a></li>
			<li><a href="#tabs_list-2">Personal Data</a></li>
			<li><a href="#tabs_list-3">Agent Data</a></li>
			<li><a href="#tabs_list-4">Call History</a></li>
		</ul>
		<!-- load :: policy data -->
		<div id="tabs_list-1" style="background-color:#FFFFFF;overflow:auto;"> 
			<?php $this->load->view('mod_contact_detail/view_contact_policydata_bcu');?> 
		</div>
		<!-- load ::personal data -->
		
		<div id="tabs_list-2" style="background-color:#FFFFFF;overflow:auto;">
			<?php $this ->load ->view('mod_contact_detail/view_customer_info');?>
		</div>
		
		<!-- load :: agent data -->
		
		<div id="tabs_list-3" style="background-color:#FFFFFF;height:70px;overflow:auto;">
			<?php $this->load->view('mod_contact_detail/view_contact_agentdata');?>
		</div>
		
		<!-- load : load history-tabs by ajax -->
		
		<div id="tabs_list-4" style="background-color:#FFFFFF;height:175px;overflow:auto;">
		</div>
	</div>

</fieldset>	
