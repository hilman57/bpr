<?php echo javascript(); ?>
<script type="text/javascript">

 
// @ pack : upload data listener 

var RandomBY =  ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/RndReport/get_random_report',
		method 	: 'POST',
		param 	: {}	
	}).json());
	
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 function ShowReport(  )  
{
  var frmReport = Ext.Serialize('frmReport');
  var VarReport = new Array();
   
  if( !frmReport.Required( new Array('report_type','start_date_claim','end_date_claim') ) ){
	Ext.Msg("Input Not Complete").Info();
	return false;
  }	 
 
  VarReport['report_title'] = Ext.Cmp('report_type').getText();
  VarReport['type_show'] = "ShowHTML";
  var report_type =  Ext.Cmp('report_type').getValue();
  Ext.Window  ({
	url 	: Ext.EventUrl(RandomBY[report_type]).Apply(),
	param   : Ext.Join([
		frmReport.getElement(), VarReport
	]).object()
  }).newtab();
  
}
/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 $('document').ready(function()
{
	$('.date').datepicker ({
		showOn : 'button', 
		buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly	: true, 
		changeYear : true,
		changeMonth : true,
		dateFormat : 'dd-mm-yy',
		readonly:true
	});

});
</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Random Debitur Report</span></legend>
<div id="panel-agent-content">
	<table border=0 width='100%'>
		<tr>
			<td width='35%' valign="top">
				<fieldset class="corner" style='margin-top:px;'>
					<legend class="icon-menulist">&nbsp;&nbsp;User Filter </legend>
					<form name="frmReport">
					<div>
						<table cellpadding='4' cellspacing=4>
							<tr>
								<td class="text_caption bottom">Select Modul Random </td>
								<td >:</td>
								<td class='bottom'><?php echo form()->combo('report_type','select auto',$cmb_random_modul );?></td>
							</tr>
							
							<tr>
								<td class="text_caption bottom">Interval Claim</td>
								<td >:</td>
								<td class='bottom'> <?php echo form()->input('start_date_claim','input_text box date');?> &nbsp- <?php echo form()->input('end_date_claim','input_text box date');?> </td>
							</tr>
							
							<tr>
								<td class="text_caption"> &nbsp;</td>
								<td >&nbsp;</td>
								<td class='bottom'>
									<?php echo form()->button('','page-go button','Show',array("click"=>"new ShowReport();") );?>
									<?php echo form()->button('','excel button','Export',array("click"=>"new ShowExcel();") );?>
								</td>
							</tr>	
						</table>
					</div>
					</form>
				</fieldset>
			</td>
		</tr>
	</table>
</div>
</fieldset>