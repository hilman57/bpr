<?php

/*mysql_connect("192.168.0.11","enigma","enigma");
mysql_select_db("enigmacollectdb");*/
	
$dateFrom	= $_REQUEST['dateFrom'];
$dateTo		= $_REQUEST['dateTo'];
$rptGroup	= $_REQUEST['rptGroup'];
$rptAgents	= $_REQUEST['rptAgents'];

/* reconstruct date */
$sdates		= explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates		= explode("-", $end_date);
$end_date	= $edates[2]."-".$edates[1]."-".$edates[0];
$_SESSION['xl_start_date'] 	= $start_date;
$_SESSION['xl_end_date'] 	= $end_date;
$_SESSION['xl_rep_title'] 	= "Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}";

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}

function showReport($start_date,$end_date){
	global $project,$agent_id,$listagent;	
	$time_start = microtime(true);
	echo '<table><tr><td colspan="3"><h2><u>Reporting CPA Status</u></h2></td></tr></table>';
	echo  '<table>'
			.'<tr><td colspan="3"><h3><u></u></h3></td></tr>'
			.'<tr><td> Priode  </td><td>:</td><td>'.$start_date.' to '.$end_date.'</td></tr>'
			.'</table>'
			.'<table class="data" border="1" style="border-collapse:collapse;" cellpadding="3px">'
		    .'<tr>'
			.'<td class="head">No.</td>'
			.'<td class="head">Customer Number</td>'
			.'<td class="head">Full Name</td>'
			.'<td class="head">Account Status</td>'
    		.'<td class="head">Tenor</td>'
			.'<td class="head">Create Date</td>'
    		.'<td class="head">Disc. Balance</td>'
			.'<td class="head">Disc. Principal</td>' 
			.'<td class="head">Payment</td>'
			.'<td class="head">Deal Payment</td>'
			.'<td class="head">WO Amount</td>'
			.'<td class="head">Agent ID</td>'
			.'<td class="head">Status CPA</td>'
			.'</tr>';
		/*
			$sqls = "SELECT a.*,ref.reff_name,
				b.fullname,b.accstatus,b.prevaccstatus,b.wo_amount,
				b.wo_amount,b.debiturid,b.agent,
				cca.userid AGENT,cca2.userid TL,
				cdr.justification,cdr.from_balance,cdr.from_princ,cdr.create_date,cdr.expired_date,cdr.totalpayment_byspv
			FROM coll_discount_lunas a 
			LEFT JOIN coll_debitur b ON a.customer_id = b.custno
			LEFT JOIN cc_agent cca ON b.agent = cca.userid 
			LEFT JOIN cc_agent cca2 ON cca2.agent_group = cca.agent_group and cca2.occupancy = 4
			LEFT JOIN coll_discount_request cdr ON a.customer_id = cdr.custno
			LEFT JOIN coll_reference ref ON b.accstatus = ref.reff_code and ref.reff = 'sg'
			WHERE a.insert_date >= '".$start_date." 00:00:00' AND a.insert_date <= '".$end_date." 23:59:00'";
		*/
		//,cdr.`from_balance`,cdr.create_date,cdr.expired_date,cdr.totalpayment_byspv
$sqls = "SELECT a.*,ref.reff_name,b.`deb_name`,b.`deb_call_status_code`,b.`deb_prev_call_status_code`,b.`deb_wo_amount`,b.deb_id,b.`deb_agent`,
cca.`userid` AS AGENT,cca2.`userid` AS TL, cdr.`justification`
 FROM t_gn_discount_trx a 
 LEFT JOIN t_gn_debitur b ON a.deb_id = b.deb_id
			LEFT JOIN cc_agent cca ON b.deb_agent = cca.userid 
			LEFT JOIN cc_agent cca2 ON cca2.agent_group = cca.agent_group AND cca2.occupancy = 4
			LEFT JOIN t_gn_discount_trx_history cdr ON a.deb_id = cdr.deb_id
			LEFT JOIN coll_reference ref ON b.`deb_call_status_code` = ref.reff_code AND ref.reff = 'sg'
	        WHERE a.create_date >= '".$start_date." 00:00:00' AND a.create_date <= '".$end_date." 23:59:00'
			and a.cpa_status = 'LUNAS'";
		//echo $sqls;
		$qrys=@mysql_query($sqls);
		$col=array();
		$n=1;
		//$res=mysql_fetch_assoc($qrys);
		while($res=@mysql_fetch_array($qrys)){
			$col["custno"][$n]=$res["deb_id"];
			$col["fullname"][$n]=$res["deb_name"];
			$col["accstat"][$n]=$res["reff_name"];
			$col["payment_periode"][$n]=$res["payment_periode"];
			$col["create_date"][$n]=$res["create_date"];
			$col["from_balance"][$n]=$res["from_balance"]*100;
			$col["from_princ"][$n]=$res["from_princ"]*100;
			$col["totalpayment_byspv"][$n]=$res["totalpayment_byspv"];
			$col["wo_amount"][$n]=$res["wo_amount"];
			$col["totalpayment_byhst"][$n]=get_payment_history($col["custno"][$n],$col["create_date"][$n],$res["expired_date"]);
			$col["agent_id"][$n]=$res["deb_agent"];
			$col["cpa_status"][$n]=$res["cpa_status"];
		$n++;
		}

		for($m=1; $m<$n; $m++){
		   echo  '<tr>'
				.'<td>'.$m.'</td>'
				.'<td>'.$col["custno"][$m].'</td>'
				.'<td>'.$col["fullname"][$m].'</td>'
				.'<td>'.$col["accstat"][$m].'</td>'
				.'<td>'.$col["payment_periode"][$m].'</td>'
				.'<td>'.$col["create_date"][$m].'</td>'
				.'<td align="right">-'.$col["from_balance"][$m].'%</td>'
				.'<td align="right">'.$col["from_princ"][$m].'%</td>'
				.'<td align="right">'.currency($col["totalpayment_byhst"][$m]).'</td>'
				.'<td align="right">'.currency($col["totalpayment_byspv"][$m]).'</td>'
				.'<td align="right">'.$col["wo_amount"][$m].'</td>'
				.'<td>'.$col["agent_id"][$m].'</td>'
				.'<td>'.$col["cpa_status"][$m].'</td>'
				.'</tr>';
		 }
		 echo    '<tr><td colspan="12">Summary : '.($m-1).' Records</td></tr>'
			    .'</table>';
}


		function get_payment_history($custno="",$create_date="",$expire_date="")
		{
			$key = substr($custno,0,1);
			if($key=="5" || $key=="4")
			{
				$sql="	SELECT sum(a.amount) as payment
						FROM enigmacollectdb2.coll_payment_history a
						WHERE a.custno = '$custno' AND trxdate >='$create_date' AND trxdate <='$expire_date'";
				$res=mysql_fetch_assoc(mysql_query($sql));
				$get_payment_history=$res['payment'];
			}
			else if($key=="0" || $key=="2")
			{
				$sql="	SELECT sum(a.amount) as payment
						FROM enigmacollectdb.coll_payment_history a
						WHERE a.custno = '$custno' AND trxdate >='$create_date' AND trxdate <='$expire_date'";
				$res=mysql_fetch_assoc(mysql_query($sql));
				$get_payment_history=$res['payment'];
			}
			
			return $get_payment_history;
			//return $sql;
		}

		function update_product($custno){
	$cfno = $custno;
	$str = substr($custno,0,1);
	$str2 = substr($custno,0,3);
	$prod = "";
	
	if($str == '2'){
		$prod = "CF";
	}else if($str == '4'){
		$prod = "CARD";
	}else if($str == '5'){
		$prod = "CARD";
	}else if($str2 == '080'){
		$prod = "PIL";
	}else if($str2 == '001'){
		$prod = "PIL";
	}else if($str2 == '071'){
		$prod = "GRF";
	}else if($str2 == '073'){
		$prod = "GRF";
	}else{
		$prod = "-";
	}
	
	return $prod;
}

function update_arrangement($reff){
	$argmt = "";
	if($reff == 'D'){
		$argmt = "SETTLEMENT";
	}else if($reff == 'R'){
		$argmt = "RESCHEDULE";
	}else if($reff == 'X'){
		$argmt = "PAID-OFF";
	}else{
		$argmt = "-";
	}
	return $argmt;
}

function getSummaryPay($custno=''){
	$sql ="select sum(a.amount) as afterpay from coll_ptp_history a
			where a.custno='$custno'";
	$qry = @mysql_query($sql);
	if( $qry && ($row = mysql_fetch_assoc($qry))){
		return $row['afterpay'];
	}	
}

function currency($currency=''){
			return number_format($currency,0,',','.');
	}

function in_min_value($integer_min=''){
	return (number_format($integer_min,4, '.','') * (-1)*100).'%';
}

function exception_levels($disc=''){
	$excpt = "";
	
	if($disc <= 5000000){
		$excpt = "1";
	}else if($disc <= 10000000){
		$excpt = "2";
	}else if($disc <= 20000000){
		$excpt = "3";
	}else if($disc <= 30000000){
		$excpt = "4";
	}else if($disc <= 50000000){
		$excpt = "5";
	}else if($disc <= 100000000){
		$excpt = "6";
	}else if($disc <= 2300000000){
		$excpt = "7";
	}else{
		$excpt = "-";
	}
	
	return $excpt;
}

?>
<html>
<head>
		<title>
			Enigma Collection Report - CPA Account Lunas
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;	
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	font-weight:bold;
}

td.head2 {
	background-color: AAEEEE;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}
			-->
			</style>
	</head>
<body>
	<?php
        showReport($start_date,$end_date);
	?>
	
	<!--a href="report_excel.cpa_status_card.php?&dateFrom=<?php echo $start_date; ?>&dateTo=<?php echo $end_date; ?>">Save as excel</a -->
	<a href="?action=excel&rptType=<?=$rptType;?>&transid=<?=$transid;?>&dateFrom=<?php echo $start_date; ?>&dateTo=<?php echo $end_date; ?>">Save as excel</a>
</body>
</html>