<?php 
__(javascript(array( 
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time())
)));?>
<script>

// cek if loaded documnet call($this);

Ext.document(document).ready(function(){
var _offset = 22;

// get title 

Ext.Cmp('legend_title').setText( Ext.System.view_file_name());

// date picker 

Ext.query('.date').datepicker ({
	showOn : 'button', 
	buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	changeYear : true,
	changeMonth : true,
	dateFormat : 'dd-mm-yy',
	readonly:true,
	onSelect	: function(date){
		if(typeof(date) =='string'){
			var x = date.split('-');
			var retur = x[2]+"-"+x[1]+"-"+x[0];
			if(new Date(retur) > new Date()) {
				Ext.Cmp($(this).attr('id')).setValue('');
			}
		}
	}
});

// customize height fieldset 

Ext.Css('panel-agent-content').style
({
	'height' : (Ext.query('#main_content').height() -(Ext.query('.extToolbars').height()+_offset)),
	'margin-top' : -5
});

 
Ext.DOM.ShowExcel = function(){
	Ext.Window
	({
		url 	: Ext.DOM.INDEX +'/ReportCpaSummary/ShowExcel/',
		param 	: { 
			GroupCallCenter : Ext.Cmp('Group').getValue(),  
			AgentId : Ext.Cmp('AgentId').getValue(),
			start_date : Ext.Cmp('start_date').getValue(), 
			end_date : Ext.Cmp('end_date').getValue(),
			mode : Ext.Cmp('Mode').getValue(),
			CallDirection : Ext.Cmp('CallDirection').getValue()
		}
	}).newtab();
}
  
 
 
// Ext.DOM.AgentGroupBy

Ext.DOM.AgentByGroup= function( Group )
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/ReportCpaSummary/getAgentByGroup/',
		param 	: {
			GroupId : Group.value
		}
	}).load('DivAgent');
	

Ext.DOM.ShowReport= function(Group )
	{
	var cth = Ext.Cmp('Mode').getValue();
	/*
	switch(cth){
		case 'tracking-report':
			var link = 'http://localhost/report_new_rev14/index.php?action=showreport';
	}
	*/
	var link = Ext.DOM.INDEX +'/report';
	

		//alert(cth);
		/*if(cth == "tracking-report")
		{
			alert('contoh2');
			
		}
		if(cth == "ptp-report")
		{
			alert('contoh2');
		}
		if(cth == "skill-report")
		{
			alert('contoh3');
		}
		if(cth == "data-keep")
		{
			alert('contoh4');
		}
		if(cth == "data-review")
		{
			alert('contoh5');
		}
		if(cth == "summary-cpa")
		{
			alert('contoh6');
		}
		if(cth == "list-lunas")
		{
			alert('contoh7');
		}
		if(cth == "cpa_status_report")
		{
			alert('contoh8');
		}
		if(cth == "account_lunas")
		{
			alert('contoh9');
		}
		if(cth == "account_batal")
		{
			alert('contoh10');
		}*/
		
	Ext.Window
	({
		url 	: link,
		param 	: { 
			action : cth,
			GroupCallCenter : Ext.Cmp('Group').getValue(),  
			AgentId : Ext.Cmp('AgentId').getValue(),
			start_date : Ext.Cmp('start_date').getValue(), 
			end_date : Ext.Cmp('end_date').getValue(),
			mode : Ext.Cmp('Mode').getValue()
			}
		
	}).newtab();
}
	
	
Ext.DOM.test= function( Group )
	{
	Ext.Window
	({
		url 	: Ext.DOM.INDEX +'/../../../../report_new_rev14/form.report.php'
	}).newtab();
}
	
});

</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Order Report</span></legend>
<div id="panel-agent-content">
	<table border=0 width='100%'>
		<tr>
			<td width='35%' valign="top"><?php $this -> load-> view('rpt_summary/report_call_activity_group');?></td>
			<td width='65%' valign="top"><?php $this -> load-> view('rpt_summary/report_call_activity_content');?></td>
		</tr>
	</table>
</div>
</fieldset>
