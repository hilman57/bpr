<?

/* reconstruct date */
$sdates = explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}

function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}
	
	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}  


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];
	
	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);
	
	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}
	
	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;
	
	return $yyyy."-".$mm."-".$dd;
}

function printNote(){
	//echo "note:<br>".
	//		 "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">".	     
	//    "</table>";
}

function showReport($vstart_date, $vend_date, $GroupCallCenter, $vAgentId){	
	global $paymentChannels;
		
		echo "<table class=\"data\" border=1 style=\"border-collapse: collapse\"><tr>"
		     .'<td class="head" width="20">No.</td>'
		     .'<td class="head" width="100">Customer ID</td>'
		     ."<td class=\"head\">Customer Name</td>"
		     ."<td class=\"head\">Agent ID</td>"
		     ."<td class=\"head\">Account Status</td>"
			 ."<td class=\"head\">Last Call</td>"
		     ."<td class=\"head\">Info PTP</td>"
		     ."<td class=\"head\">Recsource</td>"
		     ."<td class=\"head\">AmountWo</td>"
			 ."<td class=\"head\">History</td>"				     
		     ."</tr>";
		
	if($GroupCallCenter){
			$fromTbl = ", cc_agent d ";
			//periksa apakah ada agent yang dipilih
			if(count($vAgentId) and $vAgentId[0]){
			
				$agentlist = implode(",", array($vAgentId));
				//echo $agentlist;
				$cond = "AND a.deb_agent = d.userid AND d.id IN (".$agentlist.")";
			}else{
				$cond = 'AND a.deb_agent = d.userid AND  d.agent_group ='.$GroupCallCenter;
			}
		}
		
		$sql = "SELECT DATE_FORMAT(a.deb_call_activity_datets,'%d-%m-%Y') cd,a.deb_call_status_code
				,b.CallReasonDesc,d.agent_group, a.deb_prev_call_status_code,a.*,
				b.CallReasonDesc accountstatus, e.CallReasonDesc laststatus,d.userid,f.ptp_type,g.info_ptp_description,a.deb_id
				FROM t_gn_debitur  a 
				LEFT JOIN t_lk_account_status b
				ON a.deb_call_status_code = b.CallReasonCode
				LEFT JOIN t_lk_account_status e
				ON a.deb_prev_call_status_code = e.CallReasonCode
				LEFT JOIN cc_agent d ON a.deb_agent  = d.userid
				LEFT JOIN t_gn_keept_history c on a.deb_id = c.keept_deb_id
				LEFT OUTER JOIN 		t_tx_ptp f ON a.deb_id = f.deb_id
				LEFT OUTER JOIN t_lk_info_ptp g on f.ptp_type = g.info_ptp_code 
				WHERE a.deb_is_kept =1 $cond order by a.deb_agent ";
			
		$res = @mysql_query($sql);
		$cnt = 0;
		$dcr = "UNKNOWN";		
		$subtotal = 0;
		$grandtotal = 0;
		
		//echo $sql;
		while ($row = @mysql_fetch_array($res)) {
			
			if($dcr != $row['deb_agent']){
				
			/*	if($cnt>0){
					echo '<tr>';
					echo '<td colspan="8" align="right"><strong>Sub Total: </strong>&nbsp;</td>';
					echo '<td class="head2" align="right"><strong>'.number_format($subtotal).'</strong></td>';
					echo '</tr>';
				} */
				$dcr = $row['deb_agent'];
				echo '<tr>';
				echo '<td colspan="10" class="head2"><strong>'.$dcr.'</strong>&nbsp;</td>';
				echo '</tr>';
				$subtotal = 0;
			}
			
			$cnt++;
			echo "<tr>";
			echo "<td nowrap>$cnt</td>"
					 .'<td>'.$row['deb_cardno'].'</td>'
					 .'<td>'.$row['deb_name'].'</td>'
					 .'<td>'.$row['deb_agent'].'</td>'
					 .'<td>'.$row['accountstatus'].'</td>'
					.'<td>'.$row['laststatus'].'</td>'
					 .'<td align="right">'.(($row['ptp_type']=='101')?'LUNAS':(($row['ptp_type']=='102')?'CICILAN':'')).'</td>'
					 .'<td>'.$row['deb_resource'].'</td>'
					 .'<td align="right">'.number_format($row['deb_wo_amount']).'</td>'
					 .'<td>'.$row['deb_reminder'].'</td>';
			echo "</tr>";
			
			$subtotal += $row['deb_amount_wo'];
			$grandtotal += $row['deb_amount_wo'];
			
		}
		/*
		if($cnt>0){
			echo '<tr>';
			echo '<td colspan="8" align="right"><strong>Sub Total: </strong>&nbsp;</td>';
			echo '<td class="head2" align="right"><strong>'.number_format($subtotal).'</strong></td>';
			echo '</tr>';
		}
		echo '<tr>';
		echo '<td colspan="7" align="right"><strong>Grand Total: </strong>&nbsp;</td>';
		echo '<td colspan="3" class="head2" align="right"><strong>'.number_format($grandtotal).'</strong></td>';
		echo '</tr>';*/
		echo "</table><br>";

	//save to report data $start_date, $end_date, $group, $agents to session
	
	$_SESSION['xl_start_date'] 	= $start_date;
	$_SESSION['xl_end_date'] 		= $end_date;
	$_SESSION['xl_group'] 			= $group;
	$_SESSION['xl_agents'] 			= $agents;		
		
}
?>
<html>
	<head>
		<title>
			Enigma Collection Report - Data keep
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;	
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
}

td.head2 {
	background-color: AAEEEE;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}
			-->
			</style>
	</head>
<body>
	<h1><u>Data Keep Report</u></h1>
	<?php	
	//echo "<h2>Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}</h2>";
	echo "<hr size=1>";	
	showReport($start_date, $end_date, $GroupCallCenter, $AgentId);
	
	$_SESSION['xl_rep_title'] 	= "Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}";	
	printNote();
	?>	
<a href="?action=excel&rptType=<?=$rptType;?>&transid=<?=$transid;?>">Save as excel</a>
</body>
</html>