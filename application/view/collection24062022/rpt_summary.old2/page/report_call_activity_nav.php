<script>

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
function ShowAgentPerTL() {
	
	$("#content-deskoll").load(Ext.EventUrl(new Array('ReportCpaSummary','ShowAgentPerTL') ).Apply(), 
		{ TL : Ext.Cmp('user_tl').getValue()}, 
		function( response, status, xhr ) 
	{
		if( status == 'success' ){
			$("#user_agent").toogle();
		}		
	});
}	

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 function ShowReport( Group )  
{
 var frmReport = Ext.Serialize('frmReport');
 Ext.Window  ({
	url 	: Ext.EventUrl(new Array('ReportCpaSummary','ShowReport') ).Apply(),
	param   : Ext.Join([
		frmReport.getElement() 
	]).object()
	
  }).newtab();
}

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
$('documnet').ready(function(){
var _offset = 22;
 $('.date').datepicker  ({
	showOn : 'button', 
	buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	changeYear : true,
	changeMonth : true,
	dateFormat : 'dd-mm-yy',
	readonly:true,
	onSelect	: function(date){
		if(typeof(date) =='string'){
			var x = date.split('-');
			var retur = x[2]+"-"+x[1]+"-"+x[0];
			if(new Date(retur) > new Date()) {
				Ext.Cmp($(this).attr('id')).setValue('');
			}
		}
	}
});


// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
 function ShowExcel ()
{
	Ext.Window ({
		url 	: Ext.DOM.INDEX +'/ReportCpaSummary/ShowExcel/',
		param 	: { 
			GroupCallCenter : Ext.Cmp('Group').getValue(),  
			AgentId : Ext.Cmp('AgentId').getValue(),
			start_date : Ext.Cmp('start_date').getValue(), 
			end_date : Ext.Cmp('end_date').getValue(),
			mode : Ext.Cmp('Mode').getValue(),
			CallDirection : Ext.Cmp('CallDirection').getValue()
		}
	}).newtab();
}
 


	$("#user_tl").toogle();
	$("#user_agent").toogle();
});

</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Reporting</span></legend>
<div id="panel-agent-content">
	<table border=0 width='100%'>
		<tr>
			<td width='35%' valign="top"><?php $this->load->view('rpt_summary/page/report_call_activity_group');?></td>
			<td width='65%' valign="top"><?php $this->load->view('rpt_summary/page/report_call_activity_content');?></td>
		</tr>
	</table>
</div>
</fieldset>
