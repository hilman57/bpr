<?php __(javascript());  ?>

<script type="text/javascript">
/* 
 * @ pack : keyword to serach 
 */
 
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 

Ext.DOM.Record 			= 0;
Ext.DOM.datas 			= {}
Ext.DOM.Privileges 		= eval('<?php __(json_encode($privileges)); ?>');
Ext.EQuery.TotalPage 	= '<?php __($page->_get_total_page()); ?>';
Ext.EQuery.TotalRecord 	= '<?php __($page->_get_total_record()); ?>';

/* 
 * @ pack : keyword to serach 
 */
 
 Ext.DOM.datas = 
{

 transfer_account_status : "<?php echo _get_exist_session('transfer_account_status');?>",
 transfer_agent_id 	  : "<?php echo _get_exist_session('transfer_agent_id');?>",
 transfer_call_status 	  : "<?php echo _get_exist_session('transfer_call_status');?>",
 transfer_campaign_id 	  : "<?php echo _get_exist_session('transfer_campaign_id');?>",
 transfer_cust_id 		  : "<?php echo _get_exist_session('transfer_cust_id');?>",
 transfer_cust_name 	  : "<?php echo _get_exist_session('transfer_cust_name');?>",
 transfer_recsource 	  : "<?php echo _get_exist_session('transfer_recsource');?>",
 transfer_start_amountwo : "<?php echo _get_exist_session('transfer_start_amountwo');?>",
 transfer_end_amountwo   : "<?php echo _get_exist_session('transfer_end_amountwo');?>",
 transfer_start_date 	  : "<?php echo _get_exist_session('transfer_start_date');?>",
 transfer_end_date 	  : "<?php echo _get_exist_session('transfer_end_date');?>",
 order_by 			  : "<?php echo _get_exist_session('order_by');?>",
 type 				  : "<?php echo _get_exist_session('type');?>"
 
} // ==> Ext.DOM.datas

/* 
 * @ pack : keyword to serach 
 */
 
 Ext.DOM.navigation = 
{
 custnav  : Ext.DOM.INDEX+'/MgtTransferData/index/',
 custlist : Ext.DOM.INDEX+'/MgtTransferData/Content/',
} //  ==> Ext.DOM.navigation

 
/* 
 * @ pack : keyword to serach 
 */
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();


/* 
 * @ pack : keyword to serach 
 */
 
Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!='') 
 {	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.navigation.custnav
		}
  });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}




/* 
 * getLevelUser
 */ 
 Ext.DOM.getLevelUser = function(UserLevelId)
{
	Ext.Cmp('TransferTo').setText("# To User&nbsp;:&nbsp;<span id='div_transfer' style='width:200px;'></span>");
	
	Ext.Ajax ({
		url :Ext.DOM.INDEX+"/MgtTransferData/SwapUserLevel/",
		param :{
			LevelID : UserLevelId
		}
	}).load('div_transfer');
	
	$('#div_transfer').css({ 
		'padding-left': ($('#field_UserId').width()+10)
	});
	
	$('#field_UserId').addClass('box-shadow');
	$('#field_UserId').css({
		'position': 'absolute',
		'background-color' : '#FFFFFF',
		'display':'inline-block',
		'left' : 492,  
		'margin-top' : '0px',
		'z-index' : '999',
		'border-top': '1px solid blue',
		'border-bottom': '1px solid blue',
		'border-left': '1px solid blue',
		'border-right': '1px solid blue'
	});
	
	$('#div_UserId').css({
		'color' :'#000000',
		'background-color' : '#FFFFFF',
		'height' : ( $(window).height() - ( $(window).height()/2 ))
	});
//alert(UserLevelId);

}

// --------------------------------------
// @ pack  : get list data 

 Ext.DOM.SelectSwapData = function( page )
{
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/MgtTransferData/SwapListData/",
		method 	:'GET',
		param 	: {
			CampaignId : Ext.Cmp('CampaignId').getValue(),
			CallReasonId : Ext.Cmp('CallReasonId').getValue(),
			FromUserId : Ext.Cmp('FromUserId').getValue(),
			page : page
			
		}
	}).load('list_move_data');
}

// --------------------------------------
// @ pack : finding data 
	
Ext.DOM.Find = function(){

 Ext.Cmp("waiting_list").setText('Please wait...');
 Ext.Css("waiting_list").style({'color':'red'});
 
 Ext.EQuery.construct( Ext.DOM.navigation, Ext.Join([
	Ext.Serialize('frmMoveData').getElement() 
 ]).object() );
 
 Ext.EQuery.postContent();
 
 Ext.Cmp("waiting_list").setText('.');
 Ext.Css("waiting_list").style({'color':'#DDDDDD'});
 
}



// --------------------------------------
// @ pack : finding data 
Ext.DOM.UploadSwapData = function(){
	
	Ext.Ajax ({
		url :Ext.DOM.INDEX+"/MgtTransferData/UploadSwapData/",
		param :{
			time : Ext.Date().getDuration()
		}
	}).load('UploadSwapAccount');
	
	
		
	
}	

// --------------------------------------
// @ pack : finding data 
	

$(document).ready( function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle : [['Find'],['Clear'],['Move To Level'],[''],[''],['Upload'],['Transfer By Check'],['']],
		extMenu  : [['Find'],['ClerSwapData'],[],[],[],['UploadSwapData'],['SwapData'],[]],
		extIcon  : [['find.png'],['zoom_out.png'],['user_red.png'],[],[],['database_go.png'],['arrow_switch.png'],[]],
		extText  : true,
		extInput : true,
		extOption: [{
						render : 7,
						type   : 'label',
						label  : '.',
						id     : 'waiting_list',
						name   : 'waiting_list'
					}, {
						render  : 3,
						type    : 'combo',
						id 	   	: 'userLevel',
						name    : 'userLevel',
						store   : Privileges,
						triger  : 'getLevelUser'	
					},{
						render  : 4,
						type    : 'label',
						id 	   	: 'TransferTo',
						name    : 'TransferTo',
						label   : '# To User&nbsp;:&nbsp;<select id="UserId" class="select auto"></select>'
					}]
	});

	Ext.Cmp('amount_data').listener
	({
		'onkeyup' : function(e){
			Ext.Util(e).proc(function(obj){
				Ext.Set('amount_data').IsNumber();
				if(obj.value > Record)
				{
					Ext.Cmp('amount_data').setValue('0');
				}
			});
		}
	});
	
	Ext.Cmp('total_record').setAttribute('style','color:#003BFF;');
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});

/* 
 * @ pack : keyword to serach 
 */
 
 Ext.DOM.ClerSwapData = function()
{
  Ext.Serialize('frmMoveData').Clear();
  Ext.DOM.Find();
}

// --------------------------------------
// @ pack : SwapData

Ext.DOM.SwapData = function() {

Ext.Cmp("waiting_list").setText('Swap in process...');
Ext.Css("waiting_list").style({'color':'red'});

var UserId = Ext.Cmp('UserId').getValue(), 
	AssignId = Ext.Cmp('AssignId').getValue(),
	UserLevel = Ext.Cmp('userLevel').getValue();
	 
 Ext.Ajax
({
	url 	: Ext.DOM.INDEX+"/MgtTransferData/SwapData/",
	method 	: 'GET',
	param 	:  { 
		UserId : UserId, 
		AssignId : AssignId,
		UserLevel : UserLevel 
	},
	
	ERROR : function(e) {
		Ext.Util(e).proc(function(assign){
		if( assign.success ) {
			Ext.Msg("Swap Data With : "+ assign.message+" Data ").Success();
			Ext.Cmp('ToLevelUser').setText('');
			Ext.Cmp('userLevel').setValue('');
			Ext.DOM.Find();
		}
		else{ 
			Ext.Msg("Swap Data").Failed(); 
		}
					
		Ext.Cmp("waiting_list").setText('.');
		Ext.Css("waiting_list").style({'color':'#DDDDDD'});
	});
   }	
 }).post();
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
 
Ext.DOM.Upload = function()
{
	Ext.Cmp("waiting_list").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/MgtBucket/UploadBucket/',
		method 	: 'POST',
		param	: {
			TemplateId : Ext.Cmp('upload_template').getValue(),
			CampaignId : Ext.Cmp('upload_campaign').getValue()
		},
		complete : function(fn){
			var ERR = eval(fn.target.DONE);
			try {	
				if( ERR )
				{
					var CALLBACK = JSON.parse(fn.target.responseText);
					if( (typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null) ){
						var msg = "\n";
						for(var i in  CALLBACK.mesages ){
							msg += "Failed : "+ CALLBACK.mesages[i].N +",\nSuccess : "+CALLBACK.mesages[i].Y +"\n Blacklist : "+ CALLBACK.mesages[i].B +",\nDuplicate : "+  CALLBACK.mesages[i].D  +"\nDuplicate(s) : "+CALLBACK.mesages[i].X+"\n";
						}	
						Ext.Msg(msg).Error();
						Ext.DOM.Find ();
					}
					else{
						Ext.Msg(CALLBACK.mesages).Info();
					}	
					
					Ext.Cmp("waiting_list").setText('');
				}	
			}
			catch(e){ alert(e); }
		}
	}).upload()
 }
 
</script>
<fieldset class="corner" style="background-color:#FFFFFF;">
 <legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
 <form name="frmMoveData">
	<table border=0  width='80%'>
		<tr>
			<td class="text_caption bottom" valign="top" nowrap># Product</td>
			<td valign="top">:</td>
			<td class="bottom"  valign="top">
				<?php echo form()->combo('transfer_campaign_id', 'select auto',$CampaignId, _get_exist_session('transfer_campaign_id'));?>
			</td>
			<td class="text_caption bottom" valign="top" nowrap ># Caller Name</td>
			<td valign="top">:</td>
			<td class="bottom"  valign="top">
				<?php echo form()->combo('transfer_agent_id', 'select long',$Deskoll,_get_exist_session('transfer_agent_id'));?> 
			</td>
			<td class="text_caption bottom" valign="top" nowrap ># Last Call Date </td>
			<td valign="top">:</td>
			<td class="bottom"  valign="top" nowrap>
				<?php echo form()->input('transfer_start_date', 'input_text date',_get_exist_session('transfer_start_date'));?>
					&nbsp;-&nbsp;
				<?php echo form()->input('transfer_end_date', 'input_text date',_get_exist_session('transfer_end_date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption bottom" valign="top" nowrap># CustomerId </td>
			<td valign="top">:</td>
			<td class="bottom"  valign="top">
				<?php echo form()->input('transfer_cust_id', 'input_text long',_get_exist_session('transfer_cust_id'));?>
			</td>
			<td class="text_caption bottom" valign="top" nowrap># Account Status</td>
			<td valign="top">:</td>
			<td class="bottom" valign="top" >
				<?php echo form()->combo('transfer_account_status', 'select long',$AccountStatus, _get_exist_session('transfer_account_status'));?>
			</td>
			<td class="text_caption bottom">&nbsp;<td>
			<td rowspan=2><span id="UploadSwapAccount" ><td>
		</tr>
		
		<tr>	
			<td class="text_caption bottom" valign="top" nowrap># Customer Name </td>
			<td valign="top">:</td>
			<td class="bottom" valign="top">
				<?php echo form()->input('transfer_cust_name', 'input_text long',_get_exist_session('transfer_cust_name'));?>
			</td>
			
			<td class="text_caption bottom" valign="top" nowrap># Amount WO</td>
			<td valign="top">:</td>
			<td class="bottom" valign="top">
				<?php echo form()->input('transfer_start_amountwo', 'input_text box',_get_exist_session('transfer_start_amountwo'));?>
					&nbsp;to&nbsp;
				<?php echo form()->input('transfer_end_amountwo', 'input_text box',_get_exist_session('transfer_end_amountwo'));?>
			</td>
			
		</tr>
		
		
		
	</table>
 </form>
 </div>
	
<!-- content toolbars -->
	
<div id="toolbars" class="toolbars"></div>
<div class="content_table" style="margin:-4px;"></div>
<div id="pager"></div>
</fieldset>