<?php
/*
 * E.U.I 
 *
 * subject	: view_core_nav ( files )
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */

 $_get_premium_group = $data -> _get_select_premi_group();
 $_get_payment_mode  = $data -> _get_select_paymode();
 
 if(!is_array($PayMode)) {
	exit(0);
}
?>
<fieldset style="border:1px solid #dddddd;background-color:#FFFFFF;">
	<legend class="icon-application"> &nbsp;&nbsp;&nbsp;Content Grid</legend>
	<div style="border:0px solid #dddddd; background-color:white;padding-top:8px;width:'99'%;">
		
		<?php foreach( $PayMode as $_PayId => $_PayValue ){ ?>	
			<fieldset style="border:1px solid #dddddd;">
				<legend class="icon-menulist" >&nbsp;&nbsp;&nbsp;<b><?php echo $_get_payment_mode[$_PayValue]; ?></b></legend> 
				
				<!-- start :: if have group premi --> 
				
				<?php if( count($GroupPremi) >0  )  { ?>
				<div>
					<?php foreach($GroupPremi as $_GroupId => $_GroupValue ) { ?>
					
					<fieldset style="border:1px solid #dddddd;">
						<legend class="icon-customers" style="color:red;font-weight:bold;">&nbsp;&nbsp;&nbsp;<?php echo  $_get_premium_group[$_GroupValue]; ?></legend>
						
						<?php if( is_array($Gender)) { ?>
							<?php foreach( $Gender as $ux => $label ) { ?>
							  <fieldset style="border:1px solid #dddddd;">
								<legend tyle="color:red;font-weight:bold;"><?php __($GenderId[$label]); ?></legend>
								<table cellpadding="1px" cellspacing=1>
								<tr>
									<th  class='font-standars ui-corner-top ui-state-default first center'>AGE BAND </th>
									<?php for( $header=1; $header <= $ProductPlan; $header++) { ?>
										<th class='font-standars ui-corner-top ui-state-default first center'>PLAN <?php echo $header; ?></th>
									<?php } ?>
								</tr>
								<?php for( $y=1; $y <= $ProductAge; $y++){  ?> <!-- start counter of age --> 
								<tr>
									<td>
										<?php echo form() -> input("start_age_{$_PayValue}_{$_GroupValue}_{$label}_{$y}",'input_text box',null, array(), array('style'=>'text-align:right;height:20px;width:50px;')); ?> - 
										<?php echo form() -> input("end_age_{$_PayValue}_{$_GroupValue}_{$label}_{$y}",'input_text box',null, array(), array('style'=>'text-align:right;height:20px;width:50px;')); ?>
									</td>								
										
									<?php for( $plan =1; $plan <= $ProductPlan; $plan++){ ?> <!-- start counter of plan  --> 
										<td><?php echo form() -> input("plan_premi_{$_PayValue}_{$_GroupValue}_{$label}_{$y}_{$plan}", "input_text box",null, array(), array('style'=>'text-align:right;height:20px;')); ?> </td>
									<?php } ?>
								</tr>
								<?php }  ?>		
								</table>
								</fieldset><br>
							 <?php } ?>
						<?php } else { ?>	
							<!-- No Gender --> 
								<table cellpadding="1px" cellspacing=1>
										<tr>
											<th  class='font-standars ui-corner-top ui-state-default first center'>AGE BAND </th>
											<?php for( $header=1; $header <= $ProductPlan; $header++) { ?>
												<th class='font-standars ui-corner-top ui-state-default first center'>PLAN <?php echo $header; ?></th>
											<?php } ?>
										</tr>
										<?php for( $y=1; $y <= $ProductAge; $y++){  ?> <!-- start counter of age --> 
										<tr>
											<td>
												<?php echo form() -> input("start_age_{$_PayValue}_{$_GroupValue}_{$y}",'input_text box',null, array(), array('style'=>'text-align:right;height:20px;width:50px;')); ?> - 
												<?php echo form() -> input("end_age_{$_PayValue}_{$_GroupValue}_{$y}",'input_text box',null, array(), array('style'=>'text-align:right;height:20px;width:50px;')); ?>
											</td>								
												
											<?php for( $plan =1; $plan <= $ProductPlan; $plan++){ ?> <!-- start counter of plan  --> 
												<td><?php echo form() -> input("plan_premi_{$_PayValue}_{$_GroupValue}_{$y}_{$plan}", "input_text box",null, array(), array('style'=>'text-align:right;height:20px;')); ?> </td>
											<?php } ?>
										</tr>
										<?php }  ?>		
								</table>
							
						<?php } ?>
					</fieldset><br>
					<?php } ?>
					
				</div>
				<?php } else { ?>
				<!-- end :: if not have group premi --> 
					<div>
					
						<?php if( is_array($Gender)) { ?> 
						<?php foreach( $Gender as $ux => $label ) { ?>
							 <fieldset style="border:1px solid #dddddd;">
								<legend tyle="color:red;font-weight:bold;"><?php __($GenderId[$label]); ?></legend>
								
							<table cellpadding="4px">
								<!-- header -->
								<tr>
									<th class='font-standars ui-corner-top ui-state-default first center'>AGE BAND </th>
									<?php for( $header=1; $header <= $ProductPlan; $header++) { ?>
										<th class='font-standars ui-corner-top ui-state-default first center'>PLAN <?php echo $header; ?></th>
									<?php } ?>
								</tr>
								
								<?php for( $y=1; $y <= $ProductAge; $y++){  ?> <!-- start counter of age --> 
								<tr>
									<td>
										<?php echo form() -> input("start_age_{$_PayValue}_{$label}_{$y}",'input_text box',null, array(), array('style'=>'height:20px;width:50px;')); ?> - 
										<?php echo form() -> input("end_age_{$_PayValue}_{$label}_{$y}",'input_text box',null, array(), array('style'=>'height:20px;width:50px;')); ?>
									</td>								
									<?php for( $plan =1; $plan <= $ProductPlan; $plan++){ ?> <!-- start counter of plan  --> 
										<td><?php echo form() -> input("plan_premi_{$_PayValue}_{$label}_{$y}_{$plan}", "input_text box"); ?> </td>
									<?php } ?>
								</tr>
								<?php }  ?>		
							</table>
							</fieldset><br>
						<?php } ?>	
						<?php } else  {  ?>
								<table cellpadding="4px">
								<!-- header -->
								<tr>
									<th class='font-standars ui-corner-top ui-state-default first center'>AGE BAND </th>
									<?php for( $header=1; $header <= $ProductPlan; $header++) { ?>
										<th class='font-standars ui-corner-top ui-state-default first center'>PLAN <?php echo $header; ?></th>
									<?php } ?>
								</tr>
								
								<?php for( $y=1; $y <= $ProductAge; $y++){  ?> <!-- start counter of age --> 
								<tr>
									<td>
										<?php echo form() -> input("start_age_{$_PayValue}_{$y}",'input_text box',null, array(), array('style'=>'height:20px;width:50px;')); ?> - 
										<?php echo form() -> input("end_age_{$_PayValue}_{$y}",'input_text box',null, array(), array('style'=>'height:20px;width:50px;')); ?>
									</td>								
									<?php for( $plan =1; $plan <= $ProductPlan; $plan++){ ?> <!-- start counter of plan  --> 
										<td><?php echo form() -> input("plan_premi_{$_PayValue}_{$y}_{$plan}", "input_text box"); ?> </td>
									<?php } ?>
								</tr>
								<?php }  ?>		
							</table>
						<?php } ?>
						
					</div>
				<?php } ?>
			</fieldset><br>
		<?php } ?>
	</div>
</fieldset>
<?php
 // END OF FILE 
 // location : ../test

?>