<?php
/*
 * E.U.I 
 *
 * subject	: view_core_nav ( files )
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
?>
<fieldset style="border:1px solid #dddddd;background-color:white;">
	<legend class="icon-application"> &nbsp;&nbsp;&nbsp;Age Band</legend>
	<div style="border:0px solid #dddddd; background-color:white;padding-top:2px;width:'99'%;">
		<table border="0" align="center" width="99%" cellspacing=0 cellpadding=0>
		<tr> 
			<?php for( $i=1; $i<=$ProductAge; $i++) { ?> 
			<th style="color:red;font-size:12px;border-left:1px solid #ffffff;padding-left:2px;padding-right:2px;">Range <?php echo $i; ?> </th> 
			<?php } ?> 
		</tr>
		<tr> 
			<?php for( $i=1; $i<= $ProductAge; $i++) { ?> 
			<td style="border-right:1px solid #dddddd;">
				<table width="99%" align="center" border=0 cellspacing=1 cellpadding=0>
					<tr> 
						<td class='font-standars ui-corner-top ui-state-default first center'>Min. Age_<?php echo $i;?> </td>
						<td class='font-standars ui-corner-top ui-state-default first center'>Max. Age_<?php echo $i;?> </td>
					</tr>	
				</table>
			</td> 
			<?php } ?> 
		</tr>	
		<tr> 
			<?php for( $i=1; $i<= $ProductAge; $i++) { ?> 
			<td style="border-right:1px solid #dddddd;" class='center'>
				<table width="100%" align="center">
					<tr> 
						<td align="center"><?php echo form() -> input("start_range_{$i}", 'input_text box', null, array("keyup"=>"setInsert('start',this);"), array("style"=>"width:60px;border:1px solid red;height:18px;") ); ?></td>
						<td style="center"><?php echo form() -> input("end_range_{$i}", 'input_text box', null, array("keyup"=>"setInsert('end',this);"),array("style"=>"width:60px;border:1px solid red;height:18px;")); ?></td>
					</tr>	
				</table>
			</td> 
			<?php } ?> 
		</tr>
	</table>
</div>
</fieldset>