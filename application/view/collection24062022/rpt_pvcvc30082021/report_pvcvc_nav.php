<?php echo javascript(); ?>
<script type="text/javascript">


var RandomBY =  ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/PvcvcReport/get_random_report',
		method 	: 'POST',
		param 	: {}	
	}).json());

function ShowReport(){
	var frmReport = Ext.Serialize('frmReport');
	var VarReport = new Array();
   
	if( !frmReport.Required( new Array('report_type','start_date_claim','end_date_claim') ) ){
		Ext.Msg("Input Not Complete").Info();
		return false;
	}	 
 
	VarReport['report_title'] = Ext.Cmp('report_type').getText();
	VarReport['type_show'] = "ShowHTML";
	var report_type =  Ext.Cmp('report_type').getValue();
	
	Ext.Window  ({
		url 	: Ext.EventUrl('/PvcvcReport/showReport').Apply(),
		param   : Ext.Join([
					frmReport.getElement(), VarReport
				]).object()
	}).newtab();
}

function ShowReportExcel(){
	var frmReport = Ext.Serialize('frmReport');
	var VarReport = new Array();
   
	if( !frmReport.Required( new Array('report_type','start_date_claim','end_date_claim') ) ){
		Ext.Msg("Input Not Complete").Info();
		return false;
	}	 
 
	VarReport['report_title'] = Ext.Cmp('report_type').getText();
	VarReport['type_show'] = "ShowEXCEL";
	var report_type =  Ext.Cmp('report_type').getValue();
	
	Ext.Window  ({
		url 	: Ext.EventUrl('/PvcvcReport/showReport').Apply(),
		param   : Ext.Join([
					frmReport.getElement(), VarReport
				]).object()
	}).newtab();
}


$('document').ready(function(){
	$('.date').datepicker ({
		showOn : 'button', 
		buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly	: true, 
		changeYear : true,
		changeMonth : true,
		showButtonPanel: true,
		dateFormat : 'M-yy',
		readonly:true,
		onClose: function(dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
	});

});

</script>

<style>
.ui-datepicker-calendar {
    display: none;
}
</style>

<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">VC Report</span></legend>
<div id="panel-agent-content">
	<table border=0 width='100%'>
		<tr>
			<td width='35%' valign="top">
				<fieldset class="corner" style='margin-top:px;'>
					<legend class="icon-menulist">&nbsp;&nbsp;User Filter </legend>
					<form name="frmReport">
					<div>
						<table cellpadding='4' cellspacing=4>
							<tr>
								<td class="text_caption bottom">Select Report </td>
								<td >:</td>
								<td class='bottom'><?php echo form()->combo('report_type','select auto',$cmb_random_modul );?></td>
							</tr>
							
							<tr>
								<td class="text_caption bottom">Interval</td>
								<td >:</td>
								<td class='bottom'> <?php echo form()->input('start_date_claim','input_text box date');?> &nbsp- <?php echo form()->input('end_date_claim','input_text box date');?> </td>
							</tr>
							
							<tr>
								<td class="text_caption"> &nbsp;</td>
								<td >&nbsp;</td>
								<td class='bottom'>
									<?php echo form()->button('','page-go button','Show',array("click"=>"ShowReport();") );?>
									<?php echo form()->button('','excel button','Export',array("click"=>"ShowReportExcel();") );?>
								</td>
							</tr>	
						</table>
					</div>
					</form>
				</fieldset>
			</td>
		</tr>
	</table>
</div>
</fieldset>