<?php echo javascript(); ?>

<div class="custom-grid" style="background-color:#FFFFFF;margin-top:10px;">	
	<div class="content_table">
		<table width="100%" class="custom-grid" cellspacing="1" border=1>
		<thead>
			<tr height="24"> 
				<th nowrap class="font-standars ui-corner-top ui-state-default middle center" style='color:#608ba9'>&nbsp;No.</th>
				<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left">&nbsp;<span class="header_order" ><b style='color:#608ba9;'>Campaign Code</b></span></th>
				<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left">&nbsp;<span class="header_order" ><b style='color:#608ba9;'>Campaign Name</b></span></th>
				<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left">&nbsp;<span class="header_order" ><b style='color:#608ba9;'>Policies Uploaded</b></span></th>
			</tr>
		</thead>	
		<tbody>
		<?php
			$no = 1;
			if ($content) foreach( $content -> result_assoc() as $rows )
			{ 
			  $color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
			  
			 ?>
				<tr class="onselect">
					<td align="left" class="content-first"><b style="color:green;"><?php echo $no; ?></td>	
					<td align="left" class="content-middle"><b style="color:green;"><?php echo $rows['Campaign_Master_Code'];?></td>
					<td align="left" class="content-middle"><b style="color:green;"><?php echo $rows['CampaignName'];?></td>
					<td align="left" class="content-middle"><b style="color:green;"><?php echo $rows['row_upload'];?></td>
				</tr>	
						
			</tbody><?php
				$no++;
			};
		?>
		</table>
		
		
	</div>
</div>
