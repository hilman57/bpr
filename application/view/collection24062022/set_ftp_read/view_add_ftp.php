<fieldset  class='corner' style='margin:8px 15px 0px 10px;'>
	<legend class='icon-menulist'>&nbsp;&nbsp;Add Read FTP </legend>
	<div>
	<form name="FtpFormTemplate">
		<table>
			<tr>
				<td class='text_caption bottom'>* Work Project </td>
				<td><?php __(form()->combo('ftp_read_project_code','select long', $WorkProject)); ?></td>
				<td class='text_caption bottom'>* Template Name </td>
				<td><?php __(form()->combo('ftp_read_template_Id','select long', $TemplateName)); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>*  In/Out Directory</td>
				<td><?php __(form()->input('ftp_read_directory','select long')); ?></td>
				<td class='text_caption bottom'>*  History Directory</td>
				<td><?php __(form()->input('ftp_read_dir_history','select long')); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>* Read/ Create File</td>
				<td><?php __(form()->input('ftp_read_filetype','select long')); ?></td>
				<td class='text_caption bottom'>* Controll File</td>
				<td><?php __(form()->input('ftp_read_ctltype','select long')); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>* Mode</td>
				<td><?php __(form()->combo('ftp_read_mode','select long', array('GET' => 'GET','PUT' => 'PUT'))); ?></td>
				<td class='text_caption bottom'>* UserPrivileges</td>
				<td><?php __(form()->combo('ftp_read_privilege','select long', $UserPrivileges, null, array("change" => "Ext.DOM.WorkUserByLevel(this.value);") )); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'></td>
				<td></td>
				<td class='text_caption bottom'>* User </td>
				<td><span id="divUserId"><?php __(form()->combo('ftp_read_userid','select long', $UserId)); ?></span></td>
			</tr>
			
			
			<tr>
				<td class='text_caption bottom'>&nbsp;</td>
				<td>
					<?php __(form()->button('FtpSaveButton','save button', 'Save', array("click" =>"Ext.DOM.FtpSaveButton();"))); ?>
					<?php __(form()->button('FtpCloseButton','close button', 'Cancel', array("click" =>"Ext.Cmp('span_top_nav').setText('');"))); ?>
				
					</td>
			</tr>
		</table>
		</form>
	</div>
</fieldset>