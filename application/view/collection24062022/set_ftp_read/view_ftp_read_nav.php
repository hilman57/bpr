<?php echo javascript(); ?>
<script type="text/javascript">

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();


Ext.DOM.datas={}
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
$(function(){
	$('#toolbars').extToolbars({
				extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
				extTitle :[['Add'],['Edit'],['Delete']],
				extMenu  :[['FTP_adding'],['FTP_edit'],['FTP_delete']],
				extIcon  :[['add.png'],['calendar_edit.png'],['delete.png']],
				extText  :true,
				extInput :false,
				extOption: []
			});
			
});

Ext.EQuery.TotalPage   = <?php echo (INT)$page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo (INT)$page -> _get_total_record(); ?>;

/* assign navigation filter **/

Ext.DOM.navigation = {
	custnav : Ext.DOM.INDEX +'/SetReadFTP/index/',
	custlist : Ext.DOM.INDEX +'/SetReadFTP/Content/'
}

/* assign show list content **/
//console.log(navigation);
		
Ext.EQuery.construct(Ext.DOM.navigation,Ext.DOM.datas);
Ext.EQuery.postContentList();
/* FTP_delete **/
Ext.DOM.WorkUserByLevel = function( ProfileId ) {
	Ext.Ajax({
		url  : Ext.DOM.INDEX +'/SetReadFTP/WorkUserByLevel/',
		method : 'GET',
		param : {
			ProfileId : ProfileId
		}
	}).load('divUserId');
}

/* FTP_delete **/

Ext.DOM.FTP_delete = function(){
	var ftp_read_id = Ext.Cmp('ftp_id').getValue();
if( Ext.Msg('Do You want to delete this rows ?').Confirm() )
{
	 Ext.Ajax ({
		url 	: Ext.DOM.INDEX +"/SetReadFTP/FTPDelete/",
		method 	: 'POST',
		param 	: { Id : ftp_read_id },
		ERROR 	: function(e)
		{
			Ext.Util(e).proc(function(response){
				if( response.success ){
					Ext.Msg("Delete FTP Read ").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Delete FTP Read ").Failed();
				}	
			})
		}
	}).post();
 }	
}


/** add ftp configutayion **/

Ext.DOM.FTP_edit = function() {
var  ftp_read_id = Ext.Cmp('ftp_id').getValue();

	Ext.Ajax({
		url : Ext.DOM.INDEX +"/SetReadFTP/FTPEdit/",
		mthod :'GET',
		param :{
			time : Ext.Date().getDuration(),
			ftp_read_id : ftp_read_id
		}
	}).load("span_top_nav");
}


/** add ftp configutayion **/

Ext.DOM.FTP_adding = function() {
	Ext.Ajax({
		url : Ext.DOM.INDEX +"/SetReadFTP/FTPAdd/",
		mthod :'GET',
		param :{
			time : Ext.Date().getDuration()
		}
	}).load("span_top_nav");
}

// save new data read ftp 

Ext.DOM.FtpSaveButton = function() {
	var conds = Ext.Serialize('FtpFormTemplate').Complete();
	if( conds ) 
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +"/SetReadFTP/FTPSave/",
			method 	: 'POST',
			param 	: Ext.Join([
						Ext.Serialize('FtpFormTemplate').getElement()
					]).object(),
					
			ERROR 	: function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg("Save FTP Read ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Save FTP Read ").Failed();
					}	
				})
			}
		}).post();
	}
 }



/* update ftp data read **/

Ext.DOM.FtpEditButton = function() {
	var conds = Ext.Serialize('FtpEditTemplate').Complete();
	if( conds ) 
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +"/SetReadFTP/FTPUpdate",
			method 	: 'POST',
			param 	: Ext.Join([
						Ext.Serialize('FtpEditTemplate').getElement()
					]).object(),
					
			ERROR 	: function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg("Update FTP Read ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Update FTP Read ").Failed();
					}	
				})
			}
		}).post();
	}
 }
 
</script>
<!-- start : content -->
<fieldset class="corner">
 <legend class="icon-callresult">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="toolbars" style='margin:0px 15px 0px 10px;'></div>
 <div id="span_top_nav"></div>
 <div class="content_table"></div>
 <div id="pager"></div>
</fieldset>	
	
<!-- stop : content -->
	
	
	