<?php 
__(javascript
(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time())
)));
?>

<script type="text/javascript">
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
var Reason = [];

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.datas =  {
	faxnumber 	: '<?php echo _get_post('faxnumber');?>',
	agent_id 	: '<?php echo _get_post('agent_id');?>',
	agent_group : '<?php echo _get_post('agent_group');?>',
	start_time 	: '<?php echo _get_post('start_time');?>',
	end_time 	: '<?php echo _get_post('end_time');?>',
	keywords	: '<?php echo _get_post('keywords');?>',	
	order_by	: '<?php echo _get_post('order_by');?>',
	type		: '<?php echo _get_post('type');?>'
}

			
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
var navigation = {
	custnav : Ext.DOM.INDEX +'/MailGroupFollowup/index/',
	custlist : Ext.DOM.INDEX +'/MailGroupFollowup/content/'
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */		
 
 Ext.EQuery.construct(navigation,Ext.DOM.datas)
 Ext.EQuery.postContentList();

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.DOM.FindByKeyword = function(){
	Ext.EQuery.construct(navigation, {
			keywords : Ext.Cmp('keywords').getValue()
		});
	Ext.EQuery.postContent();
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.ValidateEmail = function(email) 
{ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.val());
} 

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.Edit = function(){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/MailGroupFollowup/EditMailGroup/",
		method  : 'GET',
		param   : {
			FuEMaiId : Ext.Cmp('FuEMaiId').getValue()
		}
	}).load('play_panel');
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.Add = function(){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/MailGroupFollowup/AddMailGroup/",
		method  : 'GET',
		param   : {
			time: Ext.Date().getDuration()
		}
	}).load('play_panel');
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.Delete = function(){

if( Ext.Cmp('FuEMaiId').getValue()=='' ){ 
	Ext.Msg('Please Select Rows ').Info(); } 
else 
{
  Ext.Ajax 
  ({
		url 	: Ext.DOM.INDEX+"/MailGroupFollowup/DeleteMailGroup/",
		method  : 'POST',
		param   : { 
			FuEMaiId : Ext.Cmp('FuEMaiId').getValue()
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(response){
			if(response.success){		
				Ext.Msg("Delete Mail Group Folllowup ").Success(); 
				Ext.EQuery.postContent();
			}else{
				Ext.Msg("Delete Mail Group Folllowup ").Failed(); 
			}
		});
	  }
  }).post();
}
	
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.SaveAddMailGroup = function() {
var conds = Ext.Serialize('frmAddMailGroup').Complete();
if( conds ) {
 
 if( Ext.DOM.ValidateEmail($('#FuEMailAddr')) ) 
 {
	Ext.Ajax 
	({
		url 	: Ext.DOM.INDEX+"/MailGroupFollowup/SaveAddMailGroup/",
		method  : 'POST',
		param   : Ext.Join([
			Ext.Serialize('frmAddMailGroup').getElement()
		]).object(),
			
		ERROR : function( e ) {
				Ext.Util(e).proc(function( response ) {
					if( response.success ){ 
						Ext.Msg("Save Mail Group Folllowup ").Success(); 
						Ext.EQuery.postContent();
						Ext.DOM.Add();
					}
					else{
						Ext.Msg("Save Mail Group Folllowup ").Failed(); }
				});
		}
			
	}).post();	
	
} else {
  Ext.Msg("Address  Not Valid ").Info();
  $('#FuEMailAddr').css({'border':'1px solid #FF4321' });
}

}	

}


/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.UpdateAddMailGroup = function() {
var conds = Ext.Serialize('frmEditMailGroup').Complete();
if( conds ) {
 
 if( Ext.DOM.ValidateEmail($('#FuEMailAddr')) ) 
 {
	Ext.Ajax 
	({
		url 	: Ext.DOM.INDEX+"/MailGroupFollowup/UpdateAddMailGroup/",
		method  : 'POST',
		param   : Ext.Join([
			Ext.Serialize('frmEditMailGroup').getElement()
		]).object(),
			
		ERROR : function( e ) {
				Ext.Util(e).proc(function( response ) {
					if( response.success ){ 
						Ext.Msg("Update Mail Group Folllowup ").Success(); 
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Update Mail Group Folllowup ").Failed(); }
				});
		}
			
	}).post();	
	
} else {
  Ext.Msg("Address  Not Valid ").Info();
  $('#FuEMailAddr').css({'border':'1px solid #FF4321' });
}

}	

}
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

$(function(){
	$('#toolbars').extToolbars
	({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Add'],['Edit'],['Delete'],[],['Find']],
		extMenu   : [['Add'],['Edit'],['Delete'],[],['FindByKeyword']],
		extIcon   : [['email_add.png'],['email_edit.png'],['email_delete.png'],[],['find.png']],
		extText   : true,
		extInput  : true,
		extOption : [{
			render : 3,
			type   : 'text',
			id     : 'keywords', 	
			name   : 'keywords',
			value  : Ext.DOM.datas.keywords
		}]
	});
	
	$('.box').datepicker({
		showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly: true, 
		dateFormat:'dd-mm-yy',readonly:true
	});
});

</script>

<!-- start : content -->
<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
</div>
	
	<div id="toolbars" style="margin-left:12px;margin-right:12px"></div>
	<div id="play_panel"></div>
	<div id="recording_panel" class="xbox-shadow">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	