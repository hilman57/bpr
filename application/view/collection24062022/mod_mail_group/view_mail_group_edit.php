<?php 

/** look heres **/

$work_set = NULL;
$work_disabled = NULL;

if( _get_session('HandlingType')!=USER_ROOT)
{
  $work_set = reset(_get_session('ProjectId'));
  $work_disabled = array('disabled'=>true);
}

?>
<fieldset class="corner" style="background-color:white;margin:12px;">
	<legend class="icon-application">&nbsp;&nbsp;Edit Mail Group</legend>	
	<form name="frmEditMailGroup">
	<div class ="" style="overflow:auto;margin:10px;border:0px solid #000;">
	<input type="hidden" name="FuEMaiId" id="FuEMaiId" value="<?php echo $WorkData['FuEMaiId'];?>">
	<table border=0 width="100%" align="left" cellspacing=2 cellpadding=8>
		<tr>
			<td class="text_caption bottom" nowrap>* Work Project&nbsp;: </td>
			<td class="bottom"><?php echo form()->combo('FuEMailProjectId','select long',$WorkProject,$work_set, NULL,$work_disabled);?></td>
		</tr>
		<tr>
			<td class="text_caption bottom" nowrap>* Group Code&nbsp;: </td>
			<td class="bottom" valign='middle'><?php echo form()->combo('FuEMailFollowupCode','select long',$WorkCode,$WorkData['FuEMailFollowupCode']);?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom" nowrap>* Description&nbsp;: </td>
			<td class="bottom"><?php echo form()->textarea('FuEMailDesc','textarea long long',$WorkData['FuEMailDesc'],null);?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom" nowrap>* Address&nbsp;: </td>
			<td class="bottom"><?php echo form()->input('FuEMailAddr','input_text long',$WorkData['FuEMailAddr'],null);?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom" nowrap>* Ref&nbsp;: </td>
			<td class="bottom"><?php echo form()->combo('FuEmailToCc','select',array('to'=>'To','cc'=>'CC','bcc'=>'BCC'),'to');?></td>
		</tr>
		<tr>
			<td class="text_caption bottom" nowrap>* Status&nbsp;: </td>
			<td class="bottom"><?php echo form()->combo('FuEMailShow','select',array('Y'=>'Active','N'=>'Not Active'),$WorkData['FuEMailShow']);?></td>
		</tr>
		
		<tr>	
		<td class="text_caption" width="8%">&nbsp;</td>
			<td>
				<input type="button" class="update button" onclick="Ext.DOM.UpdateAddMailGroup();" value="Update"> 
				<input type="button" class="close button" onclick="Ext.Cmp('play_panel').setText('');" value="Close"> 
			</td>
		</tr>
	<table>	
</div>	
</form>
</fieldset>			