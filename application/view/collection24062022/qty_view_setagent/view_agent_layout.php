<table border=0 align="left" cellspacing=1 width='60%'>
	<tr height=24>
		<th class="font-standars ui-corner-top ui-state-default first center" width='5%'>#</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width='5%'>No.</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width='10%'>UserId</th>
		<th class="font-standars ui-corner-top ui-state-default first center">Fullname</th>
	</tr>
<?php 
		$no = 1;
		foreach( $view_agents as $num => $rows ) :
			$color= ($no%2!=0?'#FAFFF9':'#FFFFFF');
			
	?>
		<tr height=24 class="onselect" bgcolor="<?php echo $color;?>" >
			<td class='content-first center' width='5%'><?php __(form() -> checkbox('UserId',null,$rows['UserId']))?> </td>
			<td class='content-middle center' width='5%'><?php __($num); ?> </td>
			<td class='content-middle center'>&nbsp;<?php __($rows['id']); ?></td>
			<td class='content-middle left'>&nbsp;<?php __($rows['full_name']); ?></td>
		</tr>
	<?php 
		$no++;
	endforeach; ?>
	
	<tr height=24>
		<th class="font-standars ui-corner-top ui-state-default left" colspan=4>
			<div style='margin:2px 2px 2px 2px;'>
				<span> Page : </span> 
				<span> <?php __(form()->combo('select_page', 'select box',$view_page,( $select_page ? $select_page:1), array("change" =>"Ext.DOM.LaodAgentState(this.value);")));?>  
				&nbsp;<span> Records : ( <?php __($view_record); ?> )  
			</div>
		</th>
	</tr>
	
</table>