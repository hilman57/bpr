<?php $this -> load -> view( 'mod_find_customer_detail/UserHeader'); ?>
<script type="text/javascript">

var data  = {
	cif_number 		: "<?php echo $cif_number;?>",
	customer_name 	: "<?php echo $customer_name;?>",
	page 			: "<?php echo $page;?>",
	policy_number 	: "<?php echo $policy_number;?>"
}	
		
		
//console.log(data);		
/*
 * @ def : global varibael on keyup code 
 */

Ext.DOM.INDEX =( function(e ){
	return e.opener.Ext.DOM.INDEX;
})( window );


var KEY_CODE = { 
 'KEY_ENTER'  : 13,  
 'KEY_UPDOWN' : '',
 'KEY_DOWN'   : ''
}

/*
 * @ def : global varibael on keyup code 
 * http://192.168.1.190/operation/index.php/FindCustomerDetail/index/function%20()%7Bwindow.opener.Ext.DOM.INDEX;%7D/test/apa?test=
 */
 
window.onkeyup = function( e ) {

 if(( e.keyCode == KEY_CODE.KEY_ENTER)) {	
		Ext.DOM.Page(0);
  }	
}	

/*
 * @ def : global varibael on keyup code 
 * http://192.168.1.190/operation/index.php/FindCustomerDetail/index/function%20()%7Bwindow.opener.Ext.DOM.INDEX;%7D/test/apa?test=
 */
 
Ext.DOM.Default = function() {
Ext.Ajax 
	({
		url 	: Ext.DOM.INDEX+"/FindCustomerDetail/FindByKeywords/",
		method 	: 'POST',
		param 	: {
				cif_number : data.cif_number,
				customer_name : data.customer_name,
				page : data.page,
				policy_number : data.policy_number
			}
					
	 }).load("content-data-select");
}	
 
Ext.DOM.Page = function( page )  
{
	Ext.Cmp('select_page').setValue(page);
	Ext.Ajax
	 ({
		url 	: Ext.DOM.INDEX+"/FindCustomerDetail/FindByKeywords/",
		method 	: 'POST',
		param 	: {
				cif_number : Ext.Cmp('cif_number').getValue(),
				customer_name : Ext.Cmp('customer_name').getValue(),
				page : page,
				policy_number : Ext.Cmp('policy_number').getValue()
			}
					
	 }).load("content-data-select");
}	

/*
 * @ def : global varibael on keyup code 
 */
 
 
Ext.DOM.SelectedRows = function( CustomerId , PolicyId ){
	
	 Ext.Ajax ({
		url 	: Ext.DOM.INDEX+"/FindCustomerDetail/SelectDetail/",
		method 	: 'POST',
		param 	: {
			CustomerId 	  : CustomerId,
			PolicyId 	  : PolicyId, 
			page 		  : Ext.Cmp('select_page').getValue(),
			cif_number 	  : Ext.Cmp('cif_number').getValue(),
			customer_name : Ext.Cmp('customer_name').getValue(),
			policy_number : Ext.Cmp('policy_number').getValue()
				
		}
				
	 }).load("content-data-select");
}

/*
 * @ def : global varibael on keyup code 
 */
 
Ext.DOM.Clear = function(){
	Ext.Serialize('frm_find_keywords').Clear();
	Ext.Cmp('select_page').setValue(0);
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX+"/FindCustomerDetail/FindByKeywords/",
		method 	: 'POST',
		param 	: {
				page :Ext.Cmp('select_page').getValue(0),
				cif_number : Ext.Cmp('cif_number').getValue(),
				customer_name : Ext.Cmp('customer_name').getValue(), 
				policy_number : Ext.Cmp('policy_number').getValue()
			}
					
	 }).load("content-data-select");
}
/*
 * @ def : global varibael on keyup code 
 */
 
$(document).ready(function(){ 
	Ext.DOM.Default();
	Ext.Cmp('select_page').setValue(data.page);
	Ext.Cmp('cif_number').setValue(data.cif_number);
	Ext.Cmp('customer_name').setValue(data.customer_name);
	Ext.Cmp('policy_number').setValue(data.policy_number);
});

</script>
<body>
<input type="hidden" id="select_page" name="select_page" value="<?php echo $page;?>">
<div style='margin:5px;float:center;border:0px solid #dddddd;'> 
	<form name="frm_find_keywords">
		<table border=0 width='100%' align='center'>
			<tr> 
				<td class='text_caption left'>* CIF Number</td>
				<td><?php echo form()->input('cif_number', 'input_text long'); ?></td>
				<td class='text_caption left'>* Policy Number </td>
				<td><?php echo form()->input('policy_number', 'input_text long'); ?></td>
				<td class='text_caption left'>* Customer Name</td>
				<td><?php echo form()->input('customer_name', 'input_text long'); ?>
					&nbsp;<div onclick="Ext.DOM.Clear();" class="clear" 
					style="cursor:pointer;padding-left:16px;width:30px;float:right;">&nbsp;Clear </div>
				</td>
			</tr>
		</table>
	</form>	
</div>	
<div style='margin:0px;' id="content-data-select"></div>
</body>