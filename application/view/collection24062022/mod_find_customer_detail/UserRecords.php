<?php
foreach( $record_assoc as $nums => $rows )
{
 $color = ( ($nums%2)==0 ? '#FFFEEE':'#FFFFFF');
	__("<tr bgcolor=\"{$color}\" class=\"onselect\" 
			style=\"cursor:pointer;\" id=\"{$rows['CustomerId']}\" 
			onclick=\"Ext.DOM.SelectedRows(this.id, '{$rows['PolicyId']}');\">
			<td class=\"content-first\">{$nums}</td>
			<td class=\"content-middle\">{$rows['CIFNumber']}</td>
			<td class=\"content-middle\">{$rows['PolicyNumber']}</td>
			<td class=\"content-middle\">{$rows['CustomerFirstName']}</td>
			<td class=\"content-middle center\">{$rows['PolicyIssDate']}</td>
			<td class=\"content-middle center\">{$rows['CallResult']}</td>
			<td class=\"content-middle center\">{$rows['CallAttempt']}</td>
			<td class=\"content-middle center\">{$rows['PolicyUpdatedTs']}</td>
			<td class=\"content-middle left\">{$rows['UserName']}</td>
			<td class=\"content-lasted left\">{$rows['QualityName']}</td>
		</tr>");
	
	$nums++;
}

?>

<tbody>
<tr> 
<td colspan='10'>	
<?php 
// total_page 

$totals_page = count($total_page);
$max_page = 15;

$paging  = " <div class='page-web-voice' style='margin-top:10px;'> <ul > ";
/** settup data page **/
	
$start   = (!$current ? 1: ((($current%$max_page ==0) ? ($current/$max_page) : intval($current/$max_page)+1)-1)*$max_page+1);
$end     = ((($start+$max_page-1)<=$totals_page) ? ($start+$max_page-1) : $totals_page );
		
	
$start   = (!$current ? 1: ((($current%$max_page ==0) ? ($current/$max_page) : intval($current/$max_page)+1)-1)*$max_page+1);
$end     = ((($start+$max_page-1)<=$totals_page) ? ($start+$max_page-1) : $totals_page );

if( $current > 1)
	{
		$post = (INT)(($current)-1);
		$paging .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.Page('1');\" ><a href=\"javascript:void(0);\">&lt;&lt;</a></li>";
		$paging .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.Page('$post');\" ><a href=\"javascript:void(0);\">&lt;</a></li>";
	}
	
	if($start>$max_page){
		$paging.="<li cclass=\"page-web-voice-normal\"  onclick=\"Ext.DOM.Page('" .($start-1) . "');\" ><a href=\"javascript:void(0);\">...</a></li>";
	}
		
	for( $p=$start; $p<=$end; $p++)
	{ 
		if( $p==$current) { 
			$paging .="<li class=\"page-web-voice-current\" id=\"{$selpages[$p]}\" onclick=\"Ext.DOM.Page('{$p}');\"> <a href=\"javascript:void(0);\">{$p}</a></li>";
		}					
		else {
			$paging	.=" <li class=\"page-web-voice-normal\" id=\"{{$selpages[$p]}}\" onclick=\"Ext.DOM.Page('{$p}');\"><a href=\"javascript:void(0);\">{$p}</a></li>";
		}
	}
	
	if($end<$totals_page){
		$paging.="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.Page('".($end+1) ."');\"><a href=\"javascript:void(0);\" >...</a></li>";
	}
	
	if($current<$totals_page){
		$paging.="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.Page('". ($current+1) ."');\"><a href=\"javascript:void(0);\" title=\"Next page\">&gt;</a></li>";
		$paging.="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.Page('". ($totals_page)."');\"><a href=\"javascript:void(0);\" title=\"Last page\">&gt;&gt;</a></li>";
	}
		
	
	$paging.="</ul></div>";
	__($paging);
	
?>
</td> 
</tr>	
</tbody>

