<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="author" content="<?php echo $website['_web_author']; ?>"/>
<meta name="version" content="<?php echo $website['_web_verion'];?>"/>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>
<meta http-equiv="Content-Script-Type" content="text/javascript">
<title><?php echo $website['_web_title'];?></title>

<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.overiter.css?time=<?php echo time();?>" />
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-1.3.2.js?time=<?php echo time();?>"></script>    
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-ui.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_jquery();?>/external/bgiframe/jquery.bgiframe.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_jquery();?>/plugins/extToolbars.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script>
