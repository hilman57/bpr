<?php echo base_window_header("Multi Add Phone"); ?>
<style> 
.border-left { border-right: 0px solid #000;}
.border-right { border-right: 0px solid #000;}
.border-lasted { border-right: 1px solid #ddd;}
.ui-widget-form-cell:first-child { border-left:1px solid #ddd;  border-bottom:1px solid #ddd;padding-left:2px;}
.ui-widget-form-cell:last-child { text-align:center;border-left:1px solid #ddd; border-bottom:1px solid #ddd; padding-left:2px;}
.ui-widget-form-cell:nth-child(n+1){ text-align:center;border-left:1px solid #ddd;border-bottom:1px solid #ddd; padding-left:2px;}	
 input.height { height:24px;border-radius:2px;width:98%;}
 select.height { height:26px;border-radius:2px;width:99%;}
 button.height { height:20x;}
 input:hover{ border: 1px solid red;}

</style>

<script>

var __parent__ = window.opener;

// ------------------------------------------------------------
/*
 * @ package SaveAdditionalPhone
 */
 function SaveAdditionalPhone( combo )
{
  if( !combo.checked )	{
	return false;
  }	  
  
  var frmRequired = {};
  var PhoneTyped = new Array('phone_type_',combo.value ).join(''); 
  var PhoneRelation = new Array('phone_relation_',combo.value ).join('');
  var PhoneNumber = new Array('phone_number_',combo.value ).join('');
  
  var ValPhoneTyped = Ext.Cmp(PhoneTyped).getValue();
  var ValPhoneRelation = Ext.Cmp(PhoneRelation).getValue();
  var ValPhoneNumber = Ext.Cmp(PhoneNumber).getValue();
  
  frmRequired[PhoneTyped] = PhoneTyped;
  frmRequired[PhoneRelation] = PhoneRelation;
  frmRequired[PhoneNumber] = PhoneTyped;
  

 var frmAddPhone = Ext.Serialize('frmAddPhone');
 var frmCond =  frmAddPhone.Required( Object.keys(frmRequired) );
 if( !frmCond ) {
	 Ext.Msg('Failed Input Data').Info();
	 return false;
 }
 
 var ValText = new Array("#label_", combo.value).join('');
 $(ValText).css("height", "50px").addClass("ui-widget-ajax-spiner-min"); 
 
// ------------- then send to process ------------------------------------
 
  Ext.Ajax
 ({
	url 	: __parent__.Ext.EventUrl(new Array('ModApprovePhone', 'SaveMultiplePhone') ).Apply(),
	method  : 'POST',
	param   : {
		PhoneTyped 	  : Ext.Cmp(PhoneTyped).getValue(),
		PhoneRelation : Ext.Cmp(PhoneRelation).getValue(),
		PhoneNumber   : Ext.Cmp(PhoneNumber).getValue(),
		CustomerId	  : Ext.Cmp(CustomerId).getValue()	
	},
	ERROR : function( response )
	{
		Ext.Util(response).proc(function( data )
		{
			if( data.success == 1 ) 
			{
				$(ValText).removeClass("ui-widget-ajax-spiner-min"); 
				$(ValText).html('OK');
				window.setTimeout(function(){ 
					$(ValText).html('');	
				}, 1000);
			} else {
				$(ValText).removeClass("ui-widget-ajax-spiner-min"); 	
				$(ValText).html('');
			}	
		});
	}
 }).post();
 
} 

// ------------------------------------------------------------
/*
 * @ package SaveAdditionalPhone
 */
 
$(document).ready(function(){
	$('.ui-select-disable').each(function(){
		$(this).attr("disabled", true);	
	});
});

</script>
</head>
<body style="margin:20px;">
<fieldset class="corner" style="border-radius:3px;">
	<legend class="icon-customers"> 
		<span style="margin-left:10px;">Multiple - Additional Phone</span></legend>
		
	<form name="frmAddPhone">	
	<?php echo form()->hidden("CustomerId", null, _get_post('CustomerId'));?>
	<div class="ui-widget-form-table" style="margin:5px 5px 5px 5px;width:99%;">
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell ui-corner-top ui-state-default border-left">Phone Type</div>
			<div class="ui-widget-form-cell ui-corner-top ui-state-default border-right">Phone Relation</div>
			<div class="ui-widget-form-cell ui-corner-top ui-state-default border-right">Phone Number</div>
			<div class="ui-widget-form-cell ui-corner-top ui-state-default border-lasted"># Save </div>
		</div>
		
	<?php 
	$arr_listed = PhoneMultiple(_get_post('CustomerId'));
	foreach( PhoneType() as $value_index => $value_text ) : 
		$out = new EUI_Object( $arr_listed[$value_index]);	
	?>		
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell"><?php echo form()->combo("phone_type_${value_index}", "select long height ui-select-disable", PhoneType(), $value_index );?></div>
			<div class="ui-widget-form-cell border-right"><?php echo form()->combo("phone_relation_${value_index}", "select long height", PhoneRelation(), $out->get_value('rel_phone') );?></div>
			<div class="ui-widget-form-cell border-right"><?php echo form()->input("phone_number_${value_index}", "input_text long height", $out->get_value('num_phone'));?></div>
			<div class="ui-widget-form-cell border-lasted"><?php echo form()->checkbox("chk_assign", null,$value_index, array("change" => "new SaveAdditionalPhone(this);"));?><span id="<?php echo "label_{$value_index}";?>"> </span></div>
				
		</div>
	<?php endforeach;?>
	
	</div>
	</form>
	
</fieldset>	
</body>
</html>