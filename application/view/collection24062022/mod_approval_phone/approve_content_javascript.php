<script>
var V_SCRIPT = {};

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.Reject = function(){
var ApproveItems = Ext.Cmp('chk_apprv').getValue();
  if( ApproveItems.length > 0 )
  {
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/ModApprovePhone/ApproveItem/',
		method : 'POST',
		param :{
			ApproveItemId : ApproveItems, 
			action : 'reject'
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(Reject){
				if( Reject.success ){
					Ext.Msg("Reject rows data").Success();
					Ext.EQuery.postContent();
				}	
				else{
					Ext.Msg("Reject rows data").Failed();
				}
			});
		}
	}).post();
  }
  else{
	Ext.Msg("Please select a rows ").Error();
  }
}
 
Ext.DOM.Approve = function(){
var ApproveItems = Ext.Cmp('chk_apprv').getValue();
  if( ApproveItems.length > 0 )
  {
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/ModApprovePhone/ApproveItem/',
		method : 'POST',
		param :{
			ApproveItemId : ApproveItems, 
			action : 'approve'
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(approve){
				if( approve.success ){
					Ext.Msg("Approve rows data").Success();
					Ext.EQuery.postContent();
				}	
				else{
					Ext.Msg("Approve rows data").Failed();
				}
			});
		}
	}).post();
  }
  else{
	Ext.Msg("Please select a rows ").Error();
  }
}

Ext.query(function(){


 Ext.query("#tabs" ).tabs();
 Ext.query('#toolbars').extToolbars({
		extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle  : [['Approve'],['Reject'],['Close']],
		extMenu   : [['Approve'],['Reject'],['CancelActivity']],
		extIcon   : [['accept.png'],['delete.png'],['cancel.png']],
		extText   : true,
		extInput  : false,
		extOption  : [{
				render : 0,
				type   : 'combo',
				header : 'Script ',
				id     : 'v_result_script', 	
				name   : 'v_result_script',
				triger : 'ShowWindowScript',
				width  : 220,
				store  : []
			}]
	});
});
/* 
 * @ def : ShowWindowScript
 * 
 Ext.Ajax({ url : Ext.DOM.INDEX +'/SetProductScript/getScript/' }).json()
 ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.ShowWindowScript = function(ScriptId)
{
	var WindowScript = new Ext.Window ({
			url    : Ext.DOM.INDEX +'/SetProductScript/ShowProductScript/',
			name    : 'WinProduct',
			height  : (Ext.Layout(window).Height()),
			width   : (Ext.Layout(window).Width()),
			left    : (Ext.Layout(window).Width()/2),
			top	    : (Ext.Layout(window).Height()/2),
			param   : {
				ScriptId : Ext.BASE64.encode(ScriptId),
				Time	 : Ext.Date().getDuration()
			}
		}).popup();
		
	if( ScriptId =='' ) {
		window.close();
	}
}

/* 
 * @ def : ShowWindowScript
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.PlayRecording = function(check) {
var chk = Ext.Cmp('recordId').getName();
if( check.checked ) {
  for(var c in chk ) {
	if( chk[c].checked) {
		if( chk[c].value!=check.value )  chk[c].checked = false;
		else 
		{
			$('#tabs').tabs( "option", "selected", 2 );
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/VoicePlay/",
				method 	: 'GET',
				param  	: { RecordId : check.value },
				ERROR 	: function(e){
					Ext.Util(e).proc(function(fn){
						Ext.Media("tabs-3",{ 
							url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
							width 	: '50%',
							height 	: '8%',
							options : {
								ShowControls 	: 'true',
								ShowStatusBar 	: 'true',
								ShowDisplay 	: 'true',
								autoplay 		: 'true'
							}
						}).GSMPlayer(); 
						Ext.Tpl("tabs-3", fn.data).Compile();
					});
					
					Ext.Cmp('QuikTime').setAttribute('class','textarea');
					Ext.Css('QuikTime').style({
						"margin-left":"5px","padding-top":"20px",
						"padding-left":"4px","padding-right":"4px"});
					
				}
			}).post();
		}
	}
}}}

Ext.DOM.PlayByCallSession = function(SessionId) 
{
	$('#tabs').tabs( "option", "selected", 2);
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/PlayBySessionId/",
		method 	: 'GET',
		param  	: { RecordId : SessionId },
		ERROR 	: function(e){
			Ext.Util(e).proc(function(fn){
				if(fn.success) {
					Ext.Media("tabs-3",{ 
						url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
						width 	: '99%',
						height 	: '70px',
						options : {
							ShowControls 	: 'true',
							ShowStatusBar 	: 'true',
							ShowDisplay 	: 'true',
							autoplay 		: 'true'
						}
					}).WAVPlayer();
						
					Ext.Tpl("tabs-3", fn.data).Compile();
					Ext.Cmp('MediaPlayer').setAttribute('class','textarea');
					Ext.Css('tabs-3').style({'text-align' : 'left',  'padding-left' : "8px",  'padding-top' : "20px" });
					Ext.Css('div-voice-container').style({ "margin-top" : "5px", "width" : "100%", "margin-bottom" : "20px"  });
				}
			})
		}
	}).post();
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CancelActivity =function()
 {
	ControllerId = Ext.Cmp("ControllerId").getValue();
	Ext.EQuery.Ajax
	({
		url 	: ControllerId,
		method 	: 'GET',
		param 	: { act : 'back-to-list' }
	});
 }
 

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.SaveCustomerInfo = function(){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX +'/ModSaveActivity/SaveInfoCustomer/',
		method 	: 'POST',
		param 	: Ext.Join([Ext.Serialize('frmInfoCustomer').getElement()]).object(),
		ERROR   : function(fn){
			try{
				var ERR = JSON.parse(fn.target.responseText);
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();
}

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.CallHistory = function(){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/CallHistory/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue()
		}
	}).load("tabs-1");
} 

/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
Ext.DOM.SelectPages = function(select){
	Ext.Ajax ({
		url : Ext.DOM.INDEX +'/QtyApprovalInterest/Recording/',
		method : 'GET',
		param : { 
			CustomerId : Ext.Cmp('CustomerId').getValue(), 
			Pages: select.id,
			time : Ext.Date().getDuration()	
		}
	}).load('tabs-2');
}
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.document(document).ready(function(){
 // $('#CustomerDOB').datepicker({ 
  // dateFormat : 'yy-mm-dd', 
  // changeYear : true, 
  // changeMonth : true 
 // });
 
/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
  Ext.DOM.CallHistory();
  Ext.DOM.SelectPages({'id':0});
  
});


</script>