<?php ?>
<fieldset class="corner" style="margin-top:-3px;"> 
	<legend class="icon-menulist"> &nbsp;&nbsp;Owner Information</legend> 
<div id="quality_default_info" class="box-shadow box-left-top">
<form name="frmInfoCustomer">
<input type="hidden" name="CustomerId" id="CustomerId" value="<?php echo $Customers['CustomerId']; ?>" />
<input type="hidden" name="CustomerNumber" id="CustomerNumber" value="<?php echo $Customers['CustomerNumber']; ?>" />
<table width="99%" align="center" cellpadding="2px" cellspacing="4px">
	<tr>
		<td nowrap class="text_caption">Owner Name</td>
		<td><?php echo form() -> input('CustomerFirstName','input_text long',$Customers['CustomerFirstName'],NULL,array('readonly'=> 'readonly'));?> </td>
		<td class="text_caption" nowrap>Address 1</td>
		<td><?php echo form() -> input('CustomerAddressLine1','input_text long',$Customers['CustomerAddressLine1'],NULL,array('readonly'=> 'readonly'));?></td>
		<td class="text_caption" nowrap>City  </td>
		<td><?php echo form() -> input('CustomerCity','input_text long',$Customers['CustomerCity'],NULL,array('readonly'=> 'readonly'));?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap >Gender</td>
		<td><?php echo form() -> input('GenderId','input_text long',$Combo['Gender'][$Customers['GenderId']] ,NULL,array('readonly'=> 'readonly'));?></td>
		<!-- <td><?php //echo form() -> combo('GenderId','input_text long',$Combo['Gender'], $Customers['GenderId'],NULL,0);?></td> -->
		<td class="text_caption" nowrap>Address 2</td>
		<td><?php echo form() -> input('CustomerAddressLine2','input_text long',$Customers['CustomerAddressLine2'],NULL,array('readonly'=> 'readonly'));?></td>
		<td class="text_caption" nowrap>ZIP code</td>
		<td><?php echo form() -> input('CustomerZipCode','input_text long',$Customers['CustomerZipCode'],NULL,array('readonly'=> 'readonly'));?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap >DOB </td>
		<td><?php echo form() -> input('CustomerDOB','input_text long',date("d/m/Y", strtotime($Customers['CustomerDOB'])),NULL,array('readonly'=> 'readonly'));?></td>
		<td class="text_caption" nowrap>Address3</td>
		<td><?php echo form() -> input('CustomerAddressLine3','input_text long',$Customers['CustomerAddressLine3'],NULL,array('readonly'=> 'readonly')); ?></td>
	</tr>
</table>	
</form>
</div>
</fieldset>	