<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @ def    : include benefit of product 
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
echo javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Media.js', 'eui_'=>'1.0.0', 'time'=>time())
));

?>

<!-- detail content -->
<?php $this ->load ->view('mod_approval_phone/approve_content_javascript');?>
<fieldset class="corner"> 
<legend class="icon-customers"> &nbsp;&nbsp;Additional Phone Request </legend>
<div id="toolbars" class="contact"></div>

<input type="hidden" name="ControllerId" id="ControllerId" value="<?php echo _get_post("ControllerId"); ?>"/>
<input type="hidden" name="chk_apprv" id="chk_apprv" value="<?php echo _get_post("ApproveId"); ?>"/>
<div class="contact_detail" style="margin-left:-8px;">
	<table width="100%" border=0>
		<tr>
			<td  width="70%" valign="top">
				<?php $this ->load ->view('mod_approval_phone/approve_default_phone');?>
				<?php $this ->load ->view('mod_approval_phone/approve_history_phone');?>
			</td>
		</tr>
	</table>
	<div id="change_request_dialog" >
</div>
</fieldset>	
