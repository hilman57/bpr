<script>
	Ext.DOM.submitPhoneSelectType = function(obj){
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/ModApprovePhone/PhoneNumber/',
			method  : 'POST',
			param	: {
				CustomerId : Ext.Cmp('AddCustomerId').getValue(),
				FieldName  : obj.value
			},
			ERROR 	: function(e){
				Ext.Util(e).proc(function(select){
					$('#PhoneAddTypeValueId').val(select.phoneNumber);
				});
			}
		}).post();
	} 
 
</script>


<form name="frmAddPhone">
<?php __(form()->hidden('AddCustomerId',null,$Customer['CustomerId'])); ?>
<table cellspacing=2 cellpadding=4>
	<tr>
		<td class="text_caption bottom"> * Phone Type </td>
		<td class="bottom"><?php __(form()->combo('PhoneSelectTypeId','select long', $PhoneType,null, array('change' => 'Ext.DOM.submitPhoneSelectType(this);')) );?></td>
	</tr>
	
	<tr>
		<td class="text_caption bottom"> * Relation Type </td>
		<td class="bottom"><?php __(form()->combo('RelationshipTypeId','select long', $Relationship,null));?></td>
	</tr>
	
	<tr>
		<td class="text_caption bottom"> * Phone Number </td>
		<td class="bottom"><?php __(form()->input('PhoneAddTypeValueId','input_text long', null) );?></td>
	</tr>
	
</table>
</form>