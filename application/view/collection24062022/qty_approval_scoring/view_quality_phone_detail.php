<?php

/** 
 * @ def : grid of content policy by customer ID 
 * ----------------------------------------------------------------
 
 * @ param  : $ CustomerId 
 * @ refer  : $ Pilcy ID     
 * 
 */

 ?>

<!-- Questions --->

<fieldset class="corner" style="margin-top:-3px;">
<legend class="icon-menulist">&nbsp;&nbsp;Scoring</legend>

<div style="overflow:auto;margin-top:-12px;" class="activity-content">
	<table class="activity-table" cellpadding="3px;" cellspacing="1px" width='100%' align='center'>
		<tr>
			<td class="text_caption bottom" nowrap width='10%'> * Score Quality</td>
			<td class="left">
				<?php __(form()->combo('ScoringForm','select',$PolicyNumber, null, array('disabled'=>true)));?>
				<input type="button" name='chklabel' id="chklabel" class='button assign' 
				style='margin:0px;padding-left:22px;' 
				onclick="Ext.DOM.EventFormScoring( Ext.Cmp('ScoringForm').getElementId() );" value='#Form'>
			</td>	
		</tr>
		<?php
			$UI=&get_instance();
			$UI->load->model('M_Configuration');
			$ConfigName = $UI->M_Configuration->_getAtempt( $this->EUI_Session->_get_session('ProjectId') );
			if(!is_null($ConfigName) ) : 
		?>
		<tr>
			<td class ="text_caption bottom" nowrap width="10%"> * Score Call Attempt</td>
			<td class="left">
				<?php __(form()->combo('ScoringAttempt','select',$PolicyNumber, NULL, array('disabled'=>true)));?>
					<input type="button" name='chklabel' id="chklabel" class='button assign' 
				style='margin:0px;padding-left:22px;' 
				onclick="Ext.DOM.EventFormCallAttempt( Ext.Cmp('ScoringAttempt').getElementId() );" value='#Form'>
			</td>
		</tr>
		<tr>
			<td class ="text_caption bottom" nowrap width="10%"> * Score Eager To Call</td>
			<td class="left"><?php __(form()->combo('ScoringEager','select',$PolicyNumber, null, array('disabled'=>true)));?>
					<input type="button" name='chklabel' id="chklabel" class='button assign' 
				style='margin:0px;padding-left:22px;' 
				onclick="Ext.DOM.EventFormEagerCall( Ext.Cmp('ScoringEager').getElementId() );" value='#Form'>
			</td>
		</tr>
		<?php endif;?>
		
	</table>
</div>
</div>

</fieldset>

<!-- end : question -->


<fieldset class="corner" style="margin-top:4px;"> 
	<legend class="icon-menulist"> &nbsp;&nbsp; Quality Activity</legend> 
<div style="overflow:auto;margin-top:3px;" class="activity-content box-shadow">
<form name="frmQualityActivity">
<table class="activity-table" cellpadding="2px" border=0 cellspacing="2px">
<?php 
 foreach( $Combo['QualityResultForm'] as $key => $name )
  {
	if( !in_array( $key, array(9) ) ){
		$QualityStatus[$key] = $name;
	}		
 }
?>	
	<tr>
		<td class="text_caption bottom">Quality Status : </td>
		<td><?php __(form()->combo('QualityStatus','select long',$QualityStatus, ( $Customers['CallReasonQue'] ? $Customers['CallReasonQue'] : 10),
			array('change' => 'Ext.DOM.QualityCheckedByStatus(this);')
		)); ?></td>
	</tr>
	<tr>
		<td nowrap class="text_caption bottom">Phone Number : </td>
		<td nowrap id="phone_primary_number"><?php __(form()->combo('PhoneNumber','select long', $Phones,$Callhistory['CallNumber'], null, array('disabled'=>true))); ?></td>
	</tr>	
	<tr>
		<td nowrap class="text_caption bottom">Add Phone Number : </td>
		<td nowrap id="phone_additional_number"><?php __(form()->combo('AddPhoneNumber','select long',$AddPhone,$Callhistory['CallNumber'], null, array('disabled'=>true))); ?></td>
	</tr>	
	
	<tr>
		<td>&nbsp;</td>
		<td>
			<img class="image-calls" src="<?php echo base_url(); ?>/library/gambar/PhoneCall.png" width="35px" height="35px" style="cursor:pointer;" title="Dial..." onclick="Ext.DOM.dialCustomer();">
			<img class="image-hangup" src="<?php echo base_url(); ?>/library/gambar/HangUp.png" width="35px" height="35px" style="cursor:pointer;" title="Hangup..." onclick="Ext.DOM.hangupCustomer();">
		</td>
	</tr>
	<tr>
		<td class="text_caption bottom" valign="top">Call Status : </td>
		<td> <?php __(form()->combo('CallStatus','select long', $CallCategoryId,$Customers['CallReasonCategoryId'],array('change'=>"Ext.DOM.getCallReasultId(this);") ,null)); ?></td>
	</tr>	
	<tr>
		<td class="text_caption bottom">Call Result : </td>
		<td>
			<span id="DivCallResultId"> 
				<?php __(form()->combo('CallResult','select long',$CallResultId, $Customers['CallReasonId'],array('change'=>'getEventSale(this);'),null)); ?>
			</span>	
		</td>	
	</tr>
	<tr>
		<td class="text_caption bottom">&nbsp;<span id="divHtmlInner">Complete </span> : </td>
		<td nowrap><?php __(form()->checkbox('CallComplete',null,1, array("click"=>"( this.checked ? Ext.Cmp('divHtmlInner').setText('Complete') :  
			Ext.Cmp('divHtmlInner').setText('Uncomplete') ); " ) ) );?></td>
	</tr>
	
	<tr>
		<td class="text_caption" valign="top">Quality Notes : </td>
		<td><?php __(form()->textarea('ActivityNotes','textarea',$ResultPoints['ApprovalRemark'], null,array(
			'style' => 'height:150px;width:180px;color:#333BBB;')));?></td>
	</tr>
	<tr>
		<td align="center">&nbsp;</td>
		<td>
			<?php __(form()->button('ButtonbSave','button save','Save', array("click" =>"Ext.DOM.SaveQualityActivity();") ));?>
			<?php __(form()->button('ButtonbCancel','button close','Close', array("click" =>"Ext.DOM.CancelActivity();") ));?>
		</td>
	</tr>
	</table>	
	</form>
	</div>	
</fieldset>	