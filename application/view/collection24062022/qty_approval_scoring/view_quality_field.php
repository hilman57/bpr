<!-- followup data -->

<?php 

if( !defined('FU_POS') ) define('FU_POST', 700);
if( !defined('FU_AGENCY_SUPPORT') ) define('FU_AGENCY_SUPPORT', 800);
if( !defined('FU_BANCA_SUPPORT') ) define('FU_BANCA_SUPPORT', 900);
if( !defined('FU_VISIT_TO_BRANCH') ) define('FU_VISIT_TO_BRANCH', 999);

$UI =& get_instance();
$UI->load->model('M_ProjectWorkForm');
$arrs_form = $UI->M_ProjectWorkForm ->_getFollowUpGroup();

$UserFunc = array("keyup" => "Ext.DOM.CheckedEditPolicy(this);");

?>
<div>
	<form name="frmWorkProject">
	
	<table align='center' width='100%' border=0>
	<?php foreach( $arrs_form as $group_code => $group_name ) :  ?>
		<?php if( $group_code == FU_POST ) :  ?>
			<tr> 
				<td class='text_caption center' width='1%'> <input type='checkbox' title='FU POS' name='FU_FOLLOWUP' id='FU_POST' value='<?php echo $group_code; ?>', disabled='true'></td>	
				<td class='ui-corner-top ui-state-default text_caption bottom left text_caption bottom left'> <?php echo $group_name; ?></td>
			</tr>
			<tr>
				<td colspan=2 style='padding-left:12px;'> 
					<table align='left' width='100%' cellspacing=0 cellpadding=0>
						<tr>
							<td class='text_caption bottom' width='12%' nowrap>Address 1 :</td>
							<td><?php __(form()->input("ADDRESS1_FU_POST", "$GroupCode select long", ( $_arrs_data ? $_arrs_data['FuAddr1'] : $PolicyData['CustomerAddressLine1'] ) ,$UserFunc,array('style'=>'width:99%;' )));?></td>
							<td class='text_caption bottom '>City :</td>
							<td><?php __(form()->input("CITY_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuCity'] : $PolicyData['CustomerCity'] ),$UserFunc,array('style'=>'width:99%;' )));?></td>
							
							
						</tr>
						<tr>
							<td class='text_caption bottom' width='12%' nowrap>Address 2 :</td>
							<td><?php __(form()->input("ADDRESS2_FU_POST", "$GroupCode select long", ( $_arrs_data ? $_arrs_data['FuAddr2'] : $PolicyData['CustomerAddressLine2']),$UserFunc,array('style'=>'width:99%;' )));?></td>
							<td class='text_caption bottom '>ZIP :</td>
							<td><?php __(form()->input("ZIP_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuZip'] : $PolicyData['CustomerZipCode'] ),$UserFunc,array('style'=>'width:99%;' )));?></td>
							
						</tr>
						<tr>
							<td class='text_caption bottom' width='12%' nowrap>Address 3 :</td>
							<td><?php __(form()->input("ADDRESS3_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuAddr3'] : $PolicyData['CustomerAddressLine3']),$UserFunc,array('style'=>'width:99%;' )));?></td>
							<td class='text_caption bottom'>Province :</td>
							<td><?php __(form()->input("PROVINCE_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuProvince'] : $PolicyData['ProvinceId'] ),$UserFunc,array('style'=>'width:99%;' )));?></td>
							
						</tr>
						<tr>
							<td class='text_caption bottom' width='12%' nowrap>Address 4 :</td>
							<td><?php __(form()->input("ADDRESS4_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuAddr4'] : $PolicyData['CustomerAddressLine4']),$UserFunc,array('style'=>'width:99%;' )));?></td>
							<td class='text_caption bottom'>Country :</td>
							<td><?php __(form()->input("COUNTRY_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuCountry'] : 'INDONESIA' ) ,$UserFunc,array('style'=>'width:99%;' )));?></td>
						</tr>
						
						<tr>
							<td class='text_caption bottom'>Home :</td>
							<td><?php __(form()->input("HOME_FU_POST", "$GroupCode select long",( $_arrs_data  ? $_arrs_data['FuHomePhone'] : $PolicyData['CustomerHomePhoneNum']),$UserFunc,array('style'=>'width:99%;' )));?></td>
							<td class='text_caption bottom '>Office :</td>
							<td><?php __(form()->input("OFFICE_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuOfficePhone'] : $PolicyData['CustomerWorkPhoneNum'] ),$UserFunc,array('style'=>'width:99%;' )));?></td>
						</tr>
						
						<tr>
							<td class='text_caption bottom'>Mobile :</td>
							<td><?php __(form()->input("MOBILE_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuMobilePhone'] : $PolicyData['CustomerMobilePhoneNum']),$UserFunc,array('style'=>'width:99%;' )));?></td>
							<td class='text_caption'>Email :</td>
							<td><?php __(form()->input("EMAIL_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuEmail'] : $PolicyData['CustomerEmail'] ), $UserFunc,array('style'=>'width:99%;' )));?></td>
						</tr>
						
						<tr>
							<td class='text_caption' nowrap > Payment Frequency :</td>
							<td><?php __(form()->input("PAYMENTFREQUENCY_FU_POST", "$GroupCode select long",( $_arrs_data ? $_arrs_data['FuPaymentFreq'] : 'MONTHLY'), $UserFunc, array('style'=>'width:99%;' )));?></td>
						</tr>
						
						
				</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 style='padding-left:25px;'> 
					<?php echo form()->textarea("NOTES_FU_POST", 'font-standars textarea',NULL, $UserFunc, array('style'=>'width:99%;height:25px;', 'disabled'=>TRUE ) );?> 
				</td>
			</tr>
		<?php endif; ?>
		
		<?php if( $group_code == FU_AGENCY_SUPPORT ) : ?> 
			<tr> 
				<td class='text_caption center'><input type='checkbox' title='FU Agency Support' name='FU_FOLLOWUP' id='FU_AGENCY_SUPPORT' value='<?php echo $group_code; ?>', disabled='true'></td>
				<td class='ui-corner-top ui-state-default text_caption bottom left'> <?php echo $group_name; ?></td>
			</tr>
			
			<tr>
				<td colspan=2 style='padding-left:25px;'> 
					<?php echo form()->textarea("NOTES_FU_AGENCY_SUPPORT", 'font-standars textarea',NULL, $UserFunc, array('style'=>'width:99%;height:25px;', 'disabled'=>TRUE ) );?> 
				</td>
			</tr>
		<?php endif; ?>
		
		<?php if( $group_code == FU_BANCA_SUPPORT ): ?> 
			<tr> 
				<td class='text_caption center'> <input title='FU Banca Support' type='checkbox' name='FU_FOLLOWUP' id='FU_BANCA_SUPPORT' value='<?php echo $group_code; ?>', disabled='true'></td>
				<td class='ui-corner-top ui-state-default text_caption bottom left'> <?php echo $group_name; ?></td>
			</tr>
		
			<tr>
				<td colspan=2 style='padding-left:25px;'> 
					<?php echo form()->textarea("NOTES_FU_BANCA_SUPPORT", 'font-standars textarea',NULL, $UserFunc, array('style'=>'width:99%;height:25px;', 'disabled'=>TRUE ) );?> 
				</td>
			</tr>
		<?php endif; ?>
		
		<?php if( $group_code == FU_VISIT_TO_BRANCH ) : ?> 
			<tr> 
				<td class='text_caption center'>
					<input type='checkbox' title='Visit To Branch' name='FU_FOLLOWUP' id='FU_VISIT_TO_BRANCH' value='<?php echo $group_code; ?>', disabled='true'></td>
				<td class='ui-corner-top ui-state-default text_caption bottom left'> <?php echo $group_name; ?></td>
			</tr>
			<tr>
				<td colspan=2 style='padding-left:25px;'> 
					<?php echo form()->textarea("NOTES_FU_VISIT_TO_BRANCH", 'font-standars textarea',NULL, $UserFunc, array('style'=>'width:99%;height:25px;', 'disabled'=>TRUE ) );?> 
				</td>
			</tr>
			
			
		<?php endif; ?>
		
	<?php endforeach; ?>
	<tr>
		<td colspan=2 style='padding-left:25px;text-align:right;'> 
			<input type="button" name="btnUpdate" id="btnUpdate" value="Edit" class="button update" onclick="Ext.DOM.UpdateWorkProject();" >
		</td>
	</tr>
	
	</table>
	</form>
	
	
</div>