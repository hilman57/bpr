<?php
/** 
 * @ def : grid of content policy by customer ID 
 * ----------------------------------------------------------------
 
 * @ param  : $ CustomerId 
 * @ refer  : $ Pilcy ID     
 * 
 */
 
?>
<fieldset class="corner" style="margin-top:10px;"> 
<legend class="icon-menulist"> &nbsp;&nbsp;List of Policy</legend>
<div id="innerDivPolicyList">
	<table border=0 align="left" cellspacing=1 width="100%">
		<tr height='24'>
			<th class="ui-corner-top ui-state-default first center" WIDTH="5%" nowrap>&nbsp;No</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="8%" nowrap>&nbsp;CIF Number</td>
			<th class="ui-corner-top ui-state-default first center" WIDTH="18%" nowrap>&nbsp;Policy<br>Owner</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="8%" nowrap>&nbsp;Policy Number</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="12%">&nbsp;Policy Iss Date</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Call Status</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Call Result</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Caller</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Complete</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Last Call</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Premi</td>
		</tr>
		
<?php 
 $total_policy = 0; 
 $totPolicyPremi = 0;
 $num_of_policy = 1;
 if(is_array($RowPolicyData))
  foreach($RowPolicyData as $rows ) :  $color= ($num_of_policy%2!=0?'#FAFFF9':'#FFFFFF'); ?>	
	<tr class='onselect <?php echo ($num_of_policy > 1?'':'selected-rows'); ?>' bgcolor='<?php __($color);?>' style="cursor:pointer;" 
		onclick="Ext.DOM.PolicyDetailById('<?php __($rows['PolicyId']);?>');
			$(this).addClass('selected-rows').siblings().removeClass('selected-rows');">
		<td class="content-first center" nowrap><?php __($num_of_policy);?></td>
		<td class="content-middle" nowrap><?php __($rows['CIFNumber']);?></td>
		<td class="content-middle" nowrap><?php __($rows['CustomerFirstName']);?></td>
		<td class="content-middle" ><?php __($rows['PolicyNumber']);?></td>
		<td class="content-middle"><?php __(date('d/m/Y',strtotime($rows['PolicyIssDate'])));?></td>
		
		<td class="content-middle" nowrap><?php __( ( $rows['CallReasonCategoryId'] ? $CallCategoryId[$rows['CallReasonCategoryId']] : '-'));?></td>
		<td class="content-middle" nowrap><?php __( ( $rows['CallReasonId'] ? $CallResultId[$rows['CallReasonId']] : '-'));?></td>
		<td class="content-middle" nowrap><?php __( ( $rows['full_name'] ? $rows['full_name'] : '-'));?></td>
		<td class="content-middle" ><?php __( ($rows['CallComplete']?'Complete':'-'));?></td>
		<td class="content-middle"><?php __( ( $rows['UpdateDateTs'] ? date('d/m/Y h:i:s',strtotime($rows['UpdateDateTs'])):'-'));?></td>
		<td class="content-lasted right"><?php __(_getCurrency($rows['PolicyPremi']));?></td>
	</tr>
<?php 
 $totPolicyPremi += $rows['PolicyPremi'];
 $total_policy += 1; 
 $num_of_policy++;
endforeach; ?>		
		<tr height='24'>
			<td colspan="9" style='border-top:1px solid red;background-color:#DDDEED;color:black;font-weight:bold;' class="content-middle" >Total Policy
				&nbsp;( <?php __($total_policy);?> ) </td>
			<td style='border-top:1px solid red;background-color:#DDDEED;color:black;font-weight:bold;' class="content-middle">Total &nbsp;</td>
			<td style='border-top:1px solid red;background-color:#DDDEED;color:black;font-weight:bold;' class="content-middle right">&nbsp;<?php __(_getCurrency($totPolicyPremi));?></td>
		</tr>
	</table>
	</div>
</fieldset>