<?php __(javascript()); ?>
<script type="text/javascript">
//Ext.DOM.QualityResult = [<?php echo json_encode($Combo['QualityResult']);?>];

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 	
/* create object default parameter assigning **/

var datas = 
{
	cust_number : '<?php echo _get_exist_session('cust_number');?>', 
	cust_name 	: '<?php echo _get_exist_session('cust_name') ;?>', 
	campaign_id : '<?php echo _get_exist_session('campaign_id');?>', 
	call_result : '<?php echo _get_exist_session('call_result') ;?>', 
	user_id 	: '<?php echo _get_exist_session('user_id') ;?>', 
	start_date  : '<?php echo _get_exist_session('start_date') ;?>',
	end_date    : '<?php echo _get_exist_session('end_date') ;?>', 
	category_id : '<?php echo _get_exist_session('category_id') ;?>',
	fu_followup : '<?php echo _get_exist_session('fu_followup') ;?>',
	order_by 	: '<?php echo _get_exist_session('order_by') ;?>', 
	type	 	: '<?php echo _get_exist_session('type') ;?>'
}

//console.log(datas);
		
Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

	
	
/* assign navigation filter **/
var navigation = 
{
	custnav : Ext.DOM.INDEX +'/QtyScoring/index/',
	custlist : Ext.DOM.INDEX +'/QtyScoring/Content/',
}
		
/* assign show list content **/
		
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/* function searching customers **/

var validation_check =  function(CustomerId)
	{
		if( CustomerId )
			{
				Ext.File = '../class/class.src.qualitycontrol.php'; 
				Ext.Params = {
					action:'validation_check',
					CustomerId : CustomerId
				}	
				
				return Ext.eJson();	
			} 
 } 
/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
Ext.DOM.searchCustomer = function()
{
	Ext.EQuery.construct( navigation ,Ext.Join([
		Ext.Serialize('frmQtyScoring').getElement()
	]).object() )
	Ext.EQuery.postContent();
}
	
Ext.DOM.Checked= function(){
	Ext.Cmp('chk_cust_call').setChecked();
}

/* 
 * @ def : memanggil Jquery plug in 
 * -------------------------------
 * @ param : public 
 */
 
Ext.DOM.ApprovalAll = function()
{	

var CustomerId = Ext.Cmp('chk_cust_call').getChecked();
if( CustomerId.length > 0 )
{
	Ext.Ajax
	({
		url : Ext.DOM.INDEX+"/QtyScoring/ApprovalAll/",
		method : 'POST',
		param : {
			CustomerId : CustomerId 	
		},
		
		ERROR : function(e){
			Ext.Util(e).proc(function(resp){
				if( resp.success ){
					Ext.Msg('Approval ( '+ CustomerId.length +'  ) ').Success();
					Ext.DOM.searchCustomer();
				}
				else{
					Ext.Msg('Approval ( '+ CustomerId.length +'  ) ').Failed();
				}
			});
		}
	}).post();
}
else{
	Ext.Msg('Please select rows !').Info();
}	
}
		
/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
Ext.DOM.resetSeacrh = function() 
{
	Ext.Serialize('frmQtyScoring').Clear();
	Ext.DOM.searchCustomer();
}
  
/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
Ext.DOM.showPolicy = function(CustomerId) {
if( CustomerId!='' ) 
{
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/QtyScoring/QualityDetail/',
		method  : 'GET',
		param 	: {
			CustomerId 	 : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/QtyScoring/index/', 
		}
	});
 }	
}

/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
 Ext.document().ready(function(){
   Ext.query('#toolbars').extToolbars 
	({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle :[['Search'],['Clear'],['Select All'],['Approve'], ],
		extMenu  :[['searchCustomer'],['resetSeacrh'],['Checked'],['ApprovalAll']],
		extIcon  :[['zoom.png'],['cancel.png'],['accept.png'],['tick.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
	
	$('#start_date,#end_date').datepicker({showOn: 'button', buttonImage: Ext.DOM.LIBRARY +'/gambar/calendar.gif', buttonImageOnly: true, dateFormat:'dd-mm-yy',readonly:true});
});

Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
</script>

<!-- start : content -->
<fieldset class="corner" onKeyDown="enterSearch(event)">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
<div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
<form name="frmQtyScoring">

<table cellpadding="3px;" >
	<tr>
		<td class="text_caption"> Policy Number</td>
		<td><?php echo form()->input('cust_number','input_text long',_get_exist_session('cust_number'));?></td>
		<td class="text_caption"> Campaign</td>
		<td><?php echo form()->combo('campaign_id','select long',$Combo['Campaign'],_get_exist_session('campaign_id'));?></td>
		<td class="text_caption"> Product Category</td>
		<td><?php echo form()->combo('category_id','select long',$ProductCat,_get_exist_session('category_id'));?></td>
		<td class="text_caption"> Last Call Date Interval </td>
		<td>
			<?php echo form()->input('start_date','input_text box',_get_exist_session('start_date'));?>
			<?php echo form()->input('end_date','input_text box',_get_exist_session('end_date'));?>
		</td>
	</tr>
	<tr>
		<td class="text_caption"> Owner Name </td>
		<td><?php echo form()->input('cust_name','input_text long',_get_exist_session('cust_name'));?></td>
		<td class="text_caption"> Call Result </td>
		<td><?php echo form()->combo('call_result','select long',$Combo['CallResult'],_get_exist_session('call_result'));?></td>
		<td class="text_caption"> Caller Name </td>
		<td><?php echo form()->combo('user_id','select long', $UserState,_get_exist_session('user_id'));?></td>
		<td class="text_caption"> FU Followup</td>
		<td><?php echo form()->combo('fu_followup','select long', $WorkFollowup,_get_exist_session('fu_followup'));?></td>
	</tr>
		</table>
		
	</form>	
	</div>
 </div>
 <div id="toolbars"></div>
 <div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" ></div>
	<div id="pager"></div>
 </div>
</fieldset>	
		
	
	
	
	