<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<fieldset class="corner" style="margin-top:-3px;"> 
<legend class="icon-menulist"> &nbsp;&nbsp;Application Information</legend> 
<div id="tabs-panels" class="box-shadow" style='border:0px solid #dddddd;'>
	<ul>
		<li><a href="#tabs-4" id="aPolicy">Policy Data</a></li>
		<li><a href="#tabs-5" id="aPayer">Personal Data</a></li>
		<li><a href="#tabs-7" id="aBenefiecery">Call History</a></li>
		<li><a href="#tabs-8" id="aFollowupform">Form Followup</a></li>
		
	</ul>
	
	<!-- this load by php file :: not javascript -->
		<div id="tabs-4" style="background-color:#FFFFFF;overflow:auto;">
			<?php $this->load->view('qty_approval_scoring/view_contact_policydata');?>
		</div>
		<div id="tabs-5" style="background-color:#FFFFFF;overflow:auto;">
			<?php $this->load->view('qty_approval_scoring/view_quality_personaldata');?>
		</div>
		<div id="tabs-7" style="background-color:#FFFFFF;height:175px;overflow:auto;">
		</div>
		
		<div id="tabs-8" style="background-color:#FFFFFF;height:500px;overflow:auto;">
			<?php $this->load->view('qty_approval_scoring/view_quality_followup');?>
		</div>
</div>
</fieldset>
