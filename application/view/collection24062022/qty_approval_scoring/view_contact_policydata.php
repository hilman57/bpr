<?php 
/*
 * @ def 		: view_content_policydata data load by Policy ID 
 * -----------------------------------------
 * 
 * @ params  	: $ Policy ID 
 * @ example    : -
 */

 $this->load->helper('EUI_Field_helper');
 $fld = EUI_Field_value::get_instance();
 ?>

<form name="frmInfoPolicy">
<table width="99%" align="center" cellpadding="2px" cellspacing="4px">
<?php if(is_array($PolicyLabel) ) foreach( $PolicyLabel as $LabelName => $rows ) : ?>
	<tr>
		<?php foreach( $rows as $PrimaryKeys => $values ) : ?>

			<?php if ($values['type']=='hidden'){ ?>
				<td><?php echo form() -> hidden($PrimaryKeys,$values['class'], $fld->FieldValue($PrimaryKeys, $values['value']),NULL,1);?> </td>
			<?php } else if ($values['type']=='label'){  ?>
				<td nowrap class="text_caption bottom"><?php __($values['label']);?>&nbsp;:</td>
				<td class='bottom'><?php __($fld->FieldValue($PrimaryKeys, $values['value'])); ?> </td>
			<?php } else { ?>
				<td nowrap class="text_caption bottom"><?php __($values['label']);?>&nbsp;:</td>
				<td><?php echo form() -> input($PrimaryKeys,$values['class'], $fld->FieldValue($PrimaryKeys, $values['value']),NULL,1);?> </td>
			<?php } ?>
		<?php endforeach; ?>
	</tr>
<?php endforeach; ?>	

</table>	
</form>