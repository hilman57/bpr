<script>

var WRITE_COUNTER = 0;

Ext.DOM.CheckedEditPolicy = function(obj){
	if( obj.value.length > 0  ){
		WRITE_COUNTER++;
	}
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
Ext.DOM.IsReadyFollowupGroupCode = function( PolicyId ) {
	var FU_Code = new Array(); 
	var data = ( Ext.Ajax ({ 
			url : Ext.DOM.INDEX+"/QtyScoring/FollUpGroupByPolicyId/",
			method : 'POST',
			param : {
				PolicyId : PolicyId
			}
		}).json() );
		
	if( typeof( data.code) =='object'){
		for( var i in data.code ){
			FU_Code.push( data.code[i] );
		}
	}	
	
	return FU_Code;
};

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 Ext.DOM.initFunc = {
	isEdit : false,
	isCall : false,
	isHangup : false,
	isRunCall : false,
	isStatus : false,
	Attempt : 0
 }
 
Ext.query(function(){
 Ext.query("#tabs-panels" ).tabs();
 Ext.query("#tabs" ).tabs();
 Ext.query('#toolbars').extToolbars
 ({
		extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle  : [['Product Script'],[]],
		extMenu   : [[],[]],
		extIcon   : [['page_white_acrobat.png'],[]],
		extText   : true,
		extInput  : true,
		extOption  : [{
				render : 1,
				type   : 'combo',
				header : null,
				id     : 'v_result_script', 	
				name   : 'v_result_script',
				triger : 'ShowWindowScript',
				width  : 220,
				store  : [Ext.Ajax({url:Ext.DOM.INDEX+'/SetCampaignScript/getScript/'}).json()]
			}]
	});
});

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
Ext.DOM.PolicyDataByPolicyId = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/PolicyDataByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue(),
			CustomerId : Ext.Cmp('CustomerId').getValue()	
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function( data )
			{
				Ext.Cmp('ADDRESS1_FU_POST').setValue(data.CustomerAddressLine1);
				Ext.Cmp('ADDRESS2_FU_POST').setValue(data.CustomerAddressLine2);
				Ext.Cmp('ADDRESS3_FU_POST').setValue(data.CustomerAddressLine3);
				Ext.Cmp('ADDRESS4_FU_POST').setValue(data.CustomerAddressLine4);
				Ext.Cmp('CITY_FU_POST').setValue(data.CustomerCity);
				Ext.Cmp('ZIP_FU_POST').setValue(data.CustomerZipCode);
				Ext.Cmp('PROVINCE_FU_POST').setValue(data.ProvinceId);
				Ext.Cmp('COUNTRY_FU_POST').setValue(data.CustomerCountry);
				Ext.Cmp('HOME_FU_POST').setValue(data.CustomerHomePhoneNum);
				Ext.Cmp('MOBILE_FU_POST').setValue(data.CustomerMobilePhoneNum);
				Ext.Cmp('OFFICE_FU_POST').setValue(data.CustomerWorkPhoneNum);
				Ext.Cmp('EMAIL_FU_POST').setValue(data.CustomerEmail);
				Ext.Cmp('PAYMENTFREQUENCY_FU_POST').setValue(data.PayMode);
				Ext.Cmp('NOTES_FU_POST').setValue(data.NOTES_FU_POST);
				
			});
		}
		
	}).post();	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
Ext.DOM.QuestionDataByPolicyId = function(){

//QUESTION BY POLICY ID 

	Ext.Ajax ({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/QustionByQualityPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue()
		},
		ERROR : function(e){
			Ext.Util(e).proc(function( data )
			{
				var frm_elment = Ext.Serialize('frmQuestion').getElement();
				for( var field in data )
				{
					for( var fields in frm_elment )
					{
						if( field==fields ) {
							Ext.Cmp(fields).setValue( data[field] )
						}
					}
				}
			});
		}
	}).post();	
	
}
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.PolicyFollowByPolicyId = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/PolicyFollowByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue(),
			CustomerId : Ext.Cmp('CustomerId').getValue()
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function( data ) {
				for( var field in data  ){
					Ext.Cmp(field).setValue(data[field]);
				}
				
			});
		}
		
	}).post();	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.Uncompleted = function() {
	var PolicyId = Ext.Cmp('ProductForm').getValue();
	var CustomerId = Ext.Cmp('CustomerId').getValue();
	var ActivityNotes = Ext.Cmp('ActivityNotes').getValue();
	
	if( CustomerId=='' ){ 
		Ext.Msg('CustomerId is empty').Info(); }
	else if( PolicyId==''){ 
		Ext.Msg('Policy Number is empty').Info(); }
	else if( ActivityNotes==''){
		Ext.Msg('Please giving an uncomplete notes').Info(); }
	else
	{	
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX+'/QtyScoring/SaveUnComplete/',
			method  : 'POST',
			param 	: {
				CustomerId : CustomerId,
				PolicyId : PolicyId,
				ActivityNotes : ActivityNotes
			},
			ERROR : function( e ) {
				Ext.Util(e).proc(function(response){
					if(response.success){
						Ext.Msg("Uncompleted").Success();
					}
					else{
						Ext.Msg("Uncompleted").Failed();
					}
				});
			}
		}).post();
	}	
}
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.UpdateWorkProject = function()
{
  var ActivityQuestion = Ext.Serialize('frmQuestion').Complete();	
  var ActicvityFollowup = Ext.Serialize('frmWorkProject').getElement();
  var QualityActivity = [];
	  
	  QualityActivity['PolicyId'] = Ext.Cmp('PolicyId').getValue();
	  QualityActivity['FollowupGroupCode']  = Ext.DOM.FuFollowUpByCheckedList();	
	  QualityActivity['CustomerId']  = Ext.Cmp('CustomerId').getValue();	
	  QualityActivity['IsReadyGroup']  = Ext.DOM.IsReadyFollowupGroupCode( Ext.Cmp('PolicyId').getValue() );
	 
//////////////// Check Here //////////////////////////	
  var IS_CONDS = Ext.DOM.CheckFollowup();
  console.log(IS_CONDS);
  if( IS_CONDS.callback ==false ){
	 Ext.Msg( IS_CONDS.message ).Info();
	 return IS_CONDS.callback;
  }
//////////////// Check Here //////////////////////////	
	  
	if( ActivityQuestion==false){
		alert('Please Fill up the Questions!!!'); }
	else
	{
		
		if( WRITE_COUNTER == 0 )
		{
			if(  Ext.Msg("No changes to the data on this form.\n\r whether you will keep it ?").Confirm() )
			{
				Ext.Ajax
				({
					url 	: Ext.DOM.INDEX +'/QtyScoring/UpdateQuestions/',
					method  : 'POST',
					param 	: Ext.Join([
								Ext.Serialize('frmQuestion').getElement(),
								QualityActivity, ActicvityFollowup
							]).object(),	
					
					ERROR : function(e){
						Ext.Util(e).proc(function( response ){
							if( response.success ){
								Ext.Msg('Udpate Questions && Followup ').Success();
								Ext.DOM.initFunc.isEdit = true;
								WRITE_COUNTER = 0;
							}else{
								Ext.Msg('Udpate Questions && Followup ').Failed();
							}
						})
					}	
				}).post();
		   }
		}
		else 
		{
			if(  Ext.Msg("Do you want to save this form ?").Confirm() ) {
				Ext.Ajax
				({
					url 	: Ext.DOM.INDEX +'/QtyScoring/UpdateQuestions/',
					method  : 'POST',
					param 	: Ext.Join([
								Ext.Serialize('frmQuestion').getElement(),
								QualityActivity, ActicvityFollowup
							]).object(),	
					
					ERROR : function(e){
						Ext.Util(e).proc(function( response ){
							if( response.success ){
								Ext.Msg('Udpate Questions && Followup ').Success();
								Ext.DOM.initFunc.isEdit = true;
								WRITE_COUNTER = 0;
							}else{
								Ext.Msg('Udpate Questions && Followup ').Failed();
							}
						})
					}	
			}).post();
		  }	
		}	
	}
}
 
Ext.DOM.dialCustomer = function(){
	if( Ext.DOM.initFunc.isRunCall == false ){ // dont call if in run call 
		ExtApplet.setData({   
			Phone : Ext.Cmp("CallingNumber").getValue(), 
			CustomerId  : Ext.Cmp("CustomerId").getValue() 
		}).Call();
		
		Ext.DOM.initFunc.isCall = true;
		window.setTimeout(function(){
			Ext.DOM.initFunc.isRunCall = true;
		},1000);
	}
	else{
	  Ext.Msg('Call is run').Info();	
	}
}

Ext.DOM.hangupCustomer =function(){
	if( Ext.DOM.initFunc.isCall == true ) 
	{	
		Ext.DOM.initFunc.isRunCall = false;
		Ext.DOM.initFunc.isCancel = false;
		Ext.DOM.initFunc.isHangup = true;
		ExtApplet.setHangup();
	}else{
		Ext.Msg('Please call before ').Info();
	}
	return;	
}
 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.EventFormScoring = function(x) {
	var PolicyId = x.value;
	if( PolicyId !='' ) {
		Ext.Window ({
			url 		: Ext.DOM.INDEX+'/ScoringPolicyForm/index/',	
			method 		: 'POST',
			width  		: (Ext.query(window).width()-(Ext.query(window).width()/4)), 
			height 		: Ext.query(window).height(),
			left  		: (Ext.query(window).width()/2),
			scrollbars 	: 1,
			sccrolling	: 1,
			param  		: 
			{
				ViewLayout 	 : 'START_SCORE',	
				PolicyId 	 : PolicyId,
				CustomerId 	 : Ext.Cmp('CustomerId').getValue()
			}
		}).popup();
		
		Ext.Cmp('CallStatus').disabled(true);
		Ext.Cmp('CallResult').disabled(true);
		Ext.Cmp('QualityStatus').disabled(true);
		Ext.Cmp('ButtonbCancel').disabled(true);
		Ext.Cmp('ButtonbSave').disabled(true);
	}
 }
 
/* 
 * @ def : PlayByCallSession
 * ------------------------------------------
 *
 * @ param :  - 
 * @ aksess : -   
 */
 
Ext.DOM.PlayByCallSession = function(SessionId){
	$('#tabs').tabs( "option", "selected", 1);
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/PlayBySessionId/",
		method 	: 'GET',
		param  	: { RecordId : SessionId },
		ERROR 	: function(e){
			Ext.Util(e).proc(function(fn){
				if(fn.success) {
					Ext.Media("tabs-2",{ 
						url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
						width 	: '99%',
						height 	: '70px',
						options : {
							ShowControls 	: 'true',
							ShowStatusBar 	: 'true',
							ShowDisplay 	: 'true',
							autoplay 		: 'true'
						}
					}).WAVPlayer();
						
					Ext.Tpl("tabs-2", fn.data).Compile();
					Ext.Cmp('MediaPlayer').setAttribute('class','textarea');
					Ext.Css('tabs-2').style({'text-align' : 'left',  'padding-left' : "8px",  'padding-top' : "20px" });
					Ext.Css('div-voice-container').style({ "margin-top" : "5px", "width" : "100%", "margin-bottom" : "20px"  });
				}
			})
		}
	}).post();
}

/* 
 * @ def : ShowWindowScript
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.PlayRecording = function(check) {

var chk = Ext.Cmp('recordId').getName();
if( check.checked ) {
  for(var c in chk ) {
	if( chk[c].checked) {
		if( chk[c].value!=check.value )  chk[c].checked = false;
		else 
		{
			$('#tabs').tabs( "option", "selected", 1 );
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/VoicePlay/",
				method 	: 'GET',
				param  	: { RecordId : check.value },
				ERROR 	: function(e){
					Ext.Util(e).proc(function(fn){
					
						if( fn.success ) 
						{
							Ext.Media("tabs-2",{ 
								url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
								width 	: '99%',
								height 	: '70px',
								options : {
									ShowControls 	: 'true',
									ShowStatusBar 	: 'true',
									ShowDisplay 	: 'true',
									autoplay 		: 'true'
								}
							}).WAVPlayer();
							
							Ext.Tpl("tabs-2", fn.data).Compile();
							Ext.Cmp('MediaPlayer').setAttribute('class','textarea');
							Ext.Css('tabs-2').style({'text-align' : 'left',  'padding-left' : "8px",  'padding-top' : "20px" });
							Ext.Css('div-voice-container').style({ "margin-top" : "5px", "width" : "100%", "margin-bottom" : "20px"  });
						}	
					});	
					
				}
			}).post();
		}
	}
}}}
/* 
 * @ def : ShowWindowScript
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.ShowWindowScript = function(ScriptId){
	var WindowScript = new Ext.Window ({
			url    : Ext.DOM.INDEX +'/SetCampaignScript/ShowProductScript/',
			name    : 'WinProduct',
			height  : (Ext.Layout(window).Height()),
			width   : (Ext.Layout(window).Width()),
			left    : (Ext.Layout(window).Width()/2),
			top	    : (Ext.Layout(window).Height()/2),
			param   : {
				ScriptId : Ext.BASE64.encode(ScriptId),
				Time	 : Ext.Date().getDuration()
			}
		}).popup();
		
	if( ScriptId =='' ) {
		window.close();
	}
}
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.updateQaProses = function(){
	return ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX+'/QtyScoring/updateQaProses/',
		method 	: 'POST',
		param	:{
			Customerid : Ext.Cmp('CustomerId').getValue() 
		}
	}).json());
 }
 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CancelActivity =function(){
	Ext.DOM.updateQaProses();
	ControllerId = Ext.Cmp("ControllerId").getValue();
	Ext.EQuery.Ajax ({
		url 	: ControllerId,
		method 	: 'GET',
		param 	: { act : 'back-to-list'
		}
	});
 }
 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.Check = function(CustomerId,PolicyId){
	return ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX+'/ScoringPolicyForm/CheckExisting/',
		method 	: 'POST',
		param	:{
			CustomerId : CustomerId,
			PolicyId : PolicyId
		}
	}).json().result) ;
 }
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.SaveQualityActivity =function(){

var param = [];
	param['CustomerId'] = Ext.Cmp('CustomerId').getValue();	
	param['CallReasonQue'] = Ext.Cmp('QualityStatus').getValue();
	param['CallHistoryNotes'] = Ext.Cmp('ActivityNotes').getValue();
	param['CallComplete'] = Ext.Cmp('CallComplete').getValue();
	param['PolicyId'] = Ext.Cmp('PolicyId').getValue();
	if((Ext.DOM.initFunc.isCall==true && Ext.DOM.initFunc.isEdit==true) || (Ext.DOM.initFunc.isCall==true && Ext.DOM.initFunc.isStatus==true)){
		param['Attempt'] = 1;
	}else{param['Attempt'] = 0;}
	
	
  if( WRITE_COUNTER ==0 ) 
  {	
	if(Ext.Cmp('CallComplete').getValue()==1)
	{
		if( Ext.Cmp('QualityStatus').empty() ){ 
			Ext.Msg('Please select Quality Status').Info();
		}else if( Ext.Cmp('ActivityNotes').empty() ){
			Ext.Msg('Activity Notes is empty').Info(); 
		}else if(Ext.DOM.Check(param['CustomerId'],param['PolicyId'])==0){
			Ext.Msg('Please Scoring First').Info();
		}else{
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/QtyScoring/SaveScoreQuality/',
				method 	: 'POST',
				param	: Ext.Join([param]).object(),
						
				ERROR	: function(e)
				{
					Ext.Util(e).proc(function(save){
						if(save.success) { 
							Ext.Msg("Save Quality Activity").Success();
							Ext.DOM.CallHistory(); 
							Ext.DOM.UpdatePolicyList()
						}
						else{
							Ext.Msg("Save Quality Activity").Failed();
						}
					});
				}	
			}).post();
		}
	}else if(Ext.Cmp('CallComplete').getValue()==0){
		if( Ext.Cmp('ActivityNotes').empty() ){
			Ext.Msg('Activity Notes is empty').Info();
		}else{
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/QtyScoring/SaveScoreQuality/',
				method 	: 'POST',
				param	: Ext.Join([param]).object(),
						
				ERROR	: function(e)
				{
					Ext.Util(e).proc(function(save){
						if(save.success) { 
							Ext.Msg("Save Quality Activity").Success();
							Ext.DOM.CallHistory(); 
							Ext.DOM.UpdatePolicyList()
						}
						else{
							Ext.Msg("Save Quality Activity").Failed();
						}
					});
				}	
			}).post();
		}
	}
 }
 else {
	Ext.Msg('Please Save Edit Form Before Save Activity').Info();
 }	
 
 
}
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.ValidScoring = function(){
	var conds = false;
	
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+'/QtyScoring/SaveScoreQuality/',
		method 	: 'POST',
		param	: Ext.Join([param]).object(),
				
		ERROR	: function(e)
		{
			Ext.Util(e).proc(function(save){
				if(save.success) { 
					Ext.Msg("Save Quality Activity").Success();
					Ext.DOM.CallHistory(); 
					Ext.DOM.UpdatePolicyList()
				}
				else{
					Ext.Msg("Save Quality Activity").Failed();
				}
			});
		}	
	}).post();
	
	return conds;
}
 
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.CallHistory = function(PolicyId){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/QtyScoring/CallHistory/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue(),
			PolicyId : ( PolicyId ? PolicyId : '' )
		} }).load("tabs-7");
} 

 /* 
 * @ def : update policy list if update by QA
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.UpdatePolicyList = function(CustomerId){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/QtyScoring/PolicyList/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue()
		} 
	}).load("innerDivPolicyList");
} 

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.ViewScoring = function(){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/QtyScoring/ViewScoring/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue()
		} }).load("tvabs");
} 

/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
Ext.DOM.SelectPages = function(select){
	Ext.Ajax ({
		url : Ext.DOM.INDEX +'/QtyApprovalInterest/Recording/',
		method : 'GET',
		param : { 
			CustomerId : Ext.Cmp('CustomerId').getValue(), 
			Pages: select.id,
			time : Ext.Date().getDuration()	
		}
	}).load('tabs-1');
}

/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
/* FU_FOLLOWUP **/

Ext.DOM.FuFollowUpByCheckedList = function() {	
	return Ext.Cmp('FU_FOLLOWUP').getChecked();
}

/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
/* Ext.DOM.FuFollowUpByCheckedList */

Ext.DOM.FuFollowUpByCheckedName = function() 
{
	var conds = true;
	var elem = Ext.Cmp('FU_FOLLOWUP').getName();
	var chkname = [];
	for( var i = 0; i<elem.length; i++)
	{	
		if( elem[i].checked) 
		{
			var require = 'NOTES_'+elem[i].id;
			if( Ext.Cmp(require).empty() ) {
				Ext.Msg(elem[i].title+' is empty ').Info();
				Ext.query('#'+require).focus();
				conds = false;
				return false;
			}
		}
    }
	return conds;
}
 
/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
  
Ext.DOM.DisableAddress  = function(options){
	
	Ext.Cmp('ADDRESS1_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS2_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS3_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS4_FU_POST').disabled(options);
	Ext.Cmp('CITY_FU_POST').disabled(options);
	Ext.Cmp('ZIP_FU_POST').disabled(options);
	Ext.Cmp('PROVINCE_FU_POST').disabled(options);
	Ext.Cmp('COUNTRY_FU_POST').disabled(options);
	Ext.Cmp('HOME_FU_POST').disabled(options);
	Ext.Cmp('MOBILE_FU_POST').disabled(options);
	Ext.Cmp('OFFICE_FU_POST').disabled(options);
	Ext.Cmp('EMAIL_FU_POST').disabled(options);
	Ext.Cmp('PAYMENTFREQUENCY_FU_POST').disabled(options); 
	Ext.Cmp('NOTES_FU_POST').disabled(options);
}

/* * 
 * @ def : PolicyDetailById
 * -------------------------------------------------
 * 
 */ 
Ext.DOM.array_sum = function( selector ) { 
    var sum = 0;
    for (var i = 0; i < selector.length; i++) {
		if(selector[i]!='') {
			sum+= parseFloat(selector[i]);
		}
    }
    return sum;
};

/* * 
 * @ def : PolicyDetailById
 * -------------------------------------------------
 * 
 */ 
Ext.DOM.PolicyDetailById = function(PolicyId){
 $('#tabs-panels').tabs( "option", "selected", 0);
		Ext.Ajax({
			url : Ext.DOM.INDEX+'/QtyScoring/PolicyDetailById/',
			method :'GET',
			param : {
				PolicyId : PolicyId
			}	
		}).load('tabs-4');
		
		Ext.DOM.CallHistory(PolicyId);
		Ext.Cmp('ScoringForm').setValue(PolicyId);
		
	Ext.DOM.QuestionDataByPolicyId();	
	Ext.DOM.PolicyDataByPolicyId();
	Ext.DOM.PolicyFollowByPolicyId();
	window.setTimeout(function(){
		Ext.DOM.ValidQuestion(this);
	}, 1000);
			
	
}	

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CheckFollowup = function() {

var IS_CONDS = { callback : true, message : '' }, IS_SUPPORT = [];
	IS_SUPPORT.push($('#CustField_Q4').val());	 
	IS_SUPPORT.push($('#CustField_Q5').val());
	IS_SUPPORT.push($('#CustField_Q6').val());
	 
 if( Ext.DOM.array_sum(IS_SUPPORT)==3 
	&&  $('#CustField_Q7').val()!=2 )
 {
	IS_CONDS = {
		callback : false,
		message : 'Please Select 2 in Q7 '
	}
 }	 
 
 if( Ext.DOM.array_sum(IS_SUPPORT)<3 
	&& $('#CustField_Q7').val()==2)
 {
	IS_CONDS = {
		callback : false,
		message : 'Please Select ( 1 OR 0 ) in Q7 '
	}
 }
 
 return IS_CONDS;
 
}
 
	
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.ValidateQ7 = function( FU_SUPPORT ){
console.log(FU_SUPPORT);

if( Ext.DOM.array_sum(FU_SUPPORT) > 0 )
{
	
	Ext.Cmp('CustField_Q7').disabled(false);
	
	var Q7 = parseInt(Ext.Cmp('CustField_Q7').getValue());
	if( parseInt(Q7)==1) {
		Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
		Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = true;
		Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(false);
			
	}
	else{
		Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
		Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = false;
		Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(true);
	}
		
	if( parseInt(Q7)==0 ){
		if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
			Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = true;
			Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(false);
		}
		
		if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
			Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = true;
			Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(false);
		}
	}
	else
	{
		if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
			Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').setValue('');
		}	
		
		if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
			Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').setValue('');
		}	
	}
 } 
 else {
 
 
	if( Ext.Cmp('CustField_Q4').empty() &&  
	    Ext.Cmp('CustField_Q5').empty() &&
		Ext.Cmp('CustField_Q6').empty() )
	{ 
		Ext.Cmp('CustField_Q7').setValue('');
		Ext.Cmp('CustField_Q7').disabled(false);
	}
	else{
		Ext.Cmp('CustField_Q7').disabled(false);
	}
	
		var Q7 = parseInt(Ext.Cmp('CustField_Q7').getValue());
		
		// -------------------- > 
		
		if( parseInt(Q7)==1) {
			Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
			Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = true;
			Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(false);
			
		}
		else{
			Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
			Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(true);
		}
		
		// ------------------- > 
		if( parseInt(Q7)==0 ){
			if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
				Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = true;
				Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(false);
			}
			
			if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
				Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = true;
				Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(false);
			}
		}
		else
		{
			if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
				Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
				Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = false;
				Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').setValue('');
			}	
			
			if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
				Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
				Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = false;
				Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_BANCA_SUPPORT').setValue('');
			}	
		}
		
	// ------------------------> stop here 
		
   }
}	
 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
Ext.DOM.ValidQuestion = function(object) {
var FU_POST = [], FU_SUPPORT = [];
  
  Ext.Cmp('CustField_Q7').disabled(true);
  
  FU_POST.push($('#CustField_Q1').val());
  FU_POST.push($('#CustField_Q2').val());
  FU_POST.push($('#CustField_Q3').val());
	  
// define suport 

  FU_SUPPORT.push($('#CustField_Q4').val());
  FU_SUPPORT.push($('#CustField_Q5').val());
  FU_SUPPORT.push($('#CustField_Q6').val());
	  
// open form FU POS 	  

if( Ext.DOM.array_sum(FU_POST)==3){
	 Ext.Cmp('FU_POST').disabled(true);	
	 Ext.Cmp('FU_POST').getElementId().checked = false; 
	 Ext.DOM.DisableAddress(true);
}	
else if( Ext.DOM.array_sum(FU_POST) < 3 )
{
	if( Ext.Cmp('CustField_Q1').empty() && Ext.Cmp('CustField_Q2').empty() 
		&& Ext.Cmp('CustField_Q3').empty() ) 
	{
		Ext.Cmp('FU_POST').disabled(true);
		Ext.Cmp('FU_POST').getElementId().checked = false; 
		Ext.DOM.DisableAddress(true);
	} 
	else{
		Ext.Cmp('FU_POST').disabled(true);
		Ext.Cmp('FU_POST').getElementId().checked = true; 
		Ext.DOM.DisableAddress(false);
	}
	 
  }
  else{
	Ext.Cmp('FU_POST').disabled(true);
	Ext.Cmp('FU_POST').getElementId().checked = false; 
	Ext.DOM.DisableAddress(true);
  }  
  
  Ext.DOM.ValidateQ7(FU_SUPPORT);
 
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.getCallReasultId = function(combo){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SrcCustomerList/setCallResult/',
		method  : 'GET',
		param  : {
			CategoryId : combo.value
		}	
	}).load("DivCallResultId");	
}	
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

 // gak di pake memek................
 
Ext.DOM.QualityCheckedByStatus = function( QualityCheckedByStatus )
{
	if( QualityCheckedByStatus.value !='' ) {
		Ext.Cmp('ActivityNotes').setValue('Checked');
	}
	else {
		Ext.Cmp('ActivityNotes').setValue('');
	}
}

Ext.DOM.getEventSale = function(object) 
{
	//Ext.Cmp('call_remarks').setValue(Ext.Cmp(object.id).getText() );
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/SetCallResult/getEventType/',
		method : 'GET',
		param :{
			CallResultId : object.value
		},
		ERROR : function(fn){
			try
			{
				Ext.Util(fn).proc(function( ERR ){
				   if( ERR.success) 
				   {
					if( typeof(ERR.event)=='object') {
						Ext.DOM.initFunc.isStatus = true;
						if( ERR.event.CallReasonEvent==1 ){ 
							Ext.Cmp('CallComplete').getElementId().checked=true;
							Ext.Cmp('divHtmlInner').setText('Complete');
						}
						else if( ERR.event.DefaultCompleteCheck==1)
						{
							if( parseInt(object.value)==1 ) 
							{	
								$('#tabs_list').tabs( "option", "selected", 3);
								Ext.Cmp('CallComplete').getElementId().checked=true;
								Ext.Cmp('divHtmlInner').setText('Complete');
								Ext.DOM.DisableQuestion(false);
								Ext.DOM.DisableAddress(true);
								Ext.DOM.QuestionDataByPolicyId();
								
							}
							else{
								Ext.Cmp('CallComplete').getElementId().checked=true;
								Ext.Cmp('divHtmlInner').setText('Complete');
								Ext.DOM.ResetValueOfQuestion();
								Ext.DOM.DisableQuestion(true);
								Ext.DOM.DisableAddress(true);
							}
						}
						else{
							Ext.Cmp('CallComplete').getElementId().checked=false;
							Ext.Cmp('divHtmlInner').setText('Uncomplete');
							Ext.DOM.DisableQuestion(true);
							Ext.DOM.ResetValueOfQuestion();
							Ext.DOM.DisableAddress(true);
						}
						
						if( ERR.event.CallReasonLater==1){
							Ext.Cmp('date_call_later').disabled(false);
							Ext.Cmp('hour_call_later').disabled(false);
							Ext.Cmp('minute_call_later').disabled(false);
						}
						else{
							Ext.Cmp('date_call_later').setValue('');
							Ext.Cmp('hour_call_later').setValue('00');
							Ext.Cmp('minute_call_later').setValue('00');
							Ext.Cmp('date_call_later').disabled(true);
							Ext.Cmp('hour_call_later').disabled(true);
							Ext.Cmp('minute_call_later').disabled(true);
						}
					 }
				  }
				});
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.document(document).ready(function(){
 $('#CustomerDOB').datepicker({ 
  dateFormat : 'yy-mm-dd', 
  changeYear : true, 
  changeMonth : true 
 });
 
/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
   Ext.DOM.CallHistory('');
   Ext.DOM.SelectPages({'id':0});
   Ext.DOM.PolicyDetailById(Ext.Cmp('PolicyId').getValue());
   Ext.DOM.getEventSale({value:Ext.Cmp('CallResult').getValue()});
});

</script>