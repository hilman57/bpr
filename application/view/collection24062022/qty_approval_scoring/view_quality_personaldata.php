<?php 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 * 
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 * @ example    : get by fields ID / Campaign ID
 */

$conn =& get_instance();

if( $conn ) 
{
	$conn->db->select('Field_Id');
	$conn->db->from('t_gn_field_campaign');
	$conn->db->where('CampaignId', $Customers['CampaignId']);
	$conn->db->where('Field_Active',1);
	$conn->db->order_by('Field_Id','ASC');

	foreach( $conn->db->get() -> result_assoc() as $rows )
	{ 
		if( $Flexible =& _fldFlexibleLayout($rows['Field_Id']))
		{
			$Flexible -> _setTables('t_gn_debitur'); // rcsorce data 
			$Flexible -> _setCustomerId(array('CustomerId' => $Customers['CustomerId'])); // set conditional array();
			$Flexible -> _Compile();
		}
	}
}	

// END OFF
