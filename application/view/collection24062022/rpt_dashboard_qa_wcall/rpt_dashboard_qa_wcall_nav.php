<?php 
if(!_have_get_session('UserId')) exit('Expired Session ID ');

__(javascript(array(array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()))));
$this -> load -> view('rpt_dashboard_qa_wcall/rpt_dashboard_qa_wcall_js');
?>

<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Upload Report</span></legend>
	<div id="panel-main-content">
		<form name="frmReport">
			<fieldset class="corner" style="margin-bottom:10px;">
				<legend class="icon-menulist">&nbsp;&nbsp;Group Filter </legend>
				<div align="left">
					<table cellpadding='4' cellspacing=4>
						<tr>
							<td class="text_caption" style="vertical-align:top;color:red;">* Report Type</td>
							<td style="vertical-align:top;"><?php __(form()->combo('report_type','select long',$ReportType,null));?></td>
							<td class="text_caption" style="vertical-align:top;"><span id="label_param1">Param 1</span></td>
							<td style="vertical-align:top;"><span id="content_param1"><?php echo form()->combo('filter_param1','select long',array(),null);?></span></td>
							<td class="text_caption" style="vertical-align:top;"><span id="label_param2">Param 2</span></td>
							<td style="vertical-align:top;"><span id="content_param2"><?php echo form()->combo('filter_param2','select long',array(),null);?></span></td>
							<!-- td class="text_caption" style="vertical-align:top;"><span id="label_param3">Param 3</span></td>
							<td style="vertical-align:top;"><span id="content_param3"><?#php echo form()->combo('filter_param3','select long',array(),null);?></span></t d -->
						</tr>
						<tr>
							<td class="text_caption bottom">Interval</td>
							<td class='bottom'>
								<?php __(form()->input('start_date','input_text box date'));?>&nbsp;-&nbsp;
								<?php __(form()->input('end_date','input_text box date'));?>
							</td>
							<td class="text_caption"> &nbsp;</td>
							<td class='bottom'>
								<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
								<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
							</td>
						</tr>
					</table>
				</div>
			</fieldset>
		</form>
	</div>
	<div id="panel-report" class="news-div-content justify-text textarea" style="position:relative;width:99%;"></div>

</fieldset>