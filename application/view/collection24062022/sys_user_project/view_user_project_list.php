<?php
?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" width="5%">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('WorkId').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle" width="5%">&nbsp;No.</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middleleft">&nbsp;<span class="header_order" id ="c.ProjectName" onclick="Ext.EQuery.orderBy(this.id);">User Work Project</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="b.id"  onclick="Ext.EQuery.orderBy(this.id);">User Code</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="b.full_name"  onclick="Ext.EQuery.orderBy(this.id);">User Name</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="a.CreateTs"  onclick="Ext.EQuery.orderBy(this.id);">Create Date</span></th>        
        <th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="a.UserCreateId"   onclick="Ext.EQuery.orderBy(this.id);">Create By User</span> </th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left">&nbsp;<span class="header_order" id ="a.Status"   onclick="Ext.EQuery.orderBy(this.id);">Status</span> </th>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows )
 { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
		<tr class="onselect" bgcolor="<?php echo $color;?>">
			<td class="content-first"><input type="checkbox" value="<?php echo $rows['WorkId']; ?>" name="WorkId" id="WorkId"></td>
			<td class="content-middle center"><?php echo $no; ?></td>
			<td class="content-middle left" ><?php echo (isset($rows['ProjectName'])?$rows['ProjectName']:null); ?></td>
			<td class="content-middle" style="color:blue;"><?php echo (isset($rows['id'])?$rows['id']:null); ?></td>
			<td class="content-middle" style="color:blue;"><?php echo (isset($rows['full_name'])?$rows['full_name']:null); ?></td>
			<td class="content-middle"><?php echo (isset($rows['CreateTs'])?$rows['CreateTs']:null); ?></td>
			<td class="content-middle"><?php echo (isset($rows['CreateByUser'])?$rows['CreateByUser']:'-'); ?></td>
			<td class="content-lasted"><?php echo (isset($rows['Status'])?'Active':'Not Active'); ?></td>
		</tr>		
</tbody>
<?php
	$no++;
};
?>
</table>