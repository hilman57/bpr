<?php echo javascript(); ?>
<script type="text/javascript">
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 

  
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.datas = 
{ 
	keywords : '<?php echo _get_post('keywords');?>',
	order_by : '<?php echo _get_post('order_by'); ?>', 
	type	 : '<?php echo _get_post('type'); ?>'
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/

Ext.EQuery.TotalPage   = <?php echo $page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo $page -> _get_total_record(); ?>;


/**
 ** javscript prototype system
 ** version v.0.1
 **/
$(function(){
	$('#toolbars').extToolbars({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Edit'],['Add'],['Delete'],[],['Search']],
		extMenu   : [['EditUser'],['AddUser'],['DeleteWorkUserProject'],[],['SearchSkillWord']],
		extIcon   : [['page_edit.png'],['add.png'],['cross.png'],[],['zoom.png']],
		extText   : true,
		extInput  : true,
		extOption : [{
				render 	: 3,
				type 	: 'text',
				name 	: 'keywords',
				id		: 'keywords',
				value   : datas.keywords
			}]
	});
});

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.EditUser = function(){
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/SysUserProject/EditUserWorkProject/',
		method :'GET',
		param:{
			WorkId  : Ext.Cmp('WorkId').getValue()
		}
	}).load('tpl_header');
	
}

Ext.DOM.WorkUserByLevel = function( ProfileId ) {
	Ext.Ajax({
		url  : Ext.DOM.INDEX +'/SysUserProject/WorkUserByLevel/',
		method : 'GET',
		param : {
			ProfileId : ProfileId
		}
	}).load('divUserId');
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.AddUser = function()
{
	Ext.Ajax
	({
		url  : Ext.DOM.INDEX +'/SysUserProject/AddUserWorkProject/',
		method : 'GET',
		param : {
			duration : Ext.Date().getDuration()
		}
	}).load('tpl_header');		
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.SaveUserWorkProject = function() 
{
	var conds = Ext.Serialize('frmUserWorkProject').Complete();
	if(conds)
	{
		Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX  +'/SysUserProject/SaveUserWorkProject/',
			method 	: 'POST',
			param 	: Ext.Join([ 
						Ext.Serialize('frmUserWorkProject').getElement() 
					 ]).object(),
					
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg("Save User Work Project").Success();
						Ext.EQuery.postContent();
					}		
					else{
						Ext.Msg("Save User Work Project").Failed();
					}
				})
			}
		}).post();
	}	
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 	
Ext.DOM.UpdateUserWorkProject = function()
{
var conds = Ext.Serialize('frmEditUserWorkProject').Complete(['PrivilegeId']);
	if(conds)
	{
		Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX  +'/SysUserProject/UpdateUserWorkProject/',
			method 	: 'POST',
			param 	: Ext.Join([ 
						Ext.Serialize('frmEditUserWorkProject').getElement() 
					 ]).object(),
					
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg("Update User Work Project").Success();
						Ext.EQuery.postContent();
					}		
					else{
						Ext.Msg("Update User Work Project").Failed();
					}
				})
			}
		}).post();
	}
}

	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 	
Ext.DOM.DeleteWorkUserProject = function()
{
  if( Ext.Msg("Do you want delete this rows") )
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SysUserProject/DeleteWorkUserProject/',
			method 	: 'POST',
			param 	: { WorkId : Ext.Cmp('WorkId').getValue() },
			ERROR : function(fn){
				try
				{
					var ERR = JSON.parse(fn.target.responseText);
					if( ERR.success ){
						Ext.Msg("Delete User Work Project").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Delete User Work Project").Failed();
					}
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			}
		}).post();
	}	
}	

	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 var navigation = {
	custnav  : Ext.DOM.INDEX +'/SysUserProject/index/', 
	custlist : Ext.DOM.INDEX +'/SysUserProject/Content/', 
};

Ext.EQuery.construct(navigation, datas );
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.ClearSkill = function(){
	Ext.Cmp('tpl_header').setText("");
}


/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.Cmp("keywords").listener({
	onKeyup:function( e ){
		if( e.keyCode==13 ){
			Ext.EQuery.construct(navigation,{
				keywords : e.target.value
			});
			Ext.EQuery.postContent();
		}
	}
});

	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.AddExtension =function(){
	Ext.Ajax({
		url	: Ext.DOM.SYSTEM+'/controller/class.extension.system.php',
		method :'POST',
		param :{
			action : 'add_extension_tpl' 
		}
	}).load('tpl_header');
}
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.SearchSkillWord = function()
{
	Ext.EQuery.construct( navigation, {
		keywords : Ext.Cmp('keywords').getValue()
	});
	Ext.EQuery.postContent();
}
	
</script>
	<fieldset class="corner" style="background-color:white;">
		<legend class="icon-userapplication" >&nbsp;&nbsp;<span id="legend_title"></span></legend>
			<div id="toolbars" class="toolbars"></div>
			<div id="tpl_header"></div>
			<div class="content_table"></div>
			<div id="pager"></div>
			<div id="UserTpl"></div>
	</fieldset>	
	