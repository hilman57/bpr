<style>
	table.grid{}
	td.header { background-color:#2182bf;font-family:Arial;font-weight:bold;color:#f1f5f8;font-size:12px;padding:5px;} 
	td.sub { background-color:#DEB887;font-family:Arial;font-weight:bold;color:#000000;font-size:12px;padding:5px;} 
	td.content { padding:2px;height:24px;font-family:Arial;font-weight:normal;color:#456376;font-size:12px;background-color:#f9fbfd;} 
	td.first {border-left:1px solid #dddddd;border-top:1px solid #dddddd;border-bottom:0px solid #dddddd;}
	td.middle {border-left:1px solid #dddddd;border-bottom:0px solid #dddddd;border-top:1px solid #dddddd;}
	td.lasted {border-left:1px solid #dddddd; border-bottom:0px solid #dddddd; border-right:1px solid #dddddd; border-top:1px solid #dddddd;}
	td.agent{font-family:Arial;font-weight:normal;font-size:12px;padding-top:5px;padding-bottom:5px;border-left:0px solid #dddddd; 
			border-bottom:0px solid #dddddd; border-right:0px solid #dddddd; border-top:0px solid #dddddd;
			background-color:#fcfeff;padding-left:2px;color:#06456d;font-weight:bold;}
	h1.agent{font-style:inherit; font-family:Trebuchet MS;color:blue;font-size:14px;color:#2182bf;}
	
	td.total{
				padding:2px;font-family:Arial;font-weight:normal;font-size:12px;padding-top:5px;padding-bottom:5px;border-left:0px solid #dddddd; 
			border-bottom:1px solid #dddddd; border-top:1px solid #dddddd;  
			border-right:1px solid #dddddd; border-top:1px solid #dddddd;
			background-color:#2182bf;padding-left:2px;color:#f1f5f8;font-weight:bold;}
	span.top{color:#306407;font-family:Trebuchet MS;font-size:16px;line-height:18px;}
	span.middle{color:#306407;font-family:Trebuchet MS;font-size:14px;line-height:18px;}
	span.bottom{color:#306407;font-family:Trebuchet MS;font-size:12px;line-height:18px;}
	td.subtotal{ font-family:Arial;font-weight:bold;color:#3c8a08;height:30px;background-color:#FFFCCC;}
	td.tanggal{ font-weight:bold;color:#FF4321;height:22px;background-color:#FFFFFF;height:30px;}
	h3{color:#306407;font-family:Trebuchet MS;font-size:14px;}
	h4{color:#FF4321;font-family:Trebuchet MS;font-size:14px;}
</style>

<table class="grid" cellpadding="0" cellspacing="0" width="90%">
	<tr>
		<td rowspan=5 class="header first" align="center">Campaign ID</td>
		<td rowspan=5 class="header middle" align="center" align="center">Campaign Name</td>
		<td rowspan=5 class="header middle" align="center" align="center">Batch ID</td>
		<td rowspan=5 class="header middle" align="center" align="center">Batch Name</td>
		<td rowspan=5 class="header middle" align="center" align="center">SPV Code</td>
		<td rowspan=5 class="header middle" align="center" align="center">SPV Name</td>
		<td rowspan=5 class="header middle" align="center" align="center">TSR Code</td>
		<td rowspan=5 class="header middle" align="center" align="center">TSR Name</td>
		<td rowspan=5 class="header middle" align="center" align="center">Upload Data</td>
		<td colspan=3 class="header middle" align="center" align="center">Data New Today</td>
		<td colspan=2 class="header middle" align="center">Data Old</td>
		<td rowspan=5 class="header middle" align="center">Total Utilized</td>
		<td rowspan=5 class="header middle" align="center">Call Attempt</td>
		<td rowspan=5 class="header middle" align="center">AVG Call</td>
		<td colspan=5 class="header middle" align="center">Connect</td>
		<td rowspan=5 class="header middle" align="center">Connect Rate</td>
		<td colspan=7 class="header middle" align="center">Contact</td>
		<td rowspan=5 class="header middle" align="center">Contact Rate</td>
		<td colspan=2 class="header middle" align="center">Follow UP</td>
		<td colspan=11 class="header middle" align="center">Interest</td>
		<td rowspan=5 class="header middle" align="center">Respon Rate</td>
		<td rowspan=5 class="header middle" align="center">Confersion Rate</td>
		<td colspan=7 class="header middle" align="center">Not Qualified</td>
		<td colspan=2 class="header middle" align="center">New Database</td>
		<td colspan=2 class="header middle" align="center">Old Database</td>
		<td colspan=2 class="header middle" align="center">Grand Database</td>
		<td colspan=23 class="header middle" align="center">Profile Customer</td>
		<td colspan=8 class="header middle" align="center">QA Monitoring</td>
		<td colspan=2 class="header middle" align="center">Cancel On Grand Spot</td>
		<td rowspan=5 class="header middle" align="center">User Insert</td>
		<td rowspan=5 class="header middle" align="center">User Insert</td>
		<td rowspan=5 class="header lasted" align="center">Exclude</td>
	</tr>
	<tr>
		<td rowspan=4 class="header middle" align="center">Today</td>
		<td rowspan=4 class="header middle" align="center">Utilized</td>
		<td rowspan=4 class="header middle" align="center">Remaining</td>
		<td rowspan=4 class="header middle" align="center">Utilized</td>
		<td rowspan=4 class="header middle" align="center">Remaining</td>
		<td rowspan=4 class="header middle" align="center">Y</td>
		<td colspan=4 class="header middle" align="center">N</td>
		<td rowspan=4 class="header middle" align="center">Y</td>
		<td colspan=6 class="header middle" align="center">N</td>
		<td rowspan=4 class="header middle" align="center">Thinking (301)</td>
		<td rowspan=4 class="header middle" align="center">Call Back Later (302)</td>
		<td colspan=3 class="header middle" align="center">Y</td>
		<td colspan=8 class="header middle" align="center">N</td>
		<td rowspan=4 class="header middle" align="center">Reject BMI (501)</td>
		<td rowspan=4 class="header middle" align="center">Med. Questions (502)</td>
		<td rowspan=4 class="header middle" align="center">Overage / Underage (503)</td>
		<td rowspan=4 class="header middle" align="center">Unqualified Health Questions (504)</td>
		<td rowspan=4 class="header middle" align="center">Others (505)</td>
		<td rowspan=4 class="header middle" align="center">Out Of Courier Area (506)</td>
		<td rowspan=4 class="header middle" align="center">Request Case Payment (507)</td>
		<td rowspan=4 class="header middle" align="center">Cases</td>
		<td rowspan=4 class="header middle" align="center">Anp</td>
		<td rowspan=4 class="header middle" align="center">Cases</td>
		<td rowspan=4 class="header middle" align="center">Anp</td>
		<td rowspan=4 class="header middle" align="center">Cases</td>
		<td rowspan=4 class="header middle" align="center">Anp</td>
		<td colspan=2 class="header middle" align="center">Coverage</td>
		<td colspan=9 class="header middle" align="center">Range</td>
		<td colspan=2 class="header middle" align="center">Gender</td>
		<td colspan=6 class="header middle" align="center">Payment Media</td>
		<td colspan=4 class="header middle" align="center">Pay Mode</td>
		<td colspan=4 class="header middle" align="center">Verified</td>
		<td colspan=2 class="header middle" align="center">Follow up</td>
		<td colspan=2 class="header middle" align="center">Reject</td>
		<td rowspan=4 class="header middle" align="center">Cases</td>
		<td rowspan=4 class="header middle" align="center">Tarp</td>
	</tr>
	<tr>
		<td rowspan=3 class="header middle" align="center">Invalid No (101)</td>
		<td rowspan=3 class="header middle" align="center">Busy Line (102)</td>
		<td rowspan=3 class="header middle" align="center">Fax No (103)</td>
		<td rowspan=3 class="header middle" align="center">No Answer (104)</td>
		<td rowspan=3 class="header middle" align="center">Wrong No (201)</td>
		<td rowspan=3 class="header middle" align="center">Already Moved (202)</td>
		<td rowspan=3 class="header middle" align="center">Resign (203)</td>
		<td rowspan=3 class="header middle" align="center">Mailbox(204)</td>
		<td rowspan=3 class="header middle" align="center">Meeting (205)</td>
		<td rowspan=3 class="header middle" align="center">Can`t Be Reach (299)</td>
		<td rowspan=3 class="header middle" align="center">Trust Bank (901)</td>
		<td rowspan=3 class="header middle" align="center">Trust Insurance (902)</td>
		<td rowspan=3 class="header middle" align="center">Cheap (903)</td>
		<td rowspan=3 class="header middle" align="center">Reject In Front (401)</td>
		<td rowspan=3 class="header middle" align="center">Spouse Doesnt Agree (402)</td>
		<td rowspan=3 class="header middle" align="center">Not Buy Via Phone (403)</td>
		<td rowspan=3 class="header middle" align="center">Already Insured (404)</td>
		<td rowspan=3 class="header middle" align="center">No Money (405)</td>
		<td rowspan=3 class="header middle" align="center">Need High Benefit (406)</td>
		<td rowspan=3 class="header middle" align="center">Expensive (407)</td>
		<td rowspan=3 class="header middle" align="center">Reluctant To Give CC No (408)</td>
		<td rowspan=3 class="header middle" align="center">Buy Self</td>
		<td rowspan=3 class="header middle" align="center">Buy Others</td>
		<td rowspan=3 class="header middle" align="center">0-10</td>
		<td rowspan=3 class="header middle" align="center">11-20</td>
		<td rowspan=3 class="header middle" align="center">21-30</td>
		<td rowspan=3 class="header middle" align="center">31-40</td>
		<td rowspan=3 class="header middle" align="center">41-49</td>
		<td rowspan=3 class="header middle" align="center">50-55</td>
		<td rowspan=3 class="header middle" align="center">56-60</td>
		<td rowspan=3 class="header middle" align="center">61-65</td>
		<td rowspan=3 class="header middle" align="center">65-70</td>
		<td rowspan=3 class="header middle" align="center">M</td>
		<td rowspan=3 class="header middle" align="center">F</td>
		<td rowspan=3 class="header middle" align="center">Credit Card</td>
		<td rowspan=3 class="header middle" align="center">Direct Debit</td>
		<td rowspan=3 class="header middle" align="center">Transfer</td>
		<td rowspan=3 class="header middle" align="center">ANP - Credit Card</td>
		<td rowspan=3 class="header middle" align="center">ANP - Direct Debit</td>
		<td rowspan=3 class="header middle" align="center">ANP - Transfer</td>
		<td rowspan=3 class="header middle" align="center">Monthly</td>
		<td rowspan=3 class="header middle" align="center">Quarter</td>
		<td rowspan=3 class="header middle" align="center">Semi Annual</td>
		<td rowspan=3 class="header middle" align="center">Annual</td>
		<td rowspan=3 class="header middle" align="center">Monitored</td>
		<td rowspan=3 class="header middle" align="center">%</td>
		<td rowspan=3 class="header middle" align="center">Verified</td>
		<td rowspan=3 class="header middle" align="center">%</td>
		<td rowspan=3 class="header middle" align="center">Cases</td>
		<td rowspan=3 class="header middle" align="center">Tarp</td>
		<td rowspan=3 class="header middle" align="center">Cases</td>
		<td rowspan=3 class="header middle" align="center">Tarp</td>
	</tr>
	<tr height=20 style='height:15.0pt'>
	</tr>
	<tr height=20 style='height:15.0pt'>
	</tr>
	<tr>
		<td class="content first" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="right">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content middle" align="left">&nbsp;0</td>
		<td class="content lasted" align="left">&nbsp;0</td>
	</tr>
	<tr>
		<td colspan="8" class="total middle" align="center">Total</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total middle" align="right">&nbsp;0</td>
		<td class="total lasted" align="right">&nbsp;0</td>
	</tr>
</table>
