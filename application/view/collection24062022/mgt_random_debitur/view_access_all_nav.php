<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

var time_auto = 
{
	"1" : 	[	'access_start_hour',
				'access_start_min',
				'access_start_sec',
				'access_end_hour',
				'access_end_min',
				'access_end_sec'
			],
	"0" : 	[
				'access_end_hour',
				'access_end_min',
				'access_end_sec'
			]
};
// console.log(time_auto);
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( "Filter " + Ext.System.view_file_name());
 })(); 

// @ pack : upload data listener 

var AccessTypeLink =  ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModAccessAll/linkAccessType',
		method 	: 'POST',
		param 	: {}	
	}).json());
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	ModAccess_account_status : "<?php echo _get_exist_session('ModAccess_account_status');?>",
	ModAccess_AssignType : "<?php echo _get_exist_session('ModAccess_AssignType');?>",
	ModAccess_Exist_Agent 		: "<?php echo _get_exist_session('ModAccess_Exist_Agent');?>",
	nav_Assign_To 			: "<?php echo _get_exist_session('nav_Assign_To');?>",
	ModAccess_cust_id 		: "<?php echo _get_exist_session('ModAccess_cust_id');?>",
	ModAccess_cust_name 		: "<?php echo _get_exist_session('ModAccess_cust_name');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav	 : Ext.DOM.INDEX+'/ModAccessAll/index/',
	custlist : Ext.DOM.INDEX+'/ModAccessAll/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('ModAccessAll').getElement();
	// console.log(Ext.Join([param]).object());
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('ModAccessAll').Clear();
	Ext.DOM.searchCustomer();
}
		
// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!='') 
 {	
	// alert(CustomerId);
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.navigation.custnav
		}
  });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}



// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Upload Data'],['Distribute'],['Reset By Check'],['Reset By Filter']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['UploadData'],['ManageDis'],['Reset'],['ResetAllData']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['database_go.png'],['database_add.png'],['user_delete.png'],['user_delete.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}

Ext.DOM.Upload = function(){
	var ARGV_VARS =[];
	/*
	 ARGV_VARS['DebiturId'] = Ext.Cmp('CustomerId').getValue();
   ARGV_VARS['CallNumber'] = Ext.DOM.initFunc.isCallNumber;
   ARGV_VARS['CallSessionId'] = CallSessionId;
   
   Ext.Ajax
	({
		url    : Ext.DOM.INDEX +"/ModSaveActivity/SaveActivity",
		method : 'POST',
		param  : Ext.Join([ 
					Ext.Serialize('frmContactActivity').getElement(), 
					ARGV_VARS 
				]).object(),
	*/
	var chooce = time_auto[Ext.Cmp('autostart').getValue()];
	for( var i in chooce)
	{
		ARGV_VARS[chooce[i]] = Ext.Cmp(chooce[i]).getValue();
	}
	// console.log(ARGV_VARS);
	// console.log(Ext.Cmp('upload_template').getValue());
	ARGV_VARS['TemplateId'] = Ext.Cmp('upload_template').getValue();
	// ARGV_VARS['CampaignId'] = Ext.Cmp('upload_campaign').getValue();
	// ARGV_VARS['AutoStart'] = Ext.Cmp('autostart').getValue();
	// console.log(Ext.Join([
					// ARGV_VARS 
				// ]).object());
	Ext.Cmp("loading-image").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtBucket/UploadBucket/',
		method 	: 'POST',
		param	: Ext.Join([
					Ext.Serialize('form_upload_accessall').getElement(), 
					ARGV_VARS 
				]).object(),
		complete : function(fn){
			var ERR = eval(fn.target.DONE);
			try {	
				if( ERR )
				{
					var CALLBACK = JSON.parse(fn.target.responseText);
					if( (typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null) ){
						var msg = "\n";
						for(var i in  CALLBACK.mesages ){
							msg += "Failed : "+ CALLBACK.mesages[i].N +",\nSuccess : "+CALLBACK.mesages[i].Y +"\n Blacklist : "+ CALLBACK.mesages[i].B +",\nDuplicate : "+  CALLBACK.mesages[i].D  +"\nDuplicate(s) : "+CALLBACK.mesages[i].X+"\n";
						}	
						Ext.Msg(msg).Error();
					}
					else{
						Ext.Msg(CALLBACK.mesages).Info();
					}	
					
					Ext.Cmp("loading-image").setText('');
					Ext.EQuery.construct(navigation,'')
					Ext.EQuery.postContent();
				}	
			}
			catch(e){ alert(e); }
		}
	}).upload()
 }
 
 Ext.DOM.ClosePanel = function() {
	Ext.Cmp('span_top_nav').setText("");
}

function valid_assgin_all()
{
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	var AssignTo = Ext.Cmp('Assign_To').getValue();
	if(AssignType !='')
	{
		if(AccessTypeLink[AssignType]=='AccessAllDeskol')
		{
			return true;
		}
		else
		{
			if (AssignTo != '')
			{
				return true;
			}
			else
			{
				alert('Please Select Assign To');
				return false;
			}
		}
		
	}
	else
	{
		alert('Please Select Assign Type');
		return false;
	}
}

function valid_assgin()
{
	var bucket_trx_id = Ext.Cmp('bucket_trx_id').getValue();
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	var AssignTo = Ext.Cmp('Assign_To').getValue();
	
	if(bucket_trx_id !='')
	{
		if(AssignType !='')
		{
			if(AccessTypeLink[AssignType]=='AccessAllDeskol')
			{
				return true;
			}
			else
			{
				if (AssignTo != '')
				{
					return true;
				}
				else
				{
					alert('Please Select Assign To');
					return false;
				}
			}
			
		}
		else
		{
			alert('Please Select Assign Type');
			return false;
		}
	}
	else
	{
		alert('Please Select Customer');
		return false;
	}
	
}

Ext.DOM.ResetAllData = function(){
	var modtype = Ext.Cmp('ModAccess_AssignType').getValue();
	var assignto = Ext.Cmp('nav_Assign_To').getValue();
	var exitsagent = Ext.Cmp('ModAccess_Exist_Agent').getValue();
	
	if(confirm('Are You Sure Reset All ??')){
		Ext.Ajax({
			url : Ext.DOM.INDEX + '/ModAccessAll/ResetAccessByCheckAll',
			method : 'POST',
			param : {
				modtype : modtype,
				assignto : assignto
				
			},
			ERROR	: function(e) {
						var ERROR = JSON.parse(e.target.responseText);
						if( ERROR.Message ) {
							alert(' Success Reset Access All Data');
							Ext.EQuery.construct(navigation,'')
							Ext.EQuery.postContent();
						}
						else
						{
							alert(' Fail Reset Access All Data');
						}
			}
		}).post();
	}
}

Ext.DOM.Reset = function()
{
	var bucket_trx_id = Ext.Cmp('bucket_trx_id').getValue();
	if(bucket_trx_id !='')
	{
		if(confirm('Are You Sure Reset This Customer ?'))
		{
			Ext.Ajax({
					url 	: Ext.DOM.INDEX+'/ModAccessAll/ResetAccessByCheck',
					method 	: 'POST',
					param 	: { bucket_trx_id : bucket_trx_id
							},
					ERROR	: function(e) {
						var ERROR = JSON.parse(e.target.responseText);
						if( ERROR.Message ) {
							alert(' Success('+ERROR.Success+') Fail('+ERROR.Fail+') Reset Access All Data');
							Ext.EQuery.construct(navigation,'')
							Ext.EQuery.postContent();
						}
					}	
			}).post();
		}
	}
	else
	{
		alert('Please Select Customer');
	}
}

// reset all access
Ext.DOM.ResetAll = function()
{
	
	if(confirm('Are You Sure Reset All?'))
	{
		// alert(customerid);
		Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/ModAccessAll/ResetAccessAll',
				method 	: 'POST',
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert('Success Reset All, Access All Data');
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
		}).post();
	}
};


// assign all access data
Ext.DOM.AssignAll = function()
{
	var AssignTo = Ext.Cmp('Assign_To').getValue();
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	var conf = 'Are You Sure Assign All Data?';
	if(valid_assgin_all())
	{
		if(confirm(conf))
		{
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/ModAccessAll/AssignAccessAll',
				method 	: 'POST',
				param 	: { AssignType : AssignType,
							AssignTo : AssignTo
						},
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert(' Success('+ERROR.Success+') Fail('+ERROR.Fail+') Distribute Access All Data');
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
			}).post();
		}
	}
	
	
};

Ext.DOM.Assign = function()
{
	
	if(valid_assgin())
	{
		var bucket_trx_id = Ext.Cmp('bucket_trx_id').getValue();
		var AssignType = Ext.Cmp('Assign_Type').getValue();
		var AssignTo = Ext.Cmp('Assign_To').getValue();
		// alert(bucket_trx_id);
		Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/ModAccessAll/AssignAccessCheck',
				method 	: 'POST',
				param 	: { bucket_trx_id : bucket_trx_id,
							AssignType : AssignType,
							AssignTo : AssignTo
						},
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert(' Success('+ERROR.Success+') Fail('+ERROR.Fail+') Distribute Access All Data');
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
		}).post();
	}
};
	// @ pack : upload manage distribute access all 

Ext.DOM.ManageDis = function() {
	// alert("Sorry under maintenace");
	ClosePanel();
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/ModAccessAll/ManageDistribute/',
		param : { 
			time : Ext.Date().getDuration()
		}
	}).load("span_top_nav");
};

// assign to
Ext.DOM.getAssignTo = function()
{
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	// alert(AssignType
	if(AssignType!='')
	{
		Ext.Ajax
		({
			url    : Ext.DOM.INDEX+"/ModAccessAll/"+AccessTypeLink[AssignType]+"/",
			method : 'POST',
			param  : {}	
		}).load("Assign-level");
	}
	else
	{
		Ext.Ajax
		({
			url    : Ext.DOM.INDEX+"/ModAccessAll/AccessDefault/",
			method : 'POST',
			param  : {}	
		}).load("Assign-level");
	}
	
}

// nav assign to
Ext.DOM.NavAssignTo = function()
{
	var AssignType = Ext.Cmp('ModAccess_AssignType').getValue();
	// alert(AssignType
	if(AssignType!='')
	{
		Ext.Ajax
		({
			url    : Ext.DOM.INDEX+"/ModAccessAll/"+AccessTypeLink[AssignType]+"/",
			method : 'POST',
			param  : {
				control : 'nav',
				input_type : 'combo'
			}	
		}).load("Assign-nav");
	}
	else
	{
		Ext.Ajax
		({
			url    : Ext.DOM.INDEX+"/ModAccessAll/AccessDefault/",
			method : 'POST',
			param  : {
				control : 'nav'
			}	
		}).load("Assign-nav");
	}
	
}

// @ pack : upload data listener 

Ext.DOM.UploadData = function() {
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/MgtRandDeb/UploadPanelAccessAll/',
		param : { 
			time : Ext.Date().getDuration(),
			templatetype : 2
		}
	}).load("span_top_nav");
};

// Ext.Cmp('autostart').listener({
 // onChange : function(){
  // alert("tes");
	// Ext.Ajax ({	
		// url	  : Ext.DOM.INDEX +'/MgtRandDeb/LoadTimeExp/',
		// param : { 
			// time : Ext.Date().getDuration(),
			// autostart : Ext.Cmp('autostart').getValue()
		// }
	// }).load("tes");
  // }
// });
Ext.DOM.TimeAuto = function()
{
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/MgtRandDeb/LoadTimeExp/',
		param : { 
			time : Ext.Date().getDuration(),
			autostart : Ext.Cmp('autostart').getValue()
		}
	}).load("time");
};
// var AssignTypeUI =  ( Ext.Ajax ({
		// url 	: Ext.DOM.INDEX +'/MgtRandDeb/get_random_by',
		// method 	: 'POST',
		// param 	: {}	
	// }).json());
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="ModAccessAll">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Assign Type &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('ModAccess_AssignType','select auto', $Assign_Type, _get_exist_session('ModAccess_AssignType'),array('change'=>'Ext.DOM.NavAssignTo();'));?></td>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('ModAccess_cust_id','input_text long', _get_exist_session('ModAccess_cust_id'));?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # New Agent &nbsp;:</td>
			<td class="bottom"><span id="Assign-nav"><?php echo form()->combo('nav_Assign_To','select auto', $New_Agent, _get_exist_session('nav_Assign_To'));?></span></td>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('ModAccess_cust_name','input_text long', _get_exist_session('ModAccess_cust_name'));?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # Exist Agent &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('ModAccess_Exist_Agent','select auto', $Exist_Agent, _get_exist_session('ModAccess_Exist_Agent'));?></td>
			<td class="text_caption bottom"> # Account Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('ModAccess_account_status','select auto', $AccountStatus, _get_exist_session('ModAccess_account_status'));?></td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="span_top_nav"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->