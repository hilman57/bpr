<?php
/* @ def 	 : view upload manual
 * 
 * @ param	 : sesion  
 * @ package : bucket data 
 */
 
?>
<div class="box-shadow" style="padding:10px;">
<fieldset class='corner'>
<div style="border-left:0px solid #dddddd;text-align:top;margin-top:-10px;">
	<table align="left" border=0 cellpadding="5px">
		<tr>
			<td class="left text_caption bottom" style='height:24px;'># Agent list &nbsp;</td>
			<td class="center text_caption bottom">:</td>
			<td class="left bottom" valign='top'><?php echo form()->listCombo('agent_lock_random','select auto',$agent_list);?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="left bottom" style='padding-left:12px;' colspan=2> 
				<input type="button" class="lock button" onclick="Ext.DOM.LockDebiturRandom();" value="Lock Debitur">
				<input type="button" class="close button" onclick="Ext.DOM.ClosePanel();" value="Close">
			</td>
		</tr>
	</table>
</div>
</fieldset>
</div>