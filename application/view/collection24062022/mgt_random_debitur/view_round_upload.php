<?php __(javascript()); ?>
<script type="text/javascript">


// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Back To Random']],
		extMenu  : [['backtosetup']],
		extIcon  : [['house.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});


/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
Ext.DOM.backtosetup = function()
{
	if( confirm('Do you want to back random setup ?')){
		Ext.Ajax({ url : Ext.DOM.INDEX+'/MgtRandDeb/', method :'GET', param : {}}).load("main_content");
	}
}

Ext.DOM.Upload = function()
{
	var CONDS_VARS;
	CONDS_VARS = Ext.Serialize('form_upload_round').Required([
		'TemplateId',
		'round_time_upload',
		'fileToupload'
	]);
	// console.log(CONDS_VARS);
	if( !CONDS_VARS  ){
		Ext.Msg("Please input field of form ").Info();
		return false;
	}
	$("#frm-hide").hide();
	$("#frm-loading").show();
	// Ext.Cmp("loading-image").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtBucket/UploadBucket/',
		method 	: 'POST',
		param	: Ext.Join([
					Ext.Serialize('form_upload_round').getElement()
				]).object(),
		complete : function(fn){
			var ERR = eval(fn.target.DONE);
			try {	
				if( ERR )
				{
					var CALLBACK = JSON.parse(fn.target.responseText);
					if( (typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null) ){
						var msg = "\n";
						for(var i in  CALLBACK.mesages ){
							msg += "Message : "+ CALLBACK.mesages[i].N +",\nSuccess : "+CALLBACK.mesages[i].Y +"\n Blacklist : "+ CALLBACK.mesages[i].B +",\nDuplicate : "+  CALLBACK.mesages[i].D  +"\nDuplicate(s) : "+CALLBACK.mesages[i].X+"\n";
						}	
						Ext.Msg(msg).Error();
					}
					else{
						Ext.Msg(CALLBACK.mesages).Info();
					}	
					// Ext.Cmp("loading-image").setText('');
					$("#frm-loading").hide();
					$("#frm-hide").show();
				}	
			}
			catch(e){ alert(e); }
		}
	}).upload();
}

Ext.DOM.LoadRoundLeader = function()
{
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/MgtRandDeb/RoundperTeam/',
		param : { 
			time : Ext.Date().getDuration(),
			bool_round_team : Ext.Cmp('bool_round_team').getValue()
		}
	}).load("round_leader");
};

</script>
<div id="frm-loading" class="ui-widget-ajax-spiner" style="display:none;"></div>
<!-- @ pack : start content -->
<div id= "frm-hide" > 
<fieldset class="corner"">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Round Account Upload</span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="form_upload_round">
	<table  border=0 cellpadding=1 cellspacing=1>
				<tr>
					<td class="text_caption bottom" ># Range Time&nbsp;:</td>
					<td class="bottom" ><?php echo form()->input('round_time_upload','input_text box');?> (Minute)</td>
					<td class="text_caption bottom" ># Round per Team &nbsp;:</td>
					<td class="bottom" ><?php echo form() -> combo('bool_round_team','select long', array('1'=>"YES",'0'=>"NO"),"0",array('change'=>'Ext.DOM.LoadRoundLeader();'));?></td>
				</tr>

				<tr>
					<td class='text_caption bottom' nowrap> * Template &nbsp;:</td>
					<td class='bottom'><?php echo form()->combo('TemplateId','select long', $template);?></td>
					<td colspan="2" rowspan="4"><span id="round_leader" ></span></td>
				</tr>
				<tr>
					<td class='text_caption bottom' nowrap> * Product &nbsp;:</td>
					<td class='bottom'><?php echo form()->combo('round_user_product','select long', $product);?></td>
				</tr>
				<tr>
					<td class='text_caption bottom' nowrap> * Upload File &nbsp;:</td>
					<td class='bottom'><?php echo form()->upload('fileToupload', 'input_text long'); ?></td>
				</tr>
				<?php 
				if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
				array( USER_ROOT,USER_ADMIN ) ) )
				{
					echo "<tr>
					<td class=\"text_caption bottom\" nowrap># Create Setup By&nbsp;:</td>
					<td class=\"bottom\">".form() -> combo('user_create_setup','select long', $spv)."</td>
					</tr>";
				}
				?>
				<tr>
					<td class="text_caption" >
						<span id="loading-image"></span></td>
					<td colspan='4'>
						<input type="button" class="button upload" onclick="Ext.DOM.Upload();" value="Upload">
					</td>
				</tr>
			</table>
	</form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
</fieldset>	
</div>
<!-- @ pack : stop  content -->