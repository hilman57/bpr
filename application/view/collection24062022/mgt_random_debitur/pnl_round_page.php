<?php 

if(!function_exists('RunStatus') ) 
{
  function RunStatus( $runcode = 0 ){
	$status = array(
		0=> "Stop",
		1=> "Running..."
	);
	return (string)$status[$runcode];
  }
}
/*
/*
 * @ pack : load helpers
 */

 $arr_obj = new EUI_Object($content_pages);
/*
 * @ pack : get all parameters 
 */
 
 $type 	= _get_post('type');
 $orderby = _get_post('orderby');
 $next_order = ($type=='ASC'?'DESC':'ASC');
 
 // debug only : $arr_header = $arr_obj->fetch_field();
 
$arr_header = array
(
	"modul_name"=> "Modul Name.",
	"full_name"=> "Create By",
	"st_create_date_ts"=> "Create Date",
	"duration"=>"Duration",
	"next_round_ts"=>"Next Round Time",
	"bool_running"=>"Status",
	"st_round_id"=>"Action"
); 

$arr_class = array
(
	"modul_name"=> "content-middle",
	"full_name" => "content-middle",
	"st_create_date_ts" => "content-middle",
	"duration" => "content-middle",
	"next_round_ts" => "content-middle",
	"bool_running" => "content-middle",
	"st_round_id" => "content-lasted"
); 

$arr_align = array
(
	"modul_name" => "left",
	"st_create_date_ts" => "center",
	"st_round_id" => "center",
	"duration"=>"center",
	"next_round_ts"=>"center"
); 

 /*
  * @ pack : get all labels -  array header 
  */
  
 $arr_width = array();

 /*
 * @ pack : get all labels -  array header 
 */
 $arr_function = array ( 
	"st_create_date_ts" => "_getDateTime",
	"bool_running" => "RunStatus"
 ); 
 
 /*
 * @ pack : get all labels -  array header 
 */
  
 $arr_wrap = array(
 ); 
 
 
// -------------- generate label on grid ----------------> 

echo "<table border=0 cellspacing=1 width=\"99%\">".
	"<tr height=\"25\"> ";
		echo "<th class=\"ui-corner-top ui-state-default center\" width=\"2%\" nowrap>No.</th>";
		
	foreach( $arr_header as $field => $value ){
		if( in_array($orderby, array($field) ))
		{
			echo "<th class=\"ui-corner-top ui-state-default center\" width=\"{$arr_width[$field]}\"><span class=\"header_order ".strtolower($type)."\" onclick=\"new ViewlistPanel({page:0,  orderby:'{$field}', type:'{$next_order}'});\">&nbsp;{$value}</span></th>";
		} else {
			echo "<th class=\"ui-corner-top ui-state-default center\" width=\"{$arr_width[$field]}\"><span class=\"header_order\" title=\"Sort By {$value}\" onclick=\"new ViewlistPanel({page:0, orderby:'{$field}', type:'{$next_order}' });\">&nbsp;{$value}</span></th>";
		}
	}
	
echo "</tr>";

// ---------------- content ----------------

 if( is_array($content_pages) ) 
{ 
 $no = $start_page+1;
 foreach( $content_pages as $num => $rows )
{
 $row = new EUI_Object( $rows );
// @ pack : of list color 
 $back_color = ( $num%2!=0 ? '#FFFFFF' :'#FFFEEE');
 echo "<tr bgcolor=\"{$back_color}\" class=\"onselect\" style=\"cursor:pointer;\" height=\"18\">";
 echo "<td class=\"content-first\" nowrap>{$no}</td>";
  
 	
 foreach( array_keys($arr_header) as $k => $fields )
 {
   
   if( in_array($fields, array('st_round_id') ) ) {
		$jsEvent = "&nbsp;";
		if($row->get_value('bool_running')==1)
		{
			if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
			array(USER_SPV) ) && _get_session('UserId')== $row->get_value('created_id'))
			{
				$jsEvent = '<div class="ui-widget-form-cell text_caption">'.
				form()->button("BtnStopRound", "button remove ui-context-chrome", "Stop", array("click" => "new StopStRound(".$row->get_value($fields).");") ).
				'</div>';
				
				// $jsEvent .= '<div class="ui-widget-form-cell text_caption">'.
				// form()->button("BtnForceRound", "button ui-context-chrome", "Stop", array("click" => "new ForceRound(".$row->get_value($fields).");") ).
				// '</div>';
			}
			else
			{
				$jsEvent = '<div class="ui-widget-form-cell text_caption">'.
				form()->button("BtnStopRound", "button remove ui-context-chrome", "Stop", array("click" => "new StopStRound(".$row->get_value($fields).");") ).
				'</div>';
				
				// $jsEvent .= '<div class="ui-widget-form-cell text_caption">'.
				// form()->button("BtnForceRound", "button ui-context-chrome", "Force Round", array("click" => "new ForceRound(".$row->get_value($fields).");") ).
				// '</div>';
			}
		}
   } else {
		$jsEvent = "{$row->get_value($fields, $arr_function[$fields])}";   
   } 
   
   if(strcmp( $fields, $orderby )== 0 ){
	  echo  "<td class=\"$arr_class[$fields] ui-widget-select-order {$arr_align[$fields]}\" ${arr_wrap[$fields]}>{$jsEvent}</td>";
   }else{
	  echo  "<td class=\"$arr_class[$fields] ui-widget-unselect-order {$arr_align[$fields]}\" ${arr_wrap[$fields]}>{$jsEvent}</td>";
   }
 }
 
// ---------- on event ------------------------------------------------
	echo "</tr>";
	$no++;	
 }	
 
}

/* @ pack : -------------------------------------------------------
 * @ pack : # get list off page #----------------------------------
 * @ pack : -------------------------------------------------------
 */

 $max_page = 5;
 
// @ pack : start html  

 $_li_create = " <div class='page-web-voice' style='margin-left:-5px;margin-top:2px;border-top:0px solid #FFFEEE;'><ul>";
 
// @ pack : list 
 
 $start =(int)(!$select_pages ? 1: ((($select_pages%$max_page ==0) ? ($select_pages/$max_page) : intval($select_pages/$max_page)+1)-1)*$max_page+1);
 $end   =(int)((($start+$max_page-1)<=$total_pages) ? ($start+$max_page-1) : $total_pages );
	
// @ pack : like here 

 if( $select_pages > 1) 
 {
	$post = (int)(($select_pages)-1);
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewlistPanel({page: 1, orderby:'${orderby}', type:'${type}'});\" ><a href=\"javascript:void(0);\">&lt;&lt;</a></li>";
		
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewlistPanel({page: ${post}, orderby:'${orderby}', type:'${type}'});\" ><a href=\"javascript:void(0);\">&lt;</a></li>";
 }

// @ pack : check its 

 if($start>$max_page){
	$_li_create.="<li cclass=\"page-web-voice-normal\"  onClick=\"Ext.DOM.ViewlistPanel({page:(${start}-1), orderby :'${orderby}', type:'${type}'});\" ><a href=\"javascript:void(0);\">...</a></li>";
 }

// @ pack : check its 
 
 for( $p = $start; $p<=$end; $p++)
 { 
	if( $p == $select_pages ){ 
		$_li_create .="<li class=\"page-web-voice-current\" id=\"${p}\" onClick=\"new ViewlistPanel({page:${p}, orderby :'${orderby}', type:'${type}'});\"> <a href=\"javascript:void(0);\">{$p}</a></li>";
	 } else {
		$_li_create .=" <li class=\"page-web-voice-normal\" id=\"${p}\" onClick=\"new ViewlistPanel({page:${p}, orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\">{$p}</a></li>";
	}
 }

// @ pack : check its 
  
 if($end<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewlistPanel({page:${end}, orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\" >...</a></li>";
 }

// @ pack : check its 
 
 if($select_pages<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewlistPanel({page:(${select_pages}+1),  orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\" title=\"Next page\">&gt;</a></li>";
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewlistPanel({page:${total_pages},orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\" title=\"Last page\">&gt;&gt;</a></li>";
 }
		
// @ pack : check its 
	
 $_li_create .="</ul></div>";
 echo "<tr>".
		"<td colspan='6'>{$_li_create}</td> ".
		"<td colspan='2'style='color:brown;' nowrap>Record(s)&nbsp;: <span class='input_text' style='padding:4px 2px 2px 2px;' id='ui-total-swap-record'>{$total_records}</span></td>".
	"</tr>	";
echo "</table>";
// echo "<pre>";
// print_r($content_pages);
// echo "</pre>";
?>