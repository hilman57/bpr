<?php echo base_window_header("Search Round Setup"); ?>
<style> 
.ui-chrome{ border-radius:3px;}
.ui-chrome:hover{ border:1px solid #FF4321;}
.ui-context-chrome { border-radius:4px;height:22px;}
</style>
<script>

// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewlistPanel 
 */
 
 function ViewlistPanel( obj )
{	
	$("#ui-widget-content-pnl-round").waiter 
	({
		url 	: new Array('MgtRandDeb','PanelRoundPage'),
		param 	:{
			SetupId : window.opener.Choosen_Setup
		},
		order   : {
			order_type : obj.type,
			order_by   : obj.orderby,
			order_page : obj.page	
		}, 
		complete : function( obj ) {
			$(obj).css({"height" : "100%", "padding" : "0px -5px 4px -5px" });
		}	
	});

}
// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewlistPanel 
 */
 
function StopStRound(id){

	Ext.Ajax({
			url 	: window.opener.Ext.DOM.INDEX+'/MgtRandDeb/StopRoundPanel',
			method 	: 'POST',
			param 	: { 
				st_round_id : id
			},
			ERROR	: function(e) {
				var ERROR = JSON.parse(e.target.responseText);
				if( ERROR.Message ) {
					alert("Success Stop");
					new ViewlistPanel ({ orderby : '',  type: '', page: 0	 });
				}
				else
				{
					alert("Failed to Stop");
				}
			}	
	}).post();
}

// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewlistPanel 
 */
 
$(document).ready(function(){
	new ViewlistPanel ({ orderby : '',  type: '', page: 0	 });
});
</script>
</head>
<body style="margin:10px 5px 5px 5px;">
<fieldset class="corner" style="border-radius:3px;">
	<legend class="icon-customers"> 
		<span style="margin-left:10px;">Panel Round</span></legend>
		
	<div class="ui-widget-form-table" style="margin-left:4px;margin-top:-2px;width:99%;border:0px solid #000;">
		
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell">
			
				<fieldset class="corner" style="border-radius:3px;">
				<legend> List Round Setup</legend>
				<!-- darrii -------->
					<div style="margin-top:-5px;" id="ui-widget-content-pnl-round"></div>
				</fieldset>
				
			</div>
		</div>
		
	</div>
	
</fieldset>	
</body>
</html>