<?php echo javascript(); ?>
<script type="text/javascript">

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 	
/* create object default parameter assigning **/

var datas = 
{
	cust_number_id 	 	: '<?php echo _get_exist_session('cust_number_id');?>',
	cust_first_name  	: '<?php echo _get_exist_session('cust_first_name');?>',
	cust_home_phone  	: '<?php echo _get_exist_session('cust_home_phone');?>',
	cust_office_phone	: '<?php echo _get_exist_session('cust_office_phone');?>',
	cust_mobile_phone	: '<?php echo _get_exist_session('cust_mobile_phone');?>', 
	cust_campaign_id 	: '<?php echo _get_exist_session('cust_campaign_id');?>', 
	cust_call_result 	: '<?php echo _get_exist_session('cust_call_result');?>', 
	cust_user_id 	 	: '<?php echo _get_exist_session('cust_user_id');?>',
	cust_start_date  	: '<?php echo _get_exist_session('cust_start_date');?>',
	cust_end_date    	: '<?php echo _get_exist_session('cust_end_date');?>',
	cust_approve_status : '<?php echo _get_exist_session('cust_approve_status');?>',
	order_by 		 	: '<?php echo _get_exist_session('order_by');?>',
	type	 		 	: '<?php echo _get_exist_session('type');?>',
	category_id_app		: '<?php echo _get_exist_session('category_id_app');?>'
}
		
Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';
	
	
/* assign navigation filter **/
var navigation = 
{
	custnav : Ext.DOM.INDEX +'/QtyApprovalData/index/',
	custlist : Ext.DOM.INDEX +'/QtyApprovalData/Content/',
}
		
/* assign show list content **/
		
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/* function searching customers **/

var validation_check =  function(CustomerId)
	{
		if( CustomerId )
			{
				Ext.File = '../class/class.src.qualitycontrol.php'; 
				Ext.Params = {
					action:'validation_check',
					CustomerId : CustomerId
				}	
				
				return Ext.eJson();	
			} 
 } 
	
/* function searching customers **/
	
var searchCustomer = function()
{
	Ext.EQuery.construct( navigation ,{
		cust_start_date  	: Ext.Cmp('cust_start_date').getValue(),
		cust_end_date     	: Ext.Cmp('cust_end_date').getValue(),
		cust_number_id  	: Ext.Cmp('cust_number_id').getValue(), 
		cust_first_name 	: Ext.Cmp('cust_first_name').getValue(),
		cust_home_phone   	: Ext.Cmp('cust_home_phone').getValue(),
		cust_office_phone 	: Ext.Cmp('cust_office_phone').getValue(),
		cust_mobile_phone 	: Ext.Cmp('cust_mobile_phone').getValue(),
		cust_campaign_id  	: Ext.Cmp('cust_campaign_id').getValue(),
		cust_call_result  	: Ext.Cmp('cust_call_result').getValue(),
		cust_user_id 	 	: Ext.Cmp('cust_user_id').getValue(),	
		cust_approve_status : Ext.Cmp('cust_approve_status').getValue(),
		category_id_app		: Ext.Cmp('category_id_app').getValue()
	});
	
	Ext.EQuery.postContent();
}
	
/* function approve all customer*/
var approveAll = function()
{			
		var cust_id  = Ext.checkedValue('chk_cust_call');
		if( cust_id!='')
		{
			var confirmasi_status = Ext.dom('confirmasi_status').value;
			if( confirmasi_status!='' )
			{
				if( confirm('Do you want to confirm this Customers ? '))
				{
					Ext.File = '../class/class.src.qualitycontrol.php'; 
					Ext.Method = 'POST'
					Ext.Params = { 
						action : 'approve_all',
						status : confirmasi_status,
						cust_id : cust_id
					}
					
					var error = Ext.eJson();
					if( error.result){
						alert('Success, Auto Confirm ..!');
						//Ext.EQuery.construct(navigation,datas);
						Ext.EQuery.postContent();
					}
					else{
						alert('Failed, Auto Confirm ..!');
					}
				}
			}
			else{
				alert('Please select a confirm status!'); 
				return false;
			}	
		}
		else{
				alert("No Customers Selected !");
				return false;
			}
	}
		
/* function clear searching form **/	
		
var resetSeacrh = function()
{
	Ext.Cmp('cust_start_date').setValue('');
	Ext.Cmp('cust_end_date').setValue('');
	Ext.Cmp('cust_number_id').setValue(''); 
	Ext.Cmp('cust_first_name').setValue('');
	Ext.Cmp('cust_home_phone').setValue('');
	Ext.Cmp('cust_office_phone').setValue('');
	Ext.Cmp('cust_mobile_phone').setValue('');
	Ext.Cmp('cust_campaign_id').setValue('');
	Ext.Cmp('cust_call_result').setValue('');
	Ext.Cmp('cust_user_id').setValue('');	
	Ext.Cmp('cust_approve_status').setValue('');
	Ext.Cmp('category_id_app').setValue('');
}
  
 /* go to call contact detail customers **/
 
var showPolicy = function()
		{
			var arrCallRows  = Ext.checkedValue('chk_cust_call');
			var arrCountRows = arrCallRows.split(','); 
				if( arrCallRows!='')
				{	
					if( arrCountRows.length == 1 )
					{
						var error_code = validation_check(arrCountRows[0]);
						if( error_code.result==0 || error_code.result==1) 
						{
							Ext.File = 'dta_qc_detail.php';
							Ext.Params = { CustomerId : arrCountRows[0] }
							Ext.EQuery.Content();
						}
						else{ 
							alert('Sorry , Data in Qa Process. Please select other Customers !');
							return false 
						}
					}
					else{
						alert("Select One Customers !")
						return false;
					}
					
				}else{
					alert("No Customers Selected !");
					return false;
				}	
		}
		
/* get confirm by json methode **/
	
	Ext.getConfirmStatus = function()
	{
		Ext.File = '../class/class.src.qualitycontrol.php'; 
		Ext.Params = {
			action:'get_confirm',
		}
		return Ext.eJson();
	} 	
		
	
/* memanggil Jquery plug in */
$(function(){
	$('#toolbars').extToolbars
	({
		extUrl   :Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle :[['Search'],['Cancel Approval '],['Clear']],
		extMenu  :[['searchCustomer'],['showPolicy'],['resetSeacrh']],
		extIcon  :[['zoom.png'],['application_form_delete.png'],['cancel.png']],
		extText  :true,
		extInput :true,
		extOption:
			[{
				render : 4,
				type   : 'combo',
				header : 'Call Reason ',
				id     : 'v_result_customers', 	
				name   : 'v_result_customers',
				triger : '',
				store  : []
			}]
	});
	$('#cust_start_date,#cust_end_date').datepicker({showOn: 'button', buttonImage:  Ext.DOM.LIBRARY +'/gambar/calendar.gif', buttonImageOnly: true, dateFormat:'dd-mm-yy',readonly:true});
});
	
</script>
<!-- start : content -->
<fieldset class="corner">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	

<?php

//print_r($_SESSION);
/*
	$this -> getCampaignAssigment()
	$this -> _get_lk_category()
	$this->getResultStatus()
	$this->getUserList()
	
	$db -> Factory -> getResultStatus()
	$db -> Factory -> getCampaignAssigment()
	$db -> Factory -> product_category()
	$db -> Factory -> getApprovalStatus()
	$db->Factory -> getUserList()
	
	'page' 			=> $this -> M_QtyApprovalData -> _get_default(),
		'CampaignId' 	=> $this -> M_SetCampaign -> _get_campaign_name(),
		'CardType' 		=> $this -> M_SrcCustomerList ->_getCardType(), 
		'GenderId' 		=> $this -> M_SrcCustomerList ->_getGenderId(),
		'UserId' 		=> $this -> M_SysUser -> _get_teleamarketer(), 
		'CallResult' 	=> self::getCallResult(), 
		'ProductId' 	=> self::_getProductId()
*/

?>
<div id="span_top_nav">
<div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
 <table cellpadding="3px;">
	<tr>
		<td class="text_caption"> Customer ID</td>
		<td><?php echo form() -> input('cust_number_id','input_text long',_get_exist_session('cust_number_id'));?></td>
		<td class="text_caption"> Home Phone</td>
		<td><?php echo form() -> input('cust_home_phone','input_text long',_get_exist_session('cust_home_phone'));?></td>
		<td class="text_caption"> Campaign</td>
		<td><?php echo form() -> combo('cust_campaign_id','select long',$CampaignId,_get_exist_session('cust_campaign_id'));?></td>
		<td class="text_caption"> Product Name</td>
		<td><?php echo form() -> combo('category_id_app','select long',$ProductId, _get_exist_session('category_id_app') );?></td>
	</tr>
	<tr>
		<td class="text_caption"> Customer Name </td>
		<td><?php echo form() -> input('cust_first_name','input_text long',_get_exist_session('cust_first_name'));?></td>
		<td class="text_caption"> Office Phone </td>
		<td><?php echo form() -> input('cust_office_phone','input_text long',_get_exist_session('cust_office_phone'));?></td>
		<td class="text_caption"> Call Result </td>
		<td><?php echo form() -> combo('cust_call_result','select long',$CallResult,_get_exist_session('cust_call_result'));?></td>
		<td class="text_caption"> Approve Status</td>
		<td><?php echo form() -> combo('cust_approve_status','select long',$QtyResult,_get_exist_session('cust_approve_status'));?></td>
	</tr>
	<tr>
		<td class="text_caption"> Interval </td>
		<td>
			<?php echo form() -> input('cust_start_date','input_text box',_get_exist_session('cust_start_date'));?>
			<?php echo form() -> input('cust_end_date','input_text box',_get_exist_session('cust_end_date'));?>
		</td>
		<td class="text_caption"> Mobile Phone </td>
		<td><?php echo form() -> input('cust_mobile_phone','input_text long ',_get_exist_session('cust_mobile_phone'));?></td>
		<td class="text_caption"> User ID </td>
		<td><?php echo form() -> combo('cust_user_id','select long',$UserId,_get_exist_session('cust_user_id'));?></td>
	</tr>
	</table>
</div>
 </div>
 <div id="toolbars"></div>
 <div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" ></div>
	<div id="pager"></div>
 </div>
</fieldset>	
		
	<!-- stop : content -->
	
	
	
	
	