<?php
/*
 * @ def : source EXCEL 
 *
 * ---------------------------------------------------------------
 */
 
 $this->load->helper('EUI_ExcelWorksheet'); // load library excel 
  
 $BASE_EXCEL_PATH_TEMP = APPPATH .'temp';
 $BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_TEMP ."/". date('YmdHi')."_". str_replace(" ","_",$title ) . ".xls";

 $excel_workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
 $excel_worksheet =& $excel_workbook->addworksheet();

// set style layout header 

 $excel_title =& $excel_workbook->addformat();
 $excel_title->set_bold();
 $excel_title->set_size(10);
 $excel_title->set_color('white');
 $excel_title->set_align('left');
 $excel_title->set_align('vcenter');
 $excel_title->set_fg_color('red');
 $excel_title->set_border(1);
 $excel_title->set_border_color('blue');
 
// set style layout header 

 $report_title =& $excel_workbook->addformat();
 $report_title->set_bold();
 $report_title->set_size(12);
 $report_title->set_align('left');
 $report_title->set_align('vcenter');
 
 $excel_rows = 0;
 
// title 

 $excel_worksheet->write($excel_rows, 0, $title, $report_title);
 
// then start rows  

 $start_date = $param['start_date'];
 $end_date = $param['end_date'];
 
 
 
// start rows  

 $excel_rows = $excel_rows+2; 
 while(true) 
 {
	$estart_date = $start_date;   
	 
// set rows first 
	$excel_rows = $excel_rows;
	$excel_worksheet->write($excel_rows, 0, date('d/m/Y', strtotime($estart_date)), $excel_title);
	
	$excel_rows = $excel_rows+2;
	
	$excel_worksheet->write($excel_rows, 0, "No.", $excel_title);
	$excel_worksheet->write($excel_rows, 1, "Caller Name", $excel_title);
	$excel_worksheet->write($excel_rows, 2, "Office Time", $excel_title);
	$excel_worksheet->write($excel_rows, 3, "", $excel_title);
	$excel_worksheet->write($excel_rows, 4, "", $excel_title);
	$excel_worksheet->write($excel_rows, 5, "", $excel_title);
	$excel_worksheet->write($excel_rows, 6, "", $excel_title);
	$excel_worksheet->write($excel_rows, 7, "", $excel_title);
	$excel_worksheet->write($excel_rows, 8, "", $excel_title);
	$excel_worksheet->write($excel_rows, 9, "", $excel_title);
	$excel_worksheet->write($excel_rows, 10, "Completed/Hour", $excel_title);

// set rows 2
	$excel_rows = $excel_rows+1; 
	$excel_worksheet->write($excel_rows, 0, "", $excel_title);
	$excel_worksheet->write($excel_rows, 1, "", $excel_title);
	$excel_worksheet->write($excel_rows, 2, "LOGIN Time", $excel_title);
	$excel_worksheet->write($excel_rows, 3, "LOGOUT Time", $excel_title);
	$excel_worksheet->write($excel_rows, 4, "Attempt Call", $excel_title);
	$excel_worksheet->write($excel_rows, 5, "ACD Time", $excel_title);
	$excel_worksheet->write($excel_rows, 6, "AUX Time", $excel_title);
	$excel_worksheet->write($excel_rows, 7, "Other Time", $excel_title);
	$excel_worksheet->write($excel_rows, 8, "Avail Time", $excel_title);
	$excel_worksheet->write($excel_rows, 9, "Staff Time", $excel_title); 
	$excel_worksheet->write($excel_rows, 10, "", $excel_title);  
		 
	$no = 0;
	$excel_rows = $excel_rows+1;
	if(is_array($AgentName))
	foreach( $AgentName as $UserId => $UserName )
	{
		$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
		$no++;
		
	/** sort define data by handle **/
	
		$excel_content=& $excel_workbook->addformat();
		$excel_content->set_size(10);
		$excel_content->set_align('left');
		$excel_content->set_border(1);
		$excel_content->set_border_color('blue');

	/** sort define data by handle **/
	
		$excel_number=& $excel_workbook->addformat();
		$excel_number->set_size(10);
		$excel_number->set_align('right');
		$excel_number->set_border(1);
		$excel_number->set_border_color('blue');

	/* activity callcualtion **/
	
		$total_complete 	 =($agent_activity[$estart_date][$UserId]['tot_complete'] ? $agent_activity[$estart_date][$UserId]['tot_complete'] :0);
		$total_attempt  	 =($agent_activity[$estart_date][$UserId]['tot_attempt'] ? $agent_activity[$estart_date][$UserId]['tot_attempt'] :0);
		$total_iddle_time 	 =($agent_activity[$estart_date][$UserId]['tot_idle'] ? $agent_activity[$estart_date][$UserId]['tot_idle'] :0);
		$total_acw_time 	 =($agent_activity[$estart_date][$UserId]['tot_acw'] ? $agent_activity[$estart_date][$UserId]['tot_acw'] :0);
		$total_ready_time 	 =($agent_activity[$estart_date][$UserId]['tot_not_ready'] ? $agent_activity[$estart_date][$UserId]['tot_not_ready'] :0);
		$total_busy_time 	 =($agent_activity[$estart_date][$UserId]['tot_busy'] ? $agent_activity[$estart_date][$UserId]['tot_busy'] :0);
		$total_notready_time =($agent_activity[$estart_date][$UserId]['tot_not_ready'] ? $agent_activity[$estart_date][$UserId]['tot_not_ready'] :0);
		$activity_login 	 =($agent_activity[$estart_date][$UserId]['login'] ? $agent_activity[$estart_date][$UserId]['login'] :'-');
		$activity_logout 	 =($agent_activity[$estart_date][$UserId]['logout'] ? $agent_activity[$estart_date][$UserId]['logout'] :'-');
		$total_activity 	 =($agent_activity[$estart_date][$UserId]['tot_activity'] ? $agent_activity[$estart_date][$UserId]['tot_activity'] :0);
		
	 /** callcualtion source data **/
	 
		$avg_call_complete = ( $total_complete ? round( ($total_complete / ($total_activity - $total_notready_time)), 2) : 0);
		
	 /** show detail on rows **/
	    
		$excel_worksheet->write($excel_rows, 0, $no, $excel_content );
		$excel_worksheet->write($excel_rows, 1, $UserName, $excel_content );
		$excel_worksheet->write($excel_rows, 2, $activity_login, $excel_content );
		$excel_worksheet->write($excel_rows, 3, $activity_logout, $excel_content );
		$excel_worksheet->write($excel_rows, 4, $total_attempt, $excel_content );
		$excel_worksheet->write($excel_rows, 5, ($total_busy_time?_getDuration($total_busy_time):'-'), $excel_content );
		$excel_worksheet->write($excel_rows, 6, ($total_notready_time?_getDuration($total_notready_time):'-'), $excel_content );
		$excel_worksheet->write($excel_rows, 7, ($total_acw_time?_getDuration($total_acw_time):'-'), $excel_content );
		$excel_worksheet->write($excel_rows, 8, ($total_iddle_time?_getDuration($total_iddle_time):'-'), $excel_content );
		$excel_worksheet->write($excel_rows, 9, ($total_activity?_getDuration($total_activity):'-'), $excel_content );
		$excel_worksheet->write($excel_rows, 10,($avg_call_complete?$avg_call_complete:'-'), $excel_content);
		$excel_rows+=1;
    }
	
	$excel_rows = $excel_rows+1;
	
 // set rows bottom 
 
	if ( $start_date == $end_date ) break;
		$start_date = _getNextDate($start_date);
	
 }
 
 $excel_workbook->close();

// start download file 

if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	 

?>