<?php
/*
 * @ def : source EXCEL 
 *
 * ---------------------------------------------------------------
 */
 
 $this->load->helper('EUI_ExcelWorksheet'); // load library excel 
  
 $BASE_EXCEL_PATH_TEMP = APPPATH .'temp';
 $BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_TEMP ."/". date('YmdHi')."_". str_replace(" ","_",$title ) . ".xls";

 $excel_workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
 $excel_worksheet =& $excel_workbook->addworksheet();

// set style layout header */

 $excel_title =& $excel_workbook->addformat();
 $excel_title->set_bold();
 $excel_title->set_size(10);
 $excel_title->set_color('white');
 $excel_title->set_align('left');
 $excel_title->set_align('vcenter');
 $excel_title->set_fg_color('red');
 $excel_title->set_border(1);
 $excel_title->set_border_color('blue');
 
// set style layout header 

 $report_title =& $excel_workbook->addformat();
 $report_title->set_bold();
 $report_title->set_size(12);
 $report_title->set_align('left');
 $report_title->set_align('vcenter');
 $excel_rows = 0;
 
// title 
 $excel_worksheet->write($excel_rows, 0, $title, $report_title);
// then start rows  
 $excel_rows = $excel_rows+2; 

 //1
 
 $excel_worksheet->write($excel_rows, 0, "No.", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "Caller Name", $excel_title);
 $excel_worksheet->write($excel_rows, 2, "(Contacted+Call Completed+Pending)", $excel_title);
 $excel_worksheet->write($excel_rows, 3, "", $excel_title);
 $excel_worksheet->write($excel_rows, 4, "", $excel_title);
 $excel_worksheet->write($excel_rows, 5, "", $excel_title);
 $excel_worksheet->write($excel_rows, 6, "", $excel_title);
 $excel_worksheet->write($excel_rows, 7, "", $excel_title);
 $excel_worksheet->write($excel_rows, 8, "", $excel_title);
 $excel_worksheet->write($excel_rows, 9, "", $excel_title);
 $excel_worksheet->write($excel_rows, 10, "", $excel_title);
 $excel_worksheet->write($excel_rows, 11, "", $excel_title);
 $excel_worksheet->write($excel_rows, 12, "", $excel_title);
 $excel_worksheet->write($excel_rows, 13, "", $excel_title);
 $excel_worksheet->write($excel_rows, 14, "", $excel_title);
 $excel_worksheet->write($excel_rows, 15, "", $excel_title);
 $excel_worksheet->write($excel_rows, 16, "", $excel_title);
 $excel_worksheet->write($excel_rows, 17, "", $excel_title);
 $excel_worksheet->write($excel_rows, 18, "", $excel_title);
 $excel_worksheet->write($excel_rows, 19, "Surveyed Ratio", $excel_title);
 $excel_worksheet->write($excel_rows, 20, "Contact Ratio", $excel_title); 
 
 //2
 $excel_rows = $excel_rows+1; 
 $excel_worksheet->write($excel_rows, 0, "", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "", $excel_title);
 $excel_worksheet->write($excel_rows, 2, "Total Data", $excel_title);
 $excel_worksheet->write($excel_rows, 3, "Completed", $excel_title);
 $excel_worksheet->write($excel_rows, 4, "Contacted", $excel_title);
 $excel_worksheet->write($excel_rows, 5, "", $excel_title);
 $excel_worksheet->write($excel_rows, 6, "Call Completed", $excel_title);
 $excel_worksheet->write($excel_rows, 7, "", $excel_title);
 $excel_worksheet->write($excel_rows, 8, "", $excel_title);
 $excel_worksheet->write($excel_rows, 9, "", $excel_title);
 $excel_worksheet->write($excel_rows, 10, "", $excel_title);
 $excel_worksheet->write($excel_rows, 11, "", $excel_title);
 $excel_worksheet->write($excel_rows, 12, "", $excel_title);
 $excel_worksheet->write($excel_rows, 13, "Pending - Inprogress Data", $excel_title);
 $excel_worksheet->write($excel_rows, 14, "", $excel_title);
 $excel_worksheet->write($excel_rows, 15, "", $excel_title);
 $excel_worksheet->write($excel_rows, 16, "", $excel_title);
 $excel_worksheet->write($excel_rows, 17, "", $excel_title);
 $excel_worksheet->write($excel_rows, 18, "", $excel_title);
 $excel_worksheet->write($excel_rows, 19, "", $excel_title);
 $excel_worksheet->write($excel_rows, 20, "", $excel_title);

 // 3 
 $excel_rows = $excel_rows+1;
 $excel_worksheet->write($excel_rows, 0, "", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "", $excel_title);
 $excel_worksheet->write($excel_rows, 2, "", $excel_title);
 $excel_worksheet->write($excel_rows, 3, "", $excel_title);
 $excel_worksheet->write($excel_rows, 4, "", $excel_title);
 $excel_worksheet->write($excel_rows, 5, "", $excel_title);
 $excel_worksheet->write($excel_rows, 6, "Non Contacted", $excel_title);
 $excel_worksheet->write($excel_rows, 7, "", $excel_title);
 $excel_worksheet->write($excel_rows, 8, "Uncontacted (UNCLEAN DATA)", $excel_title);
 $excel_worksheet->write($excel_rows, 9, "", $excel_title);
 $excel_worksheet->write($excel_rows, 10, "", $excel_title);
 $excel_worksheet->write($excel_rows, 11, "", $excel_title);
 $excel_worksheet->write($excel_rows, 12, "", $excel_title);
 $excel_worksheet->write($excel_rows, 13, "", $excel_title);
 $excel_worksheet->write($excel_rows, 14, "", $excel_title);
 $excel_worksheet->write($excel_rows, 15, "", $excel_title);
 $excel_worksheet->write($excel_rows, 16, "", $excel_title);
 $excel_worksheet->write($excel_rows, 17, "", $excel_title);
 $excel_worksheet->write($excel_rows, 18, "", $excel_title);
 $excel_worksheet->write($excel_rows, 19, "", $excel_title);
 $excel_worksheet->write($excel_rows, 20, "", $excel_title);

 // 4 
 $excel_rows = $excel_rows+1;
 $excel_worksheet->write($excel_rows, 0, "", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "", $excel_title);
 $excel_worksheet->write($excel_rows, 2, "", $excel_title);
 $excel_worksheet->write($excel_rows, 3, "", $excel_title);
 $excel_worksheet->write($excel_rows, 4, "Agree", $excel_title);
 $excel_worksheet->write($excel_rows, 5, "No Interest To Talk", $excel_title);
 $excel_worksheet->write($excel_rows, 6, "Can Not Be Reached", $excel_title);
 $excel_worksheet->write($excel_rows, 7, "No Answer", $excel_title);
 $excel_worksheet->write($excel_rows, 8, "Busy Tone", $excel_title);
 $excel_worksheet->write($excel_rows, 9, "Wrong Number", $excel_title);
 $excel_worksheet->write($excel_rows, 10, "Tulalit (Dead Tone)", $excel_title);
 $excel_worksheet->write($excel_rows, 11, "Leave Masssge", $excel_title);
 $excel_worksheet->write($excel_rows, 12, "Return List", $excel_title);
 $excel_worksheet->write($excel_rows, 13, "1st", $excel_title);
 $excel_worksheet->write($excel_rows, 14, "2nd", $excel_title);
 $excel_worksheet->write($excel_rows, 15, "3rd", $excel_title);
 $excel_worksheet->write($excel_rows, 16, "4th", $excel_title);
 $excel_worksheet->write($excel_rows, 17, "5th++", $excel_title);
 $excel_worksheet->write($excel_rows, 18, "Call Back Later", $excel_title);
 $excel_worksheet->write($excel_rows, 19, "", $excel_title);
 $excel_worksheet->write($excel_rows, 20, "", $excel_title);
 
 
 /** sort define data by handle **/
	
 $excel_content=& $excel_workbook->addformat();
 $excel_content->set_size(10);
 $excel_content->set_align('left');
 $excel_content->set_border(1);
 $excel_content->set_border_color('blue');

/** sort define data by handle **/
	
 $excel_number=& $excel_workbook->addformat();
 $excel_number->set_size(10);
 $excel_number->set_align('right');
 $excel_number->set_border(1);
 $excel_number->set_border_color('blue');
		
 $excel_rows = $excel_rows+1;
 $no = 0;
 foreach($AgentName as $UserId => $UserName )
 {
	$color = ($no%2!=0?'#E2D7D7':'#FFFFFF');
	$no++;
			
	// ratio of survey 
	$total_survey = ($data_size[$UserId]['tot_size_survey']?$data_size[$UserId]['tot_size_survey']:0);
	$total_not_interest_talk = ($data_size[$UserId]['tot_size_not_interest_talk']?$data_size[$UserId]['tot_size_not_interest_talk']:0);
	$total_cant_bereach = ($data_size[$UserId]['tot_size_not_reach']?$data_size[$UserId]['tot_size_not_reach']:0);
	$total_not_answer = ($data_size[$UserId]['tot_size_not_answer']?$data_size[$UserId]['tot_size_not_answer']:0);		
	$total_contacted = ($total_survey+$total_not_interest_talk);
	$total_non_contacted = ($total_contacted + ($total_cant_bereach+$total_not_answer));
		
	// ratio on percent 
	$ratio_survey = ($total_survey?(round(($total_survey/( $total_survey + $total_not_interest_talk)),2)*100) : 0);
	$ratio_contact = ($total_contacted?(round(($total_contacted/$total_non_contacted),2)*100):0);
	
	$excel_worksheet->write($excel_rows, 0, $no, $excel_content );
	$excel_worksheet->write($excel_rows, 1, $UserName, $excel_content );
	$excel_worksheet->write($excel_rows, 2,($data_size[$UserId]['tot_data_size']?$data_size[$UserId]['tot_data_size']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 3,($data_size[$UserId]['tot_size_complete']?$data_size[$UserId]['tot_size_complete']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 4,($data_size[$UserId]['tot_size_survey']?$data_size[$UserId]['tot_size_survey']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 5,($data_size[$UserId]['tot_size_not_interest_talk']?$data_size[$UserId]['tot_size_not_interest_talk']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 6,($data_size[$UserId]['tot_size_not_reach']?$data_size[$UserId]['tot_size_not_reach']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 7,($data_size[$UserId]['tot_size_not_answer']?$data_size[$UserId]['tot_size_not_answer']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 8,($data_size[$UserId]['tot_size_busy_tone']?$data_size[$UserId]['tot_size_busy_tone']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 9,($data_size[$UserId]['tot_size_wrong_number']?$data_size[$UserId]['tot_size_wrong_number']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 10,($data_size[$UserId]['tot_size_dead_tone']?$data_size[$UserId]['tot_size_dead_tone']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 11,($data_size[$UserId]['tot_size_leave_message']?$data_size[$UserId]['tot_size_leave_message']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 12,($data_size[$UserId]['tot_size_return_list']?$data_size[$UserId]['tot_size_return_list']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 13,($data_size[$UserId]['tot_size_attempt1']?$data_size[$UserId]['tot_size_attempt1']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 14,($data_size[$UserId]['tot_size_attempt2']?$data_size[$UserId]['tot_size_attempt2']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 15,($data_size[$UserId]['tot_size_attempt3']?$data_size[$UserId]['tot_size_attempt3']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 16,($data_size[$UserId]['tot_size_attempt4']?$data_size[$UserId]['tot_size_attempt4']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 17,($data_size[$UserId]['tot_size_attempt5_plush']?$data_size[$UserId]['tot_size_attempt5_plush']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 18,($data_size[$UserId]['tot_size_callback_letter']?$data_size[$UserId]['tot_size_callback_letter']:0), $excel_number );
	$excel_worksheet->write($excel_rows, 19,"{$ratio_survey}%", $excel_number );
	$excel_worksheet->write($excel_rows, 20,"{$ratio_contact}%", $excel_number );
	
 $excel_rows+=1;
		
 }
		
 
 $excel_workbook->close();

// start download file 

if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	
?>