<?php 
/**
 * @ def : source data HTML Mode 
 * -------------------------------------------------
 */
?> 
<fieldset style="margin:-1px;border:1px solid #dddddd;"><?php
 
 $start_date = $param['start_date'];
 $end_date = $param['end_date'];
 while(true)  
 {
	$estart_date = $start_date;
	__("<div style='padding:4px; width:200px;margin-left:10px; font-weight:bold;'> Date : ".$estart_date."</div>"); 
	__('<table cellspacing=1 cellpadding=2 width="95%" style="margin-left:0px;margin-bottom:20px;">
		<tr height=22 bgcolor="#E25C77" style="color:white;">
			<th class="center" rowspan="2">&nbsp;No.</th>
			<th class="center" rowspan="2">&nbsp;Caller </th>
			<th class="center" colspan="8">&nbsp;Office Time</th>
			<th class="center" rowspan="2">&nbsp;Completed / Hour</th>
		</tr>
		<tr height=22 bgcolor="#E25C77" style="color:white;">
			<th class="center">LOGIN time</th>
			<th class="center">LOGOUT Time</th>
			<th class="center">Attempt Call</th>
			<th class="center">ACD Time</th>
			<th class="center">AUX Time</th>
			<th class="center">Other Time</th>
			<th class="center">Avail Time</th>
			<th class="center">Staffed Time</th>
		</tr>' );
	
 // show data 
 
  $no = 0;
  if(is_array($AgentName))
	foreach( $AgentName as $UserId => $UserName )
  {
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	$no++;
	
/** sort define data by handle **/
	$total_complete 	 =($agent_activity[$estart_date][$UserId]['tot_complete'] ? $agent_activity[$estart_date][$UserId]['tot_complete'] :0);
	$total_attempt  	 =($agent_activity[$estart_date][$UserId]['tot_attempt'] ? $agent_activity[$estart_date][$UserId]['tot_attempt'] :0);
	$total_iddle_time 	 =($agent_activity[$estart_date][$UserId]['tot_idle'] ? $agent_activity[$estart_date][$UserId]['tot_idle'] :0);
	$total_acw_time 	 =($agent_activity[$estart_date][$UserId]['tot_acw'] ? $agent_activity[$estart_date][$UserId]['tot_acw'] :0);
	$total_ready_time 	 =($agent_activity[$estart_date][$UserId]['tot_not_ready'] ? $agent_activity[$estart_date][$UserId]['tot_not_ready'] :0);
	$total_busy_time 	 =($agent_activity[$estart_date][$UserId]['tot_busy'] ? $agent_activity[$estart_date][$UserId]['tot_busy'] :0);
	$total_notready_time =($agent_activity[$estart_date][$UserId]['tot_not_ready'] ? $agent_activity[$estart_date][$UserId]['tot_not_ready'] :0);
	$activity_login 	 =($agent_activity[$estart_date][$UserId]['login'] ? $agent_activity[$estart_date][$UserId]['login'] :'-');
	$activity_logout 	 =($agent_activity[$estart_date][$UserId]['logout'] ? $agent_activity[$estart_date][$UserId]['logout'] :'-');
	$total_activity 	 =($agent_activity[$estart_date][$UserId]['tot_activity'] ? $agent_activity[$estart_date][$UserId]['tot_activity'] :0);
	
	
	
 /** callcualtion source data **/
 
    $avg_call_complete = ( $total_complete ? round( ($total_complete / ($total_activity - $total_notready_time)), 2) : 0);
	
 /** show detail on rows **/
 
	__("<tr height=\"22\" style=\"color:#7e009d;border:1px solid #AAAAAA;background-color:".$color."\">
		<td class=\"content-first\" style=\"text-align:center;\">".$no."</td>
		<td class=\"content-middle left\" nowrap>&nbsp;".$UserName."</td>
		<td class=\"content-middle center\">".$activity_login."</td>
		<td class=\"content-middle center\">".$activity_logout."</td>
		<td class=\"content-middle center\">".$total_attempt ."</td>
		<td class=\"content-middle center\">".( $total_busy_time ? _getDuration($total_busy_time) : '-') ."</td>
		<td class=\"content-middle center\">".( $total_notready_time ? _getDuration($total_notready_time) : '-')."</td>
		<td class=\"content-middle center\">".( $total_acw_time ? _getDuration($total_acw_time): '-') ."</td>
		<td class=\"content-middle center\">".( $total_iddle_time ? _getDuration($total_iddle_time) : '-') ."</td>
		<td class=\"content-middle center\">".( $total_activity ? _getDuration($total_activity): '-' )."</td>
		<td class=\"content-lasted center\">".( $avg_call_complete ? $avg_call_complete : '-' )."</td>
	 </tr>");
   }
   
__('</table>');

 if ( $start_date == $end_date ) break;
	$start_date = _getNextDate($start_date);
}

?> </fieldset> <?php // END OF FILE SOURCE ?>