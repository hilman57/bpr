<?php

/*
 * @ def : source EXCEL 
 *
 * ---------------------------------------------------------------
 */
 
$this->load->helper('EUI_ExcelWorksheet'); // load library excel 
$BASE_EXCEL_PATH_TEMP = APPPATH .'temp';
$BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_TEMP ."/". date('YmdHi') ."_Detail_". str_replace(" ","_", $title) .".xls";

$excel_workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
$excel_worksheet =& $excel_workbook->addworksheet();

// header 
 
 

// set style layout header 

 $excel_title =& $excel_workbook->addformat();
 $excel_title->set_bold();
 $excel_title->set_size(10);
 $excel_title->set_color('white');
 $excel_title->set_align('left');
 $excel_title->set_align('vcenter');
 $excel_title->set_fg_color('red');
 $excel_title->set_border(1);
 $excel_title->set_border_color('blue');
  
// start rows 

 $excel_rows = 0;
 $excel_worksheet->write($excel_rows, 0, "No.", $excel_title );
 $excel_worksheet->write($excel_rows, 1, "Name", $excel_title );
 
 $cols_x = 2;
 if(is_array($CampaignName))
 foreach($CampaignName as $CampaignId => $name) 
 {
	$cols_merge = $cols_x;
	
	$excel_worksheet->write($excel_rows, $cols_merge, str_replace("<br/>"," - ", $name), $excel_title );
	
	$cols_merge = $cols_merge+1;
	for( $merge =1; $merge<=6; $merge++ )
	{
		$excel_worksheet->write($excel_rows, $cols_merge, "", $excel_title );
		$cols_merge+=1;
	}
	$cols_x=$cols_merge;
 }
 
// rows 2 

// format number 

$excel_center=& $excel_workbook->addformat();
$excel_center->set_size(10);
$excel_center->set_align('center');
$excel_center->set_border(1);
$excel_center->set_border_color('blue');
$excel_center->set_color('white');
$excel_center->set_fg_color('red');
 
$arrays_header_contents = array( 'New Data', '1', '2','3', '4', '5', ">=6");

$excel_rows = $excel_rows+1;
$excel_worksheet->write($excel_rows, 0, "", $excel_title );
$excel_worksheet->write($excel_rows, 1, "", $excel_title );

$cols_xh = 2;
foreach( $CampaignName as $k => $d ){ 
	$si = $cols_xh;
	
	for( $i =0; $i<count($arrays_header_contents); $i++ ) {
		$excel_worksheet->write($excel_rows, $si, "{$arrays_header_contents[$i]}", $excel_center );
		$si++;
		
	}
	
   $cols_xh=$si;
}	 

// start content 

 $excel_content=& $excel_workbook->addformat();
 $excel_content->set_size(10);
 $excel_content->set_align('left');
 $excel_content->set_border(1);
 $excel_content->set_border_color('blue');

// format number 

 $excel_number=& $excel_workbook->addformat();
 $excel_number->set_size(10);
 $excel_number->set_align('center');
 $excel_number->set_border(1);
 $excel_number->set_border_color('blue');
 
		
$excel_rows = $excel_rows+1;
$num = 1;
if(is_array($AgentName))
foreach($AgentName as $UserId => $UserName )
{
	$excel_worksheet->write($excel_rows, 0, $num, $excel_content );
	$excel_worksheet->write($excel_rows, 1, $UserName, $excel_content );
	$cols_x2 = 2;
	foreach( $CampaignName as $k => $d )
	{
		for( $i =1; $i<=7; $i++ )
		{
			$excel_worksheet->write($excel_rows, $cols_x2, ( $data_detail[$UserId][$k]["size_attempt$i"] ? 
			$data_detail[$UserId][$k]["size_attempt$i"] : 0), $excel_number );
			$cols_x2+=1;
		}
	}		
	
	$num++;
	$excel_rows +=1;
}
	
// __('<fieldset style="margin:-1px;border:1px solid #dddddd;">');
// __('<input type="button" style="margin-left:10px;" name="detail" class="page-go button" value="Back" onclick="Ext.DOM.ShowReport();">');
// __('<input type="button" style="margin-left:10px;" name="detail" class="excel button" title="excel" value="Export" onclick="Ext.DOM.ProdcuttivityDetailByAttempt(this);">');

// echo "<table cellspacing=1 cellpadding=4 align='center' width='100%'> 
		// <tr height=22 bgcolor='#E25C77' style='color:white;'>
			// <th rowspan='2' class='center'>No.</th> 
			// <th rowspan='2' class='center'>Nama</th> ";
		// foreach($CampaignName as $CampaignId => $name) {
			// echo "<td colspan=7 class='center'>{$name}</th>";
		// }
		// echo "</tr>";
		
		// echo "<tr height=22 bgcolor='#E25C77' style='color:white;'>";
			// foreach( $CampaignName as $k => $d ){
				// for( $i =0; $i<7; $i++ ){
					// echo "<th class='center'>{$arrays_header_contents[$i]}</th>";
				// }
			// }	
		// echo "</tr>";		

//show data agent 
		
// if(is_array($AgentName))
// foreach($AgentName as $UserId => $UserName )
// {
	// $color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	// $no++;
	
	// __("<tr height=\"22\" style=\"color:#7e009d;border:0px solid #AAAAAA;background-color:{$color}\">
		// <td class=\"content-first left\" nowrap>&nbsp;{$no}&nbsp;</td>
		// <td class=\"content-middle left\" nowrap>&nbsp;{$UserName}&nbsp;</td>");
	 // foreach( $CampaignName as $k => $d ){
		// for( $i =1; $i<=7; $i++ ){
			// echo "<td class='content-middle center'>". ( $data_detail[$UserId][$k]["size_attempt$i"] ? 
				// $data_detail[$UserId][$k]["size_attempt$i"] : 0) ."</td>";
		// }
	// }		
	
	// __("</tr>");
		
// }
			
// echo "</table>";
// __("</fieldset>");


 $excel_workbook->close();
if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	

?>