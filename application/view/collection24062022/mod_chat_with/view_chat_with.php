<?php ?>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script> 

<!-- interval set -->
<script>

// @ pack : Ext.document

Ext.document(document).ready(function(){
Ext.Css("windowUser").style({
		"height": (Ext.query(window).height()-50),
		"border":'0px solid #000000',
		"overflow":"auto"
	});
});	

// @ pack : Ext.document

Ext.document(document).resize(function(){

Ext.Css("windowUser").style({
	height: (Ext.query(window).height()-50),
	"border":'0px solid #000000',
	"overflow":"auto"
});

});	
</script>
</head>
<body style='background-color:#FFFFFF;'>
<fieldset class="corner" style="margin-top:10px;">
	<legend class="icon-menulist">&nbsp;&nbsp;Chat With </legend>
	<div id="windowUser">
		<table cellspacing=0 width='99%' border=0 class="custom-grid" align="center">
			<tr>
				<th class="ui-corner-top ui-state-default first center" style="height:24px;border-right:0px;">Caller</th>
				<th class="ui-corner-top ui-state-default middle center" style="height:24px;border-right:0px">Status</th>
				<th class="ui-corner-top ui-state-default lasted center" >Ext Status</th>
			</tr>	
		<?php foreach( $users as $k => $rows ) : 
			$UserTitle = explode(" ",$rows['code_user']);
			// var_dump($rows['code_user']);
			// var_dump($UserTitle);
		?>	
			<tr>
				<td  class="content-first left" style="padding-left:4px;font-weight:bold;">
					<span id="<?php echo trim($rows['Username']);?>_Username">
						<a href="#" style="text-decoration:none;" onclick="window.opener.chatWith(new Array('<?php echo trim($rows['Username']); ?>','<?php __((isset($UserTitle[0])?trim($UserTitle[0]):'anynoumous')); ?>'));"><?php echo $rows['Fullname'];?></a>
					</span>
				</td>
				<td  class="content-middle center" style="padding-left:4px;color:red;">
					<span id="<?php echo $rows['Username'];?>_AgentStatus">
						<?php echo $rows['AgentStatus'];?>
					</span>	
				</td>
				<td  class="content-middle left" style="padding-left:4px;">
					<span id="<?php echo trim($rows['Username']);?>_extStatus">
						<?php echo $rows['User_Lvl'];?>
					</span>
				</td>
			</tr>
		<?php endforeach; ?>	
			
		</table>
	</div>	
</fieldset>
</body>
</html>