<?php
/*
 * @ def : source EXCEL 
 *
 * ---------------------------------------------------------------
 */
 
function IntervalDays( $param ) 
{
	static $interval_date = array(); 
	$start_date = $param['start_date'];
	$end_date = $param['end_date']; 
	while(true) 
	{
	
		$estart_date = $start_date;
		$interval_date[$estart_date] = $estart_date;
		if ( $start_date == $end_date ) break;
			$start_date = _getNextDate($start_date);
	}

	return $interval_date;
}		


// load library 
 
 $this->load->helper('EUI_ExcelWorksheet');   
 $BASE_EXCEL_PATH_TEMP = APPPATH .'temp';
 $BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_TEMP ."/". date('YmdHi')."_Distributed_Report.xls";
 $excel_workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
 $excel_worksheet =& $excel_workbook->addworksheet();
 
// set style layout header 

 $excel_title =& $excel_workbook->addformat();
 $excel_title->set_bold();
 $excel_title->set_size(10);
 $excel_title->set_color('white');
 $excel_title->set_align('left');
 $excel_title->set_align('vcenter');
 $excel_title->set_fg_color('red');
 $excel_title->set_border(1);
 $excel_title->set_border_color('blue');
 
// set style layout header 

 $report_title =& $excel_workbook->addformat();
 $report_title->set_bold();
 $report_title->set_size(12);
 $report_title->set_align('left');
 $report_title->set_align('vcenter');
 $excel_rows = 0;
 
// get interval date 

$intervals = IntervalDays($param);
 
foreach($CampaignName as $key => $name)
{

 $excel_worksheet->write($excel_rows, 0, $name, $report_title);
 
 
// --- set rows 1 --- 
 
 $excel_rows = $excel_rows+1;
 
 $excel_worksheet->write($excel_rows, 0, "No.", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "Caller Name", $excel_title);
 
 $xls_cols_posx = 2;
 foreach( $intervals as $k => $date_headers ) {
	$excel_worksheet->write($excel_rows, $xls_cols_posx, date("d/m/Y", strtotime($date_headers)) , $excel_title);
	$xls_cols_posx+=1;
	$excel_worksheet->write($excel_rows, $xls_cols_posx, "", $excel_title);
	$xls_cols_posx+=1;
 }
 
 $excel_worksheet->write($excel_rows, $xls_cols_posx, "Summary", $excel_title);
 $excel_worksheet->write($excel_rows, $xls_cols_posx+1, "", $excel_title);
 
// --- set rows 2 --- 

 $excel_rows = $excel_rows+1;
 $excel_worksheet->write($excel_rows, 0, "", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "", $excel_title);
 
 $xls_cols_posx = 2;
 foreach( $intervals as $k => $date_headers ) 
 {
	$excel_worksheet->write($excel_rows, $xls_cols_posx,"Total Size", $excel_title);
	$xls_cols_posx+=1;
	$excel_worksheet->write($excel_rows, $xls_cols_posx,"", $excel_title);
	$xls_cols_posx+=1;
 }
 
 $excel_worksheet->write($excel_rows, $xls_cols_posx, "", $excel_title);
 $excel_worksheet->write($excel_rows, $xls_cols_posx+1, "", $excel_title); 
 
// --- set rows 3 --- 

 $excel_rows = $excel_rows+1;
 $excel_worksheet->write($excel_rows, 0, "", $excel_title);
 $excel_worksheet->write($excel_rows, 1, "", $excel_title);
 
 $xls_cols_posx = 2;
 foreach( $intervals as $k => $date_headers ) 
 {
	$excel_worksheet->write($excel_rows, $xls_cols_posx,"Customer", $excel_title);
	$xls_cols_posx+=1;
	$excel_worksheet->write($excel_rows, $xls_cols_posx,"Policy", $excel_title);
	$xls_cols_posx+=1;
 }
 
 $excel_worksheet->write($excel_rows, $xls_cols_posx, "Customer", $excel_title);
 $excel_worksheet->write($excel_rows, ($xls_cols_posx+1), "Policy", $excel_title);
     



/** sort define data by handle **/
	
 $excel_content=& $excel_workbook->addformat();
 $excel_content->set_size(10);
 $excel_content->set_align('left');
 $excel_content->set_border(1);
 $excel_content->set_border_color('blue');
 
/** sort define data by handle **/
	
 $excel_number=& $excel_workbook->addformat();
 $excel_number->set_size(10);
 $excel_number->set_align('right');
 $excel_number->set_border(1);
 $excel_number->set_border_color('blue');

 $no = 0; $excel_rows = $excel_rows+1;
 if(is_array($AgentName)) 
 foreach($AgentName as $UserId => $full_name)
{ 
	$no++;
	
	$excel_worksheet->write($excel_rows, 0, $no, $excel_content);
	$excel_worksheet->write($excel_rows, 1, $full_name, $excel_content);
  
  // show by date 
  
	$tots_value_array = NULL;
	$tots_value_array = NULL;
	$total_data_customer=0;
	$total_data_policy= 0;	

	// start cols 
	
	$xls_cols_posx = 2;
	
	if( is_array($intervals) )
		foreach( $intervals as $k => $estart_date ) 
   {
		$tots_value_array = $datas[$key][$estart_date][$UserId];
		$tots_polis_array = $policy[$key][$estart_date][$UserId];
		
		$excel_worksheet->write($excel_rows, $xls_cols_posx, ( $tots_value_array ? $tots_value_array : "" ), $excel_number);
		$xls_cols_posx+=1;
		$excel_worksheet->write($excel_rows, $xls_cols_posx, ( $tots_polis_array ? $tots_polis_array : "" ), $excel_number);
		$xls_cols_posx+=1;
		
		$total_data_customer += (INT)$tots_value_array;
		$total_data_policy += (INT)$tots_polis_array;
	 }
	 
	 $excel_worksheet->write($excel_rows, $xls_cols_posx, $total_data_customer, $excel_number);
	 $excel_worksheet->write($excel_rows, $xls_cols_posx+1, $total_data_policy, $excel_number);
	 
	 $excel_rows+=1;
	
}	 
  $excel_rows = ($excel_rows+2);
  
}

$excel_workbook->close();

// start download file 

if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	 

?>

