<?php 
$status = array('NEW','WAITING','SENDING','ERROR','FAILED');

foreach($mail_list_queue as $no => $rows )
{ 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	$num = ($no+1); 
?>
<tr class="onselect" bgcolor="<?php __($color);?>">
	<td nowrap class="content-first" ><?php echo $num; ?></td>
	
	<td nowrap class="content-middle left"><?php __($rows['EmailSubject']);?></td>
	<td nowrap class="content-middle center"><?php __($rows['EmailStatusName']);?></td>
	<td nowrap class="content-middle left"><?php __(($rows['QueueReason']?$rows['QueueReason']:'-'));?></td>
	<td nowrap class="content-middle center"><?php __($rows['QueueCreateTs']);?></td>
	<td nowrap class="content-middle center"><?php __($rows['QueueStatusTs']);?></td>
	<td nowrap class="content-middle center"><?php __(_getDuration($rows['MailDuration']));?></td>
	<td nowrap class="content-lasted center" width='10%'><?php
	if($rows['QueueTrying'] > $config['outbox.retry']) {
		__('<input type="button" name="Cancel" class="close button"  id="'.$rows['QueueId'].'" value="Cancel" onClick="Ext.DOM.Cancel(this);">');
		__('&nbsp; <input type="button" name="retry"  id="'.$rows['QueueId'].'" class="assign button"  value="Resend" onClick="Ext.DOM.Try(this);">');
	}
	else{
		__($rows['QueueTrying']);
	}		
	
	?>
	</td>
</tr>
<?php } ?>	