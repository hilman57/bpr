<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason 			= [];
Ext.DOM.datas 			= {};
Ext.DOM.handling 		= "<?php echo _get_session('HandlingType'); ?>";
Ext.EQuery.TotalPage 	= "<?php echo $page->_get_total_page(); ?>";
Ext.EQuery.TotalRecord 	= "<?php echo $page->_get_total_record(); ?>";
Ext.EQuery.Summary 		= "<?php echo _getCurrency($AmountSummary); ?>";

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	keys_account_status : "<?php echo _get_exist_session('keys_account_status');?>",
	keys_agent_id 		: "<?php echo _get_exist_session('keys_agent_id');?>",
	keys_call_status 	: "<?php echo _get_exist_session('keys_call_status');?>",
	keys_campaign_id 	: "<?php echo _get_exist_session('keys_campaign_id');?>",
	keys_cust_id 		: "<?php echo _get_exist_session('keys_cust_id');?>",
	keys_cust_name 		: "<?php echo _get_exist_session('keys_cust_name');?>",
	keys_recsource 		: "<?php echo _get_exist_session('keys_recsource');?>",
	keys_start_amountwo : "<?php echo _get_exist_session('keys_start_amountwo');?>",
 	keys_end_amountwo 	: "<?php echo _get_exist_session('keys_end_amountwo');?>",
	keys_start_date 	: "<?php echo _get_exist_session('keys_start_date');?>",
	keys_end_date 		: "<?php echo _get_exist_session('keys_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/SrcCustomerList/index/',
	custlist : Ext.DOM.INDEX+'/SrcCustomerList/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('flwCustomers').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('flwCustomers').Clear();
	Ext.DOM.searchCustomer();
}

// @ cek data if set followup  = deb_is_call = 0 

Ext.DOM.IsCall = function( CustomerId ){
  return( 
	Ext.Ajax({
		url    : Ext.DOM.INDEX +'/ModContactDetail/IsCall/',
		method : 'GET',
		param  : {
			CustomerId : CustomerId
		}
	}).json()
  );
}
		
// @ pack : On ENTER Keyboard 

 Ext.DOM.CallCustomer = function( CustomerId ) 
{
 if( CustomerId!='')  {	
	var IsCall = Ext.DOM.IsCall(CustomerId);
	
	// if( IsCall.success == 0 ){
		// Ext.Msg("\n\rSorry The data being in the follow by other users.\n\rPlease select other customer!").Info();
		// return false;
	// }
	
	Ext.ActiveMenu().NotActive();
	Ext.ShowMenu( new Array('ModContactDetail','index'), 
		Ext.System.view_file_name(), 
	{
		CustomerId : CustomerId,
		ControllerId : Ext.DOM.navigation.custnav
	}); 
	
	// Ext.EQuery.Ajax ({
		// url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		// method  : 'GET',
		// param 	: {
			// CustomerId : CustomerId,
			// ControllerId : Ext.DOM.navigation.custnav
		// }
	// });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}

Ext.DOM.KeepData = function() {
	var customerid = Ext.Cmp('CustomerId').getValue();
	Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SrcCustomerList/KeeptDataByUser',
			method 	: 'POST',
			param 	: { CustomerId : customerid,
						IsKeep : 1
					},
			ERROR	: function(e) {
				var ERROR = JSON.parse(e.target.responseText);
				if( ERROR.Message ) {
					alert('Success('+ERROR.success+') Fail('+ERROR.fail+') Keep Debitur');
					Ext.EQuery.construct(navigation,'')
					Ext.EQuery.postContent();
				}
			}	
	}).post();
	
}

// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Keep Data']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['KeepData']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['page_add.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="flwCustomers">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('keys_campaign_id','select auto', $CampaignId, _get_exist_session('keys_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('keys_agent_id','select auto', $Deskoll, _get_exist_session('keys_agent_id'));?></td>
			<td class="text_caption bottom"> # Tanggal Panggilan Terakhir &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('keys_start_date','input_text date', _get_exist_session('keys_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('keys_end_date','input_text date', _get_exist_session('keys_end_date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # ID Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('keys_cust_id','input_text long', _get_exist_session('keys_cust_id'));?></td>
			<td class="text_caption bottom"> # Status Akun &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('keys_account_status','select auto', $AccountStatus, _get_exist_session('keys_account_status'));?></td>
			<td class="text_caption bottom"> # Recsource &nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('keys_recsource','input_text long', _get_exist_session('keys_recsource'));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Nama Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('keys_cust_name','input_text long', _get_exist_session('keys_cust_name'));?></td>
			<td class="text_caption bottom"> # Status Panggilan Terakhir &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('keys_call_status','select auto', $LastCallStatus, _get_exist_session('keys_call_status'));?></td>
			<td class="text_caption bottom"> # Jumlah WO&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('keys_start_amountwo','input_text box', _get_exist_session('keys_start_amountwo') );?>
				&nbsp; <span>to</span>&nbsp;	
				<?php echo form()->input('keys_end_amountwo','input_text box', _get_exist_session('keys_end_amountwo') );?>
			</td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars" style="margin:5px;"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop   content -->