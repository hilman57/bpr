<?php
$start_date = $this->URI->_get_post('start_date_claim');
$end_date = $this->URI->_get_post('end_date_claim');
$header = array(
	'Date'              => 'Date',
	'OpenVCT'           => 'Opening',
	'BalanceOpenVCT'    => 'Balance Opening',
	'NewVCT2'            => 'New',
	'BalanceNewVCT2'     => 'Balance New',
	'ExitVCT'           => 'Exit',
	'BalanceExitVCT'    => 'Balance Exit',
	'ClosingVCT'        => 'Closing',
	'BalanceClosingVCT' => 'Balance Closing'
);

$numberingformat = array('BalanceOpenVCT', 'BalanceNewVCT', 'BalanceExitVCT', 'BalanceClosingVCT');

?>
<!DOCTYPE HTML>
<html>

<head>
	<title> <?php echo ucfirst(base_layout()); ?> :: <?php echo $title; ?> </title>
</head>

<body style="margin:0px;">
	<!-- data1 -->
	<?php
	echo "<H2> " . $title . " </>";
	echo "<br>";
	echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	echo '<tr>';
	foreach ($header as $column => $name) {
		echo '<th style="font-family:Arial;font-size:12px;padding:8px;border:1px solid #FFCCCC;background-color:#eee;">' . $name . ' </th>';
	}
	echo '</tr>';

	$no = 1;
	foreach ($data_tabel as $rows => $datarow) {
		foreach ($datarow as $row => $content) {
			echo '<tr>';
			foreach ($header as $column => $name) {
				if ($column == "no") {
					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $no . '</td>';
					$no++;
				} else {
					$number = 0;
					if (in_array($column, $numberingformat)) {
						$number = number_format($content[$column], 2, ",", ".");
					} else {
						$number = ($content[$column] ? $content[$column] : 0);
					}

					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $number . '</td>';
				}
			}
			echo '</tr>';
		}
	}
	echo '</table>';
	// echo "<pre>";
	// print_r($data_tabel);
	// echo "</pre>";
	?>
	<!-- data1 -->


	<!-- data2 -->
	<?php
	// echo "<H2> " . $title . " </>";
	// echo "<br>";
	// echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	// echo '<tr>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Debitur ID</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Acc No</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">NAMA</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">tgl_new_pvc</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Call status</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">deb_bal_afterpay</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">amount</td>
	// <td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">recsource</td>
	// </tr> ';

	// foreach ($data_tabel2 as $column => $name) {
	// 	echo "<tr>";
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['deb_id'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">`' . $name['deb_acct_no'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['deb_name'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['tgl_new_pvc'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['tanya_bu_mini'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['deb_bal_afterpay'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['deb_wo_amount'] . '</td>';
	// 	echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $name['deb_resource'] . '</td>';
	// 	echo '</tr>';
	// }
	// // }
	// echo '</table><br>';
	// echo "<pre>";
	// print_r($name);
	// echo "</pre>";
	?>
	<!-- data2 -->

	<!-- data3 -->
	<?php
	$arrDate = array();
	foreach ($data_tabel as $row_dt) {
		array_push($arrDate, $row_dt[0]['Date']);
	}
	if (is_array($openVCT)) {
		foreach ($openVCT as $key => $item) {
			if (is_array($item)) {
				if (in_array($key, $arrDate)) {
					echo "<H2> OPENING VCT BULAN " . $start_date;
					echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
					echo '<tr>

					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Acc No</td>
					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Nama</td>
					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Tanggal New VCT</td>
					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Balance</td>
					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Call status</td>
					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Resource</td>
					<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Category</td>
					
					</tr> ';
					foreach ($item as $row) {
						echo "<tr class=xl582508 style='mso-height-source:userset;height:27.0pt'>";
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;mso-number-format:\@;">' . $row['deb_acct_no'] . '</td>';
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $row['deb_name']  . '</td>';
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $row['tgl_new_pvc'] . '</td>';
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $row['deb_wo_amount'] . '</td>';
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $row['CallReasonDesc'] . '</td>';
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $row['deb_resource'] . '</td>';		
						echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'	.$row['VCReasonCode'].'</td>';
						echo '</tr>';
					}
					echo '</table><br>';
				}
			}
		}
	}
	?>
	<!-- data3 -->


	<!-- data4 -->
	<?php
	echo "<H2> NEW  VCT BULAN " . $start_date;
	echo "<br>";
	echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	echo '<tr>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Acc No</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Nama</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Tanggal New VCT</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Balance</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Call status</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Resource</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Category</td>
	</tr> ';
	// echo "<pre>";
	// var_dump($data_tabel);



	$nilai = (string)$NewVCT1['deb_acct_no'];
	foreach ($NewVCT as $column => $NewVCT1) {
		// echo "<pre>";
		// var_dump($NewVCT);
		echo "<tr class=xl582508 style='mso-height-source:userset;height:27.0pt'>";
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;mso-number-format:\@;">' . $NewVCT1['deb_acct_no']  . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewVCT1['deb_name'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewVCT1['tgl_new_pvc'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewVCT1['deb_wo_amount'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewVCT1['CallReasonDesc'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewVCT1['deb_resource'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'.$NewVCT1['VCReasonCode'].'</td>';
		echo '</tr>';
	}
	// }
	echo '</table><br>';
	// echo "<pre>";
	// print_r($data_tabel);
	// echo "</pre>";
	?>
	<!-- data4 -->

	<!-- data5 -->
	<?php
	echo "<H2> EXIT  VCT BULAN " . $start_date;
	echo "<br>";
	echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	echo '<tr>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Acc No</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Nama</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Tanggal New VCT</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Balance</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Call status</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Resource</td>
	<td style=\"font-family:Arial;font-size:15px;padding:8px;border:1px solid #FFCCCC;color:#eee;\">Category</td>
	</tr> ';
	// echo "<pre>";
	// var_dump($data_tabel);



	$nilai = (string)$NewExit1['deb_acct_no'];
	foreach ($NewExit as $column => $NewExit1) {
		echo "<tr class=xl582508 style='mso-height-source:userset;height:27.0pt'>";
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;mso-number-format:\@;">' . $NewExit1['deb_acct_no']  . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewExit1['deb_name'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewExit1['tgl_new_pvc'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewExit1['deb_wo_amount'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewExit1['CallReasonDesc'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewExit1['deb_resource'] . '</td>';
		echo '<td style="font-family:Arial;font-size:15px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">' . $NewExit1['VCReasonCode'] . '</td>';
		echo '</tr>';
	}
	// }
	echo '</table><br>';
	// echo "<pre>";
	// print_r($data_tabel);
	// echo "</pre>";
	?>
	<!-- data5 -->



</body>

</html>