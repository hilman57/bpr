<?php 
/*
 * @ pack : payment history history 
 */
 
$objPH = (object)null;
if( class_exists('M_PaymentHistory') ) 
{
  $objPH =&M_PaymentHistory::Instance();
}

$listPH = $objPH->_get_payment_detail($Customer['deb_id']);
$Month = $objPH->_get_select_month();

 
/*
 * @ pack : create function return x 
 */
 
if(!function_exists('_class_exist'))
{
  function _class_exist($Year=NULL, $month=0 ) 
 {
   $conds = 0;
   $arrYears = explode('-', $Year);
   if( is_array($arrYears) ){
	  if( $arrYears[1] == $month ){
		  $conds++;
	  }
   }
   
   return $conds;   
 }
} 

/*
 * @ pack : create div layout 
 */
if(!function_exists('_class_html'))
{
 function _class_html( $conds )
{
	$array_html = array(
	  1 => "<div class='assign' style='color:green;'>Pay</div>",	
	  0 => "<div class='close' style='color:red;'>No Pay</div>"
	);
	 
	return  $array_html[$conds];
 }
 
} 
 
 
?>

<fieldset class='corner' style='margin-top:0px;margin-bottom:0px;'>
<legend class='icon-customers' id='payment-history-detail'>&nbsp;&nbsp;Payment History</legend>
<div id="content-payment-history-detail">	
<span style='margin-left:12px;color:red;'> Date Entry : <?php echo $objPH->_get_class_entry($Customer['deb_open_date']);?></span>
 <table border=0 cellspacing=1 width='99%'>
 <?php foreach( $listPH as $key => $years ) : ?>
	<tr height=28>
	<th rowspan=2 class='ui-corner-top ui-state-default'><?php echo substr($years,0,4); ?></td>
	<?php foreach( $Month as $i => $n ) : ?>
		<th class='ui-corner-top ui-state-default'><?php echo substr(NameMonth($n),0,3); ?></th>
	<?php endforeach; ?>
 	</tr>
	<tr height=24 bgcolor='#FFFEEE' class='onselect'>
	<?php foreach( $Month as $i => $n ) : ?>
		<td align='center' class='content-middle'><?php echo _class_html(_class_exist( $years , $n )); ?></td>
	<?php endforeach; ?>
	</tr>
 <?php $i++; ?>
 <?php endforeach; ?>
	
		
 </table>	
</div>
</fieldset>