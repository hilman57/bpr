<?php echo javascript(); ?>
<script type="text/javascript">
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 Ext.DOM.onload= (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })()
 	
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.datas =  
{
	keyword	 : "<?php echo $this -> URI -> _get_post('keyword');?>",
	order_by : "<?php echo $this -> URI -> _get_post('order_by');?>",
	type	 : "<?php echo $this -> URI -> _get_post('type');?>",
	param	 : "<?php echo $this -> URI -> _get_post('param');?>",
}

	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
$(function(){
	$('#toolbars').extToolbars
	({
		extUrl  : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle:[['Enable'],['Disable'],['Add Menu'],['Remove'],['Search']],
		extMenu :[['EnableWorkApplicationMenu'],['DisableWorkApplicationMenu'],['AddWorkMenu'],['RemoveWorkMenu'],['FindWorkMenu']],
		extIcon :[['accept.png'],['cancel.png'],['add.png'],['cross.png'],['zoom.png']],
		extText :true,
		extInput:true,
		extOption:[{
			render	: 4,
			type	: 'text',
			id		: 'keyword', 	
			name	: 'keyword',
			value	: Ext.DOM.datas.keyword,
			width	: 120
		}]
	});
});
	
var load_images_id = Ext.Cmp('load_images_id');

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 	
 Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
 Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';


/**
 ** javscript prototype system
 ** version v.0.1
 **/
 var _content_page = {
	custnav  : Ext.DOM.INDEX+'/MenuWorkProject/index',
	custlist : Ext.DOM.INDEX+'/MenuWorkProject/Content'			
 }	
	
Ext.EQuery.construct(_content_page, Ext.DOM.datas);
Ext.EQuery.postContentList();

/** FIND WORK MENU NAVIGATION **/

var FindWorkMenu = function(){
	Ext.EQuery.construct( _content_page, { keyword : Ext.Cmp('keyword').getValue()  });
	Ext.EQuery.postContent();
} 	

/** ADD TO LIST OF MENU NAVIGATION **/

var AddWorkMenu = function() {
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+'/MenuWorkProject/AddMenuWorkProject/',
		method 	: 'GET',
		param 	: {
			time : Ext.Date().getDuration()
		}
	}).load("panel-content");
} 

/** ADD TO LIST OF MENU NAVIGATION **/

var ApplicationMenuByProject = function( ApplicationMenuByProject )
{
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+'/MenuWorkProject/ApplicationMenuByProject/',
		method 	: 'GET',
		param 	: {
			time : Ext.Date().getDuration(),
			WorkProjectId : ApplicationMenuByProject.value
		}
	}).load("ApplicationWorkMenu")
}

/** ADD TO LIST OF MENU NAVIGATION **/

var RemoveMenuWorkProjectById = function()
{
	var MenuWorkProjectId = Ext.Cmp('MenuWorkProjectId').getChecked();
	var WorkProjectId = Ext.Cmp('WorkProjectId').getValue();
	
	if( MenuWorkProjectId.length ==0 ){
		Ext.Msg('Please select * Available !').Info(); }
	else if( WorkProjectId=='' ){
		Ext.Msg('Please select Project Name!').Info(); }
	else
	{
		Ext.Ajax({ 
			url 	: Ext.DOM.INDEX +'/MenuWorkProject/DeleteApplicationMenuByProject/',
			method 	: 'POST',
			param 	: {
				MenuId : MenuWorkProjectId,
				ProjectId : WorkProjectId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg('Remove Menu Work Project').Success();
						ApplicationMenuByProject( Ext.Cmp('WorkProjectId').getElementId() );
					}
					else{
						Ext.Msg('Remove Menu Work Project').Failed();
					}
				})
			}
		}).post();
		
	}
}
/** ADD TO LIST OF MENU NAVIGATION **/

var RemoveWorkMenu = function()
{
	var WorkMenuId = Ext.Cmp('WorkMenuId').getChecked();
	if( WorkMenuId.length ==0 ){
		Ext.Msg('Please select a rows!').Info(); }
	else
	{
		Ext.Ajax({ 
			url 	: Ext.DOM.INDEX +'/MenuWorkProject/RemoveApplicationMenuById/',
			method 	: 'POST',
			param 	: {
				WorkMenuId : WorkMenuId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg('Remove Menu Work Project').Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg('Remove Menu Work Project').Failed();
					}
				})
			}
		}).post();
		
	}
}

/** ADD TO LIST OF MENU NAVIGATION **/

var AssignMenuWorkProjectById = function()
{
	var MenuWorkProjectId = Ext.Cmp('ApplicationMenu').getChecked();
	var WorkProjectId = Ext.Cmp('WorkProjectId').getValue();
	
	if( MenuWorkProjectId.length ==0 ){
		Ext.Msg('Please select Application Menu !').Info(); }
	else if( WorkProjectId=='' ){
		Ext.Msg('Please select Project Name!').Info(); }
	else
	{
		Ext.Ajax({ 
			url 	: Ext.DOM.INDEX +'/MenuWorkProject/AssignMenuWorkProjectById/',
			method 	: 'POST',
			param 	: {
				MenuId : MenuWorkProjectId,
				ProjectId : WorkProjectId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg('Assign Menu Work Project').Success();
						ApplicationMenuByProject( Ext.Cmp('WorkProjectId').getElementId() );
					}
					else{
						Ext.Msg('Assign Menu Work Project').Failed();
					}
				})
			}
		}).post();
	}	
}

/** ADD TO LIST OF MENU NAVIGATION **/

var EnableWorkApplicationMenu = function()
{
	var WorkMenuId = Ext.Cmp('WorkMenuId').getChecked();
	if( WorkMenuId.length ==0 ){
		Ext.Msg('Please select a rows!').Info(); }
	else
	{
		Ext.Ajax({ 
			url 	: Ext.DOM.INDEX +'/MenuWorkProject/ActiveWorkApplicationMenu/',
			method 	: 'POST',
			param 	: {
				WorkMenuId : WorkMenuId,
				Activate : 1
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg('Enable Menu Work Project').Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg('Enable Menu Work Project').Failed();
					}
				})
			}
		}).post();
		
	}
}

/** ADD TO LIST OF MENU NAVIGATION **/

var DisableWorkApplicationMenu = function()
{
	var WorkMenuId = Ext.Cmp('WorkMenuId').getChecked();
	if( WorkMenuId.length ==0 ){
		Ext.Msg('Please select a rows!').Info(); }
	else
	{
		Ext.Ajax({ 
			url 	: Ext.DOM.INDEX +'/MenuWorkProject/ActiveWorkApplicationMenu/',
			method 	: 'POST',
			param 	: {
				WorkMenuId : WorkMenuId,
				Activate : 0
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg('Disable Menu Work Project').Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg('Disable Menu Work Project').Failed();
					}
				})
			}
		}).post();
		
	}
}

/** STOP:: HERE **/

 
</script>
	<fieldset class="corner" style="background-color:white;">
	<legend class="icon-userapplication">&nbsp;&nbsp;
		<span id="legend_title"></span>
	</legend>
		<div id="toolbars" class="toolbars" style='margin:12px 12px 0px 12px;'></div>
		<div id="panel-content" ></div>
		<div class="content_table"></div>
		<div id="pager"></div>
		<div id="UserTpl"></div>
	</fieldset>	
	
<!-- END OF FILE  -->
<!-- location : // ../application/layout/view_user_nav/welcome.php -->
	