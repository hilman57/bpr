<fieldset class="corner" style="width:400px;">
	<div>
		<table>
			<tr>
				<td class='font-standars text_caption bottom'>Campaign Name </td>
				<td><?php __(form()->combo('CampaignCode','select long', $CampaignId));?></td>
			</tr>
			<tr>
				<td class='font-standars text_caption bottom'>Header Name </td>
				<td><?php __(form()->input('Field_Header','input_text long'));?></td>
			</tr>	
			
			<tr>
				<td class='font-standars text_caption bottom'>Columns Size</td>
				<td><?php __(form()->input('Field_Columns','input_text long', 2));?></td>
			</tr>
			
			<tr>
				<td class='font-standars text_caption bottom'>Status</td>
				<td><?php __(form()->combo('Field_Active','select long',$Status,1));?></td>
			</tr>
			
			<tr>
				<td class='font-standars text_caption bottom'>Field Start</td>
				<td><?php __(form()->combo('Field_start','select long',$view_field_size, null,array('change'=>'Ext.DOM.ViewLayoutGenerate(this.value);') ));?></td>
			</tr>
			
			
			<tr>
				<td class='font-standars text_caption bottom'>Field Start</td>
				<td><?php __(form()->combo('Field_end','select long',$view_field_size, null,array('change'=>'Ext.DOM.ViewLayoutGenerate(this.value);') ));?></td>
			</tr>
			
			
		</table>
	</div>
</fieldset>

<!-- load handler --> 

<fieldset class='corner' style='margin-top:5px;width:400px;'>
	<form name='frmGenerator'>
	<div id="field_generator" style='float:left;'> </div>
	</form>
</fieldset>	
