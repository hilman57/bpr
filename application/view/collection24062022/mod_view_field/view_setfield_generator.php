<?php 
/** get order rows ****/

$Order = array();
$ord = 1;
if( $view_field_start ) 
for( $ux = $view_field_start; $ux<=$view_field_end; $ux++  ) {
	$Order[$ord] = $ord;
	$ord++;
}

?>

<table>
	<tr>
		<td class='font-standars ui-corner-top ui-state-default first center'>Field</td>
		<td class='font-standars ui-corner-top ui-state-default first center'>Label</td>
		<td class='font-standars ui-corner-top ui-state-default first center'>Order</td>
	</tr>	
	
	<?php 
	$i = 1;
	if( $view_field_start ) for( $c = $view_field_start; $c<=$view_field_end; $c++ ) : ?>
		<tr>
			<td class='font-standars ui-corner-top ui-state-default bottom right'><?php __($fields[$c]['label']); ?></td>
			<td><?php __(form()->input($fields[$c]['name'],'input_text long', $fields[$c]['label']));?></td>
			<td><?php __(form()->combo("Orders_{$c}",'input_text box',$Order, $i));?></td>
		</tr>	
	<?php 
		$i++;
	endfor; ?>	
		<tr>
			<td>&nbsp;</td>
			<td colspan='2' class='right'>
				<?php __(form()->button("button_save",'save button', 'Save', array("click" => "Ext.DOM.SaveGenerateField();") ));?>
				<?php __(form()->button("button_cancel",'close button', 'Cancel', array("click" => "Ext.Cmp('panel-layout-field').setText('')") ));?>
			</td>
		</tr>	
	</table>