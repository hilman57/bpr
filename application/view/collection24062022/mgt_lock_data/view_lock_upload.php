<script type="text/javascript">
$(document).ready(function(){
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
</script>

<div style="margin:5px;">
<table cellspacing=1 width='50%' cellpadding=3>
	<tr >
		<td class='text_caption bottom' nowrap> * Upload File &nbsp;:</td>
		<td class='bottom'><?php echo form()->upload('fileToupload', 'input_text long'); ?></td>
	</tr>
	<tr >
		<td class='text_caption bottom' nowrap> * Template &nbsp;:</td>
		<td class='bottom'><?php echo form()->combo('lock_user_template','select long', $template);?></td>
	</tr>
	<tr >
		<td class='text_caption bottom' nowrap> * Product &nbsp;:</td>
		<td class='bottom'><?php echo form()->combo('lock_user_product','select long', $product);?></td>
	</tr>
	
	<tr>
		<td class='text_caption bottom' nowrap> * Reset Date &nbsp;:</td>
		<td class='bottom'>
			<?php echo form()->input('lock_start_date','input_text date');?>&nbsp;-&nbsp;
			<?php echo form()->input('lock_end_date','input_text date');?>
		</td>
	</tr>
	
</table>
</div>
		