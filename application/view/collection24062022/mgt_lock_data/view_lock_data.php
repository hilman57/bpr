<?php echo javascript(); ?>

<script type="text/javascript">

Ext.DOM.AccountLockPage = function( page ){
  Ext.Ajax 
 ({
	 url    : Ext.DOM.INDEX+"/MgtLockData/AccountLockPage/",
	 method : 'POST',
	 param  : {
		page : ( typeof(page)=='undefined' ? 0 : page )
	}
 }).load("page-content-lock");
}
 
 
// @ pack : user level leader 

Ext.DOM.UserLevelLeader =function( PrivilegeId, input ){
  Ext.Ajax
 ({
	url    : Ext.DOM.INDEX+"/MgtLockData/getUserLeader/",
	method : 'POST',
	param  : {
		PrivilegeId : PrivilegeId,
		input : input
	}	
 }).load("user-level-data2");
 
} 

// --------------------------------------
 function SetAllEventLeader( name,obj  )
{
  Ext.Cmp(name).setChecked();
 var LeaderId = Ext.Cmp(name).getValue();
  Ext.DOM.UserLevelAgent 
 ({
	PrivilegeId : '4,6',
	LeaderId : LeaderId,
	input : 'listCombo'
 });
 
}

// @ pack : user level leader 

Ext.DOM.UserLevelAgent =function( data ){

if( typeof(data)!='object'){
	var data  =  { PrivilegeId : 0, LeaderId : 0, input : 'combo' }
}

 Ext.Ajax ({
	url    : Ext.DOM.INDEX+"/MgtLockData/getUserAgent/",
	method : 'POST',
	param  : data,
 }).load("user-level-data3");
 
  $('#div_lock_user_level3').css({ width : ($('#div_lock_user_level3').width()+60) });
  $('#user-level-data3').css({ width : ($('#div_lock_user_level3').width()+40) });
  
   // $('#field_lock_account_status').css({ height : ($('#panel-content-user').height()-110) });	
   // $('#div_lock_account_status').css({ height : ($('#field_lock_account_status').height()-10) });	
}

// @ pack : user level leader 

 Ext.DOM.SetSelectedStatus = function(object)
{
  Ext.Ajax
 ({
	url    : Ext.DOM.INDEX+"/MgtLockData/getUserStatusLock/",
	method : 'POST',
	param  : {
		UserId : ( object.checked ? object.value : 0 ),
		module : Ext.Cmp('lock_modul_type').getValue(),
	},
	
 }).load("user-level-status");
 $('#field_lock_account_status').css({ height : ($('#fieldset-panel-lock1').height()-170) });	
 $('#div_lock_account_status').css({ height : ($('#field_lock_account_status').height()-10) });	
 
 Ext.DOM.SelectedAmount();
 
}

/* @ pack : LockDataByStatus # --------------- **/

 Ext.DOM.LockDataByStatus = function()
{
  var CallAccountStatus = Ext.Cmp('lock_account_status').getChecked();
  var UserDeskoll = Ext.Cmp('lock_user_level3').getChecked();
  var LockModulType = Ext.Cmp('lock_modul_type').getValue();
  
/* @ pack : LockModulType # --------------- **/

 if( Ext.Cmp('lock_modul_type').empty() ){
	Ext.Msg('Please select Lock Modul').Info();
	return false;
 }
 
/* @ pack : LockModulType # --------------- **/

 if( UserDeskoll.length==0 ){
	Ext.Msg('Please select user').Info();
	return false;
 }
 
/* @ pack : LockModulType # --------------- **/
 
  Ext.Ajax 
 ({
	url    : Ext.DOM.INDEX+"/MgtLockData/SetLockDataByStatus/",
	method : 'POST',
	param  :  {
		LockModulType 		: LockModulType,
		CallAccountStatus 	: CallAccountStatus,
		UserDeskoll 		: UserDeskoll
	},
	ERROR : function( e ){
		Ext.Util(e).proc(function(response){
			var msg = "Lock data by status"; 
			if( response.success ){ 
				Ext.Msg(msg).Success(); } 
			else {
				Ext.Msg(msg).Failed(); }
		});
	}
 }).post();
 	
}


/* @ pack : LockDataByStatus # --------------- **/

 Ext.DOM.Upload = function()
{
 
 
	var template = Ext.Cmp('lock_user_template').getValue();
	if (template=='')
	{
		alert("Please Choose Template");
		return false;
	}
	Ext.Cmp("loading-image").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
/* @ pack : LockModulType # --------------- **/
  Ext.Ajax 
 ({
	url    : Ext.DOM.INDEX+"/MgtBucket/UploadBucket/",
	method : 'POST',
	file   : 'fileToupload',
	param  :  {
		TemplateId    : template,
		CampaignId    : Ext.Cmp('lock_user_product').getValue(),
		ExpiredDate   : Ext.Cmp('lock_end_date').getValue(),
		ExpiredHour   : Ext.Cmp('lock_end_hour').getValue(),
		ExpiredMinute : Ext.Cmp('lock_end_min').getValue(),
		ExpiredSecond : Ext.Cmp('lock_end_sec').getValue(),
		StartDate     : Ext.Cmp('lock_start_date').getValue(),
		StartHour     : Ext.Cmp('lock_start_hour').getValue(),
		StartMinute   : Ext.Cmp('lock_start_min').getValue(),
		StartSecond   : Ext.Cmp('lock_start_sec').getValue(),
		auto_start    : Ext.Cmp('auto_start').getValue()
		
	},
	complete : function(fn){
			var ERR = eval(fn.target.DONE);
			try {	
				if( ERR )
				{
					var CALLBACK = JSON.parse(fn.target.responseText);
					if( (typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null) ){
						var msg = "\n";
						for(var i in  CALLBACK.mesages ){
							msg += "Failed : "+ CALLBACK.mesages[i].N +",\nSuccess : "+CALLBACK.mesages[i].Y +"\n Blacklist : "+ CALLBACK.mesages[i].B +",\nDuplicate : "+  CALLBACK.mesages[i].D  +"\nDuplicate(s) : "+CALLBACK.mesages[i].X+"\n";
						}	
						Ext.Msg(msg).Error();
						Ext.DOM.AccountLockPage(0)
					}
					else{
						Ext.Msg(CALLBACK.mesages).Info();
						Ext.DOM.AccountLockPage(0)
					}	
					
					Ext.Cmp("loading-image").setText('');
				}	
			}
			catch(e){ alert(e); }
		}
 }).upload();
 	
}

// get last data amount 

Ext.DOM.SelectedAmount = function( ){
 
 Ext.Ajax 
 ({
	url    : Ext.DOM.INDEX+"/MgtLockData/SelectedDataAmount/",
	method : 'POST',
	param  :  { 
		 UserId : Ext.Cmp('lock_user_level3').getChecked(),
		 modul : 'amount'
	},
	ERROR  : function( e ){
		Ext.Util(e).proc( function( $callback ){ 	
			if( $callback.success ){
				Ext.Cmp('lock_field_account').setValue($callback.data.ModValueParameter);
				Ext.Cmp('lock_start_amount').setValue($callback.data.ModStartParameter);
				Ext.Cmp('lock_end_amount').setValue($callback.data.ModEndParameter);
			} else {
				Ext.Cmp('lock_field_account').setValue('');
				Ext.Cmp('lock_start_amount').setValue('');
				Ext.Cmp('lock_end_amount').setValue('');
			}
		});
	}
	
  }).post();
}

// @ pack : Ext.DOM.SaveLock()

 Ext.DOM.SaveLock = function()
{
	if( Ext.Cmp('lock_modul_type').getValue()=='status' ){ 
	  Ext.DOM.LockDataByStatus(); }
	else if( Ext.Cmp('lock_modul_type').getValue()=='amount' ){
	  Ext.DOM.LockDataByAmount();
	}
}

// @ pack : user level 
 
$(document).ready(function(){
	
$("#lock_user_level").bind("change", function()
{
	if( $(this).val() == Ext.DOM.USER_LEVEL_LEADER){
		Ext.DOM.UserLevelLeader(Ext.DOM.USER_LEVEL_LEADER, 'listCombo');
		Ext.DOM.UserLevelAgent();			
	} 
	else if($(this).val() == Ext.DOM.USER_LEVEL_AGENT){
		Ext.DOM.UserLevelLeader(Ext.DOM.USER_LEVEL_LEADER, 'combo');
		Ext.DOM.UserLevelAgent({
			PrivilegeId : Ext.DOM.USER_LEVEL_AGENT,
			LeaderId : 0,
			input : 'listCombo'
		});			
	}
	else if($(this).val() == Ext.DOM.USER_LEVEL_INBOUND){
		Ext.DOM.UserLevelLeader(Ext.DOM.USER_LEVEL_LEADER, 'combo');
		Ext.DOM.UserLevelAgent({
			PrivilegeId : Ext.DOM.USER_LEVEL_INBOUND,
			LeaderId : 0,
			input : 'listCombo'
		});			
	}
 });	

// * is my style jquery *-----> 

 Ext.Cmp('user-level-status').setText("<select class='select long' id='lock_account_status' name='lock_account_status'></select>");
 
 $("#tabs-panel-lock").tabs();
 $('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
 
// is money  
 Ext.DOM.AccountLockPage(0);
 if( Ext.Session('HandlingType').getSession() == window.USER_LEVEL_LEADER ){
	$( "#tabs-panel-lock" ).tabs("disable", 1); 
 }	 
 
 
});

// @pack : LockDataByAmount 

 Ext.DOM.LockDataByAmount = function()
{
  var UserDeskoll = Ext.Cmp('lock_user_level3').getChecked();
  var LockModulType = Ext.Cmp('lock_modul_type').getValue();
  var LockAmountLabel = Ext.Cmp('lock_field_account').getValue();
  var lock_start_amount = Ext.Cmp('lock_start_amount').getValue();
  var lock_end_amount = Ext.Cmp('lock_end_amount').getValue();

/* @ pack : LockModulType # --------------- **/

 if( Ext.Cmp('lock_modul_type').empty() ){
	Ext.Msg('Please select Lock Modul').Info();
	return false;
 }

/* @ pack : LockModulType # --------------- **/

 if( UserDeskoll.length==0 ){
	Ext.Msg('Please select from User to UnLock').Info();
	return false;
 }

  Ext.Ajax 
 ({
	url    : Ext.DOM.INDEX+"/MgtLockData/SetLockAmount/",
	method : 'POST',
	param  :  { 
		UserDeskoll 		: UserDeskoll,
		LockModulType 		: LockModulType,
		LockAmountLabel     : LockAmountLabel,
		LockStartAmount 	: lock_start_amount,
		LockEndAmount 		: lock_end_amount 
	},
	ERROR  : function( e ){
		Ext.Util(e).proc( function( response ){ 	
		  if( response.success ){
			Ext.Msg("Lock filter by Amount").Success();
		  } else {
			 Ext.Msg("Lock filter by Amount").Success();
		  }
		});
	}
	
  }).post();
  
}

// @ pack : lock_modul_type

 Ext.Cmp('lock_modul_type').listener
 ({
	'onchange' : function( e ){
		if( $(this).val() == 'amount' ){
			Ext.Cmp('lock_user_level').disabled(false);
			Ext.Cmp('lock_user_level2').disabled(false);
			Ext.Cmp('lock_user_level3').disabled(false);
			Ext.Cmp('user-level-status').setText("<select class='select auto' id='lock_account_status' name='lock_account_status'></select>");
			
		} else if( $(this).val() == 'status' ){
			Ext.Cmp('lock_user_level').disabled(false);
			Ext.Cmp('lock_user_level2').disabled(false);
			Ext.Cmp('lock_user_level3').disabled(false);
			Ext.DOM.SetSelectedStatus({value:0})
		}
	}
 });
 
// @ Ext.DOM.SaveUnLockAccount ==> 
 
 Ext.DOM.SaveUnLockAccount = function() {
 
 var msg = Ext.Msg('Do you want to unlock this rows ?').Confirm();
 if( msg ==false  ){
	return false;
 }
 
	Ext.Ajax 
 ({
	url    : Ext.DOM.INDEX+"/MgtLockData/SaveUnLockAccount/",
	method : 'POST',
	param  :  { 
		 UploadId : Ext.Cmp('lock_id').getChecked()
	},
	ERROR  : function( e ){
		Ext.Util(e).proc( function( $callback ){ 	
			if( $callback.success ){
				Ext.Msg('Unlock Data').Success();
				Ext.DOM.AccountLockPage (0)
			} else {
				Ext.Msg('Unlock Data').Failed();
				Ext.DOM.AccountLockPage(0)
			}
		});
	}
	
  }).post();
 }


</script>
<div id="tabs-panel-lock" style='border:1px solid #dddddd;'>
	<ul>
		<li><a href="#tabs-panel-lock1">Lock Account Filter</a></li>
		<li><a href="#tabs-panel-lock2">Lock Account Upload</a></li>
	</ul>	
	
<!-- tabs one s----> 	

<div id="tabs-panel-lock1" style='background-color:#FFFFFF;'>
	<fieldset class="corner" id="fieldset-panel-lock1" style="background-color:white;">
	<legend class="icon-userapplication" >&nbsp;&nbsp;Lock Account Filter </legend>
		<form name="frmUploadName">
		<table  border=0 cellpadding=1 cellspacing=1>
			<tr>
				<td class="text_caption bottom" ># Lock Modul&nbsp;:</td>
				<td class="bottom" ><?php echo form()->combo('lock_modul_type','select long', $Dropdown[LOCK_DROPDOWN_MODUL]);?></td>
				<td class="text_caption bottom"  valign='top'># Field Label &nbsp;:</td>
				<td class="bottom" valign='top'><?php echo form()->combo('lock_field_account','select long', $Dropdown['LOCK_DROPDOWN_FIELD'] );?></td>
			</tr>	
			<tr>
				<td class="text_caption bottom" width='20%'># User Level&nbsp;:</td>
				<td class="bottom" ><span id="user-level-data"><?php echo form()->combo('lock_user_level','select long',$Dropdown['LOCK_DROPDOWN_PRIVILEGE']);?></span></td>
				<td class="text_caption bottom"  valign='top'># Amount&nbsp;:</td>
				<td class="bottom" valign='top'>
					<?php echo form()->input('lock_start_amount','input_text box');?> to 
					<?php echo form()->input('lock_end_amount','input_text box');?>
				</td>
			</tr>	
			
			<tr>
				 
				<td class="text_caption bottom" width='20%' valign='top'><span id="user-level-label2"># Leader &nbsp;</span> :</td>
				<td class="bottom" valign="top"><span id="user-level-data2"><?php echo form()->combo('lock_user_level2','select long');?></span></td>
				<td class="text_caption" rowspan=8 valign='top'># Status Lock &nbsp;:</td>
				<td rowspan=8 valign='top'><span id="user-level-status"><?php echo form()->listCombo('lock_account_status','select long',$Dropdown['LOCK_DROPDOWN_STATUS']);?></span></td>
			</tr>	
			
			<tr>
				<td class="text_caption" width='20%' valign='top'><span id="user-level-label3"># Deskoll&nbsp;</span> :</td>
				<td valign='top'><div id="user-level-data3"><?php echo form()->combo('lock_user_level3','select long', null);?></div></td>
			</tr>	
		
			<tr>
				<td class="text_caption" >&nbsp;</td>
				<td class='' colspan=4>
					<input type="button" class="button lock" onclick="Ext.DOM.SaveLock();" value="Submit&nbsp;&nbsp;&nbsp;">
					<input type="button" class="button unlock" onclick="Ext.DOM.SaveUnLock();" value="Unlock" style="display:none;">
					
				</td>
			</tr>
		</table>
		</form>
		</legend>
		
	</div>
	
	
<div id="tabs-panel-lock2" style='background-color:#FFFFFF;'>	
<fieldset class="corner" id="fieldset-panel-lock2" style="background-color:white;">
<legend class="icon-userapplication" >&nbsp;&nbsp;Lock Account Upload </legend>
<table  border=0>
 <tr>
   <td valign="top">
		<table border=0 cellpadding=1 cellspacing=1>
			<tr>
				<td class='text_caption bottom' nowrap> * Template &nbsp;:</td>
				<td class='bottom'><?php echo form()->combo('lock_user_template','select long', $template);?></td>
			</tr>
			<tr>
				<td class='text_caption bottom' nowrap> * Product &nbsp;:</td>
				<td class='bottom'><?php echo form()->combo('lock_user_product','select long', $product);?></td>
			</tr>
			<tr>
				<td class='text_caption bottom' nowrap> * Upload File &nbsp;:</td>
				<td class='bottom'><?php echo form()->upload('fileToupload', 'input_text long'); ?></td>
			</tr>
			<tr>
				<td class='text_caption bottom' nowrap> * Start Date &nbsp;:</td>
				<td class='bottom' nowrap>
					<?php echo form()->input('lock_start_date','input_text date', date('d-m-Y'), null, array('style'=>'width:90px;') );?>&nbsp;
					<?php echo form()->combo('lock_start_hour','select box', $Dropdown[LOCK_DROPDOWN_HOUR], '08', null, array('style'=>'width:50px;') );?>&nbsp;:
					<?php echo form()->combo('lock_start_min','select box', $Dropdown[LOCK_DROPDOWN_MINUTE], '00', null, array('style'=>'width:50px;'));?>&nbsp;:
					<?php echo form()->combo('lock_start_sec','select box', $Dropdown[LOCK_DROPDOWN_SECOND], '00', null, array('style'=>'width:50px;'));?>
				</td>
			</tr>
			<tr>
				<td class='text_caption bottom' nowrap> * Expired Date &nbsp;:</td>
				<td class='bottom' nowrap>
					<?php echo form()->input('lock_end_date','input_text date', date('d-m-Y'), null, array('style'=>'width:90px;') );?>&nbsp;
					<?php echo form()->combo('lock_end_hour','select box', $Dropdown[LOCK_DROPDOWN_HOUR], '17', null, array('style'=>'width:50px;') );?>&nbsp;:
					<?php echo form()->combo('lock_end_min','select box', $Dropdown[LOCK_DROPDOWN_MINUTE], '59', null, array('style'=>'width:50px;'));?>&nbsp;:
					<?php echo form()->combo('lock_end_sec','select box', $Dropdown[LOCK_DROPDOWN_SECOND], '59', null, array('style'=>'width:50px;'));?>
				</td>
			</tr>
			<tr>
				<td class='text_caption bottom' nowrap> * Auto Start &nbsp;:</td>
				<td class='bottom' nowrap>
					<?php echo form()->combo('auto_start','select box',array('0'=>'No','1'=>'Yes'), 'N',null);?>&nbsp;
				</td>
			</tr>
			<tr>
				<td class="text_caption" >
					<span id="loading-image"></span></td>
				<td colspan='4'>
					<input type="button" class="button upload" onclick="Ext.DOM.Upload();" value="Upload">
				</td>
			</tr>
			</table>
		</td>
		<td valign='top'>
			<div id="page-content-lock-nav">
				<table align="left" border="0" cellspacing="1" width="100%">
					<thead>
						<tr height="24">
							<th class="ui-corner-top ui-state-default first">#</th>
							<th class="ui-corner-top ui-state-default first">No</th>
							<th class="ui-corner-top ui-state-default middle">Upload Date</th>
							<th class="ui-corner-top ui-state-default middle left">&nbsp;Upload File</th>
							<th class="ui-corner-top ui-state-default middle">Uplod Size</th>
							<th class="ui-corner-top ui-state-default lasted">Lock Size</th>
							<th class="ui-corner-top ui-state-default lasted">User Upload</th>
							<th class="ui-corner-top ui-state-default lasted">Expired Lock</th>
						</tr>	
					</thead>
					<tbody id="page-content-lock"> </tbody>
					
				</table>	
			
			</div>
		</td>
	</tr>
</table>	
	</fieldset>
</div>

</div>
	