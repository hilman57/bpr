<html>
<head>
<title>Staff Productivity Report</title>
</head>
	<?php $this -> load -> view('rpt_staff_activity/rpt_staff_activity_style'); ?>
<body>
	<table border=0 cellpadding=0 cellspacing=0 width=2728 style='border-collapse:collapse;table-layout:fixed;width:2049pt'>
		<tr height=25 style='mso-height-source:userset;height:18.75pt'>
			<td colspan=33 height=25 class=xl366 width=2728 style='height:18.75pt;width:2049pt'>Staff&nbsp;Productivity&nbsp;Report</td>
		</tr>
		<tr height=25 style='mso-height-source:userset;height:18.75pt'>
			<td colspan=33 height=25 class=xl364 width=2728 style='height:18.75pt;width:2049pt'>Novermber - 2014</td>
		</tr>
		<tr height=20 style='height:15.0pt'>
			<td height=20 class=xl65 style='height:15.0pt'></td>
			<td colspan=32 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=48 style='mso-height-source:userset;height:36.0pt'>
			<td colspan=33 height=48 class=xl239 width=2728 style='height:36.0pt;width:2049pt'>ACTIVITY STAFF Welcome Call (Per User)</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl66 style='height:15.0pt'>&nbsp;</td>
			<td class=xl67 width=132 style='width:99pt'>Periode<span
			style='mso-spacerun:yes'> </span></td>
			<td colspan=2 class=xl240 width=147 style='width:110pt'>: Oct 2014</td>
			<td colspan=5 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=4 class=xl69 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl70>&nbsp;</td>
			<td colspan=3 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl71 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl69>&nbsp;</td>
			<td class=xl72>&nbsp;</td>
			<td colspan=10 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl66 style='height:15.0pt'>&nbsp;</td>
			<td class=xl73 width=132 style='width:99pt'>CCO</td>
			<td colspan=3 class=xl73 width=227 style='width:170pt'>: Dian Priyanti (Didi)</td>
			<td colspan=9 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl70>&nbsp;</td>
			<td colspan=3 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl74 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl75>&nbsp;</td>
			<td colspan=10 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=8 style='mso-height-source:userset;height:6.0pt'>
			<td height=8 colspan=3 class=xl76 style='height:6.0pt;mso-ignore:colspan'>&nbsp;</td>
			<td colspan=6 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=5 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl78>&nbsp;</td>
			<td colspan=3 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td rowspan=12 height=240 class=xl362 width=166 style='border-bottom:1.0pt solid black;height:180.0pt;width:125pt'>SUMMARY</td>
			<td class=xl81 width=132 style='width:99pt'>Jumlah Data f/u</td>
			<td class=xl82 width=80 style='width:60pt'>New</td>
			<td class=xl83 width=67 style='width:50pt'>0</td>
			<td class=xl84 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl84 width=78 style='width:59pt'>&nbsp;</td>
			<td class=xl84 width=83 style='width:62pt'>&nbsp;</td>
			<td colspan=2 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=5 rowspan=2 class=xl241 width=368 style='width:275pt'>Completed</td>
			<td rowspan=2 class=xl84 width=94 style='border-bottom:1.0pt solid black;width:71pt'>Total</td>
			<td rowspan=2 class=xl84 width=82 style='border-bottom:1.0pt solid black;width:62pt'>%</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl85 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=11 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl76 style='height:15.0pt'>&nbsp;</td>
			<td class=xl86 width=80 style='width:60pt'>Pending</td>
			<td class=xl87 width=67 style='width:50pt'>0</td>
			<td colspan=5 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl85 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=11 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl76 style='height:15.0pt'>&nbsp;</td>
			<td class=xl88 width=80 style='width:60pt'>ALL</td>
			<td class=xl86 width=67 style='width:50pt'>&nbsp;</td>
			<td class=xl89 width=80 style='width:60pt'>0</td>
			<td colspan=4 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td rowspan=4 class=xl356 width=75 style='width:56pt'>Call Completed</td>
			<td colspan=2 rowspan=2 class=xl356 width=146 style='width:109pt'>Contacted</td>
			<td colspan=2 class=xl357 width=147 style='border-left:none;width:110pt'>Agree</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl85 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=11 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl92 width=132 style='height:15.0pt;width:99pt'>Surveyed Ratio</td>
			<td class=xl87 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl87 width=67 style='width:50pt'>&nbsp;</td>
			<td class=xl87 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl93 width=78 style='width:59pt'>#DIV/0!</td>
			<td colspan=3 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl358 width=147 style='border-right:.5pt solid black;border-left:none;width:110pt'>No Interest To Talk</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl85 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=11 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl94 width=132 style='height:15.0pt;width:99pt'>Contact Ratio</td>
			<td class=xl95 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl95 width=67 style='width:50pt'>&nbsp;</td>
			<td class=xl95 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl95 width=78 style='width:59pt'>&nbsp;</td>
			<td class=xl96 width=83 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 rowspan=2 class=xl356 width=146 style='width:109pt'>Non Contacted</td>
			<td colspan=2 class=xl357 width=147 style='border-left:none;width:110pt'>Can Not Be Reached</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl85 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=11 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td colspan=2 height=20 class=xl243 width=212 style='height:15.0pt;width:159pt'>Data Consumtion (avg)</td>
			<td class=xl97 width=67 style='width:50pt'>#REF!</td>
			<td colspan=3 class=xl98 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl358 width=147 style='border-right:.5pt solid black;border-left:none;width:110pt'>No Answer</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td colspan=2 height=20 class=xl244 width=212 style='height:15.0pt;width:159pt'>Completed/hour (avg)</td>
			<td class=xl99 width=67 style='width:50pt'>&nbsp;</td>
			<td class=xl100 width=80 style='width:60pt'>#REF!</td>
			<td class=xl101 width=78 style='width:59pt'>&nbsp;</td>
			<td class=xl101 width=83 style='width:62pt'>&nbsp;</td>
			<td colspan=2 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 rowspan=5 class=xl356 width=221 style='width:165pt'>Uncontacted(UNCLEAN DATA)</td>
			<td colspan=2 class=xl361 width=147 style='border-right:.5pt solid black;width:110pt'>Busy Tone</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td colspan=2 height=20 class=xl245 width=212 style='height:15.0pt;width:159pt'># Working Days on this month</td>
			<td class=xl102 width=67 style='width:50pt'>23</td>
			<td class=xl102 width=80 style='width:60pt'>day</td>
			<td colspan=4 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl359 width=147 style='width:110pt'>Wrong Number</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl86 width=132 style='height:15.0pt;width:99pt'>#Effective Work Days</td>
			<td class=xl86 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl102 width=67 style='width:50pt'>0</td>
			<td class=xl102 width=80 style='width:60pt'>day</td>
			<td colspan=4 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl359 width=147 style='width:110pt'>Tulalit (Dead Tone)</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl76 width=132 style='height:15.0pt;width:99pt'>Capacity</td>
			<td class=xl66>&nbsp;</td>
			<td class=xl103 width=67 style='width:50pt'>0</td>
			<td colspan=5 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl359 width=147 style='width:110pt'>Leave Massage</td>
			<td class=xl90 width=94 style='width:71pt'>0</td>
			<td class=xl91 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl104 width=132 style='height:15.0pt;width:99pt'>Productivity</td>
			<td class=xl105 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl106 width=67 style='width:50pt'>#DIV/0!</td>
			<td class=xl107 width=80 style='width:60pt'>&nbsp;</td>
			<td colspan=4 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl360 width=147 style='width:110pt'>Return List</td>
			<td class=xl108 width=94 style='width:71pt'>0</td>
			<td class=xl109 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl79 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl77>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=10 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl76 width=132 style='height:15.0pt;width:99pt'>Late</td>
			<td colspan=2 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl103 width=80 style='width:60pt'>day</td>
			<td colspan=4 class=xl66 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=5 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl110 width=94 style='width:71pt'>0</td>
			<td class=xl111 width=82 style='width:62pt'>#DIV/0!</td>
			<td colspan=2 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=3 class=xl85 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl80>&nbsp;</td>
			<td colspan=11 class=xl77 style='mso-ignore:colspan'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td colspan=2 height=20 class=xl112 width=298 style='height:15.0pt;width:224pt'>A. ACTIVITY</td>
			<td class=xl112 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl112 width=67 style='width:50pt'>&nbsp;</td>
			<td class=xl112 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl112 width=78 style='width:59pt'>&nbsp;</td>
			<td class=xl112 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl112 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl112 width=77 style='width:58pt'>&nbsp;</td>
			<td class=xl113 width=75 style='width:56pt'>&nbsp;</td>
			<td class=xl113 width=75 style='width:56pt'>&nbsp;</td>
			<td class=xl113 width=71 style='width:53pt'>&nbsp;</td>
			<td class=xl113 width=75 style='width:56pt'>&nbsp;</td>
			<td class=xl113 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl70>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl113 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl113 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl114 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl114 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl114 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl115 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl113 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl113 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl113 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl113 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl113 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl113 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl113 width=64 style='width:48pt'>&nbsp;</td>
			<td colspan=4 class=xl198 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=26 style='mso-height-source:userset;height:19.5pt'>
			<td rowspan=4 height=132 class=xl247 width=166 style='border-bottom:1.0pt solid black;eight:99.0pt;border-top:none;width:125pt'>Tgl</td>
			<td rowspan=4 class=xl249 width=132 style='border-bottom:1.0pt solid black;border-top:none;width:99pt'>TOTAL DATA</td>
			<td colspan=8 class=xl252 width=624 style='border-right:1.0pt solid black;border-left:none;width:468pt'>NEW</td>
			<td colspan=23 class=xl256 width=1806 style='border-right:1.0pt solid black;border-left:none;width:1357pt'>PENDING</td>
		</tr>
		<tr height=36 style='mso-height-source:userset;height:27.0pt'>
			<td rowspan=3 height=106 class=xl259 width=80 style='border-bottom:1.0pt solid black;height:79.5pt;border-top:none;width:60pt'>Jumlah</td>
			<td rowspan=3 class=xl262 width=67 style='border-bottom:1.0pt solid black;border-top:none;width:50pt'>Issued date terlama</td>
			<td rowspan=3 class=xl264 width=80 style='border-bottom:1.0pt solid black;border-top:none;width:60pt'>Issued date terbaru</td>
			<td class=xl116 width=78 style='border-left:none;width:59pt'>WCDS1 WC11411001</td>
			<td class=xl117 width=83 style='width:62pt'>WCDS2 WC11411001</td>
			<td class=xl118 width=84 style='width:63pt'>WCDS3 WC11411001</td>
			<td class=xl119 width=77 style='width:58pt'>WCDS4 WC11411001</td>
			<td class=xl120 width=75 style='width:56pt'>WCDS1 WC01410001</td>
			<td rowspan=3 class=xl267 width=75 style='border-bottom:.5pt solid black;width:56pt'>Jumlah</td>
			<td rowspan=3 class=xl270 width=71 style='border-bottom:.5pt solid black;width:53pt'>Issued date terlama</td>
			<td rowspan=3 class=xl273 width=75 style='border-bottom:.5pt solid black;width:56pt'>Issued date terbaru</td>
			<td colspan=4 class=xl276 style='border-right:1.0pt solid black;border-left:none'>WCDS1 WC11411001</td>
			<td colspan=4 class=xl279 style='border-right:1.0pt solid black;border-left:none'>WCDS2 WC11411001</td>
			<td colspan=4 class=xl283 style='border-right:1.0pt solid black;border-left:none'>WCDS3 WC11411001</td>
			<td colspan=4 class=xl286 style='border-right:1.0pt solid black;border-left:none'>WCDS4 WC11411001</td>
			<td colspan=4 class=xl289 style='border-right:1.0pt solid black;border-left:none'>WCDS4 WC11411001</td>
		</tr>
		<tr height=35 style='mso-height-source:userset;height:26.25pt'>
			<td rowspan=2 height=70 class=xl291 width=78 style='border-bottom:1.0pt solid black;height:52.5pt;border-top:none;width:59pt'>BCA</td>
			<td rowspan=2 class=xl293 width=83 style='border-bottom:1.0pt solid black;border-top:none;width:62pt'>CIMB</td>
			<td rowspan=2 class=xl293 width=84 style='border-bottom:1.0pt solid black;border-top:none;width:63pt'>Other Bancass</td>
			<td rowspan=2 class=xl293 width=77 style='border-bottom:1.0pt solid black;border-top:none;width:58pt'>Agency</td>
			<td rowspan=2 class=xl295 width=75 style='border-bottom:1.0pt solid black;border-top:none;width:56pt'>Temporell</td>
			<td colspan=4 class=xl297 width=332 style='border-right:1.0pt solid black;border-left:none;width:250pt'>BCA</td>
			<td colspan=4 class=xl300 width=347 style='border-right:1.0pt solid black;border-left:none;width:261pt'>CIMB</td>
			<td colspan=4 class=xl303 width=378 style='border-right:1.0pt solid black;border-left:none;width:285pt'>Other Bancass</td>
			<td colspan=4 class=xl304 width=272 style='border-right:1.0pt solid black;border-left:none;width:204pt'>Agency</td>
			<td colspan=4 class=xl304 width=256 style='border-right:1.0pt solid black;border-left:none;width:192pt'>Temporell</td>
		</tr>
		<tr height=35 style='mso-height-source:userset;height:26.25pt'>
			<td height=35 class=xl121 width=72 style='height:26.25pt;border-left:none;width:54pt'>1 Attempt</td>
			<td class=xl122 width=94 style='width:71pt'>2 Attempt</td>
			<td class=xl122 width=82 style='width:62pt'>3 Attempt++</td>
			<td class=xl123 width=84 style='width:63pt'>Call Back</td>
			<td class=xl124 width=88 style='width:66pt'>1 Attempt</td>
			<td class=xl125 width=94 style='width:71pt'>2 Attempt</td>
			<td class=xl125 width=83 style='width:62pt'>3 Attempt++</td>
			<td class=xl126 width=82 style='width:62pt'>Call Back</td>
			<td class=xl125 width=97 style='width:73pt'>1 Attempt</td>
			<td class=xl125 width=93 style='width:70pt'>2 Attempt</td>
			<td class=xl125 width=74 style='width:56pt'>3 Attempt++</td>
			<td class=xl126 width=114 style='width:86pt'>Call Back</td>
			<td class=xl125 width=64 style='border-top:none;width:48pt'>1 Attempt</td>
			<td class=xl125 width=64 style='border-top:none;width:48pt'>2 Attempt</td>
			<td class=xl125 width=80 style='border-top:none;width:60pt'>3 Attempt++</td>
			<td class=xl126 width=64 style='border-top:none;width:48pt'>Call Back</td>
			<td class=xl125 width=64 style='border-top:none;width:48pt'>1 Attempt</td>
			<td class=xl125 width=64 style='border-top:none;width:48pt'>2 Attempt</td>
			<td class=xl125 width=64 style='border-top:none;width:48pt'>3 Attempt++</td>
			<td class=xl126 width=64 style='border-top:none;width:48pt'>Call Back</td>
		</tr>
		<tr height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl127 style='height:12.75pt'>1-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl137>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl139>&nbsp;</td>
			<td class=xl137>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl140>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl140>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl140>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl138>&nbsp;</td>
			<td class=xl140>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>2-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>3-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>4-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>5-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>6-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>7-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>8-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>9-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>10-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>11-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>12-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>13-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>14-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>15-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>16-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>17-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>18-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>19-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>20-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>21-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>22-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>23-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>24-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>25-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl145 style='height:15.0pt'>26-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl146 width=80 style='width:60pt'>0</td>
			<td class=xl147 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl148 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl149>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl150>&nbsp;</td>
			<td class=xl151>&nbsp;</td>
			<td class=xl152>0</td>
			<td class=xl153 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl148 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl146 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl148 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl146 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl147 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl147 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl154 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl147 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl147 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl147 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl154 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl147 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl154 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>27-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>28-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>29-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl132>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl133>&nbsp;</td>
			<td class=xl134>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl127 style='height:15.0pt'>30-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl129 width=80 style='width:60pt'>0</td>
			<td class=xl130 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl131 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl155>&nbsp;</td>
			<td class=xl156>&nbsp;</td>
			<td class=xl156>&nbsp;</td>
			<td class=xl156>&nbsp;</td>
			<td class=xl157>&nbsp;</td>
			<td class=xl135>0</td>
			<td class=xl136 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl131 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl141 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl143 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl141 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl142 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl142 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl144 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl142 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl142 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl142 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl144 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl142 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl144 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl158 style='height:15.0pt'>31-Oct-14</td>
			<td class=xl128 width=132 style='width:99pt'>0</td>
			<td class=xl159 width=80 style='width:60pt'>0</td>
			<td class=xl160 width=67 style='width:50pt'>&lt;date&gt;</td>
			<td class=xl161 width=80 style='width:60pt'>&lt;date&gt;</td>
			<td class=xl162>&nbsp;</td>
			<td class=xl163>&nbsp;</td>
			<td class=xl163>&nbsp;</td>
			<td class=xl163>&nbsp;</td>
			<td class=xl164>&nbsp;</td>
			<td class=xl165>0</td>
			<td class=xl166 width=71 style='width:53pt'>&lt;date&gt;</td>
			<td class=xl161 width=75 style='width:56pt'>&lt;date&gt;</td>
			<td class=xl167 width=72 style='width:54pt'>&nbsp;</td>
			<td class=xl168 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl168 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl169 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl167 width=88 style='width:66pt'>&nbsp;</td>
			<td class=xl168 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl168 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl170 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl168 width=97 style='width:73pt'>&nbsp;</td>
			<td class=xl168 width=93 style='width:70pt'>&nbsp;</td>
			<td class=xl168 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl170 width=114 style='width:86pt'>&nbsp;</td>
			<td class=xl168 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl168 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl168 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl170 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl168 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl168 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl168 width=64 style='width:48pt'>&nbsp;</td>
			<td class=xl170 width=64 style='width:48pt'>&nbsp;</td>
		</tr>
		<tr height=32 style='mso-height-source:userset;height:24.0pt'>
			<td height=32 class=xl171 width=166 style='height:24.0pt;width:125pt'>JUMLAH</td>
			<td class=xl172 width=132 style='width:99pt'>0</td>
			<td class=xl173 width=80 style='width:60pt'>0</td>
			<td colspan=2 class=xl198 style='mso-ignore:colspan'></td>
			<td class=xl174 width=78 style='width:59pt'>0</td>
			<td class=xl173 width=83 style='width:62pt'>0</td>
			<td class=xl173 width=84 style='width:63pt'>0</td>
			<td class=xl173 width=77 style='width:58pt'>0</td>
			<td class=xl173 width=75 style='width:56pt'>0</td>
			<td class=xl175 width=75 style='width:56pt'>0</td>
			<td colspan=2 class=xl198 style='mso-ignore:colspan'></td>
			<td class=xl176 width=72 style='width:54pt'>0</td>
			<td class=xl177 width=94 style='width:71pt'>0</td>
			<td class=xl177 width=82 style='width:62pt'>0</td>
			<td class=xl177 width=84 style='width:63pt'>0</td>
			<td class=xl177 width=88 style='width:66pt'>0</td>
			<td class=xl177 width=94 style='width:71pt'>0</td>
			<td class=xl177 width=83 style='width:62pt'>0</td>
			<td class=xl177 width=82 style='width:62pt'>0</td>
			<td class=xl177 width=97 style='width:73pt'>0</td>
			<td class=xl177 width=93 style='width:70pt'>0</td>
			<td class=xl177 width=74 style='width:56pt'>0</td>
			<td class=xl177 width=114 style='width:86pt'>0</td>
			<td class=xl177 width=64 style='width:48pt'>0</td>
			<td class=xl177 width=64 style='width:48pt'>0</td>
			<td class=xl177 width=80 style='width:60pt'>0</td>
			<td class=xl177 width=64 style='width:48pt'>0</td>
			<td class=xl177 width=64 style='width:48pt'>0</td>
			<td class=xl177 width=64 style='width:48pt'>0</td>
			<td class=xl177 width=64 style='width:48pt'>0</td>
			<td class=xl178 width=64 style='width:48pt'>0</td>
		</tr>
		<tr height=32 style='mso-height-source:userset;height:24.0pt'>
			<td height=32 class=xl179 style='height:24.0pt'>Rata-rata</td>
			<td class=xl180>0.00</td>
			<td class=xl180>0.00</td>
			<td colspan=2 class=xl198 style='mso-ignore:colspan'></td>
			<td class=xl181>#DIV/0!</td>
			<td class=xl180>#DIV/0!</td>
			<td class=xl180>#DIV/0!</td>
			<td class=xl182>0.00</td>
			<td colspan=2 class=xl183 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl197 style='mso-ignore:colspan'></td>
			<td class=xl184>&nbsp;</td>
			<td class=xl183>&nbsp;</td>
			<td class=xl185>&nbsp;</td>
			<td colspan=2 class=xl183 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl186>&nbsp;</td>
			<td class=xl183>&nbsp;</td>
			<td colspan=3 class=xl187 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl183>&nbsp;</td>
			<td colspan=2 class=xl188 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td colspan=6 class=xl197 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 colspan=3 class=xl68 style='height:15.0pt;mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl198 style='mso-ignore:colspan'></td>
			<td colspan=7 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl70>&nbsp;</td>
			<td colspan=4 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td colspan=8 class=xl197 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 colspan=3 class=xl68 style='height:15.0pt;mso-ignore:colspan'>&nbsp;</td>
			<td colspan=2 class=xl198 style='mso-ignore:colspan'></td>
			<td colspan=7 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl70>&nbsp;</td>
			<td colspan=4 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td colspan=8 class=xl197 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td colspan=3 height=20 class=xl306 width=378 style='height:15.0pt;width:284pt'>B. DETAIL ACTIVITY</td>
			<td colspan=6 class=xl192 style='mso-ignore:colspan'>&nbsp;</td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl75>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td colspan=8 class=xl198 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=30 style='mso-height-source:userset;height:22.5pt'>
			<td height=30 class=xl193 width=166 style='height:22.5pt;border-top:none;width:125pt'>Tgl</td>
			<td colspan=15 class=xl307 width=1225 style='border-right:1.0pt solid black;width:919pt'>(Contacted+Call Completed+Pending)</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl194 width=166 style='height:12.75pt;width:125pt'>&nbsp;</td>
			<td rowspan=5 class=xl311 width=132 style='border-bottom:1.0pt solid black;border-top:none;width:99pt'>Total Data<span style='mso-spacerun:yes'> </span></td>
			<td colspan=10 class=xl314 width=770 style='border-right:1.0pt solid black;border-left:none;width:577pt'>Completed</td>
			<td colspan=4 rowspan=3 class=xl316 width=323 style='border-right:1.0pt solid black;border-bottom:.5pt solid black;width:243pt'>Pending - Contact</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl194 width=166 style='height:12.75pt;width:125pt'>&nbsp;</td>
			<td class=xl195>&nbsp;</td>
			<td colspan=2 rowspan=2 class=xl322 width=147 style='border-bottom:.5pt solid black;width:110pt'>Contacted</td>
			<td colspan=7 class=xl326 width=543 style='border-right:1.0pt solid black;width:407pt'>Call Completed</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl194 width=166 style='height:12.75pt;width:125pt'>&nbsp;</td>
			<td class=xl195>&nbsp;</td>
			<td colspan=2 class=xl329 width=161 style='border-right:1.0pt solid black;width:121pt'>Non Contacted</td>
			<td colspan=5 class=xl332 width=382 style='border-right:1.0pt solid black;border-left:none;width:286pt'>Uncontacted (UNCLEAN DATA)</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=28 style='mso-height-source:userset;height:21.4pt'>
			<td rowspan=2 height=56 class=xl334 width=166 style='border-bottom:1.0pt solid black;height:42.8pt;width:125pt'>&nbsp;</td>
			<td rowspan=2 class=xl336 width=80 style='border-bottom:1.0pt solid black;width:60pt'>&nbsp;</td>
			<td rowspan=2 class=xl338 width=67 style='border-bottom:1.0pt solid black;border-top:none;width:50pt'>Agree</td>
			<td rowspan=2 class=xl340 width=80 style='border-bottom:1.0pt solid black;border-top:none;width:60pt'>No Interest To Talk</td>
			<td rowspan=2 class=xl342 width=78 style='border-bottom:1.0pt solid black;border-top:none;width:59pt'>Can Not Be Reached</td>
			<td rowspan=2 class=xl344 width=83 style='border-bottom:1.0pt solid black;border-top:none;width:62pt'>No Answer</td>
			<td rowspan=2 class=xl342 width=84 style='border-bottom:1.0pt solid black;border-top:none;width:63pt'>Busy Tone</td>
			<td rowspan=2 class=xl346 width=77 style='border-bottom:1.0pt solid black;border-top:none;width:58pt'>Wrong Number</td>
			<td class=xl199 width=75 style='width:56pt'>Tulalit</td>
			<td rowspan=2 class=xl346 width=75 style='border-bottom:1.0pt solid black;border-top:none;width:56pt'>Leave Masssge</td>
			<td rowspan=2 class=xl344 width=71 style='border-bottom:1.0pt solid black;border-top:none;width:53pt'>Return List</td>
			<td rowspan=2 class=xl348 width=75 style='border-bottom:.5pt solid black;border-top:none;width:56pt'>1st</td>
			<td rowspan=2 class=xl350 width=72 style='border-bottom:.5pt solid black;border-top:none;width:54pt'>2nd</td>
			<td rowspan=2 class=xl350 width=94 style='border-bottom:.5pt solid black;border-top:none;width:71pt'>3rd ++</td>
			<td rowspan=2 class=xl352 width=82 style='border-bottom:.5pt solid black;border-top:none;width:62pt'>Call Back Letter</td>
			<td rowspan=2 class=xl354 width=84 style='width:63pt'>&nbsp;</td>
			<td rowspan=2 class=xl189 width=88 style='width:66pt'>&nbsp;</td>
			<td rowspan=2 class=xl190 width=94 style='width:71pt'>&nbsp;</td>
			<td rowspan=2 class=xl190 width=83 style='width:62pt'>&nbsp;</td>
			<td rowspan=2 class=xl190 width=82 style='width:62pt'>&nbsp;</td>
			<td rowspan=2 class=xl191 width=97 style='width:73pt'>&nbsp;</td>
			<td rowspan=2 class=xl68 width=93 style='width:70pt'>&nbsp;</td>
			<td rowspan=2 class=xl188>&nbsp;</td>
			<td rowspan=2 class=xl68 width=114 style='width:86pt'>&nbsp;</td>
			<td rowspan=2 class=xl198 width=64 style='width:48pt'></td>
			<td rowspan=2 class=xl68 width=64 style='width:48pt'>&nbsp;</td>
			<td rowspan=2 class=xl68 width=80 style='width:60pt'>&nbsp;</td>
			<td rowspan=2 class=xl68 width=64 style='width:48pt'>&nbsp;</td>
			<td rowspan=2 class=xl68 width=64 style='width:48pt'>&nbsp;</td>
			<td rowspan=2 class=xl68 width=64 style='width:48pt'>&nbsp;</td>
			<td rowspan=2 class=xl68 width=64 style='width:48pt'>&nbsp;</td>
			<td rowspan=2 class=xl198 width=64 style='width:48pt'></td>
		</tr>
		<tr height=28 style='mso-height-source:userset;height:21.4pt'>
			<td height=28 class=xl196 width=75 style='height:21.4pt;width:56pt'>(Dead Tone)</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl200 style='height:16.5pt;border-top:none'>1-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl209 style='border-top:none'>&nbsp;</td>
			<td class=xl210 style='border-top:none'>&nbsp;</td>
			<td class=xl210 style='border-top:none'>&nbsp;</td>
			<td class=xl211 style='border-top:none'>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>2-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>3-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>4-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>5-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>6-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>7-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>8-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>9-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td colspan=8 class=xl198 style='mso-ignore:colspan'></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>10-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>11-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>12-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl223>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>13-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl223>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>14-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>15-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>16-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>17-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>18-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>19-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>20-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>21-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>22-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>23-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>24-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>25-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl217 style='height:16.5pt'>26-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl218>0</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl220>&nbsp;</td>
			<td class=xl219>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl222>&nbsp;</td>
			<td class=xl221>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>27-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl212>&nbsp;</td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>28-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>29-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl213 style='height:16.5pt'>30-Oct-14</td>
			<td class=xl201>0</td>
			<td class=xl202>0</td>
			<td class=xl203>&nbsp;</td>
			<td class=xl204>&nbsp;</td>
			<td class=xl205>&nbsp;</td>
			<td class=xl206>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl207>&nbsp;</td>
			<td class=xl208>&nbsp;</td>
			<td class=xl214>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl215>&nbsp;</td>
			<td class=xl216>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=22 style='mso-height-source:userset;height:16.5pt'>
			<td height=22 class=xl224 style='height:16.5pt'>31-Oct-14</td>
			<td class=xl225>0</td>
			<td class=xl226>0</td>
			<td class=xl227>&nbsp;</td>
			<td class=xl228>&nbsp;</td>
			<td class=xl229>&nbsp;</td>
			<td class=xl230>&nbsp;</td>
			<td class=xl231>&nbsp;</td>
			<td class=xl231>&nbsp;</td>
			<td class=xl231>&nbsp;</td>
			<td class=xl231>&nbsp;</td>
			<td class=xl232>&nbsp;</td>
			<td class=xl233>&nbsp;</td>
			<td class=xl234>&nbsp;</td>
			<td class=xl234>&nbsp;</td>
			<td class=xl235>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198></td>
		</tr>
		<tr height=31 style='mso-height-source:userset;height:23.25pt'>
			<td height=31 class=xl236 width=166 style='height:23.25pt;width:125pt'>JUMLAH</td>
			<td class=xl237 width=132 style='width:99pt'>0</td>
			<td class=xl237 width=80 style='width:60pt'>0</td>
			<td class=xl237 width=67 style='width:50pt'>0</td>
			<td class=xl237 width=80 style='width:60pt'>0</td>
			<td class=xl237 width=78 style='width:59pt'>0</td>
			<td class=xl237 width=83 style='width:62pt'>0</td>
			<td class=xl237 width=84 style='width:63pt'>0</td>
			<td class=xl237 width=77 style='width:58pt'>0</td>
			<td class=xl237 width=75 style='width:56pt'>0</td>
			<td class=xl237 width=75 style='width:56pt'>0</td>
			<td class=xl237 width=71 style='width:53pt'>0</td>
			<td class=xl237 width=75 style='width:56pt'>0</td>
			<td class=xl237 width=72 style='width:54pt'>0</td>
			<td class=xl237 width=94 style='width:71pt'>0</td>
			<td class=xl238 width=82 style='width:62pt'>0</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl189>&nbsp;</td>
			<td colspan=3 class=xl190 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl191>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl188>&nbsp;</td>
			<td class=xl68>&nbsp;</td>
			<td class=xl198></td>
			<td colspan=6 class=xl68 style='mso-ignore:colspan'>&nbsp;</td>
			<td class=xl198 width=64 style='width:48pt'></td>
		</tr>
	</table>
</body>
</html>