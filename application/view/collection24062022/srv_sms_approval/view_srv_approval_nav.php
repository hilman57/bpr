<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	apprv_agent_id 		: "<?php echo _get_exist_session('apprv_agent_id');?>",
	apprv_sms_status 	: "<?php echo _get_exist_session('apprv_sms_status');?>",
	apprv_campaign_id 	: "<?php echo _get_exist_session('apprv_campaign_id');?>",
	apprv_cust_id 		: "<?php echo _get_exist_session('apprv_cust_id');?>",
	apprv_cust_name 	: "<?php echo _get_exist_session('apprv_cust_name');?>",
	apprv_start_date 	: "<?php echo _get_exist_session('apprv_start_date');?>",
	apprv_end_date 		: "<?php echo _get_exist_session('apprv_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/SMSApproval/index/',
	custlist : Ext.DOM.INDEX+'/SMSApproval/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('flwCustomers').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('flwCustomers').Clear();
	Ext.DOM.searchCustomer();
}
		
// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!='') 
 {	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/SMSApproval/index/', 
		}
  });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}
// @ pack : On ENTER Keyboard 

Ext.DOM.SMSApprove = function() {
	var SmsId = Ext.Cmp('SmsId').getValue();
	Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SMSApproval/SMSApprove',
			method 	: 'POST',
			param 	: { SmsId : SmsId,
						Code  : 102
					},
			ERROR	: function(e) {
				Ext.Util(e).proc(function(response){
					if( response.success){
						Ext.Msg("SMS Approve").Success();
						Ext.DOM.searchCustomer();
					} else {
						Ext.Msg("SMS Approve").Failed();
					}
				});
			}	
	}).post();
	
}
// @ pack : On ENTER Keyboard 

Ext.DOM.SMSReject = function() {
	var SmsId = Ext.Cmp('SmsId').getValue();
	Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SMSApproval/SMSApprove',
			method 	: 'POST',
			param 	: { SmsId : SmsId,
						Code  : 103
					},
			ERROR	: function(e) {
				Ext.Util(e).proc(function(response){
					if( response.success){
						Ext.Msg("SMS Rejected").Success();
						Ext.DOM.searchCustomer();
					} else {
						Ext.Msg("SMS Rejected").Failed();
					}
				});
			}	
	}).post();
}



// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['SMS Approve'],['SMS Reject']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['SMSApprove'],['SMSReject']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['email_go.png'],['email_delete.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="flwCustomers">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('apprv_campaign_id','select auto', $CampaignId, _get_exist_session('apprv_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('apprv_agent_id','select auto', $Deskoll, _get_exist_session('apprv_agent_id'));?></td>
			
			
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('apprv_cust_id','input_text long', _get_exist_session('apprv_cust_id'));?></td>
			<td class="text_caption bottom"> # Approve Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('apprv_sms_status','select auto', $ApproveStatus, _get_exist_session('apprv_sms_status'));?></td>
			<td class="text_caption bottom"> &nbsp;</td>
			<td class="bottom"></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('apprv_cust_name','input_text long', _get_exist_session('apprv_cust_name'));?></td>
			<td class="text_caption bottom"> # Create Date &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('apprv_start_date','input_text date', _get_exist_session('apprv_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('apprv_end_date','input_text date', _get_exist_session('apprv_end_date'));?>
			</td>
			<td class="text_caption bottom"> &nbsp;</td>
			<td class="bottom"></td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->