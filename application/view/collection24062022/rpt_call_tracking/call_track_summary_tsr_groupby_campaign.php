<?php 
$CSS =& get_instance(); 
foreach($CALL_USERS as $ID => $rowusers ) : ?>
<div class='navigation' style='font-weight:bold;
	margin-bottom:8px;font-family:Trebuchet MS;color:#b41707;'> <?php __($rowusers['full_name']); ?>
</div>

<div class='content grid' style='margin-right:2px;width:600px;'>
	<table class="grid" cellpadding="2" cellspacing="0" border=0 width="90%">
	 <tr>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('first'));?>">Campaign</td>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Data Size</td> 		
		  <td align="center" colspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Record Data</td>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Call Attempt</td>	
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">AVG Call / Record</td>
		  <?php foreach( $CATEGORY_STATUS_CALL as $key => $category ) :?>
		  <td align="center" colspan=<?php __($CATEGORY_COUNTER_CALL[$key]); ?>  class="<?php __($CSS->setHedaerStyles('middle'));?>"><?php __($category); ?></td>
		  <?php endforeach; ?>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>" nowrap>Sales Rate</td>
	 </tr>
	
	 <tr>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>">New Data</td>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>">Utilize</td>
		  <?php foreach( $CATEGORY_STATUS_CALL as $key => $category ) :?>
		  <?php foreach( $CALL_REASON_CATEGORY[$key] as $CallReasonId => $CallReasonName ) : ?>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>"><?php __($CallReasonName); ?></td>
		  <?php endforeach; ?>		
		  <?php endforeach; ?>
		  
	 </tr>	
<?php  
/* start default totals **/

$dataSizeReasonId = array();
$totSizeInterestId  = 0;

$totsDataSize = 0;
$totsUtilizeSize = 0;
$totsNotUtilizeSize = 0;
$totsCallAtemptSize = 0;
 
/* start show data by grid contents **/

if(is_array($CALL_CONTENT[$rowusers['UserId']]))
foreach( $CALL_CONTENT[$rowusers['UserId']] as $CampaignId => $rows ) 
{
	/** calculate connect rate / atemp call **/

  $AvgCallAtempt 	 = ( $rows['data_utilize'] ? round(($rows['size_atempt']/$rows['data_utilize']),2) : 0);
	
	__("<tr>");
		__("<td class='" . $CSS->setContentStyles('first') . "' nowrap>&nbsp;". $rows['CampaignName'] ."&nbsp;</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>". ( $rows['data_size'] ? $rows['data_size'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['data_not_utilize'] ? $rows['data_not_utilize'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['data_utilize'] ? $rows['data_utilize'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['size_atempt'] ? $rows['size_atempt'] :0 )  ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $AvgCallAtempt ? $AvgCallAtempt : 0 ) ."</td>");
	
     $dataSizeInterestId = array(); 
	 foreach( $CATEGORY_STATUS_CALL as $key => $category ) 
	 {
		foreach( $CALL_REASON_CATEGORY[$key] as $CallReasonId => $CallReasonName ) 
		{
			if( in_array($CallReasonId, array_keys($STATUS_INTERESTED))) 
			{
				$dataSizeInterestId[$CampaignId] += $CALL_CONTENT[$rowusers['UserId']][$CampaignId][$CallReasonId];
			}	
			
			$dataSizeReasonId[$key][$CallReasonId] +=$CALL_CONTENT[$rowusers['UserId']][$CampaignId][$CallReasonId];
			
			__("<td class='" . $CSS->setContentStyles('middle') . "'>" . ( $CALL_CONTENT[$rowusers['UserId']][$CampaignId][$CallReasonId] ? 
				$CALL_CONTENT[$rowusers['UserId']][$CampaignId][$CallReasonId] : 0 ) ."</td>");
	    } 
	 } 
	 
	$totsSalesRate  = round((( $dataSizeInterestId[$CampaignId]/$rows['data_utilize']) * 100), 2); 
	__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $totsSalesRate ? $totsSalesRate : 0 ) ."</td>");
 
 
  // AKUMUlATIF 
  
	$totSizeInterestId  += $dataSizeInterestId[$CampaignId];
	$totsDataSize		+= $rows['data_size'];
	$totsUtilizeSize 	+= $rows['data_utilize'];
	$totsNotUtilizeSize += $rows['data_not_utilize'];
	$totsCallAtemptSize += $rows['size_atempt'];
 
} 

// result data in foooter grid ---->
 
	$totalsAvgAtempt 		= ( $totsUtilizeSize ? round(($totsCallAtemptSize/$totsUtilizeSize ),2):0);
	$totalsResponseRate     = round((($totSizeInterestId / $totsUtilizeSize) * 100), 2);  
  
  __("<tr height='22'>");
		__("<td class='".$CSS->setBottomStyles('first')."'>&nbsp;<b>Total</b></td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>".$totsDataSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totsNotUtilizeSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totsUtilizeSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totsCallAtemptSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalsAvgAtempt ."</td>");
	
	 foreach( $CATEGORY_STATUS_CALL as $key => $category ) 
	 {
		foreach( $CALL_REASON_CATEGORY[$key] as $CallReasonId => $CallReasonName ){
			__("<td class='" . $CSS->setBottomStyles('middle') . "'>" . ( $dataSizeReasonId[$key][$CallReasonId] ? $dataSizeReasonId[$key][$CallReasonId] : 0 ) ."</td>");
	    } 
	 } 
	 
	__("<td class='".$CSS->setBottomStyles('middle')."'>". ( $totalsResponseRate ?  $totalsResponseRate: 0 ) ." </td>"); 	
	
	__("</tr>"); 	
__("</table>");
__("</div>");


endforeach;
