<?php
	$_order = array();
	for($i=1; $i<=count($fields); $i++)
	$_order[$i] = $i;
?>

<fieldset class="corner">
		<legend class="icon-application">&nbsp;&nbsp;Layout Upload / Insert </legend>
		<form name="layout_template">
		<table cellspacing="1" cellpadding="0px" align="left" width='100%'>
		<tr>
			<th class="ui-state-default ui-corner-top ui-state-focus first center">&nbsp;<?php echo form() -> checkbox('CheckAll',null,null,array("click"=>"Ext.Cmp('columns').setChecked();")); ?></th>
			<th class="ui-state-default ui-corner-top ui-state-focus first center">&nbsp;<b> Name </b></th>
			<th class="ui-state-default ui-corner-top ui-state-focus first center">&nbsp;<b> Order </b></th>
		</tr>	
		
		<?php 
			$n = 1;
			foreach( $fields as $k => $v ) { ?>
			<tr class="onselect">
				<td class="content-first" align="center" valign="center">&nbsp;<?php echo form() -> checkbox('columns',null,$v); ?></td>
				<td class="content-lasted" align="center"><?php echo form() -> input($v,'input_text long',$v,null,array('style'=>'color:green;width:290px;')); ?></td>
				<td class="content-lasted" align="center"><?php echo form() -> combo('order_by','select box',$_order,$n); ?></td>
			</tr>
		<?php 
			$n++;
		} ?>
		</table>
	 </form>
	</fieldset>