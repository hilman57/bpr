<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%">&nbsp;#</th>	
		<th class="font-standars ui-corner-top ui-state-default first center" width="5%" align="center">&nbsp;No</th>	
		<th class="font-standars ui-corner-top ui-state-default first center" width="8%" align="center">&nbsp;Table Name</th>		
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;Tempalate Name.</th>    
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;Mode Query.</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;File Type.</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;Black List.</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;X - Days.</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;Created Ts.</th>
		<th class="font-standars ui-corner-top ui-state-default first center" width="15%">&nbsp;Status.</th>
		
	</tr>
</thead>	
<tbody>
<?php
$no  = $num;
 foreach( $page -> result_assoc() as $rows )
 { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>
	<tr class="onselect" bgcolor="<?php echo $color; ?>">
		<td class="content-first" ><?php echo form() -> checkbox('TemplateId',NULL,$rows['TemplateId'], NULL, NULL,0);?></td>
		<td class="content-middle" ><?php echo $no; ?></td>
		<td class="content-middle" ><b><?php echo ($rows['TemplateTableName']?$rows['TemplateTableName']:'-'); ?></b></td>
		<td class="content-middle" ><?php echo ($rows['TemplateName']?$rows['TemplateName']:'-');?></td>
		<td class="content-middle" ><?php echo ($rows['TemplateMode']?$rows['TemplateMode']:'-');?></td>
		<td class="content-middle" ><?php echo ($rows['TemplateFileType']?$rows['TemplateFileType']:'-');?></td>
		<td class="content-middle center" ><?php echo ($rows['TemplateBlacklist']?$rows['TemplateBlacklist']:'-');?></td>
		<td class="content-middle center" ><?php echo ($rows['TemplateLimitDays']?$rows['TemplateLimitDays']:'-');?></td>
		<td class="content-middle" nowrap><?php echo ($rows['TemplateCreateTs']?$rows['TemplateCreateTs']:'-');?></td>
		<td class="content-lasted" align="justify"><?php echo ($rows['TemplateFlags']?'Active':'Not Active');?></td>
	</tr>	
</tbody>
<?php $no++; }; ?>
</table>