<?php 
__(javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()) )));
?>

<script type="text/javascript">
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 Ext.DOM.onload= (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
 /**
 ** javscript prototype system
 ** version v.0.1
 **/	
 
 $(function(){
	$('#toolbars').extToolbars({
		extUrl   :Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle :[['Create Template'],['Download Template'],['Enable'],['Disable'],['Delete']],
		extMenu  :[['CreteTemplate'],['DownloadTemplate'],['Enable'],['Disable'],['Delete']],
		extIcon  :[['application_edit.png'],['application_add.png'],['accept.png'],['cancel.png'],['cross.png']],
		extText  :true,
		extInput :true,
		extOption:[{
			render : 1,
		}]
	});
});

 
Ext.EQuery.TotalPage   = <?php echo $page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo $page -> _get_total_record(); ?>;


/**
 ** javscript prototype system
 ** version v.0.1
 **/
var Clear = function(){
	Ext.Cmp('table_name').setValue('');
	Ext.Cmp('mode_input').setValue('');
	Ext.Cmp('file_type').setValue('');
	Ext.Cmp('templ_name').setValue('');
	Ext.Cmp('list_columns').setText('');
}
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
Ext.DOM.CreteTemplate=function(){
	Ext.Dialog('panel-call-center',{
		ID		: 'panel-call-center1',
		type	: 'ajax/html',
		title	: 'Create Layout Template',
		url 	: Ext.DOM.INDEX+'/SetUpload/Create/',
		param 	: { t : 1 },
		style 	: {
			width 		: ($(window).width()-($(window).width()/2.5)),
			height 		: ($(window).height()-($(window).height()/4)),
			top 		: 0, 
			left 		: 0,
			scrolling 	: 1, 
			resize 		: 1,
			minimize 	: true,
			close 		: true
		}	
	}).open();
} 
 
Ext.DOM.Delete = function() {
	var TemplateId = Ext.Cmp('TemplateId').getValue();
	if( TemplateId.length > 0 )
	{
		if( Ext.Msg("Do you want to delete this template ?").Confirm()) {
			Ext.Ajax({
				url 	: Ext.DOM.INDEX +'/SetUpload/Delete/',
				method  : 'POST',
				param	: { 
					TemplateId : TemplateId
				},
				ERROR   : function(e){
					Ext.Util(e).proc(function(Delete){
						if(Delete.success){
							Ext.Msg('Delete Template').Success();
							Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
							Ext.EQuery.postContent();	
						}
						else{
							Ext.Msg('Delete Template').Failed();
						}
					});
				}
			
			}).post();
		}
	}
}
	
Ext.DOM.datas = {} 
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM._content_page = {
	custnav  : Ext.DOM.INDEX+'/SetUpload/index',
	custlist : Ext.DOM.INDEX+'/SetUpload/Content'			
 }	
	
	
Ext.DOM.Delimiter = function(select){
	if(select.value=='txt')
		Ext.Cmp('delimiter_type').disabled(false);
	else
		Ext.Cmp('delimiter_type').disabled(true);
}	
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
	Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
	Ext.EQuery.postContentList();	
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
var Enable = function()
{

	var TemplateId = Ext.Cmp('TemplateId').getValue();
	if( (TemplateId!='') )
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SetUpload/Enable/',
			method  : 'POST',
			param	: { 
				TemplateId : TemplateId
			},
			ERROR   : function(e)
			{
				var ERR = JSON.parse(e.target.responseText);
				if( ERR.success ){
					Ext.Msg('Enable Template Upload').Success();
					Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
					Ext.EQuery.postContent();	
				}
				else{
					Ext.Msg('Enable Template Upload').Failed();
				}
			}
		}).post();
	 }
	else{
		Ext.Msg('Enable Template Upload').Error();
	} 
}
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
var Disable = function()
{
	var TemplateId = Ext.Cmp('TemplateId').getValue();
	if( (TemplateId!='') )
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SetUpload/Disable/',
			method  : 'POST',
			param	: { 
				TemplateId : TemplateId
			},
			ERROR   : function(e)
			{
				var ERR = JSON.parse(e.target.responseText);
				if( ERR.success ){
					Ext.Msg('Disable Template Upload').Success();
					Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
					Ext.EQuery.postContent();	
				}
				else{
					Ext.Msg('Disable Template Upload').Failed();
				}
			}
		}).post();
	}
	else{
		Ext.Msg('Disable Template Upload').Error();
	} 
}

//////////////////
//Ext.DOM.OrderBy

Ext.DOM.OrderBy = function()
{
	var elem = Ext.Cmp('order_by').getName();
	var select = [];
	for( var i = 0; i<elem.length; i++)
		select[i] = elem[i].value;
		
	return select;	
}

/// insert templ_name

Ext.DOM.Insert = function()
{

 var list_check = Ext.Cmp('columns').getValue();
 var table_name = Ext.Cmp('table_name').getValue();
 var mode_input = Ext.Cmp('mode_input').getValue();
 var templ_name = Ext.Cmp('templ_name').getValue();
 var file_type  = Ext.Cmp('file_type').getValue();
 var delimiter_type  = Ext.Cmp('delimiter_type').getValue();
 var black_list = Ext.Cmp('black_list').getValue();
 var expired_days = Ext.Cmp('expired_days').getValue();
 var bucket_data = Ext.Cmp('bucket_data').getValue();
 var upload_modul = Ext.Cmp('upload_modul').getValue();
 var order_by	= Ext.DOM.OrderBy();
 
  
  if( Ext.Msg('Do you want to save this template').Confirm() ) 
  {	
		var param = [], alias = [];
			param['table_name'] = table_name;
			param['mode_input'] = mode_input;
			param['templ_name'] = templ_name;
			param['list_check'] = list_check;
			param['file_type']	= file_type;
			param['delimiter']  = delimiter_type;
			param['order_by'] = order_by;
			param['expired_days'] = expired_days;
			param['black_list'] = black_list;
			param['bucket_data'] = bucket_data;
			param['upload_modul'] = upload_modul;
			
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SetUpload/saveTemplate/',
			method  : 'POST',
			param   : Ext.Join(new Array( param,_getAliasName())).object(),
			ERROR : function(e){
				var ERR = JSON.parse(e.target.responseText);
				if( ERR.success ){
						Ext.Msg('Save Template Upload').Success();
						Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
						Ext.EQuery.postContent();	
				}
				else{
						Ext.Msg('Save Template Upload').Failed();
					}
			}
		}).post();
	}
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/	
	
Ext.DOM.Update = function()
{
	var list_check 	= Ext.Cmp('columns').getValue();
	var list_keys  	= Ext.Cmp('columns_keys').getValue();
	var table_name 	= Ext.Cmp('table_name').getValue();
	var mode_input 	= Ext.Cmp('mode_input').getValue();
	var templ_name 	= Ext.Cmp('templ_name').getValue();
	var file_type  	= Ext.Cmp('file_type').getValue();
	var order_by	= Ext.DOM.OrderBy();
	var bucket_data = Ext.Cmp('bucket_data').getValue();
	var delimiter_type= Ext.Cmp('delimiter_type').getValue();
	var black_list 	 = Ext.Cmp('black_list').getValue();
	var expired_days = Ext.Cmp('expired_days').getValue();
 
	 
	if( Ext.Msg('Do you want to save this template').Confirm() )
	{	
		var param = [], alias = [];
		param['table_name'] = table_name;
		param['mode_input'] = mode_input;
		param['templ_name'] = templ_name;
		param['list_check'] = list_check;
		param['file_type']	= file_type;
		param['list_keys']  = list_keys;
		param['delimiter']  = delimiter_type;	
		param['order_by']   = order_by;
		param['expired_days'] = expired_days;
		param['black_list'] = black_list;
		param['bucket_data'] = bucket_data;
			
		
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX +'/SetUpload/saveTemplate/',
				method  : 'POST',
				param   : Ext.Join(new Array( param,_getAliasName())).object(),
				ERROR : function(e){
					var ERR = JSON.parse(e.target.responseText);
					if( ERR.success ){
						Ext.Msg('Save Template Upload').Success();
						Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
						Ext.EQuery.postContent();	
					}
					else{
						Ext.Msg('Save Template Upload').Failed();
					}
				}
			}).post();
	}
}


/**
 ** javscript prototype system
 ** version v.0.1
 **/	
 
Ext.DOM.SaveTemplate = function()
{
	switch ( Ext.Cmp('mode_input').getValue() )
	{
		case 'insert' :  Ext.DOM.Insert(); break;
		case 'update' :  Ext.DOM.Update(); break;
		default : 
			Ext.Msg("Please select mode input !").Info();
		break;
	}
}

	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 var _getAliasName = function()
 {
	var data = [], 
		element = Ext.Cmp('columns').getValue();
		for( var i = 0; i< element.length; i++ )
		{
			data[element[i]] = Ext.Cmp(element[i]).getValue(); 
		} 
		
	return data;	
 }

/**
 ** javscript prototype system
 ** version v.0.1
 **/
Ext.DOM.getTableColumns = function(select)
{

 var tables = Ext.Cmp('table_name').getValue();
	if( Ext.Cmp('table_name').empty() == false ){
		if( select.value!='' )
		{
			  
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/SetUpload/setTemplate/', 
				method 	: 'GET', 
				param   : {
					tables : tables,
					method : select.value
				}
			}).load('list_columns');
		}
		else{
			Ext.Cmp('list_columns').setText('');
		}	
	} 
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
Ext.DOM.DownloadTemplate = function()
	{
		var TemplateId = Ext.Cmp('TemplateId').getValue();
		if( (TemplateId!='') )
		{
			var WindowWin = new Ext.Window({
				url   : Ext.DOM.INDEX+'/SetUpload/DownloadTemplate/', 
				param : {
					TemplateId : TemplateId,
				}
			}).newtab();
		}
	}	
/**
 ** javscript prototype system
 ** version v.0.1
 **/	
	var getListCheck = function(object)
	{
		var alias_name = 'alias_name_'+object.value;
		var order_name = 'order_name_'+object.value;
		
		if( object.checked)
		{
			Ext.dom(alias_name).value = object.value;
			Ext.dom(order_name).value = 0;
		}
		else{
			Ext.dom(alias_name).value = '';
			Ext.dom(order_name).value = '';
		}	
	}
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/

	var ReturnNextForm = function(object)
	{
		Ext.checkedAll(object);
		var list_html_data = '';
		var list_check_data = Ext.checkedValue(object);
	}
	
</script>