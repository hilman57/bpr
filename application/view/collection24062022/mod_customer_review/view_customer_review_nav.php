<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	cust_rev_account_status : "<?php echo _get_exist_session('cust_rev_account_status');?>",
	cust_rev_agent_id 		: "<?php echo _get_exist_session('cust_rev_agent_id');?>",
	cust_rev_call_status 	: "<?php echo _get_exist_session('cust_rev_call_status');?>",
	cust_rev_campaign_id 	: "<?php echo _get_exist_session('cust_rev_campaign_id');?>",
	cust_rev_cust_id 		: "<?php echo _get_exist_session('cust_rev_cust_id');?>",
	cust_rev_cust_name 		: "<?php echo _get_exist_session('cust_rev_cust_name');?>",
	cust_rev_recsource 		: "<?php echo _get_exist_session('cust_rev_recsource');?>",
	cust_rev_start_amountwo : "<?php echo _get_exist_session('cust_rev_start_amountwo');?>",
 	cust_rev_end_amountwo 	: "<?php echo _get_exist_session('cust_rev_end_amountwo');?>",
	cust_rev_start_date 	: "<?php echo _get_exist_session('cust_rev_start_date');?>",
	cust_rev_end_date 		: "<?php echo _get_exist_session('cust_rev_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/ModCustomerReview/index/',
	custlist : Ext.DOM.INDEX+'/ModCustomerReview/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('Customer_Review').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('Customer_Review').Clear();
	Ext.DOM.searchCustomer();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.Preview = function() 
{
  var FavouriteId = Ext.Cmp('FavouriteId').getChecked();
  
  if( FavouriteId.length < 1 ){
	Ext.Msg('Pelase select rows ').Info();
	return false;
  }
  
  if( FavouriteId.length > 1 ){
	Ext.Msg('Pelase select on rows ').Info();
	return false;
  }
  
  Ext.Window 
  ({
	 url    : Ext.DOM.INDEX+"/ModCustomerReview/Preview/",
	 method : 'GET', 
	 param  : {
		FavouriteId : FavouriteId
	},
		
	scrollbars : 1,
	resizable  : 1, 
	width : ( $(window).width() - ( $(window).width()/1.5)),
	height : ( $(window).height() - ( $(window).height()/2.5))
	
  }).popup();
}		


// Ext.DOM.Unblock

Ext.DOM.UnblockAll = function(){
	//var FavouriteId = Ext.Cmp('FavouriteId').getChecked();
	Ext.Ajax
  ({
	 url    : Ext.DOM.INDEX+"/ModCustomerReview/UnblockAll/",
	 method : 'POST', 
	 param  : { 
			//FavouriteId : FavouriteId
		},
	ERROR 	: function(e){
		Ext.Util(e).proc(function(response){
			if( response.success > 0 ){
				Ext.Msg('Unblock').Success();
				Ext.DOM.searchCustomer();
			} else {
				Ext.Msg('Unblock').Failed();
			}
	   });
	}
  }).post();
}

Ext.DOM.Unblock = function(){
var FavouriteId = Ext.Cmp('FavouriteId').getChecked();
	if( FavouriteId.length < 1 ){
	Ext.Msg('Pelase select rows ').Info();
	return false;
  }
  
  Ext.Ajax
  ({
	 url    : Ext.DOM.INDEX+"/ModCustomerReview/Unblock/",
	 method : 'POST', 
	 param  : { 
			FavouriteId : FavouriteId
		},
	ERROR 	: function(e){
		Ext.Util(e).proc(function(response){
			if( response.success ){
				Ext.Msg('Unblock').Success();
				Ext.DOM.searchCustomer();
			} else {
				Ext.Msg('Unblock').Failed();
			}
	   });
	}
  }).post();
}


// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Preview'],['Unblock'],['Unblock All']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['Preview'],['Unblock'],['UnblockAll']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['application_edit.png'],['key_delete.png'],['key_delete.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="Customer_Review">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('cust_rev_campaign_id','select auto', $CampaignId, _get_exist_session('cust_rev_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('cust_rev_agent_id','select auto', $Deskoll, _get_exist_session('cust_rev_agent_id'));?></td>
			<td class="text_caption bottom"> # Last Call date &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('cust_rev_start_date','input_text date', _get_exist_session('cust_rev_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('cust_rev_end_date','input_text date', _get_exist_session('cust_rev_end_date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('cust_rev_cust_id','input_text long', _get_exist_session('cust_rev_cust_id'));?></td>
			<td class="text_caption bottom"> # Account Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('cust_rev_account_status','select auto', $AccountStatus, _get_exist_session('cust_rev_account_status'));?></td>
			<td class="text_caption bottom"> # Recsource &nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('cust_rev_recsource','input_text long', _get_exist_session('cust_rev_recsource'));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('cust_rev_cust_name','input_text long', _get_exist_session('cust_rev_cust_name'));?></td>
			<td class="text_caption bottom"> # Last Call Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('cust_rev_call_status','select auto', $LastCallStatus, _get_exist_session('cust_rev_call_status'));?></td>
			<td class="text_caption bottom"> # Amount WO&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('cust_rev_start_amountwo','input_text box', null, _get_exist_session('cust_rev_start_amountwo'));?>
				&nbsp; <span>to</span>&nbsp;	
				<?php echo form()->input('cust_rev_end_amountwo','input_text box', null, _get_exist_session('cust_rev_end_amountwo'));?>
			</td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->