<?php 
if(!function_exists('Notes')){  
 function Notes($debId) 
{
	$UI =& get_instance();
	$UI->db->select('a.CallHistoryNotes');
	$UI->db->from('t_gn_callhistory a ');
	$UI->db->where('a.CustomerId', $debId);
	$UI->db->order_by('a.CallHistoryId', 'DESC');
	$UI->db->limit(1);
	$qry = $UI->db->get();
	
	if( $qry->num_rows() > 0 )
	{
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
 }
}	

?>


<?php if( is_array($contact_data) ) : ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Customer Review</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.overwriter.css?time=<?php echo time();?>" />
	<style> .color-blue{ color:#031648;} </style>
	
</head>
<body>
	<fieldset class='corner' style='margin:10px;'>
	<legend class="icon-customers">&nbsp;&nbsp;&nbsp;Review Detail</legend>	
	<div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
		<table cellpadding="0" cellspacing="5" width="99%" align="center">
		<tr>
			<td class="text_caption font-standars13 "width="20%" nowrap># Account Number</td>
			<td class="">:</td> 
			<td class=" font-standars13 color-blue "> <b><?php echo $contact_data['deb_acct_no'];?></b></td>
		</tr>

		<tr>
			<td class="text_caption font-standars13  " nowrap># Customer Name</td>
			<td class="">:</td>  
			<td class=" font-standars13 color-blue "><?php echo $contact_data['deb_name'];?></td>
		</tr>

		<tr>
			<td class="text_caption font-standars13  " nowrap># Call To Number</td>
			<td class="">:</td>  
			<td class=" font-standars13 color-blue ">
				<b style='color:red;'><?php echo _getMasking($data_coll['PhoneNumber']);?><b>
			</td>
		</tr>

		<tr>
			<td class="text_caption font-standars13  " nowrap># Call Freq</td>
			<td class="">:</td>  
			<td class=" font-standars13 color-blue ">( <?php echo $data_coll['LastCounterCall'];?> )&nbsp;/&nbsp;<?php echo _getDateTime($data_coll['LastCallDateTs']);?> </td>
		</tr>

		<tr>
			<td class="text_caption font-standars13  " nowrap># Agent Assignment</td>
			<td class="">:</td>  
			<td class=" font-standars13 color-blue "><?php echo strtoupper($data_coll['id']);?> - <?php echo $data_coll['full_name'];?> </td>
		</tr>
		<tr>	
			<td class="text_caption font-standars13  " nowrap># Account Status</td>
			<td class="">:</td>  
			<td class=" font-standars13 color-blue "><?php echo $dropdown['COLL_DROPDOWN_CLS'][$contact_data[deb_call_status_code]];?> </td>
		</tr>

		<tr>
			<td class="text_caption font-standars13  " nowrap># Call Status</td>
			<td class="">:</td>  
			<td class=" font-standars13 color-blue "><?php echo $dropdown['COLL_DROPDOWN_CLS'][$contact_data[deb_prev_call_status_code]];?> </td>
		</tr>

		<tr nowrap>
			<td class="text_caption font-standars13 "># Account History</td>
			<td class="">:</td> 
			<td class="font-standars13 color-blue "><?php echo Notes($contact_data[deb_id]);?> </td>
		</tr>
		</table>	
		</div>	
	</fieldset>
</body>
</html>	
<?php endif; ?>