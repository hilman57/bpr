<div class="ui-widget-form-table-compact" style="width:99%;">
	<div class="ui-widget-form-row">
		<div class="ui-widget-form-cell ui-widget-content-top " style="width:70%">
			<fieldset class="corner" style="border-radius:3px;margin:-20px 0px 0px -15px;">
				<legend class="icon-menulist"> <span style="margin-left:8px;">List Data</span></legend>
				<div id="ui-widget-content-debitur-page" style="margin-top:-5px;"></div>
			</fieldset>
		</div>

		<div class="ui-widget-form-cell ui-widget-content-top " style="width:30%;">
			<!-- <legend class="edit-users-x"> <span style="margin-left:8px;">User Option</span></legend> -->
			<form name="frmSwapFilterData">
				<div class="ui-widget-form-table-compact">
					<?php //echo form()->input('swp_from_page_record', 'input_text box', 20);?>
					<input type="hidden" name="swp_from_page_record" id="swp_from_page_record" class="input_text box"
						value="20">
				</div>
			</form>



			<fieldset class="corner" style="border-radius:3px;margin-top:10px;">
				<legend class="edit-users-x"><span style="margin-left:8px;">User Action</span></legend>
				<form name="frmSwapActionData">
					<div class="ui-widget-form-table-compact">

						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question Type</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left">
								<?php echo form()->checkbox('swap_type', null, 2,array("change" =>"Ext.Cmp('swap_type').oneChecked(this);ActionCheckSwap(this);"));?><span>Checked</span>

							</div>
						</div>

						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question Data Size</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left">
								<?php echo form()->input('swp_total_data', 'input_text long');?></div>
						</div>

						<!-- test -->
						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question to Kategori</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left" id="ui-swp-kategori">
								<?php echo form()->combo('swp_to_kategori', 'select long',null, null, array('onchange' => 'pickCategory(this.val)'));?>
								<?php //echo form()->checkbox('swp_to_user_deskoll', 'select long');?>
							</div>
						</div>
						<!-- test -->

						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question to level</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left" id="ui-swp-user-lavel">
								<?php //echo form()->combo('swp_to_user_level', 'select long',Level(), null, array("change" => "new ShowUserByLevel(this);") );?>
								<select type="combo" name="swp_to_user_level" id="swp_to_user_level" class="select long"
									onchange="new ShowUserByLevel(this);">
									<option value=""> --choose --</option>
									<option value="4">DESKOLL</option>
									<!-- <option value="3">SUPERVISOR</option>\
									<option value="13">TEAM LEADER  (TL)</option> -->
								</select>
							</div>
						</div>



						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question to Leader</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left" id="ui-swp-user-leader">
								<?php echo form()->combo('swp_to_user_leader', 'select long');?></div>
						</div>

						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question to Deskoll</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left" id="ui-swp-user-deskoll">
								<?php echo form()->combo('swp_to_user_deskoll', 'select long');?>
								<?php //echo form()->checkbox('swp_to_user_deskoll', 'select long');?>
							</div>
						</div>

						<!-- <div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Swap Amount</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left">
								<?php //echo form()->input('swp_to_data_amount', 'input_text long');?></div>
						</div> -->


						<!-- <div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption">Question Method</div>
							<div class="ui-widget-form-cell center">:</div>
							<div class="ui-widget-form-cell left">
								<?php //echo form()->checkbox('swap_methode', null,1,array("change" =>"Ext.Cmp('swap_methode').oneChecked(this);ActionCheckSwap(this);"));?><span>Random</span>
								&nbsp;
								<?php //echo form()->checkbox('swap_methode', null,2,array("change" =>"Ext.Cmp('swap_methode').oneChecked(this);ActionCheckSwap(this);"), array("checked" => true));?><span>Urutan</span>
							</div>
						</div> -->
						<input type="hidden" class="" name="swap_methode" id="swap_methode" value="2" checked="1"
							onchange="Ext.Cmp('swap_methode').oneChecked(this);ActionCheckSwap(this);">

						<div class="ui-widget-form-row">
							<div class="ui-widget-form-cell text_caption"></div>
							<div class="ui-widget-form-cell center"></div>
							<div class="ui-widget-form-cell left">
								<?php echo form()->button('btnshow', 'button assign', "Submit", array("click" => "new SubmitSwapData();") );?>
								<?php echo form()->button('btnshow', 'button close', "&nbsp;Exit&nbsp;&nbsp;&nbsp;", array("click" => "new ExitSwapData();"));?>
							</div>
						</div>

					</div>
				</form>

			</fieldset>
		</div>

	</div>
</div>