<?php ?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%" align="center">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('chk_config').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left"><span class="header_order" id ="a.CallReasonCode" onclick="Ext.EQuery.orderBy(this.id);">Config Code </b></span></th>        
        <th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left"><span class="header_order" id ="a.CallReasonDesc" onclick="Ext.EQuery.orderBy(this.id);">Config Name </b></span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left"><span class="header_order" id ="a.CallReasonDesc" onclick="Ext.EQuery.orderBy(this.id);">Config Value </b></span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default lasted center" align="left"><span class="header_order" id ="a.CallReasonContactedFlag" onclick="Ext.EQuery.orderBy(this.id);">Status</span></th>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	$ConfigValue = ( $rows['ConfigPassword'] ? _setPassword($rows['ConfigValue']) : $rows['ConfigValue'] );
	
?>	
	<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"><input type="checkbox" value="<?php echo $rows['ConfigID']; ?>" name="chk_config" id="chk_config"></td>
		<td class="content-middle center"><?php echo $no ?></td>
		<td class="content-middle"><b><?php echo $rows['ConfigCode']; ?></b></td>
		<td class="content-middle"><?php echo $rows['ConfigName']; ?></td>
		<td class="content-middle"><?php echo $ConfigValue; ?></td>
		<td class="content-lasted center"><?php echo ($rows['ConfigFlags'] ? 'Active':'Not Active'); ?></td>
	</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>



