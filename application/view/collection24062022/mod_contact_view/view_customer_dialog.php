<table border=0 align="left" cellspacing=1 cellspacing="1px">
	<tr>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;No</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Campaign Code</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Owner Name</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Policy Number</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Product Category</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Issued Date</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Last Call Date</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Call Result</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Call Status</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Attempt</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Sum of Policy</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Assigned To</th>
		<th class="font-standars ui-corner-top ui-state-default first center">&nbsp;Action</th>
	</tr>
	<?php
	
	if(count($data) > 0)
	{
		$no = 0;
		foreach($data as $key => $value)
		{
			$no++;
	?>
		<tr class="onselect">
			<td class="content-first"><?php __($no); ?></td>
			<td class="content-middle"><?php __($value['CampaignCode']); ?></td>
			<td class="content-middle"><?php __($value['CustomerFirstName']); ?></td>
			<td class="content-middle"><?php __($value['PolicyNumber']); ?></td>
			<td class="content-middle"><?php __($value['PolicyProductName']); ?></td>
			<td class="content-middle"><?php __($value['PolicyIssDate']); ?></td>
			<td class="content-middle"><?php __($value['CustomerUpdatedTs']); ?></td>
			<td class="content-middle"><?php __($value['CallReasonCategoryName']); ?></td>
			<td class="content-middle"><?php __($value['CallResult']); ?></td>
			<td class="content-middle"><?php __($value['CallAttempt']); ?></td>
			<td class="content-middle"><?php __($value['SumPolicy']); ?></td>
			<td class="content-middle"><?php __(($value['full_name']?$value['full_name']:'-')); ?></td>
			<td class="content-lasted"><input type="button" class="button assign" value='Claim & Follow Up' id="ButtonUserNext" <?php echo ($this -> EUI_Session -> _get_session('HandlingType') != USER_AGENT_OUTBOUND && $this -> EUI_Session -> _get_session('HandlingType')!=USER_AGENT_INBOUND ? "onclick=\"alert('Only Caller Can Claim Policy!');\"" :  "onclick=\"Ext.DOM.ClaimPolicy('".$value['CustomerId']."','".$value['full_name']."');\"" ); ?> ></td>
		</tr>
	<?php
		}
	}
	
	?>
	<tr>
		<td class="font-standars ui-corner-top ui-state-default first center" colspan="13">&nbsp;</td>
	</tr>
</table>