<?php __(javascript()); 
// print_r($_SESSION);
?>
<script type="text/javascript">
/**
 ** javscript prototype system
 ** version v.0.1
 **/

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
Ext.DOM.Reason 		= [];
Ext.DOM.datas  	 	= {}
Ext.DOM.handling 	= '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
Ext.DOM.datas = 
 {
	flw_cust_name 	 	: '<?php echo _get_exist_session('flw_cust_name');?>',
	flw_campaign 	 	: '<?php echo _get_exist_session('flw_campaign');?>',
	flw_cif_number 		: '<?php echo _get_exist_session('flw_cif_number');?>', 
	flw_policy_number	: '<?php echo _get_exist_session('flw_policy_number');?>',
	flw_product_name 	: encodeURIComponent('<?php echo _get_exist_session('flw_product_name');?>'),
	flw_attempt_count	: '<?php echo _get_exist_session('flw_attempt_count');?>',
	flw_call_reason		: '<?php echo _get_exist_session('flw_call_reason');?>',
	order_by 			: '<?php echo _get_exist_session('order_by');?>',
	type	 			: '<?php echo _get_exist_session('type');?>',
	flw_dc	 			: '<?php echo _get_exist_session('flw_dc');?>'
}
//console.log(Ext.DOM.datas);
			
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
Ext.DOM.navigation =  {
	custnav  : Ext.DOM.INDEX+'/ModContactView/index/',
	custlist : Ext.DOM.INDEX+'/ModContactView/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
var searchCustomer = function() {
  var param = Ext.Serialize('flwCustomers').getElement();
	var arr = [];
	
	for(var i in param) {
		if(i == 'flw_product_name') {
			arr[i] = encodeURIComponent(param[i]);
		}
		else{
			arr[i] = param[i];
		}
	}

  Ext.EQuery.construct(Ext.DOM.navigation,Ext.Join([
			arr
		]).object());
		
	Ext.EQuery.postContent()
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('flwCustomers').Clear();
	Ext.DOM.searchCustomer();
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
 Ext.DOM.gotoCallCustomer = function()
 {
	var CustomerId  = Ext.Cmp('CustomerId').getChecked();
	if( CustomerId!='')
	{	
		if( CustomerId.length == 1 ) {
			Ext.ActiveMenu().NotActive();
			Ext.EQuery.Ajax
			({
				url 	: Ext.DOM.INDEX +'/ModContactView/ContactDetail/',
				method  : 'GET',
				param 	: {
					CustomerId : CustomerId,
					ControllerId : Ext.DOM.INDEX +'/ModContactView/index/', 
					
				}
			});
		}
		else{ Ext.Msg("Select One Customers !").Info(); }			
	}
	else{ Ext.Msg("No Customers Selected !").Info(); }	
 }

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
$(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear']],
		extMenu  : [['searchCustomer'],['resetSeacrh']],
		extIcon  : [['zoom.png'],['cancel.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
	$('#cust_dob').datepicker({showOn: 'button', buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', buttonImageOnly: true, dateFormat:'dd-mm-yy',readonly:true});
});

Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}

</script>

<!-- start : content -->
<fieldset class="corner" onKeyDown="enterSearch(event)">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
  <form name="flwCustomers">

	<table cellpadding="8px;">
		<tr>
			<td class="text_caption"> Policy Owner Name</td>
			<td><?php echo form() -> input('flw_cust_name','input_text long', _get_exist_session('flw_cust_name'));?></td>
			<td class="text_caption"> CIF Number</td>
			<td><?php echo form() -> input('flw_cif_number','input_text long', _get_exist_session('flw_cif_number'));?></td>
			<td class="text_caption"> Policy Number</td>
			<td><?php echo form() -> input('flw_policy_number','input_text long', _get_exist_session('flw_policy_number'));?></td>
			<td class="text_caption"> Caller</td>
			<td><?php echo form() -> combo('flw_dc','select long', (isset($Agent) ? $Agent :null ) , _get_exist_session('flw_dc'));?></td>
		</tr>
		<tr>
			<td class="text_caption"> Campaign</td>
			<td><?php echo form() -> combo('flw_campaign','select auto', (isset($cmp_active)?$cmp_active :null ) , _get_exist_session('flw_campaign'));?></td>
			<td class="text_caption"> Product Category</td>
			<td><?php echo form() -> combo('flw_product_name','input_text long',(isset($ProductId)? $ProductId :null ), _get_exist_session('flw_product_name'));?></td>			
			<td class="text_caption"> Call Result</td>
			<td><?php echo form() -> combo('flw_call_reason','select auto', (isset($CallResult)?$CallResult :null ) , _get_exist_session('flw_call_reason'));?></td>
			<td class="text_caption"> Attempt</td>
			<td><?php echo form() -> input('flw_attempt_count','input_text long', _get_exist_session('flw_attempt_count'));?></td>
		</tr>
	</table>
	 </form>
</div>
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
<div class="content_table" ></div>
<div id="pager"></div>
</div>
</fieldset>	

	<!-- stop : content -->
	
	
	
	
	