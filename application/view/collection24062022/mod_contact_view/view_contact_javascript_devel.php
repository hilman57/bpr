<script>

/*
 * Global Atemp Call 
 * by Default is zeroo 
 
 NOTES 20-11-2014
 
 - line 84 : function DisabledActivity(); => Remark Disable chkLabel
 - line 134 : function Ready; => set enable chkLabel
 
 */
 
var CallAtempt = 0;

var SaveCounterPolis = [];

var OpenFormFlag = false;


  
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
var reason = [];

Ext.DOM.DisableQuestion = function( option ){
	for ( var elm in Ext.Serialize('frmQuestion').getElement() ) {
		Ext.Cmp(elm).disabled(option);
	}
}

Ext.DOM.ResetValueOfQuestion = function(){
	for ( var elm in Ext.Serialize('frmQuestion').getElement() ) {
		Ext.Cmp(elm).setValue('');
	}
}


/*  status CallInterest policy **/

Ext.DOM.CallInterest = function(){
return( Ext.Ajax
({
	url : Ext.DOM.INDEX +'/SetCallResult/getEventType/',
	method : 'GET',
	param :{
		CallResultId : Ext.Cmp('CallResult').getValue()
	}
 }).json());	
}


 
/* FU_FOLLOWUP **/

Ext.DOM.FuFollowUpByCheckedList = function() {	
	return Ext.Cmp('FU_FOLLOWUP').getChecked();
}


/* Ext.DOM.FuFollowUpByCheckedList */

Ext.DOM.FuFollowUpByCheckedName = function() 
{
	var conds = true;
	var elem = Ext.Cmp('FU_FOLLOWUP').getName();
	var chkname = [];
	for( var i = 0; i<elem.length; i++)
	{	
		if( elem[i].checked) 
		{
			var require = 'NOTES_'+elem[i].id;
			if( Ext.Cmp(require).empty() ) {
				Ext.Msg(elem[i].title+' is empty ').Info();
				Ext.query('#'+require).focus();
				conds = false;
				return false;
			}
		}
    }
	return conds;
}

 
/*  status keberadaan policy **/
 
Ext.DOM.PolicyReady = function(){
return( Ext.Ajax ({
	url : Ext.DOM.INDEX +'/SrcCustomerList/FollowUpCollection/',
	method : 'GET',
	param :{
		CustomerId : Ext.Cmp('CustomerId').getValue()
	}
 }).json());
} 

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */ 
Ext.DOM.initFunc = { 
	validParam : false,
	isCallPhone : false,
	isRunCall : false,
	isCall : false,
	isHangup : false,
	isCancel : true,
	isSave : false,
	isScriptId : 0,	
	isRelease : false,
	isComplete : false
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.DisabledActivity = function() {
	if( Ext.DOM.initFunc.isCallPhone !=true) {
		Ext.Cmp('CallStatus').disabled(true);
		Ext.Cmp('CallResult').disabled(true); 
	}
	else {
		Ext.Cmp('CallStatus').disabled(false);
		Ext.Cmp('CallResult').disabled(false);
	}
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.query(function(){
  Ext.query("#tabs" ).tabs();
  Ext.query('#toolbars').extToolbars
  ({
		extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle  : [['Add Phone'],[],[]],
		extMenu   : [['Ext.DOM.UserWindow'],[],[]],
		extIcon   : [['monitor_edit.png'],[],[]],
		extText   : true,
		extInput  : true,
		extOption  : [{
				render : 1,
				type   : 'combo',
				header : 'Script ',
				id     : 'v_result_script', 	
				name   : 'v_result_script',
				triger : 'ShowWindowScript',
				width  : 220,
				store  : [
						Ext.Ajax
						({
							url		: Ext.DOM.INDEX+'/SetCampaignScript/getScript/', 
							param	: { 
								CustomerId : Ext.Cmp('CustomerId').getValue() 
							}
									
						}).json()]
			},{
				render : 2,
				type : 'label',
				label : '<span style="color:#dddddd;">-</span>',
				name : 'html_ajax_loading',
				id   : 'html_ajax_loading'
			}]
	});
	
  Ext.query('.date').datepicker({dateFormat:'dd-mm-yy'});	
  Ext.DOM.DisabledActivity();
  // Ext.Cmp('chklabel').disabled(false);
});
/* 
 * @ def : ShowWindowScript
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.ShowWindowScript = function(ScriptId)
{
	Ext.Cmp('ScriptSelectId').setValue(ScriptId);
	
	var WindowScript = new Ext.Window 
	({
			url    : Ext.DOM.INDEX +'/SetCampaignScript/ShowCampaignScript/',
			name    : 'WinProduct',
			height  : (Ext.Layout(window).Height()),
			width   : (Ext.Layout(window).Width()),
			left    : (Ext.Layout(window).Width()/2),
			top	    : (Ext.Layout(window).Height()/2),
			param   : {
				ScriptId : Ext.BASE64.encode(ScriptId),
				Time	 : Ext.Date().getDuration()
			}
	}).popup();
		
	// close window 	
	if( ScriptId =='' ){
		WindowScript._w.close();
	}	
}


 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.dialCustomer = function(){
if( Ext.Cmp('ScriptSelectId').empty() ){
	Ext.Msg('Please Select Script').Info(); 
	$('#v_result_script').focus();
}
else if( Ext.Cmp("CallingNumber").empty() ){
	Ext.Msg('Please Select Phone Number').Info();
	$('#PhoneNumber').focus();
}
else if( Ext.DOM.initFunc.isRunCall == false ){ // dont call if in run call  

 if( Ext.DOM.initFunc.isComplete == false ) 
 {
	Ext.DOM.DisableQuestion(false); // open question
	ExtApplet.setData({   
		Phone : Ext.Cmp("CallingNumber").getValue(), 
		CustomerId  : Ext.Cmp("CustomerId").getValue() 
	}).Call();
		
		Ext.DOM.initFunc.isCall = true;
		Ext.DOM.initFunc.isCallPhone = true;
		Ext.DOM.initFunc.isCancel = false;
		Ext.DOM.initFunc.isRelease = false;
		
		window.setTimeout(function(){
			Ext.DOM.DisabledActivity();
			Ext.DOM.initFunc.isRunCall = true;
		},1000);
	}
	else{
		Ext.Msg('Please save all policy ').Info();	
	}	
 }
 else{
	Ext.Msg('Call is run').Info();	
 }
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.hangupCustomer =function()
{
 if( Ext.DOM.initFunc.isCall == true ) 
 {	
	Ext.DOM.initFunc.isRunCall = false;
	Ext.DOM.initFunc.isCancel = false;
	Ext.DOM.initFunc.isHangup = true;
	ExtApplet.setHangup();
}
else{
	Ext.Msg('Please call before ').Info();
}	

return;
	
} 
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.getCallReasultId = function(combo)
{
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SrcCustomerList/setCallResult/',
		method  : 'GET',
		param  : {
			CategoryId : combo.value
		}	
	}).load("DivCallResultId");	
	

	Ext.Cmp('date_call_later').setValue('');
	Ext.Cmp('hour_call_later').setValue('00');
	Ext.Cmp('minute_call_later').setValue('00');
	Ext.Cmp('date_call_later').disabled(true);
	Ext.Cmp('hour_call_later').disabled(true);
	Ext.Cmp('minute_call_later').disabled(true);
	Ext.Cmp('CallComplete').getElementId().checked=false;
	Ext.Cmp('CallComplete').getElementId().disabled=false;
	Ext.DOM.DisableQuestion(true);
	Ext.DOM.ResetValueOfQuestion();
	Ext.DOM.DisableAddress(true);
	
							
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

 
Ext.DOM.CallSessionId = function(){
	try{
		return ( typeof (ExtApplet.ctiCallSessionId ) =='undefined' ? 
				'NULL': ExtApplet.ctiCallSessionId );
	}catch(a){
		return "";
	}
}

/**
 * get Json Policy Status 
 *
 */
 
Ext.DOM.PolicyDetailStatus = function( PolicyId ){
	var Policy = Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SrcCustomerList/PolicyStatusData/',
		method 	: 'POST',
		param 	: {
			PolicyId : ( PolicyId ? PolicyId : '' )
		}		
	}).json();
	
// set handle call status data **/

	if( typeof(Policy)=='object') 
	{
		if( Policy.success==1)
		{
			// please don't remove this 3 lines, just in case I don't remember
			// Ext.Cmp('CallStatus').setValue(Policy.data.CallReasonCategoryId);
			// Ext.Cmp('CallResult').setValue(Policy.data.CallReasonId);
			if( Policy.data.CallComplete==1 )
				Ext.Cmp('CallComplete').getElementId().checked = true;
			else
			{
				Ext.Cmp('CallComplete').getElementId().checked = false;
				if(Ext.Cmp('QueueId').getValue() != "")
				{
					Ext.Cmp('CallStatus').setValue(Policy.data.CallReasonCategoryId);
					Ext.Cmp('CallResult').setValue(Policy.data.CallReasonId);
				}
			}
		}
		else
		{
			Ext.Cmp('CallStatus').setValue('');
			Ext.Cmp('CallResult').setValue('');
			Ext.Cmp('CallComplete').getElementId().checked = false;
		}
	}
}
 
 

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.NextActivity = function(){
if( Ext.DOM.initFunc.isRunCall==true ){
	Ext.Msg('Please Save Status Or Hangup Call ').Info();
	return false;
}
else
{
	var CustomerId = Ext.Cmp('CustomerId').getValue();
	if( CustomerId ) 
	{
		Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX +'/SrcCustomerList/NextActivity/',
			method 	: 'POST',
			param 	: {
				ts : ""
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(response)
				{
					var next_id = 0, _curr = 0, _next_customers = 0, num = 1;
					
					var last_id = response.list[response.list.length-1];
					var next = (parseInt(CustomerId)+1);
					/* console.log(next);
					console.log(last_id); */
					
					while(true)
					{
						var _next = next;
						
						for( var i=0; i< response.list.length; i++){
							if( response.list[i]==_next ) {
								next_id = i;
								break;
							}
						}
						
						if(next_id > 0 || _next > last_id)
						{
							break;
						}
						next++;
					}
					
					/* for( var i=0; i< response.list.length; i++){
						if( response.list[i]==CustomerId ) {
							_curr = i;  next_id = (_curr + 1 );
							break;
						}
						num++;
					} */
				/*
				 * console.log('CUSTID LAST :'+ CustomerId )			
				 * console.log('CURR_ID :'+ _curr)		
				 * console.log('NEXT_ID :'+ next_id)	
				 * console.log('CUSTID NEXT '+ response.list[next_id]);
				 */
				
				var CustomerIdNext = response.list[next_id];
				if(Ext.DOM.SumPolicy()){
					/*****/
					if(CustomerIdNext) 
					{
						Ext.ActiveMenu().NotActive();
						Ext.EQuery.Ajax
						({
							url 	: Ext.DOM.INDEX +'/SrcCustomerList/ContactDetail/',
							method  : 'GET',
							param 	: {
								CustomerId : CustomerIdNext,
								ControllerId : Ext.DOM.INDEX +'/SrcCustomerList/index/'
							}
						});
					}
					else{
						Ext.Msg('this last customers').Info();
					}
					/*****/
				}else{
					alert('Please Save All Policies');
				}
			 });
		 }
	 }).post();
	}
 }	
}
 

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

 Ext.DOM.CekPolicyForm = function(){
	return(
		Ext.Ajax
		({
			url : Ext.DOM.INDEX +'/SrcCustomerList/CekPolicyForm/',
			method : 'POST',
			param :{
				CustomerId : Ext.Cmp('CustomerId').getValue(),
				CallReasonId : Ext.Cmp('CallResult').getValue()
			}
		}).json() );
 }

 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.getUniquePolicy = function(){
 return( SaveCounterPolis.filter(function(elem, pos) { 
	return SaveCounterPolis.indexOf(elem) == pos;  
  }));
}

Ext.DOM.array_sum = function( selector ) { 
    var sum = 0;
    for (var i = 0; i < selector.length; i++) {
		if(selector[i]!='') {
			sum+= parseFloat(selector[i]);
		}
    }
    return sum;
};


/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CheckFollowup = function() {

var IS_CONDS = { callback : true, message : '' }, IS_SUPPORT = [];
	IS_SUPPORT.push($('#CustField_Q4').val());	 
	IS_SUPPORT.push($('#CustField_Q5').val());
	IS_SUPPORT.push($('#CustField_Q6').val());
	 
 if( Ext.DOM.array_sum(IS_SUPPORT)==3 
	&&  $('#CustField_Q7').val()!=2 )
 {
	IS_CONDS = {
		callback : false,
		message : 'Please Select 2 in Q7 '
	}
 }	 
 
 if( Ext.DOM.array_sum(IS_SUPPORT)<3 
	&& $('#CustField_Q7').val()==2)
 {
	IS_CONDS = {
		callback : false,
		message : 'Please Select ( 1 OR 0 ) in Q7 '
	}
 }
 
 return IS_CONDS;
 
}
 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 

Ext.DOM.ValidQuestion = function(object) 
{
  var FU_POST = [], FU_SUPPORT = [];
  
  Ext.Cmp('CustField_Q7').disabled(true);
  
  FU_POST.push($('#CustField_Q1').val());
  FU_POST.push($('#CustField_Q2').val());
  FU_POST.push($('#CustField_Q3').val());
	  
// define suport 

  FU_SUPPORT.push($('#CustField_Q4').val());
	  FU_SUPPORT.push($('#CustField_Q5').val());
	  FU_SUPPORT.push($('#CustField_Q6').val());
	  
// open form FU POS 	  
if( Ext.DOM.array_sum(FU_POST)==3){
	 Ext.Cmp('FU_POST').disabled(true);	
	 Ext.Cmp('FU_POST').getElementId().checked = false; 
	 Ext.DOM.DisableAddress(true);
  }	
  else if( Ext.DOM.array_sum(FU_POST) < 3 )
  {
		if( Ext.Cmp('CustField_Q1').empty() 
			&& Ext.Cmp('CustField_Q2').empty() 
			&& Ext.Cmp('CustField_Q3').empty() )
		{
			Ext.Cmp('FU_POST').disabled(true);
			Ext.Cmp('FU_POST').getElementId().checked = false; 
			Ext.DOM.DisableAddress(true);
		} 
		else{
			Ext.Cmp('FU_POST').disabled(true);
			Ext.Cmp('FU_POST').getElementId().checked = true; 
			Ext.DOM.DisableAddress(false);
		}
  }
  else{
	Ext.Cmp('FU_POST').disabled(true);
	Ext.Cmp('FU_POST').getElementId().checked = false; 
	Ext.DOM.DisableAddress(true);
  }  
  
 if( Ext.DOM.array_sum(FU_SUPPORT) > 0 ) 
 {
	
	Ext.Cmp('CustField_Q7').disabled(false);
    var Q7 = parseInt(Ext.Cmp('CustField_Q7').getValue());
	if( parseInt(Q7)==1) 
	{
		Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
		Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = true;
		Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(false);
			
	}
	else{
		Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
		Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = false;
		Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(true);
	}
		
	if( parseInt(Q7)==0 ){
		if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
			Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = true;
			Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(false);
		}
		
		if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
			Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = true;
			Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(false);
		}
	}
	else
	{
		if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
			Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').setValue('');
		}	
		
		if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
			Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').setValue('');
		}	
	}
 } 
 else {
	
	// ---------------------------------> start here 
 
	 if( Ext.Cmp('CustField_Q4').empty() &&  
	    Ext.Cmp('CustField_Q5').empty() &&
		Ext.Cmp('CustField_Q6').empty() )
	{ 
		Ext.Cmp('CustField_Q7').setValue('');
		Ext.Cmp('CustField_Q7').disabled(false);
	}
	else{
		Ext.Cmp('CustField_Q7').disabled(false);
	}
	
		var Q7 = parseInt(Ext.Cmp('CustField_Q7').getValue());
		
		// -------------------- > 
		
		if( parseInt(Q7)==1) {
			Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
			Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = true;
			Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(false);
			
		}
		else{
			Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
			Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(true);
		}
		
		// ------------------- > 
		if( parseInt(Q7)==0 ){
			if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
				Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = true;
				Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(false);
			}
			
			if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
				Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = true;
				Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(false);
			}
		}
		else
		{
			if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
				Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
				Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = false;
				Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').setValue('');
			}	
			
			if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
				Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
				Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = false;
				Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(true);
				Ext.Cmp('NOTES_FU_BANCA_SUPPORT').setValue('');
			}	
		}
	 
// ---------------------------------> stop here 

 } 
} 
 
 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.saveActivity =function() {

 var ActivityCall = [];
 var ActivityForm = Ext.Serialize('frmActivityCall').Complete([
		'QualityStatus','ProductForm','CallingNumber',
		'PhoneNumber','AddPhoneNumber','date_call_later',
		'hour_call_later','minute_call_later','CallComplete'
    ]);
 
  var ActivityQuestion = Ext.Serialize('frmQuestion').Complete();
  var UniqPolicySaved = Ext.DOM.getUniquePolicy();
  
	  // cek followup data 
	  console.log(UniqPolicySaved.length);
  
  var ActicvityFollowup = Ext.Serialize('frmWorkProject').getElement();
  
	  ActivityCall['FollowupGroupCode']  = Ext.DOM.FuFollowUpByCheckedList();	
	  ActivityCall['CustomerId']  = Ext.Cmp('CustomerId').getValue();
	  ActivityCall['CallingNumber'] = Ext.Cmp('CallingNumber').getValue();
	  ActivityCall['CallSessionId'] = Ext.DOM.CallSessionId();
	  ActivityCall['ScriptId'] = Ext.Cmp('ScriptSelectId').getValue();
	  ActivityCall['CustomerNumber'] = Ext.Cmp('CustomerNumber').getValue();
	  ActivityCall['PolicyId'] = Ext.Cmp("PolicyId").getValue();
	
//////////////// Check Here //////////////////////////	

  var IS_CONDS = Ext.DOM.CheckFollowup();
  console.log(IS_CONDS);
  if( IS_CONDS.callback ==false ){
	 Ext.Msg( IS_CONDS.message ).Info();
	 return IS_CONDS.callback;
  }
//////////////// Check Here //////////////////////////	
	
	if( !ActivityForm ){ 
		Ext.Msg('Input Documentation not complete').Info(); }
	else if(  Ext.Cmp('ScriptSelectId').empty()){
		Ext.Msg('Please Select Script').Info(); }
	else 
	{
	  /*
	   * @ notes : done remove of data in here testing 
	   * @ will run if data on progress settle 	
	   * @ then wil progress in here 
	   */
	   
		console.log("isHangup :"+ Ext.DOM.initFunc.isHangup);
		console.log("isCancel :"+ Ext.DOM.initFunc.isRunCall);
		console.log("isCall :"+ Ext.DOM.initFunc.isCall);
		
		
		if( (Ext.DOM.initFunc.isHangup==true) 
			&& (Ext.DOM.initFunc.isRunCall==false)
			&& (Ext.DOM.initFunc.isCall==true) )
		{
			Ext.DOM.initFunc.isSave = true;
			Ext.DOM.initFunc.isCallPhone = false;
			Ext.DOM.initFunc.isCancel = true;
			 
		var MaximumSave = parseInt(Ext.Cmp('SumPolicyId').getValue());
		if(UniqPolicySaved.length < MaximumSave ) 
		{	 
		  if(!Ext.Array(UniqPolicySaved).in_array(parseInt(ActivityCall['PolicyId'])))  
		  {
			if( Ext.DOM.initFunc.isRelease ==false  )  
			{	
				if(Ext.Cmp('CallResult').getValue()==1 && ActivityQuestion == false ) {  
					alert('Please Fill up the Questions!!!'); }
				else 
				{
				
				   ActivityCall['CallAtempt'] = ((UniqPolicySaved.length==MaximumSave)?1:(UniqPolicySaved.length==0?1:0)); // is one only 
				  if( Ext.Cmp('CallResult').getValue()==1 && Ext.DOM.FuFollowUpByCheckedName()==false ) {  
				   return false; }
				  else 
				  {
					   Ext.Ajax ({
						 url 	: Ext.DOM.INDEX +'/ModContactView/SaveActivity/',
						 method : 'POST',
						 param 	: Ext.Join([
									Ext.Serialize('frmActivityCall').getElement(),
									Ext.Serialize('frmQuestion').getElement(),
									ActivityCall, ActicvityFollowup
								]).object(),
								
						 ERROR  : function(fn){
								Ext.Util(fn).proc(function(save){
									if( save.success ) 
									{
										Ext.Msg("Save Activity").Success();
										SaveCounterPolis.push(parseInt(ActivityCall['PolicyId']));
										Ext.DOM.ReloadPolicyGrid();
										Ext.DOM.CallHistory(); 
										$('#tabs_list').tabs( "option", "selected", 3);
										Ext.DOM.DisabledActivity();
										Ext.Cmp('CallStatus').disabled(false);
										Ext.Cmp('CallResult').disabled(false);
										
										var MaximumSize = parseInt(Ext.Cmp('SumPolicyId').getValue());
										 if( MaximumSize == Ext.DOM.getUniquePolicy().length ) {	
											Ext.DOM.PolicyDetailById(Ext.DOM.getUniquePolicy()[0]);
											SaveCounterPolis = [];
											Ext.DOM.initFunc.isRelease = true;
											Ext.DOM.initFunc.isComplete = false;
										}
										else{
											Ext.DOM.initFunc.isComplete = true;
											Ext.DOM.PolicyDetailById(ActivityCall['PolicyId']);
										}
									}
									else{
										Ext.Msg("Save Activity").Failed();
									}
								});
							}
					  }).post();
				  }
				 
				}
			  }
			  else{
				Ext.Msg('Please Call Again before save status ').Info();	
			  } 	
			}	
			else{
				Ext.Msg('Please save diffrent policy ').Info();
			}
		  }
		  else {
			Ext.Msg('Save Maximum '+ MaximumSave +" Policy ").Info();
		  }	
		}
		else{
			Ext.Msg("Theres No Call Activity OR Call Is Running").Info();
		}	
	}	
}

Ext.DOM.SumPolicy = function()
{
	if(Ext.DOM.initFunc.isSave) {
		var UniqPolicySaved = SaveCounterPolis.filter(function(elem, pos) { return SaveCounterPolis.indexOf(elem) == pos; }); 
		if(UniqPolicySaved.length >= parseInt(Ext.Cmp('SumPolicyId').getValue()) ){
			return true;
		}else{
			if( Ext.DOM.initFunc.isRelease ){
				return true;
			}
			else{
				return false;
			}
		}
	}else{
		return true;
	}
}
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.CallHistory = function( PolicyId ){
	
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX+"/ModSaveActivity/CallHistory/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue(),
			PolicyId : ( PolicyId ? PolicyId : '' )
		}
	}).load("tabs_list-4");
}


 /* 
 * @ def : reoad after save policy data 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.ReloadPolicyGrid = function(){
	
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX+"/SrcCustomerList/ReloadPolicyData/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue()
		}
	}).load("divInnerHTML");
}



/** 
 * @ def : ajax loading navigation 
 * -------------------------------------------------
 * 
 */
 
Ext.DOM.AjaxStart = function(){
	Ext.Cmp('html_ajax_loading').setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='10px;'> <span style='color:red;'>Please Wait...</span>");
} 
 
/* * 
 * @ def : AjaxStop
 * -------------------------------------------------
 * 
 */ 
  
Ext.DOM.AjaxStop = function(){
	Ext.Cmp('html_ajax_loading').setText('<span style="color:#dddddd;">-</span>');
} 

/* 
 * set CSS CLASS IF CLICK HERE 
 *
 **/

Ext.DOM.SelectedRows = function(prev_obejct_tr){ 
	if(!OpenFormFlag) {
		$('#'+prev_obejct_tr).addClass('selected-rows').siblings().removeClass("selected-rows");
	}
}
 
/* * 
 * @ def : PolicyDetailById
 * -------------------------------------------------
 * 
 */ 
Ext.DOM.PolicyDetailById = function(PolicyId){
	Ext.DOM.AjaxStart();
	
	if(!OpenFormFlag)
	{
		if( PolicyId!='' ) 
		{
			$('#tabs_list').tabs( "option", "selected", 0);
			Ext.Ajax
			({
				url : Ext.DOM.INDEX+'/SrcCustomerList/PolicyDetailById/',
				method :'GET',
				param : {
					PolicyId : PolicyId
				}	
			}).load('tabs_list-1');
			
			Ext.Cmp('PolicyId').setValue(PolicyId); // set policy ID 
			Ext.DOM.CallHistory(PolicyId); // get Call History By PolicyId 
			Ext.DOM.PolicyDetailStatus(PolicyId); // set call status 
			Ext.DOM.DisableQuestion(true);
		    Ext.DOM.PolicyDataByPolicyId();
			Ext.DOM.DisableAddress(true);
			Ext.DOM.PolicyFollowByPolicyId();
			Ext.DOM.QuestionDataByPolicyId();
			Ext.DOM.SelectedRows('rows_'+PolicyId);
			window.setTimeout(function(){
				Ext.DOM.ValidQuestion(this);
			}, 1000)
			
	
		}
	}
	else{
		alert("Can't Switch Other Policy, Form is Opened!");
	}
	
	Ext.DOM.AjaxStop();
	
}
	
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.getEventSale = function(object) 
{
	Ext.Cmp('call_remarks').setValue(Ext.Cmp(object.id).getText() );
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/SetCallResult/getEventType/',
		method : 'GET',
		param :{
			CallResultId : object.value
		},
		ERROR : function(fn){
			try
			{
				Ext.Util(fn).proc(function( ERR ){
				   if( ERR.success) 
				   {
					if( typeof(ERR.event)=='object') {
						if( ERR.event.CallReasonEvent==1 ){ 
							Ext.Cmp('CallComplete').getElementId().checked=true;
							Ext.Cmp('divInnerStatus').setText('Complete');
						}
						else if( ERR.event.DefaultCompleteCheck==1)
						{
							if( parseInt(object.value)==1 ) 
							{	
								$('#tabs_list').tabs( "option", "selected", 3);
								Ext.Cmp('CallComplete').getElementId().checked=true;
								Ext.Cmp('divInnerStatus').setText('Complete');
								Ext.Cmp('CallComplete').getElementId().disabled=true;
								Ext.DOM.DisableQuestion(false);
								Ext.DOM.DisableAddress(true);
								Ext.DOM.QuestionDataByPolicyId();
								
							}
							else{
								Ext.Cmp('CallComplete').getElementId().checked=true;
								Ext.Cmp('divInnerStatus').setText('Complete');
								Ext.Cmp('CallComplete').getElementId().disabled=true;
								Ext.DOM.ResetValueOfQuestion();
								Ext.DOM.DisableQuestion(true);
								Ext.DOM.DisableAddress(true);
							}
						}
						else{
							Ext.Cmp('CallComplete').getElementId().checked=false;
							Ext.Cmp('divInnerStatus').setText('Uncomplete');
							Ext.Cmp('CallComplete').getElementId().disabled=false;
							Ext.DOM.DisableQuestion(true);
							Ext.DOM.ResetValueOfQuestion();
							Ext.DOM.DisableAddress(true);
						}
						
						if( ERR.event.CallReasonLater==1){
							Ext.Cmp('date_call_later').disabled(false);
							Ext.Cmp('hour_call_later').disabled(false);
							Ext.Cmp('minute_call_later').disabled(false);
						}
						else{
							Ext.Cmp('date_call_later').setValue('');
							Ext.Cmp('hour_call_later').setValue('00');
							Ext.Cmp('minute_call_later').setValue('00');
							Ext.Cmp('date_call_later').disabled(true);
							Ext.Cmp('hour_call_later').disabled(true);
							Ext.Cmp('minute_call_later').disabled(true);
						}
					 }
				  }
				});
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CancelActivity =function(){
 if( (Ext.DOM.initFunc.isRunCall==false ) ) 
 {
	if(Ext.DOM.SumPolicy())
	{
		if(Ext.DOM.initFunc.isCancel){
			Ext.ActiveMenu().Active();
			ControllerId = Ext.Cmp("ControllerId").getValue();
			Ext.EQuery.Ajax
			({
				url 	: ControllerId,
				method 	: 'GET',
				param 	: { act : 'back-to-list' }
			});
		}else{
			Ext.Msg('Please Save Activity').Info();
		}
	}else{
		alert('Please Save All Policies');
	}
}	
 else { 
	Ext.Msg('Please Save Activity Or Hangup Call').Info();
}	
	  

 }

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.UserWindow = function(){
	Ext.DOM.AdditionalPhone( Ext.Cmp('CustomerId').getValue() );
	return false;
}


/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.PolicyDataByPolicyId = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/PolicyDataByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue(),
			CustomerId : Ext.Cmp('CustomerId').getValue()
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function( data )
			{
				Ext.Cmp('ADDRESS1_FU_POST').setValue(data.CustomerAddressLine1);
				Ext.Cmp('ADDRESS2_FU_POST').setValue(data.CustomerAddressLine2);
				Ext.Cmp('ADDRESS3_FU_POST').setValue(data.CustomerAddressLine3);
				Ext.Cmp('ADDRESS4_FU_POST').setValue(data.CustomerAddressLine4);
				Ext.Cmp('CITY_FU_POST').setValue(data.CustomerCity);
				Ext.Cmp('ZIP_FU_POST').setValue(data.CustomerZipCode);
				Ext.Cmp('PROVINCE_FU_POST').setValue(data.ProvinceId);
				Ext.Cmp('COUNTRY_FU_POST').setValue(data.CustomerCountry);
				Ext.Cmp('HOME_FU_POST').setValue(data.CustomerHomePhoneNum);
				Ext.Cmp('MOBILE_FU_POST').setValue(data.CustomerMobilePhoneNum);
				Ext.Cmp('OFFICE_FU_POST').setValue(data.CustomerWorkPhoneNum);
				Ext.Cmp('EMAIL_FU_POST').setValue(data.CustomerEmail);
				Ext.Cmp('PAYMENTFREQUENCY_FU_POST').setValue(data.PayMode);
				Ext.Cmp('NOTES_FU_POST').setValue(data.NOTES_FU_POST);
				
			});
		}
		
	}).post();	
}

Ext.DOM.PolicyFollowByPolicyId = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/PolicyFollowByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue(),
			CustomerId : Ext.Cmp('CustomerId').getValue()
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function( data ) {
				for( var field in data  ){
					Ext.Cmp(field).setValue(data[field]);
				}
				
			});
		}
		
	}).post();	
}

/* QustionByPolicyId **/

Ext.DOM.QuestionDataByPolicyId = function(){

// QUESTION BY POLICY ID 
	Ext.Ajax ({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/QustionByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue()
		},
		ERROR : function(e){
			Ext.Util(e).proc(function( data )
			{
				var frm_elment = Ext.Serialize('frmQuestion').getElement();
				for( var field in data )
				{
					for( var fields in frm_elment )
					{
						if( field==fields ) {
							Ext.Cmp(fields).setValue( data[field] )
						}
					}
				}
			});
		}
	}).post();	
	
}

/* DOM.DisableAddress **/

Ext.DOM.DisableAddress  = function(options)
{
	
	Ext.Cmp('ADDRESS1_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS2_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS3_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS4_FU_POST').disabled(options);
	Ext.Cmp('CITY_FU_POST').disabled(options);
	Ext.Cmp('ZIP_FU_POST').disabled(options);
	Ext.Cmp('PROVINCE_FU_POST').disabled(options);
	Ext.Cmp('COUNTRY_FU_POST').disabled(options);
	Ext.Cmp('HOME_FU_POST').disabled(options);
	Ext.Cmp('MOBILE_FU_POST').disabled(options);
	Ext.Cmp('OFFICE_FU_POST').disabled(options);
	Ext.Cmp('EMAIL_FU_POST').disabled(options);
	Ext.Cmp('PAYMENTFREQUENCY_FU_POST').disabled(options); 
	Ext.Cmp('NOTES_FU_POST').disabled(options);
}
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.document(document).ready(function(){
	$('#CustomerDOB').datepicker({
		dateFormat : 'dd-mm-yy',
		changeYear : true, changeMonth : true
	});
	
	$('#CallComplete').bind('click', function(){
		if( this.checked ){
			Ext.Cmp('divInnerStatus').setText('Complete');
		}else{
			Ext.Cmp('divInnerStatus').setText('Uncomplete');
		}
	});

	Ext.DOM.PolicyDetailById(Ext.Cmp('PolicyId').getValue());
	Ext.DOM.SelectedRows('rows_'+Ext.Cmp('PolicyId').getValue());
});

</script>