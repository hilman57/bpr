<?php

/** 
 * @ def : grid of content policy by customer ID 
 * ----------------------------------------------------------------
 
 * @ param  : $ CustomerId 
 * @ refer  : $ Pilcy ID     
 * 
 */
 
// time 24 jam 

 $jam = array( '00'=>'00' ); $menit = array();
 for( $_is = 7; $_is <=21; $_is ++ ) {
	$hours = ( strlen($_is)==1 ?"0$_is":"$_is");
	$jam[$hours] = $hours;
 }
 
// minute 60  

 for( $_is = 0; $_is <=59; $_is ++ ){
	$minutes = ( strlen($_is)==1 ?"0$_is":"$_is");
	$menit[$minutes] = $minutes ;
 }
?>

<fieldset class="corner" style="margin-top:4px;"> 
	<legend class="icon-menulist"> &nbsp;&nbsp; Call Activity</legend> 
<div style="overflow:auto;margin-top:3px;" class="activity-content box-shadow">
<input type="hidden" name="CallingNumber" id="CallingNumber" value="<?php __($Customers['CustomerMobilePhoneNum']);?>">
<form name="frmActivityCall">
<?php echo form()->hidden('QualityStatus',NULL,$Customers['CallReasonQue']);?>
<table class="activity-table" cellpadding="1px;" border=0 cellspacing="1px" width='100%' align='center'>
	<tr>
		<td nowrap class="text_caption bottom" width='30%'>Phone : </td>
		<td nowrap id="phone_primary_number" width='70%'><?php echo form()->combo('PhoneNumber','select long', $Phones,$Customers['CustomerMobilePhoneNum'],
		array("change" =>"Ext.Cmp('CallingNumber').setValue(this.value);") , $Disabled); ?></td>
	</tr>	
	<tr>
		<td nowrap class="text_caption bottom">Add Phone : </td>
		<td nowrap id="phone_additional_number"><?php echo form()->combo('AddPhoneNumber','select long',$AddPhone, null, 
		array("change" =>"Ext.Cmp('CallingNumber').setValue(this.value);"), $Disabled); ?></td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td><?php if($Disabled==NULL){ ?>
			<img class="image-calls" src="<?php echo base_url(); ?>/library/gambar/PhoneCall.png" width="35px" height="35px" style="cursor:pointer;" title="Dial..." onclick="dialCustomer();">
			<img class="image-hangup" src="<?php echo base_url(); ?>/library/gambar/HangUp.png" width="35px" height="35px" style="cursor:pointer;" title="Hangup..." onclick="hangupCustomer();">
			<?php }else{ ?>
			<img class="image-calls" src="<?php echo base_url(); ?>/library/gambar/PhoneCall.png" width="35px" height="35px" style="cursor:pointer;" title="Dial..." onclick="alert('View Only !!!, Dialing is not Required, Click Close Button to Exit');">
			<img class="image-hangup" src="<?php echo base_url(); ?>/library/gambar/HangUp.png" width="35px" height="35px" style="cursor:pointer;" title="Hangup..." onclick="alert('View Only !!!, Dialing is not Required, Click Close Button to Exit');">
			<?php } ?>
		</td>
	</tr>	
	<tr>
		<td class="text_caption bottom" valign="top">Call Status : </td>
		<td> <?php echo form()->combo('CallStatus','select long', $CallCategoryId,"",array('change'=>"getCallReasultId(this);"), $Disabled); //$Customers['CallReasonCategoryId']?></td>
	</tr>	
	<tr>
		<td class="text_caption bottom">Call Result : </td>
		<td id="contact_reason_text">
			<span id="DivCallResultId">
				<?php echo form()->combo('CallResult','select long',$CallResultId,"",array('change'=>'getEventSale(this);'), $Disabled); //$Customers['CallReasonId']?>
			</span>
		</td>
	</tr>	
	<tr>
		<td class="text_caption bottom" valign=top>Call Later : </td>
		<td> 
			<?php echo form()->input('date_call_later','input_text box date'); ?>&nbsp;
			<?php echo form()->combo('hour_call_later','select box', $jam, '00', null, array('style'=>'margin-top:2px;width:50px;','disabled' => 'disabled')); ?> :
			<?php echo form()->combo('minute_call_later','select box', $menit, '00', null, array('style'=>'margin-top:2px;width:50px;','disabled' => 'disabled'));?>
		</td>
	</tr>	
	
	<tr>
		<td class="text_caption bottom">&nbsp;<span id="divInnerStatus">Complete</span> : </td>
		<td nowrap><?php __(form()->checkbox('CallComplete',null,1, $Disabled));?></td>
	</tr>	
	
	<tr>
		<td class="text_caption bottom">Description : </td>
		<td align="left"> 
			<textarea id="call_remarks" name="call_remarks" onkeyup="this.value=this.value.toUpperCase();" style="height:100px;background-color:#fbf9f7;color:#000000;font-family:Arial;font-size:11px;border:1px solid #dddddd;width:200px;"></textarea>
		</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td align="left">
			<?php $jsStyles = $Disabled; if( !is_null($Disabled)) $jsStyles['style'] = 'color:#DDDDDD;'; 
				?> 
			<?php echo form()->button('ButtonUserSave', 'button save', 'Save', array("click"=>"Ext.DOM.saveActivity();"), $jsStyles ); ?>
			<?php echo form()->button('ButtonUserCancel', 'button close', 'Close', array("click"=>"Ext.DOM.CancelActivity();")); ?>
			<?php echo form()->button('ButtonUserNext', 'button assign', 'Next', array("click"=>"Ext.DOM.NextActivity();"), $jsStyles ); ?>
		</td>
	</tr>
	</table>	
	</form>
	</div>	
</fieldset>	