<table border=0 align="left" cellspacing=1 width="100%">
		<tr height='24'>
			<th class="ui-corner-top ui-state-default first center" WIDTH="5%" nowrap>&nbsp;No</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="8%" nowrap>&nbsp;CIF Number</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="18%" nowrap>&nbsp;Policy Owner </td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="8%" nowrap>&nbsp;Policy Number</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="12%">&nbsp;Policy Iss Date</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Call Status</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Call Result</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Caller</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Complete</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Update</td>
			<th class="ui-corner-top ui-state-default first center" width="8%" nowrap>&nbsp;Premi</td>
		</tr>
		
<?php 
 $total_policy = 0; 
 $totPolicyPremi = 0;
 $num_of_policy = 1;
 foreach($RowPolicyData as $rows ) :  $color= ($num_of_policy%2!=0?'#FAFFF9':'#FFFFFF'); ?>	
	<tr class='onselect' bgcolor='<?php __($color);?>' style="cursor:pointer;" id="rows_<?php __($rows['PolicyId']);?>"
		onclick="Ext.DOM.PolicyDetailById('<?php __($rows['PolicyId']);?>');">
		<td class="content-first center" nowrap><?php __($num_of_policy);?></td>
		<td class="content-middle" nowrap><?php __($rows['CIFNumber']);?></td>
		<td class="content-middle" nowrap><?php __($rows['PolicyFirstName']);?></td>
		<td class="content-middle" ><?php __($rows['PolicyNumber']);?></td>
		<td class="content-middle"><?php __(date('d/m/Y',strtotime($rows['PolicyIssDate'])));?></td>
		
		<td class="content-middle" nowrap><?php __( ( $rows['CallReasonCategoryId'] ? $CallCategoryId[$rows['CallReasonCategoryId']] : '-'));?></td>
		<td class="content-middle" nowrap><?php __( ( $rows['CallReasonId'] ? $CallResultId[$rows['CallReasonId']] : '-'));?></td>
		<td class="content-middle" nowrap><?php __( ( $rows['full_name'] ? $rows['full_name'] : '-'));?></td>
		<td class="content-middle" ><?php __( ($rows['CallComplete']?'Complete':'-'));?></td>
		<td class="content-middle"><?php __( ( $rows['UpdateDateTs'] ? date('d/m/Y',strtotime($rows['UpdateDateTs'])):'-'));?></td>
		<td class="content-lasted right"><?php __(_getCurrency($rows['PolicyPremi']));?></td>
	</tr>
<?php 
 $totPolicyPremi += $rows['PolicyPremi'];
 $total_policy += 1; 
 $num_of_policy++;
endforeach; ?>		
		<tr height='24'>
			<td colspan="7" style='border-top:1px solid red;background-color:#DDDEED;color:black;font-weight:bold;' class="content-middle" >Total Policy
				&nbsp;( <?php __($total_policy);?> ) </td>
			<td colspan="2" style='border-top:1px solid red;background-color:#DDDEED;color:black;font-weight:bold;' class="content-middle">Total Premi&nbsp;</td>
			<td colspan="2" style='border-top:1px solid red;background-color:#DDDEED;color:black;font-weight:bold;' class="content-middle right">&nbsp;Rp . <?php __(_getCurrency($totPolicyPremi));?></td>
		</tr>
	</table>