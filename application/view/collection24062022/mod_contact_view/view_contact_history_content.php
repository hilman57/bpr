<table border=0 align="left" cellspacing=1 width="100%">
<tr height='24'>
	<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
	<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;Policy No.</td>
	<th class="ui-corner-top ui-state-default first" WIDTH="15%" nowrap>&nbsp;Call Date</td>
	<th class="ui-corner-top ui-state-default first left" WIDTH="15%" nowrap>&nbsp;Caller</td>
	<th class="ui-corner-top ui-state-default first left" WIDTH="30%">&nbsp;Status</td>
	<th class="ui-corner-top ui-state-default first" >&nbsp;Note</td>
	<th class="ui-corner-top ui-state-default first left" WIDTH="15%">&nbsp;Script</td>
</tr>
<?php if(!is_null($CallHistory) && is_array($CallHistory) ) :   ?>
<?php 

	$i = 0;
	$k = 0;	
	foreach($CallHistory as $rows ) : 
	
		$color = ($i%2!=0?'#FFFEEE':'#FFFFFF');  ?>
<tr class='onselect' bgcolor='<?php __($color);?>'>
	<td class="content-first" WIDTH="5%" nowrap><?php echo $k;?></td>
	<td class="content-middle" WIDTH="5%" nowrap><?php echo $rows['PolicyNumber'];?></td>
	<td class="content-middle" WIDTH="12%" nowrap><?php echo date('d/m/Y H:i:s',strtotime($rows['CallHistoryCreatedTs']));?></td>
	<td class="content-middle" WIDTH="12%" nowrap><?php echo $rows['full_name'];?></td>
	<td class="content-middle" WIDTH="17%">
		<div class="text-content left-text" >
			Status :(<span class="call-status"><?php echo $rows['CallReasonCategoryName'];?></span>) - 
			Reason :(<span class="result-status"><?php echo $rows['CallReasonDesc'];?></span>) -
			Type :(<span class="result-status"><?php echo ($rows['PhoneType']?$rows['PhoneType']:'-');?></span>) -
			Phone : (<span class="result-status"><?php echo $rows['CallNumber'];?></span>)
		</div>
	</td>
	<td class="content-middle" style="word-wrap:break-word;" ><div class="text-content justify-text" style="word-wrap:break-word;" ><?php echo $rows['CallHistoryNotes'];?></div></td>
	<td class="content-lasted" WIDTH="15%" nowrap><div style="word-wrap:break-word;" ><?php echo $rows['Description'];?></div></td>
</tr>
<?php $i++; $k++; endforeach; ?>
<?php endif; ?>
</table>
