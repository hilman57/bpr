<?php 
/** 
 * @ def : grid of content policy by customer ID 
 * ----------------------------------------------------------------
 
 * @ param  : $ CustomerId 
 * @ refer  : $ Pilcy ID     
 * 
 */
 
$ESC=& get_instance();
$ESC->load->model('M_ProjectAnswer');

/* ------------------------------------------------ */

if( !class_exists('M_ProjectAnswer') ) 
	die('error data ');

/* ------------------------------------------------ */

 $arrs_cols  =2;
 $arrs_list  =& $ESC->M_ProjectAnswer->_getAnswerQuestion();
 $arrs_cont  =& $ESC->M_ProjectAnswer->_getAnswerKey($Customers['CustomerId'], $PolicyId);
 $arrs_baris =& ceil(count($arrs_list)/$arrs_cols); 
 
 __("<table border=0 width='100%' align='left' cellpadding=2>");
	$start = 0;	 $q = 1;
	while($q<=$arrs_baris )
	{
		__("<tr>");
			$start++;
			$n = ($start-1)*$arrs_cols;
			foreach( array_slice($arrs_list, $n, $arrs_cols) as $k => $labels) 
			{
				__("<td class='text_caption bottom' nowrap>$k .</td>\n");
				__("<td>". form()->combo("CustField_$k","select", $labels,$arrs_cont["CustField_$k"],array('change' => 'Ext.DOM.ValidQuestion(this);') ) ."</td>\n");
			}
			
		__("</tr>\n");
		$q++;		
	}
__("</table>");
		