<?php 
__(javascript
(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time())
)));
?>

<script type="text/javascript">
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
var Reason = [];

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
var datas = {
	faxnumber 	: '<?php echo _get_post('faxnumber');?>',
	agent_id 	: '<?php echo _get_post('agent_id');?>',
	agent_group : '<?php echo _get_post('agent_group');?>',
	start_time 	: '<?php echo _get_post('start_time');?>',
	end_time 	: '<?php echo _get_post('end_time');?>',
	order_by	: '<?php echo _get_post('order_by');?>',
	type		: '<?php echo _get_post('type');?>'
}

	

Ext.DOM.DownloadAttachment = function( OutboxId ){
	Ext.Window
	({
		url : Ext.DOM.INDEX +'/MailOutbox/DownloadAttachment/',
			method : 'POST',
			width : ( Ext.query(window).width()/2), height: Ext.query(window).height(),
			left  : Ext.query(window).width(),
			scrollbars : 1,
			param :{
				OutboxId : OutboxId
			}
	}).popup();
	
}
			
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
var navigation = {
	custnav : Ext.DOM.INDEX +'/MailOutbox/index/',
	custlist : Ext.DOM.INDEX +'/MailOutbox/content/'
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */		
 
 Ext.EQuery.construct(navigation,datas)
 Ext.EQuery.postContentList();

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

var searchCustomer = function(){
	Ext.EQuery.construct(navigation,Ext.Join([Ext.Serialize('frmSearch').getElement()]).object() );
	Ext.EQuery.postContent();
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */


Ext.DOM.CloseFax = function(){
	Ext.Cmp("panel-call-center").setText('');
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
Ext.DOM.Preview = function() 
{
	Ext.Window
	({
		url : Ext.DOM.INDEX +'/MailOutbox/PrintPreview/',
			method : 'POST',
			width : ( Ext.query(window).width()/2), height: Ext.query(window).height(),
			left  : Ext.query(window).width(),
			scrollbars : 1,
			param :{
				EmailOutboxId : Ext.Cmp('EmailOutboxId').getValue()
			}
	}).popup();
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.DOM.Resend = function()
{
	var EmailOutboxId  = Ext.Cmp('EmailOutboxId').getValue();
	if((EmailOutboxId.length > 0)) 
	{
		Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX +'/MailOutbox/ResendMail/',
			method 	: 'POST',
			param 	: { 
				EmailOutboxId : Ext.Cmp('EmailOutboxId').getChecked() 
			},
			ERROR 	: function(e){
				Ext.Util(e).proc(function(Resend) {
					if( Resend.success ){
						Ext.Msg("Resend Mail").Success();
						Ext.EQuery.postContent();
					}
					else {
						Ext.Msg("Resend Mail").Failed();
					}
				});
			}
		 }).post();
	}
	else{
		Ext.Msg("No selected rows ").Info();
	}
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

$(function(){
	$('#toolbars').extToolbars
	({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Preview'],['Resend']],
		extMenu   : [['Preview'],['Resend'],[]],
		extIcon   : [['email_open.png'],['email_go.png']],
		extText   : true,
		extInput  : false,
		extOption : [{
			render : 3,
			type   : 'label',
			id     : 'voice-list-wait', 	
			name   : 'voice-list-wait',
			label :'<span style="color:#dddddd;">-</span>'
		}]
	});
	
	$('.box').datepicker({
		showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly: true, 
		dateFormat:'dd-mm-yy',readonly:true
	});
});

</script>

<!-- start : content -->
<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
</div>
	
	<div id="toolbars" style="margin-left:12px;margin-right:12px"></div>
	<div id="play_panel"></div>
	<div id="recording_panel" class="xbox-shadow">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	