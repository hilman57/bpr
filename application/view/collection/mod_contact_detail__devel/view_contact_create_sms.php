<?php ?>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script> 

<!-- interval set -->
<script>
var SMS = { phone_number : 0 };


Ext.DOM.SetSMS = function( object ){
 var argv_phone = ['sms_pmr_phone','sms_ec_phone','sms_add_phone'];
if(( object.value !='') )
  {
	SMS.phone_number = Ext.BASE64.decode(object.value);
	for( var i in argv_phone ){
		if( argv_phone[i]!=object.name ){
			Ext.Cmp(argv_phone[i]).setValue('');
		}
	}
  }
  
  return; 
  
}

// @ pack : Ext.document

 Ext.DOM.TextMessage = function( object ) 
{
	Ext.Ajax
	({
		url : window.opener.Ext.DOM.INDEX +"/ModContactDetail/Template/",
		method : 'POST',
		param : {
			TemplateId : object.value,
			CustomerId : Ext.Cmp('CustomerId').getValue()
			
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function(message){
				Ext.Cmp('sms_message_text').setValue(message.text);
				Ext.Cmp('sms_message_text').disabled(true);
			});
		}
		
	}).post();
}

// @ pack : "Ext.DOM.SmsSave()

 Ext.DOM.SmsSave = function()
{
var Argv_vars = [];
	Argv_vars['SmsDestination'] = SMS.phone_number;

if( SMS.phone_number==0 ) {
	Ext.Msg('Please Select Phone Number').Info();
	return false;
}
	
if(!Ext.Serialize('frmNewSMS').Required(['sms_tempalte_id','sms_message_text']) ){
	Ext.Msg('Form not complete').Info();
	return false;
 }

Ext.Ajax 
 ({
	url : window.opener.Ext.DOM.INDEX +"/ModSaveActivity/SaveSmsActivity/",
	method : 'POST',
	param : Ext.Join([ 	
				Ext.Serialize('frmNewSMS').getElement(), 
				Argv_vars 
			]).object(),
			
	ERROR : function( e ) {
		Ext.Util(e).proc(function(response){
			if( response.success ){
				Ext.Msg("Create New SMS").Success();
			} else {
				Ext.Msg("Create New SMS").Success();
			}
		});
	}	
 }).post();
}


// @ pack : "Ext.DOM.SmsSave()

Ext.DOM.SmsExit = function()
{
 if( Ext.Msg("Do you want to close?").Confirm() ) {
	window.close('form_sms');
	return true;
 } else {
	return false;
 }
}



// @ pack : Ext.document

Ext.document(document).ready(function(){
Ext.Css("windowUser").style({
		"height": (Ext.query(window).height()-50),
		"border":'0px solid #000000',
		"overflow":"auto"
	});
Ext.Cmp('sms_message_text').disabled(true);	
});	

// @ pack : Ext.document

Ext.document(document).resize(function(){
Ext.Css("windowUser").style({
	height: (Ext.query(window).height()-50),
	"border":'0px solid #000000',
	"overflow":"auto"
});

});	
</script>
</head>
<body style='background-color:#FFFFFF;'>
<fieldset class="corner" style="margin-top:10px;">

	<legend class="icon-brodcastmsg">&nbsp;&nbsp;Create New SMS </legend>
	<form name="frmNewSMS">
	<input type="hidden" id="CustomerId" name="CustomerId" value="<?php echo _get_post('CustomerId');?>">
	<div id="windowUser" style='padding:-2px;'>
		<table cellspacing=0 width='99%' border=0 class="custom-grid" align="center">
			<tr>
				<td class="text_caption bottom left" width="10%" nowrap>Phone Number &nbsp;</td>
				<td>:</td>
				<td class="bottom" width="80%" nowrap><?php echo form()->combo('sms_pmr_phone','select long',$Dropdown[COLL_DROPDOWN_PMR], $Dropdown['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetSMS(this);") );?></td>
			</tr>	
			
			<tr>
				<td class="text_caption bottom left" width="20%" nowrap>EC Phone&nbsp;</td>
				<td>:</td>
				<td class="bottom"><?php echo form()->combo('sms_ec_phone','select long', $Dropdown[COLL_DROPDOWN_EMC], $Dropdown['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetSMS(this);"));?></td>
			</tr>
			<tr>
				<td class="text_caption bottom left" width="20%" nowrap>Additional Phone &nbsp;</td>
				<td>:</td>
				<td class="bottom"><?php echo form()->combo('sms_add_phone','select long', $Dropdown[COLL_DROPDOWN_ADD], $Dropdown['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetSMS(this);"));?></td>
			</tr>
			<tr>
				<td class="text_caption bottom left" width="20%" nowrap>Template &nbsp;</td>
				<td>:</td>
				<td class="bottom"><?php echo form()->combo('sms_tempalte_id','select long', $Dropdown[COLL_TEMPLATE_SMS], null, array("change"=>"Ext.DOM.TextMessage(this);"));?></td>
			</tr>
			
			<tr>
				<td class="text_caption  left" width="20%" nowrap valign='top'>Message&nbsp;</td>
				<td valign='top'>:</td>
				<td class="" valign='top'><?php echo form()->textarea('sms_message_text','textarea long',null, null, array("style"=>"width:100%;height:120px;") );?></td>
			</tr>
			<tr>
				<td class="text_caption  left" width="20%" nowrap>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="">
					<input type="button" class="button save" onclick="Ext.DOM.SmsSave();" value="Submit" >
					<input type="button" class="button close" onclick="Ext.DOM.SmsExit();" value="Exit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
				</td>
			</tr>
		</table>
	</div>	
	</form>
</fieldset>
</body>
</html>