<?php 
__(javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/accounting.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/views/EUI_Contact.js', 'eui_'=>'1.0.0', 'time'=>time())
	))
);
?>

<?php $this ->load ->view('mod_contact_detail/view_contact_javascript_devel');?>

<div class="contact_detail">

<?php echo form()->hidden('ControllerId',NULL,_get_post("ControllerId"));?>
<?php echo form()->hidden('CustomerId',NULL,_get_post("CustomerId"));?>
<?php echo form()->hidden('LastCallPhoneNumber',NULL,$Dropdown['COLL_CUSTOMER_PHN']);?>
<?php echo form()->hidden('IsAksessAll',NULL,$Dropdown['COLL_AKSESS_ALL']);?>
<?php echo form()->hidden('CustomerAccountNo',NULL,$Customer['deb_acct_no']);?>

<script>
 var AutoMaximum = function() 
{
  var tabs_height = $("#main_content").height();	
	$("#tabs_list").tabs();
	$("#tabs_list").tabs('select',0);
	$("#tabs_list").css({'margin-left' : '5px' });
	$("#tabs_list-2,#tabs_list-3,#tabs_list-4,#tabs_list-5").css({
		height : (tabs_height -(tabs_height/8))
	})
}

 $('document').ready(function(){
	AutoMaximum();
 });
	 
	
 $("#main_content").resize(function(){
	AutoMaximum();
});

</script>
<div id="tabs_list">	
	<ul>
		<li><a href="#tabs_list-1">Account Information</a></li>
		<li><a href="#tabs_list-2">Account Mapping</a></li>
		<li><a href="#tabs_list-3">Payment History</a></li>
		<li><a href="#tabs_list-4">SMS Inbox</a></li>
		<li><a href="#tabs_list-5">SMS Outbox</a></li>
	</ul>
	
	<!-- load :: policy data -->
	<div id="tabs_list-1" style="background-color:#FFFFFF;overflow:auto;"> 
		<?php $this ->load->view('mod_contact_detail/view_contact_information');?>
	</div>
	
	<!-- load ::personal data -->
	<div id="tabs_list-2" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_contact_accountmap');?>
	</div>
	
	<div id="tabs_list-3" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_payment_history');?> 
	</div>
	
	<div id="tabs_list-4" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_contact_smsinbox');?> 
	</div>
	<div id="tabs_list-5" style="background-color:#FFFFFF;overflow:auto;">
		<?php $this ->load->view('mod_contact_detail/view_contact_smsoutbox');?> 
	</div>
</div>
</div>	
<div id="WindowUserDialog" > </div>