<?php ?>

<fieldset class="corner" style="margin-top:8px;"> 
<legend class="icon-menulist"> &nbsp;&nbsp; Call User Activity</legend> 
<form name="frmCallUserActivity">
<div style="overflow:auto;margin-top:-5px;" class="activity-content">
	<table border=0 width='99%'>
		<tr>
			<td class="text_caption bottom" width="20%" nowrap># Phone Number &nbsp;:</td>
			<td class="bottom" width="24%" nowrap><?php echo form()->combo('select_pmr_phone','select auto',$Dropdown[COLL_DROPDOWN_PMR], 
				$Dropdown['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetCallDial(this);") );?></td>
			<td class="text_caption bottom" rowspan=2 width='10%' valign="top" nowrap> Notes&nbsp;:</td>
			<td class="bottom" rowspan=2><?php echo form()->textarea('text_call_notes','textarea',strtoupper($Customer['deb_reminder']),
				NULL, array('style'=>'width:250px;height:50px;') );?></td>
		</tr>
		<tr>
			<td class="text_caption bottom" width="20%" nowrap># EC Phone&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('select_ec_phone','select auto', $Dropdown[COLL_DROPDOWN_EMC], 
			$Dropdown['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetCallDial(this);"));?></td>
			
		</tr>
		<tr>
			<td class="text_caption bottom" width="20%" nowrap># Additional Phone &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('select_add_phone','select auto', $Dropdown[COLL_DROPDOWN_ADD], 
				$Dropdown['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetCallDial(this);"));?>
				<span class="edit" style="color:red;margin-left:3px;padding-left:6px;background-position: left center;cursor:pointer;"
				onclick="Ext.DOM.UserWindow();">&nbsp;&nbsp;&nbsp;&nbsp;Add</td>
			<td class="bottom text_caption" width='10%'>Form&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->combo('user_work_form','select auto', $Dropdown[COLL_CUSTOMER_FRM], null, 
				array("change" => "Ext.DOM.FormWindow(this.value);"));?>
				<input type="button" id="btnSavePerm" name="btnSavePerm" class="button save" onclick="Ext.DOM.SaveNotes();" value="Save">
				<input type="button" id="btnClaim" name ="btnClaim" class="button assign" onclick="Ext.DOM.SaveClaim();" value="Claim&nbsp;">
			</td>
		</tr>
		
		<tr>
			<td class="text_caption ">&nbsp;</td>
			<td >
				<img class="image-calls" src="<?php echo base_url(); ?>/library/gambar/PhoneCall.png" 
					width="40px" height="40px" style="cursor:pointer;" 
					title="Dial..." onclick="Ext.DOM.DialCustomer();">&nbsp;&nbsp;
				<img class="image-hangup" src="<?php echo base_url(); ?>/library/gambar/HangUp.png" 
					width="40px" height="40px" style="cursor:pointer;" 
					title="Hangup..." onclick="Ext.DOM.HangupCustomer();">
			</td>
			<td class="text_caption">Call Info&nbsp;:</td>
			<td >
				<span>
					<?php echo form()->input('call_number_info','input_text', null,null,array('style'=>'width:40px;') );?>&nbsp;-
					<span style="font-size:12px;font-weight:bold;">108&nbsp;</span>
					<input type="button" class="button icon-phone" value="Call&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
					<input type="button" class="button close" onclick="Ext.DOM.SaveBlock();" value="Block&nbsp;">
				</span>	
			</td>
		</tr>
	</table>
</div>	
</form>
</fieldset>	