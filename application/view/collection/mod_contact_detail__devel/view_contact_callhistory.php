<?php ?>
<script>

 var callhistoryLayout = function() {
 var tabs_height = $("#main_content").height();	
	$("#tabs_list_activity").tabs();
	$("#tabs_list_activity").tabs('select',0);
	 
	 $("#tabs_history_activity").css
	({
	 'width' : $('#tabs_list_activity').width(),
	 'float' : 'left',
	 'margin-top' : '5px',
	 'padding' : '1px 2px 2px 2px' });
	 
	
	$("#tabs_recording_activity").css
	({
	 'width' : $('#tabs_list_activity').width(),
	 'float' : 'left',
	 'margin-top' : '5px',
	 'padding' : '1px 2px 2px 2px' });
	 
	 $("#tabs_list_account_map").css
	({
	 'width' : $('#tabs_list_activity').width(),
	 'float' : 'left',
	 'margin-top' : '5px',
	 'padding' : '1px 2px 2px 2px' });
	 
 }

$('document').ready(function(){
	callhistoryLayout(); 
});

	 
	
$("#main_content").resize(function(){
	callhistoryLayout(); 
});

</script>
<fieldset class="corner" style="margin-top:8px;"> 
<legend class="icon-menulist"> &nbsp;&nbsp; Call History Activity</legend> 
<div id="tabs_list_activity">
 <ul>
	<li><a href="#tabs_list_account_map">List Account Maping</a></li>
	<li><a href="#tabs_history_activity">Call History</a></li>
	<li><a href="#tabs_recording_activity">Recording</a></li>
</ul>
	

<!-- start : acount maping ====================== -->

<div id="tabs_list_account_map" style="background-color:#FFFFFF;overflow:auto;">
<table border=0 align="left" cellspacing=1 width="100%">
   <thead>
	 <tr height='24'>
		<th class="ui-corner-top ui-state-default first" WIDTH="2%" nowrap>&nbsp;No.</td>
		<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;ID Number.</td>
		<th class="ui-corner-top ui-state-default first" WIDTH="10%" nowrap>&nbsp;Account Number</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Class Card</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Data Type</td>
	 </tr>
   </thead>
  <tbody id="list_account_content_map">
  <?php $no = 0; ?>
  <?php if(is_array($AccOthers) ) { 
	foreach( $AccOthers as $rows ) :  
		$bgcolor = ( ($no%2)==1 ?"#FFFFFF":"#FFFEEE");
	?>
		<tr class="onselect" bgcolor="<?php echo $bgcolor;?>" style="cursor:pointer;"
			onclick="Ext.DOM.DetailMapping('<?php echo $rows['AccountNo'];?>');" >
			<td class="content-first center"><?php echo ($no+1);?></td>
			<td class="content-middle left"><?php echo $rows['IdNumber'];?></td>
			<td class="content-middle left"><?php echo $rows['AccountNo'];?></td>
			<td class="content-middle left"><?php echo $rows['ClassCard'];?></td>
			<td class="content-lasted left"><?php echo $rows['DataFrom'];?></td>
		</tr>	
  <?php $no++; endforeach;?>
  <?php } ?>
  </tbody>
 </table>
</div>

<!-- start : load history by Customer -->

<div id="tabs_history_activity" style="overflow:auto;margin-top:-5px;">
 <table border=0 align="left" cellspacing=1 width="100%">
	<thead>
		<tr height='24'>
			<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
			<th class="ui-corner-top ui-state-default first" WIDTH="10%" nowrap>&nbsp;Call Date</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Deskoll</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Account Status</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Call Status</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="5%">&nbsp;SPC</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="5%">&nbsp;RPC</td>
			<th class="ui-corner-top ui-state-default first" >&nbsp;Note</td>
		</tr>
	</thead>
	<tbody id="DebiturHistory"></tbody>
 </table>
</div>

<!-- load :: policy data -->
<div id="tabs_recording_activity" style="background-color:#FFFFFF;overflow:auto;"> 
 <table border=0 align="left" cellspacing=1 width="100%">
   <thead>
	 <tr height='24'>
		<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
		<th class="ui-corner-top ui-state-default first" WIDTH="10%" nowrap>&nbsp;Extension</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;File Name</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;File Size</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Date</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="5%">&nbsp;Duration</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="5%">&nbsp;User ID</td>
	 </tr>
   </thead>
  <tbody id="DebiturRecording"></tbody>
 </table>
</div>
</div>
</fieldset>
