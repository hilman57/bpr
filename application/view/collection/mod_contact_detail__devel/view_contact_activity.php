<?php  
/*
 * @ pack  : view Debitur contact Activity 
 * @ notes : view only here 
 * ----------------------------------------------------
 
 * @ param : Customer < array >
 */
 
?>
<fieldset class='corner' style='margin-top:-10px;margin-bottom:0px;'>
<legend class='icon-customers'>&nbsp;&nbsp;User Activity</legend>
<form name="frmContactActivity">
	<div id="panel-content-tabs-activity">
		<table border=0 cellspacing=0 cellspacing=0 align='center'>
			<tr>
				<td class="text_caption bottom" nowrap>Date&nbsp;:</td>
				<td class="bottom">&nbsp;<?php echo date('d/m/Y');?></td>
				<td class="text_caption bottom" nowrap rowspan=2>WO LPD&nbsp;:</td>
				<td class="bottom" rowspan=2>&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Time &nbsp;:</td>
				<td class="bottom"><?php echo date('H:i:s');?></td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Acc. Status&nbsp;:</td>
				<td class="bottom">
					<span id="id_call_status_code">
						<?php echo form()->combo('select_account_status_code',
							'select auto', $Dropdown['COLL_DROPDOWN_ACS'], $Customer['deb_call_status_code']);?>
					</span>		
				</td>
				<td class="text_caption bottom">Discount&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('text_discount','select auto', $Dropdown[COLL_DROPDOWN_DIS], 10);?> %</td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Last Call&nbsp;:</td>
				<td class="bottom">
					<span id="id_prev_call_status_code">
						<?php echo form()->combo('select_prev_call_status_code',
						'select auto', $Dropdown['COLL_DROPDOWN_CLS'], $Customer['deb_prev_call_status_code']);?>
					</span>
				</td>
				<td class="text_caption bottom" nowrap>Base On&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('select_pay_base_on','select auto', $Dropdown[COLL_DROPDOWN_BON]);?></td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Info PTP&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('select_info_ptp','select auto', $Dropdown[COLL_DROPDOWN_PTP]);?></td>
				<td class="text_caption bottom">Tenor&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('select_tenor','select auto', $Dropdown[COLL_DROPDOWN_TNR]);?></td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>PTP Date&nbsp;:</td>
				<td class="bottom"><?php echo form()->input('text_ptp_date','input_text date');?></td>
				<td class="text_caption bottom" nowrap>Sim&nbsp;:</td>
				<td class="bottom"><?php echo form()->input('text_simulate','input_text', null, null,array('style'=>'width:80px;') );?></td>
			</tr>
			<tr>
				<td class="text_caption" nowrap>&nbsp;</td>
				<td colspan=3 align='center'><span id="payment_schedule"></span></td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Payment&nbsp;:</td>
				<td class="bottom"><?php echo form()->input('text_payment','input_text',null, null, array('style'=>'width:70px;') );?></td>
				<td class="text_caption bottom" nowrap>SPC&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('select_spc','select',$Dropdown[COLL_DROPDOWN_SPC],$Customer['deb_spc']);?></td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Channel Pay&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('select_channel_payment','select auto', $Dropdown[COLL_DROPDOWN_PCH]);?></td>
				<td class="text_caption bottom" nowrap>RPC&nbsp;:</td>
				<td class="bottom"><?php echo form()->combo('select_rpc','select auto',$Dropdown[COLL_DROPDOWN_RPC],$Customer['deb_rpc']);?></td>
			</tr>
			<tr>
				<td class="text_caption bottom" nowrap>Call later&nbsp;:</td>
				<td colspan='3'>
					<?php echo form()->input('text_call_back_date','input_text date');?>
					<?php echo form()->combo('text_call_hour','select', $Dropdown[COLL_DROPDOWN_HRS], '00', null, array('style'=>'width:50px;') );?> :
					<?php echo form()->combo('text_call_minute','select', $Dropdown[COLL_DROPDOWN_MNT], '00', null, array('style'=>'width:50px;') );?>
					</td>
			</tr>
			<tr>
				<td class="text_caption" nowrap>Remarks&nbsp;:</td>
				<td colspan='3'><?php echo form()->textarea('text_activity_notes','textarea', null, array("keyup"=>"Ext.DOM.LimitCounter(this);"), array("style"=> "width:95%;height:80px;") );?></td>
			</tr>
			<tr>
				<td class="text_caption" nowrap>&nbsp;</td>
				<td colspan='3' class='center'>
				<span style='font-style:italic;color:red;'>
					* Jumlah Karakter Yang Tersisa&nbsp;<?php echo form()->input('text_limit_comment','input_text',150, null, array('style'=>'width:30px;','readonly'=>true) );?>&nbsp;Karakter 
				</span>
			</tr>
			
			<tr>
				<td class="text_caption" nowrap>&nbsp;</td>
				<td colspan='3' class='center'> 
					<input type="button" class="button reply" onclick="Ext.DOM.SmsActivity();" value="SMS">
					<input type="button" class="button save"  onclick="Ext.DOM.SaveActivity();"  value="Save">
					<input type="button" class="button clear" onclick="Ext.DOM.ClearActivity();"  value="Clear">
					<input type="button" class="button close" onclick="Ext.DOM.CancelActivity();" value="Close">
				</td>
			</tr>
			
		</table>
	</div>
	</form>
</fieldset>
