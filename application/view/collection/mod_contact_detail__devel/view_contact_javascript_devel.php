<script>

/*
 * Global Atemp Call 
 * by Default is zeroo 
 
 NOTES 20-11-2014
 
 - line 84 : function DisabledActivity(); => Remark Disable chkLabel
 - line 134 : function Ready; => set enable chkLabel
 
 * ------------------------------------------------------------
 * @ change : 2015-01-12 
 * @ notes  : Update untuk unfinish survey / untuk status unfinish survey jika user sudah save maka 
			  data tidak di reset. 
 
 */
 
var TOTAL_CALL_ATEMPT = 0;
var TOTAL_CALL_POLICY = 0;
var TOTAL_SIZE_COUNTER = [];
var OPEN_FORM_FLAG = false;
var MAX_CHAR_LIMIT = 150;
var CHAR_NOTES_ACTIVITY = [];

/*  
 * @ pack : clear form actoviti content # ----------------
 */
 
var reason = [];

/*  
 * @ pack : clear form actoviti content # ----------------
 */
 
 Ext.DOM.initFunc = 
{ 
	isCallPhone : false,
	isCallNumber : '',
	isRunCall : false,
	isCall : false,
	isHangup : false,
	isCancel : true,
	isSave : false,
	isScriptId : 0,	
	isRelease : false,
	isComplete : false,
	isPhoneB64 : 0,
	isCallType : ''
}


/*  
 * @ pack : clear form actoviti content # ----------------
 */
 
 Ext.DOM.ClearActivity = function()
{ 
  Ext.Serialize('frmContactActivity').Clear();	
  Ext.DOM.PaymentSchedule();
}


/*  
 * @ pack : simulateCallculator # ----------------
 */
 
 Ext.DOM.SimulateCallculator = function()
{
 var tots_simulation = 0;
 var tots_percent_size = 0;
 
 var tots_discount = parseInt(Ext.Cmp('text_discount').getValue());
 var tots_baseonpay =parseInt(Ext.Cmp('select_pay_base_on').getValue());	
	 
	 tots_percent_size = ((tots_baseonpay * tots_discount)/100);	
	 tots_simulation = ((tots_baseonpay)-(tots_percent_size));
	 tots_simulation =  accounting.formatMoney(tots_simulation, "", 0, ".", ",");
	 
// @ pack : set value
	 Ext.Cmp('text_simulate').disabled(true);
	 Ext.Cmp('text_payment').setValue(accounting.toFixedNumber(tots_simulation));
	 Ext.Cmp('text_simulate').setValue(tots_simulation);
} 

/*
 * @ pack : PaymentSchedule # ----------------
 */
 
 Ext.DOM.PaymentSchedule = function()
{
  var tots_month = parseInt(Ext.Cmp("select_tenor").getValue());
  var tots_simulate = Ext.Cmp("text_simulate").getValue();
  
 /* @ pack : hitung its.  **/
 if( Ext.Cmp('select_info_ptp').getValue()=='102' )
 {
	Ext.Cmp('select_tenor').disabled(false);	
	if( tots_month > 0 
		&& accounting.toFixedNumber(tots_simulate) > 0) 
	{
		  Ext.Ajax 
		  ({
			 url : Ext.DOM.INDEX +"/ModContactDetail/PaymentSchedule/",
			 method : 'POST',
			 param : {
				month : accounting.formatNumber(tots_month),
				payment : accounting.toFixedNumber(tots_simulate)
			 }
		  }).load("payment_schedule");
		  
	  } else {
		Ext.Cmp("payment_schedule").setText("");
	  }
	  
  }	else {
	Ext.Cmp('select_tenor').disabled(true);
	Ext.Cmp('select_tenor').setValue('');
	Ext.Cmp("payment_schedule").setText("");
  }  
}

/*  
 * @ pack : clear form actoviti content # ----------------
 */
 
 Ext.DOM.DisabledActivity = function() 
{
	
}

/*
 * @ pack : set toolbars  component # ---------------------------------------;
 */

 Ext.DOM.DetailMapping = function( $account ) 
{
   
   Ext.Ajax
  ({
	url    : Ext.DOM.INDEX +'/ModContactDetail/DataAccountMaping/',
	method : 'POST',
	param  :{
		action  : 'onDetail',
		Account : $account
	},
	ERROR : function(e)
	{
		Ext.Util(e).proc(function(response)
		{
			if( response.success ){
				Ext.Ajax 
				({
				  url : Ext.DOM.INDEX +'/ModContactDetail/DataAccountMaping/',
				  method : 'GET',
				  param : {
					action : 'onShow',
					deb_id : response.data.deb_id,
					deb_cmpaign_id : response.data.deb_cmpaign_id
				  }	
				}).load("tabs_list-2");
				
				$('#tabs_list').tabs('select',1);
			} else {
				Ext.Msg("Sorry Data Not Available!").Info();
			}
		});
	}	
  
  }).post();
  
	
}

/*
 * @ pack : set toolbars  component # ---------------------------------------;
 */
 
 Ext.query(function(){
 Ext.query("#tabs" ).tabs();
 Ext.query('#toolbars').extToolbars 
({
	extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
	extTitle  : [['Add Phone'],[],[]],
	extMenu   : [['Ext.DOM.UserWindow'],[],[]],
	extIcon   : [['monitor_edit.png'],[],[]],
	extText   : true,
	extInput  : false,
	extOption  : []
 });
 
/*
 * @ pack : WriteLayoutContent  # ---------------------------------------;
 */
 
 var WriteLayoutContent = function() 
{
 var min_height = -10;	
 $(".contact_detail").css({
	"width" : ($('#main_content').width()),
	"margin-left" : "-8px"
 });
 
 $('#panel-content-tabs-information').css ({
	'padding' : '0px 0px 0px 0px', 
	'margin-left' : '0px',
	'margin-right' : '-12px'
 });
 
 $('#panel-content-tabs-activity').css ({ 
	//'height' : ($('#main_content').height() - 200)
 });
}

/*
 * @ pack : call WriteLayoutContent if on default view  # ---------------------------------------;
 */
 
 WriteLayoutContent();
 
/*
 * @ pack : on resize of the window  # ---------------------------------------;
 */
 
 $('.date').datepicker({dateFormat:'dd-mm-yy'});
 $(window).resize(function(){
	WriteLayoutContent();
 });
 
 $('#main_content').resize(function(){
	WriteLayoutContent();
 });

/*
 * @ pack : on resize of the window  # ---------------------------------------;
 */
 
Ext.Cmp('text_discount').listener({
 onKeyup : function(){ 
	Ext.DOM.SimulateCallculator(); 
	Ext.DOM.PaymentSchedule(); 
  }
});

Ext.Cmp('select_pay_base_on').listener({
 onChange : function(){ 
	Ext.DOM.SimulateCallculator(); 
	Ext.DOM.PaymentSchedule(); 
  }
});

Ext.Cmp('select_tenor').listener({
onChange : function(){ 
	Ext.DOM.PaymentSchedule(); 
  }	
});

Ext.Cmp('select_info_ptp').listener({
onChange : function(){ 
	Ext.DOM.PaymentSchedule(); 
  }	
});

/*
 * @ pack : text_limit_comment # ---------------------------------------;
 */

 Ext.Cmp('text_limit_comment').setValue(MAX_CHAR_LIMIT); 
 if(!Ext.Cmp('LastCallPhoneNumber').empty())
 {
	var isCallNumber = Ext.Cmp('LastCallPhoneNumber').getValue();
		Ext.DOM.initFunc.isCallNumber = Ext.BASE64.decode(isCallNumber);
		
	if(!Ext.Cmp('select_pmr_phone').empty()) {
		Ext.DOM.initFunc.isPhoneB64 = Ext.Cmp('select_pmr_phone').getText(); } 
	else if(!Ext.Cmp('select_ec_phone').empty()) {
		Ext.DOM.initFunc.isPhoneB64 = Ext.Cmp('select_ec_phone').getText();  } 
	else if(!Ext.Cmp('select_add_phone').empty()) {
		Ext.DOM.initFunc.isPhoneB64 = Ext.Cmp('select_add_phone').getText(); };
	Ext.DOM.initFunc.isCallPhone = true;
		
 } else {
	console.log('Last call phone is empty ');
 }
  
  
  $('.select').bind('click', function(){
	Ext.DOM.SetAutoNotes();
  });
  
  $('.image-calls').dblclick(function(e){
	console.log("double-clicked but did nothing");
	e.stopPropagation();
	e.preventDefault();
	return false;
  
  })
  
}); 

// @ pack : --> end $

 
/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.LimitCounter = function( text )
{
	if( text.value.length > MAX_CHAR_LIMIT ){
		text.value = text.value.substring( 0, MAX_CHAR_LIMIT );
	} else {
		Ext.Cmp('text_limit_comment').setValue(( MAX_CHAR_LIMIT - text.value.length ));
	}
}

/*
 * @ pack : block number if 5x  # ---------------------------------------;
 */
 
 Ext.DOM.BlockPhoneFiveCall = function()
{
  var callData = Ext.DOM.initFunc; 
  
  if( typeof( callData )!='object'){ return false; }
  
 // @ pack : set to log its # -----------------------------
 if( callData.isPhoneB64 !=0 )
 {
	 Ext.Ajax 
	({
		url 	: Ext.DOM.INDEX +'/ModFiveCall/SaveFiveCall/',
		method  : 'POST',
		param   : {
			CustomerId  : Ext.Cmp('CustomerId').getValue(),
			PhoneType   : callData.isPhoneB64.split('-')[0],
			PhoneNumber : callData.isCallNumber,
		},
		ERROR : function(e) {
			Ext.Util(e).proc( function( response ) {
				if( response.success ){
					return true;
				} else {
					return false;
				}
			});
		}
	}).post();
  }	
  
 } // ==> BlockPhoneFiveCall
 
/*
 * @ pack : block number if 5x  # ---------------------------------------;
 */
 
 Ext.DOM.IsFiveCall = function()
{
 var argv = false;
 
 if( (Ext.DOM.initFunc.isCallPhone == true ) && 
	Ext.DOM.initFunc.isPhoneB64 !=0 ) 
  {
	var argv_result =( Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX +'/ModFiveCall/CounterFiveCall/',
			method  : 'POST',
			param   : {
				CustomerId  : Ext.Cmp('CustomerId').getValue(),
				PhoneType   : Ext.DOM.initFunc.isPhoneB64.split('-')[0],
				PhoneNumber : Ext.DOM.initFunc.isCallNumber,
			} 
		}).json()
	);
	
  return argv_result;
  
  
}

 return argv;
	
} // ==> Ext.DOM.IsFiveCall
/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.DialCustomer = function()
{
 
// @ pack : step 1 

 if( !Ext.DOM.initFunc.isCallPhone){
	Ext.Msg('Please select phone number to call').Info();
	return false;
 }

// @ pack : step 2

 if( Ext.DOM.initFunc.isRunCall==true ){
	Ext.Msg('Call is running').Info();
	return false;
 }
 
// @ pack : step 3

 if( Ext.DOM.initFunc.isCallNumber==''){
	Ext.Msg('No phone number to call').Info();
	return false;
 } 
 
// @ pack : step 3 

var $call_block_check = Ext.DOM.IsFiveCall(); // set five call 

if(($call_block_check.count == true)){
	Ext.Msg('No phone number '+ Ext.DOM.initFunc.isPhoneB64 +' is block').Info();
	return false;
} 

// @ pack : step 4

if(($call_block_check.lock == true)){
	Ext.Msg('Account Number is Block ').Info();
	return false;
} 

 
// @ pack : set dial customer 

ExtApplet.setData({   
	Phone : Ext.DOM.initFunc.isCallNumber,
	CustomerId : Ext.Cmp("CustomerAccountNo").getValue() 
 }).Call();
  
 console.log(ExtApplet);
 
 Ext.DOM.initFunc.isRunCall = true;	
 Ext.DOM.initFunc.isCall = true;
 Ext.DOM.initFunc.isCallPhone = true;
 Ext.DOM.initFunc.isCancel = false;

 window.setTimeout(Ext.DOM.BlockPhoneFiveCall() ,30000) // 5 call 
 
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.HangupCustomer =function()
{
  if( Ext.DOM.initFunc.isCall == true ) {	
	  Ext.DOM.initFunc.isRunCall = false;
	  Ext.DOM.initFunc.isCancel = false;
	  Ext.DOM.initFunc.isHangup = true;
	  ExtApplet.setHangup();
 } else {
	Ext.Msg('Please call before ').Info();
 }	
  return;
} 

/*
 * @ pack : Ext.DOM.getCallReasultId Interface  # ---------------------------------------;
 */
 
 Ext.DOM.getCallReasultId = function(combo)
{
			
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
Ext.DOM.CallSessionId = function(){
	// try{
		// return ( typeof (ExtApplet.ctiCallSessionId ) =='undefined' ? 
				// 'NULL': ExtApplet.ctiCallSessionId );
	// }catch(a){
		// return "";
	// }
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.NextActivity = function()
{
	
}
 
/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.SaveActivity =function() 
{	

// @ pack cek valid of form 

var ARGV_VARS = [];
var CONDS_VARS = Ext.Serialize('frmContactActivity').Required([
	'select_account_status_code','select_prev_call_status_code',
	'text_activity_notes','select_rpc','select_spc'
 ]);
 
// ==> check of dial for save data  
 if( Ext.DOM.initFunc.isCall == false ){
	Ext.Msg('Please Call Before Save Status Activity').Info();
	return false;
 }

// ==> check condition if form complete 
 
 if( CONDS_VARS )
 {
   ARGV_VARS['DebiturId'] = Ext.Cmp('CustomerId').getValue();
   ARGV_VARS['CallNumber'] = Ext.DOM.initFunc.isCallNumber;
   
   Ext.Ajax
	({
		url    : Ext.DOM.INDEX +"/ModSaveActivity/SaveActivity",
		method : 'POST',
		param  : Ext.Join([ 
					Ext.Serialize('frmContactActivity').getElement(), 
					ARGV_VARS 
				]).object(),
				
		ERROR : function( e ){
			Ext.Util(e).proc(function(response){
				if( response.success ){
					Ext.Msg("Save Activity").Success();
					Ext.DOM.CallHistory(0);
					Ext.DOM.PaymentPTP(0); 
				} else {
					Ext.Msg("Save Activity").Failed();
				}
			});
		}		
	}).post();
	
} else {
	Ext.Msg("Please input form").Info();
}
 		
}


/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.SaveNotes = function()
{
  if(!Ext.Msg('Do you want to save this notes ?').Confirm() ) {
	return false;
  }
  
 // ==> then  
 
   Ext.Ajax
   ({
		url 	: Ext.DOM.INDEX +"/ModSaveActivity/SaveNotesActivity",
		method 	: 'POST',
		param 	: {
			notes_reminder : Ext.Cmp('text_call_notes').getValue().toUpperCase(),
			DebiturId : Ext.Cmp('CustomerId').getValue()
		},
		ERROR 	: function(e){
			Ext.Util(e).proc(function(response){
				if( response.success ){
					Ext.Msg('Save Notes Reminder').Success();
				} else {
					Ext.Msg('Save Notes Reminder').Failed();
				}
			});
		}
	}).post();
	
 }

/*
 * @ pack : claim data if action its.  # ---------------------------------------;
 */
 
 Ext.DOM.SaveClaim = function()
{
	 Ext.Ajax
   ({
		url 	: Ext.DOM.INDEX +"/ModSaveActivity/SaveClaimActivity",
		method 	: 'POST',
		param 	: {
			DebiturId : Ext.Cmp('CustomerId').getValue()
		},
		ERROR 	: function(e){
			Ext.Util(e).proc(function(response){
				if( response.success ){
					Ext.Msg('Claim Data').Success();
				} else {
					Ext.Msg('Claim Data').Failed();
				}
			});
		}
	}).post();
 }
 
 
/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.CallHistory = function( page )
{
	Ext.Ajax({
		url : Ext.DOM.INDEX+"/ModCallHistory/CustomerHistoryPage/",
		method :'GET',
		param :{
			DebiturId : Ext.Cmp('CustomerId').getValue(),
			page  : ( page ? page : 0 )
		}
	}).load('DebiturHistory');
}


/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.Recording = function( page )
{
	Ext.Ajax({
		url : Ext.DOM.INDEX+"/ModVoiceData/VioicePage/",
		method :'GET',
		param :{
			DebiturId : Ext.Cmp('CustomerId').getValue(),
			page  : ( page ? page : 0 )
		}
	}).load('DebiturRecording');
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */

 Ext.DOM.NewWindowMediaPlayer = function( RecordingId )
{
	Ext.Window
	({
		url   : Ext.DOM.INDEX +'/ModVoiceData/NewPlayWindow/',
		width : 500, height :250,
		param : {
			RecordId : RecordingId
		}
	}).popup();
}



/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.PaymentPTP = function( page )
{
	Ext.Ajax({
		url : Ext.DOM.INDEX+"/PaymentPTP/PaymentHistoryPage/",
		method :'GET',
		param :{
			DebiturId : Ext.Cmp('CustomerId').getValue(),
			page  : ( page ? page : 0 )
		}
	}).load("PaymentHistoryPage");
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.SmsOutbox = function( page )
{
	Ext.Ajax({
		url : Ext.DOM.INDEX+"/SMSOutbox/SMSOutboxByCutomerId/",
		method :'GET',
		param :{
			DebiturId : Ext.Cmp('CustomerId').getValue(),
			page  : ( page ? page : 0 )
		}
	}).load("sms_customer_outbox");
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.SmsInbox = function( page )
{
	Ext.Ajax({
		url : Ext.DOM.INDEX+"/SMSInbox/SMSInboxCutomerId/",
		method :'GET',
		param :{
			DebiturId : Ext.Cmp('CustomerId').getValue(),
			page  : ( page ? page : 0 )
		}
	}).load("sms_customer_inbox");
}



/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.AjaxStart = function()
{
	// Ext.Cmp('html_ajax_loading').setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='10px;'> <span style='color:red;'>Please Wait...</span>");
} 
 
/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.AjaxStop = function()
{
	//Ext.Cmp('html_ajax_loading').setText('<span style="color:#dddddd;">-</span>');
} 

 
/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.IsMatch = function()
{
	var thisRegex = new RegExp('-');
	if(!thisRegex.test(Ext.DOM.initFunc.isPhoneB64)){
		return false
	} else {
		return true;
	}
}

/*
 * @ pack : get auto generate text 
 */
 
 Ext.DOM.SetAutoNotes = function( param )
{
 /*  contoh : OCM2 /CH /POP 3-PTP */ 
 var param = [];
	param[0] = (Ext.DOM.initFunc.isCallType=='' ? null : Ext.DOM.initFunc.isCallType);
	param[1] = ( Ext.DOM.IsMatch() ? Ext.DOM.initFunc.isPhoneB64.split('-')[0] : null );
	param[2] = (Ext.Cmp('select_spc').empty() ? null : Ext.Cmp('select_spc').getText()) ;
	param[3] = (Ext.Cmp('select_account_status_code').empty() ? null : Ext.Cmp('select_account_status_code').getText() ) ;
	param[4] = (Ext.Cmp('select_prev_call_status_code').empty() ? null : Ext.Cmp('select_prev_call_status_code').getText() ) ; 
 
 var asJoin = Ext.Join([param]).object(), asText = '';
 var asIsLength = ((Object.keys(asJoin).length)-2);
 
  for( var i in asJoin ) 
 {
	if( asJoin[i]!==null ) {
		if( i== asIsLength ){
			asText += asJoin[i] +" - "
		} else {
			asText += asJoin[i] +" / "
		}
	 }
 }	
 Ext.Cmp('text_activity_notes').setValue(asText.substring(0, ((asText.length)-2)));
 
}

/*
 * @ pack : Ext.DOM.LimitCounter looking to User Interface  # ---------------------------------------;
 */
 
 Ext.DOM.SetCallDial = function( object ) 
{

var argv_phone = ['select_pmr_phone','select_ec_phone','select_add_phone'],
	argv_type  = "";

 if( ( object.value !='') )
 {
	 
   if( object.name =='select_pmr_phone') { 
		Ext.DOM.initFunc.isCallType = "PRIMARY"; }
   if( object.name =='select_ec_phone')  { 
		Ext.DOM.initFunc.isCallType = "EMERGENCY"; }
   if( object.name =='select_add_phone') { 
		Ext.DOM.initFunc.isCallType = "ADDITIONAL";}
    
	Ext.DOM.initFunc.isCallNumber = Ext.BASE64.decode(object.value);
	Ext.DOM.initFunc.isPhoneB64 = Ext.Cmp(object.id).getText();
	Ext.DOM.initFunc.isCallPhone = true;
	
	for( var i in argv_phone ){
		if( argv_phone[i]!=object.name ){
			Ext.Cmp(argv_phone[i]).setValue('');
		}
	}
  }
  
  return; 
} //==>Ext.DOM.SetCallDial


/*
 * @ pack : Ext.DOM.RemoveAccessKeys  # ---------------------------------------;
 */

 Ext.DOM.RemoveAccessKeys = function ()
 {
	var Validation= Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/SrcAksesAll/RemoveKeys',
				method 	: 'POST',
				param 	: { CustomerId : Ext.Cmp("CustomerId").getValue() }	
		}).json();
 }
 
/*
 * @ pack : Ext.DOM.CancelActivity  # ---------------------------------------;
 */
 
 Ext.DOM.CancelActivity =function()
{
 
// @ pack : call in running can't cancel  
 if( Ext.DOM.initFunc.isRunCall ==true  ){
	 Ext.Msg('You must hangup call for exit from this session.\n\rThank\'s you').Info();
	 return false;	
  }

// @ pack : update is call = 0 handle its 

var UpdateCall = Ext.DOM.UpdateCall(),
	IsAksessAll  = Ext.Cmp('IsAksessAll').getValue();

// @ pack : if update OK please 

if( UpdateCall.success==0 ){
	 Ext.Msg('Failed update is call').Info();
	 return false;	
}

// @ pack : if update OK please 	
if((IsAksessAll==1)) {
	 Ext.DOM.RemoveAccessKeys();
 }
  
  Ext.ActiveMenu().Active();
  ControllerId = Ext.Cmp("ControllerId").getValue();
   Ext.EQuery.Ajax 
  ({
	 url 	: ControllerId,
	 method 	: 'GET',
	 param 	: { act : 'back-to-list' }
  });
  
 } //==>Ext.DOM.CancelActivity
 
/*
 * @ pack : Ext.DOM.UserWindow::adphone  # ---------------------------------------;
 */
  
 Ext.DOM.SaveBlock=function()
{

// @ pack : -----------------------------

  if(!Ext.DOM.initFunc.isCallPhone){
	Ext.Msg('Please Select phone number to blocking').Info();
	return false;
  }
 
// @ pack : -----------------------------
 
 var MessageText = prompt("Do you want to block "+ Ext.DOM.initFunc.isPhoneB64+"\n\rRemark", "");
 
 if( MessageText !=null ){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/ModSaveActivity/SaveBlocking/",
		method 	: "POST",
		param 	: {
			CustomerId  : Ext.Cmp("CustomerId").getValue(),
			PhoneNumber : Ext.DOM.initFunc.isCallNumber,
			PhoneText   : Ext.DOM.initFunc.isPhoneB64,
			MessageText : MessageText 
		},
		ERROR :function( e ) {
			Ext.Util(e).proc(function( response ){
				if( response.success ){
					Ext.Msg("Blocking Number").Success();
				} else {
					Ext.Msg("Blocking Number").Success();
				}
			});
		}
		
	}).post();
 }
}

/*
 * @ pack : udate status is call = 0 
 * -------------------------------------------------
 */
Ext.DOM.UpdateCall = function( ){
  return( 
	Ext.Ajax({
		url    : Ext.DOM.INDEX +'/ModContactDetail/setUdateCall/',
		method : 'GET',
		param  : {
			CustomerId : Ext.Cmp("CustomerId").getValue()
		}
	}).json()
  );
}

/*
 * @ pack : Ext.DOM.UserWindow::adphone  # ---------------------------------------;
 */
 
 Ext.DOM.UserWindow = function()
{
	Ext.DOM.AdditionalPhone( Ext.Cmp('CustomerId').getValue() );
	return false;
}

/*
 * @ pack : display None or disabled if is Aksess ALL 
 */
 
 Ext.DOM.IsAksesAll = function()
{
  var IsAksessAll  = Ext.Cmp('IsAksessAll').getValue();
	if((IsAksessAll==0)){
	    $('#btnClaim').css({ 'color':'#DDDDDD'});
		$('#btnClaim').attr('disabled', true);  
	}
	else{
		$('#btnClaim').css({'display' : 'yes' });
		$('#btnClaim').attr('disabled', false);
	} 
}	

/*
 *
 */
 
 Ext.DOM.FormWindow = function( form ) 
{
 if( (typeof(form)=='string') && ( form!='')){
	Ext.Window
	({
		url 	: Ext.DOM.INDEX + '/'+ form +"/html/",
		method  : 'POST',
		width 	: ($(window).width()-( $(window).width()/3)),
		height  : ($(window).height()),
		name 	: 'form_'+form,
		scrollbars : 1,
		resizable  : 1, 
		param 	: {
			DebiturId : Ext.BASE64.encode(Ext.Cmp('CustomerId').getValue()),
		}
	}).popup();
 }
}
/*
 * @ pack : Ext.DOM.UserWindow::adphone  # ---------------------------------------;
 */
 
Ext.DOM.SmsActivity = function(){
Ext.Window
	({
		url 	: Ext.DOM.INDEX +"/ModContactDetail/CreateNewSms/",
		method  : 'POST',
		width 	: (($(window).width()/2)-100),
		height  : (($(window).height()/2)+50),
		name 	: 'form_sms',
		scrollbars : 1,
		resizable  : 1, 
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue(),
		}
	}).popup();
}

/*
 * @ pack : Ext.DOM.UserWindow::adphone  # ---------------------------------------;
 */
 $('document').ready(function() {
	Ext.DOM.CallHistory(0); // ==> load history by customer
	Ext.DOM.Recording(0); // ==> load recording by customer 
	Ext.DOM.PaymentPTP(0);  // ==> load PTP history by customer
	Ext.DOM.SmsOutbox(0); // ==> load SMS Outbox by customer
	Ext.DOM.SmsInbox(0); // ==> load SMS Inbox by customer
	Ext.DOM.IsAksesAll(); // ==> disabled button aksess all
});


</script>