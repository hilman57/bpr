<?php
/* @ def 	 : view upload manual
 * 
 * @ param	 : sesion  
 * @ package : bucket data 
 */
 
?>
<div class="box-shadow-xxx" style="padding-lef:-2px;">
<fieldset class='corner'>

<legend class="icon-campaign">&nbsp;&nbsp;Upload Data </legend>	
<div style="border-left:0px solid #dddddd;text-align:top;margin-top:-10px;">
	<table align="left" border=0 cellpadding="5px">
		<tr>
			<td class="left text_caption bottom" style='height:24px;' nowrap># Template &nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><?php echo form() -> combo('upload_template','select long', (isset($Template) ? $Template : null)); ?></td>
		</tr>
		
		<tr>
			<td class="left text_caption bottom" style='height:24px;' nowrap># Product&nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><?php echo form() -> combo('upload_campaign','select long', (isset($CampaignId) ? $CampaignId : null)); ?></td>
		</tr>
		
		<tr>
			<td class="left text_caption bottom" style='height:24px;' nowrap># File location&nbsp;</td>
			<td class="center text_caption bottom">:</td>	
			<td class="left bottom"><?php echo form() -> upload('fileToupload'); ?></td>
		</tr>
		<tr>
			<td class="left text_caption bottom" style='height:24px;'>&nbsp;</td>
			<td class="center text_caption bottom">&nbsp;</td>	
			<td class="left bottom"><span id="loading-image"></span></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td class="left bottom" style='padding-left:12px;' colspan=2> 
				<input type="button" class="save button" onclick="Ext.DOM.Upload();" value="Upload">
				<input type="button" class="close button" onclick="Ext.Cmp('UploadSwapAccount').setText('');" value="Close">
			</td>
		</tr>
	</table>
</div>
</fieldset>
</div>