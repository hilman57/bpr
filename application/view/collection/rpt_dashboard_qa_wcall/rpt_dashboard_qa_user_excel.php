<?php

/*
 * @ def : source EXCEL 
 *
 * ---------------------------------------------------------------
 */
 
$this->load->helper('EUI_ExcelWorksheet'); // load library excel 
  
$BASE_EXCEL_PATH_TEMP = APPPATH .'temp';
$BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_TEMP ."/". date('YmdHi') ."_". str_replace(" ","_", $title) .".xls";
$excel_workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
$excel_worksheet =& $excel_workbook->addworksheet();

// set style layout header 

 $excel_title =& $excel_workbook->addformat();
 $excel_title->set_bold();
 $excel_title->set_size(10);
 $excel_title->set_color('white');
 $excel_title->set_align('left');
 $excel_title->set_align('vcenter');
 $excel_title->set_fg_color('red');
 $excel_title->set_border(1);
 $excel_title->set_border_color('blue');
 
// set headers  
$excel_rows = 0;

$step_haders_one = array('No.','Caller Name','Total Data','New','Pending','Capacity','Efective Work Days', 'Productivity');

///////////////////////////////////////////////////////

$excel_rows = $excel_rows+1; // baris satu 

$excel_worksheet->write($excel_rows, 0, "No.", $excel_title);
$excel_worksheet->write($excel_rows, 1, "Caller Name", $excel_title);
$excel_worksheet->write($excel_rows, 2, "", $excel_title);
$excel_worksheet->write($excel_rows, 3, "QA Monitored", $excel_title);

$n = 4;
if(is_array($CmpName))
	foreach($CmpName as $id => $name) {
	$excel_worksheet->write($excel_rows, $n, "", $excel_title);	
	$n+=1;	
}
 
//$excel_worksheet->write($excel_rows, $n, "", $excel_title);

// $excel_worksheet->write($excel_rows, $n,"Pending", $excel_title);

// $n_x = ($n+1);
// if(is_array($CmpName))
// foreach($CmpName as $id => $name){
 // $excel_worksheet->write($excel_rows, $n_x, "", $excel_title);	
 // $n_x+=1;
// }

$excel_worksheet->write($excel_rows, ($n-1), "Capacity", $excel_title);
$excel_worksheet->write($excel_rows, $n, "Efective Work Days", $excel_title);
$excel_worksheet->write($excel_rows, $n+1, "Productivity", $excel_title);

///////////////////////////////////////////////////////

$excel_rows = $excel_rows+1; // baris dua 

$excel_worksheet->write($excel_rows, 0, '', $excel_title);
$excel_worksheet->write($excel_rows, 1, '', $excel_title);
$excel_worksheet->write($excel_rows, 2, "Jumlah", $excel_title);

$cols = 3;
if(is_array($CmpName))
	foreach($CmpName as $id => $name) {
	$excel_worksheet->write($excel_rows, $cols, str_replace("<br/>"," ", $name), $excel_title);	
	$cols++;
}

// $excel_worksheet->write($excel_rows, $cols, "Jumlah", $excel_title);	
	
// $cols_x = $cols+1;
// if(is_array($CmpName))
	// foreach($CmpName as $id => $name) {
	// $excel_worksheet->write($excel_rows, $cols_x, str_replace("<br/>"," ", $name), $excel_title);	
	// $cols_x++;
// }	

$excel_worksheet->write($excel_rows, $cols, "", $excel_title);
$excel_worksheet->write($excel_rows, $cols+1,"", $excel_title);
$excel_worksheet->write($excel_rows, $cols+2,"", $excel_title);

// START CONTENT
$excel_content=& $excel_workbook->addformat();
$excel_content->set_size(10);
$excel_content->set_align('left');
$excel_content->set_border(1);
$excel_content->set_border_color('blue');

// format number 
$excel_number=& $excel_workbook->addformat();
$excel_number->set_size(10);
$excel_number->set_align('right');
$excel_number->set_border(1);
$excel_number->set_border_color('blue');

$excel_rows = $excel_rows+1;
$excel_nums = 1;

if(is_array($UserQA)) 
foreach($UserQA as $UserId => $UserName )
{
	$cols_contents = 0;
	$excel_worksheet->write($excel_rows, $cols_contents, $excel_nums, $excel_content );
	$excel_worksheet->write($excel_rows, $cols_contents+1, $UserName, $excel_content );
	$excel_worksheet->write($excel_rows, $cols_contents+2, ($data_size[$UserId]['tot_data_size']? $data_size[$UserId]['tot_data_size'] : 0), $excel_number );
	
	$cols_contents = 3;
	foreach($CmpName as $CampaignId => $name) {
		$excel_worksheet->write($excel_rows, $cols_contents, ($data_size[$UserId][$CampaignId]['tot_size_monitored'] ? $data_size[$UserId][$CampaignId]['tot_size_monitored']:'0'), $excel_number );
		$cols_contents++;
	}
	
	// $cols_contents = $cols_contents;
	// $excel_worksheet->write($excel_rows, $cols_contents, ($data_size[$UserId]['tot_pol_utilize']?$data_size[$UserId]['tot_pol_utilize']:0), $excel_number );
	
	// $cols_contents = $cols_contents+1;
	// foreach($CmpName as $CampaignId => $name) {
		// $excel_worksheet->write($excel_rows, $cols_contents, ($data_size[$UserId][$id]['tot_size_pending'] ? $data_size[$UserId][$id]['tot_size_pending'] : '0'), $excel_number );
		// $cols_contents+=1;
	// }
	
	$tot_capacity_days = ($data_size[$UserId]['tot_capacity_day']?$data_size[$UserId]['tot_capacity_day']:0);
	$tot_efective_days = ($data_size[$UserId]['tot_efective_days']?$data_size[$UserId]['tot_efective_days']:0);
	$tot_size_data = ($data_size[$UserId]['tot_data_size']?$data_size[$UserId]['tot_data_size']:0);
	
	// callcuation 
	$tot_percent_product = ( $tot_size_data ? round( ( ($tot_size_data/($tot_capacity_days*$tot_efective_days))*100),2) : 0 );
	
	$excel_worksheet->write($excel_rows, $cols_contents, "{$tot_capacity_days}", $excel_number );
	$excel_worksheet->write($excel_rows, $cols_contents+1, "{$tot_efective_days}", $excel_number );
	$excel_worksheet->write($excel_rows, $cols_contents+2, "{$tot_percent_product}%", $excel_number );
				
			
	$excel_nums++;
	$excel_rows+=1;
}

//  then start rows  
 
 $excel_workbook->close();

// start download file 

if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	
 

?>