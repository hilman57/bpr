<?php if(!_have_get_session('UserId')) exit('Expired Session ID '); ?>
<fieldset class="corner">
	<legend class="icon-menulist">&nbsp;&nbsp;<b><?php __($title_1); ?></b></legend>
	<?php
		
		__('<table cellspacing=1 cellpadding=4 style="margin-bottom:20px;">
				<tr height=22 bgcolor="#E25C77" style="color:white;">
				 <th class="center" rowspan="2">No.</th>
				 <th class="center" rowspan="2" nowrap>Caller Name</th>
				 <th class="center" colspan="'.(count($CmpName)+1).'">QA Monitored</th>
				 <th class="center" rowspan="2">Capacity</th>
				 <th class="center" rowspan="2">Effective Work Days</th>
				 <th class="center" rowspan="2">Productivity</th>
				 <th class="center" rowspan="2">#CCO<br/>survey/day</th>
				</tr>
				
				<tr height=22 bgcolor="#E25C77" style="color:white;">
				  <th class="center">Jumlah</th>');
				 
			foreach($CmpName as $id => $name)
			{
				__(' <th class="center">'.$name.'</th> ');
			}
			
			__('</tr> ');
			
		/* CONTENT REPORT */
		
		$no = 0;
		
		foreach($UserQuality as $UserId => $UserName )
		{
			$color = ($no%2!=0?'#FFFCCC':'#FFFFFF');
			$no++;
		
			__("<tr height=\"22\" style=\"color:#7e009d;border:0px solid #AAAAAA;background-color:".$color."\">
					<td class=\"content-first center\" style=\"text-align:center;\">".$no."</td>
					<td class=\"content-middle left\" nowrap>&nbsp;".$UserName."&nbsp;</td>
					<td class=\"content-middle center\">".($data_size[$UserId]['tot_data_size']?$data_size[$UserId]['tot_data_size']:'0')."</td>");
					$Total +=($data_size[$UserId]['tot_data_size']?$data_size[$UserId]['tot_data_size']:'0');
					$total_data_peruser[$UserId] +=$data_size[$UserId]['tot_data_size']; 
			$i=0;
			foreach($CmpName as $id => $name)
			{
				__(' <td class="content-middle center">'.($data_size[$UserId][$id]['tot_size_monitored']?$data_size[$UserId][$id]['tot_size_monitored']:'0').'</td> ');
				$i++;
				$total_percampaign[$id] += ($data_size[$UserId][$id]['tot_size_monitored']?$data_size[$UserId][$id]['tot_size_monitored']:'0');
			}
			
			__("<td class=\"content-middle center\">".($data_size[$UserId]['tot_capacity_day']?$data_size[$UserId]['tot_capacity_day']:'0')."</td>
				<td class=\"content-middle center\">".($data_size[$UserId]['tot_efective_days']?$data_size[$UserId]['tot_efective_days']:'0')."</td>");
				
			// calculation here 
				$capaicty_size = (INT)$data_size[$UserId]['tot_capacity_day'];
				$effective_days = (INT)$data_size[$UserId]['tot_efective_days'];
				
				$productivity[$UserId]= ( $total_data_peruser[$UserId] ? round((($total_data_peruser[$UserId]/ ( $effective_days*$capaicty_size))*100), 2) : 0 );
				
				
			__("<td class=\"content-middle center\">{$productivity[$UserId]}</td>	
				<td class=\"content-lasted center\">0</td>	
			</tr>");
		}
		
		__("<tr height=\"22\" style=\"color:#7e009d;border:0px solid #AAAAAA;background-color:#FFFDDD;\">
			<td class=\"content-first  center\" nowrap colspan ='2'>&nbsp;<b>TOTAL</b>&nbsp;</td>
			<td class=\"content-middle  center\" nowrap ><b>".$Total."</b></td>");
			foreach($CmpName as $id => $name){
				__("<td class=\"content-middle center\" nowrap> <b>".$total_percampaign[$id]."</b></td>");
			}
		__("</tr>");
		
		/* END OF CONTENT REPORT */
			
		__(' </table> ');
	?>
</fieldset>
<div> &nbsp;</div>
<fieldset class="corner">
	<legend class="icon-menulist">&nbsp;&nbsp;<b><?php __($title_2); ?></b></legend>
	<?php
		
		__('<table cellspacing=1 cellpadding=4 style="margin-bottom:20px;">
				<tr height=22 bgcolor="#E25C77" style="color:white;">
				 <th class="center" rowspan="2">No.</th>
				 <th class="center" rowspan="2" nowrap>All User Welcome Call</th>
				 <th class="center" colspan="'.(count($CmpName)+1).'">QA Monitored</th>
				 <th class="center" rowspan="2">% QA</th>
				</tr>
				
				<tr height=22 bgcolor="#E25C77" style="color:white;">
				  <th class="center">Jumlah</th>');
				 
			
	/** like here **/
	
		 if(is_array($CmpName) ) 
			foreach($CmpName as $id => $name) 
		  {
				__("<th class=\"center\">{$name}</th> ");
		  }
		
		__('</tr> ');
			
		
	/* name space o header followup **/
	
		$num = 0;
		if(is_array($FU_Title) )
			foreach( $FU_Title as $FU_ID => $FU_Name )
		{
			$color = ($num%2!=0?'#FFFCCC':'#FFFFFF');
			$num++;
		
		
		/** set on varibale **/
		
			$total_per_followup = ( $Fu_data_size[$FU_ID]['total_size'] ? $Fu_data_size[$FU_ID]['total_size']  : 0 );
			
		 __("<tr height=\"22\" style=\"color:#7e009d;border:0px solid #AAAAAA;background-color:{$color};\">
					<td class=\"content-first center\" style=\"text-align:center;\">{$num}</td>
					<td class=\"content-middle left\" nowrap>{$FU_Name}</td>
					<td class=\"content-middle center\">{$total_per_followup}</td>");
			
			
		/** campaign name **/
			
			if( is_array( $CmpName) ) 
				foreach($CmpName as $CampaignId => $name) 
			{
				$total_per_campaign = ($Fu_data_size[$FU_ID][$CampaignId]?$Fu_data_size[$FU_ID][$CampaignId]:'0');
				__("<td class=\"content-middle center\">{$total_per_campaign}</td>");
			}
			
		/** get rate of followup per total all **/
		
			$total_persent_quality = ( $total_per_followup ? round( (( $total_per_followup/$Total)*100), 2 ) : 0 );
			__("<td class=\"content-middle right\">{$total_persent_quality}%</td> </tr>");
			
		}
		
		/* END OF CONTENT REPORT */
			
		__(' </table> ');
	?>
</fieldset>