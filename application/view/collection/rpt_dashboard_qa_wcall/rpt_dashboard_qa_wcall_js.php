<?php if(!_have_get_session('UserId')) exit('Expired Session ID '); ?>

<script>
	(function($ui){ 
		 $ui.prototype.SegmentLabels = function(object) 
		 {
			Ext.Ajax  
			({
				url    : object.url,
				method : 'POST',
				param  : object.param
			}).load(object.load);
			
			Ext.Cmp(object.labelId).setText(object.labelText);
		}
	})(E_ui);
	
	Ext.DOM.SetParam = function()
	{
		var report_type = Ext.Cmp('report_type').getValue();
		
		switch(report_type)
		{
			case 'type_user_qa' :
				Ext.SegmentLabels({					
					labelId : 'label_param1',
					labelText : 'User QA',
					url : Ext.DOM.INDEX+"/RptDashboardQaWellCall/LoadQa/",
					param  : {
						id : 'quality_id'
					},
					load : 'content_param1'
				});
				
				Ext.SegmentLabels({					
					labelId : 'label_param2',
					labelText : 'Campaign',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadCampaign/",
					param  : {
						id : 'campaign'
					},
					load : 'content_param2'
				});
				
				Ext.SegmentLabels({					
					labelId : 'label_param3',
					labelText : 'Param 3',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadEmptyCombo/",
					param  : {
						id : 'filter_param3'
					},
					load : 'content_param3'
				});
				
			break;
			
			case 'type_followup' :
				Ext.SegmentLabels({					
					labelId : 'label_param1',
					labelText : 'Campaign',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadCampaign/",
					param  : {
						id : 'campaign'
					},
					load : 'content_param1'
				});
				
				Ext.SegmentLabels({					
					labelId : 'label_param2',
					labelText : 'Param2',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadEmptyCombo/",
					param  : {
						id : 'filter_param2'
					},
					load : 'content_param2'
				});
				
				Ext.SegmentLabels({					
					labelId : 'label_param3',
					labelText : 'Param 3',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadEmptyCombo/",
					param  : {
						id : 'filter_param3'
					},
					load : 'content_param3'
				});
			break;
			
			default :
				Ext.SegmentLabels({					
					labelId : 'label_param1',
					labelText : 'Param 1',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadEmptyCombo/",
					param  : {
						id : 'filter_param1'
					},
					load : 'content_param1'
				});
				
				Ext.SegmentLabels({					
					labelId : 'label_param2',
					labelText : 'Param2',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadEmptyCombo/",
					param  : {
						id : 'filter_param2'
					},
					load : 'content_param2'
				});
				
				Ext.SegmentLabels({
					labelId : 'label_param3',
					labelText : 'Param 3',
					url : Ext.DOM.INDEX+"/RptDashboardWellCall/LoadEmptyCombo/",
					param  : {
						id : 'filter_param3'
					},
					load : 'content_param3'
				});
			break;
		}
	}
	
	Ext.document(document).ready(function()
	{
		Ext.Cmp('legend_title').setText( Ext.System.view_file_name());

		// date picker 

		Ext.query('.date').datepicker ({
			showOn : 'button', 
			buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
			buttonImageOnly	: true, 
			dateFormat : 'dd-mm-yy',
			readonly:true,
			onSelect	: function(date){
				if(typeof(date) =='string'){
					var x = date.split('-');
					var retur = x[2]+"-"+x[1]+"-"+x[0];
					if(new Date(retur) > new Date()) {
						Ext.Cmp($(this).attr('id')).setValue('');
					}
				}
			}
		});
		
		Ext.Cmp('report_type').listener
		({
			'onChange' : function(e){
				Ext.Util(e).proc(function(obj){
					Ext.DOM.SetParam();
				});
			}
		});
		
		$('#panel-report').css({
			'width' : ($(window).width()-35)+"px",
			'overflow' : 'auto'
		});
	});
	
	// Ext.DOM.ShowReport = function()
	// {
		// Ext.Ajax  
		// ({
			// url    : Ext.DOM.INDEX +'/RptDashboardQaWellCall/ShowReport/',
			// method : 'POST',
			// param  : Ext.Join([
						// Ext.Serialize("frmReport").getReportElement()
					  // ]).object()
		// }).load('panel-report');
	// }
	
	 Ext.DOM.ShowReport = function() {
	  var param = []; 	
	  var TITLE = "Report "+ Ext.Cmp('report_type').getText() +" :: from "+ Ext.Cmp('start_date').getValue()+" to "+
				   Ext.Cmp('end_date').getValue();
	  
	  param['mode'] = 'html';
	  Ext.Dialog('panelx', {
			ID		: 'panelx1',
			type	: 'ajax/html',
			title	: TITLE,
			url 	: Ext.DOM.INDEX +'/RptDashboardQaWellCall/ShowReport/',
			param 	: Ext.Join([ Ext.Serialize("frmReport").getReportElement(), param]).object(),
			style 	: {
				width  		: $(window).width(),
				height 		: $('#main_content').height(),
				scrolling 	: 1, 
				resize 		: 0,
				close		: 1,
				minimize	: 1,
				top			: 50,
				left		: 2,
				right		: 2
			}	
		}).open();
	}
	
	Ext.DOM.ShowExcel = function(){
		var param = [];
		param['mode']= 'excel'; 	
		Ext.Window ({
			url 	: Ext.DOM.INDEX +'/RptDashboardQaWellCall/ShowReport/',
			method 	: 'GET',
			width 	: 0,
			height 	: 0,
			scrollbars : 1,
			param 	: Ext.Join([ 
				Ext.Serialize("frmReport").getReportElement(),param
			]).object(),
			
		}).popup();
	}
</script>