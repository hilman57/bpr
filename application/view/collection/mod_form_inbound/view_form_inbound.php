<?php ?>
<script>
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
Ext.document(document).ready(function(){
	Ext.Cmp("slectPhone").listener
	({
		onChange : function(e) {
			Ext.Util(e).proc( function(items)
			{
				if( items.value=='CustomerHomePhoneNum'){
					Ext.Cmp('CustomerHomePhoneNum').setValue(Ext.Cmp('PhoneNumber').getValue());
					Ext.Cmp('CustomerWorkPhoneNum').setValue('');
					Ext.Cmp('CustomerMobilePhoneNum').setValue('');
				}
				
				if( items.value=='CustomerWorkPhoneNum'){
					Ext.Cmp('CustomerWorkPhoneNum').setValue(Ext.Cmp('PhoneNumber').getValue());
					Ext.Cmp('CustomerHomePhoneNum').setValue('');
					Ext.Cmp('CustomerMobilePhoneNum').setValue('');
				}
				
				if( items.value=='CustomerMobilePhoneNum'){
					Ext.Cmp('CustomerMobilePhoneNum').setValue(Ext.Cmp('PhoneNumber').getValue());
					Ext.Cmp('CustomerHomePhoneNum').setValue('');
					Ext.Cmp('CustomerWorkPhoneNum').setValue('');
				}
			});
		}
	})
	
	Ext.ActiveMenu().NotActive();
	
}); 

Ext.Cmp("ButtonEntryData").listener
({
  onClick : function(){
  
   if( !Ext.Cmp('CallSessionId').empty() )
   {
		 Ext.Ajax 
		 ({
			url 	: Ext.DOM.INDEX +'/ModFormInbound/SaveInbound/',
			method  : 'POST',
			param   : Ext.Join([Ext.Serialize("frmCallInbound").getElement()]).object(),
			ERROR 	: function(e) {
				Ext.Util(e).proc(function( response ){
					if( response.success ) 
					{
						Ext.Msg("Data Entry Incoming").Success();
						Ext.Serialize("frmCallInbound").Clear();
						Ext.ActiveMenu().Active();
					}
					else{
						Ext.Msg("Data Entry Incoming").Success();
					}
				});
			}
		}).post();
	 }
	 else { Ext.Msg('No Call Session ').Info(); }
   }	
});


</script>

<fieldset class="corner" style='width:700px; margin: 0 auto; text-align: left;' >
<legend class="icon-customers">&nbsp;&nbsp;Form Inbound</legend>
<div id="result_content_add" class='box-shadow' style="text-align:center;">

<form name="frmCallInbound">
 <?php __(form()->hidden('CampaignId', null, $Caller['Campaign']['CampaignId']));?>
 <?php __(form()->hidden('CallSessionId', null,  $Caller['CallSessionId']));?>
 <table cellpadding="8px;" border=0 align='center'>
		<tr> 
			<td class="text_caption bottom"> Campaign Name </td>
			<td> <?php echo form() -> input('CampaignName','input_text long',$Caller['Campaign']['CampaignName'], null, array('disabled'=>true));?></td>
			<td class="text_caption bottom" valign="top"> Caller Number </td>
			<td><?php echo form() -> input('PhoneNumber','input_text long',_getPhoneNumber($Caller['CallerId']), null,array('disabled'=>true));?></td>
		</tr>
		
		<tr> 
			<td class="text_caption bottom"> Customer Name</td>
			<td><?php echo form() -> input('CustomerFirstName','input_text long');?></td>
			<td class="text_caption" valign="top"> Yes Phone Number </td>
			<td><?php echo form() -> combo('slectPhone','select long', array( 'CustomerHomePhoneNum' => 'Home Phone','CustomerWorkPhoneNum' => 'Office Phone', 'CustomerMobilePhoneNum' => 'Mobile Phone')); ?>
			</td>
		</tr>
		<tr> 
			<td class="text_caption bottom"> Customer Gender</td>
			<td class="bottom"><?php echo form() -> combo('GenderId','select long',$GenderId);?></td>
			<td class="text_caption bottom"> Office Phone</td>
			<td><?php echo form() -> input('CustomerWorkPhoneNum','input_text long');?></td>
		</tr>
		<tr> 
			<td class="text_caption bottom"> &nbsp;&nbsp; </td>
			<td class="text_caption bottom"> &nbsp;&nbsp; </td>
			<td class="text_caption bottom">Mobile Phone</td>
			<td> <?php echo form() -> input('CustomerMobilePhoneNum','input_text long');?></td>
		</tr>
		
		<tr> 
			<td class="text_caption bottom"> &nbsp;&nbsp; </td>
			<td class="text_caption bottom"> &nbsp;&nbsp; </td>
			
			<td class="text_caption bottom"> Home Phone</td>
			<td> <?php echo form() -> input('CustomerHomePhoneNum','input_text long');?></td>
		</tr>
		<tr> 
			<td class="text_caption"></td>
			<td colspan=3 align='right'>
				<input type="button" class='save button' id="ButtonEntryData" name="ButtonEntryData" value='Save Data Entry'>
			</td>
		</tr>
	</table>	
</form>	
</div>
</fieldset>