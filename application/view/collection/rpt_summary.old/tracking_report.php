<?php

$sdates = explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){

	echo "End date before start date";
	exit;
}


function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}

	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];

	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);

	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}

	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;

	return $yyyy."-".$mm."-".$dd;
}

function printNote(){
	echo "Note:<br>".
			 "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">".
	     "</table>";
}

function showReport($start_date, $end_date, $group, $agents){

		echo '<table class="data" border=1 style="border-collapse: collapse">';
		echo '<tr>'
		     .'<td class="head" rowspan="2">No.</td>'
		     .'<td class="head" rowspan="2">DCR Name</td>'
		     .'<td class="head" rowspan="2">Datasize</td>'
		     .'<td class="head" rowspan="2">Volume</td>'
		     .'<td class="head" rowspan="2">Data Utilized</td>'
		     .'<td class="head" rowspan="2">Volume Utilized</td>'
		     .'<td class="head" rowspan="2">Call Initiated</td>'
		     .'<td class="head" rowspan="2">% Utilized</td>'
		     .'<td class="head" colspan="6">POP/SP</td>'
		     .'<td class="head" colspan="5">Contacted</td>'
		     .'<td class="head" colspan="2">PTP</td>'
		     .'<td class="head" colspan="4">VALID</td>'
		     .'<td class="head" colspan="5">SKIP</td>'
		     .'<td class="head" rowspan="2">Total Reach</td>'
		     .'<td class="head" rowspan="2">% Reach</td>'
		     .'</tr>';
		echo '<tr>'
		     .'<td class="head">POP</td>'
			// .'<td class="head">POP 1</td>'
			 .'<td class="head">POP 2</td>'
			 .'<td class="head">POP 3</td>'
			 .'<td class="head">POP 4</td>'
		     .'<td class="head">RE-SP</td>'
		     .'<td class="head">SP</td>'
		     .'<td class="head">RP</td>'
		     .'<td class="head">OP</td>'
		     .'<td class="head">BP</td>'
		     .'<td class="head">Total RP+OP+BP</td>'
		     .'<td class="head">% Contacted</td>'
		     .'<td class="head">Total PTP</td>'
		     .'<td class="head">% PTP</td>'
		     .'<td class="head">NA</td>'
		     .'<td class="head">NBP</td>'
		     .'<td class="head">Total Valid</td>'
		     .'<td class="head">% Valid</td>'
		     .'<td class="head">MV</td>'
		     .'<td class="head">WN</td>'
		     .'<td class="head">NK</td>'
		     .'<td class="head">Total Skip</td>'
		     .'<td class="head">% Skip</td>'
		     .'</tr>';

		
		if($group || $agents){
			$fromTbl = ", cc_agent d ";
			//periksa apakah ada agent yang dipilih
			if(count($agents) and $agents[0]){
				$agentlist = implode(",", $agents);
				$cond = ' AND b.deb_agent = d.userid AND d.id IN ('.$agentlist.') 
						  AND d.occupancy>0 AND d.spv_id IN(0, 1) ';
			}else{
				$cond = " AND b.deb_agent = d.userid 
						  AND d.occupancy>0 
						  AND d.spv_id = 1 
						  AND d.agent_group ='".$group."' ";
			}
			
			if(count($agents) and $agents[0]){
				$agentlist = implode(",", $agents);
				$condInit = ' AND d.id IN ('.$agentlist.') AND d.occupancy>0 AND d.spv_id IN(1,2)';
			}else{
				$condInit = " AND d.occupancy>0 
							  AND d.spv_id IN(1,2) 
							  AND d.agent_group ='".$group."' ";
			}
		}
		
		
		
		$agent_data = array();
		// Get data size and volume
		
		$sql = " SELECT b.deb_agent as agent , COUNT(*) AS datasize, SUM(b.deb_wo_amount) AS datavol, d.agent_group, d.occupancy ".
		       " FROM t_gn_debitur b, cc_agent d ".
		       " WHERE b.deb_agent = d.userid ".
		       " $cond ".
		       " GROUP BY b.deb_agent 
			     ORDER BY d.agent_group, b.deb_agent ";
				 
		//echo $sql;		 
		$res = @mysql_query($sql);
		 while ($row = @mysql_fetch_array($res)) 
		{
			$data = array();
			$agent = $row['agent'];
			$data['group'] = $row['agent_group'];
			$data['occupancy']= $row['occupancy'];
			$data['datasize'] = $row['datasize'];
			$data['datavol'] 	= $row['datavol'];
			$agent_data[$agent] = $data;
		}

		// Get data utilization		       
		$sql = "SELECT  b.deb_agent as agent, COUNT(DISTINCT a.CustomerId) AS u_data, SUM(DISTINCT b.deb_amount_wo) AS u_vol
				FROM t_gn_debitur b 
				LEFT OUTER JOIN t_gn_callhistory a ON a.CustomerId=b.deb_id $fromTbl 
				WHERE 
					a.CallHistoryCallDate >= '$start_date 00:00:00'
					AND a.CallHistoryCallDate <= '$end_date 23:59:59'
					$cond
					GROUP BY b.deb_agent";		       

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['u_data'] = $row['u_data'];
			$agent_data[$agent]['u_vol'] 	= $row['u_vol'];
		}

		// Get Call Initiated
		$sql = "SELECT a.agent_id, d.userid, COUNT(*) AS agent_dial ".
		       "FROM cc_call_session a , cc_agent d    ".
		       "WHERE ".
		       "a.agent_id = d.id AND a.start_time >= '$start_date 00:00:00' ".
		       "AND a.start_time <= '$end_date 23:59:59' and d.spv_id IN(1,2) $condInit ".
		       "GROUP BY a.agent_id";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= trim($row['userid']);
			$agent_data[$agent]['agent_dial'] = $row['agent_dial'];
			//echo $row['userid']." -> ".$row['agent_dial']." -> ".$agent_data[$agent]['agent_dial']."<br>";
		}

		// Get POP yang di pake cuma POP 1 aja , yang Lain Gak DI pake .
		
		$sql = " SELECT b.deb_agent as agent, COUNT(DISTINCT a.CustomerId ) AS pop_data  ".
		       " FROM t_gn_debitur b ".
			   " LEFT OUTER JOIN t_gn_callhistory ON a.CustomerId = b.deb_id $fromTbl ".
		       " WHERE ".
		       " a.CallHistoryCallDate >= '$start_date 00:00:00' ".
		       " AND a.CallHistoryCallDate <= '$end_date 23:59:59' ".
		       " AND a.CallAccountStatus = 7 $cond ".
		       " GROUP BY b.deb_agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent = $row['agent'];
			$agent_data[$agent]['pop_data'] = $row['pop_data'];
		}
		
	
		//getting pop2
	$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS pop2_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND b.accstatus = 15 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['pop2_data'] = $row['pop2_data'];
		}
		//getting pop3
	$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS pop3_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND b.accstatus = 16 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['pop3_data'] = $row['pop3_data'];
		}
		
		//getting pop4
	$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS pop4_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND b.accstatus = 17 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);

		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['pop4_data'] = $row['pop4_data'];
		}

		// Get RE-SP
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS resp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 13 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['resp_data'] = $row['resp_data'];
		}

		// Get SP
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS sp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 9 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['sp_data'] = $row['sp_data'];
		}

		//OP 	-> 1
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS op_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 1 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['op_data'] = $row['op_data'];
		}

		//RP	-> 8
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS rp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 8 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['rp_data'] = $row['rp_data'];
		}

		//BP	-> 2
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS bp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 2 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['bp_data'] = $row['bp_data'];
		}

		//PTP	-> 4
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS ptp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 4 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['ptp_data'] = $row['ptp_data'];
		}

		//NA 	-> 3
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS na_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 3 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['na_data'] = $row['na_data'];
		}

		//NBP	-> 10
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS nbp_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 10 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['nbp_data'] = $row['nbp_data'];
		}

		//MV 	-> 11
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS mv_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl ".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 11 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['mv_data'] = $row['mv_data'];
		}

		//WN	-> 5
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS wn_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 5 $cond".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['wn_data'] = $row['wn_data'];
		}

		//NK	-> 6
		$sql = "SELECT b.agent, COUNT(DISTINCT a.custno) AS nk_data ".
		       "FROM coll_debitur b LEFT OUTER JOIN coll_contact_history a ON a.custno=b.custno $fromTbl".
		       "WHERE ".
		       "a.calltime >= '$start_date 00:00:00' ".
		       "AND a.calltime <= '$end_date 23:59:59' ".
		       "AND a.contactstatus = 6 $cond ".
		       "GROUP BY b.agent";

		$res = @mysql_query($sql);
		//echo $sql."<br>";
		while ($row = @mysql_fetch_array($res)) {
			$agent 						= $row['agent'];
			$agent_data[$agent]['nk_data'] = $row['nk_data'];
		}


		$cnt = 0;
		$totData = 0;
		$totVol = 0;
		$totUData = 0;
		$totUVol = 0;
		$totCall = 0;
		$totPOP = 0;
		$totRESP = 0;
		$totSP = 0;
		$totRP = 0;
		$totOP = 0;
		$totBP = 0;
		$group = -1;
		$tl = "";
		$subTotData = 0;
		$subTotVol  = 0;
		$subTotUData = 0;
		$subTotUVol = 0;
		$subTotCall = 0;
		$subTotPop	= 0;
		$subTotPop2	= 0;
		$subTotPop3	= 0;
		$subTotPop4	= 0;
		$subTotResp	= 0;
		$subTotSp	= 0;
		$subTotRp	= 0;
		$subTotOp	= 0;
		$subTotBp	= 0;
		$subTotRpBpOp 	= 0;
		$subTotPtpdata 	= 0;
		$subTotNadata	= 0;
		$subTotNbpdata	= 0;
		$subTotValid	= 0;
		$subTotMvdata	= 0;
		$subTotWndata	= 0;
		$subTotNkdata	= 0;
		$subTotSkip		= 0;
		$subTotReach	= 0;	
		foreach($agent_data as $agent => $data){

			if($group != $data['group']){
				if($cnt>0){
					$persenUtil = $subTotData?(($subTotUData *100)/ $subTotData):0;
					echo "<tr>";
					echo '<td class="head" nowrap>&nbsp</td>'
							 .'<td class="head" ><b>'.$tl.'<b></td>'
							 .'<td class="head" align="right">'.number_format($subTotData).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotVol).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotUData).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotUVol).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotCall).'</td>';
					printf ('<td class="head" align="right">%.2f</td>', $persenUtil);
					echo '<td class="head" align="right">'.number_format($subTotPop).'</td>' 
							 .'<td class="head" align="right">'.number_format($subTotPop2).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotPop3).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotPop4).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotResp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotSp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotRp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotOp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotBp).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotRpBpOp).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotPtpdata).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotNadata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotNbpdata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotValid).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotMvdata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotWndata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotNkdata).'</td>'
							 .'<td class="head" align="right">'.number_format($subTotSkip).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>'
							 .'<td class="head" align="right">'.number_format($subTotReach).'</td>'
							 .'<td class="head" align="right">&nbsp;</td>';
					echo '</tr>';

					$tl = "";
					$subTotData = 0;
					$subTotVol  = 0;
					$subTotUData = 0;
					$subTotUVol = 0;
					$subTotCall = 0;
					$subTotPop	= 0;
					$subTotPop2	= 0;
					$subTotPop3	= 0;
					$subTotPop4	= 0;
					$subTotResp	= 0;
					$subTotSp	= 0;
					$subTotRp	= 0;
					$subTotOp	= 0;
					$subTotBp	= 0;
					$subTotRpBpOp 	= 0;
					$subTotPtpdata 	= 0;
					$subTotNadata	= 0;
					$subTotNbpdata	= 0;
					$subTotValid	= 0;
					$subTotMvdata	= 0;
					$subTotWndata	= 0;
					$subTotNkdata	= 0;
					$subTotSkip		= 0;
					$subTotReach	= 0;
				}
				$group = $data['group'];

			}

			$cnt++;
			if($data['occupancy']==4)
				$tl = $agent;
			$persenUtil = $data['datasize']?(($data['u_data'] *100)/ $data['datasize']):0;

			$Contacted 	= $data['rp_data']+$data['op_data']+$data['bp_data'];
			$Valid			= $data['na_data']+$data['nbp_data'];
			$Skip				= $data['mv_data']+$data['wn_data']+$data['nk_data'];
			$Reach			= $Contacted + $data['ptp_data'] + $data['na_data'];
			$perContacted	=	$data['u_data']?(($Contacted *100)/ $data['u_data']):0;
			$perPTP			=	$data['u_data']?((($data['ptp_data']+$data['pop_data']) *100)/ $data['u_data']):0;
			$perValid		=	$data['u_data']?(($Valid *100)/ $data['u_data']):0;
			$perSkip		=	$data['u_data']?(($Skip *100)/ $data['u_data']):0;
			$perReach		=	$data['u_data']?(($Reach *100)/ $data['u_data']):0;
			echo "<tr>";
			echo '<td nowrap>'.$cnt.'</td>'
					 .'<td>'.$agent.'</td>'
					 .'<td align="right">'.$data['datasize'].'</td>'
					 .'<td align="right">'.number_format($data['datavol']).'</td>'
					 .'<td align="right">'.$data['u_data'].'</td>'
					 .'<td align="right">'.number_format($data['u_vol']).'</td>'
					 .'<td align="right">'.number_format($data['agent_dial']).'</td>';
			printf ('<td align="right">%.2f</td>', $persenUtil);
			echo '<td align="right">'.number_format($data['pop_data']).'</td>'
					 //.'<td align="right">'.number_format($data['pop1_data']).'</td>'
					 .'<td align="right">'.number_format($data['pop2_data']).'</td>'
					 .'<td align="right">'.number_format($data['pop3_data']).'</td>'
					 .'<td align="right">'.number_format($data['pop4_data']).'</td>'
					 .'<td align="right">'.number_format($data['resp_data']).'</td>'
					 .'<td align="right">'.number_format($data['sp_data']).'</td>'
					 .'<td align="right">'.number_format($data['rp_data']).'</td>'
					 .'<td align="right">'.number_format($data['op_data']).'</td>'
					 .'<td align="right">'.number_format($data['bp_data']).'</td>'
					 .'<td align="right">'.number_format($Contacted).'</td>';
			printf ('<td align="right">%.2f</td>', $perContacted);
			echo '<td align="right">'.number_format($data['ptp_data']).'</td>';
			printf ('<td align="right">%.2f</td>', $perPTP);
			echo '<td align="right">'.number_format($data['na_data']).'</td>'
					 .'<td align="right">'.number_format($data['nbp_data']).'</td>'
					 .'<td align="right">'.number_format($Valid).'</td>';
			printf ('<td align="right">%.2f</td>', $perValid);
			echo '<td align="right">'.number_format($data['mv_data']).'</td>'
					 .'<td align="right">'.number_format($data['wn_data']).'</td>'
					 .'<td align="right">'.number_format($data['nk_data']).'</td>'
					 .'<td align="right">'.number_format($Skip).'</td>';
			printf ('<td align="right">%.2f</td>', $perSkip);
			echo '<td align="right">'.number_format($Reach).'</td>';
			printf ('<td align="right">%.2f</td>', $perReach);
			echo '</tr>';

			$totData 		+= $data['datasize'];
			$subTotData 	+= $data['datasize'];
			$totVol  		+= $data['datavol'];
			$subTotVol  	+= $data['datavol'];
			$totUData 		+= $data['u_data'];
			$subTotUData 	+= $data['u_data'];
			$totUVol 		+= $data['u_vol'];
			$subTotUVol 	+= $data['u_vol'];
			$totCall 		+= $data['agent_dial'];
			$subTotCall 	+= $data['agent_dial'];
			$totPop 		+= $data['pop_data'];
			$subTotPop		+= $data['pop_data'];
			$totPop2 		+= $data['pop2_data'];
			$subTotPop2		+= $data['pop2_data'];
			$totPop3 		+= $data['pop3_data'];
			$subTotPop3		+= $data['pop3_data'];
			$totPop4		+= $data['pop4_data'];
			$subTotPop4		+= $data['pop4_data'];
			$totResp   		+= $data['resp_data'];
			$subTotResp		+= $data['resp_data'];
			$totSp   		+= $data['sp_data'];
			$subTotSp		+= $data['sp_data'];
			$totRp   		+= $data['rp_data'];
			$subTotRp		+= $data['rp_data'];
			$totOp   		+= $data['op_data'];
			$subTotOp		+= $data['op_data'];
			$totBp   		+= $data['bp_data'];
			$subTotBp		+= $data['bp_data'];
			$totRpBpOp		+= $Contacted;
			$subTotRpBpOp	+= $Contacted;
			$totPtpdata		+= $data['ptp_data'];
			$subTotPtpdata	+= $data['ptp_data'];
			$totNadata		+= $data['na_data'];
			$subTotNadata	+= $data['na_data'];
			$totNbpdata		+= $data['nbp_data'];
			$subTotNbpdata	+= $data['nbp_data'];
			$totValid		+= $Valid;
			$subTotValid	+= $Valid;
			$totMvdata		+= $data['mv_data'];
			$subTotMvdata	+= $data['mv_data'];
			$totWndata		+= $data['wn_data'];
			$subTotWndata	+= $data['wn_data'];
			$totNkdata		+= $data['nk_data'];
			$subTotNkdata	+= $data['nk_data'];
			$totSkip		+= $Skip;
			$subTotSkip		+= $Skip;
			$totReach		+= $Reach;
			$subTotReach	+= $Reach;

		}

		$persenUtil = $totData?(($totUData *100)/ $totData):0;
		echo "<td class=\"head\"></td>"
		     ."<td class=\"head\">TOTAL </td>"
		     .'<td class="head" align="right">'.$totData.'</td>'
		     .'<td class="head" align="right">'.number_format($totVol).'</td>'
		     .'<td class="head" align="right">'.$totUData.'</td>'
		     .'<td class="head" align="right">'.number_format($totUVol).'</td>'
		     .'<td class="head" align="right">'.number_format($totCall).'</td>';
		printf ('<td class="head" align="right">%.2f</td>', $persenUtil);
		//echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totPop).'</td>';
		echo '<td class="head">'.number_format($totPop2).'</td>';
		echo '<td class="head">'.number_format($totPop3).'</td>';
		echo '<td class="head">'.number_format($totPop4).'</td>';
		echo '<td class="head">'.number_format($totResp).'</td>';
		echo '<td class="head">'.number_format($totSp).'</td>';
		echo '<td class="head">'.number_format($totRp).'</td>';
		echo '<td class="head">'.number_format($totOp).'</td>';
		echo '<td class="head">'.number_format($totBp).'</td>';
		echo '<td class="head">'.number_format($totRpBpOp).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totPtpdata).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totNadata).'</td>';
		echo '<td class="head">'.number_format($totNbpdata).'</td>';
		echo '<td class="head">'.number_format($totValid).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totMvdata).'</td>';
		echo '<td class="head">'.number_format($totWndata).'</td>';
		echo '<td class="head">'.number_format($totNkdata).'</td>';
		echo '<td class="head">'.number_format($totSkip).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '<td class="head">'.number_format($totReach).'</td>';
		echo '<td class="head">&nbsp;</td>';
		echo '</table><br>';


}
?>
<html>
	<head>
		<title>
			enigmaColection Report - Tracking
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}
			-->
			</style>
	</head>
<body>
	<h1><u><?
	switch($rptIntervalMode){
		case "hourly":
			echo "Hourly ";
			break;
		case "daily":
			echo "Daily ";
			break;
	}
		?>Tracking Report</u></h1>
	<?php
	echo "<h2>Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}</h2>";
	echo "<hr size=1>";
	showReport($start_date, $end_date, $rptGroup, $AgentId);

	printNote();
	?>
</body>
</html>