<?php __(javascript()); ?>
<script type="text/javascript">
/*  
 * @ pack : handle menu user for application system 
 */
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 

 /*
 * @ pack : store approve by form discount or lunas 
 * -------------------------------------------------
 */
Ext.DOM.ApproveList = function( ){
  return( 
	Ext.Ajax({
		url    : Ext.DOM.INDEX +'/ModApprovalDiscount/getApprovalBy/',
		method : 'GET',
		param  : {}
	}).json()
  );
}

// alert(tes);

/*  
 * @ pack : handle menu user for application system 
 */
 
Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page->_get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page->_get_total_record(); ?>';

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.datas =  
{
	reqs_account_status : "<?php echo _get_exist_session('reqs_account_status');?>",
	reqs_agent_id 		: "<?php echo _get_exist_session('reqs_agent_id');?>",
	reqs_call_status 	: "<?php echo _get_exist_session('reqs_call_status');?>",
	reqs_campaign_id 	: "<?php echo _get_exist_session('reqs_campaign_id');?>",
	reqs_cust_id 		: "<?php echo _get_exist_session('reqs_cust_id');?>",
	reqs_cust_name 		: "<?php echo _get_exist_session('reqs_cust_name');?>",
	reqs_approve_status : "<?php echo _get_exist_session('reqs_approve_status');?>",
	reqs_start_amountwo : "<?php echo _get_exist_session('reqs_start_amountwo');?>",
 	reqs_end_amountwo 	: "<?php echo _get_exist_session('reqs_end_amountwo');?>",
	reqs_start_date 	: "<?php echo _get_exist_session('reqs_start_date');?>",
	reqs_end_date 		: "<?php echo _get_exist_session('reqs_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.navigation = 
{
	custnav  : Ext.DOM.INDEX+'/ModApprovalDiscount/index/',
	custlist : Ext.DOM.INDEX+'/ModApprovalDiscount/Content/',
}

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.EQuery.construct(navigation,Ext.DOM.datas)
 Ext.EQuery.postContentList();

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.searchCustomer = function() 
{
  var param = Ext.Serialize('frmDiscount').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.resetSeacrh = function()
{
	Ext.Serialize('frmDiscount').Clear();
	Ext.DOM.searchCustomer();
}

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.ApprovePage = function() 
{
	collband = Ext.Cmp('approve_by_form').getValue(); 
	if(collband!=''){
		  Ext.Ajax
		 ({
			url 	: Ext.DOM.INDEX +'/ModApprovalDiscount/Approve/',
			method 	: 'POST',
			param 	: { 
				DiscountId : Ext.Cmp('RequestId').getChecked(),
				Status : '102',
				CollBand : collband
			},
			ERROR	: function(e) {
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg('Approve Discount').Success();
						Ext.DOM.searchCustomer();
					} else {
						Ext.Msg('Approve Discount').Failed();
					}
				})
			}	



		}).post();
	}
	else
	{
		alert ('Please Select Approve By (Coll Band)');
	}
	
}

/*  
 * @ pack : handle menu user for application system 
 */
 
/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.RejectPage = function() 
{
	var jawab = confirm("Are you sure want reject this account ? ");
	if (jawab == true){	
  Ext.Ajax
 ({
	url 	: Ext.DOM.INDEX +'/ModApprovalDiscount/Reject/',
	method 	: 'POST',
	param 	: { 
		DiscountId : Ext.Cmp('RequestId').getChecked(),
		Status : '103'
	},
	ERROR	: function(e) {
		Ext.Util(e).proc(function(response){
			if( response.success ){
				Ext.Msg('Reject Discount').Success();
				Ext.DOM.searchCustomer();
			} else {
				Ext.Msg('Reject Discount').Failed();
			}
		})
	}	
}).post();
	}
}
 
/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.PreviewPage = function() 
{
  Ext.Window
 ({
	url 	: Ext.DOM.INDEX+'/FormDiscount/PrintByDiscountId/',
	method 	: 'POST',
	param 	: {  
		DiscountId : Ext.Cmp('RequestId').getChecked(),
		action : 'preview'
	},
	scrollbars : 1,
	resizable  : 1, 
	width : ( $(window).width() - ( $(window).width()/3)),
	height : ( $(window).height() - ( $(window).height()/10))
}).popup();
	
}

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.EditPage = function() 
{
  Ext.Window
 ({
	url 	: Ext.DOM.INDEX+'/FormDiscount/PrintByDiscountId/',
	method 	: 'POST',
	param 	: {  
		DiscountId : Ext.Cmp('RequestId').getChecked(), 
		action : 'edit' 
	},
	scrollbars : 1,
	resizable  : 1, 
	width : ( $(window).width() - ( $(window).width()/3)),
	height : ( $(window).height() - ( $(window).height()/10))
}).popup();
	
}




/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.PrintPage = function() 
{
	
  Ext.Ajax
 ({
	url 	: Ext.DOM.INDEX+'/ModApprovalDiscount/Printed/',
	method 	: 'POST',
	param 	: { 
	    DiscountId : Ext.Cmp('RequestId').getChecked()
	},
	ERROR	: function(e) {
		Ext.Util(e).proc(function(response){
			if( (response.success)
				&& (typeof(response.url)=='string')
				&& (response.url !='') )
			{
				
				new Ext.Window
				({
					url 	: response.url,
					method  : 'POST',
					width   : ( $(window).width() - ( $(window).width()/3)),
					height  : ( $(window).height() - ( $(window).height()/10)),
					param 	: {
						time : Ext.Date().getDuration()
					},
					scrollbars 	: 1,
					resizable  	: 1
				}).popup();
				
				Ext.DOM.searchCustomer();
				
			} else {
				Ext.Msg('Print Page. please try again!').Failed();
			}
			
		});
	}	
}).post();
	
}

/*  
 * @ pack : handle menu user for application system 
 */
 
if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_ADMIN ||
	Ext.DOM.handling == Ext.DOM.USER_LEVEL_MANAGER ||
	Ext.DOM.handling == Ext.DOM.USER_LEVEL_SPV ||
	Ext.DOM.handling == Ext.DOM.USER_SYSTEM_LEVEL
){
	var STOREAPROVELIST = Ext.DOM.ApproveList();
	var EXTTITLE  = [['Search'],['Clear'],['Edit'],['Approve'],['Reject'],['Print']];
	var EXTMENU   = [['searchCustomer'],['resetSeacrh'],['EditPage'],['ApprovePage'],['RejectPage'],['PrintPage']];
	var EXTICON   = [['zoom.png'],['zoom_out.png'],['page_edit.png'],['tick.png'],['cross.png'], ['page_white_acrobat.png']];
	var EXTINPUT = true;
	var EXTOPTION = [{
			render	: 4,
			header	: 'Approve By ',
			type	: 'combo',
			id		: 'approve_by_form', 	
			name	: 'approve_by_form',
			store	: [STOREAPROVELIST],
			triger	: '',
			width	: 100
		}];
}

/*  
 * @ pack : handle menu user for application system 
 */
 
if( Ext.DOM.handling ==Ext.DOM.USER_LEVEL_LEADER||
    Ext.DOM.handling == 14 ||
	Ext.DOM.handling ==Ext.DOM.USER_LEVEL_QUALITY 
){
	var EXTTITLE  = [['Search'],['Clear'],['Edit'],['Reject'],['Print']];
	var EXTMENU   = [['searchCustomer'],['resetSeacrh'],['EditPage'],['RejectPage'],['PrintPage']];
	var EXTICON   = [['zoom.png'],['zoom_out.png'],['page_edit.png'],['cross.png'], ['page_white_acrobat.png']];
	var EXTINPUT = false;
	var EXTOPTION = [];
}

/*  
 * @ pack : handle menu user for application middle
 */

if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_AGENT ||
	Ext.DOM.handling == Ext.DOM.USER_LEVEL_INBOUND
){
	var EXTTITLE  = [['Search'],['Clear'],['Print']];
	var EXTMENU   = [['searchCustomer'],['resetSeacrh'],['PrintPage']];
	var EXTICON   = [['zoom.png'],['zoom_out.png'],['page_white_acrobat.png']];
	var EXTINPUT = false;
	var EXTOPTION = [];
}

/*  
 * @ pack : handle menu user for application middle
 */
 
 $(document).ready( function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : EXTTITLE,	
		extMenu  : EXTMENU,
		extIcon  : EXTICON,
		extText  : true,
		extInput : EXTINPUT,
		extOption: EXTOPTION
	});
	
	
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
	//console.log( $('#toolbars'));	
/*  
 * @ pack : handle menu user for application middle
 */
 
 Ext.DOM.enterSearch = function( e ) 
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}

// @ pack : On ENTER Keyboard 

 Ext.DOM.CallCustomer = function( CustomerId ) 
{
 if( CustomerId!='')  {	
	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.navigation.custnav
		}
	});
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}
	
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="frmDiscount">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('reqs_campaign_id','select auto', $CampaignId, _get_exist_session('reqs_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('reqs_agent_id','select auto', $Deskoll, _get_exist_session('reqs_agent_id'));?></td>
			<td class="text_caption bottom"> # Tanggal Panggilan Terakhir &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('reqs_start_date','input_text date', _get_exist_session('reqs_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('reqs_end_date','input_text date', _get_exist_session('reqs_end_date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # ID Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('reqs_cust_id','input_text long', _get_exist_session('reqs_cust_id'));?></td>
			<td class="text_caption bottom"> # Account Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('reqs_account_status','select auto', $AccountStatus, _get_exist_session('reqs_account_status'));?></td>
			<td class="text_caption bottom"> # Approve Status &nbsp;:</td>
			<td class="bottom"><?php echo form()-> combo('reqs_approve_status','select long', $ApproveStatus, _get_exist_session('reqs_approve_status'));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Nama Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('reqs_cust_name','input_text long', _get_exist_session('reqs_cust_name'));?></td>
			<td class="text_caption bottom"> # Status Panggilan Terakhir &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('reqs_call_status','select auto', $LastCallStatus, _get_exist_session('reqs_call_status'));?></td>
			<td class="text_caption bottom"> # Jumlah WO&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('reqs_start_amountwo','input_text box', _get_exist_session('reqs_start_amountwo') );?>
				&nbsp; <span>to</span>&nbsp;	
				<?php echo form()->input('reqs_end_amountwo','input_text box', _get_exist_session('reqs_end_amountwo') );?>
			</td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->