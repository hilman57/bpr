<?php
$header = array(
	'no'=>"No",
	'acc_no'=>"Account Number",
	'OldAgent' => "From Agent",
	'AgentClaim' => "Claim Agent",
	'claim_date_ts' => "Claim Date",
	'ApprovalStatus'=>"Approval Status"
);

?>
<!DOCTYPE HTML>
<html>
<head>
<title> <?php echo ucfirst(base_layout());?> :: <?php echo $title; ?> </title>
</head>
<body style="margin:0px;">
<?php 
	echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	echo '<tr>';
	foreach($header as $column => $name)
	{
		echo '<th style="font-family:Arial;font-size:12px;padding:8px;border:1px solid #FFCCCC;background-color:#eee;">' . $name .' </th>';
	}
	echo '</tr>';
	
	$no=1;
	foreach($data_tabel as $rows => $content)
	{
		echo '<tr>';
		foreach($header as $column => $name)
		{
			if($column=="no")
			{
				echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'.$no. '</td>';
				$no++;
			}
			else
			{
				echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'. $content[$column] . '</td>';
			}
			
		}
		echo '</tr>';
	}
	echo '</table>';
	// echo "<pre>";
	// print_r($data_tabel);
	// echo "</pre>";
?>
</body>
</html>	