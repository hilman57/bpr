<?php echo javascript(); ?>
<script type="text/javascript">

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
}); 
 	
/* assign navigation filter **/
	
var datas = {
	phone_customerid  	: "<?php echo _get_exist_session('phone_customerid');?>",
	phone_customername 	: "<?php echo _get_exist_session('phone_customername');?>",	 
	phone_deskoll		: "<?php echo _get_exist_session('phone_deskoll');?>",
	phone_phonenumber 	: "<?php echo _get_exist_session('phone_phonenumber');?>",
	phone_phonetype 	: "<?php echo _get_exist_session('phone_phonetype');?>",
	phone_product 		: "<?php echo _get_exist_session('phone_product');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type 	 			: "<?php echo _get_exist_session('type');?>"
}

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';
	
	
/* assign navigation filter **/

var navigation = {
	custnav  : Ext.DOM.INDEX +'/ModApprovePhone/index/',
	custlist : Ext.DOM.INDEX +'/ModApprovePhone/Content/',
}
		
/* assign show list content **/

Ext.EQuery.construct(navigation,datas);
Ext.EQuery.postContentList();

 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	

Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		Ext.DOM.viewAppList();
	}
}

Ext.DOM.viewAppList = function(){
	Ext.EQuery.construct(navigation, Ext.Join([
			Ext.Serialize('frmAppovePhone').getElement()
		]).object() 
	);
	Ext.EQuery.postContent();
}

Ext.DOM.ClearSearch = function() {
 Ext.Serialize('frmAppovePhone').Clear();
 Ext.DOM.viewAppList();
 
}

// @ cek data if set followup  = deb_is_call = 0 

Ext.DOM.IsCall = function( CustomerId ){
  return( 
	Ext.Ajax({
		url    : Ext.DOM.INDEX +'/ModContactDetail/IsCall/',
		method : 'GET',
		param  : {
			CustomerId : CustomerId
		}
	}).json()
  );
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!='') 
 {	
  
  var IsCall = Ext.DOM.IsCall(CustomerId);
  
  if( IsCall.success == 0 ){
	Ext.Msg("\n\rSorry The data being in the follow by other users.\n\rPlease select other customer!").Info();
	return false;
  }
	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/ModApprovePhone/index/', 
		}
  });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
}
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
Ext.DOM.Approve =function(){
 var AddPhoneId = Ext.Cmp('AddPhoneId').getValue();
  if( AddPhoneId.length > 0 )
  {
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/ModApprovePhone/ApproveItem/',
		method : 'POST',
		param :{
			ApproveItemId : AddPhoneId,
			ApproveCode   : '101'
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(approve){
				if( approve.success ){
					Ext.Msg("Approve rows data").Success();
					Ext.EQuery.postContent();
				}	
				else{
					Ext.Msg("Approve rows data").Failed();
				}
			});
		}
	}).post();
  }
  else{
	Ext.Msg("Please select a rows ").Error();
  }
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
Ext.DOM.Reject =function(){
 var AddPhoneId = Ext.Cmp('AddPhoneId').getValue();
  if( AddPhoneId.length > 0 )
  {
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/ModApprovePhone/ApproveItem/',
		method : 'POST',
		param :{
			ApproveItemId : AddPhoneId,
			ApproveCode   : '102'
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(approve){
				if( approve.success ){
					Ext.Msg("Reject rows data").Success();
					Ext.EQuery.postContent();
				}	
				else{
					Ext.Msg("Reject rows data").Failed();
				}
			});
		}
	}).post();
  }
  else{
	Ext.Msg("Please select a rows ").Error();
  }
} 


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 
Ext.DOM.DetailApprove = function()
{
	var ApproveItems = Ext.Cmp('chk_apprv').getValue();
	if( ApproveItems.length==1)
	{
		Ext.EQuery.Ajax
		({
			url 	: Ext.DOM.INDEX +'/ModApprovePhone/Detail/',
			method  : 'GET',
			param 	: {
				ApproveId : ApproveItems,
				ControllerId : Ext.DOM.INDEX +'/ModApprovePhone/index/' 
			}
		});
	}
}

/* load jquery **/

Ext.query(function(){
Ext.query('#toolbars').extToolbars
	({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Approve'],['Reject']],
		extMenu  : [['viewAppList'],['ClearSearch'],['Approve'],['Reject']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['tick.png'],['delete.png']],
		extText  : true,
		extInput : true,
		extOption: []
	});
});
		
</script>
<fieldset class="corner" onKeyDown="enterSearch(event)">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<form name="frmAppovePhone">
<div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
	<table>
		<tr>
			<td class="text_caption bottom"># Product&nbsp;:</td>
			<td><?php echo form()->combo('phone_product', 'select long', $DROP_CAMPAINGN, _get_exist_session('phone_product') );?></td>
			<td class="text_caption bottom"># Deskoll&nbsp;:</td>
			<td><?php echo form()->combo('phone_deskoll', 'select long', $DROP_DESKOLL, _get_exist_session('phone_deskoll'));?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"># Id Pelanggan&nbsp;:</td>
			<td><?php echo form()->input('phone_customerid', 'input_text long', _get_exist_session('phone_customerid'));?></td>
			<td class="text_caption bottom"># Tipe Telepon&nbsp;:</td>
			<td><?php echo form()->combo('phone_phonetype', 'select long',$DROP_PHONETYPE,  _get_exist_session('phone_phonetype'));?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"># Nama Pelanggan&nbsp;:</td>
			<td><?php echo form()->input('phone_customername', 'input_text long', _get_exist_session('phone_customername'));?></td>
			<td class="text_caption bottom"># Nomor Telepon&nbsp;:</td>
			<td><?php echo form()->input('phone_phonenumber', 'select long', _get_exist_session('phone_phonenumber'));?></td>
		</tr>
	</table>
</div>
</form>

	<div id="toolbars"></div>
	<div class="content_table"></div>
	<div id="pager"></div>
</fieldset>	
	
	
	