<form name="frmCreateTemplate">
<table width='100%' align='left'>
	<tr>
		<td valign='top' width='30%'> 
		 <fieldset class="corner">
		<legend class="icon-application"> &nbsp;&nbsp;&nbsp;Create Template</legend>
		<table border=0 cellspacing="1" width="100%" border=0 style="margin-top:-10px;">
			<tr>
				<td valign="top" class='text_caption' width="5%" nowrap>&nbsp;Database Table </td>
				<td valign="top" width="10%"><?php echo form()->combo('table_name','select long', $plist, 'xxx');?></td>
			</tr>
			<tr>
				<td valign="top" class='text_caption'>&nbsp;Mode Input </td>
				<td valign="top"><?php echo form() -> combo('mode_input','select long',$ModeType,NULL,array("change"=>"getTableColumns(this);"));?></td>
			</tr>
			<tr>
				<td valign="top" class='text_caption'>&nbsp;File Type </td>
				<td valign="top"><?php echo form() -> combo('file_type','select long',$FileType,NULL,array('change'=>'Ext.DOM.Delimiter(this);'));?></td>
			</tr>
			<tr>
				<td valign="top" class='text_caption'>&nbsp;Delimiter </td>
				<td valign="top"><?php echo form() -> input('delimiter_type','input_text long',NULL,null);?></td>
			</tr>
			
			<tr>
				<td valign="top" class='text_caption' nowrap>&nbsp;Template Name </td>
				<td valign="top"><?php echo form() -> input('templ_name',' input_text long',NULL);?></td>
			</tr>
			
			<tr>
				<td valign="top" class='text_caption' nowrap>&nbsp;Compare Blacklist</td>
				<td valign="top"><?php echo form() -> combo('black_list',' select long',$BlackList, 'Y');?></td>
			</tr>
			
			<tr>
				<td valign="top" class='text_caption' nowrap>&nbsp;X-Days</td>
				<td valign="top"><?php echo form() -> combo('expired_days',' select long',$LimitDays, 90);?></td>
			</tr>
			
			<tr>
				<td valign="top" class='text_caption' nowrap>&nbsp;Bucket Data</td>
				<td valign="top"><?php echo form() -> combo('bucket_data',' select long',$BlackList, 'N');?></td>
			</tr>
			
			<tr>
				<td valign="top" class='text_caption' nowrap>&nbsp;Modul Upload</td>
				<td valign="top"><?php echo form() -> combo('upload_modul',' select long',$UplodModul );?></td>
			</tr>
			
		<tr>
			<td class="text_caption" nowrap>&nbsp;</td>
			<td>
				<input type="button" class="save button" onclick="Ext.DOM.SaveTemplate();" value="Save">
				<input type="button" class="close button" onclick="Ext.Cmp('panel-call-center').setText('');" value="Close">
			</td>
		</tr>
	</table>
	 </fieldset>
 </td> 
 <td valign='top'><div id="list_columns"></div></td>
</tr>
 </table>
 </form>