<?php 
__(javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()) )));
?>

<script type="text/javascript">
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 Ext.DOM.onload= (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
 /**
 ** javscript prototype system
 ** version v.0.1
 **/	
 
 $(function(){
	$('#toolbars').extToolbars({
		extUrl   :Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle :[['Download Template'],['Enable'],['Disable']],
		extMenu  :[['DownloadTemplate'],['Enable'],['Disable']],
		extIcon  :[['application_add.png'],['accept.png'],['cancel.png']],
		extText  :true,
		extInput :true,
		extOption:[{
			render : 1,
		}]
	});
});

 
Ext.EQuery.TotalPage   = <?php echo $page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo $page -> _get_total_record(); ?>;
 
Ext.DOM.datas = {} 
	
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM._content_page = {
	custnav  : Ext.DOM.INDEX+'/SetUpload/index',
	custlist : Ext.DOM.INDEX+'/SetUpload/Content'			
 }	
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
	Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
	Ext.EQuery.postContentList();	
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
var Enable = function()
{

	var TemplateId = Ext.Cmp('TemplateId').getValue();
	if( (TemplateId!='') )
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SetUpload/Enable/',
			method  : 'POST',
			param	: { 
				TemplateId : TemplateId
			},
			ERROR   : function(e)
			{
				var ERR = JSON.parse(e.target.responseText);
				if( ERR.success ){
					Ext.Msg('Enable Template Upload').Success();
					Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
					Ext.EQuery.postContent();	
				}
				else{
					Ext.Msg('Enable Template Upload').Failed();
				}
			}
		}).post();
	 }
	else{
		Ext.Msg('Enable Template Upload').Error();
	} 
}
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
var Disable = function()
{
	var TemplateId = Ext.Cmp('TemplateId').getValue();
	if( (TemplateId!='') )
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +'/SetUpload/Disable/',
			method  : 'POST',
			param	: { 
				TemplateId : TemplateId
			},
			ERROR   : function(e)
			{
				var ERR = JSON.parse(e.target.responseText);
				if( ERR.success ){
					Ext.Msg('Disable Template Upload').Success();
					Ext.EQuery.construct(Ext.DOM._content_page,Ext.DOM.datas )
					Ext.EQuery.postContent();	
				}
				else{
					Ext.Msg('Disable Template Upload').Failed();
				}
			}
		}).post();
	}
	else{
		Ext.Msg('Disable Template Upload').Error();
	} 
}


/**
 ** javscript prototype system
 ** version v.0.1
 **/
Ext.DOM.DownloadTemplate = function()
	{
		var TemplateId = Ext.Cmp('TemplateId').getValue();
		if( (TemplateId!='') )
		{
			var WindowWin = new Ext.Window({
				url   : Ext.DOM.INDEX+'/SetUpload/DownloadTemplate/', 
				param : {
					TemplateId : TemplateId,
				}
			}).newtab();
		}
	}	


	
</script>