<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *  @ def   : mod_view_field/view_setfield_list.php 
 * -------------------------------------------------------------------------
 *  @ param : unit layout attribute list page 
 *  @ param : - 
 * -------------------------------------------------------------------------
 */
 
?>


<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('Field_Id').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No</th>	
        <th nowrap class="font-standars ui-corner-top ui-state-default middle"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.CampaignName');">&nbsp;Nama Campaign  <span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Field_Header');">&nbsp;Header</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Field_Columns');">&nbsp;Kolom</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Field_Size');">&nbsp;Ukuran Field </span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Field_Create_Ts');">&nbsp;Tanggal Dibuat</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Field_Create_UserId');">&nbsp;Buat Pengguna </span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Field_Active');">&nbsp;Status</span></th>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
	<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first center"><input type="checkbox" value="<?php __($rows['Field_Id']); ?>" name="Field_Id" id="Field_Id"></td>
		<td class="content-middle center"><?php echo $no ?></td>
		<td class="content-middle"><?php __($rows['CampaignName']); ?></td>
		<td class="content-middle"><?php __($rows['Field_Header']); ?></td>
		<td class="content-middle"><?php __($rows['Field_Columns']); ?></td>
		<td class="content-middle"><?php __($rows['Field_Size']); ?></td>
		<td class="content-middle"><?php __($rows['Field_Create_Ts']); ?></td>
		<td class="content-middle"><?php __($rows['Field_Create_UserId']); ?></td>
		<td class="content-lasted center"><?php __(($rows['Field_Active']?'Active' : 'Not Active')); ?></td>
	</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>
