<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *  @ def   : mod_view_field/view_setfield_nav.php 
 * -------------------------------------------------------------------------
 *  @ param : unit layout attribute navigation 
 *  @ param : - 
 * -------------------------------------------------------------------------
 */
 
/* load all js default **/

__(javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()))));

?>

<script type="text/javascript"> 

Ext.document('document').ready(function(){

/* load : title every controller */

 Ext.Cmp('legend_title').setText(Ext.System.view_file_name());

/*
 * @ def  : set attribute page 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 

 Ext.DOM.datas = { 
	keywords : '<?php echo _get_post('keywords'); ?>',
	order_by : '<?php echo _get_post('order_by'); ?>',
	type 	 : '<?php echo _get_post('type'); ?>',
 }

 Ext.EQuery.TotalPage   = <?php echo (INT)$page -> _get_total_page(); ?>;
 Ext.EQuery.TotalRecord = <?php echo (INT)$page -> _get_total_record(); ?>;
 
 Ext.DOM.Page = {
	custnav	 : Ext.DOM.INDEX +'/SetField/index/',
	custlist : Ext.DOM.INDEX +'/SetField/Content/',
}

/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 
 
 Ext.EQuery.construct(Ext.DOM.Page, Ext.DOM.datas);
 Ext.EQuery.postContentList();
 
/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 
	
/*
 * @ def  : loading extToolbars jQuery plugin  
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 
 	
 Ext.query('#toolbars').extToolbars 
 ({
	extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
	extTitle :[['Add'],['Edit'],['Delete'],['View'],['Search']],
	extMenu  :[['AddViewLayout'],['Edit'],['Delete'],['View'],['Search']],
	extIcon  :[['add.png'],['application_edit.png'],['delete.png'],['zoom.png'],['zoom.png']],
	extText  :true,
	extInput :true,
	extOption: [{
			render	: 4,
			type	: 'text',
			id		: 'keywords', 	
			name	: 'keywords',
			value	: Ext.DOM.datas.keywords,
			width	: 200
		}]
 });

 
Ext.DOM.Search = function(){
	Ext.EQuery.construct(Ext.DOM.Page, { keywords :  Ext.Cmp('keywords').getValue() });
	Ext.EQuery.postContent();
 
}
 
/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 
Ext.DOM.AddViewLayout = function(){
	Ext.Dialog('panel-layout-field',{
		ID		: 'panel-layout-field1',
		type	: 'ajax/html',
		title	: 'Add Layout Field',
		url 	: Ext.DOM.INDEX+'/SetField/Create/',
		param 	: { t : 1 },
		style 	: {
			width 		: (Ext.query(window).width()-(Ext.query(window).width()/1.5)),
			height 		: (Ext.query(window).height()-(Ext.query(window).height()/6)),
			top 		: 0, 
			left 		: 0,
			scrolling 	: 1, 
			resize 		: 1,
			minimize 	: true,
			close 		: true
		}	
	}).open();
}

/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 

Ext.DOM.ViewLayoutGenerate = function( n ){
	Ext.Ajax({
		url		: Ext.DOM.INDEX+'/SetField/ViewLayoutGenerate/',
		method 	: 'GET',
		param 	: {
			FldStart : Ext.Cmp('Field_start').getValue(),
			FldEnd   : Ext.Cmp('Field_end').getValue()
		}
	}).load("field_generator");
}



/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 

Ext.DOM.SaveGenerateField = function()
{	
	var param = [];
		param['CampaignCode']   = Ext.Cmp('CampaignCode').getValue();
		param['Field_Header']	= Ext.Cmp('Field_Header').getValue();
		param['Field_Columns']  = Ext.Cmp('Field_Columns').getValue();
		param['Field_Active']	= Ext.Cmp('Field_Active').getValue();
		param['Field_start']	= Ext.Cmp('Field_start').getValue();
		param['Field_end']		= Ext.Cmp('Field_end').getValue();
		
	var Layout = Ext.Serialize('frmGenerator').getElement();
		console.log(Layout);
		
		

		
	
// sent via ajax 

 Ext.Ajax
 ({
		url		: Ext.DOM.INDEX+'/SetField/SaveGenerate/',
		method 	: 'POST',
		param 	: Ext.Join([param,Layout]).object(),
		ERROR 	: function(e)
		{
			Ext.Util(e).proc(function( items ) {
				if( items.success )
				{
					Ext.Msg('Add Field Layout').Success(); // alert callback 
					Ext.Cmp('panel-layout-field').setText(''); // close dialog 
					Ext.EQuery.postContent(); // reload
				}
				else{
					Ext.Msg('Add Field Layout').Failed();
					
				}
			});
		}
  }).post();
}
/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 
 
Ext.DOM.Delete = function(){
	if(Ext.Msg("Do you want to delete this rows").Confirm() ){
		Ext.Ajax ({
			url		: Ext.DOM.INDEX+'/SetField/Delete/',
			method 	: 'POST',
			param 	: {
				Field_Id : Ext.Cmp('Field_Id').getValue()
			},
			ERROR 	: function(e){
				Ext.Util(e).proc(function( items ) {
				if( items.success ) {
					Ext.Msg('Delete Field Layout').Success(); // alert callback 
					Ext.EQuery.postContent(); // reload
				}
				else{
					Ext.Msg('Add Field Layout').Failed();
				}});
			}
		}).post();	
	}	
}
/*
 * @ def  : loading Ext libs js on Ready 
 * -----------------------------------------------------------------
 * 
 * @ param : - 
 * @ param : -
 * ----------------------------------------------------------------
 */ 

Ext.DOM.View = function(){
	Ext.Window({
		url 	: Ext.DOM.INDEX+'/SetField/ViewLayout/',
		width 	: 500,
		height  : 500,
		param 	: {
			LayoutId : Ext.Cmp('Field_Id').Encrypt(),
		}	
	}).popup();
}
	
}); 

</script>
<fieldset class="corner">
<legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span> </legend>	
	<div id="toolbars"></div>
		<div id="top-panel-content"></div>
		<div class="content_table"></div>
	<div id="pager"></div>
</fieldset>	
		
		