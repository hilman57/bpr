<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 * 
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 * @ example    : get by fields ID / Campaign ID
 */

if( $Flexible =& _fldFlexibleLayout($this -> URI->_get_64post('LayoutId')) ) {

	$Flexible -> _setTables('t_gn_debitur'); // rcsorce data 
	$Flexible -> _setCustomerId(array('deb_id' => 1)); // set conditional array();
	
	$Flexible -> _Compile();
}
?>
