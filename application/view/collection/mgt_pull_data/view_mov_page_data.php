<?php 
// -------------------------------------------------
  if(!function_exists('_setCheckBox') ) 
{
  function _setCheckBox( $AssignId ='' ){
	$arr = "<a href=\"javascript:void(0);\" ". 
			" onchange=\"new SetAllCheckBox(this, $AssignId);\" style=\"cursor:pointer;\">".
			"#</i></a>";	
	return (string)$arr;
  }
}


if( !function_exists('_setBoldColor') ){
	function _setBoldColor( $data = '' ){
		return "&nbsp;<span style=\"color:#000;padding-left:2px;line-height:18px;\">$data</span>&nbsp;";
	}
}

if( !function_exists('CutHistory') ){
	function CutHistory( $Id ){
		return wordwrap(History($Id),20, "<br>");
	}
}
if( !function_exists('CuttingStatus') ){
	function CuttingStatus( $word ='' ){
		return wordwrap($word,4, "<br>");
	}
}

if( !function_exists('CuttingData') ){
	function CuttingData( $word ='' ){
		return wordwrap($word,8, "<br>");
	}
}
/*
/*
 * @ pack : load helpers
 */

 $this->load->helpers(array('EUI_Object','EUI_Page'));
 $arr_obj = new EUI_Object($content_pages);
/*
 * @ pack : get all parameters 
 */
 
 $type 	= _get_post('type');
 $orderby = _get_post('orderby');
 $next_order = ($type=='ASC'?'DESC':'ASC');
 
 // debug only : $arr_header = $arr_obj->fetch_field();
 
 $arr_header = array
 (
	"Product"=> "Produk.",
	"AccountNo"=> "Costumer ID.",
    "CustomerName" => "Nama Pelanggan",
    "AgentId" => "Agent ID",
    "AccountStatus"=> "Status Akun",
	"LastCallStatus" => "Panggilan Terakhir",
	"CallDate" => "Tanggal Panggilan",
	"AmountWO" => "Jumlah WO",
	"BalAfterPay" => "Bal.Afterpay",
	"HistoryId" => "Histori"
); 
 
  $arr_class = array
 (
	"Product"=> "content-middle",
    "AccountNo"=> "content-middle",
    "CustomerName" => "content-middle",
	"AgentId" => "content-middle",
    "AmountWO" => "content-middle",
    "AccountStatus"=> "content-middle",
	"LastCallStatus" => "content-middle",
	"CallDate" => "content-middle",
	"BalAfterPay" => "content-middle",
	"HistoryId" => "content-lasted"
 ); 
 
   
 $arr_align = array
 (
	"Product"=> "left",
    "CustomerNumber"=> "left",
    "CustomerName" => "left",
	"AgentId" => "center",
    "AmountWO" => "right",
    "CallDate"=> "center",
	"AccountStatus" => "left",
	"LastCallStatus" => "left",
	"CallDate" => "center",
	"BalAfterPay" => "right",
	"HistoryId" => "left"
 ); 
 
 /*
  * @ pack : get all labels -  array header 
  */
  
 $arr_width = array( 
	"AccountNo"=> "12%",
    "CustomerName" => "12%",
    "AgentId" => "12%",
    "AmountWO"=> "12%",
    "GenderId" => "12%",
	"CallDate" => "12%"
 );
 
 
/*
 * @ pack : get all labels -  array header 
 */
 $arr_function = array ( 
	'AccountNo' => "_setBoldColor",
	'CustomerName' => 'CuttingData',
	'AgentId' => "CuttingData",
	'CallDate'=> "_getDateTime",
	'AmountWO' => "number_format",
	'BalAfterPay' => "number_format",
	'LastCallStatus' => "CuttingStatus",
	'AccountStatus' => "CuttingStatus",
	'HistoryId' => "CutHistory"
 ); 
 
/*
 * @ pack : get all labels -  array header 
 */
  
 $arr_wrap = array(
	"Recipient"=> "nowrap",
    "caller_name" => "nowrap",
    "SentDate" => "nowrap",
    "user_id" => "nowrap",
    "StatusCode"=> "nowrap"
 ); 
 
// -------------- generate label on grid ----------------> 

echo "<table border=0 cellspacing=1 width=\"99%\">".
	"<tr height=\"18\"> ";
		
		
		echo "<th class=\"ui-corner-top ui-state-default center th-first\" width=\"2%\" style=\"cursor:pointer;padding:4px 0px 0px 5px;\" nowrap>". form()->checkbox("SwapParId", null, "AssignId", array("change" =>"Ext.Cmp(this.value).setChecked();"), array("style" => "cursor:pointer;") )."</th>";
		echo "<th class=\"ui-corner-top ui-state-default center th-middle\" width=\"2%\" nowrap>No.</th>";
		
		
	foreach( $arr_header as $field => $value ){
		if( in_array($orderby, array($field) ))
		{
			echo "<th class=\"ui-corner-top ui-state-default th-middle center\" width=\"{$arr_width[$field]}\"><span class=\"header_order ".strtolower($type)."\" onclick=\"new ViewSwapData({page:0,  orderby:'{$field}', type:'{$next_order}'});\">&nbsp;{$value}</span></th>";
		} else {
			echo "<th class=\"ui-corner-top ui-state-default th-lasted center\" width=\"{$arr_width[$field]}\"><span class=\"header_order\" title=\"Sort By {$value}\" onclick=\"new ViewSwapData({page:0, orderby:'{$field}', type:'{$next_order}' });\">&nbsp;{$value}</span></th>";
		}
	}
	
echo "</tr>";

// ---------------- content ----------------

 if( is_array($content_pages) ) 
{ 
 $no = $start_page+1;
 foreach( $content_pages as $num => $rows )
{
 $row = new EUI_Object( $rows );
// @ pack : of list color 
 $back_color = ( $num%2!=0 ? '#FFFFFF' :'#FFFEEE');
 echo " <tr bgcolor=\"{$back_color}\" class=\"onselect\" height=\"18\">";
 echo "<td class=\"content-first\" align=\"center\" nowrap >&nbsp;". form()->checkbox("AssignId", null, $row->get_value('AssignId'))."</td>";	
 echo "<td class=\"content-middle\" nowrap>{$no}</td>";
  
 	
 foreach( array_keys($arr_header) as $k => $fields )
 {
   if(strcmp( $fields, $orderby )== 0 ){
	  echo  "<td class=\"$arr_class[$fields] ui-widget-select-order {$arr_align[$fields]}\" ${arr_wrap[$fields]}>{$row->get_value($fields, $arr_function[$fields])}</td>";
   }else{
	  echo  "<td class=\"$arr_class[$fields] ui-widget-unselect-order {$arr_align[$fields]}\" ${arr_wrap[$fields]}>{$row->get_value($fields,$arr_function[$fields])}</td>";
   }
 }
 
// ---------- on event ------------------------------------------------
	echo "</tr>";
	$no++;	
 }	
 
}

/* @ pack : -------------------------------------------------------
 * @ pack : # get list off page #----------------------------------
 * @ pack : -------------------------------------------------------
 */

 $max_page = 5;
 
// @ pack : start html  

 $_li_create = " <div class='page-web-voice' style='margin-left:-5px;margin-top:2px;border-top:0px solid #FFFEEE;'><ul>";
 
// @ pack : list 
 
 $start =(int)(!$select_pages ? 1: ((($select_pages%$max_page ==0) ? ($select_pages/$max_page) : intval($select_pages/$max_page)+1)-1)*$max_page+1);
 $end   =(int)((($start+$max_page-1)<=$total_pages) ? ($start+$max_page-1) : $total_pages );
	
// @ pack : like here 

 if( $select_pages > 1) 
 {
	$post = (int)(($select_pages)-1);
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewSwapData({page: 1, orderby:'${orderby}', type:'${type}'});\" ><a href=\"javascript:void(0);\">&lt;&lt;</a></li>";
		
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewSwapData({page: ${post}, orderby:'${orderby}', type:'${type}'});\" ><a href=\"javascript:void(0);\">&lt;</a></li>";
 }

// @ pack : check its 

 if($start>$max_page){
	$_li_create.="<li cclass=\"page-web-voice-normal\"  onClick=\"Ext.DOM.ViewSwapData({page:(${start}-1), orderby :'${orderby}', type:'${type}'});\" ><a href=\"javascript:void(0);\">...</a></li>";
 }

// @ pack : check its 
 
 for( $p = $start; $p<=$end; $p++)
 { 
	if( $p == $select_pages ){ 
		$_li_create .="<li class=\"page-web-voice-current\" id=\"${p}\" onClick=\"new ViewSwapData({page:${p}, orderby :'${orderby}', type:'${type}'});\"> <a href=\"javascript:void(0);\">{$p}</a></li>";
	 } else {
		$_li_create .=" <li class=\"page-web-voice-normal\" id=\"${p}\" onClick=\"new ViewSwapData({page:${p}, orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\">{$p}</a></li>";
	}
 }

// @ pack : check its 
  
 if($end<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewSwapData({page:${end}, orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\" >...</a></li>";
 }

// @ pack : check its 
 
 if($select_pages<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewSwapData({page:(${select_pages}+1),  orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\" title=\"Next page\">&gt;</a></li>";
	$_li_create .="<li class=\"page-web-voice-normal\" onClick=\"new ViewSwapData({page:${total_pages},orderby :'${orderby}', type:'${type}'});\"><a href=\"javascript:void(0);\" title=\"Last page\">&gt;&gt;</a></li>";
 }
		
// @ pack : check its 
	
 $_li_create .="</ul></div>";
 echo "<tr>".
		"<td colspan='6'>{$_li_create}</td> ".
		"<td colspan='2'style='color:brown;' nowrap>Record(s)&nbsp;: <span class='input_text' style='padding:4px 2px 2px 2px;' id='ui-total-swap-record'>{$total_records}</span></td>".
	"</tr>	";
?>

</table>