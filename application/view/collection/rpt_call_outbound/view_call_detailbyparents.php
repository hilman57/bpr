<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title['action']; ?></title>
	<meta charset="utf-8">
	<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.grid.css?time=<?php echo time();?>" />
</head>
<body>
 
<?php 

/** load helper flexibelity **/

$this->load->helper('EUI_Field');
$FxData = & EUI_Field_value::get_instance();


/** content testing ***/
//echo "<div>{$title['action']}</div>";	

echo "<div style='margin:8px;'>";
echo "<table class='custom-grid' align='center' border=0 cellspacing=1>";
echo "<tr>";
echo "<th class='font-standars ui-corner-top ui-state-default first center' style='font-weight:bold;'>No.</th>";

if(is_array($field) ) 
	foreach( $field as $keys => $labels )
{
	echo "<th class='font-standars ui-corner-top ui-state-default first center' style='font-weight:bold;'>{$labels['label']}</th>";
	
}	

echo "</tr>";

$num = 1;
if(is_array($content) ) 
	foreach( $content as $n =>$rows )
{
	$color = ( $num%2!=0 ? '#ffffff' : '#FAFFF9');
	
	echo "<tr bgcolor='". $color ."' class='onselect'>";
		echo "<td class='content-first content-middle' style='padding:4px;font-size:12px;height:24px;color:#043005;'>".$num."</td>";
	foreach( $field as $primary => $style ) {
		echo "<td class='content-middle content-middle' style='padding:4px;font-size:12px;height:24px;color:#043005;' align='{$style['align']}' >".$FxData->FieldValue($style['fn'], $rows[$primary])."</td>";
	}
	echo "</tr>";	
	$num++;
}
echo "</table>";
echo "</div>";

?> 

</body>
</html>