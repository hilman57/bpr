<?php  echo javascript(); ?>

<script>
var height_max = 100;
/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
 
Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 });

/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
 
Ext.document().ready(function(){
$('.date').datepicker({
		dateFormat : 'dd-mm-yy',
		changeYear: true,
		changeMonth: true
	});
	
	
 Ext.Cmp('submit_data').listener({ 
	onclick : function(){ Ext.Window ({
			url   		 : Ext.DOM.INDEX +"/CallOutboundReport/display/",
			width 		 : $(window).width(),
			height 		 : $(window).height(),
			scrollbars 	 : 1, 
			scrolling	 : 1, 
			name 		 : 'parent_window',	
			param 		 : Ext.Join([ 
				(function(form){
					var obj = form.getElement(), p = {};
						for( var i in obj ){
							p[i] = encodeURIComponent(obj[i]);
						}	
					return p;
				})(Ext.Serialize('frmAllFind'))
			]).object() 
		}).popup();
	 }
 });
	
	Ext.Cmp('clear_data').listener({ 
		onclick : function(){
			Ext.Serialize('frmAllFind').Clear();
		}	
	});
});	
</script>
<fieldset class="corner">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="curv0" style="margin:5px;" >
	<fieldset class='corner'>
			<legend class='icon-menulist'>&nbsp;&nbsp;Filter </legend>
			<div>
			<form name="frmAllFind">
				<table width="99%">
					<tr>
						<td class="text_caption bottom">Major DC</td>
						<td><?php __(form()->combo('major_dc', 'select', $MajorDC));?></td>
						<td class="text_caption  bottom">DC</td>
						<td><?php __(form()->combo('nama_dc', 'select', $DCName ));?></td>
					</tr>	
					<tr>
						<td class="text_caption  bottom">Major Produk</td>
						<td><?php __(form()->combo('major_produk', 'select',$MajorProduct));?></td>
						<td class="text_caption  bottom">Kategory</td>
						<td><?php __(form()->combo('kategory', 'select',$CategoryName ));?></td>
					</tr>	
					<tr>
						<td class="text_caption  bottom">Produk</td>
						<td><?php __(form()->combo('name_produk', 'select', $Product ));?></td>
						<td class="text_caption  bottom">Campaign</td>
						<td><?php __(form()->combo('nama_campaign', 'select', $CampaignName ));?></td>
					</tr>	
					<tr>
						<td class="text_caption  bottom">Created By</td>
						<td><?php __(form()->combo('created_by', 'select', $UserId ));?></td>
						<td class="text_caption  bottom">Mode Of Payment</td>
						<td><?php __(form()->combo('mode_of_payment', 'select', $PaymentMode ));?></td>
					</tr>	
					<tr>
						<td class="text_caption  bottom">Second Payment</td>
						<td><?php __(form()->combo('second_payment', 'select', $SecondPayment ));?></td>
						<td class="text_caption">Month of Report</td>
						<td>
							<?php //__(form()->input('start_date', 'input_text date', NULL ));?> <!-- to --> 
							<?php //__(form()->input('end_date', 'input_text date', NULL ));?> 
							<?php __(form()->combo('start_date', 'select ', $ReportMonth ));?> 
						</td>
						
						<!-- <td class="text_caption">Interval</td>
						<td>
							<?php //__(form()->input('start_date', 'input_text date', NULL ));?> to 
							<?php //__(form()->input('end_date', 'input_text date', NULL ));?> </td> -->
					</tr>	
					
					<tr>
						<td class="text_caption">&nbsp;</td>
						<td> 
							<?php __(form()->button('submit_data', 'button page-go', 'Submit' ));?> &nbsp;
							<?php __(form()->button('clear_data', 'button clear', 'Reset' ));?>
						</td>
					</tr>
				</table>	
				</form>
			</div>
	</fieldset>
</div>

</fieldset>