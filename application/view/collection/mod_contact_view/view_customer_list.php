<?php 
/** set label style on selected **/
page_background_color('#FFFCCC');
page_font_color('#8a1b08');

/** get label header attribute **/

$labels =& page_labels();
$primary =& page_primary();

/** rider get lable ****/

?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php 
			foreach( $labels as $Field => $LabelName) : 
				$class=&page_header($Field); 
				if($Field != 'CallReasonId'):  ?> 
					<th nowrap class="font-standars ui-corner-top ui-state-default middle center"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th> <?php 
				endif;
			endforeach; 
		?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
$special_status = array('1','3','6');
foreach( $page -> result_assoc() as $rows )
{ 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	$flag  = ( in_array($rows['CallReasonCategoryId'], $special_status) || ($rows['CallAttempt'] <= 5 && $rows['CallAttempt'] >= 1) ? true : false );
?>
	<tr class="onselect" <?php echo ($flag ? 'style="background:#FFDDFA;color:black;font-weight:bold;"' : 'style="background:'.$color.';"');?> >
		<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary],array('click' => 'Ext.DOM.gotoCallCustomer();'), null ); ?>
		<td class="content-middle center"> <?php echo $no;?></td>
		<?php 
		foreach( $labels as $Field => $LabelName ) : 
			$color=&page_column($Field);  
			if($Field != 'CallReasonId')
			{
		?>
		<td class="content-middle" <?php __($color) ?> ><?php echo ( $rows[$Field]?$rows[$Field]:'-' ); ?></td>
		<?php 
			}
		endforeach; 
		?>
	</tr>	
</tbody>
<?php
	$no++;
}

?>
</table>


