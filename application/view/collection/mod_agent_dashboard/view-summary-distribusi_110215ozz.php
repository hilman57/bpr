<fieldset class="corner" style="background-color:#FFFFFF;">
	<legend class="icon-menulist">&nbsp;&nbsp;Monitoring Distribusi </legend>	
<div>	
<table border=0 align="center" cellspacing="1" width="99%">
	<tr>
		<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Date </td>
		<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Caller Name</td>
		<?php endif; ?>
		<?php if( in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Campaign</td>
		<?php endif; ?>
		<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Total Customer</td>
		<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Total Policy</td>
	</tr>
	
<?php 

	$no = 1;
	// echo "<pre>";
	// print_r($DistribusiByDate);
	// echo "</pre>";
	if( is_array($DistribusiByDate) )foreach( $DistribusiByDate as $row ){  
	
		foreach( $row as $tanggal => $rows ){
			$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
		
	?>
	<tr class="onselect" bgcolor="  <?php echo $color;?>">
		<td class="content-first center">&nbsp;<?php echo date('d/m/Y',strtotime($rows['createdate']));?></td>
		<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<td class="content-middle left">&nbsp;<?php echo $rows['agentname'];?></td>
		<?php endif;?>
		<?php if( in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="content-middle right"><?php echo $rows['CampaignCode'];?></td>
		<?php endif; ?>
		<td class="content-middle right"><?php echo $rows['customer'];?>&nbsp;</td>
		<td class="content-lasted right"><?php echo $rows['policy'];?>&nbsp;</td>
	</tr>
<?php 
	$total_policy += $rows['policy'];
	$total_customer += $rows['customer'];
	$no++;
	} 
}
?>	

<tr>
		<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=2>&nbsp;<b>Total</b></td>
		<?php else : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=2>&nbsp;<b>Total</b></td>
		<?php endif; ?>
		<th class="font-standars ui-corner-top ui-state-default middle right">&nbsp;<b><?php echo $total_customer; ?></b>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted right">&nbsp;<b><?php echo $total_policy; ?></b>&nbsp;</td>
	</tr>
</table>
</div>
</fieldset>