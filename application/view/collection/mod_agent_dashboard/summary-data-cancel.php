<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-menulist">&nbsp;&nbsp;Summary Result Attempt</legend>
<div id="summary-Result-Attempt">
	<table border=0 align="center" cellspacing="1" width="99%">
		<tr>
			<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
				<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Caller Name</td>
			<?php endif; ?>
			<?php if( in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
				<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Date</th>
			<?php endif; ?>
			<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Customer</th>
			<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Policy</th>
			<th class="font-standars ui-corner-top ui-state-default first">&nbsp;Attempt</th>
		</tr>
		<?php 
			foreach($ResultAttempt as $row){
				foreach($row as $key=>$val){
					$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
		?>
		<tr class="onselect" bgcolor="  <?php echo $color;?>">
			<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
				<td class="content-first left"><?php echo $val['agentname']; ?></td>
			<?php endif; ?>
			<?php if( in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
				<td class="content-first center"><?php echo $val['createdate']; ?></td>
			<?php endif; ?>
			<td class="content-middle right"><?php echo $val['customer']; ?></td>
			<td class="content-middle right"><?php echo $val['policy']; ?></td>
			<td class="content-lasted right"><?php echo $val['Attempting']; ?></td>
		</tr>
		<?php
				$total_policy += $val['policy'];
				$total_customer += $val['customer'];
				$total_Attempting += $val['Attempting'];
				$no++;
				}
			}
?>	

<tr>
		<?php if( !in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) ) : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=1>&nbsp;<b>Total</b></td>
		<?php else : ?>
			<th class="font-standars ui-corner-top ui-state-default first" colspan=1>&nbsp;<b>Total</b></td>
		<?php endif; ?>
		<th class="font-standars ui-corner-top ui-state-default middle right">&nbsp;<b><?php echo $total_customer; ?></b>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted right">&nbsp;<b><?php echo $total_policy; ?></b>&nbsp;</td>
		<th class="font-standars ui-corner-top ui-state-default lasted right">&nbsp;<b><?php echo $total_Attempting; ?></b>&nbsp;</td>
	</tr>
	</table>
</div>
</fieldset>	