<?php 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 * 
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 * @ example    : get by fields ID / Campaign ID
 */

if( $Flexible =& _cmpFlexibleLayout($Customers['CampaignId']) ) 
{
	$Flexible -> _setTables('t_gn_debitur'); // rcsorce data 
	$Flexible -> _setCustomerId(array('CustomerId' => $Customers['CustomerId'])); // set conditional array();
	$Flexible -> _Compile();
}
// END OFF
