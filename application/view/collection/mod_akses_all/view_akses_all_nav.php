<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	ModAccess_account_status : "<?php echo _get_exist_session('ModAccess_account_status');?>",
	ModAccess_Exist_Agent 		: "<?php echo _get_exist_session('ModAccess_Exist_Agent');?>",
	ModAccess_New_Agent 			: "<?php echo _get_exist_session('ModAccess_New_Agent');?>",
	ModAccess_cust_id 		: "<?php echo _get_exist_session('ModAccess_cust_id');?>",
	ModAccess_cust_name 		: "<?php echo _get_exist_session('ModAccess_cust_name');?>",
	ModAccess_start_amountwo : "<?php echo _get_exist_session('ModAccess_start_amountwo');?>",
 	ModAccess_end_amountwo 	: "<?php echo _get_exist_session('ModAccess_end_amountwo');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav	 : Ext.DOM.INDEX+'/ModAksesAllData/index/',
	custlist : Ext.DOM.INDEX+'/ModAksesAllData/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('ModAccessAll').getElement();
	// console.log(Ext.Join([param]).object());
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('ModAccessAll').Clear();
	Ext.DOM.searchCustomer();
}
		
// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!='') 
 {	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.navigation.custnav
		}
  });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}



// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Upload Data'],['Distribute']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['UploadData'],['ManageDis']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['database_go.png'],['database_add.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}

Ext.DOM.Upload = function(){
	Ext.Cmp("loading-image").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtBucket/UploadBucket/',
		method 	: 'POST',
		param	: {
			TemplateId : Ext.Cmp('upload_template').getValue(),
			CampaignId : Ext.Cmp('upload_campaign').getValue()
		},
		complete : function(fn){
			var ERR = eval(fn.target.DONE);
			try {	
				if( ERR )
				{
					var CALLBACK = JSON.parse(fn.target.responseText);
					if( (typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null) ){
						var msg = "\n";
						for(var i in  CALLBACK.mesages ){
							msg += "Failed : "+ CALLBACK.mesages[i].N +",\nSuccess : "+CALLBACK.mesages[i].Y +"\n Blacklist : "+ CALLBACK.mesages[i].B +",\nDuplicate : "+  CALLBACK.mesages[i].D  +"\nDuplicate(s) : "+CALLBACK.mesages[i].X+"\n";
						}	
						Ext.Msg(msg).Error();
					}
					else{
						Ext.Msg(CALLBACK.mesages).Info();
					}	
					
					Ext.Cmp("loading-image").setText('');
					Ext.EQuery.construct(navigation,'')
					Ext.EQuery.postContent();
				}	
			}
			catch(e){ alert(e); }
		}
	}).upload()
 }
 
 Ext.DOM.ClosePanel = function() {
	Ext.Cmp('span_top_nav').setText("");
}

getAssignTo = function()
{
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	// alert(AssignType
	if(AssignType!='')
	{
		Ext.Ajax
		({
			url    : Ext.DOM.INDEX+"/ModAksesAllData/getAssignTo/",
			method : 'POST',
			param  : {
				AssignType : AssignType
			}	
		}).load("Assign-level");
	}
	
}
function valid_assgin_all()
{
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	var AssignTo = Ext.Cmp('Assign_To').getValue();
	if(AssignType !='')
	{
		if(AssignType==1)
		{
			return true;
		}
		else
		{
			if (AssignTo != '')
			{
				return true;
			}
			else
			{
				alert('Please Select Assign To');
				return false;
			}
		}
		
	}
	else
	{
		alert('Please Select Assign Type');
		return false;
	}
}

function valid_assgin()
{
	var customerid = Ext.Cmp('CustomerId').getValue();
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	var AssignTo = Ext.Cmp('Assign_To').getValue();
	
	if(customerid !='')
	{
		if(AssignType !='')
		{
			if(AssignType==1)
			{
				return true;
			}
			else
			{
				if (AssignTo != '')
				{
					return true;
				}
				else
				{
					alert('Please Select Assign To');
					return false;
				}
			}
			
		}
		else
		{
			alert('Please Select Assign Type');
			return false;
		}
	}
	else
	{
		alert('Please Select Customer');
		return false;
	}
	
}

Ext.DOM.Reset = function()
{
	var customerid = Ext.Cmp('CustomerId').getValue();
	if(customerid !='')
	{
		if(confirm('Are You Sure Reset This Customer ?'))
		{
			// alert(customerid);
			Ext.Ajax({
					url 	: Ext.DOM.INDEX+'/ModAksesAllData/ResetAccessByCheck',
					method 	: 'POST',
					param 	: { CustomerId : customerid
							},
					ERROR	: function(e) {
						var ERROR = JSON.parse(e.target.responseText);
						if( ERROR.Message ) {
							alert(' Success('+ERROR.Success+') Fail('+ERROR.Fail+') Reset Access All Data');
							Ext.EQuery.construct(navigation,'')
							Ext.EQuery.postContent();
						}
					}	
			}).post();
		}
	}
	else
	{
		alert('Please Select Customer');
	}
}

// reset all access
Ext.DOM.ResetAll = function()
{
	
	if(confirm('Are You Sure Reset All?'))
	{
		// alert(customerid);
		Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/ModAksesAllData/ResetAccessAll',
				method 	: 'POST',
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert('Success Reset All, Access All Data');
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
		}).post();
	}
};


// assign all access data
Ext.DOM.AssignAll = function()
{
	var AssignTo = Ext.Cmp('Assign_To').getValue();
	var AssignToText = Ext.Cmp('Assign_To').getText();
	var AssignType = Ext.Cmp('Assign_Type').getValue();
	var conf = (AssignTo?'Are You Sure Assign All Data to '+AssignToText+'?':'Are You Sure Assign All Data?');
	if(valid_assgin_all())
	{
		if(confirm(conf))
		{
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/ModAksesAllData/AssignAccessAll',
				method 	: 'POST',
				param 	: { AssignType : AssignType,
							AssignTo : AssignTo
						},
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert(' Success('+ERROR.Success+') Fail('+ERROR.Fail+') Distribute Access All Data');
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
			}).post();
		}
	}
	
	
};

Ext.DOM.Assign = function()
{
	
	if(valid_assgin())
	{
		var customerid = Ext.Cmp('CustomerId').getValue();
		var AssignType = Ext.Cmp('Assign_Type').getValue();
		var AssignTo = Ext.Cmp('Assign_To').getValue();
	
		Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/ModAksesAllData/AssignAccessCheck',
				method 	: 'POST',
				param 	: { CustomerId : customerid,
							AssignType : AssignType,
							AssignTo : AssignTo
						},
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert(' Success('+ERROR.Success+') Fail('+ERROR.Fail+') Distribute Access All Data');
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
		}).post();
	}
};
	// @ pack : upload manage distribute access all 

Ext.DOM.ManageDis = function() {
	ClosePanel();
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/ModAksesAllData/ManageDistribute/',
		param : { 
			time : Ext.Date().getDuration()
		}
	}).load("span_top_nav");
};

// @ pack : upload data listener 

Ext.DOM.UploadData = function() {
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/MgtBucket/ManualUpload/',
		param : { 
			time : Ext.Date().getDuration()
		}
	}).load("span_top_nav");
};
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="ModAccessAll">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # New Agent &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('ModAccess_New_Agent','select auto', $New_Agent, _get_exist_session('ModAccess_New_Agent'));?></td>
			<td class="text_caption bottom"> # Exist Agent &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('ModAccess_Exist_Agent','select auto', $Exist_Agent, _get_exist_session('ModAccess_Exist_Agent'));?></td>
			
		</tr>
		<tr>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('ModAccess_cust_id','input_text long', _get_exist_session('ModAccess_cust_id'));?></td>
			<td class="text_caption bottom"> # Account Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('ModAccess_account_status','select auto', $AccountStatus, _get_exist_session('ModAccess_account_status'));?></td>
			
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('ModAccess_cust_name','input_text long', _get_exist_session('ModAccess_cust_name'));?></td>
			
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="span_top_nav"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->