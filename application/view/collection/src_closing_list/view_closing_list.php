<?php
	$labels =& page_labels();
	$primary =& page_primary();
?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24">
		<th nowrap rowspan="2" class="font-standars ui-corner-top ui-state-default middle center">#</th>
		<th nowrap rowspan="2" class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>			
		<?php 
			foreach( $labels as $Field => $LabelName) : 
				$class=&page_header($Field); 
			if($Field != 'CallReasonId') {
				if($Field=='AproveName' && $this ->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND){
			} else{
			?>
				<th nowrap class="font-standars ui-corner-top ui-state-default middle center" rowspan="2"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
			<?php  } 
				}
			endforeach; ?>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Question</th>
	</tr>
</thead>	
<tbody>
<?php

$no = $num;
$arcolor = array("2"=>"style = \"background:#FFA0A0\"",
				 "3"=>"style = \"background:#FFA0A0\"",
				 "4"=>"style = \"background:#FFA0A0\"",
				 "5"=>"style = \"background:#FFA0A0\"");
				 
foreach( $page -> result_assoc() as $rows )
{
 $disabled = (in_array(_get_session('HandlingType'), array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND) )? array('disabled'=>'disabled') : NULL );
 $color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
 $crc = $rows['CallReasonCategoryId'];
?>
	<tr class="onselect" <?php echo ($flag ? 'style="background:#FFDDFA;color:black;font-weight:bold;"' : 'style="background:'.$color.';"');?> >
		<td class="content-first center" width='5%'> <?php echo form()->checkbox('CustomerId', null, $rows[$primary], null, null ); ?>
		<?php echo form()->hidden( "primx", null, $rows[$primary]);?></td>
		<td class="content-middle center" onclick = "Ext.DOM.clickGotoCallCustomer('<?php echo $rows[$primary]; ?>');"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field); ?>
			<?php if( $Field=='PhoneType' ){?>
			<td class="content-middle" <?php __($color) ?>><?php echo ( $sphone[$rows[$Field]]?$sphone[$rows[$Field]]:'-' ); ?></td>
			<?php
				}else if($Field=='AproveName' && $this ->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND){
			?>
			<?php
				}else{
			?>
			<td class="content-middle" <?php __($color) ?> onclick = "Ext.DOM.clickGotoCallCustomer('<?php echo $rows[$primary]; ?>');"><?php echo ( $rows[$Field]?$rows[$Field]:'-' ); ?></td>
			<?php
				}
			?>
		<?php endforeach;  ?>
			<td class="content-lasted"><a href="javascript:void(0);" onclick="Ext.DOM.ShowDetailQuestion('<?php echo $rows[$primary]; ?>', event);"> Show Detail Q</a> </td>
		</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>


