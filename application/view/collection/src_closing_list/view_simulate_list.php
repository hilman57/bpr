<table border=0 align="left" cellspacing=1 width="100%">
	<tr height='24'>
		<td class='class="ui-corner-top ui-state-default center'>Group Premi</td>
		<td class='class="ui-corner-top ui-state-default center'>Gender</td>
		<td class='class="ui-corner-top ui-state-default center'>Payment Mode</td>
		<td class='class="ui-corner-top ui-state-default center'>Plan</td>
		<td class='class="ui-corner-top ui-state-default center'>Premi (IDR)</td>
	</tr>
<?php 

$no = 0;

if(is_array($ProductSimulate) ) 
foreach( $ProductSimulate as $key => $rows )
{
	$color = ($key%2!=0?'#FFFEEE':'#FFFFFF');  
?>
	<tr height='20' class='onselect' bgcolor='<?php __($color);?>'>
		<td class="content-first"><b style='color:red;'><?php __($rows['PremiumGroupName']); ?><b></td>
		<td class="content-middle left"><?php __(strtoupper($rows['Gender'])); ?></td>
		<td class="content-middle center"><?php __(strtoupper($rows['PayMode'])); ?></td>
		<td class="content-middle left"><?php __(strtoupper($rows['ProductPlanName'])); ?></td>
		<td class="content-lasted right"><?php __($this ->EUI_Tools->_set_rupiah($rows['ProductPlanPremium'])); ?></td>
	</tr>
<?php }; ?>
</table>
