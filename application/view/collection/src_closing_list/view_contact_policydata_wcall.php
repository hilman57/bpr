<?php 
/*
 * @ def 		: view_content_policydata data load by Policy ID 
 * -----------------------------------------
 * 
 * @ params  	: $ Policy ID 
 * @ example    : -
 */
 // echo "<pre>";
 // print_r($PolicyData);
 // echo "</pre>";
 ?>
 
<form name="frmInfoPolicy">
<table width="99%" align="center" cellpadding="2px" cellspacing="4px">
	<tr>
		<td nowrap class="text_caption">Policy Number</td>
		<td><?php echo form() -> input('PolicyNumber','input_text long',$PolicyData['PolicyNumber'],NULL,1);?> </td>
		<td class="text_caption" nowrap >Product Name</td>
		<td><?php echo form() -> input('PolicyProductName','input_text long',$PolicyData['PolicyProductName'],NULL,1);?></td>
		<td class="text_caption" nowrap >Product Category</td>
		<td><?php echo form() -> input('PolicyProductCategory','input_text long',$PolicyData['PolicyProductCategory'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap >Product Type</td>
		<td><?php echo form() -> input('PolicyProductType','input_text long',$PolicyData['PolicyProductType'],NULL,1);?></td>
		<td class="text_caption" nowrap >Product Type Detail</td>
		<td><?php echo form() -> input('PolicyProductTypeDetail','input_text long',$PolicyData['PolicyProductTypeDetail'],NULL,1);?></td>
		<td class="text_caption" nowrap >DC Name</td>
		<td><?php echo form() -> input('Nama_Dc','input_text long',$PolicyData['Nama_Dc'],NULL,1);?></td>
	</tr>
	<tr>
		<td class="text_caption" nowrap>Branch</td>
		<td><?php echo form() -> input('Branch_Name','input_text long',$PolicyData['Branch_Name'],NULL,1); ?></td>
		<td class="text_caption" nowrap>District Name</td>
		<td><?php echo form() -> input('CustomerCountry','input_text long',$PolicyData['CustomerCountry'],NULL,1); ?></td>
		<td class="text_caption" nowrap>Policy Iss Date</td>
		<td><?php echo form() -> input('PolicyIssDate','input_text long',$PolicyData['PolicyIssDate'],NULL,1); ?></td>
	</tr>

</table>	
</form>