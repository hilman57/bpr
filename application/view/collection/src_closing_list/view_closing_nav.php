<?php
	echo javascript();
?>
<script type="text/javascript">

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText("Customer Completed");
 }); 
 	
	
var Reason = [];

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* catch of requeet accep browser **/
Ext.DOM.datas = 
{
	cls_cust_name 	 	: '<?php echo _get_exist_session('cls_cust_name');?>',
	cls_cif_number 		: '<?php echo _get_exist_session('cls_cif_number');?>', 
	cls_policy_number	: '<?php echo _get_exist_session('cls_policy_number');?>',
	cls_product_name 	: encodeURIComponent('<?php echo _get_exist_session('cls_product_name');?>'),
	cls_attempt_count	: '<?php echo _get_exist_session('cls_attempt_count');?>',
	cls_call_reason_id	: '<?php echo _get_exist_session('cls_call_reason_id');?>',	
	cls_campaign		: '<?php echo _get_exist_session('cls_campaign');?>',
	cls_caller_name		: '<?php echo _get_exist_session('cls_caller_name');?>',
	order_by 			: '<?php echo _get_post('order_by');?>',
	type	 			: '<?php echo _get_post('type');?>',
}
			
	/* assign navigation filter **/
		
Ext.DOM.navigation = {
	custnav	 : Ext.DOM.INDEX +'/SrcCustomerClosing/index/',
	custlist : Ext.DOM.INDEX +'/SrcCustomerClosing/Content/'
}
		
/* assign show list content **/

Ext.EQuery.construct(Ext.DOM.navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();
	
/* function searching customers **/
	
var searchCustomer = function(){
	var param = Ext.Serialize('clsCustomers').getElement();
	var arr = [];
	
	for(var i in param)
	{
		if(i == 'cls_product_name')
		{
			arr[i] = encodeURIComponent(param[i]);
		}
		else{
			arr[i] = param[i];
		}
	}

  Ext.EQuery.construct(Ext.DOM.navigation,Ext.Join([
			arr
		]).object());
		
	Ext.EQuery.postContent()
}
		
/* function clear searching form **/
	
var resetSeacrh = function() {
	Ext.Serialize('clsCustomers').setValue('');
	Ext.EQuery.construct(Ext.DOM.navigation,Ext.Join([
		Ext.Serialize('clsCustomers').getElement()
	]).object());
		
	Ext.EQuery.postContent()
}

Ext.DOM.Checked= function(){
	Ext.Cmp('CustomerId').setChecked();
}


/* Ext.DOM.ShowDetailQuestion **/

Ext.DOM.ShowDetailQuestion = function(CustomerId, e){
  if(CustomerId)
  {
	Ext.Window({
		url 	: Ext.DOM.INDEX+"/SrcCustomerClosing/ViewQuestion/",
		method 	:'GET',
		width 	: 450,
		height 	: ($(window).height()/2),
		left 	: $(window).width(),
		top		: e.layerY,
		param 	: {
			CustomerId : Ext.BASE64.encode(CustomerId)
		}
	}).popup();	
 }	
}


Ext.DOM.UnComplete = function() {
var CustomerIdAll = Ext.Cmp('CustomerId').getValue();
if( CustomerIdAll.length > 0 ) {
	Ext.Ajax
	({
		url : Ext.DOM.INDEX+"/SrcCustomerList/UnApprovalAll/",
		method : 'POST',
		param : {
			CustomerIdAll : CustomerIdAll 	
		},
		
		ERROR : function(e){
			Ext.Util(e).proc(function(resp){
				if( resp.success )
				{
					var msg = '';
					if( typeof(resp.failed) ){
						for( var i in resp.failed ){
							msg+= resp.failed[i]+"\n";
						}
					}
					
					if( typeof(resp.succesd) ){
						for( var i in resp.succesd ){
							msg+= resp.succesd[i]+"\n";
						}
					}
					
					alert(msg);
					
					Ext.DOM.searchCustomer();
				}
				else{
					Ext.Msg('Approval All').Failed();
				}
			});
		}
	}).post();
}
else{
	Ext.Msg('Please select a row !').Info();
	}	
}

Ext.DOM.clickGotoCallCustomer = function(CustomerId){
			Ext.ActiveMenu().NotActive();
			Ext.EQuery.Ajax
			({
				url 	: Ext.DOM.INDEX +'/ModContactView/ContactDetail/',
				method  : 'GET',
				param 	: {
					CustomerId : CustomerId,
					ControllerId : Ext.DOM.INDEX +'/SrcCustomerClosing/index/', 
					
				}
			});
}

 Ext.DOM.gotoCallCustomer = function()
 {
	var CustomerId  = Ext.Cmp('chk_cust_call').getChecked();
	if( CustomerId!='')
	{	
		if( CustomerId.length == 1 ) {
			Ext.ActiveMenu().NotActive();
			Ext.EQuery.Ajax
			({
				url 	: Ext.DOM.INDEX +'/ModContactView/ContactDetail/',
				method  : 'GET',
				param 	: {
					CustomerId : CustomerId,
					ControllerId : Ext.DOM.INDEX +'/SrcCustomerClosing/index/', 
					
				}
			});
		}
		else{ Ext.Msg("Select One Customers !").Info(); }			
	}
	else{ Ext.Msg("No Customers Selected !").Info(); }	
 }

	/* memanggil Jquery plug in */
	
		$(function(){
			
			
			$('#toolbars').extToolbars({
				extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
				extTitle  : [['Search'],

				<?php if ($this -> EUI_Session -> _get_session('HandlingType')==3) : ?>
					['Select All '],
					['Un Complete '],
				<?php else : ?>
					// ['Go to Call '],
				<?php endif ?>
				
				['Clear']],
				extMenu   : [['searchCustomer'],

				<?php if ($this -> EUI_Session -> _get_session('HandlingType')==3) : ?>
					['Checked '],
					['UnComplete '],
				<?php else : ?>
					// ['gotoCallCustomer'],
				<?php endif ?>
				
				['resetSeacrh']],
				extIcon   : [['zoom.png'],

				<?php if ($this -> EUI_Session -> _get_session('HandlingType')==3) : ?>
					['accept.png'],['tick.png'],
				<?php else : ?>
					// ['Go to Call '],
				<?php endif ?>

				['cancel.png']],
				extText   : true,
				extInput  : true,
				extOption : [{
						render : 4,
						type   : 'combo',
						header : 'Call Reason ',
						id     : 'v_result_customers', 	
						name   : 'v_result_customers',
						triger : '',
						store  : Reason
					}]
			});
		});
		
	Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>
<fieldset class="corner" onKeyDown="enterSearch(event)">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
	<div id="span_top_nav">					
	 <div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
	 <form name="clsCustomers">
		<table cellpadding="8px;">
			<tr>
				<td class="text_caption"> Owner Name</td>
				<td><?php echo form() -> input('cls_cust_name','input_text long', _get_exist_session('cls_cust_name'));?></td>
				<td class="text_caption"> CIF Number</td>
				<td><?php echo form() -> input('cls_cif_number','input_text long', _get_exist_session('cls_cif_number'));?></td>
				<td class="text_caption"> Policy Number</td>
				<td><?php echo form() -> input('cls_policy_number','input_text long', _get_exist_session('cls_policy_number'));?></td>
				<td class="text_caption"> Attempt</td>
				<td><?php echo form() -> input('cls_attempt_count','input_text box', _get_exist_session('cls_attempt_count'));?></td>
			</tr>
			<tr>
				<td class="text_caption"> Campaign</td>
				<td><?php echo form() -> combo('cls_campaign','select auto', (isset($cmp_active)?$cmp_active :null ) , _get_exist_session('cls_campaign'));?></td>
				<td class="text_caption"> Product Category</td>
				<td><?php echo form() -> combo('cls_product_name','select auto',(isset($ProductId)? $ProductId :null ), $this-> URI -> _get_post('cls_product_name'));?></td>
				<td class="text_caption"> Call Status</td>
				<td><?php echo form() -> combo('cls_call_reason_id','select auto', (isset($CallResult)?$CallResult :null ) , _get_exist_session('cls_call_reason_id'));?></td>
				<?php
				
				if(_get_exist_session('HandlingType') == USER_SUPERVISOR)
				{
				
				?>
				<td class="text_caption"> Caller</td>
				<td><?php echo form() -> combo('cls_caller_name','select auto', (isset($Agent)?$Agent :null ) , _get_exist_session('cls_caller_name'));?></td>
				<td class="text_caption"> QA Status</td>
				<td><?php echo form() -> combo('cls_qa_status','select auto', (isset($QaStatus)?$QaStatus :null ) , _get_exist_session('cls_qa_status'));?></td>
				<?php
				
				}
				
				?>
			</tr>
		</table>
		</form>
	</div>
	</div>
	<div id="toolbars"></div>
	<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	