<script>
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
var reason = [];

/*  status CallInterest policy **/

Ext.DOM.CallInterest = function(){
return( Ext.Ajax
({
	url : Ext.DOM.INDEX +'/SetCallResult/getEventType/',
	method : 'GET',
	param :{
		CallResultId : Ext.Cmp('CallResult').getValue()
	}
 }).json());	
}
 
 
/*  status keberadaan policy **/
 
Ext.DOM.PolicyReady = function(){
return( Ext.Ajax ({
	url : Ext.DOM.INDEX +'/SrcCustomerList/FollowUpCollection/',
	method : 'GET',
	param :{
		CustomerId : Ext.Cmp('CustomerId').getValue()
	}
 }).json());
} 

 
Ext.DOM.initFunc = { 
	validParam : false,
	isCallPhone : false,
	isRunCall : false,
	isHangup : false,
	isCancel : true,
	isSave : false	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.DisabledActivity = function() {
	if( Ext.DOM.initFunc.isCallPhone !=true) {
		Ext.Cmp('CallStatus').disabled(true);
		Ext.Cmp('CallResult').disabled(true); 
	}
	else {
		Ext.Cmp('CallStatus').disabled(false);
		Ext.Cmp('CallResult').disabled(false); 
	}
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.query(function(){
  Ext.query("#tabs" ).tabs();
  Ext.query('#toolbars').extToolbars
  ({
		extUrl    : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle  : [['Add Phone'],['Help'],[]],
		extMenu   : [['Ext.DOM.UserWindow'],[''],[]],
		extIcon   : [['monitor_edit.png'],['help.png'],[]],
		extText   : true,
		extInput  : true,
		extOption  : [{
				render : 1,
				type   : 'combo',
				header : 'Script ',
				id     : 'v_result_script', 	
				name   : 'v_result_script',
				triger : 'ShowWindowScript',
				width  : 220,
				store  : [
						Ext.Ajax
						({
							url		: Ext.DOM.INDEX+'/SetCampaignScript/getScript/', 
							param	: { 
								CustomerId : Ext.Cmp('CustomerId').getValue() 
							}
									
						}).json()]
			},{
				render : 2,
				type : 'label',
				label : '<span style="color:#dddddd;">-</span>',
				name : 'html_ajax_loading',
				id   : 'html_ajax_loading'
			}]
	});
	
  Ext.query('.date').datepicker({dateFormat:'dd-mm-yy'});	
  Ext.DOM.DisabledActivity();
});
/* 
 * @ def : ShowWindowScript
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.ShowWindowScript = function(ScriptId)
{
	var WindowScript = new Ext.Window 
	({
			url    : Ext.DOM.INDEX +'/SetCampaignScript/ShowCampaignScript/',
			name    : 'WinProduct',
			height  : (Ext.Layout(window).Height()),
			width   : (Ext.Layout(window).Width()),
			left    : (Ext.Layout(window).Width()/2),
			top	    : (Ext.Layout(window).Height()/2),
			param   : {
				ScriptId : Ext.BASE64.encode(ScriptId),
				Time	 : Ext.Date().getDuration()
			}
	}).popup();
		
	// close window 	
	if( ScriptId =='' ){
		WindowScript._w.close();
	}	
}


 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.dialCustomer = function(){
	// ExtApplet.setData({   
		// Phone : Ext.Cmp("CallingNumber").getValue(), 
		// CustomerId  : Ext.Cmp("CustomerId").getValue() 
	// }).Call();
	
	Ext.DOM.initFunc.isCallPhone = true;
	Ext.DOM.initFunc.isCancel = false;
	
	window.setTimeout(function(){
		Ext.DOM.DisabledActivity();
		Ext.DOM.initFunc.isRunCall = true;
	},2000);
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

Ext.DOM.hangupCustomer =function()
{
	Ext.DOM.initFunc.isRunCall = false;
	Ext.DOM.initFunc.isCancel = false;
	ExtApplet.setHangup();
	return false;
} 
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.getCallReasultId = function(combo)
{
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SrcCustomerList/setCallResult/',
		method  : 'GET',
		param  : {
			CategoryId : combo.value
		}	
	}).load("DivCallResultId");	
	
	//Ext.Cmp('create_policy').disabled(true);
	Ext.Cmp('date_call_later').setValue('');
	Ext.Cmp('hour_call_later').setValue('');
	Ext.Cmp('minute_call_later').setValue('');
	Ext.Cmp('date_call_later').disabled(true);
	Ext.Cmp('hour_call_later').disabled(true);
	Ext.Cmp('minute_call_later').disabled(true);
}


 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.SaveCustomerInfo = function(){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/ModSaveActivity/SaveInfoCustomer/',
		method 	: 'POST',
		param 	: Ext.Join([Ext.Serialize('frmInfoCustomer').getElement()]).object(),
		ERROR   : function(fn){
			try{
				var ERR = JSON.parse(fn.target.responseText);
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();
}

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

 
Ext.DOM.CallSessionId = function(){
	return ( typeof (ExtApplet.getCallSessionId() ) =='undefined' ? 
			'NULL': ExtApplet.getCallSessionId() );
}


/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */

 Ext.DOM.CekPolicyForm = function(){
	return(
		Ext.Ajax
		({
			url : Ext.DOM.INDEX +'/SrcCustomerList/CekPolicyForm/',
			method : 'POST',
			param :{
				CustomerId : Ext.Cmp('CustomerId').getValue(),
				CallReasonId : Ext.Cmp('CallResult').getValue()
			}
		}).json() );
 }

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
 
Ext.DOM.saveActivity =function() {
 var ActivityCall = [];
 var ActivityForm = Ext.Serialize('frmActivityCall').Complete([
		'QualityStatus','ProductForm','CallingNumber',
		'PhoneNumber','AddPhoneNumber','date_call_later',
		'hour_call_later','minute_call_later','CallComplete'
    ]);
   
   ActivityCall['CustomerId']		= Ext.Cmp('CustomerId').getValue();
   ActivityCall['CallingNumber'] 	= Ext.Cmp('CallingNumber').getValue();
   ActivityCall['CallSessionId'] 	= Ext.DOM.CallSessionId();
   ActivityCall['ScriptId'] 		= Ext.Cmp('v_result_script').getValue();
   
	if( !ActivityForm ){ 
		Ext.Msg('Input form not complete').Info(); }
	else 
	{
		console.log(Ext.DOM.initFunc.isCallPhone);
		console.log(Ext.DOM.initFunc.isRunCall);
		if( (Ext.DOM.initFunc.isCallPhone==true)
			&& (Ext.DOM.initFunc.isRunCall==false) )
		{
			if( (Ext.DOM.CallInterest().event.CallReasonEvent == 1) 
				&& (Ext.DOM.CallInterest().event.form == 0) 
				&& (Ext.DOM.PolicyReady().PolicyReady==0))
			{
				 Ext.Msg('No Policy Data').Info();
			}
			else
			{
			  // handle call
			  Ext.DOM.initFunc.isSave = true;
			  Ext.DOM.initFunc.isCallPhone = false;
			  Ext.DOM.initFunc.isCancel = true;
			  Ext.Ajax
			  ({
				 url 	: Ext.DOM.INDEX +'/ModSaveActivity/SaveActivity/',
				 method : 'POST',
				 param 	: Ext.Join([Ext.Serialize('frmActivityCall').getElement(),ActivityCall]).object(),
				 ERROR  : function(fn){
						Ext.Util(fn).proc(function(save){
							if( save.success ) {
								Ext.Msg("Save Activity").Success();
								Ext.DOM.SaveCustomerInfo();
								Ext.DOM.CallHistory(); 
								$('#tabs_list').tabs( "option", "selected", 3);
								Ext.DOM.DisabledActivity();
							}
							else{
								Ext.Msg("Save Activity").Failed();
							}
						});
					}
			  }).post();
		   }
		}
		else{
			Ext.Msg("Theres No Call Activity OR Call Is Running").Info();
		}	
	}	
}
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.CallHistory = function(){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/ModSaveActivity/CallHistory/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue()
		}
	}).load("tabs_list-4");
}

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.document(document).ready(function(){
	Ext.DOM.CallHistory();
	$('#CustomerDOB').datepicker({
		dateFormat : 'dd-mm-yy',
		changeYear : true, changeMonth : true
	})
});

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 Ext.DOM.EventFromPolicy = function(e){
	Ext.DOM.PolicyDetailById(e.value);
	
	if( e.value !='' ) {
		Ext.Window ({
			url 		: Ext.DOM.INDEX+'/ProjectWorkForm/index/',	
			method 		: 'POST',
			width  		: (Ext.query(window).width()-(Ext.query(window).width()/4)), 
			height 		: Ext.query(window).height(),
			left  		: (Ext.query(window).width()/2),
			scrollbars 	: 1,
			sccrolling	: 1,
			param  		: 
			{
				ViewLayout 	 : 'ADD_FORM',	
				PolicyId : Ext.Cmp(e.id).getValue(),
				CustomerId 	 : Ext.Cmp('CustomerId').getValue()
			}
		}).popup();
		
		Ext.Cmp('CallStatus').disabled(true);
		Ext.Cmp('CallResult').disabled(true);
		Ext.Cmp('ProductForm').disabled(true);
		Ext.Cmp('ButtonUserCancel').disabled(true);
		Ext.Cmp('ButtonUserSave').disabled(true);
	}
 }
 
/* * 
 * @ def : ajax loading navigation 
 * -------------------------------------------------
 * 
 */
 
Ext.DOM.AjaxStart = function(){
	Ext.Cmp('html_ajax_loading').setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='10px;'> <span style='color:red;'>Please Wait...</span>");
} 
 
/* * 
 * @ def : AjaxStop
 * -------------------------------------------------
 * 
 */ 
  
Ext.DOM.AjaxStop = function(){
	Ext.Cmp('html_ajax_loading').setText('<span style="color:#dddddd;">-</span>');
} 
 
/* * 
 * @ def : PolicyDetailById
 * -------------------------------------------------
 * 
 */ 
Ext.DOM.PolicyDetailById = function(PolicyId){
	Ext.DOM.AjaxStart();
	
	if( PolicyId!='' ) {
		$('#tabs_list').tabs( "option", "selected", 0);
		Ext.Ajax({
			url : Ext.DOM.INDEX+'/SrcCustomerList/PolicyDetailById/',
			method :'GET',
			param : {
				PolicyId : PolicyId
			}	
		}).load('tabs_list-1');
		
		
	}
	
	Ext.DOM.AjaxStop();
	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.getEventSale = function(object) {
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/SetCallResult/getEventType/',
		method : 'GET',
		param :{
			CallResultId : object.value
		},
		ERROR : function(fn){
			try
			{
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success)
				{
					if( typeof(ERR.event)=='object')
					{
						if( ERR.event.CallReasonEvent==1 ){ 
							Ext.Cmp('ProductForm').disabled(false);
						}
						else{
							Ext.Cmp('ProductForm').disabled(true);
							Ext.Cmp('ProductForm').setValue('');
						}
							
						if( ERR.event.CallReasonLater==1){
							Ext.Cmp('date_call_later').disabled(false);
							Ext.Cmp('hour_call_later').disabled(false);
							Ext.Cmp('minute_call_later').disabled(false);
						}
						else{
							Ext.Cmp('date_call_later').setValue('');
							Ext.Cmp('hour_call_later').setValue('');
							Ext.Cmp('minute_call_later').setValue('');
							Ext.Cmp('date_call_later').disabled(true);
							Ext.Cmp('hour_call_later').disabled(true);
							Ext.Cmp('minute_call_later').disabled(true);
						}
					}
				}
				else{
					
				}	
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CancelActivity =function()
 {
	if( (Ext.DOM.initFunc.isCancel==true ) ) 
	{
		Ext.ActiveMenu().Active();
		ControllerId = Ext.Cmp("ControllerId").getValue();
		Ext.EQuery.Ajax
		({
			url 	: ControllerId,
			method 	: 'GET',
			param 	: { act : 'back-to-list' }
		});
	}	
	else { 
		Ext.Msg('Please Save Activity').Info();
	  }	
 }

 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.UserWindow = function(){
	Ext.DOM.AdditionalPhone( Ext.Cmp('CustomerId').getValue() );
	return false;
}
</script>