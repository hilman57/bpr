<div class="panel-product-preview-only">
 <table border="0" cellspacing="1" align="left" width="100%">
	<tr>
		<td class="text_caption bottom left" width='10%'>Product Name</td>
		<td class="text_caption left" width='70%'><?php __(form() -> combo('ProductSimulate','select long', 
			$Product, null, array('change' => 'Ext.DOM.ProdPreview(this.value);')));?>
		</td>
	</tr>
	
	<tr height=24>
		<td height="24" class="ui-corner-top ui-state-default center" colspan='2'>Result</td>
	</tr>
	<tr>
		<td colspan=2> 
		<div id="product_list_preview" 
			style="border:1px solid #dddddd;overflow:auto;padding:2px;">
		
			</div>
		</td>
	</tr>
</table>	
</div>