<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
<fieldset class="corner" style="background-color:white;margin:3px;">
 <legend class="icon-application">&nbsp;&nbsp;&nbsp;Add Category Result </legend>	
	<table cellpadding="6px;">
	<tr>
		<td class="text_caption">* Call Type </td>
		<td><?php echo form() -> combo('CallOutboundGoalsId','input_text long',$CallType);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Interest</td>
		<td><?php echo form() -> combo('CallReasonInterest','input_text long',$Interest);?></td>
	</tr>
	<tr>
		<td class="text_caption">* Category Code</td>
		<td><?php echo form() -> input('CallReasonCategoryCode','input_text long');?></td>
	</tr>
	<tr>
		<td class="text_caption">* Category Name</td>
		<td><?php echo form() -> input('CallReasonCategoryName','input_text long');?></td>
	</tr>
	<tr>
		<td class="text_caption">* Status</td>
		<td><?php echo form() -> combo('CallReasonCategoryFlags','select long', array(0=>'Not Active',1=>'Active'));?></td>
	</tr>
	<tr>
		<td class="text_caption">* Order </td>
		<td><?php echo form() -> combo('CallReasonCategoryOrder','select long', $OrderId);?></td>
	</tr>
	<tr>
		<td class="text_caption">&nbsp;</td>
		<td>
			<input type="button" class="save button" onclick="Ext.DOM.SaveCatgory();" value="Save"></td>
	</tr>
</table>
</fieldset>
</div>