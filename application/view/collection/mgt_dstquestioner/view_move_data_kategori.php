<div class="ui-widget-form-table-compact" style="width:99%;">
    <div class="ui-widget-form-row">
        <div class="ui-widget-form-cell ui-widget-content-top " style="width:70%">
            <fieldset class="corner" style="border-radius:3px;margin:-20px 0px 0px -15px;">
                <legend class="icon-menulist"> <span style="margin-left:8px;">List Kategori Data</span></legend>
                <div id="ui-widget-content-debitur-page" style="margin-top:-5px;">

                    <div id="ui-widget-content-debitur-page" style="margin-top:-5px;">
                        <!-- <h2>haloo</h2> -->
                        <div id="toolbars" style='margin:0px 15px 0px 10px;'></div>
                        <div id="span_top_nav"></div>
                        <table width="100%" class="custom-grid" cellspacing="1">
                            <thead>
                                <tr height="24">
                                    <th nowrap class="font-standars ui-corner-top ui-state-default first left"
                                        width="5%">&nbsp;#</th>
                                    <th nowrap class="font-standars ui-corner-top ui-state-default middle center"
                                        align="center">&nbsp;No</th>
                                    <th nowrap class="font-standars ui-corner-top ui-state-default middle center"
                                        align="center">&nbsp;Nama Kategori</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "Select * from t_lk_kategori_question";
                                    $page = $this->db->query($sql);

                                    $no  = 1;
                                    foreach( $page -> result_assoc() as $rows ) { 
                                        $color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
                                        // echo "<pre>";
                                        // print_r($rows);
                                    ?>
                                <tr CLASS="onselect" bgcolor="<?php echo $color;?>">
                                    <td class="content-first">
                                        <input type="checkbox" value="<?php echo $rows['Id']; ?>" name="ktgid"
                                            id="ktgid">
                                    </td>
                                    <td class="content-middle"><?php echo $no ?></td>
                                    <td class="content-middle"><b><?php echo $rows['ktgr_name']; ?></b></td>

                                </tr>
                            </tbody>
                            <?php
                                $no++;
                            };
                            ?>
                        </table>

                    </div>
                </div>
            </fieldset>
        </div>
    </div>

</div>