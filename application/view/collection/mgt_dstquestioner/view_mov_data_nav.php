<script>
	var amount_data = {
		total_amount: 0,
		total_checked: 0
	}

	function pickCategory(value) {
		console.log(value);
		new ViewSwapData({
			orderby: '',
			type: '',
			page: 0,
			category: value
		});
		// ShowPageSwapData('category: value');
		
	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ExitSwapData() {
		if (Ext.Msg("Are you sure?").Confirm()) {
			Ext.ShowMenu(new Array("Welcome", "index"), "wlecome", {});
		} else {
			return false;
		}
	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function SetAllCheckBox(obj, Name) {
		if (obj.checked) {
			Ext.Cmp(Name).setChecked();
		} else {
			Ext.Cmp(Name).setUnchecked();
		}

	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ActionCheckSwap(obj) {
		console.log(obj.value);

		if (obj.checked) {
			if (obj.value == 2) {

				var BoxList = Ext.Cmp("AssignId").getValue()
				if (BoxList.length == 0) {
					Ext.Cmp('AssignId').setChecked();
					BoxList = Ext.Cmp("AssignId").getValue()
				}

				$("#swp_total_data").val(BoxList.length);
				$("#swp_total_data").attr("disabled", true);


			} else {
				//Ext.Cmp('AssignId').setUnchecked();	
				$("#swp_total_data").val(amount_data.total_amount);
				$("#swp_total_data").attr("disabled", true);
			}

		} else {
			//Ext.Cmp('AssignId').setUnchecked();	 
			$("#swp_total_data").val(amount_data.total_amount);
			$("#swp_total_data").attr("disabled", true);
		}
	}


	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ViewSwapData(obj) {
		console.log('category',obj);
		var frmSwapFilterData = Ext.Serialize('frmSwapFilterData');
		// alert(frmSwapFilterData);
		$('#ui-widget-content-debitur-page').waiter({
			url: new Array('Dstquestioner', 'PageSwapData'),
			param: Ext.Join(new Array(frmSwapFilterData.getElement(),obj)).object(),
			// param: obj
			// order: {
			// 	order_type: obj.type,
			// 	order_by: obj.orderby,
			// 	order_page: obj.page
			// },
			// // return false
			// complete: function (obj) {
			// 	var total_swap = $('#ui-total-swap-record').text();
			// 	amount_data.total_amount = parseInt(total_swap);
			// 	amount_data.total_checked = 0;

			// 	$(obj).css({
			// 		"height": "100%",
			// 		"padding": "0px -5px 4px -5px"
			// 	});
			// 	$("#swp_total_data").val(total_swap);
			// 	$("#swp_total_data").attr("disabled", true);
			// }
		});
	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function Uploader() {
		$("#ui-widget-content-upload").load(Ext.EventUrl(new Array('Dstquestioner', 'UploadSwapData')).Apply(), {},
			function (response, status, xhr) {
				console.log("loader page is :" + status);
			});
	}
	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */


	function ShowAgentByTL(obj) {
		$("#ui_swp_from_deskoll_id").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowAgentPerTL')).Apply(), {
				TL: Ext.Cmp('swp_from_leader_id').getValue()
			},
			function (response, status, xhr) {
				if (status == 'success') {
					$("#swp_from_deskoll_id").toogle();
				}
			});
	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 	SENIOR TEAM LEADER	document on ready jquery 
	 */


	function ShowTLBySTL(obj) {
		$("#ui_swp_from_tl_id").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowTLPerSTL')).Apply(), {
				TL: Ext.Cmp('swp_from_senior_leader_id').getValue()
			},
			function (response, status, xhr) {
				if (status == 'success') {
					$("#ui_swp_from_tl_id").toogle();
				}
			});
	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ShowDataToAgentByTL() {
		$("#ui-swp-user-deskoll").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowDataToAgentByTL')).Apply(), {
			TL: Ext.Cmp('swp_to_user_leader').getValue()
		}, function (response, status, xhr) {
			if (status == 'success') {
				$("#swp_to_user_deskoll").toogle();
			}
		});
	}

	function ShowDataKategori2() {
		$("#ui-swp-kategori").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowDataKategori2')).Apply(), {
			// TL: Ext.Cmp('swp_to_kategori').getValue()
		}, function (response, status, xhr) {
			if (status == 'success') {
				$("#swp_to_kategori").toogle();
			}
		});
	}

function ShowDataKategori() {	
	
	Ext.Ajax
 ({
	url 	: Ext.DOM.INDEX +'/Dstquestioner/ShowDataKategori/',
	 method : 'POST',	 
	ERROR 	: function(e){
	 Ext.Util(e).proc(function(response){
		 console.log('haloo',respon);
		if( response.success ){
			
		 } else {
			Ext.Msg("CPA Alerdy Exist").Info();
		 }
	  });
	}
 }).post();
}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 	SENIOR TEAM LEADER	document on ready jquery 
	 */

	function ShowDataToTLBySTL() {
		$("#ui-swp-user-deskoll").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowDataToTLBySTL')).Apply(), {
			TL: Ext.Cmp('swp_to_senior_leader').getValue()
		}, function (response, status, xhr) {
			if (status == 'success') {
				$("#swp_to_user_leader").toogle();
			}
		});
	}


	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ShowAllLeader(Level) {
		$("#ui-swp-user-leader").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowAllLeader')).Apply(), {
			UserLevel: Level
		}, function (response, status, xhr) {
			if (status == 'success') {
				$("#swp_to_user_spv").toogle();
				$("#swp_to_user_leader").toogle();
				$("#swp_to_user_deskoll").toogle();
			}
		});
	}
	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ShowAllSupervisor(Level) {
		$("#ui-swp-user-spv").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowAllSupervisor')).Apply(), {
			UserLevel: Level
		}, function (response, status, xhr) {
			if (status == 'success') {
				$("#swp_to_user_spv").toogle();
				$("#swp_to_user_leader").toogle();
				$("#swp_to_user_deskoll").toogle();
			}
		});
	}


	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */


	function ShowAllAgent(Level) {
		var UserTL = Ext.Cmp("swp_to_user_leader").getValue();
		$("#ui-swp-user-deskoll").load(Ext.EventUrl(new Array('Dstquestioner', 'ShowAllAgent')).Apply(), {
			UserLevel: Level,
			UserTL: UserTL

		}, function (response, status, xhr) {
			if (status == 'success') {
				$("#swp_to_user_leader").toogle();
				$("#swp_to_user_deskoll").toogle();
			}
		});
	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */


	function ShowPageSwapData() {
		new ViewSwapData({
			orderby: '',
			type: '',
			page: 0
		});
	}
	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */


	function ClearPageSwapData() {
		Ext.Serialize('frmSwapFilterData').Clear(new Array('swp_from_page_record'));
		new ShowPageSwapData();
	}
	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */
	function SubmitSwapData() {
		var frmSelectBox = new Array();
		var frmSwapFilterData = Ext.Serialize('frmSwapFilterData');
		var frmSwapActionData = Ext.Serialize('frmSwapActionData');
		frmSelectBox['AssignId'] = Ext.Cmp('AssignId').getValue();
		// console.log(frmSwapFilterData);
		// console.log(frmSwapActionData);
		// console.log(frmSelectBox['AssignId']);
		// return false

		Ext.Ajax({
			url: Ext.EventUrl(new Array('Dstquestioner', 'ActionSwapDataUser')).Apply(),
			method: 'POST',
			param: Ext.Join([
				frmSwapFilterData.getElement(),
				frmSwapActionData.getElement(),
				frmSelectBox
			]).object(),

			ERROR: function (e) {
				Ext.Util(e).proc(function (response) {
					if (response.success == 1) {
						Ext.Msg("Swap Data").Success();
						Ext.Serialize('frmSwapActionData').Clear(
							new Array(
								'swp_total_data', 'swp_to_user_level',
								'swp_to_user_leader', 'swp_to_user_deskoll', 'swap_methode','swp_to_kategori',
								'swap_type')
						);
						ShowPageSwapData();

					} else {
						Ext.Msg("Swap Data").Failed();
					}
				});
			}
		}).post();

	}

	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	function ShowUserByLevel(obj) {
		// alert(obj.value)
		// return false
		if (obj.value == 13) {
			ShowAllLeader(obj.value);
		} else if (obj.value == 4) {
			ShowAllLeader(13);
			ShowAllAgent(obj.value);
		} else if (obj.value == 3) {
			ShowAllSupervisor(3);
		}


		//console.log( obj.value );
	}


	function Upload() {
		Ext.Cmp("loading-image").setText("<img src=" + Ext.DOM.LIBRARY +
			"/gambar/loading.gif height='17px;'> <span style='color:red;'>Please Wait...</span>");
		Ext.Ajax({
			url: Ext.DOM.INDEX + '/MgtBucket/UploadBucketQuest/',
			method: 'POST',
			param: {
				TemplateId: Ext.Cmp('upload_template').getValue(),
				kategori_id: Ext.Cmp('kategori_id').getValue()
			},
			complete: function (fn) {
				var ERR = eval(fn.target.DONE);
				try {
					if (ERR) {
						var CALLBACK = JSON.parse(fn.target.responseText);
						if ((typeof CALLBACK.mesages == "object") && (CALLBACK.mesages !== null)) {
							var msg = "\n";
							for (var i in CALLBACK.mesages) {
								msg += "Failed : " + CALLBACK.mesages[i].N + ",\nSuccess : " + CALLBACK
									.mesages[i].Y + "\n Blacklist : " + CALLBACK.mesages[i].B +
									",\nDuplicate : " + CALLBACK.mesages[i].D + "\nDuplicate(s) : " + CALLBACK
									.mesages[i].X + "\n";
							}
							Ext.Msg(msg).Error();
							// Ext.DOM.Find();
						} else {
							Ext.Msg(CALLBACK.mesages).Info();
						}

						Ext.Cmp("loading-image").setText('');
					}
				} catch (e) {
					alert(e);
				}
			}
		}).upload()
	}


	// --------------------------------------------------------------------------------------
	/*
	 * @ package 		document on ready jquery 
	 */

	$(document).ready(function () {
		var height_default = new Array($('#main_content').innerHeight(), "px");

		$('#tabs-panel-swap').mytab().tabs();
		$('#tabs-panel-swap').mytab().tabs("option", "selected", 0);
		$("#tabs-panel-swap").mytab().close({}, false);

		$("#tabs-panel-swap").css({
			"padding": "0px 0px 0px 0px",
			"border-top": "0px solid #ddd",
			"border-left": "1px solid #ddd",
			"border-right": "1px solid #ddd",
			"height": height_default.join()
		});

		$("#tabs-panel-data").css({
			"height": "99%",
			"width": "97.8%",
			"border": "0px solid #ddd",
			"background-color": "#FFFFFF",
			"margin": "2px 2px 2px 2px"
		});

		$("#tabs-panel-upload").css({
			"height": "99%",
			"width": "97.8%",
			"border": "0px solid #ddd",
			"background-color": "#FFFFFF",
			"margin": "2px -2px 2px 2px"
		});

		$("#swp_from_campaign_id").toogle();
		$("#swp_from_account_status").toogle();
		$("#swp_from_call_status").toogle();
		$("#swp_from_spv_id").toogle();
		$("#swp_from_leader_id").toogle();
		$("#swp_from_deskoll_id").toogle();

		new ViewSwapData({
			orderby: '',
			type: '',
			page: 0,
			category: ''
		});

		$('.date').datepicker({
			showOn: 'button',
			buttonImage: Ext.Image("calendar.gif"),
			buttonImageOnly: true,
			changeYear: true,
			changeMonth: true,
			dateFormat: 'dd-mm-yy',
			readonly: true
		});

		new Uploader();
	});





	// test data
	$(function () {
		$('#toolbars').extToolbars({
			extUrl: Ext.DOM.LIBRARY + '/gambar/icon',
			extTitle: [
				['Add'],
				['Edit'],
				['Delete']
			],
			extMenu: [
				['FTP_adding'],
				['FTP_edit'],
				['FTP_delete']
			],
			extIcon: [
				['add.png'],
				['calendar_edit.png'],
				['delete.png']
			],
			extText: true,
			extInput: false,
			extOption: []
		});
		// ShowDataKategori()
		ShowDataKategori2()
	});

	Ext.DOM.FTP_adding = function () {
		// alert('haloo')
		// return false
		Ext.Ajax({
			url: Ext.DOM.INDEX + "/Dstquestioner/FTPAdd/",
			mthod: 'GET',
			param: {
				time: Ext.Date().getDuration()
			}
		}).load("span_top_nav");
	}

	Ext.DOM.FTP_edit = function () {
		var ftp_read_id = Ext.Cmp('ktgid').getValue();
		// alert(ftp_read_id)

		Ext.Ajax({
			url: Ext.DOM.INDEX + "/Dstquestioner/FTPEdit/",
			mthod: 'GET',
			param: {
				time: Ext.Date().getDuration(),
				ftp_read_id: ftp_read_id
			}
		}).load("span_top_nav");
	}

	Ext.DOM.FTP_delete = function () {
		var ftp_read_id = Ext.Cmp('ktgid').getValue();
		// alert(Ext.Cmp("ktgid").getValue())
		// alert(document.getElementById("ktgid"))
		// return false

		if (Ext.Msg('Do You want to delete this rows ?').Confirm()) {
			Ext.Ajax({
				url: Ext.DOM.INDEX + "/Dstquestioner/FTPDelete/",
				method: 'POST',
				param: {
					Id: ftp_read_id
				},
				ERROR: function (e) {
					Ext.Util(e).proc(function (response) {
						if (response.success) {
							Ext.Msg("Delete FTP Read ").Success();
							// Ext.EQuery.postContent();
							location.reload();
						} else {
							Ext.Msg("Delete FTP Read ").Failed();
						}
					})
				}
			}).post();
		}
	}

	// save new data read ftp 

	Ext.DOM.FtpSaveButton = function () {
		var conds = Ext.Serialize('FtpFormTemplate').Complete();
		// alert(conds)
		// return false
		var x = document.getElementById("span_top_nav");
		if (conds) {
			Ext.Ajax({
				url: Ext.DOM.INDEX + "/Dstquestioner/FTPSave/",
				method: 'POST',
				param: Ext.Join([
					Ext.Serialize('FtpFormTemplate').getElement()
				]).object(),

				ERROR: function (e) {
					Ext.Util(e).proc(function (response) {
						if (response.success) {
							Ext.Msg("Save FTP Read ").Success();
							// Ext.EQuery.postContent();
							x.style.display = "none";
							location.reload();
						} else {
							Ext.Msg("Save FTP Read ").Failed();
						}
					})
				}
			}).post();
		}
	}

	/* update ftp data read **/

	Ext.DOM.FtpEditButton = function () {
		var conds = Ext.Serialize('FtpEditTemplate').Complete();
		var x = document.getElementById("span_top_nav");
		if (conds) {
			Ext.Ajax({
				url: Ext.DOM.INDEX + "/Dstquestioner/FTPUpdate",
				method: 'POST',
				param: Ext.Join([
					Ext.Serialize('FtpEditTemplate').getElement()
				]).object(),

				ERROR: function (e) {
					Ext.Util(e).proc(function (response) {
						if (response.success) {
							Ext.Msg("Update FTP Read ").Success();
							x.style.display = "none";
							// Ext.EQuery.postContent();
							location.reload();
						} else {
							Ext.Msg("Update FTP Read ").Failed();
						}
					})
				}
			}).post();
		}
	}
	// test data
</script>

<div id="tabs-panel-swap" class="ui-content-swap">
	<ul>
		<li class="ui-tab-li-first"><a href="#tabs-panel-data">Question Data</a></li>
		<li class="ui-tab-li-lasted"><a href="#tabs-panel-upload">Upload Question</a></li>
		<li class="ui-tab-li-lasted"><a href="#tabs-panel-kategori">Kategori Data</a></li>
	</ul>

	<div id="tabs-panel-data"><?php $this->load->view("mgt_dstquestioner/view_move_data_content", array());?></div>
	<div id="tabs-panel-upload"><?php $this->load->view("mgt_dstquestioner/view_move_data_upload", array());?></div>
	<div id="tabs-panel-kategori"><?php $this->load->view("mgt_dstquestioner/view_move_data_kategori", array());?></div>

</div>