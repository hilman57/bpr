<!DOCTYPE html>
<html>
<head>
	<style>
		html {
			font-family: Trebuchet MS,Arial,sans-serif;
			font-size: 12px;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td {
			padding: 5px;
			position: center;
		}
		#color {
			background-color : #00FFFF;
		}
	</style>
	<title>Report Incoming Daily Detail</title>
</head>
<body>
	<?php 
		function convertToHoursMins($time, $format = '%02d:%02d') {
		    if ($time < 1) {
		        return;
		    }
		    $hours = floor($time / 60);
		    $minutes = ($time % 60);
		    return sprintf($format, $hours, $minutes);
		}

		function toDuration($seconds){
	        $sec = 0;
	        $min = 0;
	        $hour= 0;
	        $sec = $seconds%60;
	        $seconds = floor($seconds/60);
	        if ($seconds){
	                $min  = $seconds%60;
	                $hour = floor($seconds/60);
	        }

	        if($seconds == 0 && $sec == 0)
	            return sprintf("");
	        else
	            return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
		}
	?>
	<h1>REPORT INCOMING DAILY DETAIL</h1>
	<table>
		<tr>
			<th id="color">CALL DATE</th>
			<th id="color">PHONE NUMBER</th>
			<th id="color">DURATION</th>
		</tr>
	<?php
		foreach( $detail_daily as $key => $val ) {
	    	echo "<tr>";
	    	echo "<td>".$val['CALLDATE']."</td>";
	    	echo "<td>".$val['PHONENUMBER']."</td>";
	    	echo "<td>".toDuration($val['TOTAL_DURATION'])."</td>";
	    	echo "</tr>";
    	}
    ?>
	</table> <br><br><br>
	<!-- <footer>
		&copy;DIDIGANTENG
	</footer> -->
</body>
</html>