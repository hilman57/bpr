<script type="text/javascript">

/* validate of mail address **/

Ext.DOM.EmailValidation = function()
{
 var conds =  true, isvalid  = /\S+@\S+\.\S+/;
  // step 1
  if( !Ext.Cmp('MailTo').empty() ){
	if( !isvalid.test(Ext.Cmp('MailTo').getValue() )){
		Ext.Msg('To is not valid ').Info(); 
		conds = false;
	}
  }
  // step 2 
   else if( !Ext.Cmp('MailCC').empty() ){
	if( !isvalid.test(Ext.Cmp('MailCC').getValue() )){
		Ext.Msg('CC is not valid ').Info(); 
		conds = false;
	}
  }
  // step 3
   else if( !Ext.Cmp('MailBCC').empty() ){
	if( !isvalid.test(Ext.Cmp('MailBCC').getValue() )){
		Ext.Msg('BCC is not valid ').Error(); 
		conds = false;
	}
  }
  
  
  return conds;
  
} 

/* get all files ***/

Ext.DOM.CollectionFiles = function() 
{
	var collate = Ext.Cmp('files').getName(), uploads = {};
	for(var ux = 0; ux<collate.length; ux++) 
	{
		if( collate[ux].value!='' ) 
		{
			uploads[collate[ux].id] = document.getElementById(collate[ux].id).files[0];	
		}
	}
	
	return uploads;
}		  
		  
 
/*  Ext.DOM.MultipleAdd(this) **/

Ext.DOM.isValidFile = function(form){
	var match = new RegExp('.pdf');
	// if( !match.test(form.value) ){
		// Ext.Msg('Please select PDF Document').Info();
		// form.value = null;
	// }
	return true;
	
}	

/* Ext.DOM.SubmitFax **/

Ext.DOM.SubmitMail = function() {

 if( Ext.Cmp('MailSubject').empty())
 {
	 Ext.Msg('Subject is empty ').Info();
	 return false; }
 else 
 {
	var conds = Ext.DOM.EmailValidation();
	if( conds ) 
	{
		Ext.Ajax ({
			url  	: Ext.DOM.INDEX+"/MailCompose/SaveMailToQueue/",
			method 	: 'POST',
			file 	: [Ext.DOM.CollectionFiles()],
			param   :  {
					MailSubject : Ext.Cmp('MailSubject').getValue(),
					MailTo : Ext.Cmp('MailTo').getValue(),
					MailCC : Ext.Cmp('MailCC').getValue(),
					MailBCC : Ext.Cmp('MailBCC').getValue(),
					MailContent : window.frames[0].tinyMCE.getContent('web_editor')
			},
			complete :  function(e){
				Ext.Util(e).proc(function(send){
					if( send.success==1 ){
						Ext.Msg("Mail is send to queue ").Success();
					}
					else if( send.success==2 ){
						Ext.Msg(send.message).Success();
					}
					else{
						Ext.Msg("Mail is send to queue ").Failed();
					}
				});
			}
		}).MultipleUpload();
	}
}

	

	
	
	//var Ext.DOM.EmailValidation()  

}
 
 /* create object **/
 Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
	
 });
 
 
var MAIL_ADDRESS = [];
var ContentHTML = '';

/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */

	
Ext.DOM.AjaxStart = function(){
	Ext.Cmp("ajax_start").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'>");
}


/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */

	
Ext.DOM.AjaxStop = function(){
	Ext.Cmp("ajax_start").setText("");
}


/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
Ext.DOM.onloadTinyMCE = function() {
	Ext.DOM.AjaxStop();
	
	var editor = window.frames[0].tinyMCE;
	if( typeof(editor)=='object' ||  
		typeof(editor)=='function')
	{
		if(editor.isLoaded)
		{
			editor.setContent(ContentHTML);
			Ext.DOM.WebResizer(editor);
			editor.addEvent(editor.getInstanceById("web_editor").getWin(), "resize", function(){
				var allowHeight = parseInt(getCookie('TinyMCE_mce_editor_0_height'));
				var allowWidth = parseInt(getCookie('TinyMCE_mce_editor_0_width'));
	
				Ext.Cmp("web_editor_1").setAttribute('height',parseInt(allowHeight));
				Ext.Cmp("web_editor_1").setAttribute('width',parseInt(allowWidth)+25);
			});
		}	
	}
}

	
/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
 
var getCookie=function(cname)
{
	var name = cname + "=";
	var ca = window.frames[0].document.cookie.split(';');
	for(var i=0; i<ca.length; i++)
	  {
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	  }
	return "";
} 
	
/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
Ext.DOM.WebResizer  = function(editor){
	var allowHeight = parseInt(getCookie('TinyMCE_mce_editor_0_height'));
	var allowWidth = parseInt(getCookie('TinyMCE_mce_editor_0_width'));
	if( typeof(allowWidth)=='number' && parseInt(allowWidth) > 0 )
	{
		Ext.Cmp("web_editor_1").setAttribute('height',parseInt(allowHeight));
		Ext.Cmp("web_editor_1").setAttribute('width',parseInt(allowWidth)+25);
	}	
}	

/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
 
Ext.document().ready(function(){
	Ext.DOM.AjaxStart();
	Ext.Css("wrapper-compose").style({"text-align" : "left" });		
	var WindowFrame = Ext.Cmp("web_editor_1").getElementId();
		WindowFrame.src = Ext.DOM.LIBRARY+"/pustaka/tinymcpuk/index.php?time="+Ext.Date().getDuration();
		WindowFrame.frameborder=1; 
		WindowFrame.height = '100%';
		WindowFrame.scrolling="no"


});

 
</script> 
<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div class="panel-content-fax">
	<table cellspacing=5 cellpadding='2px' border=0 width='99%'>
		<tr>
			<td width='5%' class=" text_caption bottom left " style='font-size:14px;padding:0px 0px 4px 0px;'> Subject </td>
			<td class="bottom"><?php __(form()->textarea('MailSubject', 'textarea long', NULL, NULL,array('style' => 'text-align:middle;height:25px;width:680px;font-size:13px;color:#858081;')));?> </td>
		</tr>
		<tr>
			<td class="text_caption bottom left" style='font-size:14px;padding:0px 0px 4px 0px;'>* TO </td>
			<td class="bottom left"><?php __(form()->textarea('MailTo', 'textarea long', NULL, NULL,array('style' => 'height:25px;width:680px;font-size:13px;color:#858081;')));?></td>
		</tr>	
		
		<tr>
			<td class="text_caption bottom left" style='font-size:14px;padding:0px 0px 4px 0px;'>* CC </td>
			<td class="bottom left"><?php __(form()->textarea('MailCC', 'textarea long', NULL, NULL,array('style' => 'height:25px;width:680px;font-size:13px;color:#858081;')));?></td>
		</tr>	
		
		<tr>
			<td class="text_caption bottom left" style='font-size:14px;padding:0px 0px 4px 0px;'>* BCC </td>
			<td class="bottom left"><?php __(form()->textarea('MailBCC', 'textarea long', NULL, NULL,array('style' => 'height:25px;width:680px;font-size:13px;color:#858081;')));?></td>
		</tr>
		
		<tr>
			<td colspan=2>
				<div id="wrapper-compose">
					<div id="ajax_start"></div>
					<iframe onload="Ext.DOM.onloadTinyMCE();" name="iframe_web_editor" id="web_editor_1" style="text-align:left;margin:0px;border:0px solid #000;"></iframe>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan=2> 
				<div id="content_type_data">
					<form name="multiple_upload" enctype="multipart/form-data">
					<?php for( $file = 1; $file<=3; $file++ ) : ?>
						<input type="file" id="files<?php __($file);?>" name="files" onchange="Ext.DOM.isValidFile(this);"><br/>
					<?php endfor; ?>	
					</form>
				</div>
			</td>
		</tr>
		<tr>
			<td><?php __(form()->button('submit', 'save button','SUBMIT', array('click' => "Ext.DOM.SubmitMail();") ));?> </td>
		</tr>
	</table>

</div>
</fieldset>