<style>
.table_luar {
  border: 2px solid #42b983;
  border-radius: 3px;
  background-color: #fff;
}

.table_dalam {
  background-color: #fff;
}

.table_dalam th,td {
    width: 50px;
    padding: 1px 2px;
}

th {
  background-color: #42b983;
  color: rgba(255,255,255,0.66);
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

td {
  background-color: #f9f9f9;
}

</style>

<script>
Ext.query('.date').datepicker({
    showOn : 'button', 
    buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
    buttonImageOnly	: true, 
    dateFormat : 'yy-mm',
    readonly:true,
    onSelect	: function(date){
        if(typeof(date) =='string'){
            var x = date.split('-');
            var retur = x[2]+"-"+x[1]+"-"+x[0];
            if(new Date(retur) > new Date()) {
                Ext.Cmp($(this).attr('id')).setValue('');
            }
        }
    }
});
</script>
<form target="_blank" action="<?php echo base_url('index.php/Ranking/detail') ?>" method="get">

<fieldset class="corner" style='margin-top:0px;'>
	<legend class="icon-menulist">&nbsp;&nbsp; REPORT RANKING </legend>
    <div>
        <div class='bottom'>
            <input type="text" name="bulan" class="input_text box date">
            <input type="submit" value="Action" name="button">
            <input type="submit" value="Excel" name="button">
        </div>
    </div>
</fieldset>


</form>
<!--<table class="table_luar">
    <tr>
        <th>#</th>
        <th>ID TL</th>
        <th>Amount</th>
        <th>Detail</th>
    </tr>
    <?php
    $no = '1';
    foreach($data as $row) {
        $nama_tl = $this->db->query("SELECT full_name,code_user FROM t_tx_agent WHERE UserId='$row->tl_id'")->result()
    ?>
    <tr>
        <td align="center"><?php echo $no++ ?></td>
        <td style="width:300px;"><?php echo $nama_tl[0]->code_user ?> [ <?php echo $nama_tl[0]->full_name ?> ]</td>
        <td style="width:200px;">Rp. <?php echo number_format($row->amount_tl,0,'.','.') ?></td>
        <td style="width:450px;">
        <table class="table_dalam">
            <tr>
                <th>#</th>
                <th>ID Agent</th>
                <th>Amount</th>
            </tr>
            <?php
            $no_agent = '1';
            $data_agent = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.id as id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
            FROM t_gn_payment
            INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
            WHERE t_tx_agent.tl_id='$row->tl_id'
            GROUP BY t_tx_agent.id
            ORDER BY amount_tl DESC")->result();

            foreach ($data_agent as $row_agent) {
            ?>
            <tr>
                <td align="center"><?php echo $no_agent++ ?></td>
                <td style="width:300px;"><?php echo $row_agent->code_user ?> [ <?php echo $row_agent->nama_tl ?> ]</td>
                <td style="width:150px;">Rp. <?php echo number_format($row_agent->amount_tl,0,'.','.') ?></td>
            </tr>
            <?php } ?>
        </table>
        </td>
    </tr>
    <?php
    }
    ?>
</table> --!>

<!-- <table class="table_luar">
    <tr>
        <th>Ranking</th>
        <th>ID TL</th>
        <th>Amount</th>
        <th>Detail</th>
    </tr>
    <tr>
        <td>1</td>
        <td>TL01</td>
        <td>Rp <?php echo number_format('7000000',0,'.','.') ?></td>
        <td>
            <table class="table_dalam">
                <tr>
                    <th>Ranking</th>
                    <th>ID Agent</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>R001</td>
                    <td>4000000</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>R002</td>
                    <td>3000000</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>TL02</td>
        <td>4000000</td>
    </tr>
    <tr>
        <td>3</td>
        <td>TL03</td>
        <td>3000000</td>
    </tr>
</table> -->