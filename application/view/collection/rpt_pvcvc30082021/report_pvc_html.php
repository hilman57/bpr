<?php

$header = array(
	'Date'              => 'Date',
	'OpenPVC'           => 'Opening',
	'BalanceOpenPVC'    => 'Balance Opening',
	'NewPVC'            => 'New',
	'BalanceNewPVC'     => 'Balance New',
	'ExitPVC'           => 'Exit',
	'BalanceExitPVC'    => 'Balance Exit',
	'ClosingPVC'        => 'Closing',
	'BalanceClosingPVC' => 'Balance Closing'
);

$numberingformat = array('BalanceOpenPVC','BalanceNewPVC','BalanceExitPVC','BalanceClosingPVC');

?>
<!DOCTYPE HTML>
<html>
<head>
<title> <?php echo ucfirst(base_layout());?> :: <?php echo $title; ?> </title>
</head>
<body style="margin:0px;">
<?php
	echo "<H2> ".$title." </>";
	echo "<br>";
	echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	echo '<tr>';
	foreach($header as $column => $name)
	{
		echo '<th style="font-family:Arial;font-size:12px;padding:8px;border:1px solid #FFCCCC;background-color:#eee;">' . $name .' </th>';
	}
	echo '</tr>';
	
	$no=1;
	foreach($data_tabel as $rows => $datarow)
	{
		foreach($datarow as $row => $content){
			echo '<tr>';
			foreach($header as $column => $name)
			{
				if($column=="no")
				{
					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'.$no. '</td>';
					$no++;
				}
				else
				{
					$number = 0;
					if(in_array($column, $numberingformat)){
						$number = number_format($content[$column],2,",",".");
					}else{
						$number = ($content[$column]?$content[$column]:0);
					}
					
					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'. $number . '</td>';
				}
				
			}
			echo '</tr>';
		}
	}
	echo '</table>';
	// echo "<pre>";
	// print_r($data_tabel);
	// echo "</pre>";
?>
</body>
</html>	