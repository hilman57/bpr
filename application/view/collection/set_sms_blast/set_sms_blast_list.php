<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('chk_menu').setChecked();">#</a></th>		
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="center">&nbsp;No.</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Nama SMS Template </th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Isi SMS Template </th>        
        <th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Data Status</th>
		<!-- remarks untuk sms blast tidak berdasarkan set day, tapi paten berdasarkan hari 02012018-->
		<!--<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Days</th>-->
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Tanggal Terkirim</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;Waktu Terkirim</th>
	</tr>
</thead>	
<tbody>
<?php
	$no  = $num;
	foreach( $page -> result_assoc() as $rows )
	{ 
		$color= ($no%2!=0?'#FAFFF9':'#FFFFFF');
?>
	<tr class="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"> <?php echo form()->checkbox('chk_menu',null,$rows['SRV_Data_Id']);?> </td>
		<td class="content-middle" align="center"> <?php echo $no; ?></td>
		<td class="content-middle"> <?php echo $rows['SmsTplSubject']; ?></td>
		<td class="content-middle"> <?php echo $rows['SmsTplContent']; ?></td>
		<td class="content-middle"> <?php echo $rows['CallReasonDesc']; ?></td>
		<!--<td class="content-middle"><?php //echo $rows['SRV_Data_Priority']; ?></td>-->
		<td class="content-middle"><?php echo $rows['SRV_Data_SentDate']; ?></td>
		<td class="content-lasted"><?php echo $rows['SRV_Data_SentTime']; ?></td>
	</tr>
</tbody>
<?php
		$no++; 
   };
?>
</table>