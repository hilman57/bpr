<?php echo javascript(); ?>
<script>

	Ext.DOM.initUser  = 0;
	
	Ext.DOM.onload= (function(){
		Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
	})()
	
	Ext.DOM.datas = {
		key_words : '<?php echo $this -> URI -> _get_post('key_words'); ?>'
	}
	
	Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
	Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';
	
	$(function(){
		$('#toolbars').extToolbars({
			extUrl   	: Ext.DOM.LIBRARY+'/gambar/icon',
			extTitle 	: [['Add'],['Edit'],['Remove'],[],['Search']],
			extMenu  	: [['Add'],['Edit'],['Delete'],[],['Search']],
			extIcon  	: [['add.png'],['application_form_edit.png'],['cross.png'],[],['magnifier.png']],
			extText  	: true,
			extInput 	: true,
			extOption	: [{
			render		: 3,
			type		: 'text',
			value  		: Ext.DOM.datas.key_words,
			id			: 'keyword_data', 
			name		: 'keyword_data',
			width		: 160
			}]
		});
	});
	
	Ext.Cmp('keyword_data').listener({
		onKeyup : function( e ){
			if( e.keyCode == 13 )
			{
				Ext.EQuery.construct(Ext.DOM.Navigation, {
				key_words : e.target.value
				})
				Ext.EQuery.postContent();
			}
		} 
	});
	
	Ext.DOM.Navigation = {
		custnav : Ext.DOM.INDEX+'/SetSmsBlast/index', 
		custlist : Ext.DOM.INDEX+'/SetSmsBlast/content', 
	};
	Ext.EQuery.construct(Ext.DOM.Navigation, Ext.DOM.datas);
	Ext.EQuery.postContentList();
	
	Ext.DOM.Search = function(){
		Ext.EQuery.construct(Ext.DOM.Navigation, {
			key_words : Ext.Cmp('keyword_data').getValue()
		})
		Ext.EQuery.postContent();
	}
	
	Ext.DOM.Add = function( menu )
	{
		Ext.Ajax({
			url : Ext.DOM.INDEX+'/SetSmsBlast/AddSetSMS',
			method : 'POST',
			param  : {
				action:'menu_add_tpl'
			}
		}).load("top_header");
	}
	
	Ext.DOM.Edit = function()
	{
		var sms_id = Ext.Cmp('chk_menu').getChecked();

		if( sms_id!='' )
		{
			if( sms_id.length==1)
			{
				Ext.Ajax({
					url 	: Ext.DOM.INDEX+'/SetSmsBlast/EditSetSMS',
					method  : 'POST',
					param   : 	{
									sms_id : sms_id
								}
				}).load('top_header');
			} else {
				alert('Please select one rows!')
			}
		} else {
			alert("Please select rows!");
		}
	}
	
	Ext.DOM.SaveSet = function()
	{
		if( Ext.Cmp('template').empty() ) {
			alert("Please choose template !");
			Ext.Cmp('template').setFocus();
		} else if( Ext.Cmp('data_status').empty() ) {
			alert("Please choose data status !");
			Ext.Cmp('data_status').setFocus();
		} else if( Ext.Cmp('SentDate').empty() ) {
			alert("Please choose days !");
			Ext.Cmp('SentDate').setFocus();
		} else {
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX+'/SetSmsBlast/SaveSetSMS',
				method 	: 'POST',
				param  	: Ext.Serialize('frmAddSetSMS').getElement(),
				ERROR : function(e){
					Ext.Util(e).proc(function(save){
						if(save.success){
							Ext.Msg("Save Set SMS!").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Save Set SMS !").Failed();
							return false;
						}
					});
				}
			}).post();
		}
	}
	
	Ext.DOM.UpdateSet = function()
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SetSmsBlast/UpdateSetSMS',
			method 	: 'POST',
			param  	: Ext.Serialize('frmEditSetSMS').getElement(),
			ERROR : function(e){
					Ext.Util(e).proc(function(save){
						if(save.success){
							Ext.Msg("Update Set SMS!").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Update Set SMS !").Failed();
							return false;
						}
					});
				}
		}).post();
	}
	
	Ext.DOM.Delete = function()
	{
		var ID = Ext.Cmp('chk_menu').getValue();
		
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SetSmsBlast/DeleteSetSMS',
			method 	: 'POST',
			param  	: {
				ID : ID
			},
			ERROR : function(e){
					Ext.Util(e).proc(function(save){
						if(save.success){
							Ext.Msg("Delete Set SMS!").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Delete Set SMS !").Failed();
							return false;
						}
					});
				}
		}).post();
	}

</script>

<fieldset class="corner">
	<legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span></legend>
	<div id="toolbars" class="toolbars"></div>
	<div id="top_header" style="margin-top:8px;"></div>	
	<div class="content_table"></div>
	<div id="pager"></div>
</fieldset>