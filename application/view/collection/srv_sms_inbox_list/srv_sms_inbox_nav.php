<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	inbox_campaign_id	: "<?php echo _get_exist_session('inbox_campaign_id');?>",
	inbox_cust_id		: "<?php echo _get_exist_session('inbox_cust_id');?>",
	inbox_cust_name		: "<?php echo _get_exist_session('inbox_cust_name');?>",
	inbox_phone_num		: "<?php echo _get_exist_session('inbox_phone_num');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/SMSInbox/index/',
	custlist : Ext.DOM.INDEX+'/SMSInbox/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('sms_inbox').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('sms_inbox').Clear();
	Ext.DOM.searchCustomer();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!=0) 
 {	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/SMSInbox/index/', 
		}
  });
 } else { 
	Ext.Msg("Unknown Customers !").Info(); 
 }
 
}



// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear']],
		extMenu  : [['searchCustomer'],['resetSeacrh']],
		extIcon  : [['zoom.png'],['zoom_out.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="sms_inbox">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Produk&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('inbox_campaign_id','select auto', $CampaignId, _get_exist_session('inbox_campaign_id'));?></td>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('inbox_cust_id','input_text long', _get_exist_session('inbox_cust_id'));?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # Nama Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('inbox_cust_name','input_text long', _get_exist_session('inbox_cust_name'));?></td>
			<td class="text_caption bottom"> # Nomor Telpon&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('inbox_phone_num','input_text long', _get_exist_session('inbox_phone_num'));?></td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->