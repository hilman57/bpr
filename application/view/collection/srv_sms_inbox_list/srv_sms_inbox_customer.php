<table cellspacing=1 width='99%'>
<thead>
	<tr height="27">
		<th class="ui-corner-top ui-state-default first" width='2%'>No</th>
		<th class="ui-corner-top ui-state-default middle left" width='8%'>Penerima</th>
		<th class="ui-corner-top ui-state-default middle left" width='40%'>Pesan</th>
		<th class="ui-corner-top ui-state-default middle center" >&nbsp;Tanggal Terkirim</th>
		<th class="ui-corner-top ui-state-default middle" >Terima Tanggal</th>
		<th class="ui-corner-top ui-state-default lasted left" >Status</th>
	</tr>
</thead>
	
<tbody>	
<?php
if( is_array($content_pages) ) { 
 $no = $start_page+1;
 foreach( $content_pages as $num => $rows )
{
 
// @ pack : of list color 

 $back_color = ( $num%2!=0 ? '#FFFFFF' :'FFFEEE');
// @ pack : of content table 

 echo " <tr bgcolor='{$back_color}' class='onselect'>
		<td class='content-first' nowrap>{$no}</td>	
		<td class='content-middle' nowrap>&nbsp;". _getPhoneNumber($rows['Sender'])."</td>
		<td class='content-middle'><div class='text-content left-text'>". $rows['TextMessage'] ."</div></td>
		<td class='content-middle center' nowrap>&nbsp;". _getDateTime($rows['SentDate']) ."</td>
		<td class='content-middle center'>&nbsp;". _getDateTime($rows['RecvDate']) ."</td>
		<td class='content-middle left'>&nbsp;". $rows['IsRead'] ."</td>
		</tr>";
	$no++;	
 }	
 
}

/* @ pack : -------------------------------------------------------
 * @ pack : # get list off page #----------------------------------
 * @ pack : -------------------------------------------------------
 */

 $max_page = 10;
 
// @ pack : start html  

 $_li_create = " <div class='page-web-voice' style='margin-left:-5px;margin-top:2px;border-top:0px solid #FFFEEE;'><ul>";
 
// @ pack : list 
 
 $start =(int)(!$select_pages ? 1: ((($select_pages%$max_page ==0) ? ($select_pages/$max_page) : intval($select_pages/$max_page)+1)-1)*$max_page+1);
 $end   =(int)((($start+$max_page-1)<=$total_pages) ? ($start+$max_page-1) : $total_pages );
	
// @ pack : like here 

 if( $select_pages > 1) 
 {
	$post = (int)(($select_pages)-1);
	
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsInbox('1');\" ><a href=\"javascript:void(0);\">&lt;&lt;</a></li>";
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\"Ext.DOM.SmsInbox('$post');\" ><a href=\"javascript:void(0);\">&lt;</a></li>";
 }

// @ pack : check its 

 if($start>$max_page){
	$_li_create.="<li cclass=\"page-web-voice-normal\"  onclick=\" Ext.DOM.SmsInbox('" .($start-1) . "');\" ><a href=\"javascript:void(0);\">...</a></li>";
 }

// @ pack : check its 
 
 for( $p = $start; $p<=$end; $p++)
 { 
	if( $p == $select_pages ){ 
		$_li_create .="<li class=\"page-web-voice-current\" id=\"{$selpages[$p]}\" onclick=\" Ext.DOM.SmsInbox('{$p}');\"> <a href=\"javascript:void(0);\">{$p}</a></li>";
	 } else {
		$_li_create .=" <li class=\"page-web-voice-normal\" id=\"{{$selpages[$p]}}\" onclick=\" Ext.DOM.SmsInbox('{$p}');\"><a href=\"javascript:void(0);\">{$p}</a></li>";
	}
 }

// @ pack : check its 
  
 if($end<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\" Ext.DOM.SmsInbox('".($end+1) ."');\"><a href=\"javascript:void(0);\" >...</a></li>";
 }

// @ pack : check its 
 
 if($select_pages<$total_pages){
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\" Ext.DOM.SmsInbox('". ($select_pages+1) ."');\"><a href=\"javascript:void(0);\" title=\"Next page\">&gt;</a></li>";
	$_li_create .="<li class=\"page-web-voice-normal\" onclick=\" Ext.DOM.SmsInbox('". ($total_pages)."');\"><a href=\"javascript:void(0);\" title=\"Last page\">&gt;&gt;</a></li>";
 }
		
// @ pack : check its 
	
 $_li_create .="</ul></div>";
 
 if( $total_pages > 1){	
	 echo "<tr>
			<td colspan='4'>{$_li_create}</td>
			<td style='color:red;'>Record (s)&nbsp;: <span class='input_text' style='padding:2px;'>{$total_records}&nbsp;</span></td>
		 </tr>	";
 }	 
	
?>
</tbody>
</table>