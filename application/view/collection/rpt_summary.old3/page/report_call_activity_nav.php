<script>

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
function ShowAgentPerTL() {
	
	$("#content-deskoll").load(Ext.EventUrl(new Array('ReportCpaSummary','ShowAgentPerTL') ).Apply(), 
		{ TL : Ext.Cmp('user_tl').getValue()}, 
		function( response, status, xhr ) 
	{
		if( status == 'success' ){
			$("#user_agent").toogle();
		}		
	});
}

	
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */

function ShowReportType(){
  var report_type = Ext.Cmp('report_type').getValue();
  
 // -------------------------------- test ------------------------
  ShowAgentPerTL();
 
  if( report_type=='tracking-report') {
	  Ext.Cmp("report_group").disabled(0);
	  Ext.Cmp("user_tl").disabled(0);
	  Ext.Cmp("user_agent").disabled(0);
	  Ext.Cmp("report_mode").disabled(0);
		
 } else{
	  Ext.Cmp("report_group").disabled(1);
	  Ext.Cmp("user_tl").disabled(1);
	  Ext.Cmp("user_agent").disabled(1);
	  Ext.Cmp("report_mode").disabled(1);
	  Ext.Cmp("user_tl").setValue('');
	  Ext.Cmp("user_agent").setValue('');
	  Ext.Cmp("report_mode").setValue('summary');
 }
 
}

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 function ShowReport( Group )  
{
  var frmReport = Ext.Serialize('frmReport');
  if( !frmReport.Required( new Array('report_type','start_date','end_date','report_mode') ) ){
	Ext.Msg("Input Not Complete").Info();
	return false;
  }	 
 
  Ext.Window  ({
	url 	: Ext.EventUrl(new Array('ReportCpaSummary','ShowReport') ).Apply(),
	param   : Ext.Join([
		frmReport.getElement() 
	]).object()
  }).newtab();
  
}



// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
 function ShowExcel ()
{
  var frmReport = Ext.Serialize('frmReport');
  var VarReport = new Array();
  
  if( !frmReport.Required( new Array('report_type','start_date','end_date','report_mode') ) ){
	Ext.Msg("Input Not Complete").Info();
	return false;
  }	 
  
  VarReport['report_title'] = Ext.Cmp('report_type').getText();
  
  Ext.Window  ({
	url 	: Ext.EventUrl(new Array('ReportCpaSummary','ShowExcel') ).Apply(),
	param   : Ext.Join([
		frmReport.getElement() ,VarReport
	]).object()
  }).newtab();
 
}
 
 
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 $('document').ready(function()
{
	
var _offset = 22;
  $('.date').datepicker 
 ({
	showOn : 'button', 
	buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	changeYear : true,
	changeMonth : true,
	dateFormat : 'dd-mm-yy',
	readonly:true,
	onSelect	: function(date){
		console.log(date);
		
		// Ext.Cmp($(this).attr('id')).setValue('');
		// if(typeof(date) =='string'){
			// var x = date.split('-');
			// var retur = x[2]+"-"+x[1]+"-"+x[0];
			// if(new Date(retur) > new Date()) {
				// Ext.Cmp($(this).attr('id')).setValue('');
			// }
		// }
	}
});




 $("#user_tl").toogle();
 $("#user_agent").toogle();
});

</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Reporting</span></legend>
<div id="panel-agent-content">
	<table border=0 width='100%'>
		<tr>
			<td width='35%' valign="top"><?php $this->load->view('rpt_summary/page/report_call_activity_group');?></td>
			<td width='65%' valign="top"><?php $this->load->view('rpt_summary/page/report_call_activity_content');?></td>
		</tr>
	</table>
</div>
</fieldset>
