<?php  if(!_have_get_session('UserId')) exit('Expired Session ID ');
/*
 * @ modul 	 : view header label of the report navigation 
 * @ subject : AIA Insurance System 
 * @ author  : razaki team deplovment
 * -----------------------------------------------------------
 
 * @ param   : flter < string >
 * @ param   : all other request attribute data 
 *
 */

/*######################################### filter_by_atm_group_campaign ##################################################*/

if( _get_post('Filter') =='filter_by_atm_group_campaign')
{
	__("<div class=\"navigation\">");
	__("<ul>");
	__("<li class='first'> Report</li>");
	__("<li class='middle'>". $title ."</li>");
	__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
	__("<li class='lasted'>");
	__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
	__(_getDateIndonesia(_get_post('start_date')));	
	__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
	__("</li>");
	__("</ul>");	
	__("</div>");
	__("<p class='clear'></p>");
}	

/*######################################### filter_by_spv_group_campaign ##################################################*/

if( _get_post('Filter') =='filter_by_spv_group_campaign')
{
	$UI =& get_instance();
	if( $Users = array( 
		'atm' => $UI->M_SysUser->_get_detail_user(_get_post('AtmId')),
		'spv' => $UI->M_SysUser->_get_detail_user(_get_post('SpvId'))
		))
	{
		__("<div class=\"navigation\">");
		__("<ul>");
		__("<li class='first'> Report </li>");
		__("<li class='middle'>". $title ."</li>");
		__("<li class='middle'>". ucfirst($Users['spv']['full_name']) . " </li>");
		__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
		__("<li class='lasted'>");
		__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
		__(_getDateIndonesia(_get_post('start_date')));	
		__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
		__("</li>");
		__("</ul>");	
		__("</div>");
		__("<p class='clear'></p>");
	}	
}	

/*######################################### filter_by_agent_group_campaign ##################################################*/

if( _get_post('Filter') =='filter_by_agent_group_campaign')
{
	$UI =& get_instance();
	if( $Users = array( 
		'atm' => $UI->M_SysUser->_get_detail_user(_get_post('AtmId')),
		'spv' => $UI->M_SysUser->_get_detail_user(_get_post('SpvId')),
		'agent' => $UI->M_SysUser->_get_detail_user(_get_post('UserId'))
		
		
		))
	{
		__("<div class=\"navigation\">");
		__("<ul>");
		__("<li class='first'> Report </li>");
		__("<li class='middle'>". $title ."</li>");

		__("<li class='middle'>". ucfirst($Users['spv']['full_name']) . " </li>");
		__("<li class='middle'>". ucfirst($Users['agent']['full_name']) . " </li>");
		__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
		__("<li class='lasted'>");
		__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
		__(_getDateIndonesia(_get_post('start_date')));	
		__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
		__("</li>");
		__("</ul>");	
		__("</div>");
		__("<p class='clear'></p>");
	}	
}	


/*######################################### filter_by_campaign ##################################################*/

if( _get_post('Filter') =='filter_by_campaign')
{
	__("<div class=\"navigation\">");
	__("<ul>");
	__("<li class='first'> Report </li>");
	__("<li class='middle'>". $title ."</li>");
	__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
	__("<li class='lasted'>");
	__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
	__(_getDateIndonesia(_get_post('start_date')));	
	__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
	__("</li>");
	__("</ul>");	
	__("</div>");
	__("<p class='clear'></p>");
}	


/*######################################### filter_by_resource ##################################################*/

if( _get_post('Filter') =='filter_by_resource')
{
	__("<div class=\"navigation\">");
	__("<ul>");
	__("<li class='first'> Report </li>");
	__("<li class='middle'>". $title ."</li>");
	__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
	__("<li class='lasted'>");
	__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
	__(_getDateIndonesia(_get_post('start_date')));	
	__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
	__("</li>");
	__("</ul>");	
	__("</div>");
	__("<p class='clear'></p>");
}	


/*######################################### filter_by_atm ##################################################*/

if( _get_post('Filter') =='filter_by_atm')
{
	__("<div class=\"navigation\">");
	__("<ul>");
	__("<li class='first'> Report </li>");
	__("<li class='middle'>". $title ."</li>");
	__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
	__("<li class='lasted'>");
	__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
	__(_getDateIndonesia(_get_post('start_date')));	
	__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
	__("</li>");
	__("</ul>");	
	__("</div>");
	__("<p class='clear'></p>");
}	


/*######################################### filter_by_spv ##################################################*/

if( _get_post('Filter') =='filter_by_spv')
{
	__("<div class=\"navigation\">");
	__("<ul>");
	__("<li class='first'> Report </li>");
	__("<li class='middle'>". $title ."</li>");
	__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
	__("<li class='lasted'>");
	__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
	__(_getDateIndonesia(_get_post('start_date')));	
	__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
	__("</li>");
	__("</ul>");	
	__("</div>");
	__("<p class='clear'></p>");
}	



/*######################################### filter_by_agent ##################################################*/

if( _get_post('Filter') =='filter_by_agent')
{
	$UI =& get_instance();
	if( $Users = array( 
		'atm' => $UI->M_SysUser->_get_detail_user(_get_post('AtmId')),
		'spv' => $UI->M_SysUser->_get_detail_user(_get_post('SpvId'))
		))
	{
		__("<div class=\"navigation\">");
		__("<ul>");
		__("<li class='first'> Report </li>");
		__("<li class='middle'>". $title ."</li>");
		__("<li class='middle'>". ucfirst($Users['atm']['full_name']) ." </li>");
		__("<li class='middle'>". ucfirst($Users['spv']['full_name']) . " </li>");
		__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
		__("<li class='lasted'>");
		__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
		__(_getDateIndonesia(_get_post('start_date')));	
		__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
		__("</li>");
		__("</ul>");	
		__("</div>");
		__("<p class='clear'></p>");
	}	
}	

/*######################################### filter_campaign_group_agent ##################################################*/

if( _get_post('Filter') =='filter_campaign_group_agent')
{
$UI =& get_instance();
	if( $Users = array( 
		'atm' => $UI->M_SysUser->_get_detail_user(_get_post('AtmId')),
		'spv' => $UI->M_SysUser->_get_detail_user(_get_post('SpvId'))
		))
	{
		__("<div class=\"navigation\">");
		__("<ul>");
		__("<li class='first'> Report </li>");
		__("<li class='middle'>". $title ."</li>");
		__("<li class='middle'>". ucfirst($Users['spv']['full_name']) . " </li>");
		__("<li class='middle'>". ucfirst( _get_post('interval') ) . " </li>");
		__("<li class='lasted'>");
		__("<span style='font-size:10px;color:#dec6c1;'> from :&nbsp;</span>");
		__(_getDateIndonesia(_get_post('start_date')));	
		__("<span style='font-size:10px;color:#dec6c1;'>&nbsp;to&nbsp;</span>" . _getDateIndonesia( _get_post('end_date')) );
		__("</li>");
		__("</ul>");	
		__("</div>");
		__("<p class='clear'></p>");
	}	
}	