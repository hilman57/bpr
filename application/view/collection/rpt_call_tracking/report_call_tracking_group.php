<form name="frmReport">

<fieldset class="corner" style='margin-top:-10px;'>
<legend class="icon-menulist">&nbsp;&nbsp;Group Filter </legend>
<div>
	<table cellpadding='4' cellspacing=4>
		<tr>
			<td class="text_caption">Filter By </td>
			<td><?php __(form()->combo('Filter','select long', $group_filter_by, null, array("change" => "Ext.DOM.GroupFilterBy(this);") ));?></td>
		</tr>
		
		<tr>
			<td class="text_caption"><span id="label1">CampaignId<span></td>
			<td > <span id="Div1"><?php __(form()->combo('AgentId','select long',  array() ));?></span></td>
		</tr>
		
		<tr>
			<td class="text_caption"><span id="label2">SPV Name<span> </td>
			<td > <span id="Div2"><?php __(form()->combo('CampaignId','input_text box',  array() ));?></span></td>
		</tr>
		<tr>
			<td class="text_caption"><span id="label3">Caller Name<span> </td>
			<td > <span id="Div3"><?php __(form()->input('spvId','input_text box',null));?></span></td>
		</tr>
		<tr>
			<td class="text_caption"><span id="label4"><span> </td>
			<td > <span id="Div4"></span></td>
		</tr>
		
		<tr>
			<td class="text_caption">Interval </td>
			<td class=''>
				<?php __(form()->input('start_date','input_text box date'));?> &nbsp-
				<?php __(form()->input('end_date','input_text box date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption">Mode </td>
			<!-- td class=''><?php //__(form()->combo('interval','select long', $mode_call_center));?></td -->
			<td class=''><select id='interval' name='interval' class='select long'><option value="summary">Summary</option></select>
			</td>
		</tr>
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td class='bottom'>
				<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
				<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
			</td>
		</tr>
	</table>
</div>
</fieldset>
</form>