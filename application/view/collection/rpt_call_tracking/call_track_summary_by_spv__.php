<?php $CSS =& get_instance(); ?>
<div class='content grid' style='margin-right:2px'>
	<table class="grid" cellpadding="0" cellspacing="0" width="90%">
	 <tr>
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('first'));?>">SPV</td>
		   <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">Data Size</td> 		
		  <td align="center" colspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Record Data</td>
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">Call Attempt</td>
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">AVG Call / Record</td>
		  
		  <td align="center" colspan=<?php __(COUNT($STATUS_NOT_CONNECTION)+1);?>  
			class="<?php __($CSS->setHedaerStyles('middle'));?>">Connect</td>
		  
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">Connected Rate %</td>
		  
		  <td align="center" colspan=<?php __(COUNT($STATUS_NO_CONTACT)+1);?>  
			class="<?php __($CSS->setHedaerStyles('middle'));?>">Contact</td>
		  
		  <td align="center" rowspan=3 class="<?php __($CSS->setHedaerStyles('middle'));?>">Contact Rate %</td>
		  
		  <td align="center" colspan=<?php __(COUNT($STATUS_FOLLOW_UP));?>  
			class="<?php __($CSS->setHedaerStyles('middle'));?>">Follow Up</td>
		  
		  <td align="center" colspan=<?php __(COUNT($STATUS_INTERESTED)+COUNT($STATUS_NOT_INTERESTED));?> 
			class="<?php __($CSS->setHedaerStyles('middle'));?>">Interest</td>
		  
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">Response Rate %</td>
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">Conversion Rate %</td>
		  
		  <td align="center" colspan=<?php __(COUNT($STATUS_NOT_QUALIFIED));?>  
			class="<?php __($CSS->setHedaerStyles('middle'));?>">Not Qualifield</td>
		  
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">SALES</td>
		  <td align="center" rowspan=3  class="<?php __($CSS->setHedaerStyles('middle'));?>">TARP</td>
		  
	 </tr>
	 
	 <tr>
		  <td rowspan=2 class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">New</td>
		  <td rowspan=2 class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Utilized</td>
		  <td rowspan=2 class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Yes</td>
		  <td colspan=<?php __(COUNT($STATUS_NOT_CONNECTION));?> class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Not Connect</td>
		  <td rowspan=2 class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Yes</td>
		  <td colspan=<?php __(COUNT($STATUS_NO_CONTACT));?> class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Not Contact</td>
		  
		  <?php if( is_array($STATUS_FOLLOW_UP) )foreach($STATUS_FOLLOW_UP as $Primary => $Code )  : ?>
		  <td rowspan=2 class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center"><?php __($Code);?></td>
		  <?php endforeach ;?>
		  
		  <td colspan=<?php __(COUNT($STATUS_INTERESTED));?> class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Yes</td>
		  <td colspan=<?php __(COUNT($STATUS_NOT_INTERESTED));?> class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center">Not Interest</td>
		  
		  <?php if( is_array($STATUS_NOT_QUALIFIED) )foreach($STATUS_NOT_QUALIFIED as $Primary => $Code )  : ?>
		  <td rowspan=2 class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center"><?php __($Code);?></td>
		  <?php endforeach ;?>
		  
	 </tr>
	 
	<tr>
		  <?php if( is_array($STATUS_NOT_CONNECTION) )foreach($STATUS_NOT_CONNECTION as $Primary => $Code )  : ?>
		  <td class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center"><?php __($Code);?></td>
		  <?php endforeach ;?>
		  
		  <?php if( is_array($STATUS_NO_CONTACT) )foreach($STATUS_NO_CONTACT as $Primary => $Code )  : ?>
		  <td class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center"><?php __($Code);?></td>
		  <?php endforeach ;?>
		  
		  <?php if( is_array($STATUS_INTERESTED) )foreach($STATUS_INTERESTED as $Primary => $Code )  : ?>
		  <td class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center"><?php __($Code);?></td>
		  <?php endforeach ;?>
		  
		  <?php if( is_array($STATUS_NOT_INTERESTED) )foreach($STATUS_NOT_INTERESTED as $Primary => $Code )  : ?>
		  <td class="<?php __($CSS->setHedaerStyles('middle'));?>" align="center"><?php __($Code);?></td>
		  <?php endforeach ;?>
	 </tr>
<body>
<?php  
/* start default totals **/

 $totalsConnection 		= array();
 $totalsNotContactId 	= array();
 $totalsFollowUpId 		= array();
 $totalsInterestId 		= array();
 $totalsNotQualifiedId 	= array();
 $totalsDataSize 		= 0;
 $totalsUtilizeSize 	= 0;
 $totalsNotUtilizeSize 	= 0;
 $totalCallAtempt 		= 0;
 $totalsCallGroupConnect= 0;
 $totalsGroupContacted 	= 0;
 $totalConnectRate 		= 0;
 $totalContactRate 		= 0;
 $totalConversionRate 	= 0;
 $totalPolicySize 		= 0;
 $totalPremiSize 		= 0;
 
/* start show data by grid contents **/

if(is_array($CALL_CONTENT))
foreach( $CALL_CONTENT as $AtmId => $rows ) 
{
	
/* set default data  to zero ..**/

	$ResponseRate = 0;
	$ConversionRate =0;
	$ContactRate = 0;
	$ConnectRate = 0;
	$CallGroupConnect = 0;
	$AvgCallAtempt = 0;
	$GroupContacted = 0;
	

/** calculate record / atemp call **/

	if(is_array($STATUS_GROUP_CONTACTED)) 
	foreach( $STATUS_GROUP_CONTACTED as $key => $CallStatusId ) {
		$totalsGroupContacted += $rows[$CallStatusId];
		$GroupContacted+= $rows[$CallStatusId]; 
	}

/** calculate call group connect **/	

	if(is_array($STATUS_GROUP_CONNECTED)) 
	foreach( $STATUS_GROUP_CONNECTED as $key => $CallStatusId ) {
		$totalsCallGroupConnect+=$rows[$CallStatusId];
		$CallGroupConnect+= $rows[$CallStatusId]; 
	}
	
/** calculate connect rate / atemp call **/

	$AvgCallAtempt 	 = ( $rows['data_utilize'] ? round(($rows['size_atempt']/$rows['data_utilize']),2) : 0);
	$ConnectRate 	 = ( $CallGroupConnect ? round((($CallGroupConnect/$rows['data_utilize'])*100), 2) : 0);
	$ContactRate 	 = ( $GroupContacted ? round( (($GroupContacted/$rows['data_utilize'])*100), 2) : 0);
	$ConversionRate  = ( $rows['data_utilize'] ? round((($rows['size_policy']/$rows['data_utilize'])*100),2) : 0);
	$ResponseRate 	 = ( $rows['data_utilize'] ? round( (($GroupContacted/$rows['data_utilize']) * 100),2) : 0);
	
		
/** RECORD DATA ***/
	
	__("<tr>");
		__("<td class='" . $CSS->setContentStyles('first') . "' nowrap>&nbsp;". $rows['SpvName'] ."&nbsp;</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>". ( $rows['data_size'] ? $rows['data_size'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['data_not_utilize'] ? $rows['data_not_utilize'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['data_utilize'] ? $rows['data_utilize'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['size_atempt'] ? $rows['size_atempt'] :0 )  ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $AvgCallAtempt ? $AvgCallAtempt : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $CallGroupConnect ? $CallGroupConnect : 0 ) ."</td>");

	
  // STATUS_NOT_CONNECTION
   
	if(is_array($STATUS_NOT_CONNECTION)) 
		foreach( array_keys($STATUS_NOT_CONNECTION) as $keys => $NotConnectionId ) 
	{ 
		$totalsConnection[$NotConnectionId] +=(INT)$rows[$NotConnectionId];
		__("<td class='" . $CSS->setContentStyles('middle') . "'>".(isset($rows[$NotConnectionId])?$rows[$NotConnectionId]:'0') ."</td>");
	}
	
		__("<td class='" . $CSS->setContentStyles('middle') . "'>{$ConnectRate}</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>{$GroupContacted}</td>");
	
	
  // STATUS_NO_CONTACT
	
	if(is_array($STATUS_NO_CONTACT))
		foreach( array_keys($STATUS_NO_CONTACT) as $keys => $NotContactId ) 
	{
		$totalNotContactId[$NotContactId]+=(INT)$rows[$NotContactId]; 
		__("<td class='" . $CSS->setContentStyles('middle') . "'>".(isset($rows[$NotContactId])?$rows[$NotContactId]:'0') ."</td>");	
	}
	
		
		__("<td class='" . $CSS->setContentStyles('middle') . "'>{$ContactRate}</td>");
	
 // STATUS_FOLLOW_UP
	
	if(is_array($STATUS_FOLLOW_UP))
		foreach( array_keys($STATUS_FOLLOW_UP) as $keys => $FollowUpId )
	{
		$totalsFollowUpId[$FollowUpId] += $rows[$FollowUpId]; 
		__("<td class='" . $CSS->setContentStyles('middle') . "'>".(isset($rows[$FollowUpId])?$rows[$FollowUpId]:'0') ."</td>");	
	}
	
 // STATUS_INTERESTED

	if(is_array($STATUS_INTERESTED))
		foreach( array_keys($STATUS_INTERESTED) as $keys => $InterestId ) 
	{
		$totalsInterestId[$InterestId] += $rows[$InterestId]; 
		__("<td class='" . $CSS->setContentStyles('middle') . "'>".(isset($rows[$InterestId])?$rows[$InterestId]:'0') ."</td>");	
	} 
	
	
// STATUS_NOT_INTERESTED

	if(is_array($STATUS_NOT_INTERESTED))
		foreach( array_keys($STATUS_NOT_INTERESTED) as $keys => $NotInterestId )
	{
		$totalsNotInterestId[$NotInterestId] += $rows[$NotInterestId]; 
		__("<td class='" . $CSS->setContentStyles('middle') . "'>".(isset($rows[$NotInterestId])?$rows[$NotInterestId]:'0') ."</td>");	
	} 
	
		__("<td class='" . $CSS->setContentStyles('middle') . "'>{$ResponseRate}</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>{$ConversionRate}</td>");
	
	
  // STATUS_NOT_QUALIFIED
  
	if(is_array($STATUS_NOT_QUALIFIED))
		foreach( array_keys($STATUS_NOT_QUALIFIED) as $keys => $NotQualifiedId ) 
	{
		$totalsNotQualifiedId[$NotQualifiedId]+=$rows[$NotQualifiedId];
		__("<td class='" . $CSS->setContentStyles('middle') . "'>".(isset($rows[$NotQualifiedId])?$rows[$NotQualifiedId]:'0') ."</td>");	
	} 

		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ($rows['size_policy'] ? $rows['size_policy'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". _getCurrency($rows['size_premi']) ."</td>");
	__("</tr>");
	__("</tbody>");
	
	
// total calculation data 
	
	$totalsDataSize 		+= $rows['data_size'];
	$totalsUtilizeSize 		+= $rows['data_utilize'];
	$totalsNotUtilizeSize 	+= $rows['data_not_utilize'];
	$totalCallAtempt 		+= $rows['size_atempt'];
	$totalPolicySize 		+= $rows['size_policy'];
	$totalPremiSize 		+= $rows['size_premi'];
 
} 

// result data in foooter grid ---->
 
  $totalsAvgAtempt 		= ( $totalsUtilizeSize ? round(($totalCallAtempt/$totalsUtilizeSize ),2):0);
  $totalConnectRate		= ( $totalsCallGroupConnect ? round((($totalsCallGroupConnect/$totalsUtilizeSize)*100), 2) : 0);
  $totalContactRate 	= ( $totalsGroupContacted ? round( (($totalsGroupContacted/$totalsUtilizeSize)*100), 2) : 0);
  $totalConversionRate 	= ( $totalPolicySize ? round((($totalPolicySize/$totalsUtilizeSize)*100),2) : 0);
  $totalResponseRate  	= ( $totalsUtilizeSize ? round( (($totalsGroupContacted/$totalsUtilizeSize) * 100),2) : 0);
  
  __("<tr height='22'>");
		__("<td class='".$CSS->setBottomStyles('first')."'>&nbsp;<b>Total</b></td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>".$totalsDataSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalsNotUtilizeSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalsUtilizeSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalCallAtempt ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalsAvgAtempt ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalsCallGroupConnect ."</td>");
	
	if(is_array($STATUS_NOT_CONNECTION) )
	foreach( array_keys($STATUS_NOT_CONNECTION) as $keys => $totConectId )
	{
		__("<td class='".$CSS->setBottomStyles('middle')."'>".( $totalsConnection[$totConectId] ? $totalsConnection[$totConectId] : 0 ) ."</td>"); 	
	}	
	
		__("<td class='".$CSS->setBottomStyles('middle')."'>" . $totalConnectRate ."</td>"); 
		__("<td class='".$CSS->setBottomStyles('middle')."'>" . $totalsGroupContacted. "</td>"); 		
		
		
	if(is_array($STATUS_NO_CONTACT) )
	foreach( array_keys($STATUS_NO_CONTACT) as $keys => $totContactId )
	{
		__("<td class='".$CSS->setBottomStyles('middle')."'>".( $totalNotContactId[$totContactId] ? $totalNotContactId[$totContactId] : 0 ) ."</td>"); 	
	}	
		__("<td class='".$CSS->setBottomStyles('middle')."'>" . $totalContactRate . "</td>"); 
		
	if(is_array($STATUS_FOLLOW_UP) )
	foreach( array_keys($STATUS_FOLLOW_UP) as $keys => $totFollowId )
	{
		__("<td class='".$CSS->setBottomStyles('middle')."'>".( $totalsFollowUpId[$totFollowId] ? $totalsFollowUpId[$totFollowId] : 0 ) ."</td>"); 	
	}	
	
	if(is_array($STATUS_INTERESTED) )
	foreach( array_keys($STATUS_INTERESTED) as $keys => $totInterestId )
	{
		__("<td class='".$CSS->setBottomStyles('middle')."'>".( $totalsInterestId[$totInterestId] ? $totalsInterestId[$totInterestId] : 0 ) ."</td>"); 	
	}	
	
	if(is_array($STATUS_NOT_INTERESTED) )
	foreach( array_keys($STATUS_NOT_INTERESTED) as $keys => $totNotInterestId )
	{
		__("<td class='".$CSS->setBottomStyles('middle')."'>".( $totalsNotInterestId[$totNotInterestId] ? $totalsNotInterestId[$totNotInterestId] : 0 ) ."</td>"); 	
	}	
		
		__("<td class='".$CSS->setBottomStyles('middle')."'>" . $totalResponseRate . " </td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>" . $totalConversionRate . "</td>"); 
		
	if(is_array($STATUS_NOT_QUALIFIED))
	foreach( array_keys($STATUS_NOT_QUALIFIED) as $keys => $totalsNotQualifyId ) 
	{
		__("<td class='".$CSS->setBottomStyles('middle')."'>".( $totalsNotQualifiedId[$totalsNotQualifyId] ? $totalsNotQualifiedId[$totalsNotQualifyId] : 0 ) ."</td>");	
	} 	
	
	__("<td class='".$CSS->setBottomStyles('middle')."'>". ( $totalPolicySize ? $totalPolicySize : 0 ) ." </td>"); 
	__("<td class='".$CSS->setBottomStyles('middle')."'>". ( $totalPremiSize ?  _getCurrency($totalPremiSize): 0 ) ." </td>"); 	
__("</tr>"); 	
__("</table>");
__("</div>");
 