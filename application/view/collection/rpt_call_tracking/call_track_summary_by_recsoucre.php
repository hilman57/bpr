<?php $CSS =& get_instance(); ?>


<div class='content grid' style='margin-right:2px;width:600px;'>
	<table class="grid" cellpadding="2" cellspacing="0" border=0 width="90%">
	 <tr>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('first'));?>">Recsource</td>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('first'));?>">Rows Size</td>
		  <td align="center" rowspan=1 colspan=2 class="<?php __($CSS->setHedaerStyles('first'));?>">Info</td>
		  
		  <td align="center" rowspan=1 colspan="<?php echo count($CAMPAIGN_PROJECT); ?>" class="<?php __($CSS->setHedaerStyles('first'));?>">Campaign Name</td>
		  
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Data Size</td> 		
		  <td align="center" colspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Record Data</td>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">Call Attempt</td>	
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>">AVG Call / Record</td>
		  <?php foreach( $CATEGORY_STATUS_CALL as $key => $category ) :?>
		  <td align="center" colspan=<?php __($CATEGORY_COUNTER_CALL[$key]); ?>  class="<?php __($CSS->setHedaerStyles('middle'));?>"><?php __($category); ?></td>
		  <?php endforeach; ?>
		  <td align="center" rowspan=2  class="<?php __($CSS->setHedaerStyles('middle'));?>" nowrap>Sales Rate</td>
	 </tr>
	
	 <tr>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>">Success</td>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>">Failed</td>
		  <?php foreach( $CAMPAIGN_PROJECT as $CampaignId => $CampaignName ) : ?>
			 <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>"><?php __($CampaignName); ?></td>
		  <?php endforeach; ?>
		  
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>">New Data</td>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>">Utilize</td>
		  <?php foreach( $CATEGORY_STATUS_CALL as $key => $category ) :?>
		  <?php foreach( $CALL_REASON_CATEGORY[$key] as $CallReasonId => $CallReasonName ) : ?>
		  <td align="center" class="<?php __($CSS->setHedaerStyles('middle'));?>"><?php __($CallReasonName); ?></td>
		  <?php endforeach; ?>		
		  <?php endforeach; ?>
		  
	 </tr>	
<?php  

/* start default totals **/

$totSizeInterestId  	= 0;
$totSizeRows 			= 0;
$totSizeRowsSuccess 	= 0;
$totSizeRowsFailed 		= 0;
$totsDataSize 			= 0;
$totsUtilizeSize 		= 0;
$totsNotUtilizeSize 	= 0;
$totsCallAtemptSize 	= 0;

/* set start on array type **/

$totsAssignCmp 			= array();
$dataSizeReasonId 		= array();

if(is_array($CALL_CONTENT))
foreach( $CALL_CONTENT as $RecsourceId => $rows ) 
{
	
/** calculate connect rate / atemp call **/

  $AvgCallAtempt 	 = ( $rows['data_utilize'] ? round(($rows['size_atempt']/$rows['data_utilize']),2) : 0);
	
	__("<tr>");
		__("<td class='" . $CSS->setContentStyles('first') . "' nowrap>&nbsp;". $rows['RecsourceName'] ."&nbsp;</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>". $rows['RowsSize'] ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>". $rows['RowsSuccess'] ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>". $rows['RowsFailed'] ."</td>");
		
		  foreach( $CAMPAIGN_PROJECT as $CampaignId => $CampaignName ) :
			__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>".( $rows[$CampaignId] ? $rows[$CampaignId] : 0 )."</td>");
			$totsAssignCmp[$CampaignId]+= ( $rows[$CampaignId] ? $rows[$CampaignId]:0 );
			
		  endforeach; 
		  
		__("<td class='" . $CSS->setContentStyles('middle') . "' nowrap>". ( $rows['data_size'] ? $rows['data_size'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['data_not_utilize'] ? $rows['data_not_utilize'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['data_utilize'] ? $rows['data_utilize'] : 0 ) ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $rows['size_atempt'] ? $rows['size_atempt'] :0 )  ."</td>");
		__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $AvgCallAtempt ? $AvgCallAtempt : 0 ) ."</td>");
	
	 
     $dataSizeInterestId = array(); 	
	 foreach( $CATEGORY_STATUS_CALL as $key => $category ) 
	 {
		foreach( $CALL_REASON_CATEGORY[$key] as $CallReasonId => $CallReasonName ) 
		{
			if( in_array($CallReasonId, array_keys($STATUS_INTERESTED))) 
			{
				$dataSizeInterestId[$RecsourceId] +=$CALL_CONTENT[$RecsourceId][$CallReasonId];
			}	
			
			$dataSizeReasonId[$key][$CallReasonId] +=$CALL_CONTENT[$RecsourceId][$CallReasonId];
			
			__("<td class='" . $CSS->setContentStyles('middle') . "'>" . ( $CALL_CONTENT[$RecsourceId][$CallReasonId] ? $CALL_CONTENT[$RecsourceId][$CallReasonId] : 0 ) ."</td>");
	    } 
	 } 
	 
	$totsSalesRate  = round((( $dataSizeInterestId[$RecsourceId]/$rows['data_utilize']) * 100), 2); 
	__("<td class='" . $CSS->setContentStyles('middle') . "'>". ( $totsSalesRate ? $totsSalesRate : 0 ) ."</td>");
 
 
  // AKUMUlATIF 
  
    $totSizeRows		+= $rows['RowsSize'];
    $totSizeRowsSuccess	+= $rows['RowsSuccess'];
    $totSizeRowsFailed	+= $rows['RowsFailed'];
	$totsDataSize		+= $rows['data_size'];
	$totsUtilizeSize 	+= $rows['data_utilize'];
	$totsNotUtilizeSize += $rows['data_not_utilize'];
	$totsCallAtemptSize += $rows['size_atempt'];
	$totSizeInterestId  += $dataSizeInterestId[$RecsourceId];
	
} 

// result data in foooter grid ---->
 
	$totalsAvgAtempt 		= ( $totsUtilizeSize ? round(($totsCallAtemptSize/$totsUtilizeSize ),2):0);
	$totalsResponseRate     = round((($totSizeInterestId / $totsUtilizeSize) * 100), 2);  
  
  __("<tr height='22'>");
		__("<td class='".$CSS->setBottomStyles('first')."'>&nbsp;<b>Total</b></td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totSizeRows ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totSizeRowsSuccess ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totSizeRowsFailed."</td>");
		
		foreach( $CAMPAIGN_PROJECT as $CampaignId => $CampaignName ) :
		__("<td class='".$CSS->setBottomStyles('middle')."' nowrap>". $totsAssignCmp[$CampaignId]."</td>");
		endforeach; 
		  
		__("<td class='".$CSS->setBottomStyles('middle')."'>".$totsDataSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totsNotUtilizeSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totsUtilizeSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totsCallAtemptSize ."</td>");
		__("<td class='".$CSS->setBottomStyles('middle')."'>". $totalsAvgAtempt ."</td>");
	
	 foreach( $CATEGORY_STATUS_CALL as $key => $category ) 
	 {
		foreach( $CALL_REASON_CATEGORY[$key] as $CallReasonId => $CallReasonName ){
			__("<td class='" . $CSS->setBottomStyles('middle') . "'>" . ( $dataSizeReasonId[$key][$CallReasonId] ? $dataSizeReasonId[$key][$CallReasonId] : 0 ) ."</td>");
	    } 
	 } 
	 
	__("<td class='".$CSS->setBottomStyles('middle')."'>". ( $totalsResponseRate ?  $totalsResponseRate: 0 ) ." </td>"); 	
	
	__("</tr>"); 	
__("</table>");
__("</div>");
 