<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @ param : layout header call tracking for html view only 
 * @ param : under controller view only 
 *
 * -------------------------------------------------------------------------- 
  
 * @ param : razaki deplovment team 
 * @ param : -
 */

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Call Tracking :: <?php __($title);?> Layout</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style('corporate');?>/ui.all.css?time=<?php echo time();?>" />
	<?php $this -> load->view("rpt_call_tracking/report_calltrack_ccs_html");?>
</head>
<body>
<?php $this -> load->view("rpt_call_tracking/report_calltrack_label_html");?>