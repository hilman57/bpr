<?php if(!_have_get_session('UserId')) exit('Expired Session ID ');

__(javascript(array(array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()))));?>
<script>

// cek if loaded documnet call($this);
Ext.document(document).ready(function(){
 var _offset = 22;
	Ext.Cmp('Div2').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
	Ext.Cmp('Div3').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
	
// get title 
Ext.Cmp('legend_title').setText( Ext.System.view_file_name());

// date picker 

Ext.query('.date').datepicker ({
	showOn : 'button', 
	buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	dateFormat : 'dd-mm-yy',
	readonly:true,
	onSelect	: function(date){
		if(typeof(date) =='string'){
			var x = date.split('-');
			var retur = x[2]+"-"+x[1]+"-"+x[0];
			if(new Date(retur) > new Date()) {
				Ext.Cmp($(this).attr('id')).setValue('');
			}
		}
	}
});

// customize height fieldset 

Ext.Css('panel-main-content').style ({
	'height' : (Ext.query('#main_content').height() -(Ext.query('.extToolbars').height()+_offset)),
	'margin-top' : -5
});
 
 
  
// ShowAgentByAtm

Ext.DOM.ShowByAtm = function(CampaignId, type, Class, text)
{
	var filterBy = Ext.Cmp('Filter').getValue();
	Ext.Cmp('label2').setText(text);
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/CallTrackingReport/ShowByAtm/',
		param 	:  
		{ 
			CampaignId  : CampaignId, 
			type 		: type, 
			Class 		: Class,
			filterby 	: filterBy
		}
	}).load('Div2');
	
}


// ShowAgentByAtm

Ext.DOM.ShowAgentByAtm = function( AtmId, type, Class)
{
	var filterBy = Ext.Cmp('Filter').getValue();
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/CallTrackingReport/ShowAgentByAtm/',
		param 	:  
		{ 
			AtmId 		: AtmId, 
			type 		: type, 
			Class 		: Class,
			filterby 	: filterBy
		}
	}).load('Div2');
}

// ShowAgentByAtm

Ext.DOM.ShowSpvByAtm = function( AtmId, type , Class, div, label, text ) {
	
	Ext.Cmp(label).setText(text);
	var filterBy = Ext.Cmp('Filter').getValue();
	
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/CallTrackingReport/ShowSpvByAtm/',
		param 	:  
		{ 
			AtmId 		: AtmId, 
			type 	 	: type , 
			Class 	 	: Class,
			filterby 	: filterBy
		}
	}).load(div);
}
 
// ShowAgentBySpvId

Ext.DOM.ShowAgentBySpvId = function( SpvId, type , Class, div, label, text) {
	Ext.Cmp(label).setText(text);
	var filterBy = Ext.Cmp('Filter').getValue();
	Ext.Ajax 
	({
		url 	: Ext.DOM.INDEX +'/CallTrackingReport/ShowAgentBySpv/',
		param 	:  
		{ 
			SpvId 		: SpvId, 
			type 	 	: type , 
			Class 	 	: Class,
			filterby 	: filterBy
		}
	}).load(div);
	
	Ext.Cmp('label4').setText('');
	Ext.Cmp('Div4').setText('');
	
	
}
 
// Ext.DOM.ShowDataCampaignId

Ext.DOM.ShowDataCampaignId = function( UserId, type , Class, label ) 
{
 var filterBy = Ext.Cmp('Filter').getValue();
	Ext.Ajax({
		url 	: Ext.DOM.INDEX +'/CallTrackingReport/ShowDataCampaignId/',
		param 	:  
		{ 
			UserId 		: UserId, 
			type 	 	: type , 
			filterby 	: filterBy
		}
	}).load(Class);
	Ext.Cmp(label).setText("Campaign Name");
	Ext.Cmp('label3').setText("");
	Ext.Cmp('Div3').setText("");
	
}
 
 
// Ext.DOM.AgentGroupBy

Ext.DOM.GroupFilterBy= function( Group ){
var filter = [];
	if( Group.value!='' )
	{ 
		if ( Group.value=='filter_by_campaign' )
		{
			filter['filter_by_campaign'] = ["Campaign Name","showCampaign","listCombo","CampaignId","select long"];
			filter['filter_by_spv'] = ["SPV Name","showSPV", "combo","SpvId","select long"];
			filter['filter_by_agent'] = ["TSR Name","showAgent","listCombo","UserId","select long"];
			
			// alert('XXX');
	
			Ext.Cmp('label1').setText(filter[Group.value][0]);
			Ext.Ajax ({
				url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
				param 	: 
				{
					GroupId 	: Group.value,
					method 		: filter[Group.value][1],
					object		: filter[Group.value][2],
					properties 	: filter[Group.value][3],
					styles 		: filter[Group.value][4],
					
				}
			}).load('Div1');
			
			Ext.Cmp('label2').setText("");
			Ext.Cmp('label3').setText(""); 
			Ext.Cmp('label4').setText("");
			Ext.Cmp('Div2').setText("");
			Ext.Cmp('Div3').setText("");
			Ext.Cmp('Div4').setText("");
			
		}
		else if( Group.value=='filter_by_resource'){
			filter['filter_by_resource'] = ["Recsource","showRecsource","listCombo","RecsourceId","select long"];
			filter['filter_by_spv'] = ["SPV Name","showSPV", "combo","SpvId","select long"];
			filter['filter_by_agent'] = ["TSR Name","showAgent","listCombo","UserId","select long"];
			Ext.Cmp('label1').setText(filter[Group.value][0]);
			Ext.Ajax ({
				url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
				param 	: 
				{
					GroupId 	: Group.value,
					method 		: filter[Group.value][1],
					object		: filter[Group.value][2],
					properties 	: filter[Group.value][3],
					styles 		: filter[Group.value][4],
					
				}
			}).load('Div1');
			
			Ext.Cmp('label2').setText("");
			Ext.Cmp('label3').setText(""); 
			Ext.Cmp('label4').setText("");
			Ext.Cmp('Div2').setText("");
			Ext.Cmp('Div3').setText("");
			Ext.Cmp('Div4').setText("");
		}
		
		else if( Group.value=='filter_by_spv')
		{
			filter['filter_by_campaign'] = ["Campaign Name","showCampaign","listCombo","CampaignId","select long"];
			filter['filter_by_spv'] = ["SPV Name","showSPV", "listCombo","SpvId","select long"];
			filter['filter_by_agent'] = ["TSR Name","showAgent","combo","UserId","select long"];
	
			Ext.Cmp('label1').setText(filter['filter_by_spv'][0]);
			Ext.Ajax ({
				url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
				param 	: 
				{
					GroupId 	: Group.value,
					method 		: filter['filter_by_spv'][1],
					object		: filter['filter_by_spv'][2],
					properties 	: filter['filter_by_spv'][3],
					styles 		: filter['filter_by_spv'][4],
				}
			}).load('Div1');
			
			Ext.Cmp('label2').setText("");
			Ext.Cmp('label3').setText(""); 
			Ext.Cmp('label4').setText("");
			Ext.Cmp('Div2').setText("");
			Ext.Cmp('Div3').setText("");
			Ext.Cmp('Div4').setText("");
			
		}
		else if( Group.value=='filter_by_agent')
		{
			filter['filter_by_campaign'] = ["Campaign Name","showCampaign","listCombo","CampaignId","select long"];
			filter['filter_by_spv'] = ["SPV Name","showSPV", "combo","SpvId","select long"];
			filter['filter_by_agent'] = ["Caller Name","showAgent","combo","UserId","select long"];
			
			Ext.Cmp('label1').setText(filter['filter_by_spv'][0]);
			Ext.Cmp('label2').setText(filter['filter_by_agent'][0]);
			
			
			Ext.Ajax 
			({
				url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
				param 	:  {
					GroupId 	: Group.value,
					method 		: filter['filter_by_spv'][1],
					object		: filter['filter_by_spv'][2],
					properties 	: filter['filter_by_spv'][3],
					styles 		: filter['filter_by_spv'][4],
					
				}
			}).load('Div1');
			
			Ext.Cmp('label3').setText("");
			Ext.Cmp('label4').setText("");
			
			Ext.Cmp('Div2').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
			Ext.Cmp('Div3').setText("");
			Ext.Cmp('Div4').setText("");
		}	
	else if( Group.value=='filter_by_atm_group_campaign' )
	{
		filter['filter_by_campaign'] = ["Campaign Name","showCampaign","listCombo","CampaignId","select long"];
		filter['filter_by_atm'] = ["ATM Name","showATM", "combo","AtmId","select long"];
		filter['filter_by_spv'] = ["SPV Name","showSPV", "listCombo","SpvId","select long"];
		filter['filter_by_agent'] = ["TSR Name","showAgent","listCombo","UserId","select long"];
		
		Ext.Cmp('label1').setText(filter['filter_by_atm'][0]);
		Ext.Cmp('label2').setText(filter['filter_by_campaign'][0]);
		Ext.Cmp('label3').setText(filter['filter_by_spv'][0]);
		
		Ext.Ajax
		({
				url   : Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
				param : 
				{
					GroupId 	: Group.value,
					method 		: filter['filter_by_atm'][1],
					object		: filter['filter_by_atm'][2],
					properties 	: filter['filter_by_atm'][3],
					styles 		: filter['filter_by_atm'][4],
					
				}
			}).load('Div1');
			Ext.Cmp('Div2').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
			Ext.Cmp('Div3').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
			Ext.Cmp('label4').setText("");
			Ext.Cmp('Div4').setText("");
		
	}
	else if( Group.value=='filter_by_spv_group_campaign' )
	{
		filter['filter_by_campaign'] = ["Campaign Name","showCampaign","listCombo","CampaignId","select long"];
		filter['filter_by_spv'] = ["SPV Name","showSPV", "combo","SpvId","select long"];
		filter['filter_by_agent'] = ["TSR Name","showAgent","combo","UserId","select long"];
		
		Ext.Cmp('label1').setText(filter['filter_by_spv'][0]);
	
		Ext.Ajax ({
			url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
			param 	: 
			{
				GroupId 	: Group.value,
				method 		: filter['filter_by_spv'][1],
				object		: filter['filter_by_spv'][2],
				properties 	: filter['filter_by_spv'][3],
				styles 		: filter['filter_by_spv'][4],
			}
		}).load('Div1');
		
		
		Ext.Cmp('label2').setText("");	
		Ext.Cmp('label3').setText("");	
		Ext.Cmp('label4').setText("");
		Ext.Cmp('Div2').setText("");
		Ext.Cmp('Div3').setText("");
		Ext.Cmp('Div4').setText("");
	}
	else if((Group.value== 'filter_by_agent_group_campaign'))
	{
		filter['filter_by_campaign'] = ["Campaign Name","showCampaign","listCombo","CampaignId","select long"];
		filter['filter_by_spv'] = ["SPV Name","showSPV", "combo","SpvId","select long"];
		filter['filter_by_agent'] = ["Caller Name","showAgent","combo","UserId","select long"];
		
		//Ext.Cmp('label1').setText(filter['filter_by_atm'][0]);
		
		Ext.Cmp('label1').setText(filter['filter_by_spv'][0]);
		
		
		Ext.Ajax ({
			url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
			param 	: 
			{
				GroupId 	: Group.value,
				method 		: filter['filter_by_spv'][1],
				object		: filter['filter_by_spv'][2],
				properties 	: filter['filter_by_spv'][3],
				styles 		: filter['filter_by_spv'][4],
			}
		}).load('Div1');
			
		Ext.Cmp('label2').setText("");
		Ext.Cmp('label3').setText("");
		Ext.Cmp('Div2').setText("");
		Ext.Cmp('Div3').setText("");
		
	}
	else if((Group.value == 'filter_campaign_group_agent'))
	{
		
		filter['filter_by_campaign'] = ["Campaign Name","showCampaign","combo","CampaignId","select"];
		filter['filter_by_spv'] = ["SPV Name","showSPV", "listCombo","SpvId","select long"];
		filter['filter_by_agent'] = ["TSR Name","showAgent","combo","UserId","select long"];
		
		Ext.Cmp('label1').setText(filter['filter_by_campaign'][0]);
		
		
		Ext.Ajax ({
			url 	: Ext.DOM.INDEX +'/CallTrackingReport/GroupFilterBy/',
			param 	: 
			{
				GroupId 	: Group.value,
				method 		: filter['filter_by_campaign'][1],
				object		: filter['filter_by_campaign'][2],
				properties 	: filter['filter_by_campaign'][3],
				styles 		: filter['filter_by_campaign'][4],
			}
		}).load('Div1');
		
		Ext.Cmp('label2').setText('');
		Ext.Cmp('label3').setText('');
		Ext.Cmp('label4').setText('');
		Ext.Cmp('Div2').setText("");
		Ext.Cmp('Div3').setText("");
		Ext.Cmp('Div4').setText("");
		
		//Ext.Cmp('Div3').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
	}
	
 }
 else{
	Ext.Cmp('label1').setText('Campaign Name');
	Ext.Cmp('label2').setText('SPV Name');
	Ext.Cmp('label3').setText('Caller Name');
	Ext.Cmp('Div1').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
	Ext.Cmp('Div2').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
	Ext.Cmp('Div3').setText("<select class='input_text' disabled=true><option>--choose--</option></select>");
	
	
	
 }
  
}

// Ext.DOM.ShowReport 

Ext.DOM.ShowReport = function() {
	var param = [];
		param['mode'] = "HTML";
	 
	if( Ext.Cmp('Filter').getValue()=='filter_by_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();

	if( Ext.Cmp('Filter').getValue()=='filter_by_atm_group_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();
	
	
	if( Ext.Cmp('Filter').getValue()=='filter_by_resource' )
		param['RecsourceId'] = Ext.Cmp('RecsourceId').getValue();
	
	if( Ext.Cmp('Filter').getValue()=='filter_by_spv_group_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();
	
	
	if( Ext.Cmp('Filter').getValue()=='filter_by_agent_group_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();
	
	if( Ext.Cmp('Filter').getValue()=='filter_campaign_group_agent' )
		param['UserId'] = Ext.Cmp('UserId').getValue();
	
		
	if( Ext.Cmp('Filter').getValue()=='filter_by_atm' )
		param['AtmId'] = Ext.Cmp('AtmId').getChecked();
		
	if( Ext.Cmp('Filter').getValue()=='filter_by_spv' )
		param['SpvId'] = Ext.Cmp('SpvId').getChecked();

	if( Ext.Cmp('Filter').getValue()=='filter_by_agent'){
		param['AtmId'] = Ext.Cmp('AtmId').getValue();
		param['UserId'] = Ext.Cmp('UserId').getValue();
	}	
	if( Ext.Cmp('start_date').getValue()!=''){
		param['start_date'] = Ext.Cmp('start_date').getValue();
	}
	if( Ext.Cmp('end_date').getValue()!=''){
		param['end_date'] = Ext.Cmp('end_date').getValue();
	}

		
	Ext.Window({
		url : Ext.DOM.INDEX +"/CallTrackingReport/ShowReport/",
		param : Ext.Join([Ext.Serialize("frmReport").getSelect(), param ]).object()
	}).newtab();
}

Ext.DOM.ShowExcel = function() {
	var param = [];
		param['mode'] = "EXCEL";
	 
	if( Ext.Cmp('Filter').getValue()=='filter_by_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();

	if( Ext.Cmp('Filter').getValue()=='filter_by_atm_group_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();
	
	
	if( Ext.Cmp('Filter').getValue()=='filter_by_spv_group_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();
	
	
	if( Ext.Cmp('Filter').getValue()=='filter_by_agent_group_campaign' )
		param['CampaignId'] = Ext.Cmp('CampaignId').getValue();
	
	if( Ext.Cmp('Filter').getValue()=='filter_campaign_group_agent' )
		param['UserId'] = Ext.Cmp('UserId').getValue();
	
		
	if( Ext.Cmp('Filter').getValue()=='filter_by_atm' )
		param['AtmId'] = Ext.Cmp('AtmId').getChecked();
		
	if( Ext.Cmp('Filter').getValue()=='filter_by_spv' )
		param['SpvId'] = Ext.Cmp('SpvId').getChecked();

	if( Ext.Cmp('Filter').getValue()=='filter_by_agent'){
		param['AtmId'] = Ext.Cmp('AtmId').getValue();
		param['UserId'] = Ext.Cmp('UserId').getValue();
	}	
	
	if( Ext.Cmp('start_date').getValue()!=''){
		param['start_date'] = Ext.Cmp('start_date').getValue();
	}
	
	if( Ext.Cmp('end_date').getValue()!=''){
		param['end_date'] = Ext.Cmp('end_date').getValue();
	}

		
	Ext.Window({
		url : Ext.DOM.INDEX +"/CallTrackingReport/ShowExcel/",
		param : Ext.Join([Ext.Serialize("frmReport").getSelect(), param ]).object()
	}).newtab();
}
// Ext.DOM.CmpByGroup

Ext.DOM.CmpByGroup= function( Group ){
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/CallTrackingReport/getCampaignByGroup/',
		param 	: {
			GroupId : Group.value
		}
	}).load('DivCmp');
}
	
	
});

</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Order Report</span></legend>
<div id="panel-main-content">
	<table border=0 width='100%'>
		<tr>
			<td width='40%' valign="top"><?php $this -> load-> view('rpt_call_tracking/report_call_tracking_group');?></td>
			<td width='60%' valign="top"><?php $this -> load-> view('rpt_call_tracking/report_call_tracking_content');?></td>
		</tr>
	</table>
</div>
</fieldset>
