<div>
	<table cellspacing=2 cellpadding=2 >
		
		<tr>
			<td class="text_caption bottom"> * File Name </td>
			<td class="bottom"><?php __(form()->upload('file_fax')); ?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> * Destination </td>
			<td class="bottom"><?php __(form()->input('destination','input_text long')); ?></td>
		</tr>
		
		<tr height=28>
			<td class="text_caption bottom" colspan="2"><span id="loading-title-ajax"></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td>
				<?php __(form()->button('file_fax_submit','button upload','Upload', array('click'=>'Ext.DOM.UploadFax();') )); ?> 
				<?php __(form()->button('file_fax', 'button close','Close',array('click'=>'Ext.DOM.CloseFax();'))); ?>
			</td>
		</tr>
	</table>
</div>
