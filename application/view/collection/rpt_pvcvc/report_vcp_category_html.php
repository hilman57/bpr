<?php

$header = array(
	'no'				=> 'No',
	'category'			=> array('VCReasonDesc' => 'Category', 'VCReasonCode' => 'Code CTA'),
	'Closing Inventory'	=> array('Account' => '# Account', 'Balance' => 'Balance'),
	'Non Delinquent'	=> array('Account' => '# Account', 'Balance' => 'Balance'),
	'Pre Chargeoff'		=> array('Account' => '# Account', 'Balance' => 'Balance'),
	'Chargeoff'			=> array('Account' => '# Account', 'Balance' => 'Balance'),
	'No Attempt > 1mth'	=> array('Account' => '# Account', 'Balance' => 'Balance')
);

$numberingformat = array('Balance');

// echo "<pre>";
// print_r($vccategory);
// echo "</pre>";

?>
<!DOCTYPE HTML>
<html>
<head>
<title> <?php echo ucfirst(base_layout());?> :: <?php echo $title; ?> </title>
</head>
<body style="margin:0px;">
<?php 
	echo "<H2> ".$title." </>";
	echo "<br>";
	echo '<table width="80%" style="margin:4px;border-collapse:collapse;border:1px solid #FFCCCC;">';
	echo '<tr>';
	foreach($header as $column => $name)
	{
		if($column=="no"){
			echo '<th style="font-family:Arial;font-size:12px;padding:8px;border:1px solid #FFCCCC;background-color:#eee;" colspan="1" rowspan="2">' . $column .' </th>';
		}else{
			echo '<th style="font-family:Arial;font-size:12px;padding:8px;border:1px solid #FFCCCC;background-color:#eee;" colspan="2">' . $column .' </th>';
		}
	}
	echo '</tr>';
	echo '<tr>';
	foreach($header as $header1 => $header2)
	{
		foreach($header2 as $column => $name){
			if($column!="no"){
				echo '<th style="font-family:Arial;font-size:12px;padding:8px;border:1px solid #FFCCCC;background-color:#eee;">' . $column .' </th>';
			}
		}
	}
	echo '</tr>';
	
	$no=1;
	// echo '<hr />';
	// print_r($vccategory);
	foreach($vccategory as $Id => $Cat){
		echo '<tr>';
		foreach($header as $header1 => $header2){
			if($header1=="no"){
				echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'.$no. '</td>';
				$no++;
			}
			foreach($header2 as $column => $nama){
				if($column=="VCReasonDesc"){
					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'.$Cat['VCReasonDesc']. '</td>';
				}
				else if($column=="VCReasonCode"){
					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'.$Cat['VCReasonCode']. '</td>';
				}
				else{
					$number = 0;
					if(in_array($column, $numberingformat)){
						$number = number_format($data_tabel[$Id][$header1][$column],2,",",".");
					}else{
						$number = ($data_tabel[$Id][$header1][$column]?$data_tabel[$Id][$header1][$column]:0);
					}
					
					echo '<td style="font-family:Arial;font-size:11px;padding:6px;border:1px solid #FFCCCC;background-color:#fffccc;">'. $number . '</td>';
				}
			}
		}
		echo '</tr>';
	}
	echo '</table>';
	// echo "<pre>";
	// print_r($data_tabel);
	// echo "</pre>";
?>
</body>
</html>	