<?php 
if(!_have_get_session('UserId')) exit('Expired Session ID ');
__(javascript(array(array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()))));

$UserId = 0;

if(_get_session('HandlingType') == USER_SUPERVISOR)
{
	
}
?>

<script>
	var _offset = 22;
	
	Ext.DOM.LoadAgentBySpv = function()
	{
		Ext.Ajax  
		({
			url    : Ext.DOM.INDEX+"/RptUpload/LoadAgentBySpv/",
			method : 'POST',
			param  : {
				supervisor : Ext.Cmp('SpvId').getValue()
			}
		}).load('Div4');
	}
	
	Ext.document(document).ready(function()
	{
		Ext.Cmp('legend_title').setText( Ext.System.view_file_name());

		// date picker 

		Ext.query('.date').datepicker ({
			showOn : 'button', 
			buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
			buttonImageOnly	: true, 
			dateFormat : 'dd-mm-yy',
			readonly:true,
			onSelect	: function(date){
				if(typeof(date) =='string'){
					var x = date.split('-');
					var retur = x[2]+"-"+x[1]+"-"+x[0];
					if(new Date(retur) > new Date()) {
						Ext.Cmp($(this).attr('id')).setValue('');
					}
				}
			}
		});
		
		Ext.Cmp('SpvId').listener
		({
			'onChange' : function(e){
				Ext.Util(e).proc(function(obj){
					Ext.DOM.LoadAgentBySpv();
				});
			}
		});
		
		Ext.DOM.LoadAgentBySpv();
	});
	
//Ext.DOM.ShowReport
	
Ext.DOM.ShowReport = function() {
  var param = []; 	
  var TITLE = "Distributed Report :: from "+ Ext.Cmp('start_distribute').getValue()+" to "+
			   Ext.Cmp('end_distribute').getValue();
  
  param['mode'] = 'html';
  Ext.Dialog('panelx', {
		ID		: 'panelx1',
		type	: 'ajax/html',
		title	: TITLE,
		url 	: Ext.DOM.INDEX +'/RptUpload/ShowReport/',
		param 	: Ext.Join([ Ext.Serialize("frmReport").getReportElement(), param]).object(),
		style 	: {
			width  		: $(window).width(),
			height 		: $('#main_content').height(),
			scrolling 	: 1, 
			resize 		: 0,
			close		: 1,
			minimize	: 1,
			top			: 50,
			left		: 2,
			right		: 2
		}	
	}).open();
}

// Ext.DOM.ShowExcel

Ext.DOM.ShowExcel = function() 
{
  var param = [];
	  param['mode'] = 'excel';
	  
	Ext.Window
	({
		url 	: Ext.DOM.INDEX +'/RptUpload/ShowExcel/',
			param 	: Ext.Join([ Ext.Serialize("frmReport").getReportElement(), param ]).object(),
			style 	: {
				width  		: (Ext.query('#panel-call-center').width()),
				height 		: (Ext.query('#panel-main-content').height() -((_offset)+40)),
				scrolling 	: 1, 
				resize 		: 1
			}
		}).newtab();
	}
</script>


<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Upload Report</span></legend>
<div id="panel-main-content">


	<table border=0 width='100%'>
		<tr>
			<td width='40%' valign="top">
				<form name="frmReport">
					<fieldset class="corner" style='margin-top:-10px;'>
						<legend class="icon-menulist">&nbsp;&nbsp;Group Filter </legend>
						<div>
							<table cellpadding='4' cellspacing=4>
								<tr>
									<td class="text_caption bottom"><span id="label1">CampaignId<span></td>
									<td > <span id="Div1"><?php __(form()->listcombo('CampaignId',null,$Campaign,null,null));?></span></td>
								</tr>
								<tr>
									<td class="text_caption bottom"><span id="label3">Spv Name<span> </td>
									<td > <span id="Div3"><?php __(form()->combo('SpvId','select long',$SpvId,(_get_session('HandlingType') == USER_SUPERVISOR?_get_session('UserId'):null),null, (_get_session('HandlingType') == USER_SUPERVISOR?array('disabled'=>true):array()) ));?></span></td>
								</tr>
								<tr>
									<td class="text_caption"><span id="label4">Caller Name<span> </td>
									<td > <span id="Div4"><?php __(form()->listcombo('AgentId',null,array(),null,null));?></span></td>
								</tr>
								<!--
								<tr>
									<td class="text_caption bottom">Interval Upload</td>
									<td class='bottom'>
										<X?php __(form()->input('start_upload','input_text box date'));?> &nbsp-
										<X?php __(form()->input('end_upload','input_text box date'));?>
									</td>
								</tr>-->
								<tr>
									<td class="text_caption bottom">Interval Distributed</td>
									<td class='bottom'>
										<?php __(form()->input('start_distribute','input_text box date'));?> &nbsp-
										<?php __(form()->input('end_distribute','input_text box date'));?>
									</td>
								</tr>
								<tr>
									<td class="text_caption"> &nbsp;</td>
									<td class='bottom'>
										<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
										<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
									</td>
								</tr>
							</table>
						</div>
					</fieldset>
				</form>
			</td>
			<td width='60%' valign="top">&nbsp;</td>
		</tr>
	</table>
</div>
</fieldset>