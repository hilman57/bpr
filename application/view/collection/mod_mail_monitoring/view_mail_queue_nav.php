<?php echo javascript(); ?>
<script type="text/javascript">
	Ext.DOM.FaxQueueAgent = function()
	{
		Ext.Ajax
		({
			url : Ext.DOM.INDEX +'/MailMonitoring/Content/',
			param : {}	
		}).load("list_content_faxqueue");
	} 
	
	Ext.document('document').ready( function(){
		Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
		Ext.DOM.setTimeOutId = window.setInterval(function(){
			Ext.DOM.FaxQueueAgent();
		},1000);
	
	});
	
	Ext.DOM.Try = function(object){
		Ext.Ajax ({
			url : Ext.DOM.INDEX +'/MailMonitoring/Retry/',
			method :'POST',
			param :{
				QueueId : object.id
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(items){
					if( items.success ){
						Ext.Msg("Retry send mail").Success();
					}
					else{
						Ext.Msg("Retry send mail").Failed();
					}
				});
			}
		}).post();	
	}
	
	Ext.DOM.Cancel = function(object){
		Ext.Ajax ({
			url : Ext.DOM.INDEX +'/MailMonitoring/Cancel/',
			method :'POST',
			param :{
				QueueId : object.id
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(items){
					if( items.success ){
						Ext.Msg("Cancel send mail").Success();
					}
					else{
						Ext.Msg("Cancel send mail").Failed();
					}
				});
			}
		}).post();	
	}
</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
	<div id="content-activity">
		<table width="100%" class="custom-grid" cellspacing=1>
			<thead>	
				<tr height='24'> 
					<th nowrap width="25"  class="font-standars ui-corner-top ui-state-default first center">&nbsp;No.</th>
					<th nowrap width="100" class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Subject</th>
					<th nowrap width="100" class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Status</th>	
					<th nowrap width="100" class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Reason</th>	
					<th nowrap width="180" class="font-standars ui-corner-top ui-state-default middle  center">&nbsp;Start date</th>	
					<th nowrap width="180" class="font-standars ui-corner-top ui-state-default middle center">&nbsp;End date</th>
					<th nowrap width="180" class="font-standars ui-corner-top ui-state-default middle center">&nbsp;Wait Time</th>
					<th nowrap width="100" class="font-standars ui-corner-top ui-state-default lasted center">&nbsp;#Retry </th>
				</tr>
			</thead>
			<tbody id="list_content_faxqueue">&nbsp;</tbody>
		</table>

	
	</div>
</fieldset>