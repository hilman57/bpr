<!--<script src="http://code.jquery.com/jquery-1.0.1.js" integrity="sha256-7FEp9UKEmqd37mBUMmo5uStN/zA2wgVf+/XwJJplFeM=" crossorigin="anonymous"></script>-->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<form name="frmReport" method="GET">
	<script>	
	Ext.DOM.ShowReport	= function(){
		var start_date	= Ext.Cmp('start_date').getValue();
		var end_date	= Ext.Cmp('end_date').getValue();
		var type        = Ext.Cmp("type").getValue();
		// var user_agent	= Ext.Cmp("user_agent").getValue();
		// var mode        = Ext.Cmp("mode").getValue();
		
		if(start_date == "" || end_date == "" || type == "" || user_agent == ""){
			// if(start_date == "" || end_date == "" || type == "" || type =="0") {
				swal("Oops",'Form is not complete!',"error");
				// return false;
			// } else if( type == 'daily' && mode =='') {
				// swal("Oops","Select mode ,please","error");
				// return false;
		}
		else {
			Ext.Window
			({
				url 	: Ext.DOM.INDEX +'/RptDialerDetail/ShowReport',
				param 	: {
					type : Ext.Cmp("type").getValue(),
					// mode :  Ext.Cmp("mode").getValue(),
					user_agent :  Ext.Cmp("user_agent").getValue(),
					start_date : Ext.Cmp('start_date').getValue(),
					end_date : Ext.Cmp('end_date').getValue()
				}
			}).newtab();	
		}
	}
	
	Ext.DOM.ShowExcel = function()
	{
		var start_date	= Ext.Cmp('start_date').getValue();
		var end_date	= Ext.Cmp('end_date').getValue();
		// var mode        = Ext.Cmp("mode").getValue();
		var type        = Ext.Cmp("type").getValue();
		
		if(start_date == "" || end_date == ""){
		// if(start_date == "" || end_date == "" || mode == "" || mode == "0") {
			// alert('Set Interval, Please!');
			swal("Oops",'Form is not complete!',"error");
			// return false;
		} else {
			Ext.Window
			({
				url 	: Ext.DOM.INDEX +'/RptDialerDetail/ShowExcel',
				param 	: {
					type : Ext.Cmp("type").getValue(),
					// mode :  Ext.Cmp("mode").getValue(),
					start_date : Ext.Cmp('start_date').getValue(),
					user_agent :  Ext.Cmp("user_agent").getValue(),
					end_date : Ext.Cmp('end_date').getValue()
				}
			}).newtab();	
		}
	}
	
	Ext.query('.date').datepicker({
		showOn : 'button', 
		buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly	: true, 
		dateFormat : 'yy-mm-dd',
		readonly:true,
		onSelect	: function(date){
			if(typeof(date) =='string'){
				var x = date.split('-');
				var retur = x[2]+"-"+x[1]+"-"+x[0];
				if(new Date(retur) > new Date()) {
					Ext.Cmp($(this).attr('id')).setValue('');
				}
			}
		}
	});

	$('#type').change(function() {
        if ($(this).val() === 'daily') {
            $("#modes").show();
        } else {
        	$("#modes").hide();
        }
    });
	
	function ShowAgentPerTL() {
		$("#content-deskoll").load(Ext.EventUrl(new Array('ReportCpaSummary','ShowAgentPerTL') ).Apply(), 
			{ TL : Ext.Cmp('user_tl').getValue()}, 
			function( response, status, xhr ) 
		{
			if( status == 'success' ){
				$("#user_agent").toogle();
			}		
		});
	}
	
	$('document').ready(function(){
		var _offset = 22;
		  $('.date').datepicker 
		 ({
			showOn : 'button', 
			buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
			buttonImageOnly	: true, 
			changeYear : true,
			changeMonth : true,
			dateFormat : 'dd-mm-yy',
			readonly:true
		});

		 $("#user_tl").toogle();
		 $("#user_agent").toogle();
		 $("#product").toogle();
	});

</script>
	<fieldset class="corner" style='margin-top:0px;'>
	<legend class="icon-menulist">&nbsp;&nbsp; REPORT DIALER DETAIL </legend>
		<div>
			<table cellpadding='4' cellspacing=4>

				<tr id="daily">
					<td class="text_caption"> Interval </td>
					<td class='bottom'>
						<input type="text" id="start_date" name="start_date" class="input_text box date"> &nbsp-
						<input type="text" id="end_date" name="end_date" class="input_text box date">
					</td>
				</tr>
				<tr>
					<td class="text_caption bottom">Team Leader </td>
					<td class='bottom' id="content-tl"><?php echo form()->combo('user_tl','select auto', Teamleader(), _get_session('UserId'), array('change' =>'ShowAgentPerTL();'));?></td>
				</tr>
				<tr>
					<td class="text_caption bottom">Deskoll</td>
					<td class='bottom' id="content-deskoll"><?php echo form()->combo('user_agent','select auto',Agent());?></td>
				</tr>
				<tr>
					<td class="text_caption bottom"> Type </td>
					<td class='bottom'>
						<select name="type" id="type">
							<option value="0"> -- choose -- </option>
							<option value="daily"> Daily </option>
							<!-- o ption value="weekly"> Weekly </optio n -->
							<option value="monthly"> Monthly </option>
						</select>
					</td>
				</tr>

				<!-- <tr style="display: none;" id="modes">
					<td class="text_caption bottom"> mode </td>
					<td class='bottom'>
						<select name="mode" id="mode">
							<option value=""> -- choose -- </option>
							<option value="detail"> Detail </option>
							<option value="summary"> Summary </option>
						</select>
					</td>
				</tr> -->

				<!-- <p>Info : <br/>Untuk penarikan report incoming calls sebaiknya <br/>dilakukan dengan parameter interval 1 bulan jika <br/>
					lebih dari 1 bulan penarikan <br/>lebih baik setelah last call <br/>
					karena akan mengakibatkan server database <strong>overload</strong>
				</p> -->

				<tr>
					<td class="text_caption"> &nbsp;</td>
					<td class='bottom'>
						<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
						<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
					</td>
				</tr>
			</table>
		</div>
	</fieldset>
</form>