<!DOCTYPE html>
<html>
<head>
	<style>
		html {
			font-family: Trebuchet MS,Arial,sans-serif;
			font-size: 12px;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td {
			padding: 5px;
			position: center;
		}
		#color {
			background-color : #00FFFF;
		}
	</style>
	<title>Report Incoming Daily Detail</title>
</head>
<body>
	<?php 
		function convertToHoursMins($time, $format = '%02d:%02d') {
		    if ($time < 1) {
		        return;
		    }
		    $hours = floor($time / 60);
		    $minutes = ($time % 60);
		    return sprintf($format, $hours, $minutes);
		}

		function toDuration($seconds){
	        $sec = 0;
	        $min = 0;
	        $hour= 0;
	        $sec = $seconds%60;
	        $seconds = floor($seconds/60);
	        if ($seconds){
	                $min  = $seconds%60;
	                $hour = floor($seconds/60);
	        }

	        if($seconds == 0 && $sec == 0)
	            return sprintf("");
	        else
	            return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
		}
	?>
	<h1>REPORT DIALER DETAIL</h1>
	<table>
		<tr>
			<th id="color">Metrics</th>
			<th id="color">Strategy volume</th>
			<th id="color">Right Party Contact</th>
			<th id="color">Promise To Pay</th>
			<th id="color">Adjusted operation hour</th>
			<th id="color">Dials completed</th>
			<th id="color">Compliance Attempt</th>
			<th id="color">Abandons</th>
			<th id="color">Uniqe report dial completed</th>
			<th id="color">connect</th>
			<th id="color">Uniqe report dial completed %</th>
			<th id="color">overall penetration</th>
			<th id="color">predictive presentation</th>
			<th id="color">RPC % Connect to collector</th>
			<th id="color">RPC % Strategy Volume</th>
			<!-- th id="color">PTP Rate</th -->
		</tr>
	<?php
		foreach( $detail_daily as $key => $val ) {
			if($key){
				echo "<tr>";
				echo "<td>".$key."</td>"; 
				echo "<td>".$val['strategy_volume']."</td>"; 
				echo "<td>".$val['right_party_contact']."</td>"; 
				echo "<td>".$val['promise_to_pay']."</td>"; 
				echo "<td>".round($val['adjusted_operation_hour']/3600,2)."</td>"; 
				echo "<td>".$val['dials_completed']."</td>"; 
				echo "<td>".$val['compliance_attempt']."</td>"; 
				echo "<td>".$val['abandons']."</td>"; 
				echo "<td>".$val['uniqe_report_dial_completed']."</td>"; 
				echo "<td>".$val['connect']."</td>"; 
				echo "<td>".round(($val['uniqe_report_dial_completed']/$val['strategy_volume'])*100,2)."</td>"; 
				echo "<td>".round(($val['compliance_attempt']/$val['strategy_volume'])*100,2)."</td>"; 
				echo "<td>".round(($val['compliance_attempt']/$val['strategy_volume'])*100,2)."</td>"; 
				echo "<td>".round(($val['right_party_contact']/$val['connect'])*100,2)."</td>"; 
				echo "<td>".round(($val['right_party_contact']/$val['strategy_volume'])*100,2)."</td>"; 
				echo "</tr>";
			}
    	}
    ?>
	</table> <br><br><br>
	<!-- <footer>
		&copy;DIDIGANTENG
	</footer> -->
</body>
</html>