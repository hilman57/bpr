
<table width="100%" class="custom-grid" cellspacing="0">
<thead>
	<tr height="20"> 
		<th nowrap class="custom-grid th-first center" width="5%">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('prefix_id').setChecked();">#</a></th>	
		<th nowrap class="custom-grid th-middle">&nbsp;No</th>	
		<th nowrap class="custom-grid th-middle" align="left">&nbsp;<span class="header_order" id ="b.ProductCode" onclick="extendsJQuery.orderBy(this.id);">Product Code </th>        
        <th nowrap class="custom-grid th-middle" align="left">&nbsp;Product Name</th>
		<th nowrap class="custom-grid th-middle" align="left">&nbsp;Prefix</th>
		<th nowrap class="custom-grid th-middle" align="left">&nbsp;Max Length</th>
		<th nowrap class="custom-grid th-middle" align="left">&nbsp;Method</th>
		<th nowrap class="custom-grid th-middle" align="left">&nbsp;Form Input</th>
		<th nowrap class="custom-grid th-middle" align="left">&nbsp;Form Edit</th>
		<th nowrap class="custom-grid th-lasted" align="left">&nbsp;Prefix Status</th>
	</tr>
</thead>	
<tbody>
<?php
$no  = $num;
foreach( $page -> result_assoc() as $rows )
{ 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>
	<tr CLASS="onselect" bgcolor="  <?php echo $color;?>">
		<td class="content-first">  <?php echo form() -> checkbox('prefix_id',null, $rows['PrefixNumberId']); ?></td>
		<td class="content-middle"> <?php echo $no; ?></td>
		<td class="content-middle"> <?php echo $rows['ProductCode'];?></td>
		<td class="content-middle"> <?php echo $rows['ProductName'];?></td>
		<td class="content-middle"> <?php echo $rows['PrefixChar'];?></td>
		<td class="content-middle"> <?php echo $rows['PrefixLength'];?></td>
		<td class="content-middle"> <b><?php echo $rows['PrefixMethod'];?></b></td>
		<td class="content-middle"> <?php echo $rows['AddView'];?></td>
		<td class="content-middle"> <?php echo $rows['EditView'];?></td>
		<td class="content-lasted"> <?php echo $rows['Status'];?></td>
	</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>