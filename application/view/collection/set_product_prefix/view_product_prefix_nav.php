<?php echo javascript(); ?>
<script type="text/javascript">

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 


/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.datas = 
{
	keywords : '<?php echo _get_post('keywords');?>',
	order_by : '<?php echo _get_post('order_by');?>',
	type	 : '<?php echo _get_post('type'); ?>'	
}
 

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.Disable = function()
{
	var PrefixId = Ext.Cmp("prefix_id").getValue();
	Ext.Ajax({
		url : Ext.DOM.INDEX +'/SetPrefix/SetActive/',
		method : "GET",
		param : {
			active : 0,
			PrefixId : PrefixId,
		},
		ERROR : function(fn){
			try{
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Disable Prefix Number ").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Disable Prefix Number ").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Info();
			}
		}
	}).post();
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.Enable = function()
{
	var PrefixId = Ext.Cmp("prefix_id").getValue();
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/SetPrefix/SetActive/',
		method : "GET",
		param : {
			active : 1,
			PrefixId : PrefixId,
		},
		ERROR : function(fn){
			try{
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Enable Prefix Number ").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Enable Prefix Number ").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Info();
			}
		}
	}).post();
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.DeletePrefix = function()
{
	var PrefixId = Ext.Cmp("prefix_id").getValue();
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/SetPrefix/Delete/',
		method 	: "POST",
		param 	: {
			PrefixId : PrefixId,
		},
		ERROR : function(fn){
			Ext.Util(fn).proc(function(Delete){
				if( Delete.success ){
					Ext.Msg("Delete Prefix Number ").Success(); 
					Ext.EQuery.postContent(); 
				}
				else
				{
					Ext.Msg("Delete Prefix Number ").Failed(); }
			});
		}
	}).post();
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
 Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
 Ext.DOM.navigation = {
	custnav : Ext.DOM.INDEX+'/SetPrefix/index', 
	custlist : Ext.DOM.INDEX+'/SetPrefix/content', 
 }
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.EQuery.construct( Ext.DOM.navigation, Ext.DOM.datas )
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 		
Ext.DOM.searchResult = function()
{
	Ext.EQuery.construct( Ext.DOM.navigation, {
		keywords : Ext.Cmp('v_product_prefix').getValue()
	});
	Ext.EQuery.postContent();
}


Ext.document('document').ready(function(){
 Ext.Cmp("v_product_prefix").listener
 ({
	onKeyup :function(fn){
		if( fn.keyCode == 13 ){
			Ext.EQuery.construct( Ext.DOM.navigation, {
				keywords : Ext.Cmp(this).getValue()
			});
			Ext.EQuery.postContent();	
		}
	}
   })
});

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.cancelResult = function() {
	Ext.Cmp('span_top_nav').setText('');
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 

Ext.DOM.addResult = function()
{
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+'/SetPrefix/AddPrefix',
		method 	: 'POST',
		param 	: { 
			act	: '$$XCD%'
		}
	}).load('span_top_nav');
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.EditProduct = function()
{
var PrefixId = Ext.Cmp('prefix_id').getChecked();
if( PrefixId!='' ) 
{ 
	if( PrefixId.length==1) 
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SetPrefix/EditPrefix',
			method 	: 'POST',
			param 	: { 
				PrefixId : PrefixId
			}
		}).load('span_top_nav');
	}
	else{
		alert('Please select a rows !'); 
		return false;
	}
}
else{
	alert("Please select rows!");
	return false;
 }	
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.UpdatePrefix = function()
{
	Ext.Ajax
	({
		url	   : Ext.DOM.INDEX +'/SetPrefix/Update/',
		method : 'POST',
		param  : Ext.Join([Ext.Serialize("frmEditPrefix").getElement()]).object(),
		ERROR  : function(fn){
			try {
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){ 
					Ext.Msg("Update Product Prefix ").Success(); 
					
				}
				else {
					Ext.Msg("Update Product Prefix ").Failed();
				}	
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).post();
}
 
 
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
		
Ext.DOM.editResult = function()
{
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+'/SetPrefix/EditTpl',
		method 	: 'POST',
		param 	: {
			PrefixId : Ext.Cmp('prefix_id').getChecked()
		}
	}).load("span_top_nav");
}
		
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.savePrefix = function()
{
	if( Ext.Cmp('result_head_level').empty()){ 
		Ext.Cmp('result_head_level').setFocus();}
	else if( Ext.Cmp('result_method').empty()){ 
		Ext.Cmp('result_method').setFocus();}
	else if( Ext.Cmp('result_length').empty()){ 
		Ext.Cmp('result_length').setFocus(); }
	else if( Ext.Cmp('result_code').empty()){ 
		Ext.Cmp('result_code').setFocus();}
	else if( Ext.Cmp('status_active').empty()){ 
		Ext.Cmp('status_active').setFocus();}
	else if( Ext.Cmp('form_input').empty()){ 
		Ext.Cmp('form_input').setFocus();}
	else if( Ext.Cmp('form_edit').empty()){ 
		Ext.Cmp('form_edit').setFocus();}
	else
	{
		if( confirm('Do you want to save this Prefix') )
		{
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/SetPrefix/SavePrefix',
				method 	: 'POST',
				param 	: {
					result_length : Ext.Cmp('result_length').getValue(),
					result_code : Ext.Cmp('result_code').getValue(),
					status_active : Ext.Cmp('status_active').getValue(),
					form_input : Ext.Cmp('form_input').getValue(),
					form_edit : Ext.Cmp('form_edit').getValue(),
					result_method : Ext.Cmp('result_method').getValue(),
					result_head_level : Ext.Cmp('result_head_level').getValue()
				},
				ERROR : function(e){
					var ERR = JSON.parse(e.target.responseText);
					if( ERR.success ){
						alert("Succeeded, Save Prefix !");
						Ext.EQuery.postContent();		
					}
					else{
						 alert("Failed, Save Prefix !"); 
						 return false; 
					}
				}
				
			}).post();	
		}
	}
}

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
$(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Enable'],['Disable'],['Edit Prefix'],['Add Prefix'],['Delete Prefix'],['Cancel'],['Search']],
		extMenu  :[['Enable'],['Disable'],['EditProduct'],['addResult'],['DeletePrefix'],['cancelResult'],['searchResult']],
		extIcon  :[['accept.png'],['cancel.png'],['add.png'],['add.png'],['delete.png'],['cancel.png'], ['zoom.png']],
		extText  :true,
		extInput :true,
		extOption: [{
			render	: 5,
			type	: 'text',
			id		: 'v_product_prefix', 	
			name	: 'v_product_prefix',
			value	: Ext.DOM.datas.keywords,
			width	: 200
		}]
	});	
});
		
	</script>
<!-- start : content -->
<fieldset class="corner" style="background-color:#FFFFFF;">
	<legend class="icon-callresult">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
	<div id="toolbars"></div>
	<div id="span_top_nav"></div>
	<div class="content_table" style="background-color:#FFFFFF;"></div>
	<div id="pager" style="background-color:#FFFFFF;"></div>
</fieldset>	
		
	<!-- stop : content -->
	