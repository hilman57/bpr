<fieldset class="corner">
	<legend class='icon-application'>&nbsp;&nbsp;Set Assign Data </legend>
	<div> 
		<table border=0 cellspacing=0 width='100%'>
			<tr>
				<td class='text_caption bottom' width='12%'>Method </td> 
				<td class='bottom'>
					<?php __(form() -> combo('submit_filter','select long',ARRAY('CHECKED'=> 'CHECKED','AMOUNT' => 'AMOUNT'), null, array('change' => 'Ext.DOM.DetailAssignment(this);') )); ?> 
				</td>
			</tr>
			<tr>	
				<td class='text_caption left'>&nbsp;</td> 
				<td align='left'><div id='result_content_combo'>&nbsp;</div></td>
			</tr>	
			<tr>	
				<td class='text_caption bottom' nowrap>Quality Staff</td> 
				<td class='bottom'><?php __(form() -> listcombo('QualityStaffId',null,$QualityGroup)); ?></td>
			</tr>	
			
			<tr>	
				<td class='text_caption bottom'>&nbsp;</td> 
				<td class='bottom'><?php __(form() -> button('Assign','assign button','Assign', array('click' => 'Ext.DOM.AssignData();'))); ?></td>
			</tr>	
		</table>
	</div>
	
</fieldset>