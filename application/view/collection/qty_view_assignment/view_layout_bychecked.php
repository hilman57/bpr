<table width="100%" class="custom-grid" cellspacing="1" align='left'>
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" >&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('AssignId').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;No</th>			
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order">Campaign Name</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order">CIF</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order">Customer Name</span></th>  
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order">Caller</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order">Sales Date</span></th>
	</tr>
</thead>
<tbody>
	<?php 
	$no = 1; foreach( $result as $num => $rows ) : 
		$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
	?>
	<tr class="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first center">
		<input type="checkbox" value="<?php __($rows['AssignId']); ?>" id="AssignId" name="AssignId"></td>
		<td class="content-middle center"><?php  __($num); ?></td>
		<td class="content-middle left"><?php  __($rows['CampaignName']); ?></td>
		<td class="content-middle"><?php  __($rows['CustomerNumber']); ?></td>
		<td class="content-middle" nowrap><?php __($rows['CustomerFirstName']); ?></td>
		<td class="content-middle" nowrap><?php __($rows['full_name']); ?></td>
		<td class="content-lasted" nowrap><?php __($rows['PolicySalesDate']); ?></td>
	</tr>
	<?php 
		$no++;
	endforeach; ?>
</tbody>	
	<tr> 
		<td class="font-standars ui-corner-top ui-state-default" colspan=8>
			<div  class='aqPaging' style='margin:2px 2px 2px 2px;'>
				<span >Page : <?php __(form()->combo("select_page",'select box',$view_page_layout['pages'], ($selected_page?$selected_page:1),array("change"=>"Ext.DOM.SelectedPage(this.value);" ) ));?></span>
				&nbsp;&nbsp;<span >Records ( <?php __($view_page_layout['records']);?> ) </span>
				
			</div> 
		</td>
	</tr>
</table>