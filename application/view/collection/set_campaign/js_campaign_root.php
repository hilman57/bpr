<?php echo javascript(); ?>
<script type="text/javascript">

var OUTBOUND = 2;

// @ pack : upload data listener 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
// @ pack : upload data listener 

var status_campaign = '<?php echo $this -> URI -> _get_post("status_campaign"); ?>';

// @ pack : upload data listener 

var datas= { 
	status_campaign	: status_campaign,
	order_by  : '<?php echo $this -> URI -> _get_post('order_by');?>',
	type : '<?php echo $this -> URI -> _get_post('type');?>'
}



function ExportDeleted() 
{
	var CampaignId = Ext.Cmp('check_list_cmp').getValue();
	Ext.Window
	({
		url : Ext.DOM.INDEX +'/SetCampaign/ExportDeleted/',
		param :{
			CampaignId : CampaignId
		}	
	}).newtab();
		
}
// @ pack : upload data listener 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Add Campaign'],['Edit Data'],['Bucket Data'],['Detail Data'],['Export'],['Export Deleted']],
		extMenu  :[['addCampaign'],['EditData'],['BucketData'],['ShowRowData'],['ExportExcel'],['ExportDeleted']],
		extIcon  :[['cog_add.png'],['application_form_edit.png'],['database_go.png'],['table_go.png'],['page_white_excel.png'],['page_white_excel.png']],
		extText  :true,
		extInput :true,
		extOption:[{
			render	: 4,
			header	: 'Filter ',
			type	: 'combo',
			id		: 'combo_filter_campaign', 	
			name	: 'combo_filter_campaign',
			value	: status_campaign,
			store	: [{'2':'All'},{'1':'Active'},{'0':'Not Active'}],
			triger	: '',
			width	: 200
		}]
	});	
});

// @ pack : upload data listener 

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : upload data listener 

var navigation = {
	custnav	 : Ext.DOM.INDEX+'/SetCampaign/index/',
	custlist : Ext.DOM.INDEX+'/SetCampaign/Content/',
}
	
// @ pack : upload data listener 

Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

// @ pack : upload data listener 

Ext.DOM.getMethod = function(combo){
	if(combo.value==1) {
		Ext.Cmp('DirectAction').disabled(false);
		Ext.Cmp('AvailCampaignId').disabled(false);
	}
	else{
		Ext.Cmp('DirectAction').setValue('');
		Ext.Cmp('AvailCampaignId').setValue('');
		Ext.Cmp('DirectAction').disabled(true);
		Ext.Cmp('AvailCampaignId').disabled(true);
	}
}

// @ pack : upload data listener 

Ext.DOM.getDirect = function(combo) {
	if(combo.value==1){
		Ext.Cmp('DirectMethod').disabled(true);
		Ext.Cmp('DirectAction').disabled(true);
		Ext.Cmp('AvailCampaignId').disabled(false);
		Ext.Cmp('DirectMethod').setValue(2);
		Ext.Cmp('DirectAction').setValue(1);
	}
	else{
		Ext.Cmp('DirectMethod').setValue('');
		Ext.Cmp('DirectAction').setValue('');
		Ext.Cmp('AvailCampaignId').setValue('');
		Ext.Cmp('DirectMethod').disabled(true);
		Ext.Cmp('DirectAction').disabled(true);
		Ext.Cmp('AvailCampaignId').disabled(true);
	}	
}

// @ pack : upload data listener 

Ext.DOM.CampaignType = function( CampaignId ){
	var INBOUND = ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtAssignment/CampaignType',
		method 	: 'POST',
		param 	: {
			CampaignId : CampaignId
		}	
	}).json());
	
	return INBOUND;
}

// @ pack : upload data listener 

Ext.DOM.ClosePanel = function() {
	Ext.Cmp('span_top_nav').setText("");
}

// @ pack : upload data listener 

// Ext.DOM.UploadData = function() {
	// Ext.Ajax ({	
		// url	  : Ext.DOM.INDEX +'/MgtBucket/ManualUpload/',
		// param : { 
			// time : Ext.Date().getDuration()
		// }
	// }).load("span_top_nav");
// }
 
		
// @ pack : upload data listener 

 Ext.DOM.BucketData = function() {
	var aBucketData = Ext.DOM.INDEX+'/MgtBucket/';
	if( aBucketData ){
		Ext.Ajax({ url : aBucketData, method : 'GET', param:{}}).load('main_content'); 
	}	
}	
	
// @ pack : upload data listener 
	
Ext.DOM.addCampaign = function(){
	Ext.Ajax ({
		url		: Ext.DOM.INDEX+'/SetCampaign/AddTpl',
		method  : 'GET',
		param	: { }
		
	}).load("span_top_nav");
}
		
// @ pack : upload data listener 

Ext.DOM.cbMoveOn = function() {
	Ext.options ({ 
		fo : Ext.Cmp('ProductId').getElementId(),
		to : Ext.Cmp('ListProduct').getElementId() 
	}).move();
}


// @ pack : upload data listener 

Ext.DOM.ExportExcel = function() {
	var CampaignId = Ext.Cmp('check_list_cmp').getValue();
	if( CampaignId!='' )
	{
		// console.log(CampaignId.length);
		// var ArrCampaign =  CampaignId.split(",");
		if( CampaignId.length === 1 )
		{
			Ext.Window
			({
				url : Ext.DOM.INDEX +'/SetCampaign/Export/',
				param :{
					CampaignId : CampaignId
				}	
			}).newtab();
		}
		else
		{
			Ext.Msg('Please Select one Campaign').Info();
		}
	}
	else{
		Ext.Msg('Please Select Campaign').Info();
	}	
}
		
// @ pack : upload data listener 

Ext.DOM.ShowRowData = function() {
 var CampaignId = Ext.Cmp('check_list_cmp').getValue();
 if( CampaignId!='' ) {
		Ext.Window ({
			url : Ext.DOM.INDEX +'/SetCampaign/View/',
			param :{
				CampaignId : CampaignId
			}	
		}).newtab();
	}
	else{
		Ext.Msg('Please Select Campaign').Info();
	}	
}


// @ pack : upload data listener 

Ext.DOM.cbRemoveOn = function() {
	Ext.options ({
		fo : Ext.Cmp('ListProduct').getElementId(),
		to : Ext.Cmp('ProductId').getElementId() 
	}).move();
}

// @ pack : upload data listener 

Ext.DOM.cbEvent = function(opt) {
	if( opt!='' && opt==1){
		doJava.dom('cmp_upload_reason').disabled=false;
	}
	else{
		doJava.dom('cmp_upload_reason').disabled=true;
	}
}

// @ pack : upload data listener 

Ext.DOM.viewCampaign = function(){
	var status_campaign = Ext.Cmp('combo_filter_campaign').getValue();
	if( status_campaign ) {
		datas = { status_campaign: status_campaign }
		Ext.EQuery.construct(navigation,datas)
		Ext.EQuery.postContent();
	}	
}
		
// @ pack : upload data listener 

Ext.DOM.cancel = function(){
	Ext.Cmp('span_top_nav').setText('');
}

// @ pack : upload data listener 
	
Ext.DOM.EditData = function() {
  var CampaignId = Ext.Cmp('check_list_cmp').getChecked();
  if( CampaignId.length==1) {
	if( CampaignId.length==1) {
		Ext.Ajax({
			url 	: Ext.DOM.INDEX +'/SetCampaign/EditTpl/',
			method  : 'GET',
			param   : {
				CampaignId : CampaignId
			}
		}).load('span_top_nav');
	}
	else
		alert('Please Select One Rows !');
  }
  else 
		alert('Please Select Rows !')
}	
	
	
 // SetUpdate 
 
Ext.DOM.SetUpdate = function(){
	Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/SetCampaign/Update/',
		method 	: 'POST',
		param 	: {
			CampaignId 		: Ext.Cmp("CampaignId").getValue(),
			CampaignNumber	: Ext.Cmp("CampaignNumber").getValue(),
			CampaignName 	: Ext.Cmp("CampaignName").getValue(),
			CampaignCode 	: Ext.Cmp("CampaignCode").getValue(),
			CampaignDesc	: Ext.Cmp("CampaignDesc").getValue(),
			ExpiredDate 	: Ext.Cmp("ExpiredDate").getValue(),
			StartDate 		: Ext.Cmp("StartDate").getValue(),
			ProjectId 		: Ext.Cmp("ProjectId").getValue(),
			StatusActive 	: Ext.Cmp("StatusActive").getValue(),
			OutboundGoalsId : Ext.Cmp("OutboundGoalsId").getValue(),
			BuildTypeId 	: Ext.Cmp("BuildTypeId").getValue()
			
		},
		ERROR : function(fn){
			try {
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Update Campaign").Success();
					Ext.EQuery.postContent();
				}	
				else{
					Ext.Msg("Update Campaign").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
		
	}).post();
 }
	
/* save on campaign to upload **/

Ext.DOM.saveCmpUpload = function()
{	
	var param=[];
		param['time'] = Ext.Date().getDuration();
		
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+'/SetCampaign/SaveCampaign',
		method 	: 'POST',
		param 	: Ext.Join([param,Ext.Serialize('frm_add_campaign').getElement()]).object(),
		ERROR : function(e){
			var ERR = JSON.parse( e.target.responseText);
			if( ERR.success ) {
				alert("Suceeded, Adding Campaign"); 
				Ext.EQuery.postContent();
			}
			else {
				alert("Failed, Adding Campaign");
				return false;
			}
		}
			
	}).post()
}


// SaveProcess

Ext.DOM.SaveProcess = function(){
	if( Ext.Cmp('InboundCampaignId').empty())
	{
		Ext.Msg("Inbound Campaign is empty!").Info();
		Ext.Cmp("InboundCampaignId").setFocus(); }
	else if( Ext.Cmp('OutboundCampaignId').empty())
	{
		Ext.Msg("Outbound Campaign is empty!").Info();
		Ext.Cmp("OutboundCampaignId").setFocus();}
	else if( Ext.Cmp('total_data').empty())
	{
		Ext.Msg("Total Data!").Info();
		Ext.Cmp("total_data").setFocus();}
	else if( Ext.Cmp('assign_data').empty())
	{
		Ext.Msg("Assign Data!").Info();
		Ext.Cmp("assign_data").setFocus();}	
	else if( Ext.Cmp('ActionType').empty())
	{
		Ext.Msg("Action Type!").Info();
		Ext.Cmp("ActionType").setFocus(); }
	else{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX+'/SetCampaign/ManageCampaign/',
			method 	: 'POST',
			param 	: {
				InboundCampaignId : Ext.Cmp('InboundCampaignId').getValue(),
				OutboundCampaignId : Ext.Cmp('OutboundCampaignId').getValue(),
				TotalData : Ext.Cmp('total_data').getValue(),
				AssignData : Ext.Cmp('assign_data').getValue(),
				DirectAction : Ext.Cmp('ActionType').getValue()
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(process){
					if( process.success==1){
						Ext.DOM.getDataInbound( { value : InboundCampaignId });
						Ext.Msg("Assign data, Count ( "+ process.data +")").Success();
					}
					else{
						Ext.Msg("Assign data").Failed();
					}
				});
			}
			
		}).post();
	}
}

</script>