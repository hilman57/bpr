<?php
ini_set("memory_limit", "2048M");
/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
*/

$CampaignId = _get_array_post('CampaignId');

$this->load->helper(array('EUI_ExcelWorksheet','EUI_ExcelWorkbookBig'));   
$set_rows_headers  = array(
	'debiturid'				=> array('field'=>'deb_id'),
	'cardno'				=> array('field'=>'deb_cardno'),
	'custno'				=> array('field'=>'deb_acct_no'),
	'accountno'				=> array('field'=>'deb_acct_no'),
	'idno'					=> array('field'=>'deb_id_number'),
	'nbrcards'				=> array('field'=>'deb_no_card'),
	'fullname'				=> array('field'=>'deb_name'),
	'mothername'			=> array('field'=>'deb_mother_name'),
	'dateofbirth'			=> array('field'=>'deb_dob'),
	'homeaddress'			=> array('field'=>'deb_add_hm'),
	'billingaddress'		=> array('field'=>'deb_billing_add'),
	'officeaddress'			=> array('field'=>'deb_add_off'),
	'homezip'				=> array('field'=>'deb_zip_h'),
	'homephone'				=> array('field'=>'deb_home_no1'),
	'homephone2'			=> array('field'=>NULL),
	'mobilephone'			=> array('field'=>'deb_mobileno1'),
	'mobilephone2'			=> array('field'=>NULL),
	'otheraddress'			=> array('field'=>NULL),
	'otheraddress2'			=> array('field'=>NULL),
	'officephone'			=> array('field'=>'deb_office_ph1'),
	'officefax'				=> array('field'=>NULL),
	'ecname'				=> array('field'=>'deb_ec_name_f'),
	'ecaddress'				=> array('field'=>'deb_ec_add_f'),
	'ecphone'				=> array('field'=>'deb_ec_phone_f'),
	'ecmobile'				=> array('field'=>'deb_ec_mobile_f'),
	'ecrelationship'		=> array('field'=>NULL),
	'branch'				=> array('field'=>NULL),
	'area'					=> array('field'=>'deb_region'),
	'opendate'				=> array('field'=>'deb_open_date'),
	'clss_entry'			=> array('field'=>'deb_resource'),
	'creditlimit'			=> array('field'=>'deb_limit'),
	'currentbalance'		=> array('field'=>'deb_wo_amount'),
	'feeandcharge'			=> array('field'=>'deb_fees'),
	'interest'				=> array('field'=>'deb_interest'),
	'principalbalance'		=> array('field'=>'deb_principal'),
	'lastpaymentdate'		=> array('field'=>'deb_last_paydate'),
	'lastpaymentamount'		=> array('field'=>'deb_last_pay'),
	'currentpaymentdue'		=> array('field'=>NULL),
	'bpcount'				=> array('field'=>'deb_bp'),
	'ptpcount'			=> array('field'=>'ptpcounts'),
	'wo_date'				=> array('field'=>'deb_b_d'),
	'wo_amount'				=> array('field'=>'deb_wo_amount'),
	'wo_open'				=> array('field'=>NULL),
	'wo_lpd'				=> array('field'=>NULL),
	'curr_coll_id'			=> array('field'=>'deb_update_by_user'),
	'dlq'					=> array('field'=>'dlq'),
	'perm_message'			=> array('field'=>'deb_perm_msg'),
	'perm_date'				=> array('field'=>'deb_perm_msg_date'),
	'accstatus'				=> array('field'=>'CallReasonDesc'),
	'accstatusdate'			=> array('field'=>'deb_call_activity_datets'),
	'prevaccstatus'			=> array('field'=>'prevac'),
	'prevaccstatusdate'		=> array('field'=>NULL),
	'lastresponse'			=> array('field'=>NULL),
	'contacthistoryid'		=> array('field'=>NULL),
	'agent'					=> array('field'=>'deb_agent'),
	'note'					=> array('field'=>'Notes'),
	'reminder'				=> array('field'=>'deb_reminder'),
	'uploadbatch'			=> array('field'=>'uploadbatch'),
	'ptp_discount'			=> array('field'=>'ptp_discount'),
	'ptp_amount'			=> array('field'=>'ptpAmount'),
	'ptp_date'				=> array('field'=>'ptpDate'),
	'ptp_channel'			=> array('field'=>'ptpChannel'),
	'ssv_no'				=> array('field'=>'deb_ssv_no'),
	'callstatus'			=> array('field'=>'prevac'),
	'calldate'				=> array('field'=>'deb_call_activity_datets'),
	'prevcallstatus'		=> array('field'=>NULL),
	'infoptp'				=> array('field'=>'infoptp'),
	'prevcalldate'			=> array('field'=>NULL),
	'tenor'					=> array('field'=>'ptpTenor'),
	'addhomephone'			=> array('field'=>'addhomephone'),
	'addhomephone2'			=> array('field'=>'addhomephone2'),
	'addmobilephone'		=> array('field'=>'addmobilephone'),
	'addmobilephone2'		=> array('field'=>'addmobilephone2'),
	'addofficephone'		=> array('field'=>'addofficephone'),
	'addofficephone2'		=> array('field'=>'addofficephone2'),
	'addfax'				=> array('field'=>'addfax'),
	'addfax2'				=> array('field'=>'addfax2'),
	'swap_count'			=> array('field'=>'deb_swap_count'),
	'sms_count'				=> array('field'=>'deb_sms_count'),
	'other_agent'			=> array('field'=>'other_agent'),
	'other_card_number' 	=> array('field'=>'other_card_number'),
	'other_accstatus'		=> array('field'=>'other_accstatus'),
	'contacttype'			=> array('field'=>'deb_contact_type'),
	'rpc'					=> array('field'=>'deb_rpc'),
	'spc_with'				=> array('field'=>'deb_spc'),
	'namahomephone1'		=> array('field'=>NULL),
	'relativehomephone1'	=> array('field'=>NULL),
	'namahomephone2'		=> array('field'=>NULL),
	'relativehomephone2'	=> array('field'=>NULL),
	'namaofficephone1'		=> array('field'=>NULL),
	'relativeofficephone1'	=> array('field'=>NULL),
	'namaofficephone2'		=> array('field'=>NULL),
	'relativeofficephone2'	=> array('field'=>NULL),
	'namatlpphone1'			=> array('field'=>NULL),
	'relativetlpphone1'		=> array('field'=>NULL),
	'namatlpphone2'			=> array('field'=>NULL),
	'relativetlpphone2'		=> array('field'=>NULL),
	'namamobilephone1'		=> array('field'=>NULL),
	'relativemobilephone1'	=> array('field'=>NULL),
	'namamobilephone2'		=> array('field'=>NULL),
	'relativemobilephone2'	=> array('field'=>NULL),
	'keep_data'				=> array('field'=>'deb_is_kept'),
	'afterpay'				=> array('field'=>'deb_afterpay'),
	'attempt_call'			=> array('field'=>'attempt_call'),
	'isblock'				=> array('field'=>'deb_is_lock'),
	'bal_afterpay'			=> array('field'=>'deb_bal_afterpay'),
	'pri_afterpay'			=> array('field'=>'deb_pri_afterpay'),
	'tenor_value'			=> array('field'=>NULL),
	'tenor_amnt'			=> array('field'=>NULL),
	'tenor_dates'			=> array('field'=>NULL),
	'flag'					=> array('field'=>'flag'),
	'current_lock'			=> array('field'=>NULL),
	'lock_modul'			=> array('field'=>NULL),
	'cpa_count'				=> array('field'=>NULL),
	'lunas_flag'			=> array('field'=>NULL),
	'last_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'max_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'acc_mapping'			=> array('field'=>'acc_mapping'),
	'class_mapping'			=> array('field'=>NULL),
	'accstatus_name'		=> array('field'=>'accstatus_name'),
	'SPC_DESC'				=> array('field'=>'SPC_DESC'),
	'RPC_DESC'				=> array('field'=>'RPC_DESC'),
	'other_ch_office'		=> array('field'=>'other_ch_office'),
	'other_ch_home'			=> array('field'=>'other_ch_home'),
	'other_ch_mobile1'		=> array('field'=>'other_ch_mobile1'),
	'other_ch_mobile2'	 	=> array('field'=>'other_ch_mobile2'),
	'family'				=> array('field'=>'family'),
	'neighbour'				=> array('field'=>'neighbour'),
	'rel_person'			=> array('field'=>'rel_person'),
	'other_ec'				=> array('field'=>'other_ec')
);


if( !function_exists('EventInfoPTP') )
{
	function EventInfoPTP( $code = "" ){
		$arr_info_ptp = array(101=> "LUNAS", 102=>"CICILAN");
		return ( isset($arr_info_ptp[$code]) ? $arr_info_ptp[$code] : "");
	}
}

if( !function_exists('EventUploadBatch') )
{
	function EventUploadBatch( $date = "" ){
		if( !is_null($date) ){
			return date('YmdHis', strtotime($date));
		}
	return "";
	}
}
/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
if(!function_exists('EventCallStatusCode') )
{
 
 function EventCallStatusCode( $Code = NULL ) 
{
	$UI =& get_instance();
	$UI->db->reset_select();
	$UI->db->select("a.CallReasonDesc as Code", FALSE);
	$UI->db->from("t_lk_account_status a");
	$UI->db->where("a.CallReasonCode", $Code);
	$qry = $UI->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
	
 }
 
}

/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
if(!function_exists('EventSpcDesc') )
{
 
 function EventSpcDesc( $Code = NULL ) 
{
	$UI =& get_instance();
	$UI->db->reset_select();
	$UI->db->select("a.spc_description as Code");
	$UI->db->from("t_lk_speach_with a");
	$UI->db->where("a.spc_code", $Code);
	$qry = $UI->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
	
 }
 
}

/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
if(!function_exists('EventUploadDate') )
{
 
 function EventUploadDate( $Code = NULL ) 
{
	$UI =& get_instance();
	$UI->db->reset_select();
	$UI->db->select("a.FTP_UploadDateTs as Code");
	$UI->db->from("t_gn_upload_report_ftp a");
	$UI->db->where("a.FTP_UploadId", $Code);
	$qry = $UI->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
	
 }
 
}


/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
 
if(!function_exists('EventIsRPC') ) 
{
	function EventIsRPC( $code  = 0 )
	{
		return ( $code ? "YES" : "NO");
	}
}	


/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
 if(!function_exists('CallEvent') )
 {
	 function CallEvent( $Field = null, $value=null ) 
	{
		$_arrs_field = array( 
			"accstatus" => "EventCallStatusCode",  
			"prevaccstatus" => "EventCallStatusCode", 
			"isrpc" => "EventIsRPC",
			"spc_with" => "EventSpcDesc",
			"infoptp" => "EventInfoPTP",
			"uploadbatch" => "EventUploadBatch"
		
		);
		
		//  : pack @ process its 
		
		if( in_array($Field, array_keys($_arrs_field) ) ) 
		{
			return call_user_func_array($_arrs_field[$Field],array($value));
		} else {
			return $value;
		}
	}
 }
 
 
/*
 * @ def		create header name of file to process 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
 
 $file_name = $campaign."-deleted-alldata-".date('Ymd')."-".date('His').".xls";
 $BASE_DIR_NAME = dirname (__FILE__);
 $BASE_EXCEL_FILE_NAME = "${BASE_DIR_NAME}/${file_name}";

 $workbook =& new writeexcel_workbookbig($BASE_EXCEL_FILE_NAME);
 $worksheet =& $workbook->addworksheet();
		
/* pack header format every file **/
	 
 $header_format =& $workbook->addformat();
 $header_format ->set_bold();
 $header_format->set_size(10);
 $header_format->set_color('white');
 $header_format->set_align('left');
 $header_format->set_align('vcenter');
 $header_format->set_pattern();
 $header_format->set_fg_color('black');
 
 
 /*
 * @ def		called name of template download
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
 
 $num = 0; // default num from 0 to (n);
 
/*
 * @ def		called name of template download
 *
 * @ package 	 view
 * @ params 	 Content line write
 */

$ar_header = $set_rows_headers; //( isset( $result[0] ) ? $result[0] : array() );
$col_head = 0;
if(is_array( $ar_header ))
	foreach( $ar_header as $label => $name)
{
	$worksheet->write_string($num, $col_head, $label, $header_format );
	$col_head++; 	
 }

$num = $num+1; 
 
/*
 * @ def		set rows datas 
 *
 * @ package 	 view
 * @ params 	 Content line write
 */
$UI =& get_instance();
	$_conds = array();
	$UI->db->reset_select();
	$UI->db->select("
a.deb_id,
	a.deb_cardno,
	a.deb_acct_no,
	a.deb_acct_no,
	a.deb_id_number,
	a.deb_no_card,
	a.deb_name,
	a.deb_mother_name,
	a.deb_dob,
	a.deb_add_hm,
	a.deb_billing_add,
	a.deb_add_off,
	a.deb_zip_h,
	a.deb_home_no1,
	a.deb_mobileno1,
	a.deb_office_ph1,
	a.deb_ec_name_f,
	a.deb_ec_add_f,
	a.deb_ec_phone_f,
	a.deb_ec_mobile_f,
	a.deb_region,
	a.deb_open_date,
	a.deb_resource,
	a.deb_limit,
	a.deb_fees,
	a.deb_interest,
	a.deb_principal,
	a.deb_last_paydate,
	a.deb_last_pay,
	a.deb_bp,
	(select count(ptps.ptp_id) from t_tx_ptp ptps where ptps.deb_id = a.deb_id group by ptps.deb_id) as ptpcounts,
	a.deb_b_d,
	a.deb_wo_amount,
	a.deb_update_by_user,
	a.deb_perm_msg,
	a.deb_perm_msg_date,
	a.deb_call_status_code,
	b.CallReasonDesc,
	a.deb_prev_call_status_code,
	(select az.CallReasonDesc from t_lk_account_status az where az.CallReasonCode = a.deb_prev_call_status_code) as prevac,
	a.deb_agent,
	a.deb_reminder,
	a.deb_swap_count,
	a.deb_sms_count,
	a.deb_contact_type,
	a.deb_rpc,
	(select spc.rpc_name from t_lk_remote_place spc where spc.rpc_code = a.deb_rpc) as `deb_rpc`,
	(select spc.spc_name from t_lk_speach_with spc where spc.spc_code = a.deb_spc) as `deb_spc`,
	a.deb_is_kept,
	a.deb_afterpay,
	a.deb_is_lock,
	a.deb_bal_afterpay,
	a.deb_pri_afterpay,
	a.deb_last_swap_ts,
	a.deb_call_activity_datets,
	a.addhomephone as addhomephone,
	a.addhomephone2 as addhomephone2,
	a.addmobilephone as addmobilephone,
	a.addmobilephone2 as addmobilephone2,
	a.addofficephone as addofficephone,
	a.addofficephone2 as addofficephone2,
	a.other_agent as other_agent,
	a.other_card_number as other_card_number,
	a.other_accstatus as other_accstatus,
	a.addfax as addfax,
	a.addfax2 as addfax2,
	a.deb_created_ts as uploadbatch,
	a.deb_ssv_no as deb_ssv_no,
	'0' as dlq,
	'1' as flag,
	b.CallReasonDesc as accstatus_name, 
	d.spc_description as SPC_DESC, 
	e.rpc_description as RPC_DESC, 
	IF((select ptp.ptp_type from t_tx_ptp ptp where ptp.ptp_account = a.deb_acct_no and ptp.ptp_type<>0 limit 0,1) IS NULL,a.infoptp,NULL) as infoptp,
	IF(a.acc_mapping<>'', a.acc_mapping, (select group_concat(am.Card) from t_gn_account_mapping am where am.CardNo=a.deb_acct_no)) as acc_mapping,
	(select count(ts.CallHistoryId) as total from t_gn_callhistory ts where ts.AccountNo = a.deb_acct_no) as attempt_call,
	(select az.CallHistoryNotes from t_gn_callhistory az where az.AccountNo = a.deb_cardno order by az.CallHistoryUpdatedTs desc limit 0,1) as Notes,
	(select ptp.ptp_date from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpDate,
	(select ptp.ptp_amount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpAmount,
	(select pych.pch_name from t_tx_ptp ptp
		left join t_lk_payment_channel pych ON ptp.ptp_chanel = pych.pch_code
		where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpChannel,
	(select ptp.ptp_tenor from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpTenor,
	(select ptp.ptp_discount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptp_discount,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=103) as other_ch_office, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=101) as other_ch_home, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=102) as other_ch_mobile1, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=109) as other_ch_mobile2, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=104) as family, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=105) as neighbour,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=107) as rel_person, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id  AND c.addphone_type=108) as other_ec", FALSE);
	
 $UI->db->from("t_gn_debitur_deleted a");
 $UI->db->join("t_lk_account_status b ","a.deb_call_status_code=b.CallReasonCode", "LEFT");
 $UI->db->join("t_lk_speach_with d "," a.deb_spc=d.spc_code", "LEFT");
 $UI->db->join("t_lk_remote_place e "," a.deb_rpc=e.rpc_code","LEFT");
 $UI->db->join("t_gn_campaign f", "a.deb_cmpaign_id=f.CampaignId", "LEFT");
 
 if( is_array($CampaignId)  AND count($CampaignId) > 0 ) {
	$UI->db->where_in("a.deb_cmpaign_id",array_map('intval',$CampaignId));
 }
 
 
$rs = $UI->db->get(); 

$source_headers = array_keys($set_rows_headers);

$num = $num;

if( $rs->num_rows() > 0) 
	foreach( $rs->result_assoc() as $key => $value  )  
{
	$source_field = $set_rows_headers;
	foreach($source_headers as $h => $_header ) {
		$arr_source_field  = $source_field[$_header];
		if( !is_null($arr_source_field['field']) ) 
		{	
			$field = $arr_source_field['field'];
			$worksheet->write_string($num, $h, CallEvent($field, $value[$field]) );
		} 
		
	 }
	$num++;
}
$workbook->close(); // end book 


// --------------- download excel ---------------------------

if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel; charset=utf-16");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	
		
 
?>

