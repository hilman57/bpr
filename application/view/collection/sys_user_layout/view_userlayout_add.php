
<fieldset class="corner">
	<legend class="icon-application">&nbsp;&nbsp; Add User Layout</legend>
	<div class="box-shadow" style="margin-bottom:10px;">
		<table cellpadding="8px" cellspacing="8px">
			<tr>
				<td class="text-caption">Layout Themes </td>
				<td class=""><?php echo form() -> combo('UserThemes','select long',$UserThemes);?></td>
			</tr>
			<tr>
				<td class="text-caption">Layout Name</td>
				<td class=""><?php echo form() -> combo('UserLayout','select long', $UserLayout);?></td>
			</tr>
			<tr>
				<td class="text-caption">User Group</td>
				<td class=""><?php echo form() -> combo('UserGroup','select long',$UserGroup);?></td>
			</tr>
			<tr>
				<td class="text-caption"></td>
				<td class="">
					<input type="button" class="save button" onclick="Ext.DOM.SaveLayout();" value="Save">
					<input type="button" class="close button" onclick="Ext.Cmp('span_top_nav').setText('');" value="Close">
				</td>
			</tr>
	</div>
</fieldset>