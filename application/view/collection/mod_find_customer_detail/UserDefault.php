<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div style='margin-top:-20px;float:right;padding-left:16px;margin-right:7px;cursor:pointer;' 
	class="page-go" onclick="Ext.DOM.CancelFinding();">&nbsp;Back</div>	

<fieldset class="corner" style="margin-top:-3px;">  
<legend class="icon-menulist"> &nbsp;&nbsp;Customer Information</legend> 
<div id="tabs-panels" class="box-shadow" style='border:0px solid #dddddd;'>
	<ul>
		<li><a href="#tabs-4" id="aPolicy">Policy Data</a></li>
		<li><a href="#tabs-5" id="aPayer">Personal Data</a></li>
		<li><a href="#tabs-7" id="aBenefiecery">Call History</a></li>
		<li><a href="#tabs-8" id="aFollowupform">Form Followup</a></li>
		
	</ul>
	
	<!-- this load by php file :: not javascript -->
		<div id="tabs-4" style="background-color:#FFFFFF;overflow:auto;">
			<?php $this->load->view('mod_find_customer_detail/UserPolicyData');?>
		</div>
		<div id="tabs-5" style="background-color:#FFFFFF;overflow:auto;">
			<?php $this->load->view('mod_find_customer_detail/UserPersonalData');?>
		</div>
		<div id="tabs-7" style="background-color:#FFFFFF;overflow:auto;">
		</div>
		
		<div id="tabs-8" style="background-color:#FFFFFF;overflow:auto;">
			<?php $this->load->view('mod_find_customer_detail/UserFollowUp');?>
		</div>
</div>
</fieldset>
