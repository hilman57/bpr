<script>
var WRITE_COUNTER = 0;

Ext.DOM.CheckedEditPolicy = function(obj){
	if( obj.value.length > 0  ){
		WRITE_COUNTER++;
	}
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 Ext.DOM.initFunc = {
	isEdit : false,
	isCall : false,
	isHangup : false,
	isRunCall : false,
	isStatus : false,
	Attempt : 0
 }
 
Ext.query(function(){
 Ext.query("#tabs-panels" ).tabs();
 Ext.query("#tabs" ).tabs();
});

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
Ext.DOM.PolicyDataByPolicyId = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/PolicyDataByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue(),
			CustomerId : Ext.Cmp('CustomerId').getValue(), 	
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function( data )
			{
				Ext.Cmp('ADDRESS1_FU_POST').setValue(data.CustomerAddressLine1);
				Ext.Cmp('ADDRESS2_FU_POST').setValue(data.CustomerAddressLine2);
				Ext.Cmp('ADDRESS3_FU_POST').setValue(data.CustomerAddressLine3);
				Ext.Cmp('ADDRESS4_FU_POST').setValue(data.CustomerAddressLine4);
				Ext.Cmp('CITY_FU_POST').setValue(data.CustomerCity);
				Ext.Cmp('ZIP_FU_POST').setValue(data.CustomerZipCode);
				Ext.Cmp('PROVINCE_FU_POST').setValue(data.ProvinceId);
				Ext.Cmp('COUNTRY_FU_POST').setValue(data.CustomerCountry);
				Ext.Cmp('HOME_FU_POST').setValue(data.CustomerHomePhoneNum);
				Ext.Cmp('MOBILE_FU_POST').setValue(data.CustomerMobilePhoneNum);
				Ext.Cmp('OFFICE_FU_POST').setValue(data.CustomerWorkPhoneNum);
				Ext.Cmp('EMAIL_FU_POST').setValue(data.CustomerEmail);
				Ext.Cmp('PAYMENTFREQUENCY_FU_POST').setValue(data.PayMode);
				Ext.Cmp('NOTES_FU_POST').setValue(data.NOTES_FU_POST);
				
			});
		}
		
	}).post();	
}

/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
 
Ext.DOM.QuestionDataByPolicyId = function(){

//QUESTION BY POLICY ID 
	Ext.Ajax ({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/QustionByQualityPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue()
		},
		ERROR : function(e){
			Ext.Util(e).proc(function( data )
			{
				var frm_elment = Ext.Serialize('UserQuestion').getElement();
				for( var field in data )
				{
					for( var fields in frm_elment )
					{
						if( field==fields ) {
							Ext.Cmp(fields).setValue(data[field]);
							Ext.Cmp(fields).disabled(true)
						}
					}
				}
			});
		}
	}).post();	
	
}
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.PolicyFollowByPolicyId = function(){
	Ext.Ajax
	({
		url : Ext.DOM.INDEX +'/ProjectWorkForm/PolicyFollowByPolicyId/',
		method : 'POST',
		param :{
			PolicyId : Ext.Cmp('PolicyId').getValue(),
			CustomerId : Ext.Cmp('CustomerId').getValue(), 	
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function( data ) {
				for( var field in data  ){
					Ext.Cmp(field).setValue(data[field]);
				}
				
			});
		}
		
	}).post();	
}


/* 
 * @ def : PlayByCallSession
 * ------------------------------------------
 *
 * @ param :  - 
 * @ aksess : -   
 */
 
Ext.DOM.PlayByCallSession = function(SessionId){
	$('#tabs').tabs( "option", "selected", 1);
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/PlayBySessionId/",
		method 	: 'GET',
		param  	: { RecordId : SessionId },
		ERROR 	: function(e){
			Ext.Util(e).proc(function(fn){
				if(fn.success) {
					Ext.Media("tabs-2",{ 
						url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
						width 	: '99%',
						height 	: '70px',
						options : {
							ShowControls 	: 'true',
							ShowStatusBar 	: 'true',
							ShowDisplay 	: 'true',
							autoplay 		: 'true'
						}
					}).WAVPlayer();
						
					Ext.Tpl("tabs-2", fn.data).Compile();
					Ext.Cmp('MediaPlayer').setAttribute('class','textarea');
					Ext.Css('tabs-2').style({'text-align' : 'left',  'padding-left' : "8px",  'padding-top' : "20px" });
					Ext.Css('div-voice-container').style({ "margin-top" : "5px", "width" : "100%", "margin-bottom" : "20px"  });
				}
			})
		}
	}).post();
}

/* 
 * @ def : ShowWindowScript
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.PlayRecording = function(check) {

var chk = Ext.Cmp('recordId').getName();
if( check.checked ) {
  for(var c in chk ) {
	if( chk[c].checked) {
		if( chk[c].value!=check.value )  chk[c].checked = false;
		else 
		{
			$('#tabs').tabs( "option", "selected", 1 );
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX+"/QtyApprovalInterest/VoicePlay/",
				method 	: 'GET',
				param  	: { RecordId : check.value },
				ERROR 	: function(e){
					Ext.Util(e).proc(function(fn){
					
						if( fn.success ) 
						{
							Ext.Media("tabs-2",{ 
								url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
								width 	: '99%',
								height 	: '70px',
								options : {
									ShowControls 	: 'true',
									ShowStatusBar 	: 'true',
									ShowDisplay 	: 'true',
									autoplay 		: 'true'
								}
							}).WAVPlayer();
							
							Ext.Tpl("tabs-2", fn.data).Compile();
							Ext.Cmp('MediaPlayer').setAttribute('class','textarea');
							Ext.Css('tabs-2').style({'text-align' : 'left',  'padding-left' : "8px",  'padding-top' : "20px" });
							Ext.Css('div-voice-container').style({ "margin-top" : "5px", "width" : "100%", "margin-bottom" : "20px"  });
						}	
					});	
					
				}
			}).post();
		}
	}
}}}

 
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.CancelFinding =function(){
	var cif_number 	  = Ext.Cmp('cif_number').getValue(), 
		customer_name = Ext.Cmp('customer_name').getValue(), 
		policy_number = Ext.Cmp('policy_number').getValue();
		
	

	window.location.replace( 
	Ext.DOM.INDEX+"/FindCustomerDetail/index/?page="+ 
		Ext.Cmp('select_spage').getValue()+"&cif_number="+cif_number
		+"&customer_name="+customer_name+"&policy_number="+policy_number);
 }
 
 
 /* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.CallHistory = function( PolicyId ){
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/QtyScoring/CallHistory/",
		method 	: 'GET',
		param 	: {
			CustomerId : Ext.Cmp('CustomerId').getValue(),
			PolicyId : PolicyId
		} 
	}).load("tabs-7");
} 


/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
Ext.DOM.SelectPages = function(select){
	Ext.Ajax ({
		url : Ext.DOM.INDEX +'/QtyApprovalInterest/Recording/',
		method : 'GET',
		param : { 
			CustomerId : Ext.Cmp('CustomerId').getValue(), 
			Pages: select.id,
			time : Ext.Date().getDuration()	
		}
	}).load('tabs-1');
}

/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
/* FU_FOLLOWUP **/

Ext.DOM.FuFollowUpByCheckedList = function() {	
	return Ext.Cmp('FU_FOLLOWUP').getChecked();
}

/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
 
/* Ext.DOM.FuFollowUpByCheckedList */

Ext.DOM.FuFollowUpByCheckedName = function() 
{
	var conds = true;
	var elem = Ext.Cmp('FU_FOLLOWUP').getName();
	var chkname = [];
	for( var i = 0; i<elem.length; i++)
	{	
		if( elem[i].checked) 
		{
			var require = 'NOTES_'+elem[i].id;
			if( Ext.Cmp(require).empty() ) {
				Ext.Msg(elem[i].title+' is empty ').Info();
				Ext.query('#'+require).focus();
				conds = false;
				return false;
			}
		}
    }
	return conds;
}
 
/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
  
Ext.DOM.DisableAddress  = function(options){
	
	Ext.Cmp('ADDRESS1_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS2_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS3_FU_POST').disabled(options);
	Ext.Cmp('ADDRESS4_FU_POST').disabled(options);
	Ext.Cmp('CITY_FU_POST').disabled(options);
	Ext.Cmp('ZIP_FU_POST').disabled(options);
	Ext.Cmp('PROVINCE_FU_POST').disabled(options);
	Ext.Cmp('COUNTRY_FU_POST').disabled(options);
	Ext.Cmp('HOME_FU_POST').disabled(options);
	Ext.Cmp('MOBILE_FU_POST').disabled(options);
	Ext.Cmp('OFFICE_FU_POST').disabled(options);
	Ext.Cmp('EMAIL_FU_POST').disabled(options);
	Ext.Cmp('PAYMENTFREQUENCY_FU_POST').disabled(options); 
	Ext.Cmp('NOTES_FU_POST').disabled(options);
}

/* * 
 * @ def : PolicyDetailById
 * -------------------------------------------------
 * 
 */ 
Ext.DOM.array_sum = function( selector ) { 
    var sum = 0;
    for (var i = 0; i < selector.length; i++) {
		if(selector[i]!='') {
			sum+= parseFloat(selector[i]);
		}
    }
    return sum;
};

/* * 
 * @ def : PolicyDetailById
 * -------------------------------------------------
 * 
 */ 
Ext.DOM.PolicyDetailById = function(PolicyId){
 $('#tabs-panels').tabs( "option", "selected", 0);
		Ext.Ajax({
			url : Ext.DOM.INDEX+'/QtyScoring/PolicyDetailById/',
			method :'GET',
			param : {
				PolicyId : PolicyId
			}	
		}).load('tabs-4');
		
	Ext.DOM.CallHistory(PolicyId);
	Ext.DOM.QuestionDataByPolicyId();	
	Ext.DOM.PolicyDataByPolicyId();
	Ext.DOM.PolicyFollowByPolicyId();
	window.setTimeout(function(){
		Ext.DOM.ValidQuestion(this);
	}, 1000);
			
	
}	
	
/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
Ext.DOM.ValidQuestion = function(object) {
var FU_POST = [], FU_SUPPORT = [];
  
  Ext.Cmp('CustField_Q7').disabled(true);
  
  FU_POST.push($('#CustField_Q1').val());
  FU_POST.push($('#CustField_Q2').val());
  FU_POST.push($('#CustField_Q3').val());
	  
// define suport 

  FU_SUPPORT.push($('#CustField_Q4').val());
  FU_SUPPORT.push($('#CustField_Q5').val());
  FU_SUPPORT.push($('#CustField_Q6').val());
	  
// open form FU POS 
console.log(Ext.DOM.array_sum(FU_POST));
	  
if( Ext.DOM.array_sum(FU_POST)==3){
	 Ext.Cmp('FU_POST').disabled(true);	
	 Ext.Cmp('FU_POST').getElementId().checked = false; 
	 Ext.DOM.DisableAddress(true);
 }	
  else if( Ext.DOM.array_sum(FU_POST) < 3 )
 {
	if( !Ext.Cmp('CustField_Q1').empty() 
		&& !Ext.Cmp('CustField_Q1').empty() 
		&& !Ext.Cmp('CustField_Q1').empty() )
	{
		Ext.Cmp('FU_POST').disabled(true);
		Ext.Cmp('FU_POST').getElementId().checked = true; 
		Ext.DOM.DisableAddress(true);
	} 
	else{
		Ext.Cmp('FU_POST').disabled(true);
		Ext.Cmp('FU_POST').getElementId().checked = false; 
		Ext.DOM.DisableAddress(true);
	}
  }
  else{
	Ext.Cmp('FU_POST').disabled(true);
	Ext.Cmp('FU_POST').getElementId().checked = false; 
	Ext.DOM.DisableAddress(true);
  }  
  
 if( Ext.DOM.array_sum(FU_SUPPORT) > 0 ) 
 {
	
	Ext.Cmp('CustField_Q7').disabled(true);
    var Q7 = parseInt(Ext.Cmp('CustField_Q7').getValue());
	if( parseInt(Q7)==1) 
	{
		Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
		Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = true;
		Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(false);
			
	}
	else{
		Ext.Cmp('FU_VISIT_TO_BRANCH').disabled(true);
		Ext.Cmp('FU_VISIT_TO_BRANCH').getElementId().checked = false;
		Ext.Cmp('NOTES_FU_VISIT_TO_BRANCH').disabled(true);
	}
		
	if( parseInt(Q7)==0 ){
		if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
			Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = true;
			Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(false);
		}
		
		if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
			Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = true;
			Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(false);
		}
	}
	else
	{
		if( !Ext.Cmp('FU_AGENCY_SUPPORT').IsNull()) {
			Ext.Cmp('FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('FU_AGENCY_SUPPORT').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_AGENCY_SUPPORT').setValue('');
		}	
		
		if( !Ext.Cmp('FU_BANCA_SUPPORT').IsNull()) {
			Ext.Cmp('FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('FU_BANCA_SUPPORT').getElementId().checked = false;
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').disabled(true);
			Ext.Cmp('NOTES_FU_BANCA_SUPPORT').setValue('');
		}	
	}
 } 
 else {
	 Ext.Cmp('CustField_Q7').setValue('');
	 Ext.Cmp('CustField_Q7').disabled(true);
 } 
   //Ext.MathRand({form : 'frmQuestions'}).setRandForm(object);
}


/* 
 * @ def : toolbars on navigation 
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.document(document).ready(function(){
 $('#CustomerDOB').datepicker({ 
  dateFormat : 'yy-mm-dd', 
  changeYear : true, 
  changeMonth : true 
 });
 
/* @ def : onload listener 
 * -----------------------------
 *
 * @ param  : public Window
 * @ aksess : public test  
 */
   Ext.DOM.SelectPages({'id':0});
   Ext.DOM.PolicyDetailById(Ext.Cmp('PolicyId').getValue());
});

</script>