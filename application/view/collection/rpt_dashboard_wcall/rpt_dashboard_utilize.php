<?php 

/*
 * @ def  : source data HTML 
 * 
 */

			

?> <fieldset class="corner"> <?php

// echo "<pre>";
// print_r($size_data);
// echo "</pre>";
__('<table cellspacing=1 cellpadding=3 width="95%" style="margin-left:20px;margin-bottom:20px;">
	<tr height=22 bgcolor="#E25C77" style="color:white;">
		<th class="center" rowspan="4">&nbsp;No.</th>
		<th class="center" rowspan="4">&nbsp;Caller Name </th>
		<th class="center" colspan="17">&nbsp;(Contacted+Call Completed+Pending)</th>
		<th class="center" rowspan="4">&nbsp;Surveyed Ratio</th>
		<th class="center" rowspan="4">&nbsp;Contact Ratio</th>
	</tr>
	<tr height=22 bgcolor="#E25C77" style="color:white;">
		<th class="center" rowspan="3">Total Data</th>
		<th class="center" rowspan="3">Completed</th>
		<th class="center" rowspan="2" colspan="2">Contacted</th>
		<th class="center" colspan="7">Call Completed</th>
		<th class="center" rowspan="2" colspan="6">Pending - Inprogress Data</th>
	</tr>
	<tr height=22 bgcolor="#E25C77" style="color:white;">
		<th class="center" colspan="2">Non Contacted</th>
		<th class="center" colspan="5">Uncontacted (UNCLEAN DATA)</th>
	</tr>
	<tr height=22 bgcolor="#E25C77" style="color:white;">
		<th class="center">Agree</th>
		<th class="center">No Interest To Talk</th>
		<th class="center">Can Not Be Reached</th>
		<th class="center">No Answer</th>
		<th class="center">Busy Tone</th>
		<th class="center">Wrong Number</th>
		<th class="center">Tulalit (Dead Tone)</th>
		<th class="center">Leave Masssge</th>
		<th class="center">Return List</th>
		<th class="center">1st</th>
		<th class="center">2nd</th>
		<th class="center">3rd</th>
		<th class="center">4th</th>
		<th class="center">5th++</th>
		<th class="center">Call Back Later</th>
	</tr>');
	
	$no = 0;
	foreach($AgentName as $UserId => $UserName )
		{
			$color = ($no%2!=0?'#E2D7D7':'#FFFFFF');
			$no++;
			
	// ratio of survey 
	 	$total_survey = ($data_size[$UserId]['tot_size_survey']?$data_size[$UserId]['tot_size_survey']:0);
		$total_not_interest_talk = ($data_size[$UserId]['tot_size_not_interest_talk']?$data_size[$UserId]['tot_size_not_interest_talk']:0);
		$total_cant_bereach = ($data_size[$UserId]['tot_size_not_reach']?$data_size[$UserId]['tot_size_not_reach']:0);
		$total_not_answer = ($data_size[$UserId]['tot_size_not_answer']?$data_size[$UserId]['tot_size_not_answer']:0);		
		$total_contacted = ($total_survey+$total_not_interest_talk);
		$total_non_contacted = ($total_contacted + ($total_cant_bereach+$total_not_answer));
		
	// ratio on percent 
		$ratio_survey = ($total_survey?(round(($total_survey/( $total_survey + $total_not_interest_talk)),2)*100) : 0);
		$ratio_contact = ($total_contacted?(round(($total_contacted/$total_non_contacted),2)*100):0);
		
		__("<tr style=\"color:#7e009d;border:1px solid #AAAAAA;background-color:".$color."\">
			  <td class=\"content-first\" style=\"text-align:center;\">".$no."</td>
			  <td class=\"content-middle left\" nowrap>&nbsp;".$UserName."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_data_size']?$data_size[$UserId]['tot_data_size']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_complete']?$data_size[$UserId]['tot_size_complete']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_survey']?$data_size[$UserId]['tot_size_survey']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_not_interest_talk']?$data_size[$UserId]['tot_size_not_interest_talk']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_not_reach']?$data_size[$UserId]['tot_size_not_reach']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_not_answer']?$data_size[$UserId]['tot_size_not_answer']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_busy_tone']?$data_size[$UserId]['tot_size_busy_tone']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_wrong_number']?$data_size[$UserId]['tot_size_wrong_number']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_dead_tone']?$data_size[$UserId]['tot_size_dead_tone']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_leave_message']?$data_size[$UserId]['tot_size_leave_message']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_return_list']?$data_size[$UserId]['tot_size_return_list']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_attempt1']?$data_size[$UserId]['tot_size_attempt1']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_attempt2']?$data_size[$UserId]['tot_size_attempt2']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_attempt3']?$data_size[$UserId]['tot_size_attempt3']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_attempt4']?$data_size[$UserId]['tot_size_attempt4']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_attempt5_plush']?$data_size[$UserId]['tot_size_attempt5_plush']:0)."</td>
			  <td class=\"content-middle center\">".($data_size[$UserId]['tot_size_callback_letter']?$data_size[$UserId]['tot_size_callback_letter']:0)."</td>
			  <td class=\"content-middle right\">".$ratio_survey."%</td>
			  <td class=\"content-lasted right\">".$ratio_contact."%</td>
			</tr>
		");
		
		}
		
	__('</table>');
?>
</fieldset>