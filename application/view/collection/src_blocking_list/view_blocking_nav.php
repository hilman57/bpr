<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	block_phone_number  : "<?php echo _get_exist_session('block_phone_number');?>",
	block_agent_id 		: "<?php echo _get_exist_session('block_agent_id');?>",
	block_campaign_id 	: "<?php echo _get_exist_session('block_campaign_id');?>",
	block_cust_id 		: "<?php echo _get_exist_session('block_cust_id');?>",
	block_cust_name 	: "<?php echo _get_exist_session('block_cust_name');?>",
	block_start_date 	: "<?php echo _get_exist_session('block_start_date');?>",
	block_end_date 		: "<?php echo _get_exist_session('block_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/BlockingPhone/index/',
	custlist : Ext.DOM.INDEX+'/BlockingPhone/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('flwCustomers').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('flwCustomers').Clear();
	Ext.DOM.searchCustomer();
}

Ext.DOM.SetBlockPhoneAll = function() {
   
    if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_LEADER){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
    }
    if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_INBOUND){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
    }
    if(Ext.DOM.handling == Ext.DOM.USER_LEVEL_AGENT){
        Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
    }
    
    Ext.Ajax
    ({
	url 	: Ext.DOM.INDEX+'/BlockingPhone/SetUnblockPhoneAll/',
	method 	: 'POST',
	param 	: { 
		//BlockId : BlockId
	},
	ERROR	: function(e) {
	  Ext.Util(e).proc(function(response){
                console.log(response.success);
		if( response.success > 0 ){
			Ext.Msg("Unblock Phone ( "+ response.success +" )").Success();
			Ext.DOM.searchCustomer();
			
		} else {
			Ext.Msg("Unblock Phone ( "+ response.success +" )").Failed();
		}
	  });	
            }	
       }).post();

       
    
}

Ext.DOM.SetUblockPhone = function() {
 
var BlockId = Ext.Cmp('BlockId').getValue();
 
/* @ pack : handling data ---- */
 
 if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_LEADER){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }
 
/* @ pack : handling data ---- */
 
 if( Ext.DOM.handling == Ext.DOM.USER_LEVEL_INBOUND){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }
 
/* @ pack : handling data ---- */

  if( Ext.DOM.handling ==  Ext.DOM.USER_LEVEL_AGENT){
	Ext.Msg("You not have privilege akses for this Action").Info();
	return false;
 }

/* @ pack : handling data ---- */

 if( BlockId.length ==0 ){
	Ext.Msg("Please select rows ").Info();
	return false;
 }
 
/* @ pack : then execute ---- */

Ext.Ajax
({
	url 	: Ext.DOM.INDEX+'/BlockingPhone/SetUnblockPhone/',
	method 	: 'POST',
	param 	: { 
		BlockId : BlockId
	},
	ERROR	: function(e) {
	  Ext.Util(e).proc(function(response){
		if( response.success ){
			Ext.Msg("Unblock Phone ( "+ response.success +" )").Success();
			Ext.DOM.searchCustomer();
			
		} else {
			Ext.Msg("Unblock Phone ( "+ response.success +" )").Failed();
		}
	  });	
   }	
}).post();
	
}
// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( CustomerId ) {
 if( CustomerId!='') 
 {	
	Ext.ActiveMenu().NotActive();
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
		method  : 'GET',
		param 	: {
			CustomerId : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/BlockingPhone/index/', 
		}
  });
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}

// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Unblock'],['Unblock All']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['SetUblockPhone'],['SetBlockPhoneAll']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['group_delete.png'],['group_delete.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="flwCustomers">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('block_campaign_id','select auto', $CampaignId, _get_exist_session('block_campaign_id'));?></td>
			<td class="text_caption bottom"> # Nomor Telepon &nbsp;:</td>
			<td class="bottom"><?php echo form()->input('block_phone_number','input_text long', _get_exist_session('block_phone_number'));?></td>
		
			
		</tr>
		<tr>
			<td class="text_caption bottom"> # ID Pelanggan &nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('block_cust_id','input_text long', _get_exist_session('block_cust_id'));?></td>
			<td class="text_caption bottom"> # Block Date &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('block_start_date','input_text date', _get_exist_session('block_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('block_end_date','input_text date', _get_exist_session('block_end_date'));?>
			</td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Nama Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('block_cust_name','input_text long', _get_exist_session('block_cust_name'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('block_agent_id','select auto', $Deskoll, _get_exist_session('block_agent_id'));?></td>
			
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->