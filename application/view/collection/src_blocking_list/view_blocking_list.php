<?php 
// @ pack :  set align #------------------------------

 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');

// @ pack :  set align #------------------------------

 $labels =& page_labels();
 $primary =& page_primary();

// @ pack :  set align #------------------------------

 page_set_align('Product', 'center');
 page_set_align('AccountNo', 'left');
 page_set_align('CustomerName', 'left');
 page_set_align('Phone', 'left');
 page_set_align('TypePhone', 'left');
 page_set_align('BlockDate', 'center');
 page_set_align('UserCode', 'left');
 page_set_align('UserName', 'left');
 page_set_align('Notes', 'left');
  
 
  
// @ pack : set currency column in here 

 $call_user_func = array('BlockDate'=>'_getDateTime','Phone'=>'_getMasking');
 $call_link_user = array('CustomerName');
 $call_link_nowarap = array('LockCreateTs'=>'nowrap');
 $align =&page_get_align();
 
?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<a href="javascript:void(0);" style="color:red;" onclick="Ext.Cmp('<?php __($primary);?>').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle " >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); if($Field != 'CallReasonId') { ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default " align="<?php __($align[$Field]); ?>"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php  } endforeach;  ?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows ) { 
 $color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); ?>
 <tr class="onselect" bgcolor="<?php echo $color; ?>">
	<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary],NULL, NULL ); ?>
	<td class="content-middle center"> <?php __($no);?></td>
	<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field);   ?>
	<td class="content-middle" <?php __($color) ?>  align="<?php __($align[$Field]); ?>" <?php echo $call_link_nowarap[$Field];?>>
		<?php if( in_array( $Field, $call_link_user ) ) : ?>
		<a href="javascript:void(0);" style="color:#289c05;text-decoration:none;" onclick="Ext.DOM.CallCustomer('<?php __($rows['DebId']); ?>');">  <?php __($rows[$Field]); ?></a>
		<?php else : ?>
		<?php __(page_call_function($Field, $rows, $call_user_func) ); ?>
		<?php endif; ?>	
	</td>
	<?php endforeach;?>
 </tr>	
</tbody>
<?php
	$no++;
}

?>
</table>


