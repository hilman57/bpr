<fieldset  class='corner' style='margin:8px 15px 0px 10px;'>
	<legend class='icon-menulist'>&nbsp;&nbsp;Edit Read FTP </legend>
	<div>
		<form name="FtpEditTemplate">
		<table>
			<tr>
				<td class='text_caption bottom'>* Work Project </td>
				<td>
					<?php __(form()->hidden('ftp_read_id',null,$FtpReadData['ftp_read_id'])); ?>
					<?php __(form()->combo('ftp_read_project_code','select long', $WorkProject, $FtpReadData['ftp_read_project_code'])); ?>
					<td class='text_caption bottom'>* Template Name </td>
					<td><?php __(form()->combo('ftp_read_template_Id','select long',  $TemplateName, $FtpReadData['ftp_read_template_Id'])); ?></td>
				</td>
			</tr>
			<tr>
				<td class='text_caption bottom'>*  In/Out Directory</td>
				<td><?php __(form()->input('ftp_read_directory','select long',$FtpReadData['ftp_read_directory'])); ?></td>
				<td class='text_caption bottom'>*  History Directory</td>
				<td><?php __(form()->input('ftp_read_dir_history','select long',$FtpReadData['ftp_read_dir_history'])); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>* Read/ Create File</td>
				<td><?php __(form()->input('ftp_read_filetype','select long',$FtpReadData['ftp_read_filetype'])); ?></td>
				<td class='text_caption bottom'>* Controll File</td>
				<td><?php __(form()->input('ftp_read_ctltype','select long',$FtpReadData['ftp_read_ctltype'])); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>* Mode</td>
				<td><?php __(form()->combo('ftp_read_mode','select long', array('GET' => 'GET','PUT' => 'PUT'), $FtpReadData['ftp_read_mode'])); ?></td>
				<td class='text_caption bottom'>* UserPrivileges</td>
				<td><?php __(form()->combo('ftp_read_privilege','select long', $UserPrivileges, $FtpReadData['ftp_read_privilege'], array("change" => "Ext.DOM.WorkUserByLevel(this.value);"))); ?></td>
			</tr>
			<tr>
				<td class='text_caption bottom'></td>
				<td></td>
				<td class='text_caption bottom'>* User </td>
				<td><span id="divUserId"><?php __(form()->combo('ftp_read_userid','select long', $UserId, $FtpReadData['ftp_read_userid'])); ?></span></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>&nbsp;</td>
				<td>
					<?php __(form()->button('FtpUpdateButton','save button', 'Update', array("click" => "Ext.DOM.FtpEditButton();") )); ?>
					<?php __(form()->button('FtpCloseButton','close button', 'Cancel', array("click" =>"Ext.Cmp('span_top_nav').setText('');"))); ?>
				
					</td>
			</tr>
		</table>
		</form>
		
	</div>
</fieldset>