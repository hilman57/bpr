<?php 
// @ pack :  set align #------------------------------

 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');

// @ pack :  set align #------------------------------

 $labels =& page_labels();
 $primary =& page_primary();
 $hidden  =& page_hidden();
 // echo "<pre>";
 // print_r($hidden);
 // echo "</pre>";
  // echo "<pre>";
 // print_r(reset($hidden));
 // echo "</pre>";
// @ pack :  set align #------------------------------

 page_set_align('AccountNumber', 'left');
 page_set_align('CustomerName', 'left');
 page_set_align('PhoneBlock', 'left');
 page_set_align('deskol', 'center');
 page_set_align('source_complain', 'center');
 page_set_align('kasus_komplain', 'left');
 page_set_align('action_taken', 'left');
 page_set_align('result', 'left');
 page_set_align('nama_respon', 'left');
 page_set_align('complain_date', 'center');

// @ pack : set currency column in here 

 $call_user_func = array('complain_date'=>'_getDateTime','closed_date'=>'_getDateTime');
 $call_link_user = array('CustomerName');
 $call_link_nowarap = array('complain_date'=>'nowrap');
 $warna_pengingat = array("1"=>"#66ff66","2"=>"#ffff66","3"=>"#ff1a1a");
?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;<a href="javascript:void(0);" style="color:red;" onclick="Ext.Cmp('<?php __($primary);?>').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); if($Field != 'CallReasonId') { ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php  } endforeach;  ?>
	</tr>
</thead>	
<tbody>
<?php
$UI =& get_instance();

$no = $num;
$review = 1;
foreach( $page -> result_assoc() as $rows ) { 
// print_r($rows);
$warna=null;
$a = $UI -> EUI_Tools -> _DateDiff(date("Y-m-d H:i:s"),$rows['complain_date']);
$range = (int) $a['days_total'];
if( $range > 0 && $rows['respon']==$review )
{
	
	if(array_key_exists( $range ,$warna_pengingat))
	{
		$warna = $warna_pengingat[$range];
	}
	else
	{
		$warna = $warna_pengingat[3];
	}
}
 $color = ($no%2!=0?'#FFFEEE':'#FFFFFF'); ?>
 <tr class="onselect" bgcolor="<?php echo ( is_null($warna) ? $color : $warna ); ?>">
	<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary],NULL, NULL ); ?>
	<td class="content-middle center"> <?php __($no);?></td>
	<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field);  $align=&page_get_align(); ?>
	<td class="content-middle" <?php __($color) ?>  align="<?php __($align[$Field]); ?>" <?php echo $call_link_nowarap[$Field];?>>
		<?php if( in_array( $Field, $call_link_user ) ) : ?>
		<a href="javascript:void(0);" style="color:#289c05;text-decoration:none;" onclick="Ext.DOM.CallCustomer('<?php __($rows['deb_Id']); ?>');">  <?php __($rows[$Field]); ?></a>
		<?php else : ?>
		<?php __(page_call_function($Field, $rows, $call_user_func) ); ?>
		<?php endif; ?>	
	</td>
	<?php endforeach;?>
 </tr>	
</tbody>
<?php
	$no++;
}

?>
</table>


