<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	cpl_agent_id 		: "<?php echo _get_exist_session('cpl_agent_id');?>",
	cpl_campaign_id 	: "<?php echo _get_exist_session('cpl_campaign_id');?>",
	cpl_cust_id 		: "<?php echo _get_exist_session('cpl_cust_id');?>",
	cpl_cust_name 		: "<?php echo _get_exist_session('cpl_cust_name');?>",
	cpl_recsource 		: "<?php echo _get_exist_session('cpl_recsource');?>",
	cpl_start_date 		: "<?php echo _get_exist_session('cpl_start_date');?>",
	cpl_end_date 		: "<?php echo _get_exist_session('cpl_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/MgtComplainDebitur/index/',
	custlist : Ext.DOM.INDEX+'/MgtComplainDebitur/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('cpnCustomer').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('cpnCustomer').Clear();
	Ext.DOM.searchCustomer();
}

Ext.DOM.ShowComplain = function()
{
	var Complain = Ext.Cmp('IdComplain').getValue();
  
	if((Complain.length != 1)){
		Ext.Msg("Please select one row").Info();
		return false;
	}
	
	Ext.Window
	({
		url 	: Ext.DOM.INDEX+"/MgtComplainDebitur/EditComplain/",
		method  : 'POST',
		width 	: ($(window).width()-( $(window).width()/2)),
		height  : ($(window).height()),
		name 	: 'show_complain',
		scrollbars : 1,
		resizable  : 1, 
		param 	: {
			IdComplain : Complain, 
		}
	}).popup();
}

// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear'],['Show Complain']],
		extMenu  : [['searchCustomer'],['resetSeacrh'],['ShowComplain']],
		extIcon  : [['zoom.png'],['zoom_out.png'],['zoom_out.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});

 Ext.DOM.CallCustomer = function( CustomerId ) 
{
 if( CustomerId!='')  {	
	
	Ext.ActiveMenu().NotActive();
	Ext.ShowMenu( new Array('ModContactDetail','index'), 
		Ext.System.view_file_name(), 
	{
		CustomerId : CustomerId,
		ControllerId : Ext.DOM.navigation.custnav
	}); 
 } else { 
	Ext.Msg("No Customers Selected !").Info(); 
 }
 
}
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="cpnCustomer">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('cpl_campaign_id','select auto', $CampaignId, _get_exist_session('cpl_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('cpl_agent_id','select auto', $Deskoll, _get_exist_session('cpl_agent_id'));?></td>
			
		</tr>
		<tr>
			<td class="text_caption bottom"> # Pelanggan ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('cpl_cust_id','input_text long', _get_exist_session('cpl_cust_id'));?></td>
			<td class="text_caption bottom"> # Tanggal Panggilan Terakhir &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('cpl_start_date','input_text date', _get_exist_session('cpl_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('cpl_end_date','input_text date', _get_exist_session('cpl_end_date'));?>
			</td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Nama Pelanggan&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('cpl_cust_name','input_text long', _get_exist_session('cpl_cust_name'));?></td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->