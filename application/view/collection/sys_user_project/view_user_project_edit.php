<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
<fieldset class="corner" style="background-color:white;margin:3px;">
 <legend class="icon-application">&nbsp;&nbsp;&nbsp;Edit User Work Project</legend>
  <form name="frmEditUserWorkProject">
 <?php echo form() -> hidden('WorkId', null, $WorkUser['WorkId']);?>
	<table cellpadding="6px;" cellspacing="3px">
		<tr>
			<td class="text_caption">* User Privileges </td>
			<td><?php echo form()->combo('PrivilegeId','select long',$UserPrivileges, $WorkUser['handling_type'], array("change" => "Ext.DOM.WorkUserByLevel(this.value);") );?></td>
		</tr>
		<tr>
			<td class="text_caption">* User Regitration ID </td>
			<td><span id="divUserId"><?php echo form()->combo('UserId','select long',$UserRegistration, $WorkUser['UserId']);?></span></td>
		</tr>
		<tr>
			<td class="text_caption" >* Work Project</td>
			<td><?php echo form()->combo('ProjectId','select long', $UserWorkProject,$WorkUser['ProjectId']);?></td>
		</tr>
		<tr>
			<td class="text_caption">* Status</td>
			<td><?php echo form()->combo('Status','input_text date', array('1'=>'Active', '0' => 'Not Active'), $WorkUser['Status']);?></td>
		</tr>
		<tr>
			<td class="text_caption">&nbsp;</td>
			<td>
				<input type="button" class="update button" onclick="Ext.DOM.UpdateUserWorkProject();" value="Update">
				<input type="button" class="close button" onclick="Ext.Cmp('tpl_header').setText('');" value="Close">
			</td>
		</tr>
	</table>
	</form>
	</fieldset>
</div>