<div id="result_content_add" class="box-shadow" style="margin-top:10px;">
<fieldset class="corner" style="background-color:white;margin:3px;">
 <legend class="icon-application">&nbsp;&nbsp;&nbsp;Add User Work Project</legend>
 <form name="frmUserWorkProject">
	<table cellpadding="6px;">
		<tr>
			<td class="text_caption">* User Privileges </td>
			<td><?php echo form()->combo('PrivilegeId','select long',$UserPrivileges, null, array("change" => "Ext.DOM.WorkUserByLevel(this.value);") );?></td>
		</tr>
		<tr>
			<td class="text_caption">* User Regitration ID </td>
			<td><span id="divUserId"><?php echo form()->combo('UserId','select long',$UserRegistration);?></span></td>
		</tr>
		<tr>
			<td class="text_caption" >* Work Project</td>
			<td><?php echo form()->combo('ProjectId','select long', $UserWorkProject);?></td>
		</tr>
		<tr>
			<td class="text_caption">* Status</td>
			<td><?php echo form()->combo('Status','input_text date', array('1'=>'Active', '0' => 'Not Active'));?></td>
		</tr>
		<tr>
			<td class="text_caption">&nbsp;</td>
			<td>
				<input type="button" class="save button" onclick="Ext.DOM.SaveUserWorkProject();" value="Save">
				<input type="button" class="close button" onclick="Ext.Cmp('tpl_header').setText('');" value="Close">
			</td>
		</tr>
	</table>
	</form>
</div>