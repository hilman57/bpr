<form name="frmAddWorkUser">
<div  style="padding:10px;">
 <fieldset class="corner">
 <legend class="icon-application"> &nbsp;&nbsp;&nbsp;Add Menu Work Project</legend>
 
 <table>
	<tr> 
		<td>
			<div>
			<table cellspacing="2" width="60%" Border=0 style="margin-top:-5px;">
			<tr>
				<td class="text_caption bottom bottom" nowrap>* Project Name</td>
				<td><?php echo form()->combo('WorkProjectId','select long', $WorkProjectId, null, array('change' => 'Ext.DOM.ApplicationMenuByProject(this);') );?></td>
			</tr>
			
			<tr>
				<td class="text_caption bottom" width="3%" nowrap>* Application Menu </td>
				<td width="70%"> <?php echo form()-> listcombo('ApplicationMenu', 'select long', $ApplicationMenu);?> </td>
			</tr>
			<tr>
				<td class="text_caption" width="3%" nowrap>&nbsp;</td>
				<td width="70%"> <?php echo form()-> button('Assign', 'button assign', 'Assign',  array('click' => 'Ext.DOM.AssignMenuWorkProjectById();')  );?> </td>
			</tr>
		</table>
		</div>
		</td>
		<td>
			<div>
				<table cellspacing="2" width="60%" Border=0 style="margin-top:-5px;">
					<tr>
						<td class="text_caption" width="3%" nowrap>* Available </td>
						<td width="70%"> 
							<div id="ApplicationWorkMenu"> <?php echo form()-> combo('MenuWorkProjectId', 'select', array());?></div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text_caption" width="3%" nowrap>&nbsp;</td>
						<td width="70%"> 
							<?php echo form()-> button('Remove', 'button close', 'Remove', array('click' => 'Ext.DOM.RemoveMenuWorkProjectById();') );?> 
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	
 </table>
 
</fieldset>
</div>
</form>

<!-- END OF FILE -->
<!-- location : ./application/view/user/view_add_user.php