<?


/* reconstruct date */
$sdates = explode("-", $start_date);
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];

if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}

function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}
	
	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}  


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];
	
	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);
	
	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}
	
	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;
	
	return $yyyy."-".$mm."-".$dd;
}

function printNote(){
}

function switchNumber($x,$y){
			switch($y){
				case 1: return ($x<33? 'NEED IMPROVEMENT' : 'GOOD');	break;
				case 2: return ($x<28? 'NEED IMPROVEMENT' : 'GOOD');	break;
				case 3: return ($x<110?'NEED IMPROVEMENT' : 'GOOD');	break;
				case 4: return ($x<79? 'NEED IMPROVEMENT' : 'GOOD');	break;
				case 5: return ($x<60? 'NEED IMPROVEMENT' : 'GOOD');	break;
				case 6: return ($x<18? 'NEED IMPROVEMENT' : 'GOOD');	break;
			}
		}

function NilaiReview($CategoryId='',$Point=''){
	$sql =  "select f.CollCategoryId, f.CollCategoryName, SUM(b.NumberValue) as sizeByCategory
			 from coll_point_collmon a left join coll_number_collmon b on a.PointNumber=b.NumberId 
			 LEFT JOIN coll_calltype_collmon c on a.CollCallTypeId=c.CallTypeId 
			 left join coll_status_collmon d on a.CollStatusId=d.CollStatusId 
			 left join coll_subcategory_collmon e on a.SubCategoryId=e.SubCategoryId
				left join coll_category_collmon f on f.CollCategoryId=e.CategoryId 
				where a.PointId in($Point) 
				and f.CollCategoryId=$CategoryId
				group by f.CollCategoryId ";
			
			$qry = @mysql_query($sql);
			if( $qry && ( $row = @mysql_fetch_assoc($qry)) ){
				$datas[ $row['CollCategoryId'] ]['name']= $row['CollCategoryName'];
				$datas[ $row['CollCategoryId'] ]['size']= $row['sizeByCategory'];	
			}
			
			return $datas;
}
		
function GetCallType($Point){
	$datas = array();
	$sql = " select c.CallTypeName,c.CallTypeId, d.CollStatusName, d.CollStatusId from coll_point_collmon a 
							left join coll_number_collmon b on a.PointNumber=b.NumberId
				left join coll_calltype_collmon c on a.CollCallTypeId=c.CallTypeId
				left join coll_status_collmon d on a.CollStatusId=d.CollStatusId
				where a.PointId in($Point) 
				group by d.CollStatusId ";
	
	$qry = @mysql_query($sql);
	if( $qry && ( $row = @mysql_fetch_assoc($qry)) ){
		$datas['CallTypeName'] 	 	= $row['CallTypeName'];
		$datas['CollStatusName'] 	= $row['CollStatusName'];	
		$datas['CallTypeId'] 		= $row['CallTypeId'];	
		$datas['CollStatusId'] 		= $row['CollStatusId'];	
	}	

	return $datas;
}


function getCategory(){
	$sql = " select * from coll_category_collmon";
	$res = @mysql_query($sql);
	while( $row = mysql_fetch_assoc($res)){
		$datas[$row['CollCategoryId']]= $row['CollCategoryName']; 
	}
	return $datas;
}

function showReport($start_date, $end_date, $GroupCallCenter, $vAgentId){	
	global $paymentChannels;
		$category  = getCategory();
		$category2  = getCategory();
		echo "<table class=\"data\" border=0 style=\"border-left:1px solid #000000;border-top:0px; \" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">"
			 ."<tr>"
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" width=\"20\" rowspan=\"2\">No.</td>"
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" width=\"100\" rowspan=\"2\"> Customer ID </td>"
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" rowspan=\"2\" rowspan=\"2\"> Customer Name </td>"	  
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" rowspan=\"2\" rowspan=\"2\"> TL </td>"
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" rowspan=\"2\" rowspan=\"2\"> CallMon Type </td>"
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" rowspan=\"2\" rowspan=\"2\"> CallMon Status </td>"		
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" rowspan=\"2\" rowspan=\"2\"> CallMon Date </td>"				
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" colspan=\"6\" align=\"center\">Point </td>"	 
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" colspan=\"6\" align=\"center\">Result </td>"	 	 
					."<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\" rowspan=\"2\"> Total Score </td>"
				."</tr><tr>";
				
					foreach( $category as $key=>$CatName){
						echo "<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\">{$CatName}</td>";
					}
					
					foreach( $category2 as $key=>$CatName){
						echo "<td class=\"head\" style=\"border-right:1px solid #000000;border-top:1px solid #000000;\">{$CatName}</td>";
					}
					
				echo "</tr>";
		
		if($GroupCallCenter){
			$fromTbl = ", cc_agent d ";
			if(count($vAgentId) and $vAgentId[0]){
				$agentlist = implode(",", $vAgentId);
				$cond = 'AND a.agent = d.userid AND d.id IN ('.$agentlist.')';
			}else{
				$cond = 'AND a.agent = d.userid AND d.agent_group ='.$GroupCallCenter;
			}
		}
		
		$sql = "select a.*, c.accstatus, e.reff_name as acc_status, c.* from coll_transaction_collmon a 
							left join coll_debitur c on a.TransDebiturId=c.cardno 
							left join cc_agent d on a.TransCreateUserId=d.id 
							left join coll_reference e on c.accstatus=e.reff_code 
							where 
								a.TransRecId is not null  
								and c.cardno is not null
								and e.reff='sg'
								and date(TransCreateDate)>='$start_date'
								and date(TransCreateDate)<='$end_date'"; 
		//echo $sql;						
		$res = @mysql_query($sql);
		$cnt = 0;
		$dcr = "UNKNOWN";		
		$subtotal = 0;
		$grandtotal = 0;
		$cnt = 1;
		while ($row = @mysql_fetch_array($res)) {
			
			$size_datas = GetCallType($row['TransRangePoint']);
			
			echo '<tr>'
					 .'<td nowrap style="border-right:1px solid #000000;">'.$cnt.'</td>'
					 .'<td style="border-right:1px solid #000000;border-top:1px solid #000000;" nowrap>'.$row['cardno'].'</td>'
					 .'<td style="border-right:1px solid #000000;border-top:1px solid #000000;" nowrap>'.$row['fullname'].'</td>'
					 .'<td style="border-right:1px solid #000000;border-top:1px solid #000000;" nowrap>'.strtoupper($row['TransCreateUserName']).'</td>'
					 .'<td style="border-right:1px solid #000000;border-top:1px solid #000000;" nowrap>'.$size_datas['CallTypeName'].'</td>'
					 .'<td style="border-right:1px solid #000000;border-top:1px solid #000000;" nowrap>'.$size_datas['CollStatusName'].'</td>'
					 .'<td style="border-right:1px solid #000000;border-top:1px solid #000000;" nowrap>'.$row['TransCreateDate'].'</td>';
					 
					$i=1;
					$tot_mon = 0;
					foreach( $category2 as $key=>$CatName){
						$y_datas = NilaiReview($i,$row['TransRangePoint']);
						$tot_mon += $y_datas[$i]['size'];
						
						echo '<td style="text-align:right;border-right:1px solid #000000;border-top:1px solid #000000;">'.$y_datas[$i]['size'].'</td>';
						$i++;
					}
					
					
					$z=1;
					foreach( $category2 as $key=>$CatName){
						echo '<td style="border-right:1px solid #000000;border-top:1px solid #000000;" >'.switchNumber($y_datas[$z]['size'],$z).'</td>';
						$z++;
					}
					
					echo '<td style="text-align:right;border-right:1px solid #000000;border-top:1px solid #000000;">'.number_format(($tot_mon?($tot_mon/$i):0),1).'</td>';
					
				echo '</tr>';
			
			$subtotal += $row['amount'];
			$grandtotal += $row['amount'];
			$cnt++;
		}

		echo "</table><br>";

	
	$_SESSION['xl_start_date'] 	= $start_date;
	$_SESSION['xl_end_date'] 		= $end_date;
	$_SESSION['xl_group'] 			= $group;
	$_SESSION['xl_agents'] 			= $agents;		
		
}
?>
<html>
	<head>
		<title>
			Enigma Collection Report - Report CallMon Review
		</title>
		<style>
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;	
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
}

td.head2 {
	background-color: AAEEEE;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}
			-->
			</style>
	</head>
<body>
	<h1><u>Report CallMon Review</u></h1>
	<?php	
		echo "<h2>Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}</h2>";
		//echo "<hr size=1>";	
	showReport($start_date, $end_date, $GroupCallCenter, $AgentId);
	
	//echo $_SESSION['xl_rep_title'] = "Report from ${sdates[0]}-${sdates[1]}-${sdates[2]} to ${edates[0]}-${edates[1]}-${edates[2]}";	
	printNote();
	?>	
</body>
</html>