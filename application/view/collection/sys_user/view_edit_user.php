<form name="frmEditUser">
<div class="box-shadow" style="padding:10px;">
 <fieldset class="corner">
 <legend class="icon-application"> &nbsp;&nbsp;&nbsp;Edit User</legend>
<table cellspacing="2" width="50%" border=0 style="margin-top:-5px;">
	<tr>
		<td class="text_caption bottom"width="2%" nowrap>* UserId &nbsp;:</td>
		<td width="70%">
			<?php echo form()->hidden('UserId', 'input_text long',$rows['UserId']);?> 
			<?php echo form()->hidden('CcUserId', 'input_text long',$rows['Username']);?> 
			<?php echo form()->input('textUserid', 'input_text long',$rows['Username'], null, array('style' => 'border:1px solid red;'));?> 
		</td>
		<td class="text_caption bottom" nowrap>Quality Head &nbsp;:</td>
		<td><?php echo form()->combo('quality_head','select long', $User -> _get_quality_head(), $rows['quality_id']);?></td>
		
	</tr>
	
	<tr>
		<td class="text_caption bottom" nowrap>* User Code &nbsp;:</td>
		<td width="70%"><?php echo form()->input('textAgentcode','input_text long',$rows['code_user'],null, array('style' => 'border:1px solid red;'));?></td>
		<td class="text_caption bottom" nowrap>Telphone &nbsp;:</td>
		<td><?php echo form()->combo('user_telphone','select long', $User -> _get_telephone(), $rows['telphone'],null, array('style' => 'border:1px solid red;'));?></td>
	</tr>
	
	
	<tr>
		<td class="text_caption bottom" nowrap>Online Name &nbsp;:</td>
		<td width="70%"><?php echo form()->input('textOnlineCode','input_text long',$rows['init_name'],null, array('style' => 'border:1px solid red;'));?></td>
		<td class="text_caption bottom" nowrap>CC Group &nbsp;:</td>
		<td><?php echo form()->combo('cc_group','select long', $User -> _get_agent_group(), $rows['agent_group'], null, array('style' => 'border:1px solid red;'));?></td>
	</tr>
	<tr>
		<td class="text_caption bottom" nowrap>Fullname &nbsp;:</td>
		<td width="70%"><?php echo form()->input('textFullname','input_text long',$rows['full_name'],null, array('style' => 'border:1px solid red;'));?></td>
	</tr>

	<tr>
		<td class="text_caption bottom" nowrap>Cabang &nbsp;:</td>
		<td><?php echo form()->combo('cabang','select long', $User -> _cabang(), $rows['code_cabang'],null, array('style' => 'border:1px solid red;'));?></td>
	</tr>
	
	<tr>
		<td class="text_caption bottom" nowrap>Previleges &nbsp;:</td>
		<td><?php echo form()->combo('user_profile','select long', $User -> _get_handling_type(), $rows['handling_type'], null, array('style' => 'border:1px solid red;'));?></td>
		<td class="text_caption" nowrap>&nbsp;</td>
		<td rowspan=7 valign='top'>
			<input type="button" class="update button" onclick="Ext.DOM.UpdateUser();" value="Update">
			<input type="button" class="close button" onclick="Ext.Cmp('panel-content').setText('');" value="Close">
		</td>
		
	</tr>
	<tr>
		<td class="text_caption bottom" nowrap>Senior Leader &nbsp;:</td>
		<td><?php echo form()->combo('senior_leader','select long', $User -> _get_seniorleader(), $rows['tl_id'], NULL);?></td>
	</tr>
	<tr>
		<td class="text_caption bottom" nowrap>System Leader &nbsp;:</td>
		<td><?php echo form()->combo('team_leader','select long', $User -> _get_teamleader(), $rows['tl_id'], NULL);?></td>
	</tr>
	<tr>
		<td class="text_caption bottom" nowrap>System Supervisor &nbsp;:</td>
		<td><?php echo form()->combo('user_spv','select long', $User -> _get_supervisor(), $rows['spv_id'], null, array('style' => 'border:1px solid red;'));?></td>
	</tr>
	<tr>
		<td class="text_caption bottom" nowrap>System Manager &nbsp;:</td>
		<td><?php echo form()->combo('user_mgr','select long', $User -> _get_manager(), $rows['mgr_id'], null, array('style' => 'border:1px solid red;'));?></td>
	</tr>
	
	<tr>
		<td class="text_caption bottom" nowrap>Account Manager &nbsp;:</td>
		<td><?php echo form()->combo('account_manager','select long', $User -> _get_account_manager(), $rows['act_mgr'], null, array('style' => 'border:1px solid red;'));?></td>
	</tr>
	
	<tr>
		<td class="text_caption bottom" nowrap>System Admin &nbsp;:</td>
		<td><?php echo form()->combo('user_admin','select long', $User -> _get_admin(), $rows['admin_id'] );?></td>
	</tr>
	
</table>
<div style="font-size:11px;COLOR:RED;line-height:24px;padding-left:12px;">
		<span> <u>Notes : </u></span><br>
		<SPAN STYLE="font-size:11px;font-style:normal;"> * ) <b>UserId </b> - for login to application  </span>
	</div>	
</fieldset>
</div>
</form>

<!-- END OF FILE -->
<!-- location : ./application/view/user/view_add_user.php