<script>

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
function ShowAgentPerTL() {
	
	$("#content-deskoll").load(Ext.EventUrl(new Array('ReportCpaSummary','ShowAgentPerTL') ).Apply(), 
		{ TL : Ext.Cmp('user_tl').getValue()}, 
		function( response, status, xhr ) 
	{
		if( status == 'success' ){
			$("#user_agent").toogle();
		}		
	});
}

	
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */

function ShowReportType(){
  var report_type = Ext.Cmp('report_type').getValue();
  
 // -------------------------------- test ------------------------
  ShowAgentPerTL();
 
  // if( report_type=='tracking-report') {
	  // Ext.Cmp("report_group").disabled(0);
	  // Ext.Cmp("user_tl").disabled(0);
	  // Ext.Cmp("user_agent").disabled(0);
	  // Ext.Cmp("report_mode").disabled(0);
		
 // } else{
	  // Ext.Cmp("report_group").disabled(1);
	  // Ext.Cmp("user_tl").disabled(1);
	  // Ext.Cmp("user_agent").disabled(1);
	  // Ext.Cmp("report_mode").disabled(1);
	  // Ext.Cmp("user_tl").setValue('');
	  // Ext.Cmp("user_agent").setValue('');
	  // Ext.Cmp("report_mode").setValue('summary');
 // }
 
}

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 function ShowReport( Group )  
{
  var frmReport = Ext.Serialize('frmReport');
  var VarReport = new Array();
   
  if( !frmReport.Required( new Array('start_date1','start_date2','end_date1','end_date2') ) ){
	Ext.Msg("Input Not Complete").Info();
	return false;
  }	 

  
  Ext.Window  ({
	url 	: Ext.EventUrl(new Array('ReportInventory','ShowReport') ).Apply(),
	param   : Ext.Join([
		frmReport.getElement(), VarReport
	]).object()
  }).newtab();
  
}



// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
 function ShowExcel ()
{
  var frmReport = Ext.Serialize('frmReport');
  var VarReport = new Array();
  
  if( !frmReport.Required( new Array('start_date1','start_date2','end_date1','end_date2') ) ){
	Ext.Msg("Input Not Complete").Info();
	return false;
  }	 
  
  
  Ext.Window  ({
	url 	: Ext.EventUrl(new Array('ReportInventory','ShowExcel') ).Apply(),
	param   : Ext.Join([
		frmReport.getElement() ,VarReport
	]).object()
  }).newtab();
 
}
 
 
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 $('document').ready(function()
{
	
var _offset = 22;
  $('.date').datepicker 
 ({
	showOn : 'button', 
	buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
	buttonImageOnly	: true, 
	changeYear : true,
	changeMonth : true,
	dateFormat : 'dd-mm-yy',
	readonly:true
});




 $("#user_tl").toogle();
 $("#user_agent").toogle();
 $("#product").toogle();
});

</script>
<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Reporting Inventory</span></legend>
<div id="panel-agent-content">
	<table border=0 width='100%'>
		<tr>
			<td width='35%' valign="top"><?php $this->load->view('rpt_inventory/page/report_inventory_group');?></td>
			<td width='65%' valign="top"><?php //$this->load->view('rpt_inventory/page/report_call_activity_content');?></td>
		</tr>
	</table>
</div>
</fieldset>
