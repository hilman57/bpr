<?php echo base_window_header("Search All Debitur"); ?>
<style> 
.ui-chrome{ border-radius:3px;}
.ui-chrome:hover{ border:1px solid #FF4321;}
.ui-context-chrome { border-radius:4px;height:22px;}
</style>
<script>


function Minimize()
{
	window.innerWidth = 100;
	window.innerHeight = 200;
	window.screenX = screen.width;
	window.screenY = screen.height;
	alwaysLowered = true;
}

// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewAllDebitur 
 */
 
 function AllDebiturDetail( obj )
{ 
	window.opener.Ext.ActiveMenu().NotActive();
	window.opener.Ext.ShowMenu( new Array('ModContactDetail','index'), 
		window.opener.Ext.System.view_file_name(), 
	{
		CustomerId : obj.CustomerId,
		ControllerId : window.opener.Ext.EventUrl(new Array('SrcCustomerList','index') ).Apply(),
	}); 
	Minimize();
	
	//window.close(this);
}

// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewAllDebitur 
 */
 
 function ViewAllDebitur( obj )
{
	var frmAllDebitur = Ext.Serialize("frmAllDebitur");
	
	$("#ui-widget-content-all-debitur").waiter 
	({
		url 	: new Array('SysAllDebitur','SysPageAllDebitur'),
		param 	: Ext.Join( new Array( frmAllDebitur.getElement())).object(),
		order   : {
			order_type : obj.type,
			order_by   : obj.orderby,
			order_page : obj.page	
		}, 
		complete : function( obj ) {
			//alert("hello");
			// var total_swap = $('#ui-total-swap-record').text();
				// amount_data.total_amount = parseInt(total_swap);
				// amount_data.total_checked = 0;
				
			$(obj).css({"height" : "100%", "padding" : "0px -5px 4px -5px" });
			// $("#swp_total_data").val(total_swap);
			// $("#swp_total_data").attr("disabled", true);
		}	
	});

}
// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewAllDebitur 
 */
 
function EventShowAllDebitur(){
	new ViewAllDebitur ({ orderby : '',  type: '', page: 0	 });
}

// ------------------------------------------------------------------------------------------------
/*
 * @ pack 		ViewAllDebitur 
 */
 
$(document).ready(function(){
	new ViewAllDebitur ({ orderby : '',  type: '', page: 0	 });
});
</script>
</head>
<body style="margin:10px 5px 5px 5px;">
<fieldset class="corner" style="border-radius:3px;">
	<legend class="icon-customers"> 
		<span style="margin-left:10px;">Search All Debitur</span></legend>
		
	<div class="ui-widget-form-table" style="margin-left:4px;margin-top:-2px;width:99%;border:0px solid #000;">
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell">
			
				<fieldset class="corner" style="border-radius:3px;">
				<!-- darrii -------->
				<form name="frmAllDebitur">
				
				<div class="ui-widget-form-table" style="margin:5px 2px 5px 2px;">
					<div class="ui-widget-form-row">
						<div class="ui-widget-form-cell text_caption right">Product</div>
						<div class="ui-widget-form-cell center ">:</div>
						<div class="ui-widget-form-cell"><?php echo form()->combo("deb_campaignid", "select long ui-chrome", array(1=> 'Card', 2=>'CF'), null, null, array("style"=> "width:200px;height:24px;") );?></div>
						<div class="ui-widget-form-cell text_caption right">CustomerId</div>
						<div class="ui-widget-form-cell center">:</div>
						<div class="ui-widget-form-cell"><?php echo form()->input("deb_acct_no", "input_text long ui-chrome",null, null, array("style"=> "width:200px;") );?></div>
						<div class="ui-widget-form-cell text_caption">Customer Name</div>
						<div class="ui-widget-form-cell center">:</div>
						<div class="ui-widget-form-cell"><?php echo form()->input("deb_name", "select long ui-chrome", null, null, array("style"=> "width:200px;") );?></div>
						<div class="ui-widget-form-cell text_caption"><?php echo form()->button("ButonAllData", "button search ui-context-chrome", "Search", array("click" => "new EventShowAllDebitur();") );?></div>
					</div>
				</div>
				</form>
				</fieldset>
			</div>
		</div>
		
		<div class="ui-widget-form-row">
			<div class="ui-widget-form-cell">
			
				<fieldset class="corner" style="border-radius:3px;">
				<legend> List Debitur</legend>
				<!-- darrii -------->
					<div style="margin-top:-5px;" id="ui-widget-content-all-debitur"></div>
				</fieldset>
				
			</div>
		</div>
		
	</div>
	
</fieldset>	
</body>
</html>