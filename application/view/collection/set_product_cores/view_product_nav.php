<?php
/*
 * E.U.I 
 *
 * subject	: view_core_nav ( files )
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
echo javascript();
?>
<script type="text/javascript">

/* *  START : extToolbars  * */

 $(function(){
	$('#toolbars').extToolbars({
		extUrl  : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle:[['Show Grid'],['Hide Grid'],['Save Product']],
		extMenu :[['ShowGrid'],['HideGrid'],['SaveProduct']],
		extIcon :[['accept.png'],['cancel.png'],['cancel.png']],
		extText :true,
		extInput:true,
		extOption:[]
	});
});

/* *  END : extToolbars  * */

Ext.DOM.setProduct = function(opts)
{
	if( opts.value==1) 
	{
		Ext.Css('text_product_plan').style({ borderColor : 'silver'});
		Ext.Css('text_product_age').style({ borderColor : 'silver'});
		Ext.Cmp('paymode').disabled(true);
		Ext.Cmp('GroupPremi').disabled(true);
		Ext.Cmp('text_product_plan').disabled(true);
		Ext.Cmp('text_product_age').disabled(true);
		Ext.Cmp('text_product_plan').setValue('');
		Ext.Cmp('text_product_age').setValue('');
		Ext.Cmp('paymode').setUnchecked();
		Ext.Cmp('GroupPremi').setUnchecked();
	}
	else
	{
		Ext.Css('text_product_plan').style({ borderColor : 'red'});
		Ext.Css('text_product_age').style({ borderColor : 'red'});
		Ext.Cmp('paymode').disabled(false);
		Ext.Cmp('GroupPremi').disabled(false);
		Ext.Cmp('text_product_plan').disabled(false);
		Ext.Cmp('text_product_age').disabled(false);
		Ext.Cmp('paymode').setUnchecked();
		Ext.Cmp('GroupPremi').setUnchecked();
	}
}

/* *  END : extToolbars  * */


/* *  START : ShowGrid  * */
Ext.DOM.ShowGrid = function(){

 /** Ext.Ajax **/
	Ext.Ajax({
		url : Ext.DOM.INDEX+'/SetProduct/ShowGrid',
		method :'POST',
		param :{
			ProductPlan : Ext.Cmp('text_product_plan').getValue(),
			ProductAge  : Ext.Cmp('text_product_age').getValue()
		}
	}).load('html_grid_age');
	
  /** Ext.Ajax **/
	
	Ext.Ajax({
		url : Ext.DOM.INDEX+'/SetProduct/GridContent',
		method :'POST',
		param :{
			ProductPlan : Ext.Cmp('text_product_plan').getValue(),
			ProductAge  : Ext.Cmp('text_product_age').getValue(),
			PayMode		: Ext.Cmp('paymode').getValue(),
			GroupPremi  : Ext.Cmp('GroupPremi').getValue(),
			Gender		: Ext.Cmp('GenderId').getValue()
		}
	}).load('html_grid_premi');
}
/* *  END : ShowGrid  * */


/* *  START : setInsert  * */

Ext.DOM.setInsert = function( Type, opts )
{

var 
 PayMode 	 = Ext.Cmp('paymode').getValue(),
 GroupPremi  = Ext.Cmp('GroupPremi').getValue(),
 ProductAge  = Ext.Cmp('text_product_age').getValue(),
 ProductPlan = Ext.Cmp('text_product_plan').getValue(),
 Gender 	 = Ext.Cmp('GenderId').getValue();
 
// -- step1 : start_date  	

 if( Type.toUpperCase() =='START') 
  {
	var i = 0;
	while( i < PayMode.length )
	{
/* 
 * @ params 	: GroupPremi 
 * @ params		: PayMode
 * @ params		: Age
 * @ param		: Gender	
 */ 
		if(GroupPremi.length > 0 )
		{
			var g = 0;
			while(g< GroupPremi.length)
			{
			   if( Gender.length > 0 )
			   {	
				 for( var ux in Gender )
				 {	
					var p = []; p = opts.name.split('_');
					Ext.Cmp("start_age_"+ PayMode[i] +"_"+ GroupPremi[g]+"_"+Gender[ux]+"_"+ p[p.length-1]).setValue(opts.value);
				}	
			  }
			  else {
					var p = []; p = opts.name.split('_');
					Ext.Cmp("start_age_"+ PayMode[i] +"_"+GroupPremi[g]+"_"+p[p.length-1]).setValue(opts.value);
			  }
			g++;
		  }
	  }
/* 
 * @ params 	: GroupPremi 
 * @ params		: PayMode
 * @ params		: Age
 */
		else
		{
			var p = []; p = opts.name.split('_');
			if( Gender.length > 0 ){ 
				for( var ux in Gender ) {
					Ext.Cmp("start_age_"+PayMode[i]+"_"+Gender[ux]+"_"+p[p.length-1]).setValue(opts.value);	
				}
			}
			else{
				Ext.Cmp("start_age_"+ PayMode[i] +"_"+ p[p.length-1] ).setValue(opts.value);	
			}
		}
	   i++;
	}	
  }
  
// -- step2 : end_date 
	
 if( Type.toUpperCase() =='END') 
 {
	var i = 0;
	while( i < PayMode.length )
	{
	
/* @ params 	: GroupPremi 
 * @ params		: PayMode
 * @ params		: Age
 */
		if(GroupPremi.length > 0)
		{
			var g = 0;
			while(g < GroupPremi.length)
			{
				 if( Gender.length > 0 )
				 {
					for( var ux in Gender )
					{	 
						var p = []; p = opts.name.split('_');
						Ext.Cmp("end_age_"+ PayMode[i] +"_"+ GroupPremi[g]+"_"+Gender[ux]+"_"+p[p.length-1]).setValue(opts.value);
					}	
				 }	
				 else {
					var p = []; p = opts.name.split('_');
						Ext.Cmp("end_age_"+ PayMode[i] +"_"+ GroupPremi[g]+"_"+p[p.length-1]).setValue(opts.value);
				 }
				g++;
			}
		}
/* @ params		: PayMode
 * @ params		: Age
 */
		else
		{
			var p = []; p = opts.name.split('_');
			if( Gender.length > 0 ){ 
				for( var ux in Gender ) {
					Ext.Cmp("end_age_"+PayMode[i]+"_"+Gender[ux]+"_"+p[p.length-1]).setValue(opts.value);	
				}
			}
			else{
				Ext.Cmp("end_age_"+ PayMode[i] +"_"+ p[p.length-1] ).setValue(opts.value);	
			}
		}
		i++;
	}	
 }
}

Ext.DOM.HideGrid = function(){
	Ext.Cmp('html_grid_age').setText('');
	Ext.Cmp('html_grid_premi').setText('');
}


Ext.DOM.SaveProduct = function()
{
	var form_age_range  = Ext.Serialize("form_age_range").getElement();
	var form_grid_content = Ext.Serialize("form_grid_content").getElement();
	
	var form_post_action = Ext.Join( new Array(form_age_range, form_grid_content ) ).http();
	
	Ext.Ajax({
		url		: Ext.DOM.INDEX+'/SetProduct/SaveProduct',
		method 	: 'GET',
		param   : {
			post_data 	 	: form_post_action,
			CreditShield 	: Ext.Cmp('credit_shield_name').getValue(),
			ProductId 	 	: Ext.Cmp('text_product_id').getValue(),
			ProductName  	: Ext.Cmp('text_product_name').getValue(),
			ProductCores	: Ext.Cmp('select_cmp_core').getValue(),
			ProductStatus 	: Ext.Cmp('status').getValue(),	
			PayMode 		: Ext.Cmp('paymode').getValue(),
			GroupPremi 		: Ext.Cmp('GroupPremi').getValue(),
			ProductPlan 	: Ext.Cmp('text_product_plan').getValue(),
			RangeAge 		: Ext.Cmp('text_product_age').getValue(),
			ProductType 	: Ext.Cmp('select_product_type').getValue(),
			Gender 	 		: Ext.Cmp('GenderId').getValue()
			
		},
		ERROR :function(e) {
			var ERR = JSON.parse(e.target.responseText);
			if( ERR.success ){
				alert("Success, Add Prodcut !");
				return;
			}
			else{
				alert("Failed, Add prodcut.\nPlease check your field !");
				return;
			}
		}
	}).post();
}

// # END OF FILE 
// # END SCRIPT 

</script>
<fieldset class="corner">
<legend class="icon-product"> &nbsp;&nbsp;&nbsp;Add Product</legend>
	<div id="toolbars" style="margin-bottom:8px;"></div>
	<form name="options_grid">
	<div id="cmp_top_content" class="box-shadow" style="width:'100%;';height:auto;overflow:auto;padding:8px;margin:5px;">
		<table width="99%" border="0" align="center">
		<tr>
			<td valign="top" width="40%">	
			<div>
				<table cellpadding="5px" align="left">
					<tr>
						<td class="text_caption bottom"> Credit Shield</td>
						<td><?php echo form() -> combo('credit_shield_name', 'input_text long', array('1'=> 'YES', '0'=>'NO'),null,array("change"=>"setProduct(this);") ); ?></td>
					</tr>
					<tr>
						<td class="text_caption bottom"> * Product ID</td>
						<td> <?php echo form() -> input('text_product_id', 'input_text long',null, null, array('length'=>30,'style'=>'height:18px;') ); ?> &nbsp;( 30 )</td>
					</tr>
					<tr>
						<td class="text_caption bottom" valign="top"> * Product Paymode</td>
						<td>
							<?php foreach( $data -> _get_select_paymode() as $k => $v ){ ?>
								<?php echo form() -> checkbox('paymode', 'select long',$k, null, array('label'=> $v) ); ?> <br>
							<? } ?>
						</td>
					</tr>
					<tr>
						<td class="text_caption bottom" > * Campaign Core</td>
						<td><?php echo form() -> combo('select_cmp_core', 'select long', $data -> _get_select_cores()); ?></td>
					</tr>
					<tr>
						<td class="text_caption bottom" valign="top"> * Premi Group </td>
						<td>
							<?php foreach( $data -> _get_select_premi_group() as $k => $v ){ ?>
								<?php echo form() -> checkbox('GroupPremi', 'select long',$k, null, array('label'=> $v) ); ?> <br>
							<? } ?>
						</td>
					</tr>		
					<tr>
						<td class="text_caption bottom" > * Product Plan </td>
						<td> <?php echo form() -> input('text_product_plan', 'input_text long',null, array(), array('style'=>'width:85px;height:18px;') ); ?> ( Plan ) </td>
					</tr>
					<tr>
						<td class="text_caption bottom" > * Age Period</th>
						<td> <?php echo form() -> input('text_product_age', 'input_text long',null, array(), array('style'=>'width:85px;height:18px;') ); ?> ( Age )</td>
					</tr>
				</table>
			</div>
			</td>
			<td valign="top">
			<div valign="top">
				<table cellpadding="5px" align="left" border=0>
					<tr>
						<td class="text_caption bottom"> * Product Name</td>
						<td> <?php echo form() -> input('text_product_name', 'input_text long',null,null,array('style'=>'height:20px;')); ?> ( 30 ) </td>
					</tr>
					<tr>
						<td class="text_caption bottom"> * Product Type</td>
						<td> <?php echo form() -> combo('select_product_type', 'select long', $data -> _getProductType()); ?></td>
					</tr>
					
					<tr>
						<td class=" text_caption bottom top" valign='top'> * Gender</td>
						<td> <?php if(is_array($gender))foreach( $gender as $GenderId => $label ) : ?>
							 <?php echo form() -> checkbox('GenderId', 'select long',$GenderId, null, array('label'=> $label) ); ?> <br>
							 <?php endforeach; ?>
						</td>
					</tr>
					
					<tr>
						<td class="text_caption bottom"> * Product Status </td>
						<td> <?php echo form() -> combo('status', 'select long',array('0'=>'Not Active','1'=>'Active')); ?></td>
					</tr>
				</table>
			</div>
			</td>	
	</table>
</div>
</form>

<!--line of age priode -->
<form name="form_age_range">
	<div id="html_grid_age" class="box-shadow" style="background-color:#FFFFFF;"> </div>
</form>
<!--line Grid -->
<form name="form_grid_content">
	<div id="html_grid_premi" class="box-shadow" style="background-color:#FFFFFF;"> </div>
</form>
</fieldset>