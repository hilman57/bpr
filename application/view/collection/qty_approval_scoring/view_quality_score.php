<div id="div_score">
<fieldset class="corner" style='border:0px solid #000;'>
		
		<form name="frmScoring">
		<?php //__(form()->input('FinalScore', null, 100));?>
		<?php
			// $no = 0;
			// if( isset($ScoringCategory)) 
			// foreach($ScoringCategory as $key => $category)  { $no++; ?>
			<table class="custom-grid" style='border:0px solid #dddddd;' 
				cellspacing='0' cellpadding='0' width='99%' align='center'> 
				<!-- (1) Preparation -->
				<tr>
					<td class="ui-corner-top ui-state-default " colspan="3">
						<span color="red" class="icon-product product-title-icon" 
						style="padding:2px 0px 2px 16px;">&nbsp;&nbsp;:: (1) Preparation </span></td>
				</tr>
				<?php 
				//foreach($QualityScoring['SubCategory'][$key] as $id => $value) { ?>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>1.1.</td>
					<td class="content-left text_caption bottom left">
					Mempersiapkan segala sesuatunya sebelum memberikan pelayanan
					</br> <i>Prepare for the all sufficiently (such as pre-call work)</i></td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Hal - hal yang perlu disiapkan antara lain :
					</br>Selambatnya jam 8:15 staff sudah hadir dan sudah melakukan activity call (First Call)</p><div style="width:550px;"></div></td>
				</tr>

				<!-- (2) Parameter -->
				<tr>
					<td class="ui-corner-top ui-state-default " colspan="3">
						<span color="red" class="icon-product product-title-icon" 
						style="padding:2px 0px 2px 16px;">&nbsp;&nbsp;:: (2) Parameter </span></td>
				</tr>
				<?php 
				//foreach($QualityScoring['SubCategory'][$key] as $id => $value) { ?>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>2.1.</td>
					<td class="content-left text_caption bottom left">
					Follow Up 
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Melakukan daily check follow up data pending 
					</br>Buat keterangan di description jika nasabah dihubungi (ada) tidak berurutan hari kerja
					</br>Misal : Status = call back, description : "hubungi nasabah lembali 2 hari lagi karena ybs sedang naik haji"
					</td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>2.2.</td>
					<td class="content-left text_caption bottom left">
					Dokumentasi TIPS sesuai ketentuan yang berlaku
					</br>Entry TIPS
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					<ol style="margin-left: 0; padding-left: 0;">
					<li>WCO wajib input TIPS</li>
					<li>Input TIPS harus benar dan lengkap
						</br>- Activity call, Call status, act status, act type, reason 1, Q sesuai, dll
					</br>- Input alamat /  no telp / email nasabah
					</br>- Memastikan melakukan up date data, sesuai pembicaraan dengan nasabah, dll
					</li>
					</ol>
					
					</td>
				</tr>

				<!-- (3) Greetings Opening  -->
				<tr>
					<td class="ui-corner-top ui-state-default " colspan="3">
						<span color="red" class="icon-product product-title-icon" 
						style="padding:2px 0px 2px 16px;">&nbsp;&nbsp;:: (3) Greetings Opening  </span></td>
				</tr>
				<?php 
				//foreach($QualityScoring['SubCategory'][$key] as $id => $value) { ?>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>3.1.</td>
					<td class="content-left text_caption bottom left">
					Opening Greetings 
					</br>
					Follow standard opening greetings
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					DIALLING CALL
					<ol style="margin-left: 0; padding-left: 0;">
					<li>Ucapkan "Selamat pagi/siang/sore/malam, bisa bicara dengan  Bapak/Ibu &lt;sebut nama lengkap nasabah&gt;?"</li>
					<li>Mengucapkan nama nasabah disertai panggilan Bapak atau Ibu dengan ramah</li>
					<li>Perkenalkan nama Jelas anda &amp; menyampaikan tujuan menghubungi nasabah</li>
					<li>Perkenalkan nama Jelas anda &amp; menyampaikan tujuan menghubungi nasabah
					</br>"Saya &lt;sebut nama caller&gt; dari AIA. Atas nama management AIA, Kami berterima kasih atas kepercayaan Bapak/Ibu membeli asuransi jiwa unit link <sebut nama produk> melalui <khusus nasabah Bancass, sebut nama Bank> dan kami mengucapkan selamat bergabung dengan keluarga besar AIA".
					</li>
					<li>Tanyakan kesediaan waktunya beberapa menit untuk melakukan survey*)
					</br>Contoh : "Boleh kami minta waktu Bapak/Ibu beberapa menit saja?"
					</br>note :  *) Jika nasabah sedang sibuk, tanyakan kapan bisa dihubungi kembali
					</li>
					</ol>

					CALL BACK
					<p></br>Greetings :
					</br>Ucapkan : " Selamat pagi/siang/sore bisa bicara dengan Bapak/Ibu &lt;nama lengkap nasabah&gt;"
					</br>Perkenalkan nama Jelas anda
					</br>Contoh : Saya <sebut nama> dari AIA Financial Pak/Bu yang beberapa waktu lalu menghubungi Bpk/Ibu, sudah bisa minta waktunya sekitar 5 menit pak/bu?"
					</br>Tanyakan kesediaan waktunya beberapa menit untuk melakukan survey
					</br>Ucapkan : "……..bisa kita mulai dengan pertanyaan pertama ya pak/bu…"
					</br>Jelaskan maksud, tujuan dan resume pembicaraan sebelumnya (informasikan hasil call sebelumnya sudah sampai sesi apa)</p>

					</td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>3.2.</td>
					<td class="content-left text_caption bottom left">
					Ucapan nama nasabah selama pembicaraan 
					</br>Customer maintion
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Mengucapkan nama nasabah (min 3x (awal, tengah, akhir pembicaraan)) disertai panggilan Bapak atau Ibu dengan ramah
					</td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>3.3.</td>
					<td class="content-left text_caption bottom left">
					Ucapan congratulation jika nasabah berulang tahun
					</br>Say congratulation for customer birthaday at that time
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Jika tanggal nasabah menghubungi sama dengan tanggal lahir (H atau H+1)
					</td>
				</tr>

				<!-- (4) Respond to Customer -->
				<tr>
					<td class="ui-corner-top ui-state-default " colspan="3">
						<span color="red" class="icon-product product-title-icon" 
						style="padding:2px 0px 2px 16px;">&nbsp;&nbsp;:: (4) Respond to Customer </span></td>
				</tr>
				<?php 
				//foreach($QualityScoring['SubCategory'][$key] as $id => $value) { ?>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>4.1.</td>
					<td class="content-left text_caption bottom left">
					Tunjukan rasa empati kepada kondisi nasabah pada saat itu
					</br> <i>Acknowledge customer's emotions and show empathy on customer's situation</i></td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					<ol style="margin-left: 0; padding-left: 0;">
					<li>Jika nasabah menginformasikan berita duka (claim meninggal), maka WCO bisa menunjukkan rasa empatinya dengan mengatakan " Saya turut berduka, …"</li>
					<li>Minta maaf apabila ada pelayanan dari staf AIA Financial yang telah merugikan nasabah "Kami menyampaikan permohonan maaf atas pelayanan yang kurang berkenan di hati </li>
					<li>Ucapkan terima kasih apabila nasabah memberikan saran atau kritik membangun "Terimakasih atas saran/masukan yang Bpk/Ibu berikan, hal itu akan jadikan acuan untuk </li>
					</ol>
					</td>
				</tr>

				<!-- (5) Listening Skills -->
				<tr>
					<td class="ui-corner-top ui-state-default " colspan="3">
						<span color="red" class="icon-product product-title-icon" 
						style="padding:2px 0px 2px 16px;">&nbsp;&nbsp;:: (5) Listening Skills </span></td>
				</tr>
				<?php 
				//foreach($QualityScoring['SubCategory'][$key] as $id => $value) { ?>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>5.1.</td>
					<td class="content-left text_caption bottom left">
					Dengarkan pelanggan dan berikan respon singkat
					</br><i>Use listening voice and short respone</i></td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Pada saat nasabah sedang berbicara, WCO harus membuat respon yang singkat agar terjalin komunikasi yang baik 
					</br>contoh  "iya", "seperti itu ya", "baik", "ehem", lainnya
					</td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>5.2.</td>
					<td class="content-left text_caption bottom left">
					Dengarkan pernyataan atau keberatan nasabah dengan baik dan berikan respon yang sesuai dengan pernyataan atau keberatan nasabah
					</br><i>Listen attentively to customer's questions / concerns and give appropriate  response</i></td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Simple / Non Compleks Case :
					</br>WCO harus berkonsentrasi saat berbicara dengan nasabah (FOKUS) 
					</br>Misal : nasabah sudah menyebutkan nama ahli waris, namun WCO menanyakannya kembali; WCO mengulang pertanyaan pada script (padahal sebelumnya pertanyaan 
					</td>
				</tr>

				<!-- (6) Telephone Skills  -->
				<tr>
					<td class="ui-corner-top ui-state-default " colspan="3">
						<span color="red" class="icon-product product-title-icon" 
						style="padding:2px 0px 2px 16px;">&nbsp;&nbsp;:: (6) Telephone Skills </span></td>
				</tr>
				<?php 
				//foreach($QualityScoring['SubCategory'][$key] as $id => $value) { ?>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>6.1.</td>
					<td class="content-left text_caption bottom left">
					Biarkan nasabah berbicara terlebih dahulu, jangan menginterupsi nasabah dan jaga hubungan komunikasi dua arah
					</br><i>Let customer speak first / avoid interruption / maintain two-way communication</i></td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Untuk menghindari kesalahpahaman dari permintaan nasabah, WCO harus membiarkan nasabah untuk berbicara terlebih dahulu dan tetap mendengarkan apa yang  nasabah 
					</br>contoh : " baik Bapak/Ibu silahkan …" 
					</td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>6.2.</td>
					<td class="content-left text_caption bottom left">
					Jaga intonasi dan gunakan jeda pada saat yag tepat
					</br><i>Keep proper pace of speaking and pause at appropriate time</i></td>
				</tr>
				<tr class="onselect">
					<td class="content-first text_caption bottom center" 1>&nbsp;&nbsp;</td>
					<td class="content-lasted text_caption bottom left" colspan="2">
					Pada saat WCO sedang melakukan penjelasan, WCO harus mengontrol waktu jeda dan kecepatan dalam berbicara (tidak terlalu cepat atau lambat) . Agar nasabah dapat 
					</br>Diakhir penjelasan "Setelah saya jelaskan, apakah Bapak/Ibu cukup mengerti ?"
					</td>
				</tr>
			</table>
			
			<?php //} ?>
	
		<div> 
			<table>
				<tr >
					<td class="text_caption" colspan="2">Total Score</td>
					<td><?php __(form()->input("TotalScore","input_text",(INT)$QtyScoring["TotalScore"],null,
										array('readonly' => true,'style'=>'width:30px;text-align:center;')));?>  </td>
				</tr>
				<!-- <tr>
					<td class="text_caption">Remarks</td>
					<td><?php //__(form()->textarea('remarks', 'textarea',$QtyScoring['remarks'], 
						//array("keyup" =>"Ext.Cmp('call_remarksb').setValue(this.value);"), array('style'=>'color:blue;height:70px;width:750px;')));?>  </td>
				</tr> -->	
			</table>
		</div>
			</form>
	</fieldset>
</div>