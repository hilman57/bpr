<?php __(javascript()); ?>
<script type="text/javascript">
//Ext.DOM.QualityResult = [<?php echo json_encode($Combo['QualityResult']);?>];

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 	
/* create object default parameter assigning **/

var datas = 
{
	chk_cust_number : '<?php echo _get_exist_session('chk_cust_number');?>', 
	chk_cust_name 	: '<?php echo _get_exist_session('chk_cust_name') ;?>', 
	chk_home_phone  : '<?php echo _get_exist_session('chk_home_phone') ;?>', 
	chk_office_phone: '<?php echo _get_exist_session('chk_office_phone') ;?>', 
	chk_mobile_phone: '<?php echo _get_exist_session('chk_mobile_phone') ;?>',
	chk_campaign_id : '<?php echo _get_exist_session('chk_campaign_id');?>', 
	
	chk_call_result : '<?php echo _get_exist_session('chk_call_result') ;?>', 
	chk_user_id 	: '<?php echo _get_exist_session('chk_user_id') ;?>', 
	chk_start_date  : '<?php echo _get_exist_session('chk_start_date') ;?>',
	chk_end_date    : '<?php echo _get_exist_session('chk_end_date') ;?>', 
	order_by 	: '<?php echo _get_exist_session('order_by') ;?>', 
	type	 	: '<?php echo _get_exist_session('type') ;?>',
	chk_category_id : '<?php echo _get_exist_session('chk_category_id') ;?>',
	chk_caller_name : '<?php echo _get_exist_session('chk_caller_name') ;?>',
	chk_QaulityStatusId: '<?php echo _get_exist_session('chk_QaulityStatusId') ;?>',
	chk_PolicyNumber: '<?php echo _get_exist_session('chk_PolicyNumber') ;?>'
}
console.log(datas);
		
Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

	
	
/* assign navigation filter **/
var navigation = 
{
	custnav : Ext.DOM.INDEX +'/QtyCheckedList/index/',
	custlist : Ext.DOM.INDEX +'/QtyCheckedList/Content/',
}
		
/* assign show list content **/
		
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/* function searching customers **/

var validation_check =  function(CustomerId)
	{
		if( CustomerId )
			{
				Ext.File = '../class/class.src.qualitycontrol.php'; 
				Ext.Params = {
					action:'validation_check',
					CustomerId : CustomerId
				}	
				
				return Ext.eJson();	
			} 
 } 
/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
Ext.DOM.searchCustomer = function()
{
	Ext.EQuery.construct( navigation ,
		Ext.Join([Ext.Serialize('frmChecklist').getElement()]).object());
	
	Ext.EQuery.postContent();
}
	
Ext.DOM.Checked= function(){
	Ext.Cmp('chk_cust_call').setChecked();
}

/* 
 * @ def : memanggil Jquery plug in 
 * -------------------------------
 * @ param : public 
 */
 
Ext.DOM.UnApprovalAll = function()
{	

var CustomerId = Ext.Cmp('chk_cust_call').getChecked();
	//console.log(CustomerId);
	
if( CustomerId.length > 0 )
{
	Ext.Ajax
	({
		url : Ext.DOM.INDEX+"/QtyCheckedList/UnApprovalAll/",
		method : 'POST',
		param : {
			CustomerId : CustomerId 	
		},
		
		ERROR : function(e){
			Ext.Util(e).proc(function(resp){
				if( resp.success ){
					Ext.Msg('Unapprove ( '+ CustomerId.length +' ) data ').Success();
					Ext.DOM.searchCustomer();
				}
				else{
					Ext.Msg('Unapprove ( '+ CustomerId.length+' ) data ').Failed();
				}
			});
		}
	}).post();
}
else{
	Ext.Msg('Please select rows !').Info();
}	
}
		
/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
Ext.DOM.resetSeacrh = function() {
	Ext.Serialize('frmChecklist').Clear();
	Ext.DOM.searchCustomer();
}
  
/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
Ext.DOM.showPolicy = function(CustomerId) {
if( CustomerId!='' ) 
{
	Ext.EQuery.Ajax ({
		url 	: Ext.DOM.INDEX +'/QtyCheckedList/QualityDetail/',
		method  : 'GET',
		param 	: {
			CustomerId 	 : CustomerId,
			ControllerId : Ext.DOM.INDEX +'/QtyCheckedList/index/', 
		}
	});
 }
}

Ext.DOM.FuInfo = function(){
	var CustomerId = Ext.Cmp('chk_cust_call').getChecked();
	if( CustomerId.length == 1 ){
		alert(CustomerId);
		var PolicyId = Ext.Cmp('ProductForm').getValue();
		// if( PolicyId !='' ) {
			Ext.DOM.PolicyDetailById(PolicyId);
			
			Ext.Window ({
				url 		: Ext.DOM.INDEX+'/ProjectWorkForm/index/',	
				method 		: 'POST',
				width  		: (Ext.query(window).width()-(Ext.query(window).width()/4)), 
				height 		: Ext.query(window).height(),
				left  		: (Ext.query(window).width()/2),
				scrollbars 	: 1,
				sccrolling	: 1,
				param  		: 
				{
					ViewLayout 	 : 'EDIT_FORM',	
					PolicyId 	 : PolicyId,
					CustomerId 	 : Ext.Cmp('CustomerId').getValue()
				}
			}).popup();
		// }
	}else if(CustomerId.length > 1){
		alert('Please Select Only One Row');
	}else{
		alert('No Row selected');
	}
}

/* 
 * @def : memanggil Jquery plug in 
 * -------------------------------
 * @param : public 
 */
 
 Ext.document().ready(function(){
   Ext.query('#toolbars').extToolbars 
	({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle :[['Search'],['Clear'],['Select All'],['Unapprove']],
		extMenu  :[['searchCustomer'],['resetSeacrh'],['Checked'],['UnApprovalAll']],
		extIcon  :[['zoom.png'],['cancel.png'],['accept.png'],['tick.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
	
	$('.date').datepicker({showOn: 'button', buttonImage: Ext.DOM.LIBRARY +'/gambar/calendar.gif', buttonImageOnly: true, dateFormat:'dd-mm-yy',readonly:true});
});

Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}

</script>

<!-- start : content -->

<fieldset class="corner" onKeyDown="enterSearch(event)">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
<form name="frmChecklist">
<div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
<table cellpadding="3px;" border=0>
	<tr>
		<td class="text_caption"> CIF Number</td>
		<td><?php echo form()->input('chk_cust_number','input_text long',_get_exist_session('chk_cust_number'));?></td>
		<td class="text_caption"> Campaign</td>
		<td><?php echo form()->combo('chk_campaign_id','select long',$Combo['Campaign'],_get_exist_session('chk_campaign_id'));?></td>
		<td class="text_caption"> Product Category</td>
		<td><?php echo form()->combo('chk_category_id','select long',$ProductCat,_get_exist_session('chk_category_id'));?></td>
		<td class="text_caption"> Interval </td>
		<td nowrap>
			<?php echo form()->input('chk_start_date','input_text date',_get_exist_session('chk_start_date'));?>
			<?php echo form()->input('chk_end_date','input_text date',_get_exist_session('chk_end_date'));?>
		</td>
		<td class="text_caption" style="text-align:center;"> Policy Number</td>
	</tr>
	<tr>
		<td class="text_caption"> Owner Name </td>
		<td><?php echo form()->input('chk_cust_name','input_text long',_get_exist_session('chk_cust_name'));?></td>
		<td class="text_caption"> Call Result </td>
		<td><?php echo form()->combo('chk_call_result','select long',$Combo['CallResult'],_get_exist_session('chk_call_result'));?></td>
		<td class="text_caption"> Caller Name </td>
		<td><?php echo form()->combo('chk_user_id','select long',$UserState,_get_exist_session('chk_user_id'));?></td>
		<td class="text_caption"> QA Status</td>
		<td><?php echo form()->combo('chk_QaulityStatusId','select long',$qulity_array,_get_exist_session('chk_QaulityStatusId'));?></td>
		<td><?php echo form()->input('chk_PolicyNumber','input_text long',_get_exist_session('chk_PolicyNumber'));?></td>
		
	</tr>
		</table>
	</div>
	</form>
 </div>
 <div id="toolbars"></div>
 <div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" ></div>
	<div id="pager"></div>
 </div>
</fieldset>	
		
	<!-- stop : content -->
	
	
	
	
	