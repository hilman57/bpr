<?php 

/** set label style on selected **/

page_background_color('#FFFCCC');
page_font_color('#8a1b08');

/** page_set_align on grid every field **/

page_set_align('CustomerFirstName', 'left');
page_set_align('CIFNumber', 'left');
page_set_align('PolicyNumber', 'left');
page_set_align('PolicyProductName', 'left');
page_set_align('PolicyIssDate', 'center');
page_set_align('PolicyUploadDate', 'center');
page_set_align('full_name', 'left');
page_set_align('CallReasonDesc', 'left');
page_set_align('CustomerUpdatedTs', 'center');
page_set_align('CallAttempt', 'center');
page_set_align('CallReasonQue', 'left');
page_set_align('SumPolicy', 'center');

/** get label header attribute **/

$labels =& page_labels();
$primary =& page_primary();
$aligns	 =& page_get_align();

?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle <?php echo $aligns[$Field];?>"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows ) :  
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>
	<tr class="onselect" bgcolor='<?php echo $color; ?>'>
		<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary]); ?>
		<td class="content-middle center"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) : $color=&page_column($Field);  ?>
			<td class="content-middle <?php echo $aligns[$Field];?>" <?php __($color) ?> ><?php echo ( $rows[$Field]?$rows[$Field]:'-' ); ?></td>	
		<?php endforeach; ?>
	</tr>
</tbody>
<?php
 $no++; endforeach;
?>
</table>


