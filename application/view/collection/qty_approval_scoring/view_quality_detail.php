<?php 
__(javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/views/EUI_Contact.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Media.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_MathRand.js', 'eui_'=>'1.0.0', 'time'=>time())
)));

?>
	
<?php $this ->load ->view('qty_approval_scoring/view_quality_javascript');?>

<!-- detail content -->

<fieldset class="corner"> 
<legend class="icon-customers"> &nbsp;&nbsp;Quality Scoring Detail </legend>
<div id="toolbars" class="contact"></div>
<!-- START :: hidden -->
<?php echo form()->hidden('ControllerId',NULL,_get_post("ControllerId") );?>
<?php echo form()->hidden('CustomerId',NULL,$Customers['CustomerId']);?>
<?php echo form()->hidden('CustomerNumber',NULL,$Customers['CustomerNumber']);?>
<?php echo form()->hidden('PolicyId',NULL,reset(array_keys($PolicyNumber)));?>
<?php
	// for disabling reason;
	echo form()->hidden('CheckedList',NULL,$CheckedList);
?>
<!-- END :: hidden -->
<div class="contact_detail" style="margin-left:-8px;" name="parents">
	<table width="100%" border=0>
		<tr>
			<td  width="70%" valign="top">
				<?php $this->load->view('qty_approval_scoring/view_quality_default_detail');?>
				<?php $this->load->view('qty_approval_scoring/view_quality_policylist');?>
				<?php $this->load->view('qty_approval_scoring/view_quality_history_detail');?>
			</td>
			<td  width="30%" rowspan="2" valign="top">
				<?php ($CheckedList == "Checked" ? $this ->load ->view('qty_approval_scoring/view_quality_phone_detail_disabled') : $this ->load ->view('qty_approval_scoring/view_quality_phone_detail'));?>
			</td>
		</tr>
	</table>
	<div id="change_request_dialog" >
</div>
</fieldset>	
