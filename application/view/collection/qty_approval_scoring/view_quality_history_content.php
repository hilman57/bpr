<table border=0 align="left" cellspacing=1 width="100%">

<tr height='24'>
	<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
	<th class="font-standars ui-corner-top ui-state-default first" width="10%" nowrap>&nbsp;Policy No.</td>
	<th class="font-standars ui-corner-top ui-state-default first" width="10%" nowrap>&nbsp;Call Date</td>
	<th class="font-standars ui-corner-top ui-state-default first" width="10%" nowrap>&nbsp;Caller</td>
	<th class="font-standars ui-corner-top ui-state-default first" width="20%" nowrap>&nbsp;Call Status</td>
	<th class="font-standars ui-corner-top ui-state-default first" width="40%" nowrap>&nbsp;Note</td>
	<th class="font-standars ui-corner-top ui-state-default first" width="40%" nowrap colspan=2>&nbsp;Script</td>
	
</tr>
<?php 
if(!is_null($CallHistory) && is_array($CallHistory) ) 
{  
?>
 <?php $k=1; $i= 0; foreach($CallHistory as $rows ) { $color = ($i%2!=0?'#FFFEEE':'#FFFFFF');  ?>
<tr class='onselect' bgcolor='<?php __($color);?>'>
	<td class="content-first" WIDTH="5%" nowrap><?php echo $k;?></td>
	<td class="content-middle" WIDTH="5%" nowrap><?php echo $rows['PolicyNumber'];?></td>
	<td class="content-middle left" nowrap><?php __($rows['CallHistoryCreatedTs']);?></td>
	<td class="content-middle left" nowrap><?php __($rows['full_name']);?></td>
	<td class="content-middle left" width='30%'>
		<div class="text-content justify-text" >
			Status : ( <span class="call-status"><?php __($rows['CallReasonCategoryName']);?></span> ) - 
			Reason : ( <span class="result-status"><?php __($rows['CallReasonDesc']);?></span> ) -
			Type :(<span class="result-status"><?php echo ($rows['PhoneType']?$rows['PhoneType']:'-');?></span>) -
			Phone : (<span class="result-status"><?php echo $rows['CallNumber'];?></span>)
		</div>
	</td>
	<td class="content-middle left"><div class="text-content justify-text">
		<?php __($rows['CallHistoryNotes']);?>
		- Updated By : (<span class="result-status"><?php  __($rows['full_name']); ?></span>)
		</div>
	</td>
	<td class="content-middle center"><div class="text-content justify-text"><?php __($rows['Description']);?></div></td>
	<td class="content-lasted center" >
		<?php if( $rows['UpdatedById']!='') :  ?> 
			<?php echo "-"; ?>
		<?php else : ?>
			<?php __(($rows['CallSessionId']?form()->button($rows['CallSessionId'],'play button','Play',array("click"=>"Ext.DOM.PlayByCallSession(this.name);")):null));?>
		<?php endif; ?>
		
	</td>
</tr>
<?php $i++; $k++;} ?>
<?php } ?>
</table>