<?php 
__(javascript
(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Dialog.js', 'eui_'=>'1.0.0', 'time'=>time())
)));
?>

<script type="text/javascript">
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
var Reason = [];

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
var datas = {
	faxnumber 	: '<?php echo _get_post('faxnumber');?>',
	agent_id 	: '<?php echo _get_post('agent_id');?>',
	agent_group : '<?php echo _get_post('agent_group');?>',
	start_time 	: '<?php echo _get_post('start_time');?>',
	end_time 	: '<?php echo _get_post('end_time');?>',
	order_by	: '<?php echo _get_post('order_by');?>',
	type		: '<?php echo _get_post('type');?>'
}
			
/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */
 
var navigation = {
	custnav : Ext.DOM.INDEX +'/FaxContent/index/',
	custlist : Ext.DOM.INDEX +'/FaxContent/content/'
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */		
 
 Ext.EQuery.construct(navigation,datas)
 Ext.EQuery.postContentList();

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

var searchCustomer = function(){
	Ext.EQuery.construct(navigation,Ext.Join([Ext.Serialize('frmSearch').getElement()]).object() );
	Ext.EQuery.postContent();
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */


Ext.DOM.CloseFax = function(){
	Ext.Cmp("panel-call-center").setText('');
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.DOM.Delete = function(){
	var DataId  = Ext.Cmp('OutboxId').getValue();
	if((DataId!='')) 
	{
		Ext.Ajax ({
			url 	: Ext.DOM.INDEX +'/FaxContent/DeleteFax/',
			method 	: 'POST',
			param 	: { DataId : DataId },
			ERROR : function(e){
				Ext.Util(e).proc(function(Delete){
					if( Delete.success ){
						Ext.Msg("Delete File").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Delete File").Failed();
					}
				});
			}
		 }).post();
	}
	else{
		Ext.Msg("No selected rows ").Info();
	}
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.DOM.Clear = function(){
	Ext.Serialize('frmSearch').Clear();
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.DOM.Preview = function() {
	var OutboxId = Ext.Cmp('OutboxId').getChecked();
	if( OutboxId.length > 0  ) {
		Ext.Window
		({
			url   : Ext.DOM.INDEX +'/FaxContent/Preview/',
			width : ( Ext.query(window).width()/2), height: Ext.query(window).height(),
			left  : Ext.query(window).width(),
			scrolling : 1,
			param : {
				OutboxId : OutboxId
			}
		}).popup();
	}
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

Ext.DOM.Resend = function()
{
	var OutboxId  = Ext.Cmp('OutboxId').getChecked();
	if((OutboxId.length > 0)) 
	{
		Ext.Ajax 
		({
			url 	: Ext.DOM.INDEX +'/FaxContent/ResendFax/',
			method 	: 'POST',
			param 	: { 
				OutboxId : Ext.Cmp('OutboxId').getChecked() 
			},
			ERROR 	: function(e){
				Ext.Util(e).proc(function(Resend) {
					if( Resend.success ){
						Ext.Msg("Resend Fax").Success();
						Ext.EQuery.postContent();
					}
					else {
						Ext.Msg("Resend Fax").Failed();
					}
				});
			}
		 }).post();
	}
	else{
		Ext.Msg("No selected rows ").Info();
	}
}

/* 
 * @ def 	: extToolbars
 * --------------------------------------------------------------
 * @ param 	: no define
 * @ aksess : procedure
 * -------------------------------------------------------------- 
 */

$(function(){
	$('#toolbars').extToolbars
	({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Search'],['Clear'],['Delete'],['Preview'],['Resend']],
		extMenu   : [['searchCustomer'],['Clear'],['Delete'],['Preview'],['Resend']],
		extIcon   : [['zoom.png'], ['cancel.png'],['delete.png'],['page_white_acrobat.png'],['page_white_go.png']],
		extText   : true,
		extInput  : true,
		extOption : [{
			render : 5,
			type   : 'label',
			id     : 'voice-list-wait', 	
			name   : 'voice-list-wait',
			label :'<span style="color:#dddddd;">-</span>'
		}]
	});
	
	$('.box').datepicker({
		showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly: true, 
		dateFormat:'dd-mm-yy',readonly:true
	});
});

</script>

<!-- start : content -->
<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
	
	<div id="result_content_add" class="box-shadow" style="padding-bottom:4px;margin-top:2px;margin-bottom:8px;">
		<form name="frmSearch">
		<table cellpadding="3px;" border=0>
		<tr>
			<td class="text_caption"> Fax Number</td>
			<td><?php echo form() -> input('faxnumber','input_text long',_get_post('faxnumber')); ?></td>
			<td class="text_caption"> User Agent</td>
			<td><?php echo form() -> combo('agent_id','select long',$combo['AgentId'],_get_post('agent_id')); ?></td>
			
		</tr>
		<tr>
			<td class="text_caption"> User Group</td>
			<td><?php echo form() -> combo('agent_group','select long',$combo['AgentGroup'],_get_post('agent_group')); ?></td>
			<td class="text_caption"> Interval </td>
			<td>
			<?php echo form() -> input('start_time','input_text box',_get_post('start_time')); ?> - 
			<?php echo form() -> input('end_time','input_text box',_get_post('end_time')); ?> </td>
		</tr>
		</table>
		</form>
	</div>
	</div>
	
	<div id="toolbars"></div>
	<div id="play_panel"></div>
	<div id="recording_panel" class="box-shadow">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	