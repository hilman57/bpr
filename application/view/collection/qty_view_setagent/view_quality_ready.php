<table border=0 align="left" cellspacing=1 width="60%">
	<tr height=24>
		<th class='font-standars ui-corner-top ui-state-default first center' width='5%'><a href="javascript:void(0);" onclick="Ext.Cmp('QualityAgentId').setChecked();">#</a></th>
		<th class='font-standars ui-corner-top ui-state-default first center' width='5%'> No. </th>
		<th class='font-standars ui-corner-top ui-state-default first left'>&nbsp;User Name </th>
		<th class='font-standars ui-corner-top ui-state-default first left'>&nbsp;Agent </th>
		<th class='font-standars ui-corner-top ui-state-default first left'>&nbsp;Supervisor </th>
		<th class='font-standars ui-corner-top ui-state-default first left'>&nbsp;Quality Staff </th>
	</tr>
	<?php 
		$no = 1;
		// echo "<pre>";
		// print_r($view_quality_agent);
		// echo "</pre>";
		foreach( $view_quality_agent as $num => $rows ) :
			$color= ($no%2!=0?'#FAFFF9':'#FFFFFF');
			
	?>
		<tr height=24 class="onselect" bgcolor="<?php echo $color;?>" >
			<td class='content-first center' width='5%'><?php __(form() -> checkbox('QualityAgentId',null,$rows['GroupQualityId']))?> </td>
			<td class='content-middle center' width='5%'><?php __($num); ?> </td>
			<td class='content-middle left'>&nbsp;<?php __($rows['id']); ?></td>
			<td class='content-middle left'>&nbsp;<?php __($rows['full_name']); ?></td>
			<td class='content-middle left'>&nbsp;<?php __($rows['SpvId']); ?> - <?php __($rows['SpvName']); ?></td>
			<td class='content-lasted left'>&nbsp;<?php __($rows['QualityCode']); ?> - <?php __($rows['QualityName']); ?></td>
		</tr>
	<?php 
		$no++;
	endforeach; ?>
	<tr>
		<td colspan='2' class='font-standars ui-corner-top ui-state-default first'>Page : </td>
		<td colspan='4' class='font-standars ui-corner-top ui-state-default first'>
			<?php __(form()->combo("RecordPage","select box", $view_quality_pages,$view_quality_page,array('change' =>'Ext.DOM.QualitySelectAgentReady(this);')));?>
			&nbsp;<span> Records : ( <?php __($view_quality_record); ?> )  
		</td>
	</td>	
</table>


<br style="clear:both;">
<div style='margin-top:20px;'>
	<table>
		<tr>
			<td class="text_caption bottom">Quality Staff : </td>
			<td><?php __(form() -> combo('QulaityStaffId','select long',$view_quality_staff,null,null,array("style"=>"height:30px;")));?> </td>
			<td>
				<?php __(form() -> button('UpdateQualityStaff','button add','&nbsp;Update', array("click" =>"Ext.DOM.UpdateQualityAgent();" )));?>
				<?php __(form() -> button('UpdateQualityStaff','button remove','&nbsp;Remove', array("click" =>"Ext.DOM.RemoveQualityAgent();" )));?>
				<?php __(form() -> button('UpdateQualityStaff','button clear','&nbsp;Clear', array("click" =>"Ext.DOM.EmptyQualityAgent();" )));?>
			</td>
		</tr>
	</table>
</div>
