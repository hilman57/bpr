<!-- /**@ Agent Available ** / -->


<fieldset class="corner">
	<legend class="icon-customers">&nbsp;&nbsp;Agent Available </legend>
	<div style='margin:5px 5px 5px 5px;overflow:auto;border:1px solid #dddddd;' id="agent_ready_state"></div>
	<br style="clear:both;">
	<div style='margin:5px 0px 5px 5px;float:left;'>
		<?php __(form() -> button('import_user','button assign', '&nbsp;Check All', array("click" =>"Ext.Cmp('UserId').setChecked();")))?>
		<?php __(form() -> button('import_user','button add', '&nbsp;Add Agent', array("click" =>"Ext.DOM.AddAvailableAgent();" )))?>
		
	</div>
</fieldset>

<!-- /**@ Quality Agent Ready ** / -->

<fieldset class="corner" style="margin-top:15px;">
	<legend class="icon-customers">&nbsp;&nbsp;Quality Agent Ready </legend>
	<div>
		<table border=0 width='50%'>
			<tr>
				<td>
					<span style='margin-left:15px;'> 
						<?php __(form() -> checkbox('hidden_ready',null,1,array("change" =>"Ext.IsHide(this);")))?> Hide Staff Is Ready
					</span>
				</td>
				<td class='text_caption'>QA Name </td>
				<td><?php __(form() -> combo('QualityId','select long',$quality_staff,null,array("change" =>"Ext.DOM.ShowByQualityId(this);")))?></td>
				<td class='text_caption'>TSR </td>
				<td><?php __(form() -> input('TsrName','input_text long',null));?></td>
				
			</td>
		</table>
	</div>
	<div style='margin:5px 5px 5px 5px;' id="content_agent_import"></div>
</fieldset>