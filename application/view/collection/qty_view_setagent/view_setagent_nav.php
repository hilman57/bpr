<script>
Ext.document('document').ready(function(){

/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */

  Ext.DOM.AjaxStop = function(){
		Ext.Cmp("content_agent_import").setText("");
  }
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */

  Ext.DOM.AjaxStart = function(){
		Ext.Cmp("content_agent_import").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'>"+
		"<span style='color:red;'>Please Wait...</span>");
  }
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
Ext.DOM.LaodAgentState = function( page ){
Ext.Ajax
({
	url  	: Ext.DOM.INDEX+"/QtySetAgent/getAgentReadyState/",
	method  : 'POST',
	param 	: {
		page : page
	}
 }).load('agent_ready_state');
}
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
Ext.Cmp("TsrName").listener({
	onKeyup : function(e){
		if(e.keyCode==13 ){
			Ext.DOM.QualityAgentReady();
			Ext.Cmp('QulaityStaffId').setFocus();
		}
	}
	
}); 

// Ext.DOM.ShowByTSR = function(obj) {
	// console.log(window);
// }
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
Ext.DOM.QualityAgentReady = function(){
	   Ext.DOM.AjaxStart();
	   Ext.Ajax
	   ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/QualityAgentReady/",
			method  : 'POST',
			param 	: {
				time 	 : Ext.Date().getDuration(),
				Hide	 : Ext.Cmp('hidden_ready').getValue(),
				QulityId : Ext.Cmp('QualityId').getValue(),
				TsrName	 : Ext.Cmp('UserName').getValue(), 
				page 	 : Ext.Cmp('RecordPage').getValue()
			}
		}).load('content_agent_import');
	}
	
  Ext.DOM.QualityAgentReady();	
  
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
 Ext.DOM.QualitySelectAgentReady = function(obj){
	   Ext.DOM.AjaxStart();
	   Ext.Ajax
	   ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/QualityAgentReady/",
			method  : 'POST',
			param 	: {
				time 	 : Ext.Date().getDuration(),
				Hide	 : Ext.Cmp('hidden_ready').getValue(),
				QulityId : Ext.Cmp('QualityId').getValue(),
				TsrName	 : Ext.Cmp('UserName').getValue(), 
				page 	 : obj.value
			}
		}).load('content_agent_import');
		
		Ext.Cmp(obj.id).setFocus();
	}
 /*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
 Ext.DOM.AddAvailableAgent = function(){
	Ext.DOM.AjaxStart();
	
	if( Ext.Cmp('UserId').getChecked().length==0 ){ 
		Ext.Msg("Please select Agent").Info(); }	
	else
	{
	   Ext.Ajax ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/AddAvailableAgent/",
			method  : 'POST',
			param 	: {
				UserId : Ext.Cmp('UserId').getChecked()
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(err){
					if((typeof(err)=='object') && (err.success) ) {
						Ext.Msg("Add User").Success();
						Ext.DOM.LaodAgentState(0);
						Ext.DOM.QualityAgentReady();
						//Ext.Cmp('import_user').setFocus();	
					}
					else
					{
						Ext.Msg("Add User").Failed();
						Ext.DOM.AjaxStop();
						Ext.DOM.QualityAgentReady();
					}
				});
			}
		}).post();
	}	
 }
 
 /*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
 Ext.DOM.UpdateQualityAgent = function(){
	
	var QualityAgentId = Ext.Cmp('QualityAgentId').getChecked();
	var QulaityStaffId = Ext.Cmp('QulaityStaffId').getValue();
	
	if( Ext.Cmp('QualityAgentId').getChecked().length==0 ){ 
		Ext.Msg("Please select Quality Agent Ready").Info();}
	else if( Ext.Cmp('QulaityStaffId').empty()){ 
		Ext.Msg("Quality Staff").Info();}
	else
	{
		Ext.Ajax ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/UpdateQualityAgent/",
			method  : 'POST',
			param 	: {
				QualityAgentId : QualityAgentId,
				QulaityStaffId : QulaityStaffId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(err){
					if((typeof(err)=='object') && (err.success) ) {
						Ext.Msg("Update Quality Agent").Success();
						Ext.DOM.QualityAgentReady();	
						//Ext.Cmp('QulaityStaffId').setFocus();
					}
					else
					{
						Ext.Msg("Update Quality Agent").Failed();
					}
				});
			}
		}).post();
   }
} 
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
Ext.DOM.RemoveQualityAgent = function(){
	
	var QualityAgentId = Ext.Cmp('QualityAgentId').getChecked();
	
	if( Ext.Cmp('QualityAgentId').getChecked().length==0 ){ 
		Ext.Msg("Please select Quality Agent Ready").Info();}
	else
	{
		Ext.Ajax ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/RemoveQualityAgent/",
			method  : 'POST',
			param 	: {
				QualityAgentId : QualityAgentId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(err){
					if((typeof(err)=='object') && (err.success) ) {
						Ext.Msg("Delete Quality Agent").Success();
						Ext.DOM.QualityAgentReady();	
						//Ext.Cmp('QulaityStaffId').setFocus();
					}
					else
					{
						Ext.Msg("Delete Quality Agent").Failed();
					}
				});
			}
		}).post();
   }
} 

/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
Ext.DOM.EmptyQualityAgent = function(){
	
	var QualityAgentId = Ext.Cmp('QualityAgentId').getChecked();
	
	if( Ext.Cmp('QualityAgentId').getChecked().length==0 ){ 
		Ext.Msg("Please select Quality Agent Ready").Info();}
	else
	{
		Ext.Ajax ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/EmptyQualityAgent/",
			method  : 'POST',
			param 	: {
				QualityAgentId : QualityAgentId
			},
			ERROR : function(e){
				Ext.Util(e).proc(function(err){
					if((typeof(err)=='object') && (err.success) ) {
						Ext.Msg("Empty Quality Staff").Success();
						Ext.DOM.QualityAgentReady();	
						//Ext.Cmp('QulaityStaffId').setFocus();
					}
					else
					{
						Ext.Msg("Empty Quality Staff").Failed();
					}
				});
			}
		}).post();
   }
} 
  
// Ext.DOM.ShowByQualityId  
  
Ext.DOM.ShowByQualityId  = function(options){
	 Ext.DOM.AjaxStart();
	   Ext.Ajax
	   ({
			url  	: Ext.DOM.INDEX+"/QtySetAgent/QualityAgentReady/",
			method  : 'POST',
			param 	: {
				time 		: Ext.Date().getDuration(),
				QulityId 	: Ext.Cmp(options.id).getValue(),
				Hide 		: Ext.Cmp('hidden_ready').getValue(),
				page 		: 0,
			}
		}).load('content_agent_import');
 } 
// Ext.DOM.RemoveQualityAgent()

Ext.IsHide = function( checkedbox ){
	Ext.DOM.QualityAgentReady();
}

Ext.DOM.LaodAgentState(0);
	
});


</script>

<div>
	<?php $this -> load->view("{$view_layout}/view_agent_state");?>
</div>