<form name="frmReport">
<script>
	Ext.DOM.ListMgr=function()
	{
		Ext.Ajax
		(
			{
				url		: Ext.DOM.INDEX +'/SumQAReport/FilterManager/',
				param	: 	{
								FilterBy : Ext.Cmp('FilterBy').getValue()
							}
			}
		).load('list_mgr');
	}
	
	Ext.DOM.ListSpv=function()
	{
		Ext.Ajax
		(
			{
				url		: Ext.DOM.INDEX +'/SumQAReport/FilterSupervisor/',
				param	: 	{
								FilterMgr : Ext.Cmp('mgrname').getValue()
							}
			}
		).load('list_spv');
	}
	
	Ext.DOM.ShowReport=function()
	{
		if(Ext.Cmp('FilterBy').getValue()=='')
		{
			alert('Please Choose Filter By!');
		}
		else if(Ext.Cmp('mgrname').getValue()=='')
		{
			alert('Please Choose Manager!');
		}
		else if(Ext.Cmp('spvname').getValue()=='')
		{
			alert('Please Choose Supervisor!');
		}
		else if(Ext.Cmp('start_date').getValue()=='' && Ext.Cmp('end_date').getValue()=='')
		{
			alert('Please Choose Interval!');
		}
		else
		{
			Ext.Window
			(
				{
					url 	: Ext.DOM.INDEX +'/SumQAReport/ShowReport/',
					param	: 	{
									mgr_id		: Ext.Cmp('mgrname').getValue(),
									spv_id		: Ext.Cmp('spvname').getValue(),
									start_date	: Ext.Cmp('start_date').getValue(),
									end_date	: Ext.Cmp('end_date').getValue()
								}
				}
			).newtab();
		}
	}
	
	Ext.DOM.ShowExcel = function()
	{
		Ext.Window
		(
			{
				url 	: Ext.DOM.INDEX +'/SumQAReport/ShowExcel/',
				param 	:	{ 
								mgr_id		: Ext.Cmp('mgrname').getValue(),
								spv_id		: Ext.Cmp('spvname').getValue(),
								start_date	: Ext.Cmp('start_date').getValue(),
								end_date	: Ext.Cmp('end_date').getValue()
							}
			}
		).newtab();
	}
	
	Ext.query('.date').datepicker({
			showOn : 'button', 
			buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
			buttonImageOnly	: true, 
			dateFormat : 'dd-mm-yy',
			readonly:true,
			onSelect	: function(date){
				if(typeof(date) =='string'){
					var x = date.split('-');
					var retur = x[2]+"-"+x[1]+"-"+x[0];
					if(new Date(retur) > new Date()) {
						Ext.Cmp($(this).attr('id')).setValue('');
					}
				}
			}
		});
</script>
<fieldset class="corner" style='margin-top:-10px;'>
<legend class="icon-menulist">&nbsp;&nbsp;Group Filter </legend>
<div>
	<table cellpadding='4' cellspacing=4>
		<tr>
			<td class="text_caption bottom">Filter By </td>
			<td><?php __(form()->combo('FilterBy','select long', $filterby, null, array("change" => "Ext.DOM.ListMgr(this)") ));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Manager</td>
			<td><span id="list_mgr"><?php __(form()->combo('mgrname','select long', array(), null ));?></span></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Supervisor</td>
			<td><span id="list_spv"><?php __(form()->combo('spvname','select long', array(), null ));?></span></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom">Interval </td>
			<td class='bottom'>
				<?php __(form()->input('start_date','input_text box date'));?> &nbsp-
				<?php __(form()->input('end_date','input_text box date'));?>
			</td>
		</tr>
		
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td class='bottom'>
				<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReport();") ));?>
				<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
			</td>
		</tr>
	</table>
</div>
</fieldset>
</form>