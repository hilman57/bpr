<?php 

/** set label style on selected **/
 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');
 page_set_align('Attachment','center'); 
/** get label header attribute **/

 $labels =& page_labels();
 $primary =& page_primary();
 $aligns =& page_get_align();
?>

<table width="100%" class="custom-grid" cellspacing="1">
<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) :
			$class=&page_header($Field);  
		?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle <?php echo $aligns[$Field];?>"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?>
	</tr>
<tbody>

<?php 
$no = 1;
foreach( $page -> result_assoc() as $rows )
{ 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>
	<tr class="onselect" bgcolor='<?php echo $color;?>' id="rows_"<?php echo $rows[$primary];?>>
		<td class="content-first center" width='5%'> <?php echo form()->checkbox($primary, null, $rows[$primary],array("click"=>"Ext.Cmp('{$primary}').oneChecked(this);"), null ); ?>
		<td class="content-middle center"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field); ?>
		<?php if( $rows[$Field]=='Download') : ?>
			<td class="content-middle <?php echo $aligns[$Field];?>" <?php __($color) ?> >
				<a href="javascript:void(0);" title='Download Attachment' onclick="Ext.DOM.DownloadAttachment('<?php echo $rows[$primary]; ?>');" >Attachment</a>
			</td> 
		<?php else : ?>
			<td class="content-middle <?php echo $aligns[$Field];?>" <?php __($color) ?> ><?php echo ( $rows[$Field]?$rows[$Field]:'-' ); ?></td> 
		<?php endif; ?>
		<?php endforeach;  ?>
		
	</tr>	
</tbody>
<?php
	$no++;
}

?>
</table>



