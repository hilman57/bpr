<?php  
/** set label style on selected **/

page_background_color('#FFFCCC');
page_font_color('#022706');

/** set align **/

page_set_align("AdpCode","center");
page_set_align("ID","center");
page_set_align("AdpName","left");
page_set_align("AdpFlags","center");
page_set_align("AdpDescription","left");
page_set_align("AdpLimit","center");
   
/** get label header attribute **/

$labels =& page_labels();
$primary =& page_primary();
$align =& page_get_align();

?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center">&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('<?php __($primary);?>').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No.</th>	
		<?php foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); ?>
		<th nowrap class="font-standars ui-corner-top ui-state-default"
			align="<?php __($align[$Field]);?>"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach($page->result_assoc() as $rows) 
{
  $color= ($no%2!=0?'#FAFFF9':'#FFFFFF');
?>
	<tr class="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"> <?php echo form()->checkbox($primary,null,$rows[$primary]);?></td>
		<td class="content-middle"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) : $color=&page_column($Field);  ?>
		<td class="content-middle" <?php __($color) ?> align="<?php __($align[$Field]);?>" ><?php echo $rows[$Field]; ?></td>
		<?php endforeach; ?>
	</tr>
</tbody>
<?php
	$no++; 
};
?>
</table>
	
<!-- END OF FILE  -->
<!-- location : // ../application/layout/user/view_user_list.php -->