<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  
  <style>
    body{font-size:12px;}
    table{border-collapse:collapse;border-spacing:0}td,th{padding:0}.table{width:100%;max-width:100%;margin-bottom:20px}.table>tbody>tr>td,.table>tbody>tr>th,.table>tfoot>tr>td,.table>tfoot>tr>th,.table>thead>tr>td,.table>thead>tr>th{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd}.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table>caption+thead>tr:first-child>td,.table>caption+thead>tr:first-child>th,.table>colgroup+thead>tr:first-child>td,.table>colgroup+thead>tr:first-child>th,.table>thead:first-child>tr:first-child>td,.table>thead:first-child>tr:first-child>th{border-top:0}.table>tbody+tbody{border-top:2px solid #ddd}.table .table{background-color:#fff}.table-condensed>tbody>tr>td,.table-condensed>tbody>tr>th,.table-condensed>tfoot>tr>td,.table-condensed>tfoot>tr>th,.table-condensed>thead>tr>td,.table-condensed>thead>tr>th{padding:5px}.table-bordered{border:1px solid #ddd}.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #ddd}.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border-bottom-width:2px}.table-striped>tbody>tr:nth-of-type(odd){background-color:#f9f9f9}.table-hover>tbody>tr:hover{background-color:#f5f5f5}.table>tbody>tr.active>td,.table>tbody>tr.active>th,.table>tbody>tr>td.active,.table>tbody>tr>th.active,.table>tfoot>tr.active>td,.table>tfoot>tr.active>th,.table>tfoot>tr>td.active,.table>tfoot>tr>th.active,.table>thead>tr.active>td,.table>thead>tr.active>th,.table>thead>tr>td.active,.table>thead>tr>th.active{background-color:#f5f5f5}.table-hover>tbody>tr.active:hover>td,.table-hover>tbody>tr.active:hover>th,.table-hover>tbody>tr:hover>.active,.table-hover>tbody>tr>td.active:hover,.table-hover>tbody>tr>th.active:hover{background-color:#e8e8e8}.table>tbody>tr.success>td,.table>tbody>tr.success>th,.table>tbody>tr>td.success,.table>tbody>tr>th.success,.table>tfoot>tr.success>td,.table>tfoot>tr.success>th,.table>tfoot>tr>td.success,.table>tfoot>tr>th.success,.table>thead>tr.success>td,.table>thead>tr.success>th,.table>thead>tr>td.success,.table>thead>tr>th.success{background-color:#dff0d8}.table-hover>tbody>tr.success:hover>td,.table-hover>tbody>tr.success:hover>th,.table-hover>tbody>tr:hover>.success,.table-hover>tbody>tr>td.success:hover,.table-hover>tbody>tr>th.success:hover{background-color:#d0e9c6}.table>tbody>tr.info>td,.table>tbody>tr.info>th,.table>tbody>tr>td.info,.table>tbody>tr>th.info,.table>tfoot>tr.info>td,.table>tfoot>tr.info>th,.table>tfoot>tr>td.info,.table>tfoot>tr>th.info,.table>thead>tr.info>td,.table>thead>tr.info>th,.table>thead>tr>td.info,.table>thead>tr>th.info{background-color:#d9edf7}.table-hover>tbody>tr.info:hover>td,.table-hover>tbody>tr.info:hover>th,.table-hover>tbody>tr:hover>.info,.table-hover>tbody>tr>td.info:hover,.table-hover>tbody>tr>th.info:hover{background-color:#c4e3f3}.table>tbody>tr.warning>td,.table>tbody>tr.warning>th,.table>tbody>tr>td.warning,.table>tbody>tr>th.warning,.table>tfoot>tr.warning>td,.table>tfoot>tr.warning>th,.table>tfoot>tr>td.warning,.table>tfoot>tr>th.warning,.table>thead>tr.warning>td,.table>thead>tr.warning>th,.table>thead>tr>td.warning,.table>thead>tr>th.warning{background-color:#fcf8e3}.table-hover>tbody>tr.warning:hover>td,.table-hover>tbody>tr.warning:hover>th,.table-hover>tbody>tr:hover>.warning,.table-hover>tbody>tr>td.warning:hover,.table-hover>tbody>tr>th.warning:hover{background-color:#faf2cc}.table>tbody>tr.danger>td,.table>tbody>tr.danger>th,.table>tbody>tr>td.danger,.table>tbody>tr>th.danger,.table>tfoot>tr.danger>td,.table>tfoot>tr.danger>th,.table>tfoot>tr>td.danger,.table>tfoot>tr>th.danger,.table>thead>tr.danger>td,.table>thead>tr.danger>th,.table>thead>tr>td.danger,.table>thead>tr>th.danger{background-color:#f2dede}.table-hover>tbody>tr.danger:hover>td,.table-hover>tbody>tr.danger:hover>th,.table-hover>tbody>tr:hover>.danger,.table-hover>tbody>tr>td.danger:hover,.table-hover>tbody>tr>th.danger:hover{background-color:#ebcccc}
    td{text-align: center;}
    .text-left{text-align:left;}
    .bg-grey{background: #eaeaea;}
  </style>

</head>
<body>

<h1>REPORT DAY TO DAY</h1>
  
  <?php

function load_report($current,$awal,$akhir){

  //$awal = '2019-08-08 00:00:00';
  //$akhir = '2019-08-08 23:00:00';

  $no_agent = 1;
  $data_agent = $current->db->query("
        SELECT	
        ag.UserId AS TL_UID,
        ag.full_name as TL,
        c.id AS AGENT,
        c.full_name AS DC,
        a.CallAccountStatus AS ACCOUNT_STATUSID,
        st.CallReasonDesc as ACCOUNT_STATUS,
        a.AccountNo AS 'ACCOUNTNO',
        a.CallHistoryCallDate 'TGLUPDATE'

      FROM (
        SELECT AccountNo,MAX(CallHistoryCallDate) AS GROUP_ID 
        FROM t_gn_callhistory 	
        WHERE
          CallHistoryCallDate BETWEEN '".$awal."' AND '".$akhir."' 
          AND TeamLeaderId!=''
          
          
        GROUP BY AccountNo,AgentCode
      ) AS TBL_TMP
      
      INNER JOIN t_gn_callhistory a ON (a.CallHistoryCallDate=TBL_TMP.GROUP_ID AND a.AccountNo=TBL_TMP.AccountNo)

      LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
      LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
      LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
      LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId 

      WHERE
        a.CallHistoryCallDate BETWEEN '".$awal."' AND '".$akhir."' 
        AND ag.userId!=''
        
      ORDER BY TL_UID,a.ACCOUNTNO ASC
  ")->result();
  
  $user_TL = array();
  

  foreach ($data_agent as $r) {
    $tl_uid = '#'.$r->TL_UID;
    $tl_name = $r->TL;
    $status = $r->ACCOUNT_STATUS;
    $agent_id = $r->AGENT;

    if ($user_TL[$tl_uid]){

      // agent array
      if($agent[$agent_id][$status]){
        $agent[$agent_id][$status][] = $r;
      }else{

        $agent[$agent_id][$status] = array();
        $agent[$agent_id][$status][] = $r;
        
      }

      $user_TL[$tl_uid] = $agent;

      //print_r($user_TL);
      //exit();

    }else{
      $user_TL[$tl_uid] = array();
      $agent = array();
      
      // agent array
      if($agent[$agent_id][$status]){
        $agent[$agent_id][$status][] = $r;
      }else{
        $agent[$agent_id][$status] = array();
        $agent[$agent_id][$status][] = $r;
      }

      $user_TL[$tl_uid] = $agent;
      //print_r($user_TL);
      //exit();
    }
    
  }

  //print_r($user_TL);
  //exit();

  $ON_STATUS = array(
    'NEW' => 0, 
    'OS-ON PROCESS' => 0,
    'OP-ON PROSPECT' => 0,
    'VALID' => 0,
    'ON NEGO' => 0,
    'PTP-NEW' => 0,
    'PTP-POP' => 0,
    'PTP-BP' => 0,
    'PAID OFF' => 0,
    'BUSY LINE' => 0, 
    'DECEASE' => 0
  );

  $user = array();
  foreach ($user_TL as $user_id => $groups) {
    foreach($groups as $agent_id=>$row){
      
      $data = $ON_STATUS;
      $info = array();

      foreach($row as $status=>$r){
        $info = (array)$r[0];
        $data[trim($status)] = count($r);
      }
      unset($info['ACCOUNT_STATUS']);
      
      $data = array_merge($info,$data);

      $user[$user_id][$agent_id] = $data;
      
    }
  }
   

  $USERS = array();  
  $arr = array();

  foreach ($user as $user_id => $groups) {

    $ROWS = array();
    
    $first = true;
    $TOTAL_NEW = 0;
    $TOTAL_ON_PROCESS = 0;
    $TOTAL_ON_PROSPECT = 0;
    $TOTAL_VALID = 0;
    $TOTAL_ON_NEGO = 0;
    $TOTAL_PTP_NEW = 0;
    $TOTAL_PTP_POP = 0;
    $TOTAL_PTP_BP = 0;
    $TOTAL_PAID_OFF = 0;
    $TOTAL_BUSY_LINE = 0;
    $TOTAL_DECEASE = 0;
    $ALL_GRANT_TOTAL = 0;  
    $ALL_ATTEMP = 0;  

    foreach ($groups as $agent_id => $row){

      $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
      $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];

      $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OS-ON PROSPECT'];
      $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
      $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
      $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
      $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
      $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
      $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
      $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
      $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];

      $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                    $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
      
      $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
      $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 

      // ambil dari sini
      array_push($arr, $GRANT_TOTAL);
      
      $ROWS[$row['AGENT']] = array(
        'AGENT' => $row['AGENT'],
        'DC' => $row['DC'],
        'TOTAL'=> $GRANT_TOTAL
      );

      $USERS[$user_id] = array(
        'TL'=> $row['TL'],
        'rows' => $ROWS
      );

    }
  }

  //print_r($USERS);
  //exit();

  return $USERS;
}

$report = array();

$start_date = $_GET['start_date'];
$total_day = date('d',strtotime($start_date));


for($i=1;$i<=$total_day;$i++){
  $day = str_pad($i, 2, "0", STR_PAD_LEFT);
  $ym = date('Y-m',strtotime($start_date));

  $awal = $ym.'-'.$day.' 00:00:00';
  $akhir = $ym.'-'.$day.' 23:00:00';
  
  $r = load_report($this,$awal,$akhir);
  
  $report[] = $r;
   
}

// print_r($report);
// exit();

$report_agent = array();
$user_TL = array();

foreach($report as $rows){
  
  foreach($rows as $user_id => $user){
    
    $user_TL[$user_id] = $user['TL'];

    foreach($user['rows'] as $agent_id => $agent){
      
      $report_agent[$user_id][$agent_id]= $agent['DC'];    
      
    }
     
  }
  
}

?>

<table class="table table-border table-hover">
  <thead>
    <tr>
      
      <th class="text-left">TL</th>
      <th>AGENT</th>
      <th class="text-left">DC</th>
     <?php

      $no=1;
      for($no=1;$no<=$total_day;$no++){
        echo '<th>'.$no.'</th>';
      }
      
      ?>
      <th>TOTAL</th>
    </tr>
  <thead>

  <tbody>
    <?php
      echo '<br>';
      // print_r($arrtl);
      
      foreach($report_agent as $user_id => $agents){
        $f=true;
        
        $TOTAL_STATUS_PERDAY = array();
        $TL=1; 

        foreach($agents as $agent_id => $agent_name){
          // print_r($arrtl);
          echo '<tr>'; 
          
          if($f){
           echo '<td class="text-left">'.$user_TL[$user_id].'</td>';
           $f=false;
          }else{
            echo '<td></td>';
          }

          echo '<td>'.$agent_id.'</td>';
          echo '<td class="text-left">'.$agent_name.'</td>';

          # TOTAL HORIZONTAL /AGENT
          $xx=0;
          $TOTAL_STATUS_AGENT = 0;

          while($xx < $total_day){
            $agent = $report[$xx][$user_id]['rows'][$agent_id];
            
            $total_status = (int)$agent['TOTAL'];
            echo '<td>'.(($total_status) ? $total_status : '-').'</td>';

            $xx++;

            $TOTAL_STATUS_AGENT = $TOTAL_STATUS_AGENT + (int)$agent['TOTAL'];

            $TOTAL_STATUS_PERDAY[$xx] = $TOTAL_STATUS_PERDAY[$xx] + (int)$agent['TOTAL'];
          }

          echo '<td>'.$TOTAL_STATUS_AGENT.'</td>';
          
          echo '</tr>';
        }

        # TOTAL VERTICAL /TL
        echo '<tr class="bg-grey">';
        echo '<td colspan="3" class="text-left"><strong>TOTAL</strong></td>';

        $ALL_TOTAL = 0;
        foreach($TOTAL_STATUS_PERDAY as $TOTAl_PERDAY){
          echo '<td>'.(($TOTAl_PERDAY) ? $TOTAl_PERDAY : '-').'</td>';
          $ALL_TOTAL = $ALL_TOTAL + $TOTAl_PERDAY;
          
        }
        
        echo '<td>'.$ALL_TOTAL.'</td>';
        echo '</tr>';        

        echo '<tr><td colspan="'.($total_day+3).'"></td></tr>';

      }
    ?>
  </tbody>
  
</table>

</body>
</html>