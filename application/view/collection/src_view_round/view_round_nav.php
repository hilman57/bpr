<?php __(javascript()); ?>
<script type="text/javascript">

// @ pack : keyword to serach 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })(); 
 
 
// @ pack : keyword to serach 

Ext.DOM.Reason = [];
Ext.DOM.datas = {}
Ext.DOM.handling = '<?php echo $this -> EUI_Session -> _get_session('HandlingType'); ?>';
Ext.EQuery.TotalPage = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : keyword to serach 
 
Ext.DOM.datas =  {
	aksesall_account_status : "<?php echo _get_exist_session('aksesall_account_status');?>",
	aksesall_agent_id 		: "<?php echo _get_exist_session('aksesall_agent_id');?>",
	aksesall_call_status 	: "<?php echo _get_exist_session('aksesall_call_status');?>",
	aksesall_campaign_id 	: "<?php echo _get_exist_session('aksesall_campaign_id');?>",
	aksesall_cust_id 		: "<?php echo _get_exist_session('aksesall_cust_id');?>",
	aksesall_cust_name 		: "<?php echo _get_exist_session('aksesall_cust_name');?>",
	aksesall_recsource 		: "<?php echo _get_exist_session('aksesall_recsource');?>",
	aksesall_start_amountwo : "<?php echo _get_exist_session('aksesall_start_amountwo');?>",
 	aksesall_end_amountwo 	: "<?php echo _get_exist_session('aksesall_end_amountwo');?>",
	aksesall_start_date 	: "<?php echo _get_exist_session('aksesall_start_date');?>",
	aksesall_end_date 		: "<?php echo _get_exist_session('aksesall_end_date');?>",
	order_by 			: "<?php echo _get_exist_session('order_by');?>",
	type	 			: "<?php echo _get_exist_session('type');?>"
}

// @ pack : keyword to serach 
 
Ext.DOM.navigation = {
	custnav  : Ext.DOM.INDEX+'/SrcRound/index/',
	custlist : Ext.DOM.INDEX+'/SrcRound/Content/',
}
		
/**
 ** javscript prototype system
 ** version v.0.1
 **/ 	
 
Ext.EQuery.construct(navigation,Ext.DOM.datas)
Ext.EQuery.postContentList();

/**
 ** javscript prototype system
 ** version v.0.1
 **/ 
 
// @ pack : On ENTER Keyboard 

Ext.DOM.searchCustomer = function() {
  var param = Ext.Serialize('flwallCustomers').getElement();
	  Ext.EQuery.construct( Ext.DOM.navigation,Ext.Join([param]).object() );
	  Ext.EQuery.postContent();
}

// @ pack : On ENTER Keyboard 

Ext.DOM.resetSeacrh = function(){
	Ext.Serialize('flwallCustomers').Clear();
	Ext.DOM.searchCustomer();
}

/** Check Valid Customer **/
Ext.DOM.ValidAccess = function(TextId) {
var ArrId = TextId.split(",");
var CustomerId = ArrId[0];
var BucketRandomId = ArrId[1];

	var Validation= Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/SrcRound/CheckAccountAccess',
				method 	: 'GET',
				param 	: { CustomerId : CustomerId,
						BucketRandomId : BucketRandomId
				}	
		}).json();
	return Validation.Success;
}
// @ pack : On ENTER Keyboard 

Ext.DOM.CallCustomer = function( TextId ) {
var ArrId = TextId.split(",");

var CustomerId = ArrId[0];
var BucketRandomId = ArrId[1];
	if( CustomerId!='') 
	{	
		if(Ext.DOM.ValidAccess(TextId))
		{
			Ext.ActiveMenu().NotActive();
			Ext.EQuery.Ajax ({
				url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
				method  : 'GET',
				param 	: {
					CustomerId : CustomerId,
					BucketRandomId : BucketRandomId,
					ControllerId : Ext.DOM.INDEX +'/SrcRound/index/', 
				}
			});
		}
		else
		{
			Ext.Msg("This debitur expire !").Info(); 
			searchCustomer();
		}
	} 
	else
	{ 
		Ext.Msg("No Customers Selected !").Info(); 
	}
 
}


// @ pack : On ENTER Keyboard 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Search'],['Clear']],
		extMenu  : [['searchCustomer'],['resetSeacrh']],
		extIcon  : [['zoom.png'],['zoom_out.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});
		
// @ pack : On ENTER Keyboard 

Ext.DOM.enterSearch = function( e ) {
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
	
</script>

<!-- @ pack : start content -->
<fieldset class="corner" onkeydown="enterSearch(event);">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="result_content_add" class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="flwallCustomers">
	<table cellspacing="0">
		<tr>
			<td class="text_caption bottom"> # Product&nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('aksesall_campaign_id','select auto', $CampaignId, _get_exist_session('aksesall_campaign_id'));?></td>
			<td class="text_caption bottom"> # Deskoll &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('aksesall_agent_id','select auto', $Deskoll, _get_exist_session('aksesall_agent_id'));?></td>
			<td class="text_caption bottom"> # Last Call date &nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('aksesall_start_date','input_text date', _get_exist_session('aksesall_start_date'));?>
				&nbsp;-&nbsp;	
				<?php echo form()->input('aksesall_end_date','input_text date', _get_exist_session('aksesall_end_date'));?>
			</td>
		</tr>
		<tr>
			<td class="text_caption bottom"> # Customer ID&nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('aksesall_cust_id','input_text long', _get_exist_session('aksesall_cust_id'));?></td>
			<td class="text_caption bottom"> # Account Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('aksesall_account_status','select auto', $AccountStatus, _get_exist_session('aksesall_account_status'));?></td>
			<td class="text_caption bottom"> # Recsource &nbsp;:</td>
			<td class="bottom"><?php echo form()-> input('aksesall_recsource','input_text long', _get_exist_session('aksesall_recsource'));?></td>
		</tr>
		
		<tr>
			<td class="text_caption bottom"> # Customer Name&nbsp;:</td>
			<td class="bottom"><?php echo form()->input('aksesall_cust_name','input_text long', _get_exist_session('aksesall_cust_name'));?></td>
			<td class="text_caption bottom"> # Last Call Status &nbsp;:</td>
			<td class="bottom"><?php echo form()->combo('aksesall_call_status','select auto', $LastCallStatus, _get_exist_session('aksesall_call_status'));?></td>
			<td class="text_caption bottom"> # Amount WO&nbsp;:</td>
			<td class="bottom">
				<?php echo form()->input('aksesall_start_amountwo','input_text box', _get_exist_session('aksesall_start_amountwo') );?>
				&nbsp; <span>to</span>&nbsp;	
				<?php echo form()->input('aksesall_end_amountwo','input_text box', _get_exist_session('aksesall_end_amountwo') );?>
			</td>
		</tr>
	</table>
	 </form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
	<div class="content_table" style="margin:-4px;"></div>
	<div id="pager"></div>
</div>
</fieldset>	
<!-- @ pack : stop  content -->