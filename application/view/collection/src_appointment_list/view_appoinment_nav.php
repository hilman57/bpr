<?php echo javascript(); ?>
<script type="text/javascript">
/* create object **/


Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 
var Reason = [];
var datas  = {}

Ext.EQuery.TotalPage   = <?php echo $page->_get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo $page->_get_total_record(); ?>;
/* catch of requeet accep browser **/
	
 datas = {
	app_cust_name  		 : '<?php echo _get_post('app_cust_name');?>',
	app_cif_number  	 : '<?php echo _get_post('app_cif_number');?>',
	app_dc  			 : '<?php echo _get_post('app_dc');?>',
	app_campaign  		 : '<?php echo _get_post('app_campaign');?>',
	app_call_reason  	 : '<?php echo _get_post('app_call_reason');?>',
	app_start_date		 : '<?php echo _get_post('app_start_date');?>',
	app_end_date		 : '<?php echo _get_post('app_end_date');?>',
	order_by 			 : '<?php echo _get_exist_session('order_by');?>',
	type	 			 : '<?php echo _get_exist_session('type');?>',
	policy_number		 : '<?php echo _get_post('policy_number');?>'
}
			
	/* assign navigation filter **/
		
Ext.DOM.navigation = {
	custnav	 : Ext.DOM.INDEX +'/SrcAppoinment/index/',
	custlist : Ext.DOM.INDEX +'/SrcAppoinment/Content/'
}
		
/* assign show list content **/
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/* function searching customers **/

Ext.DOM.searchCustomer = function() {
	Ext.EQuery.construct( navigation, {
		app_cust_name  		 : Ext.Cmp('app_cust_name').getValue(),
		app_cif_number  	 : Ext.Cmp('app_cif_number').getValue(),
		app_start_date 		 : Ext.Cmp('app_start_date').getValue(),
		app_end_date 		 : Ext.Cmp('app_end_date').getValue(),
		app_dc  			 : Ext.Cmp('app_dc').getValue(),
		app_campaign  		 : Ext.Cmp('app_campaign').getValue(),
		app_call_reason  	 : Ext.Cmp('app_call_reason').getValue(),
		policy_number		 : Ext.Cmp('policy_number').getValue()
	});
	
	Ext.EQuery.postContent();
}
		
/* function clear searching form **/	
	
Ext.DOM.resetSeacrh = function() {
	Ext.Serialize('frmAppointment').Clear();
	Ext.DOM.searchCustomer();
}
		
 /* go to call contact detail customers **/
 
 Ext.DOM.gotoCallCustomer = function()
 {
	var CustomerId  = Ext.Cmp('chk_cust_appoinment').getChecked();
	if( Ext.Cmp('chk_cust_appoinment').empty()!= true )
	{	
		if( CustomerId.length == 1 ){
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX +'/SrcAppoinment/Update/',
				method  : 'POST',
				param 	: { CustomerId : CustomerId },
				ERROR	: function(e) {
					Ext.Util(e).proc(function(update)
					{
						if( update.success ) 
						{
							
							Ext.ShowMenu( new Array('ModContactDetail','index'), 
								Ext.System.view_file_name(), 
							{
								CustomerId : CustomerId,
								ControllerId : Ext.EventUrl(new Array("SrcAppoinment", "index")).Apply()
							}); 
							
							// Ext.EQuery.Ajax
							// ({
								// url : Ext.DOM.INDEX +'/SrcCustomerList/ContactDetail/',
								// method : 'GET',
								// param : {
									// CustomerId : CustomerId,
									// ControllerId : Ext.DOM.INDEX +'/SrcAppoinment/index/', 
								// }
							// });
						}
					});
				}
			}).post();
		}
		else{ Ext.Msg("Select One Customers !").Info(); }			
	}
	else{ Ext.Msg("No Customers Selected !").Info(); }	
 }
	
	
/* memanggil Jquery plug in */
$(function(){
	$('#toolbars').extToolbars({
		extUrl   :Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Search'],['Clear']],
		extMenu  :[['searchCustomer'],['resetSeacrh']],
		extIcon  :[['zoom.png'],['cancel.png']],
		extText  :true,
		extInput :true,
		extOption:[{
						render : 4,
						type   : 'combo',
						header : 'Call Reason ',
						id     : 'v_result_customers', 	
						name   : 'v_result_customers',
						triger : '',
						store  : Reason
					}]
		});
			
	$('.date').datepicker({
		showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly: true, 
		dateFormat:'dd-mm-yy',
		readonly:true
	});
});

Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}
		
</script>
<!-- start : content -->
		<fieldset class="corner" OnKeyDown = "enterSearch(event);" style="margin:2px;">
			<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
			<div id="result_content_add" style="padding-bottom:4px;margin-top:2px;margin-bottom:4px;" >
			<form name="frmAppointment">		
					<table cellspacing="0">
						<tr>
							<td class="text_caption bottom"># Nama Pelanggan</td>
							<td>:</td>
							<td class='bottom'><?php echo form() -> input('app_cust_name','input_text long', $this-> URI -> _get_post('app_cust_name'));?></td>
							<td class="text_caption bottom"># ID Pelanggan</td>
							<td>:</td>
							<td class='bottom'><?php echo form() -> input('app_cif_number','input_text long', $this-> URI -> _get_post('app_cif_number'));?></td>
							<td class="text_caption bottom"># Tanggal Appointment </td>
							<td>:</td>
							<td class='bottom'>
								<?php echo form() -> input('app_start_date','input_text date', $this-> URI -> _get_post('app_start_date'));?>&nbsp;-&nbsp;
								<?php echo form() -> input('app_end_date','input_text date', $this-> URI -> _get_post('app_end_date'));?>
							</td>
						</tr>
						<tr>
							<td class="text_caption bottom"># Product </td>
							<td>:</td>
							<td class='bottom'><?php echo form() -> combo('app_campaign','select auto', (isset($CampaignId)?$CampaignId :null ) , $this-> URI -> _get_post('app_campaign'));?></td>
							<td class="text_caption bottom"># Status Akun</td>
							<td>:</td>
							<td class='bottom'><?php echo form() -> combo('app_call_reason','select long', (isset($CallResult)?$CallResult :null ) , $this-> URI -> _get_post('app_call_reason'));?></td>
							
							
							<?php if ($this -> EUI_Session -> _get_session('HandlingType') != USER_AGENT_OUTBOUND and 
										$this -> EUI_Session -> _get_session('HandlingType')!=USER_AGENT_INBOUND ): ?>
							<td class="text_caption bottom"># Penelepon</td>
							<td>:</td>
							<td class='bottom'><?php echo form() -> combo('app_dc','select long', (isset($Agent) ? $Agent :null ) , $this-> URI -> _get_post('app_dc'));?></td>
							<?php endif ?>
						</tr>
					</table>
					</form>
				</div>
				<div id="toolbars" style="margin:5px;"></div>
				<div id="customer_panel" class="box-shadow" style="background-color:#FFFFFF;">
					<div class="content_table" style="margin:-4px;"></div>
					<div id="pager"></div>
				</div>
		</fieldset>	
		
	<!-- stop : content -->
	
	
	
	
	