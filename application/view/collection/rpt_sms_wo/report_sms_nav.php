<form name="frmReport">
<script>	
	Ext.DOM.ShowReportWo = function()
	{
		// var supervisor	= Ext.Cmp('supervisor').getValue();
		var start_date_wo	= Ext.Cmp('start_date_wo').getValue();
        // console.log(start_date_wo);
		
		if(start_date_wo == "") {
			alert('Set Interval, Please!');
		} else {
			Ext.Window
			({
				url 	: Ext.DOM.INDEX +'/SmsWo/ShowReportWo',
				param 	: {
					start_date : Ext.Cmp('start_date_wo').getValue(),
				}
			}).newtab();	
		}
	}
	
	
	Ext.DOM.ShowExcel = function()
	{
		var start_date	= Ext.Cmp('start_date_wo').getValue();
		if(start_date_wo == "") {
			alert('Set Interval, Please!');
		} else {
			Ext.Window
			({
				url 	: Ext.DOM.INDEX +'/SmsWo/ShowExcel',
				param 	: {
					start_date : Ext.Cmp('start_date_wo').getValue(),
				}
			}).newtab();	
		}
	}
	
	Ext.query('.date').datepicker({
			showOn : 'button', 
			buttonImage : Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
			buttonImageOnly	: true, 
			dateFormat : 'yy-mm-dd',
			//dateFormat : 'dd-mm-yy',
			readonly:true,
			onSelect	: function(date){
				if(typeof(date) =='string'){
					var x = date.split('-');
					var retur = x[2]+"-"+x[1]+"-"+x[0];
					if(new Date(retur) > new Date()) {
						Ext.Cmp($(this).attr('id')).setValue('');
					}
				}
			}
		});
</script>
<fieldset class="corner" style='margin-top:0px;'>
<legend class="icon-menulist">&nbsp;&nbsp;REPORT SMS WO </legend>
<div>
	<table cellpadding='4' cellspacing=4>
		
		<tr>
			<td class="text_caption bottom">Interval </td>
			<td class='bottom'>
				<?php __(form()->input('start_date_wo','input_text box date'));?> 
			</td>
		</tr>
		
		<tr>
			<td class="text_caption"> &nbsp;</td>
			<td class='bottom'>
				<?php __(form()->button('','page-go button','Show',array("click"=>"Ext.DOM.ShowReportWo();") ));?>
				<?php __(form()->button('','excel button','Export',array("click"=>"Ext.DOM.ShowExcel();") ));?>
			</td>
		</tr>
	</table>
</div>
</fieldset>
</form>