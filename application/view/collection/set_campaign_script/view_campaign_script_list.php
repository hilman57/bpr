<?php ?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%">&nbsp;<a href="javascript:void(0);" >#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;No</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('b.CampaignCode');">&nbsp;Kode Produk </span></th>        
        <th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('b.CampaignDesc');">&nbsp;Nama Produk </span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.Description');">&nbsp;Script Title</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.ScriptFileName');">&nbsp;Script File</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.UploadDate');">&nbsp;Tanggal Upload </span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('c.full_name');">&nbsp;Upload Oleh User</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default lasted center"><span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('a.ScriptFlagStatus');">&nbsp;Status</span></th>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
	<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"><input type="checkbox" value="<?php echo $rows['ScriptId']; ?>" name="chk_result" id="chk_result"></td>
		<td class="content-middle"><?php echo $no ?></td>
		<td class="content-middle"><?php echo $rows['CampaignCode']; ?></td>
		<td class="content-middle"><?php echo $rows['CampaignDesc']; ?></td>
		<td class="content-middle"><?php echo $rows['Description']; ?></td>
		<td class="content-middle"><?php echo $rows['ScriptFileName']; ?></td>
		<td class="content-middle"><?php echo $this -> EUI_Tools ->_date_indonesia($rows['UploadDate']); ?></td>
		<td class="content-middle"><?php echo $rows['full_name']; ?></td>
		<td class="content-lasted"><?php echo $rows['ScriptFlagStatus'];?></td>
	</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>



