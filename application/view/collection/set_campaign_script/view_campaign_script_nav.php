<?php echo javascript(); ?>
<script type="text/javascript">
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
		
var datas = { 
	keywords : '<?php echo _get_post('keywords'); ?>',
	order_by : '<?php echo _get_post('order_by'); ?>',
	type 	 : '<?php echo _get_post('type'); ?>',
}
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 		
$(function(){
		$('#toolbars').extToolbars({
				extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
				extTitle :[['Enable'],['Disable'] ,['Add Script'],['Delete Script'],['Cancel'],['Search']],
				extMenu  :[['Enable'],['Disable'],['AddScript'],['Delete'],['cancelResult'],['Search']],
				extIcon  :[['accept.png'],['cancel.png'], ['add.png'],['delete.png'],['cancel.png'], ['zoom.png']],
				extText  :true,
				extInput :true,
				extOption: [{
						render	: 5,
						type	: 'text',
						id		: 'v_product_prefix', 	
						name	: 'v_product_prefix',
						value	: datas.keywords,
						width	: 200
					}]
			});
			
		});

Ext.EQuery.TotalPage   = <?php echo (INT)$page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo (INT)$page -> _get_total_record(); ?>;
	
/* assign navigation filter **/
var navigation = {
	custnav	 : Ext.DOM.INDEX +'/SetCampaignScript/index/',
	custlist : Ext.DOM.INDEX +'/SetCampaignScript/Content/',
}
		
/* assign show list content **/

Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();


// Search

Ext.DOM.Search = function(){
  Ext.EQuery.construct(navigation,{
	keywords : Ext.Cmp('v_product_prefix').getValue()
  });
  Ext.EQuery.postContent();
}

// cancelResult
		
var cancelResult=function(){
	Ext.Cmp('span_top_nav').setText("");
}


// AddScript

Ext.DOM.AddScript = function(){
	Ext.Ajax
	({
		url    : Ext.DOM.INDEX +"/SetCampaignScript/AddScript/",
		method : "GET",
		param  : {
			time : '<?php echo time(); ?>' 
		}
	}).load("span_top_nav");
}

// UploadScript

Ext.DOM.UploadScript = function()
{
	Ext.Ajax
	({
		url    	 : Ext.DOM.INDEX+'/SetCampaignScript/Upload/',
		method 	 : 'POST',
		file 	 : 'ScriptFileName',
		param    : {
			CampaignId : Ext.Cmp('CampaignId').getValue(),
			ScriptTitle : Ext.Cmp('Description').getValue(),
			Active : Ext.Cmp('ScriptFlagStatus').getValue()
		},
		complete : function(fn){
			try {
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Upload Script").Success();
					Ext.EQuery.construct(navigation,datas)
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Upload Script").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).upload();
}

		
//Disable
		
Ext.DOM.Disable = function()
{
	var ScriptId = Ext.Cmp('chk_result').getValue();
	if(Ext.Cmp('chk_result').empty()!=true)
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+ '/SetCampaignScript/SetActive/',
			method 	:'POST',
			param 	: {
				Active : 0,
				ScriptId : ScriptId
			},
			ERROR : function(e){
				var ERR = JSON.parse(e.target.responseText);
				
				if( ERR.success ) {
					Ext.Msg("Disable Script").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Disable Script").Failed();
					return false;
				}
			}
		}).post();
	}
	else{
		Ext.Msg("Please select Rows").Error();
	}
}

//Enable
		
Ext.DOM.Enable = function()
{
	var ScriptId = Ext.Cmp('chk_result').getValue();
	if(Ext.Cmp('chk_result').empty()!=true)
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+ '/SetCampaignScript/SetActive/',
			method 	:'POST',
			param 	: {
				Active : 1,
				ScriptId : ScriptId
			},
			ERROR : function(e){
				var ERR = JSON.parse(e.target.responseText);
				
				if( ERR.success ) {
					Ext.Msg("Enable Script").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Enable Script").Failed();
					return false;
				}
			}
		}).post();
	}
	else{
		Ext.Msg("Please select Rows").Error();
	}
}


//Delete
		
Ext.DOM.Delete = function()
{
	var ScriptId = Ext.Cmp('chk_result').getValue();
	if(Ext.Cmp('chk_result').empty()!=true)
	{
		if( Ext.Msg("Do you want delete this rows").Confirm() )
		{
			Ext.Ajax({
				url 	: Ext.DOM.INDEX+ '/SetCampaignScript/Delete/',
				method 	:'POST',
				param 	: {
					ScriptId : ScriptId
				},
				ERROR : function(e){
					var ERR = JSON.parse(e.target.responseText);
					
					if( ERR.success ) {
						Ext.Msg("Delete Script").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Delete Script").Failed();
						return false;
					}
				}
			}).post();
		}
	}
	else{
		Ext.Msg("Please select Rows").Error();
	}
}
		
</script>
	
	<!-- start : content -->
	<div id="WindowUserDialog2"></div>
		<fieldset class="corner">
			<legend class="icon-callresult">&nbsp;&nbsp;<span id="legend_title"></span> </legend>	
				<div id="toolbars"></div>
				<div id="span_top_nav"></div>
				<div class="content_table"></div>
				<div id="pager"></div>
				<div id="ViewCmp"></div>
		</fieldset>	
		
	<!-- stop : content -->
	
	
	
