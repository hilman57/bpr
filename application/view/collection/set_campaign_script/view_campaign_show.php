<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
 * @ def    : include Header page HTML on layout form 
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 *
 */
 
?>

<!DOCTYPE html>
<html>
<head>
<?php echo javascript(array(array('_file' => base_enigma().'/cores/EUI_1.0.2.js', 'eui_'=>'1.0.0', 'time'=>time()))); ?>
<script>

console.log(window);
/*
 * @ def 	: @ ready function document
 * ---------------------------------------------
 *
 * @ param	: document && window
 * @ aksess : public window 
 */
 
 Ext.document(document).ready(function() {
	
	
	Ext.Cmp('frame').setAttribute('width', Ext.Layout(window).ResizeWidth());
	Ext.Cmp('frame').setAttribute('height',Ext.Layout(window).ResizeHeight());	
 });
 
/*
 * @ def 	: @ ready function document
 * ---------------------------------------------
 *
 * @ param	: document && window
 * @ aksess : public window 
 */
 
Ext.document(document).resize(function(){
	
	Ext.Cmp('frame').setAttribute('width', Ext.Layout(window).ResizeWidth());
	Ext.Cmp('frame').setAttribute('height',Ext.Layout(window).ResizeHeight());	
});

/*
 * @ def 	: @ ready function document
 * ---------------------------------------------
 *
 * @ param	: document && window
 * @ aksess : public window 
 */
</script>
</head>
<body style="margin:0px;" oncontextmenu="return false;">
<?php if( file_exists( APPPATH . 'script/' . $Data['ScriptFileName'] ) ) { ?>
 <div style="margin:-8">
	<embed id="frame" src="<?php echo base_url() .'application/script/' . $Data['ScriptFileName']; ?>"></embed>
 </div>
<?php } ?>	
</body>
</html>