<?php ?>
<!DOCTYPE html>
<html>
<head>
	<title>FAX FILE :: PREVIEW</title>
	
	<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />
	
	<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
	<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script> 
	
<script>
Ext.document().ready(function(){
	Ext.DOM.PATH_URL = function(){
	var PATH_URL  = window.opener.Ext.DOM.SYSTEM.replace('system','application/temp/');
		PATH_URL += Ext.Cmp('OutboxId').getValue();
		return PATH_URL; 
	}

	Ext.Cmp('iframeWindowViewFax').getElementId().setAttribute("src",Ext.DOM.PATH_URL());
	Ext.Cmp('iframeWindowViewFax').getElementId().setAttribute("height",Ext.query(window).height());
	Ext.Cmp('iframeWindowViewFax').getElementId().setAttribute("width",Ext.query(window).width());
	
// RESIZE VALUE OF IFRAME 
	
	Ext.query(window).on('resize',function(e){
		Ext.Cmp('iframeWindowViewFax').getElementId().setAttribute("height",Ext.query(window).height());
		Ext.Cmp('iframeWindowViewFax').getElementId().setAttribute("width",Ext.query(window).width());
	});

});	
</script>
</head>
<body>
	<?php  __(form()->hidden('OutboxId',null, $file)); ?>
	<iframe id="iframeWindowViewFax"></iframe>	
</body>
</html>