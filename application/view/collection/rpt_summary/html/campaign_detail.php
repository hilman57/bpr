<?php

global $paymentChannels;
	$paymentChannels = array(0 => "Unknown", "ATM Bank Lain", "Teller Bank Lain", "HSBC", "POS",
								  "Pickup", "ATM Mandiri", "Teller Mandiri", "ATM BCA", "Teller BCA", "RTGS",
								  "Teller", "ATM Transfer", "ATM Pembayaran", "HSBC");


/* reconstruct date */
$sdates = explode("-", $start_date);
//echo "<br>".$sdates[0];
$start_date = $sdates[2]."-".$sdates[1]."-".$sdates[0];
$edates = explode("-", $end_date);
$end_date   = $edates[2]."-".$edates[1]."-".$edates[0];


if (mktime(0, 0, 0, $edates[1], $edates[0], $edates[2]) <
    mktime(0, 0, 0, $sdates[1], $sdates[0], $sdates[2])){
    	
	echo "End date before start date";
	exit;
}

// --------------- function save URL  ------------------

 if( !function_exists('save_url_excel') )
{
  function save_url_excel()
 {
	$out = new EUI_Object(_get_all_request() );
	$save_url_excel = url_controller() ."/BigExcel?
		start_date={$out->get_value('start_date')}
		&end_date={$out->get_value('end_date')}
		&report_type={$out->get_value('report_type')}
		&report_group={$out->get_value('report_group')}
		&user_tl={$out->get_value('user_tl')}
		&user_agent={$out->get_value('user_agent')}
		&report_mode={$out->get_value('report_mode')}";
	return $save_url_excel;
 }	
 
}


/** start # omens : untuk menghitung amount wo stelah di bayar ( amount_balance after pay ) ****/

function getPchDay(){
	$sql = "select * from t_lk_payment_channel"; //where pch_flags = 1";
	$qry = @mysql_query($sql);
		while($row = mysql_fetch_assoc($qry)){
			$pch[$row['pch_code']] = $row['pch_day'];
		}
	return $pch;
}

function isPay($custno=''){
		$sql = "select count(a.paymenthistoryid) as isPay 
				from coll_payment_history a where a.custno='$custno'";
			
		$qry = @mysql_query($sql);
		$row = mysql_fetch_assoc($qry);
		if( $row['isPay'] >0 ): return true;
			else:
				return false;
			endif;	
}



function calculAfterPay($custno='', $amount_wo=0)
{
	$amount_after_pay = 0;
	$sql = "select sum(a.amount) as afterpay from coll_payment_history a where a.custno='$custno'";
	$qry = @mysql_query($sql);
	$row = mysql_fetch_assoc($qry);
	
	if( isPay($custno) ):
		if( $amount_wo!=0) : $amount_after_pay = (($amount_wo)-($row['afterpay'])); endif;
	else: $amount_after_pay = $amount_wo;  endif;	
	return $amount_after_pay;
}

/** stop # omens : untuk menghitung amount wo stelah di bayar ( amount_balance after pay ) ****/


function toDuration($seconds){
	$sec = 0;
	$min = 0;
	$hour= 0;
	$sec = $seconds%60;
	$seconds = floor($seconds/60);
	if ($seconds){
		$min  = $seconds%60;
		$hour = floor($seconds/60);
	}
	
	if($seconds == 0 && $sec == 0)
		return sprintf("");
	else
		return sprintf("%02d:%02d:%02d", $hour, $min, $sec);
}  


/* function increment date,
   input yyyy-mm-dd
 */
function nextDate($date){
	$dates = explode("-", $date);
	$yyyy = $dates[0];
	$mm   = $dates[1];
	$dd   = $dates[2];
	
	$currdate = mktime(0, 0, 0, $mm, $dd, $yyyy);
	
	$dd++;
	/* ambil jumlah hari utk bulan ini */
	$nd = date("t", $currdate);
	if($dd>$nd){
		$mm++;
		$dd = 1;
		if($mm>12){
			$mm = 1;
			$yyyy++;
		}
	}
	
	if (strlen($dd)==1)$dd="0".$dd;
	if (strlen($mm)==1)$mm="0".$mm;
	
	return $yyyy."-".$mm."-".$dd;
}

function printNote(){
	//echo "note:<br>".
	//		 "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">".	     
	//    "</table>";
}


// -----------------------------------------------------------------------------------

 function showHeaders() 
 {
	echo "<div class=\"center\">".
		 "<p class=\"normal font-size22\">Campaign Detail</p>".
		 "<p class=\"normal font-size16\">Report Mode : Summary</p>".
		 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
	"</div>";
}
 

function showReport($vstart_date, $vend_date, $GroupCallCenter, $vAgentId,$vTlId){	
	global $paymentChannels;
	
		// print_r($vAgentId);
		echo "<table class=\"data\" border=1 style=\"border-collapse: collapse\" width=\"80%\" align=\"center\"><tr>"
		     .'<td class="head" width="20">No.</td>'
				.'<td class="head" width="100">debiturid</td>'
				."<td class=\"head\">cardno</td>"			
				."<td class=\"head\">custno</td>"			
				."<td class=\"head\">accountno</td>"			
				."<td class=\"head\">idno</td>"				
				."<td class=\"head\">nbrcards</td>"			
				."<td class=\"head\">fullname</td>"			
				."<td class=\"head\">mothername</td>"		
				."<td class=\"head\">dateofbirth</td>"		
				."<td class=\"head\">homeaddress</td>"		
				."<td class=\"head\">billingaddress</td>"	
				."<td class=\"head\">officeaddress</td>"		
				."<td class=\"head\">homezip</td>"			
				."<td class=\"head\">homephone</td>"			
				."<td class=\"head\">homephone2</td>"		
				."<td class=\"head\">mobilephone</td>"		
				."<td class=\"head\">mobilephone2</td>"		
				."<td class=\"head\">otheraddress</td>"		
				."<td class=\"head\">otheraddress2</td>"		
				."<td class=\"head\">officephone</td>"		
				."<td class=\"head\">officefax</td>"			
				."<td class=\"head\">ecname</td>"			
				."<td class=\"head\">ecaddress</td>"			
				."<td class=\"head\">ecphone</td>"			
				."<td class=\"head\">ecmobile</td>"			
				."<td class=\"head\">ecrelationship</td>"	
				."<td class=\"head\">branch</td>"			
				."<td class=\"head\">area</td>"				
				."<td class=\"head\">opendate</td>"			
				."<td class=\"head\">clss_entry</td>"		
				."<td class=\"head\">creditlimit</td>"		
				."<td class=\"head\">currentbalance</td>"	
				."<td class=\"head\">feeandcharge</td>"		
				."<td class=\"head\">interest</td>"			
				."<td class=\"head\">principalbalance</td>"	
				."<td class=\"head\">lastpaymentdate</td>"	
				."<td class=\"head\">lastpaymentamount</td>"	
				."<td class=\"head\">currentpaymentdue</td>"	
				."<td class=\"head\">bpcount</td>"			
				."<td class=\"head\">ptpcount</td>"			
				."<td class=\"head\">wo_date</td>"			
				."<td class=\"head\">wo_amount</td>"			
				."<td class=\"head\">wo_open</td>"			
				."<td class=\"head\">wo_lpd</td>"			
				."<td class=\"head\">curr_coll_id</td>"		
				."<td class=\"head\">dlq</td>"				
				."<td class=\"head\">perm_message</td>"		
				."<td class=\"head\">perm_date</td>"			
				."<td class=\"head\">accstatus</td>"			
				."<td class=\"head\">accstatusdate</td>"		
				."<td class=\"head\">prevaccstatus</td>"		
				."<td class=\"head\">prevaccstatusdate</td>"	
				."<td class=\"head\">lastresponse</td>"		
				."<td class=\"head\">contacthistoryid</td>"	
				."<td class=\"head\">agent</td>"				
				."<td class=\"head\">note</td>"				
				."<td class=\"head\">reminder</td>"			
				."<td class=\"head\">uploadbatch</td>"		
				."<td class=\"head\">ptp_discount</td>"		
				."<td class=\"head\">ptp_amount</td>"		
				."<td class=\"head\">ptp_date</td>"			
				."<td class=\"head\">ptp_channel</td>"		
				."<td class=\"head\">ssv_no</td>"			
				."<td class=\"head\">callstatus</td>"		
				."<td class=\"head\">calldate</td>"			
				."<td class=\"head\">prevcallstatus</td>"	
				."<td class=\"head\">infoptp</td>"			
				."<td class=\"head\">prevcalldate</td>"		
				."<td class=\"head\">tenor</td>"				
				."<td class=\"head\">addhomephone</td>"		
				."<td class=\"head\">addhomephone2</td>"		
				."<td class=\"head\">addmobilephone</td>"	
				."<td class=\"head\">addmobilephone2</td>"	
				."<td class=\"head\">addofficephone</td>"	
				."<td class=\"head\">addofficephone2</td>"	
				."<td class=\"head\">addfax</td>"			
				."<td class=\"head\">addfax2</td>"			
				."<td class=\"head\">swap_count</td>"		
				."<td class=\"head\">sms_count</td>"			
				."<td class=\"head\">other_agent</td>"		
				."<td class=\"head\">other_card_number</td>" 
				."<td class=\"head\">other_accstatus</td>"	
				."<td class=\"head\">contacttype</td>"		
				."<td class=\"head\">rpc</td>"				
				."<td class=\"head\">spc_with</td>"			
				."<td class=\"head\">namahomephone1</td>"	
				."<td class=\"head\">relativehomephone1</td>"
				."<td class=\"head\">namahomephone2</td>"	
				."<td class=\"head\">relativehomephone2</td>"
				."<td class=\"head\">namaofficephone1</td>"	
				."<td class=\"head\">relativeofficephone</td>"	
				."<td class=\"head\">namaofficephone2</td>"	
				."<td class=\"head\">relativeofficephone</td>"
				."<td class=\"head\">namatlpphone1</td>"		
				."<td class=\"head\">relativetlpphone1</td>"	
				."<td class=\"head\">namatlpphone2</td>"		
				."<td class=\"head\">relativetlpphone2</td>"	
				."<td class=\"head\">namamobilephone1</td>"	
				."<td class=\"head\">relativemobilephone</td>"
				."<td class=\"head\">namamobilephone2</td>"	
				."<td class=\"head\">relativemobilephone</td>"
				."<td class=\"head\">keep_data</td>"			
				."<td class=\"head\">afterpay</td>"			
				."<td class=\"head\">attempt_call</td>"		
				."<td class=\"head\">isblock</td>"			
				."<td class=\"head\">bal_afterpay</td>"		
				."<td class=\"head\">pri_afterpay</td>"		
				."<td class=\"head\">tenor_value</td>"		
				."<td class=\"head\">tenor_amnt</td>"		
				."<td class=\"head\">tenor_dates</td>"		
				."<td class=\"head\">flag</td>"				
				."<td class=\"head\">current_lock</td>"		
				."<td class=\"head\">lock_modul</td>"		
				."<td class=\"head\">cpa_count</td>"			
				."<td class=\"head\">lunas_flag</td>"		
				."<td class=\"head\">last_swap_ts</td>"		
				."<td class=\"head\">max_swap_ts</td>"		
				."<td class=\"head\">acc_mapping</td>"		
				."<td class=\"head\">class_mapping</td>"		
				."<td class=\"head\">accstatus_name</td>"	
				."<td class=\"head\">SPC_DESC</td>"			
				."<td class=\"head\">RPC_DESC</td>"			
				."<td class=\"head\">other_ch_office</td>"	
				."<td class=\"head\">other_ch_home</td>"		
				."<td class=\"head\">other_ch_mobile1</td>"	
				."<td class=\"head\">other_ch_mobile2</td>"	
				."<td class=\"head\">family</td>"			
				."<td class=\"head\">neighbour</td>"			
				."<td class=\"head\">rel_person</td>"		
				."<td class=\"head\">other_ec</td>"					     
				."</tr>";
		
		if($GroupCallCenter){
			$fromTbl = ", cc_agent d ";
			//periksa apakah ada agent yang dipilih
			if(count($vAgentId) and $vAgentId[0]){
			
				$agentlist = implode(",", $vAgentId);
				// echo $agentlist;
				$cond = "AND b.deb_agent = d.userid AND d.id IN (".$agentlist.")";
			}else{
				$cond = 'AND b.deb_agent = d.userid AND d.agent_group ='.$GroupCallCenter;
			}
		}
		
		$sub = "SELECT date_format(py.pay_created_ts, '%d-%m-%Y') as pay_created_ts 
				FROM t_gn_payment py  WHERE py.pay_id = (
				select MAX(pd.pay_id) from t_gn_payment pd 
				where pd.deb_id = a.deb_id
				and pd.pay_dc_id = e.UserId )";
						
		$sql = "SELECT 
		
				b.deb_agent , a.ptp_date, b.deb_name, 
				b.deb_cardno,
				a.deb_id, a.ptp_type,c.info_ptp_name, 
				b.deb_wo_amount, a.ptp_amount ,
				a.ptp_chanel, b.deb_bal_afterpay,($sub) as pay_created_ts
				FROM t_tx_ptp a 
				INNER JOIN t_gn_debitur b ON a.deb_id = b.deb_id
				LEFT JOIN t_lk_info_ptp c on a.ptp_type = c.info_ptp_code 
				INNER JOIN cc_agent d on b.deb_agent=d.userid
				INNER JOIN t_tx_agent e on d.userid=e.id
				
				WHERE  a.ptp_date >= '$vstart_date 00:00:00' 
		        AND a.ptp_date <= '$vend_date 23:59:59' 
				AND a.is_delete = 0
				$cond
		        ORDER BY b.deb_agent ,a.ptp_date";
		// echo $sql;		

		$res = @mysql_query($sql);
		echo "<pre>".$sql."</pre>";
		$cnt = 0;
		$dcr = "UNKNOWN";		
		$subtotal = 0;
		$grandtotal = 0;
		
		//echo $sql;
		while ($row = @mysql_fetch_array($res)) {
			//print_r($row);
			$pchDay = getPchDay();
			if(array_key_exists ( $row['ptp_chanel'] , $pchDay )){
				$interval = $pchDay[$row['ptp_chanel']];
				$Duedate = strtotime($row['ptp_date']);
				$EffectiveDate = date('Y-m-d', strtotime($interval.' day', $Duedate));
			}
			
			// if($dcr != $row['deb_agent']){
				
				// if($cnt>0){
					// echo '<tr>';
					// echo '<td colspan="8" align="right"><strong>Sub Total: </strong>&nbsp;</td>';
					// echo '<td class="head2" align="right"><strong>'.number_format($subtotal).'</strong></td>';
					// echo '</tr>';
				// }
				// $dcr = $row['deb_agent'];
				// echo '<tr>';
				// echo '<td colspan="11" class="head2"><strong>'.strtoupper($dcr).'</strong>&nbsp;</td>';
				// echo '</tr>';
				// $subtotal = 0;
			// }
			
			$cnt++;
			echo "<tr>";
			//$lunas = valueSQL("select reff_name from `coll_reference` where reff='iptp' and reff_code='".$row['infoptp']."'");
			//echo $lunas;
			echo "<td nowrap>$cnt</td>"
					 .'<td>'.$row['deb_id'].'</td>'
					 .'<td nowrap>'.$row['deb_cardno'].'</td>'
					 .'<td class="text">'.$row['deb_cardno'].'</td>'
					 .'<td>'.strtoupper($row['deb_agent']).'</td>'
					//.'<td>'.$lunas.'</td>'
					 .'<td>'.(($row['ptp_type']=='101')?'LUNAS':(($row['ptp_type']=='102')?'CICILAN':'')).'</td>'
					 //.'<td align="right">'.number_format($row['wo_amount']).'</td>'
					 //.'<td align="right">'.number_format(calculAfterPay($row['custno'],$row['wo_amount'])).'</td>'
					 .'<td align="right">'. number_format($row['deb_bal_afterpay'],0,",",".").'</td>'
					 .'<td nowrap>'.$paymentChannels[$row['ptp_chanel']].'</td>'
					 // .'<td>&nbsp;</td>'
					 .'<td>'.$EffectiveDate.'</td>'
					 .'<td align="right">'.number_format($row['ptp_amount'],0,",",".").'</td>';
			echo "</tr>";
			
			$subtotal += $row['ptp_amount'];
			$grandtotal += $row['ptp_amount'];
			
		}
		// if($cnt>0){
			// echo '<tr>';
			// echo '<td colspan="8" align="right"><strong>Sub Total: </strong>&nbsp;</td>';
			// echo '<td class="head2" align="right"><strong>'.number_format($subtotal).'</strong></td>';
			// echo '</tr>';
		// }
		echo '<tr>';
		echo '<td colspan="8" align="right"><strong>Grand Total: </strong>&nbsp;</td>';
		echo '<td colspan="2" class="head2" align="right"><strong>'.number_format($grandtotal,0,",",".").'</strong></td>';
		echo '</tr>';
		echo "</table><br>";

	//save to report data $start_date, $end_date, $group, $agents to session
	
	$_SESSION['xl_start_date'] 	= $start_date;
	$_SESSION['xl_end_date'] 		= $end_date;
	$_SESSION['xl_group'] 			= $group;
	$_SESSION['xl_agents'] 			= $agents;		
		
}
?>
<html>
	<head>
		<title>
			Enigma Collection Report - Promise to Pay
		</title>
		<style>
		.text{
		mso-number-format:"\@";/*force text*/
	}
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	vertical-align:middle;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}


.left { text-align:left;}
.right{ text-align:right;}
.center{ text-align:center;}
.font-size22 { font-size:22px; color:#000;}
.font-size20 { font-size:20px; color:#000;}
.font-size18 { font-size:18px; color:#000;}
.font-size16 { font-size:16px; color:#000;} 
.font-size14 { font-size:16px; color:#000;} 

p.normal  { line-height:6px;}

			-->
			</style>
	</head>
<body>
	<?php showHeaders();?>
	<?php showReport($start_date, $end_date, $GroupCallCenter, $AgentId,$TlId); ?>
	<?php printNote(); ?>	
	<center>
	<a href="<?php echo save_url_excel();?>">Save as excel</a>
	</center>
</body>
</html>