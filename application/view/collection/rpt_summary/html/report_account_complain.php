<?php
// --------------- function save URL  ------------------

 if( !function_exists('save_url_excel') )
{
  function save_url_excel()
 {
	$out = new EUI_Object(_get_all_request() );
	$save_url_excel = url_controller() ."/ShowExcel?
		start_date={$out->get_value('start_date')}
		&end_date={$out->get_value('end_date')}
		&report_type={$out->get_value('report_type')}
		&report_group={$out->get_value('report_group')}
		&user_tl={$out->get_value('user_tl')}
		&user_agent={$out->get_value('user_agent')}
		&report_mode={$out->get_value('report_mode')}";
	return $save_url_excel;
 }	
 
}

$header = array(
	'deb_open_date' => "Tanggal Laporan",
	'AccountNumber' => "No Kartu/PIL/CF",
	'CustomerName' => "Nama",
	'CampaignDesc' => "Produk",
	'kasus_komplain' => "Jenis Kasus",
	'kasus_komplain2' => "KET.COMPLAINT",
	'action_taken' => "Action Taken",
	'result' => "result",
	'closed_date' => "CLOSING DATE",
	'PhoneBlock' => "BLOCKED NO TELP",
	'source_complain' => "SOURCE",
	'deskol' => "AGENT",
	'date_pull_out' => "DATE PULL OUT",
);

$call_user_func = array(
	'deb_open_date'=>'_getDateIndonesia',
	'closed_date'=>'_getDateIndonesia'
);

?>
<html>
	<head>
		<title>
			Enigma Collection Report - Account Complain
		</title>
		<style>
		.text{
		mso-number-format:"\@";/*force text*/
	}
			<!--
			body, td, input, select, textarea {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
}
a img {
	border: 0;
}
a.hover {
	text-decoration: none;
}
a.hover:hover {
	text-decoration: underline;
}
form {
	margin: 0;
	padding: 0;
}
table.data {
	border-style: solid;
	border-width: 1;
	border-color: silver;
	background-color: #ECF1FB;
	background-image: url(bgtablebox.jpg);
	background-position: bottom;
	background-repeat: repeat-x;
}
table.data th {
	padding: 3;
}
table.data td {
	padding: 0 6 0 6;
}
table.data td, table.data th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	vertical-align: top;
}
table.data th {
	background-color: 3565AF;
	color: white;
	font-weight: normal;
	vertical-align: top;
	text-align: left;
}
table.data th a, table.data th a:visited {
	font-weight: normal;
	color: #CCFFFF;
}
table.data td.head {
	background-color: AABBFF;
	vertical-align:middle;
}
input, textarea {
}
input.button, button.button, span.button, div.button {
	border-style: solid;
	border-width: 1;
	border-color: 6666AA;
	background-image: url(bgbutt.jpg);
	background-repeat: repeat-x;
	background-position: center;
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	font-weight: normal;
}
span.button a, div.button a {
	color: #0F31BB;
}
table.subdata th {
	font: normal 12 "trebuchet ms",tahoma,verdana,sans-serif;
	color: #637dde;
	padding: 0 5 0 0;
	text-align: left;
}


.left { text-align:left;}
.right{ text-align:right;}
.center{ text-align:center;}
.font-size22 { font-size:22px; color:#000;}
.font-size20 { font-size:20px; color:#000;}
.font-size18 { font-size:18px; color:#000;}
.font-size16 { font-size:16px; color:#000;} 
.font-size14 { font-size:16px; color:#000;} 

p.normal  { line-height:6px;}

			-->
			</style>
	</head>
<body>
	<?php 
		echo "<div class=\"center\">".
				 "<p class=\"normal font-size22\">".$report_title."</p>".
				 "<p class=\"normal font-size16\">Report Mode : Detail</p>".
				 "<p class=\"normal font-size14\">Closed Periode : ". _get_post("start_date") ." to ". _get_post("end_date") ."</p>".
				 "<p class=\"normal font-size12\">Print date : ". date('d-m-Y H:i') ."</p>".
			"</div>";
		echo "<table class=\"data\" border=1 style=\"border-collapse: collapse\" width=\"80%\" align=\"center\">
			<tr>
				<td class=\"head\" width=\"20\" nowrap>No.</td>";
		foreach($header as $index => $name )
		{
			echo "<td class=\"head\" nowrap>".$name."</td>";
		}
		echo "</tr>";
		$no=1;
		foreach($COMPLAIN as $rows => $cols )
		{	
			
			echo "<tr>
					<td nowrap>".$no.".</td>";
			foreach($header as $index => $name )
			{
				
				if(!is_null($cols[$index]))
				{
					echo "<td nowrap>".page_call_function($index, $cols, $call_user_func)."</td>";
				}
				else
				{
					echo "<td nowrap>-</td>";
				}
				
			}
			echo "</tr>";
			$no++;
		}
		echo"</table>";
		// foreach($COMPLAIN as $rows => $cols )
		// {
			
		// }
		// echo "<pre>";
		// print_r($COMPLAIN);
		// echo "</pre>";
		
	?>
	<center>
	<a href="<?php echo save_url_excel();?>">Save as excel</a>
	</center>
</body>
</html>