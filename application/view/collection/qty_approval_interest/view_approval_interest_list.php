<?php ?>	
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default first center" >&nbsp;<a href="javascript:void(0);" onclick="Ext.Cmp('chk_cust_call').setChecked();">#</a></th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;No</th>			
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('e.CampaignName');">Campaign Name</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('e.CustomerNumber');">CIF</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('c.CustomerFirstName');">Customer Name</span></th>     
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('h.AproveName');">Approve Status</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('e.full_name');">Agent Name</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('b.PolicySalesDate');">Sales Date</span></th>
		<th nowrap class="font-standars ui-corner-top ui-state-default first left">&nbsp;<span class="header_order" onclick="Ext.EQuery.orderBy('g.PolicySalesDate');">Days</span></th>
	</tr>
</thead>	
<tbody>
<?php
$no = $num;
foreach( $page -> result_assoc() as $rows )
{ 
  $color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
  $daysize = _getDateDiff(date('Y-m-d'),date('Y-m-d',strtotime($rows['PolicySalesDate']))); 
  
?>
			<tr class="onselect" bgcolor="<?php echo $color;?>">
				<td class="content-first"> <input type="checkbox" value="<?php echo $rows['CustomerId']; ?>" id="chk_cust_call" name="chk_cust_call" onclick="Ext.DOM.showPolicy(this);"></td>
				<td class="content-middle"><?php  echo $no; ?></td>
				<td class="content-middle"><?php  echo $rows['CampaignName']; ?></td>
				<td class="content-middle"><?php  echo $rows['CustomerNumber']; ?></td>
				<td class="content-middle" nowrap><?php echo $rows['CustomerFirstName']; ?></td>
				<td class="content-middle" nowrap><span style="color:#b14a06;font-weight:bold;"><?php echo ( is_null($rows['AproveName']) ? 'PENDING' : strtoupper($rows['AproveName'])) ; ?></span></td>
				<td class="content-middle" nowrap><?php echo $rows['full_name']; ?></td>
				<td class="content-middle" nowrap><?php __(date('d-m-Y H:i:s',strtotime($rows['PolicySalesDate']))); ?></td>
				<td class="content-lasted center" style="color:red;">&plusmn;&nbsp;<?php echo $daysize['days_total'];?></td>
			</tr>
</tbody>
<?php
//echo $row -> CustomerId;
$no++;
};
?>
</table>


