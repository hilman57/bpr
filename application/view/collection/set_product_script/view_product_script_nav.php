<?php echo javascript(); ?>
<script type="text/javascript">
/*
 ** javscript prototype system
 ** version v.0.1
 */
 
Ext.DOM.onload = (function(){
	Ext.Cmp('ui-widget-title').setText( Ext.System.view_file_name());
 })();
 
/*
 ** javscript prototype system
 ** version v.0.1
 */
		
var datas = { 
	keywords : '<?php echo _get_post('keywords'); ?>',
	order_by : '<?php echo _get_post('order_by'); ?>',
	type 	 : '<?php echo _get_post('type'); ?>',
}
 

Ext.EQuery.TotalPage   = <?php echo (INT)$page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo (INT)$page -> _get_total_record(); ?>;
	
//---------------------------------------------------------------------------------------
/*
 * @ package 		Search function 
 */
 
 
Ext.DOM.navigation = {
	custnav	 : Ext.DOM.INDEX +'/SetProductScript/index/',
	custlist : Ext.DOM.INDEX +'/SetProductScript/Content/',
}
		
//---------------------------------------------------------------------------------------
/*
 * @ package 		Search function 
 */
$(function(){
	$('#toolbars').extToolbars({
				extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
				extTitle :[['Enable'],['Disable'] ,['Add'],['Delete'],['Cancel'],['Clear'],['Search']],
				extMenu  :[['Enable'],['Disable'],['Add'],['Delete'],['cancelResult'],['resetSeacrh'],['Search']],
				extIcon  :[['accept.png'],['cancel.png'], ['add.png'],['delete.png'],['cancel.png'],['zoom_out.png'], ['zoom.png']],
				extText  :true,
				extInput :true,
				extOption: [{
						render	: 6,
						type	: 'text',
						id		: 'v_product_prefix', 	
						name	: 'v_product_prefix',
						value	: datas.keywords,
						width	: 200
					}]
			});
	});
	
 

	Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();


// Search

Ext.DOM.Search = function(){
	// console.log(Ext.Cmp('v_product_prefix').getValue())
	// return false
  Ext.EQuery.construct(navigation,{
	keywords : Ext.Cmp('v_product_prefix').getValue()
  });
  Ext.EQuery.postContent();
}
// cancelResult
		
var cancelResult=function(){
	Ext.Cmp('span_top_nav').setText("");
}
// Ext.DOM.resetSeacrh = function(){
// 	console.log('reset',Ext.Serialize('v_product_prefix').Clear());
// 	return false
// 	Ext.Serialize('v_product_prefix').Clear();
// 	Ext.DOM.Search()
// }
Ext.DOM.resetSeacrh = function()
{
	document.getElementById('v_product_prefix').value = ''
	new  Ext.DOM.Search();
}

//---------------------------------------------------------------------------------------
/*
 * @ package 		Search function 
 */
 
 
 

//---------------------------------------------------------------------------------------
/*
 * @ package 		Search function 
 */
 

 Ext.DOM.AddScript = function()
{
	Ext.ShowMenu(new Array("SetProductScript","AddScript"), 
		Ext.System.view_file_name(),{
			time : Ext.Date().getDuration()
	});
}

// UploadScript

Ext.DOM.UploadScript = function()
{
	Ext.Ajax
	({
		url    	 : Ext.DOM.INDEX+'/SetProductScript/Upload/',
		method 	 : 'POST',
		file 	 : 'ScriptFileName',
		param    : {
			CampaignId : Ext.Cmp('CampaignId').getValue(),
			ScriptTitle : Ext.Cmp('Description').getValue(),
			Active : Ext.Cmp('ScriptFlagStatus').getValue()
		},
		complete : function(fn){
			try {
				var ERR = JSON.parse(fn.target.responseText);
				if( ERR.success ){
					Ext.Msg("Upload Script").Success();
					Ext.EQuery.construct(navigation,datas)
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Upload Script").Failed();
				}
			}
			catch(e){
				Ext.Msg(e).Error();
			}
		}
	}).upload();
}

//---------------------------------------------------------------------------------------
/*
 * @ package 		Search function 
 */
 

Ext.DOM.ShowWindowScript = function()
{
 var ScriptId = Ext.Cmp('ScriptId').getValue();
 if( ScriptId.length == 0 || ScriptId.length >1  ){
	Ext.Msg("Please select a row ").Info();
	return false;
 }
	//console.log(ScriptId);
// ------------------- get data script --------------------

	var WindowScript = new Ext.Window 
	({
			url     : Ext.EventUrl(['SetProductScript','ShowProductScript']).Apply(), 
			name    : 'WinProduct',
			height  : ($(window).innerHeight()),
			width   : ($(window).innerWidth() - ( $(window).innerWidth()/2 )),
			left    : ($(window).innerWidth()/2),
			top	    : ($(window).innerHeight()/2),
			param   : {
				ScriptId : Ext.BASE64.encode(ScriptId.join('')),
				Time	 : Ext.Date().getDuration()
			}
		}).popup();
		
	if( ScriptId =='' ) {
		window.close();
	}
}


//Disable
		
Ext.DOM.Disable = function()
{
	// var ScriptId = Ext.Cmp('ScriptId').getValue();
	var ScriptId = Ext.Cmp('chk_menu').getValue();
	
	if(Ext.Cmp('chk_menu').empty()!=true)
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+ '/SetProductScript/SetActive/',
			method 	:'POST',
			param 	: {
				Active : 0,
				ScriptId : ScriptId
			},
			ERROR : function(e){
				var ERR = JSON.parse(e.target.responseText);
				
				if( ERR.success ) {
					Ext.Msg("Disable Script").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Disable Script").Failed();
					return false;
				}
			}
		}).post();
	}
	else{
		Ext.Msg("Please select Rows").Error();
	}
}

//Enable
		
Ext.DOM.Enable = function()
{
	// var ScriptId = Ext.Cmp('ScriptId').getValue();
	var ScriptId = Ext.Cmp('chk_menu').getValue();
	if(Ext.Cmp('chk_menu').empty()!=true)
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+ '/SetProductScript/SetActive/',
			method 	:'POST',
			param 	: {
				Active : 1,
				ScriptId : ScriptId
			},
			ERROR : function(e){
				var ERR = JSON.parse(e.target.responseText);
				
				if( ERR.success ) {
					Ext.Msg("Enable Script").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Enable Script").Failed();
					return false;
				}
			}
		}).post();
	}
	else{
		Ext.Msg("Please select Rows").Error();
	}
}




/*
 ** javscript prototype system
 ** version v.0.1
 **/
 		

	

	Ext.DOM.Add = function( menu )
	{
		Ext.Ajax({
			url : Ext.DOM.INDEX+'/SetProductScript/AddScript',
			method : 'POST',
			param  : {
				action:'menu_add_tpl'
			}
		}).load("top_header");
	}
	// cancelResult
		
var cancelResult=function(){
	Ext.Cmp('top_header').setText("");
}

	
	Ext.DOM.SaveSet = function()
	{
		if( Ext.Cmp('template').empty() ) {
			alert("Please choose template !");
			Ext.Cmp('template').setFocus();
		} else if( Ext.Cmp('data_status').empty() ) {
			alert("Please choose data status !");
			Ext.Cmp('data_status').setFocus();
		} else if( Ext.Cmp('SentDate').empty() ) {
			alert("Please choose days !");
			Ext.Cmp('SentDate').setFocus();
		} else {
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX+'/SetSmsBlast/SaveSetSMS',
				method 	: 'POST',
				param  	: Ext.Serialize('frmAddSetSMS').getElement(),
				ERROR : function(e){
					Ext.Util(e).proc(function(save){
						if(save.success){
							Ext.Msg("Save Set SMS!").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Save Set SMS !").Failed();
							return false;
						}
					});
				}
			}).post();
		}
	}
	
	Ext.DOM.UpdateSet = function()
	{
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SetSmsBlast/UpdateSetSMS',
			method 	: 'POST',
			param  	: Ext.Serialize('frmEditSetSMS').getElement(),
			ERROR : function(e){
					Ext.Util(e).proc(function(save){
						if(save.success){
							Ext.Msg("Update Set SMS!").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Update Set SMS !").Failed();
							return false;
						}
					});
				}
		}).post();
	}
	
	Ext.DOM.Delete = function()
	{
		var ScriptId = Ext.Cmp('chk_menu').getValue();
		// console.log(ScriptId);
		// return false
		
		Ext.Ajax({
			url 	: Ext.DOM.INDEX+'/SetProductScript/Delete',
			method 	: 'POST',
			param  	: {
				ScriptId : ScriptId
			},
			ERROR : function(e){
					Ext.Util(e).proc(function(save){
						if(save.success){
							Ext.Msg("Delete Script!").Success();
							Ext.EQuery.postContent();
						} else {
							Ext.Msg("Delete Script !").Failed();
							return false;
						}
					});
				}
		}).post();
	}

	
</script>
	
<fieldset class="corner">
	<legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span> Manage Scripts</legend>
	<div id="toolbars" class="toolbars"></div>
	<div id="top_header" style="margin-top:8px;"></div>	
	<div class="content_table"></div>
	<div id="pager"></div>
</fieldset>
		
	<!-- stop : content -->
	
	
	
