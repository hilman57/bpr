<?php ?>

<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left" width="5%">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="center">&nbsp;No</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left" align="center">&nbsp;Kode Proyek Kerja</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left" align="center">&nbsp;Nama Proyek Kerja</th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="center">&nbsp;Pekerjaan Buat Ts </th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle left" align="center">&nbsp;Buat Pengguna Kerja </th>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" align="left">&nbsp;Status Proyek Kerja</th>
	</tr>
</thead>	
<tbody>
<?php
 $no  = $num;
 foreach( $page -> result_assoc() as $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
	<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
		<td class="content-first"><input type="checkbox" value="<?php echo $rows['ProjectId']; ?>" name="ProjectId" id="ProjectId"></td>
		<td class="content-middle center"><?php echo $no ?></td>
		<td class="content-middle"><b><?php echo $rows['ProjectCode']; ?></b></td>
		<td class="content-middle"><?php echo $rows['ProjectName']; ?></td>
		<td class="content-middle center"><?php echo $rows['ProjectCreateTs']; ?></td>
		<td class="content-middle"><?php echo $rows['full_name']; ?></td>
		<td class="content-lasted center"><?php echo ($rows['ProjectFlags']?'Active':'Not Active'); ?></td>
	</tr>	
</tbody>
<?php
	$no++;
};
?>
</table>



