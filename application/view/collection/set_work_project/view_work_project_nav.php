<?php echo javascript(); ?>
<script type="text/javascript">

/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();


Ext.DOM.datas={}
 
/**
 ** javscript prototype system
 ** version v.0.1
 **/
 
$(function(){
	$('#toolbars').extToolbars({
				extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
				extTitle :[['Add'],['Edit'],['Delete']],
				extMenu  :[['WorkProjectAdd'],['WorkProjectEdit'],['WorkProjectDelete']],
				extIcon  :[['add.png'],['calendar_edit.png'],['delete.png']],
				extText  :true,
				extInput :false,
				extOption: []
			});
			
});

Ext.EQuery.TotalPage   = <?php echo (INT)$page -> _get_total_page(); ?>;
Ext.EQuery.TotalRecord = <?php echo (INT)$page -> _get_total_record(); ?>;

/* assign navigation filter **/

Ext.DOM.navigation = {
	custnav : Ext.DOM.INDEX +'/SetWorkProject/index/',
	custlist : Ext.DOM.INDEX +'/SetWorkProject/Content/'
}

/* assign show list content **/
//console.log(navigation);
		
Ext.EQuery.construct(Ext.DOM.navigation,Ext.DOM.datas);
Ext.EQuery.postContentList();

/* FTP_delete **/

Ext.DOM.WorkProjectDelete= function(){
	var ProjectId = Ext.Cmp('ProjectId').getValue();
if( Ext.Msg('Do You want to delete this rows ?').Confirm() )
{
	 Ext.Ajax ({
		url 	: Ext.DOM.INDEX +"/SetWorkProject/WorkProjectDelete/",
		method 	: 'POST',
		param 	: { ProjectId : ProjectId },
		ERROR 	: function(e)
		{
			Ext.Util(e).proc(function(response){
				if( response.success ){
					Ext.Msg("Delete Work Project").Success();
					Ext.EQuery.postContent();
				}
				else{
					Ext.Msg("Delete Work Project").Failed();
				}	
			})
		}
	}).post();
 }	
}


/** add ftp configutayion **/

Ext.DOM.WorkProjectEdit = function() {
var  ProjectId = Ext.Cmp('ProjectId').getValue();

	Ext.Ajax({
		url : Ext.DOM.INDEX +"/SetWorkProject/WorkProjectEdit/",
		mthod :'GET',
		param :{
			time : Ext.Date().getDuration(),
			ProjectId : ProjectId
		}
	}).load("span_top_nav");
}


/** add ftp configutayion **/

Ext.DOM.WorkProjectAdd = function() {
	Ext.Ajax({
		url : Ext.DOM.INDEX +"/SetWorkProject/WorkProjectAdd/",
		mthod :'GET',
		param :{
			time : Ext.Date().getDuration()
		}
	}).load("span_top_nav");
}

// save new data read ftp 

Ext.DOM.WorkSaveButton = function() {
	var conds = Ext.Serialize('WorkFormTemplate').Complete();
	if( conds ) 
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +"/SetWorkProject/WorkProjectSave/",
			method 	: 'POST',
			param 	: Ext.Join([
						Ext.Serialize('WorkFormTemplate').getElement()
					]).object(),
					
			ERROR 	: function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg("Save Work Project ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Save Work Project ").Failed();
					}	
				})
			}
		}).post();
	}
 }



/* update ftp data read **/

Ext.DOM.WorkEditButton = function() {
	var conds = Ext.Serialize('WorkEditTemplate').Complete();
	if( conds ) 
	{
		Ext.Ajax
		({
			url 	: Ext.DOM.INDEX +"/SetWorkProject/WorkProjectUpdate/",
			method 	: 'POST',
			param 	: Ext.Join([
						Ext.Serialize('WorkEditTemplate').getElement()
					]).object(),
					
			ERROR 	: function(e){
				Ext.Util(e).proc(function(response){
					if( response.success ){
						Ext.Msg("Update Work Project ").Success();
						Ext.EQuery.postContent();
					}
					else{
						Ext.Msg("Update Work Project ").Failed();
					}	
				})
			}
		}).post();
	}
 }
 
</script>
<!-- start : content -->
<fieldset class="corner">
 <legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
 <div id="toolbars" style='margin:0px 15px 0px 10px;'></div>
 <div id="span_top_nav"></div>
 <div class="content_table"></div>
 <div id="pager"></div>
</fieldset>	
	
<!-- stop : content -->
	
	
	