<fieldset  class='corner' style='margin:8px 15px 0px 10px;'>
	<legend class='icon-menulist'>&nbsp;&nbsp;Edit Work Project </legend>
	<div>
		<form name="WorkEditTemplate">
		<table>
			<tr>
				<td class='text_caption bottom'>* Work Project Code </td>
				<td>
					<?php __(form()->hidden('ProjectId',null,$WorkProject['ProjectId'])); ?>
					<?php __(form()->input('ProjectCode','select long', $WorkProject['ProjectCode'])); ?>
				</td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>* Work Project Name </td>
				<td><?php __(form()->input('ProjectName','select long', $WorkProject['ProjectName'])); ?></td>
			</tr>
			
			
			<tr>
				<td class='text_caption bottom'>* Work Project Status</td>
				<td><?php __(form()->combo('ProjectFlags','select long', array('0' => 'Not Active','1' => 'Active'), $WorkProject['ProjectFlags'] )); ?></td>
			</tr>
			
			<tr>
				<td class='text_caption bottom'>&nbsp;</td>
				<td>
					<?php __(form()->button('FtpUpdateButton','save button', 'Update', array("click" => "Ext.DOM.WorkEditButton();") )); ?>
					<?php __(form()->button('FtpCloseButton','close button', 'Cancel', array("click" =>"Ext.Cmp('span_top_nav').setText('');"))); ?>
				
					</td>
			</tr>
		</table>
		</form>
		
	</div>
</fieldset>