<?php
 $UI = & get_instance();
 $array_call_user_function = $UI->M_ReportDailyReport->report_type();
 $this->load->helper('EUI_ExcelWorksheet'); // load library excel 

/** create FILE EXCEL **/

 $BASE_EXCEL_PATH_TEMP = APPPATH .'temp';
 $BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_TEMP ."/". date('YmdHi')."_". strtoupper($this->EUI_Session->_get_session('KodeUser')) ."_".strtoupper( $UI->M_ReportDailyReport->report_group() ).".xls";
 
 /*** set create of header  ****/
 
 $workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
 $worksheet =& $workbook->addworksheet();
 
 /** set title of headers ***/
 
 $title_header =& $workbook->addformat();
 $title_fontsize =& $workbook->addformat();
 
 $title_header->set_bold();
 $title_header->set_size(14);
 $title_header->set_color('black');
 $title_header->set_align('left');
 $title_header->set_align('vcenter');

 $title_fontsize->set_bold();
 
 $start_rows = 1;
 
/** set info report by request **/

 $worksheet->write($start_rows, 0,"Daily / {$vheader['title']} / Interval : ". date('d M Y', strtotime( $UI->M_ReportDailyReport->start_date() ))." to ". date('d M Y', strtotime( $UI->M_ReportDailyReport->end_date() )), $title_header);
 
/** set tart of rows ***/

$header    =& $workbook->addformat();
$fontsize  =& $workbook->addformat();

/** set header layout **/
		
 $header->set_bold();
 $header->set_size(11);
 $header->set_color('white');
 $header->set_align('left');
 $header->set_align('vcenter');
 $header->set_pattern();
 $header->set_fg_color('black');
 $fontsize->set_bold();
 
$start_rows = $start_rows+3;
if(is_array( $array_call_user_function ))
	foreach( $array_call_user_function as $keys => $stack )
{	

/** get all attribute methode ***/

	$array_label_header = $call_user[$stack]['header'];
	$array_field_content  = $call_user[$stack]['field'];
	$array_source_content  = $call_user[$stack]['content'];
	$array_title_content  = $call_user[$stack]['title'];
	
/** create sub title 	**/
	$worksheet->write($start_rows, 0, $array_title_content, $header);
	$start_rows = $start_rows+1;
	
/** set content sub **/
	
	 $subheader  =& $workbook->addformat();
	 $subheader->set_size(11);
	 $subheader->set_color('white');
	 $subheader->set_align('center');
	 $subheader->set_align('vcenter');
	 $subheader->set_pattern();
	 $subheader->set_fg_color('red');
	
	$start_rows = $start_rows+1;
	
	if(is_array($array_label_header)) 
	foreach( $array_label_header as $n => $labels ){
		$worksheet->write($start_rows, $n, ucfirst(strtolower($labels)), $subheader);
	}
	
/**	create content **/

  $start_rows = $start_rows+1;
  if(is_array($array_source_content) ) 
  foreach( $array_source_content as $n => $rows )
  {
	 foreach( $array_field_content as $k => $field ) {
		$worksheet->write_string($start_rows, $k, $rows[$field] );	
	 }
	 
	 $start_rows +=1;
  }
  
  $start_rows = $start_rows+5;

}

$workbook->close();

// start download file 

if( file_exists($BASE_EXCEL_FILE_NAME))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Type: audio/x-gsm");
	header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
	readfile($BASE_EXCEL_FILE_NAME); 
	@unlink($BASE_EXCEL_FILE_NAME);
}	
				
		
?>