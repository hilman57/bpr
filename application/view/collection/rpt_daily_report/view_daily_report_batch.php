<?php

// group code send of mail 

$FU_GROUP_CODE['bancass']= array ( 
	'NEDED_AGENT' 	 => array('code'=>'800','name'=> 'Report Detail Need Agent'), 
	'VISIT_BRANCH' 	 => array('code'=>'999','name'=> 'Report Detail Visit to Branch'), '999', 
	'CHANGE_ADDRESS' => array('code'=>'700','name'=> 'Report Change Address/Ph number/Email'), '700', 
	'SURVEY_CLEAN' 	 => array('code'=>'7001','name'=> 'Report Detail Surveyed Clean'), '7001', 
	'UNCONTACTED' 	 => array('code'=>'7002','name'=> 'Report Uncontacted'), '7002' 
);


$FU_GROUP_CODE['agency']= array ( 
	'NEDED_AGENT' 	 => array('code'=>'800','name'=> 'Report Detail Need Agent'), 
	'VISIT_BRANCH' 	 => array('code'=>'999','name'=> 'Report Detail Visit to Branch'), '999', 
	'CHANGE_ADDRESS' => array('code'=>'700','name'=> 'Report Change Address/Ph number/Email'), '700', 
	'SURVEY_CLEAN' 	 => array('code'=>'7001','name'=> 'Report Detail Surveyed Clean'), '7001', 
	'UNCONTACTED' 	 => array('code'=>'7002','name'=> 'Report Uncontacted'), '7002' 
);


// pre define of path 

define('PATH_BATCH_EXPORT', str_replace('system','application', BASEPATH) );

// instance of parent UI 

$UI = & get_instance();

// call of function report type 
$array_sender_mailers = $UI->M_ReportDailyReport->FollowupMailAddressByGroup();
$start_interval_date = $UI->M_ReportDailyReport->start_date();
$end_interval_date  = $UI->M_ReportDailyReport->end_date();
$array_call_user_fugroup  = $UI->M_ReportDailyReport->report_group();
$array_call_user_function = $UI->M_ReportDailyReport->report_type();
$array_call_user_realname = STRTOUPPER(reset($array_call_user_function));
$array_call_user_contents = $call_user[STRTOLOWER($array_call_user_realname)]['content'];

// if rows is empty exit here 

if(!count($array_call_user_contents))
{
	exit("rows is empty.\n\r");
}

// load helper attribute 

$this->load->helper('EUI_ExcelWorksheet'); // load library excel 
$this->load->helper('EUI_SMLGenerator');

/** create FILE EXCEL **/

$BASE_EXCEL_PATH_DIR = PATH_BATCH_EXPORT .'batch/wcall/'.DATE('Ymd');
 
 if( !is_dir($BASE_EXCEL_PATH_DIR) ){
	mkdir($BASE_EXCEL_PATH_DIR, 0777, true );
 }
 
 if(!is_dir($BASE_EXCEL_PATH_DIR) )
	exit("Failed create directory on $BASE_EXCEL_PATH_DIR");
	
// if ok the next process 

 $BASE_INTERVAL_DATE = DATE('Ymd', strtotime($start_interval_date) )."_".date('Ymd', strtotime($end_interval_date) ); 
 $BASE_EXCEL_FILE_NAME = $BASE_EXCEL_PATH_DIR ."/". $BASE_INTERVAL_DATE ."_". STRTOUPPER($array_call_user_fugroup)."_". $array_call_user_realname.".xls";

 /*** set create of header  ****/
 
 $workbook  =& new writeexcel_workbook($BASE_EXCEL_FILE_NAME);
 $worksheet =& $workbook->addworksheet();
 
 /** set title of headers ***/
 
 $title_header =& $workbook->addformat();
 $title_fontsize =& $workbook->addformat();
 
 $title_header->set_bold();
 $title_header->set_size(14);
 $title_header->set_color('black');
 $title_header->set_align('left');
 $title_header->set_align('vcenter');

 $title_fontsize->set_bold();
 
 $start_rows = 0;
 
/** set info report by request **/

 $report_type_header = $FU_GROUP_CODE[$array_call_user_fugroup][$array_call_user_realname]['name'];
 $report_type_groups = $FU_GROUP_CODE[$array_call_user_fugroup][$array_call_user_realname]['code'];
 $worksheet->write($start_rows, 0,$report_type_header, $title_header);
 
/** set tart of rows ***/

$header    =& $workbook->addformat();
$fontsize  =& $workbook->addformat();

/** set header layout **/
		
 $header->set_size(11);
 $header->set_color('white');
 $header->set_align('left');
 $header->set_align('vcenter');
 $header->set_pattern();
 $header->set_border(1);
 $header->set_border_color('blue');
 $header->set_fg_color('black');
 $fontsize->set_bold();
 
$start_rows = $start_rows+1;

//print_r($call_user);


if(is_array( $array_call_user_function ))
	foreach( $array_call_user_function as $keys => $stack )
{	

 if( in_array(strtolower($stack), array_keys($call_user) ) )
{
/** get all attribute methode ***/

	$array_label_header = $call_user[$stack]['header'];
	$array_field_content  = $call_user[$stack]['field'];
	$array_source_content  = $call_user[$stack]['content'];
	
/** set content sub **/
	
	 $subheader  =& $workbook->addformat();
	 $sfontsize  =& $workbook->addformat();
	 
	 $subheader->set_bold();
	 $subheader->set_size(11);
	 $subheader->set_color('white');
	 $subheader->set_align('center');
	 $subheader->set_align('vcenter');
	 $subheader->set_border(1);
	 $subheader->set_border_color('blue');
	 $subheader->set_pattern();
	 $subheader->set_fg_color('red');
	 $sfontsize->set_bold();
	
	$start_rows = $start_rows+1;
	
	if(is_array($array_label_header)) 
	foreach( $array_label_header as $n => $labels ){
		$worksheet->write($start_rows, $n, ucfirst(strtolower($labels)), $subheader);
	}
	
/**	create content **/

   $xlscontent  =& $workbook->addformat();
   $xlscontent->set_size(11);
   $xlscontent->set_align('vcenter');
   $xlscontent->set_border(1);
   $xlscontent->set_border_color('blue');
    
  $start_rows = $start_rows+1;
  if(is_array($array_source_content) ) 
  foreach( $array_source_content as $n => $rows )
  {
	 foreach( $array_field_content as $k => $field ) {
		$worksheet->write_string($start_rows, $k, $rows[$field], $xlscontent);	
	 }
	 
	 $start_rows +=1;
  }
  $start_rows = $start_rows+5;
}

}

$workbook->close();

/** set send to email if exist rows and data file **/

if( (count($array_call_user_contents)> 0) 
	AND (file_exists($BASE_EXCEL_FILE_NAME)) )
{
	$constants = $array_sender_mailers[$report_type_groups];
	
	if( is_array($constants) )
	{	
		SML_Generator()->set_send_date(date('Y-m-d H:i:s'));
		SML_Generator()->set_add_assign(1);
		SML_Generator()->set_add_title("Daily {$report_type_header}");
		SML_Generator()->set_add_body("WCALL - Daily {$report_type_header} <br> Interval : {$start_interval_date} to {$end_interval_date}");
		/* to **/
		if( isset($constants['to']))
		foreach($constants['to'] as $key => $to_add_address ){ 
			SML_Generator()->set_add_to($to_add_address, $to_add_address);	
		}
		/* cc **/ 
		if( isset($constants['cc']))
		foreach($constants['cc'] as $key => $cc_add_address ){
			SML_Generator()->set_add_cc($cc_add_address, $cc_add_address);	
		}  
		/* bcc **/	
		if( isset($constants['bcc']))
		foreach($constants['bcc'] as $key => $bcc_add_address ) {
			SML_Generator()->set_add_bcc($bcc_add_address, $bcc_add_address);	
		 }
		 
		SML_Generator()->set_add_attachment($BASE_EXCEL_FILE_NAME);
		SML_Generator()->set_compile();
	}	
}


// stop in here 
exit("done.\n\r");

?>