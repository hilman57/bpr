<?php echo javascript(); ?>
<script type="text/javascript">

// @ pack : upload data listener 

var datas= { 
	order_by  : '<?php echo $this -> URI -> _get_post('order_by');?>',
	type : '<?php echo $this -> URI -> _get_post('type');?>'
}

// @ pack : upload data listener 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Set Agent to Lock'],['Lock Agent by Check'],['Unlock Agent by Check']],
		extMenu  :[['SetAgentLock'],['LockbyCheck'],['UnlockbyCheck']],
		extIcon  :[['cog_add.png'],['lock.png'],['lock_open.png']],
		extText  :true,
		extInput :false
	});	
});

// @ pack : upload data listener 

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : upload data listener 

var navigation = {
	custnav	 : Ext.DOM.INDEX+'/MgtRandDeb/lock_agent_nav/',
	custlist : Ext.DOM.INDEX+'/MgtRandDeb/lock_agent_list/'
}
	
// @ pack : upload data listener 

Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

 Ext.DOM.ClosePanel = function() {
	Ext.Cmp('span_top_nav').setText("");
}

Ext.DOM.SetAgentLock = function()
{
	ClosePanel();
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/MgtRandDeb/ManageAgentLock/',
		param : { 
			time : Ext.Date().getDuration()
		}
	}).load("span_top_nav");
}

Ext.DOM.LockDebiturRandom = function()
{
	var agent_lock_random = Ext.Cmp('agent_lock_random').getValue();
	
	// alert(agent_lock_random);
	if(agent_lock_random !='')
	{
		Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/MgtRandDeb/SetAgentLock',
				method 	: 'POST',
				param 	: { agent_list : agent_lock_random
						},
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.status ) {
						alert(ERROR.message);
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
					else
					{
						alert(ERROR.message);
						ClosePanel();
					}
				}	
		}).post();
	}
	else
	{
		alert('Please Select Agent');
	}
}

Ext.DOM.LockbyCheck = function()
{
	var lock_agent_id = Ext.Cmp('lock_agent_id').getValue();
	if(lock_agent_id !='')
	{
		if(confirm('Are You Sure this Agent again ?'))
		{
			Ext.Ajax({
					url 	: Ext.DOM.INDEX+'/MgtRandDeb/LockAgentByCheck',
					method 	: 'POST',
					param 	: { lock_agent_id : lock_agent_id },
					ERROR	: function(e) {
						var ERROR = JSON.parse(e.target.responseText);
						if( ERROR.status ) {
							alert(ERROR.message);
							Ext.EQuery.construct(navigation,'')
							Ext.EQuery.postContent();
						}
					}	
			}).post();
		}
	}
	else
	{
		alert('Please Select Agent');
	}
}

Ext.DOM.UnlockbyCheck = function()
{
	var lock_agent_id = Ext.Cmp('lock_agent_id').getValue();
	if(lock_agent_id !='')
	{
		if(confirm('Are You Sure unlock this Agent?'))
		{
			Ext.Ajax({
					url 	: Ext.DOM.INDEX+'/MgtRandDeb/UnlockAgentByCheck',
					method 	: 'POST',
					param 	: { lock_agent_id : lock_agent_id },
					ERROR	: function(e) {
						var ERROR = JSON.parse(e.target.responseText);
						if( ERROR.status ) {
							alert(ERROR.message);
							Ext.EQuery.construct(navigation,'')
							Ext.EQuery.postContent();
						}
					}	
			}).post();
		}
	}
	else
	{
		alert('Please Select Agent');
	}
}


</script>
	
	<!-- start : content -->
	
		<fieldset class="corner">
			<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Lock Debitur Random</span> </legend>
				<div id="toolbars"></div>
				<div id="span_top_nav"></div>
				<div class="box-shadow" style="background-color:#FFFFFF;margin-top:10px;">	
					<div class="content_table"></div>
					<div id="pager"></div>
				</div>	
		</fieldset>	
		
	<!-- stop : content -->