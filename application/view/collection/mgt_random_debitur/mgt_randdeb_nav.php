<?php echo javascript(); ?>
<script type="text/javascript">


// @ pack : upload data listener 

Ext.DOM.onload = (function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 })();
 
// @ pack : upload data listener 

var RandomBY =  ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtRandDeb/get_random_by',
		method 	: 'POST',
		param 	: {}	
	}).json());
var Choosen_Setup;

// @ pack : upload data listener 

var datas= { 
	order_by  : '<?php echo $this -> URI -> _get_post('order_by');?>',
	type : '<?php echo $this -> URI -> _get_post('type');?>'
}

// @ pack : upload data listener 

 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle :[['Go To Setup'],['Stop Random'],['Lock Mode']],
		extMenu  :[['GoToSetup'],['StopRandom'],['SetupLockRandom']],
		extIcon  :[['cog_add.png'],['delete.png'],['lock_go.png']],
		extText  :true,
		extInput :true,
		extOption:[{
			render	: 0,
			header	: 'Random By ',
			type	: 'combo',
			id		: 'random_by', 	
			name	: 'random_by',
			value	: '',
			store	: [RandomBY.combo],
			triger	: '',
			width	: 200
		}]
	});	
});

// @ pack : upload data listener 

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

// @ pack : upload data listener 

var navigation = {
	custnav	 : Ext.DOM.INDEX+'/MgtRandDeb/index/',
	custlist : Ext.DOM.INDEX+'/MgtRandDeb/Content/',
}
	
// @ pack : upload data listener 

Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/*  
 * @ pack : handle menu user for application system 
 */
 
 Ext.DOM.ShowDetailSetup = function( value ) 
{
	// var cek  = "1,10";
  var list = value.split(",");
  var SetupId = list[0];
  var ModId = list[1];
  var detail = RandomBY.ui_detail_setup
  Choosen_Setup = SetupId;
  // console.log(SetupId);
  // console.log(ModId);
  Ext.Window
 ({
	url 	: Ext.DOM.INDEX+'/'+detail[ModId]+'/',
	method 	: 'GET',
	param 	: {  
		SetupId : SetupId, 
		action : 'show' 
	},
	scrollbars : 1,
	resizable  : 1, 
	width : ( $(window).width() - ( $(window).width()/3)),
	height : ( $(window).height() - ( $(window).height()/10))
}).popup();
	
}

Ext.DOM.GoToSetup = function()
{
	var random_by = Ext.Cmp('random_by').getValue();
	if(random_by == '')
	{
		alert('Please Choose Random By');
		return false;
	}
	
	var random =  ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtRandDeb/RunModul',
		method 	: 'POST',
		param 	: {
			random_by : random_by
		}	
	}).json());
	if( random.success == "0" )
	{
		alert(random.msg);
		return false;
	}
	
	var ui= RandomBY.ui_setup;
	// console.log(ui[random_by]);
	Ext.Ajax({	url 	: Ext.DOM.INDEX+'/'+ui[random_by], 
				method	: 'GET',
				param	: {
					random_by : random_by, 
				}
			}).load('main_content'); 
	
}

Ext.DOM.SetupLockRandom = function ()
{
	Ext.Ajax({	
		url 	: Ext.DOM.INDEX+'/MgtRandDeb/lock_agent_nav', 
		method	: 'GET',
		param	: {time : Ext.Date().getDuration()}
	}).load('main_content');
}

Ext.DOM.StopRandom = function()
{
	var SetupId= Ext.Cmp('modul_setup_id').getValue();
	
	if(SetupId =="")
	{
		alert("Please Choose Setup List");
		return false;
	}
	var lenghtSetupId = Object.keys(SetupId).length;
	if( lenghtSetupId > 1 )
	{
		alert("Please Choose One Setup");
		return false;
	}	
	var MsgClaim =  ( Ext.Ajax ({
		url 	: Ext.DOM.INDEX +'/MgtRandDeb/CheckClaimReq',
		method 	: 'POST',
		param 	: {SetupId : SetupId}	
	}).json());
	
	var text;
	if(MsgClaim.JumlahClaim=='0')
	{
		text = "Are you sure to stop it?";
	}
	else
	{
		text = "You have "+ MsgClaim.JumlahClaim+" request claim ( "+ MsgClaim.SetupName+" ) \n Are you sure to stop it ?";
	}
	if(confirm(text))
	{
		Ext.Ajax({
				url 	: Ext.DOM.INDEX+'/MgtRandDeb/StopRandomModule',
				method 	: 'POST',
				param 	: { SetupId : SetupId
						},
				ERROR	: function(e) {
					var ERROR = JSON.parse(e.target.responseText);
					if( ERROR.Message ) {
						alert("Success Stop");
						Ext.EQuery.construct(navigation,'')
						Ext.EQuery.postContent();
					}
				}	
		}).post();
	}
	
	
}
</script>
	
	<!-- start : content -->
	
		<fieldset class="corner">
			<legend class="icon-campaign">&nbsp;&nbsp;<span id="legend_title"></span> </legend>	
				<div id="toolbars"></div>
				<div id="span_top_nav"></div>
					<div class="box-shadow" style="background-color:#FFFFFF;margin-top:10px;">	
						<div class="content_table"></div>
						<div id="pager"></div>
					</div>	
		</fieldset>	
		
	<!-- stop : content -->