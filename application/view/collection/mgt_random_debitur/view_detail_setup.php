<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="author" content="<?php echo $website['_web_author']; ?>"/>
<meta name="version" content="<?php echo $website['_web_verion'];?>"/>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>
<meta http-equiv="Content-Script-Type" content="text/javascript">
<title><?php echo $website['_web_title'];?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />
<!-- script :: load all library jquery all :: componen--> 
  
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-1.4.4.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-ui.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_jquery();?>/plugins/extToolbars.js?time=<?php echo time();?>"></script>
 
</head>
<body>
<?php 

if(is_array($detail) and (count($detail)>0) )
{
	$header_table = $detail['header'];
	$content_table = $detail['content'];
	if( (count($header_table) > 0) AND (count($content_table) > 0) )
	{
		echo "<table cellspacing='1' width='100%' align='left' border='0'>
				<thead>
				<tr>";
		foreach($header_table as $index_header => $header_name)
		{
			echo "<th class='ui-corner-top ui-state-default middle' height='19px'>".$header_name."</th>";
		}
		echo "</tr></thead>";
		
		echo "<tbody><tr>";
		foreach($header_table as $index_header => $header_name)
		{
			foreach($content_table[$index_header] as $index => $value)
			{
				if($index_header=='Filename')
				{
					echo "<td class='content-middle center'>".basename($value)."</td>";
				}
				else
				{
					echo "<td class='content-middle center'>{$value}</td>";
				}
				
			}
		}
		echo "</tr></tbody></table>";
	}
}
?>
</body>
</html>

