<?php __(javascript()); ?>
<script type="text/javascript">


// @ pack : On ENTER Keyboard 
//,['control_fastforward_blue.png']
 $(function(){
	$('#toolbars').extToolbars({
		extUrl   : Ext.DOM.LIBRARY +'/gambar/icon',
		extTitle : [['Back To Random'],['Start']],
		extMenu  : [['backtosetup'],['StartRound']],
		extIcon  : [['house.png'],['control_play_blue.png']],
		extText  :true,
		extInput :false,
		extOption:[]
	});
			
	$('.date').datepicker({showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY + '/gambar/calendar.gif', 
		buttonImageOnly: true, dateFormat:'dd-mm-yy',
		readonly:true
	});
});

Ext.DOM.StartRound = function()
{
	var Req = [
		'round_account_status',
		'round_time_status',
		'random_by',
		'bool_round_team'
	];
	var CONDS_VARS;
	if(Ext.Cmp('bool_round_team').getValue()=="1")
	{
		Req = [
			'round_account_status',
			'round_time_status',
			'random_by',
			'bool_round_team',
			'round_tl'
		];
	}
	
	CONDS_VARS = Ext.Serialize('frmRoundStatus').Required(Req);
	if( !CONDS_VARS  ){
		Ext.Msg("Please input field of form ").Info();
		return false;
	}
	$("#frm-hide").hide();
	$("#frm-loading").show();
	Ext.Ajax 
	({
		url    : Ext.DOM.INDEX+"/MgtRandDeb/RoundByStatus/",
		method : 'POST',
		param  :  Ext.Join([
					Ext.Serialize('frmRoundStatus').getElement() 
				]).object(),
		ERROR : function( e ){
			Ext.Util(e).proc(function(response){
				var msg = "Round data by status"; 
				if( response.success ){ 
					Ext.Msg(msg+'\n'+response.debitur + ' debitur'+' to '+response.Agent +' Deskol').Success(); } 
				else {
					Ext.Msg(msg+'\n'+response.debitur + ' debitur '+response.Agent +' Deskol').Failed(); }
			});
			$("#frm-loading").hide();
			$("#frm-hide").show();
			
		}
	}).post();
}

/*
 * @ def	 : function ready 
 * 
 * @ params	 : method on ready pages 
 * @ package : bucket datas 
 */
Ext.DOM.backtosetup = function()
{
	if( confirm('Do you want to back random setup ?')){
		Ext.Ajax({ url : Ext.DOM.INDEX+'/MgtRandDeb/', method :'GET', param : {}}).load("main_content");
	}
}

// Ext.DOM.ForceRound = function()
// {
	// Ext.Ajax 
	// ({
		// url    : Ext.DOM.INDEX+"/MgtRandDeb/ForceRound/",
		// method : 'POST',
		// param  :  {
			// Force 	: 1
		// },
		// ERROR : function( e ){
			// Ext.Util(e).proc(function(response){
				// var msg = "Force round by status"; 
				// if( response.success ){ 
					// Ext.Msg(msg).Success(); } 
				// else {
					// Ext.Msg(msg).Failed(); }
			// });
		// }
	// }).post();
// }

Ext.DOM.LoadRoundLeader = function()
{
	Ext.Ajax ({	
		url	  : Ext.DOM.INDEX +'/MgtRandDeb/RoundperTeam/',
		param : { 
			time : Ext.Date().getDuration(),
			bool_round_team : Ext.Cmp('bool_round_team').getValue()
		}
	}).load("round_leader");
};

</script>

<!-- @ pack : start content -->
<div id="frm-loading" class="ui-widget-ajax-spiner" style="display:none;"></div>
<div id= "frm-hide" > 
<fieldset class="corner">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title">Round Account Status</span></legend>	
 <div class="box-shadowz" style="padding-bottom:4px;margin-top:-2px;margin-bottom:2px;">
  <form name="frmRoundStatus">
	<table  border=0 cellpadding=1 cellspacing=1>
				<tr>
					<td ><?php echo form()->hidden('random_by','input_text box',$RandomBy);?></td>
				</tr>
				<tr>
					<td class="text_caption bottom" ># Range Time&nbsp;:</td>
					<td class="bottom" ><?php echo form()->input('round_time_status','input_text box');?> (Minute)</td>
					<td class="text_caption bottom" ># Round per Team &nbsp;:</td>
					<td class="bottom" ><?php echo form() -> combo('bool_round_team','select long', array('1'=>"YES",'0'=>"NO"),"0",array('change'=>'Ext.DOM.LoadRoundLeader();'));?></td>
				</tr>
				<tr>
					
					<td class="text_caption bottom"  valign='top'># Account Status &nbsp;:</td>
					<td class="bottom" valign='top'><?php echo form()->listCombo('round_account_status','select auto',$Dropdown['DROPDOWN_ROUND_STATUS']);?></td>
					<td colspan="2" ><span id="round_leader" ></span></td>
				</tr>
				<?php
					if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
				array( USER_ROOT,USER_ADMIN ) ) )
				{
					echo '<tr>
					<td class="text_caption bottom"  valign="top"># Created By &nbsp;:</td>
					<td class="bottom" valign="top">'.form() -> combo('user_create_setup','select long', $spv).'</td>
					</tr>';
				}
				?>
				
			</table>
	</form>
</div>

<!-- @ pack : content table -->
<div id="toolbars"></div>
</fieldset>
<div>
<!-- @ pack : stop  content -->