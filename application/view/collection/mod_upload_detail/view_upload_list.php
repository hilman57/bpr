<?php 

/** @ pack : set label style on selected **/

 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');

/** @ pack :  get label header attribute **/

 $labels =& page_labels();
 $primary =& page_primary();

?>
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); ?>
			<th nowrap class="font-standars ui-corner-top ui-state-default middle center"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?> 
	</tr>
</thead>	
<tbody>
<?php

 $no = $num;
 foreach( $page->result_assoc() as $rows )  { 
   $color = ($no%2!=0?'#FAFFF9':'#FFFFFF');
?>
	<tr class="onselect" bgcolor="<?php echo $color; ?>">
		<td class="content-first center" width='5%'> 
		<?php echo form()->checkbox($primary, NULL, $rows[$primary], NULL, NULL ); ?>
		<td class="content-middle center"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field); ?>
		<?php if( $Field=='FTP_UploadFilename') {  ?>
		<td class="content-middle" <?php __($color) ?> ><?php echo ( $rows[$Field] ? basename($rows[$Field]) : '-' ); ?></td>
		<?php } else {  ?>
		<td class="content-middle" <?php __($color) ?> ><?php echo ( $rows[$Field]?$rows[$Field] : '-' ); ?></td>
		<?php } ?>	
		<?php endforeach;  ?>
	</tr>	
</tbody>
<?php $no++; } ?>
</table>