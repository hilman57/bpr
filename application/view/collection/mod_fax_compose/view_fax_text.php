
<script>
var MAIL_ADDRESS = [];
var ContentHTML = '';

/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */

	
Ext.DOM.AjaxStart = function(){
	Ext.Cmp("ajax_start").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='17px;'>");
}


/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */

	
Ext.DOM.AjaxStop = function(){
	Ext.Cmp("ajax_start").setText("");
}


/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
Ext.DOM.onloadTinyMCE = function() {
	Ext.DOM.AjaxStop();
	
	var editor = window.frames[0].tinyMCE;
	if( typeof(editor)=='object' ||  
		typeof(editor)=='function')
	{
		if(editor.isLoaded)
		{
			editor.setContent(ContentHTML);
			Ext.DOM.WebResizer(editor);
			editor.addEvent(editor.getInstanceById("web_editor").getWin(), "resize", function(){
				var allowHeight = parseInt(getCookie('TinyMCE_mce_editor_0_height'));
				var allowWidth = parseInt(getCookie('TinyMCE_mce_editor_0_width'));
	
				Ext.Cmp("web_editor_1").setAttribute('height',parseInt(allowHeight));
				Ext.Cmp("web_editor_1").setAttribute('width',parseInt(allowWidth)+25);
			});
		}	
	}
}

	
/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
 
var getCookie=function(cname)
{
	var name = cname + "=";
	var ca = window.frames[0].document.cookie.split(';');
	for(var i=0; i<ca.length; i++)
	  {
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	  }
	return "";
} 
	
/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
Ext.DOM.WebResizer  = function(editor){
	var allowHeight = parseInt(getCookie('TinyMCE_mce_editor_0_height'));
	var allowWidth = parseInt(getCookie('TinyMCE_mce_editor_0_width'));
	if( typeof(allowWidth)=='number' && parseInt(allowWidth) > 0 )
	{
		Ext.Cmp("web_editor_1").setAttribute('height',parseInt(allowHeight));
		Ext.Cmp("web_editor_1").setAttribute('width',parseInt(allowWidth)+25);
	}	
}	

/* 
 * -----------------------------------------------------------------------
 * @ def 	: unit Test javascript 
 * @ author : razaki team
 * -----------------------------------------------------------------------
 *
 * @ param  : MailId  <INT>
 * @ param  : MailType <STR>
 * ------------------------------------------------------------------------
 */
 
Ext.document().ready(function(){
	Ext.DOM.AjaxStart();
	Ext.Css("wrapper-compose").style({"text-align" : "left" });		
	var WindowFrame = Ext.Cmp("web_editor_1").getElementId();
		WindowFrame.src = Ext.DOM.LIBRARY+"/pustaka/tinymcpuk/index.php?time="+Ext.Date().getDuration();
		WindowFrame.frameborder=1; 
		WindowFrame.height = '100%';
		WindowFrame.scrolling="no"


});


</script>

<div id="wrapper-compose">
	<div id="ajax_start"></div>
	<iframe onload="Ext.DOM.onloadTinyMCE();" name="iframe_web_editor" id="web_editor_1" style="text-align:left;margin:0px;border:0px solid #000;"></iframe>
</div>