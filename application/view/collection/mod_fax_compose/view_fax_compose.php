<script type="text/javascript">

/* load content Text **/

 Ext.DOM.ContentTypeText = function(){
	
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/FaxCompose/FaxText/",
		method 	: 'GET',
		param 	: {
			time : Ext.Date().getDuration()
		}	
	}).load("content_type_data");	
 }
 
 
/* load content File **/

 Ext.DOM.ContentTypeFile = function(){
	
	Ext.Ajax({
		url 	: Ext.DOM.INDEX+"/FaxCompose/FaxFile/",
		method 	: 'GET',
		param 	: {
			time : Ext.Date().getDuration()
		}	
	}).load("content_type_data");	
 }
  
/*  Ext.DOM.MultipleAdd(this) **/

Ext.DOM.isValidFile = function(form){
	var match = new RegExp('.pdf');
	if( !match.test(form.value) ){
		Ext.Msg('Please select PDF Document').Info();
		form.value = null;
	}
}	

/* Ext.DOM.SubmitFax **/

Ext.DOM.SubmitFax = function()
{
 var CONTENT_TYPE = Ext.Cmp("ContentType").getValue();
 
 // send if file document 
 
 if( Ext.Cmp("FaxNumber").empty() ){
	Ext.Msg("Fax Number is empty !").Info(); }
 else 
 {
	if( CONTENT_TYPE =='FILE') 
	{
		var tag_names = Ext.Cmp('files').getName(), uploads = {};
		for(var ux = 0; ux<tag_names.length; ux++) {
			  if( tag_names[ux].value!='' ) {
					uploads[tag_names[ux].id] = document.getElementById(tag_names[ux].id).files[0];	
			  }
		  }
		  
		 // cek file data 
		 
		 if( typeof( uploads ) =='object' ){
			 Ext.Ajax 
			 ({
				url  	: Ext.DOM.INDEX+"/FaxCompose/FaxFileUpload/",
				method 	: 'POST',
				file 	: [uploads],
				param   : {
					FaxNumber : Ext.Cmp('FaxNumber').getValue()
				},
				complete :  function(e){
					Ext.Util(e).proc(function(send){
						if( send.success ){
							Ext.Msg("Fax is send ").Success();
						}
						else{
							Ext.Msg("Fax is send ").Failed();
						}
					});
				}
			 }).MultipleUpload();
		 }
	 }

	///// send if file text / mime  ////  
	 
	if( CONTENT_TYPE =='TEXT')
	{
		var FaxContent = window.frames[0].tinyMCE.getContent('web_editor');
		if( FaxContent==''){
			Ext.Msg("Content Data is empty").Info();}
		else
		{
			Ext.Ajax 
			({
				url 	: Ext.DOM.INDEX+"/FaxCompose/FaxFileText/",
				method 	: 'POST',
				param 	: { 
					FaxNumber : Ext.Cmp('FaxNumber').getValue(),
					FaxContent : FaxContent,
				},
				ERROR : function(e){
					Ext.Util(e).proc(function(send){
						if( send.success ){ 
							Ext.Msg("Fax is send ").Success(); }
						else {
							Ext.Msg("Fax is send ").Failed(); }
					});
				}
			}).post();	
		}	
	}
 }

}
 
/* Ext.DOM.SelectContentType */

Ext.DOM.SelectContentType = function( option ){
	if( option !='' ) {
		if( option=='FILE'){
			Ext.DOM.ContentTypeFile();
		}
		else{
			Ext.DOM.ContentTypeText();
		}
	}
}

/* create object **/
 Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
	Ext.DOM.ContentTypeText();
 });
 
</script> 
<fieldset class="corner" style="background-color:#FFFFFF;">
<legend class="icon-menulist">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div class="panel-content-fax">
	<table cellspacing=5 cellpadding='2px' border=0 width='99%'>
		<tr>
			<td class=" text_caption bottom left" style='font-size:14px;padding:0px 0px 4px 0px;'>* Destination Number</td>
		</tr>	
		<tr>
			<td class="bottom"><?php __(form()->input('FaxNumber', 'input_text long', NULL, NULL,array('style' => 'font-size:13px;height:25px;')));?> </td>
		</tr>	
		<tr>
			<td class="text_caption bottom left" style='font-size:14px;padding:0px 0px 4px 0px;'>* Content Type</td>
		</tr>

		<tr>
			<td class="bottom left"><?php __(form()->combo('ContentType', 'select long', array('TEXT' => 'Text', 'FILE' => 'PDF File ( document )'), 'TEXT', 
				array("change" =>"Ext.DOM.SelectContentType(this.value);"),array('style' => 'font-size:13px;height:30px;')));?> </td>
		</tr>	
		<tr>
			<td class="text_caption bottom left" style='font-size:14px;padding:0px 0px 4px 0px;'>* Content Data</td>
		</tr>
		<tr>
			<td> 
				<div id="content_type_data" style="padding:2px;border:1px solid #dddddd;">
					<?php __(form()->textarea('ContentData', 'textarea', NULL, NULL, array('style'=>'width:50%;height:200px;') ));?> 
				</div>
			</td>
		</tr>
		<tr>
			<td><?php __(form()->button('submit', 'save button','SUBMIT', array('click' => "Ext.DOM.SubmitFax();") ));?> </td>
		</tr>
	</table>

</div>
</fieldset>