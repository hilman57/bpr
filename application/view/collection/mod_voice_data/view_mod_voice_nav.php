<?php 
echo javascript(array( 
	array('_file' => base_jquery().'/plugins/extToolbars.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_jquery().'/plugins/Paging.js', 'eui_'=>'1.0.0', 'time'=>time()),
	array('_file' => base_enigma().'/helper/EUI_Media.js', 'eui_'=>'1.0.0', 'time'=>time())
));
?>

<script type="text/javascript">
/* create object **/

var KEY_ENTER = 13;

Ext.document('document').ready( function(){
	Ext.Cmp('legend_title').setText( Ext.System.view_file_name());
 }); 
 
var Reason = [];

Ext.EQuery.TotalPage   = '<?php echo $page -> _get_total_page(); ?>';
Ext.EQuery.TotalRecord = '<?php echo $page -> _get_total_record(); ?>';

/* catch of requeet accep browser **/
var datas = {
	voice_call_result 	: '<?php echo _get_exist_session('voice_call_result');?>',
	voice_campaign_id 	: '<?php echo _get_exist_session('voice_campaign_id');?>',
	voice_cust_name 	: '<?php echo _get_exist_session('voice_cust_name');?>',
	voice_cust_number 	: '<?php echo _get_exist_session('voice_cust_number');?>',
	voice_destination 	: '<?php echo _get_exist_session('voice_destination');?>',
	voice_end_date 		: '<?php echo _get_exist_session('voice_end_date');?>',
	voice_start_date 	: '<?php echo _get_exist_session('voice_start_date');?>',
	voice_user_id 		: '<?php echo _get_exist_session('voice_user_id');?>',
	order_by 			: '<?php echo _get_exist_session('order_by');?>',
	type	 			: '<?php echo _get_exist_session('type');?>'
}
			
/* assign navigation filter **/

var navigation = {
	custnav : Ext.DOM.INDEX +'/ModVoiceData/index/',
	custlist : Ext.DOM.INDEX +'/ModVoiceData/Content/'
}
		
/* assign show list content **/
		
Ext.EQuery.construct(navigation,datas)
Ext.EQuery.postContentList();

/* function searching customers **/
	
var searchCustomer = function()
{
	Ext.EQuery.construct(navigation,Ext.Join([Ext.Serialize('frmSearch').getElement()]).object() );
	Ext.EQuery.postContent();
}

// simple clear under form document

Ext.DOM.Clear = function(){
	Ext.Serialize('frmSearch').Clear();
	Ext.DOM.searchCustomer();
}

/* 
 * @ def : play
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
$(function(){
	$('#toolbars').extToolbars
	({
		extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		extTitle  : [['Search'],['Clear'],['Quick Time'],['Window Media Player'],['New Window Media Player'],['Stop'],['Download'],[]],
		extMenu   : [['searchCustomer'],['Clear'],['play'],['PlayWav'],['NewWindowMediaPlayer'],['StopPlay'], ['Download'],[]],
		extIcon   : [['zoom.png'], ['cancel.png'],['control_play_blue.png'],['control_play_blue.png'],['control_play_blue.png'],['cancel.png'],['disk.png'],[]],
		extText   : true,
		extInput  : true,
		extOption : [{
			render : 5,
			type   : 'label',
			id     : 'voice-list-wait', 	
			name   : 'voice-list-wait',
			label :'<span style="color:#dddddd;">-</span>'
		}]
	});
	
	$('.box').datepicker({
		showOn: 'button', 
		buttonImage: Ext.DOM.LIBRARY+'/gambar/calendar.gif', 
		buttonImageOnly: true, 
		dateFormat:'dd-mm-yy',readonly:true,
		onSelect	: function(date){
				if(typeof(date) =='string'){
					var x = date.split('-');
					var retur = x[2]+"-"+x[1]+"-"+x[0];
					if(new Date(retur) > new Date()) {
						Ext.Cmp($(this).attr('id')).setValue('');
					}
				}
			}
	});
});

Ext.DOM.NewWindowMediaPlayer = function(){
var RecordingId = Ext.Cmp('chk_record_call').getValue();
	
	Ext.Window
	({
		url   : Ext.DOM.INDEX +'/ModVoiceData/NewPlayWindow/',
		width : 500, height :280,
		param : {
			RecordId : RecordingId
		}
	}).popup();
}

/* 
 * @ pack : dial go to contact detail from recording OK 
 * ------------------------------------------
 */
 
 Ext.DOM.DialByDebiturId = function( CustomerId )
{
 
 if( CustomerId=='' ){ return false; }
 
   Ext.EQuery.Ajax
  ({
	url 	: Ext.DOM.INDEX +'/ModContactDetail/index/',
	method  : 'GET',
	param 	: {
		CustomerId : CustomerId,
		ControllerId : navigation.custnav
	}
  });
  
} // ==>  Ext.DOM.DialByDebiturId

/* 
 * @ def : play
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.play = function() {

  var RecordingId = Ext.Cmp('chk_record_call').getValue();
  if((RecordingId!='')) {
	Ext.Cmp("voice-list-wait").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='10px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax({
		url 	: Ext.DOM.INDEX +'/ModVoiceData/SetVoicePlay/',
		method 	: 'GET',
		param 	: { RecordId : RecordingId },
		ERROR   : function(e)
		{
			Ext.Cmp("voice-list-wait").setText('<span style="color:silver;">done.</span>');
			Ext.Util(e).proc(function(fn){
				if( fn.success )
				{
					Ext.Cmp("play_media").setValue(fn.data.file_voc_name);
					Ext.Media('play_panel', { 
						url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
						width 	: '40%',
						height 	: '40px',
						options : {
							ShowControls : 'true',
							ShowStatusBar: 'true',
							ShowDisplay  : 'true',
							autoplay 	 : 'true'
						}
					}).GSMPlayer(); 	
					
					Ext.Tpl("play_panel", fn.data).Compile();
					Ext.Cmp('QuikTime').setAttribute('class','textarea');
					Ext.Css('play_panel').style({
						'text-align' : 'left', 
						'padding-left' : "2px", 
						'padding-top' : "0px",
					});
					Ext.Css('div-voice-container').style({
						"margin-top" : "-10px", "width" : "40%",
						"margin-bottom" : "20px" 
					});
				}	
			});
		}	
	}).post();
 }
 else
	Ext.Msg('Please select a row!').Error();
}

// PlayWav 

Ext.DOM.PlayWav = function() {
	var RecordingId = Ext.Cmp('chk_record_call').getChecked();
  if((RecordingId!=''))
  {
	Ext.Cmp("voice-list-wait").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='10px;'> <span style='color:red;'>Please Wait...</span>");
	Ext.Ajax({
		url 	: Ext.DOM.INDEX +'/ModVoiceData/SetVoicePlay/',
		method 	: 'GET',
		param 	: { 
			RecordId : RecordingId,
			filetype : 'wav'
		},
		ERROR   : function(e)
		{
			Ext.Cmp("voice-list-wait").setText('<span style="color:silver;">done.</span>');
			Ext.Util(e).proc(function(fn){
				if( fn.success )
				{
					Ext.Cmp("play_media").setValue(fn.data.file_voc_name);
					Ext.Media('play_panel', { 
						url 	: Ext.System.view_app_url() +'/temp/'+ fn.data.file_voc_name,
						width 	: '40%',
						height 	: '120px',
						options : {
							ShowControls : 'true',
							ShowStatusBar: 'true',
							ShowDisplay  : 'true',
							autoplay 	 : 'true'
						}
					}).WAVPlayer(); 	
					
					Ext.Tpl("play_panel", fn.data).Compile();
					Ext.Cmp('MediaPlayer').setAttribute('class','textarea');
					Ext.Css('play_panel').style({'text-align' : 'left',  'padding-left' : "8px",  'padding-top' : "20px" });
					Ext.Css('div-voice-container').style({ "margin-top" : "5px", "width" : "40%", "margin-bottom" : "20px"  });
				}	
			});
		}	
	}).post();
 }
 else
	Ext.Msg('Please select a row!').Error();
}

/* memanggil Jquery plug in */

var Download = function() {
  var RecordingId = Ext.Cmp('chk_record_call').getValue();
  if( RecordingId !='' )
  {
		Ext.Cmp("voice-list-wait").setText("<img src="+ Ext.DOM.LIBRARY +"/gambar/loading.gif height='10px;'> <span style='color:red;'>Please Wait...</span>");
		Ext.Ajax({
			url 	: Ext.DOM.INDEX +'/ModVoiceData/DownloadVoice/',
			method 	: 'GET',
			param 	: { RecordId : RecordingId },
			ERROR   : function(e)
			{
				Ext.Util(e).proc(function(voice)
				{
					if( voice.success ) 
					{
						Ext.Cmp("voice-list-wait").setText('<span style="color:silver;">done.</span>');
						Ext.Window
						({
							url : Ext.DOM.INDEX +'/ModVoiceData/WgetDownload/',
							width : 10, height:10,
							param :{
								VoiceName : Ext.BASE64.encode(voice.data.file_voc_name)
							}
						}).popup();
					}
				});
			}
		}).post();
	}
}

/* 
 * @ def 	: play
 * ------------------------------------------------------------------
 * @ param  : - 
 */
 
Ext.Cmp("voice_user_id").listener({
	onkeyup: function(e) {
		Ext.Cmp('voice_user_id').setValue( Ext.Cmp('voice_user_id').getValue().toUpperCase());
		if( e.keyCode==KEY_ENTER ) {
			Ext.DOM.searchCustomer();
			Ext.Util(e).proc(function(properties) {
				Ext.Cmp(properties.id).setFocus();
			});
		}
	}
});

// on keyup by voice name 

Ext.Cmp("voice_cust_name").listener({
	onkeyup: function(e) {
		Ext.Cmp('voice_cust_name').setValue( Ext.Cmp('voice_cust_name').getValue().toUpperCase());
		if( e.keyCode==KEY_ENTER ) {
			Ext.DOM.searchCustomer();
			Ext.Util(e).proc(function(properties) {
				Ext.Cmp(properties.id).setFocus();
			});
		}
	}
});

// on keyup by voice name
 
Ext.Cmp("voice_destination").listener({
	onkeyup: function(e) {
		Ext.Cmp('voice_destination').setValue( Ext.Cmp('voice_destination').getValue().toUpperCase());
		if( e.keyCode==KEY_ENTER ) {
			Ext.DOM.searchCustomer();
			Ext.Util(e).proc(function(properties){
				Ext.Cmp(properties.id).setFocus();
			});
		}
	}
});

/* 
 * @ def : play
 * ------------------------------------------
 *
 * @ param : no define
 * @ aksess : procedure  
 */
 
Ext.DOM.StopPlay = function()
{

if(!Ext.Cmp("play_media").empty())
{
	Ext.Ajax
	({
		url 	: Ext.DOM.INDEX +'/ModVoiceData/DeletedVoice/',
		method 	: 'POST',
		param 	: {
			filename : Ext.Cmp("play_media").getValue()
		},
		ERROR : function(e){
			Ext.Util(e).proc(function(Delete){
				if(Delete.success){
					Ext.Cmp('play_panel').setText("");
				}
			});
		}
	  }).post();
  }

}

Ext.DOM.enterSearch = function( e )
{
	var _window = e;
	if( _window.keyCode == 13){
		searchCustomer();
	}
}

</script>
<!-- start : content -->
<fieldset class="corner" style="background-color:#FFFFFF;" onKeyDown="enterSearch(event)">
<legend class="icon-customers">&nbsp;&nbsp;<span id="legend_title"></span></legend>	
<div id="span_top_nav">
	
	<div id="result_content_add"  style="padding-bottom:4px;margin-top:2px;margin-bottom:4px;">
		<form name="frmSearch">
		<table cellpadding="3px" border=0>
		<tr>
			<td class="text_caption bottom"># Customer ID</td>
			<td>:</td>
			<td class='bottom'><?php echo form() -> input('voice_cust_number','input_text long',_get_exist_session('voice_cust_number')); ?></td>
			<td class="text_caption bottom"># Produk </td>
			<td>:</td>
			<td class='bottom'><?php echo form() -> combo('voice_campaign_id','select long',$CampaignId,_get_exist_session('voice_campaign_id')); ?></td>
			
		</tr>
		<tr>
			<td class="text_caption bottom"># Nama Pelanggan</td>
			<td>:</td>
			<td class='bottom'><?php echo form() -> input('voice_cust_name','input_text long',_get_exist_session('voice_cust_name')); ?></td>
			<td class="text_caption bottom"> Status Telpon </td>
			<td>:</td>
			<td class='bottom'><?php echo form() -> combo('voice_call_result','select long',$CallResult,_get_exist_session('voice_call_result')); ?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"># Tujuan </td>
			<td>:</td>
			<td><?php echo form() -> input('voice_destination','input_text long',_get_exist_session('voice_destination')); ?></td>
			<td class="text_caption bottom"># Penelepon  </td>
			<td>:</td>
			<td><?php echo form() -> combo('voice_user_id','select long',$UserId,_get_exist_session('voice_user_id')); ?></td>
		</tr>
		<tr>
			<td class="text_caption bottom"># Interval </td>
			<td>:</td>
			<td>
			<?php echo form()->input('voice_start_date','input_text date',(_have_get_session('voice_start_date')?_get_exist_session('voice_start_date'):date('d-m-Y'))); ?> <?php echo ('to')?>
			<?php echo form()->input('voice_end_date','input_text date',(_have_get_session('voice_end_date')?_get_exist_session('voice_end_date'):date('d-m-Y'))); ?> 
		</tr>
		</table>
		</form>
	</div>
	</div>
	
	<div id="toolbars"></div>
	<input type="hidden" name="play_media" id="play_media" value="">
	<div id="play_panel"></div>
	<div id="recording_panel" class="box-shadow">
		<div class="content_table" ></div>
		<div id="pager"></div>
	</div>
</fieldset>	