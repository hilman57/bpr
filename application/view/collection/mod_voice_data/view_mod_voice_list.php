<?php 

/** set label style on selected **/

 page_background_color('#FFFCCC');
 page_font_color('#8a1b08');
 
// @ pack :  set align #------------------------------

page_set_align('CampaignCode', 'center');
page_set_align('AccountNo', 'left');
page_set_align('CustomerName', 'left');
page_set_align('AmountWO', 'right');
page_set_align('DialNo', 'left');
page_set_align('CallDate', 'center');
page_set_align('UserCode', 'left');
page_set_align('Fullname', 'left');
page_set_align('AgentExt', 'center');
page_set_align('FileName', 'left');
page_set_align('FileSize', 'right');
page_set_align('Duration', 'center');
 
  
// @ pack : get all attribute  ------------------------------------

$labels =& page_labels();
$primary =& page_primary();
$aligns =& page_get_align();
$hidden =& page_hidden();

// @ pack : set order parameter -------------------------------

$call_user_func = array("AmountWO"=>"_getCurrency","DialNo"=>"_getMasking",
					    "FileSize"=>"_getFormatSize","CallDate"=>"_getDateTime",
						"Duration" => "_getDuration");
$call_link_user = array("CustomerName" =>"Ext.DOM.CallDialCustomer");

// ==> end pack 

?>

<table width="100%" class="custom-grid" cellspacing="1">
<tr height="24"> 
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center">&nbsp;#</th>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" >&nbsp;No</th>	
		<?php  foreach( $labels as $Field => $LabelName) : $class=&page_header($Field); ?>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle <?php echo $aligns[$Field];?>"> <span class="header_order <?php __($class);?>"  onclick="Ext.EQuery.orderBy('<?php __($Field);?>');">&nbsp;<?php __($LabelName);?></span></th>
		<?php endforeach; ?>
	</tr>
<tbody>

<?php 
$no = $num;
foreach($page->result_assoc() as $rows )
{ 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>
	<tr class="onselect" bgcolor='<?php echo $color;?>' id="rows_"<?php echo $rows[$primary];?>>
		<td class="content-first center" width='5%'> 
		<?php echo form()->checkbox($primary, null, $rows[$primary],array("change"=>"Ext.Cmp(this.id).oneChecked(this);"), null ); ?> </td>	
		<td class="content-middle center"> <?php echo $no;?></td>
		<?php foreach( $labels as $Field => $LabelName ) :  $color=&page_column($Field); ?>
		<td class="content-middle <?php echo $aligns[$Field];?>" <?php __($color) ?> >
			<?php if(!in_array($Field, array_keys( $call_link_user) ) ) : ?>
			<?php echo page_call_function($Field, $rows, $call_user_func); ?>
			<?php else : ?>
				<a href="javascript:void(0);" 
				  title='click here' style='cursor:pointer;text-decoration:none;'
				  onclick="Ext.DOM.DialByDebiturId('<?php echo $rows[reset($hidden)]; ?>');" >
				  <?php echo $rows[$Field]; ?> </a>
			<?php endif; ?>	
		</td> 		
		<?php endforeach;  ?>
	</tr>	
</tbody>
<?php $no++; } ?>
</table>






