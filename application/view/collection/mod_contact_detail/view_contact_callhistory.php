<?php ?>
<script>

 var callhistoryLayout = function() {
 var tabs_height = $("#main_content").height();	
	$("#tabs_list_activity").tabs();
	$("#tabs_list_activity").tabs('select',0);
	 
	 $("#tabs_history_activity").css
	({
	 'width' : "99%",
	 'float' : 'left',
	 'margin-top' : '5px',
	 'padding' : '1px 2px 2px 2px' });
	 
	
	$("#tabs_recording_activity").css
	({
	 'width' : "99%", //$('#tabs_list_activity').width(),
	 'float' : 'left',
	 'margin-top' : '5px',
	 'padding' : '1px 2px 2px 2px' });
	 
	 // $("#tabs_list_account_map").css
	// ({
	 // 'width' : $('#tabs_list_activity').width(),
	 // 'float' : 'left',
	 // 'margin-top' : '5px',
	 // 'padding' : '1px 2px 2px 2px' });
	 
	// CSS untuk tabs old history call
	 $("#tabs_old_history_activity").css
	({
	 'width' : "99%",
	 'float' : 'left',
	 'margin-top' : '5px',
	 'padding' : '1px 2px 2px 2px' });
	 
 }

$('document').ready(function(){
	callhistoryLayout(); 
});

	 
	
$("#main_content").resize(function(){
	callhistoryLayout(); 
});

</script>
<fieldset class="corner"> 
<legend class="icon-menulist"> &nbsp;&nbsp; Aktivitas Riwayat Panggilan</legend> 
<div id="tabs_list_activity" style ="overflow:auto;margin-top:-10px;height:115px;">
 <ul>
	<!--<li><a href="#tabs_list_account_map">List Account Maping</a></li> --> 
	
	<li><a href="#tabs_history_activity">Riwayat panggilan</a></li>
	<li><a href="#tabs_recording_activity">Rekaman</a></li>
	<?php 
	// Handling jika agent yang login maka tabs old call history tidak akan muncul
	 if( $this->EUI_Session->_get_session('HandlingType') != USER_AGENT_OUTBOUND) {
	 ?>
	<li><a href="#tabs_old_history_activity">Riwayat Panggilan Lama</a></li>
	<?php } ?>
</ul>


<!-- start : load history by Customer -->

<div id="tabs_history_activity" style="overflow:auto;margin-top:-5px;">
 <table border=0 align="left" cellspacing=1 width="99%">
	<thead>
		<tr height='24'>
			<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
			<th class="ui-corner-top ui-state-default first" WIDTH="15%" nowrap>&nbsp;Tanggal Panggilan</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="15%" nowrap>&nbsp;Deskoll</td>			
			<th class="ui-corner-top ui-state-default first" width="65%">&nbsp;Catatan</td>
		</tr>
	</thead>
	<tbody id="DebiturHistory"></tbody>
 </table>
</div>

<!-- load :: policy data -->
<div id="tabs_recording_activity" style="background-color:#FFFFFF;overflow:auto;"> 
 <table border=0 align="left" cellspacing=1 width="99%">
   <thead>
	 <tr height='24'>
		<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
		<th class="ui-corner-top ui-state-default first" WIDTH="10%" nowrap>&nbsp;Extension</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Nama File</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;UkuranFile</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="10%" nowrap>&nbsp;Tanggal</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="5%">&nbsp;Durasi</td>
		<th class="ui-corner-top ui-state-default first left" WIDTH="5%">&nbsp;ID Pengguna</td>
	 </tr>
   </thead>
  <tbody id="DebiturRecording"></tbody>
 </table>
</div>


<?php 
 if( $this->EUI_Session->_get_session('HandlingType') != USER_AGENT_OUTBOUND) {
 ?>
<!-- start : load old history by Customer -->
<div id="tabs_old_history_activity" style="overflow:auto;margin-top:-5px;">
 <table border=0 align="left" cellspacing=1 width="99%">
	<thead>
		<tr height='24'>
			<th class="ui-corner-top ui-state-default first" WIDTH="5%" nowrap>&nbsp;No.</td>
			<th class="ui-corner-top ui-state-default first" WIDTH="15%" nowrap>&nbsp;Tanggal Panggilan</td>
			<th class="ui-corner-top ui-state-default first left" WIDTH="15%" nowrap>&nbsp;Deskoll</td>			
			<th class="ui-corner-top ui-state-default first" width="65%">&nbsp;Catatan</td>
		</tr>
	</thead>
	<tbody id="DebiturOldHistory"></tbody>
 </table>
</div>
<?php } ?>

</div>
</fieldset>
