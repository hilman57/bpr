<?php 
/**
 * @ def 		: view agent data old by customer ID 
 * -----------------------------------------
 * 
 * @ params  	: $ Customer ID 
 * @ example    : -
 *
 **/
 ?>
 
<table width="99%" align="center" cellpadding="2px" cellspacing="4px">
	<tr>
		<td nowrap class="text_caption">Nama Agent</td>
		<td><?php echo form() -> input('Employee_ID','input_text long',$Customers['Employee_ID'],NULL,1);?> </td>
		<td class="text_caption" nowrap >Jenis Kelamin</td>
		<td><?php echo form() -> input('Employee_Gender','input_text long',$Combo['Gender'][$Customers['Employee_Gender']],NULL,1);?></td>
	</tr>
	<tr>	
		<td class="text_caption" nowrap >No Telepon </td>
		<td><?php echo form() -> input('Employee_Ph_No','input_text long',$Customers['Employee_Ph_No'],NULL,1);?></td>
	</tr>
</table>	