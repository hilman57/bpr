<?php echo base_window_header("Form Complain"); ?>
<style> 
.ui-chrome{ border-radius:3px;}
.ui-chrome:hover{ border:1px solid #FF4321;}
.ui-context-chrome { border-radius:4px;height:22px;}
</style>
<script>
var vcreason = null
Ext.DOM.pickReason = function(data) {
	var x = document.getElementById('vcreason')
	for (var i = 0; i < x.length; i++) {
		var option = x.options[i];
		if (option.value == data.value) {
			var split = option.text.split(' ')
			vcreason = split[0]
		}
	}
} 
// efine url 
 
 var URL = function () 
{
	return window.opener.Ext.DOM.INDEX;
}
var phoneblock = function()
{
	return window.opener.BLOCK_PHONE;
}
Ext.DOM.SaveComplain = function()
{
	// if(phoneblock()=="")
	// {
		// alert("Please block some phone");
		// return false;
	// }
	var FrmComplain = Ext.Serialize('frmComplain');
	var VarComplain = new Array();
	
	VarComplain['CustomerId'] = Ext.Cmp("DebiturId").getValue();
	// VarComplain['PhoneBlock'] = phoneblock();
	Ext.Ajax
	({
		url 	: URL()+"/ModSaveActivity/save_vc_reason/",
		method 	: "POST",
		param 	: Ext.Join([
					FrmComplain.getElement() ,VarComplain
				]).object(),
		ERROR :function( e ) {
			Ext.Util(e).proc(function( response ){
				if( response.success ){
					Ext.Msg("VC Reason").Success();
					localStorage.setItem('reasonVC', vcreason+'- Approve')
					Ext.Serialize('frmComplain').Clear();
				} else {
					Ext.Msg("VC Reason").Failed();
				}
			});
		}
		
	}).post();
};
</script>
</head>
<body style="margin:10px 5px 5px 5px;">
	<fieldset class="corner" style="border-radius:3px;margin-top:10px;">
		<legend class="edit-users-x"><span style="margin-left:8px;">VC Reason</span></legend>
		<form name="frmComplain">
		<?php echo form()->hidden('DebiturId',null, $DEBITUR);?>
		<?php echo "\r\n";?>
		<div class="ui-widget-form-table-compact">
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption">Reason</div>
				<div class="ui-widget-form-cell center">:</div>
				<div class="ui-widget-form-cell left"><?php echo form()->combo('vcreason', 'select long',$REASON_VC, NULL, array("change" => "Ext.DOM.pickReason(this, $REASON_VC)") );?></div>
			</div>
			<div class="ui-widget-form-row">
				<div class="ui-widget-form-cell text_caption"></div>
				<div class="ui-widget-form-cell center"></div>
				<div class="ui-widget-form-cell left">
					<?php echo form()->button('btnshow', 'button save', "Save", array("click" => "Ext.DOM.SaveComplain();") );?>
				</div>
			</div>
			
		</div>
		</form>
		
	</fieldset>
</body>
</html>