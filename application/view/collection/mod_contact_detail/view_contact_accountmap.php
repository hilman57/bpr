<div style="margin-top:25px;" id="account-maping-data" >
<?php 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 * 
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 * @ example    : get by fields ID / Campaign ID
 */

$conn =& get_instance();
if( $conn AND is_array($OtherAccount) ) 
{
 
 $conn->db->select('Field_Id');
 $conn->db->from('t_gn_field_campaign');
 $conn->db->where('CampaignId', $OtherAccount['deb_cmpaign_id']);
 $conn->db->where('Field_Active',1);
 $conn->db->order_by('Field_Id','ASC');
 // echo $conn->db->_get_var_dump();
 
 foreach( $conn->db->get() -> result_assoc() as $rows )
 { 
	if( $Flexible =& _fldFlexibleLayout($rows['Field_Id']))
	{
		$Flexible -> _setTables('t_gn_debitur'); // rcsorce data 
		$Flexible -> _setCustomerId(array('deb_id' => $OtherAccount['deb_id'] )); // set conditional array();
		$Flexible -> _Compile();
	}
 }
 
}	

?>
</div>