<html>
<head>
<?php $this -> load -> view('rpt_sum_activity_staff/rpt_sum_activity_staff_style'); ?>
<title>RECAP SUMMARY ACTIVITY STAFF Welcome Call</title>
</head>
<body>
	<table border=0 cellpadding=0 cellspacing=0 width=1189 class=xl971645 style='border-collapse:collapse;table-layout:fixed;width:896pt'>
		<tr class=xl1001645 height=31 style='mso-height-source:userset;height:23.25pt'>
			<td rowspan=10 height=217 class=xl1771645 width=22 style='height:162.75pt;width:17pt'>&nbsp;</td>
			<td colspan=5 class=xl1791645 width=462 style='width:348pt'>RECAP SUMMARY ACTIVITY STAFF Welcome Call</td>
			<td class=xl1171645 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl1171645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl1171645 width=77 style='width:58pt'>&nbsp;</td>
			<td class=xl1171645 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl1171645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1171645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1171645 width=95 style='width:71pt'>&nbsp;</td>
			<td class=xl1171645 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl1181645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td colspan=2 height=20 class=xl1801645 width=196 style='height:15.0pt;width:148pt'>Periode<span style='mso-spacerun:yes'> </span></td>
			<td class=xl1191645 width=106 style='width:80pt'>: Oct 2014</td>
			<td class=xl1191645 width=76 style='width:57pt'>&nbsp;</td>
			<td class=xl1201645 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl1201645 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl1201645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl1201645 width=77 style='width:58pt'>&nbsp;</td>
			<td class=xl1201645 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl1201645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1201645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1201645 width=95 style='width:71pt'>&nbsp;</td>
			<td class=xl1201645 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=8 style='mso-height-source:userset;height:6.0pt'>
			<td height=8 class=xl1021645 width=106 style='height:6.0pt;width:80pt'>&nbsp;</td>
			<td class=xl1021645 width=90 style='width:68pt'>&nbsp;</td>
			<td class=xl1021645 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl1011645 width=76 style='width:57pt'>&nbsp;</td>
			<td class=xl1011645 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl1011645 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl1011645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl1011645 width=77 style='width:58pt'>&nbsp;</td>
			<td class=xl1011645 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl1011645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1011645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1011645 width=95 style='width:71pt'>&nbsp;</td>
			<td class=xl1011645 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;123</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td rowspan=3 height=78 class=xl1691645 style='border-bottom:1.0pt solid black;height:58.5pt'>No</td>
			<td colspan=2 rowspan=3 class=xl1661645 style='border-bottom:1.0pt solid black'>User Name</td>
			<td colspan=6 class=xl1951645 width=482 style='border-bottom:.5pt solid black;border-left:none;width:362pt'>Sample QA</td>
			<td rowspan=3 class=xl1741645 width=74 style='border-bottom:1.0pt solid black;width:56pt'>Capacity</td>
			<td rowspan=3 class=xl1741645 width=74 style='border-bottom:1.0pt solid black;width:56pt'>Effective Work Days</td>
			<td rowspan=3 class=xl1741645 width=95 style='border-bottom:1.0pt solid black;width:71pt'>Productivity</td>
			<td rowspan=3 class=xl1911645 width=94 style='border-bottom:1.0pt solid black;width:71pt'># CCO <br>survey/day</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=38 style='mso-height-source:userset;height:28.5pt'>
			<td rowspan=2 height=58 class=xl1941645 width=76 style='border-bottom:1.0pt solid black;height:43.5pt;width:57pt'>Jumlah</td>
			<td class=xl1061645 width=84 style='border-top:none;border-left:none;width:63pt'>WCDS1 WC11411001</td>
			<td class=xl1061645 width=83 style='border-top:none;border-left:none;width:62pt'>WCDS2 WC11411001</td>
			<td class=xl1061645 width=80 style='border-top:none;border-left:none;width:60pt'>WCDS3 WC11411001</td>
			<td class=xl1061645 width=77 style='border-top:none;border-left:none;width:58pt'>WCDS4 WC11411001</td>
			<td class=xl1471645 width=82 style='border-top:none;border-left:none;width:62pt'>WCDS1 WC01410001</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1151645 width=84 style='height:15.0pt;border-top:none;border-left:none;width:63pt'>BCA</td>
			<td class=xl1151645 width=83 style='border-top:none;border-left:none;width:62pt'>CIMB</td>
			<td class=xl1151645 width=80 style='border-top:none;border-left:none;width:60pt'>Other Bancass</td>
			<td class=xl1151645 width=77 style='border-top:none;border-left:none;width:58pt'>Agency</td>
			<td class=xl1161645 width=82 style='border-top:none;border-left:none;width:62pt'>Temporell</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1111645 style='height:15.0pt'>1</td>
			<td colspan=2 class=xl1841645 style='border-left:none'>Nita Sumarjiati</td>
			<td class=xl1121645 width=76 style='border-left:none;width:57pt'>0</td>
			<td class=xl1131645 width=84 style='border-left:none;width:63pt'>&nbsp;</td>
			<td class=xl1131645 width=83 style='border-left:none;width:62pt'>&nbsp;</td>
			<td class=xl1131645 width=80 style='border-left:none;width:60pt'>&nbsp;</td>
			<td class=xl1131645 width=77 style='border-left:none;width:58pt'>&nbsp;</td>
			<td class=xl1131645 width=82 style='border-left:none;width:62pt'>&nbsp;</td>
			<td class=xl1141645 width=74 style='border-left:none;width:56pt'>&nbsp;</td>
			<td class=xl1141645 width=74 style='border-left:none;width:56pt'>&nbsp;</td>
			<td class=xl1141645 width=95 style='border-left:none;width:71pt'>&nbsp;</td>
			<td class=xl1141645 width=94 style='border-left:none;width:71pt'>&nbsp;</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1101645 style='height:15.0pt;border-top:none'>3</td>
			<td colspan=2 class=xl1851645 style='border-left:none'>TBA1</td>
			<td class=xl1071645 width=76 style='border-top:none;border-left:none;width:57pt'>0</td>
			<td class=xl1081645 width=84 style='border-top:none;border-left:none;width:63pt'>&nbsp;</td>
			<td class=xl1081645 width=83 style='border-top:none;border-left:none;width:62pt'>&nbsp;</td>
			<td class=xl1081645 width=80 style='border-top:none;border-left:none;width:60pt'>&nbsp;</td>
			<td class=xl1081645 width=77 style='border-top:none;border-left:none;width:58pt'>&nbsp;</td>
			<td class=xl1081645 width=82 style='border-top:none;border-left:none;width:62pt'>&nbsp;</td>
			<td class=xl1091645 width=74 style='border-top:none;border-left:none;width:56pt'>&nbsp;</td>
			<td class=xl1091645 width=74 style='border-top:none;border-left:none;width:56pt'>&nbsp;</td>
			<td class=xl1091645 width=95 style='border-top:none;border-left:none;width:71pt'>&nbsp;</td>
			<td class=xl1091645 width=94 style='border-top:none;border-left:none;width:71pt'>&nbsp;</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1481645 style='height:15.0pt;border-top:none'>4</td>
			<td colspan=2 class=xl1861645 style='border-left:none'>TBA2</td>
			<td class=xl1451645 width=76 style='border-top:none;border-left:none;width:57pt'>0</td>
			<td class=xl1461645 width=84 style='border-top:none;border-left:none;width:63pt'>&nbsp;</td>
			<td class=xl1461645 width=83 style='border-top:none;border-left:none;width:62pt'>&nbsp;</td>
			<td class=xl1461645 width=80 style='border-top:none;border-left:none;width:60pt'>&nbsp;</td>
			<td class=xl1461645 width=77 style='border-top:none;border-left:none;width:58pt'>&nbsp;</td>
			<td class=xl1461645 width=82 style='border-top:none;border-left:none;width:62pt'>&nbsp;</td>
			<td class=xl1491645 width=74 style='border-top:none;border-left:none;width:56pt'>&nbsp;</td>
			<td class=xl1491645 width=74 style='border-top:none;border-left:none;width:56pt'>&nbsp;</td>
			<td class=xl1491645 width=95 style='border-top:none;border-left:none;width:71pt'>&nbsp;</td>
			<td class=xl1491645 width=94 style='border-top:none;border-left:none;width:71pt'>&nbsp;</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1221645 width=22 style='height:15.0pt;width:17pt'>&nbsp;</td>
			<td class=xl1501645 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl1511645 width=90 style='width:68pt'>TOTAL</td>
			<td class=xl1511645 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl1521645 width=76 style='width:57pt'>0</td>
			<td class=xl1531645 width=84 style='border-left:none;width:63pt'>0</td>
			<td class=xl1531645 width=83 style='border-left:none;width:62pt'>0</td>
			<td class=xl1531645 width=80 style='border-left:none;width:60pt'>0</td>
			<td class=xl1531645 width=77 style='border-left:none;width:58pt'>0</td>
			<td class=xl1531645 width=82 style='border-left:none;width:62pt'>0</td>
			<td class=xl1531645 width=74 style='border-left:none;width:56pt'>0</td>
			<td class=xl1531645 width=74 style='border-left:none;width:56pt'>0</td>
			<td class=xl1531645 width=95 style='border-left:none;width:71pt'>0</td>
			<td class=xl1541645 width=94 style='border-left:none;width:71pt'>0</td>
			<td class=xl1211645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1231645 width=22 style='height:15.0pt;width:17pt'>&nbsp;</td>
			<td class=xl1241645 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl1241645 width=90 style='width:68pt'>&nbsp;</td>
			<td class=xl1241645 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl1241645 width=76 style='width:57pt'>&nbsp;</td>
			<td class=xl1241645 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl1241645 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl1241645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl1241645 width=77 style='width:58pt'>&nbsp;</td>
			<td class=xl1241645 width=82 style='width:62pt'>&nbsp;</td>
			<td class=xl1241645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1241645 width=74 style='width:56pt'>&nbsp;</td>
			<td class=xl1241645 width=95 style='width:71pt'>&nbsp;</td>
			<td class=xl1241645 width=94 style='width:71pt'>&nbsp;</td>
			<td class=xl1251645 width=46 style='width:35pt'>&nbsp;</td>
		</tr>
		<tr class=xl1011645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl981645 width=22 style='height:15.0pt;border-top:none;width:17pt'>&nbsp;</td>
			<td class=xl981645 width=106 style='border-top:none;width:80pt'>&nbsp;</td>
			<td class=xl981645 width=90 style='border-top:none;width:68pt'>&nbsp;</td>
			<td class=xl981645 width=106 style='border-top:none;width:80pt'>&nbsp;</td>
			<td class=xl981645 width=76 style='border-top:none;width:57pt'>&nbsp;</td>
			<td class=xl981645 width=84 style='border-top:none;width:63pt'>&nbsp;</td>
			<td class=xl981645 width=83 style='border-top:none;width:62pt'>&nbsp;</td>
			<td class=xl981645 width=80 style='border-top:none;width:60pt'>&nbsp;</td>
			<td class=xl981645 width=77 style='border-top:none;width:58pt'>&nbsp;</td>
			<td class=xl991645 width=82 style='border-top:none;width:62pt'>&nbsp;</td>
			<td class=xl981645 width=74 style='border-top:none;width:56pt'>&nbsp;</td>
			<td class=xl981645 width=74 style='border-top:none;width:56pt'>&nbsp;</td>
			<td class=xl981645 width=95 style='border-top:none;width:71pt'>&nbsp;</td>
			<td class=xl981645 width=94 style='border-top:none;width:71pt'>&nbsp;</td>
			<td class=xl1001645 width=46 style='width:35pt'></td>
		</tr>
		<tr class=xl1001645 height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1011645 width=22 style='height:15.0pt;width:17pt'>&nbsp;</td>
			<td colspan=2 class=xl1871645 width=196 style='width:148pt'>&nbsp;</td>
			<td class=xl1031645 width=106 style='width:80pt'>&nbsp;</td>
			<td class=xl1031645 width=76 style='width:57pt'>&nbsp;</td>
			<td class=xl1031645 width=84 style='width:63pt'>&nbsp;</td>
			<td class=xl1031645 width=83 style='width:62pt'>&nbsp;</td>
			<td class=xl1041645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl1001645 width=77 style='width:58pt'></td>
			<td class=xl1001645 width=82 style='width:62pt'></td>
			<td class=xl1001645 width=74 style='width:56pt'></td>
			<td class=xl1001645 width=74 style='width:56pt'></td>
			<td class=xl1001645 width=95 style='width:71pt'></td>
			<td class=xl1001645 width=94 style='width:71pt'></td>
			<td class=xl1001645 width=46 style='width:35pt'></td>
		</tr>
		<tr height=26 style='mso-height-source:userset;height:19.5pt'>
			<td height=26 class=xl1051645 style='height:19.5pt'>&nbsp;</td>
			<td rowspan=4 class=xl1881645 width=106 style='border-bottom:1.0pt solid black;border-top:none;width:80pt'>Tgl</td>
			<td class=xl1551645 colspan=2>QA Monitored</td>
			<td class=xl1561645>&nbsp;</td>
			<td class=xl1561645>&nbsp;</td>
			<td class=xl1561645>&nbsp;</td>
			<td class=xl1571645>&nbsp;</td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
		</tr>
		<tr height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1051645 style='height:12.75pt'>&nbsp;</td>
			<td colspan=5 class=xl1811645 width=439 style='border-right:.5pt solid black;border-left:none;width:330pt'>&lt;User Welcome Call Name&gt;</td>
			<td class=xl1581645 width=80 style='border-left:none;width:60pt'>&nbsp;</td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
		</tr>
		<tr height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1051645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1061645 width=90 style='border-top:none;width:68pt'>WCDS1WC11411001</td>
			<td class=xl1061645 width=106 style='border-top:none;border-left:none;width:80pt'>WCDS2 WC11411001</td>
			<td class=xl1061645 width=76 style='border-top:none;border-left:none;width:57pt'>WCDS3 WC11411001</td>
			<td class=xl1061645 width=84 style='border-top:none;border-left:none;width:63pt'>WCDS4 WC11411001</td>
			<td class=xl1471645 width=83 style='border-top:none;border-left:none;width:62pt'>WCDS1 WC01410001</td>
			<td class=xl1581645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
		</tr>
		<tr class=xl1261645 height=33 style='height:24.75pt'>
			<td height=33 class=xl1261645 style='height:24.75pt'>&nbsp;</td>
			<td class=xl1151645 width=90 style='border-top:none;width:68pt'>BCA</td>
			<td class=xl1151645 width=106 style='border-top:none;border-left:none;width:80pt'>CIMB</td>
			<td class=xl1151645 width=76 style='border-top:none;border-left:none;width:57pt'>Other Bancass</td>
			<td class=xl1151645 width=84 style='border-top:none;border-left:none;width:63pt'>Agency</td>
			<td class=xl1161645 width=83 style='border-top:none;border-left:none;width:62pt'>Temporell</td>
			<td class=xl1591645 width=80 style='width:60pt'>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>1-Oct</td>
			<td class=xl1281645 style='border-left:none'>&nbsp;</td>
			<td class=xl1291645 style='border-left:none'>&nbsp;</td>
			<td class=xl1291645 style='border-left:none'>&nbsp;</td>
			<td class=xl1291645 style='border-left:none'>&nbsp;</td>
			<td class=xl1291645 style='border-left:none'>&nbsp;</td>
			<td class=xl1301645 style='border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>2-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>3-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>4-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>5-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>6-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>7-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>8-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1341645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1341645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>9-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
		</tr>
		<tr class=xl1341645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1341645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>10-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
			<td class=xl1341645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>11-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>12-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>13-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>14-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>15-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>16-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>17-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>18-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>19-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>20-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>21-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>22-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>23-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>24-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>25-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1601645>26-Oct</td>
			<td class=xl1611645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1621645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>27-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>28-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>29-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>30-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr class=xl1261645 height=17 style='mso-height-source:userset;height:12.75pt'>
			<td height=17 class=xl1261645 style='height:12.75pt'>&nbsp;</td>
			<td class=xl1271645>31-Oct</td>
			<td class=xl1311645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1321645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1331645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
			<td class=xl1261645>&nbsp;</td>
		</tr>
		<tr height=20 style='mso-height-source:userset;height:15.0pt'>
			<td height=20 class=xl1051645 style='height:15.0pt'>&nbsp;</td>
			<td class=xl1631645>1-Nov</td>
			<td class=xl1641645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1651645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1651645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1651645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1651645 style='border-top:none;border-left:none'>&nbsp;</td>
			<td class=xl1651645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1351645></td>
			<td class=xl1361645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
		</tr>
		<tr class=xl971645 height=18 style='height:13.5pt'>
			<td height=18 class=xl1051645 style='height:13.5pt'>&nbsp;</td>
			<td class=xl1371645 style='border-top:none'>Rata-rata</td>
			<td class=xl1381645 style='border-top:none'>#DIV/0!</td>
			<td class=xl1391645 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl1391645 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl1391645 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl1391645 style='border-top:none;border-left:none'>#DIV/0!</td>
			<td class=xl1401645>0.00</td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
		</tr>
		<tr class=xl971645 height=18 style='height:13.5pt'>
			<td height=18 class=xl1051645 style='height:13.5pt'>&nbsp;</td>
			<td class=xl1411645 style='border-top:none'>Jumlah/staff</td>
			<td class=xl1421645 style='border-top:none'>0</td>
			<td class=xl1431645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1431645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1431645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1431645 style='border-top:none;border-left:none'>0</td>
			<td class=xl1441645 style='border-top:none'>0</td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
			<td class=xl971645></td>
		</tr>
	</table>
</body>
</html>