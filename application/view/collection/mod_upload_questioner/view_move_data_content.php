<div class="ui-widget-form-table-compact" style="width:99%;">
    <div class="ui-widget-form-row">
        <div class="ui-widget-form-cell ui-widget-content-top " style="width:70%">
            <fieldset class="corner" style="border-radius:3px;margin:-20px 0px 0px -15px;">
                <legend class="icon-menulist"> <span style="margin-left:8px;">List Data</span></legend>
                <div id="ui-widget-content-debitur-page" style="margin-top:-5px;"></div>
            </fieldset>
        </div>

        <div class="ui-widget-form-cell ui-widget-content-top " style="width:30%;">
            <fieldset class="corner" style="border-radius:3px;margin:-20px 0px 0px 0px;">
                <legend class="edit-users-x"> <span style="margin-left:8px;">User Option</span></legend>
                <form name="frmSwapFilterData">
                    <div class="ui-widget-form-table-compact">
                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">* Product</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->combo('swp_from_campaign_id', 'select long',$CampaignId, _get_exist_session('transfer_campaign_id'));?>
                            </div>

                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Account Status</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->combo('swp_from_account_status', 'select long',$AccountStatus, _get_exist_session('transfer_account_status'));?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Call Status</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->combo('swp_from_call_status', 'select long',$AccountStatus, _get_exist_session('transfer_account_status'));?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Call Date</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->input('swp_from_call_start_date', 'input_text date');?>
                                <?php echo form()->input('swp_from_call_end_date', 'input_text date');?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Amount WO</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->input('swp_from_amount_start', 'input_text box');?> to
                                <?php echo form()->input('swp_from_amount_end', 'input_text box');?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Customer ID</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->textarea('swp_from_customer_id', 'textarea long',null,null, array('style' => 'height:22px;width:165px;') );?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Supervisor</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->combo('swp_from_spv_id', 'select long', Supervisor());?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Senior Leader</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->combo('swp_from_senior_leader_id', 'select long', Seniorleader(), null ,array("change" => "new ShowTLBySTL(this);") );?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Leader</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui_swp_from_tl_id">
                                <?php echo form()->combo('swp_from_leader_id', 'select long', Teamleader(), null ,array("change" => "new ShowAgentByTL(this);") );?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Deskoll</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui_swp_from_deskoll_id">
                                <?php echo form()->combo('swp_from_deskoll_id', 'select long');?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Page/Record</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->input('swp_from_page_record', 'input_text box', 20);?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption"></div>
                            <div class="ui-widget-form-cell center"></div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->button('btnshow', 'button search', "&nbsp;Show&nbsp;&nbsp;", array("click" => "ShowPageSwapData();") );?>
                                <?php echo form()->button('btnshow', 'button clear', "Reset&nbsp;&nbsp;", array("click" => "new ClearPageSwapData();"));?>
                            </div>
                        </div>
                    </div>
                </form>

            </fieldset>

            <fieldset class="corner" style="border-radius:3px;margin-top:10px;">
                <legend class="edit-users-x"><span style="margin-left:8px;">User Action</span></legend>
                <form name="frmSwapActionData">
                    <div class="ui-widget-form-table-compact">

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap Type</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->checkbox('swap_type', null, 1, array("change" =>"Ext.Cmp('swap_type').oneChecked(this);ActionCheckSwap(this);"));?><span>Amount</span>
                                &nbsp;
                                <?php echo form()->checkbox('swap_type', null, 2,array("change" =>"Ext.Cmp('swap_type').oneChecked(this);ActionCheckSwap(this);"));?><span>Checked</span>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap Data Size</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->input('swp_total_data', 'input_text long');?></div>
                        </div>


                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap to level</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui-swp-user-lavel">
                                <?php echo form()->combo('swp_to_user_level', 'select long',Level(), null, array("change" => "new ShowUserByLevel(this);") );?>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap to Supervisor</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui-swp-user-spv">
                                <?php echo form()->combo('swp_to_user_spv', 'select long');?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap to Senior Leader</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui-swp-senior-leader">
                                <?php echo form()->combo('swp_to_senior_leader', 'select long');?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap to Leader</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui-swp-user-leader">
                                <?php echo form()->combo('swp_to_user_leader', 'select long');?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap to Deskoll</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left" id="ui-swp-user-deskoll">
                                <?php echo form()->combo('swp_to_user_deskoll', 'select long');?></div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap Amount</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->input('swp_to_data_amount', 'input_text long');?></div>
                        </div>


                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption">Swap Methode</div>
                            <div class="ui-widget-form-cell center">:</div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->checkbox('swap_methode', null,1,array("change" =>"Ext.Cmp('swap_methode').oneChecked(this);ActionCheckSwap(this);"));?><span>Random</span>
                                &nbsp;
                                <?php echo form()->checkbox('swap_methode', null,2,array("change" =>"Ext.Cmp('swap_methode').oneChecked(this);ActionCheckSwap(this);"), array("checked" => true));?><span>Urutan</span>
                            </div>
                        </div>

                        <div class="ui-widget-form-row">
                            <div class="ui-widget-form-cell text_caption"></div>
                            <div class="ui-widget-form-cell center"></div>
                            <div class="ui-widget-form-cell left">
                                <?php echo form()->button('btnshow', 'button assign', "Submit", array("click" => "new SubmitSwapData();") );?>
                                <?php echo form()->button('btnshow', 'button close', "&nbsp;Exit&nbsp;&nbsp;&nbsp;", array("click" => "new ExitSwapData();"));?>
                            </div>
                        </div>

                    </div>
                </form>

            </fieldset>
        </div>

    </div>
</div>