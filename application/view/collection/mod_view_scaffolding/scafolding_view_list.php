
<table width="100%" class="custom-grid" cellspacing="1">
<thead>
	<tr height="24"> 
	<th nowrap class="font-standars ui-corner-top ui-state-default first center" width="5%">&nbsp;#</th>	
	<th nowrap class="font-standars ui-corner-top ui-state-default middle center" width="5%">&nbsp;No</th>
	<?php foreach( $field_label as $keys => $labels ) : ?>
		<?php if(in_array($labels, array_keys($field_class) ) ) : ?>
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" width="5%">
			<span class="header_order <?php __($field_class[$labels]);?>" style="color:red;" onclick="JavaScript:Ext.EQuery.orderBy('<?php __($labels);?>');">&nbsp; <?php __($labels);?> </span></th>	
		<?php else : ?>	
		<th nowrap class="font-standars ui-corner-top ui-state-default middle center" width="5%">
			<span class="header_order" onclick="JavaScript:Ext.EQuery.orderBy('<?php __($labels);?>');">&nbsp; <?php __($labels);?></span></th>	

		<?php endif; ?>	
	<?php $pos++;
		endforeach; ?>
	</tr>
</thead>	
<tbody>
<?php
$no  = 1;
 foreach( $field_data as $nums => $rows ) { 
	$color = ($no%2!=0?'#FFFEEE':'#FFFFFF');
?>	
	<tr CLASS="onselect" bgcolor="<?php echo $color;?>">
	<td class="content-first"><input type="checkbox" value="<?php echo $rows[reset($field_label)]; ?>" name="chk_skill" id="chk_skill"></td>
	<td class="content-middle center"><?php __($nums);?></td>
	<?php foreach( $field_label as $keys => $labels ) : ?>
		<?php 
				$color_select= ( in_array( $labels, array_keys($field_class)) ? 'style="background-color:#FFFCCC;"':'' ); 
			
		?>
		<td class="content-middle" <?php __($color_select);?> nowrap><?php echo $rows[$labels]; ?></td>
	<?php endforeach; ?>
	</tr>	
</tbody>
	<?php
		$no++;
		};
	?>
</table>