<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SMSOutbox extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_SMSOutbox() 
{
	$this->load->model(array('M_SetCampaign'));
}

 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.deb_id");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId","INNER");
	$this->EUI_Page->_setJoin("serv_sms_outbox c"," a.deb_id=c.MasterId","INNER");
	$this->EUI_Page->_setJoin("serv_sms_status d","c.SmsStatus = d.StatusId","INNER", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	
	
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	//USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'outbox_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'outbox_cust_id', TRUE);	
	$this->EUI_Page->_setLikeCache('a.deb_name', 'outbox_cust_name', TRUE);
	$this->EUI_Page->_setLikeCache('c.Recipient', 'outbox_phone_num', TRUE);
	$this->EUI_Page->_setAndCache('c.SmsStatus', 'outbox_sms_status', TRUE);
	$this->EUI_Page->_setAndOrCache("c.SendDate>='".StartDate(_get_post('outbox_start_sentdate'))."' ", 'outbox_start_sentdate', true);
	$this->EUI_Page->_setAndOrCache("c.SendDate<='".EndDate(_get_post('outbox_end_sentdate'))."' ", 'outbox_end_sentdate', true);
	
	
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.deb_id AS CustomerId"=> array('CustomerId','ID','primary'),
		"e.CampaignDesc as CampaignDesc" => array("CampaignDesc","Produk"),
		"a.deb_acct_no AS CustomerNumber"=> array('CustomerNumber','Customer Id'),
		"a.deb_name AS CustomerName"=>array('CustomerName','Nama Pelanggan'),
		"c.Message AS Message"=>array('Message','Pesan'),
		"c.Recipient AS PhNumber"=>array('PhNumber','Nomor Telpon'),
		"c.SendDate as SendDate" => array('SendDate','Tanggal kirim'),
		"IF(c.SentDate IS NULL,'-',c.SentDate) AS SentDate"=>array('SentDate','Tanggal Terkirim'),
		"d.StatusCode as StatusDesc"=>array('StatusDesc','Status')
	));
	
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId","INNER");
	$this->EUI_Page->_setJoin("serv_sms_outbox c"," a.deb_id=c.MasterId","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign e"," a.deb_cmpaign_id =e.CampaignId","INNER");
	$this->EUI_Page->_setJoin("serv_sms_status d","c.SmsStatus = d.StatusId","INNER", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	
	
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	//USER_SUPERVISOR
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'outbox_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'outbox_cust_id', TRUE);	
	$this->EUI_Page->_setLikeCache('a.deb_name', 'outbox_cust_name', TRUE);
	$this->EUI_Page->_setLikeCache('c.Recipient', 'outbox_phone_num', TRUE);
	$this->EUI_Page->_setAndCache('c.SmsStatus', 'outbox_sms_status', TRUE);
	$this->EUI_Page->_setAndOrCache("c.SendDate>='".StartDate(_get_post('outbox_start_sentdate'))."' ", 'outbox_start_sentdate', true);
	$this->EUI_Page->_setAndOrCache("c.SendDate<='".EndDate(_get_post('outbox_end_sentdate'))."' ", 'outbox_end_sentdate', true);
	
	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	} else {
		$this->EUI_Page->_setOrderBy("c.SendDate","DESC");		
	}	
	// echo $this ->EUI_Page->_getCompiler();
	$this->EUI_Page->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 } 

 
/*
 * @ pack : get sms outbox by Debitur or customer Id 
 */ 
public function _get_sms_outbox( $MasterId = 0 )
{


  $recsource = array();

	$this->db->reset_select();
	$this->db->select('
		a.SmsId,a.RequestTs, a.Recipient, 
		a.SentDate, a.Message, b.StatusDesc , 
		c.id, c.full_name', FALSE);
	
	$this->db->from('serv_sms_outbox a');
	$this->db->join('serv_sms_status b ','a.SmsStatus=b.StatusId', 'left');
	$this->db->join('t_tx_agent c ',' a.UserCreate=c.UserId','left');
	$this->db->where('MasterId', $MasterId);
	$this->db->order_by('a.SmsId', 'DESC');
	
	
	$qry  = $this->db->get();
	if( $qry->num_rows() > 0 ) 
	{
		$recsource  = $qry->result_assoc();
	}
	
	return $recsource;
}
 

}

// END OF CLASS 



?>