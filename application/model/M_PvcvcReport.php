<?php

class M_PvcvcReport extends EUI_Model
{

	public function modul_random()
	{
		return array(
			"pvc" => "PVC",
			"vct" => "VC Temporary",
			"vcp" => "VC Permanent",
			// "pvc_category" => "PVC per Category",
			"vct_category" => "VC Temporary per Category",
			"vcp_category" => "VC Permanent per Category"
		);
	}

	public function getDataPVC(){
		$start_date = $this -> URI->_get_post('start_date_claim');
		$end_date = $this -> URI->_get_post('end_date_claim');
		
		// $period = new DatePeriod(
			 // new DateTime($start_date),
			 // new DateInterval('P1M'),
			 // new DateTime($end_date)
		// );
		
		$date1 = $start_date;
		$date2 = $end_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		// $subfinal = date("Y-m-d", strtotime("+0 month", $final));
		$subfinal = date('Y-m-01 06:00:00', strtotime($start_date));

		// $final = $start_date;
		$subtime = strtotime($final);
		$subfinal2 = date("Y-m-d", strtotime("+0 month", $subtime));

		// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
		// $awal = date('Y-01-01 23:00:00');
		$awal = date('Y-m-01 23:00:00', strtotime('0 months', strtotime($subfinal2)));
		$akhir = date('Y-m-t 23:00:00', strtotime('0 months', strtotime($subfinal2)));
		$awal_open = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
		$akhir_open = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($subfinal)));

		$this->db->reset_select();
		$this->db->select("	
		(select 
		SUM(addressexists)
		FROM (SELECT IF(a.deb_acct_no IS NULL,0,1) AS addressexists
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='NewPVC' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' 
		GROUP BY a.deb_id,a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode 

		UNION ALL
		SELECT IF(d.deb_acct_no IS NULL,0,1) AS addressexists
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='NewPVC' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' 
		GROUP BY d.deb_id, d.deb_acct_no, d.deb_name, d.deb_wo_amount, d.deb_resource, s.CallReasonDesc, vcrs.VCReasonCode
		) as abc
		) as NewPVC_REV,

		(
		select 
		SUM(balance) 

		FROM (
		SELECT a.deb_wo_amount AS balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='NewPVC' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' 
		GROUP BY a.deb_id,a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode 

		UNION ALL
		SELECT d.deb_wo_amount AS balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='NewPVC' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' 
		GROUP BY d.deb_id,d.deb_acct_no,d.deb_name,d.deb_wo_amount,d.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode 
		) as abc) as BalanceNewPVC2,
				
		(SELECT (
			SELECT SUM(data1)
			FROM (
			
			SELECT IF(a.deb_acct_no IS NULL, 0, 1) AS data1
			FROM t_gn_pvcvc_report_log rl
			INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewPVC') AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' 
			GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance UNION ALL
			SELECT IF(d.deb_acct_no IS NULL, 0, 1) AS data1
			FROM t_gn_pvcvc_report_log rl
			INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewPVC') AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' 
			GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance ) AS New2) AS NewVCT4 ) as OpenPVC,


			(SELECT (
				SELECT SUM(balance)
				FROM (
				
				SELECT a.deb_wo_amount AS balance
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType IN ('NewPVC') AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' 
				GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance UNION ALL
				SELECT d.deb_wo_amount AS balance
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType IN ('NewPVC') AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' 
				GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance ) AS New2) AS NewVCT4 ) as BalanceOpenPVC,



		(
			SELECT (OpenPVC +  IF(
				NewPVC_REV-suba.ExitPVC IS NULL, 0,NewPVC_REV-suba.ExitPVC
				))
			FROM t_gn_pvcvc_report suba
			WHERE suba.Date = a.Date) AS ClosingPVC,a.Date,
			a.NewPVC, a.ExitPVC,
			(select (suba.BalanceNewPVC - suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as BalanceOpenPVC2,
			a.BalanceNewPVC, a.BalanceExitPVC,
			
			(select ((ifnull((select (BalanceNewPVC2-BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+BalanceNewPVC2)-BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingPVC
			",false);
		$this->db->from("t_gn_pvcvc_report a");
		$this->db->where("a.Date = '".$final."'");
		$rs = $this->db->get();
		// echo "<pre>";
		// print_r($this->db->last_query());
		if( $rs->num_rows() > 0 ){
			$data[$i] = $rs->result_assoc();
		}
		// for($i=0;$i<=$diff;$i++){
		// 	$subtime = strtotime($final);
		// 	$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));
			
		// 	$this->db->reset_select();
		// 	$this->db->select("a.Date,
		// 		(select (suba.NewPVC+suba.ExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as OpenPVC,
		// 		a.NewPVC, a.ExitPVC,
				
		// 		(select ((ifnull((select (suba.NewPVC+suba.ExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+suba.NewPVC)-suba.ExitPVC) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingPVC,
				
		// 		(select (suba.BalanceNewPVC+suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as BalanceOpenPVC,
		// 		a.BalanceNewPVC, a.BalanceExitPVC,
				
		// 		(select ((ifnull((select (suba.BalanceNewPVC+suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+suba.BalanceNewPVC)-suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingPVC
		// 		",false);
		// 	$this->db->from("t_gn_pvcvc_report a");
		// 	$this->db->where("a.Date = '".$final."'");
		// 	$rs = $this->db->get();
		// 	if( $rs->num_rows() > 0 ){
		// 		$data[$i] = $rs->result_assoc();
		// 	}
		// 	$time = strtotime($final);
		// 	$final = date("M-Y", strtotime("+1 month", $time));
		// }
		
		return $data;
	}
	public function getDataPVC2()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;



		$this->db->reset_select();

		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.Balance,rl.UpdatedAt AS tgl_new_pvc FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			WHERE rl.ColumnType='NewPVC'
			and rl.UpdatedAt >='" . $tgl_awal . "'
			and rl.UpdatedAt <='" . $tgl_akhir . "'
			order BY rl.UpdatedAt DESC";

		// print_r($sql);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql)->result_array();
		return $data;
	}
	public function getDataPVCopenPVC()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		$date1 = $start_date;
		$date2 = $start_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		$data = array();
		
		for ($i = 0; $i <= $diff; $i++) {
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

			// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			// $awal = date('Y-01-01 23:00:00');
			$awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			$akhir = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($subfinal)));

			$this->db->reset_select();
			$sql2="SELECT rl.ColumnType,a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
			 from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur a on a.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewPVC') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			
			GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance
			UNION ALL
			SELECT  rl.ColumnType,d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource
			from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewPVC') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			
			GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance
			";

			$sql = "SELECT a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource FROM t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
			WHERE  rl.ColumnType IN ('NewPVC','ExitPVC')
			and rl.UpdatedAt >='" . $awal . "'
			and rl.UpdatedAt <='" . $akhir . "'
			order BY rl.UpdatedAt DESC";
			$rs = $this->db->query($sql2);
			// print_r($this->db->Last_query());
			if ($rs->num_rows() > 0) {
				$data[$final] = $rs->result_assoc();
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		return $data;
	}
	public function getDataPVCopenPVC2()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		$date1 = $start_date;
		$date2 = $start_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		$data = array();
		
		for ($i = 0; $i <= $diff; $i++) {
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

			// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			// $awal = date('Y-01-01 23:00:00');
			$awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			$akhir = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($subfinal)));

			$this->db->reset_select();
			$sql2="SELECT rl.ColumnType,a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
			 from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur a on a.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('ExitPVC') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			
			GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance,rl.UpdatedAt
			UNION ALL
			SELECT  rl.ColumnType,d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource
			from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('ExitPVC') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			
			GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance,rl.UpdatedAt
			";

			$sql = "SELECT rl.DebiturId as deb_id, rl.Balance as deb_wo_amount,rl.ColumnType,rl.UpdatedAt
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
			WHERE  rl.ColumnType IN ('ExitPVC')
			and rl.UpdatedAt >='" . $awal . "'
			and rl.UpdatedAt <='" . $akhir . "'
			order BY rl.UpdatedAt DESC";
			$rs = $this->db->query($sql2);
			// var_dump($this->db->Last_query());
			if ($rs->num_rows() > 0) {
				$data[$final] = $rs->result_assoc();
				
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		// echo "<pre>";
		// var_dump($data);
		return $data;
	}
	public function getDataNewPVC()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));

		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;
		$this->db->reset_select();
		$sql2="SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
		a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
		 from t_gn_pvcvc_report_log rl 
		inner join t_gn_debitur a on a.deb_id=rl.DebiturId
		left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='NewPVC' and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
		
		GROUP BY a.deb_id,a.deb_acct_no,a.deb_name, s.CallReasonDesc, 
		a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
		UNION ALL
		SELECT  d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
		d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource
		from t_gn_pvcvc_report_log rl 
		inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
		left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='NewPVC' and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
		
		GROUP BY d.deb_id,d.deb_acct_no,d.deb_name, s.CallReasonDesc, d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource 
		";

		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,a.deb_wo_amount,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc
		 FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			inner JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
			WHERE rl.ColumnType='NewPVC'
			and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
			order BY rl.UpdatedAt DESC";

		// print_r($sql2);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql2)->result_array();
		return $data;
	}
	public function getDataExitPVC()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;
		$this->db->reset_select();
		$sql2="SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
				a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
				from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur a on a.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='ExitPVC' and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
				#AND vcrs.AccountStatusCode=115
				GROUP BY rl.Id
				UNION ALL
				SELECT  d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
				d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource
				from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='ExitPVC' and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
				#AND vcrs.AccountStatusCode=115
				GROUP BY rl.Id
				";
		
		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,a.deb_wo_amount,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			inner JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
			WHERE rl.ColumnType='ExitPVC'
			and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
			order BY rl.UpdatedAt DESC";

		// print_r($sql);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql2)->result_array();
		return $data;
	}

	public function getDataVCT()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		// $period = new DatePeriod(
		// new DateTime($start_date),
		// new DateInterval('P1M'),
		// new DateTime($end_date)
		// );

		$date1 = $start_date;
		$date2 = $end_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		// $subfinal = date("Y-m-d", strtotime("+0 month", $final));
		$subfinal = date('Y-m-01 06:00:00', strtotime($start_date));
		

		// $final = $start_date;
		$subtime = strtotime($final);
		$subfinal2 = date("Y-m-d", strtotime("+0 month", $subtime));

		// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
		// $awal = date('Y-01-01 23:00:00');
		$awal = date('Y-m-01 23:00:00', strtotime('0 months', strtotime($subfinal2)));
		$akhir = date('Y-m-t 23:00:00', strtotime('0 months', strtotime($subfinal2)));
		$awal_open = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
		$akhir_open = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($subfinal)));
		

		$this->db->reset_select();
		$this->db->select(
			"(
				select SUM(data1) from 
				(SELECT   IF(a.deb_acct_no IS NULL,0,1) AS data1
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCT' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=116
				GROUP BY a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode UNION ALL
				SELECT  IF(d.deb_acct_no IS NULL,0,1) AS data1
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCT' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=116
				GROUP BY d.deb_acct_no,d.deb_name,d.deb_wo_amount,d.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode) as New2
				
				) as NewVCT2,
				(
				select SUM(balance) 
				
				from (
				SELECT a.deb_wo_amount AS balance
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCT' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=116
				GROUP BY a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode UNION ALL
				SELECT d.deb_wo_amount as amount
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCT' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=116
				GROUP BY d.deb_acct_no,d.deb_name,d.deb_wo_amount,d.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode
				) as amount
				) as BalanceNewVCT2,	
				( SELECT SUM(data1)
					FROM (
					SELECT IF(a.deb_acct_no IS NULL, 0, 1) AS data1
					FROM t_gn_pvcvc_report_log rl
					INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
					LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
					LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
					LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
					WHERE rl.ColumnType IN ('NewVCT','ExitVCT') AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' AND vcrs.AccountStatusCode=116
					GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance
					UNION ALL
					SELECT IF(d.deb_acct_no IS NULL, 0, 1) AS data1
					FROM t_gn_pvcvc_report_log rl
					INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
					LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
					LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
					LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
					WHERE rl.ColumnType IN ('NewVCT','ExitVCT') 
					AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' AND vcrs.AccountStatusCode=116
					GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance) AS New2 
				) as  OpenVCT,

				( SELECT SUM(balance)
					FROM (
					SELECT a.deb_wo_amount AS balance
					FROM t_gn_pvcvc_report_log rl
					INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
					LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
					LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=a.deb_id
					LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
					WHERE rl.ColumnType IN ('NewVCT','ExitVCT') AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' AND vcrs.AccountStatusCode=116
					GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance
					UNION ALL
					SELECT d.deb_wo_amount AS balance
					FROM t_gn_pvcvc_report_log rl
					INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
					LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
					LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=d.deb_id
					LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
					WHERE rl.ColumnType IN ('NewVCT','ExitVCT') 
					AND rl.UpdatedAt >='" . $awal_open . "' AND rl.UpdatedAt <='" . $akhir_open . "' AND vcrs.AccountStatusCode=116
					GROUP BY rl.DebiturId,rl.ColumnType,rl.Balance) AS New2 
				) as  BalanceOpenVCT,
				

			(
				SELECT ( OpenVCT + IF(
					NewVCT2-suba.ExitVCT IS NULL, 0,NewVCT2-suba.ExitVCT
					))
				FROM t_gn_pvcvc_report suba
				WHERE suba.Date = a.Date) AS ClosingVCT,
			a.Date,
			
			a.NewVCT, a.ExitVCT,
			
			(select ((ifnull((select (suba.NewVCT+suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')),null)+suba.NewVCT)-suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingVCT_SALAH,
			
			(select (suba.BalanceNewVCT - suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')) as BalanceOpenVCT2,
			a.BalanceNewVCT, a.BalanceExitVCT,
			
			(select ((ifnull((select (BalanceNewVCT2-BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')),null)+BalanceNewVCT2)-BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingVCT",
			false
		);
		$this->db->from("t_gn_pvcvc_report a");
		$this->db->where("a.Date = '" . $final . "'");
		$rs = $this->db->get();
		// echo "<pre>";
		// print_r($this->db->last_query());
		if ($rs->num_rows() > 0) {
			$data[$i] = $rs->result_assoc();
		}

		// for ($i = 0; $i <= $diff; $i++) {
		// 	$subtime = strtotime($final);
		// 	$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

		// 	$this->db->reset_select();
		// 	$this->db->select(
		// 		"a.Date,
		// 		(select (suba.NewVCT+suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')) as OpenVCT,
		// 		a.NewVCT, a.ExitVCT,
				
		// 		(select ((ifnull((select (suba.NewVCT+suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')),null)+suba.NewVCT)-suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingVCT,
				
		// 		(select (suba.BalanceNewVCT+suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')) as BalanceOpenVCT,
		// 		a.BalanceNewVCT, a.BalanceExitVCT,
				
		// 		(select ((ifnull((select (suba.BalanceNewVCT+suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')),null)+suba.BalanceNewVCT)-suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingVCT",
		// 		false
		// 	);
		// 	$this->db->from("t_gn_pvcvc_report a");
		// 	$this->db->where("a.Date = '" . $final . "'");
		// 	$rs = $this->db->get();
		// 	if ($rs->num_rows() > 0) {
		// 		$data[$i] = $rs->result_assoc();
		// 	}

		// 	$time = strtotime($final);
		// 	$final = date("M-Y", strtotime("+1 month", $time));
		// }

		return $data;
	}
	public function getDataVCT2()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));

		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc AS tanya_bu_mini,
		a.deb_bal_afterpay, a.deb_wo_amount,a.deb_resource
		 FROM 
		t_gn_debitur a
		inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
		WHERE rl.ColumnType='NewVCT'
		and rl.UpdatedAt >='" . $tgl_awal . "'
		and rl.UpdatedAt <='" . $tgl_akhir . "'
		order BY rl.UpdatedAt DESC";


		$data = $this->db->query($sql)->result_array();
		// var_dump($data);
		return $data;
	}
	public function getDataVCTopenVCT()
	{
		
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		$date1 = $start_date;
		$date2 = $start_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		$data = array();

		for ($i = 0; $i <= $diff; $i++) {
			// $final = $start_date;
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

			// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			// $awal = date('Y-01-01 23:00:00');
			$awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			$akhir = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($subfinal)));

			$this->db->reset_select();
			$sql2="SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource, vcrs.VCReasonCode
			 from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur a on a.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			left join t_gn_vc_review vcrw on vcrw.debitur_id=a.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewVCT','ExitVCT') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			and vcrs.AccountStatusCode=116
			group by rl.DebiturId,rl.ColumnType,rl.Balance
			UNION ALL
			SELECT  d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource, vcrs.VCReasonCode
			from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			left join t_gn_vc_review vcrw on vcrw.debitur_id=d.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewVCT','ExitVCT') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			and vcrs.AccountStatusCode=116
			group by rl.DebiturId,rl.ColumnType,rl.Balance
			";

			$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
		FROM 
				   t_gn_debitur a
				   inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
				   inner JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
				   
				   WHERE  rl.ColumnType IN ('NewVCT','ExitVCT')
				   and rl.UpdatedAt >='" . $awal . "'
					and rl.UpdatedAt <='" . $akhir . "'
				  	order BY rl.UpdatedAt DESC";
			$rs = $this->db->query($sql2);
			// print_r($sql2);
			if ($rs->num_rows() > 0) {
				$data[$final] = $rs->result_assoc();
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		return $data;
	}
	public function getDataNewVCT()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;
		$this->db->reset_select();
		$sql2="SELECT a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc, vcrs.VCReasonCode
		from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur a on a.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			left JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
			left JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='NewVCT' 
			and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
			and vcrs.AccountStatusCode=116
			GROUP BY a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,s.CallReasonDesc, vcrs.VCReasonCode  
			UNION ALL
			SELECT d.deb_acct_no,d.deb_name,d.deb_wo_amount,d.deb_resource ,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc, vcrs.VCReasonCode
			from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			left JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
			left JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='NewVCT' 
			and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
			and vcrs.AccountStatusCode=116
			GROUP BY rl.Id ";

		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource, vcrs.VCReasonCode FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			left join t_gn_vc_review vcrw on vcrw.debitur_id=a.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='NewVCT'
			and rl.UpdatedAt >='" . $tgl_awal . "'
			and rl.UpdatedAt <='" . $tgl_akhir . "'
			group by a.deb_id
			order BY rl.UpdatedAt DESC";

		// print_r($sql2);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql2)->result_array();
		return $data;
	}
	public function getDataExitVCT()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;
		$this->db->reset_select();
		$sql2="SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
				a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
				from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur a on a.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				left join t_gn_vc_review vcrw on vcrw.debitur_id=a.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='ExitVCT' and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
				and vcrs.AccountStatusCode=116 group by rl.Id
				UNION ALL
				SELECT  d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
				d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource
				from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				left join t_gn_vc_review vcrw on vcrw.debitur_id=d.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='ExitVCT' and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
				and vcrs.AccountStatusCode=116 group by rl.Id";
		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource, vcrs.VCReasonCode FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			inner JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			left join t_gn_vc_review vcrw on vcrw.debitur_id=a.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='ExitVCT'
			and rl.UpdatedAt >='" . $tgl_awal . "'
			and rl.UpdatedAt <='" . $tgl_akhir . "'
			group by a.deb_id
			order BY rl.UpdatedAt DESC";

		// print_r($sql);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql2)->result_array();
		return $data;
	}




	public function getDataVCP()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		// $period = new DatePeriod(
		// new DateTime($start_date),
		// new DateInterval('P1M'),
		// new DateTime($end_date)
		// );

		$date1 = $start_date;
		$date2 = $end_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		$subfinal = date("Y-m-d", strtotime("+0 month", $final));
		$awal = date('Y-m-01 06:00:00', strtotime($start_date));
		$mundurAwal = date('M-Y', strtotime('-1 months', strtotime($start_date)));
		$mundurAwalBulan = date('Y-m-01 06:00:00', strtotime('-1 months', strtotime($start_date)));
		// $awal = date('Y-01-01 23:00:00');
		$subtime = strtotime($final);
		$subfinal2 = date("Y-m-d", strtotime("+0 month", $subtime));

		// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
		// $awal = date('Y-01-01 23:00:00');
		$awal = date('Y-m-01 23:00:00', strtotime('0 months', strtotime($subfinal2)));
		$akhir = date('Y-m-t 23:00:00', strtotime('0 months', strtotime($subfinal2)));

		// var_dump($mundurAwalBulan);
		$this->db->reset_select();
		$this->db->select("
		(
			SELECT SUM(CVPC)
			FROM 
			(
				SELECT IF(a.deb_acct_no IS NULL,0,1) as CVPC
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id UNION ALL
				SELECT IF(d.deb_acct_no IS NULL,0,1) as CVPC
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id 
			) AS CountVCP
			) as NewVCP2,
			
			(
			select SUM(balance)
			from 
			(
			SELECT a.deb_wo_amount as balance
			
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id UNION ALL
				SELECT d.deb_wo_amount as balance
				
				FROM t_gn_pvcvc_report_log rl
				INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
				LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' AND rl.UpdatedAt >='" . $awal . "' AND rl.UpdatedAt <='" . $akhir . "' AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id
			) as SUMVCP
			) as BalanceNewVCP2,
			(select (suba.NewVCP+suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $awal . "' - interval 1 month, '%b-%Y')) as OpenVCP,
			(SELECT (OpenVCP +  IF(
				NewVCP2-suba.ExitVCP IS NULL, 0,1
				) )
				FROM t_gn_pvcvc_report suba
				WHERE suba.Date = a.Date) AS ClosingVCP,
		
			a.Date,a.NewVCP, a.ExitVCP,
			
			
			
			(select (suba.BalanceNewVCP - suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $awal . "' - interval 1 month, '%b-%Y')) as BalanceOpenVCP,
			a.BalanceNewVCP, a.BalanceExitVCP,
			(select ((ifnull((select (suba.BalanceNewVCP-suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $awal . "' - interval 1 month, '%b-%Y')),0)+suba.BalanceNewVCP)-suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingVCP
			", false);
		$this->db->from("t_gn_pvcvc_report a");
		$this->db->where("a.Date = '" . $final . "'");
		$rs = $this->db->get();
		// echo "<pre>";
		// print_r($this->db->last_query());

		if ($rs->num_rows() > 0) {
			$data[$i] = $rs->result_assoc();
		}

		// for ($i = 0; $i <= $diff; $i++) {
		// 	$subtime = strtotime($final);
		// 	$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

		// 	$this->db->reset_select();
		// 	$this->db->select("a.Date,
		// 		(select (suba.NewVCP+suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')) as OpenVCP,
		// 		a.NewVCP, a.ExitVCP,
				
		// 		(select ((ifnull((select (suba.NewVCP+suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')),0)+suba.NewVCP)-suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingVCP,
				
		// 		(select (suba.BalanceNewVCP+suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')) as BalanceOpenVCP,
		// 		a.BalanceNewVCP, a.BalanceExitVCP,
		// 		(select ((ifnull((select (suba.BalanceNewVCP+suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('" . $subfinal . "' - interval 1 month, '%b-%Y')),0)+suba.BalanceNewVCP)-suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingVCP
		// 		", false);
		// 	$this->db->from("t_gn_pvcvc_report a");
		// 	$this->db->where("a.Date = '" . $final . "'");
		// 	$rs = $this->db->get();

		// 	if ($rs->num_rows() > 0) {
		// 		$data[$i] = $rs->result_assoc();
		// 	}

		// 	$time = strtotime($final);
		// 	$final = date("M-Y", strtotime("+1 month", $time));
		// }

		return $data;
	}

	public function getDataVCP2()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;

		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc AS tanya_bu_mini,
		a.deb_bal_afterpay, a.deb_wo_amount,a.deb_resource
		 FROM 
		t_gn_debitur a
		inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
		WHERE rl.ColumnType='NewVCP'
		and rl.UpdatedAt >='" . $tgl_awal . "'
		and rl.UpdatedAt <='" . $tgl_akhir . "'
		order BY rl.UpdatedAt DESC";

		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql)->result_array();
		// var_dump($data);
		return $data;
	}

	public function getDataVCPopenVCP()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');

		$date1 = $start_date;
		$date2 = $start_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		$data = array();

		for ($i = 0; $i <= $diff; $i++) {
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

			// $awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			// $awal = date('Y-01-01 23:00:00');
			$awal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($subfinal)));
			$akhir = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($subfinal)));

			$this->db->reset_select();
			$sql2="SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource, vcrs.VCReasonCode
			 from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur a on a.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewVCP','ExitVCP') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			AND vcrs.AccountStatusCode=117
			GROUP BY rl.Id
			UNION ALL
			SELECT  d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
			d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource, vcrs.VCReasonCode
			from t_gn_pvcvc_report_log rl 
			inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
			left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType IN ('NewVCP','ExitVCP') and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
			AND vcrs.AccountStatusCode=117
			GROUP BY rl.Id
			";

			$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
		FROM 
				   t_gn_debitur a
				   inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
				   LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code 
				   WHERE  rl.ColumnType IN ('NewVCP','ExitVCP')
				   and rl.UpdatedAt >='" . $awal . "' and rl.UpdatedAt <='" . $akhir . "'
				  	order BY rl.UpdatedAt DESC";
			$rs = $this->db->query($sql2);
			// print_r($sql2);
			if ($rs->num_rows() > 0) {
				$data[$final] = $rs->result_assoc();
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		return $data;
	}
	
	public function getDataNewVCP()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;
		$this->db->reset_select();
		$sql2="SELECT a.deb_acct_no,a.deb_name,a.deb_wo_amount,a.deb_resource,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc, vcrs.VCReasonCode
					from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur a on a.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				left JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				left JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' 
				and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
				AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id 
				UNION ALL
				SELECT d.deb_acct_no,d.deb_name,d.deb_wo_amount,d.deb_resource ,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc, vcrs.VCReasonCode
				from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				left JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				left JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' 
				and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
				AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id ";


		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,a.deb_wo_amount,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc,a.deb_resource, vcrs.VCReasonCode FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			INNER JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			left join t_gn_vc_review vcrw on vcrw.debitur_id=a.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='NewVCP'
			and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
			group by a.deb_id
			order BY rl.UpdatedAt DESC";

		// print_r($sql2);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql2)->result_array();
		return $data;
	}
	public function getDataExitVCP()
	{
		$start_date = $this->URI->_get_post('start_date_claim');
		$end_date = $this->URI->_get_post('end_date_claim');
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));
		// $tgl_akhir_bulan=date('t');
		// echo $tgl_akhir;
		$this->db->reset_select();
		$sql2="SELECT a.deb_id,a.deb_acct_no,a.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
		a.deb_bal_afterpay,a.deb_wo_amount,a.deb_resource
		from t_gn_pvcvc_report_log rl 
		inner join t_gn_debitur a on a.deb_id=rl.DebiturId
		left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='ExitVCP' and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
		AND vcrs.AccountStatusCode=117
		GROUP BY rl.Id 
		UNION ALL
		SELECT  d.deb_id,d.deb_acct_no,d.deb_name,rl.UpdatedAt AS tgl_new_pvc, s.CallReasonDesc,
		d.deb_bal_afterpay,d.deb_wo_amount,d.deb_resource
		from t_gn_pvcvc_report_log rl 
		inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
		left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType='ExitVCP' and rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "'
		AND vcrs.AccountStatusCode=117
		GROUP BY rl.Id 
		";

		$sql = "SELECT a.deb_id,a.deb_acct_no,a.deb_name,a.deb_wo_amount,rl.UpdatedAt AS tgl_new_pvc,s.CallReasonDesc,a.deb_resource, vcrs.VCReasonCode FROM 
			t_gn_debitur a
			inner JOIN t_gn_pvcvc_report_log rl ON a.deb_id=rl.DebiturId 
			INNER JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			left join t_gn_vc_review vcrw on vcrw.debitur_id=a.deb_id
			left JOIN t_lk_vc_reason vcrs on vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='ExitVCP'
			and rl.UpdatedAt >='" . $tgl_awal . "'
			and rl.UpdatedAt <='" . $tgl_akhir . "'
			group by a.deb_id
			order BY rl.UpdatedAt DESC";

		// print_r($sql);
		// $rs = $this->db->get($sql);
		$data = $this->db->query($sql2)->result_array();
		return $data;
	}

	public function getDataVCTPerCategory()
	{
		$start_date	= date("Y-m-d", strtotime($this->URI->_get_post('start_date_claim')));
		$end_date	= date("Y-m-t", strtotime($this->URI->_get_post('end_date_claim')));
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));		
		// $mundurAwal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($start_date)));
		// $mundurAkhir = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($start_date)));
		$mundurAwal = date('Y-m-01 23:00:00', strtotime($start_date));
		$mundurAkhir = date('Y-m-t 23:00:00', strtotime($start_date));

	


		// echo $start_date."-".$end_date; exit();

		// $subtime = strtotime($start_date);
		// $subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

		$this->db->reset_select();
		// $this->db->select("a.vc_reason_id, b.VCReasonDesc, b.VCReasonCode, count(distinct a.debitur_id) as 'Account', sum(a.wo_bal) as Balance", false);
		// $this->db->from("t_gn_vc_review a");
		// $this->db->join("t_lk_vc_reason b", "a.vc_reason_id = b.Id", "LEFT");
		// $this->db->where("a.create_date >= '" . $start_date . "'");
		// $this->db->where("a.create_date <= '" . $end_date . "'");
		// $this->db->where("b.AccountStatusCode", 116);
		// $this->db->group_by("a.vc_reason_id");
		// echo $this->db->_get_var_dump();
		// $rs = $this->db->get();
	
		$sql="SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, COUNT(DISTINCT vcrw.debitur_id) AS 'Account', a.deb_wo_amount AS Balance
			FROM t_gn_pvcvc_report_log rl
			INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='NewVCT' AND rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "' AND vcrs.AccountStatusCode=116
			GROUP BY a.deb_id UNION ALL
			SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, COUNT(DISTINCT vcrw.debitur_id) AS 'Account', d.deb_wo_amount AS Balance
			FROM t_gn_pvcvc_report_log rl
			INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
			LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
			LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
			LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
			WHERE rl.ColumnType='NewVCT' AND rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "' AND vcrs.AccountStatusCode=116
			GROUP BY rl.Id";
		
		$sql2="SELECT 
		VCReasonDesc,
		DebiturId,
		vc_reason_id,
		VCReasonCode,
		Account as 'Account',
		sum(Balance) as Balance
		from 
		(SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account', 
		a.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCT','ExitVCT') 
		AND rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "' AND vcrs.AccountStatusCode=116
		GROUP BY rl.DebiturId
		ORDER BY a.deb_id DESC) AS table1 
		UNION ALL
		SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
		 d.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCT','ExitVCT') AND rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $mundurAkhir . "' AND vcrs.AccountStatusCode=116
		GROUP BY rl.DebiturId
		ORDER BY d.deb_id DESC) AS tabel2
		
		UNION ALL
		SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
		a.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCT','ExitVCT') AND rl.UpdatedAt >='" . $mundurAwal . "'	and rl.UpdatedAt <='" . $mundurAkhir . "' AND vcrs.AccountStatusCode=116
		GROUP BY rl.DebiturId
		ORDER BY a.deb_id DESC) AS datakemarin1
		UNION ALL
		SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
		d.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCT','ExitVCT') AND rl.UpdatedAt >='" . $mundurAwal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "' AND vcrs.AccountStatusCode=116
		GROUP BY rl.DebiturId
		ORDER BY d.deb_id DESC) AS datakemarin2
		)all_union
		group by VCReasonDesc,
		DebiturId,
		vc_reason_id,
		VCReasonCode";
		
		
		$rs = $this->db->query($sql);
				// print_r($sql);
				
		foreach ($rs->result_assoc() as $rows) {

			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Account'] += $rows['Account'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Balance'] += $rows['Balance'];

			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Account'] += $rows['Account'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Balance'] += $rows['Balance'];
		}
		// echo "<pre>";
		// print_r($data);

		return $data;
	}

	public function getDataVCPPerCategory()
	{
		$start_date	= date("Y-m-d", strtotime($this->URI->_get_post('start_date_claim')));
		$end_date	= date("Y-m-t", strtotime($this->URI->_get_post('end_date_claim')));
		$tgl_awal = date('Y-m-01 06:00:00', strtotime($start_date));
		// $tgl_akhir = date('Y-m-t 23:00:00', strtotime($end_date));
		$tgl_akhir = date('Y-m-t 23:00:00', strtotime($start_date));
		
		// $mundurAwal = date('Y-m-01 23:00:00', strtotime('-1 months', strtotime($start_date)));
		// $mundurAkhir = date('Y-m-t 23:00:00', strtotime('-1 months', strtotime($start_date)));
		$mundurAwal = date('Y-m-01 23:00:00', strtotime($start_date));
		$mundurAkhir = date('Y-m-t 23:00:00', strtotime($start_date));

		// echo $start_date."-".$end_date; exit();

		// $subtime = strtotime($start_date);
		// $subfinal = date("Y-m-d", strtotime("+0 month", $subtime));

		// $this->db->reset_select();
		// $this->db->select("a.vc_reason_id, b.VCReasonDesc, b.VCReasonCode, count(distinct a.debitur_id) as 'Account', sum(a.wo_bal) as Balance", false);
		// $this->db->from("t_gn_vc_review a");
		// $this->db->join("t_lk_vc_reason b", "a.vc_reason_id = b.Id", "LEFT");
		// $this->db->where("a.create_date >= '" . $start_date . "'");
		// $this->db->where("a.create_date <= '" . $end_date . "'");
		// $this->db->where("b.AccountStatusCode", 117);
		// $this->db->group_by("a.vc_reason_id");

		// // echo $this->db->_get_var_dump();
		// $rs = $this->db->get();
			// echo "test2";
		$sql="SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
					COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
					a.deb_wo_amount AS Balance
					from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur a on a.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
				left JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				left JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' 
				and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
				AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id 
				UNION ALL
				SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
				COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
				d.deb_wo_amount AS Balance
				from t_gn_pvcvc_report_log rl 
				inner join t_gn_debitur_deleted d on d.deb_id=rl.DebiturId
				left JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
				left JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
				left JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
				WHERE rl.ColumnType='NewVCP' 
				and rl.UpdatedAt >='" . $tgl_awal . "' and rl.UpdatedAt <='" . $tgl_akhir . "'
				AND vcrs.AccountStatusCode=117
				GROUP BY rl.Id ";

		
		
		$sql2="SELECT 
		VCReasonDesc,
		DebiturId,
		vc_reason_id,
		VCReasonCode,
		Account as 'Account',
		sum(Balance) as Balance
		from 
		(SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account', 
		a.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCP','ExitVCP')  
		AND rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "' AND vcrs.AccountStatusCode=117
		GROUP BY rl.DebiturId
		ORDER BY a.deb_id DESC) AS table1 
		UNION ALL
		SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
		 d.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCP','ExitVCP')  AND rl.UpdatedAt >='" . $tgl_awal . "'	and rl.UpdatedAt <='" . $mundurAkhir . "' AND vcrs.AccountStatusCode=117
		GROUP BY rl.DebiturId
		ORDER BY d.deb_id DESC) AS tabel2
		
		UNION ALL
		SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
		a.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur a ON a.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=a.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCP','ExitVCP')  AND rl.UpdatedAt >='" . $mundurAwal . "'	and rl.UpdatedAt <='" . $mundurAkhir . "' AND vcrs.AccountStatusCode=117
		GROUP BY rl.DebiturId
		ORDER BY a.deb_id DESC) AS datakemarin1
		UNION ALL
		SELECT *
		FROM (
		SELECT vcrs.VCReasonDesc,rl.DebiturId,vcrw.vc_reason_id,vcrs.VCReasonCode, 
		COUNT(DISTINCT vcrw.debitur_id) AS 'Account',
		d.deb_wo_amount AS Balance
		FROM t_gn_pvcvc_report_log rl
		INNER JOIN t_gn_debitur_deleted d ON d.deb_id=rl.DebiturId
		LEFT JOIN t_lk_account_status s ON s.CallReasonCode=d.deb_call_status_code
		LEFT JOIN t_gn_vc_review vcrw ON vcrw.debitur_id=rl.DebiturId
		LEFT JOIN t_lk_vc_reason vcrs ON vcrs.Id = vcrw.vc_reason_id
		WHERE rl.ColumnType IN ('NewVCP','ExitVCP')  AND rl.UpdatedAt >='" . $mundurAwal . "'	and rl.UpdatedAt <='" . $tgl_akhir . "' AND vcrs.AccountStatusCode=117
		GROUP BY rl.DebiturId
		ORDER BY d.deb_id DESC) AS datakemarin2
		)all_union
		group by VCReasonDesc,
		DebiturId,
		vc_reason_id,
		VCReasonCode";
		$rs = $this->db->query($sql);
		// print_r($sql);
		// echo '<pre>';
		// print_r($rs->result_assoc());
		// die;
		$arrData = array();
		$account = 0;
		$balance = 0;
		foreach ($rs->result_assoc() as $item) {
			if($arrData[$item['vc_reason_id']]['vc_reason_id'] == $item['vc_reason_id']) {
				$account = $arrData[$item['vc_reason_id']]['Account'];
				$balance = $arrData[$item['vc_reason_id']]['Balance'];
				$account += $item['Account'];
				$balance += $item['Balance'];
			} else {
				$account = 0;
				$balance = 0;
				$account = $item['Account'];
				$balance = $item['Balance'];
			}
			$arrData[$item['vc_reason_id']] = array(
				'vc_reason_id' => $item['vc_reason_id'],
				'VCReasonDesc' => $item['VCReasonDesc'],
				'VCReasonCode' => $item['VCReasonCode'],
				'Account' => $account,
				'Balance' => $balance,
			);
		}
		foreach ($arrData as $rows) {
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Account'] += $rows['Account'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Balance'] += $rows['Balance'];

			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Account'] += $rows['Account'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Balance'] += $rows['Balance'];
		}
		
		return $data;
	}

	public function getVCReason($CallReasonCode)
	{
		$this->db->reset_select();
		$this->db->select("a.Id, a.VCReasonDesc, a.VCReasonCode", false);
		$this->db->from("t_lk_vc_reason a");
		$this->db->where("AccountStatusCode", $CallReasonCode);
		// echo $this->db->_get_var_dump();
		$rs = $this->db->get();

		foreach ($rs->result_assoc() as $rows) {
			$data[$rows['Id']]['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['Id']]['VCReasonCode'] = $rows['VCReasonCode'];
		}

		return $data;
	}

	public function getAccStatusVC($CallReasonDesc)
	{
		$reportType = array("vcp_category" => "VC PERMANENT", "vct_category" => "VC TEMPORARY");
		$this->db->reset_select();
		$this->db->select("a.CallReasonCode", false);
		$this->db->from("t_lk_account_status a");
		$this->db->where("a.CallReasonDesc", $reportType[$CallReasonDesc]);
		// echo $this->db->_get_var_dump();
		$rs = $this->db->get();

		foreach ($rs->result_assoc() as $rows) {
			$data = $rows['CallReasonCode'];
		}

		return $data;
	}






	public function get_claim_access_all()
	{
		/*EXPLAIN
SELECT a.claim_id,
b.deb_id,
b.acc_no,
c.deb_id,
c.deb_acct_no,
d.CampaignDesc,
f.id AS OldAgent,
a.claim_date_ts,
e.id AS AgentClaim,
a.approval_date_ts,
g.AproveName AS ApprovalStatus,
a.bucket_trx_id,
i.bucket_trx_id,
i.access_all_id,
j.*
FROM t_gn_claim_debitur a 
INNER JOIN t_gn_buckettrx_debitur b ON a.bucket_trx_id=b.bucket_trx_id
LEFT JOIN t_gn_debitur c ON b.deb_id=c.deb_id
LEFT JOIN t_gn_campaign d ON c.deb_cmpaign_id =d.CampaignId
LEFT JOIN t_tx_agent e ON a.claim_by=e.UserId
LEFT JOIN t_tx_agent f ON a.from_owner=f.UserId
LEFT JOIN t_lk_aprove_status g ON a.approval_status=g.AproveCode
INNER JOIN t_gn_modul_setup h ON b.modul_setup_id=h.modul_setup_id
INNER JOIN t_gn_access_all i ON a.bucket_trx_id = i.bucket_trx_id
LEFT JOIN t_st_access_all j ON i.st_access_all_id=j.st_access_all_id

WHERE a.claim_date_ts like '2016-04-19%'*/

		$data = array();
		$start_claim_date = null;
		$end_claim_date = null;
		$report_type = array(0);
		if ($this->URI->_get_have_post("report_type")) {
			$report_type = $this->URI->_get_post('report_type');
		}
		if (!$this->URI->_get_array_post()) {
			$start_claim_date = _getDateEnglish($this->URI->_get_post('start_date_claim'));
			$end_claim_date = _getDateEnglish($this->URI->_get_post('end_date_claim'));
			$this->db->reset_select();
			$this->db->select(
				"a.claim_id,
								b.deb_id,
								b.acc_no,
								c.deb_id,
								c.deb_acct_no,
								d.CampaignDesc,
								f.id AS OldAgent,
								a.claim_date_ts,
								e.id AS AgentClaim,
								a.approval_date_ts,
								g.AproveName AS ApprovalStatus,
								a.bucket_trx_id,
								i.bucket_trx_id,
								i.access_all_id"
			);
			$this->db->from("t_gn_claim_debitur a");
			$this->db->join("t_gn_buckettrx_debitur b ", "a.bucket_trx_id=b.bucket_trx_id", "INNER");
			$this->db->join("t_gn_debitur c", "b.deb_id=c.deb_id", "LEFT");
			$this->db->join("t_gn_campaign d", "c.deb_cmpaign_id =d.CampaignId", "LEFT");
			$this->db->join("t_tx_agent e", "a.claim_by=e.UserId", "LEFT");
			$this->db->join("t_tx_agent f", "a.from_owner=f.UserId", "LEFT");
			$this->db->join("t_lk_aprove_status g", "a.approval_status=g.AproveCode", "LEFT");
			$this->db->join("t_gn_modul_setup h", "b.modul_setup_id=h.modul_setup_id", "INNER");
			$this->db->join("t_gn_access_all i", "a.bucket_trx_id = i.bucket_trx_id", "INNER");
			$this->db->join("t_st_access_all j", "i.st_access_all_id=j.st_access_all_id", "LEFT");
			$this->db->where("a.claim_date_ts BETWEEN '" . $start_claim_date . "' AND '" . $end_claim_date . "'");
			$this->db->where_in("h.modul_id_rnd", $report_type);
			$rs = $this->db->get();
			// echo $this->db->last_query();
			if ($rs->num_rows() > 0) {
				$data = $rs->result_assoc();
			}
		}
		return $data;
	}
}
