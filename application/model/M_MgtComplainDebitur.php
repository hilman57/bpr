<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_MgtComplainDebitur extends EUI_Model
{
	/*
	 * @ def 		: __construct // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	 public function _get_default() 
	{
		$this->EUI_Page->_setPage(25); 
		$this->EUI_Page->_setSelect("a.id_complain_review");
		$this->EUI_Page->_setFrom("t_gn_complain_review a");
		$this->EUI_Page->_setJoin("t_gn_debitur b","a.debitur_id=b.deb_id","INNER");
		$this->EUI_Page->_setJoin("t_tx_agent c","a.userid=c.UserId ","INNER");
		$this->EUI_Page->_setJoin("t_lk_source_complain d ","a.id_source_complain=d.id_source_complain ","INNER");
		$this->EUI_Page->_setJoin("t_lk_complain_respon f ","a.id_respon_review=f.id_respon ","INNER");
		$this->EUI_Page->_setJoin("t_gn_campaign g ","b.deb_cmpaign_id = g.CampaignId ","INNER",TRUE);
		
		// @ pack : filter by panel ---------------------------------------------------------------------------------
 	
		$this->EUI_Page->_setAndCache('b.deb_acct_no', 'cpl_cust_id', TRUE);
		$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'cpl_campaign_id',TRUE);
		$this->EUI_Page->_setAndCache('a.userid', 'cpl_agent_id', TRUE);
		$this->EUI_Page->_setLikeCache('b.deb_name', 'cpl_cust_name', TRUE);
		$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets>='". _getDateEnglish(_get_post('cpl_start_date')) ."'", 'cpl_start_date', TRUE);
		$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets<='". _getDateEnglish(_get_post('cpl_end_date')) ."'", 'cpl_end_date', TRUE);
		
		//echo $this ->EUI_Page->_getCompiler();
		if($this->EUI_Page->_get_query()) {
			return $this->EUI_Page;
		}
	}
	
	/*
	 * @ def 		: __construct // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_content()
	{
		
		$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
		$this->EUI_Page->_setPage(25);
		/*
		** 
			SELECT a.id_complain_review AS IdComplain,g.CampaignDesc AS CampaignDesc,b.deb_acct_no AS AccountNumber,b.deb_name AS CustomerName,c.id AS deskol,d.source_complain,e.kasus_komplain,a.action_taken,a.result,a.PhoneBlock,f.nama_respon
			FROM (t_gn_complain_review a)
			INNER JOIN t_gn_debitur b ON a.debitur_id=b.deb_id
			INNER JOIN t_tx_agent c ON a.userid=c.UserId
			INNER JOIN t_lk_source_complain d ON a.id_source_complain=d.id_source_complain
			INNER JOIN t_lk_kasus_komplain e ON a.id_kasus_komplen=e.id_kasus_komplain
			INNER JOIN t_lk_complain_respon f ON a.id_respon_review=f.id_respon
			INNER JOIN t_gn_campaign g ON b.deb_cmpaign_id = g.CampaignId
			WHERE 1=1
			LIMIT 0, 25
		*/
		/** default of query ***/
		$this->EUI_Page->_setArraySelect(array(
			"a.id_complain_review AS IdComplain"=> array('IdComplain','ID','primary'),
			"a.debitur_id AS deb_Id"=> array('deb_Id','deb_Id','hidden'),
			"a.id_respon_review AS respon"=> array('respon','respon','hidden'),
			"g.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Product'),
			"b.deb_acct_no AS AccountNumber "=> array('AccountNumber','Pelanggan ID'),
			"b.deb_name AS CustomerName"=> array('CustomerName','Nama Pelanggan'),
			"c.id as deskol "=> array('deskol','Agent ID'),
			"d.source_complain"=> array('source_complain','Source Komplain'),
			"f.nama_respon"=> array('nama_respon','Respon'),
			"a.create_date as complain_date"=> array('complain_date','Tanggal Komplain')
		
		));
		
		$this->EUI_Page->_setFrom("t_gn_complain_review a");
		$this->EUI_Page->_setJoin("t_gn_debitur b","a.debitur_id=b.deb_id ","INNER");
		$this->EUI_Page->_setJoin("t_tx_agent c","a.userid=c.UserId ","INNER");
		$this->EUI_Page->_setJoin("t_lk_source_complain d ","a.id_source_complain=d.id_source_complain ","INNER");
		$this->EUI_Page->_setJoin("t_lk_complain_respon f ","a.id_respon_review=f.id_respon ","INNER");
		$this->EUI_Page->_setJoin("t_gn_campaign g ","b.deb_cmpaign_id = g.CampaignId ","INNER",TRUE);
		
		// @ pack : filter by panel ---------------------------------------------------------------------------------
 	
		$this->EUI_Page->_setAndCache('b.deb_acct_no', 'cpl_cust_id', TRUE);
		$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'cpl_campaign_id',TRUE);
		$this->EUI_Page->_setAndCache('a.userid', 'cpl_agent_id', TRUE);
		$this->EUI_Page->_setLikeCache('b.deb_name', 'cpl_cust_name', TRUE);
		$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets>='". _getDateEnglish(_get_post('cpl_start_date')) ."'", 'cpl_start_date', TRUE);
		$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets<='". _getDateEnglish(_get_post('cpl_end_date')) ."'", 'cpl_end_date', TRUE);
		
		
		/* set order by **/
		
		if( $this->URI->_get_have_post('order_by')) {
			$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
		}
		else
		{
			$this->EUI_Page->_setOrderBy("a.create_date","ASC");
		}
		
		$this->EUI_Page->_setLimit();
		// echo "<!-- ".$this->EUI_Page->_getCompiler()." -->";
		// echo $this->EUI_Page->_getCompiler();
		
	}
	
	/*
	 * @ def 		: _get_resource // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_resource()
	{
		self::_get_content();
		if(($this->EUI_Page->_get_query()!=="" )) 
		{
			return $this->EUI_Page->_result();
		}	
	}
	 
	/*
	 * @ def 		: _get_page_number // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_page_number() 
	{
		if( $this -> EUI_Page -> _get_query()!='' ) {
			return $this -> EUI_Page -> _getNo();
		}	
	}
	
	/*
	 * @ def 		: _get_resource // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _getCampaignByActive() 
	{
	 $_is_active = null;
	 if( is_null($_is_active)) {
		$_is_active = $this->M_SetCampaign->_get_campaign_name();
	 }
		
	 return $_is_active;
		
	}
	
	/*
	 * @ def : _getCustomerByOtherNumberCIF 
	 * --------------------------------------------------
	 */
	 
	 public function _getDeskollByLogin() 
	{
		$Deskoll = array();
		$_array_select = $this->M_SysUser->_get_user_by_login();
		foreach( $_array_select as $UserId => $rows ) 
		{
			$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
		}

		return $Deskoll;
	  
	}
	
	public function _getComplain($where=array())
	{
		$complain = array();
  
		$this->db->reset_select();
		$this->db->select("a.id_complain_review AS IdComplain,
			g.CampaignDesc AS CampaignDesc,
			b.deb_acct_no AS AccountNumber,
			b.deb_name AS CustomerName,
			b.deb_open_date,
			c.id AS deskol,
			a.id_source_complain as id_source_complain,
			d.source_complain,
			a.id_kasus_komplen as id_kasus_komplen,
			e.kasus_komplain,
			e.kasus_komplain as kasus_komplain2,
			a.action_taken,
			a.result,
			a.PhoneBlock,
			f.nama_respon,
			a.id_respon_review,
			a.call_status_code,
			a.account_status_code,
			a.closed_date,
			'' AS date_pull_out",
		FALSE);
		$this->db->from("t_gn_complain_review a");
		$this->db->join("t_gn_debitur b","a.debitur_id=b.deb_id ","INNER");
		$this->db->join("t_tx_agent c","a.userid=c.UserId ","INNER");
		$this->db->join("t_lk_source_complain d","a.id_source_complain=d.id_source_complain ","INNER");
		$this->db->join("t_lk_kasus_komplain e","a.id_kasus_komplen=e.id_kasus_komplain ","INNER");
		$this->db->join("t_lk_complain_respon f","a.id_respon_review=f.id_respon ","INNER");
		$this->db->join("t_gn_campaign g","b.deb_cmpaign_id = g.CampaignId ","INNER");
		
		if(isset($where['IdComplain']))
		{
			$this->db->where("a.id_complain_review",$where['IdComplain']);
		}
		if(isset($where['CampaignId']))
		{
			$this->db->where_in("b.deb_cmpaign_id",$where['CampaignId']);
		}
		if(isset($where['UserId']))
		{
			$this->db->where_in("a.userid",$where['UserId']);
		}
		if(isset($where['StartComplain']) && isset($where['EndComplain']))
		{
			$this->db->where("a.create_date >=",$where['StartComplain']." 00:00:00");
			$this->db->where("a.create_date <= ",$where['EndComplain']." 23:59:59");
		}
		
		$qry = $this->db->get();
		// echo $this->db->last_query();
		if( $qry->num_rows() > 0)
		{
			$complain = $qry->result_assoc();
		}
		return $complain;
	}
	
	public function _UpdateComplain()
	{
		$argv  =& $this->URI->_get_all_request();
		$updated = false;
		$review = 1;
		$reject = 3;
		$closed = 2;
		$unprogress = 0;
		// echo "<pre>";
		// print_r($argv);
		// echo "</pre>";
		if(isset($argv['ComplainId']))
		{
			if( $argv['respon'] == $review )
			{
				$unprogress = 1;
			}
			elseif( $argv['respon'] == $reject )
			{
				$this->UpdateDebComplain($argv['ComplainId']);
				$this->db->set("closed_date",date('Y-m-d H:i:s'));
			}
			elseif($argv['respon'] == $closed)
			{
				$this->db->set("closed_date",date('Y-m-d H:i:s'));
			}
			
			$this->db->set("id_source_complain",$argv['source_complain']);
			$this->db->set("id_kasus_komplen",$argv['jenis_kasus']);
			$this->db->set("action_taken",$argv['text_action_taken']);
			$this->db->set("result",$argv['text_result']);
			$this->db->set("id_respon_review",$argv['respon']);
			$this->db->set("respon_by",$this->EUI_Session->_get_session('UserId') );
			$this->db->set("is_progress",$unprogress);
			$this->db->where("id_complain_review",$argv['ComplainId']);
			$this->db->update("t_gn_complain_review");
			if($this->db->affected_rows()>0)
			{
				$updated = true;
			}
		}
		return $updated;
		
	}
	
	private function UpdateDebComplain($complain_id=0)
	{
		$complain = $this->_getComplain(array('IdComplain'=>$complain_id));
		// echo "<pre>";
		// print_r($complain);
		// echo "</pre>";
		foreach( $complain as $index => $cols )
		{
			$this->db->set('deb_call_status_code', $cols['account_status_code']);
			$this->db->set('deb_prev_call_status_code', $cols['call_status_code']);
			$this->db->where('deb_id',$cols['debitur_id']);
			if ( $this->db->update('t_gn_debitur') )
			{
				// $this->db->set("CustomerId",$rows['deb_id']);
				// $this->db->set("CreatedById",$rows['LastAgentId']);
				// $this->db->set("TeamLeaderId",$rows['LastTeamLeaderId']);
				// $this->db->set("SupervisorId",$rows['LastSupervisorId']);
				// $this->db->set("CallHistoryNotes","REJECT COMPLAIN BY -".strtoupper(_get_session('Username')));
				// $this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s'));
				// $this->db->insert("t_gn_callhistory");
			}	
		}
		
	}
	
	public function ComplainIsProgress($deb_id=0)
	{
		$progress = 1;
		$valid = false;
		$this->db->where('is_progress', $progress);
		$this->db->where('debitur_id', $deb_id);
		$this->db->from('t_gn_complain_review');
		if( $this->db->count_all_results() >= 1 )
		{
			$valid = true;
		}
		// echo $this->db->last_query();
		return $valid;
	}

}
//end class