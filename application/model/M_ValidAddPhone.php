<?php

	Class M_ValidAddPhone Extends EUI_Model
	{	
		public function _get_default()
		{
			$this -> EUI_Page -> _setPage(10); 
			$this -> EUI_Page -> _setQuery( "select
						a.ValidID, a.DebiturId, b.deb_acct_no,b.deb_cardno, b.deb_name, a.PhoneNumber, 
						case a.`Status` 
						   when a.`Status` = 1 then 'Approve By Team Leader'
						   when a.`Status` = 2 then 'Approve By Supervisor'
						   when a.`Status` = null then '-'
						end as valid_status, e.adp_name,
						UPPER(concat(c.id,' - ',c.full_name)) as agent_name,
						a.ValidDate, if(a.ValidApproveDate is null,' - ',a.ValidApproveDate) ValidApproveDate
					from t_gn_validphone a
						inner join t_gn_debitur b on a.DebiturId = b.deb_id
						inner join t_tx_agent c on a.AgentID = c.UserId
						inner join t_gn_additional_phone d on a.PhoneNumber = d.addphone_no
						inner join t_lk_additional_phone_type e on d.addphone_type = e.adp_code" ); 
			
			$filter = '';
			if( $this -> URI -> _get_have_post('keys_cust_id') )
			{
				$filter .= " AND b.deb_acct_no LIKE '%".$this -> URI -> _get_post('keys_cust_id')."%' ";
			}
			
			if( $this -> URI -> _get_have_post('keys_cust_name') )
			{
				$filter .= " AND b.deb_acct_no LIKE '%".$this -> URI -> _get_post('keys_cust_name')."%' ";
			}
			
			if( $this -> URI -> _get_have_post('keys_phone_number') )
			{
				$filter .= " AND b.deb_acct_no LIKE '%".$this -> URI -> _get_post('keys_phone_number')."%' ";
			}
			
			$filter .= " AND e.adp_code in (104)";

			$this -> EUI_Page -> _setWhere( $filter );   
			
			if( $this -> EUI_Page -> _get_query() ) {
				return $this -> EUI_Page;
			}
		}
		
		public function _get_content()
		{
			$this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
			$this -> EUI_Page->_setPage(10);

			$sql= " select
						a.ValidID, a.DebiturId, b.deb_acct_no,b.deb_cardno, b.deb_name, a.PhoneNumber, 
						case a.`Status` 
						   when a.`Status` = 1 then 'Approve By Team Leader'
						   when a.`Status` = 2 then 'Approve By Supervisor'
						   when a.`Status` = null then '-'
						end as valid_status, e.adp_name,
						UPPER(concat(c.id,' - ',c.full_name)) as agent_name,
						a.ValidDate, if(a.ValidApproveDate is null,' - ',a.ValidApproveDate) ValidApproveDate
					from t_gn_validphone a
						inner join t_gn_debitur b on a.DebiturId = b.deb_id
						inner join t_tx_agent c on a.AgentID = c.UserId
						inner join t_gn_additional_phone d on a.PhoneNumber = d.addphone_no
						inner join t_lk_additional_phone_type e on d.addphone_type = e.adp_code";

			$this -> EUI_Page ->_setQuery($sql);
			$filter = '';
			if( $this -> URI -> _get_have_post('keys_cust_id') )
			{
				$filter .= " AND b.deb_acct_no LIKE '%".$this -> URI -> _get_post('keys_cust_id')."%' ";
			}
			
			if( $this -> URI -> _get_have_post('keys_cust_name') )
			{
				$filter .= " AND b.deb_acct_no LIKE '%".$this -> URI -> _get_post('keys_cust_name')."%' ";
			}
			
			if( $this -> URI -> _get_have_post('keys_phone_number') )
			{
				$filter .= " AND b.deb_acct_no LIKE '%".$this -> URI -> _get_post('keys_phone_number')."%' ";
			}

			$filter .= " AND e.adp_code in (104)";


			$this -> EUI_Page->_setWhere( $filter );
			// $this -> EUI_Page -> _setWhere('e.adp_code in (104)');
			$this -> EUI_Page->_setOrderBy('a.ValidID','ASC');
			$this -> EUI_Page->_setLimit();
			
			// echo "<pre>".$this -> EUI_Page ->_get_query()."</pre>";
		}
		
		public function _get_resource()
		{
			self::_get_content();
			if(($this->EUI_Page->_get_query()!=="" )) 
			{
				return $this->EUI_Page->_result();
			}
		}
		
		public function _get_page_number() 
		{
			if( $this -> EUI_Page -> _get_query()!='' ) {
				return $this -> EUI_Page -> _getNo();
			}
		}
		
		Public Function _set_approve_number()
		{
			$result		= 0;
			$ApproveID	= $this -> URI-> _get_post('ApproveID');
			$PisahID	= explode(",",$ApproveID);
			$Tanggal	= date("Y-m-d h:i:s");
			
			foreach($PisahID as $Data) {
				
				if(in_array( $this -> EUI_Session -> _get_session('HandlingType'),array(USER_LEADER))) {
					$this -> db -> set('Status', 1);
				} elseif (in_array( $this -> EUI_Session -> _get_session('HandlingType'),array(USER_SUPERVISOR))) {
					$this -> db -> set('Status', 2);
				} else {
					$this -> db -> set('Status', null);
				}
				
				$this -> db -> set('ApproveBy', $this->EUI_Session->_get_session('UserId'));
				$this -> db -> set('ValidApproveDate', $Tanggal);
				$this -> db -> where('ValidID', $Data);
				$this -> db -> update('t_gn_validphone'); 
				
				$this -> db -> last_query();
				$result++;
				
			}
			return $result;
		}
	}

?>