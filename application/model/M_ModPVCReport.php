<?php

class M_ModPVCReport extends EUI_Model{

	var $argv_vars = array();

	public function __construct(){
		parent::__construct();
		$this->load->model(array('M_SetCallResult','M_Loger'));
		if( is_array($this->URI->_get_all_request())){
			$this->argv_vars = (array)$this->URI->_get_all_request();
		}
	}
	
	private function getPreStatus($deb_id){
		$Filter = null;
		$this -> db -> select("a.CallAccountStatus");
		$this -> db -> from("t_gn_callhistory a");
		$this -> db -> where("a.CustomerId",$deb_id);
		$this -> db -> order_by("a.CallHistoryCreatedTs","DESC");
		$this -> db -> limit(1);
		// echo $this -> db -> _get_var_dump();
		foreach($this -> db -> get() -> result_assoc() as $rows){
			$Filter = $rows['CallAccountStatus'];
		}
		return $Filter;
	}
	
	private function getdeb_amount_wo($deb_id){
		$Filter = null;
		$this -> db -> select("a.deb_amount_wo");
		$this -> db -> from("t_gn_debitur a");
		$this -> db -> where("a.deb_id",$deb_id);
		// echo $this -> db -> _get_var_dump();
		foreach($this -> db -> get() -> result_assoc() as $rows){
			$Filter = $rows['deb_amount_wo'];
		}
		return $Filter;
	}
	
	private function pvc($argbv, $Type){
		$deb_amount_wo = $this->getdeb_amount_wo($argbv['DebiturId']);
		$ColumnType = array('New'=>'NewPVC','Exit'=>'ExitPVC');
		$Balance	= array('New'=>'BalanceNewPVC','Exit'=>'BalanceExitPVC');
		
		$this->db->reset_write();
		$this->db->set("Id", 0);
		$this->db->set("Date", date('M-Y'));
		$this->db->set($ColumnType[$Type], 1, FALSE);
		$this->db->set($Balance[$Type], $deb_amount_wo, FALSE);
		$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
		if( $this->db->insert("t_gn_pvcvc_report") ){
			$this->setpvcvcStatusLog($argbv['DebiturId'], $ColumnType[$Type], $deb_amount_wo);
			return TRUE;
		}else{
			$this->db->reset_write();
			// $this->db->set("Id", 0);
			$this->db->set($ColumnType[$Type], $ColumnType[$Type]."+1", FALSE);
			$this->db->set($Balance[$Type], $Balance[$Type]."+".$deb_amount_wo, FALSE);
			$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
			$this->db->where("Date", date('M-Y'));
			if( $this->db->update("t_gn_pvcvc_report") ){
				$this->setpvcvcStatusLog($argbv['DebiturId'], $ColumnType[$Type], $deb_amount_wo);
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	private function vct($argbv, $Type){
		$deb_amount_wo = $this->getdeb_amount_wo($argbv['DebiturId']);
		$ColumnType = array('New'=>'NewVCT','Exit'=>'ExitVCT');
		$Balance	= array('New'=>'BalanceNewVCT','Exit'=>'BalanceExitVCT');

		$this->db->reset_write();
		$this->db->set("Id", 0);
		$this->db->set("Date", date('M-Y'));
		$this->db->set($ColumnType[$Type], 1, FALSE);
		$this->db->set($Balance[$Type], $deb_amount_wo, FALSE);
		$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
		if( $this->db->insert("t_gn_pvcvc_report") ){
			$this->setpvcvcStatusLog($argbv['DebiturId'], $ColumnType[$Type], $deb_amount_wo);
			return TRUE;
		}else{
			$this->db->reset_write();
			// $this->db->set("Id", 0);
			$this->db->set($ColumnType[$Type], $ColumnType[$Type]."+1", FALSE);
			$this->db->set($Balance[$Type], $Balance[$Type]."+".$deb_amount_wo, FALSE);
			$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
			$this->db->where("Date", date('M-Y'));
			if( $this->db->update("t_gn_pvcvc_report") ){
				$this->setpvcvcStatusLog($argbv['DebiturId'], $ColumnType[$Type], $deb_amount_wo);
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	private function vcp($argbv, $Type){
		$deb_amount_wo = $this->getdeb_amount_wo($argbv['DebiturId']);
		$ColumnType = array('New'=>'NewVCP','Exit'=>'ExitVCP');
		$Balance	= array('New'=>'BalanceNewVCP','Exit'=>'BalanceExitVCP');

		$this->db->reset_write();
		$this->db->set("Id", 0);
		$this->db->set("Date", date('M-Y'));
		$this->db->set($ColumnType[$Type], 1, FALSE);
		$this->db->set($Balance[$Type], $deb_amount_wo, FALSE);
		$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
		if( $this->db->insert("t_gn_pvcvc_report") ){
			$this->setpvcvcStatusLog($argbv['DebiturId'], $ColumnType[$Type], $deb_amount_wo);
			return TRUE;
		}else{
			$this->db->reset_write();
			// $this->db->set("Id", 0);
			$this->db->set($ColumnType[$Type], $ColumnType[$Type]."+1", FALSE);
			$this->db->set($Balance[$Type], $Balance[$Type]."+".$deb_amount_wo, FALSE);
			$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
			$this->db->where("Date", date('M-Y'));
			if( $this->db->update("t_gn_pvcvc_report") ){
				$this->setpvcvcStatusLog($argbv['DebiturId'], $ColumnType[$Type], $deb_amount_wo);
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	private function setpvcvcStatusLog($DebiturId, $type, $balance){
		$this->db->reset_write();
		$this->db->set("Id", 0);
		$this->db->set("DebiturId", $DebiturId);
		$this->db->set("ColumnType", $type);
		$this->db->set("Balance", $balance, FALSE);
		$this->db->set("UpdatedAt", date('Y-m-d H:i:s'));
		if( $this->db->insert("t_gn_pvcvc_report_log") ){
			return TRUE;
		}
	}
	
	public function setpvcvcStatus($argbv){
		$pvcvcc = array(115=>"pvc",116=>"vct",117=>"vcp");
		// print_r($pvcvcc);
		$preAccStatus = $this->getPreStatus($argbv['DebiturId']);
		$curAccStatus = $argbv['select_account_status_code'];
		
		$preFlag = in_array($preAccStatus, array_keys($pvcvcc));
		$curFlag = in_array($curAccStatus, array_keys($pvcvcc));
		// echo $preFlag.$curFlag."{".$preAccStatus."|".$curAccStatus."}";
		if(!$preFlag AND $curFlag){
			$this->{$pvcvcc[$curAccStatus]}($argbv, "New");
			// echo "1";
		}
		else if($preFlag AND !$curFlag){
			$this->{$pvcvcc[$preAccStatus]}($argbv, "Exit");
			// echo "2";
		}
		else if($preFlag AND $curFlag AND ($preAccStatus != $curAccStatus)){
			$this->{$pvcvcc[$curAccStatus]}($argbv, "New");
			// echo "3";
		}
	}
	
	

// END CLASS 

}

?>
