<?php
	Class M_ReportPDS Extends EUI_Model
	{
		Function M_ReportPDS()
		{
			$this -> start_date = _getDateEnglish( $this -> URI->_get_post('start_date'));
			$this -> end_date 	= _getDateEnglish( $this -> URI->_get_post('end_date'));
			$this -> campaign	= _get_post('Campaign');
			$this -> TmrId		= _get_post('TmrId');
			$this -> spvId		= _get_post('spvId');
			$this -> group		= _get_post('group_type');
			// $this -> recsource	= _get_post('Recsource');
			// echo _get_post('Recsource')."<=asd";
			$this -> recsource	= explode(',',_get_post('Recsource'));
			$this -> recsource	= implode("','",$this->recsource);
			$this -> recsources = array();
		}
		
		Public Function _getPDSData(){
			$_data = array();
			/*$sqlbackup = "select ct.Recsource, case when cmpds.CampaignId IS NULL THEN 0 ELSE 1 END AS CampaignId, ct.CustomerId,
					count(distinct asglog.CustomerId) as Total, COUNT(DISTINCT callog.CustomerId) AS TotalCall,
					CASE WHEN (select concat(
						cast(CASE WHEN b.CustomerMobilePhoneNum<>'' THEN '1,' ELSE '' END AS VARCHAR),
						cast(CASE WHEN b.CustomerHomePhoneNum<>'' THEN '2,' ELSE '' END AS VARCHAR),
						cast(CASE WHEN b.CustomerWorkPhoneNum<>'' THEN '3' ELSE '' END AS VARCHAR)) AS Prefered
						FROM t_gn_campaign_pds a
						INNER JOIN t_gn_customer_pds b ON a.CampaignId = b.CampaignId AND b.CustomerId = cpds.CustomerId AND b.CustomerPdsId=cpds.CustomerPdsId)
						LIKE concat('%',cmpds.CallPreference,'%') THEN 0 ELSE 1 END AS lightening,
					(CASE WHEN canlog.CustomerId IS NOT NULL THEN 1 ELSE 0 END) as Cancel, CASE WHEN cpds.DialStatus = 0 THEN 1 ELSE 0 END AS Notyet,
					(SELECT COUNT(CustomerId) FROM dialer_call_log WHERE DialSeqno = MAX(callog.DialSeqNo) AND CustomerId = callog.CustomerId AND DialStatusText IN ('failed', 'uncontact', 'CONGESTION', 'NOANSWER')) AS Uncontacted,
					(SELECT COUNT(CustomerId) FROM dialer_call_log WHERE DialSeqno = MAX(callog.DialSeqNo) AND CustomerId = callog.CustomerId AND CallStatusText IN ('CONTACTED')) AS Success,
					(SELECT COUNT(CustomerId) FROM dialer_call_log WHERE DialSeqno = MAX(callog.DialSeqNo) AND CustomerId = callog.CustomerId AND CallStatusText IN ('ABANDONED')) AS Abandon
				FROM t_gn_customer ct
				LEFT JOIN t_gn_customer_pds_asgmt_log asglog ON ct.CustomerId = asglog.CustomerId AND asglog.AssignDate >= '$this->start_date' AND asglog.AssignDate <= '$this->end_date 23:00'
				LEFT JOIN t_gn_customer_pds_cancel canlog ON asglog.AssignSession = canlog.AssignSession AND asglog.CustomerId = canlog.CustomerId AND canlog.CancelDate >= '$this->start_date' AND canlog.CancelDate <= '$this->end_date 23:00'
				LEFT JOIN dialer_call_log callog ON asglog.CustomerId = callog.CustomerId AND callog.DialTime >= '$this->start_date' AND callog.DialTime <= '$this->end_date 23:00'
				LEFT JOIN t_gn_customer_pds cpds ON asglog.CustomerId = cpds.CustomerId AND cpds.CampaignId = asglog.AssignToCampaign
				LEFT JOIN t_gn_campaign_pds cmpds ON asglog.AssignToCampaign = cmpds.CampaignId
				WHERE 1=1 ";
			
				if($this -> recsource!=""){
					$sqlbackup.=" AND ct.Recsource in ('".$this->recsource."') ";
				}
				
				$sqlbackup .= "GROUP BY ct.Recsource, cmpds.CampaignId, ct.CustomerId, asglog.CustomerId, canlog.CustomerId, cpds.DialStatus, callog.CustomerId, cpds.CustomerId, cmpds.CallPreference, cpds.CustomerPdsId
						ORDER BY Cancel DESC";*/
			$sqlbackup = "SELECT b.deb_resource, c.CustomerId, COUNT(distinct a.Id) AS TotalDataAss, COUNT(distinct a.CustomerId) AS Total, COUNT(DISTINCT c.CustomerId) AS TotalCall,
					CASE WHEN (select concat(
										(CASE WHEN ba.CustomerMobilePhoneNum<>'' THEN '1,' ELSE '' END),
										(CASE WHEN ba.CustomerHomePhoneNum<>'' THEN '2,' ELSE '' END),
										(CASE WHEN ba.CustomerWorkPhoneNum<>'' THEN '3' ELSE '' END)) AS Prefered
										FROM t_gn_customer_pds ba where c.CampaignId = ba.CampaignId AND ba.CustomerId = c.CustomerId AND ba.CustomerPdsId=max(c.CustomerPdsId))
										LIKE concat('%',d.CallPreference,'%') THEN 0 ELSE 1 END AS lightening,
					CASE WHEN e.CustomerId IS NOT NULL THEN 1 ELSE 0 END AS Cancel,
					CASE WHEN c.DialStatus = 0 THEN 1 ELSE 0 END AS Notyet,
					(SELECT COUNT(distinct CustomerId) FROM dialer_call_log WHERE DialTime = MAX(f.DialTime) AND CustomerId = f.CustomerId AND DialStatusText IN ('failed', 'uncontact', 'CONGESTION', 'NOANSWER')) AS Uncontacted,
					(SELECT COUNT(distinct CustomerId) FROM dialer_call_log WHERE DialTime = MAX(f.DialTime) AND CustomerId = f.CustomerId AND CallStatusText IN ('CONTACTED')) AS Success,
					(SELECT COUNT(distinct CustomerId) FROM dialer_call_log WHERE DialTime = MAX(f.DialTime) AND CustomerId = f.CustomerId AND CallStatusText IN ('ABANDONED')) AS Abandon
				FROM t_gn_customer_pds_asgmt_log a
				LEFT JOIN t_gn_debitur b ON a.CustomerId = b.deb_id
				LEFT JOIN t_gn_customer_pds c ON a.CustomerId = c.CustomerId AND c.DialStatus > 0
				LEFT JOIN t_gn_campaign_pds d ON a.AssignToCampaign = d.CampaignId
				LEFT JOIN t_gn_customer_pds_cancel e ON a.CustomerId = e.CustomerId AND a.AssignToCampaign = e.CampaignId AND c.CustomerId <> e.CustomerId
				LEFT JOIN dialer_call_log f ON a.CustomerId = f.CustomerId AND f.DialTime >= '$this->start_date' AND f.DialTime <= '$this->end_date 23:00'
				WHERE 1=1 ";
			
				if($this -> recsource!=""){
					$sqlbackup.=" AND b.deb_resource in ('".$this->recsource."') ";
				}
				
				$sqlbackup .= "GROUP BY b.deb_resource, c.CustomerId, c.DialStatus, c.CampaignId, d.CallPreference, e.CustomerId, f.CustomerId";
				
			$sql = "SELECT b.deb_resource,c.CustomerId,COUNT(distinct a.Id) AS Total, COUNT(distinct c.CustomerPdsId) AS TotalCall,
					CASE WHEN (select concat(
										(CASE WHEN ba.CustomerMobilePhoneNum<>'' THEN '1,' ELSE '' END),
										(CASE WHEN ba.CustomerHomePhoneNum<>'' THEN '2,' ELSE '' END),
										(CASE WHEN ba.CustomerWorkPhoneNum<>'' THEN '3' ELSE '' END)) AS Prefered
										FROM t_gn_customer_pds ba where c.CampaignId = ba.CampaignId AND ba.CustomerId = c.CustomerId AND ba.CustomerPdsId=c.CustomerPdsId)
										LIKE concat('%',d.CallPreference,'%') THEN 0 ELSE 1 END AS lightening,
					CASE WHEN e.CustomerId IS NOT NULL THEN 1 ELSE 0 END AS Cancel,
					CASE WHEN c.DialStatus = 0 THEN 1 ELSE 0 END AS Notyet,
					(SELECT COUNT(distinct CustomerId) FROM dialer_call_log WHERE DialTime = MAX(f.DialTime) AND CustomerId = f.CustomerId AND DialStatusText IN ('failed', 'uncontact', 'CONGESTION', 'NOANSWER')) AS Uncontacted,
					(SELECT COUNT(distinct CustomerId) FROM dialer_call_log WHERE DialTime = MAX(f.DialTime) AND CustomerId = f.CustomerId AND CallStatusText IN ('CONTACTED')) AS Success,
					(SELECT COUNT(distinct CustomerId) FROM dialer_call_log WHERE DialTime = MAX(f.DialTime) AND CustomerId = f.CustomerId AND CallStatusText IN ('ABANDONED')) AS Abandon
				FROM t_gn_customer_pds_asgmt_log a
				left JOIN t_gn_debitur b ON a.CustomerId = b.deb_id
				left JOIN t_gn_customer_pds c ON a.AssignSession = c.AssignSession AND a.CustomerId = c.CustomerId AND a.AssignToCampaign = c.CampaignId AND c.DialStatus > 1
				LEFT JOIN t_gn_campaign_pds d ON c.CampaignId = d.CampaignId
				LEFT JOIN t_gn_customer_pds_cancel e ON a.AssignSession = e.AssignSession AND a.CustomerId = e.CustomerId AND e.CampaignId = a.AssignToCampaign
				LEFT JOIN dialer_call_log f ON a.CustomerId = f.CustomerId AND f.DialTime >= '$this->start_date' AND f.DialTime <= '$this->end_date 23:00'
				WHERE 1=1 ";
				
				if($this -> recsource!=""){
					$sql.=" AND b.deb_resource in ('".$this->recsource."') ";
				}
				
				$sql .= "GROUP BY b.deb_resource,a.Id,c.CustomerPdsId,c.CampaignId,c.CustomerId,d.CallPreference,e.CustomerId,c.DialStatus,f.CustomerId";
				
			// echo $sql;
			// echo $sqlbackup;
			$qry = $this->db->query($sqlbackup);
			foreach($qry->result_assoc() as $rows){
				$_data[$rows['deb_resource']]['deb_resource']		= $rows['deb_resource'];
				$_data[$rows['deb_resource']]['Total']			+= $rows['Total'];
				$_data[$rows['deb_resource']]['TotalCall'] 	+= $rows['TotalCall'];
				$_data[$rows['deb_resource']]['HasPhoneN']		+= ($rows['CustomerId']?$rows['lightening']:0);
				$_data[$rows['deb_resource']]['Cancel']		+= ($rows['CustomerId']?0:$rows['Cancel']);
				// $_data[$rows['Recsource']]['Uncontacted']	+= ($rows['CustomerId']?$rows['Uncontacted']:0);
				// $_data[$rows['Recsource']]['Success']		+= ($rows['CustomerId']?$rows['Success']:0);
				// $_data[$rows['Recsource']]['Notyet']		+= ($rows['CustomerId']?$rows['Notyet']:0);
				// $_data[$rows['Recsource']]['Abandon']		+= ($rows['CustomerId']?$rows['Abandon']:0);
				$_data[$rows['deb_resource']]['Uncontacted']	+= $rows['Uncontacted'];
				$_data[$rows['deb_resource']]['Success']		+= $rows['Success'];
				$_data[$rows['deb_resource']]['Notyet']		+= $rows['Notyet'];
				$_data[$rows['deb_resource']]['Abandon']		+= $rows['Abandon'];
			}
			return $_data;
		}
		
		Public Function _get_type(){
			return array(	'call_report' => 'Call Report',
							'calltracking_report' => 'Call Tracking Report');
		}
		
		Public Function _get_campaign(){
			$_data = array();
			$sql = "select a.CampaignId, a.CampaignName from t_gn_campaign a where a.CampaignStatusFlag = 1";
			$qry = $this->db->query($sql);
			foreach($qry->result_assoc() as $rows){
				$_data[$rows['CampaignId']] = $rows['CampaignName'];
			}
			return $_data;
		}
		
		Public Function _get_recsource($Campaign=0){
			$_data = array();
			if($Campaign){
				$sql = "select a.deb_cmpaign_id, b.CampaignCode, a.deb_resource
						From t_gn_debitur a
						left join t_gn_campaign b on a.deb_cmpaign_id = b.CampaignId
						where a.deb_cmpaign_id in (".$Campaign.") group by a.deb_resource";
				
				$qry = $this->db->query($sql);
				// echo $sql;
				foreach($qry->result_assoc() as $rows){
					$_data[$rows['CampaignCode']][$rows['deb_resource']] = $rows['deb_resource'];
				}
			}
			
			// $this->recsources = $_data;
			return $_data;
		}
		
		Public Function _get_spv(){
			$_data = array();
			$sql = "select a.UserId, a.full_name from tms_agent a where a.user_state = 1 and a.handling_type = 3";
			$qry = $this->db->query($sql);
			foreach($qry->result_assoc() as $rows){
				$_data[$rows['UserId']] = $rows['full_name'];
			}
			return $_data;
		}
		
		Public Function _get_tmr($spvId){
			$_data = array();
			$sql = "select a.UserId, a.full_name from tms_agent a where a.user_state = 1 and a.handling_type = 4 and a.spv_id = '".$spvId."'";
			$qry = $this->db->query($sql);
			// $tmrid = explode(',',$this->TmrId);
			// echo $sql;
			foreach($qry->result_assoc() as $rows){
				// if(array_search(0, $tmrid) != ""){
					// $_data[0] = '-';
				// }
				$_data[$rows['UserId']] = $rows['full_name'];
			}
			return $_data;
		}
		
		Public Function _get_mode(){
			return array(2 => 'Summary');
		}

		Public Function _getLooprecs($param=null){
			if($param['Recsource']){
				$recsources = array();
				if($param!=null){
					$recs = explode(',',$param['Recsource']);
					$recsources = array();
					foreach($recs as $key=>$val){
						$recsources[$val] = $val;
					}
				}else{
					$recsources = NULL;
				}
			}else{
				$recsources = $this->_get_recsource($param['Campaign']);
			}
			return $recsources;
		}
		
		Public Function _getDL(){
			$_data = array();
			$sql = "select
						a.DisagreeId, a.CampaignId, a.DisagreeCode
					from t_lk_disagree a
					where a.CampaignId IN (".$this -> campaign.") order by a.DisagreeOrder asc";
			$qry = $this->db->query($sql);
			foreach($qry->result_assoc() as $rows){
				// $_data[$rows['DisagreeCode']] = $rows['DisagreeCode'];
			}
			$i=0;
			for($i=0;$i<=19;$i++){
				$_data['DL'.$i] = 'DL'.$i;
			}
			return $_data;
		}

		Public Function _getLoop(){
			$_data = array();
			if($this->URI->_get_post('group_type') == 1){
				if($this->TmrId=="" OR $this->TmrId==NULL){
					$handling_type = 4;
					// $where =  " AND a.user_state = 1 and a.UserId IN (".$this->TmrId.")";
				}else{
					$handling_type = 4;
					$where =  " AND a.user_state = 1 and a.UserId IN (".$this->TmrId.")";
				}
			}else if($this->URI->_get_post('group_type') == 2){
				$handling_type = 3;
				$where =  " AND a.user_state = 1 and a.UserId IN (".$this->spvId.")";
			}else if($this->URI->_get_post('group_type') == 3){
				$handling_type = 4;
				// $where =  " and a.UserId IN (".$this->TmrId.")";
			}
			
			$sql = "select
						a.UserId, a.id, a.full_name
					from tms_agent a
					where a.handling_type = ".$handling_type."";
			$sql .= $where;
			$tmrid = explode(',',$this->TmrId);
			// if(array_search(0, $tmrid) !== false){
					// $_data[0]['fullname'] = '-';
					// $_data[0]['id'] = '-';
				// }
			// echo $sql;
			// print_r($tmrid);
			// echo array_search(0, $tmrid);
			$qry = $this->db->query($sql);
			foreach($qry->result_assoc() as $rows){
				$_data[$rows['UserId']]['fullname'] = $rows['full_name'];
				$_data[$rows['UserId']]['id'] = $rows['id'];
			}
			// echo array_search(0, $tmrid);
			return $_data;
		}
	
		public function getStatusContacted(){
			$statuses = array();
			
			$sql = "SELECT a.CallReasonCode, a.CallReasonAlias from t_lk_account_status a WHERE a.CallReasonStatusFlag = 1 AND a.CallReasonCategoryId = 14;";
			$qry = $this->db->query($sql);
			foreach($qry->result_assoc() as $rows){
				$statuses[$rows['CallReasonAlias']] = $rows['CallReasonCode'];
			}
			return $statuses;
		}
		public function getStatusUncontacted(){
			$statuses = array();
			
			$sql = "SELECT a.CallReasonCode, a.CallReasonAlias from t_lk_account_status a WHERE a.CallReasonStatusFlag = 1 AND a.CallReasonCode = 118;";
			$qry = $this->db->query($sql);
			foreach($qry->result_assoc() as $rows){
				$statuses[$rows['CallReasonAlias']] = $rows['CallReasonCode'];
			}
			return $statuses;
		}
		
		public function getDataSize(){
			$dataSize		= array();
			$statusContact	= implode(",",$this->getStatusContacted());
			// $recsource		= $this->_get_recsource($this -> campaign);
			
			$sql = "select deb.deb_cmpaign_id, b.CampaignCode, deb.deb_resource, COUNT(deb.deb_id) as totalData
					FROM t_gn_debitur deb
					left join t_gn_campaign b on deb.deb_cmpaign_id = b.CampaignId
					GROUP BY deb.deb_resource";
			$qry = $this->db->query($sql);
			
			// foreach($recsource as $key)
			foreach($qry->result_assoc() as $rows){
				$dataSize[$rows['CampaignCode']][$rows['deb_resource']]['DataSize'] += ($rows['totalData']?$rows['totalData']:0);
			}
			
			return $dataSize;
		}
		
		public function getDataCallTracking(){
			$data = array();
			
			$sql = "SELECT d.CampaignCode, c.deb_resource, b.CustomerId, MAX(b.CallHistoryId) as CallHistoryId, 
						(SELECT sb.CallReasonId FROM t_gn_callhistory sb WHERE sb.CallHistoryId = MAX(b.CallHistoryId)) AS CallReasonId,
						(SELECT ac.CallReasonAlias FROM t_gn_callhistory sb
							LEFT JOIN t_lk_account_status ac ON sb.CallReasonId = ac.CallReasonCode
							WHERE sb.CallHistoryId = MAX(b.CallHistoryId)) AS ReasonDesc
					FROM t_gn_callhistory as b
					LEFT JOIN t_gn_debitur c ON b.CustomerId = c.deb_id
					LEFT JOIN t_gn_campaign d ON c.deb_cmpaign_id = d.CampaignId
					LEFT JOIN t_lk_account_status e ON b.CallReasonId = e.CallReasonCode
					WHERE b.FlagPDS = 1
					GROUP BY b.CustomerId;";
			
			$qry = $this->db->query($sql);
			
			// foreach($recsource as $key)
			foreach($qry->result_assoc() as $rows){
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['CustomerId'] = $rows['CustomerId'];
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['CallHistoryId'] = $rows['CallHistoryId'];
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['CallReasonId'] = $rows['CallReasonId'];
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['ReasonDesc'] = $rows['ReasonDesc'];
				$data[$rows['CampaignCode']][$rows['deb_resource']][$rows['ReasonDesc']] +=1;
			}
			
			return $data;
		}
		
		public function getDataCallInitiate(){
			$data = array();
			
			$sql = "SELECT d.CampaignCode, c.deb_resource, b.CustomerId, b.CallHistoryId, b.CallReasonId, e.CallReasonAlias as ReasonDesc
					FROM t_gn_callhistory as b
					LEFT JOIN t_gn_debitur c ON b.CustomerId = c.deb_id
					LEFT JOIN t_gn_campaign d ON c.deb_cmpaign_id = d.CampaignId
					LEFT JOIN t_lk_account_status e ON b.CallReasonId = e.CallReasonCode
					WHERE b.FlagPDS = 1
					GROUP BY b.CallHistoryId;";
			
			$qry = $this->db->query($sql);
			
			// foreach($recsource as $key)
			foreach($qry->result_assoc() as $rows){
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['CustomerId'] = $rows['CustomerId'];
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['CallHistoryId'] = $rows['CallHistoryId'];
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['CallReasonId'] = $rows['CallReasonId'];
				// $data[$rows['CampaignCode']][$rows['deb_resource']][$rows['CustomerId']]['ReasonDesc'] = $rows['ReasonDesc'];
				$data[$rows['CampaignCode']][$rows['deb_resource']][$rows['ReasonDesc']] +=1;
			}
			
			return $data;
		}
		
		public function getCallInitiatedHeader(){
			return $callInitiated = array("Contacted","Freq Call/Lead","Uncontacted","Freq Call/Lead");
		}
		
		public function getDataUtilize(){
			$dataSize		= array();
			$statusContact	= implode(",",$this->getStatusContacted());
			// $recsource		= $this->_get_recsource($this -> campaign);
			
			$sql = "select deb.deb_cmpaign_id, b.CampaignCode, deb.deb_resource, COUNT(deb.deb_id) as totalData
					FROM t_gn_debitur deb
					left join t_gn_campaign b on deb.deb_cmpaign_id = b.CampaignId
					WHERE deb.deb_call_status_code <> 100
					GROUP BY deb.deb_resource";
			$qry = $this->db->query($sql);
			
			// foreach($recsource as $key)
			foreach($qry->result_assoc() as $rows){
				$dataSize[$rows['CampaignCode']][$rows['deb_resource']]['DataUtilize'] += ($rows['totalData']?$rows['totalData']:0);
			}
			
			return $dataSize;
		}
		
		public function getDataContacted(){
			$dataContacted	= array();
			$statusContact	= implode(",",$this->getStatusContacted());
			// $recsource		= $this->_get_recsource($this -> campaign);
			
			$sql = "select deb.deb_cmpaign_id, b.CampaignCode, deb.deb_resource, COUNT(deb.deb_id) as totalData
					FROM t_gn_debitur deb
					left join t_gn_campaign b on deb.deb_cmpaign_id = b.CampaignId
					WHERE deb.deb_call_status_code IN ($statusContact)
					GROUP BY deb.deb_resource";
			$qry = $this->db->query($sql);
			
			// foreach($recsource as $key)
			foreach($qry->result_assoc() as $rows){
				$dataContacted[$rows['CampaignCode']][$rows['deb_resource']]['DataContacted'] += ($rows['totalData']?$rows['totalData']:0);
			}
			
			return $dataContacted;
		}
		
		public function getDataUncontacted(){
			$dataUncontacted	= array();
			$statusContact	= implode(",",$this->getStatusContacted());
			// $recsource		= $this->_get_recsource($this -> campaign);
			
			$sql = "SELECT cmp.CampaignCode, deb.deb_resource, count(distinct hist.CustomerId) AS totalData
					FROM t_gn_callhistory hist
					LEFT JOIN t_gn_debitur deb ON hist.CustomerId = deb.deb_id
					LEFT JOIN t_gn_campaign cmp ON deb.deb_cmpaign_id = cmp.CampaignId
					WHERE hist.CallReasonId = 118";
			$qry = $this->db->query($sql);
			
			// foreach($recsource as $key)
			foreach($qry->result_assoc() as $rows){
				$dataUncontacted[$rows['CampaignCode']][$rows['deb_resource']]['DataUncontacted'] += ($rows['totalData']?$rows['totalData']:0);
			}
			
			return $dataUncontacted;
		}
		
		
		
		
		
		// Class End
	}
?>