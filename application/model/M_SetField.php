<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
 
class M_SetField extends EUI_Model
{

	
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function M_SetField() 
 { 
	// && run esklusives 
 }
 
 
/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
function _getFieldByCampaignId( $CampaignId = 0 )
{ 
  $this ->db -> select('*');
  $this ->db -> from('t_gn_field_campaign a');
  $this ->db -> where("a.CampaignId", $CampaignId);
  $this ->db -> where("a.Field_Active", 1);
  
  if( $rows = $this -> db -> get() -> result_first_assoc() )  
	return $rows;
  else
	return false;
}

 
/*
 * @ def 		:  get by filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getFieldByLayoutId( $LayoutId = 0 )
{  
	$this ->db -> select('*');
	$this ->db -> from('t_gn_field_campaign a');
	$this ->db -> where("a.Field_Id", $LayoutId);
	$this ->db -> where("a.Field_Active", 1);
	
	 if( $rows = $this -> db -> get() -> result_first_assoc() )  
		return $rows;
	else
		return false;
 }
 
 
 /*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getLabelLayout( $Field_Id = 0 )
{
	$Layout = array();
	
	$this ->db -> select('*');
	$this ->db -> from('t_gn_field_rowset a');
	$this ->db -> where("a.Field_Id", $Field_Id);
	$this ->db -> order_by("a.Rows_Orders","ASC");
	
	$i = 0;
	foreach( $this ->db -> get() -> result_assoc() as $rows )
	{
		$Layout['Names'][$i]  = $rows['Rows_Names'];
		$Layout['Labels'][$i] = $rows['Rows_Labels'];
		$Layout['Ordes'][$i]  = $rows['Rows_Orders'];
		$i++;
	}
	
	return $Layout;

}
 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery("SELECT a.Field_Id, b.CampaignName FROM t_gn_field_campaign a LEFT JOIN t_gn_campaign b ON a.CampaignId=b.CampaignId"); 
	if( $this->URI->_get_have_post('keywords')) 
	{
		$keywords = $this -> URI -> _get_post("keywords");
		$this -> EUI_Page -> _setWhere(" AND ( a.Field_Id LIKE '%$keywords%' 
							OR a.Field_Header LIKE '%$keywords%' 
							OR a.Field_Columns LIKE '%$keywords%' 
							OR a.Field_Active LIKE '%$keywords%'  
							OR a.Field_Create_Ts LIKE '%$keywords%' 
							OR a.Field_Create_UserId LIKE '%$keywords%' 
						   )");
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	if( $this -> EUI_Page -> _get_query()){
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{
	$this -> EUI_Page -> _postPage( $this -> URI -> _get_post('v_page') );
	$this -> EUI_Page -> _setPage(10);
	$this -> EUI_Page -> _setQuery("SELECT a.*, b.CampaignName FROM t_gn_field_campaign a LEFT JOIN t_gn_campaign b ON a.CampaignId=b.CampaignId ");
	
	if( $this->URI->_get_have_post('keywords'))
	{
		$keywords = $this -> URI -> _get_post("keywords");
		$this -> EUI_Page -> _setWhere(" AND ( a.Field_Id LIKE '%$keywords%' 
							OR a.Field_Header LIKE '%$keywords%' 
							OR a.Field_Columns LIKE '%$keywords%' 
							OR a.Field_Active LIKE '%$keywords%'  
							OR a.Field_Create_Ts LIKE '%$keywords%' 
							OR a.Field_Create_UserId LIKE '%$keywords%' 
						   )");
	}	
	
	$this -> EUI_Page -> _setWhere(); 
	if($this->URI->_get_have_post('order_by')){
		$this -> EUI_Page -> _setOrderBy($this->URI->_get_post('order_by'), $this->URI->_get_post('type') );
	}
	
	$this -> EUI_Page->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 
public function _getCols()
{
	$cols = array();
	$hide = $this->M_ManageDatabase->_getSchemaHide('t_gn_debitur');
	$i = 1;
	foreach( $this->db->list_fields('t_gn_debitur') as $key => $values )
	{
		if( !in_array($values, array_keys($hide)))
		{
			$cols[$i] = array('label' => $values, 'name' => $values );
			$i++;
		}
	}
	
	return $cols;
	
} 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 public function _getFieldSize()
 {
	$size = array(); $tbls = count( self::_getCols() ); 
	for( $ux = 1; $ux<=$tbls; $ux++){
		$size[$ux] = $ux;
	}	
	return $size;	
 }
 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
function _setDeleted( $params ){
$_conds = 0; 	
	foreach( $params as $keys => $fld_id ) 
	{
		$this -> db -> where("Field_Id", $fld_id);
		if( $this -> db ->delete('t_gn_field_campaign') )
		{
			$this -> db -> where("Field_Id", $fld_id);
			if( $this -> db ->delete('t_gn_field_rowset') )
			{
				$_conds++;
			}
		}
	}
	return $_conds;
} 
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _setSaveGenerate( $params = null )
 {
 
	$_conds = 0; $rows= 0;
	if( is_array($params) AND $params['CampaignCode'] )
	{
		$labels = $this->_getCols();
		$label_setup = array(); $orders_by = array();
		$i=1;
		foreach($labels as $key => $labelname )
		{
			if( in_array($labelname['name'], array_keys($params)) ) 
			{
				$label_setup[$labelname['name']] = array ( 
					'label' => $params[$labelname['name']],
					'name' => $labelname['name'],
					'order' => $this->URI->_get_post("Orders_". $i)
				);	
				$i++;
			}
		}
		
		$FieldSize =((($params['Field_end']) - ($params['Field_start']))+1);
		if( $FieldSize )
		{
			$this -> db -> set("CampaignId", $params['CampaignCode']);
			$this -> db -> set("Field_Header", $params['Field_Header']);
			$this -> db -> set("Field_Columns", $params['Field_Columns']);
			$this -> db -> set("Field_Size", $FieldSize );
			$this -> db -> set("Field_Active", $params['Field_Active']);
			$this -> db -> set("Field_Create_Ts", date('Y-m-d H:i:s'));
			$this -> db -> set("Field_Create_UserId", $this -> EUI_Session->_get_session('Fullname'));
			$this -> db -> insert('t_gn_field_campaign');
			
			if( $this -> db ->affected_rows() > 0 )
			{
				$InsertId = $this -> db -> insert_id();
				if( $InsertId )
				{
					if(is_array($label_setup))foreach( $label_setup as $keword => $row_set )
					{
						$this->db->set("Field_Id", $InsertId);
						$this->db->set("Rows_Names", $row_set['name']);
						$this->db->set("Rows_Labels",$row_set['label']);
						$this->db->set("Rows_Orders", $row_set['order']);
						$this->db->insert('t_gn_field_rowset');
						if($this -> db ->affected_rows() > 0) $rows++;
					}
				}
			}
		
		}
	  // return data true 
	  
		if( $rows ) $_conds++; 
	}
	
	return $_conds;
 }
 
 
}

/*
 * END OF CLASS 
 */