<?php

	Class M_SetSmsBlast Extends EUI_Model
	{
		function _get_default()
		{
			$this -> EUI_Page -> _setPage(10); 
			$this -> EUI_Page -> _setQuery("select
												a.SRV_Data_Id
											from serv_sms_automatic a
												left join serv_sms_tpl b on a.SRV_Data_TemplateId = b.SmsTplId
												left join t_lk_account_status c on a.SRV_Data_Status = c.CallReasonCode");

			$filter = '';

			// if( $this->URI->_get_have_post('key_words') ) 
			// {
				// $filter = " AND ( a.menu LIKE '%".$this -> URI -> _get_post('key_words')."%' 
				// OR a.file_name LIKE '%".$this -> URI -> _get_post('key_words')."%' 
				// OR a.el_id LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.description LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.flag LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.updated_by LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.OrderId LIKE '%".$this -> URI -> _get_post('key_words')."%' ) ";
			// }				

			$this -> EUI_Page -> _setWhere( $filter );
			// echo $this -> EUI_Page -> _get_query();
			if( $this -> EUI_Page -> _get_query() ) {
				return $this -> EUI_Page;
			}
		}

		public function _get_select_dropdown()
		{
			$dropdown = array(
				'COLL_DROPDOWN_HRS' => $this->_get_select_hours(),
				'COLL_DROPDOWN_MNT' => $this->_get_select_minutes()
			);
			return $dropdown;
		}

		public function _get_select_hours() 
		{
  			$Ih = array(); for( $i=0; $i<=23; $i++) 
  			{
				$i_s = (string)sprintf('%02d', $i);
				$Ih[$i_s] = $i_s;
  			}
	
 			return $Ih;
		}

		public function _get_select_minutes() 
		{
  			$iM = array();
  			for( $i = 0; $i<=59; $i++) 
  			{
				$iM[sprintf('%02d',$i)] = (string)sprintf('%02d', $i);
  			}
  
 			return $iM;
  		}
		
		public function _get_content()
		{
			//@ create object 

			$this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
			$this -> EUI_Page->_setPage(10);

			$sql = "select
						a.SRV_Data_Id, 
						a.SRV_Data_TemplateId, b.SmsTplSubject, b.SmsTplContent,
						a.SRV_Data_Status, c.CallReasonDesc,
						a.SRV_Data_Priority, a.SRV_Data_CreateTs,
						#a.SRV_Data_SentDate,
						DATE_FORMAT(a.SRV_Data_SentDate,'%d') as SRV_Data_SentDate,
						a.SRV_Data_SentTime
					from serv_sms_automatic a
						left join serv_sms_tpl b on a.SRV_Data_TemplateId = b.SmsTplId
						left join t_lk_account_status c on a.SRV_Data_Status = c.CallReasonCode";

			$this -> EUI_Page ->_setQuery($sql);
			$filter = '';
			// if( $this -> URI -> _get_have_post('key_words') )
			// {
				// $filter = " AND ( a.menu LIKE '%".$this -> URI -> _get_post('key_words')."%' 
				// OR a.file_name LIKE '%".$this -> URI -> _get_post('key_words')."%' 
				// OR a.el_id LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.description LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.flag LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.updated_by LIKE '%".$this -> URI -> _get_post('key_words')."%'
				// OR a.OrderId LIKE '%".$this -> URI -> _get_post('key_words')."%' ) ";
			// }				

			$this -> EUI_Page->_setWhere( $filter );
			$this -> EUI_Page->_setOrderBy('a.SRV_Data_Id','ASC');
			$this -> EUI_Page->_setLimit();
			// echo "<pre>".$this -> EUI_Page -> _get_query()."</pre>";
			
		}
		
		function _get_resource()
		{
			self::_get_content();
			if( $this -> EUI_Page -> _get_query()!='') 
			{
				return $this -> EUI_Page -> _result();
			}	
		}
		
		function _get_page_number() 
		{
			if( $this -> EUI_Page -> _get_query()!='' ) {
				return $this -> EUI_Page -> _getNo();
			}	
		}
		
		function _get_template()
		{
			$_conds = array();

			$sql = "select * from serv_sms_tpl a
					where a.SmsTplFlags = 1";
			$qry = $this -> db -> query($sql);

			foreach( $qry -> result_assoc() as $rows )
			{
				$_conds[$rows['SmsTplId']] = $rows['SmsTplSubject'];
			}

			return $_conds;
		}
		
		function _get_data_status()
		{
			$_conds = array();

			$sql = "select * from t_lk_account_status a
					where a.CallReasonStatusFlag = 1";
			$qry = $this -> db -> query($sql);

			foreach( $qry -> result_assoc() as $rows )
			{
				$_conds[$rows['CallReasonCode']] = $rows['CallReasonDesc'];
			}

			return $_conds;
		}
		
		function _setSaveSMS()
		{
			$Valid = 0;
			// $_call_back_date = _getDateEnglish($this->argv_vars['text_call_back_date'])." ".$this->argv_vars['text_call_hour'].":".$this->argv_vars['text_call_minute'].":00"; 
			$Date 		= date('Y-m-d');
			// $Durasi		= _get_post('days');
			// $SentDate	= date('Y-m-d', strtotime($Date. ' + '.$Durasi. 'days'));
			$SentTime	= date('H:i:s');
			
			$this->db->set("SRV_Data_TemplateId", _get_post('template'));	
			$this->db->set("SRV_Data_Status", _get_post('data_status'));
			// $this->db->set("SRV_Data_Priority", _get_post('days'));
			// $this->db->set("SRV_Data_Priority", _get_post('SentDate'));
			$this->db->set("SRV_Data_Context", "default");
			$this->db->set("SRV_Data_CreateTs", date('Y-m-d H:i:s'));
			// $this->db->set("SRV_Data_SentDate", $SentDate);
			$this->db->set("SRV_Data_SentDate", date('Y-m-d', strtotime(_getDateEnglish(_get_post('SentDate')))));
			// $this->db->set("SRV_Data_SentTime", $SentTime);
			$this->db->set("SRV_Data_SentTime", date('H:i:s', strtotime(_getDateEnglish(_get_post('SentDate')))));
			$this->db->set("SRV_Data_CreatorId", _get_session('UserId'));
			$this->db->set("SRV_Data_Active", 0);
			$this->db->set("SRV_Data_Process", 0);
			$this->db->insert('serv_sms_automatic');
			
			$Valid++;
				
			return $Valid;
		}
		
		function _get_set_sms_detail( $Id )
		{
			$sql = "select
						a.SRV_Data_Id, 
						a.SRV_Data_TemplateId, b.SmsTplSubject, b.SmsTplContent,
						a.SRV_Data_Status, c.CallReasonDesc,
						a.SRV_Data_Priority, 
						a.SRV_Data_SentDate,
						a.SRV_Data_CreateTs
					from serv_sms_automatic a
						inner join serv_sms_tpl b on a.SRV_Data_TemplateId = b.SmsTplId
						inner join t_lk_account_status c on a.SRV_Data_Status = c.CallReasonCode
					where a.SRV_Data_Id = $Id";

			$qry = $this -> db -> query($sql);

			if( !$qry -> EOF() ) 
			{
				return $qry -> result_first_assoc();
			}
		}
		
		function _setUpdateSMS()
		{
			$_conds = 0;

			$this->db->where('SRV_Data_Id', _get_post('SRV_Data_Id'));
			$this->db->set('SRV_Data_TemplateId', _get_post('template'));
			$this->db->set('SRV_Data_Status', _get_post('data_status'));
			$this->db->set("SRV_Data_SentDate", date('Y-m-d', strtotime(_getDateEnglish(_get_post('SentDate')))));
			$this->db->set('SRV_Data_UpdateTs', date('Y-m-d H:i:s'));
			$this->db->update('serv_sms_automatic');
			
			$_conds++;
			
			return $_conds;
		}
		
		function _setDeleteSMS($ID)
		{
			$_conds = 0;

			$this -> db -> where('SRV_Data_Id', $ID);
			$this -> db -> delete('serv_sms_automatic');
			// echo $this -> db -> last_query();
			$_conds++;
			
			return $_conds;
		}
	}

?>