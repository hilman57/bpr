<?php
class M_ChatWith extends EUI_Model
{

 private static $instance = null;

 // patern 
 
 public static function &get_instance() 
 {
	if(is_null(self::$instance))
	{
		self::$instance = new self();
	}
	
	return self::$instance;
 }
 
 // aksesor 
 function M_ChatWith() { }
 
 
// @ pack: function get Ready -----------------------------

 public function _getUserReady()
{

  $UserReady = array();
 
 // @ pack : User Login Query Its. 
 
  $this ->db ->select(
		"a.UserId,a.id AS Username, a.full_name AS Fullname,
		a.init_name AS code_user, IF(a.logged_state=1,'Login','Logout') AS AgentStatus,
		g.name AS User_Lvl",
		FALSE);
						
  $this->db->from("t_tx_agent a");
  $this->db->join('t_gn_user_project_work f ','a.UserId=f.UserId','LEFT');
  $this->db->join('t_gn_agent_profile g ','a.handling_type = g.id','INNER');
  $this->db->where_in('f.ProjectId ', $this->EUI_Session->_get_session('ProjectId') );
	
 // @ pack : user admin

  $USER_PRIVILEGES = (INT)_get_session('HandlingType');
  
   if( in_array($USER_PRIVILEGES,array(USER_ROOT)) )
   {
	  $this->db->where('g.IsActive',1);
	  $this->db->where('a.user_state',1);
   }
	
// @ pack : user admin

 	if( in_array($USER_PRIVILEGES, array(USER_ADMIN))) {
		$this->db->where_in('g.id', array(USER_ADMIN,USER_SUPERVISOR,USER_MANAGER, USER_SENIOR_TL,USER_LEADER, USER_AGENT_OUTBOUND));
	}
	
	if( $USER_PRIVILEGES == USER_MANAGER ){
		$this->db->where_in('g.id', array(USER_ADMIN,USER_SUPERVISOR,USER_MANAGER,USER_SENIOR_TL, USER_LEADER, USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	}
	
// @ pack : supervisor 
	
	if( USER_PRIVILEGES == USER_SUPERVISOR){
		$this->db->where_in('g.id', array(USER_ADMIN,USER_SUPERVISOR,USER_MANAGER,USER_SENIOR_TL, USER_LEADER, USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	}
	
// @ pack : Leader

	if( $USER_PRIVILEGES ==USER_LEADER ){
		$this->db->where_in('g.id', array(USER_ADMIN,USER_MANAGER,USER_SUPERVISOR,USER_SENIOR_TL,USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->or_where_in('a.stl_id', array(_get_session('SeniorLeaderId')));	
		$this->db->or_where_in('a.tl_id', array(_get_session('TeamLeaderId')));	
		$this->db->where_not_in('a.UserId', array(_get_session('UserId')));
	}

//senior leader
	if( $USER_PRIVILEGES ==USER_SENIOR_TL ){
		$this->db->where_in('g.id', array(USER_ADMIN,USER_LEADER,USER_MANAGER,USER_SUPERVISOR,USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->or_where_in('a.stl_id', array(_get_session('UserId')));	
		$this->db->where_not_in('a.UserId', array(_get_session('UserId')));
	}	
	
// @ pack : user agent inbound only 
	if( in_array( $USER_PRIVILEGES, 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND)))
	{
		$this->db->where_in('g.id', array(USER_LEADER,USER_SENIOR_TL));
		$this->db->where_in('a.UserId', array( _get_session('TeamLeaderId'), _get_session('SupervisorId'), _get_session('ManagerId'),_get_session('SeniorLeaderId')));
	
	}
	
	$this->db->order_by("g.name,a.full_name","ASC");

	foreach( $this->db->get()-> result_assoc() as $rows ){
		$UserReady[$rows['UserId']] = $rows;
	}
	// echo "<pre>".$this->db->last_query()." </pre>";
	return $UserReady;
}
 
	
}
?>