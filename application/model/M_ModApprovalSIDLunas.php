<?php
/*
 * @ pack : model class M_ModApprovalDiscount
 *
 */
 
class M_ModApprovalSIDLunas extends EUI_Model
{
/*
 * @ pack : lock is 
 */

var $IsLockOpen = array(0);
var $IsLockClose = array(1);
var $PtpNew = '106'; // PTP-NEW

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_ModApprovalSIDLunas() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser','M_SetProduct', 
	'M_SetCampaign','M_ProjectWorkForm',
	'M_SetResultCategory', 'M_Combo', 
	'M_MaskingNumber','M_ModContactDetail', 
	'M_SysUser','M_ModCallHistory'));
	
}

/*
 * @ pack : RquestApproveStatus
 */
 
 public function _getRequestApproveStatus()
{
 $conds = array();
 
 $this->db->reset_select();
 $this->db->select("a.StatusCode, a.StatusName",FALSE);
 $this->db->from("t_lk_reqdiscount_status a ");
 $this->db->where("a.StatusFlags", 1);
 $this->db->where_in("a.StatusCode",array(101,102,104,105));
 
 foreach( $this->db->get()->result_assoc() as $rows ){
	$conds[$rows['StatusCode']] = STRTOUPPER($rows['StatusName']);
 }

  return $conds;  
}

/*
 * @ pack : RquestApproveStatus
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{

	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.id");
	$this->EUI_Page->_setFrom("t_gn_discount_trx a");
	$this->EUI_Page->_setJoin("t_gn_debitur b","a.deb_id=b.deb_id","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status c "," b.deb_call_status_code = c.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status d "," b.deb_prev_call_status_code = d.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_reqdiscount_status e "," a.aggrement = e.StatusCode ","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent g", "g.UserId=a.agent_id ","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent h", "h.UserId=a.updated_by ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment f","b.deb_id=f.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign i "," b.deb_cmpaign_id=i.CampaignId ","LEFT", true);

// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('f.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('f.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('f.AssignSpv IS NOT NULL', FALSE);
 	 $this->EUI_Page->_setAnd('i.CampaignStatusFlag',1);
	 $this->EUI_Page->_setAnd("a.form_type in ('L','D')");

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 		
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('f.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('f.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('f.AssignSelerId IS NOT NULL');
	}		
	$this->EUI_Page->_setAnd('a.sid_status >= 1');
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'reqs_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'reqs_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'reqs_account_status', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'reqs_call_status', TRUE);
	$this->EUI_Page->_setAndCache('a.sid_status', 'sid_status', TRUE);
	$this->EUI_Page->_setAndCache('g.UserId', 'reqs_agent_id', TRUE);
	$this->EUI_Page->_setAndCache('a.aggrement', 'reqs_approve_status', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'reqs_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("date(a.create_date)>='". _getDateEnglish(_get_post('reqs_start_date')) ."'", 'reqs_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("date(a.create_date)<='". _getDateEnglish(_get_post('reqs_end_date')) ."'", 'reqs_end_date', TRUE);
	
	// echo "<pre>". $this ->EUI_Page->_getCompiler()."</pre>";
	
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.id as RequestId "=>array('RequestId','ID','primary'),
		"b.deb_id AS CustomerId"=> array('CustomerId','ID','hidden'),
		"b.deb_acct_no AS AccountNumber "=> array('AccountNumber','ID Pelanggan'),
		"b.deb_name AS CustomerName"=> array('CustomerName','Nama Pelanggan'),
		"c.CallReasonDesc AS AccountStatus "=> array('AccountStatus','Status Akun'),
		"d.CallReasonDesc AS CallStatus "=> array('CallStatus','Panggilan Terakhir'),
		"b.deb_amount_wo AS AmountWO "=> array('AmountWO','Jumlah WO'),
		"a.from_princ as from_princ" => array("from_princ","From Principle"),
		"UPPER(g.id) AS ReqByDeskoll "=> array('ReqByDeskoll','* Request By'),
		"IF(a.aggrement IN(103), UPPER(j.id), UPPER(h.id)) AS AprvByDeskoll "=> array('AprvByDeskoll','* Approve By'),
		"UPPER(e.StatusName) as RequestName" => array("RequestName","* Req. Status"),
		"a.create_date AS CreateDate"=> array('CreateDate','Dibuat Tanggal'),
		"IF(a.aggrement IN(103), a.reject_date, a.udate_date) AS UpdateDate"=> array('UpdateDate','Update Date'),
		"a.cpa_status as CPAStatus" => array("CPAStatus","Cpa.Status"),
		"a.sid_status as SIDStatus" => array("SIDStatus","SID.Status")
	));
	
	$this->EUI_Page->_setFrom("t_gn_discount_trx a");
	$this->EUI_Page->_setJoin("t_gn_debitur b","a.deb_id=b.deb_id","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status c "," b.deb_call_status_code = c.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status d "," b.deb_prev_call_status_code = d.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_reqdiscount_status e "," a.aggrement = e.StatusCode ","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent g", "g.UserId=a.agent_id ","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent h", "h.UserId=a.updated_by ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment f","b.deb_id=f.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign i "," b.deb_cmpaign_id=i.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent j ","a.reject_by=j.UserId ","LEFT", true);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('f.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('f.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('f.AssignSpv IS NOT NULL', FALSE);
 	 $this->EUI_Page->_setAnd('i.CampaignStatusFlag',1);
	 $this->EUI_Page->_setAnd("a.form_type in ('L','D')");
	 
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 		
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('f.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('f.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('f.AssignSelerId IS NOT NULL');
	}
	
	$this->EUI_Page->_setAnd('a.sid_status >= 1');
	
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'reqs_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'reqs_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'reqs_account_status', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'reqs_call_status', TRUE);
	$this->EUI_Page->_setAndCache('a.sid_status', 'sid_status', TRUE);
	$this->EUI_Page->_setAndCache('g.UserId', 'reqs_agent_id', TRUE);
	$this->EUI_Page->_setAndCache('a.aggrement', 'reqs_approve_status', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'reqs_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.create_date>='". _getDateEnglish(_get_post('reqs_start_date')) ."'", 'reqs_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.create_date<='". _getDateEnglish(_get_post('reqs_end_date')) ."'", 'reqs_end_date', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	// echo "<pre>". $this ->EUI_Page->_getCompiler()."</pre>";
	$this->EUI_Page->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
 

/*
 * @ generate pdf and then set on DB 
 */
 
 public function _doPDF( $DiscountId = 0 )
{
 
 $conds= FALSE;
 
 if(_have_get_session('UserId'))
 {
	$this->load->library('PDF');
	$doPDF =&EUI_PDF::Instance();
	
 // lucky process PDF 
 
	$doPDF->set_conf('ip',$_SERVER['HTTP_HOST']); // server IP
	$doPDF->set_conf('host','hsbc'); // name of host
	$doPDF->set_conf('controller','FormLunas'); // controller 
	$doPDF->set_conf('method','PrintByDiscountId'); // function call 
	 
	// @ pack : set argument 
	 
	$doPDF->set_argv('DiscountId',$DiscountId); 
	$doPDF->set_argv('action','pdf'); 
	$doPDF->set_pdf(md5($DiscountId));
	
	if( $result = $doPDF->doPDF()) 
	{
		$conds  = (string)$result;
	 }
 }
	return $conds;
} 

/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _find_rows_by_id( $id )
{
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_gn_discount_trx');
	$this->db->where('id', $id);
	
	return $this->db->get()->result_first_assoc();
	
}


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
public function _setApprovalLunas()
{
    $conds = 0;
    $DiscountId = _get_post('DiscountId');
	$this->db->set('sid_status', 2);
	$this->db->where('id', $DiscountId);
  
  if( $this->db->update('t_gn_discount_trx') ) 
  {
	 // $status_code = _get_post('Status');
	 // if( $status_code )
	 // {
		// $ClsUser =& get_class_instance('M_SysUser');
		// $ClsDetail =& get_class_instance('M_ModContactDetail');
		// if(is_object($ClsUser) 
		   // AND is_object($ClsDetail) )
		// {
			
			 // $DataDiscount =& $this->_find_rows_by_id($DiscountId);
			 // $UserData =& $ClsUser->_getUserByCode(array('UserId'=>$DataDiscount['agent_id']));
			 // $DataDetail =& $ClsDetail->_get_select_debitur($DataDiscount['deb_id']);
			 
		// @ pack : set update debitur with status PTP-NEW 
		
			/*$this->db->set('deb_call_status_code', $this->PtpNew);
			$this->db->where('deb_id', $DataDetail['deb_id']);
			
			if( $this->db->update('t_gn_debitur') )
			{
			  $this->db->set('CustomerId', $DataDetail['deb_id']);
			  $this->db->set('CallReasonId', $DataDetail['deb_prev_call_status_code']);
			  $this->db->set('CallAccountStatus',$this->PtpNew );
			  $this->db->set('ApprovalStatusId',$DataDiscount['aggrement']);
			  $this->db->set('CreatedById', $DataDiscount['agent_id']);
			  $this->db->set('TeamLeaderId', $DataDiscount['tl']);
			  $this->db->set('SupervisorId', $DataDiscount['spv']);
			  $this->db->set('UpdatedById', _get_session('UserId'));
			  $this->db->set('CallHistoryNotes', "APPROVE FORM LUNAS");
			  $this->db->set('CallHistoryCreatedTs',DATE('Y-m-d H:i:s'));
			  $this->db->set('CallHistoryUpdatedTs',DATE('Y-m-d H:i:s'));
			  $this->db->insert('t_gn_callhistory');
			}*/
		// }
		
	 // }
	$conds++;
  }
  
  return $conds;
}


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _setRejectDiscount()
{
 $conds = 0;
 $DiscountId = _get_post('DiscountId');
 
  $this->db->set('aggrement', _get_post('Status'));
  $this->db->set('reject_by', _get_session('UserId'));
  $this->db->set('reject_date', date('Y-m-d H:i:s'));
  $this->db->where_not_in('aggrement', array('103'));
  $this->db->where_in('aggrement', array('101'));
  $this->db->where('id', $DiscountId);
  $this->db->update('t_gn_discount_trx');
  
  if(  $this->db->affected_rows() > 0) 
  {
		$ClsUser =& get_class_instance('M_SysUser');
		$ClsDetail =& get_class_instance('M_ModContactDetail');
		if(is_object($ClsUser) 
		   AND is_object($ClsDetail) )
		{
			$DataDiscount =& $this->_find_rows_by_id($DiscountId);
			$UserData =& $ClsUser->_getUserByCode(array('UserId'=>$DataDiscount['agent_id']));
			$DataDetail =& $ClsDetail->_get_select_debitur($DataDiscount['deb_id']);
			
			$this->db->set('CustomerId', $DataDetail['deb_id']);
			$this->db->set('CallReasonId', $DataDetail['deb_prev_call_status_code']);
			$this->db->set('CallAccountStatus',$DataDetail['deb_call_status_code'] );
			$this->db->set('ApprovalStatusId',$DataDiscount['aggrement']);
			$this->db->set('CreatedById', $DataDiscount['agent_id']);
			$this->db->set('TeamLeaderId', $DataDiscount['tl']);
			$this->db->set('SupervisorId', $DataDiscount['spv']);
			$this->db->set('UpdatedById', _get_session('UserId'));
			$this->db->set('CallHistoryNotes', "REJECT FORM LUNAS");
			$this->db->set('CallHistoryCreatedTs',DATE('Y-m-d H:i:s'));
			$this->db->set('CallHistoryUpdatedTs',DATE('Y-m-d H:i:s'));
			$this->db->insert('t_gn_callhistory');
		}
		
	$conds++;
  }
  
  return $conds;
}

// END OF CLASS 

 
}



?>