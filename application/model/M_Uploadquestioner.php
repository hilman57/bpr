<?php

class M_Uploadquestioner Extends EUI_Model
{

 function M_Uploadquestioner() { 
 
 }
 
 /*
  *
  */
  
 function _get_default()
 {
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setQuery("SELECT * FROM t_gn_upload_report_ftp a"); 	
	
	$flt = null;
	if($this ->URI->_get_have_post('keywords') ) {
		$flt .=  " AND ( 
			a.FTP_UploadFilename LIKE '%{$this ->URI->_get_post('keywords')}%' 
			OR FTP_Flags LIKE '%{$this ->URI->_get_post('keywords')}%' ) ";
	}
	
	$this->EUI_Page->_setWhere($flt);
	$this->EUI_Page->_setGroupBy('a.FTP_UploadId');
	if( $this -> EUI_Page -> _get_query() )
	{
		return $this -> EUI_Page;
	}
 }
 
 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{
	$this->EUI_Page->_postPage($this -> URI -> _get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
	$this->EUI_Page->_setArraySelect(array(
		"a.FTP_UploadId as ftp_upload_id" => array('ftp_upload_id','ID','primary'),
		"a.FTP_UploadType as FTP_UploadType" => array('FTP_UploadType','Source'),
		"a.FTP_UploadFilename as FTP_UploadFilename" => array('FTP_UploadFilename','File Name'),
		"a.FTP_UploadRows as FTP_UploadRows" => array('FTP_UploadRows','Total Data'),
		"a.FTP_UploadSuccess as FTP_UploadSuccess" => array('FTP_UploadSuccess','Success'),
		"a.FTP_UploadDuplicate as FTP_UploadDuplicate" => array('FTP_UploadDuplicate','Duplicate'),
		"a.FTP_UploadFailed as FTP_UploadFailed" => array('FTP_UploadFailed','Failed'),
		"a.FTP_UploadDateTs as FTP_UploadDateTs" => array('FTP_UploadDateTs','Upload Date')
	));
	
 // @ pack : get from selected  ----------------------------------------

	$this->EUI_Page->_setFrom('t_gn_upload_report_ftp a',TRUE);

 // @ pack : get from filter keyword   ----------------------------------------

	if($this ->URI->_get_have_post('keywords') ) {
		$this->EUI_Page->_setAnd(" ( a.FTP_UploadFilename LIKE '%{$this ->URI->_get_post('keywords')}%' 
								   OR FTP_Flags LIKE '%{$this ->URI->_get_post('keywords')}%' ) " );
	}
	
 // @ pack : get group  ----------------------------------------
	
	$this->EUI_Page->_setGroupBy('a.FTP_UploadId');
	
 // @ pack : get order by field  ----------------------------------------
	
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}
	else{
		$this->EUI_Page-> _setOrderBy('a.FTP_UploadId','DESC');
	}
	
 // @ pack : set execute  ----------------------------------------
	
	$this->EUI_Page->_setLimit();
	
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }


function _getDataUpload($UploadId=null)
{
 $_conds = array();
 
	if(is_array($UploadId) )
	{
		$this -> db -> select('a.*');
		$this -> db -> from('t_gn_upload_report_ftp a');
		$this -> db -> where_in('a.FTP_UploadId', $UploadId);
		foreach( $this ->db->get()->result_assoc() as $rows )
		{
			$_conds[$rows['FTP_UploadId']] = $rows;
		}
	}
	
	return $_conds;
} 


// _setHidden

function _setHidden( $params = null, $_active = 1 )
{
	$_conds = 0;
	if(!is_null($params) )
	{	
	
		foreach($params as $k => $ftpid )
		{
			$this -> db->set('FTP_Flags',$_active);
			$this -> db->where('FTP_UploadId', $ftpid);
			$this -> db->update('t_gn_upload_report_ftp');
			if( $this-> db ->affected_rows() > 0 )
			{
				$_conds++;	
			}
		}
	}
	
	return $_conds;
}


// _setDeleted
function _setDeleted( $params = null )
{
	$_conds = 0;
	
	if(!is_null($params) )
	{	foreach($params as $k => $ftpid )
		{
			$this -> db->where('FTP_UploadId', $ftpid);
			$this -> db->delete('t_gn_upload_report_ftp');
			if( $this-> db ->affected_rows() > 0 )
			{
				$_conds++;	
			}
		}
	}
	
	return $_conds;
}


}

?>