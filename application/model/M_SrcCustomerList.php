<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SrcCustomerList extends EUI_Model
{

/*
 * @ pack : lock is 
 */

 var $IsLockOpen = array(0);
 var $IsLockClose = array(1);
 var $ClsLockData = null;
 var $IsUnlockStatus = null;
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function M_SrcCustomerList() 
{
  $this->load->model(array(	'M_SetCallResult','M_SysUser','M_SetProduct','M_SetCampaign',
							'M_ProjectWorkForm','M_SetResultCategory', 'M_Combo','M_MaskingNumber',
							'M_MgtLockData','M_MgtRandDeb' ));
  
/*  
 * @ pack : cek modul this
 */
  
 
  
  
} // ==> M_SrcCustomerList


// _NextActivity 

public function _NextActivity()
{
    $next_activity = array();
	$this->_get_default();
	$sql = $this->EUI_Page->_getCompiler();
	if( $sql ) 
	{
		$qry = $this->db->query($sql);
		$num = 0;
		foreach( $qry->result_assoc() as $rows ){
			$next_activity[] = $rows['CustomerId'];  
			$num++;
		}
	}
	
	return $next_activity;
	
} // ==> _NextActivity

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 

/*
 * @ def 		: get total summary on bottom  
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

 public function _get_select_summary_amount()
{

 $SummaryAmountWO = 0;
 
 $this->db->reset_select();
 $this->db->select("SUM(a.deb_amount_wo) as SummaryAmountWO", FALSE);
 $this->db->from("t_gn_debitur a ");
 $this->db->join("t_gn_assignment b "," a.deb_id=b.CustomerId", "INNER");
 $this->db->join("t_tx_agent ag "," b.AssignSelerId=ag.UserId", "INNER");
 
// @ pack : filter by default is agent/deskoll ---------------------------------------------------------------------------------
 if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
 array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
 {
	$this->db->where('b.AssignSelerId',$this->EUI_Session->_get_session('UserId'));
 }
 
// @ pack : filter by default is leader ---------------------------------------------------------------------------------
 if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
 array(USER_LEADER) ) )
 {
	$this->db->where('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
 }		
 
 // @ pack : filter by default is Seniorleader ---------------------------------------------------------------------------------
 if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
 array(USER_SENIOR_TL) ) )
 {
	$this->db->where('ag.stl_id',$this->EUI_Session->_get_session('UserId'));
 }		
 
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 ){
	$SummaryAmountWO = $qry->result_singgle_value();
 }
 
 
 return $SummaryAmountWO;
 
} 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->IsUnlockStatus =& M_MgtLockData::_get_select_active_status();
 
	$this->EUI_Page->_setPage(30); 
	$this->EUI_Page->_setSelect("a.deb_id");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.deb_cmpaign_id=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.deb_call_status_code = f.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.deb_cmpaign_id=g.CampaignId","LEFT", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	
/** @ pack : filter by default session --------------------------------------------------------------------------------- **/
	
	$this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	
	
/** @ pack : look parameter amount  **** --------------------------------------------------------------------------------------------- **/
	$Amount =& M_MgtLockData::_get_select_active_amount();
	//var_dump($Amount);
	if( !is_null($Amount) AND is_array($Amount) ){
		foreach( $Amount as $field => $value ){
		  $this->EUI_Page->_setAnd("a.{$field}>={$value[start_amount]}");
		  $this->EUI_Page->_setAnd("a.{$field}<={$value[end_amount]}");
		}
	}
	
/** @ pack : look of show status by TL configuration --------------------------------------------------------------------------------- **/
	
	 $IsUnlockStatus= & M_MgtLockData::_get_select_active_status();
	 if( !is_null($IsUnlockStatus) AND is_array($IsUnlockStatus) ) {	
		$this->EUI_Page->_setWhereIn('a.deb_call_status_code', $IsUnlockStatus);
    }
   
/** @ pack : look of show status by TL configuration --------------------------------------------------------------------------------- **/
  
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setWherein('a.deb_is_lock',$this->IsLockOpen );
		$this->EUI_Page->_setAnd('a.deb_is_access_all', 0);
		$agent_lock_random = $this->M_MgtRandDeb->getLockedAgentRandom();
		if(count($agent_lock_random)>0)
		{
			if(in_array($this->EUI_Session->_get_session('UserId'), $agent_lock_random ) )
			{
				$this->EUI_Page->_setAnd('b.AssignSelerId',null);
			}
			else
			{
				$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
			}
		}
		else
		{
			$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
		}
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		
	
// @ pack : filter by default is senior leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SENIOR_TL) ) )
	{
		 $this->EUI_Page->_setAnd('ag.stl_id',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'keys_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'keys_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'keys_account_status', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_prev_call_status_code', 'keys_call_status', TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'keys_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'keys_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'keys_cust_name', TRUE);
	
 // @ pack : start date ------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets>='". _getDateEnglish(_get_post('keys_start_date')) ." 00:00:00'", 'keys_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets<='". _getDateEnglish(_get_post('keys_end_date')) ." 23:59:59'", 'keys_end_date', TRUE);

 // @ pack : amount wo  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo>='". _get_post('keys_start_amountwo') ."'", 'keys_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo<='". _get_post('keys_end_amountwo')."'", 'keys_end_amountwo', TRUE);
	
 // @ pack : group_by 
 
 	$this->EUI_Page->_setGroupBy('a.deb_id');
	
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(30);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.deb_is_kept as isKeeptData" => array('isKeeptData', 'isKeep', 'hidden'),
		"a.deb_id AS CustomerId"=> array('CustomerId','ID','primary'),
		"d.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Product'),
		"a.deb_acct_no AS AccountNumber "=> array('AccountNumber','ID Pelanggan'),
		"a.deb_name AS CustomerName"=> array('CustomerName','Nama Pelanggan'),
		"ag.id AS UserId "=> array('UserId','Agent ID'),
		"f.CallReasonDesc AS AccountStatus "=> array('AccountStatus','Akun Status'),
		"h.CallReasonDesc AS CallStatus "=> array('CallStatus','Status Telpon'),
		"a.deb_call_activity_datets AS LastCallDate "=> array('LastCallDate','Tanggal Panggilan Terakhir'),
		"a.deb_resource AS Recsource "=> array('Recsource','Recsource'),
		"a.deb_amount_wo AS AmountWO "=> array('AmountWO','Jumlah WO'),
		"a.deb_bal_afterpay AS BalanceAffterPay "=> array('BalanceAffterPay','Bal. Setelah Bayar'),
		"a.deb_id AS History "=> array('History','History')
	));
	
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.deb_cmpaign_id=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.deb_call_status_code = f.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status h "," a.deb_prev_call_status_code = h.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.deb_cmpaign_id=g.CampaignId","LEFT", TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
 	 $this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	  if($this->EUI_Session->_get_session('HandlingType') == 4) {
		$this->EUI_Page->_setWhereNotin('f.CallReasonCode', array(116, 117,115));
	}
// @ pack : of list OK 
	 
	 $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	
	 
/** @ pack : look parameter amount  **** --------------------------------------------------------------------------------------------- **/
	$Amount =& M_MgtLockData::_get_select_active_amount();
	if( !is_null($Amount) AND is_array($Amount) )foreach( $Amount as $field => $value ){
	  $this->EUI_Page->_setAnd("a.{$field}>={$value[start_amount]}");
	  $this->EUI_Page->_setAnd("a.{$field}<={$value[end_amount]}");
	}
	
 // @ pack : look of show status by TL configuration --------------------------------------------------------------------------------- **/
 
   $IsUnlockStatus=& M_MgtLockData::_get_select_active_status();
   if( !is_null( $IsUnlockStatus) AND is_array( $IsUnlockStatus) ) {	
	   $this->EUI_Page->_setWhereIn('a.deb_call_status_code',  $IsUnlockStatus);
    }
	
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 		
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setWherein('a.deb_is_lock',$this->IsLockOpen );
		$this->EUI_Page->_setAnd('a.deb_is_access_all', 0);
		$agent_lock_random = $this->M_MgtRandDeb->getLockedAgentRandom();
		if(count($agent_lock_random)>0)
		{
			if(in_array($this->EUI_Session->_get_session('UserId'), $agent_lock_random ) )
			{
				$this->EUI_Page->_setAnd('b.AssignSelerId',null);
			}
			else
			{
				$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
			}
		}
		else
		{
			$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
		}
	}		
	
 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		
	
// @ pack : filter by default is senior leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SENIOR_TL) ) )
	{
		 $this->EUI_Page->_setAnd('ag.stl_id',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array( USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'keys_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'keys_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'keys_account_status', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_prev_call_status_code', 'keys_call_status', TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'keys_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'keys_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'keys_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets>='". _getDateEnglish(_get_post('keys_start_date')) ." 00:00:00'", 'keys_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets<='". _getDateEnglish(_get_post('keys_end_date')) ." 23:59:59'", 'keys_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo>=". _get_post('keys_start_amountwo') ."", 'keys_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo<=". _get_post('keys_end_amountwo')."", 'keys_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('a.deb_id');
	// echo "<pre>". $this ->EUI_Page->_getCompiler()."</pre>";	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
 
 public function getListAgent()
{ 
  $this->db->reset_select();
	$this->db->select(' stall.st_access_all_id, agt.id, agt.UserId, agt.full_name, agt.handling_type, agt2.id ',FALSE);
	$this->db->from(' t_st_access_all stall ');
	$this->db->join('t_gn_access_all ttall ',' stall.st_access_all_id = ttall.st_access_all_id', 'INNER');
	$this->db->join('t_gr_user_access utall ',' ttall.access_all_id = utall.access_all_id', 'INNER');
	$this->db->join('t_gn_modul_setup mset ',' stall.modul_setup_id = mset.modul_setup_id', 'INNER');
	$this->db->join('t_tx_agent agt ',' agt.UserId = utall.userid', 'INNER');
	$this->db->join('t_tx_agent agt2 ',' agt2.UserId = mset.mod_set_createdby', 'INNER');
	$this->db->where('mset.modul_id_rnd', 1);
	$this->db->where('mset.mod_set_running', 1);
	$this->db->group_by('utall.userid');
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
		if($rows['handling_type']==13){
			$returns = $this->getListDc($rows['UserId']);
			foreach($returns as $userid => $id){
				$getListDc[$userid] = $id;
			}
			unset($returns);
		}else{
			$getListDc[$rows['UserId']] = $rows['id'];
		}
	}
	
	return $getListDc;
}

public function getListDc($SPVId=null)
{
  $_conds = array();
  if($SPVId){
	  $this->db->reset_select();
		$this->db->select(" a.UserId, a.id ",FALSE);
		$this->db->from(" t_tx_agent a ");
		$this->db->where("a.tl_id", $SPVId);
		$this->db->where("a.handling_type", 4);
		$this->db->where("a.user_state", 1);
	   
		$qry = $this->db->get();
		if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
			$_conds[$rows['UserId']] = $rows['id'];
		}
  }
  
	return $_conds;
}



}

// END OF CLASS 



?>