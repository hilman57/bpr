<?php
class M_MonAgentActivity extends EUI_Model 
{

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function _getAgentStatus($pos=0)
{	
	$_conds = array(0=> "Logout", 1=> "Ready", "Not Ready", "ACW", "Busy");	
	return $_conds[$pos];
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getExtStatus($pos=0)
{	
	$_conds = array(4=>"Offhook", "Ringing", "Dialing", "Talking", "Held", 17=>"Reserved", 25 => "Idle");
	return $_conds[$pos];
}


function UserActivity( $UserId=null )
{
	$Time = 0;
	$this ->db->select("
		UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(b.ext_status_time) as ext_duration, 
		UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(b.status_time) as stat_duration, 
		IF(b.`status` IN(1,2,3), (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(b.ext_status_time)), 0 ) as limit_timer,
		b.ext_status as ExtStatus ",
			
	FALSE);
	$this->db->from('cc_agent a');
	$this->db->join("cc_agent_activity b","a.id= b.agent","LEFT");
	$this->db->join("t_tx_agent c ", "a.userid = c.id","LEFT");
	$this->db->where("c.UserId",$UserId);
	$rows= $this->db->get()->result_first_assoc();
	if( $rows ) {
		$Time = array(
			'timer' => $rows['stat_duration'],
			'milisecond' => $rows['limit_timer'],
			'ExtStatus' => $rows['ExtStatus']
		);
		
	}
	
	return $Time;
	
}


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function _getStore()
{
	$CallActivity = array();
	if($this->EUI_Session->_get_session('HandlingType')== USER_QUALITY_STAFF )
	{ 
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	if($this->EUI_Session->_get_session('HandlingType')== USER_QUALITY_HEAD )
	{ 
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	if($this->EUI_Session->_get_session('HandlingType')== USER_ROOT )
	{ 
		$this-> db ->select(
				'a.userid,  a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_ADMIN)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_QUALITY)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	else if($this->EUI_Session->_get_session('HandlingType')==USER_ACCOUNT_MANAGER)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.act_mgr', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_MANAGER,USER_SUPERVISOR,USER_LEADER,USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
		
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_MANAGER)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$$this-> db ->where('c.mgr_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_SUPERVISOR,USER_LEADER,USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		// $this-> db ->where('c.spv_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_LEADER,USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	else if($this->EUI_Session->_get_session('HandlingType')==USER_LEADER)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.tl_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_SENIOR_TL)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.stl_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND)
	{
		$this-> db ->select(
				'a.userid, a.id as agent_id, b.ext_number, b.status, b.ext_status, 
				 d.reason_desc as ReasonStatus, b.ext_status_time, b.status_time, b.login_time, 
				 unix_timestamp(now()) - unix_timestamp(b.ext_status_time) as ext_duration,
				 unix_timestamp(now()) - unix_timestamp(b.status_time) as stat_duration,remote_number, data as datas,
				 (select a.deb_id as CustomerId from t_gn_debitur a where a.deb_acct_no=b.data) as CustomerId',FALSE);
				 
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	if( $this -> URI->_get_have_post('AgentStatus') )
	{
		if( _get_post('AgentStatus') !='NOL' ){
			$this ->db->where('b.status', _get_post('AgentStatus') );
		}
		else{
			$this ->db->where('b.status', 0);
		}
	}
	$this ->db->order_by('c.full_name','ASC');
// echo $this-> db ->_get_var_dump();

	$i = 0;
	foreach( $this ->db->get()->result_assoc() as $ActivityAgent )
	{	
		$UserId			= $ActivityAgent['userid'];
		$UserName		= $ActivityAgent['name'];
		$remote	   	   	= $ActivityAgent['remote_number'];
		$ExtNumber	   	= $ActivityAgent['ext_number'];
		
		$AgentStatus	= (INT)$ActivityAgent['status'];
		$StatDuration 	= (INT)$ActivityAgent['stat_duration'];
		$ExtDuration  	= (INT)$ActivityAgent['ext_duration'];
		$ExtStatus     	= (INT)$ActivityAgent['ext_status'];
		$StatusTime   	= (INT)$ActivityAgent['status_time'];
		$ReasonStatus  	= $ActivityAgent['ReasonStatus'];
		
		
		if($StatDuration<=0){
			$StatDuration = _getDuration(0);
		}
		
		if($ExtDuration<0) {
			$ExtDuration = _getDuration(0); 
		}
		

		if($ExtStatus==7) {
			$ExtensionStatus = $this -> _getExtStatus($ExtStatus)." with $remote (". _getDuration($ExtDuration)." ) ";
			$CallerData 	 = '<span onclick="Ext.DOM.DetailData(\''. $ActivityAgent['CustomerId'] .'\');" style="cursor:pointer;color:red;font-weight:bold;">'. $ActivityAgent['datas'].'</span>';
			$CallerAction	 = '<span onclick="Ext.DOM.SpyAgent(\''.$this -> EUI_Session->_get_session('agentExt').'\', \''.$ExtNumber.'\')" style="cursor:pointer;color:red;font-weight:bold;"> [ Spy ]</span> '.
							   '<span onclick="Ext.DOM.CoachAgent(\''.$this -> EUI_Session->_get_session('agentExt').'\', \''.$ExtNumber.'\')" style="cursor:pointer;color:red;font-weight:bold;"> [ Coach ]</span>'.
							   '<span onclick="Ext.DOM.HangupAgent(\''. $ActivityAgent['agent_id'] .'\')" style="cursor:pointer;color:red;font-weight:bold;"> [ Hangup ]</span>';
		}
		else if($ExtStatus==5){
			$ExtensionStatus  = $this -> _getExtStatus($ExtStatus) ." ( ". _getDuration($ExtDuration) ." )";
			$CallerData = '';
			$CallerAction = '';
		}
		else{
			$ExtensionStatus  = $this -> _getExtStatus($ExtStatus);
			$CallerData 	  = '';
			$CallerAction  	  = '';
		}
			
		$AgentStatusStr  = null;
		$AgentStatusStr.= $this -> _getAgentStatus($AgentStatus);
		
		
			
		
		// ready  
		if($AgentStatus==1)
			$StatDuration = _getDuration($StatDuration);
		
		// not ready 
		if($AgentStatus==2)
			$StatDuration = _getDuration($StatDuration);
			
		if($AgentStatus==3)
			$StatDuration = _getDuration($StatDuration);	
		
		if($AgentStatus==4)
			$StatDuration = _getDuration($StatDuration);	
			
		// reason 
		
		if(!is_null($ReasonStatus))
			$AgentStatusStr.= " ( $ReasonStatus )";
			
		
		if($AgentStatus == 0)
		{
			$style='color: red;';
			$ExtensionStatus = '';
			$CallerData = '';
			$StatusTime = '';
		}
		else{ 
			$style = 'color:blue;'; 
		}
		
		
		$CallActivity[$i]['UserId']		= (!is_null($UserId) ? $UserId : '-'); 
		$CallActivity[$i]['Name'] 		= (!is_null($UserName) ? $UserName : '-');
		$CallActivity[$i]['Extension']	= (!is_null($ExtNumber) ? $ExtNumber :'-');
		$CallActivity[$i]['Status'] 	= (!is_null($AgentStatusStr) ? $AgentStatusStr : '-' );
		$CallActivity[$i]['Duration'] 	= (!is_null($StatDuration) ? $StatDuration : '-' );
		$CallActivity[$i]['ExtStatus'] 	= (!is_null($ExtensionStatus) ? $ExtensionStatus : '-' );
		$CallActivity[$i]['Data'] 		= (!is_null($CallerData) ? $CallerData : '-' );
		$CallActivity[$i]['Action'] 	= (!is_null($CallerAction)? $CallerAction: '-');
		$CallActivity[$i]['Styles'] 	= (!is_null($style) ? $style: '-');
		
		
		
		$i++;
	}
	
	return $CallActivity;
	
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function _getIndex()
{
	$CallActivity = array();
	
	if($this->EUI_Session->_get_session('HandlingType')== USER_ROOT )
	{ 
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_QUALITY_STAFF)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_ADMIN)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_QUALITY)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_ACCOUNT_MANAGER)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.act_mgr', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_MANAGER,USER_SUPERVISOR,USER_LEADER,USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_MANAGER)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.mgr_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_SUPERVISOR,USER_LEADER,USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		// $this-> db ->where('c.spv_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_LEADER,USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	else if($this->EUI_Session->_get_session('HandlingType')==USER_LEADER)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.tl_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	else if($this->EUI_Session->_get_session('HandlingType')==USER_SENIOR_TL)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where('c.stl_id', $this -> EUI_Session->_get_session('UserId')); 
		$this-> db ->where_in('c.handling_type',array(USER_AGENT_OUTBOUND, USER_AGENT_INBOUND)); 
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
		// echo $this-> db ->_get_var_dump();
	}
	
	else if($this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	else if($this->EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND)
	{
		$this-> db ->select('a.userid as UserId, a.name as UserName',FALSE);
		$this-> db ->from('cc_agent a');
		$this-> db ->join('cc_agent_activity b','a.id = b.agent','LEFT OUTER');
		$this-> db ->join('t_tx_agent c','a.userid = c.id','LEFT OUTER');
		$this-> db ->join('cc_reasons d','b.status_reason=d.reasonid','LEFT');
		$this-> db ->where('c.user_state',1);
		$this-> db ->where_not_in('c.handling_type',array(USER_ROOT));
	}
	
	
	if( $this -> URI->_get_have_post('AgentStatus') ){
		if( _get_post('AgentStatus') !='NOL' ){
			$this ->db->where('b.status', _get_post('AgentStatus') );
		}
		else{
			$this ->db->where('b.status', 0);
		}
	}
	
	
	$this ->db->order_by('c.full_name','ASC');
	
	$i = 0;
	foreach( $this ->db->get()->result_assoc() as $ActivityAgent )
	{
		$CallActivity[$i] = $ActivityAgent; 
		$i++;
	}
	
	return $CallActivity;
	
}


}

?>