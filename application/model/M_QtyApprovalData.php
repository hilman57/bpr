<?php
/*
 * E.U.I 
 * -----------------------------------------------
 *
 * subject	: M_QtyApprovalData
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class M_QtyApprovalData Extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function M_QtyApprovalData()
 {
	$this -> load -> model(array('M_SetCallResult','M_SetResultQuality'));
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 private function _getApprovalInterest()
 {
	$_conds = array();
	if(class_exists('M_SetCallResult'))
	{
		$i = 0;
		foreach( $this -> M_SetCallResult -> _getEventSale() as $k => $rows )
		{
			$_conds[$i] = $k;
			$i++;
		}
	}
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 private function _getQualityConfirm()
 {
	$_conds = array();
	if(class_exists('M_SetResultQuality'))
	{
		$i = 0;
		foreach( $this -> M_SetResultQuality -> _getQualityConfirm() as $k => $rows )
		{
			$_conds[$i] = $k;
			$i++;
		}
	}
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_default()
{
	$this -> EUI_Page->_setPage(10);
	$this -> EUI_Page -> _setQuery
		 (" SELECT 
				a.CustomerId, a.CampaignId, a.CustomerNumber, 
				a.CustomerFirstName, a.CustomerLastName, f.AproveName,
				IF(a.CustomerCity is null,'-',a.CustomerCity) as CustomerCity, 
				a.CustomerUploadedTs, a.CustomerOfficeName, c.CampaignNumber 
			FROM t_gn_debitur a INNER JOIN t_gn_assignment b on a.CustomerId=b.CustomerId 
			LEFT JOIN t_gn_campaign c on a.CampaignId=c.CampaignId 
			LEFT JOIN t_gn_policyautogen d on a.CustomerId=d.CustomerId
			LEFT JOIN t_gn_policy e ON d.PolicyNumber=e.PolicyNumber 
			LEFT JOIN t_lk_aprove_status f ON f.ApproveId=a.CallReasonQue"); 
	
 // filtering 
	$flt = " AND b.AssignAdmin is not null 
			 AND b.AssignMgr is not null 
			 AND b.AssignSpv is not null
			 AND b.AssignBlock=0  
			 AND a.CallReasonQue is not null
			 AND c.CampaignStatusFlag=1";
			 
				 
	$flt.=" AND a.CallReasonId IN('". implode("','", self::_getApprovalInterest())."') ";
	
  // filtring by login 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR )			 
		$flt.=" AND b.AssignSpv ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
	if($this -> EUI_Session -> _get_session('HandlingType')==USER_TELESALES)
		$flt.=" AND b.AssignSelerId ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
 // filtering by session customize 
 
	if( $this -> URI->_get_have_post('cust_call_result') ){ 
		$$flt.=" ANDa.CallReasonId ='".$this -> URI->_get_post('cust_call_result')."'"; 
		$this -> EUI_Session ->replace_session('cust_call_result',$this -> URI->_get_post('cust_call_result'));
	}
	else{
		if(!isset($_REQUEST['cust_call_result'])){
			if($this -> EUI_Session ->_have_get_session('cust_call_result') )
			$$flt.=" AND a.CallReasonId ='".$this -> EUI_Session->_get_session('cust_call_result')."'"; 
		}
		elseif(isset($_REQUEST['cust_call_result']) && $_REQUEST['cust_call_result']==''){
			$this -> EUI_Session ->deleted_session('cust_call_result');	
		}
	}
	
	
	if( $this -> URI->_get_have_post('cust_first_name') ){ 
		$$flt.=" AND a.CustomerFirstName LIKE '%".$this -> URI->_get_post('cust_first_name')."%'"; 
		$this -> EUI_Session ->replace_session('cust_first_name',$this -> URI->_get_post('cust_first_name'));
	}
	else{
		if(!isset($_REQUEST['cust_first_name'])){
			if($this -> EUI_Session ->_have_get_session('cust_first_name') )
			$$flt.=" AND a.CustomerFirstName LIKE '%".$this -> EUI_Session->_get_session('cust_first_name')."%'"; 
		}
		elseif(isset($_REQUEST['cust_first_name']) && $_REQUEST['cust_first_name']==''){
			$this -> EUI_Session ->deleted_session('cust_first_name');	
		}
	}
	
///	**************** ??
	if( $this -> URI->_get_have_post('cust_number_id') ) {
		$$flt.=" AND a.CustomerNumber LIKE '%".$this -> URI->_get_post('cust_number_id')."%'"; 
		$this -> EUI_Session ->replace_session('cust_number_id',$this -> URI->_get_post('cust_number_id'));
	}
	else{
		if(!isset($_REQUEST['cust_number_id'])){
			if($this -> EUI_Session ->_have_get_session('cust_number_id') )
			$$flt.=" AND a.CustomerNumber LIKE '%".$this -> EUI_Session->_get_session('cust_number_id')."%'"; 
		}
		elseif(isset($_REQUEST['cust_number_id']) && $_REQUEST['cust_number_id']==''){
			$this -> EUI_Session ->deleted_session('cust_number_id');	
		}
	}	

///	**************** ??
	
	if( $this -> URI->_get_have_post('cust_campaign_id') ){
		$$flt.=" AND c.CampaignId =".$this -> URI->_get_post('cust_campaign_id');	
		$this -> EUI_Session ->replace_session('cust_campaign_id',$this -> URI->_get_post('cust_campaign_id'));
	}
	else{
		if(!isset($_REQUEST['cust_campaign_id'])){
			if($this -> EUI_Session ->_have_get_session('cust_campaign_id') )
			$$flt.=" AND c.CampaignId ='".$this -> EUI_Session->_get_session('cust_campaign_id')."'";	
		}
		elseif(isset($_REQUEST['cust_campaign_id']) && $_REQUEST['cust_campaign_id']==''){
			$this -> EUI_Session ->deleted_session('cust_campaign_id');	
		}
	}
	
// src category id session 
	
	if( $this -> URI->_get_have_post('category_id_app') ){
		$$flt.=" AND d.ProductId =".$this -> URI->_get_post('category_id_app');	
		$this -> EUI_Session ->replace_session('category_id_app',$this -> URI->_get_post('category_id_app'));
	}
	else{
		if(!isset($_REQUEST['category_id_app'])){
			if($this -> EUI_Session ->_have_get_session('category_id_app') )
			$$flt.=" AND d.ProductId ='".$this -> EUI_Session->_get_session('category_id_app')."'";	
		}
		elseif(isset($_REQUEST['category_id_app']) && $_REQUEST['category_id_app']==''){
			$this -> EUI_Session ->deleted_session('category_id_app');	
		}
	}	
	
///	**************** ??
	
	if( $this -> URI->_get_have_post('cust_start_date') && $this -> URI->_get_have_post('cust_end_date') ){
		$$flt .= " AND date(e.PolicySalesDate) >= '".$this -> EUI_Tools->_date_english($_REQUEST['cust_start_date'])."' 
					 AND date(e.PolicySalesDate) <= '".$this -> EUI_Tools->_date_english($_REQUEST['cust_end_date'])."' "; 
		$this -> EUI_Session ->replace_session('cust_start_date',$this -> URI->_get_post('cust_start_date'));
		$this -> EUI_Session ->replace_session('cust_end_date',$this -> URI->_get_post('cust_end_date'));
		
	}
	else
	{
		if(!isset($_REQUEST['cust_start_date']) && !isset($_REQUEST['cust_end_date']) )
		{
			if($this -> EUI_Session ->_have_get_session('cust_start_date') && $this -> EUI_Session ->_have_get_session('cust_end_date') ){
				$$flt .= " AND date(e.PolicySalesDate) >= '".$this -> EUI_Tools->_date_english( $this -> EUI_Session ->_get_session('cust_start_date'))."' 
							 AND date(e.PolicySalesDate) <= '".$this -> EUI_Tools->_date_english( $this -> EUI_Session ->_get_session('cust_end_date'))."' "; 
			}
		}
		elseif(isset($_REQUEST['cust_start_date']) && ($_REQUEST['cust_start_date']=='') 
			&& (isset($_REQUEST['cust_end_date'])) && ($_REQUEST['cust_end_date']) )
		{
			$this -> EUI_Session ->deleted_session('cust_start_date');	
			$this -> EUI_Session ->deleted_session('cust_end_date');	
		}
	}
	
///	**************** ??
	
	if($this -> URI->_get_have_post('cust_user_id')){
		$$flt.=" AND b.AssignSelerId = '".$this -> URI->_get_post('cust_user_id')."'";
		$this -> EUI_Session ->replace_session('cust_user_id',$this -> URI->_get_post('cust_user_id'));
	}
	else{
		if(!isset($_REQUEST['cust_user_id'])){
			if($this -> EUI_Session ->_have_get_session('cust_user_id') )
			$$flt.=" AND b.AssignSelerId ='".$this -> EUI_Session->_get_session('cust_user_id')."'";	
		}
		elseif(isset($_REQUEST['cust_user_id']) && $_REQUEST['cust_user_id']==''){
			$this -> EUI_Session ->deleted_session('cust_user_id');	
		}
	}	
	
/** cust_approve_status **/

	if($this -> URI->_get_have_post('cust_approve_status')) {
		$filter.=" AND a.CallReasonQue ='".$this -> URI->_get_post('cust_approve_status')."'";	
		$this -> EUI_Session ->replace_session('cust_approve_status',$this -> URI->_get_post('cust_approve_status'));
	}
	else
	{
		if(!isset($_REQUEST['cust_approve_status']))
		{
			if($this -> URI->_get_have_post('cust_approve_status') )
			$filter.=" AND a.CallReasonQue ='".$this -> EUI_Session->_get_session('cust_approve_status')."'";	
		}
		elseif(isset($_REQUEST['cust_approve_status']) && $_REQUEST['cust_approve_status']==''){
			$this -> EUI_Session ->deleted_session('cust_approve_status');	
		}
	}
	
// run filter
	
	$this->EUI_Page->_setWhere( $flt ); 
	$this->EUI_Page->_setGroupBy('a.CustomerId');   
	
	if($this -> EUI_Page -> _get_query())
	{
		return $this -> EUI_Page;
	}	 
	
	
 }
 
 /*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 public function _get_content()
 {
	$this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this -> EUI_Page->_setPage(10);
	$this -> EUI_Page -> _setQuery
		 (" SELECT 
				a.CustomerId, a.CampaignId, a.CustomerNumber, c.CampaignName,
				a.CustomerFirstName, a.CustomerLastName, f.AproveName,
				IF(a.CustomerCity is null,'-',a.CustomerCity) as CustomerCity, 
				a.CustomerUploadedTs, a.CustomerOfficeName, c.CampaignNumber ,
				h.full_name, e.PolicySalesDate
			FROM t_gn_debitur a INNER JOIN t_gn_assignment b on a.CustomerId=b.CustomerId 
			LEFT JOIN t_gn_campaign c on a.CampaignId=c.CampaignId 
			LEFT JOIN t_gn_policyautogen d on a.CustomerId=d.CustomerId
			LEFT JOIN t_gn_policy e ON d.PolicyNumber=e.PolicyNumber 
			LEFT JOIN t_lk_aprove_status f ON f.ApproveId=a.CallReasonQue
			LEFT JOIN t_gn_assignment g on a.CustomerId=g.CustomerId
			LEFT JOIN t_tx_agent h on g.AssignSelerId=h.UserId "); 
	
 // filtering 
	$flt = " AND b.AssignAdmin is not null 
			 AND b.AssignMgr is not null 
			 AND b.AssignSpv is not null
			 AND b.AssignBlock=0  
			 AND a.CallReasonQue is not null
			 AND c.CampaignStatusFlag=1";
			 
				 
	$flt.=" AND a.CallReasonId IN('". implode("','", self::_getApprovalInterest())."') ";
	
  // filtring by login 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR )			 
		$flt.=" AND b.AssignSpv ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
	if($this -> EUI_Session -> _get_session('HandlingType')==USER_TELESALES)
		$flt.=" AND b.AssignSelerId ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
 // filtring by keep session 
	
	
	if( $this -> URI->_get_have_post('cust_call_result') ) 
		$flt.=" AND a.CallReasonId ='".$this -> URI->_get_post('cust_call_result')."'"; 
	
	if( $this -> URI->_get_have_post('cust_first_name') ) 
		$flt.=" AND a.CustomerFirstName LIKE '%".$this -> URI->_get_post('cust_first_name')."%'"; 
	
	if( $this -> URI->_get_have_post('cust_number_id') ) 
		$flt.=" AND a.CustomerNumber LIKE '%".$this -> URI->_get_post('cust_number_id')."%'"; 
		
	
	if( $this -> URI->_get_have_post('cust_campaign_id') )
		$flt.=" AND c.CampaignId =".$this -> URI->_get_post('cust_campaign_id');	
	
	if( $this -> URI->_get_have_post('category_id_app') )
		$flt.=" AND d.ProductId =".$this -> URI->_get_post('category_id_app');	

	if( $this -> URI->_get_have_post('cust_start_date') && $this ->URI ->_get_have_post('cust_end_date') )
		$flt .= " AND date(e.PolicySalesDate)>= '".$this ->EUI_Tools ->_date_english($this ->URI ->_get_post('cust_start_date'))."' 
				  AND date(e.PolicySalesDate)<= '".$this ->EUI_Tools ->_date_english($this ->URI ->_get_post('cust_end_date'))."' "; 
	
	
	if($this -> URI->_get_have_post('cust_user_id'))
		$flt.=" AND b.AssignSelerId = '".$this -> URI->_get_post('cust_user_id')."'";
		

    if( $this -> URI->_get_have_post('cust_approve_status'))
		$flt .=" AND a.CallReasonQue ='".$this -> URI->_get_post('cust_approve_status')."'"; 
		
	$this->EUI_Page->_setWhere($flt);  
	$this->EUI_Page->_setGroupBy('a.CustomerId');   
	$this->EUI_Page->_setLimit();
	
 }
 
 
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_resource()
 {
	self::_get_content();
	
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 
}
?>