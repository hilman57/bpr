<?php
/*
 * E.U.I 
 * --------------------------------------------------------------------------
 *
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/QualityAssignment/index/?
 * ----------------------------------------------------------------------------
 */
 
 
class M_QualityAssignment extends EUI_Model
{

 var $perpage = 10;

/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
 
 public function M_QualityAssignment(){ 
	$this -> load -> model('M_QtyScoring');
 }
 
 
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
 
 public function _getQualityGroup()
 {
	$_group = array();
	
	$this -> db -> select("c.*");
	$this -> db -> from("t_gn_quality_group a ");
	$this -> db -> join("t_lk_quality_skill b"," a.Quality_Skill_Id=b.Quality_Skill_Id","LEFT");
	$this -> db -> join("t_tx_agent c ","a.Quality_Staff_id=c.UserId","LEFT");
	$this -> db -> where("a.Quality_Skill_Id", QUALITY_APPROVE );
	
	// login identified not root 
	
	if( $this -> EUI_Session->_get_session('HandlingType')!=USER_ROOT){
		$this -> db -> where("c.quality_id", $this -> EUI_Session -> _get_session('UserId') );
	}
	
	foreach( $this -> db ->get() -> result_assoc() as $rows )
	{
		$_group[$rows['UserId']] = $rows['full_name']; 
	}
	
	return $_group;

 }
 
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
  
  
public function _getRecord()
{
	$count = 0; 
	
	// get campaign outbound 
	
	$CampaignOutbound = $this -> M_SetCampaign -> _getCampaignGoals(2);
	
	$this -> db -> select("COUNT(a.AssignId) as jumlah", FALSE);
	$this -> db -> from("t_gn_assignment a ");
	$this -> db -> join("t_gn_debitur b ","a.CustomerId=b.CustomerId","LEFT");
	$this -> db -> join("t_gn_campaign c", "b.CampaignId=c.CampaignId","LEFT");
	$this -> db -> join("t_tx_agent d", "a.AssignSelerId=d.UserId","LEFT");
	$this -> db -> join("t_gn_quality_assignment e ", "a.AssignId = e.Assign_Data_Id","LEFT");
	$this -> db -> where("e.Assign_Data_Id IS NULL");
	
	// get all data closing 
	
	$this -> db -> where_in("b.CallReasonId",$this -> M_QtyScoring -> Sale());

	// if campaign ok is Outbound Only 
	
	$this -> db -> where_in("b.CampaignId", array_keys($CampaignOutbound ));
	if( $this -> URI->_get_have_post('CampaignId'))
	{
		$this -> db -> where_in("b.CampaignId", $this -> URI->_get_array_post('CampaignId'));	
	}
	
	
	// if start_date 
	
	if( $this -> URI->_get_have_post('start_date') 
		AND $this -> URI->_get_have_post('end_date') )
	{
		//$this -> db -> where_in("b.CampaignId", $this -> URI->_get_array_post('CampaignId'))	
	}
		
	if( $rows = $this -> db -> get() -> result_first_assoc() ){
		$count = $rows['jumlah'];
	}
	
	return $count;
}


 
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
  
  
public function _getPage()
{
	$Records = $this -> _getRecord();
	$Pages   = ceil($Records /$this -> perpage);
	$ul_array = array();
	
	for( $i = 1;  $i<=$Pages; $i++){	
		$_counter = $i;
		$ul_array[$i]= $_counter;	
	}
	return array(
		'records' => $Records,
		'pages' =>	$ul_array
	);
}
 

 
 
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
  
 public function _getDataByChecked()
 {
	$result = array();
	// get campaign outbound 
	$CampaignOutbound = $this -> M_SetCampaign -> _getCampaignGoals(2);
	
	$this -> db -> select("a.AssignId, b.*, c.*, d.full_name, e.Assign_Data_Id");
	$this -> db -> from("t_gn_assignment a ");
	$this -> db -> join("t_gn_debitur b ","a.CustomerId=b.CustomerId","LEFT");
	$this -> db -> join("t_gn_campaign c", "b.CampaignId=c.CampaignId","LEFT");
	$this -> db -> join("t_tx_agent d", "a.AssignSelerId=d.UserId","LEFT");
	$this -> db -> join("t_gn_quality_assignment e ", "a.AssignId = e.Assign_Data_Id","LEFT");
	$this -> db -> where("e.Assign_Data_Id IS NULL");
	
	/* get all data closing **/
	
	$this -> db -> where_in("b.CallReasonId",$this -> M_QtyScoring -> Sale());
	
	/* if campaign ok is Outbound Only */
	$this -> db -> where_in("b.CampaignId", array_keys($CampaignOutbound));
	
	if( $this -> URI->_get_have_post('CampaignId') ){
		$this -> db -> where_in("b.CampaignId", $this -> URI->_get_array_post('CampaignId'));	
	}
	
	/* if start_date */
	
	if( $this -> URI->_get_have_post('start_date') 
		AND $this -> URI->_get_have_post('end_date') )
	{
		//$this -> db -> where_in("b.CampaignId", $this -> URI->_get_array_post('CampaignId'))	
		
	}
		
	
	$start = 0; 
	
	if( $this -> URI->_get_have_post('start_page') )
	{
		$start_pages = (INT)$this -> URI->_get_post('start_page');
		
		if( $start_pages > 0 )
			$start = ((($start_pages)-1) * $this -> perpage); 
		else
			$start = 0;
	}
	
	/* set limit show data **/
	
	$this -> db -> limit($this -> perpage ,$start);
	
	$num = $start+1;
	foreach( $this -> db ->get() -> result_assoc() as $rows ) 
	{
		$result[$num] = $rows; 
		$num++;
	}
	
	return $result;
	
 }
 
  
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
  
 public function _getDataByAmount()
 {
	return $this -> _getRecord();
 }

 
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : AssignId (ARRAY);
 * @ param : QualityStaffId ( ARRAY)
 */
 
public function _setAssignByChecked( $AssignId = null , $QualityStaffId = null )
{
	$_conds = 0;
	if( !is_null($AssignId) AND !is_null($QualityStaffId)  )
	{
		$jumlah_data = count($AssignId);
		$jumlah_user = count($QualityStaffId);
		$jumlah_per_staff_quality = (INT)(($jumlah_data)/($jumlah_user));
		
		if( $jumlah_per_staff_quality ) 
		{
			$start = 1; $list_assign = array();
			for( $user = 0;  $user < $jumlah_user; $user++)
			{
				$offset = ( (($start)-1) * $jumlah_per_staff_quality);
					$list_assign[$QualityStaffId[$user]] = array_slice($AssignId, $offset, $jumlah_per_staff_quality);
				$start++;
			}
			// if OK bagi 
			foreach( $list_assign as $Quality_Staff_Id => $rows ) {
			  $_conds+= $this -> _setAssignment($Quality_Staff_Id, $rows );	
			}	
		}
	}
	
	return $_conds;
}

/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : AssignId (ARRAY);
 * @ param : QualityStaffId ( ARRAY)
 */
 
 private function _setAssignment( $Quality_Staff_Id = null, $Assign_Data_Id = null )
 {
	$_conds = 0;
	if(!is_null($Quality_Staff_Id) 
		AND !is_null($Assign_Data_Id)) foreach($Assign_Data_Id AS $key => $AssignId )
	{
		$this -> db -> set('Quality_Staff_Id',$Quality_Staff_Id);
		$this -> db -> set('Assign_Data_Id',$AssignId);
		$this -> db -> set('Assign_Create_By', $this -> EUI_Session->_get_session('UserId'));
		$this -> db -> set('Assign_Create_Ts',date('Y-m-d H:i:s'));
		
		$this -> db -> insert('t_gn_quality_assignment');
		if( $this -> db ->affected_rows() > 0 ) {
			$_conds++;
		}
	}
	
	return $_conds;
 }
 

 
/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : $AssignSizeData (INT);
 * @ param : QualityStaffId ( ARRAY)
 */
 
public function _setAssignByAmount( $AssignSizeData = 0 , $QualityStaffId = null )
{
	$_conds = 0;
	if( !is_null($AssignSizeData) 
		AND ($AssignSizeData!=FALSE) 
		AND !is_null($QualityStaffId) 
		AND $QualityStaffId!=FALSE )
	{
	
		$assign_array = array(); $List_data = $this -> _getDataByChecked();
		$i = 0;
		
		if(is_array($List_data) ) foreach( $List_data as $key => $rows ){
			$assign_array[$i] = $rows['AssignId'];
			$i++;
		}
		
	// cek valid off array 
	
		if( is_array($assign_array) 
			AND count($assign_array) > 0 )
		{
			$AssignId 	 = array_slice($assign_array, 0, $AssignSizeData);
			$jumlah_data = count($AssignId);
			$jumlah_user = count($QualityStaffId);
			$jumlah_per_staff_quality = (INT)(($jumlah_data)/($jumlah_user));
			
			if( $jumlah_per_staff_quality ) 
			{
				$start = 1; $list_assign = array();
				for( $user = 0;  $user < $jumlah_user; $user++)
				{
					$offset = ( (($start)-1) * $jumlah_per_staff_quality);
						$list_assign[$QualityStaffId[$user]] = array_slice($AssignId, $offset, $jumlah_per_staff_quality);
					$start++;
				}
				// if OK bagi 
				foreach( $list_assign as $Quality_Staff_Id => $rows ) 
				{
				  $_conds+= $this -> _setAssignment($Quality_Staff_Id, $rows );	
				}	
			}
		}
		
	}
	
	return $_conds;
}
 
 

}
?>