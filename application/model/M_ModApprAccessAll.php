<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_ModApprAccessAll extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_ModApprAccessAll() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser','M_SetCampaign','M_ModDistribusi'));
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("b.deb_id");

	$this->EUI_Page->_setFrom("t_gn_akses_all a");
	$this->EUI_Page->_setJoin("t_gn_debitur b","a.akses_deb_id = b.deb_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign c","b.deb_cmpaign_id = c.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent d","a.akses_claim_by = d.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent e","a.akses_existing_agent_id = e.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_lk_aprove_status f","a.approve_status = f.AproveCode","LEFT",TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('a.akses_all_flag',1);
	 $this->EUI_Page->_setAnd('a.akses_claim_by IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('a.akses_type <> 0', FALSE);
 	 $this->EUI_Page->_setAnd('a.akses_target_id IS NOT NULL',FALSE);
	 $this->EUI_Page->_setAnd('a.approve_status',104);
 	
	// $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	 
	 
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'claim_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'claim_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'claim_account_status', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'claim_call_status', TRUE);
	// $this->EUI_Page->_setAndCache('ag.UserId', 'claim_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_resource', 'claim_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'claim_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets>='". _getDateEnglish(_get_post('claim_start_date')) ."'", 'claim_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets<='". _getDateEnglish(_get_post('claim_end_date')) ."'", 'claim_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo>=". (INT)_get_post('claim_start_amountwo') ."", 'claim_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo<=". (INT)_get_post('claim_end_amountwo')."", 'keep_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('b.deb_id');
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	
	
	$this->EUI_Page->_setArraySelect(array(
		"b.deb_id AS CustomerId"=> array('CustomerId','ID','primary'),
		"c.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Product'),
		"b.deb_acct_no AS AccountNumber "=> array('AccountNumber','Customer ID'),
		"b.deb_name AS CustomerName"=> array('CustomerName','Customer Name'),
		"d.id AS AgentClaim"=>array('AgentClaim','Agent Claim'),
		"e.id AS OldAgent"=>array('OldAgent','Old Agent'),
		"a.akses_claim_dateTs AS DateClaim"=>array('DateClaim','Date Claim'),
		"b.deb_reminder AS History "=> array('History','History'),
		"f.AproveName AS ApprovalStatus"=>array('ApprovalStatus','Approval Status')
	));

	$this->EUI_Page->_setFrom("t_gn_akses_all a");
	$this->EUI_Page->_setJoin("t_gn_debitur b","a.akses_deb_id = b.deb_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign c","b.deb_cmpaign_id = c.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent d","a.akses_claim_by = d.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent e","a.akses_existing_agent_id = e.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_lk_aprove_status f","a.approve_status = f.AproveCode","LEFT",TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('a.akses_all_flag',1);
	 $this->EUI_Page->_setAnd('a.akses_claim_by IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('a.akses_type <> 0', FALSE);
 	 $this->EUI_Page->_setAnd('a.akses_target_id IS NOT NULL',FALSE);
	 $this->EUI_Page->_setAnd('a.approve_status',104);
 	
	// $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	 
	 
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'claim_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'claim_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'claim_account_status', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'claim_call_status', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_resource', 'claim_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'claim_cust_name', TRUE);
	// $this->EUI_Page->_setAndCache('ag.UserId', 'claim_agent_id', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets>='". _getDateEnglish(_get_post('claim_start_date')) ."'", 'claim_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets<='". _getDateEnglish(_get_post('claim_end_date')) ."'", 'claim_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo>=". (INT)_get_post('claim_start_amountwo') ."", 'claim_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo<=". (INT)_get_post('claim_end_amountwo')."", 'keep_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('b.deb_id');
		
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }

	
	/*
	 * @ def 		: update_approval_status //  
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	public function UpdateRejectStatus($key='',$data)
	{
		
		if($key!='')
		{
			$this->db->set("approve_status",$data);
			$this->db->where(array('akses_deb_id'=>$key,'akses_all_flag'=>1));
			if( $this->db->update("t_gn_akses_all") )
			{
				return TRUE;
			}
		}
	}
	
	/*
	 * @ def 		: update_approval_status //  
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	public function UpdateApproveStatus ($data=array())
	{
		$_conds['Message']= 1;
		$_conds['success'] = 0;
		$_conds['fail']=0;
		foreach ($data AS $debiturid => $array_value)
		{
			$AssignId = $this->get_assign_id($debiturid);
			$this->db->set("approve_status",$array_value['code']);
			$this->db->set("akses_all_flag",0);
			$this->db->where("akses_deb_id",$debiturid);
			if( $this->db->update("t_gn_akses_all") )
			{
				$this->db->set("AssignSelerId",$array_value['claim']['agent_id']);
				$this->db->set("AssignLeader",$array_value['claim']['tl_id']);
				$this->db->set("AssignMode",'CLM');
				$this->db->set("AssignDate",date('Y-m-d H:i:s'));
				$this->db->where($AssignId);
				if( $this->db->update("t_gn_assignment") )
				{
					if($this->M_ModDistribusi->_setSaveLog( array( 
							'AssignId' => $AssignId['AssignId'], 
							'UserId' => $array_value['claim']['agent_id'], 
							'tl_id' => $array_value['claim']['tl_id'], 
							'assign_status'=>'CLM'
						 ),'ASSIGN.CLAIM' )) 
					{
						$this->db->set("deb_agent",$array_value['claim']['agent_code']);
						$this->db->set("deb_is_access_all",0);
						$this->db->where("deb_id",$debiturid);
						if( $this->db->update("t_gn_debitur") )
						{
							$_conds['success']++;
						}
						else 
						{
							$_conds['fail']++;
						}
					}
					
				}
				else 
				{
					$this->db->set("deb_is_access_all",0);
					$this->db->where("deb_id",$debiturid);
					if( $this->db->update("t_gn_debitur") )
					{
						$_conds['success']++;
					}
					else 
					{
						$_conds['fail']++;
					}
				}
			}
			else
			{
				$_conds['fail']++;
			}
			
		}
		return $_conds;
		
	}

	/**
	**
	** get_assign_id
	**/
	private function get_assign_id($CustomerId = 0)
	{
		$this->db->reset_select();
		$this->db->select("a.AssignId");
		$this->db->from("t_gn_assignment a");
		$this->db->where("CustomerId",$CustomerId);
		
		$qry = $this->db->get();
		if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) 
		{
			$data['AssignId'] = $rows['AssignId'];
		}
		return $data;
		
	}
	/*
	 * @ def 		: update_approval_status //  
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition get_claim_id 
	 * @ return 	: void(0)
	 */
	public function get_claim_id($key=null)
	{
		$data = array();
		$this->db->reset_select();
		$this->db->select("	a.akses_deb_id,
							a.akses_claim_by,b.id,
							b.tl_id AS claim_tl");
		$this->db->from("t_gn_akses_all a");
		$this->db->join('t_tx_agent b', 'a.akses_claim_by = b.UserId', 'INNER');
		
		if( !is_null($key) )
		{	
			if( is_array($key) ) {
				$this->db->where_in("a.akses_deb_id", $key);
			} else {
				$this->db->where_in("a.akses_deb_id", array($key));
			}	
		}
		 
		$qry = $this->db->get();
		if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) 
		{
			$data[$rows['akses_deb_id']]['agent_id'] = $rows['akses_claim_by'];
			$data[$rows['akses_deb_id']]['tl_id'] = $rows['claim_tl'];
			$data[$rows['akses_deb_id']]['agent_code'] = $rows['id'];
		}
		return $data;
		
	}

}

// END OF CLASS 



?>