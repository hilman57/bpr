<?php
class M_SaveMemo extends EUI_Model
{
	private static $Instance = null;

	/*
	 * @ pack : instance 
	 */
	public static function &Instance(){
		if( is_null(self::$Instance) ){
			self::$Instance = new self();
		}
	return self::$Instance;
	}

	public function _getNotes(){
		
		$conds = array();
		$UserId = $this -> EUI_Session -> _get_session('UserId');
		
		$this->db->reset_select();
		$this->db->select('a.NoteId, a.Note');
		$this->db->from('t_gn_note a');
		$this->db->where('a.UserId', $UserId);
		$this->db->order_by('a.NoteId','DESC');
		$this->db->limit(1);
		// echo $this->db->_get_var_dump();
		if($rs = $this->db->get() -> result_first_assoc() ){
			$conds = $rs;
		}
		return $conds;
	}
	
	function _updateMemo($NoteId, $Note, $UserId){
		$query = "UPDATE t_gn_note SET Note = '$Note',
 			UserId= $UserId, UpdateDate = NOW() WHERE
 			NoteId = $NoteId;";
		$this->db->query($query);
	}
	
	function _insertMemo($Note,$UserId){
		$query = "INSERT INTO t_gn_note (Note,UserId,Insertdate) VALUES
 			('$Note', $UserId, NOW());";
		$this->db->query($query);
	}
 
}
?>
