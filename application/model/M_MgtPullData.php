<?php
/*
 * @def : M_MgtPullData
 * -------------------------------
 * 
 * @param : Unit Test 
 * @param : Unit Test
 */
 
class M_MgtPullData 
	extends EUI_Model
{

 
// -------------------------------------
/*
 * @ pack : static properties  
 */
 
 private static $Instance = NULL;

// -------------------------------------
/*
 * @ pack : Instance of class  
 */

 public static function &Instance()
{
 if( is_null( self::$Instance) )
 {
	self::$Instance = new self();
 }
 
 return self::$Instance;
 
}

 
// ----------------------------------------------
/*
 * @ pack : selected data assignment 
 */
 
 public function __construct() 
{ 
  $this->load->model(array('M_SysUser','M_ModDistribusi','M_ModOutBoundGoal','M_Combo','M_MgtBucket', 'M_SetCampaign', 'M_SrcCustomerList')); 
	
}
 
// ----------------------------------------------
/*
 * @ pack : selected data assignment 
 */

 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.deb_id");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.deb_cmpaign_id=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.deb_call_status_code = f.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.deb_cmpaign_id=g.CampaignId","LEFT", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	

 // @ pack : filter by default session ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	$this->EUI_Page->_setWherein('a.deb_is_lock',$this->IsLockOpen );
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}
	
// @ pack : filter by default is senior leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SENIOR_TL) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSeniorLeader',$this->EUI_Session->_get_session('UserId'));
	}
	
	// @ pack : filter by default is seniorleader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SENIOR_TL) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSeniorLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'transfer_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'transfer_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'transfer_account_status', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_prev_call_status_code', 'transfer_call_status', TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'transfer_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'transfer_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'transfer_cust_name', TRUE);
	
 // @ pack : start date ------------------------------------------------------
	$this->EUI_Page->_setAndOrCache("DATE(a.deb_call_activity_datets)>='". _getDateEnglish(_get_post('transfer_start_date')) ."'", 'transfer_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("DATE(a.deb_call_activity_datets)<='". _getDateEnglish(_get_post('transfer_end_date')) ."'", 'transfer_end_date', TRUE);

 // @ pack : amount wo  ---------------------------------------------------------------------------------
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo>='". _get_post('transfer_start_amountwo') ."'", 'transfer_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo<='". _get_post('transfer_end_amountwo')."'", 'transfer_end_amountwo', TRUE);
	
 // @ pack : group_by 
 	$this->EUI_Page->_setGroupBy('a.deb_id');
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
} // ==> get_default
 
 
// ----------------------------------------------
/*
 * @ pack : selected data assignment 
 */
 
 public function _get_content()
{
	
 $this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
 $this->EUI_Page->_setPage(10);
 $this->EUI_Page->_setArraySelect(array(
	"a.deb_id as DebiturId" => array("DebiturId","DebiturId", "hidden"),
	"a.deb_is_kept as isKeeptData" => array('isKeeptData', 'isKeep', 'hidden'),
	"b.AssignId AS AssignId"=> array('AssignId','ID','primary'),
	"d.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Produk'),
	"a.deb_acct_no AS AccountNumber "=> array('AccountNumber','Costumer ID'),
	"a.deb_name AS CustomerName"=> array('CustomerName','Nama Pengguna'),
	"ag.id AS UserId "=> array('UserId','Agent ID'),
	"cg.id AS TeamleaderId "=> array('TeamleaderId','Team Leader'),
	"f.CallReasonDesc AS AccountStatus "=> array('AccountStatus','Status Akun'),
	"h.CallReasonDesc AS CallStatus "=> array('CallStatus','Panggilan terakhir'),
	"a.deb_call_activity_datets AS LastCallDate "=> array('LastCallDate','Tanggal Panggilan terakhir'),
	"a.deb_resource AS Recsource "=> array('Recsource','Recsource'),
	"a.deb_amount_wo AS AmountWO "=> array('AmountWO','Jumlah WO'),
	"a.deb_bal_afterpay AS BalanceAffterPay "=> array('BalanceAffterPay','Bal. Afterpay'),
	"IF( a.deb_is_kept IN(1), 'On Process',NULL) AS KeeptData "=> array('KeeptData','Keept Data'),
	"a.deb_reminder AS History "=> array('History','History') ));
	
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent cg","cg.UserId=b.AssignLeader ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.deb_cmpaign_id=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.deb_call_status_code = f.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status h "," a.deb_prev_call_status_code = h.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.deb_cmpaign_id=g.CampaignId","LEFT", TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
 	 $this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	 
// @ pack : of list OK 
	 
	 $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	 $this->EUI_Page->_setWherein('a.deb_is_lock',$this->IsLockOpen );
	
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 		
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}
	
	// @ pack : filter by default is SENIOR leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SENIOR_TL) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSeniorLeader',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'transfer_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'transfer_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'transfer_account_status', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_prev_call_status_code', 'transfer_call_status', TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'transfer_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'transfer_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'transfer_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("DATE(a.deb_call_activity_datets)>='". _getDateEnglish(_get_post('transfer_start_date')) ."'", 'transfer_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("DATE(a.deb_call_activity_datets)<='". _getDateEnglish(_get_post('transfer_end_date')) ."'", 'transfer_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo>=". _get_post('transfer_start_amountwo') ."", 'transfer_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo<=". _get_post('transfer_end_amountwo')."", 'transfer_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('a.deb_id');
		
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	//echo $this->EUI_Page->_getCompiler();
	
	
} // ==> _get_content 


// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 public function _get_swap_content_asign( $AssignId  = null )
{
	if( is_array($AssignId)  AND count($AssignId) == 0 ) return false;
	
	$this->db->reset_select();
	$this->db->select("a.AssignId as AssignId, a.CustomerId as DebiturId", FALSE);
	$this->db->from("t_gn_assignment a ");
	$this->db->where_in("a.AssignId", $AssignId);
	$rs = $this->db->get();
	if( $rs->num_rows() > 0 ) {
		return $rs->result_assoc();
	}
	return false;
}	
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 public function _get_swap_content( $out, $compose = null )
{
 
 $this->array_result = array();
 $this->db->reset_select();
 if( is_null( $compose ) )
 {
	$this->db->select(" 
		b.AssignId as AssignId,
		a.deb_id as DebiturId, 
		a.deb_acct_no as AccountNo, 
		a.deb_name as CustomerName, 
		a.deb_amount_wo as AmountWO, 
		upper(a.deb_agent) as AgentId,
		c.full_name as Fullname, 
		d.CallReasonDesc as AccountStatus, 
		a.deb_call_activity_datets  as CallDate,
		e.CampaignCode as Product,
		ts.CallReasonDesc as LastCallStatus,
		a.deb_bal_afterpay as BalAfterPay,
		a.deb_id as HistoryId,
		(SELECT tg.UserId  FROM t_tx_agent tg 
		 WHERE tg.id=a.deb_agent) as DebAgentId", 
	FALSE); 
 } else {
	$this->db->select($compose, FALSE); 
 }
 
 $this->db->from("t_gn_debitur a");
 $this->db->join("t_gn_assignment b "," a.deb_id=b.CustomerId","LEFT");
 $this->db->join("t_tx_agent c "," b.AssignSelerId=c.UserId","LEFT");
 $this->db->join("t_lk_account_status d", "a.deb_call_status_code=d.CallReasonCode","LEFT");
 $this->db->join("t_gn_campaign e", "a.deb_cmpaign_id=e.CampaignId","LEFT");
 $this->db->join("t_gn_campaign_project f ","f.CampaignId=e.CampaignId","LEFT");
 $this->db->join("t_lk_account_status ts ","a.deb_prev_call_status_code=ts.CallReasonCode","LEFT");
  
 if(_get_session('HandlingType')==USER_LEADER){
	$this->db->where('b.AssignLeader',_get_session('UserId'));	
 }
 //filter for senior team leader
 if(_get_session('HandlingType')==USER_SENIOR_TL){
	$this->db->where('b.AssignSeniorLeader',_get_session('UserId'));	
 }
 
// --------------- filter data  ----------------------------------
  
 if( _get_have_post('swp_from_call_start_date') &&  
	 _get_have_post('swp_from_call_end_date') ) 
 {
	$this->db->where("a.deb_call_activity_datets>='". $out->get_value('swp_from_call_start_date','StartDate') ."'", "", false);
	$this->db->where("a.deb_call_activity_datets<='". $out->get_value('swp_from_call_end_date','EndDate') ."'", "", false);
	
 }
 
 if( _get_have_post('swp_from_amount_start')) 
 {
	$this->db->where("a.deb_amount_wo>='". $out->get_value('swp_from_amount_start','intval') ."'", "", false);
 }
 
 if( _get_have_post('swp_from_amount_end') )
 {
	$this->db->where("a.deb_amount_wo<='". $out->get_value('swp_from_amount_end','intval') ."'", "", false); 
 }
 
 if( _get_have_post('swp_from_campaign_id') )
 {
	$this->db->where_in('e.CampaignId', $out->get_array_value('swp_from_campaign_id','intval'));
 }
 
 if( _get_have_post('swp_from_customer_id') )
 {
	$this->db->or_like_group("a.deb_acct_no",$out->get_array_value('swp_from_customer_id'));
 }
  
 if( _get_have_post('swp_from_account_status') ){
	$this->db->where_in('a.deb_call_status_code', $out->get_array_value('swp_from_account_status','intval') );
 }
 
 if( _get_have_post('swp_from_call_status') ){
	$this->db->where_in('a.deb_prev_call_status_code', $out->get_array_value('swp_from_call_status','intval') );
 }
  
 if( _get_have_post('swp_from_leader_id') ){
	$this->db->where_in('b.AssignLeader', $out->get_array_value('swp_from_leader_id', 'intval') );
 }
 
 if( _get_have_post('swp_from_deskoll_id') ){
	$this->db->where_in('b.AssignSelerId', $out->get_array_value('swp_from_deskoll_id','intval') );
 } 
 else {
	if( _get_have_post('swp_from_leader_id') ){
		$this->db->where_in('b.AssignSelerId', $out->get_array_value('swp_from_leader_id', 'intval') );
	}
 }
 
 
 if( _get_have_post('orderby') ){
	$this->db->order_by(_get_post('orderby'),_get_post('type'));
 }	

 if( _get_have_post('swp_from_spv_id') ){
	$arr_spv = implode(",", $out->get_array_value('swp_from_spv_id','intval'));
	$this->db->having("DebAgentId IN ($arr_spv)", "", FALSE);
}

//echo $this->db->print_out();
 
$qry = $this->db->get();
if( $qry->num_rows() > 0 ) {
	$this->array_result  = $qry->result_assoc();
 }
 
 return $this->array_result;
 
}
 
 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
public function _getUserLevel( $LevelId )
 {
 
 $UserList = array();
 
  if(in_array($this ->EUI_Session ->_get_session('HandlingType'), 
	array
	( 
		  USER_ROOT, USER_ADMIN, 
		  USER_MANAGER, USER_SUPERVISOR, 
		  USER_LEADER, USER_QUALITY,
		  USER_SENIOR_TL,
		  USER_ACCOUNT_MANAGER, USER_BACK_OFFICE, 
		  USER_QUALITY_STAFF, USER_QUALITY_HEAD
	)))
	{
		$_SESSION_USERID = $this -> EUI_Session->_get_session('UserId');
		
		$this -> db ->select('a.*');
		$this -> db ->from('t_tx_agent a');
		$this -> db ->join('cc_agent b ',' a.id=b.userid','INNER');
		$this -> db -> where('a.user_state', 1);
		$this -> db -> where('a.handling_type', $LevelId);
		
		// admin 
		
		if( $this ->EUI_Session ->_get_session('HandlingType')==USER_ADMIN){
			$this -> db -> where('a.admin_id', $_SESSION_USERID);	
		}
		if( $this ->EUI_Session ->_get_session('HandlingType')==USER_ACCOUNT_MANAGER){
			$this -> db -> where('a.act_mgr', $_SESSION_USERID);	
		}
		// manager 
		
		if( $this ->EUI_Session ->_get_session('HandlingType')==USER_MANAGER){
			$this -> db -> where('a.mgr_id', $_SESSION_USERID);	
		}
		// supervisor 
		
		if( $this ->EUI_Session ->_get_session('HandlingType')==USER_SUPERVISOR){
			$this -> db -> where('a.spv_id', $_SESSION_USERID);	
		}
		
		// leader 
		
		if( $this ->EUI_Session ->_get_session('HandlingType')==USER_LEADER){
			$this -> db -> where('a.tl_id', $_SESSION_USERID);	
		}
		
		//senior leader
		if( $this ->EUI_Session ->_get_session('HandlingType')==USER_SENIOR_TL){
			$this -> db -> where('a.stl_id', $_SESSION_USERID);	
		}
		
		$this->db->order_by("a.full_name");
		// other condistion && run on here.. :);
		
		// the run of query 
		//echo $this -> db -> _get_var_dump();
		$list = 0;
		foreach( $this -> db -> get() -> result_assoc() as $rows ){
			$UserList[$list] = $rows; $list++;	
		}	
	}
	
	return $UserList;
	
 }
 
 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
function _setSwapData( $SellerId=NULL, $AssignId=NULL ) 
{

// set zero success 

 $total_success = 0;
 
// calculation data && user
 
  $count_sellerid = count($SellerId);
  $count_assignid = count($AssignId);
  $count_page = floor(($count_assignid/$count_sellerid));
  
// define off columns data 
 
 $COLUMNS_MAPS = array( 
	USER_ADMIN => 'AssignAdmin', USER_MANAGER => 'AssignMgr', 
	USER_ACCOUNT_MANAGER => 'AssignAmgr', USER_SUPERVISOR =>'AssignSpv',  
	USER_LEADER => 'AssignLeader', USER_AGENT_INBOUND  => 'AssignSelerId',
	USER_SENIOR_TL => 'AssignSeniorLeader', USER_AGENT_INBOUND  => 'AssignSelerId',
	USER_AGENT_OUTBOUND => 'AssignSelerId' 
 );
	
// cek capability data 

if( count($count_page) > 0 )  
{
	$start = 0; $list_by_user = array(); $i = 0;
	foreach( $SellerId as $k => $UserId ) 
	{
		if( $i==0 )	
			$start = ($i * $count_page);
		else
			$start = ($i * $count_page); 
	 
  // set to list data 
	 
	 $list_by_user[$UserId] = array_slice($AssignId,$start,$count_page); $i++;	
   }
		
 // set to agent 	
	if( count($list_by_user) > 0 )  
	{
		$Levels = $this -> URI ->_get_post('UserLevel');
		foreach($list_by_user as $ID => $arrResult ) 
		{
			$rows = $this -> M_SysUser -> _getUserDetail($ID);
			if( is_array($rows) )
			{
				foreach( $arrResult as $k => $idxAssignId )
				{
					if(in_array( $Levels , array_keys($COLUMNS_MAPS) ) ) 
					{
						$this -> db -> set($COLUMNS_MAPS[$Levels],$rows['UserId'],FALSE);
					
					// level AGENT ------------------------------>
					
						if( ($Levels == USER_AGENT_INBOUND) 
							OR ($Levels== USER_AGENT_OUTBOUND) )
						{
							$this->db->set('AssignAdmin',$rows['admin_id']);
							
							/*
							 * $this->db->set('AssignMgr', $rows['mgr_id']);  
							 * $this->db->set('AssignAmgr', $rows['act_mgr']);  
							 * $this->db->set('AssignSpv', $rows['spv_id']); 
							 */
							 
							$this->db->set('AssignLeader', $rows['tl_id']);
							$this->db->set('AssignDate', date('Y-m-d H:i:s') );
							$this->db->set('AssignMode', 'MOV');
						}	
						
					// level LEADER --------------------------------------->
					
						
						if( ($Levels == USER_LEADER) ) 
						{
							
						   /*
							* $this -> db -> set('AssignAdmin',$rows['admin_id']);
							* $this -> db -> set('AssignMgr', $rows['mgr_id']);  
							* $this -> db -> set('AssignSpv', $rows['spv_id']); 
							*/
							
							$this->db->set('AssignSelerId','NULL',FALSE);
							$this->db->set('AssignDate', date('Y-m-d H:i:s') );
							$this->db->set('AssignMode', 'MOV');
						}
					
					// level SENIOR LEADER --------------------------------------->
					
						
						if( ($Levels == USER_SENIOR_TL) ) 
						{
							
						   /*
							* $this -> db -> set('AssignAdmin',$rows['admin_id']);
							* $this -> db -> set('AssignMgr', $rows['mgr_id']);  
							* $this -> db -> set('AssignSpv', $rows['spv_id']); 
							*/
							
							$this->db->set('AssignSelerId','NULL',FALSE);
							$this->db->set('AssignDate', date('Y-m-d H:i:s') );
							$this->db->set('AssignMode', 'MOV');
						}
						
					// level SUPERVISOR --------------------------------------->
						
						if( ($Levels == USER_SUPERVISOR ) ) 
						{
							$this->db->set('AssignAdmin',$rows['admin_id']);
							$this->db->set('AssignMgr', $rows['mgr_id']);  
							$this->db->set('AssignSelerId','NULL',FALSE);
							$this->db->set('AssignLeader', 'NULL',FALSE);
							$this->db->set('AssignDate', date('Y-m-d H:i:s') );
							$this->db->set('AssignMode', 'MOV');
						}
						
						
					// level MANAGAER --------------------------------------->
					
						if( ($Levels == USER_MANAGER )) 
						{
							$this->db->set('AssignAdmin',$rows['admin_id']);
							$this->db->set('AssignSpv', 'NULL', FALSE); 
							$this->db->set('AssignSelerId','NULL',FALSE);
							$this->db->set('AssignLeader', 'NULL',FALSE);
							$this->db->set('AssignDate', date('Y-m-d H:i:s') );
							$this->db->set('AssignMode', 'MOV');
						}
						
					// level MANAGAER --------------------------------------->
						
						if( ($Levels == USER_ADMIN )) 
						{
							$this->db->set('AssignMgr', 'NULL',FALSE);  
							$this->db->set('AssignSpv', 'NULL',FALSE); 
							$this->db->set('AssignLeader', 'NULL',FALSE);
							$this->db->set('AssignDate', date('Y-m-d H:i:s') );
							$this->db->set('AssignMode', 'MOV');
						}
					 }
			// then update data assignment 
			
					$this->db->where('AssignId', $idxAssignId, FALSE);
					if( $this->db->update('t_gn_assignment')) {	
						if($this->M_ModDistribusi->_setSaveLog( array( 
							'AssignId' => $idxAssignId, 
							'UserId' => $rows['UserId'], 
							'tl_id' => $rows['tl_id'], 
							'spv_id' => $rows['spv_id'],
							'agent_code' => $rows['Username'],
							'assign_status'=>'MOV'
						 ),'TRANSFER.BY_CHECK' )) 
						{
							$total_success+=1;
						}	
					}	
				}
			}
		}
	 }	
  }
  
  return $total_success;
  
 }
 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
function _setSwapAmountData($param)
{
	$total = array('data'=>0,'agent'=>0);
	
	$counter = 0;
	$AssignId = $this->_getListMoveData($param);
	$SellerId = $param['ToUserId'];
	
	/* var_dump($AssignId); */
	// print_r($param);
	// exit(0);
	
	$COLUMNS_MAPS = array( 
		USER_ADMIN => 'AssignAdmin', USER_MANAGER => 'AssignMgr', 
		USER_ACCOUNT_MANAGER => 'AssignAmgr', USER_SUPERVISOR =>'AssignSpv',  
		USER_LEADER => 'AssignLeader', USER_AGENT_INBOUND  => 'AssignSelerId',
		USER_SENIOR_TL => 'AssignSeniorLeader', USER_AGENT_INBOUND  => 'AssignSelerId',
		USER_AGENT_OUTBOUND => 'AssignSelerId' 
	 );
	
	$Levels = $this -> URI ->_get_post('UserLevel');
	
	$total_agent = count($SellerId);
	
	if( count($AssignId) < $total_agent )
	{
		$total['agent'] = count($AssignId);
	}
	else{
		$total['agent'] = $total_agent;
	}
	
	$rows = $this -> M_SysUser -> _getUserInDetail(implode(',',$SellerId));
	
	/* print_r($rows);
	
	exit(0); */
	if(count($rows) > 0)
	{
		foreach($AssignId as $id)
		{
			if($counter > $total_agent-1)
			{
				$counter = 0;
			}
			
			if(in_array( $Levels , array_keys($COLUMNS_MAPS) ) ) 
			{
				$UserCaller = $rows[$counter];
				
				$this -> db -> set($COLUMNS_MAPS[$Levels],$UserCaller['UserId'],FALSE);
			
			// level AGENT ------------------------------>
			
				if( ($Levels == USER_AGENT_INBOUND) 
					OR ($Levels== USER_AGENT_OUTBOUND) )
				{
					$this -> db -> set('AssignAdmin',$UserCaller['admin_id']);
					$this -> db -> set('AssignMgr', $UserCaller['mgr_id']);  
					$this -> db -> set('AssignAmgr', $UserCaller['act_mgr']);  
					$this -> db -> set('AssignSpv', $UserCaller['spv_id']); 
					$this -> db -> set('AssignLeader', $UserCaller['tl_id']);
					$this -> db -> set('AssignDate', date('Y-m-d H:i:s') );
					$this -> db -> set('AssignMode', 'MOV');
				}	
				
			// level LEADER --------------------------------------->
			
				if( ($Levels == USER_LEADER) ) 
				{
					$this -> db -> set('AssignAdmin',$UserCaller['admin_id']);
					$this -> db -> set('AssignMgr', $UserCaller['mgr_id']);  
					$this -> db -> set('AssignSpv', $UserCaller['spv_id']); 
					$this -> db -> set('AssignSelerId','NULL',FALSE);
					$this -> db -> set('AssignDate', date('Y-m-d H:i:s') );
					$this -> db -> set('AssignMode', 'MOV');
				}
			
			// level senior LEADER --------------------------------------->
			
				if( ($Levels == USER_SENIOR_TL) ) 
				{
					$this -> db -> set('AssignAdmin',$UserCaller['admin_id']);
					$this -> db -> set('AssignMgr', $UserCaller['mgr_id']);  
					$this -> db -> set('AssignSpv', $UserCaller['spv_id']); 
					$this -> db -> set('AssignSelerId','NULL',FALSE);
					$this -> db -> set('AssignDate', date('Y-m-d H:i:s') );
					$this -> db -> set('AssignMode', 'MOV');
				}
				
			// level SUPERVISOR --------------------------------------->
				
				if( ($Levels == USER_SUPERVISOR ) ) 
				{
					$this -> db -> set('AssignAdmin',$UserCaller['admin_id']);
					$this -> db -> set('AssignMgr', $UserCaller['mgr_id']);  
					$this -> db -> set('AssignSelerId','NULL',FALSE);
					$this -> db -> set('AssignLeader', 'NULL',FALSE);
					$this -> db -> set('AssignDate', date('Y-m-d H:i:s') );
					$this -> db -> set('AssignMode', 'MOV');
				}
				
				
			// level MANAGAER --------------------------------------->
				if( ($Levels == USER_MANAGER )) 
				{
					$this -> db -> set('AssignAdmin',$UserCaller['admin_id']);
					$this -> db -> set('AssignSpv', 'NULL', FALSE); 
					$this -> db -> set('AssignSelerId','NULL',FALSE);
					$this -> db -> set('AssignLeader', 'NULL',FALSE);
					$this -> db -> set('AssignDate', date('Y-m-d H:i:s') );
					$this -> db -> set('AssignMode', 'MOV');
				}
				
			// level MANAGAER --------------------------------------->
				if( ($Levels == USER_ADMIN )) {
					$this -> db -> set('AssignMgr', 'NULL',FALSE);  
					$this -> db -> set('AssignSpv', 'NULL',FALSE); 
					$this -> db -> set('AssignLeader', 'NULL',FALSE);
					$this -> db -> set('AssignDate', date('Y-m-d H:i:s') );
					$this -> db -> set('AssignMode', 'MOV');
				}
			}
			
			$this -> db -> where('AssignId', $id['AssignId'], FALSE);
			$this -> db -> update('t_gn_assignment');
			
			if( $this -> db -> affected_rows() ) 
			{
				if( $this -> M_ModDistribusi -> _setSaveLog(
					array(
						'AssignId' => $id['AssignId'], 
						'UserId'   => $UserCaller['UserId'] ) , 'TRANSFER_DATA.BY_AMOUNT'
				))
				{
					$total['data']++;
				}	
			}
			
			$counter++;
		}
	}
	
	return $total;
 }
 

 // -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
function _setSwapLoger( $arr_deb = array(), $arr_new = array() )
{
	
	$this->db->reset_write();
	
	$this->db->set("assign_dc_id_new", $arr_new['assign_dc_id_new'] );
	$this->db->set("assign_tl_id_new", $arr_new['assign_tl_id_new'] );
	$this->db->set("assign_spv_id_new", $arr_new['assign_spv_id_new'] );
	$this->db->set("deb_id", $arr_deb['deb_id'] );
	$this->db->set("assign_id", $arr_deb['assign_id']);
	$this->db->set("assign_status", $arr_deb['assign_status']);
	$this->db->set("assign_type", $arr_deb['assign_type']);
	$this->db->set("assign_log_created_ts", date('Y-m-d H:i:s'));
	$this->db->set("assign_log_create_by", _get_session('UserId'));
	$this->db->insert("t_gn_assignment_log");
	
} 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
 function _setSwapDataByAmount( $out )
{
  $this->asng   = array();	
  $this->user   = array();
  $this->rows   = $this->_get_swap_content($out, null);
  $this->amount = $out->get_value('swp_to_data_amount', 'intval');
   
  if( count($this->rows) ==0  ) {
	  return false;
  }
  
  if( is_array($this->rows)) 
	  foreach( $this->rows as $no => $row )
 {
	  $this->asng[] = array( 'AssignId' => $row['AssignId'], 'deb_id' => $row['DebiturId']);
  }
  
 $this->asng = array_slice($this->asng, 0, $this->amount);
 if( $out->get_value('swap_methode') == 1){ 
	shuffle($this->asng); 
 }
  
// tl   
 if( $out->get_value('swp_to_user_level') == 13 ){
	$this->asg_user = $out->get_array_value('swp_to_user_leader');
 }
 
// stl   
 if( $out->get_value('swp_to_user_level') == 14 ){
	$this->asg_user = $out->get_array_value('swp_to_senior_leader');
 }
 
// deskol  
 if( $out->get_value('swp_to_user_level') == 4 ){
	$this->asg_user = $out->get_array_value('swp_to_user_deskoll');
 }
 
// spv  
 if( $out->get_value('swp_to_user_level') == 3 ){
	$this->asg_user = $out->get_array_value('swp_to_user_spv');
 }
 
 
 $this->data_per_user =floor((count($this->asng)/count($this->asg_user)));
 $this->data_asg_user = array();
 
 
 $start = 0;
 if( is_array($this->asg_user) ) 
  foreach( $this->asg_user as $num => $UserId )  
{
	$offset = ( $start * $this->data_per_user);
	$this->data_asg_user[$UserId] = array_slice($this->asng, $offset, $this->data_per_user);
	$start++;
 }
 
 if( !is_array($this->data_asg_user) 
	 OR count($this->data_asg_user) == 0 ) 
 {
	 return false;
 }
 
 
// step out 
 
 $process = 0;
 
 $Level = $out->get_value('swp_to_user_level');
 foreach( $this->data_asg_user as $UserId => $row )
 {
	$arr_users = $this->M_SysUser->_getUserInDetail($UserId);
	$objUser = new EUI_Object(reset($arr_users));
	
	
	// ---------- loger ----------------
	
	
	
	if( $Level == 13 )  // to leader 
		foreach($row as $n => $values ) 
	{
		$xls = new EUI_Object($values);
		
		$this->_setSwapLoger(array(
			'deb_id' => $xls->get_value('deb_id'), 
			'assign_id' => $xls->get_value('AssignId'),
			'assign_type' => 'ASSIGN.SWAP',
			'assign_status' => 'MOV'
		), array(
			'assign_dc_id_new' => NULL, 
			'assign_tl_id_new' => $objUser->get_value('UserId', 'intval'),
			'assign_spv_id_new' => $objUser->get_value('spv_id', 'intval')
		));
		
		
		$this->db->reset_write();
		$this->db->where("AssignId", $xls->get_value('AssignId'));
		$this->db->set('AssignAdmin',$objUser->get_value('admin_id', 'intval'));
		$this->db->set('AssignMgr', $objUser->get_value('mgr_id', 'intval'));  
		$this->db->set('AssignSpv', $objUser->get_value('spv_id', 'intval')); 
		$this->db->set('AssignLeader', $objUser->get_value('UserId', 'intval')); 
		$this->db->set('AssignSelerId',$objUser->get_value('UserId', 'intval'));
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->set('AssignMode', 'MOV');
		
		 if( $this->db->update("t_gn_assignment")) 
		{
			$this->db->reset_write();
			$this->db->set("deb_swap_count", "((deb_swap_count)+1)", FALSE);
			$this->db->set("deb_last_swap_ts", date('Y-m-d H:i:s'));
			$this->db->set("deb_agent", $objUser->get_value('Username'));
			$this->db->where("deb_id", $xls->get_value('deb_id'));
			$this->db->update("t_gn_debitur");
			$process++;
		}
	}
	
	if( $Level == 4 )  // to agent deskoll  
		foreach($row as $n => $values ) 
	{
		$xls = new EUI_Object($values);
		
		$this->_setSwapLoger(array(
			'deb_id' => $xls->get_value('deb_id'), 
			'assign_id' => $xls->get_value('AssignId'),
			'assign_type' => 'ASSIGN.SWAP',
			'assign_status' => 'MOV'
		), array(
			'assign_dc_id_new' => $objUser->get_value('UserId', 'intval'), 
			'assign_tl_id_new' => $objUser->get_value('tl_id', 'intval'),
			'assign_spv_id_new' => $objUser->get_value('spv_id', 'intval')
		));
		
		$this->db->reset_write();
		$this->db->where("AssignId", $xls->get_value('AssignId'));
		$this->db->set('AssignAdmin',$objUser->get_value('admin_id', 'intval'));
		$this->db->set('AssignMgr', $objUser->get_value('mgr_id', 'intval'));  
		$this->db->set('AssignSpv', $objUser->get_value('spv_id', 'intval')); 
		$this->db->set('AssignLeader', $objUser->get_value('tl_id', 'intval')); 
		$this->db->set('AssignSelerId',$objUser->get_value('UserId', 'intval'));
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->set('AssignMode', 'MOV');
		
		 if( $this->db->update("t_gn_assignment")) 
		{
			$this->db->reset_write();
			$this->db->set("deb_swap_count", "((deb_swap_count)+1)", FALSE);
			$this->db->set("deb_last_swap_ts", date('Y-m-d H:i:s'));
			$this->db->set("deb_agent", $objUser->get_value('Username'));
			$this->db->where("deb_id", $xls->get_value('deb_id'));
			$this->db->update("t_gn_debitur");
			$process++;
		}
	}
	
	if( $Level == 3 )  // to agent SPV 
		foreach($row as $n => $values ) 
	{
		$xls = new EUI_Object($values);
		
		$this->_setSwapLoger(array(
			'deb_id' => $xls->get_value('deb_id'), 
			'assign_id' => $xls->get_value('AssignId'),
			'assign_type' => 'ASSIGN.SWAP',
			'assign_status' => 'MOV'
		), array(
			'assign_dc_id_new' => $objUser->get_value('UserId', 'intval'), 
			'assign_tl_id_new' => $objUser->get_value('tl_id', 'intval'),
			'assign_spv_id_new' => $objUser->get_value('spv_id', 'intval')
		));
		
		$this->db->reset_write();
		$this->db->where("AssignId", $xls->get_value('AssignId'));
		$this->db->set('AssignSelerId',$objUser->get_value('UserId'));
		$this->db->set('AssignSpv', 0); 
		$this->db->set('AssignLeader', 0); 
		
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->set('AssignMode', 'MOV');
		
		 if( $this->db->update("t_gn_assignment")) 
		{
			$this->db->reset_write();
			$this->db->set("deb_swap_count", "((deb_swap_count)+1)", FALSE);
			$this->db->set("deb_last_swap_ts", date('Y-m-d H:i:s'));
			$this->db->set("deb_agent", $objUser->get_value('Username'));
			$this->db->where("deb_id", $xls->get_value('deb_id'));
			$this->db->update("t_gn_debitur");
			$process++;
		}
	}
 }
 
 return $process;	
} 


// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
 function _setSwapDataByChecked( $out )
{
  
  $this->asng   = array();	
  $this->user   = array();
  $this->rows   = $this->_get_swap_content_asign($out->get_array_value('AssignId'));
  $this->amount = $out->get_value('swp_to_data_amount', 'intval');
  
  if( count($this->rows) ==0  ) {
	  return false;
  }
  
  if( is_array($this->rows)) 
	  foreach( $this->rows as $no => $row )
 {
	  $this->asng[] = array( 'AssignId' => $row['AssignId'], 'deb_id' => $row['DebiturId']);
  }
  
 $this->asng = array_slice($this->asng, 0, $this->amount);
 if( $out->get_value('swap_methode') == 1){ 
	shuffle($this->asng); 
 }
  
 // leader  
 if( $out->get_value('swp_to_user_level') == 13 ){
	$this->asg_user = $out->get_array_value('swp_to_user_leader');
 }
 
// seniorleader  
 if( $out->get_value('swp_to_user_level') == 14 ){
	$this->asg_user = $out->get_array_value('swp_to_senior_leader');
 } 
 
 // deskoll 
 if( $out->get_value('swp_to_user_level') == 4 ){
	$this->asg_user = $out->get_array_value('swp_to_user_deskoll');
 }
 
 // spv 
  if( $out->get_value('swp_to_user_level') == 3 ){
	$this->asg_user = $out->get_array_value('swp_to_user_spv');
 }
 
 $this->data_per_user =floor((count($this->asng)/count($this->asg_user)));
 $this->data_asg_user = array();
 
 
 $start = 0;
 if( is_array($this->asg_user) ) 
  foreach( $this->asg_user as $num => $UserId )  
{
	$offset = ( $start * $this->data_per_user);
	$this->data_asg_user[$UserId] = array_slice($this->asng, $offset, $this->data_per_user);
	$start++;
 }
 
 if( !is_array($this->data_asg_user) 
	 OR count($this->data_asg_user) == 0 ) 
 {
	 return false;
 }
 
 
// step out 
 
 $process = 0;
 
 $Level = $out->get_value('swp_to_user_level');
 foreach( $this->data_asg_user as $UserId => $row )
 {
	$arr_users = $this->M_SysUser->_getUserInDetail($UserId);
	$objUser = new EUI_Object(reset($arr_users));
	
	
	// ---------- loger ----------------
	
	
	
	if( $Level == 13 )  // to leader 
		foreach($row as $n => $values ) 
	{
		$xls = new EUI_Object($values);
		
		$this->_setSwapLoger(array(
			'deb_id' => $xls->get_value('deb_id'), 
			'assign_id' => $xls->get_value('AssignId'),
			'assign_type' => 'ASSIGN.SWAP',
			'assign_status' => 'MOV'
		), array(
			'assign_dc_id_new' => NULL, 
			'assign_tl_id_new' => $objUser->get_value('UserId', 'intval'),
			'assign_spv_id_new' => $objUser->get_value('spv_id', 'intval')
		));
		
		
		$this->db->reset_write();
		$this->db->where("AssignId", $xls->get_value('AssignId'));
		$this->db->set('AssignAdmin',$objUser->get_value('admin_id', 'intval'));
		$this->db->set('AssignMgr', $objUser->get_value('mgr_id', 'intval'));  
		$this->db->set('AssignSpv', $objUser->get_value('spv_id', 'intval')); 
		$this->db->set('AssignLeader', $objUser->get_value('UserId', 'intval')); 
		$this->db->set('AssignSelerId',$objUser->get_value('UserId', 'intval'));
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->set('AssignMode', 'MOV');
		
		 if( $this->db->update("t_gn_assignment")) 
		{
			$this->db->reset_write();
			$this->db->set("deb_swap_count", "((deb_swap_count)+1)", FALSE);
			$this->db->set("deb_last_swap_ts", date('Y-m-d H:i:s'));
			$this->db->set("deb_agent", $objUser->get_value('Username'));
			$this->db->where("deb_id", $xls->get_value('deb_id'));
			$this->db->update("t_gn_debitur");
			$process++;
		}
	}
	
	if( $Level == 4 )  // to agent deskoll  
		foreach($row as $n => $values ) 
	{
		$xls = new EUI_Object($values);
		
		$this->_setSwapLoger(array(
			'deb_id' => $xls->get_value('deb_id'), 
			'assign_id' => $xls->get_value('AssignId'),
			'assign_type' => 'ASSIGN.SWAP',
			'assign_status' => 'MOV'
		), array(
			'assign_dc_id_new' => $objUser->get_value('UserId', 'intval'), 
			'assign_tl_id_new' => $objUser->get_value('tl_id', 'intval'),
			'assign_spv_id_new' => $objUser->get_value('spv_id', 'intval')
		));
		
		$this->db->reset_write();
		$this->db->where("AssignId", $xls->get_value('AssignId'));
		$this->db->set('AssignAdmin',$objUser->get_value('admin_id', 'intval'));
		$this->db->set('AssignMgr', $objUser->get_value('mgr_id', 'intval'));  
		$this->db->set('AssignSpv', $objUser->get_value('spv_id', 'intval')); 
		$this->db->set('AssignLeader', $objUser->get_value('tl_id', 'intval')); 
		$this->db->set('AssignSelerId',$objUser->get_value('UserId', 'intval'));
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->set('AssignMode', 'MOV');
		
		 if( $this->db->update("t_gn_assignment")) 
		{
			$this->db->reset_write();
			$this->db->set("deb_swap_count", "((deb_swap_count)+1)", FALSE);
			$this->db->set("deb_last_swap_ts", date('Y-m-d H:i:s'));
			$this->db->set("deb_agent", $objUser->get_value('Username'));
			$this->db->where("deb_id", $xls->get_value('deb_id'));
			$this->db->update("t_gn_debitur");
			$process++;
		}
	}
	
	if( $Level == 3 )  // to agent SPV 
		foreach($row as $n => $values ) 
	{
		$xls = new EUI_Object($values);
		
		$this->_setSwapLoger(array(
			'deb_id' => $xls->get_value('deb_id'), 
			'assign_id' => $xls->get_value('AssignId'),
			'assign_type' => 'ASSIGN.SWAP',
			'assign_status' => 'MOV'
		), array(
			'assign_dc_id_new' => $objUser->get_value('UserId', 'intval'), 
			'assign_tl_id_new' => $objUser->get_value('tl_id', 'intval'),
			'assign_spv_id_new' => $objUser->get_value('spv_id', 'intval')
		));
		
		
		$this->db->reset_write();
		$this->db->where("AssignId", $xls->get_value('AssignId'));
		$this->db->set('AssignSelerId',$objUser->get_value('UserId'));
		$this->db->set('AssignSpv', 0); 
		$this->db->set('AssignLeader', 0); 
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->set('AssignMode', 'MOV');
		
		 if( $this->db->update("t_gn_assignment")) 
		{
			$this->db->reset_write();
			$this->db->set("deb_swap_count", "((deb_swap_count)+1)", FALSE);
			$this->db->set("deb_last_swap_ts", date('Y-m-d H:i:s'));
			$this->db->set("deb_agent", $objUser->get_value('Username'));
			$this->db->where("deb_id", $xls->get_value('deb_id'));
			$this->db->update("t_gn_debitur");
			$process++;
		}
	}
 }
 
 
 return $process;	
} 

// ================= END CLASS ======================================= 
 
}
