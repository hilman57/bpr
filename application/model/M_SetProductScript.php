<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SetProductScript extends EUI_Model
{
	
var $set_limit_page = 10;

// ----------------------------------------------------------------------
/*
 * @ package		function get detail content list page 
 * @ param			not assign parameter
 */		
 
private static $Instance   = null; 
 public static function &Instance()
{
	if( is_null(self::$Instance) ){
		self::$Instance = new self();
	}	
	return self::$Instance;
}

// ----------------------------------------------------------------------
/*
 * @ package		function get detail content list page 
 * @ param			not assign parameter
 */	
 
function __construct() {
  $this->load->model(array('M_SetProduct','M_SetPrefix'));
}


// ----------------------------------------------------------------------
/*
 * @ package		function get detail content list page 
 * @ param			not assign parameter
 */	
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery(" SELECT a.CampaignCode, a.CampaignDesc FROM t_gn_campaign a
			 LEFT JOIN t_gn_campaign_script b on a.CampaignId=b.CampaignId
			 LEFT JOIN t_tx_agent c on b.UploadBy=c.UserId"); 
	
	$flt = '';
	if( $this -> URI -> _get_have_post('keywords'))
	{
		$flt.= " AND (
				b.ProductCode LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR b.ProductName LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.ScriptFlagStatus LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR c.full_name LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.ScriptFileName LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.Description LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.ScriptUpload LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.UploadDate	LIKE '%{$this -> URI->_get_post('keywords')}%' 
			)";	
	}				
  
	$this -> EUI_Page->_setWhere($flt);	

	// echo $this->EUI_Page->_getCompiler();	

	if($this -> EUI_Page -> _get_query())
	{
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
  $this -> EUI_Page ->_setQuery
		(
			"SELECT  b.CampaignCode,  b.CampaignDesc, a.ScriptFileName, 
			a.Description, a.ScriptUpload, a.UploadDate, a.ScriptId,a.UploadBy,a.ScriptFlagStatus 
			FROM t_gn_campaign_script  a
			LEFT JOIN t_gn_campaign b on a.CampaignId=b.CampaignId
			" );
  
  $flt = '';
  if( $this -> URI -> _get_have_post('keywords') ) {
	$flt.= " AND ( a.ScriptFlagStatus LIKE '%{$this -> URI->_get_post('keywords')}%' 
			
			OR a.ScriptFileName LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.Description LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.ScriptUpload LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.UploadDate	LIKE '%{$this -> URI->_get_post('keywords')}%' 
		)";	
  }				
  
  $this -> EUI_Page->_setWhere($flt);
  
  if($this -> URI -> _get_have_post('order_by') )
  {
	$this -> EUI_Page->_setOrderBy($this -> URI -> _get_post('order_by'),$this -> URI -> _get_post('type'));	
  }
  $this -> EUI_Page->_setLimit();

  // echo $this->EUI_Page->_getCompiler();	
}
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }

 function _get_campaign_name()
{
	$datas = array();
	
	$this -> db ->select("a.CampaignId, a.CampaignName");
	$this -> db ->from("t_gn_campaign a ");
	$this -> db ->join("t_lk_outbound_goals b", "a.OutboundGoalsId=b.OutboundGoalsId","LEFT");
	$this -> db ->where("a.CampaignStatusFlag",1);
	$this -> db ->where("Name", "outbound");
	
	foreach( $this -> db ->get() ->result_assoc() as $rows )
	{
        // print_r($this->db->last_query());
		$datas[$rows['CampaignId']] = $rows['CampaignName'];
	}
	
	return $datas;
}
 

	function Flags()  {
		return array('0' => 'Not Active', '1' => 'Active');
	}
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setUpload( $_post =array() )
{
	// echo "<pre>";
	// var_dump($_post['post_files']['ScriptFileName']);
	// die;
   $this->db->set('CampaignId', $_post['post_data']['CampaignId']);
   $this->db->set('ScriptFileName', $_post['post_files']['ScriptFileName']['name']);
   $this->db->set('ScriptUpload', $_post['post_files']['ScriptFileName']['name']);
   $this->db->set('Description', $_post['post_data']['ScriptTitle']);
   $this->db->set('ScriptFlagStatus', $_post['post_data']['Active']);
   $this->db->set('UploadDate', $this -> EUI_Tools ->_date_time());
   $this->db->set('UploadBy', $this -> EUI_Session ->_get_session('UserId'));
   
   $this ->db->insert('t_gn_campaign_script');
//    var_dump($this->db->query());
//    die;
   
   if( $this ->db->affected_rows() > 0 ) 
	   return true;
   else
	   return false;
}


//  function _setUpload( $_post= array() )
//  {
// 	if( $this -> db -> insert('t_gn_campaign_script',
// 		array
// 		(
// 			'ProductId' => $_post['post_data']['ProductName'], 
// 			'ScriptFileName'=> $_post['post_files']['ScriptFileName']['name'], 
// 			'Description'=> $_post['post_data']['ScriptTitle'], 
// 			'ScriptFlagStatus'=> $_post['post_data']['Active'], 
// 			'ScriptUpload'=> $_post['post_files']['ScriptFileName']['name'],
// 			'UploadDate'=> $this -> EUI_Tools ->_date_time(),
// 			'UploadBy'=> $this -> EUI_Session ->_get_session('UserId')
// 		)
// 	)){
// 		return true;
// 	}
// 	else{
// 		return false;
// 	}
	
//  }
 
  
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setActive($_post = null)
 {
	$_conds = 0;
	if(!is_null($_post))
	{
		foreach($_post['ScriptId'] as $keys => $ScriptId )
		{
			if( $this -> db -> update('t_gn_campaign_script', 
			 array( 'ScriptFlagStatus'=>$_post['Flags']), 
			 array( 'ScriptId' => $ScriptId)))
			{
				$_conds+=1;	
			}
		}
	}
	
	return $_conds;
 }

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setDelete($_post = null)
 {
	$_conds = 0;
	if(!is_null($_post))
	{
		foreach($_post['ScriptId'] as $keys => $ScriptId )
		{
			if( $this -> db -> delete('t_gn_campaign_script',array('ScriptId'=>$ScriptId)))
			{
				$_conds+=1;	
			}
		}
	}
	
	return $_conds;
 }
 
// ------------------------------------------------------------------ 
/* 
 * @ package 	select all product under campaign selected .
 */
 
 public function _select_row_campaign_product( $ProductId  = null )
{
   $arr_campaign = array();
   if( !is_null($ProductId) 
	   OR ($ProductId ==0) 
	   OR ($ProductId==='') )
  {
	 return (array)$arr_campaign;
  }
  
 //  -------------- next proces  ------------------------------
 
  if( !is_array($ProductId) ){
	  $ProductId = array($ProductId);
  }
  
  $this->db->reset_select();
  $this->db->where_in('ProductId', $ProductId);
  $this->db->select("CampaignId", FALSE);
  $this->db->from("t_gn_campaignproduct");
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows ) 
 {
	$arr_campaign[$rows['CampaignId']] = (int)$rows['CampaignId'];
	
  }
  return (array)$arr_campaign;
  
} 
 
// ------------------------------------------------------------------ 
/* 
 * @ package 	select all product under campaign selected .
 */
 
 public function _select_row_product_campaign( $CampaignId  = null )
{
   $arr_product = array();  
  if( is_null($CampaignId)  OR ($CampaignId ==0) OR ($CampaignId == '' ) )
  {
	 return (array)$arr_product;
  }
  
 //  -------------- next proces  ------------------------------
 
  if( !is_array($CampaignId) ){
	  $CampaignId = array($CampaignId);
  }
  
  $this->db->reset_select();
  $this->db->where_in('CampaignId', $CampaignId);
  $this->db->select("ProductId", FALSE);
  $this->db->from("t_gn_campaignproduct");
 
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows ) 
 {
	$arr_product[$rows['ProductId']]	 = (int)$rows['ProductId'];
	
  }
  return (array)$arr_product;
  
} 
 
// --------------------------------------------------------------- 
/*
 * @ package 		 _get_page_number // constructor class 
 * @ return 	: void(0)
 */
 
 public function _getScript( $CampaignId = null )
{
 
 $ProductId = array();
 
 // if( !is_null($CampaignId) AND (int)$CampaignId > 0 ) {
	$ProductId =& $this->_select_row_product_campaign( $CampaignId );
 // }
 // echo $CampaignId;
 // print_r($this->_select_row_product_campaign( $CampaignId ));
 
 $arr_script_agent = array();
 $this->db->reset_select();
 $this->db-> select("a.ScriptId , a.Description,
	( SELECT pr.ProductName FROM t_gn_campaign pr WHERE pr.ProductId=a.ProductId ) 
	As ProductName", FALSE);
		
 $this->db->from('t_gn_campaign_script a');
 $this->db->join('t_gn_campaign b','a.ProductId=b.CampaignId','LEFT');
 $this->db->where('a.ScriptFlagStatus', 1);
 
// -------- if have data posted -----------------------------------
 
  // if( is_array($ProductId) AND count( $ProductId ) > 0 ) { 
	// $this->db->where_in('b.CampaignId', $CampaignId);
  // }
  
// ---- debug  echo $this->db->print_out(); ------------------
 // echo $this->db->print_out();
 $rs = $this->db->get();
 if($rs->num_rows() > 0 ) 
	 foreach( $rs->result_assoc() as $rows )
 {
	$arr_script_agent[$rows['ScriptId']] = $rows['Description'];
  }
  
 return (array)$arr_script_agent;
}
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getDataScript($ScriptId=0)
 {	
	$_conds = array();
	$this->db->reset_select();
	$this->db->select('a.*');
	$this->db->from('t_gn_campaign_script a');
	$this->db->where('a.ScriptId',$ScriptId);
	if( $rows = $this -> db->get()-> result_first_assoc() ) {
		// print_r($this->db->last_query());
		$_conds = $rows;
	}
	
	return $_conds;
 }

 function _get_CallReason()
 {
	$_conds = array();
	$sql = "SELECT a.ScriptId, a.Description from t_gn_campaign_script a ";
	$qry = $this -> db -> query($sql);
	foreach( $qry -> result_assoc() as $rows ) {
		$_conds[$rows['ScriptId']] = $rows['Description'];
	}
	
	return $_conds;
 }
 
 // ========================================================= END CLASS =========================================================
}

?>