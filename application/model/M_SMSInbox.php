<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SMSInbox extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_SMSInbox() 
{
	$this->load->model(array('M_SetCampaign'));
}


// _NextActivity 

public function _NextActivity()
{
    $next_activity = array();
	$this->_get_default();
	$sql = $this->EUI_Page->_getCompiler();
	if( $sql ) 
	{
		$qry = $this->db->query($sql);
		$num = 0;
		foreach( $qry->result_assoc() as $rows ){
			$next_activity[] = $rows['CustomerId'];  
			$num++;
		}
	}
	
	return $next_activity;
	
}

 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("b.deb_id");
	$this->EUI_Page->_setFrom("serv_sms_inbox a");
	$this->EUI_Page->_setJoin("t_gn_debitur b","a.MasterId = b.deb_id","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment c "," b.deb_id = c.CustomerId","LEFT", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	// $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	// $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	// $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	
	
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('c.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	//USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN
	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'inbox_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'inbox_cust_id', TRUE);	
	$this->EUI_Page->_setLikeCache('b.deb_name', 'inbox_cust_name', TRUE);
	$this->EUI_Page->_setLikeCache('a.Sender', 'inbox_phone_num', TRUE);
	
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"IF(b.deb_id IS NULL,0,b.deb_id) AS CustomerId"=> array('CustomerId','ID','primary'),
		"IF(b.deb_acct_no IS NULL,'Unknown',b.deb_acct_no) AS CustomerNumber"=> array('CustomerNumber','Customer Id'),
		"IF(b.deb_name IS NULL,'Unknown',b.deb_name) AS CustomerName"=>array('CustomerName','Nama Pelanggan'),
		"a.TextMessage AS Message"=>array('Message','Pesan'),
		"a.Sender AS PhNumber"=>array('PhNumber','Nomor Telpon'),
		"IF(a.IsRead=0,'New Message','Read') AS ReadStatus"=>array('ReadStatus','Status Pesan'),
		"a.RecvDate AS RecvDate"=>array('RecvDate','Tanggal Balas')
	));
	
	$this->EUI_Page->_setFrom("serv_sms_inbox a");
	$this->EUI_Page->_setJoin("t_gn_debitur b","a.MasterId = b.deb_id","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment c "," b.deb_id = c.CustomerId","LEFT", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	// $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	// $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	// $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	
	
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('c.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'inbox_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'inbox_cust_id', TRUE);	
	$this->EUI_Page->_setLikeCache('b.deb_name', 'inbox_cust_name', TRUE);
	$this->EUI_Page->_setLikeCache('a.Sender', 'inbox_phone_num', TRUE);
		
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	// echo $this ->EUI_Page->_getCompiler();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 } 
 
/*
 * @ pack : get sms outbox by Debitur or customer Id 
 */ 
public function _get_sms_inbox( $MasterId = 0 )
{

  $recsource = array();
	$this->db->reset_select();
	$this->db->select('
		a.SmsId, a.Sender, 
		a.SentDate, a.RecvDate, 
		a.TextMessage, 
		IF(a.IsRead = 1, "READ","UNREAD") as IsRead ', FALSE);

	$this->db->from('serv_sms_inbox a ');
	$this->db->where('a.MasterId', $MasterId);
	$this->db->order_by('a.SmsId', 'DESC');
	
	$qry  = $this->db->get();
	if( $qry->num_rows() > 0 ) 
	{
		$recsource  = $qry->result_assoc();
	}
	
	return $recsource;
}

// END OF CLASS 
 
}





?>