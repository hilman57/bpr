<?php
/*
 * E.U.I 
 *
 
 * subject	: M_Utility modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SysUserLayout extends EUI_Model
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	$this -> EUI_Page -> _setQuery
	(
	 " SELECT a.Id
	   FROM t_gn_grouplayout a left join t_gn_agent_profile b on a.GroupId=b.id 
	   LEFT join t_gn_layout c on a.LayoutId=c.Id"); 
	
	$filter = '';
	
	if( $this->URI->_get_have_post('key_words') ) 
	{
		$filter ="";
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	if( $this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
 
 $sql = "SELECT * FROM t_lk_branch a ";
			
 $this -> EUI_Page ->_setQuery("
		 select a.*, b.name as GroupName, c.Name as LayoutName, d.full_name as UserName
		 from t_gn_grouplayout a left join t_gn_agent_profile b on a.GroupId=b.id 
		 left join t_gn_layout c on a.LayoutId=c.Id 
		 left join t_tx_agent d on a.CreateByUserId=d.UserId");
		 
 $filter = '';
  if( $this -> URI -> _get_have_post('key_words') )
  {
	$filter = " ";	
  }				
		
  $this -> EUI_Page->_setWhere();
  $this -> EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
    
 function _setSaveLayout( $data =null )
 {
	$_conds = false;
	if(!is_null($data))
	{
		if( $this->db->insert('t_gn_grouplayout',$data)){
			$_conds = true;
		}
	}
	return $_conds;	
 }
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function _setLayout( $data )
 {
	$_totals = 0;
	foreach($data['LayoutId'] as $k => $v )
	{	
		if( $this -> db -> update('t_gn_grouplayout',
			array('Flags'=>$data['SetLayout']), 
			array('Id'=>$v) 
		))
		{
			$_totals++;
		}	
	}	
	
	return $_totals;
 }
  /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function _setDeleteLayout( $LayoutId )
 {
	$_totals = 0;
	foreach($LayoutId as $k => $v )
	{	
		$this ->db->where('Id', $v);
		if( $this ->db->delete('t_gn_grouplayout') ){
			$_totals++;
		}
	}	
	
	return $_totals;
 }
 

 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getLayout($LayoutId=null)
 {
	$_conds = array();
	if( !is_null($LayoutId) AND (INT)$LayoutId!=0 )
	{
		$this -> db -> select('*');
		$this -> db -> from('t_gn_grouplayout');
		$this -> db -> where('Id',$LayoutId);
		
		foreach($this -> db ->get() -> result_assoc() as $rows )
		{
			$_conds = $rows;
		}
	}
	
	return $_conds;
	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setUpdateLayout($data = null)
 {
	$_conds = false;
	if( !is_null($data))
	{
		if( $this -> db ->update('t_gn_grouplayout',
			array(
				'GroupId' => $data['GroupId'],
				'LayoutId'=> $data['LayoutId'],
				'Themes'=> $data['Themes']
			),
			array('Id' => $data['Id'])
		))
		{
			$_conds = true;
		}
	}
	
	return $_conds;
 }
 
 
}

?>