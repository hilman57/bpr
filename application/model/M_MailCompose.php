<?php
/*
 * @ notes  : 1 = incoming mail , 2= outgoing mail 
 * 
 * --------------------------------------------
 
 * @ param :  - 
 * @ param :  -
 *
 */
 
class M_MailCompose extends EUI_Model
{


public function M_MailCompose() {
	$this->load->model(array('M_Configuration'));
}
	
/*
 * @ def 		: _setSaveMailToQueue
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _setSaveMailToQueue( $param  = null )
{
   $OutboxId = 0;
   $Config = $this->M_Configuration->_getMail(); // require mail 
   
   if( !is_null($param) )
   {
	 $this->db->set('EmailSender', $Config['smtp.auth']); // on configuratio 
	 $this->db->set('EmailContent', $param['MailContent']); // html or text 
	 $this->db->set('EmailSubject', $param['MailSubject']); // subject 
	 $this->db->set('EmailStatus', '1001'); // Ready status
	 $this->db->set('EmailCreateTs', date('Y-m-d H:i:s')); // create TS 
	 $this->db->set('EmailCreateById', $this->EUI_Session->_get_session('UserId')); // user create 
	 $this->db->set('EmailAssignDataId', 1); // user create 
	 $this->db->insert('email_outbox');
	 
	 if( $this->db->affected_rows() > 0 )
	 {
		$OutboxId = $this->db->insert_id();
	 }
	 
	}
	
	return $OutboxId;
}

/*
 * @ def 		: _setMailDestination
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function _setMailDestination($param=null, $OutboxId=0 )
{

  $conds = 0;
  if( (!is_null($param)) AND ($OutboxId!=FALSE) )
  {
	foreach( $param as $keys => $anAddress )
	{
		$this->db->set('EmailDestination',$anAddress); 
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2);
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_destination');
		
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
	return $conds;
}


/*
 * @ def 		: _setMailCC
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function _setMailCC($param=null, $OutboxId = 0 ) {

 $conds = 0;
  if( (!is_null($param)) AND ($OutboxId!=FALSE) )
 {
	foreach( $param as $keys => $anAddress )
	{
		$this->db->set('EmailCC',$anAddress); 
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2);
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_copy_carbone');
		
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
	return $conds;
}


/*
 * @ def 		: _setMailBCC
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 

public  function _setMailBCC( $param = null, $OutboxId = 0){

$conds = 0;
 if( (!is_null($param)) AND ($OutboxId!=FALSE) )
 {
	foreach( $param as $keys => $anAddress )
	{
		$this->db->set('EmailBCC',$anAddress); 
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2);
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_blindcopy_carbone');
		
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
return $conds;
	
}

/*
 * @ def 		: _setSaveAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function _setSaveAttachment( $param = null, $OutboxId = 0 )
{
  
 $conds = 0;
 if( (!is_null($param)) AND ($OutboxId!=FALSE) )
 {
	foreach( $param as $keys => $rows )
	{
		$this->db->set('EmailAttachmentPath',$rows['path']); 
		$this->db->set('EmailAttachmentSize',$rows['size']);
		$this->db->set('EmailAttachmentType',$rows['mime']);
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2); 
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_attachment_url');
		
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
return $conds;
}


/*
 * @ def 		: _setSaveAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function _setSaveQueue( $OutboxId = 0 )
{
  $conds = 0;
  if( $OutboxId )
  {
	$this->db->set('QueueStatusTs',date('Y-m-d H:i:s'));
	$this->db->set('QueueCreateTs',date('Y-m-d H:i:s')); 
	$this->db->set('QueueMailId', $OutboxId );
	$this->db->set('QueueStatus', '1001');
	$this->db->set('QueueTrying',0);
	$this->db->insert('email_queue');
	
	if( $this->db->affected_rows()> 0 ) {
		$conds++;
	}
	
  }
  
  return $conds;
	
}
				 



}
	
?>