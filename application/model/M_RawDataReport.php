<?php
	class M_RawDataReport extends EUI_Model
	{
		private static $instance = null;	

		function M_RawDataReport()
		{
		  $this -> load ->model(array('M_Combo','M_SysUser'));
		}
		
		public static function & get_instance() 
		{
			if( is_null(self::$instance)) 
			{
				self::$instance = new self();
			}
			return self::$instance;
		}
		
		public function FilterBy()
		{
			return array
			(
				'cmp' => 'Campaign'
			);
		}
		
		public function ModeBy()
		{
			return array
			(
				'summary' => 'Summary'
			);
		}
		
		function filter_by($Filter__ = null)
		{
			$Filter = array();
			$this -> db -> select("a.CampaignId, a.CampaignName");
			$this -> db -> from("t_gn_campaign a");
			$this -> db -> where("a.CampaignStatusFlag",1);
			
			foreach($this -> db -> get() -> result_assoc() as $rows)
			{
				$Filter[$rows['CampaignId']] = $rows['CampaignName'];
			}
			return $Filter;
		}
		
	}
?>