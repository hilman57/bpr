<?php
if(!defined('PROEJECT_WCALL')) define('PROEJECT_WCALL', 1);

class M_ReportDailyReport extends EUI_Model
{

 var $report_group = null;
 var $start_date = null;
 var $end_date = null;
 var $report_type  = array();
 var $config = array();
 
public function M_ReportDailyReport()
{
	
 $this->report_group 	= $this->URI->_get_post('report_group');
 $this->start_date 		= $this->URI->_get_post('start_date');
 $this->end_date 		= $this->URI->_get_post('end_date');
 $this->report_type 	= $this->URI->_get_array_post('report_type');
 
 $this->config['report_type'] = array( 
		array( 'name' =>'neded_agent', 'title' => 'Detail Need Agent'),
		array( 'name' =>'visit_branch', 'title' => 'Detail Visit to Branch'),
		array( 'name' =>'uncontacted', 'title' => 'Detail Uncontacted'),
		array( 'name' =>'change_address', 'title' => 'Detail Change Address/Ph number/Email'),
		array( 'name' =>'survey_clean', 'title' => 'Detail Surveyed Clean') 
	);
	
$this->config['report_group'] = array( 
		array( 'name' => 'agency','title' => 'Report Agency', 'model' => 'R_DailyReportAgency',  'view' => 'view_daily_content_agency'),
		array( 'name' => 'bancass','title' => 'Report Bancass','model' => 'R_DailyReportBancass', 'view' => 'view_daily_content_bancass')
	);
	
}	


/** report_group ***/

public function report_group() 
{
	global $argv;
 if( isset($argv[3]) )
 {
	$this->report_group = $argv[3];
 }
 
 return $this->report_group;
}

/** report_type **/

public function report_type() 
{
  global $argv;
  
  if( isset($argv[4])){
	 $this->report_type[strtolower($argv[4])] = strtolower($argv[4]);
  }
  
	return $this->report_type;
 }

/** start_date **/

public function start_date() {
	
  return ( $this->start_date!='' ? $this->start_date : date('d-m-Y') );
  
}

/** end_date **/

public function end_date(){
	return  ( $this->end_date!='' ? $this->end_date : date('d-m-Y') );
}


/** _getReportType **/
	
public function _getReportType()
{
	$combo = array();
	foreach( $this->config['report_type'] as $k => $rows ){
		$combo[$rows['name']] = $rows['title'];
	}
	
	return $combo;
}


/* _getReportGroup **/

public function _getReportGroup()
{
	$combo = array();
	foreach( $this->config['report_group'] as $k => $rows ) {
		$combo[$rows['name']] = $rows['title'];
	}
	
	return $combo;
}


//** getConfig ***/

public function config( $stack = null )
{
	static $config = array();
	foreach( $this->config[$stack] as $k => $values  ) {
		$config[$values['name']] = $values;
	}
	
	return $config;
}

//** _getHeaderLayout ***/

public function _getHeaderLayout($stack = null )
{	
	$package = array();
	$config = self::config('report_group');
	if(!is_null($stack) )
	{
		$package = $config[$stack];
		if( is_array( $package ) )  // push array 
		{
			$package['start_date'] = _getDateEnglish($this->start_date());
			$package['end_date'] = _getDateEnglish($this->end_date());
		}
	}
	
	return $package;
}


// get email addres by Group FollowUp 
public function FollowupMailAddressByGroup()
{

 static $config = array();
 
 $this->db->select("
	a.FuEMaiId as MailId,	 
	b.FuGroupCode as GroupCode, 
	a.FuEmailToCc as MailType , 
	a.FuEMailAddr as MailAddress, 
	b.FuGroupDesc as MailDescription", FALSE);
		
 $this->db->from('t_lk_followup_mailgroup a ');
 $this->db->join('t_lk_followupgroup b ',' a.FUEMailFollowupId=b.FuGroupId', 'LEFT');
 $this->db->where_in('a.FuEMailProjectId', array(PROEJECT_WCALL));
 $this->db->where('a.FuEMailShow', 1);
 
 // result assoc as res 
 $num =0;
 foreach( $this->db->get()->result_assoc() as $rows ) 
 {
	$config[$rows['GroupCode']][$rows['MailType']][]= $rows['MailAddress'];
	$num++;
 }
 
 return $config;

}


	
}
?>