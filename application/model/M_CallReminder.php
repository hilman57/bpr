<?php 
class M_CallReminder extends EUI_Model
{

/*  
 * @ param  : PrimaryID ( INT ) / CustomerId / AppoinmentId / CallVerified ID  
 * @ param  : - 
 * ------------------------------------------------------------------------------
 
 * @ notes  : - 
 */ 
 
public function _setUpdateAppoinment( $PrimaryID = null )
{

  $_conds = 0;
  if( !is_null($PrimaryID) ) 
  {
	 $this->db->set('ApoinmentFlag',1);
	 $this->db->where('AppoinmentId', $PrimaryID);
	 $this->db->update('t_gn_appoinment');
	 
	 if( $this->db->affected_rows() > 0 )
	 {
		$_conds++;
	 }
   }
   
   return $_conds;
}


/*  
 * @ param  : PrimaryID ( INT ) / CustomerId / AppoinmentId / CallVerified ID  
 * @ param  : - 
 * ------------------------------------------------------------------------------
 
 * @ notes  : - 
 */ 
 
 public function _getSelectAppoinment()
 {
	$_conds = array();
	
	$this->db->select(" a.AppoinmentId, a.CustomerId, 
			DATE_FORMAT(a.ApoinmentDate,'%H:%i') as TryCallAgain, 
			b.deb_cmpaign_id AS CampaignId, 
			b.deb_name AS CustomerFirstName",FALSE);
	$this->db->from("t_gn_appoinment a");
	$this->db->join("t_gn_debitur  b ", "a.CustomerId=b.deb_id","INNER");
	$this->db->join("t_gn_assignment c ", "a.CustomerId=c.CustomerId","INNER");
	$this->db->where("c.AssignSelerId", $this -> EUI_Session->_get_session('UserId'));
	$this->db->where("DATE_ADD(now(), INTERVAL '05:00' minute)>=a.ApoinmentDate");
	$this->db->where("NOW()<=a.ApoinmentDate");
	$this->db->where("YEAR(a.ApoinmentDate)>0");
	$this->db->where("a.ApoinmentFlag", 0);
	
	if( $rs = $this -> db -> get() )
	{
		$num_rows = $rs -> num_rows();
		if( $vol_rows = $rs -> result_first_assoc() )
		{
			$_conds['CustomerId'] = $vol_rows['CustomerId'];
			$_conds['CampaignId'] = $vol_rows['CampaignId'];
			$_conds['CustomerName'] = $vol_rows['CustomerFirstName'];
			$_conds['PrimaryID'] = $vol_rows['AppoinmentId'];
			$_conds['TryCallAgain']	= $vol_rows['TryCallAgain'];
			$_conds['counter'] = (INT)$num_rows;	
		}
	}
	
	return $_conds;
 }		
 
/*
 * @ pack : _getSmsCounter 
 */
 
 public function _getSmsCounter()
{
  $conds = 0;
  $this->db->reset_select();	
  $this->db->select("count(a.SmsId) as Jumlah", FALSE);
  $this->db->from("t_gn_sms_approval a ");
  $this->db->where_in("a.SmsApprovalStatus", array(101));
  $qry = $this->db->get();
  if( $qry->num_rows() > 0 )
  {
	$conds = (INT)$qry->result_singgle_value();
  }
  
  return $conds;
} // _getSmsCounter ==========================> 
 
/*
 * @ pack : _getDiscountCounter 
 */
 
 public function _getDiscountCounter()
{
  $conds = 0;
  $this->db->reset_select();	
  $this->db->select("COUNT(a.id) as Jumlah", FALSE);
  $this->db->from("t_gn_discount_trx a ");
  $this->db->where_in("a.aggrement", array(101));
  $qry = $this->db->get();
  if( $qry->num_rows() > 0 )
  {
	$conds = (INT)$qry->result_singgle_value();
  }
  
  return $conds;
} 
// _getDiscountCounter ==========================> 
 
 
}