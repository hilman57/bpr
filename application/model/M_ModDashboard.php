<?php
class M_ModDashboard extends EUI_Model
{

function _getSummaryDataResult()
{
	$_conds = array();
	
	$this -> db ->select("
			COUNT(a.CustomerId) as Jumlah, IF(a.CallReasonId <> 0, a.CallReasonId, 'NEW') as CallReasonId, e.Name as CallType,
			IF( c.CallReasonDesc is null, 'New',c.CallReasonDesc) as CallReasonDesc, 
			IF( a.CallComplete, 'Completed','Uncompleted') as CallComplete",FALSE
			);
				
	$this -> db ->from("t_gn_debitur a"); 
	$this -> db ->join("t_gn_assignment b ","a.CustomerId=b.CustomerId","INNER");
	$this -> db ->join("t_lk_account_status c ","a.CallReasonId=c.CallReasonId","LEFT");
	$this -> db ->join("t_lk_customer_status d ","c.CallReasonCategoryId=d.CallReasonCategoryId","LEFT");
	$this -> db ->join("t_lk_outbound_goals e ", "d.CallOutboundGoalsId=e.OutboundGoalsId","LEFT");
	$this -> db ->join("t_tx_agent f ", "b.AssignSelerId=f.UserId","LEFT");
	
	
// user admin
 
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_ADMIN ){
		$this ->db ->where('f.admin_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// manager
 	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_ACCOUNT_MANAGER ){
		$this ->db ->where('f.act_mgr',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// manager
 	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$this ->db ->where('f.mgr_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// supervisor 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$this ->db ->where('f.spv_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
//	Leader

	if( $this -> EUI_Session->_get_session('HandlingType')==USER_LEADER ){
		$this ->db ->where('f.tl_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// inbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this ->db ->where('f.UserId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// outbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this ->db ->where('f.UserId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// CampaignId 
	
	if( $this -> URI -> _get_have_post('CampaignId')){
		$this ->db ->where('a.CampaignId', $this -> URI->_get_post('CampaignId'));
	}	
			
	$this ->db ->group_by("a.CallComplete, a.CallReasonId");
	//echo $this->db->_get_var_dump();
	
	$i = 0;
	foreach( $this ->db -> get() ->result_assoc() as $rows ){
		$_conds[$rows['CallComplete']][$i] = $rows;
		$i++;
	}	
	
	return $_conds;
}

function _getSummaryPolicyResult()
{
	$_conds = array();
	
	$this -> db ->select("
			COUNT(g.PolicyId) as Jumlah, IF(a.CallReasonId <> 0, a.CallReasonId, 'NEW') as CallReasonId, e.Name as CallType,
			IF( c.CallReasonDesc is null, 'New',c.CallReasonDesc) as CallReasonDesc, 
			IF( a.CallComplete, 'Completed','Uncompleted') as CallComplete",FALSE
			);
				
	$this -> db ->from("t_gn_debitur a"); 
	$this -> db ->join("t_gn_assignment b ","a.CustomerId=b.CustomerId","INNER");
	$this -> db ->join("t_lk_account_status c ","a.CallReasonId=c.CallReasonId","LEFT");
	$this -> db ->join("t_lk_customer_status d ","c.CallReasonCategoryId=d.CallReasonCategoryId","LEFT");
	$this -> db ->join("t_lk_outbound_goals e ", "d.CallOutboundGoalsId=e.OutboundGoalsId","LEFT");
	$this -> db ->join("t_tx_agent f ", "b.AssignSelerId=f.UserId","LEFT");
	$this -> db ->join("t_gn_policy_detail g ", "a.CustomerNumber=g.EnigmaId","LEFT");
	
// user admin
 
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_ADMIN ){
		$this ->db ->where('f.admin_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// manager
 	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_ACCOUNT_MANAGER ){
		$this ->db ->where('f.act_mgr',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// manager
 	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$this ->db ->where('f.mgr_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// supervisor 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$this ->db ->where('f.spv_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
//	Leader

	if( $this -> EUI_Session->_get_session('HandlingType')==USER_LEADER ){
		$this ->db ->where('f.tl_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// inbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this ->db ->where('f.UserId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// outbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this ->db ->where('f.UserId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// CampaignId 
	
	if( $this -> URI -> _get_have_post('CampaignId')){
		$this ->db ->where('a.CampaignId', $this -> URI->_get_post('CampaignId'));
	}	
			
	$this ->db ->group_by("a.CallComplete, a.CallReasonId");
	// echo $this->db->_get_var_dump();
	// $i = 0;
	foreach( $this ->db -> get() ->result_assoc() as $rows ){
		$_conds[$rows['CallComplete']][$rows['CallReasonId']] = $rows;
		// $i++;
	}	
	
	return $_conds;
}

// appointment day

function _getSummaryAppoinmentData()
{
	$_conds = array();
	
	$this -> db -> select("COUNT(a.AppoinmentId) as jumlah,   
				DATE_FORMAT(a.ApoinmentDate,'%d-%M-%Y') as tanggal, 
				DATE(a.ApoinmentDate) as AppoinmentDate", FALSE);
				
	$this -> db -> from("t_gn_appoinment a");
	$this -> db -> join("t_gn_debitur b "," a.CustomerId=b.CustomerId","LEFT");
	$this -> db -> join("t_gn_assignment c ", " a.CustomerId=c.CustomerId","LEFT");
	$this -> db -> join("t_tx_agent f ", "c.AssignSelerId=f.UserId","LEFT");
	
	$this -> db -> where("a.ApoinmentFlag",0);
	$this -> db -> where("DATE(a.ApoinmentDate)>=", date('Y-m-d'));
	$this -> db -> where("DATE(a.ApoinmentDate)<=", date('Y-m-d'));
	$this -> db -> where("DATE(a.ApoinmentDate)!=", '0000-00-00');
	
// user admin
 
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_ADMIN ){
		$this ->db ->where('f.admin_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// account manager
 	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_ACCOUNT_MANAGER ){
		$this ->db ->where('f.mgr_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// manager
 	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$this ->db ->where('f.mgr_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// supervisor 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$this ->db ->where('f.spv_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
//	Leader

	if( $this -> EUI_Session->_get_session('HandlingType')==USER_LEADER ){
		$this ->db ->where('f.tl_id',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// inbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this ->db ->where('f.UserId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// outbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this ->db ->where('f.UserId',$this -> EUI_Session -> _get_session('UserId'));
	}

	
// group by 
	
	$this -> db -> group_by("tanggal");
	
	$i = 0;
	foreach($this -> db->get()->result_assoc() as $rows ){
		$_conds[$i] = $rows; $i++;
	}	
	
	return $_conds;
}


/**
 *
 *
 */
 
public function _getSummaryDistributeByDate()
{
	$this->db->reset_select();
	
   $totals = array();
   $this->db->select("
		DATE_FORMAT(a.LogCreatedDate, '%Y-%m-%d') as tgl,  
		if(b.AssignSelerId is null,'Bank Data',b.AssignSelerId) as UserIds, 
		if(f.full_name is null,'Bank Data',f.full_name) as full_name,g.CampaignCode,
		COUNT(DISTINCT c.CustomerNumber) as customer, 
		count(e.PolicyNumber) as policy", 
	FALSE);
	
	$this->db->from('t_gn_distribusi_log a');
	$this->db->join('t_gn_assignment b ','a.LogAssignmentId=b.AssignId','LEFT');
	$this->db->join('t_gn_debitur c ',' b.CustomerId=c.CustomerId','LEFT');
	$this->db->join('t_gn_campaign g ','c.CampaignId=g.CampaignId','LEFT');
	$this->db->join('t_gn_cif_customer d ',' c.CustomerNumber=d.EnigmaId','LEFT');
	$this->db->join('t_gn_policy_detail e ',' d.EnigmaId=e.EnigmaId', 'LEFT');
	$this->db->join('t_tx_agent f ','b.AssignSelerId=f.UserId','LEFT');
	
// supervisor 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$this ->db ->where('b.AssignSpv',$this -> EUI_Session -> _get_session('UserId'));
	}
	
//	Leader

	if( $this -> EUI_Session->_get_session('HandlingType')==USER_LEADER ){
		$this ->db ->where('b.AssignLeader',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// inbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this ->db ->where('b.AssignSelerId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// outbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this ->db ->where('b.AssignSelerId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
	$this ->db ->where('a.LogTrfMenu like "%TRANSFER_DATA%"');
	$this->db->group_by('UserIds');
	$this->db->group_by('tgl');
	$this->db->group_by('CampaignCode');
	$this->db->having("full_name != 'Bank Data'");
	$this->db->order_by('tgl','DESC');
	// echo $this->db->_get_var_dump();
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$totals[$rows['tgl']][$rows['CampaignCode']]['createdate'] = $rows['tgl'];
		$totals[$rows['tgl']][$rows['CampaignCode']]['policy'] += $rows['policy'];
		$totals[$rows['tgl']][$rows['CampaignCode']]['customer']+= $rows['customer'];
		$totals[$rows['tgl']][$rows['CampaignCode']]['agentname'] = $rows['full_name'];
		$totals[$rows['tgl']][$rows['CampaignCode']]['userid'] 	= $rows['UserIds'];
		$totals[$rows['tgl']][$rows['CampaignCode']]['CampaignCode']= $rows['CampaignCode'];
	}
	return $totals;

}

public function _getResultAttempt()
{
	$this->db->reset_select();
	
   $totals = array();
   $this->db->select("
		DATE_FORMAT(a.LogCreatedDate, '%Y-%m-%d') AS tgl, IF(b.AssignSelerId IS NULL, 'Bank Data', b.AssignSelerId) AS UserId,
		IF(f.full_name IS NULL, 'Bank Data', f.full_name) AS full_name, g.CampaignCode, COUNT(DISTINCT c.CustomerNumber) AS customer,
		COUNT(e.PolicyNumber) AS policy, SUM(c.CallAttempt) AS Atttempting", FALSE);
	
	$this->db->from('t_gn_distribusi_log a');
	$this->db->join('t_gn_assignment b ','a.LogAssignmentId=b.AssignId','LEFT');
	$this->db->join('t_gn_debitur c ',' b.CustomerId=c.CustomerId','LEFT');
	$this->db->join('t_gn_campaign g ','c.CampaignId=g.CampaignId','LEFT');
	$this->db->join('t_gn_cif_customer d ',' c.CustomerNumber=d.EnigmaId','LEFT');
	$this->db->join('t_gn_policy_detail e ',' d.EnigmaId=e.EnigmaId', 'LEFT');
	$this->db->join('t_tx_agent f ','b.AssignSelerId=f.UserId','LEFT');
	
// supervisor 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$this ->db ->where('b.AssignSpv',$this -> EUI_Session -> _get_session('UserId'));
	}
	
//	Leader

	if( $this -> EUI_Session->_get_session('HandlingType')==USER_LEADER ){
		$this ->db ->where('b.AssignLeader',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// inbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this ->db ->where('b.AssignSelerId',$this -> EUI_Session -> _get_session('UserId'));
	}
	
// outbound 
	
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this ->db ->where('b.AssignSelerId',$this -> EUI_Session -> _get_session('UserId'));
	}

	$this->db->group_by('UserId');
	$this->db->group_by('tgl');
	$this->db->group_by("full_name != 'Bank Data'");
	
	$this->db->order_by('tgl','DESC');
	//echo $this->db->_get_var_dump();
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$totals[$rows['tgl']][$rows['UserId']]['createdate'] = $rows['tgl'];
		$totals[$rows['tgl']][$rows['UserId']]['policy']	+= $rows['policy'];
		$totals[$rows['tgl']][$rows['UserId']]['customer']	+= $rows['customer'];
		$totals[$rows['tgl']][$rows['UserId']]['agentname']	 = $rows['full_name'];
		$totals[$rows['tgl']][$rows['UserId']]['userid']	 = $rows['UserId'];
		$totals[$rows['tgl']][$rows['UserId']]['CampaignCode']	= $rows['CampaignCode'];
		$totals[$rows['tgl']][$rows['UserId']]['Attempting'] = $rows['Atttempting'];
	}
	
	return $totals;
}


}
?>