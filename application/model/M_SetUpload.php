<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysMenu  
 * 			  extends under model class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysmenu/
 */
 
class M_SetUpload extends EUI_Model
{
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function M_SetUpload()
 {
	$this -> load -> model(array('M_Loger','M_Configuration'));
 }
 
 
 
/*
 * @ def 		: blaklist data 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _LimitDays()
 {
	$limit = array();
	for( $i = 0; $i<=12; $i++){
		if( $s_i = ($i * 30) ){
			$limit[$s_i] = "$s_i Days"; 
		}
	}
	return $limit;
 }

/*
 * @ def 		: blaklist data 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _BlackList() {
	return $list = array( 'Y'=>'YES', 'N'=>'NO');
 }

/*
 * @ def 		: blaklist data 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function _UploadModul()
{
	$conds = array();
	$config =& M_Configuration::get_instance();
	
	if( is_object($config) ) {
		$conds = $config->_getUplodModul(); 
	} 
	
	return $conds; 
	
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	$this -> EUI_Page -> _setQuery( " select * from t_gn_template "); 
	
	$filter = '';
	
	if( $this->URI->_get_have_post('key_words') ) 
	{
		$filter ="";
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	if( $this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

//@ create object 

 $this->EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
 $this->EUI_Page->_setPage(10); 
 $sql = " SELECT * FROM t_gn_template ";
 $this -> EUI_Page ->_setQuery($sql);
 
  $filter = null;
  if( $this -> URI -> _get_have_post('key_words') )
  {
	$filter = " ";	
  }				
		
  $this->EUI_Page->_setWhere();
  $this->EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' )
	{
		return $this -> EUI_Page -> _getNo();
	}	
 }
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _get_list_tables()
 {
	$_conds = array(); 
	$_mixed = $this->M_Configuration->_getTemplateLayout();
	if( !is_null($_mixed))
	{
		foreach( $this -> db -> list_tables(true) as $k => $n)
		{	
			if( in_array($n, array_keys($_mixed)))
			{
				$_conds[$n] = $_mixed[$n];
			}
		}
	}
	
	return $_conds;
 }
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _get_field_column($table)
 {
	$_conds= array();
	
	if(!is_null($table))
	{
		$field = $this->db->list_fields($table);
		$hide = $this -> _get_hide_schema($table);
		
		foreach( $field as $k => $v )
		{
			if(!in_array($v, array_keys($hide)))
			{
				$_conds[$v] = $v;
			}
		}
	}
	
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _get_hide_schema($table = null )
 {
	$schema = array();
	
	$sql = " select a.table_field_name from t_gn_hide_tables a where a.table_name='$table'";
	$qry = $this -> db ->query($sql);
	foreach( $qry -> result_assoc() as $rows )
	{
		$schema[$rows['table_field_name']] = $rows['table_field_name'];
	}
	
	return $schema;
 }
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public  function _getTypeUpload()
 {
	return array
	(
		'insert' => 'Insert',
		'update' => 'Update'
	);
 }
 
 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
  public function _getTypeFile()
  {
	return array
	(
		'excel' => 'Excel',
		'txt' => 'TXT'
	);
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _set_saveInsert($_array = null )
 {
	$_conds = false;
	if( !is_null($_array) && is_array($_array) )
	{
		$_post = false;
		
		if( isset($_array['mode']) AND !is_null($_array['mode']) ) $_post  = true;
		if( isset($_array['table']) AND !is_null($_array['table']) ) $_post  = true;
		if( isset($_array['tplname']) AND !is_null($_array['tplname']) ) $_post  = true;
		if( isset($_array['filetype']) AND !is_null($_array['filetype']) ) $_post  = true;
		if( isset($_array['content']) AND !is_null($_array['content']) ) $_post  = true;
		if( isset($_array['delimiter']) AND !is_null($_array['delimiter']) ) $_post  = true;
		if( isset($_array['order_by']) AND !is_null($_array['order_by']) ) $_post  = true;
		if( isset($_array['black_list']) AND !is_null($_array['black_list']) ) $_post  = true;
		if( isset($_array['expired_days']) AND !is_null($_array['expired_days']) ) $_post  = true;
		if( isset($_array['bucket_data']) AND !is_null($_array['bucket_data']) ) $_post  = true;
		if( isset($_array['upload_modul']) AND !is_null($_array['bucket_data']) ) $_post  = true;
		
		if( $_post )
		{
			if( $this -> db -> insert('t_gn_template', array
			(
				'TemplateTableName' => $_array['table'], 
				'TemplateName'  => $_array['tplname'],
				'TemplateMode' => $_array['mode'],
				'TemplateFileType' => $_array['filetype'],
				'TemplateSparator' => $_array['delimiter'],
				'TemplateBlacklist' => $_array['black_list'],
				'TemplateLimitDays' => $_array['expired_days'],
				'TemplateBucket' => $_array['bucket_data'],
				'TemplateModul' => $_array['upload_modul'],
				'TemplateFlags' => 1,
				'TemplateCreateTs' => date('Y-m-d H:i:s') 
				
			)))
			{
				$insert_id = $this -> db -> insert_id();
				if( $insert_id )
				{
					$total = 0; 
					$Order = 0;
					
					$order_list = $_array['order_by'];
					
					foreach($_array['content'] as $Keys => $Names )
					{
						
						if( $this -> db -> insert('t_gn_detail_template',array(
							'UploadTmpId' => $insert_id, 
							'UploadColsName' => $Keys, 
							'UploadColsAlias' => $Names,
							'UploadColsOrder' => $order_list[$Order]
						))){
							$totals++;
						}
						$Order++;
					}
				}
			}
		}
	}
	
	return ( ($totals>0) ? true : $_conds );
 }
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function _set_saveUpdate($_array = null ) 
{
	$_conds = false;
	if( !is_null($_array) && is_array($_array) )
	{
		$_post = false;
		
		if( isset($_array['mode']) AND !is_null($_array['mode']) ) $_post  = true;
		if( isset($_array['table']) AND !is_null($_array['table']) ) $_post  = true;
		if( isset($_array['tplname']) AND !is_null($_array['tplname']) ) $_post  = true;
		if( isset($_array['filetype']) AND !is_null($_array['filetype']) ) $_post  = true;
		if( isset($_array['content']) AND !is_null($_array['content']) ) $_post  = true;
		if( isset($_array['keys']) AND !is_null($_array['keys']) ) $_post  = true;
		if( isset($_array['delimiter']) AND !is_null($_array['delimiter']) ) $_post  = true;
		if( isset($_array['order_by']) AND !is_null($_array['order_by']) ) $_post  = true;
		if( isset($_array['black_list']) AND !is_null($_array['black_list']) ) $_post  = true;
		if( isset($_array['expired_days']) AND !is_null($_array['expired_days']) ) $_post  = true;
		if( isset($_array['bucket_data']) AND !is_null($_array['bucket_data']) ) $_post  = true;
		if( isset($_array['upload_modul']) AND !is_null($_array['upload_modul']) ) $_post  = true;
		
		if( $_post )
		{
			if( $this->db->insert('t_gn_template', array
			(
				'TemplateTableName' => $_array['table'], 
				'TemplateName' => $_array['tplname'],
				'TemplateMode' => $_array['mode'],
				'TemplateFileType' 	=> $_array['filetype'],
				'TemplateBlacklist' => $_array['black_list'],
				'TemplateLimitDays' => $_array['expired_days'],
				'TemplateBucket' => $_array['bucket_data'],
				'TemplateModul'	=> $_array['upload_modul'],
				'TemplateFlags' => 1,
				'TemplateCreateTs' => date('Y-m-d H:i:s') 
				
			)))
			{
				$insert_id = $this -> db -> insert_id();
				if( $insert_id )
				{
					$total = 0; 
					$Order = 0;
					$order_list = $_array['order_by'];
					foreach($_array['content'] as $Keys => $Names )
					{
						
						if( in_array($Keys,array_keys($_array['keys']) )) {
							$this -> db -> set('UploadKeys',1);
						}
						
						$this->db->set('UploadTmpId',$insert_id);
						$this->db->set('UploadColsName',$Keys);
						$this->db->set('UploadColsAlias',$Names);
						$this->db->set('UploadColsOrder',$order_list[$Order]);
						
						$this -> db -> insert("t_gn_detail_template");
						if( $this->db->affected_rows() > 0 )
						{
							$totals++;
						}
						$Order++;
						
					}
				}
			}
		}
	}
	
	return ( ($totals>0) ? true : $_conds );
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_name_template($TemplateId = null)
 {	
	$_data = array();
	
	if( !is_null($TemplateId))
	{
		$this -> db -> select('TemplateName,TemplateSparator, TemplateFileType');
		$this -> db -> from('t_gn_template');
		$this -> db -> where( array('TemplateId'=>$TemplateId) );
		
		$qry = $this -> db -> get();
		if( !$qry->EOF() )
		{
			$_data = $qry -> result_first_assoc();
		}
	}
	
	return $_data;
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_detail_template($TemplateId = null)
 {	
	$_data = array();
	
	if( !is_null($TemplateId))
	{
		$this -> db -> select('*');
		$this -> db -> from('t_gn_template');
		$this -> db -> where( array('TemplateId'=>$TemplateId) );
		
		$qry = $this -> db -> get();
		if( !$qry->EOF() )
		{
			$_data = $qry -> result_first_assoc();
		}
	}
	
	return $_data;
}
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function _get_download_template($TemplateId = null)
 {	
	$_data = array();
	
	if( !is_null($TemplateId))
	{
		$this -> db -> select('*');
		$this -> db -> from('t_gn_detail_template');
		$this -> db -> where(array('UploadTmpId'=>$TemplateId));
		$this -> db -> order_by('UploadColsOrder','ASC');
		
		$qry = $this -> db -> get();
		foreach( $qry -> result_assoc() as $rows )
		{	
			$_data[$rows['UploadColsName']] = $rows['UploadColsAlias']; 
		}
	}
	return $_data;
 }
 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _set_active_template( $_flags, $TemplateId )
{
	$_conds = false;
	if( !is_null($_flags) AND !is_null($TemplateId)  )
	{
		if( $this -> db -> update('t_gn_template', array( 'TemplateFlags' => $_flags ), array( 'TemplateId' => $TemplateId )))
		{
			$_conds = true;
		}
	}	
		
	return $_conds;	
}

 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_ready_template($tmptype="")
{
 
 $_conds = array();
 $config=& get_config();
 
 if($this->EUI_Session->_have_get_session('UserId'))
 {
	$this->db->reset_select();
	$this->db->select('TemplateId,TemplateName, TemplateModul', FALSE);
	$this->db->from('t_gn_template');
	$this->db->where('TemplateFlags',1);

	if($tmptype!=""){
		$this->db->where('TemplateType',$tmptype);
	}
	// echo "<pre>";
	// echo $this->db->_get_var_dump();
	// echo "</pre>";
	$qry = $this->db->get();
	if( $qry->num_rows()> 0 ) 
		foreach( $qry->result_assoc() as $rows ){
			$_conds[$rows['TemplateId']] = $rows['TemplateName'];
		// $is_uer_aksess = array();
		
		// $aksess_modul[$rows['TemplateModul']] = $config[$rows['TemplateModul']];
		
		// // @ pack : get modul on template aksess compare user login 
		
		// $UserAksesModul = $aksess_modul[$rows['TemplateModul']];
		
		// if( is_array($UserAksesModul) ) 
		// 	foreach( $UserAksesModul as $key => $values ) 
		//  {
		// 	$user_defined_constants = get_defined_constants();
		// 	$is_uer_aksess[$values] = $user_defined_constants[$values];
		//  }
				
		//  // @ pack 	: looek modul akses by user login 
			
		//   if( in_array(_get_session('HandlingType'), array_values($is_uer_aksess) ) )
		// 	{
		// 		$_conds[$rows['TemplateId']] = $rows['TemplateName'];
		// 	}
		}	
	}	
	return $_conds;
}
 
 
// _set_delete_template

function _set_delete_template($id = null)
{
	
	$_conds = 0;
	if( !is_null($id))
	{
		foreach( $id as $k => $TemplateId )
		{	
			//t_gn_template
			$this ->db->where('UploadTmpId',$TemplateId);
			$this ->db->delete('t_gn_detail_template');
			
			if( $this ->db->affected_rows() > 0 )
			{
				$this -> db->where('TemplateId',$TemplateId);
				$this -> db->delete('t_gn_template'); 
				 
				if($this ->db->affected_rows() > 0)
				{
					$_conds++;
				}
				
			}
		}
	}
	
	return $_conds;
} 

}

?>
