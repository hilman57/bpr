<?php
class M_ScoringPolicyForm extends EUI_Model
{
	function M_ScoringPolicyForm()
	{
		// echo "XXX";
	}
	
	function _SaveScore($param)
	{
		$conds = array('success'=>0);
		
		$sql['CustomerId'] = $param['CustomerId'];
		$sql['PolicyId'] = $param['PolicyId'];
		$sql['ProjectId'] = $param['ProjectId'];
		$sql['ScoringRemark'] = $param['RemarkScore'];
		$sql['ScoringTotal1'] = $param['FinalScore'];
		$sql['ScroingQualityId'] = $this->EUI_Session->_get_session('UserId');
		$sql['ScoringCreateTs'] = date('Y-m-d H:i:s');
		
		$this->db->insert('t_gn_qa_scoring',$sql);
		
		if( $this->db->affected_rows() > 0 )
		{
			$InsertId = $this -> db->insert_id();
			
			$bebek = array();
			
			for($x=1;$x<=8;$x++)
			{
				if( isset($param['chk_'.$x]) )
				{
					$bebek['chk_'.$x] = $param['chk_'.$x];
				}
				else{
					$bebek['chk_'.$x] = 0;
				}
			}
			
			$anoa['ScoringId'] = $InsertId;
			$anoa['ScoringQuestionId'] = implode(',',array_keys($bebek));
			$anoa['ScoringQuestionValue'] = implode(',',$bebek);
			$anoa['ScoringQuestionDates'] = date('Y-m-d H:i:s');
			$anoa['ScoringTotalScore1'] = $param['FinalScore'];
			$anoa['ScoringByUserId'] = $this->EUI_Session->_get_session('UserId');
			
			// print_r($anoa);
			
			$this->db->insert('t_gn_scoring_point',$anoa);
			if( $this->db->affected_rows() > 0 )
			{
				$conds = array('success'=>1);
			}
			// var_dump($this->db);
		}
		
		return $conds;
	}
	
	function _GetData($CustomerId,$PolicyId,$ProjectId)
	{
		$datas = array();
		
		$sql = "select * from t_gn_qa_scoring a
				left join t_gn_scoring_point b on a.Id = b.ScoringId
				where a.CustomerId = '".$CustomerId."' 
				and a.PolicyId = '".$PolicyId."' 
				and a.ProjectId = '".$ProjectId."'";
		$qry = $this->db->query($sql);
		
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas = $rows;
			}
		}
		
		return $datas;
	}
	
	function _getProjectCode($projectId)
	{
		$datas = array();
		
		$sql = "SELECT
					a.ProjectId,
					a.ProjectCode,
					a.ProjectName
				FROM t_lk_work_project a
				WHERE 1=1
					AND a.ProjectId = '".$projectId."'";
		
		$qry = $this->db->query($sql);
		// echo $sql;
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas = $rows;
			}
		}
	}
}
?>