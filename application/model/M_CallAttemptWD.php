<?php
	class M_CallAttemptWD extends EUI_Model
	{
		function M_CallAttemptWD()
		{
			$this -> start_date = _getDateEnglish( $this -> URI->_get_post('start_date'));
			$this -> end_date 	= _getDateEnglish( $this -> URI->_get_post('end_date'));
		}
		
		function _getData()
		{
			$data = array();
			$sql = "SELECT
						b.CallAttempt,
						b.CallAttempt AS Attempt,
						SUM(IF(a.PolicyId IS NOT NULL AND a.CallReasonId IN (1,5,8,36,20,21),1,0)) Amount,
						(
							SELECT 
								SUM(IF(z.PolicyId IS NOT NULL AND z.CallReasonId IN (1,5,8,36,20,21),1,0))
							FROM t_gn_callhistory z
							WHERE 1=1
							AND b.CallAttempt != 0
							AND DATE(z.CallHistoryCreatedTs) >= '".$this -> start_date."'
							AND DATE(z.CallHistoryCreatedTs) <= '".$this -> end_date."'
						) AS Total
					FROM t_gn_callhistory a
						LEFT JOIN t_gn_debitur b ON a.CustomerId = b.CustomerId
					WHERE 1=1
						AND DATE(a.CallHistoryCreatedTs) >= '".$this -> start_date."'
						AND DATE(a.CallHistoryCreatedTs) <= '".$this -> end_date."'
					GROUP BY b.CallAttempt";
			$qry = $this->db->query($sql);
			// echo "<pre>$sql</pre>";
			foreach( $qry->result_assoc() as $rows )
			{
				$data[$rows['CallAttempt']]['Attempt'] = $rows['Attempt'];
				$data[$rows['CallAttempt']]['Amount'] = $rows['Amount'];
				$data[$rows['CallAttempt']]['Total'] = $rows['Total'];
			}
			return $data;
		}
		
		function _getDataHour()
		{
			$data = array();
			$sql = "SELECT
						HOUR(a.CallHistoryCreatedTs) AS Hours,
						SUM(IF(a.PolicyId IS NOT NULL AND a.CallReasonId IN (1),1,0)) Amount,
						(
							SELECT 
								SUM(IF(z.PolicyId IS NOT NULL AND z.CallReasonId IN (1),1,0))
							FROM t_gn_callhistory z
							WHERE b.CallAttempt != 0
							AND DATE(z.CallHistoryCreatedTs) >= '".$this -> start_date."'
							AND DATE(z.CallHistoryCreatedTs) <= '".$this -> end_date."'
						) AS Total
					FROM t_gn_callhistory a
						LEFT JOIN t_gn_debitur b ON a.CustomerId = b.CustomerId
						LEFT JOIN t_gn_campaign c ON b.CampaignId = c.CampaignId
						LEFT JOIN t_gn_campaign_project d ON c.CampaignId = d.CampaignId
					WHERE 1=1
						AND d.ProjectId = 1
						AND DATE(a.CallHistoryCreatedTs) >= '".$this -> start_date."'
						AND DATE(a.CallHistoryCreatedTs) <= '".$this -> end_date."'
					GROUP BY Hours";
			$qry = $this->db->query($sql);
			// echo "<pre>$sql</pre>";
			foreach( $qry->result_assoc() as $rows )
			{
				$data[$rows['Hours']]['Amount'] = $rows['Amount'];
				$data[$rows['Hours']]['Total'] = $rows['Total'];
			}
			return $data;
		}
		
		function _getDataFinished()
		{
			$data = array();
			$sql = "SELECT
						b.CallAttempt,
						b.CallAttempt AS Attempt,
						SUM(IF(a.PolicyId IS NOT NULL AND a.CallReasonId IN (1,5,8,36,20,21),1,0)) Amount,
						(
							SELECT 
								SUM(IF(z.PolicyId IS NOT NULL AND z.CallReasonId IN (1,5,8,36,20,21),1,0))
							FROM t_gn_callhistory z
							WHERE 1=1
							#AND b.CallAttempt = 1
							AND DATE(z.CallHistoryCreatedTs) >= '".$this -> start_date."'
							AND DATE(z.CallHistoryCreatedTs) <= '".$this -> end_date."'
						) AS Total
					FROM t_gn_callhistory a
						LEFT JOIN t_gn_debitur b ON a.CustomerId = b.CustomerId
					WHERE 1=1
						AND DATE(a.CallHistoryCreatedTs) >= '".$this -> start_date."'
						AND DATE(a.CallHistoryCreatedTs) <= '".$this -> end_date."'
						#AND b.CallAttempt = 1
					GROUP BY b.CallAttempt";
			$qry = $this->db->query($sql);
			// echo "<pre>$sql</pre>";
			foreach( $qry->result_assoc() as $rows )
			{
				$data[$rows['CallAttempt']]['Attempt'] = $rows['Attempt'];
				$data[$rows['CallAttempt']]['Amount'] = $rows['Amount'];
				$data[$rows['CallAttempt']]['Total'] = $rows['Total'];
			}
			return $data;
		}
	}
?>