<?php
/*
 * E.U.I 
 *
 
 * subject	: ProductForm
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/ProductForm/
 */
 
class M_ProjectWorkForm extends EUI_Model
{
	
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
 
public function _getAttributes( $PolicyId = null, $CustomerId = null)
{
 $attrib = array();
 
	if( !is_null($PolicyId) )
	{
		$this->db->select('a.CampaignId, e.ProjectCode, e.ProjectId, f.CampaignTypeId');
		$this->db->from('t_gn_debitur a ');
		$this->db->join('t_gn_cif_customer b ',' a.CustomerNumber=b.EnigmaId','LEFT');
		$this->db->join('t_gn_policy_detail c ',' c.EnigmaId=c.EnigmaId','LEFT');
		$this->db->join('t_gn_campaign_project d ','a.CampaignId=d.CampaignId','LEFT');
		$this->db->join('t_lk_work_project e ',' d.ProjectId=e.ProjectId','LEFT');
		$this->db->join('t_gn_campaign f ',' a.CampaignId=f.CampaignId','LEFT');
		$this->db->where('a.CustomerId', $CustomerId);
		$this->db->where('c.PolicyId', $PolicyId );
		
		foreach( $this->db->get()->result_assoc() as $rows )
		{	
			 $attrib['CampaignId']  = $rows['CampaignId'];
			 $attrib['ProjectCode'] = $rows['ProjectCode'];
			 $attrib['ProjectId'] = $rows['ProjectId'];
		}
	}
	return $attrib;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
public function _getWorkForm( $ProjectCode = null )
{

$_arr_form = array();
 $this->db->select('*');
 $this->db->from('t_gn_form_layout a');
 $this->db->where('a.ProjectCode', $ProjectCode);
 foreach( $this->db->get()->result_assoc() as $rows ) {
	$_arr_form = $rows;
 }
 
 return $_arr_form;
 
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
protected function _policy_data_not_exist($CustomerId)
{
	
	$this->db->select(' 
		a.CustomerFirstName, 
		b.PayMode as PayMode,
		a.CustomerAddressLine1 as CustomerAddressLine1,
		a.CustomerAddressLine2 as CustomerAddressLine2,
		a.CustomerAddressLine3  as CustomerAddressLine3,
		a.CustomerAddressLine4 as CustomerAddressLine4,
		a.CustomerCity as CustomerCity,
		a.ProvinceId  as ProvinceId,
		a.CustomerZipCode as CustomerZipCode,
		a.CustomerCountry as CustomerCountry,
		a.CustomerHomePhoneNum as CustomerHomePhoneNum,
		a.CustomerMobilePhoneNum as CustomerMobilePhoneNum,
		a.CustomerWorkPhoneNum  as CustomerWorkPhoneNum,
		a.CustomerEmail as CustomerEmail,
		"" AS NOTES_FU_POST', 
	FALSE);
	
 $this->db->from('t_gn_debitur  a');
 $this->db->join('t_gn_policy_detail c',' a.CustomerNumber=c.EnigmaId', 'LEFT');
 $this->db->join('t_lk_paymode b ','c.PaymentMode=b.PayModeId', 'LEFT');
 $this->db->where('a.CustomerId', $CustomerId);
 foreach( $this->db->get()->result_assoc() as $rows ) 
 {
	$_arr_pools = $rows;
 }
 
 return $_arr_pools;
 
 }


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
protected function _policy_data_is_exist($PolicyId, $CustomerId)
{
	
	$this->db->select(' b.CustomerFirstName, 
	IF( d.FuPaymentFreq is null,  c.PayMode , d.FuPaymentFreq) as PayMode,
	IF( d.FuAddr1 is null, b.CustomerAddressLine1, d.FuAddr1) as CustomerAddressLine1,
	IF( d.FuAddr2 is null, b.CustomerAddressLine2, d.FuAddr2) as CustomerAddressLine2,
	IF( d.FuAddr3 is null, b.CustomerAddressLine3, d.FuAddr3) as CustomerAddressLine3,
	IF( d.FuAddr4 is null, b.CustomerAddressLine4, d.FuAddr4) as CustomerAddressLine4,
	IF( d.FuCity is null, b.CustomerCity, d.FuCity ) as CustomerCity,
	IF( d.FuProvince is null, b.ProvinceId,d.FuProvince) as ProvinceId,
	IF( d.FuZip is null, b.CustomerZipCode, d.FuZip) as CustomerZipCode,
	IF( d.FuCountry is null,  b.CustomerCountry,d.FuCountry) as CustomerCountry,
	IF( d.FuHomePhone is null,  b.CustomerHomePhoneNum, d.FuHomePhone) as CustomerHomePhoneNum,
	IF( d.FuMobilePhone is null,  b.CustomerMobilePhoneNum, d.FuMobilePhone) as CustomerMobilePhoneNum,
	IF( d.FuOfficePhone is null, b.CustomerWorkPhoneNum, d.FuOfficePhone) as CustomerWorkPhoneNum,
	IF( d.FuEmail is null, b.CustomerEmail, d.FuEmail) as CustomerEmail,
	d.FuNotes AS NOTES_FU_POST', 
	FALSE);
	
 $this->db->from('t_gn_policy_detail a');
 $this->db->join('t_gn_debitur b ','a.EnigmaId=b.CustomerNumber','LEFT');
 $this->db->join('t_lk_paymode c ',' a.PaymentMode=c.PayModeId', 'LEFT');
 $this->db->join('t_gn_followup d ',' a.PolicyNumber=d.FuPolicyNo','LEFT');
 $this->db->where('d.FuGroupCode', '700');
 $this->db->where('a.PolicyId', $PolicyId);
 $this->db->where('b.CustomerId', $CustomerId);
 
 foreach( $this->db->get()->result_assoc() as $rows ) {
	$_arr_pools = $rows;
 }
 
 return $_arr_pools;
 
 }
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
public function _getPolicyHistory( $PolicyId = null, $CustomerId = null  )
{
 $conds = self::_policy_data_is_exist( $PolicyId, $CustomerId );
 
 if( count($conds) > 0 )	
	return $conds;
 else
	return self::_policy_data_not_exist($CustomerId);
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
public function _getFollowUpGroup()
{	
	$arr_disabled = $this->_WorkProjectWorkFormDisabled();
	$arr_followup = null;
	$this->db->select("*");
	$this->db->from("t_lk_followupgroup a");
	$this->db->where_in('a.FuProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->where('a.FuGroupShow', 'Y');
	$this->db->where_not_in('a.FuGroupCode', $arr_disabled);
	$this->db->order_by('a.FuGroupCode', 'ASC');
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$arr_followup[$rows['FuGroupCode']] = $rows['FuGroupDesc'];
	}
	
	return $arr_followup;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
public function _getWorkProject( $ProjectId = null ) 
{
	$this->db->select('*');
	$this->db->from('t_lk_work_project');
	$this->db->where_in('ProjectId', $ProjectId);
	return $this->db->get()->result_first_assoc();
	
	
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
 
public function _getFollowUpExist($param)
{

  $follow_up_data = array();

	$this->db->select("*");
	$this->db->from("t_gn_followup a ");
	$this->db->where("a.FuCustId", $param['CustomerId']);
	$this->db->where("a.FuCIF", $param['CIFNumber']);
	$this->db->where("a.FuPolicyNo", $param['PolicyNumber']);
	
	foreach( $this->db->get() -> result_assoc() as $rows ) {
		$follow_up_data[$rows['FuGroupCode']] = $rows; 
	}
	
	return $follow_up_data;
}


/**
 * @ def : get disabled of form of project 
 ^ --------------------------------------------

 * @ param : Customer ID <int>
 *  
 */
 
 public function _WorkProjectWorkFormDisabled()
 {
	$this->db->reset_select();
	
	$config = array();
	
	$this->db->select('a.GroupCode');
	$this->db->from('t_gn_followup_disabled a ');
	$this->db->join('t_gn_campaign b','a.CampaignCode=b.CampaignCode','LEFT');
	$this->db->join('t_gn_debitur c ','b.CampaignId=c.CampaignId','LEFT');
	$this->db->where('c.CustomerId', $this->URI->_get_post('CustomerId') );
	
	foreach( $this->db->get()-> result_assoc() as $rows )
	{
		$config[$rows['GroupCode']] = $rows['GroupCode'];
	}
	
	return $config;
 }
 
 /**
 * @ def : get disabled of form of project 
 ^ --------------------------------------------

 * @ param : Customer ID <int>
 *  
 */
 
 public function _getQustionByPolicyId( $PolicyId = 0 )
 {
 
	$Question = array();
	
	$this->db->select("
		a.CustField_Q1, a.CustField_Q2,
		a.CustField_Q3, a.CustField_Q4,
		a.CustField_Q5, a.CustField_Q6,
		a.CustField_Q7, a.CustField_Q8,
		a.CustField_Q9, a.CustField_Q10", FALSE);
		
   $this->db->from('t_gn_followup_question a ');
   $this->db->where('a.PolicyId',$PolicyId);
  // echo $this->db->_get_var_dump();
   
   
   foreach( $this->db->get()->result_assoc() as $rows  )
   {
		foreach( $rows as $field => $values ) {
			$Question[$field] = $values;
		}
   }
   
   return $Question;
 }
 
 
/**
 * @ def : get disabled of form of project 
 ^ --------------------------------------------

 * @ param : Customer ID <int>
 *  
 */
 
 public function _getQustionByQualityPolicyId( $PolicyId = 0 )
 {
	$Question = array();
	
	$this->db->select("
		a.QuestionId as QuestionId,
		a.CustField_Q1, a.CustField_Q2,
		a.CustField_Q3, a.CustField_Q4,
		a.CustField_Q5, a.CustField_Q6,
		a.CustField_Q7, a.CustField_Q8,
		a.CustField_Q9, a.CustField_Q10", FALSE);
		
   $this->db->from('t_gn_followup_question a ');
   $this->db->where('a.PolicyId',$PolicyId);
   
   foreach( $this->db->get()->result_assoc() as $rows  ) 
   {
		foreach( $rows as $field => $values ) 
		{
			$Question[$field] = $values;
		}
   }
   
   return $Question;
   
 }
 
 
 
 /**
  * _getPolicyFollowByPolicyId 
  
  */
  
 public function _getPolicyFollowByPolicyId( $PolicyId = null, $CustomerId = null  )
 {
	$config = array(); $array_result = array();
	
	$arr_disabled = $this->_WorkProjectWorkFormDisabled();
	$arr_config = array ( 800 => 'NOTES_FU_AGENCY_SUPPORT', 900 => 'NOTES_FU_BANCA_SUPPORT', 999 => 'NOTES_FU_VISIT_TO_BRANCH' );
	$this->db->select('a.FuGroupCode, a.FuNotes');
	$this->db->from('t_gn_followup a');
	$this->db->join('t_gn_policy_detail b','a.FuPolicyNo=b.PolicyNumber', 'LEFT');
	$this->db->where('a.FuCustId', $CustomerId);
	$this->db->where('b.PolicyId', $PolicyId);
	$this->db->where_not_in('a.FuGroupCode', $arr_disabled);
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		if( $arr_config[$rows['FuGroupCode']]!='' )
		{
			$config_code = trim($arr_config[$rows['FuGroupCode']]);
			$config[$config_code] = $rows['FuNotes'];	
		}	
	}
	
	if( count($config) > 0)  
		$array_result = $config;
	else
	{
		$_config = array();
		foreach( $arr_config as $code => $name ) 
		{
			if( !in_array($code, $arr_disabled) )
				$_config[$name] = "";
		}
		
		$array_result = $_config;
	}
	
	return $array_result;
 }
 
}
?>