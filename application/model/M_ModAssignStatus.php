<?php
/*
 * @ pack : under model M_ModulRPC
 */ 
 
class M_ModAssignStatus extends EUI_Model {

/*
 * @ pack : under model M_InfoPTP
 */ 
 
var $_table_space = array();

/*
 * @ pack : under model M_InfoPTP
 */ 
 
 public function M_ModAssignStatus()
{
  $this->_table_space = array('t_gn_assign_acount_status');
  $this->load->model(array('M_SysPrivileges','M_SetCampaign','M_Loger'));
  
}

/*
 * @ pack : get status by Level && exist status on here 
	SER_LEVEL 	USER_AGENT_INBOUND 	6 	Active
	2 	USER_LEVEL 	USER_ROOT 		8 	Active
	3 	USER_LEVEL 	USER_ADMIN 		1 	Active
	4 	USER_LEVEL 	USER_SUPERVISOR 	3 	Active
	5 	USER_LEVEL 	USER_QUALITY_STAFF 	11 	Active
	6 	USER_LEVEL 	USER_QUALITY_HEAD 	5 	Active
	7 	USER_LEVEL 	USER_ACCOUNT_MANAGER 	9 	Active
	8 	USER_LEVEL 	USER_MANAGER 	2 	Active
	9 	USER_LEVEL 	USER_AGENT_OUTBOUND 	4 	Active
	10 	USER_LEVEL 	USER_LEADER
	
 */
 
public function _get_level_status_privileges( $level = 0 )
{

 $_account_status_level = array();
 
 if( $this->EUI_Session->_have_get_session('UserId') )
 {
	$this->db->reset_select();
	
	$this->db->select(" 
		a.acs_code_level1 as Level1, c.CallReasonDesc as DescLeve1, 
		a.acs_code_level2 as Level2, b.CallReasonDesc as DescLeve12", FALSE);
	
	$this->db->from("t_gn_assign_acount_status a");
	$this->db->join("t_lk_account_status b "," a.acs_code_level2=b.CallReasonCode","LEFT");
	$this->db->join("t_lk_account_status c  "," a.acs_code_level1=c.CallReasonCode","LEFT");
	
	
/*
 * @ pack : root & am, acm admin 
 */ 
	$USER_LEVEL = _get_session('HandlingType');
	if( in_array(
		$USER_LEVEL, array(USER_ROOT,USER_ADMIN,USER_ACCOUNT_MANAGER)))
	{
		$this->db->where("a.acs_code_level1", $level);
	}
	
/*
 * @ pack : if login by TL 
 */ 
	if( in_array(
		$USER_LEVEL, array(USER_LEADER,USER_AGENT_OUTBOUND,USER_AGENT_INBOUND)))
	{
		$this->db->where("a.acs_code_level1", $level);
		$this->db->where("a.acs_code_level1", $level);
	}
	
	$this->db->where("a.acs_code_flags",1);
	
	$qry = $this->db->get();
	if( $qry->num_rows()>0)
		foreach( $qry->result_assoc() as $rows )
	{
		$_account_status_level[$rows['Level1']] = $rows['DescLeve1'];
		$_account_status_level[$rows['Level2']] = $rows['DescLeve12'];	
	}
	
	
	
	
	
  }
  
  return $_account_status_level;
  
}
 
 

/*
 * @ pack : _get_level_account_status
 */
 
 public function _get_level_account_status()
{
	self::_get_level_status_privileges();
	
 $array_select_account = array();
 $this->db->reset_select();
 $this->db->select("a.CallReasonCode, a.CallReasonDesc ",FALSE);
 $this->db->from("t_lk_account_status a");
 $this->db->order_by("a.CallReasonOrder", "ASC");
 
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 )
 foreach( $qry->result_assoc() as $rows ) 
 {
	$array_select_account[$rows['CallReasonCode']] = $rows['CallReasonDesc'];
 }
 
 return $array_select_account;
}

/*
 * @ pack : _get_level_account_status
 */
 
public function _get_user_privileges()
{
	$select_user_privilege = null;
	if( is_null( $select_user_privilege ))
	{
		if( $rows = $this->M_SysPrivileges->_get_select_user_privilege(array(1)) ) {
			$select_user_privilege = $rows;
		}
	}
		
	return $select_user_privilege;
}

/*
 * @ pack : _get campaign of name on here 
 */
 
public function _get_select_campaign()
{
	$_get_select_campaign = null;
	if( is_null( $_get_select_campaign ))
	{
		if( $rows = $this->M_SetCampaign->_get_campaign_name() ) {
			$_get_select_campaign = $rows;
		}
	}
		
	return $_get_select_campaign;
}


// @ pack : (acs_code_id, acs_code_level1, acs_code_level2, acs_code_privileges, 
// @ pack : acs_code_campaign, acs_code_created, 
// @ pack : acs_code_createby, acs_code_flags)


/*
 * @ pack : select all 
 */
 
 public function _get_labels_acs()
{
	$_acs_label = array(
		'acs_code_campaign' => 'Campaign',
		'acs_code_level1' => 'Level1',
		'acs_code_level2' => 'Level2',
		'acs_code_privileges' => 'Privileges',
		'acs_code_created' => 'Create By',
		'acs_code_createby' => 'Create By',
		'acs_code_flags' => 'Status'
	);
	
	return $_acs_label;
}


/*
 * @ pack : select all 
 */
 
 public function _get_select_acs()
{

  $_acs_label = array();
  return $_acs_label;
   
}

/*
 * @ pack : select all 
 */
 
 public function _get_detail_acs( $argv_vars = null )
{
  $conds = array();	
  
  $this->db->reset_select();
  $this->db->select("*",FALSE);
  $this->db->from(reset($this->_table_space));
  $this->db->where('acs_code_id', $argv_vars);
  
  $qry = $this->db->get();
  if( $qry->num_rows()>0 ) {
	$conds = $qry->result_first_assoc();
  }
  
  return $conds;
	
}


/*
 * @ pack : select all 
 */
 
 public function _get_field_acs()
{
	return $this->db->list_fields(reset($this->_table_space));
}

/*
 * @ pack : select all 
 */
 
 public function _get_component_acs() 
{
	return array 
	( 
		'primary' => array('acs_code_id'),
		'combo'   => array('acs_code_flags','acs_code_level1','acs_code_level2','acs_code_privileges','acs_code_campaign'),
		'option'  => array(
			'acs_code_campaign' 	=> $this->_get_select_campaign(),
			'acs_code_level1' 		=> $this->_get_level_account_status(),
			'acs_code_level2' 		=> $this->_get_level_account_status(),
			'acs_code_privileges' 	=> $this->_get_user_privileges(),
			'acs_code_flags' 		=> array('1'=>'Active','0'=>'Not Active')
		)
	);
}

/*
 * @ pack : _set_save_spc
 */
 
public function _set_save_acs( $arg_vars = null )
{
  $conds = 0;	
  if( is_array($arg_vars) )
  {
	foreach( $arg_vars as $field => $value ) {
		$this->db->set($field, $value);
	}
	
	$this->db->set('acs_code_created', date('Y-m-d H:i:s'));
	$this->db->set('acs_code_createby', _get_session('UserId'));
	
	// @ pack : set save data 
	$this->db->insert(reset($this->_table_space));
	if( $this->db->affected_rows() > 0 ) 
	{
		$this->M_Loger->set_activity_log("Add Assign Account Status ::".implode(',',$arg_vars));
		$conds++;
	}
  }
 
 return $conds;
  
} 


/*
 * @ pack : _set_save_spc
 */
 
 public function _set_update_acs( $argv_vars = null )
{

 $conds = 0;
 
 // @ pack : reset all select query ..
 
 $this->db->reset_select(); 
 
 // @ pack : cek argument set vars ..
  
 if( is_array($argv_vars) )	
	foreach( $argv_vars as $field => $value )
 {
	if(in_array($field,array('acs_code_id') ) ) {
		$this->db->where($field, $value);
	} else {
		$this->db->set($field, $value);
	}
	
	$this->db->set('acs_code_created', date('Y-m-d H:i:s'));
	$this->db->set('acs_code_createby', _get_session('UserId'));
	
  }
  
  // @ pack : execute query from here ..
   
  $this->db->update(reset($this->_table_space));
  
  if($this->db->affected_rows()>0){
    $this->M_Loger->set_activity_log("Edit Assign Account Status ::".implode(',',$argv_vars));
	$conds++;
  }
 
  return $conds;
 
}

/*
 * @ pack : _set_save_spc
 */
 
public function _set_delete_acs( $acsId = NULL )
{
  $conds = 0;
  if( is_array($acsId) ) 
	foreach( $acsId as $PrimaryId => $value )
  {
	  $this->db->where('acs_code_id', $value);	
      $this->db->delete(reset($this->_table_space));
	  
	  if( $this->db->affected_rows() > 0 )
	  {
		 $this->M_Loger->set_activity_log("Delete Assign Account Status ::".implode(',',$acsId));
		 $conds++;
	  }
  }
  
  return $conds;
}

/*
 * @ pack : _get_default
 */
 
 public function _get_default_acs()
{
  $this->EUI_Page->_setPage(10); 
  $this->EUI_Page->_setSelect("a.acs_code_id");
  $this->EUI_Page->_setFrom(reset($this->_table_space) ." a", TRUE); 
  
  // @ pack : set filter --------------------
	
   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd(" 
			a.acs_code_id LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_level1 LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_level2 LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_privileges LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_campaign LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.acs_code_created LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.acs_code_createby LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.acs_code_flags LIKE '%{$this->URI->_get_post('keywords')}%' "
		);
  }		
	
 // @ pack : process its  
 
  if( $this->EUI_Page->_get_query() ) {
		return $this->EUI_Page;
  }
}

/*
 * @ pack : _get_default
 */
 
 public function _get_content_acs()
{
  $this->EUI_Page->_postPage($this->URI->_get_post('v_page') );
  $this->EUI_Page->_setPage(10);
  
// @ pack : set filter --------------------
	  
  $this->EUI_Page->_setArraySelect(array(
		"a.acs_code_id AS acs_code_id" => array("acs_code_id","ID","primary"),
		"a.acs_code_id AS ID" => array("ID","ID"),
		"e.CampaignDesc AS CampaignId" => array("CampaignId","Campaign Name"),
		"c.CallReasonDesc AS Level1" => array("Level1","Level 1"),
		"d.CallReasonDesc AS Level2" => array("Level2","Level 2"),
		"b.name AS PrivilegeId" => array("PrivilegeId","Privileges"),
		"a.acs_code_created AS CreateTs" => array("CreateTs","Create Date"),
		"f.full_name AS UserName" => array("UserName","Create By User"),
		"IF(a.acs_code_flags=1,'Active','Not Active') AS acs_code_flags" => array("acs_code_flags","Status")
   ));
  
   $this->EUI_Page->_setFrom(reset( $this->_table_space) ." a ");
   $this->EUI_Page->_setJoin("t_gn_agent_profile b ", "a.acs_code_privileges=b.id", "LEFT");
   $this->EUI_Page->_setJoin("t_lk_account_status c ","a.acs_code_level1=c.CallReasonCode","LEFT");
   $this->EUI_Page->_setJoin("t_lk_account_status d ","a.acs_code_level2=d.CallReasonCode","LEFT");
   $this->EUI_Page->_setJoin("t_tx_agent f ","a.acs_code_createby=f.UserId","LEFT");
   
   $this->EUI_Page->_setJoin("t_gn_campaign e ","a.acs_code_campaign=e.CampaignId","LEFT",TRUE);
   
	
  // @ pack : set filter --------------------
	
   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd(" 
			a.acs_code_id LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_level1 LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_level2 LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_privileges LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.acs_code_campaign LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.acs_code_created LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.acs_code_createby LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.acs_code_flags LIKE '%{$this->URI->_get_post('keywords')}%' "
		);
  }	
	
  if( $this->URI->_get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy($this ->URI->_get_post('order_by'),$this ->URI->_get_post('type'));
  }
  
  $this -> EUI_Page->_setLimit();
}

/*
 * @ pack : _get_default
 */
 
 public function _get_resource_acs()
{
	self::_get_content_acs();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
}

/*
 * @ pack : _get_default
 */
 
 public function _get_page_number_acs() 
{
	if( $this->EUI_Page->_get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }

// END CLASS 
 
}



?>