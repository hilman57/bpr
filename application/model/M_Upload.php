<?php
/* 
 * @ def 	: class Upload Modul ALL DATA 
 * ----------------------------------------
 *
 * @ author : razaki team 
 * @ param  : class properties 
 */

/*
 * @ update : 2014-08-31 optimize logic method constans
 * -------------------------------------------------------------
 */ 
 
/* set memory iniated  &&  set time exexcute 
 * --------------------------------------------
 */

ini_set("memory_limit","1024M");
set_time_limit(3600);

class M_Upload extends EUI_Model 
{
var $file_excel_copy = false;

/* 
 * @ def  : 	
 * ---------------------------------------------
 * @ param : test unit
 */
 
 private static $_attributes;
 
 /* 
 * @ def  : 	
 * ---------------------------------------------
 * @ param : test unit
 */
 
 private static $_request;
 
 /* 
 * @ def  : 	
 * ---------------------------------------------
 * @ param : test unit
 */
 
 private static $_reportid;
 
 private static $Instance = null;
 
 /* 
 * @ def  : 	
 * ---------------------------------------------
 * @ param : test unit
 */
 
  public static function &Instance()
 {
	 if( is_null(self::$Instance) )
	{
		self::$Instance = new self();
	}		
	return self::$Instance;
 }
 
 /* 
 * @ def  : 	
 * ---------------------------------------------
 * @ param : test unit
 */
 
 function M_Upload()
 {
	$this -> load -> model(array('M_BlackList','M_XDays','M_Tools'));
	
 }

/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function FileExploits( $file_name  = null )
 {
	$process = explode(".", $file_name);
	
	if( is_array( $process) )
	{
		$uniqid = date('H:i:s') . $process[0];
		$filename = $process[0] .'_'. md5(base64_encode($uniqid)) . '.' . $process[count($process)-1];
	}
	$this->_FileExploits =  $filename;
	return $this->_FileExploits;
 }
/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function _report_log_data( $type='FTP' )
 {
	$_insertid = 0;
	
	$PATH_FILES = base_url() . APPPATH . 'temp/'. self::FileExploits(self::$_attributes['fileToupload']['name']);
	if( $PATH_FILES )
	{
		$this->db->set('FTP_UploadFilename', $PATH_FILES);
		$this->db->set('FTP_UploadStartDateTs', date('Y-m-d H:i:s'));
		$this->db->set('FTP_UploadDateTs', date('Y-m-d H:i:s'));
		$this->db->set('FTP_UploadType', $type);
		
  // @ pack : process to log 
		//echo $this->db->last_query()."\n\r"; exit;
		$this->db->insert("t_gn_upload_report_ftp");
		if( $this->db->affected_rows() )
		{
			$_insertid = $this->db->insert_id();	
		}
	}
	
  // @ pack : insert to constans vars 
  
	if( $_insertid ) {
		self::$_reportid = $_insertid; 
	}	
	
	return self::$_reportid;
 }
 
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
/*
 * @ attr 		set common path 
 * @ package 	handle file data 
 */
 
 public function _read_common_path()
{
	$this->common_path = str_replace("system/", "application/temp", BASEPATH);
	return $this->common_path;
} 


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
/*
 * @ attr 		set common path 
 * @ package 	handle file data 
 */
 
 public function _is_temp_file_name()
{
	if( !isset( self::$_attributes['fileToupload']['name'] ) ){
		return FALSE;	
	}	
	return true;
}
 
 
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
/*
 * @ attr 		set common path 
 * @ package 	handle file data 
 */
 
 public function _read_temp_name()
{
	if( $this->_is_temp_file_name() ) {
		return self::$_attributes['fileToupload']['tmp_name'];
	}
	return FALSE;
}
 
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
/*
 * @ attr 		set common path 
 * @ package 	handle file data 
 */
 
 public function _read_file_name() 
{
	 if( $this->_is_temp_file_name() ) { 
		return self::$_attributes['fileToupload']['name'];
	}
	return FALSE;
} 

 
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
/*
 * @ attr 		set common path 
 * @ package 	handle file data 
 */
 
  public function _read_file_path() 
{
	$this->arr_paths = array( $this->_read_common_path(), $this->_read_file_name());
	return join("/", $this->arr_paths);
} 


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
/*
 * @ attr 		set common path 
 * @ package 	handle file data 
 */
 
 private function _copy_file()
{
	$this->file_excel_copy= FALSE;
	if( $this->_is_temp_file_name() )
	{
		$Names = $this->_read_file_name();
		$Files = $this->_read_file_path();
		
	// jika nama file di temukan dari sebelumnya  = sama maka rename file sebelum di copy 
		
		$copy_files=@copy( $this->_read_temp_name(), $this->_read_file_path());
		if($copy_files)
		{
			$this->file_excel_copy = TRUE;
		}
	}
	
	return $this->file_excel_copy;
} 
/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function _read_excel_header()
{
	$_conds = $this->_copy_file(); 
	
	$_callback = '';
	
	if( ($_conds) AND file_exists( $this->_read_file_path() ) )
	{
		ExcelImport()->_ReadData( $this->_read_file_path());
		$pos = 1; $num=0; $data = array();
		while( $pos <= ExcelImport()->rowcount(0) ) 
		{
			if( $pos==1) 
			{
				for( $i= 1; $i<=ExcelImport()->colcount(0); $i++) 
				{
					$xls_header_value = ExcelImport() ->val($pos,$i);
					if( strlen($xls_header_value) > 0  ) {
						$data[$i] = $xls_header_value;
					}
					
					/*
					 *$data[$i] = ExcelImport() ->val($pos,$i); 
					 */
				}
			}
			$pos++;
		}
		
		$_callback = $data;
	}	
	else
		$_callback = "Failed to copy data ";
		
		
	return $_callback;
} 


/*
 * @ def 		: Upload TXT 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _read_text_header()
{
	self::_copy_file(); 
	if( ($_conds) AND file_exists($this->_read_file_path()) ) 
 {
		TextImport()->ReadText($this->_read_file_path());
		TextImport()->setDelimiter('|');
		return TextImport()->getHeader();
	}	
	
	return "Failed to copy data ";
} 


/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _read_table_data()
{
	
 if( !isset(self::$_request['TemplateId']) )  {
	return NULL;
  }	
  
  $this->db->reset_select();
  $this->db->select('TemplateTableName', false);
  $this->db->from('t_gn_template');
  $this->db->where('TemplateId', self::$_request['TemplateId']);
  // echo "<pre>";
  // echo $this->db->_get_var_dump();
  // echo "</pre>"; exit;
  $rs = $this->db->get();
  
  if( $rs->num_rows() > 0 )  {
	return $rs->result_singgle_value();
 }
 
 return null;
 
}
 
 
/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _read_column_data()
{
	$_fields = array();
	
	if(isset(self::$_request['TemplateId']) )
	{	
		$this -> db ->reset_select();
		$this -> db ->select('UploadColsName, UploadColsAlias');
		$this -> db ->from('t_gn_detail_template');
		$this -> db ->where('UploadTmpId', self::$_request['TemplateId']);
		$this -> db ->order_by('UploadColsOrder','ASC');
		
		// echo "<pre>";
		// echo $this->db->_get_var_dump();
		// echo "</pre>"; exit;
		foreach( $this ->db ->get() ->result_assoc() as $rows )
		{
			$_fields[$rows['UploadColsName']] = $rows['UploadColsAlias'];	
		}
	}	
	
	return $_fields;
}


/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _read_upload_data( $combine = array(), $add_columns = null, $Template = NULL )
{
	$data = array();
	
	/// type upload data excel mode 1998 - 2013 
	
	if( strtolower($Template['TemplateFileType'])=='excel' )
	{
		ExcelImport() -> _ReadData( $this->_read_file_path() );
		$p = 2; $n=1; $count = ExcelImport()->rowcount(0);
		while( $p <=$count )
		{
			$_c = 1;
			
			foreach( $combine as $k => $v ){
				$data[$n][$v] = ExcelImport() ->val($p,$_c); 
				$_c++;
			}
			
		  // add columns 
		
			if( is_array($add_columns) AND count($add_columns)> 0 ) 
			{
				foreach( $add_columns as $field => $value ){
					$data[$n][$field] = $value;  	
				}
			}
				 
			$n++;  $p++; 
		}
		
	}	
	
	/// type upload data excel mode 1998 - 2013 
	
	if( strtolower($Template['TemplateFileType'])=='txt' ) 
	{
		TextImport() -> ReadText($this->_read_file_path());
		TextImport() -> setDelimiter($Template['TemplateSparator']);
		
		$p = 2; $n=1; 
		$count = TextImport()->getCount();
		while( $p <= $count ) 
		{
			$_c = 0; // start index 0 for txt file version 
			foreach( $combine as $k => $v ) {
				$data[$n][$v] = TextImport()->getValue( $p , $_c ); 
				$_c++;
			}
			
			if( is_array($add_columns) AND count($add_columns)> 0 ) 
			{
				foreach( $add_columns as $field => $value ) {
					$data[$n][$field] = $value;  	
				}
			}
				 
			$n++;  $p++; 
		}
		
	}	
			
	
	return $data;
}
 
/*
 * @ def : Global Upload By Template Create By UserId
 * ------------------------------------------------------
 * @ param : attribute data && file 
 * @ param : M_Template Class / under model class 
 */
 
public function setInsertUpload( $_array =null, $add_columns=null )
{
// echo "coba";exit;
 ob_start(); // start over buffer
  
  self::$_attributes =& $_array['file_attribut'];
  self::$_request =& $_array['request_attribut'];
  
  $TemplateName =& self::_read_table_data();
  $TemplateId =& self::$_request['TemplateId'];
  // print_r($TemplateName);exit;
  
  $is_conds	= FALSE;
  
  if(!is_null( self::$_attributes ) )
  {
	$Template =&M_Template::_getDetailTemplate($TemplateId);
	$UploadClass = &M_Template::_getTemplateModul($TemplateId); 
	//var_dump($UploadClass);exit;

 // @ pack : select modul class -------------------------------------------
	
	if( isset($UploadClass)) {
		$this->load->upload($UploadClass);
	}
	
 // @ pack :  included class  -------------------------------------------
	
	if( !class_exists($UploadClass) ) {
		return "\n\rmodul class not_found.";
		break;
	}
	
 // @ pack :  of header & columns args -------------------------------------------
	
	$ExcelHeader = ( strtoupper($Template['TemplateFileType'])=='TXT' ? self::_read_text_header() : self::_read_excel_header() );
	$TemplColumn  = self::_read_column_data();
	//var_dump($TemplColumn);exit;
	
  // @ cek files  ----------------------
	
	if( $this->file_excel_copy == FALSE ) 
	{
		$gFiles = self::$_attributes['fileToupload']['name'];
		return "\n\r Copy File { $gFiles } Error. \n\rTry upload with diffrent Name.";
		break;
	}
  
  
 // @ pack : content template is ok
	
	if( !is_array($TemplColumn) OR is_null($TemplColumn) )  {
		return 'Template not found.';
		break;
	}
	
 // @ pack : is match of upload with exist templates -------------------------------------------
	
	if( count($ExcelHeader) != count($TemplColumn)) 
	{
		return "\n\rColumn on excel { ". count($ExcelHeader). " } --> template { ". count($TemplColumn). " }.\n\r".
			   "Identification not match. Please check your file.";
		break;
	}
	
 // @ pack : header label && content tempalate -------------------------------------------
	
	 $LabelMatch =& M_Template::_getMatch($ExcelHeader, $TemplColumn);
	 if( !is_bool($LabelMatch) ) 
	 {
		$msg = "\n\rLabel No Match :\n\r";
		foreach( $LabelMatch as $key => $values ){
			$msg .= " on excel is { $values[N] } --> on template is { $values[Y] }\n\r";
		}
		
		$msg.=" Please chek you file.\n\r";
		return $msg;
		
		break;
	 }
	 
  // @ pack : sequence of template && content -------------------------------------------
	 
	 $LabelOrder =&M_Template::_getOrder($ExcelHeader, $TemplColumn);
	 if( $LabelOrder == FALSE ) {
		return $LabelOrder;
		break;
	 }
	 
  // @ pack : result of rows content -------------------------------------------
  
	$args_vars =& self::_read_upload_data( array_keys($TemplColumn), $add_columns, $Template );
	//var_dump($args_vars);exit;
	if( count($args_vars)==0 ){
		return '\n\rData empty .';
		break;
	}	
	
  // @ pack : next process if no Error -------------------------------------------
 
	$IsClass = call_user_func(array($UploadClass, "Instance"));
	// var_dump($IsClass);exit;
	if(!is_object($IsClass) ){
		return "\n\rNo call_user_func {$UploadClass}::Instance.";
		break;
	}
	
   $UploadId = self::_report_log_data('IGT'); // generate upload ID 
// @ pack : NEXT process  -------------------------------------------
	if( !$UploadId ){
		return "\n\rFailed Upload Report ";
		break;
	}
	
  // @ pack : NEXT process  -------------------------------------------
	// var_dump($UploadId);exit;
	

	$IsClass->_set_uploadId($UploadId); 
	$IsClass->_set_table($TemplateName);
	$IsClass->_set_content($args_vars);
	if( TRUE == $IsClass->_get_is_complete() ) {
		$IsClass->_set_process($args_vars);
	}
	
 // @ pack : result object ------------------------------------------- 
	$stdClass = $IsClass->_get_class_callback();
	if( is_object($stdClass) )
	{
		$this->db->set('FTP_UploadRows',$stdClass->TOTAL_UPLOAD );
		$this->db->set('FTP_UploadSuccess',$stdClass->TOTAL_SUCCES);
		$this->db->set('FTP_UploadFailed',$stdClass->TOTAL_FAILED);
		$this->db->set('FTP_UploadDuplicate',$stdClass->TOTAL_DUPLICATE);
		$this->db->set('FTP_UploadEndDateTs',date('Y-m-d H:i:s'));
		$this->db->where('FTP_UploadId', self::$_reportid);
		$this->db->update('t_gn_upload_report_ftp');
		if(($this->db->affected_rows() > 0)) 
		{
			
			$uplod_original_file =  APPPATH . 'temp/' . $this->_read_file_name();
			$uplod_destination_file = APPPATH . 'temp/' . $this->FileExploits( $this->_read_file_name() );
			
			// @ pack : rename file : 
			
			if( file_exists($uplod_original_file) )
			{
				rename($uplod_original_file,$uplod_destination_file);
			}
			$is_conds = true;
		}
	}
	
 // @ pack : if not read and insert to table.  	
 
	$_conds = array();
	if( $is_conds )
	{
		$_conds = array(
			'N' => $stdClass->TOTAL_FAILED,
			'Y' => $stdClass->TOTAL_SUCCES,
			'D' => $stdClass->TOTAL_DUPLICATE,
			'O' => $stdClass->TOTAL_NOMASTERDEB_FOUND,
			'B' => 0,
			'X' => 0 
		);
	}
	
	return array($_conds);
 }
  
  ob_end_clean(); // clear buffer 
  
}
 
 // stop global upload 
 
 /*
 * @ def : Global Upload By Template Create By UserId
 * ------------------------------------------------------
 * @ param : attribute data && file 
 * @ param : M_Template Class / under model class 
 */
 
public function setUpdateUpload($_array =null, $add_columns=null )
{

  self::$_attributes = $_array['file_attribut'];
  self::$_request = $_array['request_attribut'];
	
  $template_name_table = self::_read_table_data();
  
  if( !is_null(self::$_attributes) )
  {
	$_read_excel_header = self::_read_excel_header();
	$_read_column_data  = self::_read_column_data();
	
	if( is_array($_read_excel_header) AND is_array($_read_column_data) ) 
	{
		if( (count($_read_excel_header) == count($_read_column_data))  )
		{
			$is_match =& M_Template::_getMatch($_read_excel_header, $_read_column_data);
			if( !is_array($is_match) AND $is_match!=FALSE )
			{
				$is_order =& M_Template::_getOrder($_read_excel_header, $_read_column_data);
				$is_keys  =& M_Template::_getKeys(self::$_request['TemplateId']);
				
				if( !is_array($is_order) AND $is_order!=FALSE ) 
				{
					$result =& self::_read_upload_data( array_keys($_read_column_data), $add_columns);
					if(count($result) > 0 ) 
					{
						self::_report_log_data('UGT'); // insert global tables;
						
						$_success = 0; $_failed = 0; $_duplicate = 0;  $num =1; 
						foreach( $result as $k => $data)
						{
							foreach($data as $cols => $value) {
								if(in_array($cols, $is_keys))
									$this -> db -> where($cols, $value);
								else
									$this -> db -> set($cols, $value);
							}
							
							$this->db->update($template_name_table);
							
							if( $this->db->affected_rows() > 0)
							{
								if( $this -> db -> update('t_gn_upload_report_ftp',array(
									'FTP_UploadRows' 	=> $num,
									'FTP_UploadSuccess' => ($_success+1),
									'FTP_UploadDateTs' 	=> date('Y-m-d H:i:s')
									), 
									array('FTP_UploadId'=>self::$_reportid )))
								{
									$_success++; 
								}
							}
							else
							{
								$_error = $this->db->_error_message(); // get error 
								if( $this -> db -> update('t_gn_upload_report_ftp',array(
									'FTP_UploadRows' 	=> $num,
									'FTP_UploadFailed' 	=> ($_failed+1),
									'FTP_UploadDateTs' 	=> date('Y-m-d H:i:s')
									), 
									array('FTP_UploadId'=>self::$_reportid )))
								{
									if( preg_match('/\Dup/i',$_error ) ) $_duplicate++;
									else
										$_failed++;
								}
							}
							
							$num++;
						}
						
						$_conds = array(0=>array('N' => $_failed, 'Y' => $_success));
						return $_conds;
					}
					else { return 'Data empty .'; }
				}
				else { return $is_order; }
			}
			else { return $is_match; }
		}
		// column not match 
		else 
		{	 
			return 'Column identification not match. Please check your file.'; 
			break;
		}
	}
	
	// cek avail template 
	else
	{
		return 'Tempalate not found.';
		break;
	}
	
  }
  
} 

// edit hilman 13-5-2022 besok
public function setInsertUploadquestioner( $_array =null, $add_columns=null )
{
	ob_start(); // start over buffer
	
	self::$_attributes =& $_array['file_attribut'];
	self::$_request =& $_array['request_attribut'];
	
	$TemplateName =& self::_read_table_data();
	$TemplateId =& self::$_request['TemplateId'];
	//   print_r($TemplateName);exit;
	
	$is_conds	= FALSE;
	
	if(!is_null( self::$_attributes ) )
	{
		$Template =&M_Template::_getDetailTemplate($TemplateId);
		$UploadClass = &M_Template::_getTemplateModul($TemplateId);
		// echo "<pre>";
		// print_r($UploadClass);exit;

	// @ pack : select modul class -------------------------------------------
		
	// 	if( isset($UploadClass)) {
	// 		$this->load->upload($UploadClass);
	// 	}
		
	//  // @ pack :  included class  -------------------------------------------
		
	// 	if( !class_exists($UploadClass) ) {
	// 		return "\n\rmodul class not_found.";
	// 		break;
	// 	}
		
	// @ pack :  of header & columns args -------------------------------------------
		
		$ExcelHeader = ( strtoupper($Template['TemplateFileType'])=='TXT' ? self::_read_text_header() : self::_read_excel_header() );
		$TemplColumn  = self::_read_column_data();
		// echo '<pre>';
		// var_dump($ExcelHeader);
		// var_dump($TemplColumn);
		// exit;
		
	// @ cek files  ----------------------
	//   var_dump($this->file_excel_copy == FALSE);
	//   die;
		if( $this->file_excel_copy == FALSE ) 
		{
			$gFiles = self::$_attributes['fileToupload']['name'];
			return "\n\r Copy File { $gFiles } Error. \n\rTry upload with diffrent Name.";
			break;
		}
	
	
	// @ pack : content template is ok
		
		if( !is_array($TemplColumn) OR is_null($TemplColumn) )  {
			return 'Template not found.';
			break;
		}
		
	// @ pack : is match of upload with exist templates -------------------------------------------
		// echo '<pre>';
		// var_dump(count($ExcelHeader) )."<br>";
		// var_dump( count($TemplColumn));
		// die;
		if( count($ExcelHeader) != count($TemplColumn)) 
		{
			return "\n\rColumn on excel { ". count($ExcelHeader). " } --> template { ". count($TemplColumn). " }.\n\r".
				"Identification not match. Please check your file.";
			break;
		}
			
	// @ pack : header label && content tempalate -------------------------------------------
		
		$LabelMatch =& M_Template::_getMatch(strtolower($ExcelHeader), strtolower($TemplColumn));
		// var_dump($TemplColumn);
		// die;

		if( !is_bool($LabelMatch) ) 
		{
			$msg = "\n\rLabel No Match :\n\r";
			foreach( $LabelMatch as $key => $values ){
				$msg .= " on excel is { $values[N] } --> on template is { $values[Y] }\n\r";
			}
			
			$msg.=" Please chek you file.\n\r";
			return $msg;
			
			break;
		}
		
	// @ pack : sequence of template && content -------------------------------------------
		
		$LabelOrder =&M_Template::_getOrder($ExcelHeader, $TemplColumn);
		if( $LabelOrder == FALSE ) {
			return $LabelOrder;
			break;
		}
		
	// @ pack : result of rows content -------------------------------------------
	
		$args_vars =& self::_read_upload_data( array_keys($TemplColumn), $add_columns, $Template );
		
		$totUpdate = 0;
		$dp = 0;
		// var_dump( $this->URI->_get_post('kategori_id'));
		// die;
		foreach ($args_vars as $value) {
			// print_r($value);
			// exit;

			$this->db->reset_write();
			$this->db->set('questioner_desc',$value['tipe']);
			$this->db->set('kategori_id', $this->URI->_get_post('kategori_id'));
			$this->db->set('questioner_flag','1');
			$this->db->insert('t_gn_questioner');
			if( $this->db->affected_rows() > 0 )
				$InsertId_quest = $this -> db->insert_id();
			

			$this->db->reset_write();
			$this->db->set('survey_question',$value['pertanyaan']);
			$this->db->set('quest_id',$InsertId_quest);		
			$this->db->insert('t_lk_question_survey');

			if( $this->db->affected_rows() > 0 )
				$InsertId_qs = $this -> db->insert_id();
			
			$this->db->reset_write();
			$this->db->set('quest_id',$InsertId_quest);
			$this->db->set('a',$value['a']);
			$this->db->set('b',$value['b']);
			$this->db->set('c',$value['c']);
			$this->db->set('d',$value['d']);
			$this->db->set('jawabanbenar',$value['jawabanbenar']);
			$this->db->insert('t_lk_type_ans_survey');
			if($this->db->affected_rows() > 0) {					
				$totUpdate += 1;
			}else{					
				// $totUpdate += 0;
				$dp += 1;	
			}
			
		}

		// die;
		// var_dump($args_vars);

		if( count($args_vars)==0 ){
			return '\n\rData empty .';
			break;
		}	
		
	// @ pack : next process if no Error -------------------------------------------
	
		$IsClass = call_user_func(array($UploadClass, "Instance"));
		// var_dump($IsClass);exit;
		// if(!is_object($IsClass) ){
		// 	return "\n\rNo call_user_func {$UploadClass}::Instance.";
		// 	break;
		// }
		
	$UploadId = self::_report_log_data('IGT'); // generate upload ID 
	// @ pack : NEXT process  -------------------------------------------
		if( !$UploadId ){
			return "\n\rFailed Upload Report ";
			break;
		}
		
	// @ pack : NEXT process  -------------------------------------------
		// var_dump($UploadId);exit;
		

		// $IsClass->_set_uploadId($UploadId); 
		// $IsClass->_set_table($TemplateName);
		// $IsClass->_set_content($args_vars);
		


		// if( TRUE == $IsClass->_get_is_complete() ) {
		// 	$IsClass->_set_process($args_vars);
		// }
		
	// @ pack : result object ------------------------------------------- 
		// $stdClass = $IsClass->_get_class_callback();
		// if( is_object($stdClass) )
		// {
		// 	$this->db->set('FTP_UploadRows',$stdClass->TOTAL_UPLOAD );
		// 	$this->db->set('FTP_UploadSuccess',$stdClass->TOTAL_SUCCES);
		// 	$this->db->set('FTP_UploadFailed',$stdClass->TOTAL_FAILED);
		// 	$this->db->set('FTP_UploadDuplicate',$stdClass->TOTAL_DUPLICATE);
		// 	$this->db->set('FTP_UploadEndDateTs',date('Y-m-d H:i:s'));
		// 	$this->db->where('FTP_UploadId', self::$_reportid);
		// 	$this->db->update('t_gn_upload_report_ftp');
		// 	if(($this->db->affected_rows() > 0)) 
		// 	{
				
		// 		$uplod_original_file =  APPPATH . 'temp/' . $this->_read_file_name();
		// 		$uplod_destination_file = APPPATH . 'temp/' . $this->FileExploits( $this->_read_file_name() );
				
		// 		// @ pack : rename file : 
				
		// 		if( file_exists($uplod_original_file) )
		// 		{
		// 			rename($uplod_original_file,$uplod_destination_file);
		// 		}
		// 		$is_conds = true;
		// 	}
		// }
		
	// @ pack : if not read and insert to table.  	
	
		$_conds = array();
		// if( $is_conds )
		// {
			$_conds = array(
				'R' => $totUpdate,
				'N' => ($totUpdate - $dp ) - $totUpdate,
				'Y' => $totUpdate,
				'D' => $dp,
				'O' => 0,
				'B' => 0,
				'X' => 0 			
			);
		// }
		
		return array($_conds);
	}
	
	ob_end_clean(); // clear buffer 
  
}

 
}
