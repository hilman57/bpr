<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class M_ModVoiceData Extends EUI_Model
{

/* 
 * @ pack :  instance  
 * ---------------------------------------------------------------
 */
 
 private static $Instance = NULL;

 /* 
 * @ pack :  instance  
 * ---------------------------------------------------------------
 */
 
 public static function &Instance()
{
  if(is_null(self::$Instance) ) {
	self::$Instance = new self();
  }
  return self::$Instance;
  
 }
 
// -----------------------------------------------------------------
/* 
 * @ pack : --- session filter adn cache 
 * @ params : null 
 */
 
 public function M_ModVoiceData() { }
  
// -----------------------------------------------------------------
/* 
 * @ pack : --- session filter adn cache 
 * @ params : null 
 */
  
 function __construct()
{ 
  $this->constant = (object)array(
	'start_time' => join(" ", array( date('Y-m-d'), '00:00:00')),
	'end_time' =>join(" ", array( date('Y-m-d'), '23:59:59'))
  );
}
 
 
 
 public function _get_default()
 {
	$this->EUI_Page->_setPage(20);
	$this->EUI_Page->_setSelect("a.id as RecordId");
	// $this->EUI_Page->_setFrom("cc_recording a ");
	// $this->EUI_Page->_setJoin("t_gn_debitur c "," a.assignment_data =c.deb_acct_no", "LEFT");
	// $this->EUI_Page->_setJoin("t_lk_account_status d "," c.deb_call_status_code =d.CallReasonCode", "LEFT");
	// $this->EUI_Page->_setJoin("t_gn_campaign e "," c.deb_cmpaign_id=e.CampaignId", "LEFT");
	// $this->EUI_Page->_setJoin("cc_agent f "," a.agent_id=f.id", "LEFT");
	// $this->EUI_Page->_setJoin("t_tx_agent g "," f.userid=g.id");
	// $this->EUI_Page->_setJoin("t_gn_campaign_project k "," e.CampaignId=k.CampaignId","LEFT", TRUE);
	
	$this->EUI_Page->_setFrom("cc_recording a ");
	$this->EUI_Page->_setJoin("t_gn_debitur b ","a.assignment_data =b.deb_acct_no","INNER");
	$this->EUI_Page->_setJoin("t_lk_account_status c ","b.deb_call_status_code=c.CallReasonCode","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d ","b.deb_cmpaign_id=d.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("cc_agent e ","a.agent_id=e.id","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent f "," f.userid=e.id","LEFT",TRUE);
	// $this->EUI_Page->_setJoin("t_gn_campaign_project g "," d.CampaignId = g.CampaignId","LEFT", TRUE);
	
 /* 
  * @ pack : --- session filter adn cache 
  * ---------------------------------------------------------------
  */
  
	// $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId'));
	// if(_get_session('HandlingType')== USER_SUPERVISOR){ 
		// $this->EUI_Page->_setAnd("f.spv_id",_get_session('UserId'));
	// }	
	
	if(_get_session('HandlingType')== USER_LEADER) {
		$this->EUI_Page->_setAnd("f.tl_id",_get_session('UserId'));
	}
	
	if(_get_session('HandlingType')== USER_SENIOR_TL) {
		$this->EUI_Page->_setAnd("f.stl_id",_get_session('UserId'));
	}
	
	if(_get_session('HandlingType')== USER_AGENT_OUTBOUND) {
		$this->EUI_Page->_setAnd("f.UserId",_get_session('UserId'));
	}
	
	if(_get_session('HandlingType')==  	USER_AGENT_INBOUND) {
		$this->EUI_Page->_setAnd("f.UserId",_get_session('UserId'));
	}
	
	if(!_get_have_post('voice_end_date') 
	   AND !_have_get_session('voice_end_date') )
	{
		$this->EUI_Page->_setAnd("a.start_time >='{$this->constant->start_time}'");	
		$this->EUI_Page->_setAnd("a.start_time <='{$this->constant->end_time}'");	
	}
 /* 
  * @ pack : --- session filter adn cache 
  * ---------------------------------------------------------------
  */
	
	$this->EUI_Page->_setLikeCache("b.deb_name", 'voice_cust_name', TRUE);
	$this->EUI_Page->_setAndCache("b.deb_cmpaign_id", 'voice_campaign_id', TRUE);
	$this->EUI_Page->_setAndCache("b.deb_call_status_code", 'voice_call_result', TRUE);
	$this->EUI_Page->_setAndCache("f.UserId", 'voice_user_id', TRUE);
	$this->EUI_Page->_setLikeCache("a.anumber", 'voice_destination', TRUE);
	$this->EUI_Page->_setLikeCache("b.deb_acct_no", 'voice_cust_number', TRUE);
	$this->EUI_Page->_setAndOrCache("a.start_time>='" . _getDateEnglish(_get_post('voice_start_date'))." 00:00:00'",'voice_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.start_time<='" . _getDateEnglish(_get_post('voice_end_date'))." 23:59:59'" , 'voice_end_date', TRUE);
	// echo $this->EUI_Page->_getCompiler();
 /* 
  * @ pack : --- session filter adn cache 
  * ---------------------------------------------------------------
  */
  
	// $this->EUI_Page->_setGroupBy('a.id');
	// echo $this->EUI_Page->_getCompiler();
	
	if($this->EUI_Page->_get_query()) 
	{
		return $this->EUI_Page;
	}
	
 } // ==> get_default
 
 
// -----------------------------------------------------------------
/* 
 * @ pack : --- session filter adn cache 
 * @ params : null 
 */
 
 public function _get_content()
{
    $this->EUI_Page->_postPage((INT)_get_post('v_page'));
	$this->EUI_Page->_setPage(20); 
	
	$this->EUI_Page->_setArraySelect(array(
		"a.id as chk_record_call" => array("chk_record_call","chk_record_call","primary"),
		"b.deb_id as deb_id" => array("deb_id","deb_id", "hidden"),
		"d.CampaignCode as CampaignCode" => array("CampaignCode","Campaign"),
		"b.deb_acct_no as AccountNo" => array("AccountNo","Customer ID"),
		"b.deb_name as CustomerName" => array("CustomerName","Nama Pelanggan"),
		"b.deb_amount_wo as AmountWO" => array("AmountWO","Jumlah WO"),
		"a.anumber as DialNo" => array("DialNo","Nomor Panggilan"),
		"f.id as UserCode" => array("UserCode","ID Pengguna"),
		"f.full_name as Fullname" => array("Fullname","Nama Pengguna"),
		"a.agent_ext as AgentExt" => array("AgentExt","Extension"),
		"a.file_voc_name as FileName" => array("FileName","Nama File"), 
		"a.start_time as CallDate" => array("CallDate","Tanggal Panggilan"), 
		"a.duration as Duration" => array("Duration","Durasi"), 
		"a.file_voc_size as FileSize" => array("FileSize","Ukuran File")
	));
		
	$this->EUI_Page->_setFrom("cc_recording a ");
	$this->EUI_Page->_setJoin("t_gn_debitur b ","a.assignment_data =b.deb_acct_no","INNER");
	$this->EUI_Page->_setJoin("t_lk_account_status c ","b.deb_call_status_code=c.CallReasonCode","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d ","b.deb_cmpaign_id=d.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("cc_agent e ","a.agent_id=e.id","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent f "," f.userid=e.id","LEFT",TRUE);
	// $this->EUI_Page->_setJoin("t_gn_campaign_project g "," d.CampaignId = g.CampaignId","LEFT", TRUE);

 /* 
  * @ pack : --- session filter adn cache 
  * ---------------------------------------------------------------
  */
	
	// $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId'));
	// if(_get_session('HandlingType')== USER_SUPERVISOR){ 
		// $this->EUI_Page->_setAnd("f.spv_id",_get_session('UserId'));
	// }	
	
	if(_get_session('HandlingType')== USER_LEADER) {
		$this->EUI_Page->_setAnd("f.tl_id",_get_session('UserId'));
	}
	
	if(_get_session('HandlingType')== USER_SENIOR_TL) {
		$this->EUI_Page->_setAnd("f.stl_id",_get_session('UserId'));
	}
	
	if(_get_session('HandlingType')== USER_AGENT_OUTBOUND) {
		$this->EUI_Page->_setAnd("f.UserId",_get_session('UserId'));
	}
	
	if(_get_session('HandlingType')==  	USER_AGENT_INBOUND) {
		$this->EUI_Page->_setAnd("f.UserId",_get_session('UserId'));
	}
	if(!_get_have_post('voice_end_date') 
	   AND !_have_get_session('voice_end_date') )
	{
		$this->EUI_Page->_setAnd("a.start_time >='{$this->constant->start_time}'");	
		$this->EUI_Page->_setAnd("a.start_time <='{$this->constant->end_time}'");	
	}
	// echo $this->EUI_Page->_getCompiler();
	
 /* 
  * @ pack : --- session filter adn cache 
  * ---------------------------------------------------------------
  */
	
	$this->EUI_Page->_setLikeCache("b.deb_name", 'voice_cust_name', TRUE);
	$this->EUI_Page->_setAndCache("b.deb_cmpaign_id", 'voice_campaign_id', TRUE);
	$this->EUI_Page->_setAndCache("b.deb_call_status_code", 'voice_call_result', TRUE);
	$this->EUI_Page->_setAndCache("f.UserId", 'voice_user_id', TRUE);
	$this->EUI_Page->_setLikeCache("a.anumber", 'voice_destination', TRUE);
	$this->EUI_Page->_setLikeCache("b.deb_acct_no", 'voice_cust_number', TRUE);
	$this->EUI_Page->_setAndOrCache("a.start_time>='" . _getDateEnglish(_get_post('voice_start_date'))." 00:00:00'",'voice_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.start_time<='" . _getDateEnglish(_get_post('voice_end_date'))." 23:59:59'" , 'voice_end_date', TRUE);
	
 /* 
  * @ pack : --- session filter adn cache 
  * ---------------------------------------------------------------
  */
  
	// $this->EUI_Page->_setGroupBy('a.id');
	
	// if(!_get_have_post('order_by'))
		// $this->EUI_Page->_setOrderBy('a.start_time', 'DESC'); 
	// else
		// $this -> EUI_Page -> _setOrderBy($this -> URI ->_get_post('order_by'),$this -> URI ->_get_post('type')); 
    
	// echo $this->EUI_Page->_getCompiler();
	$this->EUI_Page->_setLimit();	
	
}

// -----------------------------------------------------------------
 
 /* 
  * @ pack : --- session filter adn cache 
  * @ params : null 
  */
 
 function _getVoiceData($VoiceId=0 )
 {
	$this -> db -> select("*");
	$this -> db -> from("cc_recording a");
	$this -> db -> where('id',$VoiceId);
	
	
	$_result =  array();
	
	if( $_conds = $this -> db->get() -> result_first_assoc() )
	{
		foreach($_conds as $fld => $values )
		{
			if( $fld=='file_voc_size' ) 
				$_result[$fld] = $this->EUI_Tools->_get_format_size($values);
				
			else if( $fld=='duration' ) 
				$_result[$fld] = $this->EUI_Tools->_set_duration($values);
				
			else if( $fld=='anumber' ) 
				$_result[$fld] = $this->EUI_Tools->_getPhoneNumber($values);	
				
			else if( $fld=='start_time' ) 
				$_result[$fld] = $this->EUI_Tools->_datetime_indonesia($values);	
				
			else 
				$_result[$fld] = $values;
		}
		
		return $_result;
	}
	else
		return null;
 }


// -----------------------------------------------------------------
/* 
 * @ pack : --- session filter adn cache 
 * @ params : null 
 */
 
public function _get_resource()
 {
	self::_get_content();
	if( $this->EUI_Page->_get_query()!='') 
	{
		return $this->EUI_Page-> _result();
	}	
 }
 
// -----------------------------------------------------------------
/* 
 * @ pack : --- session filter adn cache 
 * @ params : null 
 */
 
 public function _get_page_number() 
{
	
  if( $this->EUI_Page->_get_query()!='' )
  {
	return $this->EUI_Page->_getNo();
  }
  
 }
 
// -----------------------------------------------------------------
/* 
 * @ pack : --- session filter adn cache 
 * @ params : null 
 */
 
 public function  _get_voice_content( $DebiturId = 0 )
{
  $result = array();
  $this->db->reset_select();
  $this->db->select(
	'a.id as rec_id,
	 a.agent_ext, a.start_time, a.file_voc_size, 
	 a.duration, a.file_voc_name, 
	 d.init_name, d.full_name', FALSE);
		
  $this->db->from('cc_recording a');
  $this->db->join('t_gn_debitur b ','a.assignment_data=b.deb_acct_no', 'LEFT');
  $this->db->join('cc_agent c ','a.agent_id=c.id', 'LEFT');
  $this->db->join('t_tx_agent d ','c.userid=d.id', 'LEFT');
  $this->db->where('b.deb_id', $DebiturId);
  
  $qry = $this->db->get();
  if( $qry->num_rows()>0 )
  {
	$result = $qry->result_assoc();
  }
  
  return $result;
}
 
 
// END CLASS 

}
?>