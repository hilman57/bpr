<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetLastCall modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SetWorkProject 
	extends EUI_Model
{

/* pack : instance of class **/


private static $Instance = null;


/* pack : instance of class **/

public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery("SELECT * FROM t_lk_work_project a"); 
	
	$filter = '';
	
	if( $this->URI->_get_have_post('key_words') ) 
	{
		$filter ="";
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	if( $this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
  $this -> EUI_Page ->_setQuery( "SELECT * FROM t_lk_work_project a 
								  LEFT JOIN t_tx_agent b ON a.ProjectCreateById=b.UserId " );
  $filter = '';
  if( $this -> URI -> _get_have_post('key_words') ) {
	$filter = " ";	
  }				
		
  $this -> EUI_Page->_setWhere();
  $this -> EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
/** get detail data **/

public function _getWorkProject( $ProjectId = 0  ) 
{
	$conds = array();
	
	$this ->db ->select('*');
	$this ->db ->from('t_lk_work_project');
	$this ->db ->where('ProjectId', $ProjectId);
	if( $rs = $this ->db->get() -> result_first_assoc() ) {
		$conds = $rs;
	}
	
	return $conds;
}

 /* function get Product code **/
 
public  function _getWorkProjectId()
 {
	$works = array();
	
	$this->db->select('*');
	$this->db->from('t_lk_work_project');
	$this->db->where('ProjectFlags',1);
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$works[$rows['ProjectId']] = $rows['ProjectName'];
	}
	
	return $works;
	
 } 
 
 /* function get Product code **/
 
 public  function _getWorkProjectCode()
 {
	$works = array();
	
	$this->db->select('*');
	$this->db->from('t_lk_work_project');
	$this->db->where('ProjectFlags',1);
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$works[$rows['ProjectCode']] = $rows['ProjectName'];
	}
	
	return $works;
	
 }
 
 
  /* function get TemplateName **/
 
 public function _getTemplateName() 
 {
	$works = array();
	
	$this->db->select('*');
	$this->db->from('t_gn_template');
	$this->db->where('TemplateFlags',1);
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$works[$rows['TemplateId']] = $rows['TemplateName'];
	}
	
	return $works;
	
 }
 
 /* _setFTPSave **/
 
 public function _setWorkProjectUpdate( $params = null )
 {
	$conds = 0;
	if( !is_null($params) )
	{
		foreach( $params as $fieldname => $fieldvalue ) 
		{
			if( $fieldname!='ProjectId' ){
				$this->db->set($fieldname, $fieldvalue);
			}
			else{
				$this->db->where($fieldname, $fieldvalue);
			}
		}
		
		$this ->db->update('t_lk_work_project');
		if($this ->db->affected_rows() > 0)
			$conds++;
	}
	
	return $conds;
 }
 
 
 /* _setFTPSave **/
 
 public function _setWorkProjectSave( $params = null )
 {
	$conds = 0;
	if( !is_null($params) )
	{
		foreach( $params as $fieldname => $fieldvalue ) {
			$this->db->set($fieldname, $fieldvalue);
		}
		$this->db->set('ProjectCreateTs', date('Y-m-d H:i:s'));
		$this->db->set('ProjectCreateById', $this->EUI_Session->_get_session('UserId'));
		$this->db->insert('t_lk_work_project');
		
		if($this ->db->affected_rows() > 0)
			$conds++;
	}
	
	return $conds;
 }
 
 
 // ** _setFTPDelete **/
 
 public function _setWorkProjectDelete($ProjectId  = null )
 {
	$conds = 0;
	if( !is_null($ProjectId) ) 
	{
		foreach( $ProjectId  as $k => $fieldId )
		{
			$this->db->where('ProjectId', $fieldId);
			$this->db->delete('t_lk_work_project');
			if( $this->db->affected_rows() > 0 ){
				$conds++;
			}
		}
	}
	
	return $conds;
	
 } 
 
 
/*
 * @ pack : handle by session ID 
 */ 
 
 public function _setAutomaticRegister( $UserId = null )
{
 $conds = 0;
 $work = $this->_getWorkProjectId();
 if( count($work) == 1 )
 {
	if( !is_null($UserId) ){
		$this->db->set('UserId',$UserId);
		$this->db->set('UserCreateId',$UserId);
	} else {
		$this->db->set('UserId',_get_session('UserId'));
		$this->db->set('UserCreateId',_get_session('UserId'));
	}
	
	$this->db->set('ProjectId',reset(array_keys($work)) );
	$this->db->set('CreateTs',date('Y-m-d H:i:s'));
	$this->db->set('Status',1); // active 
	
	$this->db->insert('t_gn_user_project_work');
	if( $this->db->affected_rows() > 0 )
	{
		$conds++;
		$this->load->model('M_Loger');
		$InsertId = $this->db->insert_id();
		if( $InsertId ) 
		{
			$this->M_Loger->set_activity_log("AUTOMATIC SET WORK USER PROJECT[$InsertId]");
		}
	}
 }
 
 return $conds;

}
 
// END CLASS  
}

?>