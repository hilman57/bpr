<?php
/**
 * @ model data get website parameter 
 * @ return  : array 
 * @ author  : omens 
**/

class M_Website extends EUI_Model{


/**
 *@ constructor 
 **/
 
function M_Website()
{
	$this -> load -> meta('_web_config'); 
}

/**
 **
 **/
 
function _web_query( $_modul ='WEBSITE', $_param = 'PARAM_NAME')
{
	$sql = " SELECT ". $this -> _web_config -> _get_cols_select() ." FROM ". $this -> _web_config -> _get_meta_index() ."
			 WHERE ". $this -> _web_config -> _get_cols_post(0) ."  = '$_modul' 
			 AND ". $this -> _web_config -> _get_cols_post(1)  ." = '$_param'";
			
	$qry = $this -> db -> query($sql);
	// print_r($this->db->last_query());
	// die;
	if(!$qry -> EOF() ) 
	{
		$_names = $qry -> result_first_assoc();
	}
	
	return $_names;
}
/**
 **
 **/
 function _web_title()
 {
	$_result  = self::_web_query( 'WEBSITE', 'TITLE' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
 }
 
/**
 **
 **/
 
 function _web_verion()
 {
	$_result  = self::_web_query( 'WEBSITE', 'VERSION' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
 }
 
 /**
 **
 **/
 function _web_author()
 {
	$_result  = self::_web_query( 'WEBSITE', 'AUTHOR' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
 }
 
 /**
 **
 **/
 function _web_copyright()
 {
	$_result  = self::_web_query( 'WEBSITE', 'COPYRIGHT' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
 }
 
 /**
 **
 **/
 function _web_logo_dark()
 {
	$_result  = self::_web_query( 'WEBSITE', 'LOGO_DARK' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
 }
 
/**
 **
 **/
 
 function _web_logo_orange()
 {
	$_result  = self::_web_query( 'WEBSITE', 'LOGO_ORANGE' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
 }
 
/**
 **
 **/
 
 function _web_themes()
 {
	$_result  = self::_web_query( 'CONFIG', 'THEME' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
		
 }
 
 
/**
 **
 **/
 
 function _web_default()
 {
	$_result  = self::_web_query( 'INSTRUCTION', 'INSTRUCTION' );
	if( $_result ){
		return $_result['content'];
	}
	else
		return null;
		
	
 }
 
/**
 **
 **/
 function _web_company_name()
 {
	$_result  = self::_web_query( 'COMPANY', 'COMPANY_NAME' );
	if( $_result ){
		return $_result['param_value'];
	}
	else
		return null;
	
 }
 
 function _web_get_data()
 {
	$_website['website'] = array
	(
		'_web_company_name' => self::_web_company_name(),
		'_web_default' 		=> self::_web_default(),
		'_web_themes' 		=> self::_web_themes(),
		'_web_logo_orange' 	=> self::_web_logo_orange(),
		'_web_logo_dark' 	=> self::_web_logo_dark(),
		'_web_copyright' 	=> self::_web_copyright(),
		'_web_author' 		=> self::_web_author(),
		'_web_title' 		=> self::_web_title(),
		'_web_verion' 		=> self::_web_verion() 
	);
	
	return $_website;
 
 }

} 
?>