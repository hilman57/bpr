<?php

/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SrcAppoinment extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function M_SrcAppoinment()
{
	$this -> load -> model(array('M_SetCallResult'));
	
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function CallBackLater()
{
	$_conds = array(); $_a = array(); 
	if(class_exists('M_SetCallResult') )
	{
		$_data = $this -> M_SetCallResult -> _getCallback();
		foreach( $_data as $_k => $_v )
		{
			$_conds[$_k] = $_k;  
		}
	}
	return $_conds;
}
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_default()
{
 /** set pages **/
 
 $this->EUI_Page->_setPage(10); 
 $this->EUI_Page->_setSelect("a.deb_id");
 $this->EUI_Page->_setFrom('t_gn_debitur a');
 $this->EUI_Page->_setJoin('t_gn_assignment b ','a.deb_id=b.CustomerId','INNER');
 $this->EUI_Page->_setJoin('t_gn_campaign c ',' a.deb_cmpaign_id=c.CampaignId','LEFT');
 $this->EUI_Page->_setJoin('t_gn_appoinment d ',' a.deb_id=d.CustomerId','LEFT', TRUE);
 
/** set filter **/
 
 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL');
 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL');
 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL');
 $this->EUI_Page->_setWherein('a.deb_call_status_code', self::CallBackLater() );
$this->EUI_Page->_setWherein('d.ApoinmentFlag', array(0,1));
	
/** set fiter by session ID **/
			
 if( $this ->EUI_Session ->_get_session('HandlingType')== USER_SUPERVISOR )	
	 $this->EUI_Page->_setAnd('b.AssignSpv', $this->EUI_Session->_get_session('UserId'));
		
 if( $this ->EUI_Session ->_get_session('HandlingType')== USER_AGENT_OUTBOUND )
	 $this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	
 if( $this ->EUI_Session ->_get_session('HandlingType')== USER_AGENT_INBOUND )
	 $this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
		
		
/** set filter post cache ***/
	
	$this->EUI_Page->_setLikeCache('a.deb_name', 'app_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'app_cif_number', TRUE);
	$this->EUI_Page->_setAndCache('b.AssignSelerId', 'app_dc', TRUE);
	$this->EUI_Page->_setAndCache('c.CampaignId', 'app_campaign', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'app_call_reason', TRUE);
	

/** set filter by REQUEST **/
	
	if( $this->URI->_get_have_post('app_start_date'))
	{
		$this->EUI_Page->_setAndOrCache("
			DATE(d.ApoinmentDate)>= '". date('Y-m-d',strtotime($this->URI->_get_post('app_start_date')))."'
			AND DATE(d.ApoinmentDate)<= '". date('Y-m-d',strtotime($this->URI->_get_post('app_end_date')))."' ", 'app_start_date', TRUE);
	}
	else{
		$this->EUI_Page->_setAnd("DATE(d.ApoinmentDate)=DATE(NOW())");
	}
	
	//echo $this ->EUI_Page->_getCompiler();
	if($this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{

/** set pages **/
 $this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
 $this->EUI_Page->_setPage(10); 
 $this->EUI_Page->_setArraySelect(array(
	 "a.deb_id AS chk_cust_appoinment" => array('chk_cust_appoinment','ID', 'primary'),
	 "a.deb_acct_no AS deb_acct_no" => array('deb_acct_no', 'Acct No'),
	 "a.deb_name AS CustomerFirstName" => array('CustomerFirstName', 'Nama pemilik'),
	 "e.CallReasonDesc AS CallReasonDesc" => array('CallReasonDesc', 'Call Status'),
	 "DATE_FORMAT(d.ApoinmentDate, '%d/%m/%Y %H:%i') AS ApoinmentDate" => array('ApoinmentDate', 'Tanggal Janji'), 
	 "DATE_FORMAT(d.ApoinmentCreate, '%d/%m/%Y %H:%i:%s') AS ApoinmentCreate" => array('ApoinmentCreate','Tanggal Panggilan Terakhir')
	 
  ));
  /*
 "IF(d.ApoinmentFlag=1, 'Alerted','Not Alerted') AS ApoinmentFlag" => array('ApoinmentFlag','Is Show'), 
	 "IF(d.ApointmentRead=0, 'Unread','Read') AS ApointmentRead" => array('ApointmentRead','Read')
  */
// @ pack : filter status 
 
 $this->EUI_Page->_setFrom('t_gn_debitur a');
$this->EUI_Page->_setJoin('t_gn_assignment b ','a.deb_id=b.CustomerId','INNER');
// $this->EUI_Page->_setJoin('t_gn_campaign c ',' a.deb_cmpaign_id=c.CampaignId','LEFT');
 $this->EUI_Page->_setJoin('t_gn_appoinment d ',' a.deb_id=d.CustomerId','LEFT');
 $this->EUI_Page->_setJoin('t_lk_account_status e ',' a.deb_call_status_code=e.CallReasonCode','LEFT', TRUE);
  
/** set filter **/
 
 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL');
 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL');
 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL');
 //$this->EUI_Page->_setWherein('a.deb_call_status_code', self::CallBackLater());
 $this->EUI_Page->_setWherein('d.ApoinmentFlag', array(0,1));
 
/** set filter session By Session ID **/
			
 if($this ->EUI_Session ->_get_session('HandlingType')==USER_SUPERVISOR){	
	$this->EUI_Page->_setAnd('b.AssignSpv', $this->EUI_Session->_get_session('UserId'));
 }
 
 if($this ->EUI_Session ->_get_session('HandlingType')==USER_AGENT_OUTBOUND){
	$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
 }	 
	
 if($this ->EUI_Session ->_get_session('HandlingType')==USER_AGENT_INBOUND){
	$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
 }
 
/** set filter post cache ***/
	
 $this->EUI_Page->_setLikeCache('a.deb_name', 'app_cust_name', TRUE);
 $this->EUI_Page->_setAndCache('a.deb_acct_no', 'app_cif_number', TRUE);
 $this->EUI_Page->_setAndCache('b.AssignSelerId', 'app_dc', TRUE);
 $this->EUI_Page->_setAndCache('c.CampaignId', 'app_campaign', TRUE);
 $this->EUI_Page->_setAndCache('a.deb_call_status_code', 'app_call_reason', TRUE);
	
/** set filter by request **/
	
  if( $this->URI->_get_have_post('app_start_date')) {
	 $this->EUI_Page->_setAndOrCache("
		DATE(d.ApoinmentDate)>= '". date('Y-m-d',strtotime($this->URI->_get_post('app_start_date')))."'
		AND DATE(d.ApoinmentDate)<= '". date('Y-m-d',strtotime($this->URI->_get_post('app_end_date')))."' ", 'app_start_date', TRUE);
  }
  else{
	$this->EUI_Page->_setAnd("DATE(d.ApoinmentDate)=DATE(NOW())");
  }

/** @ pack : set order by **/
  
  if( $this->URI->_get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
  }	
	
  $this->EUI_Page->_setLimit();
	//echo $this ->EUI_Page->_getCompiler();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
}

?>