<?php

class M_ReportCpaSummary extends EUI_Model {

// view body data 

private static $view_body = null;


// instance 
	
private static $instance = null;

// param

private static $param = null;

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function index( $param = null) 
{
	if( !is_null($param) ) 
	{
		self::$param = $param;
		if( !is_null(self::$param) )
		{	
			switch(self::$param['group_by']) 
			{
				case 'group_spv' 	: $this -> ActivityGroupBySpv(); break;
				case 'group_agent'  : $this -> ActivityGroupByAgent(); break;
			}
		}	
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function ActivityGroupBySpv()
{
	if( !is_null(self::$param) )
	{
		switch(trim(self::$param['mode']))
		{
			// case 'hourly' 	: self::hourlyEfficiencyGroupBySpv(); 	break;
			// case 'daily'  	: self::dailyEfficiencyGroupBySpv(); 		break;
			case 'summary'  : $this -> summaryActivityGroupBySpv(); 	break;	
		}
	}
}



/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function ActivityGroupByAgent()
{
	if( !is_null(self::$param) )
	{
		switch(trim(self::$param['mode']))
		{
			// case 'hourly' 	: self::hourlyActivityGroupBySpv(); 	break;
			// case 'daily'  	: self::dailyActivityGroupBySpv(); 		break;
			case 'summary'  : self::summaryActivityGroupByAgent(); 	break;	
		}
	}
} 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function summaryActivityGroupByAgent()
{
 
 /* get login history **/
	
	$this->db->select("
		c.UserId, sum(if( a.`status` in(1), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as ready,
		sum(if( a.`status` in(2), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as notready,
		sum(if( a.`status` in(3), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as acw,
		sum(if( a.`status` in(4), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as busy",
	FALSE);
		
	$this->db->from('cc_agent_activity_log a'); 
	$this->db->join('cc_agent b','a.agent=b.id','LEFT');
	$this->db->join('t_tx_agent c','b.userid=c.id','LEFT');
	$this->db->where("a.start_time >=",self::$param['start_date'] . ' 00:00:00');
	$this->db->where("a.start_time <=",self::$param['end_date'] . ' 23:59:59');
	$this->db->where_in('c.UserId',self::$param['agent_id']);
	$this->db->group_by('c.UserId');

	foreach( $this ->db ->get()->result_assoc() as $rows )
	{
		self::$view_body[$rows['UserId']]['Ready']= $rows['ready'];
		self::$view_body[$rows['UserId']]['NotReady']= $rows['notready'];
		self::$view_body[$rows['UserId']]['ACW']= $rows['acw'];
		self::$view_body[$rows['UserId']]['Busy']= $rows['busy'];
	}
	
  /* sum talk call session **/
  
	$this->db->select(" 
		c.UserId, SUM(IF( a.status IN(3004,3005), (unix_timestamp(a.end_time)-unix_timestamp(a.agent_time)),0)) as sum_talk", 
		FALSE);
	
	$this->db->from("cc_call_session a");
	$this->db->join("cc_agent b", "a.agent_id=b.id",'LEFT');
	$this->db->join("t_tx_agent c", "b.userid=c.id",'LEFT');
	$this->db->where('a.agent_time IS NOT NULL','',FALSE);
	$this->db->where("date(a.agent_time)!=", "0000-00-00",FALSE);
	$this->db->where('a.start_time >=',self::$param['start_date'] . ' 00:00:00');
	$this->db->where('a.start_time <=',self::$param['end_date'] . ' 23:59:59');
	$this->db->where_in('c.UserId',self::$param['agent_id']);
	$this->db->group_by('c.UserId');
	
	foreach( $this ->db ->get()->result_assoc() as $rows )
	{
		self::$view_body[$rows['UserId']]['TalkTime']= $rows['sum_talk'];
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function summaryActivityGroupBySpv()
{

	/* get login history **/
	
	$this->db->select("
		c.spv_id, sum(if( a.`status` in(1), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as ready,
		sum(if( a.`status` in(2), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as notready,
		sum(if( a.`status` in(3), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as acw,
		sum(if( a.`status` in(4), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as busy",
	FALSE);
		
	$this->db->from('cc_agent_activity_log a'); 
	$this->db->join('cc_agent b','a.agent=b.id','LEFT');
	$this->db->join('t_tx_agent c','b.userid=c.id','LEFT');
	$this->db->where("a.start_time >=",self::$param['start_date'] . ' 00:00:00');
	$this->db->where("a.start_time <=",self::$param['end_date'] . ' 23:59:59');
	$this->db->where_in("c.spv_id",self::$param['spv_id']);
	$this->db->group_by("c.spv_id");

	foreach( $this ->db ->get()->result_assoc() as $rows )
	{
		self::$view_body[$rows['spv_id']]['Ready']= $rows['ready'];
		self::$view_body[$rows['spv_id']]['NotReady']= $rows['notready'];
		self::$view_body[$rows['spv_id']]['ACW']= $rows['acw'];
		self::$view_body[$rows['spv_id']]['Busy']= $rows['busy'];
	}
	
  /* sum talk call session **/
  
	$this->db->select(" 
		c.spv_id, SUM(IF( a.status IN(3004,3005), (unix_timestamp(a.end_time)-unix_timestamp(a.agent_time)),0)) as sum_talk", 
		FALSE);
	
	$this->db->from("cc_call_session a");
	$this->db->join("cc_agent b", "a.agent_id=b.id",'LEFT');
	$this->db->join("t_tx_agent c", "b.userid=c.id",'LEFT');
	$this->db->where('a.agent_time IS NOT NULL','',FALSE);
	$this->db->where("date(a.agent_time)!=", "0000-00-00",FALSE);
	$this->db->where('a.start_time >=',self::$param['start_date'] . ' 00:00:00');
	$this->db->where('a.start_time <=',self::$param['end_date'] . ' 23:59:59');
	$this->db->where_in("c.spv_id",self::$param['spv_id']);
	$this->db->group_by("c.spv_id");
	
	foreach( $this ->db ->get()->result_assoc() as $rows )
	{
		self::$view_body[$rows['spv_id']]['TalkTime']= $rows['sum_talk'];
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

public function _getBodyView()
{
	if( !is_null(self::$view_body) )
	{
		return self::$view_body;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getParam()
{
	if( !is_null(self::$param) )
	{
		return self::$param;
	}
}
 
 
 
// _get Reason Type 

function _getSummaryReasonType( $param = null )
{
	$this ->db ->select(" b.id AS AgentId,  d.reasonid as ReasonId, SUM(IF(a.status IN(2), (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) AS Times",  FALSE);
	$this ->db ->from("cc_agent_activity_log a");
	$this ->db ->join("cc_agent b"," a.agent=b.id ", "LEFT");
	$this ->db ->join("t_tx_agent c "," b.userid=c.id","LEFT");
	$this ->db ->join("cc_reasons d "," a.reason=d.reasonid","LEFT");
	
	$this->db->where("d.reasonid IS NOT NULL ",'', FALSE);
	
	// is param 
	
	if( is_array($param) 
		AND isset($param['GroupCallCenter']) ) {
		$this->db->where("b.agent_group",$param['GroupCallCenter'], FALSE);
	}
	
	// filter next agent_id 
	
	if( is_array($param) 
		AND isset($param['AgentId']) AND !empty($param['AgentId']) ) {
		$this->db->where_in('b.id',$param['AgentId']);
	}
	
	// filter next start date 
	
	if( is_array($param) 
		AND isset($param['start_date']) AND isset($param['end_date']) ) {
		$this->db->where("a.start_time >=", _getDateEnglish($param['start_date']) . ' 00:00:00');
		$this->db->where("a.start_time <=", _getDateEnglish($param['end_date']) . ' 23:59:59');
	}
	
	
	$this -> db -> group_by("a.reason, a.agent");
	
	//echo $this -> db ->_get_var_dump();
	
	foreach( $this -> db ->get()->result_assoc() as $rows ) {
		$results[$rows['AgentId']][$rows['ReasonId']] += $rows['Times'];
	}

	return $results;
} 
 
 // getBySummary
 
 function _getSummary( $param = null )
 {
	$results = array();
	$this->db->select("
		b.id as AgentId, sum(if( a.`status` in(1), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as ready,
		sum(if( a.`status` in(2), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as notready,
		sum(if( a.`status` in(3), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as acw,
		sum(if( a.`status` in(4), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as busy",
	FALSE);
		
	$this->db->from('cc_agent_activity_log a'); 
	$this->db->join('cc_agent b','a.agent=b.id','LEFT');
	$this->db->join('t_tx_agent c','b.userid=c.id','LEFT');
	
	// is param 
	
	if( is_array($param) 
		AND isset($param['GroupCallCenter']) ) {
		$this->db->where("b.agent_group",$param['GroupCallCenter'], FALSE);
	}
	
	// filter next agent_id 
	
	if( is_array($param) 
		AND isset($param['AgentId']) AND !empty($param['AgentId']) ) {
		$this->db->where_in('b.id',$param['AgentId']);
	}
	
	// filter next start date 
	
	if( is_array($param) 
		AND isset($param['start_date']) AND isset($param['end_date']) ) {
		$this->db->where("a.start_time >=", _getDateEnglish($param['start_date']) . ' 00:00:00');
		$this->db->where("a.start_time <=", _getDateEnglish($param['end_date']) . ' 23:59:59');
	}
	
	$this -> db ->group_by("AgentId");
	
	// echo $this -> db ->_get_var_dump();
	
	foreach( $this -> db ->get()->result_assoc() as $rows ) {
		$results[$rows['AgentId']]['ready'] += $rows['ready'];
		$results[$rows['AgentId']]['notready'] += $rows['notready'];
		$results[$rows['AgentId']]['acw'] += $rows['acw'];
		$results[$rows['AgentId']]['busy'] += $rows['busy'];
		$results[$rows['AgentId']]['TalkTime']+= 0;
	}
	
	
/* sum talk call session **/
  
	$this->db->select("a.agent_id, SUM(IF( a.status IN(3004,3005), (unix_timestamp(a.end_time)-unix_timestamp(a.agent_time)),0)) as sum_talk", FALSE);
	$this->db->from("cc_call_session a");
	$this->db->join("cc_agent b", "a.agent_id=b.id",'LEFT');
	$this->db->join("t_tx_agent c", "b.userid=c.id",'LEFT');
	$this->db->where('a.agent_time IS NOT NULL','',FALSE);
	$this->db->where("date(a.agent_time)!=", "0000-00-00",FALSE);
	
// param date 	

	if( is_array($param) 
		AND isset($param['start_date']) AND isset($param['end_date']) ) 
		{
		$this->db->where("a.start_time >=", _getDateEnglish($param['start_date']) . ' 00:00:00');
		$this->db->where("a.start_time <=", _getDateEnglish($param['end_date']) . ' 23:59:59');
	}
	
// AgentId date	

	if( is_array($param) 
		AND isset($param['AgentId']) AND !empty($param['AgentId']) ) 
	{
		$this->db->where_in('a.agent_id',$param['AgentId']);
	}
	
// agent && run query 
	
	$this->db->group_by("a.agent_id");
	foreach( $this ->db ->get()->result_assoc() as $rows ) {
		$results[$rows['agent_id']]['TalkTime']+= $rows['sum_talk'];
	}

	return $results;
 } 
 
 

// _get Reason Type 

function _getDailyReasonType( $param = null )
{
	$this ->db ->select(" 
		b.id AS AgentId,  d.reasonid as ReasonId, date(a.start_time) as tgl, 
		SUM(IF(a.status IN(2), (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) AS Times",  
	FALSE);
	
	$this ->db ->from("cc_agent_activity_log a");
	$this ->db ->join("cc_agent b"," a.agent=b.id ", "LEFT");
	$this ->db ->join("t_tx_agent c "," b.userid=c.id","LEFT");
	$this ->db ->join("cc_reasons d "," a.reason=d.reasonid","LEFT");
	
	$this->db->where("d.reasonid IS NOT NULL ",'', FALSE);
	
	// is param 
	
	if( is_array($param) 
		AND isset($param['GroupCallCenter']) ) {
		$this->db->where("b.agent_group",$param['GroupCallCenter'], FALSE);
	}
	
	// filter next agent_id 
	
	if( is_array($param) 
		AND isset($param['AgentId']) AND !empty($param['AgentId']) ) {
		$this->db->where_in('b.id',$param['AgentId']);
	}
	
	// filter next start date 
	
	if( is_array($param) 
		AND isset($param['start_date']) AND isset($param['end_date']) ) {
		$this->db->where("a.start_time >=", _getDateEnglish($param['start_date']) . ' 00:00:00');
		$this->db->where("a.start_time <=", _getDateEnglish($param['end_date']) . ' 23:59:59');
	}
	
	
	$this -> db -> group_by(" a.reason, a.agent, tgl");
	
	//__($this -> db ->_get_var_dump());
	
	foreach( $this -> db ->get()->result_assoc() as $rows ) {
		$results[$rows['AgentId']][$rows['tgl']][$rows['ReasonId']] += $rows['Times'];
	}

	return $results;
} 
  
  
// getByDaily

function _getDaily($param=null)
{
	$results = array();
	$this->db->select("
		b.id as AgentId, date(a.start_time) as tgl, 
		sum(if( a.`status` in(1), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as ready,
		sum(if( a.`status` in(2), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as notready,
		sum(if( a.`status` in(3), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as acw,
		sum(if( a.`status` in(4), (unix_timestamp(a.end_time)-unix_timestamp(a.start_time)),0)) as busy",
	FALSE);
		
	$this->db->from('cc_agent_activity_log a'); 
	$this->db->join('cc_agent b','a.agent=b.id','LEFT');
	$this->db->join('t_tx_agent c','b.userid=c.id','LEFT');
	
	// is param 
	
	if( is_array($param) 
		AND isset($param['GroupCallCenter']) ) {
		$this->db->where("b.agent_group",$param['GroupCallCenter'], FALSE);
	}
	
	// filter next agent_id 
	
	if( is_array($param) 
		AND isset($param['AgentId']) AND !empty($param['AgentId']) ) {
		$this->db->where_in('b.id',$param['AgentId']);
	}
	
	// filter next start date 
	
	if( is_array($param) 
		AND isset($param['start_date']) AND isset($param['end_date']) ) {
		$this->db->where("a.start_time >=", _getDateEnglish($param['start_date']) . ' 00:00:00');
		$this->db->where("a.start_time <=", _getDateEnglish($param['end_date']) . ' 23:59:59');
	}
	
	$this -> db ->group_by("AgentId,tgl");
	
	//__($this -> db ->_get_var_dump());
	
	foreach( $this -> db ->get()->result_assoc() as $rows ) {
		$results[$rows['AgentId']][$rows['tgl']]['ready'] += $rows['ready'];
		$results[$rows['AgentId']][$rows['tgl']]['notready'] += $rows['notready'];
		$results[$rows['AgentId']][$rows['tgl']]['acw'] += $rows['acw'];
		$results[$rows['AgentId']][$rows['tgl']]['busy'] += $rows['busy'];
		$results[$rows['AgentId']][$rows['tgl']]['TalkTime']+= 0;
	}
	
	
/* sum talk call session **/
  
	$this->db->select("a.agent_id, date(a.start_time) as tgl, SUM(IF( a.status IN(3004,3005), (unix_timestamp(a.end_time)-unix_timestamp(a.agent_time)),0)) as sum_talk", FALSE);
	$this->db->from("cc_call_session a");
	$this->db->join("cc_agent b", "a.agent_id=b.id",'LEFT');
	$this->db->join("t_tx_agent c", "b.userid=c.id",'LEFT');
	$this->db->where('a.agent_time IS NOT NULL','',FALSE);
	$this->db->where("date(a.agent_time)!=", "0000-00-00",FALSE);
	
// param date 	

	if( is_array($param) 
		AND isset($param['start_date']) AND isset($param['end_date']) ) 
		{
		$this->db->where("a.start_time >=", _getDateEnglish($param['start_date']) . ' 00:00:00');
		$this->db->where("a.start_time <=", _getDateEnglish($param['end_date']) . ' 23:59:59');
	}
	
// AgentId date	

	if( is_array($param) 
		AND isset($param['AgentId']) AND !empty($param['AgentId']) ) 
	{
		$this->db->where_in('a.agent_id',$param['AgentId']);
	}
	

// agent && run query 
	
	$this->db->group_by("a.agent_id, tgl");
	foreach( $this ->db ->get()->result_assoc() as $rows ) {
		$results[$rows['agent_id']][$rows['tgl']]['TalkTime']+=$rows['sum_talk'];
	}

	return $results;
	
 } 
 
 
 // getBySummary

function _getHourly()
{
	$results = array();
	$sql = " select count(a.id) as tots, 
			b.name as Username, b.userid,
			SUM(IF(a.`status` in(3005,3004),1,0)) as tot_connected,
			SUM(IF(a.`status` NOT IN(3005,3004),1,0)) as tot_abandone,
			SUM(IF(a.`status` IN(3005,3004),unix_timestamp(a.end_time)-unix_timestamp(a.start_time),0)) as tot_talk
			from cc_call_session a left join cc_agent b on a.agent_id=b.id
			where b.userid is not null
			group by Username ";
			
	$qry = $this -> db -> query($sql);
	
	if( !$qry -> EOF() ){
		$results = $qry -> result_assoc();
	}
	return $results;
 } 
  
public function get_cpa_list_lunas($filter)
{
	$list_cpa_lunas =array();
	 $this->db->reset_select();
	$this->db->select("
		a.region,
		a.create_date as proposal_date,
		a.reff_no,
		c.CampaignDesc as product,
		a.arrangement,
		CONCAT(\"'\",b.deb_acct_no) AS deb_acct_no,
		b.deb_name as fullname,
		b.deb_open_date as open_date,
		'8' as cycle_DLQ,
		a.card_status,
		e.full_name as negotiatior,
		a.placement,
		'AKS' as agency_name,
		a.outstanding_byspv,
		a.totalpayment_byspv,
		a.downpayment_byspv,
		a.futurepay_byspv,
		a.payment_periode,
		b.deb_principal,
		'0' as charge_fee,
		a.discountamo_byspv,
		a.from_balance,
		a.from_princ,
		f.OccIndonesian as occopution,
		g.reason_name as reason,
		'>=3' as DLQ_Debt,
		d.PayerName as pay_handle,
		'-' as mapping_account,
		a.justification,a.justification_byspv,
		a.ExceptionalLevel,
		'-' as authority,
		'-' as approver_name,
		'-' as balance_tobe_writen,
		'-' as barcode,
		a.create_date,
		a.approval_date",FALSE);
	$this->db->from("t_gn_discount_trx a");
	$this->db->join("t_gn_debitur b", "a.deb_id=b.deb_id",'INNER');
	$this->db->join("t_gn_campaign c", "b.deb_cmpaign_id=c.CampaignId",'LEFT');
	$this->db->join("t_lk_payers d", "a.pay_handled_by=d.PayerCode",'LEFT');
	$this->db->join("t_tx_agent e", "a.agent_id=e.UserId",'LEFT');
	$this->db->join("t_lk_occupation_code f", "a.occupation=f.OccCode",'LEFT');
	$this->db->join("t_lk_cpa_reason g", "a.reason=g.reason_code",'LEFT');

	$this->db->where('a.approval_date >=',$filter->get_value('start_date','_getDateEnglish')." 00:00:00");
	$this->db->where('a.approval_date <=',$filter->get_value('end_date','_getDateEnglish')." 23:59:59");
	$this->db->where('a.cpa_status', 'LUNAS');
	// echo $this->db->print_out();
	$qry = $this->db->get();
	
	if( $qry->num_rows() >0 )
	{ 
		$list_cpa_lunas = $qry->result_assoc();
	}
	return $list_cpa_lunas;
}

public function get_acc_dc($filter)
{
	$acc_dc = array();
	$this->db->reset_select();
	$this->db->select("
		concat(a.id,'-',a.full_name) as agent,
		de.deb_acct_no as Account_No,
		ac.CallReasonDesc,
		de.deb_name as customer_name,
		DATE_FORMAT(de.deb_call_activity_datets,'%d-%m-%Y') as tanggal",FALSE);
	$this->db->from("t_gn_debitur de");
	$this->db->join("t_tx_agent a"," a.UserId=de.deb_update_by_user",'INNER');
	$this->db->join("t_lk_account_status ac","ac.CallReasonCode=de.deb_call_status_code",'INNER');
	
	$this->db->where('de.deb_call_activity_datets >=',$filter->get_value('start_date','_getDateEnglish')." 00:00:00");
	$this->db->where('de.deb_call_activity_datets <=',$filter->get_value('end_date','_getDateEnglish')." 23:00:00");
	$this->db->where_in('a.UserId',self::$param['agent_id']);
	$this->db->where_in("c.spv_id",self::$param['spv_id']);
	$this->db->order_by("agent","asc");
	// echo $this->db->print_out();
	$qry = $this->db->get();


	if($qry->num_rows() >0)
	{
		$acc_dc = $qry->result_assoc();
	}
	return $acc_dc;
}
 
public function get_list_lunas_full($filter)
{
	$list_lunas_full =array();
	$this->db->reset_select();
	$this->db->select("
		a.deb_id,
		SUM(b.pay_amount) as jumlah_bayar,
		a.deb_amount_wo,
		if(SUM(b.pay_amount) >= a.deb_amount_wo,'LUNAS','BELUM') as pay_status,
		'-' as region,
		'-' as proposal_date,
		'-' as reff_no,
		c.CampaignDesc as product,
		'-' as arrangement,
		CONCAT(\"'\",a.deb_acct_no) AS deb_acct_no,
		a.deb_name as fullname,
		a.deb_open_date as open_date,
		'-' as cycle_DLQ,
		'-' as card_status,
		'-' as negotiatior,
		'-' as placement,
		'AKS' as agency_name,
		'0' as outstanding_byspv,
		'0' as totalpayment_byspv,
		'0' as downpayment_byspv,
		'0' as futurepay_byspv,
		'0' as payment_periode,
		a.deb_principal,
		'0' as charge_fee,
		'0' as discountamo_byspv,
		'0%' as from_balance,
		'0%' as from_princ,
		'-' as occopution,
		'-' as reason,
		'-' as DLQ_Debt,
		'-' as pay_handle,
		'-' as mapping_account,
		'-' as justification_byspv,
		'-' as ExceptionalLevel,
		'-' as authority,
		'-' as approver_name,
		'-' as balance_tobe_writen,
		'-' as barcode,
		'-' as create_date,
		'-' as approval_date",FALSE);
		$this->db->from("t_gn_debitur a");
		$this->db->join("t_gn_payment b", "a.deb_id=b.deb_id",'LEFT');
		$this->db->join("t_gn_campaign c", "a.deb_cmpaign_id=c.CampaignId",'INNER');
		$this->db->group_by("a.deb_id"); 
		$this->db->having('pay_status', 'LUNAS');
		$qry = $this->db->get();
	
		if( $qry->num_rows() >0 )
		{ 
			$list_lunas_full = $qry->result_assoc();
		}
		return $list_lunas_full;
	
}
 

}


?>
