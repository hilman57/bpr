<?php
/*
 * @ pack : model CallWIthArea
 */
 
class M_CallWIthArea extends EUI_Model
{
 
/*
 * @ pack : property settup ==========> 
 */ 
 
 private static $Instance = null;
 
/*
 * @ pack : property settup ==========> 
 */ 

 protected $_limit_time_wita = null;
 
/*
 * @ pack : property settup ==========> 
 */ 
 
 protected $_limit_time_wit = null; 
 
/*
 * @ pack : property settup ==========> 
 */ 

 protected $_call_phone = null;
 
/*
 * @ pack : property settup ==========> 
 */ 
 
 protected $_call_time = null;
 
/*
 * @ pack : public function Instance 
 */ 
 
 public function  M_CallWIthArea()
{	
 if( is_null($this->_time_wita) ){
	$this->_limit_time_wita = '20:00:00';
 }
 
 if( is_null($this->_time_wit) ){
	$this->_limit_time_wit = '20:00:00';
 }
 
 $this->_call_time = strtotime(time());
 
}

/*
 * @ pack : public function Instance 
 */ 
 
 public static function &Instance()
{
 if( is_null(self::$Instance) )
{
 self::$Instance = new self();
}
 return self::$Instance;
	
}

/*
 * @ pack : public function Instance 
 */
 
 public function _select_operation_time()
{
 
 $res_json = array('call' => 0 );
 
 $this->_call_phone = _get_post('PhoneNumber');
 if( !is_null($this->_call_phone) 
	AND strlen($this->_call_phone)>0 )
 {
	$conds = $this->_select_wita_time();	
	if( $conds ){
		$res_json = array('call' => 1);
		return $res_json;
	}
	
	$conds = $this->_select_wit_time();	
	if( $conds ){
		$res_json = array('call' => 1);
		return $res_json;
	}
 }
 
 return $res_json;
	
}

/*
 * @ pack : public function Instance 
 */
 
 public function _select_wita_time()
{

 $call_phone = 0;
 $this->db->reset_select();
 $this->db->select("COUNT(a.area_id) jumlah ",FALSE);
 $this->db->from("t_lk_area_wita a ");
 $this->db->where("a.area_code REGEXP('^{$this->_call_phone}')", "", FALSE);
 $qry = $this->db->get();
 
 if( $qry->num_rows()) {
	$call_phone = (INT)$qry->result_singgle_value();
 }
 
// pack : call time its =======================================> 

 if( $call_phone AND (($this->_call_time)>($this->_limit_time_wita)) )
{
	return TRUE;
 } 
 else {
	return FALSE;
 }
  
} 

// _select_wita_time ===================> 


/*
 * @ pack : public function Instance 
 */
 
 public function _select_wit_time()
{

 $call_phone = 0;
 $this->db->reset_select();
 $this->db->select("COUNT(a.area_id) jumlah ",FALSE);
 $this->db->from("t_lk_area_wit a ");
 $this->db->where("a.area_code REGEXP('^{$this->_call_phone}')", "", FALSE);
 $qry = $this->db->get();
 
 if( $qry->num_rows()) {
	$call_phone = (INT)$qry->result_singgle_value();
 }
 
// pack : call time its =======================================> 
 
 if( $call_phone AND (($this->_call_time)>($this->_limit_time_wit))) {
	return TRUE;
 } 
 else {
	return FALSE;
 }
 
 // -----> SELECT COUNT(*) 
 // -----> JUMLAH FROM t_lk_area_wit a 
 // -----> WHERE a.area_code REGEXP('^09')
 
} 

// _select_wit_time ===================> 


// END CLASS 
}
?>