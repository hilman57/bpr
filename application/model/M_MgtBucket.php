<?php
/*
 * E.U.I 
 *
 
 * subject	: get M_MgtBucket modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 

class M_MgtBucket extends EUI_Model
{

/*
 * EUI :: _get_default() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
function _get_default()
{
	$this -> EUI_Page->_setPage(20); 
	$this->EUI_Page->_setSelect("a.BucketId");
	$this->EUI_Page->_setFrom("t_gn_bucket_customers a");
	if( $this -> EUI_Page -> _get_query()) {
		return $this -> EUI_Page;
	}
}

/*
 * EUI :: _get_content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */		
 
public function _get_content() {

 // @ pack : select array to view here 	----------------------------------------
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page') );
	$this->EUI_Page->_setPage(20);
	
 // @ pack : select array to view here 	----------------------------------------
 
	$this->EUI_Page->_setArraySelect(array(
		"a.BucketId AS CustomerId"=> array('CustomerId','ID','primary'),
		"b.CampaignDesc as CampaignName" => array("CampaignName","Product"),
		"a.deb_acct_no AS AccountNumber "=> array('AccountNumber','Costumer ID'),
		"a.deb_name AS CustomerName"=> array('CustomerName','Customer Name'),
		"a.deb_region AS Region"=> array('Region','Region'),
		"a.bucket_created_ts AS UploadDate "=> array('UploadDate','Upload Date'),
		"a.deb_resource AS Recsource "=> array('Recsource','Recsource'),
		"a.deb_interest AS Interest "=> array('Interest','Interest'),
		"a.deb_principal AS Principal" => array('Principal','Principal'),
		"a.deb_amount_wo AS AmountWO "=> array('AmountWO','Amount WO'),
		"a.deb_reminder AS History "=> array('History','History')
	));
	
 // @ pack :  set from table ----------------------------------------
 
  $this->EUI_Page->_setFrom("t_gn_bucket_customers a");
  $this->EUI_Page->_setJoin("t_gn_campaign b","a.deb_cmpaign_id=b.CampaignId","LEFT", TRUE);
  
	// $this->EUI_Page->_setQuery("select * from t_gn_bucket_customers a");
	// $flt = " AND a.CustomerDeleted=0 ";
	
		// if( $this -> URI -> _get_have_post('assign_data') ) 
			// $flt .= " AND a.AssignCampaign='".$this -> URI->_get_post('assign_data')."'";
		
		// if( $this -> URI -> _get_have_post('work_branch') ) 
			// $flt .= " AND a.CustomerZipCode IN ('".IMPLODE("','",EXPLODE(',',$this -> URI->_get_post('work_branch')))."')";
		
		// if( $this -> URI -> _get_have_post('city') )
			// $flt .= " AND a.CustomerCity LIKE '%". $this -> URI->_get_post('city') ."%'";
		
		// if( $this -> URI -> _get_have_post('card_type') )	 
			// $flt .= " AND a.CustomerCardType LIKE '%".$this -> URI->_get_post('card_type')."%'";
		
		// if( $this -> URI -> _get_have_post('file_upload'))  
			// $flt .= " AND a.BukcetSourceId ='".$this -> URI->_get_post('file_upload')."'";
		
		// if( $this -> URI -> _get_have_post('start_date') ){  
			// $flt .= " AND date(a.CustomerUploadedTs)>='". $this ->EUI_Tools ->_date_english($this -> URI->_get_post('start_date'))."' 
					  // AND date(a.CustomerUploadedTs)<='". $this ->EUI_Tools ->_date_english($this -> URI->_get_post('end_date'))."'";
		// }
		
	// $this -> EUI_Page ->  _setWhere( (!is_null($flt)?$flt:'') );
	
// set order 
	
	if( $this -> URI -> _get_have_post('order_by') ) 
		$this -> EUI_Page -> _setOrderBy( $this -> URI -> _get_post('order_by'),$this -> URI -> _get_post('type'));
		
		$this -> EUI_Page ->  _setLimit();
}	

/*
 * EUI :: _get_resource_query() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='')
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _get_page_number()
  {
	if( $this -> EUI_Page -> _get_query()!='' )
	{
		return $this -> EUI_Page -> _getNo();
	}	
  }
  
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_template($tmptype = '') {
	$_conds= array();
	$this -> load -> model(array('M_SetUpload'));
	if(class_exists('M_SetUpload') )
	{
		$_conds = $this -> M_SetUpload -> _get_ready_template($tmptype);
	}
	
	return $_conds;
 }
 
 
}

?>