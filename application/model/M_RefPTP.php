<?php

/*
 * @ pack : under model M_InfoPTP
 */ 
 
class M_RefPTP extends EUI_Model 
{

 var $_table_spce = null;
 var $_select_all = array();
	

/*
 * @ pack : aksesor 
 */ 
 
public function M_RefPTP()
{
	$this->_table_spce = array('t_lk_info_ptp');
} 


/*
 * @ pack   : aksesor 
 * @ return : array
 */ 
 
public function _get_InfoPTP()
{
 
 $this->db->reset_select();
 $this->db->select('*');
 $this->db->from('t_lk_info_ptp');
 $this->db->where_in('info_ptp_flags',array(0,1));
	
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 ) foreach( $qry->result_assoc() as $rows ) {
	$this->_select_all[$rows['info_ptp_code']] = $rows['info_ptp_name'];
 } 
	
 return $this->_select_all;
 
} 

/*
 * @ pack   : _getInfoPTPByCode 
 * @ return : String
 * @ param	: $code is int/string 
 */ 
 
public function _get_PTPName( $code = null )
{

 $_result_ptp = 0;
 $_select_all =& $this->_getInfoPTP();
 
 if( is_array($_select_all) )
 {
	if( isset($_select_all[$code])) 
	{
		$_result_ptp = $_select_all[$code];
	}
 }
 
 return $_result_ptp;
 
} 

/*
 * @ pack   : _getInfoPTPByCode 
 * @ return : String
 * @ param	: $code is int/string 
 */ 
 
public function _get_PTPCode( $name = NULL )
{

 $code_select = null;
 
 $this->db->reset_select();
 $this->db->select('*');
 $this->db->from('t_lk_info_ptp');
 $this->db->like('info_ptp_name', $name);
 $this->db->where_in('info_ptp_flags',array(0,1));
 
 // @ pack : query result ------------------------
 
 $qry = $this->db->get();
 if( $qry->num_rows()>0 )
 {
	if( $rows = $qry->result_first_assoc() )
	{
		$code_select = $rows['info_ptp_code'];
	}
 }
 
 return $code_select;
 
} 
  

/*
 * @ pack   : _getInfoPTPByCode 
 * @ return : String
 * @ param	: $code is int/string 
 */ 
 
 public function _set_addPTP( $data = array() )
{
	$conds = FALSE;
	
	$_insert_select = array('info_ptp_code', 'info_ptp_name', 'info_ptp_description'); 
	foreach( $data as $field => $value ) {
		if( in_array($field, $_insert_select) ) {
			$this->db->set($field, $value);
		}
	}
	
	$this->db->insert('t_lk_info_ptp');
	if( $this->db->affected_rows() > 0 ){
		$conds = TRUE;
	}
	
	return $conds;
 }
 

/*
 * @ pack   : _getInfoPTPByCode 
 * @ return : String
 * @ param	: $code is int/string 
 */ 
 
 public function _set_deletePTP( $where = array() )
{
 
 $conds = FALSE;
 
 $_insert_select = array('info_ptp_code', 'info_ptp_name', 'info_ptp_description'); 
 foreach( $where AS $field => $value ) 
 {
	if( in_array($field, $_insert_select) ) {
		$this->db->where($field, $value);
	}
 }
	
 $this->db->delete('t_lk_info_ptp');
 if( $this->db->affected_rows() > 0 )
 {
	$conds = TRUE;
 }
  
  return (bool)$conds;
 
 } 
 
 
}

// END CLASS 


?>