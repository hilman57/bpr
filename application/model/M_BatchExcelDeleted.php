<?php
ini_set("memory_limit", "2048M");
// -------------------------------------------------------------------
/*
 * @ pack   : Batch for dumper data all db width mode daily **
 * @ notes  : will run by scheduler 
 * @ aksess : 0777 
 */
// ---------------------------------------------------------------------

class M_BatchExcelDeleted extends EUI_Model
{

var $set_event_handler = array();
var $set_rows_headers  = array(
	'debiturid'				=> array('field'=>'deb_id'),
	'cardno'				=> array('field'=>'deb_cardno'),
	'custno'				=> array('field'=>'deb_acct_no'),
	'accountno'				=> array('field'=>'deb_acct_no'),
	'idno'					=> array('field'=>'deb_id_number'),
	'nbrcards'				=> array('field'=>'deb_no_card'),
	'fullname'				=> array('field'=>'deb_name'),
	'mothername'			=> array('field'=>'deb_mother_name'),
	'dateofbirth'			=> array('field'=>'deb_dob'),
	'homeaddress'			=> array('field'=>'deb_add_hm'),
	'billingaddress'		=> array('field'=>'deb_billing_add'),
	'officeaddress'			=> array('field'=>'deb_add_off'),
	'homezip'				=> array('field'=>'deb_zip_h'),
	'homephone'				=> array('field'=>'deb_home_no1'),
	'homephone2'			=> array('field'=>NULL),
	'mobilephone'			=> array('field'=>'deb_mobileno1'),
	'mobilephone2'			=> array('field'=>NULL),
	'otheraddress'			=> array('field'=>NULL),
	'otheraddress2'			=> array('field'=>NULL),
	'officephone'			=> array('field'=>'deb_office_ph1'),
	'officefax'				=> array('field'=>NULL),
	'ecname'				=> array('field'=>'deb_ec_name_f'),
	'ecaddress'				=> array('field'=>'deb_ec_add_f'),
	'ecphone'				=> array('field'=>'deb_ec_phone_f'),
	'ecmobile'				=> array('field'=>'deb_ec_mobile_f'),
	'ecrelationship'		=> array('field'=>NULL),
	'branch'				=> array('field'=>NULL),
	'area'					=> array('field'=>'deb_region'),
	'opendate'				=> array('field'=>'deb_open_date'),
	'clss_entry'			=> array('field'=>'deb_resource'),
	'creditlimit'			=> array('field'=>'deb_limit'),
	'currentbalance'		=> array('field'=>'deb_wo_amount'),
	'feeandcharge'			=> array('field'=>'deb_fees'),
	'interest'				=> array('field'=>'deb_interest'),
	'principalbalance'		=> array('field'=>'deb_principal'),
	'lastpaymentdate'		=> array('field'=>'deb_last_paydate'),
	'lastpaymentamount'		=> array('field'=>'deb_last_pay'),
	'currentpaymentdue'		=> array('field'=>NULL),
	'bpcount'				=> array('field'=>'deb_bp'),
	'ptpcount'				=> array('field'=>'ptpcounts'),
	'wo_date'				=> array('field'=>'deb_b_d'),
	'wo_amount'				=> array('field'=>'deb_wo_amount'),
	'wo_open'				=> array('field'=>NULL),
	'wo_lpd'				=> array('field'=>NULL),
	'curr_coll_id'			=> array('field'=>'deb_update_by_user'),
	'dlq'					=> array('field'=>'dlq'),
	'perm_message'			=> array('field'=>'deb_perm_msg'),
	'perm_date'				=> array('field'=>'deb_perm_msg_date'),
	'accstatus'				=> array('field'=>'CallReasonDesc'),
	'accstatusdate'			=> array('field'=>'deb_call_activity_datets'),
	'prevaccstatus'			=> array('field'=>'prevac'),
	'prevaccstatusdate'		=> array('field'=>NULL),
	'lastresponse'			=> array('field'=>NULL),
	'contacthistoryid'		=> array('field'=>NULL),
	'agent'					=> array('field'=>'deb_agent'),
	'note'					=> array('field'=>'Notes'),
	'reminder'				=> array('field'=>'deb_reminder'),
	'uploadbatch'			=> array('field'=>'uploadbatch'),
	'ptp_discount'			=> array('field'=>'ptp_discount'),
	'ptp_amount'			=> array('field'=>'ptpAmount'),
	'ptp_date'				=> array('field'=>'ptpDate'),
	'ptp_channel'			=> array('field'=>'ptpChannel'),
	'ssv_no'				=> array('field'=>'deb_ssv_no'),
	'callstatus'			=> array('field'=>'prevac'),
	'calldate'				=> array('field'=>'deb_call_activity_datets'),
	'prevcallstatus'		=> array('field'=>NULL),
	'infoptp'				=> array('field'=>'infoptp'),
	'prevcalldate'			=> array('field'=>NULL),
	'tenor'					=> array('field'=>'ptpTenor'),
	'addhomephone'			=> array('field'=>'addhomephone'),
	'addhomephone2'			=> array('field'=>'addhomephone2'),
	'addmobilephone'		=> array('field'=>'addmobilephone'),
	'addmobilephone2'		=> array('field'=>'addmobilephone2'),
	'addofficephone'		=> array('field'=>'addofficephone'),
	'addofficephone2'		=> array('field'=>'addofficephone2'),
	'addfax'				=> array('field'=>'addfax'),
	'addfax2'				=> array('field'=>'addfax2'),
	'swap_count'			=> array('field'=>'deb_swap_count'),
	'sms_count'				=> array('field'=>'deb_sms_count'),
	'other_agent'			=> array('field'=>'other_agent'),
	'other_card_number' 	=> array('field'=>'other_card_number'),
	'other_accstatus'		=> array('field'=>'other_accstatus'),
	'contacttype'			=> array('field'=>'deb_contact_type'),
	'rpc'					=> array('field'=>'deb_rpc'),
	'spc_with'				=> array('field'=>'deb_spc'),
	'namahomephone1'		=> array('field'=>NULL),
	'relativehomephone1'	=> array('field'=>NULL),
	'namahomephone2'		=> array('field'=>NULL),
	'relativehomephone2'	=> array('field'=>NULL),
	'namaofficephone1'		=> array('field'=>NULL),
	'relativeofficephone1'	=> array('field'=>NULL),
	'namaofficephone2'		=> array('field'=>NULL),
	'relativeofficephone2'	=> array('field'=>NULL),
	'namatlpphone1'			=> array('field'=>NULL),
	'relativetlpphone1'		=> array('field'=>NULL),
	'namatlpphone2'			=> array('field'=>NULL),
	'relativetlpphone2'		=> array('field'=>NULL),
	'namamobilephone1'		=> array('field'=>NULL),
	'relativemobilephone1'	=> array('field'=>NULL),
	'namamobilephone2'		=> array('field'=>NULL),
	'relativemobilephone2'	=> array('field'=>NULL),
	'keep_data'				=> array('field'=>'deb_is_kept'),
	'afterpay'				=> array('field'=>'deb_afterpay'),
	'attempt_call'			=> array('field'=>'attempt_call'),
	'isblock'				=> array('field'=>'deb_is_lock'),
	'bal_afterpay'			=> array('field'=>'deb_bal_afterpay'),
	'pri_afterpay'			=> array('field'=>'deb_pri_afterpay'),
	'tenor_value'			=> array('field'=>NULL),
	'tenor_amnt'			=> array('field'=>NULL),
	'tenor_dates'			=> array('field'=>NULL),
	'flag'					=> array('field'=>'flag'),
	'current_lock'			=> array('field'=>NULL),
	'lock_modul'			=> array('field'=>NULL),
	'cpa_count'				=> array('field'=>NULL),
	'lunas_flag'			=> array('field'=>NULL),
	'last_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'max_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'acc_mapping'			=> array('field'=>'acc_mapping'),
	'class_mapping'			=> array('field'=>NULL),
	'accstatus_name'		=> array('field'=>'accstatus_name'),
	'SPC_DESC'				=> array('field'=>'SPC_DESC'),
	'RPC_DESC'				=> array('field'=>'RPC_DESC'),
	'other_ch_office'		=> array('field'=>'other_ch_office'),
	'other_ch_home'			=> array('field'=>'other_ch_home'),
	'other_ch_mobile1'		=> array('field'=>'other_ch_mobile1'),
	'other_ch_mobile2'	 	=> array('field'=>'other_ch_mobile2'),
	'family'				=> array('field'=>'family'),
	'neighbour'				=> array('field'=>'neighbour'),
	'rel_person'			=> array('field'=>'rel_person'),
	'other_ec'				=> array('field'=>'other_ec')
);

/*
 * @ pack : object call instance 
 */
 
 private static $Instance = null;

// -----------------------------------------------------
/*
 * @ pack : instance of class 
 */
 // -----------------------------------------------------

 public function __construct()
{
	$this->load->model(array('M_Configuration'));
	$this->Inialize();
}

// -----------------------------------------------------
/*
 * @ pack : instance of class 
 */
 // -----------------------------------------------------

 public static function &Instance()
{
 if(is_null(self::$Instance) ){
	self::$Instance = new self();
 }
 return self::$Instance;
 
}  
// Instance ======================> 


// -----------------------------------------------------
/*
 * @ pack   : call of query result in array 
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 private function _getDumperExcelData()
{
  $source_data = null;
  $this->db->reset_select();
  $this->db->select("a.deb_id,
	a.deb_cardno,
	a.deb_acct_no,
	a.deb_acct_no,
	a.deb_id_number,
	a.deb_no_card,
	a.deb_name,
	a.deb_mother_name,
	a.deb_dob,
	a.deb_add_hm,
	a.deb_billing_add,
	a.deb_add_off,
	a.deb_zip_h,
	a.deb_home_no1,
	a.deb_mobileno1,
	a.deb_office_ph1,
	a.deb_ec_name_f,
	a.deb_ec_add_f,
	a.deb_ec_phone_f,
	a.deb_ec_mobile_f,
	a.deb_region,
	a.deb_open_date,
	a.deb_resource,
	a.deb_limit,
	a.deb_fees,
	a.deb_interest,
	a.deb_principal,
	a.deb_last_paydate,
	a.deb_last_pay,
	a.deb_bp,
	a.deb_b_d,
	a.deb_wo_amount,
	a.deb_update_by_user,
	a.deb_perm_msg,
	a.deb_perm_msg_date,
	a.deb_call_status_code,
	a.deb_prev_call_status_code,
	a.deb_agent,
	a.deb_reminder,
	a.deb_swap_count,
	a.deb_sms_count,
	a.deb_contact_type,
	a.deb_rpc,
	a.deb_spc,
	a.deb_is_kept,
	a.deb_afterpay,
	a.deb_is_lock,
	a.deb_bal_afterpay,
	a.deb_pri_afterpay,
	a.deb_last_swap_ts,
	b.CallReasonDesc as accstatus_name, 
	d.spc_description as SPC_DESC, 
	e.rpc_description as RPC_DESC, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=103) as other_ch_office, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=101) as other_ch_home, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=102) as other_ch_mobile1, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=109) as other_ch_mobile2, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=104) as family, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=105) as neighbour,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=107) as rel_person, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id  AND c.addphone_type=108) as other_ec", FALSE);
	
  $this->db->from("t_gn_debitur a");
  $this->db->join("t_lk_account_status b ","a.deb_call_status_code=b.CallReasonCode", "LEFT");
  $this->db->join("t_lk_speach_with d "," a.deb_spc=d.spc_code", "LEFT");
  $this->db->join("t_lk_remote_place e "," a.deb_rpc=e.rpc_code","LEFT");
  $this->db->join("t_gn_campaign f", "a.deb_cmpaign_id=f.CampaignId", "LEFT");
  $this->db->where("1","1", FALSE);
  
 // @ pack : ====================> 
 $num = 0;
  $query = $this->db->get(); 
  if( $query->num_rows() > 0 ) 
	  foreach( $query->result_assoc() as $rows )
{
	if(is_array($this->set_rows_headers) )
		foreach( $this->set_rows_headers as $hd => $row )
	{
		$source_data['data'][$num][$hd] = ( is_null( $row['field'] ) ? "" : $this->_SetEventHandler( $row['field'], $rows[$row['field']]) );
	}
	$num++;
  } 
  
  $source_data['header'] = array_keys($this->set_rows_headers);
  return $source_data;
  
}  
// _getDumperExcelData ================>

 
// -----------------------------------------------------
/*
 * @ pack   : call setProsessDumperExcel  
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 public function Inialize()
{
   $config =&M_Configuration::get_instance(); 
   if( is_object($config) ) 
	 foreach($config->_get_config('EXCEL_DUMPER') as $key => $value )
   {
		$this->{strtolower($key)}= $value; 
   }
} 

// -----------------------------------------------------
/*
 * @ pack   : call setProsessDumperExcel  
 * @ return : return tru/false only 
 */
 // -----------------------------------------------------
 
 public function _setProsessDumperExcel()
{
 $conds = FALSE;
 
 if( (strtolower($this->excel_dumper_split)=='yes') OR 
	( $this->excel_dumper_split ==1 )  OR 
	( strtolower($this->excel_dumper_split)=='true') )
 {
	$conds = $this->_SplitProsessDumperExcel(); // by splite excel file 
	
 } else {
	$conds = $this->_SetExportBigExcel();
	//$conds = $this-> _DefaultProsessDumperExcel(); // no split is default 
 }
 
 return $conds;
	
} // setProsessDumperExcel ====================> 


// -----------------------------------------------------
/*
 * @ pack   : _SplitProsessDumperExcel
 * @ return : return of process 
 */
 // -----------------------------------------------------
 
 
 private function _SplitProsessDumperExcel()
{
 
 $this->load->helper(array('EUI_ExcelWorksheet','EUI_ExcelWorkbookBig'));
 
 
  
/* pack -------------- addd data dumpers ---------- P*/
 
  $xls_rows_source = $this->_getDumperExcelData();	
  
  $tot_all_source  = (array)$xls_rows_source['data'];
  $header_all_data = (array)$xls_rows_source['header']; 
  $tot_records_xls = count($tot_all_source); 
  $tot_files_excel = ceil( $tot_records_xls/ $this->excel_dumper_perfile);
  $file_name_generate = 'card-alldata-'.date('ymd').'-'.date('His');

/* cek file path OK **/

  $BASE_PATH_HOME = ( $this->excel_dumper_path ? $this->excel_dumper_path : null );
  
  if(is_null($BASE_PATH_HOME)) {
	return FALSE;
  } 
  
  if(!is_dir($BASE_PATH_HOME)){
	mkdir($BASE_PATH_HOME,0777, TRUE);
  }
  
/* pack to result assoc **/
  
  if($tot_files_excel) 
	for( $i_xls= 0; $i_xls< $tot_files_excel;  $i_xls++ )
  {
		$start_length = $i_xls;
		
		$start_post = ($start_length * $this->excel_dumper_perfile);
		$data_slice = array_slice($tot_all_source, $start_post, $this->excel_dumper_perfile);
		
	/* pack start of file in here **/
		
		$BASE_EXCEL_FILE_NAME = "{$BASE_PATH_HOME}/{$file_name_generate}_file". ($i_xls+1) .".xls";
	/* pack excel modul on lib helper class */
	
		$workbook =& new writeexcel_workbookbig($BASE_EXCEL_FILE_NAME);
		$worksheet =& $workbook->addworksheet();
		
	 /* pack header format every file **/
	 
		$header_format =& $workbook->addformat();
		$header_format ->set_bold();
		$header_format->set_size(10);
		$header_format->set_color('white');
		$header_format->set_align('left');
		$header_format->set_align('vcenter');
		$header_format->set_pattern();
		$header_format->set_fg_color('black');
	 
	/** start create headers of data on file =============================>  **/
	
		$start_rows_excel = 0;
		foreach($header_all_data as $_key => $values ){
			$worksheet->write_string($start_rows_excel, $_key, $values, $header_format );
		}
		
	// set pack on content =============================> 
	
		$start_rows_excel = $start_rows_excel+1;
		foreach( $data_slice as $key => $value )
		{
			foreach($header_all_data as $h => $_header )
			{
				
				$worksheet->write_string($start_rows_excel, $h, $this->_SetEventHandler($_header, $value[$_header]));
			}
			
			$start_rows_excel+=1;
		}
		
		$workbook->close(); // end book
  }
  
 // pack : return call vack to user =========================> 
 
  if( !$i_xls ) return FALSE;
  
   return TRUE;
	
  
} // _SplitProsessDumperExcel ===================> 

// -----------------------------------------------------
/*
 * @ pack   : _SplitProsessDumperExcel
 * @ return : return of process 
 */
 // -----------------------------------------------------
 public function _SetClearDate( $argv  = null )
{
	if( is_null($argv) ){
		return NULL;
	}
	
	if(((string)$argv =='0000-00-00') 
		OR ( (string)$argv == '00-00-0000') )
	{
		return NULL;
	}
	
	return $argv;
}

// -----------------------------------------------------
/*
 * @ pack   : _SplitProsessDumperExcel
 * @ return : return of process 
 */
 // -----------------------------------------------------
 
 
 public function EventCallStatusCode( $code  = null ) 
{
	$this->db->reset_select();
	$this->db->select("a.CallReasonDesc", FALSE);
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCode", $code);
	
	$qry = $this->db->get();
	if( $qry -> num_rows() > 0 ){
		return $qry->result_singgle_value();
	} else {
		return NULL;
	}
}

// -----------------------------------------------------
/*
 * @ pack   : _SplitProsessDumperExcel
 * @ return : return of process 
 */
 // -----------------------------------------------------
 
 
function EventInfoPTP( $code = "" ){
	$arr_info_ptp = array(101=> "LUNAS", 102=>"CICILAN");
	return ( isset($arr_info_ptp[$code]) ? $arr_info_ptp[$code] : "");
}

function EventUploadBatch( $date = "" ){
	if( !is_null($date) ){
		return date('YmdHis', strtotime($date));
	}
	return "";
}
// -----------------------------------------------------
/*
 * @ pack   : _SplitProsessDumperExcel
 * @ return : return of process 
 */
 // -----------------------------------------------------
 
 private function _SetEventHandler( $field=null, $argv=null)
{
	
	$_event = array(
		"deb_perm_msg_date"=>"_SetClearDate", 
		"deb_call_status_code" => "EventCallStatusCode",
		"deb_prev_call_status_code" => "EventCallStatusCode",
		"infoptp" => "EventInfoPTP",
		"uploadbatch" => "EventUploadBatch"
	); 
	
	if( in_array($field, array_keys($_event))){
		
		//var_dump($_event[$field]);
		
		return call_user_func_array(
			array(get_class_instance(get_class($this)), $_event[$field]), 
			array($argv)
		);
	}
	return $argv;
	 
} // _SetEventHandler ======================> 


// -----------------------------------------------------
/*
 * @ pack   : _SplitProsessDumperExcel
 * @ return : return of process 
 */
 // -----------------------------------------------------
 
function _DefaultProsessDumperExcel()
{
 /* load model library =======================> */
 
 $this->load->helper(array('EUI_ExcelWorksheet','EUI_ExcelWorkbookBig'));
 $start_time = strtotime(date('Y-m-d H:i:s'));
 
 $source_data_excel = $this->_getDumperExcelData();	
 $end_time = _getDuration(strtotime(date('Y-m-d H:i:s')) -$start_time );
 echo "CI -> Query(s)". $end_time ." \n\r";
 
/* source of data  =======================> */
 
 $source_contents = (array)$source_data_excel['data'];
 $source_headers = (array)$source_data_excel['header'];
 
/* pack of attribute here **/

 $file_name_generate = 'card-alldata-deleted'.date('ymd').'-'.date('His').".xls";
 
/* cek file path OK **/

  $BASE_PATH_HOME = ( $this->excel_dumper_path ? $this->excel_dumper_path : null );
  
  if( is_null($BASE_PATH_HOME) ){
	return FALSE;
  }
  
  if(!is_dir($BASE_PATH_HOME)){
	mkdir($BASE_PATH_HOME,0777, TRUE);
  } 
  
 $BASE_EXCEL_FILE_NAME = "${BASE_PATH_HOME}/${file_name_generate}";
  
/* pack excel modul on lib helper class */

echo "Start Export to excel workbook Big.\n\r";
 
 $workbook =& new writeexcel_workbookbig($BASE_EXCEL_FILE_NAME);
 $worksheet =& $workbook->addworksheet();
		
/* pack header format every file **/
	 
 $header_format =& $workbook->addformat();
 $header_format ->set_bold();
 $header_format->set_size(10);
 $header_format->set_color('white');
 $header_format->set_align('left');
 $header_format->set_align('vcenter');
 $header_format->set_pattern();
 $header_format->set_fg_color('black');
 
 /** start create headers of data on file =============================>  **/
 
  $start_rows_excel = 0;
  if(is_array($source_headers))
	foreach($source_headers as $_key => $values )
  {
	$worksheet->write_string($start_rows_excel, $_key, $values, $header_format );
  }
  
 /** start create content of data on file =============================>  **/
 
 $start_rows_excel = $start_rows_excel+1;
 if(is_array($source_contents))
	foreach( $source_contents as $key => $value )
 {
	echo "$start_rows_excel.\n\r";
	
	foreach($source_headers as $h => $_header ){
		$worksheet->write_string($start_rows_excel, $h, $this->_SetEventHandler($_header, $value[$_header]) );
	 }
    $start_rows_excel+=1;
  }
		
  $workbook->close(); // end book 
  return TRUE;
  
	 
 } // _DefaultProsessDumperExcel ===================> 
 

 
// ===================== Function Default BigExcel process ================
function _SetExportBigExcel()
{
 global $argv, $argc;
 

 $campaignid = 0;
 $name_excel = "card-cf-deleted-alldata";
 if( isset($argv[4]) AND strtolower($argv[4]) == 'card' ){
	$name_excel = "card-deleted-alldata";
	$campaignid = 1;
 }
 
 if( isset($argv[4]) AND strtolower($argv[4]) == 'cf' ){
	$name_excel = "cf-deleted-alldata";
	$campaignid = 2;
 }
 
// ----------------------------------------------------------------------------------
 $this->load->helper(array('EUI_ExcelWorksheet','EUI_ExcelWorkbookBig'));   
 $file_name_generate = $name_excel . '-'.date('ymd').'-'.date('His').".xls";
 $BASE_PATH_HOME = ( $this->excel_dumper_path ? $this->excel_dumper_path : null );

  
  if( is_null($BASE_PATH_HOME) ){
	return FALSE;
  }
  
  if(!is_dir($BASE_PATH_HOME)){
	mkdir($BASE_PATH_HOME,0777, TRUE);
  } 
  
 $BASE_EXCEL_FILE_NAME = "${BASE_PATH_HOME}/${file_name_generate}";
 echo "Start Export to excel workbook Big.\n\r";
 
 $workbook =& new writeexcel_workbookbig($BASE_EXCEL_FILE_NAME);
 $worksheet =& $workbook->addworksheet();
		
/* pack header format every file **/
	 
 $header_format =& $workbook->addformat();
 $header_format ->set_bold();
 $header_format->set_size(10);
 $header_format->set_color('white');
 $header_format->set_align('left');
 $header_format->set_align('vcenter');
 $header_format->set_pattern();
 $header_format->set_fg_color('black');
 
 /** start create headers of data on file =============================>  **/
 
  $start_rows_excel = 0;
  $source_headers  = array_keys($this->set_rows_headers);
  if(is_array($source_headers))
	foreach($source_headers as $_key => $values )
  {
	$worksheet->write_string($start_rows_excel, $_key, $values, $header_format );
  }
  
 $start_exec_time = strtotime(date('Y-m-d H:i:s'));
 
 // ========== query ================================
 
  $this->db->reset_select();
  $this->db->select("
	a.deb_id,
	a.deb_cardno,
	a.deb_acct_no,
	a.deb_acct_no,
	a.deb_id_number,
	a.deb_no_card,
	a.deb_name,
	a.deb_mother_name,
	a.deb_dob,
	a.deb_add_hm,
	a.deb_billing_add,
	a.deb_add_off,
	a.deb_zip_h,
	a.deb_home_no1,
	a.deb_mobileno1,
	a.deb_office_ph1,
	a.deb_ec_name_f,
	a.deb_ec_add_f,
	a.deb_ec_phone_f,
	a.deb_ec_mobile_f,
	a.deb_region,
	a.deb_open_date,
	a.deb_resource,
	a.deb_limit,
	a.deb_fees,
	a.deb_interest,
	a.deb_principal,
	a.deb_last_paydate,
	a.deb_last_pay,
	a.deb_bp,
	(select count(ptps.ptp_id) from t_tx_ptp ptps where ptps.deb_id = a.deb_id group by ptps.deb_id) as ptpcounts,
	a.deb_b_d,
	a.deb_wo_amount,
	a.deb_update_by_user,
	a.deb_perm_msg,
	a.deb_perm_msg_date,
	a.deb_call_status_code,
	b.CallReasonDesc,
	a.deb_prev_call_status_code,
	(select az.CallReasonDesc from t_lk_account_status az where az.CallReasonCode = a.deb_prev_call_status_code) as prevac,
	a.deb_agent,
	a.deb_reminder,
	a.deb_swap_count,
	a.deb_sms_count,
	a.deb_contact_type,
	(select spc.rpc_name from t_lk_remote_place spc where spc.rpc_code = a.deb_rpc) as `deb_rpc`,
	(select spc.spc_name from t_lk_speach_with spc where spc.spc_code = a.deb_spc) as `deb_spc`,
	a.deb_is_kept,
	a.deb_afterpay,
	a.deb_is_lock,
	a.deb_bal_afterpay,
	a.deb_pri_afterpay,
	a.deb_last_swap_ts,
	a.deb_call_activity_datets,
	a.addhomephone as addhomephone,
	a.addhomephone2 as addhomephone2,
	a.addmobilephone as addmobilephone,
	a.addmobilephone2 as addmobilephone2,
	a.addofficephone as addofficephone,
	a.addofficephone2 as addofficephone2,
	a.other_agent as other_agent,
	a.other_card_number as other_card_number,
	a.other_accstatus as other_accstatus,
	a.addfax as addfax,
	a.addfax2 as addfax2,
	a.deb_created_ts as uploadbatch,
	a.deb_ssv_no as deb_ssv_no,
	'0' as dlq,
	'1' as flag,
	b.CallReasonDesc as accstatus_name, 
	d.spc_description as SPC_DESC, 
	e.rpc_description as RPC_DESC, 
	IF((select ptp.ptp_type from t_tx_ptp ptp where ptp.ptp_account = a.deb_acct_no and ptp.ptp_type<>0 limit 0,1) IS NULL,a.infoptp,NULL) as infoptp,
	IF(a.acc_mapping<>'', a.acc_mapping, (select group_concat(am.Card) from t_gn_account_mapping am where am.CardNo=a.deb_acct_no)) as acc_mapping,
	(select count(ts.CallHistoryId) as total from t_gn_callhistory ts where ts.AccountNo = a.deb_acct_no) as attempt_call,
	(select az.CallHistoryNotes from t_gn_callhistory az where az.AccountNo = a.deb_cardno order by az.CallHistoryUpdatedTs desc limit 0,1) as Notes,
	(select ptp.ptp_date from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpDate,
	(select ptp.ptp_amount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpAmount,
	(select ptp.ptp_chanel from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpChannel,
	(select ptp.ptp_tenor from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptpTenor,
	(select ptp.ptp_discount from t_tx_ptp ptp where ptp.deb_id = a.deb_id order by ptp.ptp_create_ts desc limit 0,1) as ptp_discount,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=103) as other_ch_office, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=101) as other_ch_home, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=102) as other_ch_mobile1, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=109) as other_ch_mobile2, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=104) as family, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=105) as neighbour,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=107) as rel_person, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id  AND c.addphone_type=108) as other_ec", FALSE);
	
  $this->db->from("t_gn_debitur_deleted a");
  $this->db->join("t_lk_account_status b ","a.deb_call_status_code=b.CallReasonCode", "LEFT");
  $this->db->join("t_lk_speach_with d "," a.deb_spc=d.spc_code", "LEFT");
  $this->db->join("t_lk_remote_place e "," a.deb_rpc=e.rpc_code","LEFT");
  $this->db->join("t_gn_campaign f", "a.deb_cmpaign_id=f.CampaignId", "LEFT");
  
  if( $campaignid <> 0 ){
	$this->db->where("a.deb_cmpaign_id", $campaignid, FALSE);
  }	  
  
  // @ pack : ====================> 
  
 
  
  $start_rows_excel = $start_rows_excel+1;
 // echo $this->db->last_query();
  $query = $this->db->get(); 
  //echo $this->db->last_query();
  // exit();
  
  $end_exec_time = strtotime(date('Y-m-d H:i:s'));
  $end_exec_durs = ($end_exec_time-$start_exec_time);
  echo "Querie(s) time :". _getDuration($end_exec_durs) ." Done.\n\r";
  
  
  $num = 0;
   echo "start write to excel.\n\r";
  if( $query->num_rows() > 0 ) 
	foreach( $query->result_assoc() as $key => $value  ) 
{
	echo ".";
	$source_field = $this->set_rows_headers;
	foreach($source_headers as $h => $_header )
	{
		$arr_source_field  = $source_field[$_header];
		if( !is_null($arr_source_field['field']) ) 
		{	
			$field = $arr_source_field['field'];
			$worksheet->write_string($start_rows_excel, $h, $this->_SetEventHandler($field, $value[$field]) );
		} 
		
	 }
    $start_rows_excel+=1;
  } 
  $workbook->close(); // end book 
  return TRUE;
  
  
} 

// END CLASS 
}

?>