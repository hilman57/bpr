<?php

/*
 * @ pack : manage round data its
 */
 
class M_MgtRandDeb extends EUI_Model 
{
	private static $Instance = null;
/*
 * @ pack : constractor of class
 */	

	public function M_MgtRandDeb()
	{
		$this->load->model(array('M_SysUser','M_SetCallResult','M_MgtBucket','M_SetCampaign'));
		
	}
	
	/*
	 * @ pack : get Call status
	 */
	 
	 private function _get_select_status()
	{
	 $conds = array();
	 
	 $recs = $this->M_SetCallResult->_getCallReasonId();
	 foreach( $recs as $code => $values ) {
		$conds[$code] = $values['name']; 
	 }
		
	 return $conds;
	 
	}
	
	/*
	 * @ pack : manage round data its
	 */	
	 
	 public static function &Instance()
	{
		if( is_null(self::$Instance) ) {
			self::$Instance = new self();
		}
		
		return self::$Instance;
	}
	
	public function get_modul_random()
	{
		$modul = array();
		
		$this->db->select("a.modul_id_rnd, a.modul_name, a.modul_ui_setup, a.modul_setup_detail,a.modul_ui_report");
		$this->db->from("t_lk_modul_random a");
		
		// $this->db->order_by('a.CallReasonOrder','ASC');
		foreach( $this ->db ->get()->result_assoc() as $rows ){
			$modul['combo'][$rows['modul_id_rnd']] = $rows['modul_name'];	
			$modul['ui_setup'][$rows['modul_id_rnd']] = $rows['modul_ui_setup'];	
			$modul['ui_detail_setup'][$rows['modul_id_rnd']] = $rows['modul_setup_detail'];	
			$modul['ui_report'][$rows['modul_id_rnd']] = $rows['modul_ui_report'];	
		}	

		return $modul;		 
	}
	
	/*
	 * @ pack : manage lock data its
	 */
	 
	public function _get_dropdown()
	{	
		$dropdown = array(
		'DROPDOWN_ROUND_STATUS' => $this->_get_select_status()
		);
		
		return $dropdown;
	}
	
	
	
	/*
	 * EUI :: _get_default() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */	
	 
	public function _get_default()
	{
		$this -> EUI_Page -> _setPage(20); 
		
	 /* set default of query ***/
	 
		$this->EUI_Page->_setSelect("a.modul_setup_id");
		$this->EUI_Page->_setFrom("t_gn_modul_setup a ");
		
	 /* join tables **/
		$this->EUI_Page->_setJoin("t_tx_agent b " ,"a.mod_set_createdby = b.UserId","INNER");
		$this->EUI_Page->_setJoin("t_lk_modul_random c " ,"a.modul_id_rnd=c.modul_id_rnd","INNER",TRUE);
		
	/* set filter **/
		
		// if($this->URI->_get_have_post('status_campaign')) {
			// $this->EUI_Page->_setAnd("a.CampaignStatusFlag", $this->URI->_get_post('status_campaign'));
		// }
		//echo $this->EUI_Page->_getCompiler();
		return $this -> EUI_Page;
	}
	
	/*
	 * @ def 		: _get_resource // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_resource()
	{
		self::_get_content();
		if(($this->EUI_Page->_get_query()!=="" )) 
		{
			return $this->EUI_Page->_result();
		}	
	}
	
	/*
	 * @ def 		: _get_page_number // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_page_number() 
	 {
		if( $this -> EUI_Page -> _get_query()!='' ) {
			return $this -> EUI_Page -> _getNo();
		}	
	 }
	
	/*
	 * EUI :: _get_content() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */		
	 
	public function _get_content()
	{
		$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
		$this->EUI_Page->_setPage(20);
		
	/* default of select **/
		$this->EUI_Page->_setArraySelect(array(
			"a.modul_id_rnd as random_id" => array('random_id', 'random_id', 'hidden'),
			"a.modul_setup_id AS modul_setup_id"=> array('modul_setup_id','ID','primary'),
			"c.modul_name as modul_name" => array("modul_name","Random By"),
			"b.full_name as agent_name" => array("agent_name","Created By"),
			"a.mod_set_createdts AS TimeCreate" => array("TimeCreate","Created Date"), 
			"IF(a.mod_set_running=1,'Running...','Stoped') AS RunModul" => array("RunModul","Status")
		));
			
	/* set from query select */
		
		$this->EUI_Page->_setFrom("t_gn_modul_setup a");
		$this->EUI_Page->_setJoin("t_tx_agent b " ,"a.mod_set_createdby = b.UserId","INNER");
		$this->EUI_Page->_setJoin("t_lk_modul_random c " ,"a.modul_id_rnd=c.modul_id_rnd","INNER",TRUE);
		
	/** set filtering **/

		// if( $this->URI->_get_have_post('status_campaign')){
			// $this->EUI_Page->_setAnd("a.CampaignStatusFlag", $this->URI->_get_post('status_campaign'));
		// }
		
	
	/* set order **/

		if( $this->URI->_get_have_post('order_by')){
			$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
		}else{
			$this->EUI_Page->_setOrderBy('a.mod_set_createdts','DESC');
		}
		$this->EUI_Page->_setLimit();
		// echo $this->EUI_Page->_getCompiler();
	}
	public function ModulRunning( $modul = null )
	{
		$setup['running'] = false;
		if(!is_null($modul))
		{
			$this->db->reset_select();
			$this->db->select("a.modul_setup_id,a.modul_id_rnd");
			$this->db->from("t_gn_modul_setup a");
			$this->db->where("a.mod_set_running", "1");
			if(is_array($modul) )
			{
				$this->db->where_in("a.modul_id_rnd", $modul );
			}
			else
			{
				$this->db->where("a.modul_id_rnd", $modul );
			}
			$qry = $this->db->get();
			if( $qry->num_rows() > 0 )
			{
				if( $rows = $qry->result_first_assoc() )
				{
					$setup['running'] = true;
					$setup['modul_id'] = $rows['modul_setup_id'];
					$setup['random_id'] = $rows['modul_id_rnd'];
					
				}
			}
			return $setup;
		}
		
	}
	private function saveRoundSetup()
	{
		$RandomBy = _get_post('random_by');
		$modul_running = $this->ModulRunning( $RandomBy );
		$modul_setup_id = null;
		$setup_round_id = null;
		$insert_setup= false;
		$user_create = _get_session('UserId');
		$duration = _get_post('round_time_status');
		$split_round = _get_post('bool_round_team');
		if( $this->URI->_get_have_post('user_create_setup') )
		{
			$user_create = _get_post('user_create_setup');
		}
		
		if( $modul_running['running'] )
		{
			$modul_setup_id = $modul_running['modul_id'];
			$datail_round = $this->_get_detail_round($modul_running['modul_id']);
			$setup_round_id = $datail_round['st_round_id'];
		}
		else
		{
			$this->db->set('modul_id_rnd',$RandomBy);
			$this->db->set('mod_set_createdby',_get_session('UserId'));
			$this->db->set('mod_set_running',1);
			$this->db->set('mod_set_createdts','now()',false);
			if( $this->db->insert('t_gn_modul_setup') )
			{
				$modul_setup_id = $this->db->insert_id();
			}
		}
		
		if(!is_null($modul_setup_id))
		{
			$this->db->set('modul_setup_id',$modul_setup_id);
			$this->db->set('duration',$duration);
			$this->db->set('create_by',$user_create);
			$this->db->set('bool_running',1);
			$this->db->set('bool_round_split',$split_round);
			$this->db->set('st_create_date_ts',date('Y-m-d H:i:s'));
			$this->db->set('next_round_ts','SEC_TO_TIME(('.$duration.'*60)+TIME_TO_SEC(now()))',false);
			if( $this->db->insert('t_st_round') )
			{
				$setup_round_id = $this->db->insert_id();
				if( $split_round== 1)
				{
					$Tl_List = explode(',',_get_post('round_tl'));
					foreach( $Tl_List as $index => $tl_id)
					{
						$this->db->set('st_round_id',$setup_round_id);
						$this->db->set('user_handling',3);
						$this->db->set('userid',$tl_id);
						$this->db->insert('t_gr_user_round');
					}
				}
				$insert_setup = true;
			}
		}
		
		
		return array(
			'modul_setup_id' => $modul_setup_id, 
			'st_round_id'=>$setup_round_id,
			'status_setup'=>$insert_setup);
	}

	public function _SetRoundStatus()
	{
		$Msg = array('success'=>0,'Agent'=>0,'debitur'=>0);
		if( $this->URI->_get_have_post('round_account_status') AND
			$this->URI->_get_have_post('round_time_status') AND
			$this->URI->_get_have_post('random_by') AND 
			$this->URI->_get_have_post('bool_round_team')
		  )
		{
			if( _get_post('bool_round_team') == 1 AND !$this->URI->_get_have_post('round_tl') )
			{
				$Msg = array('success'=>0,'Agent'=>"Not Found",'debitur'=>0);
				return $Msg;
				exit;
			}

			$status_to_round = explode(',',_get_post('round_account_status'));
			$user_round_rilter = array(
				'a.user_state'=>1,
				'a.logged_state'=>1
			);
			if(_get_post('bool_round_team'))
			{
				$Tl_List = explode(',',_get_post('round_tl'));
				$user_round_rilter = array(
					'a.user_state'=>1,
					'a.logged_state'=>1,
					'a.tl_id'=>$Tl_List
				);
			}
			
			
			$deb = $this->get_debitur_status($status_to_round);
			$ActDeskoll =$this->M_SysUser->_get_teleamarketer($user_round_rilter);
			// echo $this->db->last_query();

			if( count($deb) <= 0 )
			{
				return $Msg;
				exit;
			}
			
			if( count($ActDeskoll) <= 0 )
			{
				$Msg = array('success'=>0,'Agent'=>0,'debitur'=>count($deb));
				return $Msg;
				exit;
			}
			
			if ( count($deb) < count($ActDeskoll) )
			{
				$Msg = array('success'=>0,'Agent'=>count($ActDeskoll),'debitur'=>count($deb));
				return $Msg;
				exit;
			}
			
			$setup = $this->saveRoundSetup();
			
			//exit;
			// echo "INI debitur";
			// echo "<pre>";
			// print_r($deb);
			// echo "</pre>";
			if($setup['status_setup']==false)
			{
				$Msg = array('success'=>0,'Agent'=>"Failed",'debitur'=>"Failed");
				return $Msg;
				exit;
			}
			$i=0;
			foreach($deb as $rows => $ArrayValue)
			{
				$this->db->set('deb_id',$ArrayValue['deb_id']);
				$this->db->set('acc_no',$ArrayValue['deb_acct_no']);

				
				$this->db->set('modul_setup_id',$setup['modul_setup_id']);
				if( $this->db->insert('t_gn_buckettrx_debitur') )
				{
					$TRX_ID[$i] = $this->db->insert_id();

					$this->db->set('a.deb_is_access_all',1);
					$this->db->where('a.deb_id',$ArrayValue['deb_id'] );
					$this->db->update('t_gn_debitur a');
					$i++;
				}
			}
			// echo "INI bucket trx debitur";
			// echo "<pre>";
			// print_r($TRX_ID);
			// echo "</pre>";
			if ( count($TRX_ID) > 0 )
			{
				$random = $this->_RandomAssign( $ActDeskoll,$TRX_ID );
				// echo "INI random debitur";
				// echo "<pre>";
				// print_r($random);
				// echo "</pre>";
				foreach($random as $Agentid => $ArrayValue)
				{
					foreach($ArrayValue as $rows => $trx_id){

						$this->db->set('bucket_trx_id',$trx_id);
						$this->db->set('agent_id',$Agentid);
						$this->db->set('st_round_id',$setup['st_round_id']);
						if( $this->db->insert('t_gn_round_assign') )
						{
							$Msg['debitur']++;
						}
					}
					$Msg['Agent']++;
				}
				$Msg['success']=1;
			}
			else
			{
				$this->db->set('a.bool_running',0);
				$this->db->where('a.st_round_id',$setup['st_round_id'] );
				$this->db->update('t_st_round a');
			}
			
			// print_r($setup);
		}		  
		return $Msg;
	}
	
	/*** Acak Kepemilikan data agent yang login (Pertama Kali)
	**** _RandomAssign()
	**** param : AgentId, DebiturId
	**** return : array()
	***/
	
	public function _RandomAssign($AgentId,$Debitur)
	{
	
		//inisialisasi awal
		$SumAssign=0;
		$i=0;
		$j=0;
		$k=0;
		$tempagent = array ();
		
		(int)$SigmaDebitur = count($Debitur);
		(int)$SigmaDeskoll = count($AgentId);
		
		//cek debitur lebih sedikit dari agent yang online
		if( $SigmaDebitur < $SigmaDeskoll )
		{
			//ambil id agent online secara acak sebanyak data debitur
			$rand_agent_id=array_rand($AgentId,$SigmaDebitur);
			
			foreach($rand_agent_id as $urut => $choose_agent_id )
			{
				$tempagent[$choose_agent_id] = $AgentId[$choose_agent_id];
			}
			$AgentId = $tempagent;
			$tempagent = array();
			(int)$SigmaDeskoll = count($AgentId);
		}
		//hitung sisa data
		(int)$ModDeb = $SigmaDebitur % $SigmaDeskoll;
		
		
		//hitung perolehan data yang akan didapat deskoll
		$SumAssign = (INT)($SigmaDebitur/$SigmaDeskoll);
		
		//pecah debitur sebanyak perolehan data
		$split_deb = array_chunk($Debitur,$SumAssign);
		
		//membagi rata debitur ke deskol
		foreach($AgentId as $deskollid => $deskollname){			
			foreach($split_deb[$i] as $index => $deb_id_split){
				$RandomAssign[$deskollid][$j]=$deb_id_split;
				$j++;
			}
			$i++;
		}
		if($ModDeb>0)
		{
			$sisa_debitur = array();
			
			/**acak deskolll untuk membagi sisa debitur
			** pengacak berdasarkan sisa hasil bagi data
			**/
			$rand_deskoll=array_rand($AgentId,$ModDeb);
			
			/**
			** Membagi sisa data debitur ke deskoll yang terpilih			
			**/
			
			for ($index_split = $i; $index_split <= count($split_deb); $index_split++) {
				$sisa_split[] = $split_deb[$index_split];
			}
			$AgentId = array(); //bersihkan data
			$split_deb = array();
			foreach( $sisa_split as $index_sisa_split => $array_sisa)
			{
				foreach( $array_sisa as $index_sisa => $deb)
				{
					$sisa_debitur[] = $deb;
				}
			}
			if(is_array($rand_deskoll))
			{
				foreach($rand_deskoll as $ind=>$desk_id){
					$RandomAssign[$desk_id][$j+$k]=$sisa_debitur[$k];
					$k++;
				}
			}
			else
			{
				foreach(array($rand_deskoll) as $ind=>$desk_id){
					$RandomAssign[$desk_id][$j+$k]=$sisa_debitur[$k];
					$k++;
				}
			}
		}
		
		
		//menampilkan kelayar
		/*
		echo "agent 12 menadapat ".count($RandomAssign[12]);
		echo ", agent 87 menadapat ".count($RandomAssign[87]);
		echo ", agent 2 menadapat ".count($RandomAssign[2]);
		
		// echo "<pre>";
		// print_r($Debitur);
		// echo "</pre>";
		echo "< br />";
		echo "Jumlah Debitur ".$SigmaDebitur;
		
		// echo "<pre>";
		// print_r($AgentId);
		// echo "</pre>";
		echo "Deskol yang aktif ".$SigmaDeskoll;
		
		// echo "< br/>";
		// echo $ModDeb;
		echo "< br />";
		echo "Perolehan deskol = ".$SumAssign;
		
		echo "<br /> sisa hasil bagi";
		echo $ModDeb;
		echo "<pre>";
		print_r($RandomAssign);
		echo "</pre>";
		*/
		// echo "spliiiiittt<pre>";
		// print_r($split_deb);
		// echo "</pre>";
		// echo $duration;
		
	
		return $RandomAssign;
	}
	
	private function get_debitur_status($status)
	{
		$var_argv = array();
		$this->db->reset_select();
		$this->db->select('a.deb_id,a.deb_acct_no');
		$this->db->from('t_gn_debitur a');
		$this->db->join('t_gn_assignment b ',' a.deb_id = b.CustomerId','INNER');
		$this->db->where_in('a.deb_call_status_code', $status );
		$this->db->where('b.AssignSelerId IS NOT NULL');
		$this->db->order_by("a.deb_id", "RANDOM");
		
		$qry = $this->db->get();
		if( $qry->num_rows() > 0 ) {
			$i=0;
			foreach($qry->result_assoc() as $rows)
			{
				$var_argv[$i] = $rows;
				$i++;
			}
		}
		// echo $this->db->last_query();
		return $var_argv;
	}
	
	public function _RoundDebitur($round_setup = array())
	{
		$effect = false;
		if(count($round_setup) > 0)
		{
			$trx_id_deb = $this->get_trx_round($round_setup);
			$ActDeskoll = $this->getUserRound($round_setup);
			
			if( count($trx_id_deb) <= 0 )
			{
				return $effect;
			}
			
			if( count($ActDeskoll) <= 0 )
			{
				
				return $effect;
			}
			
			if ( count($trx_id_deb) < count($ActDeskoll) )
			{
				return $effect;
			}
			
			$random = $this->_RandomAssign( $ActDeskoll,$trx_id_deb );
			foreach($random as $Agentid => $ArrayValue)
			{
				foreach($ArrayValue as $rows => $trx_id){
					
					$this->db->where('round_id', $trx_id);
					$this->db->update('t_gn_round_assign',array('agent_id'=>$Agentid) );
					if($this->db->affected_rows()>0)
					{
						$effect=true;
					}
				}
			}
		}
		return $effect;
	}
	
	private function getUserRound($round_setup)
	{
		$user = array();
		$user_round_rilter = array(
			'a.user_state'=>1,
			'a.logged_state'=>1
		);
			
		
		if(count($round_setup) > 0 AND isset($round_setup['st_round_id']) )
		{
			if($round_setup['bool_round_split'])
			{
				$Tl_List = array();
				$this->db->reset_select();
				$this->db->select('*');
				$this->db->from('t_gr_user_round a');
				$this->db->where('a.st_round_id',$round_setup['st_round_id']);				
				$this->db->where('a.user_handling',3);				
				$qry = $this->db->get();
				if( $qry->num_rows() > 0 ) 
				{
					$i=0;
					foreach($qry->result_assoc() as $rows)
					{
						$Tl_List[$i] = $rows['userid'];
						$i++;
					}
				}
				$user_round_rilter = array(
					'a.user_state'=>1,
					'a.logged_state'=>1,
					'a.tl_id'=>$Tl_List
				);
			}
		}
		$user = $this->M_SysUser->_get_teleamarketer($user_round_rilter);
		return $user;
	}
	
	// public function _force_process_round()
	// {
		// $callback = false;
		// $round_setup = $this ->get_round_setup();
		// if(isset($round_setup['st_round_id']))
		// {
			// if( $this ->_RoundDebitur() )
			// {
				// $this->db->set('next_round_ts','SEC_TO_TIME(('.$round_setup['duration'].'*60)+TIME_TO_SEC(now()))',false);
				// $this->db->where("st_round_id", $round_setup['st_round_id']);
				// if( $this->db->update("t_st_round") )
				// {
					// $callback = true;
				// }
			// }
		// }
		// return $callback;
	// }
	
	public function get_round_setup()
	{
		$round = array(2,3) ;
		$datail_round = array();
		$modul_running = $this->ModulRunning( $round );
		if( $modul_running['running'] )
		{
			$datail_round = $this->_get_detail_round($modul_running['modul_id']);
		}
		return $datail_round;
	}
	
	public function get_access_all_setup()
	{
		$access_all = array(1) ;
		$datail_access_all = array();
		$modul_running = $this->ModulRunning( $access_all );
		// echo"<pre>";
		// var_dump($modul_running);
		// echo"</pre>";
		if( $modul_running['running'] )
		{
			$datail_access_all = $this->_get_detail_access_all($modul_running['modul_id']);
			// echo"<pre>";
			// var_dump($datail_access_all);
			// echo"</pre>";
		}
		return $datail_access_all;
	}
	
	private function get_trx_round($round_setup = array())
	{
		$var_argv = array();
		if(count($round_setup) > 0 AND isset($round_setup['st_round_id']) )
		{
			$this->db->reset_select();
			$this->db->select('c.round_id');
			$this->db->from('t_gn_modul_setup a');
			$this->db->join('t_gn_buckettrx_debitur b ',' a.modul_setup_id = b.modul_setup_id ','INNER');
			$this->db->join('t_gn_round_assign c ',' b.bucket_trx_id = c.bucket_trx_id','INNER');
			$this->db->join('t_st_round d','c.st_round_id=d.st_round_id','INNER');
			$this->db->where('a.mod_set_running','1');
			$this->db->where('b.claim_flag','0');
			$this->db->where('d.st_round_id',$round_setup['st_round_id']);
			$this->db->order_by("c.round_id", "RANDOM");
			$qry = $this->db->get();
			if( $qry->num_rows() > 0 ) {
				$i=0;
				foreach($qry->result_assoc() as $rows)
				{
					$var_argv[$i] = $rows['round_id'];
					$i++;
				}
			}
			// echo "<pre>";
			// echo $this->db->last_query();
			// echo "</pre>";
		}
		
		return $var_argv;
	}
	
	public function _get_detail_access_all( $setupid = null )
	{
		$data = array();
		if(is_null($setupid) and !$this->URI->_get_have_post('SetupId') )
		{
			return $data;
		}
		
		$this->db->reset_select();
		$this->db->select("	a.modul_setup_id,
							d.modul_name,
							a.modul_id_rnd,
							a.mod_set_createdts,
							b.st_access_create_id as created_id,
							c.full_name,
							b.st_access_all_id,
							b.st_access_start,
							b.st_access_end,
							b.bool_auto_start,
							b.bool_running"
						);
		$this->db->from('t_gn_modul_setup a');
		$this->db->join('t_st_access_all b','a.modul_setup_id=b.modul_setup_id','INNER');
		$this->db->join('t_tx_agent c','b.st_access_create_id = c.UserId','INNER');
		$this->db->join('t_lk_modul_random d','a.modul_id_rnd=d.modul_id_rnd','INNER');
		
		if(!is_null($setupid))
		{
			if(is_array($setupid))
			{
				$this->db->where_in('a.modul_setup_id',$setupid);
			}
			else
			{
				$this->db->where('a.modul_setup_id',$setupid);
			}
			$this->db->where('b.bool_running',1);
		}
		elseif($this->URI->_get_have_post('SetupId'))
		{
			$out = new EUI_Object(_get_all_request());
			
			$this->db->where('a.modul_setup_id', $out->get_value('SetupId') );
			
			if(_get_have_post('orderby') )  {
				$this->db->order_by($out->get_value('orderby'), $out->get_value('type'));
			} else {
				$this->db->order_by("b.st_create_date_ts", "DESC");
			}
		}
		else
		{
			return $data;
		}
		
		$qry = $this->db->get();
		if( $qry->num_rows() > 0 ) 
		{
			$data = $qry->result_assoc();
		}
		return $data;
	}
	
	public function _get_detail_round( $setupid = null )
	{
		$data = array();
		// var_dump(is_null($setupid));
		if(is_null($setupid) and !$this->URI->_get_have_post('SetupId') )
		{
			return $data;
		}
		
		$this->db->reset_select();
		$this->db->select("	
			a.modul_setup_id,
			d.modul_name,
			a.modul_id_rnd,
			a.mod_set_createdts,
			c.full_name,
			b.st_round_id,
			b.duration,
			b.next_round_ts,
			b.st_create_date_ts,
			b.bool_running,
			b.bool_round_split,
			b.create_by as created_id"
		);
		$this->db->from('t_gn_modul_setup a');
		$this->db->join('t_st_round b','a.modul_setup_id = b.modul_setup_id','INNER');
		$this->db->join('t_tx_agent c','b.create_by =c.UserId','LEFT');
		$this->db->join('t_lk_modul_random d','a.modul_id_rnd=d.modul_id_rnd','INNER');
		if(!is_null($setupid))
		{
			if(is_array($setupid))
			{
				$this->db->where_in('a.modul_setup_id',$setupid);
			}
			else
			{
				$this->db->where('a.modul_setup_id',$setupid);
			}
			$this->db->where('b.bool_running',1);
		}
		elseif($this->URI->_get_have_post('SetupId'))
		{
			$out = new EUI_Object(_get_all_request());
			
			$this->db->where('a.modul_setup_id', $out->get_value('SetupId') );
			
			if(_get_have_post('orderby') )  {
				$this->db->order_by($out->get_value('orderby'), $out->get_value('type'));
			} else {
				$this->db->order_by("b.st_create_date_ts", "DESC");
			}
		}
		else
		{
			return $data;
		}
		$qry = $this->db->get();
		if( $qry->num_rows() > 0 ) 
		{
			
			$data = $qry->result_assoc();
		}
		return $data;
	}
	
	public function CheckDebiturIsRandom($bucket_trx_id=null)
	{
		$data = array();
		if(!is_null($bucket_trx_id))
		{
			$this->db->reset_select();
			$this->db->select(" a.modul_id_rnd,
								b.bucket_trx_id, 
								c.access_all_id,
								d.round_id,
								b.deb_id",FALSE);
			$this->db->from("t_gn_modul_setup a");
			$this->db->join("t_gn_buckettrx_debitur b","a.modul_setup_id=b.modul_setup_id","INNER");
			$this->db->join("t_gn_access_all c","b.bucket_trx_id=c.bucket_trx_id","LEFT");
			$this->db->join("t_gn_round_assign d","b.bucket_trx_id=d.bucket_trx_id","LEFT");
			$this->db->join("t_gn_debitur e","b.deb_id=e.deb_id","INNER");
			$this->db->where("a.mod_set_running","1");
			$this->db->where("e.deb_is_access_all","1");
			$this->db->where("b.bucket_trx_id",$bucket_trx_id);
			$qry = $this->db->get();
			if( $qry->num_rows() > 0 ) {
				foreach($qry->result_assoc() as $rows)
				{
					$data['modul_id_rnd'] = $rows['modul_id_rnd'];
					$data['bucket_trx_id'] = $rows['bucket_trx_id'];
					$data['access_all_id'] = $rows['access_all_id'];
					$data['round_id'] = $rows['round_id'];
					$data['deb_id'] = $rows['deb_id'];
				}
			}
		}
		return $data;
	}
	
	public function _StopRandomModule( $SetupId = null )
	{
		$msg=array('Message'=>0);
		// $SetupId = $this->URI->_get_post('SetupId');
		if(!is_null($SetupId))
		{
			$this->db->reset_select();
			$this->db->where( "a.modul_setup_id", $SetupId );
			$this->db->from("t_gn_buckettrx_debitur a");
			if( $this->db->count_all_results() == 0)
			{
				$this->db->reset_select();
				$this->db->set("mod_set_running","0");
				$this->db->where("mod_set_running","1");
				$this->db->update("t_gn_modul_setup");
				if( $this->db->affected_rows()>0 )
				{
					$msg['Message']=1;
				}
			}
			else
			{
				$this->db->reset_select();
				$this->db->query(  "UPDATE t_gn_modul_setup a
									INNER JOIN t_gn_buckettrx_debitur b ON a.modul_setup_id=b.modul_setup_id
									INNER JOIN t_gn_debitur c ON b.deb_id=c.deb_id
									SET c.deb_is_access_all = 0,a.mod_set_running=0
									WHERE a.mod_set_running = 1
									AND a.modul_setup_id = ".$SetupId );
				if( $this->db->affected_rows()>0 )
				{
					$msg['Message']=1;
				}
			}
		}
		
		
		return $msg;
	}
	
	public function _CheckClaimReq()
	{
		$msg = array('JumlahClaim'=>0);
		if($this->URI->_get_have_post('SetupId'))
		{
			$SetupId = $this->URI->_get_post('SetupId');
			$this->db->reset_select();
			$this->db->select("d.modul_name, count(c.claim_id) AS jumlahClaim");
			$this->db->from("t_gn_modul_setup a");
			$this->db->join("t_gn_buckettrx_debitur b","a.modul_setup_id=b.modul_setup_id","INNER");
			$this->db->join("t_gn_claim_debitur c","b.bucket_trx_id = c.bucket_trx_id","INNER");
			$this->db->join("t_lk_modul_random d","a.modul_id_rnd=d.modul_id_rnd","INNER");
			$this->db->where("c.approval_status","104");
			$this->db->where("a.modul_setup_id",$SetupId);
			$this->db->group_by("a.modul_id_rnd"); 
			$qry = $this->db->get();
			if( $qry->num_rows() > 0 ) {
				foreach( $qry->result_assoc() as $rows ) {
					$msg['SetupName'] = $rows['modul_name'];
					$msg['JumlahClaim'] = $rows['jumlahClaim'];
				}
			}
		}
		return $msg;
		
	}
	/*
	 * EUI :: _get_default() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */	
	 
	public function _get_default_lock_random()
	{
		$this -> EUI_Page -> _setPage(20); 
		
	 /* set default of query ***/
	 
		$this->EUI_Page->_setSelect("a.lock_random_id");
		$this->EUI_Page->_setFrom("t_gn_agentlock_random a ");
		
	 /* join tables **/
		$this->EUI_Page->_setJoin("t_tx_agent b " ,"a.agent_id = b.UserId","INNER",TRUE);
		
	/* set filter **/
		$modul_random = $this->get_modul_random();
		$running_modul = $this->ModulRunning(array_keys($modul_random['combo']));
		if($running_modul['running']===true)
		{
			$this->EUI_Page->_setAnd('a.modul_setup_id', $running_modul['modul_id'] );
		}
		else
		{
			$this->EUI_Page->_setAnd('a.modul_setup_id', null );
		}
		// if($this->URI->_get_have_post('status_campaign')) {
			// $this->EUI_Page->_setAnd("a.CampaignStatusFlag", $this->URI->_get_post('status_campaign'));
		// }
		//echo $this->EUI_Page->_getCompiler();
		return $this -> EUI_Page;
	}
	
	public function _get_resource_lock_random()
	{
		self::_get_content_lock_random();
		if(($this->EUI_Page->_get_query()!=="" )) 
		{
			return $this->EUI_Page->_result();
		}	
	}
	
	/*
	 * @ def 		: _get_page_number // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_page_number_lock_random() 
	 {
		if( $this -> EUI_Page -> _get_query()!='' ) {
			return $this -> EUI_Page -> _getNo();
		}	
	 }
	 
	 /*
	 * EUI :: _get_content() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */		
	 
	public function _get_content_lock_random()
	{
		$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
		$this->EUI_Page->_setPage(20);
		
	/* default of select **/
		$this->EUI_Page->_setArraySelect(array(
			"a.lock_random_id as lock_agent_id" => array('lock_agent_id', 'ID', 'primary'),
			"b.id as agent_code" => array("agent_code","Agent Code"),
			"b.full_name as agent_name" => array("agent_name","Agent Locked"),
			"IF(a.lock_agent_status=1,'Locked','Unlock') AS Lock_status" => array("Lock_status","Lock Status")
		));
			
	/* set from query select */
		
		$this->EUI_Page->_setFrom("t_gn_agentlock_random a ");
		
	 /* join tables **/
		$this->EUI_Page->_setJoin("t_tx_agent b " ,"a.agent_id = b.UserId","INNER",TRUE);
		
	/** set filtering **/
		$modul_random = $this->get_modul_random();
		$running_modul = $this->ModulRunning(array_keys($modul_random['combo']));
		if($running_modul['running']===true)
		{
			$this->EUI_Page->_setAnd('a.modul_setup_id', $running_modul['modul_id'] );
		}
		else
		{
			$this->EUI_Page->_setAnd('a.modul_setup_id', null );
		}
		// if( $this->URI->_get_have_post('status_campaign')){
			// $this->EUI_Page->_setAnd("a.CampaignStatusFlag", $this->URI->_get_post('status_campaign'));
		// }
		
	
	/* set order **/

		if( $this->URI->_get_have_post('order_by')){
			$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
		}else{
			$this->EUI_Page->_setOrderBy('a.lock_random_id','DESC');
		}
		$this->EUI_Page->_setLimit();
		// echo $this->EUI_Page->_getCompiler();
	}
	
	public function save_lock_agent()
	{
		$success = 0;
		$fail = 0;
		$cond = array('status'=>0,'message'=>"");
		if($this->URI->_get_have_post('agent_list'))
		{
			$agent = $this ->URI->_get_array_post('agent_list');
			$modul_random = $this->get_modul_random();
			$running_modul = $this->ModulRunning(array_keys($modul_random['combo']));
			if($running_modul['running']===true)
			{
				// <pre>Array
					// (
						// [running] => 1
						// [modul_id] => 20
						// [random_id] => 1
					// )
				// </pre>
				foreach( $agent as $index => $agentid ) 
				{
					$this->db->set('lock_agent_status',1);
					$this->db->set('agent_id',$agentid);
					$this->db->set('modul_setup_id',$running_modul['modul_id']);
					if( $this->db->insert('t_gn_agentlock_random') )
					{
						$success++;
					}
					else
					{
						$fail++;
					}
				}
				$cond = array('status'=>1,'message'=>"success( ".$success." ) fail ( ".$fail." ) locked agent");
				
			}
			else
			{
				$cond = array('status'=>0,'message'=>"Random modul is not running");
			}
		}
		return $cond;
	}
	public function setlockbycheck()
	{
		$success = 0;
		$fail = 0;
		$lock = 1;
		$cond = array('status'=>0,'message'=>"");
		if($this->URI->_get_have_post('lock_agent_id'))
		{
			$lock_agent = $this ->URI->_get_array_post('lock_agent_id');
			foreach( $lock_agent as $index => $lock_agent_id ) 
			{
				$this->db->set('lock_agent_status',$lock);
				$this->db->where("lock_random_id", $lock_agent_id);
				$this->db->update("t_gn_agentlock_random");
				if( $this->db->affected_rows() == 1 )
				{
					$success++;
				}
				else
				{
					$fail++;
				}
			}
			$cond = array('status'=>1,'message'=>"success( ".$success." ) fail ( ".$fail." ) lock agent");	
		}
		return $cond;
	}
	public function setunlockbycheck()
	{
		$success = 0;
		$fail = 0;
		$unlock = 0;
		$cond = array('status'=>0,'message'=>"");
		if($this->URI->_get_have_post('lock_agent_id'))
		{
			$lock_agent = $this ->URI->_get_array_post('lock_agent_id');
			foreach( $lock_agent as $index => $lock_agent_id ) 
			{
				$this->db->set('lock_agent_status',$unlock);
				$this->db->where("lock_random_id", $lock_agent_id);
				$this->db->update("t_gn_agentlock_random");
				if( $this->db->affected_rows() == 1 )
				{
					$success++;
				}
				else
				{
					$fail++;
				}
			}
			$cond = array('status'=>1,'message'=>"success( ".$success." ) fail ( ".$fail." ) unlock agent");	
		}
		return $cond;
	}
	
	public function getLockedAgentRandom()
	{
		$agent = array();
		$modul_random = $this->get_modul_random();
		$running_modul = $this->ModulRunning(array_keys($modul_random['combo']));
		//<pre>Array
					// (
						// [running] => 1
						// [modul_id] => 20
						// [random_id] => 1
					// )
				// </pre>
		if($running_modul['running']===true)
		{
			$this->db->reset_select();
			$this->db->select("a.lock_random_id,a.agent_id");
			$this->db->from("t_gn_agentlock_random a");
			$this->db->where_in("a.modul_setup_id ",$running_modul['modul_id']);
			$this->db->where("a.lock_agent_status ","1");
			$qry = $this->db->get();
			if( $qry->num_rows() > 0 ) {
				foreach( $qry->result_assoc() as $rows ) {
					$agent[] = $rows['agent_id'];
				}
			}
		}
		return $agent;
		
	}
	
	/*
	 * @ pack : manage lock data its
	 */	
	 
	 public function _get_select_seconds()
	{
		$seconds = array();
		for( $i = 0; $i<=59; $i++ )
		{
			$time = ( strlen($i)==2 ? "$i" : "0$i");
			$seconds[$time] = $time;
		}
		return $seconds;
	} 
	 
	/*
	 * @ pack : manage lock data its
	 */	
	 
	 
	  public function _get_select_minute()
	 {
		$minute = array();
		for( $i = 0; $i<=59; $i++ )
		{
			$time = ( strlen($i)==2 ? "$i" : "0$i");
			$minute[$time] = $time;
		}
		return $minute;
	 } 
	 
	/*
	 * @ pack : manage lock data its
	 */	
	 
	 
	 public function _get_select_hour($hour=0)
	{
		$hours = array();
		for( $i = $hour; $i<=23; $i++ )
		{
			$time = ( strlen($i)==2 ? "$i" : "0$i");
			$hours[$time] = $time;
		}
		return $hours;
	 }
	 
	public function _stopPanelAcessAll()
	{
		$cond = array('Message'=>0);
		if($this->URI->_get_have_post('st_access_id'))
		{
			$setup = $this ->URI->_get_post('st_access_id');
		
				$this->db->set('bool_running',0);
				$this->db->where("st_access_all_id", $setup);
				$this->db->update("t_st_access_all");
				if( $this->db->affected_rows() == 1 )
				{
					$this->db->reset_select();
					$this->db->query(  "UPDATE t_st_access_all a
										INNER JOIN t_gn_access_all b ON a.st_access_all_id=b.st_access_all_id
										INNER JOIN t_gn_buckettrx_debitur c ON b.bucket_trx_id=c.bucket_trx_id
										INNER JOIN t_gn_debitur d ON c.deb_id=d.deb_id
										SET d.deb_is_access_all = 0
										WHERE a.st_access_all_id = ". $setup );
					$cond = array('Message'=>1);
				}
		}
		return $cond;
	}
	
	public function _stopPanelRound()
	{
		$cond = array('Message'=>0);
		if($this->URI->_get_have_post('st_round_id'))
		{
			$setup = $this ->URI->_get_post('st_round_id');
		
				$this->db->set('bool_running',0);
				$this->db->where("st_round_id", $setup);
				$this->db->update("t_st_round");
				if( $this->db->affected_rows() == 1 )
				{
					$this->db->reset_select();
					$this->db->query(  "UPDATE t_st_round a
										INNER JOIN t_gn_round_assign b ON a.st_round_id=b.st_round_id
										INNER JOIN t_gn_buckettrx_debitur c ON b.bucket_trx_id=c.bucket_trx_id
										INNER JOIN t_gn_debitur d ON c.deb_id=d.deb_id
										SET d.deb_is_access_all = 0
										WHERE a.st_round_id = ". $setup );
					$cond = array('Message'=>1);
				}
		}
		return $cond;
	}
}
//END of Class
?>