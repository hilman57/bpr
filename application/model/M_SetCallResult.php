<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SetCallResult extends EUI_Model
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect('a.CallReasonId');
	$this->EUI_Page->_setFrom('t_lk_account_status a');
	$this->EUI_Page->_setJoin('t_lk_customer_status b', 'a.CallReasonCategoryId=b.CallReasonCategoryId', 'LEFT', TRUE);
	$this->EUI_Page->_setWherein('a.CallWorkProjectId',$this->EUI_Session->_get_session('ProjectId') );
	
	if($this->URI->_get_have_post('keywords'))
	{
	
	  $this->EUI_Page->_setAnd(" ( 
			a.CallReasonCategoryId LIKE '%{$this->URI->_get_post('keywords')}%' 
			OR a.CallReasonLevel LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonCode LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonStatusFlag LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonContactedFlag LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonEvent LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonLater LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonOrder LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonNoNeed LIKE '%{$this->URI->_get_post('keywords')}%' 
			)", FALSE);
	}				
	
	if( $this->EUI_Page->_get_query() ){
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this->EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this->EUI_Page->_setPage(10);
  $this->EUI_Page->_setSelect("
		 a.*,IF(a.CallReasonEvent=0,'No','YES') as triger,
		 IF(a.CallReasonLater=0,'No','YES') as calllater, IF(a.CallReasonNoNeed=0,'No','YES') as notinterest, 
		 b.*, IF(CallReasonStatusFlag=0,'Not Active','Active') as statusResult, c.ProjectName",FALSE);
		 
  $this->EUI_Page->_setFrom("t_lk_account_status a",FALSE);
  $this->EUI_Page->_setJoin("t_lk_customer_status b", "a.CallReasonCategoryId=b.CallReasonCategoryId", "LEFT",FALSE);
  $this->EUI_Page->_setJoin("t_lk_work_project c", "a.CallWorkProjectId=c.ProjectId", "LEFT",TRUE);
  $this->EUI_Page->_setWherein('a.CallWorkProjectId',$this->EUI_Session->_get_session('ProjectId') );
 
  if( $this->URI->_get_have_post('keywords') ) {
	$this->EUI_Page->_setAnd(" ( 
			a.CallReasonCategoryId LIKE '%{$this->URI->_get_post('keywords')}%' 
			OR a.CallReasonLevel LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonCode LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonStatusFlag LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonContactedFlag LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonEvent LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonLater LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonOrder LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.CallReasonNoNeed LIKE '%{$this->URI->_get_post('keywords')}%' 
			)", FALSE);
  }				
		
  if( $this->URI->_get_have_post('order_by'))
  {
	$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
  }
  
  $this -> EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getNotInterest()
{
	$_conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonDesc, a.CallReasonCode", FALSE);
	$this->db->from("t_lk_account_status a ");
	$this->db->join('t_lk_customer_status b', 'a.CallReasonCategoryId=b.CallReasonCategoryId', 'LEFT');
	$this->db->join('t_lk_work_project c', 'a.CallWorkProjectId=c.ProjectId', 'LEFT');
	$this->db->where('a.CallReasonNoNeed',1);
	$this->db->where('b.CallReasonInterest', '0');
	$this->db->where_in('c.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->order_by('a.CallReasonOrder','ASC');
	
	foreach( $this->db->get()->result_assoc() as $rows ) {
		$_conds[$rows['CallReasonCode']] = array('name' => $rows['CallReasonDesc'],'code' => $rows['CallReasonCode']);	
	}
	
	return $_conds;
}

/*
 * @ def 		: _getInterestSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _getInterestSale() 
{
	$_conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonDesc, a.CallReasonCode", FALSE);
	$this->db->from("t_lk_account_status a ");
	$this->db->join('t_lk_customer_status b', 'a.CallReasonCategoryId=b.CallReasonCategoryId', 'LEFT');
	$this->db->join('t_lk_work_project c', 'a.CallWorkProjectId=c.ProjectId', 'LEFT');
	$this->db->where('a.CallReasonEvent', 1);
	$this->db->where_in('c.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->order_by('a.CallReasonOrder','ASC');
	
	$res = $this->db->get();
	if($res) foreach( $res->result_assoc() as $rows ) {
		$_conds[$rows['CallReasonCode']] = array( 'name' => $rows['CallReasonDesc'],  'code' => $rows['CallReasonCode'] );
	}
	
	return $_conds;
 }


 /*
 * @ def 		: _getCallback
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _getBoolean()
{
	$array = array('1'=>'Yes','0'=>'No');
	return $array;
}	

/*
 * @ def 		: _getCallback
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _getCallback()
{
	$_conds = array();
	
	$this->db->select("a.CallReasonId, a.CallReasonDesc, a.CallReasonCode", FALSE);
	$this->db->from("t_lk_account_status a ");
	$this->db->join("t_lk_work_project b "," a.CallWorkProjectId = b.ProjectId",'LEFT');
	$this->db->where("a.CallReasonLater",1);
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$_conds[$rows['CallReasonId']] = array (  'name' => $rows['CallReasonDesc'],  'code' => $rows['CallReasonCode'] );	
	}
	
	return $_conds;
}

/*
 * @ def 		: _getCallReasonId
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getCallReasonId( $CategoryId=NULL )
{
	$_conds = array();
	
	$this->db->select("a.CallReasonCode, a.CallReasonDesc, a.CallReasonCode");
	$this->db->from("t_lk_account_status a");
	$this->db->join("t_lk_customer_status b ","a.CallReasonCategoryId=b.CallReasonCategoryId","LEFT");
	$this->db->join("t_lk_work_project c "," a.CallWorkProjectId = c.ProjectId",'LEFT');
	$this->db->where_in('c.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where('b.CallReasonCategoryFlags',1);
	
	if(!is_null($CategoryId)){
		$this -> db -> where("a.CallReasonCategoryId",$CategoryId);
	}
	
	$this->db->order_by('a.CallReasonOrder','ASC');
	foreach( $this ->db ->get()->result_assoc() as $rows ){
		$_conds[$rows['CallReasonCode']] = array('name' => $rows['CallReasonDesc'],'code' => $rows['CallReasonCode']);	
	}	

	return $_conds;		 
} 

/*
 * @ def 		: _getCallReasonId
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
function _getInboundReasonId()
{
	$_conds = array();
	
	$this->db->select("a.CallReasonId, a.CallReasonDesc, a.CallReasonCode");
	$this->db->from("t_lk_account_status a");
	$this->db->join("t_lk_customer_status b ","a.CallReasonCategoryId=b.CallReasonCategoryId","LEFT");
	$this->db->where("b.CallOutboundGoalsId",1);
	$this->db->join("t_lk_work_project c "," a.CallWorkProjectId = c.ProjectId",'LEFT');
	$this->db->where_in('c.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$_conds[$rows['CallReasonId']] = array  ( 'name' => $rows['CallReasonDesc'], 'code' => $rows['CallReasonCode'] );	
	}	
	return $_conds;		 
} 

/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getEventSale()
{
	$_conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonDesc, a.CallReasonCode", FALSE);
	$this->db->from("t_lk_account_status a");
	$this->db->join("t_lk_work_project b "," a.CallWorkProjectId = b.ProjectId",'LEFT');
	$this->db->where("a.CallReasonEvent",1);
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$_conds[$rows['CallReasonId']] = array ('name' => $rows['CallReasonDesc'], 'code' => $rows['CallReasonCode']);	
	}	
	return $_conds;		 
}
 
/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setActive($data=array())
 {
	$_conds = 0;
	if(is_array($data))
	{
		foreach( $data['CallReasonId'] as $keys => $CallReasonId )
		{
			if( $this -> db ->update('t_lk_account_status', 
				array('CallReasonStatusFlag'=> $data['Active']), 
				array('CallReasonId' => $CallReasonId)
			))
			{
				$_conds++;
			}	
		}
	}	
	return $_conds;
 }
 
/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setSaveCallResult( $post=array() )
 {
	$_conds = 0;
	if( $this -> db->insert('t_lk_account_status',$post) ) 
	{
		$_conds++;
	}
	return $_conds;
 }
 
/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setDeleteCallResult($_post)
 {
	$_conds = 0;
	foreach($_post as $a => $PK )
	{
		if( $this -> db ->delete('t_lk_account_status', 
			array('CallReasonId' => $PK) 
		)) 
		{
			$_conds++;
		}
	}
	
	return $_conds; 
 }
 
/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _getDataResult( $ResultId=0 )
{  
	$_conds = array();
	
	$this->db->select('*');
	$this->db->from('t_lk_account_status a');
	$this->db->join("t_lk_work_project b "," a.CallWorkProjectId = b.ProjectId",'LEFT');
	$this->db->where('a.CallReasonId',$ResultId);
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	if( $rows = $this -> db -> get() -> result_first_assoc())
	{
		$_conds = $rows;
	}
	
	return $_conds;
}

/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _setUpdateCallResult($_post = array() )
{
	$_conds = 0; $update = array(); $where = array();
	foreach( $_post as $fields => $values )
	{
		if(($fields!='CallReasonId'))
			$update[$fields] = $values;
		else
			$where[$fields] = $values;
	}
	
	if( $this -> db -> update('t_lk_account_status',$update,$where)){
		$_conds++;	
	}
	
	return $_conds;
}

/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getEventType($CallReasonId)
{
	$this->db->select('a.CallReasonEvent, a.CallReasonLater');
	$this->db->from('t_lk_account_status a');
	$this->db->join("t_lk_work_project b "," a.CallWorkProjectId = b.ProjectId",'LEFT');
	$this->db->where('a.CallReasonStatusFlag',1);
	$this->db->where('a.CallReasonId', $CallReasonId);
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	if( $rows = $this -> db->get()->result_first_assoc()){
		return $rows;
	}
	else
		return null;

}

public function _getCallOpenForm()
{
	$form_opens = array(); 
	$this->db->select('a.CallReasonId');
	$this->db->from('t_lk_account_status a ');
	$this->db->where('a.CallFormOpen',1);
	foreach( $this->db->get() -> result_assoc() as $rows )
	{
		$form_opens[$rows['CallReasonId']] = $rows['CallReasonId'];	
	}
	
	return $form_opens;
}


/*
 * @ def 		: _getPendingInfo
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getPendingInfo()
{
	$_conds = array();
	
	$this->db->select('a.CallReasonId, a.CallReasonDesc');
	$this->db->from('t_lk_account_status a');
	$this->db->join("t_lk_work_project b "," a.CallWorkProjectId = b.ProjectId",'LEFT');
	$this->db->where('a.CallReasonStatusFlag',1);
	$this->db->where('a.CallReasonPendingQA', 1);
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	foreach( $this->db->get()->result_assoc() as $rows ) {
		$_conds[$rows['CallReasonId']]= $rows['CallReasonDesc'];
	}	
	
	return $_conds;
 }
 
 
/*
 * @ def 		: _getPendingInfo
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getRealInterest()
 {
	$Int = $this->_getInterestSale();
	$Pen = $this->_getPendingInfo();
	
	$process = array();
	foreach( $Int as $k => $v ){
		if(!in_array($k,array_keys($Pen) )){
			$process[$k] = $v;
		}
	}
	
	return $process;
	
 }


function _getDefaultCompleteChecked()
{
	$array_cat = array(1,6,7);
	$_conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonDesc, a.CallReasonCode", FALSE);
	$this->db->from("t_lk_account_status a");
	$this->db->join("t_lk_work_project b "," a.CallWorkProjectId = b.ProjectId",'LEFT');
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->where_in("a.CallReasonCategoryId", $array_cat);
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$_conds[$rows['CallReasonId']] = array ('name' => $rows['CallReasonDesc'], 'code' => $rows['CallReasonCode']);	
	}	
	return $_conds;		 
}

/** 
 * @ def  : get intereset by CallReason Event on t_lk_account_status 
 * @ date : 20141204
 * @ auth : omens 
**/

 public function _getCallReasonEvent()
{
	static $config = array();
	
// null of select string 
	$this->db->reset_select(); 
	
// real query string 
	
	$this->db->select('a.CallReasonId, a.CallReasonCode');
	$this->db->from('t_lk_account_status a');
	$this->db->where_in('a.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in('a.CallReasonEvent', array(1));
	$recsource = $this->db->get();
	
	if( $recsource->num_rows() > 0 )
	{
		foreach( $recsource->result_assoc() as $rows )
		{
			$config[$rows['CallReasonId']] = $rows['CallReasonCode'];
		}
	}
 return $config;
 
}

	/** 
	* @ def  : get complain by CallReason Event on t_lk_account_status 
	* @ date : 20160915
	**/

	public function _getCallReasonComplain()
	{
	static $config = array();
	
	// null of select string 
		$this->db->reset_select(); 
		
	// real query string 
		
		$this->db->select('a.CallReasonId, a.CallReasonCode,a.CallReasonDesc');
		$this->db->from('t_lk_account_status a');
		$this->db->where_in('a.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this->db->where_in('a.CallReasonComplain', 1);
		$recsource = $this->db->get();
		
		if( $recsource->num_rows() > 0 )
		{
			foreach( $recsource->result_assoc() as $rows )
			{
				$config[$rows['CallReasonCode']] = $rows['CallReasonDesc'];
			}
		}
	 return $config;
	}


}
?>