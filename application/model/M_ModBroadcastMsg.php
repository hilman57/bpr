<?php
/*
 * E.U.I 
 *
 
 * subject	: M_ModBroadcascollg modul 
 * 			  extends under EUI_Model class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
class M_ModBroadcastMsg extends EUI_Model {

/*
 * EUI :: M_ModBroadcascollg // construtor() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 

 function M_ModBroadcastMsg(){
	parent::__construct();
 }
 
/*
 * @ def 		: _getAllUser() 
 * -----------------------------------------
 *
 * @ return		: array() 
 * @ param		: none;
 */	
 
 function _getAllUser()
 {
	$sql = null; $_conds = array();
	
	// level : root 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_ROOT)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_in("user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
	}
	
	// level : admin
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_ADMIN)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
	}

	// quality HEAD
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_QUALITY_HEAD)
	{
	
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
	}		
	
	// quality STAFF
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_QUALITY_STAFF)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
	}	

	// level : manager 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_MANAGER)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where('a.mgr_id',$this -> EUI_Session ->_get_session('UserId'));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
	}	

		// level : manager 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_ACCOUNT_MANAGER)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where('a.act_mgr',$this -> EUI_Session ->_get_session('UserId'));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
	}			
			
				
	// level : supervisor
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_SUPERVISOR)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_in('a.handling_type', array(USER_AGENT_INBOUND, USER_QUALITY_STAFF, USER_AGENT_OUTBOUND,USER_SENIOR_TL,USER_LEADER,USER_SUPERVISOR));
		$this -> db -> where(" ( a.spv_id = '". $this -> EUI_Session ->_get_session('UserId')."' OR a.spv_id = 0 ) ",'', FALSE);
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		// echo "<pre>".$this->db->_getCompiler()."</pre>";
		
	}	
		// level : leader
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_LEADER)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where('a.tl_id',$this -> EUI_Session ->_get_session('UserId'));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
	}	
	
	// level : Senior leader
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_SENIOR_TL)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where('a.stl_id',$this -> EUI_Session ->_get_session('UserId'));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_not_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
		
		
	}	
	// level : Telemarketing 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_AGENT_INBOUND ){
		
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
	}			
	
	// level : Quality Insurance ( QA ) 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')== USER_QUALITY)
	{
		$this -> db -> select("a.*");
		$this -> db -> from("t_tx_agent a");
		$this -> db -> join("t_gn_user_project_work b ","a.UserId=b.UserId","LEFT");
		$this -> db -> where_not_in('a.handling_type', array(USER_ROOT));
		$this -> db -> where_in("a.user_state", array(1));
		// $this -> db -> where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		$this -> db -> where_in('a.UserId',$this -> EUI_Session ->_get_session('UserId'));
		
		// $this -> db -> select("*");
		// $this -> db -> from("t_tx_agent");
		// $this -> db -> where(array("user_state"=> 1) );
		// $this -> db -> where_not_in('handling_type', array(USER_ROOT));
	}	

	// echo $this->db->_get_var_dump();
	$qry = $this -> db -> get();
	if( !is_null($qry))
	{
		foreach( $qry -> result_assoc()  as $rows ) {
			$_conds[$rows['UserId']] = array
			(
				'name' => $rows['full_name'],
				'code' => $rows['id'] 
			);
		}
	}	
	
	return $_conds;
	
 }
 
 
/*
 * @ def 		: _getUserOnline() 
 * -----------------------------------------
 *
 * @ return		: array() 
 * @ param		: none;
 */	
 
 
 function _getUserOnline()
 {
	$sql = null; $_conds = array();
	
	// level : root 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_ROOT)
		$sql = "SELECT * FROM t_tx_agent a where a.user_state=1 AND logged_state = 1";
	
	// level : admin
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_ADMIN)
		$sql = "SELECT * FROM t_tx_agent a where a.user_state=1 AND logged_state = 1
				a.handling_type NOT IN('" . USER_ROOT ."') ";
	
	// level : manager 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_MANAGER)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1  
				AND a.mgr_id ='". $this -> EUI_Session ->_get_session('UserId') ."' 
				AND a.handling_type='".USER_TELESALES."' AND logged_state = 1";
				
	// level : supervisor
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_SUPERVISOR)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1  
				AND a.spv_id ='". $this -> EUI_Session ->_get_session('UserId') ."' 
				AND a.handling_type='".USER_TELESALES."' AND logged_state = 1";
	
	// level : leader 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_LEADER)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1  
				AND a.tl_id='".$this->EUI_Session->_get_session('UserId') ."' 
				AND a.handling_type='".USER_TELESALES."' AND logged_state = 1";
	
	//senior leader
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_SENIOR_TL)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1  
				AND a.stl_id='".$this->EUI_Session->_get_session('UserId') ."' 
				AND a.handling_type='".USER_TELESALES."' AND logged_state = 1";
	
	// level : Telemarketing 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_TELESALES )
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1 
				AND a.handling_type='".USER_TELESALES."' AND logged_state = 1";
	
	// level : Quality Insurance ( QA ) 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_QUALITY)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1 AND logged_state = 1";
		// echo $sql;
	// execute 
	
	if( !is_null( $sql) )
	{
		$qry = $this -> db -> query($sql);
		foreach( $qry -> result_assoc() as $rows )
		{
			$_conds[$rows['UserId']] = array
			(
				'name' => $rows['full_name'],
				'code' => $rows['id'] 
			);
		}
	}	
	
	return $_conds;
 }
 
 
/*
 * @ def 		: _getUserOffline() 
 * -----------------------------------------
 *
 * @ return		: array() 
 * @ param		: none;
 */	
 
 function _getUserOffline()
 {
	$sql = null; $_conds = array();
	
	// level : root 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_ROOT)
		$sql = "SELECT * FROM t_tx_agent a where a.user_state=1 AND logged_state=0";
	
	// level : admin
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_ADMIN)
		$sql = "SELECT * FROM t_tx_agent a where a.user_state=1 AND logged_state=0
				a.handling_type NOT IN('" . USER_ROOT ."') ";
		
	
	// level : manager 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_MANAGER)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1  
				AND a.mgr_id ='". $this -> EUI_Session ->_get_session('UserId') ."' 
				AND a.handling_type='".USER_TELESALES."' AND logged_state=0";
				
	// level : supervisor
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_SUPERVISOR)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1  
				AND a.spv_id ='". $this -> EUI_Session ->_get_session('UserId') ."' 
				AND a.handling_type='".USER_TELESALES."' AND logged_state =0";
	
	// level : Telemarketing 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_TELESALES )
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1 
				AND a.handling_type='".USER_TELESALES."' AND logged_state =0";
	
	// level : Quality Insurance ( QA ) 
	
	if( $this -> EUI_Session -> _have_get_session('HandlingType')== USER_QUALITY)
		$sql = "SELECT * FROM t_tx_agent a WHERE a.user_state=1 AND logged_state =0";
		
	// execute 
	
	if( !is_null( $sql) )
	{
		$qry = $this -> db -> query($sql);
		foreach( $qry -> result_assoc() as $rows )
		{
			$_conds[$rows['UserId']] = array
			(
				'name' => $rows['full_name'],
				'code' => $rows['id'] 
			);
		}
	}	
	
	return $_conds;
 }
 
/*
 * @ def 		: _setSendUserOnline() 
 * -----------------------------------------
 *
 * @ return		: array() 
 * @ param		: none;
 */	
 
 function _setSendUserOnline( $data=null )
 {
	$totals = 0;
	if(!is_null($data) && isset($data['Users']) )
	{
		foreach( $data['Users'] as $keys => $UserId )
		{
			$_Insert['`from`'] = $this -> EUI_Session -> _get_session('UserId'); 
			$_Insert['`message`'] = $data['Message']; 
			$_Insert['`sent`'] = date('Y-m-d H:i:s');
			$_Insert['`to`'] = $UserId; 
			$_Insert['`recd`'] = 0;
			
			if( $this -> db -> insert( 't_tx_agent_msgbox', $_Insert ))
			{
				$totals++;
			}		
		}			
	}
	
	return $totals;
	
 }
  
/*
 * @ def 		: _setSendUserOffline() 
 * -----------------------------------------
 *
 * @ return		: array() 
 * @ param		: none;
 */	
 
 function _setSendUserOffline( $data=null )
 {
	$totals = 0;
	if(!is_null($data) && isset($data['Users']) )
	{
		foreach( $data['Users'] as $keys => $UserId )
		{
			$_Insert['`from`'] = $this -> EUI_Session -> _get_session('UserId'); 
			$_Insert['`message`'] = $data['Message']; 
			$_Insert['`sent`'] = date('Y-m-d H:i:s');
			$_Insert['`to`'] = $UserId; 
			$_Insert['`recd`'] = 0;
			
			if( $this -> db -> insert( 't_tx_agent_msgbox', $_Insert ))
			{
				$totals++;
			}		
		}			
	}
	
	return $totals;
	
 }
 
/*
 * @ def 		: _setSendUserAll() 
 * -----------------------------------------
 *
 * @ return		: array() 
 * @ param		: none;
 */	
 function _setSendUserAll( $data=null )
 {
	$totals = 0;
	if(!is_null($data) && isset($data['Users']) )
	{
		foreach( $data['Users'] as $keys => $UserId )
		{
			$_Insert['`from`'] = $this -> EUI_Session -> _get_session('UserId'); 
			$_Insert['`message`'] = $data['Message']; 
			$_Insert['`sent`'] = date('Y-m-d H:i:s');
			$_Insert['`to`'] = $UserId; 
			$_Insert['`recd`'] = 0;
			
			if( $this -> db -> insert( 't_tx_agent_msgbox', $_Insert ))
			{
				$totals++;
			}		
		}			
	}
	
	return $totals;
	
 }
 
 
  
 /*
 * @ def	: function get detail content list page
 * ---------------------------------------------------
 * 
 * @ return : array()
 * @ param	: not assign parameter
 */	
 
 function _setUpdateAll($UserId=null)
 {
	$conds = array('success'=>0);
	
	$this -> db -> set('recd',1);
	$this -> db ->where('to', $UserId);
	if( $this -> db -> update('t_tx_agent_msgbox')){
		$conds = array('success'=>1);
	}
	
	return $conds;
 }
 
 /*
 * @ def	: function get detail content list page
 * ---------------------------------------------------
 * 
 * @ return : array()
 * @ param	: not assign parameter
 */	
 
function _setUpdateMessage($messageid=null)
{
	$conds = array('success'=>0);
	
	$this -> db -> set('recd',1);
	$this -> db ->where('id',$messageid);
	if( $this -> db -> update('t_tx_agent_msgbox')){
		$conds = array('success'=>1);
	}
	
	return $conds;
} 
 

 
}
?>