<?php
class M_CallOutboundReport extends EUI_Model
{

var $config = null;
var $reference = null;
var $param  = array();
var $completed =array(); 

/** constructor of class model **/

public function M_CallOutboundReport()
{
 
 static $reference = array();
 
 $this->config['major_dc'] 		 = array('all'=> 'All DC','agency_dc' => 'Agency', 'bancassurance' => 'Bancassurance');
 $this->config['second_payment'] = array('yes'=> 'Yes','no' => 'No' );
 $this->config['major_produk'] 	 = array( "ul" => 'Unitlink', "non" => 'Non Unitlink');
 
 $this->config['produk'] 		 = array();
 $this->config['user_create'] 	 = array();
 $this->config['dc_name'] 		 = array();
 $this->config['campaign'] 		 = array();
 $this->config['category'] 		 = array();
 $this->config['payment_mode']   = array();
 
/** auto reference :: this fuck reference ***/
 
 $this->reference['major_dc']['agency_dc'] = array('10'=>'10','11'=>'11','12'=>'12','17'=>'17','18'=>'18', '19'=>'19','41'=>'41','43'=>'43','71'=>'71','81'=>'81','82'=>'82','83' =>'83' );
 $this->reference['major_dc']['bancassurance'] = array ('20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','26'=>'26','27'=>'27','28'=>'28','29'=>'29','61'=>'61','63'=>'63','64'=>'64','66'=>'66','67'=>'67','68'=>'68','69'=>'69' );
 $this->reference['major_dc']['all'] = array ('10'=>'10','11'=>'11','12'=>'12','17'=>'17','18'=>'18', '19'=>'19','41'=>'41','43'=>'43','71'=>'71','81'=>'81','82'=>'82','83' =>'83','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24', '26'=>'26','27'=>'27','28'=>'28','29'=>'29','61'=>'61','63'=>'63','64'=>'64','66'=>'66','67'=>'67','68'=>'68', '69'=>'69' );
 
/** auto content and label ***/
 
 /**
   new req :
   ------------------------- >
   No	
   Policy_Number	
   Policy_Holder_Name	
   RCD	
   Paid_to_Date	
   Branch_Name	
   Product_Code	
   Major_Product	
   
   
   Payment_Frequency	
   Payment_Method
 **/
 
 $this->_content   = array();								
 $this->label_key  = array(
		'policy_number' 	=> array('label'=>'Policy Number.', 'align' => 'left', 'fn' => FALSE),
		'payer' 			=> array('label'=>'Policy Holder Name.', 'align' => 'left', 'fn' => FALSE),
		'RCD' 				=> array('label'=>'RCD.', 'align' => 'left', 'fn' => 'PolicyIssDate'),
		'Paid_to_Date'		=> array('label'=>'Paid to Date.', 'align' => 'left', 'fn' => 'PolicyIssDate'),
		'nama_cbg'			=> array('label'=>'Branch Name.', 'align' => 'left', 'fn' => FALSE),	
		'Product_Code'		=> array('label'=>'Product Kode.', 'align' => 'left', 'fn' => FALSE),
		'Major_Product'		=> array('label'=>'Major Produk','align' => 'left', 'fn' => FALSE),
		'nama_produk' 		=> array('label'=>'Nama Produk','align' => 'left', 'fn' => FALSE),
		'ccy' 				=> array('label'=>'Currency','align' => 'left', 'fn' => FALSE),
		'premi' 			=> array('label'=>'Premi','align' => 'right', 'fn' => 'PolicyPremi'),
		'mode_payment'		=> array('label'=>'Payment Frequency','align' => 'center', 'fn' => 'PaymentMode'),
		'payment' 			=> array('label'=>'Payment','align' => 'left', 'fn' => FALSE)
	);
	
	
// set session start from filter
 
	$this->_setSessionDisplay();
}


/**
 * @ def  : set Display Params to object 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
private function _setSessionDisplay()
{
	$this->ReferenceMajorDC= NULL;
	
	if( $this->EUI_Session->_have_get_session('major_dc') )
	{
		$this->ReferenceMajorDC =  $this->reference['major_dc'][$this->EUI_Session->_get_session('major_dc')];
	}	
}

 
/**
 * @ def  : set Display Params to object 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */

public function getRptMonth()
{
	$arr=array();
	$i=1;

	$result = mysql_query("select 
		date(now()) d12,
		date(now()-interval 1 month) d11,
		date(now()-interval 2 month) d10,
		date(now()-interval 3 month) d9,
		date(now()-interval 4 month) d8,
		date(now()-interval 5 month) d7,
		date(now()-interval 6 month) d6,
		date(now()-interval 7 month) d5,
		date(now()-interval 8 month) d4,
		date(now()-interval 9 month) d3,
		date(now()-interval 10 month) d2,
		date(now()-interval 11 month) d1

");

	$arr=array();
	$i=1;
	while ($row = mysql_fetch_array($result)) {
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		$arr[$row["d".$i]]=date("M",strtotime($row["d".$i]))." - ".date("Y",strtotime($row["d".$i])); $i++;
		// echo "<tr><td>$row[id]</td><td>$row[full_name]</td></tr>";
	}

	return $arr;
}
 
public function setDisplayParams( $param )
{
	if( is_array($param) )
	{
		$this->param = $param;
	}
}
 
/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
public function getMajorDC()
 {
	static $config= array();
	
	$config=& $this->config['major_dc'];
	
	if( $config )
	{
		return $config;
	}
	
 }
 

/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
 
 public function getMajorProduct()
 {
	static $config= array();
	
	$config=& $this->config['major_produk'];
	
	if( $config )
	{
		return $config;
	}
 }
 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
 
public function getProduct()
 {
	static $config= array();
	
	$config=& $this->config['produk'];
	$this->db->select('a.PolicyProductName, a.PolicyProductName');
	$this->db->from('t_gn_policy_detail a');
	$this->db->group_by('a.PolicyProductName');
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$config[$rows['PolicyProductName']] = $rows['PolicyProductName'];
	}
	return $config;
	
 } 
 
 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */

public function getUserCreate()
 {
	static $config= array();
	$config=& $this->config['user_create'];
	
	$this->load->model('M_SysUser');
	
/** user leave **/

	$SysUser  =& $this->M_SysUser->_getUserRegistration(1);
	$UserWork =& $this->getUserWorkProject();
	
	foreach( $SysUser as $UserId => $Username )
	{
		if(in_array($UserId, array_keys($UserWork)) )
		{
			$config[$UserId] = $Username;
		}	
	}
	
	return $config;
	
 } 

/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
protected function getUserWorkProject()
 {
	static $users = array();
	
	$this->db->select('a.UserId');
	$this->db->from('t_gn_user_project_work a');
	$this->db->join('t_tx_agent b','a.UserId=b.UserId');
	$this->db->join('t_gn_agent_profile c','b.profile_id=c.id');
	$this->db->where_in('c.id', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	$this->db->where_in('a.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$users[$rows['UserId']] = $rows['UserId'];
	}
	
	return $users;
	
 }
 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
public function getSecondPayment()
{
	static $config;
	
	$config =& $this->config['second_payment'];
	if( $config )
	{
		return $config;
	}	
	 
} 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
public function getCampaignName()
{
	static $config;
	
	$config =& $this->config['campaign'];
	
	$this->db->select('a.CampaignCode, a.CampaignName');
	$this->db->from('t_gn_campaign a ');
	$this->db->join('t_gn_campaign_project b ',' a.CampaignId=b.CampaignId','LEFT');
	$this->db->where_in('b.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->group_by('a.CampaignCode');
	foreach( $this->db->get()->result_assoc() as $rows ) {
		$config[$rows['CampaignCode']] = $rows['CampaignName'];
	}
	
	if( $config )
	{
		return $config;
	}	
	 
} 
 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
 
public function getCategory()
{
	static $config;
	
	$config =& $this->config['category'];
	
	$this->db->select("a.PolicyProductCategory");
	$this->db->from("t_gn_policy_detail a  ");
	$this->db->group_by("a.PolicyProductCategory");
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{
		$config[$rows['PolicyProductCategory']] = $rows['PolicyProductCategory'];
	}
	
	if( $config ) {
		return $config;
	}	
	 
} 
 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
public function getDCName()
{
	static $config;
	$config =& $this->config['dc_name'];
	
	$this->db->select("c.Nama_Dc");
	$this->db->from("t_gn_debitur a ");
	$this->db->join("t_gn_cif_customer b "," a.CustomerNumber=b.EnigmaId", "LEFT");
	$this->db->join("t_gn_bucket_customers c "," b.BucketId=c.BucketId","LEFT");
	$this->db->group_by("c.Nama_Dc");
	
	foreach( $this->db-> get()->result_assoc() as $rows )
	{
		if( $keys = explode(" ", $rows['Nama_Dc']) ){
			$config[$keys[0]] = $keys[0];
		}		
	}
	
	
	if( $config )
	{
		return $config;
	}
	
	 
} 

/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
public function getPaymentMode()
{
	static $config;
	
	$config =& $this->config['payment_mode'];
	$this->db->select('*');
	$this->db->from('t_lk_paymode');
	foreach( $this->db->get()->result_assoc() as $rows ) {
		$config[$rows['PayModeId']] = $rows['PayMode'];
	}
	
	if( $config )
	{
		return $config;
	}	
	 
} 

 /**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
public function DetailByParents()
{
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join("t_gn_debitur g "," a.EnigmaId=g.CustomerNumber");
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	/** filter get have filter keywords ***/
	
	if( isset($this->param['status_code']) 
		AND $this->param['status_code']!='') 
	{
	    if(trim($this->param['status_code'])=='IS_NULL') {
			$this->db->where('f.CallReasonCode IS NULL');
		}	
		else if(trim($this->param['status_code'])=='IS_NOT_NULL') {
			$this->db->where('f.CallReasonCode IS NOT NULL');
		}	
		else {
			$this->db->where_in('f.CallReasonCode', array($this->param['status_code']));
		}	
	}
	
 /* cek completed status ***/
 
	if( isset($this->param ['complete']) ){
		$this->db->where_in('a.CallComplete', array($this->param['complete']) );
	}
	
  /* filtering **/
  
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyCreatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyCreatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	//echo $this->db->_get_var_dump();
	
	
  /** result of content  ***/
  
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	
	
	
	return array( 
		'field' 	=> $this->label_key, 
		'content' 	=> $this->_content,
		'title' 	=> $this->param 
	);
}
 
/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function DisplayDetailByAtempt()
{
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->where('a.CallReasonId IS NOT NULL','', FALSE);
	if( isset($this->param['CallAtempt'])  )
	{
		if( (INT)$this->param['CallAtempt'] < 10 ){
			$this->db->where("g.CallAttempt",$this->param['CallAtempt'],FALSE);	
		}
		else
		{
			$this->db->where("g.CallAttempt >=",$this->param['CallAtempt'], FALSE);	
		}
	}
	
/* filtering **/
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	//echo $this->db->_get_var_dump();
	
	/**result of content  ***/
    
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
public function DisplayCallAtempDetail()
{
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->where('a.CallReasonId IS NOT NULL','', FALSE);
	$this->db->where("g.CallAttempt >", 0, FALSE);
	
 /* filtering **/
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	

/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	/**result of content  ***/
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DisplayContactDetail()
{
	$is_ptp =& $this->CallReasonIdByCode(array('29001'));
	$contact =& $this->CallReasonIdByCategoryCode(array('B002','B003'));
	
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		d.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		d.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join("t_gn_debitur b","a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c","b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d","c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_gn_campaign e","b.CampaignId=e.CampaignId","LEFT");
	$this->db->join("t_gn_campaign_project f","e.CampaignId=f.CampaignId","LEFT");
	$this->db->join("t_lk_account_status g","a.CallReasonId=g.CallReasonId","LEFT");
	$this->db->where_in('f.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	if(is_array($contact) ){
		$this->db->where_in('g.CallReasonId', array_keys($contact));
	}
	
    /* filtering **/
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('d.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("d.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	/**result of content  ***/
	//echo $this->db->_get_var_dump();
   
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	// return to view screen 
	
	return array(
		'field' 	=> $this->label_key, 
		'content' 	=> $this->_content,
		'title' 	=> $this->param 
	);
}
 
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DisplayNotPtpDetail()
{
	$is_not_ptp =& $this->CallReasonIdByCategoryCode(array('B003'));
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	if( is_array($is_not_ptp) )
	{
		$this->db->where_in('f.CallReasonId', array_keys($is_not_ptp));
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	

/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	
/**result of content  ***/
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	// return to view screen 
	
	return array(
		'field' 	=> $this->label_key, 
		'content' 	=> $this->_content,
		'title' 	=> $this->param 
	);
}
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DisplayPtpDetail()
{
	$is_ptp =& $this->CallReasonIdByCategoryCode(array('B002'));
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	
	if( is_array($is_ptp ) )
	{
		$this->db->where_in('f.CallReasonId', array_keys($is_ptp ));
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	

/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
		
	
/**result of content  ***/
// echo $this->db->_get_var_dump();
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	// return to view screen 
	
	return array(
		'field' 	=> $this->label_key, 
		'content' 	=> $this->_content,
		'title' 	=> $this->param 
	);
}

/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */

public function DisplayDetailReturnList()
{
$ReturnList =& $this->CallReasonIdByCategoryCode(array('B005'));
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	
	if( is_array($ReturnList ) )
	{
		$this->db->where_in('f.CallReasonId', array_keys($ReturnList ));
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}	
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
/**result of content  ***/
//echo $this->db->_get_var_dump();
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	// return to view screen 
	
	return array(
		'field' 	=> $this->label_key, 
		'content' 	=> $this->_content,
		'title' 	=> $this->param 
	);
}

/**
 * @ def  : ambil detail data untuk Status contact
	( Cust Promise to Pay + Cust Not Promise to Pay ) dengan 
	atempt yang di tentukan .
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */

 
public function DisplayDetailByAtemptAndContact()
{
	$is_ptp =& $this->CallReasonIdByCode(array('29001'));
	$is_not_ptp =& $this->CallReasonIdByCategoryCode(array('B002','B003'));
	
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	
 // get status contact only 
 
	if( is_array($is_ptp) AND is_array($is_not_ptp) )
	{
		$this->db->where_in('f.CallReasonId', array_keys($is_ptp), FALSE);
		$this->db->where_in('f.CallReasonId', array_keys($is_not_ptp), FALSE);
	}
	
 // x - atempt call 
 
	if( isset($this->param['CallAtempt'])  )
	{
		if( (INT)$this->param['CallAtempt'] < 10 ){
			$this->db->where("g.CallAttempt",$this->param['CallAtempt'],FALSE);	
		}
		else
		{
			$this->db->where("g.CallAttempt >=",$this->param['CallAtempt'], FALSE);	
		}
	}

	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}	
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	/**result of content  ***/
    
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}

/**
 * @ def  : ambil detail data untuk Status contact
 *  (Cust Promise to Pay + Cust Not Promise to Pay) 
 *  dengan semua atempt  .
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
public function DisplayDetailAtemptAndContact()
{
	$is_ptp =& $this->CallReasonIdByCode(array('29001'));
	$is_not_ptp =& $this->CallReasonIdByCategoryCode(array('B002','B003'));
	
	$this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->where("g.CallAttempt >=",0, FALSE);
	
 // get status contact only 
 
	if( is_array($is_ptp) AND is_array($is_not_ptp) )
	{
		$this->db->where_in('f.CallReasonId', array_keys($is_ptp), FALSE);
		$this->db->where_in('f.CallReasonId', array_keys($is_not_ptp), FALSE);
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
	
	return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}

/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DetailOptionCompleted()
{
  static $config  = array();
  
  $config=& $this->CallReasonIdByCategoryCode(array('B004'));
  $this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
	if( $this->param['complete']!='NULL'){
		$this->db->where("a.CallComplete", $this->param['complete']);
	}
	else{
		$this->db->where_in("a.CallComplete", array('0','1') );
	}
	
 // get status contact only 
 
	if( is_array($config) ){
		$this->db->where_in('f.CallReasonId', array_keys($config), FALSE);
	}
	
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	
//echo $this->db->_get_var_dump();
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
  return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}
 
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DetailDeadTone()
{
  static $DeadTone  = array();
  $DeadTone =& $this->CallReasonIdByCategoryCode(array('B001'));
  $this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
 // get status contact only 
 
	if( is_array($DeadTone) ){
		$this->db->where_in('f.CallReasonId', array_keys($DeadTone), FALSE);
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
  return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */ 
 
public function DetailUnconnected()
{
	
  $Contacted =& $this->CallReasonIdByCategoryCode(array('B002','B003'));
  $Uncontacted =& $this->CallReasonIdByCategoryCode(array('B004'));
  $this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
 // get status contact only 
 
	if( is_array($Contacted) 
		AND is_array($Uncontacted) 
		AND (COUNT($Contacted)>0) )
	{
		$this->db->where_not_in('f.CallReasonId', $Contacted, FALSE);
		$this->db->where_not_in('f.CallReasonId', $Uncontacted, FALSE);
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	
	//echo $this->db->_get_var_dump();
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
  return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}

/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DetailConnected()
{
  static $DeadTone  = array();
  
  $Contacted 	=& $this->CallReasonIdByCategoryCode(array('B002','B003'));
  $Uncontacted 	=& $this->CallReasonIdByCategoryCode(array('B004'));
  
  
  $this->db->select(" 
		DISTINCT a.EnigmaId, 
		upper(a.PolicyProductName) as nama_produk,  
		upper(a.PolicyFirstName) as payer,
		a.PolicyNumber as policy_number,
		a.PolicyPremi as premi,
		a.PolicyCurrency as ccy,
		a.PolicyIssDate as issued_date,
		a.PolicyDOB as dob,
		e.Branch_Name as nama_cbg,
		'&nbsp;' as reason,
		e.Payment_Type as payment,
		'&nbsp;' as activity_status,
		a.PaymentMode as mode_payment,
		a.PolicyRCD AS RCD,
		a.PolicyPTD AS Paid_to_Date,
		a.PolicyMajorProduct AS Major_Product,
		a.PolicyProductCode AS Product_Code,
		'&nbsp;' as second_payment", FALSE 
	);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur g ','a.EnigmaId=g.CustomerNumber','LEFT');
	$this->db->join("t_gn_cif_customer lg ","g.CustomerNumber=lg.EnigmaId");
	$this->db->join('t_gn_campaign b ','g.CampaignId=b.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->join("t_gn_bucket_customers e ","lg.BucketId=e.BucketId","LEFT");
	$this->db->join("t_lk_account_status f ","a.CallReasonId= f.CallReasonId","LEFT");
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
 // get status contact only 
 
	if( is_array($Contacted) 
		AND is_array($Uncontacted) )
	{
		$this->db->where(" ( f.CallReasonId IN(". implode(',',array_keys($Contacted))." ) 
			OR ( f.CallReasonId IN(". implode(',',array_keys($Uncontacted))." ) 
				 AND a.CallComplete=1 )) ", NULL, FALSE);
	}
	
  /* filtering **/
	
	if( $this->URI->_get_have_post('interval')){
		
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')>='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
		$this->db->where("DATE_FORMAT(a.PolicyUpdatedTs,'%Y-%m')<='" . $this->URI->_get_post('interval') . "'", NULL, FALSE); 
	}
	
/** if set major dc ***/	

	if( $this->EUI_Session->_have_get_session('major_dc') 
		AND ($this->EUI_Session->_get_session('major_dc')!='all') )
	{
		$this->db->where_in('e.DC_Code',$this->M_CallOutboundReport->ReferenceMajorDC);
	}	
	
/** if set major produk ***/

	if( $this->EUI_Session->_have_get_session('major_produk')){
		if( $this->EUI_Session->_get_session('major_produk')=='non')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
		
		if( $this->EUI_Session->_get_session('major_produk')=='ul')
			$this->db->like('a.PolicyMajorProduct', $this->EUI_Session->_get_session('major_produk'));
	}
	
/** nama produk **/

	if( $this->EUI_Session->_have_get_session('name_produk')
		AND trim($this->EUI_Session->_get_session('name_produk'))!=''
		AND trim($this->EUI_Session->_get_session('name_produk'))!='all') 
	{
		$this->db->where("a.PolicyProductName",$this->EUI_Session->_get_session('name_produk'));
	}	
	
/** created_by **/
	
	if( $this->EUI_Session->_have_get_session('created_by') 
		AND $this->EUI_Session->_get_session('created_by')!='' ) 
	{
		$this->db->where("a.CreateByUserId",$this->EUI_Session->_get_session('created_by'));
	}
	
/** second_payment***/

	if( $this->EUI_Session->_have_get_session('second_payment') 
		AND (trim($this->EUI_Session->_get_session('second_payment'))!='') )
	{
		if( $this->EUI_Session->_get_session('second_payment') =='yes')
		{
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		
		if( $this->EUI_Session->_get_session('second_payment') =='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
	
/** nama DC **/
	
	if( $this->EUI_Session->_have_get_session('nama_dc')  AND $this->EUI_Session->_get_session('nama_dc')!=''
		AND ($this->EUI_Session->_get_session('nama_dc')!='all') )
	{
		$this->db->like("e.Nama_Dc",trim($this->EUI_Session->_get_session('nama_dc')));
	}
	
/** category **/

	if( $this->EUI_Session->_have_get_session('kategory') 
		AND (trim($this->EUI_Session->_get_session('kategory'))!='')
		AND (trim($this->EUI_Session->_get_session('kategory'))!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->EUI_Session->_get_session('kategory')));
	}
	
/** nama_campaign **/

	if( $this->EUI_Session->_have_get_session('nama_campaign')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='')
		AND (trim($this->EUI_Session->_get_session('nama_campaign'))!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->EUI_Session->_get_session('nama_campaign')));
	}
	
/** payment_mode  **/

	if( $this->EUI_Session->_have_get_session('mode_of_payment')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='')
		AND (trim($this->EUI_Session->_get_session('mode_of_payment'))!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->EUI_Session->_get_session('mode_of_payment')));
	}
	
	
	$num = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{	
		foreach( $rows as $fld => $vls ) 
		{
			$this->_content[$num][$fld] = $vls;
		}	
		
		$num++;
	}
  return array( 'field' => $this->label_key, 'content' => $this->_content,'title' => $this->param );
}
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */

/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 *
 **/
 
public function CallReasonIdByCode( $code = NULL )
{
 static $config = array();
	
  if(!is_null($code) 
	AND is_array($code) )
  {
	 $this->db->select('a.CallReasonId');
	 $this->db->from('t_lk_account_status a ');
	 $this->db->join('t_lk_customer_status b ',' a.CallReasonCategoryId=b.CallReasonCategoryId','LEFT');
	 $this->db->where_in('a.CallReasonCode', $code);
	 foreach( $this->db->get()->result_assoc() as $rows )
	 {	
		$config[$rows['CallReasonId']] = $rows['CallReasonId'];  
	 }	
  }
  
  return $config;
  
}
 
/**
 * @ def  : page of index read first class 
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 *
 **/
 
public function CallReasonIdByCategoryCode( $code = NULL )
{
 static $config = array();
	
  if(!is_null($code) 
	AND is_array($code) )
  {
	 $this->db->select('a.CallReasonId');
	 $this->db->from('t_lk_account_status a ');
	 $this->db->join('t_lk_customer_status b ',' a.CallReasonCategoryId=b.CallReasonCategoryId','LEFT');
	 $this->db->where_in('b.CallReasonCategoryCode', $code);
	 foreach( $this->db->get()->result_assoc() as $rows )
	 {	
		$config[$rows['CallReasonId']] = $rows['CallReasonId'];  
	 }	
  }
  
  return $config;
  
}
 
/**
 * @ def : MajorProduct --
 * ----------------------------------------------------
  
 * @ param : - 
 * @ param : -  
 */
 public function getInitialize()
 {
	$UI = array(
		'MajorDC' => $this->getMajorDC(),
		'MajorProduct' => $this->getMajorProduct(),
		'Product' => $this->getProduct(),
		'UserId' => $this->getUserCreate(),
		'SecondPayment' => $this->getSecondPayment(), 
		'CampaignName' => $this->getCampaignName(),
		'CategoryName' => $this->getCategory(),
		'DCName' => $this->getDCName(),
		'PaymentMode' => $this->getPaymentMode()
		);
	
	return $UI;
 } 
function TotalPolicyByParents()
{
	$this->db->select("*");
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_campaign b ','a.PolicyCampaign_Master_Code=b.CampaignCode','LEFT');
	$this->db->join('t_gn_campaign_project d ',' b.CampaignId=d.CampaignId','LEFT');
	$this->db->where_in('d.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->group_by('bln');
	$this->db->having("bln IS NOT NULL");
}

 

 }
?>