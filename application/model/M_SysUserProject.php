<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SysUserProject extends EUI_Model
{


/* 
 * pack : instance of class 
 */
 
private static $Instance = null;

/* 
 * pack : instance of class 
 */

public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
}

/* 
 * pack : instance of class 
 */

function M_SysUserProject()
{
	$this->load->meta('_cc_extension_agent');
}
 
/* 
 * pack : instance of class 
 */

function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery
			(" select a.WorkId FROM t_gn_user_project_work a 
				left join t_tx_agent b on a.UserId=b.UserId
				left join t_lk_work_project c on a.ProjectId=c.ProjectId ");
 
	
	
	$filter = '';
	if( $this->URI->_get_have_post('keywords') ) 
	{
		$keywords = $this -> URI -> _get_post("keywords");
		
		$filter = " AND ( c.ProjectName LIKE '%$keywords%' 
							OR b.full_name LIKE '%$keywords%' 
							OR d.full_name LIKE '%$keywords%' 
							OR a.CreateTs LIKE '%$keywords%' 
						   )";
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	
	if( $this -> EUI_Page -> _get_query() )
	{
		return $this -> EUI_Page;
	}
}

/* 
 * pack : instance of class 
 */

function _get_content()
{

	$this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this -> EUI_Page->_setPage(10);
 
	$this -> EUI_Page -> _setQuery
			("select a.*, b.*, c.ProjectName, d.full_name as  CreateByUser from t_gn_user_project_work a 
				left join t_tx_agent b on a.UserId=b.UserId
				left join t_lk_work_project c on a.ProjectId=c.ProjectId 
				left join t_tx_agent d on a.UserCreateId=d.UserId"); 
	
			
	$filter = '';
	if( $this->URI->_get_have_post('keywords') ) 
	{
		$keywords = $this -> URI -> _get_post("keywords");
		
		$filter = " AND ( c.ProjectName LIKE '%$keywords%' 
							OR b.full_name LIKE '%$keywords%' 
							OR d.full_name LIKE '%$keywords%' 
							OR a.CreateTs LIKE '%$keywords%' 
						   )";
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	
	if( $this -> URI ->_get_have_post('order_by') )
	{
		$this -> EUI_Page->_setOrderBy($this -> URI ->_get_post('order_by'),$this -> URI ->_get_post('type') );
	}
	$this -> EUI_Page->_setLimit();
}


/* 
 * pack : instance of class 
 */

 function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/* 
 * pack : instance of class 
 */

function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/* 
 * pack : instance of class 
 */

 function _getUserSkill()
 {
	$_conds = array();
	
	$this -> db -> select('*');
	$this -> db -> from('cc_skill');
	
	foreach( $this -> db -> get()->result_assoc() as $rows ){
		$_conds[$rows['id']] = $rows['description'];
	}
	
	return $_conds;
	
 }
 
//*** _getUserAvailaible **/
 
private function _getUserAvailaible()
{
	$result = true;
	$shs = array(
			USER_ROOT, USER_ADMIN, USER_ACCOUNT_MANAGER, 
			USER_QUALITY_STAFF, USER_MANAGER,USER_QUALITY_HEAD,
			USER_SUPERVISOR);
	
	/***** default *********************************/
	
	$this->db->select('count(*) as jumlah', False);
	$this->db->from('t_gn_user_project_work');
	
	/* cek parameter test ***/
	
	if( $params = $this->URI->_get_all_request() ) 
	{
		$count = 0;
		if( in_array( $params['PrivilegeId'],$shs) ) 
		{
			$this->db->where('UserId', $params['UserId']);
			$this->db->where('ProjectId',$params['ProjectId']);
			if($rows = $this->db->get()->result_first_assoc()) {
				$count = (INT)$rows['jumlah'];	
				if(($count>=1)) $result = FALSE;
			}
		}
		else
		{
			$this->db->where('UserId', $params['UserId']);
			
			if($rows = $this->db->get()->result_first_assoc())
			{
				$count = (INT)$rows['jumlah'];	
				if(($count>=1)) $result = FALSE;
			}
		}
	}
	
	
	
	return $result;
} 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setSaveUserWorkProject($params = null )
 {
	$array_filter = array('PrivilegeId');
	
	$_conds = 0;
	if(is_array( $params ) ) 
	{
		foreach( $params as $fieldname => $fieldvalues )
		{
			if( !in_array($fieldname, $array_filter) )
			{
				$this->db->set($fieldname, $fieldvalues);
			}
		}
		
		$this->db->set('UserCreateId',$this->EUI_Session->_get_session('UserId'));
		$this->db->set('CreateTs',date('Y-m-d H:i:s'));
		
		// cek avibility **
		if(self::_getUserAvailaible())
		{
			$this->db->insert('t_gn_user_project_work');
			if( $this->db->affected_rows() > 0 ) {
				$_conds++;
			}
		}
	}
	
	return $_conds;
 }
 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 function _setUpdateUserWorkProject($params)
 {
	$array_filter = array('PrivilegeId'); $_conds = 0;
	if(is_array($params) )
	{
		foreach( $params as $fieldname => $fieldvalues )
		{
			if( $fieldname=='WorkId'){
				$this->db->where($fieldname, $fieldvalues);
			}
			
			if( !in_array($fieldname, $array_filter) ) {
				$this->db->set($fieldname, $fieldvalues);
			}
		}
		
		$this->db->update('t_gn_user_project_work');
		if( $this->db->affected_rows() > 0 ) {
			$_conds++;
		}
	}
	
	return $_conds;
 }
 
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getUserSkillData( $SkillId=0 )
 {
	$_conds = array();
	
	$this -> db->select('*');
	$this -> db->from('cc_agent_skill');
	$this -> db->where('id', $SkillId);
	if( $rows = $this -> db->get() -> result_first_assoc() ) {
		$_conds  = $rows;
	}
	
	return $_conds;
	
 }

 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setDeleteWorkUserProject( $params =array() )
 {
	$_conds = 0;
	foreach( $params as $k => $WorkId )
	{
		if( $this -> db -> delete('t_gn_user_project_work',array( 'WorkId' => $WorkId )) ) {
			$_conds++;
		}
	}
	
	return $_conds;	
 }
 
 // _getUserWorkProject 
 
 function _getUserWorkProject( $WorkId = 0 )
 {
	$this->db->select('*');
	
	$this->db->from('t_gn_user_project_work a');
	$this->db->join('t_tx_agent b ', 'a.UserId = b.UserId');
	$this->db->where('a.WorkId', $WorkId);
	
	return $this->db->get()->result_first_assoc();
	
 }
 
 
// _getUserWorkProject 

 public function _getUserExistOnProject( $UserId = 0 )
 {
	$conds  = 0;
	
	$this->db->select('count(a.UserId) as count', FALSE);
	$this->db->from('t_gn_user_project_work a');
	$this->db->where('a.UserId', $UserId);
	$qry = $this->db->get();
	
	if( $rows = $qry->result_first_assoc() )
	{
		$conds = (INT)$rows['count'];
	}
	
	return $conds;
 }
 
 
}

?>