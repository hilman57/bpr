<?php

class M_PvcvcReport extends EUI_Model{
	
	public function modul_random(){
		return array(
			"pvc"=>"PVC",
			"vct"=>"VC Temporary",
			"vcp"=>"VC Permanent",
			"pvc_category"=>"PVC per Category",
			"vct_category"=>"VC Temporary per Category",
			"vcp_category"=>"VC Permanent per Category"
		);
	}
	
	public function getDataPVC(){
		$start_date = $this -> URI->_get_post('start_date_claim');
		$end_date = $this -> URI->_get_post('end_date_claim');
		
		// $period = new DatePeriod(
			 // new DateTime($start_date),
			 // new DateInterval('P1M'),
			 // new DateTime($end_date)
		// );
		
		$date1 = $start_date;
		$date2 = $end_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		for($i=0;$i<=$diff;$i++){
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));
			
			$this->db->reset_select();
			$this->db->select("a.Date,
				(select (suba.NewPVC+suba.ExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as OpenPVC,
				a.NewPVC, a.ExitPVC,
				
				(select ((ifnull((select (suba.NewPVC+suba.ExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+suba.NewPVC)-suba.ExitPVC) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingPVC,
				
				(select (suba.BalanceNewPVC+suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as BalanceOpenPVC,
				a.BalanceNewPVC, a.BalanceExitPVC,
				
				(select ((ifnull((select (suba.BalanceNewPVC+suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+suba.BalanceNewPVC)-suba.BalanceExitPVC) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingPVC
				",false);
			$this->db->from("t_gn_pvcvc_report a");
			$this->db->where("a.Date = '".$final."'");
			$rs = $this->db->get();
			
			if( $rs->num_rows() > 0 ){
				$data[$i] = $rs->result_assoc();
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		
		return $data;
	}
	
	public function getDataVCT(){
		$start_date = $this -> URI->_get_post('start_date_claim');
		$end_date = $this -> URI->_get_post('end_date_claim');
		
		// $period = new DatePeriod(
			 // new DateTime($start_date),
			 // new DateInterval('P1M'),
			 // new DateTime($end_date)
		// );
		
		$date1 = $start_date;
		$date2 = $end_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		for($i=0;$i<=$diff;$i++){
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));
			
			$this->db->reset_select();
			$this->db->select("a.Date,
				(select (suba.NewVCT+suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as OpenVCT,
				a.NewVCT, a.ExitVCT,
				
				(select ((ifnull((select (suba.NewVCT+suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),null)+suba.NewVCT)-suba.ExitVCT) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingVCT,
				
				(select (suba.BalanceNewVCT+suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as BalanceOpenVCT,
				a.BalanceNewVCT, a.BalanceExitVCT,
				
				(select ((ifnull((select (suba.BalanceNewVCT+suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),null)+suba.BalanceNewVCT)-suba.BalanceExitVCT) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingVCT"
				,false);
			$this->db->from("t_gn_pvcvc_report a");
			$this->db->where("a.Date = '".$final."'");
			$rs = $this->db->get();
			
			if( $rs->num_rows() > 0 ){
				$data[$i] = $rs->result_assoc();
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		
		return $data;
	}
	
	public function getDataVCP(){
		$start_date = $this -> URI->_get_post('start_date_claim');
		$end_date = $this -> URI->_get_post('end_date_claim');
		
		// $period = new DatePeriod(
			 // new DateTime($start_date),
			 // new DateInterval('P1M'),
			 // new DateTime($end_date)
		// );
		
		$date1 = $start_date;
		$date2 = $end_date;

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		$final = $start_date;
		
		for($i=0;$i<=$diff;$i++){
			$subtime = strtotime($final);
			$subfinal = date("Y-m-d", strtotime("+0 month", $subtime));
			
			$this->db->reset_select();
			$this->db->select("a.Date,
				(select (suba.NewVCP+suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as OpenVCP,
				a.NewVCP, a.ExitVCP,
				
				(select ((ifnull((select (suba.NewVCP+suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+suba.NewVCP)-suba.ExitVCP) from t_gn_pvcvc_report suba where suba.Date = a.Date) as ClosingVCP,
				
				(select (suba.BalanceNewVCP+suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')) as BalanceOpenVCP,
				a.BalanceNewVCP, a.BalanceExitVCP,
				(select ((ifnull((select (suba.BalanceNewVCP+suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = date_format('".$subfinal."' - interval 1 month, '%b-%Y')),0)+suba.BalanceNewVCP)-suba.BalanceExitVCP) from t_gn_pvcvc_report suba where suba.Date = a.Date) as BalanceClosingVCP
				",false);
			$this->db->from("t_gn_pvcvc_report a");
			$this->db->where("a.Date = '".$final."'");
			// echo $this->db->_get_var_dump();
			$rs = $this->db->get();
			
			if( $rs->num_rows() > 0 ){
				$data[$i] = $rs->result_assoc();
			}
			
			$time = strtotime($final);
			$final = date("M-Y", strtotime("+1 month", $time));
		}
		
		return $data;
	}
	
	public function getDataVCTPerCategory(){
		$start_date	= date("Y-m-d", strtotime($this -> URI->_get_post('start_date_claim')));
		$end_date	= date("Y-m-t", strtotime($this -> URI->_get_post('end_date_claim')));
		
		// echo $start_date."-".$end_date; exit();
		
		// $subtime = strtotime($start_date);
		// $subfinal = date("Y-m-d", strtotime("+0 month", $subtime));
		
		$this->db->reset_select();
		$this->db->select("a.vc_reason_id, b.VCReasonDesc, b.VCReasonCode, count(distinct a.debitur_id) as 'Account', sum(a.wo_bal) as Balance", false);
		$this->db->from("t_gn_vc_review a");
		$this->db->join("t_lk_vc_reason b","a.vc_reason_id = b.Id","LEFT");
		$this->db->where("a.create_date >= '".$start_date."'");
		$this->db->where("a.create_date <= '".$end_date."'");
		$this->db->where("b.AccountStatusCode", 116);
		$this->db->group_by("a.vc_reason_id");
		
		// echo $this->db->_get_var_dump();
		$rs = $this->db->get();
		
		foreach($rs->result_assoc() as $rows){
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Account'] = $rows['Account'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Balance'] = $rows['Balance'];
			
			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Account'] = $rows['Account'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Balance'] = $rows['Balance'];
		}
		
		return $data;
	}
	
	public function getDataVCPPerCategory(){
		$start_date	= date("Y-m-d", strtotime($this -> URI->_get_post('start_date_claim')));
		$end_date	= date("Y-m-t", strtotime($this -> URI->_get_post('end_date_claim')));
		
		// echo $start_date."-".$end_date; exit();
		
		// $subtime = strtotime($start_date);
		// $subfinal = date("Y-m-d", strtotime("+0 month", $subtime));
		
		$this->db->reset_select();
		$this->db->select("a.vc_reason_id, b.VCReasonDesc, b.VCReasonCode, count(distinct a.debitur_id) as 'Account', sum(a.wo_bal) as Balance", false);
		$this->db->from("t_gn_vc_review a");
		$this->db->join("t_lk_vc_reason b","a.vc_reason_id = b.Id","LEFT");
		$this->db->where("a.create_date >= '".$start_date."'");
		$this->db->where("a.create_date <= '".$end_date."'");
		$this->db->where("b.AccountStatusCode", 117);
		$this->db->group_by("a.vc_reason_id");
		
		// echo $this->db->_get_var_dump();
		$rs = $this->db->get();
		
		foreach($rs->result_assoc() as $rows){
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Account'] = $rows['Account'];
			$data[$rows['vc_reason_id']]['Closing Inventory']['Balance'] = $rows['Balance'];
			
			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['vc_reason_id']]['Chargeoff']['VCReasonCode'] = $rows['VCReasonCode'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Account'] = $rows['Account'];
			$data[$rows['vc_reason_id']]['Chargeoff']['Balance'] = $rows['Balance'];
		}
		
		return $data;
	}
	
	public function getVCReason($CallReasonCode){
		$this->db->reset_select();
		$this->db->select("a.Id, a.VCReasonDesc, a.VCReasonCode", false);
		$this->db->from("t_lk_vc_reason a");
		$this->db->where("AccountStatusCode", $CallReasonCode);
		// echo $this->db->_get_var_dump();
		$rs = $this->db->get();
		
		foreach( $rs->result_assoc() as $rows){
			$data[$rows['Id']]['VCReasonDesc'] = $rows['VCReasonDesc'];
			$data[$rows['Id']]['VCReasonCode'] = $rows['VCReasonCode'];
		}
		
		return $data;
	}
	
	public function getAccStatusVC($CallReasonDesc){
		$reportType = array("vcp_category"=>"VC PERMANENT","vct_category"=>"VC TEMPORARY");
		$this->db->reset_select();
		$this->db->select("a.CallReasonCode", false);
		$this->db->from("t_lk_account_status a");
		$this->db->where("a.CallReasonDesc", $reportType[$CallReasonDesc]);
		// echo $this->db->_get_var_dump();
		$rs = $this->db->get();
		
		foreach( $rs->result_assoc() as $rows){
			$data = $rows['CallReasonCode'];
		}
		
		return $data;
	}
	
	
	
	
	
	
	public function get_claim_access_all()
	{
		/*EXPLAIN
SELECT a.claim_id,
b.deb_id,
b.acc_no,
c.deb_id,
c.deb_acct_no,
d.CampaignDesc,
f.id AS OldAgent,
a.claim_date_ts,
e.id AS AgentClaim,
a.approval_date_ts,
g.AproveName AS ApprovalStatus,
a.bucket_trx_id,
i.bucket_trx_id,
i.access_all_id,
j.*
FROM t_gn_claim_debitur a 
INNER JOIN t_gn_buckettrx_debitur b ON a.bucket_trx_id=b.bucket_trx_id
LEFT JOIN t_gn_debitur c ON b.deb_id=c.deb_id
LEFT JOIN t_gn_campaign d ON c.deb_cmpaign_id =d.CampaignId
LEFT JOIN t_tx_agent e ON a.claim_by=e.UserId
LEFT JOIN t_tx_agent f ON a.from_owner=f.UserId
LEFT JOIN t_lk_aprove_status g ON a.approval_status=g.AproveCode
INNER JOIN t_gn_modul_setup h ON b.modul_setup_id=h.modul_setup_id
INNER JOIN t_gn_access_all i ON a.bucket_trx_id = i.bucket_trx_id
LEFT JOIN t_st_access_all j ON i.st_access_all_id=j.st_access_all_id

WHERE a.claim_date_ts like '2016-04-19%'*/
		
		$data=array();
		$start_claim_date = null;
		$end_claim_date = null;
		$report_type=array(0);
		if($this -> URI->_get_have_post("report_type"))
		{
			$report_type = $this -> URI->_get_post('report_type');
		}
		if(!$this -> URI->_get_array_post())
		{
			$start_claim_date = _getDateEnglish($this -> URI->_get_post('start_date_claim'));
			$end_claim_date = _getDateEnglish($this -> URI->_get_post('end_date_claim'));
			$this->db->reset_select();		
			$this->db->select(	"a.claim_id,
								b.deb_id,
								b.acc_no,
								c.deb_id,
								c.deb_acct_no,
								d.CampaignDesc,
								f.id AS OldAgent,
								a.claim_date_ts,
								e.id AS AgentClaim,
								a.approval_date_ts,
								g.AproveName AS ApprovalStatus,
								a.bucket_trx_id,
								i.bucket_trx_id,
								i.access_all_id"
			);
			$this->db->from("t_gn_claim_debitur a");
			$this->db->join("t_gn_buckettrx_debitur b ","a.bucket_trx_id=b.bucket_trx_id", "INNER");
			$this->db->join("t_gn_debitur c","b.deb_id=c.deb_id", "LEFT");
			$this->db->join("t_gn_campaign d","c.deb_cmpaign_id =d.CampaignId", "LEFT");
			$this->db->join("t_tx_agent e","a.claim_by=e.UserId", "LEFT");
			$this->db->join("t_tx_agent f","a.from_owner=f.UserId", "LEFT");
			$this->db->join("t_lk_aprove_status g","a.approval_status=g.AproveCode", "LEFT");
			$this->db->join("t_gn_modul_setup h","b.modul_setup_id=h.modul_setup_id", "INNER");
			$this->db->join("t_gn_access_all i","a.bucket_trx_id = i.bucket_trx_id", "INNER");
			$this->db->join("t_st_access_all j","i.st_access_all_id=j.st_access_all_id", "LEFT");
			$this->db->where("a.claim_date_ts BETWEEN '".$start_claim_date."' AND '".$end_claim_date."'");
			$this->db->where_in("h.modul_id_rnd",$report_type);
			$rs = $this->db->get();
			// echo $this->db->last_query();
			if( $rs->num_rows() > 0 )
			{
				$data = $rs->result_assoc();
			}
		}
		return $data;
	}

	
	
}