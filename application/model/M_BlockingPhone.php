<?php
/*
 * @ pack : M_BlockingPhone handle select blocking number 
 * @ auth : omens 
 * @ date : 2015-03-03 
 */
 
 
class M_BlockingPhone extends EUI_Model
{

var $_bloking_table = array('t_tx_blocking');

/*
 * @ pack : M_BlockingPhone 
 */ 
 
 private static $Instance= null;
 
/*
 * @ pack : M_BlockingPhone 
 */ 
 
 public function M_BlockingPhone()
{
  $this->load->model(array('M_SrcCustomerList','M_ModSaveActivity'));
} 
/*
 * @ pack : Instance 
 */ 
 
 public static function &Instance()
{
 if( is_null(self::$Instance) ) {
	self::$Instance = new self();
 }
 return self::$Instance;
}

/*
 * @ pack : Instance 
 */ 
 
 public function _get_select_bloking( $CustomerId = null ) 
{
	
 $recsource = array();
// @ pack : selector 
 $this->db->reset_select();
 $this->db->select('Blocking_phone_no');
 $this->db->from(reset($this->_bloking_table));
 
 if( !is_null( $CustomerId) )
 {
	$this->db->where('Blocking_CustomerId', $CustomerId);
 }
 
 $qry = $this->db->get();
 if( $qry &&($qry->num_rows() > 0 )) 
	foreach( $qry->result_assoc() as $rows )
 {
	$recsource[$rows['Blocking_phone_no']] = $rows['Blocking_phone_no'];	
 }
 
 return $recsource;
 
} 


/*
 * @ pack : Instance 
 */ 
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.Blocking_id ");
	$this->EUI_Page->_setFrom("t_tx_blocking a ");
	$this->EUI_Page->_setJoin("t_gn_debitur b "," a.Blocking_CustomerId=b.deb_id","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign c "," b.deb_cmpaign_id=c.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent d ","a.Blocking_UserId=d.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment e ","b.deb_id=e.CustomerId","LEFT", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('e.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('e.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('e.AssignSpv IS NOT NULL', FALSE);
 	$this->EUI_Page->_setAnd('c.CampaignStatusFlag',1);
	

 // @ pack : filter by default session ---------------------------------------------------------------------------------
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
	   $this->EUI_Page->_setAnd('e.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		$this->EUI_Page->_setAnd('e.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		$this->EUI_Page->_setAnd('e.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setLikeCache('a.Blocking_phone_no', 'block_phone_number', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'block_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'block_campaign_id',TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'block_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'block_call_status', TRUE);
	$this->EUI_Page->_setAndCache('d.UserId', 'block_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'block_recsource', TRUE);
	
	
 // @ pack : start date ------------------------------------------------------
 
	$this->EUI_Page->_setAndOrCache("a.Blocking_date>='". _getDateEnglish(_get_post('block_start_date')) ." 00:00:00'", 'block_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.Blocking_date<='". _getDateEnglish(_get_post('block_end_date')) ." 23:59:59'", 'block_end_date', TRUE);
	
 // echo $this ->EUI_Page->_getCompiler();
	
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}

/*
 * @ pack : Instance 
 */ 
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.Blocking_id AS BlockId"=> array('BlockId','ID','primary'),
		"a.Blocking_CustomerId as DebId " => array('DebId','ID','hidden'),
		"c.CampaignDesc AS Product" =>array('Product','Product.'),
		"b.deb_acct_no AS AccountNo" =>array('AccountNo','No Akun.'),
		"b.deb_name AS CustomerName" =>array('CustomerName','Nama Pelanggan'),
		"a.Blocking_phone_no AS Phone" =>array('Phone','Nomor Telepon.'),
		"a.Blocking_phone_type AS TypePhone" =>array('TypePhone','Telepon Tipe'),
		"a.Blocking_remarks AS Notes" =>array('Notes','Catatan'),
		"a.Blocking_date AS BlockDate" =>array('BlockDate','Tanggal Di Block'),
		"d.code_user AS UserCode" =>array('UserCode','User ID'),
		"d.full_name AS UserName" =>array('UserName','Nama User')
	));
	
	$this->EUI_Page->_setFrom("t_tx_blocking a ");
	$this->EUI_Page->_setJoin("t_gn_debitur b "," a.Blocking_CustomerId=b.deb_id","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign c"," b.deb_cmpaign_id=c.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent d ","a.Blocking_UserId=d.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment e ","b.deb_id=e.CustomerId","LEFT", TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	$this->EUI_Page->_setAnd('e.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('e.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('e.AssignSpv IS NOT NULL', FALSE);
 	$this->EUI_Page->_setAnd('c.CampaignStatusFlag',1);
	 
// @ pack : filter by default session ---------------------------------------------------------------------------------
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
	   $this->EUI_Page->_setAnd('e.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		$this->EUI_Page->_setAnd('e.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		$this->EUI_Page->_setAnd('e.AssignSelerId IS NOT NULL');
	}	
	
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setLikeCache('a.Blocking_phone_no', 'block_phone_number', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'block_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'block_campaign_id',TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'block_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'block_call_status', TRUE);
	$this->EUI_Page->_setAndCache('d.UserId', 'block_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'block_recsource', TRUE);
	
	
 // @ pack : start date ------------------------------------------------------
 
	$this->EUI_Page->_setAndOrCache("a.Blocking_date>='". _getDateEnglish(_get_post('block_start_date')) ." 00:00:00'", 'block_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.Blocking_date<='". _getDateEnglish(_get_post('block_end_date')) ." 23:59:59'", 'block_end_date', TRUE);
	
// @ pack : start date ------------------------------------------------------
	
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
}

/*
 * @ pack : Instance 
 */ 
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ pack : Instance 
 */ 
 
 public function _get_page_number()
{
	if( $this -> EUI_Page -> _get_query()!='' ) 
	{
		return $this -> EUI_Page -> _getNo();
	}	
 }

/*
 * @ pack : SetUnblockPhone
 */
 
 public function _SetUnblockPhone()
{
 
 $Loger =& M_Loger::Instance();
  
 $conds = 0;
 $argv = _get_array_post('BlockId');
 if( is_array( $argv ) )
   foreach ( $argv as $key => $BlockId )
 {
	$this->db->where('Blocking_id', $BlockId);
	if( $this->db->delete('t_tx_blocking') ) 
	{
		$Loger->set_activity_log("UNBLOCK PHONE -> BLOCKING_ID[{$BlockId}]");
		$conds++;
	}
  }
  
 return $conds;
 
}
 
 public function _SetUnblockPhoneAll()
{
	$Loger =& M_Loger::Instance();
	$this->db->reset_select();
	$this->db->select('a.*');
	$this->db->from('t_tx_blocking a');
        $this->db->join('t_gn_debitur b','b.deb_id = a.Blocking_CustomerId');
        
	$conds = 0;
	$query = $this->db->get();       
        
        if($query->num_rows > 0){
            
            foreach ($query->result_array() as $rows)
            {
                    $BlockId = $rows['Blocking_id'];
                    //$this->db->where('Blocking_id', $BlockId);
                    if( $this->db->delete('t_tx_blocking',array('Blocking_id'=> $BlockId)) ) {
                            $Loger->set_activity_log("UNBLOCK PHONE -> BLOCKING_ID[{$BlockId}]");
                            
                    }
                    $conds++;
            }                 
        }
	return $conds;
	
}
// END CLASS 

 
}

?>