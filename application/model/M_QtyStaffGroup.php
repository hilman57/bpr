<?php
class M_QtyStaffGroup extends EUI_Model 
{
	
/*
 * @ def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

function M_QtyStaffGroup() {
	$this -> load->model(array('M_SysUser'));
}

/*
 * @ def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

 public function _getUnitTest()
 {
	echo "<h1 class='font-standars ui-corner-top ui-state-default' style='height:30px;padding-top:10px;'> Hello World </h1>";
 }
 
/*
 * @ def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

 
public function _getStaffAvailable()
{
	$_conds = $this -> M_SysUser -> _get_quality_staff();
	if( $this -> EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD ) {
		$_conds  = $this -> M_SysUser -> _get_quality_staff($this -> EUI_Session->_get_session('UserId')); 
	}
	return  $_conds;
}


/*
 * @ def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

public function _getStaffSkill()
{
	$skill = array();
	
	$this -> db -> select('*');
	$this -> db -> from('t_lk_quality_skill');
	foreach( $this ->db -> get() ->result_assoc() as $rows )
	{
		$skill[$rows['Quality_Skill_Id']] = $rows['Quality_Skill_Desc'];
	}
	
	return $skill;
}

/*
 * @ def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

public function _getStaffByGroup()
{

 $quality_group = array();
 
 $this -> db -> select("
		a.*, c.id as QualityStaffUser,  
		c.full_name as QualityStaffName,
		d.id as QualityHeadUser,
		d.full_name as QualityHeadName, 
		b.Quality_Skill_Name, 
		b.Quality_Skill_Desc"
	);
	
 $this ->db -> from("t_gn_quality_group a");
 $this ->db -> join("t_lk_quality_skill b ","a.Quality_Skill_Id=b.Quality_Skill_Id","LEFT");
 $this ->db -> join("t_tx_agent c ","a.Quality_Staff_id=c.UserId","LEFT");
 $this ->db -> join("t_tx_agent d ", "c.quality_id=d.UserId","LEFT");
 $this ->db -> join("t_gn_user_project_work e ","c.UserId=e.UserId","LEFT");
 
 
 $this ->db -> where_in("e.ProjectId", $this -> EUI_Session->_get_session('ProjectId'));
 
// root on head of the staff 
 
 if( $this -> EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD ){
	$this ->db -> where("d.quality_id", $this -> EUI_Session->_get_session('UserId'));
 }
 
 // root on login 
 if( $this -> EUI_Session->_get_session('HandlingType')==USER_ROOT ){
	$this ->db -> where("c.user_state", 1);
 }
 
 // echo $this->db->_get_var_dump();
 
 foreach( $this ->db -> get() ->result_assoc() as $array_quality_group )
 {
	if( is_array( $array_quality_group) 
		AND !is_null($array_quality_group) )
	{
		$quality_group[$array_quality_group['Quality_Group_Id']] = $array_quality_group;
	}	
 }
 
 return $quality_group;
}



/*
 * @ def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
 
function _setAddAvailableSkill( $QualityStaffId = null )
{
	$_conds = 0;
	if( !is_null($QualityStaffId) 
	AND is_array($QualityStaffId) )
	{
		foreach( $QualityStaffId as $i => $QualityId )
		{
			$this -> db -> set('Quality_Staff_id',$QualityId);
			$this -> db-> set('Quality_Create_Ts', DATE('Y-m-d H:i:s'));
			$this -> db -> insert('t_gn_quality_group');
			
			if( $this -> db -> affected_rows()< 0 )
			{
				$this -> db-> where('Quality_Staff_id', $QualityId);
				$this -> db-> set('Quality_Create_Ts', DATE('Y-m-d H:i:s'));
				$this -> db -> update('t_gn_quality_group');
			}
			$_conds++;	
		}	
	
	}
	
	return $_conds;
}

 

/*
 * @ def : _setAssignQualitySkill 
 * ----------------------------------
 *
 * @ param : Quality_Group_Id  ( array )
 * @ param : Quality_Skill_Id  ( integer )
 */
 
function _setAssignQualitySkill( $Quality_Group_Id = null, $Quality_Skill_Id = 0 )
{
	$_conds = 0;
	if( !is_null($Quality_Group_Id) 
		AND is_array($Quality_Group_Id) 
		AND $Quality_Skill_Id!=FALSE )
	{
		foreach( $Quality_Group_Id as $i => $GroupId )
		{
			$this -> db -> where('Quality_Group_Id', $GroupId);
			$this -> db -> set('Quality_Skill_Id',$Quality_Skill_Id);
			$this -> db -> set('Quality_Create_Ts', DATE('Y-m-d H:i:s'));
			if( $this -> db -> update('t_gn_quality_group')) {
				$_conds++;	
			}
				
		}	
	
	}
	
	return $_conds;
}


/*
 * @ def : _setRemoveQualitySkill 
 * ----------------------------------
 *
 * @ param : Quality_Group_Id  ( array )
 * @ param : -
 */
 
function _setRemoveQualitySkill( $Quality_Group_Id = null )
{
	$_conds = 0;
	if( !is_null($Quality_Group_Id) AND is_array($Quality_Group_Id) )  
	{
		foreach( $Quality_Group_Id as $i => $GroupId )
		{
			$this -> db -> where('Quality_Group_Id', $GroupId);
			
			if( $this -> db -> delete('t_gn_quality_group')) 
			{
				$_conds++;	
			}
				
		}	
	
	}
	
	return $_conds;
}



/*
 * @ def : _setClearQualitySkill 
 * ----------------------------------
 *
 * @ param : Quality_Group_Id  ( array )
 * @ param : -
 */
 
function _setClearQualitySkill( $Quality_Group_Id = null )
{
	$_conds = 0;
	if( !is_null($Quality_Group_Id) 
		AND is_array($Quality_Group_Id)  )
	{
		foreach( $Quality_Group_Id as $i => $GroupId )
		{
			$this -> db -> where('Quality_Group_Id', $GroupId);
			$this -> db -> set('Quality_Skill_Id','NULL',FALSE);
			$this -> db -> set('Quality_Create_Ts',DATE('Y-m-d H:i:s'));
			if( $this -> db -> update('t_gn_quality_group')) {
				$_conds++;	
			}
				
		}	
	
	}
	
	return $_conds;
}

  
//  STOP : Critical

}
	
?>
