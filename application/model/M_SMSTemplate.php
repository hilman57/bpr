<?php
class M_SMSTemplate extends EUI_Model
{

// meta table ;

 private static $meta = null; 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function M_SMSTemplate()
 {
	if( is_null(self::$meta) )
	{
		self::$meta = 'serv_sms_tpl';
	}
 }


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getData( $id = NULL )
{
  $object  = self::_getComponent();
 
	$this->db->select("*");
	$this->db->from(self::$meta);
	$this->db->where(reset($object['primary']),$id);
	return $this->db->get()->result_first_assoc();
}

/*
 * @ pack : get properties object 
 */ 
 
public function _getComponent()
{
	return array
	( 
		'combo'   => array('SmsTplFlags','GroupTplId'),
		'input'   => array('SmsTplSubject'),
		'textarea' => array('SmsTplContent'),
		'primary' => array('SmsTplId')
	);
}

/*
 * @ pack : get properties object 
 */ 
 
 public function _getLabels()
{
 $labels = array(
	'GroupTplId' => 'Category', 
	'SmsTplSubject'=> 'Title', 
	'SmsTplContent'=> 'Text Message', 
	'SmsTplFlags'=> 'Status');
	
 return $labels;
 
}

/*
 * @ pack : get properties object 
 */ 

public function _getDropdown()
{
   $Dropdown['SmsTplFlags'] = array('1'=> 'Active','0'=> 'UnActive');
   
   
  // pack : get group TPL 
  
   $this->db->reset_select();
   $this->db->select('a.GroupTplId, a.GroupTplName, a.GroupTplDesc');
   $this->db->from('serv_sms_group_tpl a');
   
   foreach( $this->db->get()->result_assoc() as $rows )
   {
		$Dropdown['GroupTplId'][$rows[GroupTplId]] = $rows[GroupTplDesc];
   }
   
   return $Dropdown;
}


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getFieldName() 
{
	return $this->db->list_fields(self::$meta);
}

 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setQuery(" SELECT * FROM " . self::$meta ." a "); 
	$flt = '';
	
	if( $this->URI->_get_have_post('keywords') ) 
	{
		$flt .=" AND ( 
				a.GroupTplId LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.SmsTplSubject LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.SmsTplContent LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.SmsTplFlags LIKE '%{$this->URI->_get_post('keywords')}%' 
			)";
	}				
			
	$this->EUI_Page->_setWhere($flt); 
	// echo $this->EUI_Page->_getCompiler();
	if( $this->EUI_Page->_get_query() ) {
		return $this->EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_content()
{
  
  $this->EUI_Page->_postPage(_get_post('v_page'));
  $this->EUI_Page->_setPage(10);
  
 /* selected array **/
 
  $this->EUI_Page->_setArraySelect(array(
  "a.SmsTplId as SMSID" => array("SMSID","ID", "primary"),
  "b.GroupTplName as GroupTplName" => array("GroupTplName","kategori"),
  "a.SmsTplSubject as SmsTplSubject" => array("SmsTplSubject","Title"),
  "a.SmsTplContent as SmsTplContent" => array("SmsTplContent","Pesan Text"),
  "IF( a.SmsTplFlags =1, 'Active','UnActive') as SmsTplFlags" => array("SmsTplFlags","Status")
  ));
  
  $this->EUI_Page->_setFrom(self::$meta ." a ");
  $this->EUI_Page->_setJoin("serv_sms_group_tpl b", "a.GroupTplId=b.GroupTplId", "LEFT", TRUE);
  
  if( $this->URI->_get_have_post('keywords') ) {
	$this->EUI_Page->_setAnd(" ( 
				a.GroupTplId LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.SmsTplSubject LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.SmsTplContent LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.SmsTplFlags LIKE '%{$this->URI->_get_post('keywords')}%')", FALSE);
	}	
	
  if( _get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy(_get_post('order_by'),_get_post('type'));
  }
  // echo $this->EUI_Page->_get_query();
  $this -> EUI_Page->_setLimit();
}


/*
 * @ pack : _setUpdate sms template 
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this->EUI_Page->_get_query()!='') 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ pack : _setUpdate sms template 
 */
 
 public function _get_page_number() 
{
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ pack : _setUpdate sms template 
 */
 
 public function _setSave($params=array())
{
	$_conds= 0;
	foreach($params  as $k => $v ) {
		$this -> db -> set($k,$v);
	}
	
	$this -> db -> insert( self::$meta );
	if( $this -> db -> affected_rows()>0 ) {
		$_conds++;
	}
	
	return $_conds;
 }
 
  
/*
 * @ pack : _setUpdate sms template 
 */
 
 public function _setUpdate($params=array())
{

 $object  = self::_getComponent();
 $_conds= 0;
 
 // @ pack : array OK 
 
  if( is_array($params) )
	foreach($params  as $field => $value )
 {
	if( $field == reset($object['primary']) )
		$this->db->where($field,$value);
	else
		$this->db->set($field,$value);
  }
	
// @ pack : then update 
 	
	if($this->db->update(self::$meta)) {
		$_conds++;
	}
	
	return $_conds;
}
 
/*
 * @ pack : _setUpdate sms template 
 */
 
 public function _setDelete($params = null )
{
	$object  = self::_getComponent();
	$_conds = 0;
	$this->db->where_in(reset($object['primary']),$params);
	if( $this->db->delete( self::$meta )){
		$_conds++;
	}
	
	return $_conds;
	
 }
 
 
// END CLASS  
}
?>