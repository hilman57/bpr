<?php
/*
 * @ pack : model class M_ModApprovalDiscount
 *
 */
 
class M_SrcPtpList extends EUI_Model
{
	public function __construct()
	{
		$this->load->model(array('M_SysUser','M_SetCallResult','M_SetCampaign'));
	}
	
	public function _get_default() 
	{
		$this->EUI_Page->_setPage(10); 
		/*
		SELECT a.ptp_id,
		a.deb_id,
		b.deb_name,
		a.ptp_date,
		a.ptp_amount,
		d.pch_name
		FROM t_tx_ptp a
		INNER JOIN t_gn_debitur b ON a.deb_id=b.deb_id
		INNER JOIN t_gn_assignment c ON a.deb_id=c.CustomerId
		LEFT JOIN t_lk_payment_channel d ON a.ptp_chanel = d.id
		*/
		$this->EUI_Page->_setSelect("a.ptp_id");
		$this->EUI_Page->_setFrom("t_tx_ptp a");
		$this->EUI_Page->_setJoin("t_gn_debitur b","a.deb_id=b.deb_id","INNER");
		$this->EUI_Page->_setJoin("t_gn_assignment c","a.deb_id=c.CustomerId ","INNER");
		$this->EUI_Page->_setJoin("t_lk_payment_channel d ","a.ptp_chanel = d.id","LEFT");
		$this->EUI_Page->_setJoin("t_gn_campaign e ","b.deb_cmpaign_id = e.CampaignId","LEFT", TRUE);
	
		//	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

		$this->EUI_Page->_setAnd('c.AssignAdmin IS NOT NULL', FALSE);
		$this->EUI_Page->_setAnd('c.AssignMgr IS NOT NULL', FALSE);
		$this->EUI_Page->_setAnd('c.AssignSpv IS NOT NULL', FALSE);
		$this->EUI_Page->_setAnd('e.CampaignStatusFlag',1);
		$this->EUI_Page->_setAnd('a.is_delete',0);
		
		if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
			array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
		{
			$this->EUI_Page->_setAnd('c.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
		}	

		  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
			
		if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
			array(USER_LEADER) ) )
		{
			 $this->EUI_Page->_setAnd('c.AssignLeader',$this->EUI_Session->_get_session('UserId'));
		}		

		// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
		if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
			array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
		{
			 $this->EUI_Page->_setAnd('c.AssignSelerId IS NOT NULL');
		}		
	
		// @ pack : set cache on page **/
		
		$this->EUI_Page->_setAndCache('b.deb_acct_no', 'ptp_cust_id', TRUE);
		$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'ptp_campaign_id',TRUE);
		$this->EUI_Page->_setAndCache('ag.UserId', 'ptp_agent_id', TRUE);
		$this->EUI_Page->_setLikeCache('a.deb_name', 'ptp_cust_name', TRUE);
	
		// @ pack : PTP date ---------------------------------------------------------------------------------
		
		$this->EUI_Page->_setAndOrCache("a.ptp_date>='". _getDateEnglish(_get_post('ptp_start_date')) ."'", 'ptp_start_date', TRUE);
		$this->EUI_Page->_setAndOrCache("a.ptp_date<='". _getDateEnglish(_get_post('ptp_end_date')) ."'", 'ptp_end_date', TRUE);
		
		// @ pack : PTP amount  ---------------------------------------------------------------------------------

		$this->EUI_Page->_setAndOrCache("a.ptp_amount>=". (INT)_get_post('ptp_start_amountwo') ."", 'ptp_start_amountwo', TRUE);
		$this->EUI_Page->_setAndOrCache("a.ptp_amount<=". (INT)_get_post('ptp_end_amountwo')."", 'ptp_end_amountwo', TRUE);
	
		// @ pack : group_by 
		// $this->EUI_Page->_setGroupBy('a.deb_id');
		// echo $this ->EUI_Page->_getCompiler();
		if($this->EUI_Page->_get_query()) {
			return $this->EUI_Page;
		}
	}
	
	/*
	 * @ def 		: __construct // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_content()
	{
		
		$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
		$this->EUI_Page->_setPage(10);
		
	/** default of query ***/

		$this->EUI_Page->_setArraySelect(array(
			"a.ptp_id AS PTP_ID"=> array('PTP_ID','ID','primary'),
			"a.deb_id as deb_id" => array('deb_id', 'debid', 'hidden'),
			"e.CampaignDesc AS CampaignDesc"=> array('CampaignDesc','Product'),
			"b.deb_acct_no AS AccountNumber"=> array('AccountNumber','ID Pelanggan'),
			"b.deb_name AS CustomerName"=> array('CustomerName','Nama Pelanggan'),
			"a.ptp_date AS ptpdate "=> array('ptpdate','Tanggal PTP'),
			"a.ptp_amount AS ptp_amount "=> array('ptp_amount','Jumlah PTP'),
			"d.pch_name AS pch_name "=> array('pch_name','PTP Channel'),
			"ag.id AS UserId "=> array('UserId','Agent ID')
			
		));
		/*
		INNER JOIN  e ON 
			,
		a.deb_id,
		b.deb_name,
		a.ptp_date,
		a.ptp_amount,
		d.pch_name
		*/
		$this->EUI_Page->_setFrom("t_tx_ptp a");
		$this->EUI_Page->_setJoin("t_gn_debitur b","a.deb_id=b.deb_id","INNER");
		$this->EUI_Page->_setJoin("t_gn_assignment c","a.deb_id=c.CustomerId ","INNER");
		$this->EUI_Page->_setJoin("t_lk_payment_channel d ","a.ptp_chanel = d.id","LEFT");
		$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=c.AssignSelerId ","LEFT");
		$this->EUI_Page->_setJoin("t_gn_campaign e ","b.deb_cmpaign_id = e.CampaignId","LEFT", TRUE);
		
	// pack : process ---------------------------------------------------------------------------------
		 
		 $this->EUI_Page->_setAnd('c.AssignAdmin IS NOT NULL', FALSE);
		 $this->EUI_Page->_setAnd('c.AssignMgr IS NOT NULL', FALSE);
		 $this->EUI_Page->_setAnd('c.AssignSpv IS NOT NULL', FALSE);
		 $this->EUI_Page->_setAnd('e.CampaignStatusFlag',1);
		 $this->EUI_Page->_setAnd('a.is_delete',0);
		 
	// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
			
		if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
			array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
		{
			$this->EUI_Page->_setAnd('c.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
		}		
		
	 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
		
		if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
			array(USER_LEADER) ) )
		{
			 $this->EUI_Page->_setAnd('c.AssignLeader',$this->EUI_Session->_get_session('UserId'));
		}		

	// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
		
		if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
			array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
		{
			 $this->EUI_Page->_setAnd('c.AssignSelerId IS NOT NULL');
		}		
		
	// @ pack : set cache on page **/
		
		$this->EUI_Page->_setAndCache('b.deb_acct_no', 'ptp_cust_id', TRUE);
		$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'ptp_campaign_id',TRUE);
		$this->EUI_Page->_setAndCache('ag.UserId', 'ptp_agent_id', TRUE);
		$this->EUI_Page->_setLikeCache('a.deb_name', 'ptp_cust_name', TRUE);
		
	// @ pack : PTP date ---------------------------------------------------------------------------------
		
		$this->EUI_Page->_setAndOrCache("a.ptp_date>='". _getDateEnglish(_get_post('ptp_start_date')) ."'", 'ptp_start_date', TRUE);
		$this->EUI_Page->_setAndOrCache("a.ptp_date<='". _getDateEnglish(_get_post('ptp_end_date')) ."'", 'ptp_end_date', TRUE);
		
	// @ pack : PTP amount  ---------------------------------------------------------------------------------

		$this->EUI_Page->_setAndOrCache("a.ptp_amount>=". (INT)_get_post('ptp_start_amountwo') ."", 'ptp_start_amountwo', TRUE);
		$this->EUI_Page->_setAndOrCache("a.ptp_amount<=". (INT)_get_post('ptp_end_amountwo')."", 'ptp_end_amountwo', TRUE);
		
	// @ pack : group by  ---------------------------------------------------------------------------------
		
		// $this->EUI_Page->_setGroupBy('a.deb_id');
			
	/* set order by **/
		if( $this->URI->_get_have_post('order_by')) {
			$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
		}	
		
		$this->EUI_Page->_setLimit();
	}
	
	/*
	* @ def : _getCustomerByOtherNumberCIF 
	* --------------------------------------------------
	*/

	public function _getDeskollByLogin() 
	{
		$Deskoll = array();
		$_array_select = $this->M_SysUser->_get_user_by_login();
		foreach( $_array_select as $UserId => $rows ) 
		{
			$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
		}

		return $Deskoll;
	}
	
	/*
	 * @ def 		: _get_page_number // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _getGenderId()
	{
	  $_conds = array();
	  
	  $this->db->reset_select();
	  $this->db->select("a.GenderId, a.Gender",FALSE);
	  $this->db->from("t_lk_gender a");
	  
	  $qry = $this->db->get();
	  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
		$_conds[$rows['GenderId']] = $rows['Gender'];
	  }
	  
	  return $_conds;
	}
	
	/*
	 * @ def 		: __construct // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	 public function _getAccountStatus()
	{
		$_account_status = null;
		if( is_null($_account_status) )
		{
			$result = $this->M_SetCallResult->_getCallReasonId();
			foreach( $result as $AccountStatusCode => $rows ) {
				$_account_status[$AccountStatusCode] = $rows['name'];
			}	
		}  
			return $_account_status;
	}
	
	/*
	 * @ def 		: __construct // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	 public function _getLastCallStatus() 
	{
		$_last_call_status = null;
		if( is_null($_account_status) )
		{
			$result = $this->M_SetCallResult->_getCallReasonId();
			foreach( $result as $AccountStatusCode => $rows ) 
			{
				$_last_call_status[$AccountStatusCode] = $rows['name'];
			}	
		}   
		return $_last_call_status;		
	}
	
	/*
	 * @ def 		: _get_resource // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _getCampaignByActive() 
	{
		$_is_active = null;
		if( is_null($_is_active)) {
			$_is_active = $this->M_SetCampaign->_get_campaign_name();
		}
		return $_is_active;
	}
	
	/*
	 * @ def 		: _get_resource // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	 public function _get_resource()
	{
		self::_get_content();
		if(($this->EUI_Page->_get_query()!=="" )) 
		{
			return $this->EUI_Page->_result();
		}	
	}
	
	/*
	 * @ def 		: _get_page_number // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function _get_page_number() 
	{
		if( $this -> EUI_Page -> _get_query()!='' ) {
			return $this -> EUI_Page -> _getNo();
		}	
	}

}