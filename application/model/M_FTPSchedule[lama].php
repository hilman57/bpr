<?php
/**
 * @ def 		: FTPCopyCustomers
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
 
class M_FTPSchedule extends EUI_Model
{

private $_ignore_labels  = array();
private $_callfunc_users = array();
private $_policy_arrrays = array();
private $_primary_policy  = array();

/**
 * @ def 		: M_FTPSchedule
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: M_FTPSchedule --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 

public function M_FTPSchedule()
{	
	$this->_ignore_labels  = array('CustomerNumber');
	$this->_callfunc_users = array('Campaign_Master_Code' => 'CampaignId');
	
	$this->_primary_policy = array(
		'PolicyCreatedTs' 		=> date('Y-m-d H:i:s'),
		'PolicyUploadDate' 		=> date('Y-m-d H:i:s') 
	);
	
	$this->_policy_arrrays = array(
	  //t_gn_bucket_customers      t_gn_policy_detail
		'EnigmaId'				=> 'EnigmaId',
		'CIFNumber'				=> 'CIFNumber',
		'Major_Product'			=> 'PolicyMajorProduct',
		'Product_Name'			=> 'PolicyProductName',
		'Grace_Periode'			=> 'PolicyGracePriode',
		'Batch_Date'			=> 'PolicyBatchDate',
		'Last_Installment'		=> 'PolicyLastInstallment',
		'Product_type'			=> 'PolicyProductType',
		'Product_Type_detail'	=> 'PolicyProductTypeDetail',
		'Product_Category'		=> 'PolicyProductCategory',
		'Payment_Type'			=> 'PolicyPaymentType',
		'Coverage'				=> 'PolicyCoverage',
		'Beneficiery1'			=> 'PolicyBeneficiery1',
		'BeneficieryGender1'	=> 'PolicyBeneficieryGender1',
		'BeneficieryDOB1'		=> 'PolicyBeneficieryDOB1',
		'Beneficiery2'			=> 'PolicyBeneficiery2',
		'BeneficieryGender2'	=> 'PolicyBeneficieryGender2',
		'BeneficieryDOB2'		=> 'PolicyBeneficieryDOB2',
		'Sisa_Grace_Period'		=> 'PolicySisaGracePeriod',
		'Policy_Iss_Date'		=> 'PolicyIssDate',
		'Policy_Sales_Number'	=> 'PolicyNumber',
		'Rcd'					=> 'PolicyRCD',
		'Freq_Payment'			=> 'PaymentMode',
		'Premi'					=> 'PolicyPremi',
		'Life_Assured_Name'		=> 'PolicyFirstName',
		'Life_Assured_Gender'	=> 'PolicyGenderId',
		'Life_Assured_DOB'		=> 'PolicyDOB',
		'PTD'					=> 'PolicyPTD',
		'Product_Code'			=> 'PolicyProductCode',
		'Currency'				=> 'PolicyCurrency',
		'Payment_Method'		=> 'PolicyPayment_Method',
		'UL_Type'				=> 'PolicyUL_Type',
		'Campaign_Master_Code'	=> 'PolicyCampaign_Master_Code',
		'Ag_Aktif'				=> 'PolicyAg_Aktif',
		'Employee_ID'			=> 'PolicyEmployee_ID',
		'Last_Message_Collection'=> 'PolicyLast_Message_Collection',
		'Year_Premi'			=> 'PolicyYear_Premi',
		'Suspense'				=> 'PolicySuspense'
	);		
	
 }
 

/**
 * @ def 		: getFTP
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
 public function getFTP($id = 0 )
 {
	$data = array();
	$this->db->select('*');
	
	if($id){
		$this->db->where('ftp_read_id',$id );
	}
	
	$this->db->from('t_lk_ftp_read');
	
	foreach( $this->db->get() -> result_assoc() as $rows ) 
	{
		$data[$rows['ftp_read_id']]['uploadid']	 = $rows['ftp_read_id'];
		$data[$rows['ftp_read_id']]['project']	 = $rows['ftp_read_project_code'];
		$data[$rows['ftp_read_id']]['template']  = $rows['ftp_read_template_Id'];
		$data[$rows['ftp_read_id']]['incoming']  = $rows['ftp_read_directory'];
		$data[$rows['ftp_read_id']]['controll']  = $rows['ftp_read_ctltype'];
		$data[$rows['ftp_read_id']]['filetype']  = $rows['ftp_read_filetype'];
		$data[$rows['ftp_read_id']]['history']   = $rows['ftp_read_dir_history'];
		$data[$rows['ftp_read_id']]['privilege'] = $rows['ftp_read_privilege'];
		$data[$rows['ftp_read_id']]['userid'] = $rows['ftp_read_userid'];
	}
	
	return $data;
 }
 

/**
 * @ def 		: _setWillUpload
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
 public function _setWillUpload( $data = null, $model = null )
 {
	$CallBack = array('success' => 0, 'failed'=> 0, 'totals' => 0 );
	if( !is_null($data)  AND !is_null($model) ) 
	{
		$count = 0; $success = 0; $failed = 0; 
		
		foreach( $data as $sn => $rows ) 
		{
			foreach( $rows as $field => $values ) { 
				$this->db->set($field, $values);
			}
			
			$this->db->insert($model['tablename']);			
			if( $this->db->affected_rows() > 0 )
			{
				if( $BucketId = $this->db->insert_id() ){
					$success +=1; // yang berhasil 
				}
			}	
			else
				$failed +=1; // yang gagal upload 
				
				
			// set jumlah 	
			$count +=1;
		}
		
		$CallBack['CIFNumber'] = self::CIF($model);
		$CallBack['success'] = $success;
		$CallBack['failed'] = $failed; 
		$CallBack['totals'] = $count;
	}
	
	
	return $CallBack;
 }

/**
 * @ def 		: CampaignId
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
public function CampaignId( $CampaignCode = null ) 
{
 $rs = null;
 if( !is_null($CampaignCode) )
 {
	$this->db->select("a.CampaignId");
	$this->db->from("t_gn_campaign a");
	$this->db->where("a.CampaignCode",$CampaignCode);
	
	if( $rows = $this ->db->get()->result_first_assoc() ) {
		$rs = $rows['CampaignId'];
	}
}

return $rs;	
}

/**
 * @ def 		: CIF
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
 
 private function CIF( $Bucket=NULL )
 {
	$autonumber = array();
	
	$this->load->model('M_Utility');
	$this->db->select('a.*');
	$this->db->from('t_gn_bucket_customers a');
	$this->db->where('a.BukcetSourceId', $Bucket['sourceid']);
	$this->db->group_by('a.CustomerNumber');
	
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{
		$this->db->set('CIFNumber',$rows['CIFNumber']);
		$this->db->set('BucketId',$rows['BucketId']);
		$this->db->set('UploadId',$rows['BukcetSourceId']);
		$this->db->set('UploadTs',date('Y-m-d H:i:s'));
		$this->db->insert('t_gn_cif_customer');
		if( $this->db->affected_rows() > 0 ) 
		{
			$_autoID= $this->db->insert_id();
			if($_autoID) 
			{
				if( $ID = $this->M_Utility->getAutoprefixNumber($_autoID, $Bucket['project']) )
				{
					$autonumber[$ID] = $ID;
					$this->db->set('EnigmaId', $ID);
					$this->db->where('Id', $_autoID);
					$this->db->update('t_gn_cif_customer');
				}
			}
		}
	}
	
	return $autonumber;
}

/**
 * @ def 		: setAssignmentDataByCustomers
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
private function setAssignmentDataByCustomers( $CustomerId=null, $fs=null )
{

  $cond = 0;
  if( !is_null($CustomerId) AND ($CustomerId!=FALSE) ) 
  {
	$Levels = $fs['privilege'];	
	
	/** level LEADER ---------------------------------------> **/
	
	if( $Users  = $this->M_SysUser->_getUserDetail($fs['userid']) )
	{
		if( ($Levels == USER_LEADER) ) 
		{
			$this->db->set('AssignAdmin',$Users['admin_id']);
			$this->db->set('AssignMgr',$Users['mgr_id']);
			$this->db->set('AssignSpv',$Users['spv_id']);
			$this->db->set('AssignLeader',$Users['tl_id']);
		}
							
		/** levelSUPERVISOR---------------------------------------> **/
		
		if(($Levels==USER_SUPERVISOR))
		{
			$this->db->set('AssignAdmin',$Users['admin_id']);
			$this->db->set('AssignMgr',$Users['mgr_id']);
			$this->db->set('AssignSpv',$Users['spv_id']);
		}
							
		/** level MANAGAER---------------------------------------> **/
					
		if(($Levels==USER_MANAGER)) {	
			$this->db->set('AssignAdmin',$Users['admin_id']);
			$this->db->set('AssignAmgr', $Users['act_mgr']);
			$this->db->set('AssignMgr',$Users['mgr_id']);
		}
							
		/** levelMANAGAER---------------------------------------> **/
		
		if(($Levels==USER_ADMIN))
		{
			$this->db->set('AssignAdmin',$Users['admin_id']);
		}
	}
					
	$this->db->set("CustomerId",$CustomerId);
	$this->db->set("AssignDate",date('Y-m-d H:i:s'));
	$this->db->set("AssignMode","DIS");
	$this->db->insert('t_gn_assignment');
	
	if($this->db->affected_rows()>0) $cond++;
	
  }
  
  return $cond;
  
} 

/**
 * @ def 		: getBucketCustomerByAutonumberForPolicy
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/

private function getBucketCustomerByAutonumberForPolicy( $_autonumber = null, $sourceid = 0  )
{
	$conds = null;
	
	if(!is_null($_autonumber) )
	{ 
		$this->db->select('*');
		$this->db->from('t_gn_cif_customer a ');
		$this->db->join('t_gn_bucket_customers b ','a.CIFNumber=b.CIFNumber', 'LEFT');
		$this->db->where('a.EnigmaId',$_autonumber);
		$this->db->where('b.BukcetSourceId',$sourceid);
		
		if( is_null($conds) ) {
			$conds = $this->db->get()->result_assoc();
		}
	}	
	
	return $conds;	
} 
 

/**
 * @ def 		: getBucketCustomerByAutonumber
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/

private function getBucketCustomerByAutonumber( $_autonumber = null )
{
	$conds = null;
	if(!is_null($_autonumber) )
	{ 
		$this->db->select('*');
		$this->db->from('t_gn_cif_customer a ');
		$this->db->join('t_gn_bucket_customers b ','a.BucketId=b.BucketId', 'INNER');
		$this->db->where('a.EnigmaId',$_autonumber);
		if( $rows_array = $this->db->get()->result_first_assoc()) 
		{
			$conds = $rows_array;
		}
	}	
	
	return $conds;	
} 
 
/*
 * @ def 		: FTPCopyPolicyDetail
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 */

 
public function FTPCopyPolicyDetail($fs = null, $autonumber = null )
{
	$conds = 0;
	
	foreach($autonumber['CIFNumber'] as $keys => $CustomerNumber ) 
	{
		foreach($this->getBucketCustomerByAutonumberForPolicy($CustomerNumber, $fs['sourceid']) as $rows )
		{ 
			foreach($rows as $label => $value ) 
			{
				if(in_array($label, array_keys($this->_policy_arrrays))) 
					$this->db->set($this->_policy_arrrays[$label],$value);
				
				if( is_array($this->_primary_policy))
					foreach( $this->_primary_policy as $PolicyLabel => $value )
				{
					$this->db->set($PolicyLabel,$value);
				}
			}
			
			$this->db->insert('t_gn_policy_detail');
			if( $this->db->affected_rows() > 0 ) $conds++;
		}
	}
	
	return $conds;
} 
 
/*
 * @ def 		: FTPCopyCustomers
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: date upload::array 
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 */

public function FTPCopyCustomers( $fs = null, $autonumber = null )
 {
	$fields = array();
	$fieldsname = array_values($this->db->list_fields("t_gn_debitur"));
	
	if( !is_null($fs) 
		AND ($fs['sourceid']!='') AND ($fs['uploadid']!='') )
	{
		foreach($autonumber['CIFNumber'] as $keys => $CustomerNumber )
		{
			if( $rows = $this->getBucketCustomerByAutonumber($CustomerNumber) )
			{
				foreach(array_keys($rows) as $n => $labels ) 
				{
					/** default all data ***/  
					
					if( in_array($labels, $fieldsname) 
						AND ( in_array($labels, $this->_ignore_labels)!= TRUE) ) {
							$this->db->set($labels, $rows[$labels]);
					}	
					
					/** get customer number :: uniq ***/
					
					if( in_array( $labels, $this->_ignore_labels ) ) 
						$this->db->set( $labels, $CustomerNumber );
					
					/** get call user function **/
					
					if( in_array( $labels, array_keys($this->_callfunc_users) ) ) {
						$_callfunc_users = $this->_callfunc_users[$labels];
						$this->db->set($_callfunc_users, $this->{$_callfunc_users}($rows[$labels]));
					}	
				}
				
				/* insert data to customer ***/
			
				$this->db->insert('t_gn_debitur');
				if( $this->db->affected_rows() > 0 ){
					$this->setAssignmentDataByCustomers((INT)$this->db->insert_id(), $fs );
				}
			}
		}	
	}	
 }
 

}


?>