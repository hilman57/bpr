<?php

class M_ModApprovePhone extends EUI_Model 
{


// @ pack : set approve phone status 

 var $_request_phone_status = null;
 var $_approve_phone_status = null;	
 var $_History = null;
 
 
// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */

 private static $Instance  = null;
 public static function &Instance() {
	if( is_null(self::$Instance) ){
		self::$Instance = new self();
	}
	return self::$Instance;
}


// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */
 
 public function __construct()
{
	$this->load->model(array('M_ModContactDetail','M_SysUser', 'M_SrcCustomerList', 'M_RefAddPhoneType','M_Loger', 'M_ModCallHistory'));
	if( is_null($this->_request_phone_status) ){
		$this->_request_phone_status = array(103,104); // request && pending 
		$this->_approve_phone_status = array(101,102); // approve && reject 
	}
	$this->_History=& get_class_instance('M_ModCallHistory');
}

// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */
 
 public function _get_default() 
{

/*
 * @ pack : set select # -------------------------
 */
 
  $this->EUI_Page->_setPage(10);
  $this->EUI_Page->_setSelect('a.addphone_id');
  $this->EUI_Page->_setFrom("t_gn_additional_phone a");
  
  $this->EUI_Page->_setJoin("t_lk_additional_phone_type b "," a.addphone_type=b.adp_code","LEFT");
  $this->EUI_Page->_setJoin("t_tx_agent c "," a.addphone_request_user=c.UserId","LEFT");
  $this->EUI_Page->_setJoin("t_lk_aprove_status d "," a.addphone_approve_status=d.AproveCode","LEFT");
  $this->EUI_Page->_setJoin("t_gn_debitur e "," a.deb_id=e.deb_id", "LEFT", TRUE);


// @ pack : on filter 
  
 $this->EUI_Page->_setLikeCache("e.deb_acct_no", "phone_customerid", TRUE);
 $this->EUI_Page->_setLikeCache("e.deb_name", "phone_customername", TRUE);
 $this->EUI_Page->_setAndCache("c.UserId", "phone_deskoll", TRUE);
 $this->EUI_Page->_setAndCache("a.addphone_no", "phone_phonenumber", TRUE);
 $this->EUI_Page->_setAndCache("a.addphone_type", "phone_phonetype", TRUE);
 $this->EUI_Page->_setAndCache("e.deb_cmpaign_id", "phone_product", TRUE);

// @ pack : filter by default session  
 
  if(in_array(_get_session('HandlingType'), 
	array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND)))
 {
	 $this->EUI_Page->_setAnd('c.UserId', $this->EUI_Session->_get_session('UserId'));
  }	
  
 // @ pack : filter by default session 
 
  if( in_array(_get_session('HandlingType'),
	array(USER_LEADER, USER_SUPERVISOR, USER_ADMIN, USER_SENIOR_TL)))
  {
	  $this->EUI_Page->_setWhereIn('a.addphone_approve_status', $this->_request_phone_status);
  }	
  
  // echo $this->EUI_Page->_getCompiler();
// @ pack : process 
  
 if($this->EUI_Page->_get_query()) {
	return $this->EUI_Page;
 }
 
	
 }

/*
 * @ def 		: _get_content
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_content() 
{

/*
 * @ pack : set select its. 
 */
 $this->EUI_Page->_postPage(_get_post('v_page'));
 $this->EUI_Page->_setPage(10);
	

/*
 * @ pack : set select its. 
 */
 $this->EUI_Page->_setArraySelect(array(
	"a.addphone_id AS AddPhoneId" => array("AddPhoneId","ID","primary"), 
	"a.deb_id AS DebiturId" => array("DebiturId","DebiturId","hidden"), 
	"h.CampaignCode As CampaignCode" => array("CampaignCode","Product."),
	"e.deb_acct_no AS AccountNo" => array("AccountNo","Akun No."), 
	"e.deb_name AS CustomerName" => array("CustomerName","Nama Pelanggan."),
	"c.full_name AS UserFullname" => array("UserFullname","Request By."),
	"a.addphone_created_ts AS addphone_created_ts" => array("addphone_created_ts","Tanggal Request  ."),
	"a.addphone_no AS addphone_no" => array("addphone_no","Nomor Telepon."),
	"b.adp_description AS adp_description" => array("adp_description","Tipe Telepon."),
	"g.RelationshipTypeDesc as FamilyDesc" => array("FamilyDesc","Relationship"),
	"f.full_name AS ApproveBy" => array("ApproveBy","Approve Oleh."),
	"a.addphone_approve_ts AS ApproveDate" => array("ApproveDate","Tanggal Approval."),
	"d.AproveName AS AproveName" => array("AproveName","Approval Status.")
	
	
 ));
 
/*
 * @ pack : set select join. 
 */		
  
  $this->EUI_Page->_setFrom("t_gn_additional_phone a");
  $this->EUI_Page->_setJoin("t_lk_additional_phone_type b "," a.addphone_type=b.adp_code","LEFT");
  $this->EUI_Page->_setJoin("t_tx_agent c "," a.addphone_request_user=c.UserId","LEFT");
  $this->EUI_Page->_setJoin("t_lk_aprove_status d "," a.addphone_approve_status=d.AproveCode","LEFT");
  $this->EUI_Page->_setJoin("t_gn_debitur e "," a.deb_id=e.deb_id", "LEFT");
  $this->EUI_Page->_setJoin("t_lk_relationshiptype g "," a.addphone_family_code =g.RelationshipTypeCode", "LEFT");
  $this->EUI_Page->_setJoin("t_gn_campaign h ","e.deb_cmpaign_id=h.CampaignId", "LEFT");
  $this->EUI_Page->_setJoin("t_tx_agent f "," a.addphone_approve_user=f.UserId", "LEFT",TRUE);
  
// @ pack : on filter 
  
  $this->EUI_Page->_setLikeCache("e.deb_acct_no", "phone_customerid", TRUE);
  $this->EUI_Page->_setLikeCache("e.deb_name", "phone_customername", TRUE);
  $this->EUI_Page->_setAndCache("c.UserId", "phone_deskoll", TRUE);
  $this->EUI_Page->_setAndCache("a.addphone_no", "phone_phonenumber", TRUE);
  $this->EUI_Page->_setAndCache("a.addphone_type", "phone_phonetype", TRUE);
  $this->EUI_Page->_setAndCache("e.deb_cmpaign_id", "phone_product", TRUE);
 
 // @ pack : filter by default session  
 
  if(in_array(_get_session('HandlingType'), array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND))){
	$this->EUI_Page->_setAnd('c.UserId', $this->EUI_Session->_get_session('UserId'));
	 
  }	
  
 // @ pack : filter by default session 
 
  if( in_array(_get_session('HandlingType'),
	array(USER_LEADER, USER_SUPERVISOR, USER_ADMIN)))
  {
	   $this->EUI_Page->_setWhereIn('a.addphone_approve_status',$this->_request_phone_status);
  }			
	
 // @ pack : order_by 
	
  if( _get_have_post('order_by')){
	$this->EUI_Page->_setOrderBy(_get_post('order_by'),_get_post('type'));
  } else {
    $this->EUI_Page->_setOrderBy("a.addphone_id","DESC");
  }
 
 // @ pack : test 
 
  $this->EUI_Page->_setLimit(); 
	
  }
  
  
  
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_resource()
 {
	self::_get_content();
	
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }

/* 
 * @ pack : selected rows data by ID 
 * 
 */

 public function _find_rows_by_id( $find = null )
{
 $result = array();
 $this->db->reset_select();
 $this->db->select("*");
 $this->db->from("t_gn_additional_phone ");

// @ pack : not null  	

 if( !is_null($find) ) 
 {
	if( is_array( $find ) ) foreach( $find as $field => $value ) 
	{
		if( !is_array($value) ){
			$this->db->where_in($field, array($value));
		} else {
			$this->db->where_in($field, $value);
		}
	} else {
		$this->db->where_in('addphone_id', array($find));
	}
 }
	
// @ execute data 
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ) {
		$result = (array)$qry->result_first_assoc();
	}
	
	return $result ;
} 
 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getApproveItem()
 {
	$_conds = array();
	
	$this -> db->select('*');
	$this -> db->from('t_lk_approvalitem');
	
	foreach( $this -> db->get()->result_assoc() as $rows )
	{
		$_conds[$rows['ApprovalItemId']] = $rows['ApprovalItem'];
	}
	
	return $_conds;
	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setApproveItem( $items =null )
{
 
 $_conds = 0;
 $_code = _get_post('ApproveCode');
 $_Customer = get_class_instance('M_ModContactDetail');
 
 if(is_array($items) )
 {
   foreach( $items as $key => $ApproveId )
  {
	 $this->db->set("addphone_approve_status",$_code);
	 $this->db->set("addphone_approve_user",_get_session('UserId'));
	 $this->db->set("addphone_approve_status",$_code);
	 $this->db->set("addphone_approve_ts",date('Y-m-d H:i:s') );
	 $this->db->where("addphone_id",$ApproveId);
	 if( $this->db->update("t_gn_additional_phone") )
	 {
	 
	 // @ get reference of data ==================================>
	 
		  $find = $this->_find_rows_by_id($ApproveId);
		  $Users = $this->M_SysUser->_getUserByCode( array('UserId'=>$find['addphone_request_user']));
		  $Debitur = $_Customer->_get_select_debitur($find['deb_id']);
		  
	// @ get reference of data ==================================>
	 
	 
		  if( is_array($find) 
			AND is_array($Users) )
		 {
			  $this->db->set('CustomerId', $find['deb_id']);
			  $this->db->set('CallReasonId', $Debitur['deb_prev_call_status_code']);
			  $this->db->set('CallAccountStatus',$Debitur['deb_call_status_code']);
			  $this->db->set('ApprovalStatusId',$find['addphone_approve_status']);
			  $this->db->set('CreatedById', $find['addphone_request_user']);
			  $this->db->set('TeamLeaderId', $Users['tl_id']);
			  $this->db->set('SupervisorId', $Users['spv_id']);
			  $this->db->set('UpdatedById', _get_session('UserId'));
			  $this->db->set('CallHistoryNotes', " APPROVE ADDITIONAL PHONE - ". _setMasking($find['addphone_no']));
			  $this->db->set('CallHistoryCreatedTs',date('Y-m-d H:i:s'));
			  $this->db->set('CallHistoryUpdatedTs', date('Y-m-d H:i:s'));
			  $this->db->set('PhoneType',$find['addphone_type']);
			  $this->db->insert('t_gn_callhistory');
			  
		 }
		 $_conds++;
	 }
  }
  
 }

 // @ pack : set_activity_log
 
	$this->M_Loger->set_activity_log("APPROVAL_PHONE -> CODE[$_code] -> ".implode(',',$items));
	return $_conds;
	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function _getCustomerId($ApproveId=0)
 {
	$_conds = 0;
	
	$this ->db->select('a.CustomerId');
	$this ->db->from('t_gn_approvalhistory a');
	$this ->db->where('a.ApprovalHistoryId',$ApproveId);
	
	$_conds = $this ->db->get()->result_singgle_value();
	
	return $_conds;
	
 }
  
 /*
 * @ def 		: _SaveSubmitPhone // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function _getPhoneTypeIdByName( $Name = null ) 
 {
	$this ->db ->select('a.PhoneTypeId');
	$this ->db ->from('t_lk_phonetype a ');
	$this ->db ->where('a.PhoneField', $Name);
	
	if( $rows = $this ->db ->get() -> result_first_assoc() )
	{
		return $rows['PhoneTypeId'];
	}
	else
		return null;
 }
 
/*
 * @ pack : _SaveSubmitPhone
 */
 public function _SaveSubmitPhone( $params = null )
{
  $_conds = 0;
   if( !is_null($params) )
  {
	 $this->db->set("deb_id",$params['AddCustomerId']);
	 $this->db->set("addphone_type",$params['PhoneSelectTypeId']);
	 $this->db->set("addphone_no",$params['PhoneAddTypeValueId']);
	 $this->db->set("addphone_family_code",$params['RelationshipTypeId']);
	 $this->db->set("addphone_created_ts",date('Y-m-d H:i:s'));	
	 $this->db->set("addphone_request_user",_get_session('UserId'));	
	 $this->db->insert("t_gn_additional_phone");
	
	 if( $this->db->affected_rows() > 0 ){
		$this->M_Loger->set_activity_log("
		ADD_PHONE->TYPE[{$params['PhoneAddType']}]->VALUE[{$params['PhoneAddTypeValue']}]->
		REL[{$params['RelationshipTypeCode']}]->CUST[{$params['CustomerId']}]");
		
		$_conds++;
	 }
  }
  return $_conds;
  
 }
 
 
// ----------------------------------------------------------------------------
/*
 * @ packaage SaveMultiplePhone
 * 
 */
 
 public function _SaveMultiplePhone( $out= null ) 
{
 if( is_null($out) OR !is_object($out) ) {
	return false;
 }
 
// ---------------- select on write ---------------------------------
 $this->db->reset_write();
 $this->db->set("deb_id", $out->get_value('CustomerId','intval'));
 $this->db->set("addphone_type", $out->get_value('PhoneTyped','strval'));
 $this->db->set("addphone_family_code", $out->get_value('PhoneRelation','strval'));
 $this->db->set("addphone_no", $out->get_value('PhoneNumber','trim'));
 $this->db->set("addphone_created_ts", date('Y-m-d H:i:s'));
 $this->db->set("addphone_request_user",_get_session('UserId'));
	
 $this->db->duplicate("addphone_family_code", $out->get_value('PhoneRelation', 'strval'));
 $this->db->duplicate("addphone_no", $out->get_value('PhoneNumber','trim'));
 $this->db->duplicate("addphone_created_ts", date('Y-m-d H:i:s'));
 $this->db->duplicate("addphone_request_user",_get_session('UserId'));
 
  if( $this->db->insert_on_duplicate("t_gn_additional_phone") )
 {
	return TRUE;
 }
 return FALSE;
 
} 
 
// ================= END CLASS ======================
 
}

?>