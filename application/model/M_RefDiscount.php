<?php
/*
 * @ pack : under model M_ModulRPC
 // */

class M_RefDiscount extends EUI_Model {
/*
 * @ pack : under model M_InfoPTP
 */

var $_table_space = array();
/*
 * @ pack : under model M_InfoPTP
 */
/*
 * @ pack : _aksessor 
 */ 
private static $Instance = null;
 
/*
 * @ pack : _aksessor 
 */ 
 
 public static function &Instance()
{
  if( is_null(self::$Instance) ) {
		self::$Instance = new self();
	}
	
  return self::$Instance;	
  
} 
 

public function M_RefDiscount()
{
  $this->_table_space = array('t_lk_discount');
}
/*
 * @ pack : select all
 */

 public function _get_select_disc()
{
  $remote_place_with = array();
  $this->db->reset_select();
  $this->db->select('*',FALSE);
  $this->db->from(reset($this->_table_space));
  $this->db->where('disc_flags', 1);

  $qry = $this->db->get();
  if( $qry->num_rows() > 0 )
   foreach( $qry ->result_assoc() as $rows )
  {
	$remote_place_with[$rows['disc_code']] = $rows['disc_name'];
  }

 return $remote_place_with;

}
/*
 * @ pack : select all
 */


 public function _get_detail_disc( $argv_vars = null )
{
  $conds = array();

  $this->db->reset_select();
  $this->db->select('*',FALSE);
  $this->db->from(reset($this->_table_space));
  $this->db->where('id', $argv_vars);


  //echo $this->db->_get_var_dump();

  $qry = $this->db->get();
  if( $qry->num_rows()>0 )
  {
	$conds = $qry->result_first_assoc();
  }

  return $conds;

}
/*
 * @ pack : select all
 */

 public function _get_field_disc()
{
	return $this->db->list_fields(reset($this->_table_space));
}
/*
 * @ pack : select all
 */

 public function _get_component_disc()
{
	return array (
		'primary' => array('id'),
		'combo' => array('disc_flags'),
		'input' => array('disc_code', 'disc_name', 'disc_description'),
		'option' => array('1'=>'Active','0'=>'Not Active')
	);
}
/*
 * @ pack : _set_save_spc
 */

public function _set_save_disc( $arg_vars = null )
{
  $conds = 0;
  if( is_array($arg_vars) )
  {
	foreach( $arg_vars as $field => $value ) {
		$this->db->set($field, $value);
	}

	// @ pack : set save data
	$this->db->insert(reset($this->_table_space));
	if( $this->db->affected_rows() > 0 ) {
		$conds++;
	}
  }

 return $conds;

}
/*
 * @ pack : _set_save_spc
 */

 public function _set_update_disc( $argv_vars = null )
{
 $conds = 0;

 // @ pack : reset all select query ..

 $this->db->reset_select();

 // @ pack : cek argument set vars ..

 if( is_array($argv_vars) )
	foreach( $argv_vars as $field => $value )
 {
	if(in_array($field,array('id') ) ) {
		$this->db->where($field, $value);
	} else {
		$this->db->set($field, $value);
	}
  }

  // @ pack : execute query from here ..

  $this->db->update(reset($this->_table_space));
  if($this->db->affected_rows()>0){
	$conds++;
  }

  return $conds;

}
/*
 * @ pack : _set_save_spc
 */

public function _set_delete_disc( $discId = NULL )
{
  $conds = 0;
  if( is_array($discId) )
	foreach( $discId as $PrimaryId => $value )
  {
	  $this->db->where('id', $value);
      $this->db->delete(reset($this->_table_space));

	  if( $this->db->affected_rows() > 0 )
	  {
		 $conds++;
	  }
  }

  return $conds;
}
/*
 * @ pack : _get_default
 */

 public function _get_default_disc()
{
  $this->EUI_Page->_setPage(10);
  $this->EUI_Page->_setSelect("*");
  $this->EUI_Page->_setFrom(reset($this->_table_space), TRUE);

  // @ pack : set filter --------------------

  if( $this->URI->_get_have_post('keywords')){
	  $this->EUI_Page->_setAnd("
			id LIKE '%{$this->URI->_get_post('keywords')}%'
			OR disc_code LIKE '%{$this->URI->_get_post('keywords')}%'
			OR disc_name LIKE '%{$this->URI->_get_post('keywords')}%'
			OR disc_description LIKE '%{$this->URI->_get_post('keywords')}%'");

	}

 // @ pack : process its

  if( $this->EUI_Page->_get_query() ) {
		return $this->EUI_Page;
  }
}
/*
 * @ pack : _get_default
 */

 public function _get_content_disc()
{
  $this->EUI_Page->_postPage($this->URI->_get_post('v_page') );
  $this->EUI_Page->_setPage(10);

// @ pack : set filter --------------------

  $this->EUI_Page->_setArraySelect(array(
		"id AS DiscId" => array("DiscId","ID","primary"),
		"id AS ID" => array("ID","ID"),
		"disc_code AS DiscCode" => array("DiscCode","Discount Code"),
		"disc_name AS DiscName" => array("DiscName","Discount Name"),
		"disc_description as DiscDescription" => array("DiscDescription","Discount Description"),
		"IF(disc_flags=1,'Active','Not Active') AS DiscFlags" => array("DiscFlags","Status")
   ));

   $this->EUI_Page->_setFrom(reset( $this->_table_space), TRUE);

 // @ pack : set filter --------------------

   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd("
			id LIKE '%{$this->URI->_get_post('keywords')}%'
			OR disc_code LIKE '%{$this->URI->_get_post('keywords')}%'
			OR disc_name LIKE '%{$this->URI->_get_post('keywords')}%'
			OR disc_description LIKE '%{$this->URI->_get_post('keywords')}%'");
  }


  if( $this->URI->_get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy($this ->URI->_get_post('order_by'),$this ->URI->_get_post('type'));
  }


  $this -> EUI_Page->_setLimit();
}
/*
 * @ pack : _get_default
 */

public function _get_resource_disc()
 {
	self::_get_content_disc();
	if( $this -> EUI_Page -> _get_query()!='')
	{
		return $this -> EUI_Page -> _result();
	}
 }

/*
 * @ pack : _get_default
 */
public function _get_page_number_disc()
 {
	if( $this->EUI_Page->_get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}
 }
// END CLASS

}
?>