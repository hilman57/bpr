<?php
/*
 * EUI Model  
 *
 
 * Section  : < M_User > get information user on table 
 * author 	: razaki team  
 * link		: http://www.razakitechnology.com/eui/controller 
 */

class M_Menu extends EUI_Model
{

	/*
 * @ aksesor 	:  
 * --------------------------------------
 * @ test 
 * @ result 
 */

	public function M_Menu()
	{
		// run & first load	
	}

	/*
 * @ def 	: UserWorkApplication on this apps modul 
 * --------------------------------------
 * @ param 	: - 
 * @ param 	: - 
 */

	protected function UserWorkApplication()
	{
		static $config = NULL;

		$this->db->reset_select();

		$this->db->select('a.MenuId');
		$this->db->from('t_gn_menu_project a');
		$this->db->where_in('a.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
		// var_dump($this->db->last_query());
		// die;

		/** if login with not user show in active only **/

		if ($this->EUI_Session->_get_session('HandlingType') != USER_ROOT) {
			$this->db->where('a.MenuActive', 1);
		}

		foreach ($this->db->get()->result_assoc() as $rows) {
			$config[$rows['MenuId']] = $rows['MenuId'];
		}

		return $config;
	}

	/*
 * @ def	: set get_base_url 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */

	function _get_acess_menu()
	{
		$_merger_menu = array();
		$_user_menu 	= self::_get_list_menu();
		$_user_akses 	= self::_get_akses_menu();
		$_UserWork	= &self::UserWorkApplication();

		if (is_array($_user_menu)) {
			foreach ($_user_menu as $k => $menu) {
				$sql = " SELECT a.id, a.menu, a.file_name, 
				 	 a.el_id ,a.updated_by , 
					 b.GroupName, b.GroupId, a.images
			   FROM t_gn_application_menu a 
			   LEFT JOIN t_gn_group_menu b ON a.group_menu=b.GroupId
			   WHERE a.group_menu = '$menu' 
			   AND a.flag=1 ORDER BY a.OrderId ASC ";

				$qry = $this->db->query($sql);
				// print_r($this->db->last_query());
				// die;
				foreach ($qry->result_assoc() as $rows) {
					if (in_array($rows['id'], $_user_akses)) {

						if (
							is_null($_UserWork)
							and ($this->EUI_Session->_get_session('HandlingType') == USER_ROOT)
						) {
							$_merger_menu[$rows['GroupName']][$rows['id']] = array(
								'file_name' => $rows['file_name'],
								'menu' 		=> $rows['menu'],
								'id' 		=> $rows['el_id'],
								'groupid' 	=> $rows['GroupId'],
								'images'	=> $rows['images'],
								'style' 	=> 'cssmenus'
							);
						} else {
							if (in_array($rows['id'], $_UserWork)) {
								$_merger_menu[$rows['GroupName']][$rows['id']] = array(
									'file_name' => $rows['file_name'],
									'menu' 		=> $rows['menu'],
									'id' 		=> $rows['el_id'],
									'groupid' 	=> $rows['GroupId'],
									'images'	=> $rows['images'],
									'style' 	=> 'cssmenus'
								);
							}
						}
					}
				}
			}
		}
		return $_merger_menu;
	}

	/**
	 **
	 **/

	function _get_list_menu()
	{
		$this->db->reset_select();
		$_array_menu = array();

		$sql = " SELECT menu_group FROM t_gn_agent_profile WHERE id='" . $this->EUI_Session->_get_session('HandlingType') . "'";
		$qry = $this->db->query($sql);
		if (!$qry->EOF()) {
			$rows = $qry->result_first_assoc();
			if (($rows['menu_group'] != FALSE) && (!is_null($rows['menu_group']))) {
				$_array_menu  = explode(',', $rows['menu_group']);
			}
		}
		return $_array_menu;
	}
	/*
 * @ def	: set get_base_url 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */

	function _get_akses_menu()
	{
		$this->db->reset_select();
		$_array_menu = array();
		$sql = " SELECT menu FROM t_gn_agent_profile WHERE id='" . $this->EUI_Session->_get_session('HandlingType') . "'";
		$qry = $this->db->query($sql);
		// print_r($this->db->last_query());
		// die;
		if (!$qry->EOF()) {
			$rows = $qry->result_first_assoc();
			if (($rows['menu'] != FALSE) && (!is_null($rows['menu']))) {
				$_array_menu  = explode(',', $rows['menu']);
			}
		}
		return $_array_menu;
	}
}
