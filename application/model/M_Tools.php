<?php

class M_Tools extends EUI_Model
{

var $__key= null ;

/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function M_Tools()
{
	if( is_null( $this ->__key ) )
	{
		$this ->__key = array
		(
			"CustomerDOB" 			 => "CustomerDOB",
			"Life_Assured_DOB"		 => "CustomerDOB",
			"BeneficieryDOB1"		 => "CustomerDOB",
			"BeneficieryDOB2" 		 => "CustomerDOB",
			"Policy_Iss_Date"		 => "CustomerDOB",
			"PolicyIssDate"			 => "CustomerDOB",
			"Last_Installment"		 => "CustomerDOB",
			"PTD"					 => "CustomerDOB",
			"Rcd"					 => "CustomerDOB",
			"Batch_Date"			 => "CustomerDOB",
			"GenderId" 				 => "GenderId",
			"Employee_Gender"		 => "GenderId",
			"BeneficieryGender1"	 => "GenderId",
			"BeneficieryGender2"	 => "GenderId",
			"Life_Assured_Gender"	 => "GenderId",	
			"CustomerHomePhoneNum" 	 => "CustomerHomePhoneNum",
			"CustomerMobilePhoneNum" => "CustomerMobilePhoneNum",
			"CustomerWorkPhoneNum" 	 => "CustomerWorkPhoneNum",
			"Employee_Ph_No"		 => "CustomerMobilePhoneNum",
			"Marital_Status" 		 => "Marital_Status",
			"Freq_Payment"           => "PayMode",
			// "ProvinceId"			 => "ProvinceId",
			"Tel_1" 				 => "Tel_1",
			"Tel_2" 				 => "Tel_2",
			"Tel_3" 				 => "Tel_3",
			"Tel_4" 				 => "Tel_4"
		);
	}
}




/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function __tools_format( $keys = null, $values=null )
 {
	if( !is_null($keys) )
	{
		if( in_array($keys, array_keys($this ->__key) ) ) 
		{
			$callfunc_users = $this ->__key;
			return $this ->{$callfunc_users[$keys]}($values);
		}
		else
			return $values;
	}
	else
		return null;
	
 } 
 
/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function CustomerDOB( $date = null ) // m/d/y 
 {
	$dates = NULL;
	
	if(!is_null($date) ) 
	{
		if( preg_match("/".  preg_quote($date, "/")  ."/", $date)) {
			$date = preg_replace('/\//','-',$date);	
		}
		
		$dates = date('Y-m-d', strtotime($date));
	}
	
	return $dates;
	
 }
 
/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function PayMode( $PayMode = null ) // m/d/y 
{ 

	$result = null;
	
	if(!is_null($PayMode) AND strlen($PayMode)==2
		// AND is_integer($PayMode)
		){
		$_arrs_paymode = array ( 
			'12' 	=> array('id' => '2', 'name' => 'Monthly'),
			'02' 	=> array('id' => '4', 'name' => 'SemiAnnually'),
			'04' 	=> array('id' => '3', 'name' => 'Quarterly'),
			'01' 	=> array('id' => '1', 'name' => 'Annually')
		);
		
		$result = $_arrs_paymode[$PayMode]['id'];
		return $result;
	}
	
	if(!is_null($PayMode) AND is_string($PayMode)){
		$_arrs_paymode = array ( 
			'monthly' 		=> array('id' => '2', 'name' => '1'),
			'semiannually' 	=> array('id' => '4', 'name' => '6'),
			'quarterly' 	=> array('id' => '3', 'name' => '4'),
			'annually' 		=> array('id' => '1', 'name' => '12')
		);
		
		$result = $_arrs_paymode[strtolower($PayMode)]['id'];
	}
	
	return $result;
}

/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function ProvinceId( $Province = null ) // m/d/y 
{ 
	$this->load->model('M_Combo'); $conds = 0;
	$_arr_province = $this->M_Combo->_getProvince();
	foreach( $_arr_province as $id => $provincename )
	{
		if(strcasecmp(strtolower($Province), strtolower($provincename))==0 )
		{
			$conds = $id;
		}
	}
	
	return $conds;
}

/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function GenderId( $gender = null ) // m/d/y 
 {
	$GenderID = NULL;
	if( !is_null($gender) ) 
	{
		if( preg_match("/\M/i", $gender) 
			OR preg_match("/\m/i", $gender) )
		{
			$GenderID = 1;
		}	
		if( preg_match("/\F/i", $gender) 
			OR preg_match("/\f/i", $gender) )
		{
			$GenderID = 2;
		}
		
	}
	
	return $GenderID;
	
 } 
 
 /*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function Marital_Status( $Marital_Status = null )
 {
	$this->load->model('M_Combo');
	$ismaried = $this->M_Combo->_getMaried();
	
	$conds = null;
	if(is_array($ismaried) )
	foreach( $ismaried as $code => $string )
	{
		if(preg_match("/^$Marital_Status/i", $string) ){
			$conds = $code;
			break;
		}	
	}
	
	return $conds;
 }
 
/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function CustomerHomePhoneNum($phone=null){
	return _getPhoneNumber($phone);
}
 
/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CustomerMobilePhoneNum($phone=null){
	return _getPhoneNumber($phone);
}

/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CustomerWorkPhoneNum($phone=null){
	return _getPhoneNumber($phone);
}

/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Tel_1($phone=null){
	return _getPhoneNumber($phone);
}

/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Tel_2($phone=null){
	return _getPhoneNumber($phone);
}


/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Tel_3($phone=null){
	return _getPhoneNumber($phone);
}/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Tel_4($phone=null){
	return _getPhoneNumber($phone);
}
 // 
}
?>