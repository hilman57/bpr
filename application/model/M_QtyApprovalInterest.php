<?php
/*
 * E.U.I 
 * -----------------------------------------------
 *
 * subject	: M_QtyApprovalInterest
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class M_QtyApprovalInterest Extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function M_QtyApprovalInterest()
 {
	$this -> load -> model(array('M_SetCallResult','M_SetResultQuality','M_ModOutBoundGoal'));
 }
 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Sale()
{
	$_a = array(); $_b = array();
	if( class_exists('M_SetCallResult'))
	{
		$_a = $this -> M_SetCallResult -> _getInterestSale(); 
		foreach( $_a as $_k => $_v )
		{
			$_b[$_k] = $_k;  
		}	
	}
	
	return $_b;
} 
  
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 private function _getApprovalInterest()
 {
	$_conds = array();
	if(class_exists('M_SetCallResult'))
	{
		$i = 0;
		foreach( $this -> M_SetCallResult -> _getEventSale() as $k => $rows )
		{
			$_conds[$i] = $k;
			$i++;
		}
	}
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 private function _getQualityConfirm()
 {
	$_conds = array();
	if(class_exists('M_SetResultQuality'))
	{
		$i = 0;
		foreach( $this -> M_SetResultQuality -> _getQualityConfirm() as $k => $rows )
		{
			$_conds[$i] = $k;
			$i++;
		}
	}
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_default()
{
	/* get instance class outbound **/	
	
	$CallDirection =& M_ModOutBoundGoal::get_instance();
	
	$this -> EUI_Page->_setPage(10);
	$this -> EUI_Page -> _setQuery
		 (" select c.*, d.full_name, d.id,
				IF( e.CallReasonDesc is null, 'New', e.CallReasonDesc) as CallReasonDesc 
			from t_gn_quality_assignment a 
			LEFT JOIN t_gn_assignment b on a.Assign_Data_Id=b.AssignId
			LEFT JOIN t_gn_debitur c on b.CustomerId=c.CustomerId
			LEFT JOIN t_tx_agent d on b.AssignSelerId=d.UserId
			LEFT JOIN t_lk_account_status e on c.CallReasonId=e.CallReasonId
			LEFT JOIN t_gn_quality_group g on a.Quality_Staff_Id=g.Quality_Staff_id 
			LEFT JOIN t_tx_agent h on  a.Quality_Staff_Id=h.UserId 
			LEFT JOIN t_gn_campaign i on c.CampaignId=i.CampaignId  
			LEFT JOIN t_gn_policyautogen j on c.CustomerId=j.CustomerId
			LEFT JOIN t_gn_policy k  on j.PolicyNumber=k.PolicyNumber  "); 
			
	
	/* default for method approval **/
	
		$this -> EUI_Page->_setWhere("AND i.OutboundGoalsId='{$CallDirection -> _getOutboundId()}'");
		
	// get status in sale only 
	
		$this -> EUI_Page->_setWhere("AND e.CallReasonId IN('". implode( "','", self::Sale()) ."')");
		
		if( $this -> EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD 
			OR $this -> EUI_Session->_get_session('HandlingType')==USER_QUALITY_STAFF )
		{
			$this -> EUI_Page->_setWhere("AND  g.Quality_Skill_Id='". QUALITY_APPROVE ."'");
		}
	
	/* level user quality Head **/
	
		if( $this -> EUI_Session->_get_session('HandlingType')== USER_QUALITY_HEAD )
		{
			$this -> EUI_Page->_setWhere("AND  h.quality_id ='{$this -> EUI_Session->_get_session('UserId')}'");
		}

	/* 	level login quality staff **/
	
		if( $this -> EUI_Session->_get_session('HandlingType')== USER_QUALITY_STAFF )
		{
			$this -> EUI_Page->_setWhere("AND a.Quality_Staff_Id ='{$this -> EUI_Session->_get_session('UserId')}'");
		}
	
 
 // filtering 
	$this -> EUI_Page->_setWhere("AND c.CallReasonId IN('".implode("','",self::_getApprovalInterest())."') 
			 AND ( c.CallReasonQue IS NULL OR c.CallReasonQue IN('".implode("','",self::_getQualityConfirm())."')) ");
			 
				 
			
  // filtring by login 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==SUPERVISOR )			 
		$flt.=" AND b.AssignSpv ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
	if($this -> EUI_Session -> _get_session('HandlingType')==TELESALES)
		$flt.=" AND b.AssignSelerId ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
 // filtering by session customize 

	if( $this -> URI->_get_have_post('cust_name') ){ 
		$flt.=" AND c.CustomerFirstName LIKE '%".$this -> URI->_get_post('cust_name')."%'"; 
		$this -> EUI_Session ->replace_session('cust_name',$this -> URI->_get_post('cust_name'));
	}
	else{
		if(!isset($_REQUEST['cust_name'])){
			if($this -> EUI_Session ->_have_get_session('cust_name') )
			$$flt.=" AND c.CustomerFirstName LIKE '%".$this -> EUI_Session->_get_session('cust_name')."%'"; 
		}
		elseif(isset($_REQUEST['cust_name']) && $_REQUEST['cust_name']==''){
			$this -> EUI_Session ->deleted_session('cust_name');	
		}
	}
	
///	**************** ??
	if( $this -> URI->_get_have_post('cust_number') ) {
		$flt.=" AND c.CustomerNumber LIKE '%".$this -> URI->_get_post('cust_number')."%'"; 
		$this -> EUI_Session ->replace_session('cust_number',$this -> URI->_get_post('cust_number'));
	}
	else{
		if(!isset($_REQUEST['cust_number'])){
			if($this -> EUI_Session ->_have_get_session('cust_number') )
			$$flt.=" AND c.CustomerNumber LIKE '%".$this -> EUI_Session->_get_session('cust_number')."%'"; 
		}
		elseif(isset($_REQUEST['cust_number']) && $_REQUEST['cust_number']==''){
			$this -> EUI_Session ->deleted_session('cust_number');	
		}
	}	

///	**************** ??
	
	if( $this -> URI->_get_have_post('campaign_id') ){
		$flt.=" AND c.CampaignId =".$this -> URI->_get_post('campaign_id');	
		$this -> EUI_Session ->replace_session('campaign_id',$this -> URI->_get_post('campaign_id'));
	}
	else{
		if(!isset($_REQUEST['campaign_id'])){
			if($this -> EUI_Session ->_have_get_session('campaign_id') )
			$flt.=" AND c.CampaignId ='".$this -> EUI_Session->_get_session('campaign_id')."'";	
		}
		elseif(isset($_REQUEST['campaign_id']) && $_REQUEST['campaign_id']==''){
			$this -> EUI_Session ->deleted_session('campaign_id');	
		}
	}
	
// src category id session 
	
	if( $this -> URI->_get_have_post('category_id') ){
		$flt.=" AND j.ProductId =".$this -> URI->_get_post('category_id');	
		$this -> EUI_Session ->replace_session('category_id',$this -> URI->_get_post('category_id'));
	}
	else{
		if(!isset($_REQUEST['category_id'])){
			if($this -> EUI_Session ->_have_get_session('category_id') )
			$flt.=" AND j.ProductId ='".$this -> EUI_Session->_get_session('category_id')."'";	
		}
		elseif(isset($_REQUEST['category_id']) && $_REQUEST['category_id']==''){
			$this -> EUI_Session ->deleted_session('category_id');	
		}
	}	
	
///	**************** ??
	
	if( $this -> URI->_get_have_post('start_date') AND $this -> URI->_get_have_post('end_date') ){
		$flt .= " AND date(k.PolicySalesDate) >= '".$this -> EUI_Tools->_date_english($_REQUEST['start_date'])."' 
					 AND date(k.PolicySalesDate) <= '".$this -> EUI_Tools->_date_english($_REQUEST['end_date'])."' "; 
		$this -> EUI_Session ->replace_session('start_date',$this -> URI->_get_post('start_date'));
		$this -> EUI_Session ->replace_session('end_date',$this -> URI->_get_post('end_date'));
		
	}
	else
	{
		if(!isset($_REQUEST['start_date']) && !isset($_REQUEST['end_date']) )
		{
			if($this -> EUI_Session ->_have_get_session('start_date') && $this -> EUI_Session ->_have_get_session('end_date') ){
				$$flt .= " AND date(k.PolicySalesDate) >= '".$this -> EUI_Tools->_date_english( $this -> EUI_Session ->_get_session('start_date'))."' 
							 AND date(k.PolicySalesDate) <= '".$this -> EUI_Tools->_date_english( $this -> EUI_Session ->_get_session('end_date'))."' "; 
			}
		}
		elseif(isset($_REQUEST['start_date']) && ($_REQUEST['start_date']=='') 
			&& (isset($_REQUEST['end_date'])) && ($_REQUEST['end_date']) )
		{
			$this -> EUI_Session ->deleted_session('start_date');	
			$this -> EUI_Session ->deleted_session('end_date');	
		}
		else{
			$this -> EUI_Session ->deleted_session('start_date');	
			$this -> EUI_Session ->deleted_session('end_date');	
			
		}
	}
	
///	**************** ??
	
	if($this -> URI->_get_have_post('user_id')){
		$flt.=" AND d.AssignSelerId = '".$this -> URI->_get_post('user_id')."'";
		$this -> EUI_Session ->replace_session('user_id',$this -> URI->_get_post('user_id'));
	}
	else{
		if(!isset($_REQUEST['user_id'])){
			if($this -> EUI_Session ->_have_get_session('user_id') )
			$flt.=" AND d.AssignSelerId ='".$this -> EUI_Session->_get_session('user_id')."'";	
		}
		elseif(isset($_REQUEST['user_id']) && $_REQUEST['user_id']==''){
			$this -> EUI_Session ->deleted_session('user_id');	
		}
	}	
///	**************** ??		

    if( $this -> URI->_get_have_post('call_result'))
	{ 
		$flt .=" AND c.CallReasonId ='".$this -> URI->_get_post('call_result')."'"; 
		$this -> EUI_Session ->replace_session('call_result',$this -> URI->_get_post('call_result'));
	}
	else{
		if(!isset($_REQUEST['call_result'])){
			if($this -> EUI_Session ->_have_get_session('call_result') )
			$flt.=" AND c.CallReasonId ='".$this -> EUI_Session->_get_session('call_result')."'";	
		}
		elseif(isset($_REQUEST['call_result']) && $_REQUEST['call_result']==''){
			$this -> EUI_Session ->deleted_session('call_result');	
		}
	}
	
// run filter
	
	$this->EUI_Page->_setWhere($flt); 
	$this->EUI_Page->_setGroupBy('c.CustomerId');  
	if($this -> EUI_Page -> _get_query())
	{
		return $this -> EUI_Page;
	}	 
	
	
 }
 
 /*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 public function _get_content()
 {
	/* get instance class outbound **/	
	
	$CallDirection =& M_ModOutBoundGoal::get_instance();
	
	/* default of query selector  **/
	
	$this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this -> EUI_Page->_setPage(10);
	$this -> EUI_Page->_setQuery(" 
			SELECT c.*, d.full_name, d.id, f.CampaignName,
				IF( e.CallReasonDesc is null, 'New', e.CallReasonDesc) as CallReasonDesc , k.PolicySalesDate,
					m.AproveName
			FROM t_gn_quality_assignment a 
			LEFT JOIN t_gn_assignment b on a.Assign_Data_Id=b.AssignId
			LEFT JOIN t_gn_debitur c on b.CustomerId=c.CustomerId
			LEFT JOIN t_tx_agent d on b.AssignSelerId=d.UserId
			LEFT JOIN t_lk_account_status e on c.CallReasonId=e.CallReasonId
			LEFT JOIN t_gn_campaign f on c.CampaignId=f.CampaignId 
			LEFT JOIN t_gn_quality_group g on a.Quality_Staff_Id=g.Quality_Staff_id
			LEFT JOIN t_tx_agent h on  a.Quality_Staff_Id=h.UserId  
			LEFT JOIN t_gn_policyautogen j on c.CustomerId=j.CustomerId
			LEFT JOIN t_gn_policy k  on j.PolicyNumber=k.PolicyNumber 
			LEFT JOIN t_lk_aprove_status m on c.CallReasonQue=m.ApproveId ");
 
	
	
	/* default for method approval **/
		
		$this -> EUI_Page->_setWhere("AND f.OutboundGoalsId='{$CallDirection -> _getOutboundId()}'");
		
	// get status in sale only 
	
		$this -> EUI_Page->_setWhere("AND e.CallReasonId IN('". implode( "','", self::Sale()) ."')");
			
		if( $this -> EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD 
			OR $this -> EUI_Session->_get_session('HandlingType')==USER_QUALITY_STAFF )
		{
			$this -> EUI_Page->_setWhere("AND  g.Quality_Skill_Id='". QUALITY_APPROVE ."'");
		}
	
	/* level user quality Head **/
	
		if( $this -> EUI_Session->_get_session('HandlingType')== USER_QUALITY_HEAD )
		{
			$this -> EUI_Page->_setWhere(" AND h.quality_id ='{$this -> EUI_Session->_get_session('UserId')}'");
		}

	/* 	level login quality staff **/
	
		if( $this -> EUI_Session->_get_session('HandlingType')== USER_QUALITY_STAFF )
		{
			$this -> EUI_Page->_setWhere("AND  g.Quality_Staff_Id ='{$this -> EUI_Session->_get_session('UserId')}'");
		}
	
	/* filter next data if not empty filter **/
	
	$flt = " AND c.CallReasonId IN('".implode("','",self::_getApprovalInterest())."') 
			 AND ( c.CallReasonQue IS NULL OR c.CallReasonQue IN('".implode("','",self::_getQualityConfirm())."')) ";
				 		
			
  // filtring by login 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==SUPERVISOR )			 
		$flt.=" AND b.AssignSpv ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
	if($this -> EUI_Session -> _get_session('HandlingType')==TELESALES)
		$flt.=" AND b.AssignSelerId ='".$this -> EUI_Session -> _get_session('UserId')."' ";
		
 // filtring by keep session 
					
	if( $this -> URI->_get_have_post('cust_name') ) 
		$flt.=" AND c.CustomerFirstName LIKE '%".$this -> URI->_get_post('cust_name')."%'"; 
	
	if( $this -> URI->_get_have_post('cust_number') ) 
		$flt.=" AND c.CustomerNumber LIKE '%".$this -> URI->_get_post('cust_number')."%'"; 
		
	
	if( $this -> URI->_get_have_post('campaign_id') )
		$flt.=" AND c.CampaignId =".$this -> URI->_get_post('campaign_id');	
	
	if( $this -> URI->_get_have_post('category_id') )
		$flt.=" AND j.ProductId =".$this -> URI->_get_post('category_id');	

	if( $this -> URI->_get_have_post('start_date') && $this ->URI ->_get_have_post('end_date') )
		$flt .= " AND date(k.PolicySalesDate)>= '".$this ->EUI_Tools ->_date_english($this ->URI ->_get_post('start_date'))."' 
					 AND date(k.PolicySalesDate)<= '".$this ->EUI_Tools ->_date_english($this ->URI ->_get_post('end_date'))."' "; 
	
	
	if($this -> URI->_get_have_post('user_id'))
		$flt.=" AND b.AssignSelerId = '".$this -> URI->_get_post('user_id')."'";
		

    if( $this -> URI->_get_have_post('call_result'))
		$flt .=" AND c.CallReasonId ='".$this -> URI->_get_post('call_result')."'"; 
		
	$this->EUI_Page->_setWhere($flt);   
	// echo "<pre>{$this->EUI_Page->_query}</pre>";
	
	
	$this->EUI_Page->_setGroupBy('c.CustomerId');
	$this -> EUI_Page ->_setLimit();
	
 }
 
 
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCountVoice($CustomerId=0)
 {
	$_count = 0;
	$this -> db -> select("count(a.id) as jumlah",FALSE);
	$this -> db -> from("cc_recording a");
	$this -> db -> join("cc_call_session b","a.session_key = b.session_id ","INNER");
	
	if( $this -> URI->_get_have_post('CustomerId')) 
	{
		$this ->db->where("a.assignment_data",$this -> URI->_get_post('CustomerId'));
	}
	
	if( $rows = $this -> db -> get() -> result_first_assoc() ){
		$_count = (INT)$rows['jumlah']; 
	}
	
	return $_count;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getPages($CustomerId=0)
 {
	$PagesList = array();
	
	$record = $this -> _getCountVoice();
	$counts = ceil($record/5);
	
	for($p = 1; $p <= (INT)$counts; $p++) {
		$PagesList[$p] = $p;
	}
	
	return $PagesList;
	
 }
 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getLastCallHistory( $CustomerId )
 {
	$_conds = array();
	
	$this -> db -> select('*');
	$this -> db -> from('t_gn_callhistory');
	$this -> db -> where('CustomerId',$CustomerId);
	$this -> db -> order_by('CallHistoryId','DESC');
	$this -> db -> limit(1);
	if( $avail = $this -> db -> get()->result_first_assoc() ) {
		$_conds = $avail;
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
  function _getListVoice($param = array() )
 {
	$_voice = array();
	
 // $start
	$start = 0; $perpages = 5; 
	
 // total page 
	
	$record = $this -> _getCountVoice();
	$pages = ceil($record/$perpages);
	
	//  get start pages 
	
	if( isset($param['pages']) ){
		if( (INT)$param['pages'] > 0 )
			$start = ( (($param['pages'])-1) * $perpages); 
		else
			$start = 0;
	}
	
	// run data 
	
	$this -> db -> select("a.*");
	$this -> db -> from("cc_recording a");
	$this -> db -> join("cc_call_session b","a.session_key = b.session_id ","INNER");
	
	if( $this -> URI->_get_have_post('CustomerId') ) 
	{
		$this ->db->where("a.assignment_data", $this -> URI->_get_post('CustomerId'));
	}
	
	$this -> db -> limit($perpages,$start);
	
	$qry = $this->db->get();
	$num = $start+1;
	foreach($qry -> result_assoc() as $rows )
	{
		$_voice[$num] = $rows;	
		$num++;
	}	
	return $_voice;
 }
 

 
 
 
 /* get rows data **/
 
 function _getVoiceResult($VoiceId=0 )
 {
	$this -> db -> select("a.*");
	$this -> db -> from("cc_recording a");
	$this -> db -> where('id',$VoiceId);

	
	$_result =  array();
	
	if( $_conds = $this -> db->get() -> result_first_assoc() )
	{
		foreach($_conds as $fld => $values )
		{
			if( $fld=='file_voc_size' ) 
				$_result[$fld] = $this->EUI_Tools->_get_format_size($values);
				
			else if( $fld=='duration' ) 
				$_result[$fld] = $this->EUI_Tools->_set_duration($values);
				
			else if( $fld=='anumber' ) 
				$_result[$fld] = $this->EUI_Tools->_getPhoneNumber($values);	
				
			else if( $fld=='start_time' ) 
				$_result[$fld] = $this->EUI_Tools->_datetime_indonesia($values);	
				
			else 
				$_result[$fld] = $values;
		}
		
		return $_result;
	}
	else
		return null;
 }

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

private function _setSaveApprovalLog( $param = null )
{
 
 $_conds = 0;
 if( !is_null($param) )
 {
	$this -> db ->set('ApprovalId', $param['ApprovalId']);
	$this -> db ->set('ApprovalPoints', $param['ApprovalPoints']);
	$this -> db ->set('ApprovalById', $param['ApprovalById']);
	$this -> db ->set('ApprovalStatus', $param['ApprovalStatus']);
	$this -> db ->set('ApprovalRemark',$param['ApprovalRemark']);
	$this -> db ->set('ApprovalTs', date('Y-m-d H:i:s'));
	$this -> db ->insert('t_gn_log_approval');
	
	if( $this -> db -> affected_rows() > 0 )
	{
		$_conds++;
	}
 }
 
 return $_conds;
 
}
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _SaveQualityAssesment()
{
	$_conds = 0;
	
	$AppPointValue = $this -> URI -> _get_array_post('ApprovalPoints');
	$AppPointKeys = $this -> URI -> _get_array_post('ApprovalById');
	if( is_array($AppPointValue) 
	AND  is_array($AppPointKeys) ) 
	{
		$_keys_value = implode(",", array_slice($AppPointValue, 1, count($AppPointValue)));
		$_keys_index = implode(",", array_slice($AppPointKeys, 1, count($AppPointKeys)));
		$_CustomerId = $this -> URI ->_get_post('CustomerId');
		
		if( !is_null($_keys_value) 
			AND !is_null($_keys_index) )
		{
			$this -> db -> set('ApprovalUserId', $this -> EUI_Session->_get_session('UserId'));
			$this -> db -> set('ApprovalUserName',$this -> EUI_Session->_get_session('Username'));
			$this -> db -> set('ApprovalStatus',$this -> URI ->_get_post('ApprovalStatus'));
			$this -> db -> set('ApprovalTs',_getOptionDate($this -> URI ->_get_post('ApprovalTs'), 'en', '/') );
			$this -> db -> set('ApprovalRemark',$this -> URI ->_get_post('ApprovalRemark'));
			$this -> db -> set('ApprovalById',$_keys_index);
			$this -> db -> set('ApprovalPoints',$_keys_value);
			$this -> db -> set('CustomerId',$_CustomerId);
			
			$this -> db ->insert('t_gn_qa_approval');
			
			// save call CustomerId   
			
			if( $this ->db->affected_rows() > 0)  
			{
				$param['ApprovalId'] 		= $this->db->insert_id();
				$param['ApprovalStatus'] 	= $this->URI->_get_post('ApprovalStatus');
				$param['ApprovalRemark'] 	= $this->URI->_get_post('ApprovalRemark');
				$param['ApprovalPoints'] 	= $_keys_value; 
				$param['ApprovalById'] 		= $_keys_index;
				$param['ApprovalTs'] 		= date('Y-m-d H:i:s');
				
			 /* Next update history && customers **/
			 
				if(  $this -> _setSaveApprovalLog($param) )
				{
					$this -> db ->set('CallReasonQue',$this -> URI ->_get_post('ApprovalStatus'));
					$this -> db ->set('QueueId',$this -> EUI_Session->_get_session('UserId'));
					$this -> db ->set('CustomerUpdatedTs',date('Y-m-d H:i:s'));
					$this -> db ->where('CustomerId',$_CustomerId);
					$this -> db ->update('t_gn_debitur');
					if( $this ->db->affected_rows() > 0) 
						$_conds++; 
				}
			}
			else
			{	
				$this->db->select('*');
				$this->db->from('t_gn_qa_approval');
				$this->db->where('CustomerId',$_CustomerId);
				if( $rows = $this -> db->get() -> result_first_assoc() )
				{
					$this -> db -> set('ApprovalStatus',$this -> URI ->_get_post('ApprovalStatus'));
					$this -> db -> set('ApprovalTs',_getOptionDate($this -> URI ->_get_post('ApprovalTs'), 'en', '/') );
					$this -> db -> set('ApprovalRemark',$this -> URI ->_get_post('ApprovalRemark'));
					$this -> db -> set('ApprovalById',$_keys_index);
					$this -> db -> set('ApprovalPoints',$_keys_value);
					$this -> db -> where('ApprovalId', $rows['ApprovalId']);
				  
				  if( $this -> db ->update('t_gn_qa_approval') )
				  {
					
					$param['ApprovalId'] 		= $rows['ApprovalId'];
					$param['ApprovalStatus'] 	= $this->URI->_get_post('ApprovalStatus');
					$param['ApprovalRemark'] 	= $this->URI->_get_post('ApprovalRemark');
					$param['ApprovalPoints'] 	= $_keys_value; 
					$param['ApprovalById'] 		= $_keys_index;
					$param['ApprovalTs'] 		= date('Y-m-d H:i:s');
				
					/* Next update history && customers **/
					
				 	if( $this -> _setSaveApprovalLog($param) )
					{
						$this -> db ->set('CallReasonQue',$this -> URI ->_get_post('ApprovalStatus'));
						$this -> db ->set('QueueId',$this -> EUI_Session->_get_session('UserId'));
						$this -> db ->set('CustomerUpdatedTs',date('Y-m-d H:i:s'));
						$this -> db ->where('CustomerId',$_CustomerId);
						$this -> db ->update('t_gn_debitur');
						if( $this ->db->affected_rows() > 0) 
							$_conds++; 
					 }	
				   }
				}
			}
			
			
			// save call history 
			if( ($_conds) 
				AND ( $CallHistory = $this ->_getLastCallHistory($_CustomerId) ) ) 
			{
				$this -> db-> set('CallHistoryCreatedTs',date('Y-m-d H:i:s'));
				$this -> db-> set('CallHistoryUpdatedTs',date('Y-m-d H:i:s'));
				$this -> db-> set('CreatedById',$this -> EUI_Session->_get_session('UserId'));
				$this -> db-> set('UpdatedById',$this -> EUI_Session->_get_session('UserId'));
				$this -> db-> set('CallHistoryNotes',$this -> URI ->_get_post('ApprovalRemark'));
				$this -> db-> set('CallSessionId',$CallHistory['CallSessionId']);
				$this -> db-> set('CustomerId',$CallHistory['CustomerId']);
				$this -> db-> set('CallReasonId',$CallHistory['CallReasonId']);
				$this -> db-> set('ApprovalStatusId',$this -> URI ->_get_post('ApprovalStatus'));
				$this -> db-> set('CallHistoryCallDate',$CallHistory['CallHistoryCallDate']);
				$this -> db-> set('CallNumber',$CallHistory['CallNumber']);
				$this -> db ->insert('t_gn_callhistory');
				
				if( $this ->db->affected_rows() > 0) 
						$_conds++;
			}
			
		} 
	}
	
	return $_conds;
		
}
 
 
}
?>