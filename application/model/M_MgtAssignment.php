<?php
/*
 * E.U.I 
 *
 
 * subject	: M_MgtAssignment modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_MgtAssignment extends EUI_Model
{

 private static $Active;
 private static $NotActive;
 private static $CampaignId = NULL;

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function M_MgtAssignment()
{
	self::$Active = 1;
	self::$NotActive = 0;
	
	$this -> load -> model(array("M_ModDistribusi"));
} 
 
 
private function _get_config_value()
{
	$this->load->model('M_Configuration');
	$config = $this->M_Configuration->_getAtempt( $this->EUI_Session->_get_session('ProjectId'));
	
	return $config;
} 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
function _get_default()
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect('a.CampaignId, a.CampaignNumber, a.CampaignName, b.Description');
	$this->EUI_Page->_setFrom('t_gn_campaign a ');
	$this->EUI_Page->_setJoin('t_lk_outbound_goals b','a.OutboundGoalsId=b.OutboundGoalsId','LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign_project c ',' a.CampaignId=c.CampaignId','LEFT', TRUE);
	$this->EUI_Page->_setAnd('a.CampaignStatusFlag',1);
	$this->EUI_Page->_setWhereIn('c.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
	
	// setoutbound 
	
	if( $this -> URI -> _get_have_post('OutboundGoalId') ){
		$this->EUI_Page->_setAnd('a.OutboundGoalsId', $this->URI->_get_post('OutboundGoalId')); 
	}
	
   // set order
	if( $this->URI->_get_have_post('order_by') ){
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}
	
	if( $this -> EUI_Page -> _get_query() ) 
	{
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
	
  $this->EUI_Page->_setSelect('a.CampaignId, a.CampaignNumber, a.CampaignName, a.CampaignCode, b.Description');
  $this->EUI_Page->_setFrom('t_gn_campaign a ');
  $this->EUI_Page->_setJoin('t_lk_outbound_goals b','a.OutboundGoalsId=b.OutboundGoalsId','LEFT');
  $this->EUI_Page->_setJoin('t_gn_campaign_project c ',' a.CampaignId=c.CampaignId','LEFT', TRUE);
  $this->EUI_Page->_setAnd('a.CampaignStatusFlag',1);
  $this->EUI_Page->_setWhereIn('c.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
  
  // set outbound 
  
  if( $this -> URI -> _get_have_post('OutboundGoalId') ){
		$this->EUI_Page->_setAnd('a.OutboundGoalsId', $this->URI->_get_post('OutboundGoalId')); 
	}
 
 // set order type 
 
 
  if( $this -> URI -> _get_have_post('order_by') ){
		$this -> EUI_Page -> _setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}
	else{
		$this -> EUI_Page -> _setOrderBy('a.CampaignCode','asc');
	}
	
	$this -> EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
function _getShowData($Data=null)
{
	$_counter = 0;	
	if( !is_null($Data) )
	{
		$sql = "select 
					COUNT(b.AssignId) as Counter 
				from t_gn_debitur a
				left join t_gn_assignment b on a.CustomerId=b.CustomerId
				where 1=1
				and a.CampaignId = '".$Data['CampaignId']."'
				and a.CallReasonId not in (".implode(',',array_keys($this->M_SetCallResult->_getRealInterest())).") ";
				
		if( isset($Data['UserId']) 
			AND is_array($Data['UserId']) )
		{
			$sql .= " and b.AssignSelerId in (".implode(',',$Data['UserId']).") ";
			// $this -> db -> where_in('b.AssignSelerId',$Data['UserId']);
		}
		
		if( isset($Data['CallResultId']) 
			AND is_array($Data['CallResultId']) )
		{
			$idReason = array();
			$filter = "";
			
			foreach($Data['CallResultId'] as $reason)
			{
				if($reason == 'new'){
					$filter = " a.CallReasonId = 0 ";
				}
				else{
					$idReason[] = $reason;
				}
			}
			
			if(!empty($filter))
			{
				if(count($idReason)>0)
				{
					$sql .= " and (a.CallReasonId in (".implode(',',$idReason).") or ".$filter." )";
				}
				else{
					$sql .= " and ".$filter." ";
				}
			}
			else{
				$sql .= " and a.CallReasonId in (".implode(',',$idReason).")";
			}
		}
		
		$qry = $this->db->query($sql);
		if($rows = $qry->result_first_assoc()) {
			$_counter = $rows['Counter'];
		}
	}
	
	return $_counter;
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_field_level($Level=null )
{ 
	$_conds = null;
	
	if(!is_null($Level))
	{
		$_list_user = array
		( 
			USER_ADMIN => 'AssignAdmin', 
			USER_MANAGER => 'AssignMgr', 
			USER_ACCOUNT_MANAGER => 'AssignAmgr',
			USER_SUPERVISOR => 'AssignSpv',
			USER_AGENT_OUTBOUND => 'AssignSelerId', 
			USER_AGENT_INBOUND => 'AssignSelerId', 
			USER_LEADER => 'AssignLeader',
			USER_QUALITY => 'AssignAdmin', 
			USER_ROOT => 'AssignAdmin'
		);
			
		if( count($_list_user))
		{
			$_conds = $_list_user[$Level];
		}
	}
	return $_conds;
}	
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_select_campaign()
 {
	$sql = " SELECT * FROM t_gn_campaign ";
	$qry = $this -> db -> query($sql);
	foreach( $qry -> result_assoc() as $rows ){
		$_conds[$rows['CampaignStatusFlag']][$rows['CampaignId']] = $rows;
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
function _get_all_campaign()
 {
	$_all_size = array();
	foreach( self::_get_select_campaign() as $ky => $vl )
	{
		foreach( $vl as $rows ){
			$_all_size[$rows['CampaignId']] = $rows['CampaignName'];
		}
	}
	
	return $_all_size;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_active_campaign()
 {
	$_conds = array();
	$_campaign = self::_get_select_campaign();
	if( is_array($_campaign) )
	{
		$_conds = $_campaign[self::$Active];
	}
	return $_conds;
 } 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_unactive_campaign()
 {
	$_conds = array();
	$_campaign = self::_get_select_campaign();
	if( is_array($_campaign) )
	{
		$_conds = $_campaign[self::$NotActive];
	}
	return $_conds;
 } 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_policy_available( $CampaignId=0, $_status='' )
 {
	$total = 0;
	
	
	
	return $total;
 }
 
function _get_count_privileges( $CampaignId=0, $_status='')
 {
	$_totals = array();
	
	$this->db->select('count(b.CustomerId) as jumlah,  a.CampaignStatusFlag as conds', false);
	$this->db->from('t_gn_campaign a');
	$this->db->join('t_gn_debitur b ',' a.CampaignId=b.CampaignId ', 'INNER');
	$this->db->join('t_gn_assignment  c ',' b.CustomerId=c.CustomerId', 'INNER');
	$this->db->where('a.CampaignId', $CampaignId);
	$this->db->where_in('a.CampaignStatusFlag',array(self::$Active, self::$NotActive) );
	//echo $this -> db -> _get_var_dump();
	
	/** filterb && group by session handle **/
	
	if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ROOT )	{
		$this->db->where('c.AssignAmgr IS NULL');
		$this->db->where('c.AssignMgr IS NULL');
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ADMIN ){
		$this->db->where('c.AssignMgr IS NULL');
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 
						 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ACCOUNT_MANAGER )	{
		$this->db->where('c.AssignAmgr', $this->EUI_Session->_get_session('UserId'));
		$this->db->where('c.AssignMgr IS NULL');
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 
					 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_MANAGER ){
		$this->db->where('c.AssignMgr', $this->EUI_Session->_get_session('UserId'));
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
	}
	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_SUPERVISOR ){
		$this->db->where('c.AssignMgr IS NOT NULL');
		$this->db->where('c.AssignSpv', $this->EUI_Session->_get_session('UserId'));
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 

	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_LEADER )	{
		$this->db->where('AssignLeader', $this -> EUI_Session -> _get_session('UserId'));
		$this->db->where('c.AssignMgr IS NOT NULL');
		$this->db->where('c.AssignSpv IS NOT NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 
	
 // start && run query 
	// echo $this -> db -> _get_var_dump();
	$qry = $this->db->get();
	
	foreach( $qry -> result_assoc() as $rows ){
		$_totals[$rows['conds']]+= $rows['jumlah'];
	}
	
	if( $_status !='' ) 
		return (INT)$_totals[$_status];
	else
	{
		return (($_totals[self::$Active])+($_totals[self::$NotActive]));
	}
}

// add by : omen  
// date : 2014-11-17
// add handle utilize policy 

function _get_policy_utilize_size( $CampaignId = 0  )
{
	$_totals = array();
	
	$this->db->select('count(b.CustomerId) as jumlah,  a.CampaignStatusFlag as conds', false);
	$this->db->from('t_gn_campaign a');
	$this->db->join('t_gn_debitur b ',' a.CampaignId=b.CampaignId ', 'INNER');
	$this->db->join('t_gn_assignment  c ',' b.CustomerId=c.CustomerId', 'INNER');
	$this->db->join('t_gn_policy_detail  d ','b.CustomerNumber = d.EnigmaId', 'LEFT');
	$this->db->where('a.CampaignId', $CampaignId);
	$this->db->where_in('a.CampaignStatusFlag',array(self::$Active, self::$NotActive) );
	//echo $this -> db -> _get_var_dump();
	
	/** filterb && group by session handle **/
	
	if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ROOT )	{
		$this->db->where('c.AssignAmgr IS NULL');
		$this->db->where('c.AssignMgr IS NULL');
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ADMIN ){
		$this->db->where('c.AssignMgr IS NULL');
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 
						 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ACCOUNT_MANAGER )	{
		$this->db->where('c.AssignAmgr', $this->EUI_Session->_get_session('UserId'));
		$this->db->where('c.AssignMgr IS NULL');
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 
					 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_MANAGER ){
		$this->db->where('c.AssignMgr', $this->EUI_Session->_get_session('UserId'));
		$this->db->where('c.AssignSpv IS NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
	}
	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_SUPERVISOR ){
		$this->db->where('c.AssignMgr IS NOT NULL');
		$this->db->where('c.AssignSpv', $this->EUI_Session->_get_session('UserId'));
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 

	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_LEADER )	{
		$this->db->where('AssignLeader', $this -> EUI_Session -> _get_session('UserId'));
		$this->db->where('c.AssignMgr IS NOT NULL');
		$this->db->where('c.AssignSpv IS NOT NULL');
		$this->db->where('c.AssignSelerId IS NULL ');
		$this->db->group_by('a.CampaignStatusFlag');
	}				 
	
 // start && run query 
	// echo $this -> db -> _get_var_dump();
	$qry = $this->db->get();
	
	foreach( $qry -> result_assoc() as $rows ){
		$_totals[$rows['conds']]+= $rows['jumlah'];
	}
	
	if( $_status !='' ) 
		return (INT)$_totals[$_status];
	else
	{
		return (($_totals[self::$Active])+($_totals[self::$NotActive]));
	}
}


function _get_policy_size($CampaignId=0, $_status='') {
	$this->db->reset_select();
	$_totals = 0;
	$sql = "SELECT COUNT(b.CustomerId) AS policy_size
			FROM t_gn_campaign a
			INNER JOIN t_gn_debitur b ON a.CampaignId = b.CampaignId
			INNER JOIN t_gn_assignment c ON b.CustomerId=c.CustomerId
			LEFT JOIN t_gn_policy_detail d on b.CustomerNumber = d.EnigmaId
			WHERE 1=1
			AND a.CampaignId = '".$CampaignId."' 
			AND a.CampaignStatusFlag IN (".implode(',',array(self::$Active, self::$NotActive)).") ";
	
	$qry = $this->db->query($sql);
	$_totals = $qry->result_first_assoc();
	
	return $_totals['policy_size'];
}

function _get_size_privileges($CampaignId=0, $_status='') {
	$this->db->reset_select();
	$_totals = array();
	
	$sql = "SELECT 
				count(b.CustomerId) as jumlah, 
				a.CampaignStatusFlag as conds 
			FROM t_gn_campaign a 
			INNER JOIN t_gn_debitur b ON a.CampaignId = b.CampaignId 
			INNER JOIN t_gn_assignment c ON b.CustomerId = c.CustomerId 
			WHERE 1=1 
			AND a.CampaignId = '".$CampaignId."' 
			AND a.CampaignStatusFlag IN (".implode(',',array(self::$Active, self::$NotActive)).") ";
	
	$qry = $this->db->query($sql);
	
	foreach( $qry -> result_assoc() as $rows ){
		$_totals[$rows['conds']]+= $rows['jumlah'];
	}
	
	if( $_status !='' ) 
		return (INT)$_totals[$_status];
	else
	{
		return (($_totals[self::$Active])+($_totals[self::$NotActive]));
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function getParameter() {
	if( $this -> EUI_Session -> _get_session("UserId") )
	{
		$sql = " SELECT a.`query` as str  FROM t_gn_query a 
				 where a.query_level=".$this -> EUI_Session -> _get_session("HandlingType")." 
				 AND a.query_section='distribusi'";
				 
	
		$qry = $this->db->query($sql);
		foreach($qry->result_assoc() as $rows )
		{
			$datas1 = str_replace('{SESSION_USER}',$this->EUI_Session->_get_session('UserId'),$rows['str']);
			$datas2 = str_replace('{CAMPAIGN_ID}',self::$CampaignId, $datas1);
			return $datas2;
		}
	}
}
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function getQueryList( $CampaignId=null )
{
	$_conds = null;
	if($this -> EUI_Session -> _have_get_session("UserId") !=FALSE )
	{
		$sql = " SELECT a.`query` as str 
				 FROM t_gn_query a where a.query_level='".$this->EUI_Session->_get_session("HandlingType")."' 
				 AND a.query_section='querylist'";
		$qry = $this -> db -> query($sql);
		foreach( $qry -> result_assoc() as $rows )
		{
			$_conds1 = str_replace('{SESSION_USER}',$this->EUI_Session->_get_session('UserId'),$rows['str']);
			$_conds = str_replace('{CAMPAIGN_ID}', $CampaignId, $_conds1 );
		}
	}
	
  
  // rekonstruksi ----> 
  
  if( !is_null($_conds) AND ($_conds!='') )
  {
		if( $this -> URI->_get_have_post('StartAge') 
			AND $this -> URI->_get_have_post('EndAge') )
		{
			$start_age = $this -> URI->_get_post('StartAge');
			$end_age = $this -> URI->_get_post('EndAge');
			$_conds = preg_replace("/\*/i", " a.* , FLOOR(DATEDIFF(NOW(), DATE(b.CustomerDOB))/365)  as CUSTOMER_AGE ", $_conds );  
		}
		
		// if true of the CustomerCity 
		
		if( $this -> URI->_get_have_post('CustomerCity') 
			AND $this -> URI->_get_post('CustomerCity')!='' )
		{
			$_conds .= " AND b.CustomerCity LIKE '%{$this -> URI->_get_post('CustomerCity')}%' ";
		}
			
		// if true of the GenderId 
		
		if( $this -> URI->_get_have_post('GenderId') 
			AND $this -> URI->_get_post('GenderId')!='' )
		{
			$_conds .= " AND b.GenderId ='{$this -> URI->_get_post('GenderId')}'";
		}
		
		if( $this -> URI->_get_have_post('StartDate') 
			AND $this -> URI->_get_have_post('EndDate') )
		{
			$sql .= " AND b.Policy_Iss_Date >='{$this -> URI->_get_post('StartDate')}' 
					  AND b.Policy_Iss_Date <='{$this -> URI->_get_post('EndDate')}' ";
		}
		
		if( $this -> URI->_get_have_post('ProductCategory') 
			AND $this -> URI->_get_post('ProductCategory') )
		{
			$sql .= " AND e.Product_Category >='{$this -> URI->_get_post('ProductCategory')}' ";
		}
			
		// if true of the StartAge 
		
		if( $this -> URI->_get_have_post('StartAge') 
			AND $this -> URI->_get_have_post('EndAge') )
		{
			$_conds .= " GROUP BY a.AssignId HAVING CUSTOMER_AGE >= {$start_age} AND CUSTOMER_AGE <= {$end_age} ";
		}
  }
  
  return $_conds;
}

	
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function ImplodeStatus()
{
	$status = self::getStatusQA();
	if( is_array($status) )
	{
		return implode(",",array_values($status));
	}
}
	
	
// _set_CampaignId

public function _set_CampaignId( $_CampaignId ) {
	if( $_CampaignId ) 
	{
		if( is_null( self::$CampaignId ) ) 
		{
			self::$CampaignId = $_CampaignId;
		}
	}
}

// __entity 

public function __entity() 
{
 static $_config = array();
 $this->db->reset_select();
 $this->db->select('*');
 $this->db->from('t_gn_campaign a ');
 $this->db->where('a.CampaignId', self::$CampaignId);
 
 $rec = $this->db->get();
 if( $rec->num_rows()>0 )
  foreach( $rec->result_first_assoc() as $field => $values ) 
 {
		$_config[$field] = $values;
 }
 
 return $_config;
	
 }
		
// getCampaignName

public function getCampaignName() {
  $conds = NULL;
  $config = self::__entity();
  
  if( is_array($config) 
	AND isset($config['CampaignName']) )  
  {
	 $conds = $config['CampaignName'];
  }
 
 return $conds;
 
}

// getCampaignCode 

public function getCampaignCode( $CampaignNumber = null )  {

  self::$CampaignId = $CampaignNumber;
  $conds = NULL; $config = self::__entity(); 
  if(is_array($config) 
	AND isset($config['CampaignCode']) )  
  {
	 $conds = $config['CampaignCode'];
  }
 
  return $conds;
 
}

// getCampaignNumber  		
public function getCampaignNumber() {
  $conds = NULL; $config = self::__entity(); 
  if(is_array($config) 
	AND isset($config['CampaignNumber']) )  
  {
	 $conds = $config['CampaignNumber'];
  }
  
 return $conds;
 
}

// getCampaignId

public function getCampaignId() {

 $conds = NULL; $config = self::__entity(); 
  if(is_array($config) 
	AND isset($config['CampaignId']) )  
  {
	 $conds = $config['CampaignId'];
  }
  
 return $conds;
 
}

// _get_product_category

public function _get_product_category($_CampaignId) {
 $datas = array();
 if( isset(self::$CampaignId) ) 
 {
	$sql = str_replace('count(a.AssignId) as jumlah','distinct(e.Product_Category) as Product_Category', $this->getParameter());
	$qry = $this->db->query($sql);
	if( $qry->num_rows())
	{
		foreach($qry->result_assoc() as $rows) {
			$datas[$rows['Product_Category']] = $rows['Product_Category'];
		}
	}
 }
 return $datas;
 
}

// getLevelUser
	
public function getLevelUser() {
	$_array_level = null; $data = array();
	
/** get level user handle by login session **/
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ROOT) 
		$_array_level = array( USER_ADMIN, USER_QUALITY_STAFF,USER_QUALITY_HEAD, USER_ROOT );
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ADMIN ) 
		$_array_level = array( USER_ADMIN, USER_QUALITY_STAFF,USER_QUALITY_HEAD, USER_ROOT );
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER ) 
		$_array_level = array( USER_ADMIN, USER_ROOT, USER_QUALITY_STAFF,USER_QUALITY_HEAD,USER_ACCOUNT_MANAGER);
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_MANAGER ) 
		$_array_level = array( USER_ADMIN, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ROOT, USER_QUALITY_STAFF,USER_QUALITY_HEAD,);
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_QUALITY_STAFF ) 
		$_array_level = array();
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_QUALITY_HEAD ) 
		$_array_level = array();
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ) 
		$_array_level = array( USER_ADMIN, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ROOT, USER_QUALITY_STAFF,USER_QUALITY_HEAD, USER_SUPERVISOR );
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_LEADER ) 
		$_array_level = array( USER_ADMIN, USER_MANAGER, USER_ROOT, USER_ACCOUNT_MANAGER, USER_QUALITY_STAFF,USER_QUALITY_HEAD, USER_SUPERVISOR, USER_LEADER);	
	
/** implode in query statments **/
	
	$_conds  = null;
	if( !is_null($_array_level) ) {
		$_conds = implode("','",$_array_level);
	}

/** run of query statments **/
	
	if( !is_null($_conds) )
	{
		$sql   = "SELECT a.id, a.name FROM t_gn_agent_profile a WHERE a.id NOT IN('$_conds') AND a.IsActive=1";	
		$qry   = $this -> db -> query($sql);
		foreach( $qry -> result_assoc() as $rows )
		{
			$data[$rows['id']] = $rows['name'];
		}
	}
	
	return $data;
}
		
// DistribusiType

public function DistribusiType() {
	$datas = array(1=>'Manual',2=>'Automatic');
	if( is_array($datas) )
	{
		return $datas;
	}
}

// JumlahData

public function JumlahData() {
	$_conds = 0;	
	if( $sql = self::getParameter() )
	{
	
	  // rekonstruksi ----> 
	  
		if( $this -> URI->_get_have_post('StartAge') 
			AND $this -> URI->_get_have_post('EndAge') )
		{
			$start_age = $this -> URI->_get_post('StartAge');
			$end_age = $this -> URI->_get_post('EndAge');
			
			$sql = str_replace("count(a.AssignId) as jumlah", 
					"COUNT(a.AssignId) as jumlah, a.AssignId as ID, FLOOR(DATEDIFF(NOW(), DATE(b.CustomerDOB))/365)  as CUSTOMER_AGE ", $sql );  
				
		}
		
		// if true of the campaign 
		
		if( $this -> URI->_get_have_post('CustomerCity') 
			AND $this -> URI->_get_post('CustomerCity')!='' )
		{
			$sql .= " AND b.CustomerCity LIKE '%{$this -> URI->_get_post('CustomerCity')}%' ";
		}
		
		// if true of the campaign 
		
		if( $this -> URI->_get_have_post('GenderId') 
			AND $this -> URI->_get_post('GenderId')!='' )
		{
			$sql .= " AND b.GenderId ='{$this -> URI->_get_post('GenderId')}'";
		}
		
		// startDate
		
		if( $this -> URI->_get_have_post('StartDate')
			AND $this -> URI->_get_post('StartDate')!='' ) 
		{
			$sql .= " AND date_format(b.Policy_Iss_Date, '%Y-%m') >='".date('Y-m', strtotime(_getDateEnglish($this -> URI->_get_post('StartDate'))) ) . "'
					  AND date_format(b.Policy_Iss_Date, '%Y-%m') <='".date('Y-m', strtotime(_getDateEnglish($this -> URI->_get_post('StartDate'))) ) . "'";
		}
		
		// ProductCategory 
		
		if( $this -> URI->_get_have_post('ProductCategory') 
			AND $this -> URI->_get_post('ProductCategory') )
		{
			$sql .= " AND e.Product_Category = '{$this -> URI->_get_post('ProductCategory')}' ";
		}
		
		// if true of the campaign 
		
		if( $this -> URI->_get_have_post('StartAge') 
			AND $this -> URI->_get_have_post('EndAge') )
		{
			$sql .= " GROUP BY a.AssignId HAVING CUSTOMER_AGE >= {$start_age} AND CUSTOMER_AGE <= {$end_age} ";
		}
	}
	// condition check available 
	
	$counter = array();
	
	if( $sql AND $this -> URI->_get_have_post('StartAge') 
		AND $this -> URI->_get_have_post('EndAge') )
	{
		$qry = $this -> db -> query($sql);
		foreach( $qry -> result_assoc() as $rows ) {
			$_conds+=1; 
		}
	}
	else
	{
		$qry = $this -> db -> query($sql);
		if( !$qry -> EOF() )
		{
			$_conds = $qry -> result_singgle_value();
		}	
	}

	return $_conds;
}


// SizePolicyData 

public function SizePolicyData() {
	
	
	$_conds = 0;	
	$this->db->reset_select();
	
	if( $sql = self::getParameter() )
	{
	
	  // rekonstruksi ----> 
	  
		if( $this -> URI->_get_have_post('StartAge') 
			AND $this -> URI->_get_have_post('EndAge') )
		{
			$start_age = $this -> URI->_get_post('StartAge');
			$end_age = $this -> URI->_get_post('EndAge');
			
			$sql = str_replace("count(a.AssignId) as jumlah", "COUNT(a.AssignId) as jumlah, a.AssignId as ID,  
			SUM(IF(e.Policy_Sales_Number IS NOT NULL, 1,0)) as total_policy,  FLOOR(DATEDIFF(NOW(), DATE(b.CustomerDOB))/365)  as CUSTOMER_AGE ", $sql );  
					 
				
		}
		
		// if true of the campaign 
		
		if( $this -> URI->_get_have_post('CustomerCity') 
			AND $this -> URI->_get_post('CustomerCity')!='' )
		{
			$sql .= " AND b.CustomerCity LIKE '%{$this -> URI->_get_post('CustomerCity')}%' ";
		}
		
		// if true of the campaign 
		
		if( $this -> URI->_get_have_post('GenderId') 
			AND $this -> URI->_get_post('GenderId')!='' )
		{
			$sql .= " AND b.GenderId ='{$this -> URI->_get_post('GenderId')}'";
		}
		
		// startDate
		
		if( $this -> URI->_get_have_post('StartDate')
			AND $this -> URI->_get_post('StartDate')!='' ) 
		{
			$sql .= " AND date_format(b.Policy_Iss_Date, '%Y-%m') >='".date('Y-m', strtotime(_getDateEnglish($this -> URI->_get_post('StartDate'))) ) . "'
					  AND date_format(b.Policy_Iss_Date, '%Y-%m') <='".date('Y-m', strtotime(_getDateEnglish($this -> URI->_get_post('StartDate'))) ) . "'";
		}
		
		// ProductCategory 
		
		if( $this -> URI->_get_have_post('ProductCategory') 
			AND $this -> URI->_get_post('ProductCategory') )
		{
			$sql .= " AND e.Product_Category = '{$this -> URI->_get_post('ProductCategory')}' ";
		}
		
		// if true of the campaign 
		
		if( $this -> URI->_get_have_post('StartAge') 
			AND $this -> URI->_get_have_post('EndAge') )
		{
			$sql .= " GROUP BY a.AssignId HAVING CUSTOMER_AGE >= {$start_age} AND CUSTOMER_AGE <= {$end_age} ";
		}
	}
	// condition check available 
	
	$counter = array();
	
	if( $sql AND $this -> URI->_get_have_post('StartAge') 
		AND $this -> URI->_get_have_post('EndAge') )
	{
		$qry = $this -> db -> query($sql);
		foreach( $qry -> result_assoc() as $rows ) {
			$_conds+=1; 
		}
	}
	else
	{	
		
		$sql = str_replace("count(a.AssignId) as jumlah", "COUNT(a.AssignId) AS jumlah, SUM(IF(e.Policy_Sales_Number IS NOT NULL, 1,0)) as total_policy", $sql);
		$qry = $this -> db -> query($sql);
		if( !$qry -> EOF() )
		{
			if( $rows = $qry -> result_first_assoc() ){
				$_conds =(INT)$rows['total_policy'];
			}
		}	
	}
	// echo "<pre>$sql</pre>";

	return $_conds;
}

// DistribusiMode

public function DistribusiMode() { 
	return array( 1 => 'Urutan',2 => 'Acak' );
}
 
// _get_size_data 
public function _get_size_data( $_array = null ) {
	
	$totals = 0;
	$User = null;
	if( isset($_array['level']) 
		AND isset($_array['UserId']) 
		AND isset($_array['CampaignId']) )
	{
		$User = $_array;
		$this -> db -> select('COUNT(a.AssignId) as SizeData');
		$this -> db -> from('t_gn_assignment a');
		$this -> db -> join('t_gn_debitur b', 'a.CustomerId=b.CustomerId', 'INNER');
		$this -> db -> join('t_gn_campaign c', 'b.CampaignId=c.CampaignId', 'LEFT');
		$this -> db -> where('c.CampaignId',$User['CampaignId']);
		$this -> db -> where("a.{$this ->_get_field_level($User['level'])}", $User['UserId']);
		
		
		if( $rows = $this -> db -> get() -> result_first_assoc() )
		{
			if( is_array($rows) ){
				$totals = $rows['SizeData'];
			}
		}
		
	}

	return $totals;
} 
  
// _get_automatic_distribusi

public  function _get_automatic_distribusi($LevelUser) {

	$_conds = array();
	$UserId = $this -> EUI_Session -> _get_session('UserId');
	
	/* ON USER LEVEL USER_ROOT */
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ROOT )
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.user_state',1);
	}	
	
	/* ON USER LEVEL USER_ADMIN */
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ADMIN )
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.user_state',1);	
	}
	
	/* ON USER LEVEL USER_ACCOUNT_MANAGER */
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.admin_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.act_mgr', $UserId);
		$this->db->where('a.user_state',1);	
	 }
	
	/* ON USER LEVEL USER_MANAGER */
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_MANAGER ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.act_mgr = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.mgr_id', $UserId);
		$this->db->where('a.user_state',1);	
	}
		
	/* ON USER LEVEL USER_SUPERVISOR */
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		// $this->db->join('t_tx_agent b ',' a.mgr_id = b.userid','LEFT');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.spv_id', $UserId);
		$this->db->where('a.user_state',1);	
	}
		
	/* ON USER LEVEL USER_LEADER */
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_LEADER ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.tl_id', $UserId);
		$this->db->where('a.user_state',1);	
	 }
	
	/* FILTER IF USER COOSE ON CHEKLIST FILTER */
	 
	 if( $this -> URI->_get_have_post('ListUserId') 
		AND is_array($this -> URI->_get_array_post('ListUserId'))  )
	  {
		 if( $int_select_users = array_values($this -> URI->_get_array_post('ListUserId')) )
		 {
			$this->db->where_in('a.UserId', $int_select_users);	
		 }
	  }
	  
	/* THEN PROCESS QUERY TO START HERE ... */
	  
	$this -> db -> order_by('a.full_name','ASC');

	$no =0;
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$int_rows_size  = $this -> _get_size_data(array(
			'CampaignId' => $this -> URI->_get_post('CampaignId'),
			'level' => $LevelUser,
			'UserId' => $rows['UserId']
		));
		
		
		/* OK IF TRU OR FALSE NO PROBLEMO :) */
		
		if( ($int_rows_size==TRUE) 
			OR ($int_rows_size ==FALSE) )
		{
			$_conds[$no]['UserId'] = $rows['UserId'];
			$_conds[$no]['id'] = $rows['id'];
			$_conds[$no]['full_name'] = strtoupper($rows['full_name']);
			$_conds[$no]['full_name_spv'] = strtoupper($rows['full_name_spv']);
			$_conds[$no]['size'] = (INT)$int_rows_size; 
			$no++;
		}	
	}
	
	return $_conds;
	
 }
// _get_manual_distribusi

public function _get_manual_distribusi( $LevelUser ) {

	$_conds = array();
	
	$UserId = $this -> EUI_Session -> _get_session('UserId');
	
	// ON USER LEVEL USER_ROOT 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ROOT )
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.user_state',1);


	}	
	
	// ON USER LEVEL USER_ADMIN
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ADMIN )
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.user_state',1);	
	}
	
	// ON USER LEVEL USER_ACCOUNT_MANAGER
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.admin_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.act_mgr', $UserId);
		$this->db->where('a.user_state',1);	
	 }			 
	 
	// ON USER LEVEL USER_MANAGER
		
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_MANAGER ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.act_mgr = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.mgr_id', $UserId);
		$this->db->where('a.user_state',1);	
	}
	
	// ON USER LEVEL USER_SUPERVISOR
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		// $this->db->join('t_tx_agent b ',' a.mgr_id = b.userid','LEFT');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.spv_id', $UserId);
		$this->db->where('a.user_state',1);	
	}
	
	// ON USER LEVEL USER_LEADER
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_LEADER ) 
	{
		$this->db->select('a.*,b.full_name as full_name_spv ');
		$this->db->from('t_tx_agent a');
		$this->db->join('t_tx_agent b ',' a.spv_id = b.userid','LEFT');
		$this->db->where('a.handling_type', $LevelUser);
		$this->db->where('a.tl_id', $UserId);
		$this->db->where('a.user_state',1);	
   }
   
   // FILTER IF USER COOSE ON CHEKLIST FILTER 
   
   if( $this -> URI->_get_have_post('ListUserId') 
	AND is_array($this -> URI->_get_array_post('ListUserId'))  )
  {
	 if( $int_select_users = array_values($this -> URI->_get_array_post('ListUserId')) )
	 {
		$this->db->where_in('a.UserId', $int_select_users);	
	 }
  }
  
  /* THEN PROCESS QUERY TO START HERE ... */
  
   $this -> db -> order_by('a.full_name','ASC');
  
   $no =0;
   foreach( $this->db->get()->result_assoc() as $rows ) 
   {
		/* GET SIZE OF DATA EVERY USER SELECTED **/
		
		$int_rows_size  = $this -> _get_size_data(array( 
			'CampaignId' => $this -> URI->_get_post('CampaignId'), 
			'level' => $LevelUser, 
			'UserId' => $rows['UserId']
		));
		
		/* SHOW DATA TRUE OR FALSE IS ONLY VIEW **/
		
		if( ($int_rows_size==TRUE) 
			OR ($int_rows_size ==FALSE) ) 
		{
			$_conds[$no]['UserId'] = $rows['UserId'];
			$_conds[$no]['id'] = $rows['id'];
			$_conds[$no]['full_name'] = strtoupper($rows['full_name']);
			$_conds[$no]['full_name_spv'] = strtoupper($rows['full_name_spv']);
			$_conds[$no]['size'] = (INT)$int_rows_size;
			$no++;
		}	
	}
	
	return $_conds;
	
 }
// _getOutboundGoals

public function _getOutboundGoals() {
	$_conds = array();
	$this -> db -> select('*');
	$this -> db -> from('t_lk_outbound_goals');
	
	foreach( $this -> db -> get()-> result_assoc() as $rows ) {
		$_conds[$rows['OutboundGoalsId']] = $rows['Description'];
	}
	
	return $_conds;
 }
 
// _getAssignData 

private function _getAssignData( $data = null ) 
{
	$_conds = array(); $sql = null;
	if( isset($data) AND !is_null($data) )
	{
		$sql =& self::getQueryList($data['CampaignId']);
		
		//cek filter options by category code 
		
		if( $this->URI->_get_have_post('ProductCategory') 
			AND $this->URI->_get_post('ProductCategory')!='' )
		{	
			$ProductCategory = $this->URI->_get_post('ProductCategory');
			if( $ProductCategory ) {
				$sql .= " AND e.Product_Category = '". urldecode($ProductCategory)."'";
			}	
		}
		
		// StartDate	Nov-2014
		// cek filter options by date 
		
		if( $this -> URI->_get_have_post('StartDate')
			AND $this -> URI->_get_post('StartDate')!='' ) 
		{
			$sql .= " AND date_format(b.Policy_Iss_Date, '%Y-%m') >='".date('Y-m', strtotime(_getDateEnglish($this -> URI->_get_post('StartDate'))) ) . "'
					  AND date_format(b.Policy_Iss_Date, '%Y-%m') <='".date('Y-m', strtotime(_getDateEnglish($this -> URI->_get_post('StartDate'))) ) . "'";
		}
		
		switch( $data['DistribusiMode'])
		{ 
			case 2 : $sql .= ' ORDER BY RAND() '; break;
			case 1 : $sql .= ' ORDER BY AssignId ASC '; break;
		}	
		
		$qry = $this -> db ->query($sql);
		$i=0;
		foreach( $qry -> result_assoc() as $rows )
		{
			$_conds[$i] = $rows['AssignId']; 
			$i++;
		}
	}
	
	return $_conds;
}

// _getReAssignData

private function _getReAssignData($Data = null ) {
	$_conds= array();
	
	if( !is_null($Data) )
	{
		if( isset($Data['fltUserId']) AND isset($Data['fltCallResultId']) 
			AND is_array($Data['fltUserId']) AND is_array($Data['fltCallResultId']) )
		{
			$this ->db ->select('b.AssignId',TRUE);
			$this ->db ->from('t_gn_debitur a');
			$this ->db ->join('t_gn_assignment b', 'a.CustomerId=b.CustomerId','LEFT JOIN');
			$this ->db ->where('a.CampaignId', $Data['CampaignId']);
			$this ->db ->where_in('b.AssignSelerId',$Data['fltUserId']);
			$this ->db ->where_in('a.CallReasonId',$Data['fltCallResultId']);
			
			if( (INT)$Data['DistribusiMode']==1){
				$this ->db ->order_by('b.AssignId','ASC');
			}
			
			if( (INT)$Data['DistribusiMode']==2){
				$this ->db ->order_by('RAND()','',TRUE);
			}
			
			$num = 0;
			foreach($this -> db -> get()->result_assoc() as $rows)
			{
				$_conds[$num] = $rows['AssignId']; 
				$num++;
			}
		}
	}
	return $_conds;
}
	
	
// _setAgentDistribusi

public function _setAgentDistribusi($data) {

 $_conds = 0; $data['Data'] =& self::_getAssignData($data);	
 
 if(class_exists('M_ModDistribusi')) 
 {
	if( $data['DistribusiType']==1) 
	{
		$_conds = $this->M_ModDistribusi->_setDistribusi($data, 'ManualDistribusi');
	}
    
	if( $data['DistribusiType']==2)
	{
		$_conds = $this->M_ModDistribusi->_setDistribusi($data, 'AutomaticDistribusi');
	}	
  }
  
  return $_conds;	
  
}

 
// _setReAssignment

public function _setReAssignment($Data) {
	$_conds = 0; $Data['Data'] =& self::_getReAssignData($Data);
	
	if(class_exists('M_ModDistribusi')) 
	{
		switch($Data['DistribusiType'])
		{
			case 1 : 
				$_conds = $this->M_ModDistribusi->_setReAssigment($Data, 'ManualReassignment');
			break;
			
			case 2 : 	
				$_conds = $this->M_ModDistribusi->_setReAssigment($Data, 'AutomaticReassignment');
			break;
		}	
	}

	return $_conds;	
}
 
// get campaign Type ID  

public function _getCampaignType( $CampaignId = 0 ) {
	$_conds = null;
	
	$this -> db -> select("OutboundGoalsId");
	$this -> db -> from("t_gn_campaign");
	$this -> db -> where("CampaignId", $CampaignId);
	
	if( $rows = $this -> db -> get()->result_first_assoc() ) 
	{
		$_conds = $rows['OutboundGoalsId'];	
	} 
	
	return $_conds;
}

// _getShowDataDetail

public  function _getShowDataDetail($Data=null) {
	$_counter = 0;
	
	if( !is_null($Data) )
	{
		$interest = $this->M_SetCallResult->_getInterestSale();

		$this ->db ->select('*',TRUE);
		$this ->db ->from('t_gn_debitur a');
		$this ->db ->join('t_gn_assignment b', 'a.CustomerId=b.CustomerId','LEFT JOIN');
		$this ->db ->where('a.CampaignId', $Data['CampaignId']);
		$this ->db ->where_not_in('a.CallReasonId',array_keys($interest)); 
		
		if( isset($Data['UserId']) 
			AND is_array($Data['UserId']) )
		{
			$this -> db -> where_in('b.AssignSelerId',$Data['UserId']);
		}
		
		if( isset($Data['CallResultId']) 
			AND is_array($Data['CallResultId']) ) {
			$this -> db -> where_in('a.CallReasonId',$Data['CallResultId']);
		}

		// return $this->db->_get_var_dump();

		$_counter = $this -> db->get();
	}
	
	return $_counter;
}

// get Hide table 

public  function _getHideTables() {
	
	$this -> db->select("*");
	$this -> db->from("t_gn_hide_tables");
	$this -> db->where_in("table_name",array("t_gn_bucket_customers","t_gn_assignment"));
	
	$viewfield = array();
	$fieldname = array();
	$res = $this -> db->get();
	if($res) foreach($res->result_assoc() as $rows ){
		$fieldname[$rows['table_field_name']] = $rows['table_field_name']; 
	}
	
	$skip_arrays = array('CustomerNumber');
	$array_show = self::_getListFieldShowTables();
	
	// data name 
	
	foreach( $array_show as $key => $values )
	{
		if( !in_array( $key, array_keys($fieldname) ) )
		{
			if( !in_array($key, $skip_arrays )){
				$viewfield[$key] = $values;
			}
		}
	}
	//print_r(self::_getListFieldShowTables());
	return $viewfield;
	
	
}

// function _getListFieldShowTables

private function _getListFieldShowTables() {
	$fld = array();
	$num = 0;
	foreach( $this->db->list_fields('t_gn_bucket_customers') as $fieldname )
	{
		$fld[$fieldname] = 	$fieldname;
		$num++;
	}
	
	foreach( $this->db->list_fields('t_gn_assignment') as $fieldname )
	{
		$fld[$fieldname] = 	$fieldname;
		$num++;
	}
	
	return $fld;
}

// public function get jumlah Customer ALL **/

public function _AmountCustomerData( $UserId=null, $HandlingType = null, $CampaignNumber = null ){
  $this->db->reset_select();
  $conds = array();
  if(is_array($UserId) AND !is_null($HandlingType))
  {
	
	$this->levels = null;
	$this->index_in = null;
	
  /** level manager 	***/
  
	if( $HandlingType == USER_MANAGER )
	{
		$this->levels  = array ( 'COUNT(distinct a.EnigmaId) as tots_size_data', 'c.AssignMgr' );
			
		$this->db->where("c.AssignMgr IS NOT NULL");
		$this->db->where_in('c.AssignMgr', $UserId);
		if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
		if(in_array(2,$this->EUI_Session->_get_session('ProjectId'))){
			$this->db->where('a.CallComplete','0');
			$this->db->where('b.CallComplete','0');
		}
		$this->db->group_by('c.AssignMgr');
		
		$this->index_in = 'AssignMgr';
	 }
	 
/** level SPV OR Leader **/
 
	 if( $HandlingType == USER_SUPERVISOR )
	{
		// select 
		
		$this->levels  = array ( 'COUNT(distinct a.EnigmaId) as tots_size_data', 'c.AssignSpv' );
			
		// condition 
		
		$this->db->where("c.AssignSpv IS NOT NULL");
		$this->db->where_in('c.AssignSpv', $UserId);
		if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
		if(in_array(2,$this->EUI_Session->_get_session('ProjectId'))){
			$this->db->where('a.CallComplete','0');
			$this->db->where('b.CallComplete','0');
		}
		$this->db->group_by('c.AssignSpv');
		
		$this->index_in = 'AssignSpv';
		
	 }
	 
/** level Caller Name OR Leader **/
 
	 if( $HandlingType == USER_AGENT_OUTBOUND )
	{
		// select 
		
		$this->levels  = array ( 'COUNT(distinct a.EnigmaId) as tots_size_data', 'c.AssignSelerId' );
			
		// condition 
		
		$this->db->where("c.AssignSelerId IS NOT NULL");
		$this->db->where_in('c.AssignSelerId', $UserId);
		if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
		if(in_array( self::_get_config_value(),$this->EUI_Session->_get_session('ProjectId'))){
			$this->db->where('a.CallComplete','0');
			$this->db->where('b.CallComplete','0');
		}
		$this->db->group_by('c.AssignSelerId');
		$this->index_in = 'AssignSelerId';
		
		
	 }
		
		 
/** generate data **/

	if( !is_null($this->levels) AND is_array($this->levels) )
	{
		$this->db->select(implode(",",$this->levels), FALSE);
		$this->db->from('t_gn_policy_detail a ');
		$this->db->join('t_gn_debitur b ',' a.EnigmaId=b.CustomerNumber','LEFT');
		$this->db->join('t_gn_assignment c ',' b.CustomerId=c.CustomerId','LEFT');
		$this->db->join('t_gn_campaign d', ' b.CampaignId=d.CampaignId','LEFT');
		$this->db->where("a.CallComplete = 0");
		// echo $this->db->_get_var_dump();
		foreach($this->db->get()->result_assoc() as $rows )
		{
			$conds[$rows[$this->index_in]]+= $rows['tots_size_data'];
		}
	}
	
  }
  
  return $conds;

	
}

// public function get jumlah Customer ALL **/

public function _getCheckAmountOfDataPerCampaign($UserId = null, $HandlingType = null, $CampaignNumber = null){
	
	$config =& self::_get_config_value();
	$conds = array();
	if(is_array($UserId) AND !is_null($HandlingType))
	{
		$this->leves = null;
		$this->index_in = null;
		
	if( $HandlingType == USER_AGENT_OUTBOUND )
	{
		// $this->levels  = array( 'COUNT(DISTINCT b.CustomerNumber) AS tots_size_data', 'SUM(IF(b.CallReasonId IS NULL OR b.CallReasonId = 0, 1, 0)) AS tot_new_data',
					// 'SUM(IF((b.CallReasonId != 0) AND b.CallComplete = 0, 1, 0)) AS tot_ped_data', 'c.AssignSelerId');
					
		$this->levels  = array( 'COUNT(DISTINCT b.PolicyNumber) AS tots_size_data', 'SUM(IF(b.CallReasonId IS NULL OR b.CallReasonId = 0, 1, 0)) AS tot_new_data',
					'SUM(IF((b.CallReasonId != 0) AND b.CallComplete = 0, 1, 0)) AS tot_ped_data', 'c.AssignSelerId');
				
			$this->db->where("c.AssignSelerId IS NOT NULL");
			$this->db->where_in('c.AssignSelerId', $UserId);
			
			
			// if have campaign 
			
			if(!is_null($CampaignNumber)){
				$this->db->where('d.CampaignNumber', $CampaignNumber );
			}
			
			// $this->db->where('a.CallComplete',0);
			// $this->db->where('b.CallComplete',0);
			$this->db->group_by('c.AssignSelerId');
			
			$this->index_in = 'AssignSelerId';
		}
	
// get data by next level 
	
	 if( $HandlingType == USER_SUPERVISOR )
	{
		// $this->levels  = array( 'COUNT(DISTINCT b.CustomerNumber) AS tots_size_data', 'SUM(IF(b.CallReasonId IS NULL OR b.CallReasonId = 0, 1, 0)) AS tot_new_data',
					// 'SUM(IF((b.CallReasonId != 0) AND b.CallComplete = 0, 1, 0)) AS tot_ped_data', 'c.AssignSelerId');
					
		$this->levels  = array( 'COUNT(DISTINCT b.PolicyNumber) AS tots_size_data', 'SUM(IF(b.CallReasonId IS NULL OR b.CallReasonId = 0, 1, 0)) AS tot_new_data',
					'SUM(IF((b.CallReasonId != 0) AND b.CallComplete = 0, 1, 0)) AS tot_ped_data', 'c.AssignSelerId');
			
		// condition 
		
		$this->db->where("c.AssignSpv IS NOT NULL");
		$this->db->where_in('c.AssignSpv', $UserId);
		if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
			
		$this->db->where('a.CallComplete','0');
		$this->db->where('b.CallComplete','0');
		$this->db->group_by('a.CallReasonId');
		$this->db->group_by('c.AssignSpv');
		$this->index_in = 'AssignSpv';
		
	 }
	 
		
		if( !is_null($this->levels) AND is_array($this->levels) )
		{
			if( $HandlingType == USER_AGENT_OUTBOUND){
				$this->db->select(implode(",",$this->levels), FALSE);
				$this->db->from('t_gn_policy_detail b ');
				$this->db->join('t_gn_debitur e ',' b.EnigmaId = e.CustomerNumber','LEFT');
				$this->db->join('t_gn_assignment c ',' e.CustomerId=c.CustomerId','LEFT');
				$this->db->join('t_gn_campaign d ',' e.CampaignId=d.CampaignId','LEFT');
				foreach($this->db->get()->result_assoc() as $rows )
				{
					$conds[$rows[$this->index_in]]['new_data'] += $rows['tot_new_data'];
					$conds[$rows[$this->index_in]]['ped_data'] += $rows['tot_ped_data'];
					$conds[$rows[$this->index_in]]['cmp_code'] = $rows['CampaignCode'];
				}
			}else if( $HandlingType == USER_SUPERVISOR){
				$this->db->select(implode(",",$this->levels), FALSE);
				$this->db->from('t_gn_debitur b ');
				$this->db->join('t_gn_assignment c ',' b.CustomerId=c.CustomerId','LEFT');
				$this->db->join('t_gn_campaign d', ' b.CampaignId=d.CampaignId','LEFT');
				echo $this->db->_get_var_dump();
				foreach($this->db->get()->result_assoc() as $rows )
				{
					$conds[$rows[$this->index_in]]['new_data'] += $rows['tot_new_data'];
					$conds[$rows[$this->index_in]]['ped_data'] += $rows['tot_ped_data'];
					$conds[$rows[$this->index_in]]['cmp_code'] = $rows['CampaignCode'];
				}
			}
		}
	}
	return $conds;
}
 
// _getCheckAmountOfData
 
public function _getCheckAmountOfData($UserId=null, $HandlingType = null, $CampaignNumber = null ) {
	
  $conds = array();
  if(is_array($UserId) AND !is_null($HandlingType))
  {
	
	$this->levels = null;
	$this->index_in = null;
	
  /** level manager 	***/
  
	if( $HandlingType == USER_MANAGER )
	{
		$this->levels  = array ( 'COUNT(distinct a.EnigmaId) as tots_size_data', 'COUNT(distinct a.PolicyNumber) as tots_size_policy', 
			'IF( a.CallReasonId is null,0, a.CallReasonId) as callSizeByReasonId', 'SUM(b.CallAttempt) as CallAttempt', 'c.AssignMgr' );
			
		$this->db->where("c.AssignMgr IS NOT NULL");
		$this->db->where_in('c.AssignMgr', $UserId);
if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
		if(in_array( self::_get_config_value(),$this->EUI_Session->_get_session('ProjectId'))){
			$this->db->where('a.CallComplete','0');
			$this->db->where('b.CallComplete','0');
		}
		$this->db->group_by('a.CallReasonId');
		$this->db->group_by('c.AssignMgr');
		
		$this->index_in = 'AssignMgr';
	 }
	 
/** level SPV OR Leader **/
 
	 if( $HandlingType == USER_SUPERVISOR )
	{
		// select 
		
		$this->levels  = array ( 'COUNT(distinct a.EnigmaId) as tots_size_data', 'COUNT(distinct a.PolicyNumber) as tots_size_policy', 
			'IF( a.CallReasonId is null,0, a.CallReasonId) as callSizeByReasonId','SUM(b.CallAttempt) as CallAttempt', 'c.AssignSpv' );
			
		// condition 
		
		$this->db->where("c.AssignSpv IS NOT NULL");
		$this->db->where_in('c.AssignSpv', $UserId);
		if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
		if(in_array( self::_get_config_value(),$this->EUI_Session->_get_session('ProjectId'))){
			$this->db->where('a.CallComplete','0');
			$this->db->where('b.CallComplete','0');
		}
		$this->db->group_by('a.CallReasonId');
		$this->db->group_by('c.AssignSpv');
		
		$this->index_in = 'AssignSpv';
		
	 }
	 
/** level Caller Name OR Leader **/
 
	 if( $HandlingType == USER_AGENT_OUTBOUND )
	{
		// select 
		
		$this->levels  = array ( 'COUNT(distinct a.EnigmaId) as tots_size_data', 'COUNT(distinct a.PolicyNumber) as tots_size_policy', 
			'IF( a.CallReasonId is null,0, a.CallReasonId) as callSizeByReasonId','SUM(b.CallAttempt) as CallAttempt', 'c.AssignSelerId' );
			
		// condition 
		
		$this->db->where("c.AssignSelerId IS NOT NULL");
		$this->db->where_in('c.AssignSelerId', $UserId);
		if(!is_null($CampaignNumber)){
			$this->db->where_in('d.CampaignNumber', $CampaignNumber);
		}
		if(in_array( self::_get_config_value(),$this->EUI_Session->_get_session('ProjectId'))){
			$this->db->where('a.CallComplete','0');
			$this->db->where('b.CallComplete','0');
		}
		$this->db->group_by('a.CallReasonId');
		$this->db->group_by('c.AssignSelerId');
		
		$this->index_in = 'AssignSelerId';
		
		
	 }
		 
/** generate data **/
	
	if( !is_null($this->levels) AND is_array($this->levels) )
	{
		$this->db->select(implode(",",$this->levels), FALSE);
		$this->db->from('t_gn_policy_detail a ');
		$this->db->join('t_gn_debitur b ',' a.EnigmaId=b.CustomerNumber','LEFT');
		$this->db->join('t_gn_assignment c ',' b.CustomerId=c.CustomerId','LEFT');
		$this->db->join('t_gn_campaign d', ' b.CampaignId=d.CampaignId','LEFT');
		// echo $this->db->_get_var_dump();
		foreach($this->db->get()->result_assoc() as $rows )
		{
			$conds[$rows[$this->index_in]][$rows['callSizeByReasonId']]['size_data'] += $rows['tots_size_data'];
			$conds[$rows[$this->index_in]][$rows['callSizeByReasonId']]['size_policy'] += $rows['tots_size_policy'];
			$conds[$rows[$this->index_in]][$rows['callSizeByReasonId']]['CallAttempt'] += $rows['CallAttempt'];
		}
	}
	
  }
  
  return $conds;
  
	
 }
 
// _get_today_privileges
 
public function _get_policy_today($CampaignId=0, $_status='') {

	$this->db->reset_select();
	$_totals = 0;
	
	$sql = "SELECT COUNT(b.CustomerId) AS policy_size
			FROM t_gn_campaign a
			INNER JOIN t_gn_debitur b ON a.CampaignId = b.CampaignId
			INNER JOIN t_gn_assignment c ON b.CustomerId=c.CustomerId
			LEFT JOIN t_gn_policy_detail d on b.CustomerNumber = d.EnigmaId
			LEFT JOIN t_gn_cif_customer e on (b.CustomerNumber = e.EnigmaId AND b.CIFNumber = e.CIFNumber)
			LEFT JOIN t_gn_upload_report_ftp f on e.UploadId = f.FTP_UploadId
			WHERE 1=1
			AND a.CampaignId = '".$CampaignId."' 
			AND a.CampaignStatusFlag IN (".implode(',',array(self::$Active, self::$NotActive)).") 
			AND date(f.FTP_UploadDateTs) = date(NOW()) ";
			
	/* if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ROOT )	{
		$sql .= ' 	AND c.AssignAmgr IS NULL
					AND c.AssignMgr IS NULL
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ';
	}
	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ADMIN ){
		$sql .= ' 	AND c.AssignAmgr IS NULL
					AND c.AssignMgr IS NULL
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ';
	}
						 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ACCOUNT_MANAGER )	{
		$sql .= " 	AND c.AssignAmgr = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignMgr IS NULL
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ";
	}
					 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_MANAGER ){
		$sql .= " 	AND c.AssignMgr = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ";
	}
	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_SUPERVISOR ){
		$sql .= " 	AND c.AssignMgr IS NOT NULL
					AND c.AssignSpv = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignSelerId IS NULL ";
	}

	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_LEADER )	{
		$sql .= " 	AND c.AssignMgr IS NOT NULL
					AND c.AssignSpv IS NOT NULL
					AND c.AssignLeader = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignSelerId IS NULL ";
	} */
	// echo $sql;
	$qry = $this->db->query($sql);
	$_totals = $qry->result_first_assoc();
	
	return $_totals['policy_size'];
}
// _get_today_privileges

public function _get_today_privileges($CampaignId=0, $_status='') {
	$_totals = array();
	
	$sql = "SELECT 
				count(b.CustomerId) as jumlah, 
				a.CampaignStatusFlag as conds 
			FROM t_gn_campaign a 
			INNER JOIN t_gn_debitur b ON a.CampaignId = b.CampaignId 
			INNER JOIN t_gn_assignment c ON b.CustomerId = c.CustomerId 
			LEFT JOIN t_gn_cif_customer e ON (b.CustomerNumber = e.EnigmaId AND b.CIFNumber = e.CIFNumber)
			LEFT JOIN t_gn_upload_report_ftp f ON e.UploadId = f.FTP_UploadId
			WHERE 1=1 
			AND a.CampaignId = '".$CampaignId."' 
			AND a.CampaignStatusFlag IN (".implode(',',array(self::$Active, self::$NotActive)).") 
			AND date(f.FTP_UploadDateTs) = date(NOW()) ";
	
	/* if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ROOT )	{
		$sql .= ' 	AND c.AssignAmgr IS NULL
					AND c.AssignMgr IS NULL
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ';
	}
	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ADMIN ){
		$sql .= ' 	AND c.AssignAmgr IS NULL
					AND c.AssignMgr IS NULL
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ';
	}
						 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_ACCOUNT_MANAGER )	{
		$sql .= " 	AND c.AssignAmgr = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignMgr IS NULL
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ";
	}
					 
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_MANAGER ){
		$sql .= " 	AND c.AssignMgr = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignSpv IS NULL
					AND c.AssignSelerId IS NULL ";
	}
	
	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_SUPERVISOR ){
		$sql .= " 	AND c.AssignMgr IS NOT NULL
					AND c.AssignSpv = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignSelerId IS NULL ";
	}

	else if( $this -> EUI_Session -> _get_session('HandlingType') == USER_LEADER )	{
		$sql .= " 	AND c.AssignMgr IS NOT NULL
					AND c.AssignSpv IS NOT NULL
					AND c.AssignLeader = '".$this->EUI_Session->_get_session('UserId')."'
					AND c.AssignSelerId IS NULL ";
	} */
	
	// echo $sql;
	$qry = $this->db->query($sql);
	
	foreach( $qry -> result_assoc() as $rows ){
		$_totals[$rows['conds']]+= $rows['jumlah'];
	}
	
	if( $_status !='' ) 
		return (INT)$_totals[$_status];
	else
	{
		return (($_totals[self::$Active])+($_totals[self::$NotActive]));
	}
}
 
 /* END OF UPDATE 19-11-2014 ----------------------------------------------------------------------------------------------------------------------------- */
	public function _getCheckAmountCustomerPerCampaign($UserId = null, $HandlingType = null, $CampaignNumber = null){
	
		$config =& self::_get_config_value();
		$conds = array();
		if(is_array($UserId) AND !is_null($HandlingType)){
			$this->leves = null;
			$this->index_in = null;
		if( $HandlingType == USER_AGENT_OUTBOUND ){
			$this->levels  = array( 'COUNT(DISTINCT b.CustomerId) AS tots_size_data',
									'SUM(IF(b.CallReasonId IS NULL OR b.CallReasonId = 0, 1, 0)) AS tot_new_data',
									'SUM(IF((b.CallReasonId != 0) AND b.CallComplete = 0, 1, 0)) AS tot_ped_data',
									'c.AssignSelerId');
			$this->db->where("c.AssignSelerId IS NOT NULL");
			$this->db->where_in('c.AssignSelerId', $UserId);
			
			// if have campaign 
			if(!is_null($CampaignNumber)){
				$this->db->where('d.CampaignNumber', $CampaignNumber );
			}
			// $this->db->where('a.CallComplete',0);
			// $this->db->where('b.CallComplete',0);
			$this->db->group_by('c.AssignSelerId');
				
			$this->index_in = 'AssignSelerId';
		}
	
		// get data by next level 
		if( $HandlingType == USER_SUPERVISOR ){
			$this->levels  = array( 'COUNT(DISTINCT b.CustomerId) AS tots_size_data',
									'SUM(IF(b.CallReasonId IS NULL OR b.CallReasonId = 0, 1, 0)) AS tot_new_data',
									'SUM(IF((b.CallReasonId != 0) AND b.CallComplete = 0, 1, 0)) AS tot_ped_data',
									'c.AssignSelerId');
			$this->db->where("c.AssignSpv IS NOT NULL");
			$this->db->where_in('c.AssignSpv', $UserId);
			if(!is_null($CampaignNumber)){
				$this->db->where_in('d.CampaignNumber', $CampaignNumber);
			}
			// $this->db->where('a.CallComplete','0');
			// $this->db->where('b.CallComplete','0');
			$this->db->group_by('b.CallReasonId');
			$this->db->group_by('c.AssignSpv');
			$this->index_in = 'AssignSpv';
		}

		if( !is_null($this->levels) AND is_array($this->levels) ){
			if( $HandlingType == USER_AGENT_OUTBOUND){
				$this->db->select(implode(",",$this->levels), FALSE);
				$this->db->from('t_gn_debitur b ');
				$this->db->join('t_gn_assignment c ',' b.CustomerId = c.CustomerId','LEFT');
				$this->db->join('t_gn_campaign d ',' b.CampaignId=d.CampaignId','LEFT');
				// echo $this->db->_get_var_dump();
				foreach($this->db->get()->result_assoc() as $rows )
				{
					$conds[$rows[$this->index_in]]['new_data'] += $rows['tot_new_data'];
					$conds[$rows[$this->index_in]]['ped_data'] += $rows['tot_ped_data'];
					$conds[$rows[$this->index_in]]['cmp_code'] = $rows['CampaignCode'];
				}
			}else if( $HandlingType == USER_SUPERVISOR){
				$this->db->select(implode(",",$this->levels), FALSE);
				$this->db->from('t_gn_debitur b ');
				$this->db->join('t_gn_assignment c ',' b.CustomerId=c.CustomerId','LEFT');
				$this->db->join('t_gn_campaign d', ' b.CampaignId=d.CampaignId','LEFT');
				echo $this->db->_get_var_dump();
				foreach($this->db->get()->result_assoc() as $rows )
				{
					$conds[$rows[$this->index_in]]['new_data'] += $rows['tot_new_data'];
					$conds[$rows[$this->index_in]]['ped_data'] += $rows['tot_ped_data'];
					$conds[$rows[$this->index_in]]['cmp_code'] = $rows['CampaignCode'];
				}
			}
		}
	}
	return $conds;
}
 // end class 
 
 
}

?>