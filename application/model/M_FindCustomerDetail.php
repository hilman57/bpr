<?php
/**
 * @ def : FindCustomerDetail class 
 * -------------------------------------------
 
 * @ package : controller 
 * @ subject : operation collection ( AIA )
 *
 */
 
class M_FindCustomerDetail extends EUI_Model
{

 private $TotRecords = 0;
 private $TotPages = 0;
 private $PerPage= 0;
 private $Recsource = 0;

/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
public function __construct()
{
	$this->PerPage = 10;
}


/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 

public function _set_wheres()
{
/** set key projectID **/

 if( $this->EUI_Session->_have_get_session('ProjectId') ) {
	$this->db->where_in('g.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
 }
 
 
/** set key cif customer_name **/

 if( $this->URI->_get_have_post('policy_number') ) {
	$this->db->where('a.PolicyNumber', $this->URI->_get_post('policy_number') );
 }
 
 /** set key cif customer_name **/
 
 if( $this->URI->_get_have_post('customer_name') ) {
	$this->db->like('a.PolicyFirstName', $this->URI->_get_post('customer_name') );
 }
 
/** set key cif customer_name **/
	
  if( $this->URI->_get_have_post('cif_number') ){
	$this->db->where('a.CIFNumber', $this->URI->_get_post('cif_number'));
 }
 
}


/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
private function record_data() 
{
	$this->db->select("a.PolicyId");
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b","a.EnigmaId= b.CustomerNumber","INNER");
	$this->db->join("t_gn_assignment d "," b.CustomerId=d.CustomerId","LEFT");
	$this->db->join("t_lk_account_status c ","  b.CallReasonId=c.CallReasonId","LEFT");
	$this->db->join("t_tx_agent e "," d.AssignSelerId=e.UserId","LEFT");
	$this->db->join("t_tx_agent f "," b.QueueId=f.UserId","LEFT"); 
	$this->db->join("t_gn_campaign_project g "," b.CampaignId=g.CampaignId","LEFT");
	
	$this->_set_wheres();
	
	$recsource = $this->db->get();
	if( $recsource->num_rows() > 0)  
	{ 
		$this->TotRecords = $recsource->num_rows();
	} 
 }
 
 
/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
 
public function get_record_data() 
{

 self::record_data();
 $rcd = 0;
 if( !is_null($this->TotRecords) )
 {
	$rcd  = $this->TotRecords;
 }	
 return $rcd;	
}
 
/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
 public function total_page()
 {
	$pages = array();
	
	if( $this->TotRecords ) 
	{
		$this->TotPages = ceil($this->TotRecords/$this->PerPage);
		if($this->TotPages)
			for( $p = 1; $p<=$this->TotPages; $p++ ) 
		{
			$pages[$p] = $p;	
		}
	}
	
	return $pages;
 }
 
 /**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
 public function recsource_page_data()
 {
	$source = array();
  
	$this->db->select("	
		b.CustomerId, 
		a.EnigmaId, 
		a.CIFNumber, 
		a.PolicyNumber, 
		a.PolicyId, 
		a.PolicyFirstName, 
		b.CustomerFirstName, 
		b.CallAttempt, 
		IF(c.CallReasonDesc is null, '-', c.CallReasonDesc) as CallResult,
		IF(e.full_name is null, '-',e.full_name) as UserName,
		IF(f.full_name is null, '-', f.full_name) as QualityName,
		DATE_FORMAT(a.PolicyIssDate, '%d/%m/%Y') AS PolicyIssDate, 
		DATE_FORMAT(a.PolicyUploadDate, '%d/%m/%Y') AS PolicyUploadDate, 
		DATE_FORMAT(a.PolicyUpdatedTs, '%d/%m/%Y %H:%i:%s') AS PolicyUpdatedTs", 
	FALSE);
			
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b","a.EnigmaId= b.CustomerNumber","INNER");
	$this->db->join("t_gn_assignment d "," b.CustomerId=d.CustomerId","LEFT");
	$this->db->join("t_lk_account_status c ","  b.CallReasonId=c.CallReasonId","LEFT");
	$this->db->join("t_tx_agent e "," d.AssignSelerId=e.UserId","LEFT");
	$this->db->join("t_tx_agent f "," b.QueueId=f.UserId","LEFT"); 
	$this->db->join("t_gn_campaign_project g "," b.CampaignId=g.CampaignId","LEFT");
	$this->_set_wheres();
	
	
	if( $this -> URI->_get_have_post('page')) 
	{
		if((INT)_get_post('page')> 0 )
			$start = (( ((INT)_get_post('page')) -1 ) * $this->PerPage );
		else
			$start = 0;
   }	
   
   $this -> db -> limit($this -> PerPage,$start); 
   $num = $start+1;
   
   $this->db->limit(10);
   if( $recsource = $this->db->get() ) 
   {
		$num = $start+1;
		foreach( $recsource ->result_assoc() as $rows )
		{
			$source[$num]= $rows;
			$num++;
		}
	
	}
	
	return $source;
	
 }

/*
 * @ def 		: get policy data by customer ID AND by Upload ID **
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 

 public function _getPolicyCustomer($PolicyId=0 )
 {
	$datas = array();
	
	$this->db->select('b.PolicyId, b.PolicyNumber');
	$this->db->from('t_gn_debitur a');
	$this->db->join('t_gn_policy_detail b ',' a.CustomerNumber=b.EnigmaId', 'LEFT');
	$this->db->where('b.PolicyId', $PolicyId);
	
	foreach($this->db->get()->result_assoc() as $rows){
		$datas[$rows['PolicyId']] = $rows['PolicyNumber'];
	}
	
	return $datas;
 }
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 public function _getDetailCustomer($CustomerId=null)
 {
	$_conds = array();
	
	$this -> db -> select('a.*, b.Gender, f.id, f.full_name, c.Salutation, d.CampaignName, d.CampaignNumber, e.CallReasonDesc, e.CallReasonCategoryId ', FALSE);
	$this -> db -> from('t_gn_debitur a');
	$this -> db -> join('t_lk_gender b','a.GenderId=b.GenderId','LEFT');
	$this -> db -> join('t_lk_salutation c','a.SalutationId=c.SalutationId','LEFT');
	$this -> db -> join('t_gn_campaign d','a.CampaignId=d.CampaignId','LEFT');
	$this -> db -> join('t_lk_account_status e','a.CallReasonId=e.CallReasonId','LEFT');
	$this -> db -> join('t_gn_assignment g','a.CustomerId=g.CustomerId','LEFT');
	$this -> db -> join('t_tx_agent f','g.AssignSelerId=f.UserId','LEFT');
	$this -> db -> where('a.CustomerId',$CustomerId);
	
	foreach($this -> db ->get() -> result_assoc() as $rows )
	{
		foreach($rows as $k => $v ){
			$_conds[$k] = $v;
		}
	}
	
	return $_conds;
 }
 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getLastCallHistory($PolicyId)
 {
	$_conds = array();
	
	$this -> db -> select('*');
	$this -> db -> from('t_gn_callhistory');
	$this -> db -> where('PolicyId',$PolicyId);
	
	if( $avail = $this -> db -> get()->result_first_assoc() )
	{
		$_conds = $avail;
	}
	
	return $_conds;
 }
 
}

?>