<?php

class M_WriteReportUpload extends EUI_Model
{	

// M_WriteReportUpload

public function M_WriteReportUpload()
{
	$this->load->model('M_Configuration');	
	//_getMail 
}	


// public _get_config_mail

public function _get_config_mail( $WorkCode  = null )
{
		static $config = array();
		$_BASE_PATH  = BASEPATH ."keys/" . MD5('asdf') . ".ini";
		
		if( file_exists($_BASE_PATH)) 
		{
			$config = parse_ini_file( $_BASE_PATH, TRUE);
	
		}
		
		if( is_array( $config[$WorkCode] ) )
		{
		
			return $config[$WorkCode];
		}
		else
			return false;
	}
	
	
// _get_work_project_code

private function _get_work_project_code( $ProjectId = NULL )
{
		$config = null;
		
		$this->db->reset_select();
		$this->db->select('a.ftp_read_project_code');
		$this->db->from('t_lk_ftp_read a ');
		$this->db->where('a.ftp_read_id', $ProjectId );
		if( $rs = $this->db->get() -> result_first_assoc() ) 
		{
			if( $rs ) 
			{
				$config = trim($rs['ftp_read_project_code']);	
			}	
		}
		return $config;
	}
	
// getContent 

public function getContent($uploadId='')
{
	$this->db->select("*");
	$this->db->from("v_rpt_upload_cmp");

		$str='';
		$res = $this->db->get()->result_assoc();

		$str .= "Daily Upload Report";
		$str .= "</br>";
		$str .= "<table border=1 BORDERCOLOR=BLUE>";
		$No=1;
		$str .= "<tr align='left'><th>No</th><th>Campaign Code</th><th>Row uploaded</th></tr>";
		if ($res) foreach ($res as $key => $value) {
			$str .=  "<tr>";
			$str .=  "<td>$No</td><td>$value[Campaign_Master_Code]</td><td>$value[rows_uploaded]</td>";
			$str .=  "</tr>";
			$No++;
		}
		$str .=  "</table>";

		return $str;
	}
	
// public function getContent2 

public function getContent2($uploadId='', $SourceId='')
{ 
	$this->db->select("d.ProjectName, a.BuketUploadId, a.Campaign_Master_Code, c.CampaignName, count(a.BucketId) row_upload, b.* ");
	$this->db->from("t_gn_bucket_customers a","");
	$this->db->join("t_gn_upload_report_ftp b","b.FTP_UploadId = a.BukcetSourceId","left");
	$this->db->join("t_gn_campaign c","c.CampaignCode=a.Campaign_Master_Code","left");
	$this->db->join("t_lk_work_project d", "d.ProjectId=a.BuketUploadId","left");
	$this->db->where("a.BuketUploadId", $uploadId );
	$this->db->where("a.BukcetSourceId", $SourceId );
	$this->db->group_by("Campaign_Master_Code");
	
	$res = $this->db->get();
	$str = '';
	$project = '';
	$str .= " Daily Upload Report";
	$str .= "<p>Generated : ".date("d-m-Y H:i:s");
	$str .= "<p>";
	$str .= "<table border=1 BORDERCOLOR=BLUE>";
	$No=1;
	
	$str .= "<tr align='left'><th>No</th><th>Campaign Name</th><th>Campaign Code</th><th>Row uploaded</th></tr>";
		if ($res) foreach ($res->result_assoc() as $value) {
			$project = $value['ProjectName'];

			$str .=  "<tr>";
			$str .=  "<td>$No</td><td>$value[CampaignName]</td><td>$value[Campaign_Master_Code]</td><td>$value[row_upload] rows</td>";
			$str .=  "</tr>";
			$No++;
			$total_row_uploaded +=$value['row_upload'];
		}
			$str .=  "<tr>";
			$str .=  "<th colspan='3' align='center'>Total Uploaded</th><th>$total_row_uploaded rows</th>";
			$str .=  "</tr>";
		
		$str .=  "</table>";
		$str .= "<p>";
		$str .= "<p>";
		$str .= "Enigma System";
		$str = $project . $str;

		return $str;
	}

// getRawInfo 
	
public function getRawInfo($uploadId='')
	{
		$this->db->select(" a.BuketUploadId, a.Campaign_Master_Code, c.CampaignName, count(a.BucketId) row_upload, b.* ");
		$this->db->from("t_gn_bucket_customers a","");
		$this->db->join("t_gn_upload_report_ftp b","b.FTP_UploadId = a.BukcetSourceId","left");
		$this->db->join("t_gn_campaign c","c.CampaignCode=a.Campaign_Master_Code","left");
		$this->db->where("b.FTP_UploadId", $uploadId);
		$this->db->group_by("Campaign_Master_Code");

		// echo $this->db->_get_var_dump();

		$res = $this->db->get();

		return $res;
	}

// doSendUploadReport 
	
public function doSendUploadReport($UploadId='',$SourceId='')	// public generate and mail upload report
{
	$contents = $this->getContent2($UploadId,$SourceId);
	$OutboxId = $this->writeOutbox($contents, $UploadId);
	if( $OutboxId !=FALSE )
	{
		$this->writeDestination($OutboxId, $UploadId);
		$this->writeQueue($OutboxId);
	}	
}

// writeOutbox 

 public function writeOutbox( $Content = NULL , $UploadId = NULL )
 {
	$OutboxId = FALSE;
	$WorkCode = $this->_get_work_project_code($UploadId);
	$Configuration = $this->M_Configuration->_getMail();
	
	if( !is_null($WorkCode))
	{
		$Setup = $this->_get_config_mail($WorkCode);
		if( is_array($Setup) )
		{
			$this->db->set('EmailSender',$Configuration['smtp.auth']);
			$this->db->set('EmailContent', $Content);
			$this->db->set('EmailSubject',$Setup['title']);
			$this->db->set('EmailStatus','1001');
			$this->db->set('EmailCreateTs',date("Y-m-d h:i:s"));
			$this->db->set('EmailAssignDataId',$UploadId);	
			$this->db->set('EmailCreateById',0);
			
			$this->db->insert('email_outbox');
			if( $this->db->affected_rows() > 0 ) 
			{	
				$OutboxId = $this->db->insert_id();
			}
		}	
	 }
		
	return $OutboxId;
}

// public write writeDestination 

public function writeDestination( $OutboxId=FALSE, $UploadId = NULL )
{ 
	$WorkCode = $this->_get_work_project_code( $UploadId );
	
	if(($OutboxId!=FALSE) AND (!is_null($WorkCode)) )
	{
		$config = $this->_get_config_mail($WorkCode);
		
		/***  cek mail destination prefix to ***/
		
		if( isset($config['to']) AND is_array($config['to']) )
		{
			foreach($config['to'] as $n => $to_adresss )
			{
				if( $to_adresss !='' )
				{
					$this->db->set('EmailDestination',trim($to_adresss) );
					$this->db->set('EmailReffrenceId', $OutboxId);
					$this->db->set('EmailDirection',2);
					$this->db->set('EmailCreateTs',date("Y-m-d h:i:s"));
					$this->db->insert('email_destination');
				}	
			}	
		}	
		
		/***  cek mail destination prefix cc ***/
		
		if( isset($config['cc']) AND is_array($config['cc']) )
		{
			foreach($config['cc'] as $n => $cc_adresss )
			{
				if( $cc_adresss!='' )
				{
					$this->db->set('EmailReffrenceId', $OutboxId);
					$this->db->set('EmailCC',trim($cc_adresss) );
					$this->db->set('EmailDirection',2);
					$this->db->set('EmailCreateTs',date("Y-m-d h:i:s"));
					$this->db->insert('email_copy_carbone');
				}
			}	
		}	
		
		/***  cek mail destination prefix bcc ***/
		
		if( isset($config['bcc']) AND is_array($config['bcc']) )
		{
			foreach($config['bcc'] as $n => $bcc_adresss )
			{
				if( $bcc_adresss!='' )
				{
					$this->db->set('EmailReffrenceId', $OutboxId);
					$this->db->set('EmailBCC',trim($bcc_adresss) );
					$this->db->set('EmailDirection',2);
					$this->db->set('EmailCreateTs',date("Y-m-d h:i:s"));
					$this->db->insert('email_blindcopy_carbone');
				}	
			}	
		}	
	 }
 }

// public writeQueue 
 
public function writeQueue( $EmailReffrenceId= FALSE )
{
	if($EmailReffrenceId)
	{
		$currentDate = date("Y-m-d h:i:s");
		$futureDate = $currentDate+(60*5);
		$formatDate = date("Y-m-d h:i:s", $futureDate);
		
		$this->db->set('QueueMailId',$EmailReffrenceId);
		$this->db->set('QueueStatusTs', date("Y-m-d h:i:s"));
		$this->db->set('QueueCreateTs', date("Y-m-d h:i:s"));
		$this->db->set('QueueStatus','1001');
		$this->db->insert('email_queue');
	}	
}


// END OF CLASSS 

}
?>