<?php
/*
 * @ def 	: User Javascript 
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
?>

<script type="text/javascript">
 Ext.DOM.USER_LEVEL_ADMIN = 1; 
 Ext.DOM.USER_LEVEL_MANAGER = 2;
 Ext.DOM.USER_LEVEL_SPV = 3;
 Ext.DOM.USER_LEVEL_AGENT = 4;
 Ext.DOM.USER_LEVEL_QUALITY = 5;
 Ext.DOM.USER_SYSTEM_LEVEL = 9;
 
/* render of cti documemts
 * author < omens >
 */
 
 Ext.Session().getStore();
 
 /* render of cti documemts
 * author < omens >
 */
 Ext.DOM.INDEX = Ext.System.view_page_index();
/* render of cti documemts
 * author < omens >
 */
 Ext.DOM.URL = Ext.System.view_app_url(); 
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.LIBRARY = Ext.System.view_library_url();
/* render of cti documemts
 * author < omens >
 */ 
Ext.DOM.SYSTEM = Ext.System.view_sytem_url();
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.V_STATUS_STORE = ( function(){
	return ( 
		Ext.Ajax({
			url		: Ext.DOM.INDEX +'/Callreason/store',
			method 	: 'POST',
			param 	: {
				action :'aux_reason'
			}
		}).json()
	)
})();

/* render of cti documemts
 * author < omens >
 */
 CTI.init( Ext.Session('HandlingType').getSession(), Ext.Session('HandlingType').getSession() )
/* render of cti documemts
 * author < omens >
 */
 Ext.DOM.onload = function(){
	CTI.prepareCTIClient();
	CTI.disableAllButton();
 };
/* 
 * render of cti documemts
 * author < omens >
 */
 Ext.DOM.onUnload =  function(){
	 CTI.prepareDisconnect(); //document.ctiapplet.ctiDisconnect();
 };
/* 
 * render of cti documemts
 * author < omens >
 */
Ext.ActiveMenu().setup([
	{id:'cust_closing', name:'src_customer_closing_nav.php'},
	{id:'app_menu', name:'src_appoinment_nav.php',},
	{id:'src_menu',name:'src_customer_nav.php'}
]);
/* 
 * reander on document jquery
 * register global < @ Window >
 * author < omens >
 */
$( function() { 

 var createLayout = function() {					   
	var 
		h  = $(window).height(),
		m  = $(window).height(),
		lw = $('#accordion').width(),
		w  = $(window).width(),
		ch = $(window).height();
		h  = h-90;
		m  = m-90;	
		
		$('body').css({overflow : 'hidden'});	
		
		aw = w-(lw+20);
		ch = ch-(h-70);
		$('#main_content').css({height : m, "overflow-y":'auto',"background" : "url("+ Ext.DOM.LIBRARY+"/gambar/gradient_orange.png) repeat-x 0 bottom", "overflow-x" : "hidden", "padding" : "4px",'width':aw});
		$('#left_menu').css({height : m,overflow : 'hidden',width:210});	
		$('.chat').css({height:ch});
	}
		
createLayout();   				   
$(window).resize(function(){ createLayout(); });

/*  
 * Ext toolbars Bottom nvigation jQuery
 * register global Window 
 * on plugin < jquery >
 */
 
$('#toolbars').extToolbars
  ({
	 extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
	 extTitle  : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Title(),
	 extMenu   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Menu(),
	 extIcon   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Icon(),
	 extText   : true,
	 extInput  : true,
	 extOption : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Option()
 });
});
</script>
</head>