<?php $this -> load -> layout( base_layout() .'/UserHeader'); ?>
<body class="easyui-layout" style="margin:0px;">

<?php $this -> load -> layout( base_layout() .'/UserPassword'); ?>

	<div id="headers"  data-options="region:'north',border:false" style="background-color:#dbecfb;height:35px;">&nbsp;
		<ul class="header-user-login">
				<li class="middle"><?php echo _get_session('Fullname');?></li>
				<li class="middle"><?php echo _get_session('GroupName')?></li>
				<li class="lasted"><?php echo date('d-m-Y H:i:s',strtotime(_get_session('LastLogin')));?></li>
			</ul>
	</div>
	<div data-options="region:'west',split:true,title:'Menu',collapsible:true" style="width:200px;padding:2px;">
		<div id="manu-panel" style="margin:-0px;padding:2px;border:0px solid 0px;">
			<?php $this->load->layout(base_layout()."/UserMenu");?>
		</div>
	</div>
	<div data-options="region:'south',border:false" id="footerPanel" style="height:35px;background:#dbecfb;padding:10px;">
		<?php $this -> load -> layout(base_layout() .'/UserToolbars'); ?>
	</div>
	<div data-options="region:'center'">
		<div class="easyui-tabs" data-options="tools:'#tab-tools'" style="padding:1px;">
			<div id="main_content" data-options="closable:false" title="Main Content" style="padding:10px"></div>
		</div>
	</div>
	<?php $this -> load -> layout(base_layout() .'/UserDialog'); ?>
	<div>
		<?php $this -> load -> layout(base_layout() .'/UserCti'); ?>
	</div>
</body>
</html>