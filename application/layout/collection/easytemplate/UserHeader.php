<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title><?php echo $website['_web_title'];?></title>
<!--<title><?php //echo $website['_web_title'];?> :: <?php //echo ucfirst(base_layout());?> Layout</title>-->
<link type="text/css" rel="stylesheet" href="<?php echo base_themes_style($website['_web_themes']);?>/ui.all.css?time=<?php echo time();?>" />
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.cores.css?time=<?php echo time();?>" />

<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_jquery();?>/plugins/extToolbars.js?time=<?php echo time();?>"></script>	
<script type="text/javascript" src="<?php echo base_jquery();?>/plugins/migrate.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/ui.datepicker.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_jquery();?>/plugins/jquery.autocomplete.js?time=<?php echo time();?>"></script>  

<!-- ENIGMA USER INTERFACE E.U.I -->

<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.1.3.js?time=<?php echo time();?>"></script> 
<script type="text/javascript" src="<?php echo base_enigma();?>/views/EUI_Main.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/helper/EUI_jQueryMsg.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/helper/EUI_Purr.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/helper/EUI_jQuery.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/helper/EUI_Chat.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/helper/EUI_ActiveMenu.js?time=<?php echo time();?>"></script>
<script type="text/javascript" src="<?php echo base_enigma();?>/helper/EUI_CTIScript.js?time=<?php echo time();?>"></script> 
<script type="text/javascript" src="<?php echo base_enigma();?>/views/EUI_Contact.js?time=<?php echo time();?>"></script> 
<script type="text/javascript" src="<?php echo base_js_layout();?>/jquery.easyui.min.js?time=<?php echo time();?>"></script>  
<?php $this -> load -> layout( base_layout() .'/UserJavascript');?> 
