<?php
/**
 * @ def : handle all menu to convert tree view model JS 
 * ----------------------------------------------------------------------
 *
 * @ param : - 
 * @ param : -
 *
 */
 
$arrs_menu = base_menu_model(); $ls_keys = null;
$pID = 1;
if(is_array($arrs_menu))foreach( $arrs_menu as $keys => $rows ) {
	$ls_keys .= "{ id :$pID, pId : 0, name : '$keys', open : false },\n";
	if(is_array($rows)) foreach( $rows as $k => $rs ) {
		$ls_keys .= "{ id : $pID$k, pId : $pID, name : '$rs[menu]', file : '$rs[file_name]' },\n";
	}
	$pID++;
}

$ls_keys = substr(trim($ls_keys), 0,(strlen(trim($ls_keys))-1));

?>
<script>

/**
 * @ def : handle all menu to convert tree view model JS 
 * ----------------------------------------------------------------------
 *
 * @ param : - 
 * @ param : -
 *
 */
 
 
 var zTree, bodyH, zNodes, demoIframe, setting;
 
/**
 * @ def : handle all menu to convert tree view model JS 
 * ----------------------------------------------------------------------
 *
 * @ param : - 
 * @ param : -
 *
 */ 
 setting =  {
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false
	},
	data: {
		simpleData: {
			enable:true,
			idKey: "id",
			pIdKey: "pId"
		}
	},
	callback: {
		beforeClick: function(treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("tree");
			if (treeNode.isParent) {
				zTree.expandNode(treeNode);
				return false;
			} else {
				demoIframe.attr("src",treeNode.file + ".html");
				return true;
			}
		},
		onClick : function(e,treeId, treeNode ){
				Ext.ShowMenu(treeNode.file,treeNode.name);
			}	
		}
	};

/**
 * @ def : store of menu to show 
 * ----------------------------------------------------------------------
 *
 * @ param : - 
 * @ param : -
 *
 */
 
 
zNodes = [<?php __($ls_keys); ?>];	

	
/**
 * @ def : on ready then load this tree 
 * ----------------------------------------------------------------------
 *
 * @ param : - 
 * @ param : -
 *
 */
 
$(document).ready(function() {	
var t = $("#tree");
	t = $.fn.zTree.init(t, setting, zNodes);
	demoIframe = $("#testIframe");
	demoIframe.bind("load", function(){
		bodyH = demoIframe.contents().find("body").get(0).scrollHeight,
		htmlH = demoIframe.contents().find("html").get(0).scrollHeight,
		maxH = Math.max(bodyH, htmlH), minH = Math.min(bodyH, htmlH),
		h = demoIframe.height() >= maxH ? minH:maxH ;
		if (h < 530) h = 530;
		demoIframe.height(h);
	});
	
	var zTree = $.fn.zTree.getZTreeObj("tree");
		zTree.selectNode(zTree.getNodeByParam("id", 101));

});



</script>

