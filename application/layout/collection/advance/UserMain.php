<?php $this -> load -> layout( base_layout() .'/UserHeader'); ?>
<body class='body-jquery-layout'>

<?php $this -> load -> layout( base_layout() .'/UserPassword'); ?>

<!-- start :: top --> 
<div class="ui-layout-north"> 
	<div id="logo"><?php echo $website['_web_title'];?>&nbsp;<?php echo $website['_web_verion'];?></div>
	<div id="user-login"> 
		<ul class="header-user-login">
				<li class="middle"><?php __(_get_session('Fullname'));?></li>
				<li class="middle"><?php __(ucfirst(STRTOLOWER(_get_session('GroupName'))))?></li>
				<li class="lasted"><?php __(date('d-m-Y H:i:s',strtotime(_get_session('LastLogin'))));?></li>
		</ul>
	</div>
	
	<div id="navigation"></div>
</div>
<!-- end :: top --> 

<!-- start :: this main content:: load page here -->
<div class="ui-layout-south" style='height:300px;'> 
	<?php $this->load->layout(base_layout().'/UserToolbars'); ?>
</div>
<!-- end :: this main content:: load page here -->

<!-- start :: this main content:: load page here -->
<div class="ui-layout-center content" id="main_content"> </div>
<!-- end :: this main content:: load page here -->

<!-- start :: ui-layout-west :: menu -->
<div class="ui-layout-west" style="display: none;">
	<div class="header">Main Menu</div>
	<div class="ui-layout-content" style="overflow: auto;border:0px solid #000;
		margin-left:-25px;margin-top:0px;">
		<ul id="tree" class="ztree" style="width:200px; overflow:auto;"></ul>
	</div>
</div>
<!-- end :: ui-layout-west :: menu -->

<?php $this -> load -> layout( base_layout() .'/UserLogout'); ?> 
<?php $this->load->layout(base_layout().'/UserCti'); ?>
</body>
</html>

