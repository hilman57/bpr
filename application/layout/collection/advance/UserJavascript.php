<?php
/*
 * @ def 	: User Javascript 
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
?>

<script type="text/javascript">

 Ext.DOM.ExtApplet 				 	= null;
 Ext.DOM.USER_LEVEL_ADMIN 		 	= 1; 
 Ext.DOM.USER_LEVEL_MANAGER 	 	= 2;
 Ext.DOM.USER_LEVEL_ACT_MANAGER  	= 9;
 Ext.DOM.USER_LEVEL_SPV 		 	= 3;
 Ext.DOM.USER_LEVEL_AGENT 		 	= 4;
 Ext.DOM.USER_LEVEL_INBOUND 	 	= 6;
 Ext.DOM.USER_LEVEL_QUALITY_HEAD 	= 5;
 Ext.DOM.USER_LEVEL_QUALITY_STAFF  	= 11;
 Ext.DOM.USER_SYSTEM_LEVEL 			= 8;
 Ext.DOM.USER_LEVEL_LEADER			= 13;
/* render of cti documemts
 * author < omens >
 */
 
 Ext.Session().getStore();
 
 /* render of cti documemts
 * author < omens >
 */
 Ext.DOM.INDEX = Ext.System.view_page_index();
/* render of cti documemts
 * author < omens >
 */
 Ext.DOM.URL = Ext.System.view_app_url(); 
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.LIBRARY = Ext.System.view_library_url();
/* render of cti documemts
 * author < omens >
 */ 
Ext.DOM.SYSTEM = Ext.System.view_sytem_url();
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.V_STATUS_STORE = ( function(){
	return ( 
		Ext.Ajax({
			url		: Ext.DOM.INDEX +'/Callreason/store',
			method 	: 'POST',
			param 	: {
				action :'aux_reason'
			}
		}).json()
	)
})();

Ext.DOM.V_PRODUCTSSCRIPT = ( function(){
	// alert('test4')
	// return false\
	// console.log('ini ok');
	return ( 
		Ext.Ajax({
			url		: Ext.DOM.INDEX +'/SetProductScript/store',
			method 	: 'POST',
			param 	: {
				action :'aux_reason'
			}
		}).json()
	)
})();
/* render of cti documemts
 * author < omens >
 */
 CTI.init( Ext.Session('HandlingType').getSession(), Ext.Session('HandlingType').getSession() )
/* render of cti documemts
 * author <omens>
 */
 Ext.DOM.onload = function(){
	CTI.prepareCTIClient();
	CTI.disableAllButton();
 };
/* 
 * render of cti documemts
 * author <omens>
 */
 
 Ext.DOM.onUnload =  function(){
	 CTI.prepareDisconnect(); //document.ctiapplet.ctiDisconnect();
 };
 
/* 
 * render of cti documemts
 * author < omens >
 */
 
Ext.ActiveMenu().setup(new Ext.Ajax({ 
	url : Ext.DOM.INDEX +'/ModActiveMenu/getActiveMenu', 
	param:{ action :'aux_reason' } }).json()
);
/* load all function library tree view  **/

var pageLayout;
	
/* load all function library get layout page **/

$(document).ready(function() {
 ExtApplet = new Ext.ViewPort(document.ctiapplet);
 ExtApplet.setApplet();
 
 
 pageLayout = $('body').layout ({
	scrollToBookmarkOnLoad : false,	
	defaults : { },	
	north : { 
		size :"auto" ,	
		spacing_open:0 ,	
		closable:false,	
		resizable:false 
	},
	
	west :  {
		size : 220,	spacing_closed : 22,	
		togglerLength_closed :	140,	
		togglerAlign_closed : "top" ,	
		togglerContent_closed : "C<BR>o<BR>n<BR>t<BR>e<BR>n<BR>t<BR>s",	
		togglerTip_closed : "Open & Pin Contents",	
		sliderTip : "Slide Open Contents",	
		slideTrigger_open : "mouseover" 
	 },
	 south: {
		spacing_open:2,	
		closable:false,	
		
	 }
 });

scrollToBookmark( self.location.hash );

// create layout 

var createLayout = function()
{		   
 var h  = $(window).height(), m  = $(window).height(),
	 lw = $('.ui-layout-west').width(),
	 w  = $(window).width(), ch = $(window).height();
	 h  = h-90;
	 m  = m-90;	
	
 $('body').css({overflow : 'hidden'});	
	
 aw = w-(lw+15);
 ch = ch-(h-70);
 
 $('.ui-layout-west').css({
	"overflow-x" : "hidden"
 });
 $('#main_content').css
 ({
	'margin-bottom' : '2px',
	'margin-right'	: '2px',
	'height'		: function(){ // auto width on load page 
		var __constans = 57, __window_defults = $(window).height(),
			__layoutHeight = ($('.ui-layout-north').height() + $('.ui-layout-south').height());
		return 	((__window_defults) - (__layoutHeight + __constans));
	},
	
	'overflow' 		: 'auto',
	'margin-top' 	: '0px',
	'margin-left' 	: '-2px',
	'padding-left' 	: '3px',
	'padding-right' : '-50',
	'width'			: aw
 });
}
		
 createLayout();   				   
 $(window).resize(function(){ 
	createLayout(); 
 });
 
 $('#toolbars-foot').extToolbars({
		 extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
		 extTitle  : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Title(),
		 extMenu   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Menu(),
		 extIcon   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Icon(),
		 extText   : true,
		 extInput  : true,
		 extOption : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Option()
	 });
	 
Ext.Cmp("auxReason").setAttribute('class',"input_text");
Ext.Cmp("auxReason").setAttribute('style',"border:1px solid #7ea3c6;");	
 
 
});

</script>
</head>