<?php
/*
 * @ def 	: User Javascript 
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
?>

<script type="text/javascript">
 Ext.DOM.USER_LEVEL_ADMIN 	= 1; 
 Ext.DOM.USER_LEVEL_MANAGER = 2;
 Ext.DOM.USER_LEVEL_SPV 	= 3;
 Ext.DOM.USER_LEVEL_AGENT 	= 4;
 Ext.DOM.USER_LEVEL_INBOUND = 6;
 Ext.DOM.USER_LEVEL_QUALITY = 5;
 Ext.DOM.USER_SYSTEM_LEVEL 	= 8;
 
/* render of cti documemts
 * author < omens >
 */
 
 Ext.Session().getStore();
 
 /* render of cti documemts
 * author < omens >
 */
 Ext.DOM.INDEX = Ext.System.view_page_index();
/* render of cti documemts
 * author < omens >
 */
 Ext.DOM.URL = Ext.System.view_app_url(); 
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.LIBRARY = Ext.System.view_library_url();
/* render of cti documemts
 * author < omens >
 */ 
Ext.DOM.SYSTEM = Ext.System.view_sytem_url();
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.V_STATUS_STORE = ( function(){
	return ( Ext.Ajax({ url : Ext.DOM.INDEX +'/Callreason/store'}).json());
})();

/* render of cti documemts
 * author < omens >
 */
 CTI.init( Ext.Session('HandlingType').getSession(), Ext.Session('HandlingType').getSession() )
/* render of cti documemts
 * author <omens>
 */
 Ext.DOM.onload = function(){
	CTI.prepareCTIClient();
	CTI.disableAllButton();
 };
/* 
 * render of cti documemts
 * author <omens>
 */
 
 Ext.DOM.onUnload =  function(){
	 CTI.prepareDisconnect(); //document.ctiapplet.ctiDisconnect();
 };
 
/* 
 * render of cti documemts
 * author < omens >
 */
 
Ext.ActiveMenu().setup([
	{id:'cust_closing', name:'src_customer_closing_nav.php'},
	{id:'app_menu', name:'src_appoinment_nav.php',},
	{id:'src_menu',name:'src_customer_nav.php'}
]);

/* 
 * reander on document jquery
 * register global < @ Window >
 * author < omens >
 */

$(function(){ 
// handle active menu 
 $("#nav-menu li a.menu-li").click(function (e) {
	e.preventDefault();
	$('#nav-menu li a.menu-li').addClass("hover-active").not(this).removeClass("hover-active");	
});
 $(" ul.submenu li.child").click(function (e) {
	e.preventDefault();
	$(' ul.submenu li.child').addClass("hover-activew").not(this).removeClass('hover-activew');	
});	
// create layout 
var createLayout = function() {					   
	var 
		h  = $(window).height(),
		m  = $(window).height(),
		lw = $('#accordion').width(),
		w  = $(window).width(),
		ch = $(window).height();
		h  = h-90;
		m  = m-90;	
		
		$('body').css({overflow : 'hidden'});	
		
		aw = w-(lw+20);
		ch = ch-(h-70);
		
		$('.content').css({'background' : "url("+Ext.DOM.LIBRARY+"/gambar/next.gif) no-repeat 0 0"});
		$('#main_content').css({
			'height' : ($(window).height()-145),
			"overflow-y":'auto',
			"background" : "url("+ Ext.DOM.LIBRARY+"/gambar/hilight.png) repeat-x 0 bottom", 
			"overflow-x" : "hidden", 
			"margin-bottom" :"2px",
			"margin-top" :"-2px",
			'padding-left' : '12px',
			'padding-right' : '-10px',
			'width':aw
		});
		
		//$('.chat').css({height:ch});
	}
		
createLayout();   				   
$(window).resize(function(){ createLayout(); });

/*  
 * Ext toolbars Bottom nvigation jQuery
 * register global Window 
 * on plugin < jquery >
 */
 
$('#toolbars').extToolbars
  ({
	 extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
	 extTitle  : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Title(),
	 extMenu   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Menu(),
	 extIcon   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Icon(),
	 extText   : true,
	 extInput  : true,
	 extOption : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Option()
 });
});
</script>
</head>