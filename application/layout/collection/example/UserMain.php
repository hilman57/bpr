<?php $this -> load -> layout( base_layout() .'/UserHeader'); ?>
<body>
<?php $this -> load -> layout( base_layout() .'/UserPassword'); ?>
<div id="main" style="border:0px solid #000;">
	<?php $this -> load -> layout( base_layout() .'/UserBanner'); ?>
	<?php $this -> load -> layout( base_layout() .'/UserMenu'); ?>
</div> 
<div id="main_content"></div>
<div id="logout" style="display:none;" title="Logout from this session?">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
	</span>You will logged-out from this session.<br />Are you sure?</p>
</div>
<?php $this -> load -> layout( base_layout() .'/UserFooter'); ?> 
