<?php
/*
 * @ def 	: User Footer
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
?>

<div id="foot" style="border:1px solid #dddddd;height:35px;overflow:hidden;">
<span>  
	<div id="toolbars" style="padding-right:10px;font-size:12px;border:0px solid #ddd;height:25px;text-align:right;margin-top:-8px;margin-left:1px;margin-right:-0px;"></div>
<?php $this -> load -> layout(base_layout() .'/UserCti'); ?>
</span>
</div>
<?php $this -> load -> layout(base_layout() .'/UserDialog'); ?>
</body>
</html>