<?php $this -> load -> layout( base_layout() .'/UserHeader'); ?>
<body>
<?php $this -> load -> layout( base_layout() .'/UserPassword'); ?>
<div id="container-inside" class="inner">
	 <!-- Header -->
	<div id="header">	
		<div id="web-title-header" class="web-title-header">E.U.I Call Center</div>
		<!-- Header Controls -->
			<div class="header-controls">
				<ul>
					<li class="first"><?php echo $this -> EUI_Session -> _get_session('Fullname');?></li>
					<li><?php echo $this -> EUI_Session -> _get_session('OnlineName');?>&nbsp;</li>
					<li><a href="javascript:void(0);" title="Logout from system" onclick="Ext.ShowMenu('Logout','Logout');">Logout</a></li>
				</ul>
			</div>
			<!-- /Header Controls -->
			<div class="clear-both"></div>
	</div>
 <!-- /Header -->
<!-- Primary Navigation -->		
<?php $this -> load -> layout( base_layout() .'/UserMenu'); ?> 
<!-- /Primary Navigation -->
<!-- start : MAIN CONTENT -->
<div id="main_content" align="center"> </div>	
<!-- endof : Main Content -->
</div>	

<?php $this -> load -> layout( base_layout() .'/UserLogout'); ?> 
<?php $this -> load -> layout( base_layout() .'/UserFooter'); ?> 

