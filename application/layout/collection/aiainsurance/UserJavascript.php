<?php
/*
 * @ def 	: User Javascript 
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
?>

<script type="text/javascript">

 Ext.DOM.ExtApplet 				 	= null;
 Ext.DOM.USER_LEVEL_ADMIN 		 	= 1; 
 Ext.DOM.USER_LEVEL_MANAGER 	 	= 2;
 Ext.DOM.USER_LEVEL_ACT_MANAGER  	= 9;
 Ext.DOM.USER_LEVEL_SPV 		 	= 3;
 Ext.DOM.USER_LEVEL_AGENT 		 	= 4;
 Ext.DOM.USER_LEVEL_INBOUND 	 	= 6;
 Ext.DOM.USER_LEVEL_QUALITY_HEAD 	= 5;
 Ext.DOM.USER_LEVEL_QUALITY_STAFF  	= 11;
 Ext.DOM.USER_SYSTEM_LEVEL 			= 8;
 Ext.DOM.USER_LEVEL_LEADER			= 13;
 
/* render of cti documemts
 * author < omens >
 */
 
 Ext.Session().getStore();
 
 /* render of cti documemts
 * author < omens >
 */
 Ext.DOM.INDEX = Ext.System.view_page_index();
/* render of cti documemts
 * author < omens >
 */
 Ext.DOM.URL = Ext.System.view_app_url(); 
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.LIBRARY = Ext.System.view_library_url();
/* render of cti documemts
 * author < omens >
 */ 
Ext.DOM.SYSTEM = Ext.System.view_sytem_url();
/* render of cti documemts
 * author < omens >
 */
Ext.DOM.V_STATUS_STORE = ( function(){
	return ( 
		Ext.Ajax({
			url		: Ext.DOM.INDEX +'/Callreason/store',
			method 	: 'POST',
			param 	: {
				action :'aux_reason'
			}
		}).json()
	)
})();

Ext.DOM.V_PRODUCTSSCRIPT = ( function(){
	// alert('test4')
	// return false\
	// console.log('ini ok');
	return ( 
		Ext.Ajax({
			url		: Ext.DOM.INDEX +'/SetProductScript/store',
			method 	: 'POST',
			param 	: {
				action :'aux_reason'
			}
		}).json()
	)
})();
/* render of cti documemts
 * author < omens >
 */
 CTI.init( Ext.Session('HandlingType').getSession(), Ext.Session('HandlingType').getSession() )
/* render of cti documemts
 * author <omens>
 */
 Ext.DOM.onload = function(){
	CTI.prepareCTIClient();
	CTI.disableAllButton();
	CTI.setLabelReady();
 };
/* 
 * render of cti documemts
 * author <omens>
 */
 
 Ext.DOM.onUnload =  function(){
	 CTI.prepareDisconnect(); //document.ctiapplet.ctiDisconnect();
 };
 
/* 
 * render of cti documemts
 * author < omens >
 */

Ext.ActiveMenu().setup(new Ext.Ajax({ 
	url : Ext.DOM.INDEX +'/ModActiveMenu/getActiveMenu', 
	param:{ action :'aux_reason' } }).json()
);

/* 
 * reander on document jquery
 * register global < @ Window >
 * author < omens >
 */

$(function(){ 
ExtApplet = new Ext.ViewPort(document.ctiapplet);
ExtApplet.setApplet();
//Ext.Timer('time_counter').Active(1000);

 $("#nav-menu li a.menu-li").click(function (e) {
	e.preventDefault();
	$('#nav-menu li a.menu-li').addClass("hover-active").not(this).removeClass("hover-active");	
});
 $(" ul.submenu li.child").click(function (e) {
	e.preventDefault();
	$(' ul.submenu li.child').addClass("hover-activew").not(this).removeClass('hover-activew');	
});	
// create layout 
var createLayout = function() {					   
	var 
		h  = $(window).height(),
		m  = $(window).height(),
		lw = $('#accordion').width(),
		w  = $(window).width(),
		ch = $(window).height();
		h  = h-90;
		m  = m-90;	
		
		$('body').css({overflow : 'hidden'});	
		
		aw = w-(lw+20);
		ch = ch-(h-70);
		
		$('.content').css({'background' : "url("+Ext.DOM.LIBRARY+"/gambar/next.gif) no-repeat 0 0"});
		$('#main_content').css({
			'height' : ($(window).height()-110),
			"overflow-y":'auto',
			"background" : "url("+ Ext.DOM.LIBRARY+"/gambar/hilight.png) repeat-x 0 bottom", 
			"overflow-x" : "hidden", 
			"margin" : "-2px 2px 2px 2px",
			//"margin-bottom" :"2px",
			//"margin-top" :"-2px",
			'padding-left' : '1px',
			'padding-right' : '2px',
			'width':"99.7%"
		});
		
		//$('.chat').css({height:ch});
	}
		
createLayout();   				   
$(window).resize(function(){ createLayout(); });

/*  
 * Ext toolbars Bottom nvigation jQuery
 * register global Window 
 * on plugin < jquery >
 */
 // --> console.log(Ext.ActiveBars(Ext.Session('HandlingType').getSession()));
console.log(Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Title());
console.log(Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Menu());
console.log(Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Option());
$('#toolbars-footer').extToolbars
  ({
	 extUrl    : Ext.DOM.LIBRARY+'/gambar/icon',
	 extTitle  : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Title(),
	 extMenu   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Menu(),
	 extIcon   : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Icon(),
	 extText   : true,
	 extInput  : true,
	 extOption : Ext.ActiveBars(Ext.Session('HandlingType').getSession()).Option()
 });
});


</script>
</head>