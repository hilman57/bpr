<?php 
/*
 * @ def 	: User Main Content 
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
?> 
<?php $this -> load -> layout( base_layout() .'/UserHeader'); ?>
<body> 
<?php $this -> load -> layout( base_layout() .'/UserPassword'); ?>
<div id="wrapper">
	<div id="header">
	<!-- start//: right info -->
	<div class="top_info" style="border:0px solid #000;width:660px;">
		<div class="top_info_right">
			<p>
				Profile : <b><?php echo $this -> EUI_Session -> _get_session('GroupName');?></b><br />
				Last updated : <?php echo date("l"); ?>, <?php echo date("j F Y, H:i:s"); ?>		
			</p>
		</div>
		<div class="top_info_left" style="margin-top:-2px;padding:left:2px;border:0px solid blue;width:310px;font-size:11px;">
			<?php if( $this -> EUI_Session -> _get_session('HandlingType')!=4){?>
				<div class="msg_icon" style="cursor:pointer;margin-right:6px;"><img class="msg_n" src="<?php echo base_url(); ?>library/gambar/msg.png" /></div>&nbsp;	
			<?php }?>
				<b><?php echo $this -> EUI_Session -> _get_session('Fullname');?>,</b> you are logged in<br />
				<p style="margin-left:12px;"> &nbsp;on <?php echo $this -> EUI_Session -> _get_session('LastLogin'); ?></p>
		</div>
	</div>
	
	<!-- stop//: right info -->	
	<div class="top_logo" style="border:0px solid #000;width:600px;z-index:999;">
		<div class="top_info_left" style="border:0px solid #000;width:300px;">
			<h1 style="font-size:25px;margin-top:-4px;">
				<a href="#" title="<?php echo $website['_web_title']; ?>"> 
				<img src="<?php echo base_image_layout(); ?>/telin_timor_leste.png" height="30px">
				</a>
			</h1>                 
		</div>
	</div>
	
	</div>
	<!-- start//: content --> 
	<div id="container">	
		<?php  $this -> load -> layout( base_layout() .'/UserMenu'); ?>
		<div id="main_content"></div>	
	</div>
</div>
	
<div id="logout" style="display:none;" title="Logout from this session?">
  	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
    </span>You will logged-out from this session.<br />Are you sure?</p>
</div>
<?php $this -> load -> layout( base_layout() .'/UserFooter'); ?> 