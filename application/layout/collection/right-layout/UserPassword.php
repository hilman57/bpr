<?php

/*
 * @ def 	: User Password 
 * -----------------------------------
 * @ param 	: layout section
 * @ aksess : public
 * @ author	: razaki team
 */
 
?>
<div id="pass" title="Change Password" style="display:none;">
  	<fieldset class="change_password" style="border:0px;">
		<table border="0">
  		  <tr>
  		    <td><label for="curr_password">Current Password</label></td>
  		    <td><input type="password" name="curr_password" id="curr_password" class="text ui-widget-content ui-corner-all" /></td>
  		  </tr>
  		  <tr>
  		    <td><label for="new_password">New Password</label></td>
  		    <td><input type="password" name="new_password" id="new_password" value="" class="text ui-widget-content ui-corner-all" /></td>
  		  </tr>
  		  <tr>
  		    <td><label for="re_new_password">Re-type New Pass.</label></td>
  		    <td><input type="password" name="re_new_password" id="re_new_password" value="" class="text ui-widget-content ui-corner-all" /></td>
  		  </tr>
  		</table>
  	</fieldset>
 </div> 
 <div id="password_confirm" title="Change Password"></div>