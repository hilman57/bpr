<!-- E.U.I view / Layout -->
<!-- /////////////////// -->
<!-- package  : Layout User embed CTI -->
<!-- author   : razaki team deplovment -->
<!-- link 	: http://www.razakitechnology.com/layout?use=user  -->
 
<?php  if( $this -> EUI_Session -> _get_session('HandlingType') !='' ) {  ?>
	<form name="frmAgent" id="idFrmAgent" >
	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" >
		<tr>
			<td valign="top" nowrap class="small">
				<input type="button" name="btnReady" id="btnReady" onClick="document.ctiapplet.agentSetReady();return false;" value="Ready"/>
				<input type="button" name="btnAUX"   onClick="document.ctiapplet.agentSetNotReady(document.frmAgent.auxReason.value);return false;" value="Not Ready"/>
				<select name="auxReason" style="font-size:11px; width:75px; border:1px solid #ddd;height:22px;background-color:#eee;">
					<option value="0">Break</option>
					<option value="1">Solat</option>
				</select>
			</td>
			<td width=70 align="center" nowrap class="small">
				<b><div id="idAgentStatus" style="border:0px solid red;position:absolute;width:100;margin-top:-10px;margin-left:-30">&nbsp;Agent Status</div></b>
			</td>
			<td width=150 align="center" nowrap class="small">
			</td>
			<td valign="top">
				<button name="btnHold" id="btnHold" onClick="onButtonHoldClick();return false;" style="display:none;">Hold</button>			
				<input type="button" name="btnHangup"  id="btnHangup"  style="display:none;" onClick="document.ctiapplet.callHangup();return false;" value="Hangup"/>
			</td>
		</tr>
		<tr>
			<td colspan=4>
				<applet name="ctiapplet" code="centerBackAgent.class" archive="<?php echo base_url();?>library/EUI/asset/centerBackAgentApplet.jar" width="215" height="55"  MAYSCRIPT onLoad="document.ctiapplet.setAgentSkill(1);document.ctiapplet.ctiConnect();">
					<param name="CTIHost"  value="<?php echo $this -> EUI_Session -> _get_session('ctiIp'); ?>"/>
					<param name="CTIPort"  value="<?php echo $this -> EUI_Session -> _get_session('ctiUdpPort');?>"/>
					<param name="agentId"  value="<?php echo $this -> EUI_Session -> _get_session('agentId');?>"/>
					<param name="agentLogin" value="<?php echo $this -> EUI_Session -> _get_session('agentLogin');?>"/>
					<param name="agentName"  value="<?php echo $this -> EUI_Session -> _get_session('agentName');?>"/>        
					<param name="agentGroup" value="<?php echo $this -> EUI_Session -> _get_session('agentGroup');?>"/>
					<param name="agentLevel" value="<?php echo $this -> EUI_Session -> _get_session('agentLevel');?>"/>
					<param name="agentExt"   value="<?php echo $this -> EUI_Session -> _get_session('agentExt');?>"/>        
					<param name="agentPbxGroup" value="<?php echo $this -> EUI_Session -> _get_session('agentPbxGroup'); ?>"/>
					<param name="debugLevel" value="10"/>
					alt="Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason."
					Your browser is completely ignoring the &lt;APPLET&gt; tag!
				</applet>
			
				<input name="destNo" type="hidden" />
				<input name="passwd" type="hidden" />
				<input name="callAction" type="hidden" />
			</td>  		
		</tr>
	</table>
  </form>
<?php }; ?> 
<!-- END OF FILE  -->
<!-- location : // ../application/layout/UserCti.php -->