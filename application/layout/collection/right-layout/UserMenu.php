<div id="right_menu"><?php

/* @ def  : example get menu by hleper Layout 
 * ----------------------------------------
 *
 * @ param 	: menu array 
 * @ aksess : view on base layout  
 */

// get menu container 

echo base_menu_layout
(
	array
	(
		'data'   => $menu, 
		'container' => array('class' => null, 'id' => 'accordion', 'extra' => '1px solid #000000;'),
		'parent' => array('show' => true, 'ahref' => '<h3><a href="javascript:void(0)">{title}</a></h3>'),
		'child'  => array('show' => true, 'class' => array('ul' => null, 'li' => null)),
		'click'  => array('action' => 'Ext.ShowMenu'),
		'modul'  => array('chat', 'show' => true) 
	) 		
);  

// get chat container 

echo base_chat_layout
(
	array 
	(
		'data' => null,
		'container' => array('id' => 'accordions', 'class' => null, 'extra' => 'border:0px solid #000;width:200px;' ),
		'parent' => array('title' => '<h3><a href="javascript:void(0)"> Chat Friend List</a></h3>'),
		'ul' => array('id'=>null, 'class' => null, 'extra'=> 'height:100px')
	)
);

?> 
</div>
