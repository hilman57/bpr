<?php
$CategoryMenu =& base_menu_model(); 
?>
<body>
 <div id="ribbon">
	<span class="ribbon-window-title application-title">
		<b style="color:#4b4be9;"><?php __(strtoupper(_get_session('Username')));?></b>
		<span style="color:#ddd4d3;">|</span>
        <?php __(strtoupper(_get_session('Fullname')));?>		
	</span>
	
	<?php if(is_array($CategoryMenu)) 
		foreach( $CategoryMenu as $CategoryName => $CategoryRecs )  : ?>
		 <div class="ribbon-tab" id="<?php echo str_replace(" ","_", $CategoryName);?>-tab">
			<span class="ribbon-title"><?php echo ucwords(strtolower($CategoryName));?></span>
		<?php if(is_array($CategoryRecs) ) 
			foreach( $CategoryRecs as $ResultID => $ResultName ) :  ?>	
			<div class="ribbon-section"> 
				<div class="ribbon-button ribbon-button-large" 
					id="<?php echo $ResultName['id'];?>" 
					onclick="Ext.ShowMenu('<?php echo $ResultName['id']; ?>','<?php echo $ResultName['menu']; ?>');">
						<span class="button-title"><?php echo $ResultName['menu'];?></span>
					<?php if( !is_null($ResultName['images']) AND !empty($ResultName['images']) ) : ?>	
						<img class="ribbon-icon ribbon-normal" src="<?php echo base_image_layout();?>/icons/<?php echo $ResultName['images']?>_c.png" width="32" height="32"/>
						<img class="ribbon-icon ribbon-hot" src="<?php echo base_image_layout();?>/icons/<?php echo $ResultName['images']?>_o.png" width="32" height="32"/>
						<img class="ribbon-icon ribbon-disabled" src="<?php echo base_image_layout();?>/icons/<?php echo $ResultName['images']?>_d.png" width="32" height="32"/>
					<?php else : ?>
						<img class="ribbon-icon ribbon-normal" src="<?php echo base_image_layout();?>/icons/new-page_c.png" width="32" height="32"/>
						<img class="ribbon-icon ribbon-hot" src="<?php echo base_image_layout();?>/icons/new-page_o.png" width="32" height="32"/>
						<img class="ribbon-icon ribbon-disabled" src="<?php echo base_image_layout();?>/icons/new-page_d.png" width="32" height="32"/>	
					<?php endif; ?>	
				</div>	
			</div>
		 <?php endforeach; ?>
		</div>
	<?php endforeach; ?>	
	<?php $this->load->layout(base_layout().'/UserAboutUs');?>
</div>

