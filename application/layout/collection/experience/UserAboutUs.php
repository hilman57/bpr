<!-- add menu Help -> about as -->
	<div class="ribbon-tab" id="help-about">
		<span class="ribbon-title">Help</span>	
		<div class="ribbon-section osx"> 
			<div class="ribbon-button ribbon-button-large" id="about-id" >
				<span class="button-title osx">About Us</span>
				<img class="ribbon-icon ribbon-normal" src="<?php echo base_image_layout();?>/icons/open-table_c.png" />
				<img class="ribbon-icon ribbon-hot" src="<?php echo base_image_layout();?>/icons/open-table_o.png"  />
				<img class="ribbon-icon ribbon-disabled" src="<?php echo base_image_layout();?>/icons/open-table_d.png" />
			</div>		
		</div>	
	</div>