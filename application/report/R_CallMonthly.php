<?php

/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
 // note data in Progress beda cara ambilnya ? aneh :::
 
class R_CallMonthly extends EUI_Report 
{


 private $StartDate = NULL;
 private $EndDate = NULL;
 private $CampaignId = null;
 
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
 public function __construct() {

 }
 
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
public function _set_campaign_id( $campaignid = NULL )
 {
	if( is_null( $this->CampaignId ) )
	{
		$this->CampaignId = $campaignid;
	}	
 }
 
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */

public function _set_start_date( $start_date = NULL )
 {
	if( is_null($this->StartDate) )
	{
		$this->StartDate = date('Y-m-d', strtotime($start_date) );
	}	
 }
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */ 
 
public function _set_end_date( $end_date = null )
{
	if( is_null($this->EndDate) ) {
		$this->EndDate = date('Y-m-d', strtotime($end_date) );
	}	
 }
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */ 
 
public function index() 
{
	$UI = array( 'size_data' => $this->SummaryDataByInterval() );
	return $UI;
}

/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */ 
 
public function SummaryDataByInterval() 
{
	
  static $summary = array();
 $this->db->reset_select();  
  $this->db->select("
		DATE_FORMAT(a.PolicyIssDate,'%Y-%m') as IssuedDate,
		b.CampaignId  AS CampaignId, 
		COUNT(a.PolicyId) as tot_size_data,
		SUM(IF(a.CallAttempt> 0, 1, 0)) as tot_call_attempt,
		SUM(IF(a.PolicyUpdatedTs IS NULL, 1, 0)) as tot_untouched,
		SUM(IF(a.CallReasonId IN(1,2,3,4,8,9,12,13,14,15,16,17,18) AND a.CallComplete IN(1), 1, 0)) as tot_conducted_attempt_dialed,
		
		SUM(IF(( a.CallReasonId IN(1) AND a.CallComplete IN(1)), 1, 0)) as tot_call_surveys,
		SUM(IF(a.CallReasonId IN( 2,3,4) AND a.CallComplete IN(1), 1, 0)) as tot_call_not_interest_talk,
		SUM(IF(a.CallReasonId IN(9,10,11,12,13,14) AND a.CallComplete IN(1), 1, 0)) as tot_call_not_connected,
		SUM(IF(a.CallReasonId IN(8) AND a.CallComplete IN(1), 1, 0)) as tot_call_connected,
		
		SUM(IF(a.CallReasonId IN(9)  AND a.CallComplete IN(1), 1, 0)) as tot_call_busy,
		SUM(IF(a.CallReasonId IN(13) AND a.CallComplete IN(1), 1, 0)) as tot_call_deadtone,
		SUM(IF(a.CallReasonId IN(12) AND a.CallComplete IN(1), 1, 0)) as tot_call_leftmesage,
		SUM(IF(a.CallReasonId IN(15,16,17,18) AND a.CallComplete IN(1), 1, 0)) as tot_call_returnlist,
		SUM(IF(a.CallReasonId IN(14) AND a.CallComplete IN(1), 1, 0)) as tot_call_wrongnumber,
		
		SUM(IF(a.CallReasonId IN(9,6,7,5,12,13,14,8) AND a.CallComplete IN(0), 1, 0)) as tot_call_inprogress,
		SUM(IF(a.CallReasonId IN(9)  AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_busy,
		SUM(IF(a.CallReasonId IN(6,7) AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_back_later,
		SUM(IF(a.CallReasonId IN(5)  AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_not_bereach,
		SUM(IF(a.CallReasonId IN(13) AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_deadtone,
		SUM(IF(a.CallReasonId IN(12) AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_leftmesage,
		SUM(IF(a.CallReasonId IN(8)  AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_not_answer,
		SUM(IF(a.CallReasonId IN(14) AND a.CallComplete IN(0), 1, 0)) as tot_inpr_call_wrongnumber,
		
		#ABC
		SUM(IF(a.CallReasonId IN (1,2,3,4) AND a.CallComplete IN(1),1,0)) as tot_answer_clean_data,
		SUM(IF(a.CallReasonId IN (8) AND a.CallComplete IN(1),1,0)) as tot_connected_and_notanaswer,
		SUM(IF(a.CallReasonId IN (9,12,13,14,15,16,17,18) AND a.CallComplete IN(1),1,0)) as tot_connected_unclean_data,
		
		SUM(IF(a.CallReasonId IN(5) AND a.CallComplete IN(1), 1, 0)) as tot_non_contacted,
		SUM(IF(a.CallReasonId IN(1,2,3,4) AND a.CallComplete IN(1), 1, 0)) as tot_call_contacted ",
	FALSE);
	
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_campaign_project c "," b.CampaignId=c.CampaignId", "LEFT");
	$this->db->where_in("c.ProjectId", array(1));
// if have post campaignid // 
	
	if( $this->URI->_get_have_post('CampaignId')) {
		$this->db->where_in("c.CampaignId", $this->CampaignId);
	}
	
	if(!is_null($this->StartDate)){
		$this->db->where("DATE_FORMAT(a.PolicyIssDate,'%Y-%m') >='". date('Y-m', strtotime($this->StartDate)). "'", '', FALSE);
	}	

// end_date 
	
	if(!is_null($this->EndDate)){
		$this->db->where("DATE_FORMAT(a.PolicyIssDate,'%Y-%m')<='". date('Y-m', strtotime($this->EndDate)) ."'", '', FALSE);
	}
	
	$this->db->group_by("CampaignId");	
	$this->db->group_by("IssuedDate");	
	// echo $this -> db ->_get_var_dump();
// set summary data 
	
	$tots_all_size = 0;
	$tots_call_inprogress = 0;
	$tots_untouched = 0;
	$tots_conducted_attempt_dialed = 0;
	
	$tots_connected_unclean_data = 0; 
	$tots_answer_clean_data = 0;  
	$tots_call_contacted = 0; 
	$tots_call_surveys = 0; 
	$tots_call_not_interest_talk = 0; 
	$tots_non_contacted = 0; 
	$tots_connected_and_notanaswer = 0; 
	$tots_call_busy = 0; 
	$tots_call_deadtone = 0;  
	$tots_call_leftmesage = 0; 
	$tots_call_returnlist = 0; 
	$tots_call_wrongnumber = 0;
	$tots_call_back_later = 0;
	$tots_call_not_bereach = 0;
	$tots_call_not_answer = 0;
	
	
	foreach( $this->db->get() ->result_assoc() as $rows ) 
	{
		foreach( $rows as $field => $jumlah ){
			$summary[$rows['IssuedDate']][$rows['CampaignId']][$field] += $jumlah;
		}
		
	/** total all data p1+p2_p3 **/
	
		$tot_call_inprogress = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_inprogress'];
		$tot_untouched = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_untouched'];
		$tot_conducted_attempt_dialed = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_conducted_attempt_dialed'];
		
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_all_size'] += $tot_call_inprogress;
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_all_size'] += $tot_untouched;
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_all_size'] += $tot_conducted_attempt_dialed;
		
		$total_all_size = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_all_size'];
		
	/** total percent on contact rasio **/
	
		$tot_all_survey = ($summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_surveys']?$summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_surveys']:0); 
		$tot_all_clean 	= ($summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_answer_clean_data'] ? $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_answer_clean_data']:0); 
		$tot_all_size   = ($summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_all_size'] ? $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_all_size']:0);
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_contact_ratio'] = round((($tot_all_clean/$tot_all_size)*100),2);
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_success_survey'] = round((($tot_all_survey/$tot_all_size)*100),2);
		
	/** rate all position **/
		
		$tot_connected_unclean_data = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_connected_unclean_data'];
		$tot_answer_clean_data = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_answer_clean_data'];
		$tot_call_contacted = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_contacted'];
		$tot_call_surveys = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_surveys'];
		$tot_call_not_interest_talk = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_not_interest_talk'];
		$tot_non_contacted = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_non_contacted'];
		$tot_connected_and_notanaswer = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_connected_and_notanaswer'];
		// $tot_call_returnlist = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_returnlist'];
		
		/** not connected unclean **/
		$tot_call_busy		= $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_busy'];
		$tot_call_deadtone	= $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_deadtone'];
		$tot_call_leftmesage= $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_leftmesage'];
		$tot_call_returnlist= $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_returnlist'];
		$tot_call_wrongnumber= $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_wrongnumber'];
		
		// $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_call_inprogress'];
		/** inprogress **/
		$tot_inpr_call_busy		   = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_busy'];
		$tot_inpr_call_back_later  = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_back_later'];
		$tot_inpr_call_not_bereach = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_not_bereach'];
		$tot_inpr_call_deadtone	   = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_deadtone'];
		$tot_inpr_call_leftmesage  = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_leftmesage'];
		$tot_inpr_call_not_answer  = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_not_answer'];
		$tot_inpr_call_wrongnumber = $summary[$rows['IssuedDate']][$rows['CampaignId']]['tot_inpr_call_wrongnumber'];
		
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_inprogress'] = ( $tot_call_inprogress ? round((($tot_call_inprogress/$total_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_untouched'] = ( $tot_untouched ? round((($tot_untouched/$total_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_conducted_attempt_dialed'] = ( $tot_conducted_attempt_dialed ? round((($tot_conducted_attempt_dialed/$total_all_size)*100),2) : 0 );
		
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_answer_clean_data'] =( $tot_answer_clean_data ? round((($tot_answer_clean_data/$total_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_contacted'] =( $tot_call_contacted ? round((($tot_call_contacted/$tot_answer_clean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_surveys'] = ( $tot_call_surveys ? round((($tot_call_surveys/$tot_call_contacted)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_not_interest_talk'] = ( $tot_call_not_interest_talk ? round((($tot_call_not_interest_talk/$tot_call_contacted)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_non_contacted'] = ( $tot_non_contacted ? round((($tot_non_contacted/$tot_answer_clean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_connected_and_notanaswer'] = ( $tot_connected_and_notanaswer ? round((($tot_connected_and_notanaswer/$total_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_connected_unclean_data'] = ( $tot_connected_unclean_data ? round((($tot_connected_unclean_data/$total_all_size)*100),2) : 0 );
		
		/** not connected unclean **/
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_returnlist'] = ( $tot_call_returnlist ? round((($tot_call_returnlist/$tot_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_busy'] = ( $tot_call_busy ? round((($tot_call_busy/$tot_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_deadtone'] = ( $tot_call_deadtone ? round((($tot_call_deadtone/$tot_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_leftmesage'] = ( $tot_call_leftmesage ? round((($tot_call_leftmesage/$tot_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_call_wrongnumber'] = ( $tot_call_wrongnumber ? round((($tot_call_wrongnumber/$tot_connected_unclean_data)*100),2) : 0 );
		
		/** inprogress **/
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_busy']		=( $tot_inpr_call_busy ? round((($tot_inpr_call_busy/$tot_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_deadtone']	=( $tot_inpr_call_deadtone ? round((($tot_inpr_call_deadtone/$tot_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_leftmesage']	=( $tot_inpr_call_leftmesage ? round((($tot_inpr_call_leftmesage/$tot_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_wrongnumber']=( $tot_inpr_call_wrongnumber ? round((($tot_inpr_call_wrongnumber/$tot_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_back_later']	=( $tot_inpr_call_back_later ? round((($tot_inpr_call_back_later/$tot_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_not_bereach']=( $tot_inpr_call_not_bereach ? round((($tot_inpr_call_not_bereach/$tot_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_inpr_call_not_answer']	=( $tot_inpr_call_not_answer ? round((($tot_inpr_call_not_answer/$tot_call_inprogress)*100),2) : 0 );
	
	// test callcualtion 
	
		$tots_all_size+=$total_all_size;
		$tots_call_inprogress+= $tot_call_inprogress;
		$tots_untouched+= $tot_untouched;
		$tots_conducted_attempt_dialed+= $tot_conducted_attempt_dialed;
		
		$tots_connected_unclean_data+= $tot_connected_unclean_data;
		$tots_answer_clean_data+= $tot_answer_clean_data;
		$tots_call_contacted+= $tot_call_contacted;
		$tots_call_surveys+= $tot_call_surveys;
		$tots_call_not_interest_talk+= $tot_call_not_interest_talk;
		$tots_non_contacted+= $tot_non_contacted;
		$tots_connected_and_notanaswer+= $tot_connected_and_notanaswer;
		
		/** not connected unclean **/
		$tots_call_returnlist+= $tot_call_returnlist;
		$tots_call_busy+= $tot_call_busy;
		$tots_call_deadtone+= $tot_call_deadtone;
		$tots_call_leftmesage+= $tot_call_leftmesage;
		$tots_call_wrongnumber+= $tot_call_wrongnumber;
		
		/** inprogress **/
		$tots_inpr_call_busy+= $tot_inpr_call_busy;
		$tots_inpr_call_deadtone+= $tot_inpr_call_deadtone;
		$tots_inpr_call_leftmesage+= $tot_inpr_call_leftmesage;
		$tots_inpr_call_wrongnumber+= $tot_inpr_call_wrongnumber;
		$tots_inpr_call_back_later+= $tot_inpr_call_back_later;
		$tots_inpr_call_not_bereach+= $tot_inpr_call_not_bereach;
		$tots_inpr_call_not_answer+= $tot_inpr_call_not_answer;
		
		
		$summary[$rows['IssuedDate']]['rate_call_inprogress'] = ( $tots_call_inprogress ? round((($tots_call_inprogress/$tots_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_untouched'] = ( $tots_untouched ? round((($tots_untouched/$tots_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_conducted_attempt_dialed'] = ( $tots_conducted_attempt_dialed ? round((($tots_conducted_attempt_dialed/$tots_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_answer_clean_data'] =( $tots_answer_clean_data ? round((($tots_answer_clean_data/$tots_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_contacted'] =( $tots_call_contacted ? round((($tots_call_contacted/$tots_answer_clean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_surveys'] = ( $tots_call_surveys ? round((($tots_call_surveys/$tots_call_contacted)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_not_interest_talk'] = ( $tots_call_not_interest_talk ? round((($tots_call_not_interest_talk/$tots_call_contacted)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_non_contacted'] = ( $tots_non_contacted ? round((($tots_non_contacted/$tots_answer_clean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_connected_and_notanaswer'] = ( $tots_connected_and_notanaswer ? round((($tots_connected_and_notanaswer/$tots_all_size)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_connected_unclean_data'] = ( $tots_connected_unclean_data ? round((($tots_connected_unclean_data/$tots_all_size)*100),2) : 0 );
		
		/** not connected unclean **/
		$summary[$rows['IssuedDate']]['rate_call_busy']			=( $tots_call_busy ? round((($tots_call_busy/$tots_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_deadtone']		=( $tots_call_deadtone ? round((($tots_call_deadtone/$tots_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_leftmesage']	=( $tots_call_leftmesage ? round((($tots_call_leftmesage/$tots_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_wrongnumber']	=( $tots_call_wrongnumber ? round((($tots_call_wrongnumber/$tots_connected_unclean_data)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_call_returnlist']	=( $tots_call_returnlist ? round((($tots_call_returnlist/$tots_connected_unclean_data)*100),2) : 0 );
		
		/** inprogress **/
		$summary[$rows['IssuedDate']]['rate_inpr_call_busy']		=( $tots_inpr_call_busy ? round((($tots_inpr_call_busy/$tots_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_inpr_call_deadtone']	=( $tots_inpr_call_deadtone ? round((($tots_inpr_call_deadtone/$tots_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_inpr_call_leftmesage']	=( $tots_inpr_call_leftmesage ? round((($tots_inpr_call_leftmesage/$tots_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_inpr_call_wrongnumber']	=( $tots_inpr_call_wrongnumber ? round((($tots_inpr_call_wrongnumber/$tots_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_inpr_call_back_later']	=( $tots_inpr_call_back_later ? round((($tots_inpr_call_back_later/$tots_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_inpr_call_not_bereach']	=( $tots_inpr_call_not_bereach ? round((($tots_inpr_call_not_bereach/$tots_call_inprogress)*100),2) : 0 );
		$summary[$rows['IssuedDate']]['rate_inpr_call_not_answer']	=( $tots_inpr_call_not_answer ? round((($tots_inpr_call_not_answer/$tots_call_inprogress)*100),2) : 0 );
		
		$summary[$rows['IssuedDate']][$rows['CampaignId']]['summary_inprogress'] = $tots_call_inprogress + $tots_conducted_attempt_dialed;
		// $summary[$rows['IssuedDate']][$rows['CampaignId']]['rate_summary_inprogress'] =  round((((($tots_call_inprogress + $tots_conducted_attempt_dialed)/$tots_call_inprogress)*100),2);
	}
	
	// summary data on bottom 
		// echo $tot_connected_unclean_data."|";
		// echo $tots_call_back_later."?";
		// echo $summary[$rows['IssuedDate']]['rate_call_back_later'];
		
	return $summary;
}

 
 
 
 
 
}

?>