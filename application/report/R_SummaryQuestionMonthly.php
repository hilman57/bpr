<?php
/*
 * @ def : R_SummaryQuestionMonthly 
 *
 */
class R_SummaryQuestionMonthly extends EUI_Report 
{

/** start set parameter **/

 private $StartDate = NULL;
 private $EndDate = NULL;
 private $CampaignId = null;
 
 
/* 
 * @ def : function contruct 
 *
 */
public function __construct(){ }
 
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
public function _set_campaign_id( $campaignid = NULL )
 {
	if( is_null( $this->CampaignId ) )
	{
		$this->CampaignId = $campaignid;
	}	
 }
 
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */

public function _set_start_date( $start_date = NULL )
 {
	if( is_null($this->StartDate) )
	{
		$this->StartDate = date('Y-m-d', strtotime($start_date) );
	}	
 }
 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */ 
 
public function _set_end_date( $end_date = null )
{
	if( is_null($this->EndDate) ) {
		$this->EndDate = date('Y-m-d', strtotime($end_date) );
	}	
 } 
/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */ 
 
public function index() 
{
	$UI = array( 'size_data' => $this->SummaryDataByInterval() );
	return $UI;
} 

/*
 * @ def  : source control form R_CallMonthly Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */ 
 
public function SummaryDataByInterval()
{

$summary = array();

$this->db->select(" DATE_FORMAT(c.PolicyIssDate,'%Y-%m') as bln, 
	d.CampaignId as CampaignId, 
	SUM(IF(b.CustField_Q1=0, 1,0)) as tot_q1_0,
	SUM(IF(b.CustField_Q1=1, 1,0)) as tot_q1_1,
	SUM(IF(b.CustField_Q1=2, 1,0)) as tot_q1_2,
	SUM(IF(b.CustField_Q2=0, 1,0)) as tot_q2_0,
	SUM(IF(b.CustField_Q2=1, 1,0)) as tot_q2_1,
	SUM(IF(b.CustField_Q2=2, 1,0)) as tot_q2_2,
	SUM(IF(b.CustField_Q3=0, 1,0)) as tot_q3_0,
	SUM(IF(b.CustField_Q3=1, 1,0)) as tot_q3_1,
	SUM(IF(b.CustField_Q3=2, 1,0)) as tot_q3_2,
	SUM(IF(b.CustField_Q4=0, 1,0)) as tot_q4_0,
	SUM(IF(b.CustField_Q4=1, 1,0)) as tot_q4_1,
	SUM(IF(b.CustField_Q4=2, 1,0)) as tot_q4_2,
	SUM(IF(b.CustField_Q5=0, 1,0)) as tot_q5_0,
	SUM(IF(b.CustField_Q5=1, 1,0)) as tot_q5_1,
	SUM(IF(b.CustField_Q5=2, 1,0)) as tot_q5_2,
	SUM(IF(b.CustField_Q6=0, 1,0)) as tot_q6_0,
	SUM(IF(b.CustField_Q6=1, 1,0)) as tot_q6_1,
	SUM(IF(b.CustField_Q6=2, 1,0)) as tot_q6_2,
	SUM(IF(b.CustField_Q7=0, 1,0)) as tot_q7_0,
	SUM(IF(b.CustField_Q7=1, 1,0)) as tot_q7_1,
	SUM(IF(b.CustField_Q7=2, 1,0)) as tot_q7_2,
	SUM(IF(b.CustField_Q8=0, 1,0)) as tot_q8_0,
	SUM(IF(b.CustField_Q8=1, 1,0)) as tot_q8_1,
	SUM(IF(b.CustField_Q8=2, 1,0)) as tot_q8_2,
	SUM(IF(b.CustField_Q9=0, 1,0)) as tot_q9_0,
	SUM(IF(b.CustField_Q9=1, 1,0)) as tot_q9_1,
	SUM(IF(b.CustField_Q9=2, 1,0)) as tot_q9_2,
	SUM(IF(b.CustField_Q10=0, 1,0)) as tot_q10_0,
	SUM(IF(b.CustField_Q10=1, 1,0)) as tot_q10_1,
	SUM(IF(b.CustField_Q10=2, 1,0)) as tot_q10_2 ", 
   FALSE);
	
	
	$this->db->from("t_gn_followup_question b ");
	$this->db->join("t_gn_policy_detail c ","b.PolicyId=c.PolicyId","LEFT");
	$this->db->join("t_gn_debitur d "," c.EnigmaId=d.CustomerNumber","LEFT");
	$this->db->join("t_gn_campaign_project e ","d.CampaignId=e.CampaignId","LEFT");
	$this->db->where("e.ProjectId", 1);
	
// campaignid 

	if(!is_null($this->CampaignId) ){
	 $this->db->where_in("d.CampaignId", $this->CampaignId);	
	}
	
// start_date 

	if(!is_null($this->StartDate)){
	 // $this->db->where("c.PolicyIssDate >='{$this->StartDate}'", '', FALSE);
	 $this->db->where("DATE_FORMAT(c.PolicyIssDate,'%Y-%m') >='".date('Y-m', strtotime($this->StartDate))."'", '', FALSE);
	}	

// end_date 
	
	if(!is_null($this->EndDate)){
	 // $this->db->where("c.PolicyIssDate <='{$this->EndDate}'", '', FALSE);
	 $this->db->where("DATE_FORMAT(c.PolicyIssDate,'%Y-%m')<='".date('Y-m', strtotime($this->EndDate))."'", '', FALSE);
	}
	
	
	$this->db->group_by("bln");
	$this->db->group_by("CampaignId");
	// echo $this -> db ->_get_var_dump();
	
	foreach( $this->db->get()->result_assoc() as $rows  ) 
	{
		foreach( $rows as $field => $name )	
		{
			$summary[$rows['bln']][$rows['CampaignId']][$field] = $name; 
		}
	}
	
	return $summary;
	
	
}
 

 
 
}
?>