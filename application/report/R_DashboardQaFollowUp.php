<?php
class R_DashboardQaFollowUp extends EUI_Report
{
	function R_DashboardQaFollowUp()
	{
		$this -> load -> model(array(
			'M_RptUpload'
		));
	}
	
	function _get_param()
	{
		return array(
			'campaign' 	 => $this->URI->_get_post('campaign'),
			'start_date' => ( $this->URI->_get_have_post('start_date') ? date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : '' ),
			'end_date'	 => ( $this->URI->_get_have_post('end_date') ? date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : '' )
		);
	}
	
	function _send_param()
	{
		$param = $this->_get_param();
		
		return array(
			'title' => 'Daily Dashboard QA By Follow Up',
			'param' => $param,
			'CmpName' => $this->_getCampaignName( array('CampaignId' => $param['campaign']) )
		);
	}
	
	function _getCampaignName($param)
	{
		$datas = array();
		
		$sql = "select a.CampaignId, a.CampaignNumber, a.CampaignName, a.CampaignCode from t_gn_campaign a where a.CampaignId in (".$param['CampaignId'].") order by a.CampaignCode asc";
		$qry = $this->db->query($sql);
		
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0))
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['CampaignId']] = $rows['CampaignCode']."<br/>".$rows['CampaignName'];
			}
		}
		
		return $datas;
	}
}
?>