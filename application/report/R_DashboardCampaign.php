<?php
/**
 * # def : R_DashboardCampaign
 * ----------------------------------------------------
 
 * @ source of data telephony Only On cc_agent_acivity_log
 * @ join by agent collection < t_tx_agent > 
 *
 */
 
class R_DashboardCampaign extends EUI_Report
{

/**
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */

 
public function R_DashboardCampaign() {
 $this -> load -> model(array( 'M_RptUpload' ));
		
}
	
/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */

public function _get_param() {
	return array(
		'spv' 		 => $this->URI->_get_post('supervisor'),
		'caller' 	 => $this->URI->_get_post('caller'),
		'campaign' 	 => $this->URI->_get_post('campaign'),
		'start_date' => ( $this->URI->_get_have_post('start_date') ? date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : '' ),
		'end_date'	 => ( $this->URI->_get_have_post('end_date') ? date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : '' )
	);
}
	
/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
	
public function _send_param() 
{
	$param = $this->_get_param();
	return array (
		'title' 	=> 'Daily Dashboard Campaign Follow Up',
		'param' 	=> $param,
		'AgentName' => $this->M_RptUpload->_getAgentName( array('AgentId' => $param['caller']) ),
		'CampaignName' 	=> $this->CampaignNameByParam(array('CampaignId' => $param['campaign']) ),
		'data_size' => $this->SummarySizeByCampaign($param),
		'data_detail' => $this->DetailSizeByCampaign($param),
	);
}
	
	
/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
public function CampaignNameByParam($param)
{
	$datas = array();
	$sql = "select a.CampaignId, a.CampaignNumber, a.CampaignName, a.CampaignCode from t_gn_campaign a where a.CampaignId in (".$param['CampaignId'].") order by a.CampaignCode asc";
	$qry = $this->db->query($sql);
	
	if( ($qry!==FALSE) AND ($qry->num_rows() > 0))
	{
		foreach($qry->result_assoc() as $rows)
		{
			$datas[$rows['CampaignId']] = $rows['CampaignCode']."<br/>".$rows['CampaignName'];
		}
	}	
	
  return $datas;
}


/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */

public function SummarySizeByCampaign($param)
{
 $datas = array();
 
 $this->db->select(" 
	COUNT( distinct date(a.ActivityDateTs)) as tot_login,
	DATE(a.ActivityDateTs) as tgl, 
	a.UserId as UserId ", 
 FALSE);
 
 $this->db->from("t_tx_agent_activity a");
 $this->db->where("a.ActivityDateTs >='{$param['start_date']} 00:00:00'",'', FALSE);
 $this->db->where("a.ActivityDateTs <='{$param['end_date']} 23:59:59'",'', FALSE);
 $this->db->where("a.ActivityAction", 'LOGIN');
 $this->db->group_by("UserId");
 $this->db->group_by("tgl");
 
 // show data report OK 	
 
 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 )
   foreach($recsource->result_assoc() as $rows )
  { 
	$datas[$rows['UserId']]['tot_efective_days']+= $rows['tot_login'];
	$datas[$rows['UserId']]['tot_capacity_day']= 70;
  }

 $this->db->select("b.AssignSelerId as UserId,
		a.CampaignId as CampaignId,
		COUNT(d.PolicyId) as tot_policy,
		SUM(IF(d.CallComplete = 1, 1,0)) as tot_pol_complete,
		SUM(IF(d.PolicyUpdatedTs IS NOT NULL, 1, 0)) AS tot_pol_pending, 
		SUM(IF((d.CallComplete = 0 AND d.PolicyUpdatedTs IS NULL), 1, 0 )) as total_pol_new ", FALSE);
		
	$this->db->from('t_gn_debitur a ');
	$this->db->join('t_gn_assignment b ',' a.CustomerId=b.CustomerId', 'INNER');
	$this->db->join('t_gn_cif_customer c ',' a.CustomerNumber=c.EnigmaId', 'LEFT');
	$this->db->join('t_gn_policy_detail d ',' a.CustomerNumber=d.EnigmaId', 'LEFT');
	$this->db->join('t_gn_bucket_customers e ',' c.BucketId=e.BucketId', 'LEFT');
	$this->db->join('t_gn_campaign f ',' a.CampaignId=f.CampaignId', 'LEFT');
	$this->db->join('t_gn_campaign_project g ','f.CampaignId=g.CampaignId', 'LEFT');
	$this->db->where_in('g.ProjectId',array(1));
	$this->db->group_by('UserId');
	$this->db->group_by('CampaignId');
	$this->db->having("UserId IS NOT NULL");
	
// show data report OK 
	
	$recsource = $this->db->get();
	
	if( $recsource->num_rows() > 0 )foreach($recsource->result_assoc() as $rows )
	 { 
		
		$datas[$rows['UserId']]['tot_data_size']+= $rows['tot_policy'];
		$datas[$rows['UserId']]['tot_pol_unutilize']+= $rows['total_pol_new'];
		$datas[$rows['UserId']]['tot_pol_utilize']+= $rows['tot_pol_pending'];
		$datas[$rows['UserId']][$rows['CampaignId']]['tot_size_pending']+= $rows['tot_pol_pending'];
		$datas[$rows['UserId']][$rows['CampaignId']]['tot_size_new']+= $rows['total_pol_new'];
	 }
	
	 return $datas;
 }

/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
 
public function DetailSizeByCampaign() 
{
 $datas = array();
 $this->db->select("b.AssignSelerId as UserId,
	a.CampaignId as CampaignId,
	COUNT(d.PolicyId) as tot_policy,
	SUM(IF((d.CallComplete = 0 AND d.PolicyUpdatedTs IS NULL), 1, 0)) AS size_attempt1,
	SUM(IF(d.CallAttempt=1, 1, 0)) AS size_attempt2,
	SUM(IF(d.CallAttempt=2, 1, 0)) AS size_attempt3,
	SUM(IF(d.CallAttempt=3, 1, 0)) AS size_attempt4,
	SUM(IF(d.CallAttempt=4, 1, 0)) AS size_attempt5,
	SUM(IF(d.CallAttempt=5, 1, 0)) AS size_attempt6,
	SUM(IF(d.CallAttempt>=6, 1, 0)) AS size_attempt7 ", 
	FALSE);
	
 $this->db->from('t_gn_debitur a ');
 $this->db->join('t_gn_assignment b ',' a.CustomerId=b.CustomerId', 'INNER');
 $this->db->join('t_gn_cif_customer c ',' a.CustomerNumber=c.EnigmaId', 'LEFT');
 $this->db->join('t_gn_policy_detail d ',' a.CustomerNumber=d.EnigmaId', 'LEFT');
 $this->db->join('t_gn_bucket_customers e ',' c.BucketId=e.BucketId', 'LEFT');
 $this->db->join('t_gn_campaign f ',' a.CampaignId=f.CampaignId', 'LEFT');
 $this->db->join('t_gn_campaign_project g ','f.CampaignId=g.CampaignId', 'LEFT');
 $this->db->where_in('g.ProjectId',array(1));
 $this->db->group_by('UserId');
 $this->db->group_by('CampaignId');
 $this->db->having("UserId IS NOT NULL");
 
 // echo $this->db->_get_var_dump();
 
 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 )foreach($recsource->result_assoc() as $rows )
 { 
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt1']+= $rows['size_attempt1'];	
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt2']+= $rows['size_attempt2'];
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt3']+= $rows['size_attempt3'];
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt4']+= $rows['size_attempt4'];
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt5']+= $rows['size_attempt5'];
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt6']+= $rows['size_attempt6'];
	$datas[$rows['UserId']][$rows['CampaignId']]['size_attempt7']+= $rows['size_attempt7'];
 }
 
 return $datas;	
 
			
 }
 
 
}
?>