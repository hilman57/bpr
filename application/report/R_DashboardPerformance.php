<?php
/**
 * # def : R_DashboardPerformance
 * ----------------------------------------------------
 
 * @ source of data telephony Only On cc_agent_acivity_log
 * @ join by agent collection < t_tx_agent > 
 *
 */
 
class R_DashboardPerformance extends EUI_Report
{

/**
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
protected $StartDate  = NULL;
protected $EndDate    = NULL;
protected $CallerId 	= NULL;

/**
 * @ def : aksessor of class attributr
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : void()
 */
 
public function R_DashboardPerformance() 
{
	$this -> load -> model(array( 'M_RptUpload' ));
	
// set parameter data 

	$this->StartDate = ($this->URI->_get_have_post('start_date') ? date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : NULL );
	$this->EndDate   = ($this->URI->_get_have_post('end_date') ? date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : NULL );
	$this->CallerId  = ($this->URI->_get_have_post('caller') ? $this->URI->_get_array_post('caller'): NULL );
}

/**
 * @ def : function set attribute post parameter 
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : function 
 */
 
public function _get_param()
{
	return array(
		'spv' 		 => $this->URI->_get_post('supervisor'),
		'caller' 	 => $this->URI->_get_post('caller'),
		'start_date' => ( $this->URI->_get_have_post('start_date') ? date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : '' ),
		'end_date'	 => ( $this->URI->_get_have_post('end_date') ? date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : '' )
	);
}


/**
 * @ def : function set attribute post parameter 
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : function
 */
 
public function _send_param()
{

$param = $this->_get_param();	
if( strtotime($this->StartDate) <= strtotime($this->EndDate) ) {	
	return array(
		'title' => 'Daily Dashboard Caller Performance',
		'param' => $param,
		'AgentName' => $this->M_RptUpload->_getAgentName( array('AgentId' => $param['caller']) ),
		'agent_activity' => $this->AgentDailyActivityPerformance()
	);
}
else{
	exit(" start date more then en date .");
}
	
}

/**
 * @ def : function set attribute post parameter 
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : function
 */
 	
public function AgentDailyActivityPerformance()
{
 $agent_activity = array();


/*############# section login / logout ##################*/

 $this->db->select(" 
	 a.UserId as UserId, 
	 DATE(a.ActivityDateTs) as tgl,
	(SELECT act.ActivityDateTs
	 FROM t_tx_agent_activity act
	 WHERE DATE(act.ActivityDateTs) = DATE(a.ActivityDateTs) 
	 AND act.ActivityAction='LOGIN' 
	 AND act.UserId=a.UserId 
	 ORDER BY act.ActivityId ASC LIMIT 1) AS LOGIN, 
	
	(SELECT MAX(act.ActivityDateTs)
	 FROM t_tx_agent_activity act
	 WHERE DATE(act.ActivityDateTs) = DATE(a.ActivityDateTs) 
	 AND act.ActivityAction='LOGOUT'
	 AND act.UserId=a.UserId
	 ORDER BY act.ActivityId DESC LIMIT 1 ) AS LOGOUT ", FALSE);
 $this->db->from("t_tx_agent_activity a");

 
/** if have start_date **/

 if( !is_null($this->StartDate)) 
 $this->db->where("a.ActivityDateTs>='{$this->StartDate} 00:00:00'", "", FALSE);

/** if have end_date **/

 if( !is_null($this->EndDate))
 $this->db->where("a.ActivityDateTs<='{$this->EndDate} 23:59:59'", "", FALSE);
	
/** if have post Caller ID **/

 if( !is_null($this->CallerId))
 $this->db->where_in("a.UserId", $this->CallerId);
 
 
/* grouping data **/

 $this->db->group_by("tgl");
 $this->db->group_by("UserId");
/* on recsource **/

 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 ) 
 foreach( $recsource->result_assoc() as $rows )
 {
 
   // cek 1 
	$agent_activity[$rows['tgl']][$rows['UserId']]['login']= ( !is_null($rows['LOGIN']) ? 
			date('H:i:s', strtotime($rows['LOGIN'])) : '07:00:00');
	
  // cek 2 
  
	$agent_activity[$rows['tgl']][$rows['UserId']]['logout']= ( !is_null($rows['LOGOUT']) ? 
		date('H:i:s',strtotime($rows['LOGOUT'])) : '19:00:00' );
	
 // cek callculation
 
	if( isset($rows['LOGOUT']) AND isset($rows['LOGIN']) 
		AND !is_null($rows['LOGOUT']) AND !is_null($rows['LOGIN']) )
	{ 
		$agent_activity[$rows['tgl']][$rows['UserId']]['tot_activity']=(strtotime($rows['LOGOUT'])-strtotime($rows['LOGIN']));
	}
	else
	{
		$start_time = date('Y-m-d', strtotime($rows['LOGIN']));
		$end_time  = $start_time . ' 19:00:00';
		$agent_activity[$rows['tgl']][$rows['UserId']]['tot_activity']=(strtotime($end_time)-strtotime($rows['LOGIN']));
		
	}
 }	
 

/*############# section call complet ever dial ##################*/
 
 $this->db->select(" 
	date(e.CallHistoryCreatedTs) as tgl, 
	d.AssignSelerId as UserId,
	count(distinct e.PolicyId) as tot_complete", FALSE);
	
 $this->db->from("t_gn_callhistory e ");
 $this->db->join("t_gn_policy_detail a","e.PolicyId=a.PolicyId","LEFT"); 
 $this->db->join("t_gn_debitur b","a.EnigmaId=b.CustomerNumber","LEFT");
 $this->db->join("t_gn_cif_customer c","b.CustomerNumber=c.EnigmaId","INNER"); 
 $this->db->join("t_gn_assignment d","b.CustomerId=d.CustomerId","LEFT");
 $this->db->where("a.CallComplete",1);
 
/** if have start_date **/

 if( !is_null($this->StartDate)) 
 $this->db->where("e.CallHistoryCreatedTs>='{$this->StartDate} 00:00:00'", "", FALSE);

/** if have end_date **/

 if( !is_null($this->EndDate))
 $this->db->where("e.CallHistoryCreatedTs<='{$this->EndDate} 23:59:59'", "", FALSE);
	
/** if have post Caller ID **/

 if( !is_null($this->CallerId))
 $this->db->where_in("d.AssignSelerId", $this->CallerId);
 
/* grouping data **/
 
 $this->db->group_by("tgl");
 $this->db->group_by("UserId");
 
 /* recsource **/
 
 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 ) 
 foreach( $recsource->result_assoc() as $rows )
 {
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_complete']+= $rows['tot_complete'];
 }	
	

/*############# section call atemp ever dial ##################*/
  
 $this->db->select("
		c.UserId as UserId, 
		c.full_name as UserName, 
		date(a.start_time) as tgl,
		COUNT(a.id) as tot_attempt", FALSE);

 $this->db->from('cc_call_session a ');
 $this->db->join("cc_agent b "," a.agent_id=b.id","LEFT");
 $this->db->join("t_tx_agent c "," b.userid=c.id ","LEFT");
 
/** if have start_date **/
 
 if( !is_null($this->StartDate)) 
 $this->db->where(" a.start_time>='{$this->StartDate} 00:00:00'", "", FALSE);

/** if have end_date **/

 if( !is_null($this->EndDate))
 $this->db->where(" a.start_time<='{$this->EndDate} 23:59:59'", "", FALSE);
	
/** if have post Caller ID **/

 if( !is_null($this->CallerId))
 $this->db->where_in("c.UserId", $this->CallerId);
 
 $this->db->group_by("tgl"); 
 $this->db->group_by("UserId");
 $this->db->having("UserId IS NOT NULL"); 
 
/* on recsource **/

 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 ) 
 foreach( $recsource->result_assoc() as $rows )
 {
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_attempt']+= $rows['tot_attempt'];
 }	
	
	
/*############# section agent activity ##################*/
 	
 $this->db->select("
		DATE(a.start_time) as tgl, c.UserId as UserId, c.full_name,
		SUM(IF(a.`status` = 1, (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) as tot_ready,
		SUM(IF(a.`status` = 2, (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) as tot_not_ready,
		SUM(IF(a.`status` IN(1,2), (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) as tot_idle,
		SUM(IF(a.`status` IN(4), (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) as tot_busy,
		SUM(IF(a.`status` IN(3), (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) as tot_acw,
		SUM(IF(a.`status` IN(0), (UNIX_TIMESTAMP(a.end_time)- UNIX_TIMESTAMP(a.start_time)), 0)) as tot_logout", FALSE);
	
 $this->db->from('cc_agent_activity_log a ');
 $this->db->join('cc_agent b ',' a.agent=b.id','LEFT');
 $this->db->join('t_tx_agent c ',' b.userid=c.id','LEFT');
	
/** if have start_date **/

 if( !is_null($this->StartDate)) 
 $this->db->where(" a.start_time>='{$this->StartDate} 00:00:00'", "", FALSE);

/** if have end_date **/

 if( !is_null($this->EndDate))
 $this->db->where(" a.start_time<='{$this->EndDate} 23:59:59'", "", FALSE);
	
/** if have post Caller ID **/

 if( !is_null($this->CallerId))
 $this->db->where_in("c.UserId", $this->CallerId);
	
/** then group date by UserId **/
	
 $this->db->group_by('tgl');
 $this->db->group_by('UserId');
 $this->db->having('UserId IS NOT NULL');
 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 ) 
 foreach( $recsource->result_assoc() as $rows )
 {
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_ready']+= $rows['tot_ready'];
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_not_ready']+= $rows['tot_not_ready'];
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_idle']+= $rows['tot_idle'];
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_busy']+= $rows['tot_busy'];
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_acw']+= $rows['tot_acw'];
	$agent_activity[$rows['tgl']][$rows['UserId']]['tot_logout']+= $rows['tot_logout'];
 }
 
 
 return $agent_activity;
 
}	

}
?>