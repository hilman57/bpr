<?php
class R_DashboardQaUser extends EUI_Report
{

var $is_checked_quality = NULL;
var $FU_Title = null;

/* @ def : constr$uctor **/

public function __construct() 
{
	$this->load->model(array( 'M_RptUpload' ));
	$this->is_checked_quality = array(1); //checked  
	
	if(is_null($this->FU_Title))
	{
		$this->FU_Title['need_agent'] = "Surveyed - Need Agent";
		$this->FU_Title['visit_to_branch'] ="Surveyed - Visit to Branch";
		$this->FU_Title['POS'] = "Surveyed - Change Address/Ph/Email";
		$this->FU_Title['pos_need_agent'] = "Surveyed - Need Agent & Change Address/Ph/Email";
		$this->FU_Title['pos_visit_to_branch'] = "Surveyed - Visit to Branch & Change Address/Ph/Email";
		$this->FU_Title['clean'] = "Surveyed - Clean";
	}
}

/* @ def : 	_get_param **/

public function _get_param()
{
	return array(
		'quality_id' => $this->URI->_get_array_post('quality_id'),
		'campaign' 	 => $this->URI->_get_array_post('campaign'),
		'start_date' => ( $this->URI->_get_have_post('start_date') ? date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : '' ),
		'end_date'	 => ( $this->URI->_get_have_post('end_date') ? date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : '' )
	);
}

	
/* @ def : _send_param */
	
public function _send_param()
{

 $config = NULL;
 
 $param = $this->_get_param();
 
 if( is_null($config) ) {
   $config = array 
   (
		'title_1' => 'Daily Dashboard QA By User QA',
		'param' => $param,
		'UserQuality' => $this->getQualityName(),
		'CmpName' => $this->CampaignNameByParam(array('CampaignId' => $param['campaign']) ),
		'data_size'	=> $this->SummarySizeByCampaign($param),
		'Fu_data_size'	=> $this->SummarySizeByFollowup($param),
		'title_2'	=> 'Qa Monitor All User',
		'FU_Title'	=> $this->FU_Title
	);
 }	
 
 return $config;
 
}

	
/** @ def : _getCampaignName **/

public function getQualityName()
{
	$QualityId = array();
	
	$this->db->select("a.UserId, a.full_name, a.code_user", FALSE);
	$this->db->from("t_tx_agent a");
	$this->db->where_in("a.UserId", $this->URI->_get_array_post('quality_id'));
	
	$res = $this->db->get();
	if( $res->num_rows()>0 )
		foreach( $res->result_assoc() as $rows )
	{
		$QualityId[$rows['UserId']] = $rows['code_user'] ." - ". $rows['full_name'];	
	}
	
	return $QualityId;
}

	
/** @ def : _getCampaignName **/
	
 public function _getCampaignName($param) 
 {
 
  $CampaignName = array();
  
  $this->db->select("a.CampaignId, a.CampaignNumber, a.CampaignName, a.CampaignCode",FALSE);
  $this->db->from("t_gn_campaign a ");
  $this->db->where_in("a.CampaignId", $param['CampaignId']);
  $this->db->order_by("a.CampaignCode", "ASC");
  
  $res = $this->db->get();
  if( $res->num_rows()>0) 
	foreach($res->result_assoc() as $rows)
  {
	$CampaignName[$rows['CampaignId']] = $rows['CampaignCode']."<br/>".$rows['CampaignName'];
  }
 
 return $$CampaignName;
 
}
	
	
/** _get_datas_cmp **/
	
public function _get_datas_cmp($prm)
	{
		$datas = array();
		
		$sql = "select 
					a.QueueId,
					b.CampaignId, ";
		
		$cmp_arr = explode(',',$prm['campaign']);
		
		foreach($cmp_arr as $id)
		{
			$sql .= " sum(if(b.CampaignId='".$id."',1,0)) as Campaign".$id.", ";
		}
							
		$sql .=	" count(a.CustomerId) as TotalData 
				from t_gn_debitur a
				left join t_gn_campaign b on a.CampaignId = b.CampaignId
				left join t_gn_policy_detail c on a.CustomerNumber = c.EnigmaId
				where 1=1
				AND c.CallComplete = 1
				AND a.CallReasonQue is not null
				AND a.QueueId is not null
				AND a.QueueId in (".$prm['quality_id'].")
				AND date(a.CustomerRejectedDate) >= '".$prm['start_date']."'
				AND date(a.CustomerRejectedDate) <= '".$prm['end_date']."'
				group by a.QueueId";
		$qry = $this->db->query($sql);
		
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['QueueId']] = $rows;
			}
		}
		
		return $datas;
	}
	
/** @ def : 	_get_QAData_per_Campaign **/

public function _get_QAData_per_Campaign($param)
{
	$sql = " select distinct(b.CampaignId), concat(c.CampaignCode,' ',c.CampaignName) as CampaignName , a.QualityUserId, e.full_name, count(a.PolicyId) as Counts
	from t_gn_policy_detail a
	left join t_gn_debitur b on b.CustomerNumber = a.EnigmaId
	left join t_gn_campaign c on b.CampaignId = c.CampaignId
	left join t_gn_campaign_project d on c.CampaignId = d.CampaignId
	left join t_tx_agent e ON a.QualityUserId = e.UserId
	where a.CallComplete = 1 AND d.ProjectId = 1
	group by b.CampaignId, a.QualityUserId";
	
	$qry = $this->db->query($sql);
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$data[$rows['CampaignId']]['CampaignName'] = $rows['CampaignName'];
			$data[$rows['CampaignId']][$rows['QualityUserId']] = $rows['full_name'];
			$data[$rows['CampaignId']]['Count'] = $rows['Counts'];
		}
	}
		
	return $data;
}

/** new line **/

public function CampaignNameByParam($param)
{
	static $datas = array();
	
	
	$this->db->reset_select();
	$this->db->select("a.CampaignId, a.CampaignNumber, a.CampaignName, a.CampaignCode", FALSE);
	$this->db->from("t_gn_campaign a");
	$this->db->where_in("a.CampaignId", $param['CampaignId']);
	$this->db->order_by("a.CampaignCode", "ASC");
	$qry = $this->db->get();
	if( $qry->num_rows() > 0)  
		foreach($qry->result_assoc() as $rows) 
	{
		$datas[$rows['CampaignId']] = $rows['CampaignCode']."<br/>".$rows['CampaignName'];
	}
	
  return $datas;
}


/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */

public function SummarySizeByCampaign($param)
{
 $datas = array();
 //$this->db->reset_select();
 
 $this->db->select("COUNT( distinct date(a.ActivityDateTs)) as tot_login, DATE(a.ActivityDateTs) as tgl, a.UserId as UserId ",FALSE);
 $this->db->from("t_tx_agent_activity a");
 $this->db->where("a.ActivityDateTs >='{$param['start_date']} 00:00:00'",'', FALSE);
 $this->db->where("a.ActivityDateTs <='{$param['end_date']} 23:59:59'",'', FALSE);
 $this->db->where("a.ActivityAction", 'LOGIN');
 $this->db->where_in("a.UserId",$param['quality_id']);
 $this->db->group_by("UserId");
 $this->db->group_by("tgl");
 
 // show data report OK 	
 
 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 )
   foreach($recsource->result_assoc() as $rows )
  { 
	$datas[$rows['UserId']]['tot_efective_days']+= $rows['tot_login'];
	$datas[$rows['UserId']]['tot_capacity_day']= 70;
  }
 
 
/** reset array cache **/
 
 //$this->db->reset_select();
 
 $this->db->select("COUNT(a.PolicyNumber) as total_size, a.QualityStatusId, a.QualityUserId , b.CampaignId, a.QualityUpdateTs", FALSE);
 $this->db->from("t_gn_policy_detail a ");
 $this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
 $this->db->where("a.QualityUpdateTs>= '{$param['start_date']} 00:00:00'", "", FALSE);
 $this->db->where("a.QualityUpdateTs<= '{$param['end_date']} 23:59:59'", "", FALSE);
 $this->db->where_in("a.QualityStatusId", $this->is_checked_quality); // checked && checked ALL 
 $this->db->where_in("a.QualityUserId",$param['quality_id']);
 $this->db->where_in("b.CampaignId", $param['campaign']);
 $this->db->group_by(array("a.QualityUserId","b.CampaignId"));


// show data report OK 
	
	$recsource = $this->db->get();
	
	if( $recsource->num_rows() > 0 )foreach($recsource->result_assoc() as $rows )
	 { 
		$datas[$rows['QualityUserId']]['tot_data_size']+= $rows['total_size'];
		$datas[$rows['QualityUserId']][$rows['CampaignId']]['tot_size_monitored']+= $rows['total_size'];
		$datas[$rows['QualityUserId']][$rows['CampaignId']]['tot_size_new']+= $rows['total_pol_new'];
	 }
	 
	 return $datas;
 }

/* *
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
public function SummarySizeByFollowup($param)
{
 $data = array();
	// $this->db->reset_select();
	
	$this->db->select(" 
		c.CampaignId AS CampaignId, a.QualityUserId,
		SUM(IF((d.CustField_Q7=1 AND d.CustField_Q1=1 AND d.CustField_Q2=1 AND d.CustField_Q3=1), 1,0)) as visit_to_branch,
		SUM(IF( ( (d.CustField_Q1+d.CustField_Q2+d.CustField_Q3)=3 AND d.CustField_Q7 IN(2)), 1,0)) as Clean,
		SUM(IF(( (d.CustField_Q7=0) AND (d.CustField_Q1+d.CustField_Q2+d.CustField_Q3)=3 ), 1,0)) as need_agent,
		SUM(IF(( (d.CustField_Q1+d.CustField_Q2+d.CustField_Q3)<3 AND d.CustField_Q7 IN(2) ) ,1,0) ) as POS,
		SUM(IF(((d.CustField_Q1+d.CustField_Q2+d.CustField_Q3)<3 AND d.CustField_Q7 IN(1) ) ,1,0) ) as pos_visit_to_branch,
		SUM(IF(((d.CustField_Q1+d.CustField_Q2+d.CustField_Q3)<3 AND d.CustField_Q7 IN(0) ) ,1,0) ) as pos_need_agent", 
	 FALSE);
	
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur c "," a.EnigmaId=c.CustomerNumber","LEFT");
	$this->db->join("t_gn_followup_question d "," a.PolicyId=d.PolicyId","LEFT");
	$this->db->where("a.QualityUpdateTs>= '{$param['start_date']} 00:00:00'", "",FALSE);
	$this->db->where("a.QualityUpdateTs<= '{$param['end_date']} 23:59:59'", "",FALSE);
	$this->db->where_in("a.QualityStatusId",$this->is_checked_quality);
	$this->db->where_in("a.QualityUserId",$param['quality_id']);
	$this->db->where_in("c.CampaignId", $param['campaign']);
	
	$this->db->group_by("a.QualityUserId,c.CampaignId");
	
	//echo $this->db->_get_var_dump();
	
	
	$recsource = $this->db->get();
	if( $recsource->num_rows() > 0 )
	foreach($recsource->result_assoc() as $rows )
	{
		$data['POS'][$rows['CampaignId']]+= $rows['POS'];
		$data['visit_to_branch'][$rows['CampaignId']]+= $rows['visit_to_branch'];
		$data['need_agent'][$rows['CampaignId']]+= $rows['need_agent'];
		$data['pos_need_agent']	[$rows['CampaignId']]+= $rows['pos_need_agent'];
		$data['pos_visit_to_branch'][$rows['CampaignId']]+= $rows['pos_visit_to_branch'];
		$data['clean'][$rows['CampaignId']]+= $rows['Clean'];
		$data['POS']['total_size']+= $rows['POS'];
		$data['visit_to_branch']['total_size']+= $rows['visit_to_branch'];
		$data['need_agent']['total_size']+= $rows['need_agent'];
		$data['pos_need_agent']['total_size']+= $rows['pos_need_agent'];
		$data['pos_visit_to_branch']['total_size']+= $rows['pos_visit_to_branch'];
		$data['clean']['total_size']+= $rows['Clean'];
		
		
	}

	return $data;
}

}
?>