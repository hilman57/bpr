<?php



class R_CallOutboundReport extends EUI_Report
{
private static $_level1  = array();
private static $_level2  = array();
private static $_level3  = array();

/** attribute protected **/

protected $_config = array();
protected $_params = array();

/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function R_CallOutboundReport() 
{


// tree level data view 

$this->_level1 = array (
	array('iP' => 1, 'iD'=>0, 'iL' => 'Number of Policies', 'cases' => 'total_policy', 'premi' => 'sum_policy', 'iSS' => FALSE, 'jSS' => 'totalPolicyNumber'),
	array('iP' => 2, 'iD'=>0, 'iL' => 'Untouched', 'cases' => 'total_untouched', 'premi' => 'sum_untouched', 'iSS' => FALSE, 'jSS' => 'totalUntouched'  ),
	array('iP' => 3, 'iD'=>0, 'iL' => 'Touched', 'cases' => 'total_touched', 'premi' => 'sum_touched', 'iSS' => FALSE, 'jSS' => 'totalTouched' ),
	array('iP' => 4, 'iD'=>0, 'iL' => 'Dead Tone', 'cases' => 'total_dead_tone', 'premi' => 'sum_dead_tone', 'iSS' => TRUE, 'jSS' => 'ParentDetailDeadTone', 'iC' => array 
			( 
				array('iP' =>4, 'iD' =>'28001', 'iL' => 'Invalid Number','cases' => 'total_invalid_number', 'premi' => 'sum_invalid_number','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
				// array('iP' =>4, 'iD' =>'28002', 'iL' => 'Wrong Number','cases' => 'total_wrong_number', 'premi' => 'sum_wrong_number','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
				array('iP' =>4, 'iD' =>'28003', 'iL' => 'Dead Tone','cases' => 'total_dead_tone', 'premi' => 'sum_dead_tone','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
				array('iP' =>4, 'iD' =>'28004', 'iL' => 'Tulalit','cases' => 'total_tulalit', 'premi' => 'sum_tulalit','iSS' => TRUE, 'jSS' => 'ChildDetailByCode')
			) 
		 ),

	array('iP' => 5, 'iD'=>0, 'iL' => 'Connected','cases' => 'total_connected', 'premi' => 'sum_connected','iSS' => FALSE, 'jSS' => 'ParentDetailConnected'),
	array('iP' => 6, 'iD'=>0, 'iL' => 'Unconnected','cases' => 'total_unconnected', 'premi' => 'sum_unconnected','iSS' => FALSE, 'jSS' => 'ParentDetailUnconnected'),
	array('iP' => 7, 'iD'=>0, 'iL' => 'Contacted','cases' => 'total_contacted', 'premi' => 'sum_contacted', 'iSS' => FALSE, 'jSS' => 'ParentDetailContacted'),
	
	array('iP' => 8, 'iD'=>0, 'iL' => 'Cust Promise to Pay','cases' => 'total_promise_to_pay', 'premi' => 'sum_promise_to_pay', 'iSS' => TRUE, 'jSS' => 'ParentDetailPTP', 'iC' => array(
		array('iP' =>8, 'iD' =>'29001', 'iL' => 'Cust Promise to Pay', 'cases' => 'total_customer_promise_to_pay', 'premi' => 'sum_customer_promise_to_pay', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>8, 'iD' =>'29002', 'iL' => 'Already paid on System','cases' => 'total_already_paid_on_system', 'premi' => 'sum_already_paid_on_system', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode')
	)),
	
	array('iP' => 9, 'iD'=>0, 'iL' => 'Cust Not Promise to Pay', 'cases' => 'total_cust_not_promise_to_pay', 'premi' => 'sum_cust_not_promise_to_pay', 'iSS' => FALSE, 'jSS' => 'ParentDetailNotPTP', 'iC' => array(
		array('iP' =>9, 'iD' =>'21001', 'iL' => 'Financial-Problem', 'cases' => 'total_financial_problem', 'premi' => 'sum_financial_problem', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21002', 'iL' => 'Unsuitable Product','cases' => 'total_unsuitable_product', 'premi' => 'sum_unsuitable_product', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21003', 'iL' => 'APH' ,'cases' => 'total_automatic_premium_holiday', 'premi' => 'sum_automatic_premium_holiday', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21004', 'iL' => 'Miss Representative' ,'cases' => 'total_miss_representative', 'premi' => 'sum_miss_representative', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21005', 'iL' => 'Dissapoint/Complaint' ,'cases' => 'total_dissapoint', 'premi' => 'sum_dissapoint', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21006', 'iL' => 'Others*','cases' => 'total_others', 'premi' => 'sum_others' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21007', 'iL' => 'Moving' ,'cases' => '', 'premi' => '','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21008', 'iL' => 'Buy New Policy','cases' => 'total_buy_new_policy', 'premi' => 'sum_buy_new_policy','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'00000', 'iL' => 'Considering' ,'cases' => '', 'premi' => '','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21009', 'iL' => 'No reason','cases' => 'total_no_reason', 'premi' => 'sum_no_reason', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'00000', 'iL' => 'Pay Only 5 Years' ,'cases' => '', 'premi' => '', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>9, 'iD' =>'21011', 'iL' => 'Want to Surrender', 'cases' => 'total_want_to_surrender', 'premi' => 'sum_want_to_surrender', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCode') 
	)),
	
	
	array('iP' =>10, 'iD'=>'NULL', 'iL' => 'Uncontacted', 'cases' => 'total_notcontacted', 'premi' => 'sum_notcontacted', 'iSS' => TRUE, 'jSS' => 'ParentDetailOptionCompleted', 'iC' => array(
		array('iP' =>10, 'iD' =>'1', 'iL' => 'Completed', 'cases' => 'total_completed', 'premi' => '', 'iSS' => TRUE, 'jSS' => 'ParentDetailOptionCompleted',  'iC' => array
		(
			array('iP' =>1, 'iD' =>'21102', 'iL' => 'No Answer' , 'cases' => '', 'premi' => '', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeIsComplete'),
			array('iP' =>1, 'iD' =>'21101', 'iL' => 'Busy Tone', 'cases' => '', 'premi' => '', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeIsComplete'),
			array('iP' =>1, 'iD' =>'21103', 'iL' => 'Mail Box', 'cases' => '', 'premi' => '' , 'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeIsComplete'),
			array('iP' =>1, 'iD' =>'21104', 'iL' => 'Cust. Are not in place' , 'cases' => '', 'premi' => '', 'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeIsComplete'),
			array('iP' =>1, 'iD' =>'21105', 'iL' => 'Cust. Is Busy', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeIsComplete'),
			// array('iP' =>1, 'iD' =>'21106', 'iL' => 'Call back Later' , 'cases' => '', 'premi' => '','iSS' => TRUE, 'jSS' => 'ChildDetailByCodeIsComplete') 
			array('iP' =>2, 'iD' =>'21106', 'iL' => 'Call back Later', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'28002', 'iL' => 'Wrong Number', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'21107', 'iL' => 'Fax Number', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete')
		)),
		
		array('iP' =>10, 'iD' =>'0', 'iL' => 'Not Completed','cases' => 'total_not_completed', 'premi' => 'sum_not_completed', 'iSS' => TRUE, 'jSS' => 'ParentDetailOptionCompleted', 'iC' => array
		(
			array('iP' =>2, 'iD' =>'21102', 'iL' => 'No Answer', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'21101', 'iL' => 'Busy Tone', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'21103', 'iL' => 'Mail Box', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'21104', 'iL' => 'Cust. Are not in place' , 'cases' => '', 'premi' => '','iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'21105', 'iL' => 'Cust. Is Busy', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			// array('iP' =>2, 'iD' =>'21106', 'iL' => 'Call back Later', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete')
			array('iP' =>2, 'iD' =>'21106', 'iL' => 'Call back Later', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'28002', 'iL' => 'Wrong Number', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete'),
			array('iP' =>2, 'iD' =>'21107', 'iL' => 'Fax Number', 'cases' => '', 'premi' => '' ,'iSS' => TRUE, 'jSS' => 'ChildDetailByCodeNotComplete')
		))
	)),
	
	
	array('iP' =>11, 'iD'=>0, 'iL' => 'Premium Paid','cases' => '', 'premi' => '','iC' =>array
	(
		array('iP' =>11, 'iD' =>1, 'iL' => 'From Promise to Pay','cases' => '', 'premi' => ''),
		array('iP' =>11, 'iD' =>2, 'iL' => 'From Not Promise to Pay','cases' => '', 'premi' => ''),
		array('iP' =>11, 'iD' =>3, 'iL' => 'Others', 'cases' => '', 'premi' => '')
	),'iSS' => true, 'jSS' => ''),	
	
	array('iP' =>12, 'iD'=>'0', 'iL' => 'Return List','cases' => 'total_return_list', 'premi' => 'sum_return_list', 'iSS' => TRUE, 'jSS' => 'ParentDetailReturnList',  'iC' => array
	(
		array('iP' =>12, 'iD' =>'21301', 'iL' => 'Grace Periode Ended', 'cases' => 'total_grace_periode_ended', 'premi' => 'sum_grace_periode_ended','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>12, 'iD' =>'21007', 'iL' => 'Customer Move Overseas','cases' => 'total_moving_to_overseas', 'premi' => 'sum_moving_to_overseas','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>12, 'iD' =>'21302', 'iL' => 'Pass Away','cases' => 'total_customer_passed_away', 'premi' => 'sum_customer_passed_away','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		//array('iP' =>12, 'iD' =>'XXXXX', 'iL' => 'No Phone Numbers','cases' => '', 'premi' => '','iSS' => TRUE, 'jSS' => 'ChildDetailByCode'),
		array('iP' =>12, 'iD' =>'21303', 'iL' => 'Surrender','cases' => 'total_want_to_surrender', 'premi' => 'sum_want_to_surrender','iSS' => TRUE, 'jSS' => 'ChildDetailByCode') 
	
	)),
	
	array('iP' =>13, 'iD'=>0, 'iL' =>'Customer Not Received Letter Notification','cases' => 'total_want_to_surrender', 'premi' => 'sum_want_to_surrender'),
	array('iP' =>14, 'iD'=>0, 'iL' =>'Customer Not Received SMS Notification','cases' => 'total_want_to_surrender', 'premi' => 'sum_want_to_surrender'),
	array('iP' =>15, 'iD'=>0, 'iL' =>'Policy Changes Request','cases' => 'total_customer_request', 'premi' => 'sum_customer_request'),
	
	array('iP' =>16, 'iD'=>0, 'iL' => 'Call Attempt','cases' => 'total_call_atempt', 'premi' => 'sum_call_atempt', 'iSS' => true, 'jSS' => 'ParentDetailByAtempt', 'iC' => array
		( 
		   array('iP' =>16,  'iD'=>'1', 'iL' => '1stcall','cases' => 'total_1stcall_atempt', 'premi' => 'sum_1stcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'2', 'iL' => '2ndcall','cases' => 'total_2ndcall_atempt', 'premi' => 'sum_2ndcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'3', 'iL' => '3thcall','cases' => 'total_3thcall_atempt', 'premi' => 'sum_3thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'4', 'iL' => '4thcall','cases' => 'total_4thcall_atempt', 'premi' => 'sum_4thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'5', 'iL' => '5thcall','cases' => 'total_5thcall_atempt', 'premi' => 'sum_5thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'6', 'iL' => '6thcall','cases' => 'total_6thcall_atempt', 'premi' => 'sum_6thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'7', 'iL' => '7thcall','cases' => 'total_7thcall_atempt', 'premi' => 'sum_7thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'8', 'iL' => '8thcall','cases' => 'total_8thcall_atempt', 'premi' => 'sum_8thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'9', 'iL' => '9thcall','cases' => 'total_9thcall_atempt', 'premi' => 'sum_9thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt'),
		   array('iP' =>16,  'iD'=>'10', 'iL' => '10thcall and more','cases' => 'total_10thcall_atempt', 'premi' => 'total_10thcall_atempt','iSS' => true, 'jSS' => 'ChildDetailByAtempt')
		) 
	),
		
	array('iP' =>17, 'iD'=>0, 'iL' => 'Call Attempt to Contacted', 'cases' => 'total_call_contact', 'premi' => 'sum_call_contact', 'iSS' => true, 'jSS' => 'ParentDetailByAtemptContact', 'iC' => array( 
		1=>array('iP' =>17, 'iD'=>1, 'iL' => '1stcall', 'cases' => 'total_1stcall_contact', 'premi' => 'sum_1stcall_contact','iSS' => true,  'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>2, 'iL' => '2ndcall', 'cases' => 'total_2ndcall_contact', 'premi' => 'sum_2ndcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>3, 'iL' => '3thcall', 'cases' => 'total_3thcall_contact', 'premi' => 'sum_3thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>4, 'iL' => '4thcall', 'cases' => 'total_4thcall_contact', 'premi' => 'sum_4thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>5, 'iL' => '5thcall', 'cases' => 'total_5thcall_contact', 'premi' => 'sum_5thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>6, 'iL' => '6thcall', 'cases' => 'total_6thcall_contact', 'premi' => 'sum_6thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>7, 'iL' => '7thcall', 'cases' => 'total_7thcall_contact', 'premi' => 'sum_7thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>8, 'iL' => '8thcall', 'cases' => 'total_8thcall_contact', 'premi' => 'sum_8thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>9, 'iL' => '9thcall', 'cases' => 'total_9thcall_contact', 'premi' => 'sum_9thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact'),
		   array('iP' =>17, 'iD'=>10, 'iL' => '10thcall and more','cases' => 'total_10thcall_contact', 'premi' => 'total_10thcall_contact', 'iSS' => true, 'jSS' => 'ChildDetailByAtemptContact')
		) ) );
		
//tree level data view 

		
// request data to class
 
}

/* 
 * @ def : ambi call reason ID by category Code  
 * @ berdasarkan interval bulan yang di maksud 
 */

 
public function setConfig()
{
if( class_exists('M_CallOutboundReport') )
{
	$this->reference = array();
	$this->config = array();
	
	// set for model 
	
	$this->load->model('M_CallOutboundReport');
	$this->config= $this->M_CallOutboundReport->config;
	foreach( $this->M_CallOutboundReport->reference['major_dc'] as $key => $values )
	{
		$this->reference[$key] = $values;
	}
}
}


/* 
 * @ def : ambi call reason ID by category Code  
 * @ berdasarkan interval bulan yang di maksud 
 */
public function setDisplayRequest()
{
	$date = array('start_date', 'end_date');
	foreach( $this->URI->_get_all_request() as $field => $values )
	{
		if(!in_array($field, $date)) 
		{
			$this->EUI_Session->deleted_session($field);
			$this->$field = trim($values);
			$this->EUI_Session->_set_session($field,trim($values));
		}
	}
}



/* 
 * @ def : ambi call reason ID by category Code  
 * @ berdasarkan interval bulan yang di maksud 
 */
 
protected function CallReasonIdByCategoryCode( $code = NULL )
{
 static $config = array();
	
  if(!is_null($code) 
	AND is_array($code) )
  {
	 $this->db->select('a.CallReasonId');
	 $this->db->from('t_lk_account_status a ');
	 $this->db->join('t_lk_customer_status b ',' a.CallReasonCategoryId=b.CallReasonCategoryId','LEFT');
	 $this->db->where_in('b.CallReasonCategoryCode', $code);
	 foreach( $this->db->get()->result_assoc() as $rows )
	 {	
		$config[$rows['CallReasonId']] = $rows['CallReasonId'];  
	 }	
  }
  
  return $config;
  
}

/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 

public function getConfig() {
	return $this->_level1;
}
/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function getDisplayData()
{
   static $result = array();
   
/* ########################### Hitung total policy ############################# */ 

   $this->setDisplayRequest();
   $this->setConfig();
   
/* ########################### set config filter ############################# */

// major dc 

 if( !is_null($this->major_dc) )
 {
	$MajorDC = $this->reference[$this->major_dc];
 }
 
// major produk get 
 
  if( !is_null($this->major_produk) )
  {
	$MajorProduct= strtoupper($this->major_produk);
 }
 
 
  
 /* ########################### Hitung total policy ############################# */

	$this->db->select("
		COUNT(a.PolicyNumber) as  total_policy,  
		SUM(IF(a.UpdateDateTs IS NOT NULL, 1,0)) as total_touched,
		SUM(IF(a.UpdateDateTs IS NULL, 1,0)) as total_untouched,
		SUM(a.PolicyPremi) as sum_policy,
		SUM(IF(a.UpdateDateTs IS NOT NULL, a.PolicyPremi,0)) as sum_touched,
		SUM(IF(a.UpdateDateTs IS NULL, a.PolicyPremi,0)) as sum_untouched,
		DATE_FORMAT(a.PolicyUploadDate, '%Y-%m') as bln", 
		FALSE);
		
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join('t_gn_debitur b ','a.EnigmaId=b.CustomerNumber','LEFT');
	$this->db->join('t_gn_cif_customer c ',' b.CustomerNumber=c.EnigmaId','LEFT');
	$this->db->join('t_gn_bucket_customers d ',' c.BucketId=d.BucketId','LEFT');
	$this->db->join('t_gn_campaign e ',' b.CampaignId=e.CampaignId','LEFT');
	$this->db->join('t_gn_campaign_project f ',' f.CampaignId=e.CampaignId','LEFT');
	$this->db->where_in('f.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->group_by('bln');
	$this->db->having("bln IS NOT NULL");
	
// Major DC	

	if( !is_null($MajorDC) AND (COUNT($MajorDC) >0)){
		$this->db->where_in('d.DC_Code',$MajorDC);
	}
	
// Major Product 

	if(!is_null($MajorProduct)) 
	{
		if($MajorProduct=='UL' ) 
			$this->db->like("a.PolicyMajorProduct", $MajorProduct);
		
		if($MajorProduct=='NON' ) 
			$this->db->like("a.PolicyMajorProduct",$MajorProduct);
	}
	
/** nama produk **/

	if( !is_null($this->name_produk) 
		AND (trim($this->name_produk)!='') )
	{
		$this->db->where("a.PolicyProductName", trim($this->name_produk));
	}	
	
/** created_by **/
	if( !is_null($this->created_by) 
		AND (trim($this->created_by)!=''))
	{
		$this->db->where("a.CreateByUserId", trim($this->created_by));
	}
	
/** second_payment***/

	if( !is_null($this->second_payment) 
		AND (trim($this->second_payment)!=''))
	{
		if( $this->second_payment=='yes'){
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		if( $this->second_payment=='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
/** nama DC **/
	
	if( !is_null($this->nama_dc) 
		AND (trim($this->nama_dc)!='') 
		AND (trim($this->nama_dc)!='all') )
	{
		$this->db->like("d.Nama_Dc",trim($this->nama_dc));
	}
	
/** category **/

	if( !is_null($this->kategory) 
		AND (trim($this->kategory)!='') 
		AND (trim($this->kategory)!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->kategory));
	}
	
/** nama_campaign **/

	if( !is_null($this->nama_campaign) 
		AND (trim($this->nama_campaign)!='') 
		AND (trim($this->nama_campaign)!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->nama_campaign));
	}
	
/** mode_of_payment **/
	
	if( !is_null($this->mode_of_payment) 
		AND (trim($this->mode_of_payment)!='') 
		AND (trim($this->mode_of_payment)!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->mode_of_payment));
	}
	
 //echo $this->db->_get_var_dump();
	
// result data 
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		foreach( $rows as $field => $totals ) {
			$result[$field][$rows['bln']]+= $totals;
		}
	}
	
/* ########################### summary data per call category  ############################# */

	$this->db->select(" 
		    SUM(IF(f.CallReasonCategoryCode IN('B001'),1, 0)) as total_dead_tone,
		    SUM(IF(f.CallReasonCategoryCode IN('B002'),1, 0)) as total_promise_to_pay,
		    SUM(IF(f.CallReasonCategoryCode IN('B003'),1, 0)) as total_cust_not_promise_to_pay,
		    SUM(IF(f.CallReasonCategoryCode IN('B004'),1, 0)) as total_notcontacted,
			SUM(IF(f.CallReasonCategoryCode IN('B002','B003'),1, 0)) as total_contacted,
			SUM(IF(f.CallReasonCategoryCode IN('B005'),1, 0)) as total_return_list,
		    SUM(IF((f.CallReasonCategoryCode IN('B004') AND a.CallComplete IN(1) ),1, 0)) as total_completed,
		    SUM(IF((f.CallReasonCategoryCode IN('B004') AND a.CallComplete IN(0) ),1, 0)) as total_not_completed,
		    SUM(IF(f.CallReasonCategoryCode IN('B001'),a.PolicyPremi, 0)) as sum_dead_tone,
		    SUM(IF(f.CallReasonCategoryCode IN('B002'),a.PolicyPremi, 0)) as sum_promise_to_pay,
		    SUM(IF(f.CallReasonCategoryCode IN('B003'),a.PolicyPremi, 0)) as sum_cust_not_promise_to_pay,
		    SUM(IF(f.CallReasonCategoryCode IN('B004'),a.PolicyPremi, 0)) as sum_notcontacted,
		    SUM(IF(f.CallReasonCategoryCode IN('B002','B003'),a.PolicyPremi, 0)) as sum_contacted,
		    SUM(IF(f.CallReasonCategoryCode IN('B005'),a.PolicyPremi, 0)) as sum_return_list,
		    SUM(IF((f.CallReasonCategoryCode IN('B004') AND a.CallComplete IN(1) ),a.PolicyPremi, 0)) as sum_completed,
		    SUM(IF((f.CallReasonCategoryCode IN('B004') AND a.CallComplete IN(0) ),a.PolicyPremi, 0)) as sum_not_completed,
			DATE_FORMAT(a.PolicyUpdatedTs, '%Y-%m') as bln", FALSE);
			
	$this->db->from("t_gn_policy_detail a ");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c "," a.EnigmaId=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d "," c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_lk_account_status e "," a.CallReasonId = e.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status f "," e.CallReasonCategoryId=f.CallReasonCategoryId","LEFT");
	$this->db->join("t_gn_campaign g "," b.CampaignId = g.CampaignId","LEFT");
	$this->db->join("t_gn_campaign_project h "," g.CampaignId=h.CampaignId","LEFT");
	$this->db->where_in('e.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->where_in('f.CallReasonProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->db->where_in('h.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
  /** start here ***/
	
	if( ($this->EUI_Session->_have_get_session('start_date')) 
		AND ($this->EUI_Session->_have_get_session('end_date')) )
	{
	   $this->db->where("a.PolicyUpdatedTs>='". $this->EUI_Session->_get_session('start_date') ."'", "", FALSE);
	   $this->db->where("a.PolicyUpdatedTs<='". $this->EUI_Session->_get_session('end_date') ."'", "", FALSE);
	}
	
  /** Major DC **/
	
	if( !is_null($MajorDC) AND (COUNT($MajorDC) >0)){
		$this->db->where_in('d.DC_Code',$MajorDC);
	}
	
// Major Product 

	if(!is_null($MajorProduct)) 
	{
		if($MajorProduct=='UL' ) 
			$this->db->like("a.PolicyMajorProduct", $MajorProduct);
		
		if($MajorProduct=='NON' ) 
			$this->db->like("a.PolicyMajorProduct",$MajorProduct);
	}
	
/** nama produk **/

	if( !is_null($this->name_produk) 
		AND (trim($this->name_produk)!='') )
	{
		$this->db->where("a.PolicyProductName", trim($this->name_produk));
	}	
	
/** created_by **/
	if( !is_null($this->created_by) 
		AND (trim($this->created_by)!=''))
	{
		$this->db->where("a.CreateByUserId", trim($this->created_by));
	}
	
/** second_payment***/

	if( !is_null($this->second_payment) 
		AND (trim($this->second_payment)!=''))
	{
		if( $this->second_payment=='yes'){
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		if( $this->second_payment=='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
/** nama DC **/
	
	if( !is_null($this->nama_dc) 
		AND (trim($this->nama_dc)!='') 
		AND (trim($this->nama_dc)!='all') )
	{
		$this->db->like("d.Nama_Dc",trim($this->nama_dc));
	}
	
/** category **/

	if( !is_null($this->kategory) 
		AND (trim($this->kategory)!='') 
		AND (trim($this->kategory)!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->kategory));
	}
	
/** nama_campaign **/

	if( !is_null($this->nama_campaign) 
		AND (trim($this->nama_campaign)!='') 
		AND (trim($this->nama_campaign)!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->nama_campaign));
	}
	
/** mode_of_payment **/
	
	if( !is_null($this->mode_of_payment) 
		AND (trim($this->mode_of_payment)!='') 
		AND (trim($this->mode_of_payment)!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->mode_of_payment));
	}
	
	
	
	$this->db->group_by('bln');
	$this->db->having("bln IS NOT NULL");
	
	foreach( $this->db->get() -> result_assoc() as $rows )
	{
		foreach( $rows as $field => $totals ) {
			$result[$field][$rows['bln']]+= $totals;
		}
	}
	
/* ########################### summary data per call status  ############################# */
	
	$this->db->select("
		 SUM(IF(g.CallReasonCode IN('21304'), 1, 0)) AS total_customer_request, 
		 SUM(IF(g.CallReasonCode IN('21303'), 1, 0)) AS total_policy_status_surrender, 
		 SUM(IF(g.CallReasonCode IN('21302'), 1, 0)) AS total_customer_passed_away, 
		 SUM(IF(g.CallReasonCode IN('21301'), 1, 0)) AS total_grace_periode_ended, 
		 SUM(IF(g.CallReasonCode IN('21107'), 1, 0)) AS total_fax_number, 
		 SUM(IF(g.CallReasonCode IN('21106'), 1, 0)) AS total_call_back_later, 
		 SUM(IF(g.CallReasonCode IN('21105'), 1, 0)) AS total_cust_is_busy, 
		 SUM(IF(g.CallReasonCode IN('21104'), 1, 0)) AS total_cust_are_not_in_place, 
		 SUM(IF(g.CallReasonCode IN('21103'), 1, 0)) AS total_mail_box, 
		 SUM(IF(g.CallReasonCode IN('21102'), 1, 0)) AS total_no_answer, 
		 SUM(IF(g.CallReasonCode IN('21101'), 1, 0)) AS total_busy_tone, 
		 SUM(IF(g.CallReasonCode IN('21011'), 1, 0)) AS total_want_to_surrender, 
		 SUM(IF(g.CallReasonCode IN('21010'), 1, 0)) AS total_pay_only_certain_years, 
		 SUM(IF(g.CallReasonCode IN('21009'), 1, 0)) AS total_no_reason, 
		 SUM(IF(g.CallReasonCode IN('21008'), 1, 0)) AS total_buy_new_policy, 
		 SUM(IF(g.CallReasonCode IN('21007'), 1, 0)) AS total_moving_to_overseas, 
		 SUM(IF(g.CallReasonCode IN('21006'), 1, 0)) AS total_others, 
		 SUM(IF(g.CallReasonCode IN('21005'), 1, 0)) AS total_dissapoint, 
		 SUM(IF(g.CallReasonCode IN('21004'), 1, 0)) AS total_miss_representative, 
		 SUM(IF(g.CallReasonCode IN('21003'), 1, 0)) AS total_automatic_premium_holiday, 
		 SUM(IF(g.CallReasonCode IN('21002'), 1, 0)) AS total_unsuitable_product, 
		 SUM(IF(g.CallReasonCode IN('21001'), 1, 0)) AS total_financial_problem, 
		 SUM(IF(g.CallReasonCode IN('29002'), 1, 0)) AS total_already_paid_on_system, 
		 SUM(IF(g.CallReasonCode IN('29001'), 1, 0)) AS total_customer_promise_to_pay, 
		 SUM(IF(g.CallReasonCode IN('28004'), 1, 0)) AS total_tulalit, 
		 SUM(IF(g.CallReasonCode IN('28003'), 1, 0)) AS total_dead_tone, 
		 SUM(IF(g.CallReasonCode IN('28002'), 1, 0)) AS total_wrong_number, 
		 SUM(IF(g.CallReasonCode IN('28001'), 1, 0)) AS total_invalid_number,
		 SUM(IF(g.CallReasonCode IN('21304'), a.PolicyPremi, 0)) AS sum_customer_request, 
		 SUM(IF(g.CallReasonCode IN('21303'), a.PolicyPremi, 0)) AS sum_policy_status_surrender, 
		 SUM(IF(g.CallReasonCode IN('21302'), a.PolicyPremi, 0)) AS sum_customer_passed_away, 
		 SUM(IF(g.CallReasonCode IN('21301'), a.PolicyPremi, 0)) AS sum_grace_periode_ended, 
		 SUM(IF(g.CallReasonCode IN('21107'), a.PolicyPremi, 0)) AS sum_fax_number, 
		 SUM(IF(g.CallReasonCode IN('21106'), a.PolicyPremi, 0)) AS sum_call_back_later, 
		 SUM(IF(g.CallReasonCode IN('21105'), a.PolicyPremi, 0)) AS sum_cust_is_busy, 
		 SUM(IF(g.CallReasonCode IN('21104'), a.PolicyPremi, 0)) AS sum_cust_are_not_in_place, 
		 SUM(IF(g.CallReasonCode IN('21103'), a.PolicyPremi, 0)) AS sum_mail_box, 
		 SUM(IF(g.CallReasonCode IN('21102'), a.PolicyPremi, 0)) AS sum_no_answer, 
		 SUM(IF(g.CallReasonCode IN('21101'), a.PolicyPremi, 0)) AS sum_busy_tone, 
		 SUM(IF(g.CallReasonCode IN('21011'), a.PolicyPremi, 0)) AS sum_want_to_surrender, 
		 SUM(IF(g.CallReasonCode IN('21010'), a.PolicyPremi, 0)) AS sum_pay_only_certain_years, 
		 SUM(IF(g.CallReasonCode IN('21009'), a.PolicyPremi, 0)) AS sum_no_reason, 
		 SUM(IF(g.CallReasonCode IN('21008'), a.PolicyPremi, 0)) AS sum_buy_new_policy, 
		 SUM(IF(g.CallReasonCode IN('21007'), a.PolicyPremi, 0)) AS sum_moving_to_overseas, 
		 SUM(IF(g.CallReasonCode IN('21006'), a.PolicyPremi, 0)) AS sum_others, 
		 SUM(IF(g.CallReasonCode IN('21005'), a.PolicyPremi, 0)) AS sum_dissapoint, 
		 SUM(IF(g.CallReasonCode IN('21004'), a.PolicyPremi, 0)) AS sum_miss_representative, 
		 SUM(IF(g.CallReasonCode IN('21003'), a.PolicyPremi, 0)) AS sum_automatic_premium_holiday, 
		 SUM(IF(g.CallReasonCode IN('21002'), a.PolicyPremi, 0)) AS sum_unsuitable_product, 
		 SUM(IF(g.CallReasonCode IN('21001'), a.PolicyPremi, 0)) AS sum_financial_problem, 
		 SUM(IF(g.CallReasonCode IN('29002'), a.PolicyPremi, 0)) AS sum_already_paid_on_system, 
		 SUM(IF(g.CallReasonCode IN('29001'), a.PolicyPremi, 0)) AS sum_customer_promise_to_pay, 
		 SUM(IF(g.CallReasonCode IN('28004'), a.PolicyPremi, 0)) AS sum_tulalit, 
		 SUM(IF(g.CallReasonCode IN('28003'), a.PolicyPremi, 0)) AS sum_dead_tone, 
		 SUM(IF(g.CallReasonCode IN('28002'), a.PolicyPremi, 0)) AS sum_wrong_number, 
		 SUM(IF(g.CallReasonCode IN('28001'), a.PolicyPremi, 0)) AS sum_invalid_number,
		 DATE_FORMAT(a.PolicyUploadDate, '%Y-%m') as bln", FALSE);
		
	$this->db->from("t_gn_policy_detail a ");
	
	$this->db->join("t_gn_debitur b ","a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c ","b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d ","c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_gn_campaign e"," b.CampaignId = e.CampaignId","LEFT");
	$this->db->join("t_gn_campaign_project f"," e.CampaignId=f.CampaignId","LEFT");
	$this->db->join("t_lk_account_status g "," a.CallReasonId= g.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status h ","g.CallReasonCategoryId=h.CallReasonCategoryId","LEFT");
	$this->db->where_in('g.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId') );
	$this->db->where_in('h.CallReasonProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->db->where_in('f.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
	
/** start here ***/
	
	if( ($this->EUI_Session->_have_get_session('start_date')) 
		AND ($this->EUI_Session->_have_get_session('end_date')) )
	{
	   $this->db->where("a.PolicyUpdatedTs>='". $this->EUI_Session->_get_session('start_date') ."'", "", FALSE);
	   $this->db->where("a.PolicyUpdatedTs<='". $this->EUI_Session->_get_session('end_date') ."'", "", FALSE);
	}
	
/** Major DC **/
	
	if( !is_null($MajorDC) AND (COUNT($MajorDC) >0)){
		$this->db->where_in('d.DC_Code',$MajorDC);
	}
	
// Major Product 

	if(!is_null($MajorProduct)) 
	{
		if($MajorProduct=='UL' ) 
			$this->db->like("a.PolicyMajorProduct", $MajorProduct);
		
		if($MajorProduct=='NON' ) 
			$this->db->like("a.PolicyMajorProduct",$MajorProduct);
	}
	
/** nama produk **/

	if( !is_null($this->name_produk) 
		AND (trim($this->name_produk)!='') )
	{
		$this->db->where("a.PolicyProductName", trim($this->name_produk));
	}	
	
/** created_by **/
	if( !is_null($this->created_by) 
		AND (trim($this->created_by)!=''))
	{
		$this->db->where("a.CreateByUserId", trim($this->created_by));
	}
	
/** second_payment***/

	if( !is_null($this->second_payment) 
		AND (trim($this->second_payment)!=''))
	{
		if( $this->second_payment=='yes'){
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		if( $this->second_payment=='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
/** nama DC **/
	
	if( !is_null($this->nama_dc) 
		AND (trim($this->nama_dc)!='') 
		AND (trim($this->nama_dc)!='all') )
	{
		$this->db->like("d.Nama_Dc",trim($this->nama_dc));
	}
	
/** category **/

	if( !is_null($this->kategory) 
		AND (trim($this->kategory)!='') 
		AND (trim($this->kategory)!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->kategory));
	}
	
/** nama_campaign **/

	if( !is_null($this->nama_campaign) 
		AND (trim($this->nama_campaign)!='') 
		AND (trim($this->nama_campaign)!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->nama_campaign));
	}
	
/** mode_of_payment **/
	
	if( !is_null($this->mode_of_payment) 
		AND (trim($this->mode_of_payment)!='') 
		AND (trim($this->mode_of_payment)!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->mode_of_payment));
	}
	
	$this->db->group_by('bln');
	$this->db->having("bln IS NOT NULL");
	
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		foreach( $rows as $field => $totals ){
			$result[$field][$rows['bln']]+= $totals;
		}
	}
	
/* ########################### summary data atempt  ############################# */

	$this->db->select("
		SUM(IF((b.CallAttempt=1),1,0)) as total_1stcall_atempt,
		SUM(IF((b.CallAttempt=2),1,0)) as total_2ndcall_atempt,
		SUM(IF((b.CallAttempt=3),1,0)) as total_3thcall_atempt,
		SUM(IF((b.CallAttempt=4),1,0)) as total_4thcall_atempt,
		SUM(IF((b.CallAttempt=5),1,0)) as total_5thcall_atempt,
		SUM(IF((b.CallAttempt=6),1,0)) as total_6thcall_atempt,
		SUM(IF((b.CallAttempt=7),1,0)) as total_7thcall_atempt,
		SUM(IF((b.CallAttempt=8),1,0)) as total_8thcall_atempt,
		SUM(IF((b.CallAttempt=9),1,0)) as total_9thcall_atempt,
		SUM(IF((b.CallAttempt>= 10),1,0)) as total_10thcall_atempt,
		SUM(IF((b.CallAttempt=1),a.PolicyPremi,0)) as sum_1stcall_atempt,
		SUM(IF((b.CallAttempt=2),a.PolicyPremi,0)) as sum_2ndcall_atempt,
		SUM(IF((b.CallAttempt=3),a.PolicyPremi,0)) as sum_3thcall_atempt,
		SUM(IF((b.CallAttempt=4),a.PolicyPremi,0)) as sum_4thcall_atempt,
		SUM(IF((b.CallAttempt=5),a.PolicyPremi,0)) as sum_5thcall_atempt,
		SUM(IF((b.CallAttempt=6),a.PolicyPremi,0)) as sum_6thcall_atempt,
		SUM(IF((b.CallAttempt=7),a.PolicyPremi,0)) as sum_7thcall_atempt,
		SUM(IF((b.CallAttempt=8),a.PolicyPremi,0)) as sum_8thcall_atempt,
		SUM(IF((b.CallAttempt=9),a.PolicyPremi,0)) as sum_9thcall_atempt,
		SUM(IF((b.CallAttempt>= 10),a.PolicyPremi,0)) as sum_10thcall_atempt,
		SUM(IF((b.CallAttempt=1 AND h.CallReasonCategoryCode IN('B002','B003') ),1,0)) as total_1stcall_contact,
		SUM(IF((b.CallAttempt=2 AND h.CallReasonCategoryCode IN('B002','B003')) ,1,0)) as total_2thcall_contact,
		SUM(IF((b.CallAttempt=3 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_3thcall_contact,
		SUM(IF((b.CallAttempt=4 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_4thcall_contact,
		SUM(IF((b.CallAttempt=5 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_5thcall_contact,
		SUM(IF((b.CallAttempt=6 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_6thcall_contact,
		SUM(IF((b.CallAttempt=7 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_7thcall_contact,
		SUM(IF((b.CallAttempt=8 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_8thcall_contact,
		SUM(IF((b.CallAttempt=9 AND h.CallReasonCategoryCode IN('B002','B003')),1,0)) as total_9thcall_contact,
		SUM(IF((b.CallAttempt>=10 AND h.CallReasonCategoryCode IN('B002','B003') ),1,0)) as total_10thcall_contact,
		SUM(IF((b.CallAttempt=1 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_1stcall_contact,
		SUM(IF((b.CallAttempt=2 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_2ndcall_contact,
		SUM(IF((b.CallAttempt=3 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_3thcall_contact,
		SUM(IF((b.CallAttempt=4 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_4thcall_contact,
		SUM(IF((b.CallAttempt=5 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_5thcall_contact,
		SUM(IF((b.CallAttempt=6 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_6thcall_contact,
		SUM(IF((b.CallAttempt=7 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_7thcall_contact,
		SUM(IF((b.CallAttempt=8 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_8thcall_contact,
		SUM(IF((b.CallAttempt=9 AND h.CallReasonCategoryCode IN('B002','B003')),a.PolicyPremi,0)) as sum_9thcall_contact,
		SUM(IF((b.CallAttempt>= 10 AND h.CallReasonCategoryCode IN('B002','B003') ),a.PolicyPremi,0)) as sum_10thcall_contact,
		DATE_FORMAT(a.PolicyUploadDate, '%Y-%m') as bln", FALSE);
		
		$this->db->from("t_gn_policy_detail a"); 
		
		$this->db->join("t_gn_debitur b","a.EnigmaId=b.CustomerNumber","LEFT");
		$this->db->join("t_gn_cif_customer c","b.CustomerNumber=c.EnigmaId","LEFT");
		$this->db->join("t_gn_bucket_customers d","c.BucketId=d.BucketId","LEFT");
		$this->db->join("t_gn_campaign e","b.CampaignId=e.CampaignId","LEFT");
		$this->db->join("t_gn_campaign_project f","e.CampaignId=f.CampaignId","LEFT");
		$this->db->join("t_lk_account_status g","a.CallReasonId=g.CallReasonId","LEFT");
		$this->db->join("t_lk_customer_status h","g.CallReasonCategoryId=h.CallReasonCategoryId","LEFT");
		
		$this->db->where_in('g.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId') );
		$this->db->where_in('h.CallReasonProjectId',$this->EUI_Session->_get_session('ProjectId') );
		$this->db->where_in('f.ProjectId', $this->EUI_Session->_get_session('ProjectId') );
		
/** start here ***/
		
	if( ($this->EUI_Session->_have_get_session('start_date')) 
		AND ($this->EUI_Session->_have_get_session('end_date')) )
	{
		$this->db->where("a.PolicyUpdatedTs>='". $this->EUI_Session->_get_session('start_date') ."'", "", FALSE);
		$this->db->where("a.PolicyUpdatedTs<='". $this->EUI_Session->_get_session('end_date') ."'", "", FALSE);
	}
		
/** Major DC **/
	
	if( !is_null($MajorDC) AND (COUNT($MajorDC) >0)){
		$this->db->where_in('d.DC_Code',$MajorDC);
	}
		
// Major Product 

	if(!is_null($MajorProduct)) 
	{
		if($MajorProduct=='UL' ) 
			$this->db->like("a.PolicyMajorProduct", $MajorProduct);
		
		if($MajorProduct=='NON' ) 
			$this->db->like("a.PolicyMajorProduct",$MajorProduct);
	}
	
/** nama produk **/

	if( !is_null($this->name_produk) 
		AND (trim($this->name_produk)!='') )
	{
		$this->db->where("a.PolicyProductName", trim($this->name_produk));
	}	
	
/** created_by **/
	if( !is_null($this->created_by) 
		AND (trim($this->created_by)!=''))
	{
		$this->db->where("a.CreateByUserId", trim($this->created_by));
	}
	
/** second_payment***/

	if( !is_null($this->second_payment) 
		AND (trim($this->second_payment)!=''))
	{
		if( $this->second_payment=='yes'){
			$this->db->where("a.PolicyRCD = a.PolicyLastInstallment", "", FALSE);
		}
		if( $this->second_payment=='no'){
			$this->db->where("a.PolicyRCD < a.PolicyLastInstallment", "", FALSE);		
		}
	}
/** nama DC **/
	
	if( !is_null($this->nama_dc) 
		AND (trim($this->nama_dc)!='') 
		AND (trim($this->nama_dc)!='all') )
	{
		$this->db->like("d.Nama_Dc",trim($this->nama_dc));
	}
	
/** category **/

	if( !is_null($this->kategory) 
		AND (trim($this->kategory)!='') 
		AND (trim($this->kategory)!='all') )
	{
		$this->db->where("a.PolicyProductCategory",trim($this->kategory));
	}
	
/** nama_campaign **/

	if( !is_null($this->nama_campaign) 
		AND (trim($this->nama_campaign)!='') 
		AND (trim($this->nama_campaign)!='all') )
	{
		$this->db->where("a.PolicyCampaign_Master_Code",trim($this->nama_campaign));
	}
	
/** mode_of_payment **/
	
	if( !is_null($this->mode_of_payment) 
		AND (trim($this->mode_of_payment)!='') 
		AND (trim($this->mode_of_payment)!='all') )
	{
		$this->db->where("a.PaymentMode",trim($this->mode_of_payment));
	}
	
	$this->db->group_by('bln');
	$this->db->having("bln IS NOT NULL");
		
	/** result data ***/
	  
		foreach( $this->db->get()->result_assoc() as $rows )
		{
			foreach( $rows as $field => $totals ) {
				$result[$field][$rows['bln']]+= $totals;
			}
			
		// step -1 : all atempt without contacted reason 
		
			$result['total_call_atempt'][$rows['bln']] += $rows['total_1stcall_atempt']+$rows['total_2ndcall_atempt'];
			$result['total_call_atempt'][$rows['bln']] += $rows['total_3thcall_atempt']+$rows['total_4thcall_atempt'];
			$result['total_call_atempt'][$rows['bln']] += $rows['total_5thcall_atempt']+$rows['total_6thcall_atempt'];
			$result['total_call_atempt'][$rows['bln']] += $rows['total_7thcall_atempt']+$rows['total_8thcall_atempt']; 
			$result['total_call_atempt'][$rows['bln']] += $rows['total_9thcall_atempt']+$rows['total_10thcall_atempt'];
			
			/** summary premi **/
			
			$result['sum_call_atempt'][$rows['bln']] += $rows['sum_1stcall_atempt']+$rows['sum_2ndcall_atempt'];
			$result['sum_call_atempt'][$rows['bln']] += $rows['sum_3thcall_atempt']+$rows['sum_4thcall_atempt'];
			$result['sum_call_atempt'][$rows['bln']] += $rows['sum_5thcall_atempt']+$rows['sum_6thcall_atempt'];
			$result['sum_call_atempt'][$rows['bln']] += $rows['sum_7thcall_atempt']+$rows['sum_8thcall_atempt']; 
			$result['sum_call_atempt'][$rows['bln']] += $rows['sum_9thcall_atempt']+$rows['sum_10thcall_atempt'];
			
		// step -2 : all atempt without contacted reason 
		
			$result['total_call_contact'][$rows['bln']] += $rows['total_1stcall_contact']+$rows['total_2ndcall_contact'];
			$result['total_call_contact'][$rows['bln']] += $rows['total_3thcall_contact']+$rows['total_4thcall_contact'];
			$result['total_call_contact'][$rows['bln']] += $rows['total_5thcall_contact']+$rows['total_6thcall_contact'];
			$result['total_call_contact'][$rows['bln']] += $rows['total_7thcall_contact']+$rows['total_8thcall_contact']; 
			$result['total_call_contact'][$rows['bln']] += $rows['total_9thcall_contact']+$rows['total_10thcall_contact'];
			/** summary premi **/
			$result['sum_call_contact'][$rows['bln']] += $rows['sum_1stcall_contact']+$rows['sum_2ndcall_contact'];
			$result['sum_call_contact'][$rows['bln']] += $rows['sum_3thcall_contact']+$rows['sum_4thcall_contact'];
			$result['sum_call_contact'][$rows['bln']] += $rows['sum_5thcall_contact']+$rows['sum_6thcall_contact'];
			$result['sum_call_contact'][$rows['bln']] += $rows['sum_7thcall_contact']+$rows['sum_8thcall_contact']; 
			$result['sum_call_contact'][$rows['bln']] += $rows['sum_9thcall_contact']+$rows['sum_10thcall_contact'];
		}	
	return $result;
}

/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function IntervalDisplay()
{

// get date on post 
 $start_date = ( !$this->URI->_get_post('start_date') ? date('Y-m') : date('Y-m', strtotime(_getDateEnglish($this->URI->_get_post('start_date')))) );
 $end_date   = ( !$this->URI->_get_post('end_date') ? date('Y-m') : date('Y-m', strtotime(_getDateEnglish($this->URI->_get_post('end_date')))) );
 
// while looping here ---->   
 $headers = array();
 
 while( true ) 
 {
	$estart_date = $start_date;
	$dates = explode('-', $estart_date); 
	$bln = $dates[0] .'-'. $dates[1];
	
	$headers[$bln]['h0'] = NameMonth((INT)$dates[1]) ." ". $dates[0];
	$headers[$bln]['h1']   = 'Casess';
	$headers[$bln]['h2']   = 'Premi';
	$headers[$bln]['h3']   = 'Detail 1';
	$headers[$bln]['h4']   = 'Detail 2';
	
	if ( $start_date == $end_date ) break;
		$start_date = NextMonth($start_date);
 }
 
 return $headers;
 
}


/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
 
public function display( $post = null )
{
 static $config = array();
 
 // get config  
 
 $config =& $this->getConfig();
 
// get data by month 
 
 $tots_initiated  =& $this->getDisplayData();
 $IntervalDisplay =& $this->IntervalDisplay();
 
// conmpile 
 
 $_html = '';	
 foreach( $config as $iP => $level_1 )
 {
	if( !is_array($level_1['iC']) )
	{
		// dtl 1
		$_html .=" <tr data-tt-id='$iP' class='onselect' ><td nowrap><span class='file'>". $level_1['iL'] ."</span></td> ";
			foreach( $IntervalDisplay as $tgl => $labels )
			{
				$_html .="<td class='right'>". ($tots_initiated[$level_1['cases']] ? ( $tots_initiated[$level_1['cases']][$tgl] ? $tots_initiated[$level_1['cases']][$tgl] : 0 ) : 0 ) ."&nbsp;</td>";
				$_html .="<td class='right'>". ($tots_initiated[$level_1['premi']] ? _getCurrency( $tots_initiated[$level_1['premi']][$tgl] ) : 0 ) ."&nbsp;</td>";
				$_html .="<td class='center'><a href=\"javascript:void(0);\" onclick='". $this->_setRetrival(array('config' => $level_1, 'opt' => 0, 'tgl' => $tgl))."'>Detail</a></td>";
				$_html .="<td class='center' style='border-right:1px solid #8e94e2;'></td>";
			}		
				
			$_html .="</tr> \n";
			
	}
	else
	{
		// dtl 1
		$_html .=" <tr data-tt-id='$iP' class='onselect'>
						<td  nowrap><span class='folder'>". $level_1['iL'] ."</span></td>";
						
			foreach( $IntervalDisplay as $tgl => $labels )
			{			
			  $_html .=" <td class='right'>". ($tots_initiated[$level_1['cases']] ? ( $tots_initiated[$level_1['cases']][$tgl] ? $tots_initiated[$level_1['cases']][$tgl] : 0 ) : 0 ) ."&nbsp;</td>
						 <td class='right'>". ($tots_initiated[$level_1['premi']] ? _getCurrency( $tots_initiated[$level_1['premi']][$tgl] ) : 0 ) ."&nbsp;</td>
						 <td class='center'><a href=\"javascript:void(0);\" onclick='". $this->_setRetrival(array('config'=>$level_1, 'opt'=>0, 'tgl'=> $tgl))."'>Detail</a></td>
						 <td class='center' style='border-right:1px solid #8e94e2;'>&nbsp;</td> ";
			}			
			
			$_html .="</tr> \n";
		
		foreach( $level_1['iC'] as $iC => $level_2 )
		{
			if( is_array($level_2['iC']) )
			{
				// dtl 1
				$_html .=" <tr data-tt-id='$iP-$iC' data-tt-parent-id='$iP' class='onselect'>
								<td nowrap><span class='folder'>". $level_2['iL'] ."</span></td> ";
				foreach( $IntervalDisplay as $tgl => $labels )
				{					
				  $_html .=" <td class='right'>". ($tots_initiated[$level_2['cases']] ? ( $tots_initiated[$level_2['cases']][$tgl] ? $tots_initiated[$level_2['cases']][$tgl] : 0 ) : 0 ) ."&nbsp;</td>
							 <td class='right'>". ($tots_initiated[$level_2['premi']] ? _getCurrency( $tots_initiated[$level_2['premi']][$tgl] ) : 0 ) ."&nbsp;</td>
							 <td class='center'><a href=\"javascript:void(0);\" onclick='". $this->_setRetrival(array('config'=>$level_2, 'opt'=>0, 'tgl'=> $tgl))."'>Detail</a></td>
							 <td class='center' style='border-right:1px solid #8e94e2;'></td> ";
				}			 
				
				 $_html .=" </tr>\n";
				 
				// dtl 2  
				foreach( $level_2['iC'] as $kh => $level_3 )
				{
					$_html .=" <tr data-tt-id='$iP-$iC-$kh' data-tt-parent-id='$iP-$iC' class='onselect'>
									<td nowrap><span class='file'>". $level_3['iL'] ."</span></td> ";
					foreach( $IntervalDisplay as $tgl => $labels )
					{				
					   $_html .=" <td class='right'>". ($tots_initiated[$level_3['cases']] ? ( $tots_initiated[$level_3['cases']][$tgl] ? $tots_initiated[$level_3['cases']][$tgl] : 0 ) : 0 ) ."&nbsp;</td>
								 <td class='right'>". ($tots_initiated[$level_3['premi']] ? _getCurrency( $tots_initiated[$level_3['premi']][$tgl] ) : 0 ) ."&nbsp;</td>
								 <td class='center'></td>
								 <td class='center' style='border-right:1px solid #8e94e2;'>
								 <a href=\"javascript:void(0);\" onclick='". $this->_setRetrival(array('config'=>$level_3, 'opt'=>1, 'tgl'=> $tgl)). "'> Detail</a></td>";
					}
					 
					 $_html .="</tr>\n";
				}
			}
			else
			{
				// dtl 2 
				$_html .=" <tr data-tt-id='$iP-$iC' data-tt-parent-id='$iP' class='onselect'>
						   <td nowrap><span class='file'>". $level_2['iL'] ."</span></td> ";
						   
				foreach( $IntervalDisplay as $tgl => $labels )
				{			  
					$_html .=" <td class='right'>". ($tots_initiated[$level_2['cases']] ? ( $tots_initiated[$level_2['cases']][$tgl] ? $tots_initiated[$level_2['cases']][$tgl] : 0 ) : 0 ) ."&nbsp;</td>
							   <td class='right'>". ($tots_initiated[$level_2['premi']] ? _getCurrency( $tots_initiated[$level_2['premi']][$tgl] ) : 0 ) ."&nbsp;</td>
							   <td class='center'></td>
							   <td class='center' style='border-right:1px solid #8e94e2;'>
							   <a href=\"javascript:void(0);\" onclick='". $this->_setRetrival(array('config'=>$level_2, 'opt' =>1, 'tgl'=> $tgl))."'> Detail</a></td> ";
				}				
				
				$_html .=" </tr>\n";
			}
		}	
	}
 }	
	
	return $_html;		

}


/**
 * @ def : retrival function js 
 *
 */
 
protected function _setRetrival( $config = NULL)
{
	if( !is_null($config) )
	{
	
	 if(($config['opt'] == 0)) 
	 {
		$JSON = json_encode($config);
		
		if( isset($config['config']['iSS']) 
			AND is_bool($config['config']['iSS']) ==TRUE  
			AND $config['config']['iSS']==TRUE)
		{
			return "parent.Ext.DetailByParents({$JSON}).{$config['config']['jSS']}();";		
		}
		else{
			return "parent.Ext.DetailByParents({$JSON}).{$config['config']['jSS']}();";
		}
	 }

	 if(($config['opt'] == 1) ) 
	 {
		$JSON = json_encode($config);
		
		if( isset($config['config']['iSS']) 
			AND is_bool($config['config']['iSS']) ==TRUE  
			AND $config['config']['iSS']==TRUE)
		{
			return "parent.Ext.DetailByChild({$JSON}).{$config['config']['jSS']}();";	
		}
		else{
			return "parent.Ext.DetailByChild({$JSON}).{$config['config']['jSS']}();";
		}
	 }
	}
} 

 
	
}
?>