<?php 

/*
 * @ param : class under call tracking report 
 * @ nmae  : R_CallTrackGroupRecsource 
 * ----------------------------------------------------
 * @ param : - 
 * @ param : 
 */
 
define('STATUS_NOT_CONNECTION',7);
define('STATUS_INTERESTED',1);
define('STATUS_NOT_INTERESTED',2);
define('STATUS_NO_CONTACT',5);
define('STATUS_FOLLOW_UP',3);
define('STATUS_NOT_QUALIFIED',4);

/* ---------------------------------------------- */
 
class R_CallTrackGroupRecsource extends EUI_Report 
{

 protected $CampaignId 	= null;
 protected $AgentId 	= null;
 protected $SpvId 		= null;
 protected $AtmId 		= null;
 protected $StartDate 	= null;
 protected $EndDate 	= null;
 protected $Interval 	= null;
 protected $BukcetSourceId = null;
 

/* @ param : aksesor **/

 public function R_CallTrackGroupRecsource() 
{

/* set Interval filter  **/
	if( $this->URI->_get_post('interval') 
		AND !is_null($this->URI->_get_post('interval')) )
	{
		$this->Interval = $this->URI->_get_post('interval');
	}
	
/* set BukcetSourceId **/
	if( $this->URI->_get_array_post('RecsourceId') 
		AND !is_null($this->URI->_get_array_post('RecsourceId')) )
	{
		$this->BukcetSourceId = $this -> URI->_get_array_post('RecsourceId');
	}
	
/* set campaign ID **/
	
	if( $this->URI->_get_array_post('CampaignId') 
		AND !is_null($this->URI->_get_array_post('CampaignId')) )
	{
		$this->CampaignId = $this -> URI->_get_array_post('CampaignId');
	}
	
/* set ATM ID **/
	
	if( $this->URI->_get_array_post('AtmId') 
		AND !is_null($this->URI->_get_array_post('AtmId')) )
	{
		$this->AtmId = $this -> URI->_get_array_post('AtmId');
	}
	
/* set SPV ID **/	

	if( $this -> URI->_get_array_post('SpvId')  
		AND !is_null($this->URI->_get_array_post('SpvId')) )
	{
		$this -> SpvId = $this -> URI->_get_array_post('SpvId');
	}
	
/* set AGENT ID **/
	
	if( $this -> URI->_get_array_post('AgentId')  
		AND !is_null($this->URI->_get_array_post('AgentId')) )
	{
		$this -> AgentId = $this -> URI->_get_array_post('AgentId');
	}
	
/* set AGENT ID **/
	
	if( $this -> URI->_get_post('start_date')  
		AND !is_null($this->URI->_get_post('start_date')) )
	{
		$this->StartDate = date('Y-m-d', strtotime($this -> URI->_get_post('start_date')));
		$this->EndDate = date('Y-m-d', strtotime($this -> URI->_get_post('end_date')));
		
	}
	
}
	

/* @ param : get interval mode in / hourly/ daily/ summary   **/

public function _getResultReport() 
{
	
    $_conds = array();
	if( !empty($this -> Interval) 
		AND ($this -> Interval=='summary') )
	{
		$_conds = array ( 
			'CALL_CONTENT' => $this -> _getSummaryReport(), 
			'CALL_VIEWER' => 'call_track_summary_by_recsoucre',
			'CATEGORY_STATUS_CALL' => $this ->_getCategoryStatus(),
			'CATEGORY_COUNTER_CALL' => $this ->_getCounterStatus(),
			'CALL_REASON_CATEGORY' => $this -> _getReasonByCategory(),
			'STATUS_NOT_CONNECTION'	=> $this->_getNotConnection(),
			'STATUS_INTERESTED' => $this->_getInterest(),
			'CAMPAIGN_PROJECT' => $this->_getCampaignProject()
		); 	
	}
	
	return $_conds;
}


/* _getCampaignProject **/

public function _getCampaignProject()
{
	$campaign = array();
	
	$this ->db->select('b.CampaignId, b.CampaignName, b.CampaignCode',FALSE);
	$this ->db->from('t_gn_campaign_project a ');
	$this ->db->join('t_gn_campaign b ',' a.CampaignId=b.CampaignId','LEFT');
	$this ->db->where_in('a.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	$this ->db->where('b.CampaignId IS NOT NULL');
	
	foreach( $this ->db->get() ->result_assoc() as $rows ){
		$campaign[$rows['CampaignId']] = $rows['CampaignName'];
	}
	
	return $campaign;

}

/* @ param : get interval mode in / hourly/ daily/ summary   **/

public function _getCategoryStatus()
{

	$_category = array();
	
	$this -> db -> select("a.*");
	$this -> db -> from("t_lk_customer_status a");
	$this -> db -> where("a.CallActivityShow","1");
	$this -> db ->where_in('a.CallReasonProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this -> db -> order_by("a.CallReasonCategoryId", "ASC");
	
	foreach( $this -> db -> get() -> result_assoc() as $rows ) {
		$_category[$rows['CallReasonCategoryId']] = $rows['CallReasonCategoryName'];
	}
	
	return $_category;
}


/* @ param : get interval mode in / hourly/ daily/ summary   **/

public function _getCounterStatus()
{
	$_category = array();
	
	$this ->db->select("count(a.CallReasonId) as jumlah ,a.CallReasonCategoryId ", false);
	$this ->db->from("t_lk_account_status a ");
	$this ->db->where_in('a.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this ->db->group_by("a.CallReasonCategoryId");
	$this ->db->order_by("a.CallReasonCategoryId","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_category[$rows['CallReasonCategoryId']]= $rows['jumlah'];
	}
	
	return $_category;
}


/* @ param : get interval mode in / hourly/ daily/ summary   **/

public function _getReasonByCategory()
{
	$_category = array();
	$this ->db->select("*");
	$this ->db->from("t_lk_account_status a ");
	$this ->db->where("a.CallReasonStatusFlag",1);
	$this ->db->where_in('a.CallWorkProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_category[$rows['CallReasonCategoryId']][$rows['CallReasonId']]= $rows['CallReasonDesc'];
	}
	
	return $_category;
}





/* @ param : get interval mode in / hourly/ daily/ summary   **/

public function CallGroupContacted()
{
	$CallGroupCantacted = array();
	foreach( array( 
		array_keys($this->_getFollowup()), 
		array_keys($this->_getInterest()), 
		array_keys($this->_getNotInterest()), 
		array_keys($this->_getNotQualified())) as $key => $values )
	{
		if( is_array($values) ) foreach($values as $key => $CallReasonId )
		{
			$CallGroupCantacted[$CallReasonId] = $CallReasonId;
		}
	}
	
	// return 
	
	if( is_array($CallGroupCantacted) )
	{
		return array_keys($CallGroupCantacted);
	}
}

/* @ param : get interval mode in / hourly/ daily/ summary   **/

public function CallGroupConnect()
{
	$CallGroupConnect = array();
	foreach( array( 
		array_keys($this->_getFollowup()), 
		array_keys($this->_getInterest()), 
		array_keys($this->_getNotInterest()), 
		array_keys($this->_getNotQualified()),
		array_keys($this->_getNotContact())) as $key => $values )
	{
		if( is_array($values) ) foreach($values as $key => $CallReasonId )
		{
			$CallGroupConnect[$CallReasonId] = $CallReasonId;
		}
	}
	
	// return 
	
	if( is_array($CallGroupConnect) )
	{
		return array_keys($CallGroupConnect);
	}
}

/* @ param : get interval mode in / hourly/ daily/ summary   **/

function _getInterest()
{
	$conds = array();
	
	$this->db->select("a.CallReasonId, a.CallReasonCode ");
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCategoryId",STATUS_INTERESTED );
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->order_by("a.CallReasonCode", "ASC");
	
	foreach( $this->db->get()-> result_assoc() as $rows ) {
		$conds[$rows['CallReasonId']] = $rows['CallReasonCode'];
	}
	
	return $conds;	
}

/* @ param : get interval mode in / hourly/ daily/ summary   **/

function _getNotInterest()
{
	$conds = array();
	
	$this->db->select("a.CallReasonId, a.CallReasonCode ");
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCategoryId",STATUS_NOT_INTERESTED);
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->order_by("a.CallReasonCode", "ASC");
	
	foreach( $this->db->get()-> result_assoc() as $rows ) 
	{
		$conds[$rows['CallReasonId']] = $rows['CallReasonCode'];
	}
	
	return $conds;	
}

/* @ param : get interval mode in / hourly/ daily/ summary   **/

function _getNotQualified()
{
	$conds = array();
	
	$this->db->select("a.CallReasonId, a.CallReasonCode ");
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCategoryId",STATUS_NOT_QUALIFIED);
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->order_by("a.CallReasonCode", "ASC");
	
	foreach( $this->db->get()-> result_assoc() as $rows ) {
		$conds[$rows['CallReasonId']] = $rows['CallReasonCode'];
	}
	
	return $conds;	
}

/* @ param : get interval mode in / hourly/ daily/ summary   **/

function _getFollowup()
{
	$conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonCode ");
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCategoryId", STATUS_FOLLOW_UP );
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->order_by("a.CallReasonCode", "ASC");
	
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{
		$conds[$rows['CallReasonId']] = $rows['CallReasonCode'];
	}
	
	return $conds;	
}	

/* @ param : get interval mode in / hourly/ daily/ summary   **/

function _getNotConnection()
{
	$conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonCode");
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCategoryId",STATUS_NOT_CONNECTION);
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->order_by("a.CallReasonCode", "ASC");
	
	foreach( $this->db->get()-> result_assoc() as $rows ) 
	{
		$conds[$rows['CallReasonId']] = $rows['CallReasonCode'];
	}
	
	return $conds;
}	

/* @ param : get interval mode in / hourly/ daily/ summary   **/

function _getNotContact()
{
	$conds = array();
	$this->db->select("a.CallReasonId, a.CallReasonCode ");
	$this->db->from("t_lk_account_status a");
	$this->db->where("a.CallReasonCategoryId", STATUS_NO_CONTACT );
	$this->db->where("a.CallReasonStatusFlag",1);
	$this->db->order_by("a.CallReasonCode", "ASC");
	
	foreach( $this->db->get()-> result_assoc() as $rows ) 
	{
		$conds[$rows['CallReasonId']] = $rows['CallReasonCode'];
	}
	
	return $conds;
}
	


/* _summaryCallTrackGroupCampaign **/

public function _getSummaryReport()
{
  $_conds = array();
 
 /** ################################################################# get new data and data size ################################################################# **/
 
 
$this ->db ->select(" 
		COUNT(a.CustomerId) AS JUMLAH, d.BukcetSourceId,
		SUM(IF((a.CallReasonId IS NULL OR a.CallReasonId=0), 1, 0)) AS NewData, 
		SUM(IF((a.CallReasonId IS NOT NULL AND a.CallReasonId<>0), 1, 0)) AS Utilize ", FALSE);
	
	$this->db->from("t_gn_debitur a ");
	$this->db->join('t_gn_cif_customer b ',' a.CustomerNumber=b.EnigmaId','LEFT');
	$this->db->join('t_gn_assignment c ',' a.CustomerId=c.CustomerId','LEFT');
	$this->db->join('t_gn_bucket_customers d ',' b.BucketId=d.BucketId','LEFT');
	

	if( is_array($this->BukcetSourceId)) {
		$this->db->where_in("d.BukcetSourceId", $this->BukcetSourceId );
	}	
	
	$this->db->group_by("d.BukcetSourceId");
	
	
	foreach( $this -> db -> get() -> result_assoc() as $rows )
	{
		$_conds[$rows['BukcetSourceId']]['data_size']+= (INT)$rows['JUMLAH'];
		$_conds[$rows['BukcetSourceId']]['data_not_utilize']+= (INT)$rows['NewData'];
	}
	
/** ################################################################# get callreason ID on campaign  ################################################################### **/
 
 
	$this->db->select("COUNT(a.CampaignId) AS size, d.BukcetSourceId , a.CampaignId");
	$this->db->from("t_gn_debitur a");
	$this->db->join("t_gn_cif_customer b","a.CustomerNumber=b.EnigmaId","LEFT");
	$this->db->join("t_gn_assignment c ","a.CustomerId=c.CustomerId","LEFT");
	$this->db->join("t_gn_bucket_customers d","b.BucketId=d.BucketId","LEFT");
	
	if( is_array($this->BukcetSourceId)) {
		$this->db->where_in("d.BukcetSourceId", $this->BukcetSourceId );
	}	
	
	$this->db->group_by("a.CampaignId");
	$this->db->group_by("d.BukcetSourceId");
	
	foreach( $this -> db -> get() -> result_assoc() as $rows )
	{
		$_conds[$rows['BukcetSourceId']][$rows['CampaignId']]+= (INT)$rows['size'];
	}

	
 /** ################################################################# get callreason ID size ################################################################### **/
 
	
	
$this ->db ->select(" 
		count(e.PolicyId) AS JUMLAH, d.BukcetSourceId,a.CallReasonId,
		SUM(IF((a.CallReasonId IS NULL OR a.CallReasonId=0), 1, 0)) AS NewData, 
		SUM(IF((a.CallReasonId IS NOT NULL AND a.CallReasonId<>0), 1, 0)) AS Utilize ", FALSE);
	
	$this->db->from("t_gn_debitur a ");
	$this->db->join('t_gn_cif_customer b ',' a.CustomerNumber=b.EnigmaId','LEFT');
	$this->db->join('t_gn_assignment c ',' a.CustomerId=c.CustomerId','LEFT');
	$this->db->join('t_gn_bucket_customers d ',' b.BucketId=d.BucketId','LEFT');
	$this->db->join('t_gn_policy_detail e ',' b.BucketId=e.BucketId','LEFT');
	
	if( is_array($this->BukcetSourceId)) {
		$this->db->where_in("d.BukcetSourceId", $this->BukcetSourceId );
	}	
	
	$this->db->group_by("a.CallReasonId");
	$this->db->group_by("d.BukcetSourceId");
	$res = $this -> db -> get();

	if ($res) foreach( $res -> result_assoc() as $rows )
	{
		$_conds[$rows['BukcetSourceId']][$rows['CallReasonId']]+= (INT)$rows['JUMLAH'];
		$_conds[$rows['BukcetSourceId']]['data_utilize']+= (INT)$rows['Utilize'];
		
	}

	
 /** ########################################################## get size call atempt ########################################################################## **/

	$this->db->select("count(a.CallHistoryId) as size_atempt, e.BukcetSourceId",FALSE);
	$this->db->from("t_gn_callhistory a ");
	
	
	$this->db->join("t_gn_debitur b "," a.CustomerId=b.CustomerId","LEFT");
	$this->db->join('t_gn_cif_customer c ',' b.CustomerNumber=c.EnigmaId','LEFT');
	$this->db->join('t_gn_assignment d ',' b.CustomerId=d.CustomerId','LEFT');
	$this->db->join('t_gn_bucket_customers e ','c.BucketId=e.BucketId','LEFT');
	
	if( is_array($this->BukcetSourceId)) {
		$this->db->where_in("e.BukcetSourceId", $this->BukcetSourceId );
	}	
	
	if( !is_null($this->StartDate) 
		AND !is_null($this->EndDate) )
	{
		$this->db->where("a.CallHistoryCreatedTs >='{$this->StartDate} 00:00:00' ");
		$this->db->where("a.CallHistoryCreatedTs <='{$this->EndDate} 23:59:59' ");
	}
	
	$this->db->group_by("e.BukcetSourceId");
	
	foreach( $this -> db -> get() -> result_assoc() as $rows )
	{
		$_conds[$rows['BukcetSourceId']]['size_atempt']+= (INT)$rows['size_atempt'];
	}	

/** ########################################################## get name of the campaign ######################################################################### **/
	
	
	$this->db->select("a.FTP_UploadId as BukcetSourceId, a.FTP_UploadFilename, a.FTP_UploadRows, a.FTP_UploadSuccess, a.FTP_UploadFailed");
	$this->db->from("t_gn_upload_report_ftp a ");
	$this->db->join("t_gn_bucket_customers b "," a.FTP_UploadId = b.BukcetSourceId","LEFT");
	$this->db->join("t_lk_ftp_read c ","b.BuketUploadId=c.ftp_read_id","LEFT");
	$this->db->join("t_lk_work_project d "," c.ftp_read_project_code=d.ProjectCode", "LEFT");
	$this->db->where_in("d.ProjectId", $this->EUI_Session->_get_session('ProjectId'));
	
	if( is_array($this->BukcetSourceId)) {
		$this->db->where_in("a.FTP_UploadId", $this->BukcetSourceId );
	}	
	
	$this->db->group_by("a.FTP_UploadId");
	
	foreach( $this->db->get()->result_assoc() as $rows ){
		$_conds[$rows['BukcetSourceId']]['RecsourceName'] = basename($rows['FTP_UploadFilename']);
		$_conds[$rows['BukcetSourceId']]['RowsSize'] = $rows['FTP_UploadRows'];
		$_conds[$rows['BukcetSourceId']]['RowsSuccess'] = $rows['FTP_UploadSuccess'];
		$_conds[$rows['BukcetSourceId']]['RowsFailed'] = $rows['FTP_UploadFailed'];
		
	}
	
	
/** ########################################################## hitung jumlah sales dan Premi total ############################################################## **/
	
	return $_conds;
}
   	
}

?>