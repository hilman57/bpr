<?php 
class R_DailyReportAgency extends EUI_Report 
{
 var $StartDate   	= null;
 var $EndDate 	   	= null;
 var $ReportType   	= null;
 var $ReportGroup  	= null;
 var $LayoutHeader  = null; 
 var $ProjectId		 = null;
 var $CallUserReport = array();
  
/**
 *
 */
 var $MajorDC  =  NULL; 
 
/* 
 * @ param : aksesor default of report model 
 * @ akses : public method 
 */ 
 
 var $Contacted = array();
 var $Uncontacted = array();
 var $Complete = array();
 var $FUAgencyPos = array();
 var $FUAgencySupport = array();
 var $FUBancaSupport = array();
 var $FUVisitToBranch = array();
 var $FUNotInProcess = array();
 
/* 
 * @ param : aksesor default of report model 
 * @ akses : public method 
 */ 
 
public function R_DailyReportAgency()
 {
	
   $this->load->model('M_ReportDailyReport');
    $this->ProjectId = ($this->EUI_Session->_have_get_session('ProjectId') ? $this->EUI_Session->_get_session('ProjectId') : array(1) ); 
   /** report type  **/
	
	if(!is_null($this->M_ReportDailyReport->report_type()) ) 
	{
		$this->ReportType = $this->M_ReportDailyReport->report_type();
	}
	
 /** report group **/
 
	if(!is_null( $this->M_ReportDailyReport->report_group() )) 
	{
		$this->ReportGroup = $this->M_ReportDailyReport->report_group();
	}
	
/** start of the date with format yyyy-mm-dd  **/
	
	if(!is_null( $this->M_ReportDailyReport->start_date() ) )
	{
		$this->StartDate = _getDateEnglish($this->M_ReportDailyReport->start_date());
	}
	
/** end of the date with format yyyy-mm-dd **/
	
	if(!is_null( $this->M_ReportDailyReport->end_date() ) ) 
	{
		$this->EndDate = _getDateEnglish($this->M_ReportDailyReport->end_date());
	}
	
/** set default of data status ***/

  $this->MajorDC = 'Agency';
  $this->Contacted = array('W007');
  $this->Uncontacted = array('W005');
  $this->Complete  = array(1);
  $this->FUAgencyPos = array('700');
  $this->FUAgencySupport = array('800');
  $this->FUBancaSupport = array('900');
  $this->FUVisitToBranch = array('999');
  $this->FUNotInProcess = array(800,999,700,900);
  
  
	
/** set layout function **/ 

  $this->CallUserReport['neded_agent']['user_func']	   = 'DailyReportAgencyByNeedAgency';
  $this->CallUserReport['visit_branch']['user_func']   = 'DailyReportAgencyByVisitBranch';
  $this->CallUserReport['uncontacted']['user_func']    = 'DailyReportAgencyByUncontacted';
  $this->CallUserReport['change_address']['user_func'] = 'DailyReportAgencyByChangeAddress';
  $this->CallUserReport['survey_clean']['user_func']   = 'DailyReportAgencyBySurveyClean';
  

/** set layout title **/

  $this->CallUserReport['neded_agent']['title']		= 'Detail Need Agent';
  $this->CallUserReport['visit_branch']['title']	= 'Detail Visit to Branch'; 
  $this->CallUserReport['uncontacted']['title']		= 'Detail Uncontacted';
  $this->CallUserReport['change_address']['title'] 	= 'Detail Change Address/Ph number/Email';
  $this->CallUserReport['survey_clean']['title']	= 'Detail Surveyed Clean';
  
// layout header neded_agent **/
// layout header neded_agent **/

   $this->CallUserReport['neded_agent']['layout'] = array (
		'PolicyUpdatedTs'=>'Activity Date',
		'PolicyNumber'=>'Policy No',
		'PolicyFirstName'=>'Policy Holder',
		'CampaignCode'=>'Campaign Code',
		'DistributionChannel'=>'Distribute Chanel',
		'District'=>'District Name',
		'BranchName'=>'Branch',
		'EmployeeID'=>'Agent Detail',
		'ProductCategory'=>'Product Category',
		'ProductCode'=>'Group Product',
		'ProductName'=>'Product',
		'IssuedDate'=>'Issued Date',
		'HomePhone'=>'Home No.',
		'OfficePhone'=>'Office No.',
		'MobilePhone'=>'Mobile No.',
		'Q1'=>'Q1',
		'Q2'=>'Q2',
		'Q3'=>'Q3',
		'Q4'=>'Q4',
		'Q5'=>'Q5',
		'Q6'=>'Q6',
		'Q7'=>'Q7',
		'Q8'=>'Q8',
		'Q9'=>'Q9',
		'Q10'=>'Q10',
		'Description'=>'Description',
		'Caller'=>'Caller',
		'CallStatus'=>'Call Status',
		'CallResult'=>'Call Result'
	);
	
// layout header visit_branch **/	
	
   $this->CallUserReport['visit_branch']['layout'] = array
	(
		'PolicyUpdatedTs'=>'Activity Date',
		'PolicyNumber'=>'Policy No',
		'PolicyFirstName'=>'Policy Holder',
		'CampaignCode'=>'Campaign Code',
		'DistributionChannel'=>'Distribute Chanel',
		'District'=>'District Name',
		'BranchName'=>'Branch',
		'EmployeeID'=>'Agent Detail',
		'ProductCategory'=>'Product Category',
		'ProductCode'=>'Group Product',
		'ProductName'=>'Product',
		'IssuedDate'=>'Issued Date',
		'HomePhone'=>'Home No.',
		'OfficePhone'=>'Office No.',
		'MobilePhone'=>'Mobile No.',
		'Q1'=>'Q1',
		'Q2'=>'Q2',
		'Q3'=>'Q3',
		'Q4'=>'Q4',
		'Q5'=>'Q5',
		'Q6'=>'Q6',
		'Q7'=>'Q7',
		'Q8'=>'Q8',
		'Q9'=>'Q9',
		'Q10'=>'Q10',
		'Description'=>'Description',
		'Caller'=>'Caller',
		'CallStatus'=>'Call Status',
		'CallResult'=>'Call Result'
	);	
	
// layout header visit_branch **/	
	
   $this->CallUserReport['uncontacted']['layout'] = array
	(
		'PolicyUpdatedTs'=>'Activity Date',
		'PolicyNumber'=>'Policy No',
		'PolicyFirstName'=>'Policy Holder',
		'CampaignCode'=>'Campaign Code',
		'DistributionChannel'=>'Distribute Chanel',
		'District'=>'District Name',
		'BranchName'=>'Branch',
		'EmployeeID'=>'Agent Detail',
		'ProductCategory'=>'Product Category',
		'ProductCode'=>'Group Product',
		'ProductName'=>'Product',
		'IssuedDate'=>'Issued Date',
		'HomePhone'=>'Home No.',
		'OfficePhone'=>'Office No.',
		'MobilePhone'=>'Mobile No.',
		'Q1'=>'Q1',
		'Q2'=>'Q2',
		'Q3'=>'Q3',
		'Q4'=>'Q4',
		'Q5'=>'Q5',
		'Q6'=>'Q6',
		'Q7'=>'Q7',
		'Q8'=>'Q8',
		'Q9'=>'Q9',
		'Q10'=>'Q10',
		'Description'=>'Description',
		'Caller'=>'Caller',
		'CallStatus'=>'Call Status',
		'CallResult'=>'Call Result'
	);	

// layout header visit_branch **/	
	
   $this->CallUserReport['change_address']['layout'] = array
	(
		'PolicyUpdatedTs'=>'Activity Date',
		'PolicyNumber'=>'Policy No',
		'PolicyFirstName'=>'Policy Holder',
		'CampaignCode'=>'Campaign Code',
		'DistributionChannel'=>'Distribute Chanel',
		'District'=>'District Name',
		'BranchName'=>'Branch',
		'EmployeeID'=>'Agent Detail',
		'ProductCategory'=>'Product Category',
		'ProductCode'=>'Group Product',
		'ProductName'=>'Product',
		'IssuedDate'=>'Issued Date',
		'AddAdress1'=> 'Address1',
		'AddAdress2'=> 'Address2',
		'AddAdress3'=> 'Address3',
		'AddAdress4'=> 'Address4',
		'City' => 'City',
		'ZipCode' => 'Zip',
		'Province' => 'Province',
		'Country' => 'Country',
		'AddEmailAddress' =>  'Email',
		'HomePhone'=>'Home No.',
		'OfficePhone'=>'Office No.',
		'MobilePhone'=>'Mobile No.',
		'Q1'=>'Q1',
		'Q2'=>'Q2',
		'Q3'=>'Q3',
		'Q4'=>'Q4',
		'Q5'=>'Q5',
		'Q6'=>'Q6',
		'Q7'=>'Q7',
		'Q8'=>'Q8',
		'Q9'=>'Q9',
		'Q10'=>'Q10',
		'Description'=>'Description',
		'Caller'=>'Caller',
		'CallStatus'=>'Call Status',
		'CallResult'=>'Call Result'
	);	
	
// layout header visit_branch **/	
	
   $this->CallUserReport['survey_clean']['layout'] = array
	(
		'PolicyUpdatedTs'=>'Activity Date',
		'PolicyNumber'=>'Policy No',
		'PolicyFirstName'=>'Policy Holder',
		'CampaignCode'=>'Campaign Code',
		'DistributionChannel'=>'Distribute Chanel',
		'District'=>'District Name',
		'BranchName'=>'Branch',
		'EmployeeID'=>'Agent Detail',
		'ProductCategory'=>'Product Category',
		'ProductCode'=>'Group Product',
		'ProductName'=>'Product',
		'IssuedDate'=>'Issued Date',
		'HomePhone'=>'Home No.',
		'OfficePhone'=>'Office No.',
		'MobilePhone'=>'Mobile No.',
		'Q1'=>'Q1',
		'Q2'=>'Q2',
		'Q3'=>'Q3',
		'Q4'=>'Q4',
		'Q5'=>'Q5',
		'Q6'=>'Q6',
		'Q7'=>'Q7',
		'Q8'=>'Q8',
		'Q9'=>'Q9',
		'Q10'=>'Q10',
		'Description'=>'Description',
		'Caller'=>'Caller',
		'CallStatus'=>'Call Status',
		'CallResult'=>'Call Result'
	);		
 }
 
 
/**
 * @ def :  DailyReportAgencyByNeedAgency 
 *  ---------------------------------------------------------
 *
 * @ akses : public method 
 * @ param : all request 
 */ 
 
public function ReportIndex()
{
	$report = array();
	if( is_array($this->ReportType))
	{
		foreach( $this->ReportType as $k => $stack ) 
		{
			if(in_array($stack, array_keys($this->CallUserReport) ))
			{
				$report[$stack]['title'] 	= $this->CallUserReport[$stack]['title'];
				$report[$stack]['header'] 	= array_values($this->CallUserReport[$stack]['layout']);
				$report[$stack]['field'] 	= array_keys($this->CallUserReport[$stack]['layout']);
				$report[$stack]['content'] 	= $this->{$this->CallUserReport[$stack]['user_func']}(); 
			}	
		}
	}

	return $report;
}
 
/**
 * @ def :  DailyReportAgencyByNeedAgency 
 *  ---------------------------------------------------------
 *
 * @ akses : public method 
 * @ param : all request 
 */ 
 
public function DailyReportAgencyByNeedAgency() 
{ 
 static $config = array();
	
/** this content query resulting Method **/
	
	$this->db->reset_select();
	$this->db->select(" DISTINCT a.EnigmaId,
		a.PolicyUpdatedTs as PolicyUpdatedTs, 
		a.PolicyNumber as PolicyNumber,
		a.PolicyFirstName as PolicyFirstName, 
		a.PolicyCampaign_Master_Code as CampaignCode,
		if( d.Major_Dc=0, 'Agency', d.Major_Dc) as DistributionChannel,
		d.Nama_Dc as District,
		d.Branch_Name as BranchName,
		d.Employee_ID as EmployeeID,
		d.Product_Category as ProductCategory,
		d.Product_Code as ProductCode,
		d.Product_Name as ProductName,
		a.PolicyIssDate as IssuedDate,
		b.CustomerHomePhoneNum as HomePhone,
		b.CustomerWorkPhoneNum as OfficePhone,
		b.CustomerMobilePhoneNum as MobilePhone,
		h.CustField_Q1 as Q1, 
		h.CustField_Q2 as Q2, 
		h.CustField_Q3 as Q3,
		h.CustField_Q4 as Q4,
		h.CustField_Q5 as Q5,
		h.CustField_Q6 as Q6,
		h.CustField_Q7 as Q7,
		h.CustField_Q8 as Q8,
		h.CustField_Q9 as Q9,
		h.CustField_Q10 as Q10,
		i.FuNotes as Description,
		g.full_name as Caller,
		f.CallReasonCategoryName as CallStatus,
		e.CallReasonDesc as CallResult", FALSE);
		
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c "," b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d "," c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_lk_account_status e "," a.CallReasonId=e.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status f "," e.CallReasonCategoryId=f.CallReasonCategoryId","LEFT");
	$this->db->join("t_tx_agent g ","a.CreateByUserId=g.UserId","LEFT");
	
	$this->db->join("t_gn_followup_question h ", " a.PolicyId=h.PolicyId","LEFT");
	$this->db->join("t_gn_followup i ","h.QuestionId=i.FuQuestionId","LEFT");
	
	$this->db->join("t_lk_followupgroup j"," i.FuGroupCode=j.FuGroupCode", "LEFT");
	$this->db->join("t_gn_campaign k ", "b.CampaignId=k.CampaignId","LEFT");
	$this->db->where_in("a.CallComplete", $this->Complete);
	$this->db->where_in("f.CallReasonCategoryCode", $this->Contacted);
	$this->db->where_in("f.CallReasonProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("j.FuProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("j.FuGroupCode", $this->FUAgencySupport);
	$this->db->like("k.CampaignName", $this->MajorDC);
	
/** get filter data start date **/
	if(!is_null($this->StartDate) )
	$this->db->where(" i.FuCreatedDate>='{$this->StartDate} 00:00:00'", '', FALSE);
/** get filter data start date **/	
	if(!is_null($this->EndDate))
	$this->db->where(" i.FuCreatedDate<='{$this->EndDate} 23:59:59'", '', FALSE);
	
	$num = 0;
	foreach($this->db->get()->result_assoc() as $rows ) 
	{
		foreach( $rows as $field => $values ){
			$config[$num][$field] = $values;	
		}
		$num++;
	}
	
	
	return $config;
}

/** DailyReportAgencyByVisitBranch **/

public function DailyReportAgencyByVisitBranch(){ 
	 static $config = array();
	
/** this content query resulting Method **/
	
	$this->db->reset_select();
	$this->db->select(" DISTINCT a.EnigmaId,
		a.PolicyUpdatedTs as PolicyUpdatedTs, 
		a.PolicyNumber as PolicyNumber,
		a.PolicyFirstName as PolicyFirstName, 
		a.PolicyCampaign_Master_Code as CampaignCode,
		if( d.Major_Dc=0, 'Agency', d.Major_Dc) as DistributionChannel,
		d.Nama_Dc as District,
		d.Branch_Name as BranchName,
		d.Employee_ID as EmployeeID,
		d.Product_Category as ProductCategory,
		d.Product_Code as ProductCode,
		d.Product_Name as ProductName,
		a.PolicyIssDate as IssuedDate,
		b.CustomerHomePhoneNum as HomePhone,
		b.CustomerWorkPhoneNum as OfficePhone,
		b.CustomerMobilePhoneNum as MobilePhone,
		h.CustField_Q1 as Q1, 
		h.CustField_Q2 as Q2, 
		h.CustField_Q3 as Q3,
		h.CustField_Q4 as Q4,
		h.CustField_Q5 as Q5,
		h.CustField_Q6 as Q6,
		h.CustField_Q7 as Q7,
		h.CustField_Q8 as Q8,
		h.CustField_Q9 as Q9,
		h.CustField_Q10 as Q10,
		i.FuNotes as Description,
		g.full_name as Caller,
		f.CallReasonCategoryName as CallStatus,
		e.CallReasonDesc as CallResult", FALSE);
		
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c "," b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d "," c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_lk_account_status e "," a.CallReasonId=e.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status f "," e.CallReasonCategoryId=f.CallReasonCategoryId","LEFT");
	$this->db->join("t_tx_agent g ","a.CreateByUserId=g.UserId","LEFT");
	
	$this->db->join("t_gn_followup_question h ", " a.PolicyId=h.PolicyId","LEFT");
	$this->db->join("t_gn_followup i ","h.QuestionId=i.FuQuestionId","LEFT");
	
	$this->db->join("t_lk_followupgroup j"," i.FuGroupCode=j.FuGroupCode", "LEFT");
	$this->db->join("t_gn_campaign k ", "b.CampaignId=k.CampaignId","LEFT");
	
	$this->db->where_in("a.CallComplete", $this->Complete);
	$this->db->where_in("f.CallReasonCategoryCode", $this->Contacted);
	$this->db->where_in("f.CallReasonProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("j.FuProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("j.FuGroupCode", $this->FUVisitToBranch);
	$this->db->like("k.CampaignName", $this->MajorDC);
	
/** get filter data start date **/
	if(!is_null($this->StartDate) )
	$this->db->where(" i.FuCreatedDate>='{$this->StartDate} 00:00:00'", '', FALSE);
/** get filter data start date **/	
	if(!is_null($this->EndDate))
	$this->db->where(" i.FuCreatedDate<='{$this->EndDate} 23:59:59'", '', FALSE);
	
	$num = 0;
	foreach($this->db->get()->result_assoc() as $rows ) 
	{
		foreach( $rows as $field => $values ){
			$config[$num][$field] = $values;	
		}
		$num++;
	}
	
	
	return $config;
}

/* @ def 	: DailyReportAgencyByUncontacted 
 * @ notes  : adalah data2 policy yang status-nya not contacted  dalam aplikasi = w005
 * --------------------------------------------------------------------------------------------
 
 * @ update : 2015-01-16 
 * @ auth : omens  
 *
 */
public function DailyReportAgencyByUncontacted() 
{ 
	static $config = array();
	/** this content query resulting Method **/
	
	$this->db->reset_select();
	$this->db->select(" DISTINCT a.EnigmaId,
		a.PolicyUpdatedTs as PolicyUpdatedTs, 
		a.PolicyNumber as PolicyNumber,
		a.PolicyFirstName as PolicyFirstName, 
		a.PolicyCampaign_Master_Code as CampaignCode,
		if( d.Major_Dc=0, 'Agency', d.Major_Dc) as DistributionChannel,
		d.Nama_Dc as District,
		d.Branch_Name as BranchName,
		d.Employee_ID as EmployeeID,
		d.Product_Category as ProductCategory,
		d.Product_Code as ProductCode,
		d.Product_Name as ProductName,
		a.PolicyIssDate as IssuedDate,
		b.CustomerHomePhoneNum as HomePhone,
		b.CustomerWorkPhoneNum as OfficePhone,
		b.CustomerMobilePhoneNum as MobilePhone,
		'' as Q1, 
		'' as Q2, 
		'' as Q3,
		'' as Q4,
		'' as Q5,
		'' as Q6,
		'' as Q7,
		'' as Q8,
		'' as Q9,
		'' as Q10,
		(SELECT hs.CallHistoryNotes FROM t_gn_callhistory hs  WHERE hs.PolicyId=a.PolicyId AND hs.UpdatedById IS NULL ORDER BY hs.CallHistoryId DESC LIMIT 1 ) as Description, 
		g.full_name as Caller,
		f.CallReasonCategoryName as CallStatus,
		e.CallReasonDesc as CallResult", FALSE);
		
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c "," b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d "," c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_lk_account_status e "," a.CallReasonId=e.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status f "," e.CallReasonCategoryId=f.CallReasonCategoryId","LEFT");
	$this->db->join("t_tx_agent g ","a.CreateByUserId=g.UserId","LEFT");
	$this->db->join("t_gn_campaign h ", "b.CampaignId=h.CampaignId","LEFT");
	$this->db->join("t_gn_campaign_project i ","h.CampaignId=i.CampaignId  ","LEFT");
	$this->db->where_in("a.CallComplete", array('1','0'));
	$this->db->where_in("f.CallReasonCategoryCode", $this->Uncontacted);
	$this->db->where_in("f.CallReasonProjectId", $this->ProjectId);
	$this->db->where_in("i.ProjectId", $this->ProjectId);
	$this->db->like("h.CampaignName", $this->MajorDC);
	
/** get filter data start date **/
	if(!is_null($this->StartDate) )
	{
		$this->db->where(" a.PolicyUpdatedTs >='{$this->StartDate} 00:00:00'", "", FALSE);
	}
/** get filter data start date **/	

	if(!is_null($this->EndDate)){
		$this->db->where(" a.PolicyUpdatedTs <='{$this->EndDate} 23:59:59'", "", FALSE);
	}
	
	//echo $this->db->_get_var_dump();
	
	$num = 0;
	foreach($this->db->get()->result_assoc() as $rows ) 
	{
		foreach( $rows as $field => $values ){
			$config[$num][$field] = $values;	
		}
		$num++;
	}
	
	
	return $config;
}

/** DailyReportAgencyByChangeAddress **/
/** have bug on here fixedbug#2015-02-04#omens **/

public function DailyReportAgencyByChangeAddress()
{ 

 static $config = array();
	
/** this content query resulting Method **/	
	$this->db->reset_select();
	$this->db->select(" DISTINCT a.EnigmaId,
		a.PolicyUpdatedTs as PolicyUpdatedTs, 
		a.PolicyNumber as PolicyNumber,
		a.PolicyFirstName as PolicyFirstName, 
		a.PolicyCampaign_Master_Code as CampaignCode,
		if( d.Major_Dc=0, 'Agency', d.Major_Dc) as DistributionChannel,
		d.Nama_Dc as District,
		d.Branch_Name as BranchName,
		d.Employee_ID as EmployeeID,
		d.Product_Category as ProductCategory,
		d.Product_Code as ProductCode,
		d.Product_Name as ProductName,
		a.PolicyIssDate as IssuedDate,	
		i.FuAddr1 AS AddAdress1, 
		i.FuAddr2 AS AddAdress2, 
		i.FuAddr3 AS AddAdress3,
		i.FuAddr4 AS AddAdress4,
		i.FuCity as City, 
		i.FuZip as ZipCode,
		i.FuProvince as Province,
		i.FuCountry as Country,
		i.FuHomePhone as HomePhone,
		i.FuMobilePhone as MobilePhone,
		i.FuOfficePhone as OfficePhone,
		i.FuEmail as AddEmailAddress,
		h.CustField_Q1 as Q1, 
		h.CustField_Q2 as Q2, 
		h.CustField_Q3 as Q3,
		h.CustField_Q4 as Q4,
		h.CustField_Q5 as Q5,
		h.CustField_Q6 as Q6,
		h.CustField_Q7 as Q7,
		h.CustField_Q8 as Q8,
		h.CustField_Q9 as Q9,
		h.CustField_Q10 as Q10,
		i.FuNotes as Description,
		g.full_name as Caller,
		f.CallReasonCategoryName as CallStatus,
		e.CallReasonDesc as CallResult", FALSE);
		
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c "," b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d "," c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_lk_account_status e "," a.CallReasonId=e.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status f "," e.CallReasonCategoryId=f.CallReasonCategoryId","LEFT");
	$this->db->join("t_tx_agent g ","a.CreateByUserId=g.UserId","LEFT");
	$this->db->join("t_gn_followup_question h ", " a.PolicyId=h.PolicyId","LEFT");
	$this->db->join("t_gn_followup i ","h.QuestionId=i.FuQuestionId","LEFT");
	
	$this->db->join("t_lk_followupgroup j"," i.FuGroupCode=j.FuGroupCode", "LEFT");
	$this->db->join("t_gn_campaign k ", "b.CampaignId=k.CampaignId","LEFT");
	
	$this->db->where_in("a.CallComplete", $this->Complete);
	$this->db->where_in("f.CallReasonCategoryCode", $this->Contacted);
	$this->db->where_in("f.CallReasonProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("j.FuProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("j.FuGroupCode", $this->FUAgencyPos);
	$this->db->like("k.CampaignName", $this->MajorDC);
		
/** get filter data start date **/
	if(!is_null($this->StartDate) )
	$this->db->where(" i.FuCreatedDate>='{$this->StartDate} 00:00:00'", '', FALSE);
/** get filter data start date **/	
	if(!is_null($this->EndDate))
	$this->db->where(" i.FuCreatedDate<='{$this->EndDate} 23:59:59'", '', FALSE);
	
	
	//echo $this->db->_get_var_dump();
	
	$num = 0;
	foreach($this->db->get()->result_assoc() as $rows ) 
	{
		foreach( $rows as $field => $values ){
			$config[$num][$field] = $values;	
		}
		$num++;
	}
	
	
	return $config;
}


/** DailyReportAgencyBySurveyClean **/

public function DailyReportAgencyBySurveyClean()
{ 
	 static $config = array();
	
/** this content query resulting Method **/
	
	$this->db->reset_select();
	$this->db->select(" DISTINCT a.EnigmaId,
		a.PolicyUpdatedTs as PolicyUpdatedTs, 
		a.PolicyNumber as PolicyNumber,
		a.PolicyFirstName as PolicyFirstName, 
		a.PolicyCampaign_Master_Code as CampaignCode,
		if( d.Major_Dc=0, 'Agency', d.Major_Dc) as DistributionChannel,
		d.Nama_Dc as District,
		d.Branch_Name as BranchName,
		d.Employee_ID as EmployeeID,
		d.Product_Category as ProductCategory,
		d.Product_Code as ProductCode,
		d.Product_Name as ProductName,
		a.PolicyIssDate as IssuedDate,
		b.CustomerHomePhoneNum as HomePhone,
		b.CustomerWorkPhoneNum as OfficePhone,
		b.CustomerMobilePhoneNum as MobilePhone,
		h.CustField_Q1 as Q1, 
		h.CustField_Q2 as Q2, 
		h.CustField_Q3 as Q3,
		h.CustField_Q4 as Q4,
		h.CustField_Q5 as Q5,
		h.CustField_Q6 as Q6,
		h.CustField_Q7 as Q7,
		h.CustField_Q8 as Q8,
		h.CustField_Q9 as Q9,
		h.CustField_Q10 as Q10,
		'-' as Description,
		g.full_name as Caller,
		f.CallReasonCategoryName as CallStatus,
		e.CallReasonDesc as CallResult", FALSE);
		
	$this->db->from("t_gn_policy_detail a");
	$this->db->join("t_gn_debitur b "," a.EnigmaId=b.CustomerNumber","LEFT");
	$this->db->join("t_gn_cif_customer c "," b.CustomerNumber=c.EnigmaId","LEFT");
	$this->db->join("t_gn_bucket_customers d "," c.BucketId=d.BucketId","LEFT");
	$this->db->join("t_lk_account_status e "," a.CallReasonId=e.CallReasonId","LEFT");
	$this->db->join("t_lk_customer_status f "," e.CallReasonCategoryId=f.CallReasonCategoryId","LEFT");
	$this->db->join("t_tx_agent g ","a.CreateByUserId=g.UserId","LEFT");
	$this->db->join("t_gn_followup_question h ", " a.PolicyId=h.PolicyId","LEFT");
	
	$this->db->join("t_gn_campaign k ", "b.CampaignId=k.CampaignId","LEFT");
	$this->db->join("t_gn_followup fp "," h.QuestionId=fp.FuQuestionId","LEFT");	
	$this->db->join("t_gn_campaign_project cp ", "b.CampaignId=cp.CampaignId","LEFT");
	
	$this->db->where_in("a.CallComplete", $this->Complete);
	$this->db->where_in("f.CallReasonCategoryCode", $this->Contacted);
	$this->db->where_in("f.CallReasonProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where_in("cp.ProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->where("fp.FuQuestionId IS NULL","",FALSE);
	$this->db->like("k.CampaignName", $this->MajorDC);
	
/** get filter data start date **/
	if(!is_null($this->StartDate) )
	$this->db->where(" h.CreateTs>='{$this->StartDate} 00:00:00'", '', FALSE);
/** get filter data start date **/	
	if(!is_null($this->EndDate))
	$this->db->where(" h.CreateTs<='{$this->EndDate} 23:59:59'", '', FALSE);
	
	$num = 0;
	foreach($this->db->get()->result_assoc() as $rows ) 
	{
		foreach( $rows as $field => $values ){
			$config[$num][$field] = $values;	
		}
		$num++;
	}
	
	
	return $config;

}
 
 
 }