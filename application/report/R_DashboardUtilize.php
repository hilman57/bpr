<?php
/**
 * # def : R_DashboardUtilize
 * ----------------------------------------------------
 
 * @ source of data telephony Only On cc_agent_acivity_log
 * @ join by agent collection < t_tx_agent > 
 *
 */
 
class R_DashboardUtilize extends EUI_Report
{

/**
 * @ def     : singgle functoin prototype function handle all 
 * -----------------------------------------------------------------------
 
 * @ param 	 : -
 * @ testing : - 
 *
 */
 
 protected $StartDate  = NULL;
 protected $EndDate    = NULL;
 protected $CallerId 	= NULL;

/**
 * @ def : aksessor of class attributr
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : void()
 */
 
 public function R_DashboardUtilize() 
 {
	$this->load->model(array('M_RptUpload'));
	
 /** set data parameters **/	
 
	$this->StartDate = ($this->URI->_get_have_post('start_date')?date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : NULL );
	$this->EndDate = ($this->URI->_get_have_post('end_date')?date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : NULL );
	$this->CallerId = ($this->URI->_get_have_post('caller')?$this->URI->_get_array_post('caller'): NULL );
 }

/**
 * @ def : aksessor of class attributr
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : void()
 */
 	
 public function _get_param()
 {
	return array(
		'spv' 		 => $this->URI->_get_post('supervisor'),
		'caller' 	 => $this->URI->_get_post('caller'),
		'start_date' => ( $this->URI->_get_have_post('start_date') ? date("Y-m-d", strtotime($this->URI->_get_post('start_date'))) : '' ),
		'end_date'	 => ( $this->URI->_get_have_post('end_date') ? date("Y-m-d", strtotime($this->URI->_get_post('end_date'))) : '' )
	);
}

/**
 * @ def : aksessor of class attributr
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : void()
 */	
 
 public function _send_param()
 {
	$param = $this->_get_param();
	return array
	(
		'title' => 'Daily Dashboard Customer Utilize',
		'param' => $param,
		'AgentName' => $this->M_RptUpload->_getAgentName( array('AgentId' => $param['caller']) ),
		'data_size' => $this->SummaryDataByAgentUtilize()
	);
 }
 
/**
 * @ def : aksessor of class attributr
 * ------------------------------------------------------
 
 * @ akses  : public 
 * @ method : void()
 */	
 
public function SummaryDataByAgentUtilize()
{
 static $size_data = array();
 
 $this->db->select("
		d.AssignSelerId as UserId,
		COUNT(a.PolicyId) as tot_data_size, 
		SUM(IF(a.CallComplete IN(1), 1, 0)) as tot_size_complete,
		SUM(IF(a.CallReasonId IN(1) && a.CallComplete IN(1), 1, 0)) as tot_size_survey,
		SUM(IF(a.CallReasonId IN(5) && a.CallComplete IN(1), 1, 0)) as tot_size_not_reach,
		SUM(IF(a.CallReasonId IN(8) && a.CallComplete IN(1), 1, 0)) as tot_size_not_answer,
		SUM(IF(a.CallReasonId IN(9) && a.CallComplete IN(1), 1, 0)) as tot_size_busy_tone,
		SUM(IF(a.CallReasonId IN(14) && a.CallComplete IN(1), 1, 0)) as tot_size_wrong_number,
		SUM(IF(a.CallReasonId IN(10) && a.CallComplete IN(1), 1, 0)) as tot_size_dead_tone,
		SUM(IF(a.CallReasonId IN(12) && a.CallComplete IN(1), 1, 0)) as tot_size_leave_message,
		SUM(IF(a.CallReasonId IN(5) && a.CallComplete IN(1), 1, 0)) as tot_no_contacted,
		SUM(IF(a.CallReasonId IN(15,16,17,18) && a.CallComplete IN(1), 1, 0)) as tot_size_return_list,
		SUM(IF(a.CallReasonId IN(6,7,54) && a.CallComplete IN(0), 1, 0)) as tot_size_callback_letter,
		SUM(IF(a.CallReasonId IN(2,3,4) && a.CallComplete IN(1), 1, 0)) as tot_size_not_interest_talk,
		SUM(IF(a.CallAttempt IN(1) && a.CallComplete IN(0) && a.CallReasonId NOT IN(6,7,54), 1, 0)) as tot_size_attempt1,
		SUM(IF(a.CallAttempt IN(2) && a.CallComplete IN(0) && a.CallReasonId NOT IN(6,7,54), 1, 0)) as tot_size_attempt2,
		SUM(IF(a.CallAttempt IN(3) && a.CallComplete IN(0) && a.CallReasonId NOT IN(6,7,54), 1, 0)) as tot_size_attempt3,
		SUM(IF(a.CallAttempt IN(4) && a.CallComplete IN(0) && a.CallReasonId NOT IN(6,7,54), 1, 0)) as tot_size_attempt4,
		SUM(IF(a.CallAttempt>=5 && a.CallComplete IN(0) && a.CallReasonId NOT IN(6,7,54), 1, 0)) as tot_size_attempt5_plush", FALSE );
		
 $this->db->from("t_gn_policy_detail a ");
 $this->db->join("t_gn_debitur b","a.EnigmaId=b.CustomerNumber","LEFT");
 $this->db->join("t_gn_cif_customer c","b.CustomerNumber=c.EnigmaId","LEFT");
 $this->db->join("t_gn_assignment d","b.CustomerId=d.CustomerId","LEFT");
 $this->db->join("t_gn_campaign_project e "," b.CampaignId=e.CampaignId","LEFT");
 
/** set filter **/
 $this->db->where_in("e.ProjectId", $this->EUI_Session->_get_session('ProjectId'));

/** if have start_date **/
 if( !is_null($this->StartDate)) 
  $this->db->where("a.PolicyUpdatedTs>='{$this->StartDate} 00:00:00'", "", FALSE);

/** if have end_date **/
 if( !is_null($this->EndDate))
  $this->db->where("a.PolicyUpdatedTs<='{$this->EndDate} 23:59:59'", "", FALSE);
	
/** if have post Caller ID **/
 if( !is_null($this->CallerId))
 $this->db->where_in("d.AssignSelerId", $this->CallerId);
 
 $this->db->group_by("UserId");
 $this->db->having("UserId IS NOT NULL");
// echo $this->db->_get_var_dump();
/* get resource data **/
 
 $recsource = $this->db->get();
 if( $recsource->num_rows() > 0 ) 
 foreach( $recsource->result_assoc() as $rows )
 {
	foreach( $rows as $field => $content )
	{ 
	  if( !in_array($field, array('UserId')) )
	  {
		$size_data[$rows['UserId']][$field] += $content; 
	  }		
	}
 }

 return $size_data;
 
 
 
 }
 
 
}

// END OF CLASS 

?>