<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_UpdatePrincipalBalance extends EUI_Upload 
{

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 var $_pop_status = '108';
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

/*
 * @ pack : private static $instance  =null
 */

 private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/*
 * @ pack : cek duplicate in error result .
 */
 
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
 private function _is_duplicate_error()
{
	$_error = mysql_error();
	// var_dump($_error);
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack  :  constructor  
 */
 
public function U_UpdatePrincipalBalance() 
{ 
	$this->_get_campaignId();
	//$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
 
/*
 * @ pack : private get Deb+id 
 */ 
 
 private function _getDebiturId( $AccountNo=0 )
{
   $DebiturId = null;
   $this->db->reset_select();
   $this->db->select("a.deb_id, a.deb_prev_call_status_code, a.deb_cmpaign_id");
   $this->db->from("t_gn_debitur a");
   $this->db->where("a.deb_acct_no", $AccountNo);
   
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 )
   {
	 if( $rows = $qry->result_first_assoc() ){
		$DebiturId = array('deb_id'=>$rows['deb_id'], 
				'prev_call_status'=> $rows['deb_prev_call_status_code'],
				'deb_cmpaign_id' => $rows['deb_cmpaign_id']
			);
	 }
   }
   
   return $DebiturId;
   
 } 
 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
public function _set_process() 
{

// @ pack : set process save to debitur

 if( is_array($this->_upload_table_data) 
	AND !is_null($this->_upload_table_name) ) 
	foreach( $this->_upload_table_data as $n => $values ) 
 {
 
	
// @ unset : DC ID then replace with its 

    $Debitur = $this->_getDebiturId($values['deb_acct_no']);
	if( is_array($Debitur) )
	{
		$this->_set_additional('deb_id', $Debitur['deb_id']);
		$this->_set_additional('deb_cmpaign_id', $Debitur['deb_cmpaign_id']);
	}
	
	
 // @ pack : look add field 
	
	if( is_array($this->_field_additional) 
		AND count($this->_field_additional)>0 )
	{
		foreach( $this->_field_additional as $field => $value ){
			$values[$field] = $value;
		}
	}
	// var_dump ($values);
   // @ pack : concate array 
	
	$this->_set_update_debitur($values);
	$this->_tots_success +=1;
	
 } 
 
}

/*
 * @ pack : private function  
 */
 
 private function _get_assign_id( $CustomerId )
{	
 $this->db->reset_select();
 $this->db->select('AssignId');
 $this->db->from('t_gn_assignment');
 $this->db->where('CustomerId',$CustomerId);
 
 $qry = $this->db->get();
 if( $qry->num_rows() > 0)
 {
	return $qry->result_singgle_value();
 }  else {
	return FALSE;
 } 
 
}
 
/*
 * @ pack : private function  
 */
 
 private function _set_update_debitur( $xls= null ) 
{
  $conds = 0; 
  if( !is_null($xls) AND is_array($xls) )
  {
	
    $this->db->set('deb_pri_afterpay',$xls['deb_pri_afterpay']);
    $this->db->set('deb_bal_afterpay',$xls['deb_bal_afterpay']);
	$this->db->set('deb_updated_ts', date('Y-m-d H:i:s'));
    $this->db->where('deb_acct_no',$xls['deb_acct_no']);
    $this->db->where('deb_id',$xls['deb_id']);
	$this->db->update('t_gn_debitur');
	if($this->db->affected_rows()>0 )
	{
		$conds++;
	}
	
  }
  
  return $conds;
  
} 

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
$CampaignId = self::_get_campaignId();
	
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	$this->_set_additional('deb_upload_id', $this->_field_uploadId);
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
  }
}

}

// END CLASS 

?>