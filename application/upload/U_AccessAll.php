<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_AccessAll extends EUI_Upload 
{
	/* 
	 * @ pack : variable upload 
	 */
	 
	 var $_upload_table_name = null;
	 var $_upload_table_data = null;

	 
	/* 
	 * @ pack : variable upload 
	 */
	 
	 var $_tots_rowselect = 0;
	 var $_tots_success = 0;
	 var $_tots_failed = 0;
	 var $_tots_duplicate = 0;
	 
	/*
	 * @ pack : private static $instance  =null
	 */

	private $_field_additional = array(); 
	private $_field_uploadId = 0;
	private $_is_complete = FALSE;
	private $_campaignId = 0;
	private $_bucket_setup_id = null ;
	private $AccessAll = 1;
	 
	private static $Instance = null;

	/*
	 * @ pack : private get $instance  =null
	 */
	 
	public static function &Instance()
	{
		if( is_null(self::$Instance) ) 
		{
			self::$Instance = new self();
		}

		return self::$Instance;
	}
	
	/*
	 * @ pack  :  constructor  
	 */
	 
	public function U_AccessAll() 
	{ 
		// $this->_get_campaignId();
		// $this->load->model(array('M_MgtRoundData'));
	}
	
	/*
	 * @ pack : _reset_argvs
	 */
	 
	public function _reset_class_argvs() 
	{	
	 $this->_tots_rowselect = 0;
	 $this->_tots_success = 0;
	 $this->_tots_failed = 0;
	 $this->_campaignId = 0;
	 $this->_field_uploadId= 0;
	 $this->_tots_duplicate = 0;
	 $this->_is_complete = false;
	 $this->_upload_table_name = null;
	 $this->_upload_table_data= null;
	 $this->_field_additional = array(); 
	 $this->_bucket_setup_id = null;
	 
	}
	
	 /*
	 * @ pack : cek duplicate in error result .
	 */
 
	private function _is_duplicate_error()
	{
		$_error = mysql_error();
		if(preg_match("/\Dup/i", $_error)) {
			return true;
		}	
		else{
			return false;
	  }
	}
	public function _get_class_callback(){
		$_call_back = array (
			'TOTAL_UPLOAD' => $this->_tots_rowselect,
			'TOTAL_SUCCES' => $this->_tots_success,
			'TOTAL_FAILED' => $this->_tots_failed,
			'TOTAL_DUPLICATE' => $this->_tots_duplicate 
		);
		
		return (object)$_call_back;
	}
	
	/*
	 * @ pack  : _set_templateid Ok 
	 */
 
	public function _set_uploadId( $_set_uploadId = 0 ) 
	{
		if( $_set_uploadId ) 
		{
			$this->_field_uploadId= $_set_uploadId;
		}	
	}
	/*
	 * @ pack  : _set_templateid Ok 
	 */
	 
	public function _set_additional( $field = null, $values=null  ) 
	 {
		if( !is_null($field) )
		{
			$this->_field_additional[$field] = $values;
		}	
	 }
	
	/*
	 * @ pack : insert moethode of distribute Ok 
	 */
	 
	public function _set_table( $table = null )
	{
	  if( !is_null( $table ) ) 
	  {
		 $this->_upload_table_name = $table;
	  }
	}
	/*
	 * @ pack : insert moethode of distribute Ok 
	 */
	 
	public function _get_is_complete()
	{
		return $this->_is_complete;
	}

	/*
	 * @ pack : insert moethode of distribute Ok 
	 */
	 
	public function _set_content( $argv = null )
	{
		//$CampaignId = self::_get_campaignId();
		
	  if( !is_null($argv) AND is_array($argv) )
	  {
		$this->_upload_table_data = $argv;
		$this->_tots_rowselect = count($argv);
		$this->_is_complete  = true;
	  }
	}
	
	/*
	 * @ pack  : insert moethode of distribute Ok 
	 */
	 
	public function _set_process() 
	{
		// insert setup
		$this->save_setup_access_all();
		
		// @ pack : set process save to debitur
   
		if( is_array($this->_upload_table_data) 
			AND !is_null($this->_upload_table_name) )
		{
			// print_r($this->_upload_table_data);
			foreach( $this->_upload_table_data as $n => $values ) 
			{
			
				// @ unset : DC ID then replace with its 
				$Debitur = $this->_getDebiturId(trim($values['acc_no']));
				// unset($values['deb_id']);
				$values['modul_setup_id'] = $this->_bucket_setup_id;
				$values['upload_file_id'] = $this->_field_uploadId;
				if( is_array($Debitur)){
					$values['deb_id'] = (int)$Debitur['deb_id'];
				} else {
					$values['deb_id'] = 0;
				}	
				
				// look add field 
				if( is_array($this->_field_additional) 
					AND count($this->_field_additional)>0 )
				{
					foreach( $this->_field_additional as $field => $value ){
						$values[$field] = $value;
					}
				}
			   // @ pack : concate array 
				if($values['deb_id'] != "0")
				{
					$this->db->insert( $this->_upload_table_name, $values );
					// echo $this->db->last_query()."\n";
					if($this->db->affected_rows()<1)  
					{
						if( $this->_is_duplicate_error() ){
							$this->_tots_duplicate+=1;
						} else {
							$this->_tots_failed +=1; 
						}
						
					} else {
						$this->_tots_success +=1;
						$TRX_ID[] = $this->db->insert_id();
						$this->_set_update_debitur($values['deb_id']);
					}
				}
				else
				{
					$this->_tots_failed +=1; 
				}
				
			}
		}
		
		if ( count($TRX_ID) > 0 )
		{
			$this->insert_manage_access_all($TRX_ID);
		}
	}
	
	private function save_setup_access_all()
	{
		$running = $this->AccesAllIsRunning();
		// var_dump($this->_bucket_setup_id);
		// var_dump($running == FALSE);
		if( $running == FALSE )
		{
			$user_create = _get_session('UserId');
			// if( $this->URI->_get_have_post('user_create_setup')) {
				// $user_create = $this -> URI->_get_post('user_create_setup');
			// }
			
			$this->db->set( 'mod_set_createdby', $user_create );
			$this->db->set( 'mod_set_createdts','now()',false );
			$this->db->set( 'mod_set_running', '1' );
			$this->db->set( 'modul_id_rnd', $this->AccessAll );
			/*** tidak dipake karena data untuk access all bisa di suntik /upload ulang
			**** $this->db->set( 'mod_upload_id', $this->_field_uploadId );
			***/
			if( $this->db->insert('t_gn_modul_setup') )
			{
				$this->_bucket_setup_id = $this->db->insert_id();
			}
		}
		
	}
	//check access all already running
	private function AccesAllIsRunning()
	{
		$running = false;
		
		$this->db->reset_select();
		$this->db->select("a.modul_setup_id");
		$this->db->from("t_gn_modul_setup a");
		$this->db->where("a.mod_set_running", "1");
		$this->db->where("a.modul_id_rnd", $this->AccessAll);
		$qry = $this->db->get();
		if( $qry->num_rows() > 0 )
		{
			if( $rows = $qry->result_first_assoc() )
			{
				$this->_bucket_setup_id = $rows['modul_setup_id'];
				$running = true;
			}
		}
		// echo $this->db->last_query();
		return $running;
	}
	
	/*
	 * @ pack : private get Deb+id 
	 */ 
	 
	 private function _getDebiturId( $AccountNo=0 )
	{
	   $DebiturId = null;
	   $this->db->reset_select();
	   $this->db->select("a.deb_id, a.deb_prev_call_status_code");
	   $this->db->from("t_gn_debitur a");
	   $this->db->where("a.deb_acct_no", $AccountNo);
	   
	   $qry = $this->db->get();
	   if( $qry->num_rows() > 0 )
	   {
		 if( $rows = $qry->result_first_assoc() ){
			$DebiturId = array('deb_id'=>$rows['deb_id'], 'prev_call_status'=> $rows['deb_prev_call_status_code']);
		 }
	   }
	   
	   return $DebiturId;
	   
	} 
	
	/*
	** update deb_access_all
	**
	*/
	
	private function _set_update_debitur($key)
	{
		
		$this->db->set("deb_is_access_all",1);
		$this->db->set("deb_is_lock",0);
		$this->db->where("deb_id", $key);
		$this->db->update("t_gn_debitur");
	}
	
	private function insert_manage_access_all($bucket)
	{
		$auto = $this -> URI->_get_post('AutoStart');
		if($auto)
		{
			$start = 	$this -> URI->_get_post('access_start_hour').":".
						$this -> URI->_get_post('access_start_min').":".
						$this -> URI->_get_post('access_start_sec');
			$end = 	$this -> URI->_get_post('access_end_hour').":".
					$this -> URI->_get_post('access_end_min').":".
					$this -> URI->_get_post('access_end_sec');
			$run = "0";
		}
		else
		{
			$start = date('H:i:s');
			$end = 	$this -> URI->_get_post('access_end_hour').":".
					$this -> URI->_get_post('access_end_min').":".
					$this -> URI->_get_post('access_end_sec');
			$run = "1";
		}
		$user_create = _get_session('UserId');
		if( $this->URI->_get_have_post('user_create_setup')) {
			$user_create = $this -> URI->_get_post('user_create_setup');
		}
		$this->db->set( 'modul_setup_id', $this->_bucket_setup_id );
		$this->db->set( 'st_access_start', $start );
		$this->db->set( 'st_access_end', $end );
		$this->db->set( 'st_access_create_id', $user_create );
		$this->db->set( 'st_autostart', $auto );
		$this->db->set( 'bool_auto_start', $auto );
		$this->db->set( 'bool_running', $run );
		$this->db->set( 'st_create_date_ts', date('Y-m-d H:i:s') );
		if( $this->db->insert('t_st_access_all') )
		{
			$st_access_all_id = $this->db->insert_id();
			foreach($bucket as $index => $bucket_trx_id)
			{
				$this->db->insert('t_gn_access_all',
					array(
						'bucket_trx_id'=>$bucket_trx_id,
						'st_access_all_id' =>$st_access_all_id
					)
				);
			}
		}
		
		
	}
	
	
	
}

// END CLASS 

?>