<?php
/*
 * @ package : under upload model 
 * 
 */

class U_DebiturAccount extends EUI_Upload
{

	/* 
 * @ pack : variable upload 
 */

	var $_upload_table_name = null;
	var $_upload_table_data = null;

	/* 
 * @ pack : variable upload 
 */

	var $_tots_rowselect = 0;
	var $_tots_success = 0;
	var $_tots_failed = 0;
	var $_tots_duplicate = 0;

	/*
 * @ pack : private static $instance  =null
 */

	private $_field_additional = array();
	private $_field_uploadId = 0;
	private $_is_complete = FALSE;
	private $_campaignId = 0;

	/*
 * @ pack : private static $instance  =null
 */

	private static $Instance = null;

	/*
 * @ pack : private get $instance  =null
 */

	public static function &Instance()
	{
		if (is_null(self::$Instance)) {
			self::$Instance = new self();
		}

		return self::$Instance;
	}

	/*
 * @ pack : _reset_argvs
 */

	public function _reset_class_argvs()
	{
		$this->_tots_rowselect = 0;
		$this->_tots_success = 0;
		$this->_tots_failed = 0;
		$this->_campaignId = 0;
		$this->_field_uploadId = 0;
		$this->_tots_duplicate = 0;
		$this->_is_complete = false;
		$this->_upload_table_name = null;
		$this->_upload_table_data = null;
		$this->_field_additional = array();
	}

	/*
 * @ pack : cek duplicate in error result .
 */

	public function _get_class_callback()
	{
		$_call_back = array(
			'TOTAL_UPLOAD' => $this->_tots_rowselect,
			'TOTAL_SUCCES' => $this->_tots_success,
			'TOTAL_FAILED' => $this->_tots_failed,
			'TOTAL_DUPLICATE' => $this->_tots_duplicate
		);

		return (object)$_call_back;
	}

	/*
 * @ pack : cek duplicate in error result .
 */

	private function _is_duplicate_error()
	{
		$_error = mysql_error();
		if (preg_match("/\Dup/i", $_error)) {
			return true;
		} else {
			return false;
		}
	}

	/*
 * @ pack  :  constructor  
 */

	public function U_DebiturAccount()
	{
		$this->_get_campaignId();
		$this->load->model(array('M_SysUser'));
	}

	/*
 * @ pack : Project Id 
 */

	private function _get_ProjectId()
	{
		$CampaignId = $this->_get_campaignId();
		$stdClass = array('ProjectId' => 0, 'CampaignId' => 0);

		if ($CampaignId) {
			$this->db->reset_select();
			$this->db->select("a.ProjectId, a.CampaignId", FALSE);
			$this->db->from("t_gn_campaign_project a ");
			$this->db->where("a.CampaignId", $CampaignId);
			$res = $this->db->get();
			if ($res->num_rows() > 0) {
				if ($rows = $res->result_first_assoc()) {
					$stdClass = array(
						'ProjectId' => $rows['ProjectId'],
						'CampaignId' => $rows['CampaignId']
					);
				}
			}
		}

		return (object)$stdClass;
	}

	/*
 * @ pack  : _set_templateid Ok 
 */

	public function _set_additional($field = null, $values = null)
	{
		if (!is_null($field)) {
			$this->_field_additional[$field] = $values;
		}
	}


	/*
 * @ pack  : _set_templateid Ok 
 */

	public function _get_campaignId()
	{
		if ($this->URI->_get_have_post('CampaignId')) {
			$this->_campaignId = $this->URI->_get_post('CampaignId');
		}
		return $this->_campaignId;
	}


	/*
 * @ pack  : _set_templateid Ok 
 */

	public function _set_uploadId($_set_uploadId = 0)
	{
		if ($_set_uploadId) {
			$this->_field_uploadId = $_set_uploadId;
		}
	}

	/*
 * @ pack  : insert moethode of distribute Ok 
 */

	private function _set_write_bucket()
	{
		if (is_array($this->_upload_table_data)) {

			// @ pack : reset array -----------------------

			$stdClass = $this->_get_ProjectId();
			$array_assoc = array('deb_created_ts' => 'bucket_created_ts');

			foreach ($this->_upload_table_data as $n => $values) {
				$_array_select = null;
				foreach ($values as $field => $value) {
					if (in_array($field, array_keys($array_assoc))) {
						$this->db->set($array_assoc[$field], $value);
					} else {
						$this->db->set($field, $value);
					}

					// @ pack : additional field insert 

					$this->db->set('BukcetSourceId', $this->_field_uploadId);
					$this->db->set('BuketUploadId', $stdClass->ProjectId);
					$this->db->set('deb_cmpaign_id', $stdClass->CampaignId);
					$this->db->set('deb_cardno', $values['deb_acct_no']);
				}

				// insert here additional 

				$this->db->insert("t_gn_bucket_customers", $_array_select);
			}
		}
	}

	/*
 * @ pack  : insert moethode of distribute Ok 
 */

	public function _set_process()
	{

		$AgentCode = null;

		// @ pack : set process save to bucket here  
		$this->_set_write_bucket();

		// @ pack : set process save to debitur
		if (
			is_array($this->_upload_table_data)
			and !is_null($this->_upload_table_name)
		)
			foreach ($this->_upload_table_data as $n => $values) {
				$result_array2 = null;
				$result_array2 = $values;
				// $data = $result_array2->fetch_assoc();
				// var_dump($result_array2['deb_acct_no']);
				// die;

				// $this->_set_additional('deb_cardno',$values['deb_acct_no']); 
				// $this->_set_additional('deb_b_d', $values['deb_wo_date']);
				// var_dump($values);
				unset($values['code_cabang']);
				// die;
				// --------------------- if isset deb_acct_no ---------------------
				if (isset($values['deb_acct_no'])) {
					$this->_set_additional('deb_cardno', $values['deb_acct_no']);
				}

				// --------------------- if isset deb_wo_date ---------------------
				if (isset($values['deb_wo_date'])) {
					$this->_set_additional('deb_b_d', $values['deb_wo_date']);
				}


				// --------------------- if isset deb_amount_wo ---------------------
				if (
					isset($values['deb_amount_wo'])
					and $values['deb_amount_wo'] > 0
				) {
					$this->_set_additional('deb_wo_amount', $values['deb_amount_wo']);
				}



				// look add additional	

				$AgentCode = ($values['deb_agent'] ? $values['deb_agent'] : 0);

				// look add field 

				if (
					is_array($this->_field_additional)
					and count($this->_field_additional) > 0
				) {
					foreach ($this->_field_additional as $field => $value) {
						$values[$field] = $value;
					}
				}

				// @ pack : concate array 

				$this->db->insert($this->_upload_table_name, $values);
				// print_r($this->db->last_query());
				if ($this->db->affected_rows() < 1) {
					if ($this->_is_duplicate_error()) {
						$this->_tots_duplicate += 1;
					} else {
						$this->_tots_failed += 1;
					}
				} else {

					// @ pack : get insert assign ID 

					$conds = $this->_set_assignment($this->db->insert_id(), $AgentCode, $result_array2);
					if ($conds) {
						$this->_tots_success += 1;
					}
				}
			}
	}

	/*
 * @ package : set_assignment data **
 */

	private function _set_assignment($DebiturId = 0, $AgentCode = null, $values = null)
	{
		$conds  = FALSE;
		$UserId = &$this->EUI_Session->_get_session('UserId');
		$Agent  = &$this->M_SysUser->_getUserByCode($AgentCode);




		if ($DebiturId) {
			$this->db->reset_select();
			$this->db->select("*");
			$this->db->from("t_tx_agent");
			$this->db->where("code_cabang", $values['code_cabang']);
			$this->db->where("user_state", 1);
			$this->db->order_by("RAND()");
			$result = $this->db->get()->row_array();

			$this->db->set('CustomerId', $DebiturId);
			$this->db->set('AssignAdmin', $UserId);
			$this->db->set('AssignLeader', $result['tl_id']);
			$this->db->set('AssignSelerId', $result['UserId']);
			$this->db->set('AssignAmgr', 0);
			$this->db->set('AssignMgr', 0);
			$this->db->set('AssignSpv', 0);

			$this->db->set('AssignDate', date('Y-m-d H:i:s'));
			$this->db->insert('t_gn_assignment');
			// print_r($this->db->last_query());

			if ($this->db->affected_rows() > 0) {
				// @ pack : set to assign history log  -----------------------------

				$AssignId = $this->db->insert_id();

				// @ pack : AssignId ------------------------------------------------

				$this->db->set('deb_id', $DebiturId);
				$this->db->set('assign_id', $AssignId);
				$this->db->set('assign_tl_id', $result['tl_id']);
				$this->db->set('assign_dc_id', $result['UserId']);
				$this->db->set('assign_status', 'DIS');
				$this->db->set('assign_type', 'ASSIGN.UPLOAD');
				$this->db->set('assign_log_created_ts', date('Y-m-d H:i:s'));

				// @ pack : if OK 	 -----------------------------------------

				$this->db->insert('t_gn_assignment_log');
				if ($this->db->affected_rows() > 0) {
					$conds = TRUE;
				}
			}
		}

		return $conds;
	}

	/*
 * @ pack : insert moethode of distribute Ok 
 */

	public function _set_table($table = null)
	{
		if (!is_null($table)) {
			$this->_upload_table_name = $table;
		}
	}

	/*
 * @ pack : insert moethode of distribute Ok 
 */

	public function _get_is_complete()
	{
		return $this->_is_complete;
	}

	/*
 * @ pack : insert moethode of distribute Ok 
 */

	public function _set_content($argv = null)
	{
		if (!is_null($argv) and is_array($argv)) {
			$this->_upload_table_data = $argv;
			$this->_set_additional('deb_created_ts', date('Y-m-d H:i:s'));
			$this->_set_additional('deb_cmpaign_id', $this->_campaignId);
			$this->_set_additional('deb_upload_id', $this->_field_uploadId);
			$this->_tots_rowselect = count($argv);
			$this->_is_complete  = true;
		}
	}
}

// END CLASS 
