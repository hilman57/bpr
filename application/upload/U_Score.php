<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_Score extends EUI_Upload 
{

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

/*
 * @ pack : private static $instance  =null
 */

 private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/*
 * @ pack : cek duplicate in error result .
 */
 
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
private function _is_duplicate_error()
{
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack  :  constructor  
 */
 
public function U_Score() 
{ 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
private function _set_write_bucket()
{
 if( is_array($this->_upload_table_data)) 
 {

 // @ pack : reset array -----------------------
	
	$stdClass = $this->_get_ProjectId();
	$array_assoc = array('deb_created_ts'=>'bucket_created_ts');
	
	foreach( $this->_upload_table_data as $n => $values )
	{
		$_array_select = null;
		foreach( $values as $field => $value )
		{ 
			if( in_array($field, array_keys($array_assoc) ) )  {
				$this->db->set($array_assoc[$field], $value);
			} else {
				$this->db->set($field, $value);	
			}
			
		// @ pack : additional field insert 
		
			$this->db->set('BukcetSourceId', $this->_field_uploadId);
			$this->db->set('BuketUploadId',$stdClass->ProjectId);
			$this->db->set('deb_cmpaign_id', $stdClass->CampaignId);	
			$this->db->set('deb_cardno', $values['deb_acct_no']);	
		}
		
		// insert here additional 
		
		$this->db->insert("t_gn_bucket_customers", $_array_select);
			
	}
	
 }

} 

/*
 * @ pack  : insert moethode of distribute Ok 
 */

function logScoreQuery($qry = null) {
	$location = '/opt/enigma/webapps/hsbc/application/log_score/log_query_'.date("Y-m-d").'.txt';
	if(!file_exists($location)) {
		$content = $qry;
		$fh = fopen($location, 'w') or die("can't open file");
		fwrite($fh, $content);
		fclose($fh);
	} else {
		$readFile = fopen($location, "r") or die("Unable to open file!");
		$content = fread($readFile, filesize($location));
		$concatText = $content."\n".$qry;
		fclose($readFile);
		$writeFile = fopen($location, "w") or die("Unable to open file!");
		fwrite($writeFile, $concatText);
		fclose($writeFile);
	}
}

function logScoreCardno($cardno = null) {
	$location = '/opt/enigma/webapps/hsbc/application/log_score/log_cardno_'.date("Y-m-d").'.txt';
	if(!file_exists($location)) {
		$content = $cardno;
		$fh = fopen($location, 'w') or die("can't open file");
		fwrite($fh, $content);
		fclose($fh);
	} else {
		$readFile = fopen($location, "r") or die("Unable to open file!");
		$content = fread($readFile, filesize($location));
		$concatText = $content."\n".$cardno;
		fclose($readFile);
		$writeFile = fopen($location, "w") or die("Unable to open file!");
		fwrite($writeFile, $concatText);
		fclose($writeFile);
	}
}

public function _set_process() 
{
 
  $AgentCode = null;
  
// @ pack : set process save to bucket here  
//   $this->_set_write_bucket();
 
// @ pack : set process save to debitur
  if( is_array($this->_upload_table_data) 
	AND !is_null($this->_upload_table_name) ) 
	foreach( $this->_upload_table_data as $n => $values ) 
 {
	//  var_dump($values['score_month']);
	 $score_month = str_replace("/","-",$values['score_month']);
	 $score_month;
	 $sm = explode("-",$score_month);
	//  var_dump ($sm[0]);
	if($sm[0] == 1 || $sm[0] == 01){
		$bln = "Januari";
	}elseif ($sm[0] == 2 || $sm[0] == 02) {
		$bln = "Februari";
	}elseif ($sm[0] == 3 || $sm[0] == 03) {
		$bln = "Maret";
	
	}elseif ($sm[0] == 4 || $sm[0] == 04) {
		$bln = "April";
	
	}elseif ($sm[0] == 5 || $sm[0] == 05) {
		$bln = "Mei";
	
	}elseif ($sm[0] == 6 || $sm[0] == 06) {
		$bln = "Juni";
	
	}elseif ($sm[0] == 7 || $sm[0] == 07) {
		$bln = "Juli";
	
	}elseif ($sm[0] == 8 || $sm[0] == 08) {
		$bln = "Agustus";
	
	}elseif ($sm[0] == 9 || $sm[0] == 09) {
		$bln = "September";
	
	}elseif ($sm[0] == 10 || $sm[0] == 010) {
		$bln = "Oktober";
	
	}elseif ($sm[0] == 11 || $sm[0] == 011) {
		$bln = "November";
	
	}elseif ($sm[0] == 12 || $sm[0] == 012) {
		$bln = "Desember";
	}
	// var_dump($bln)."<br>";
	// $sm2 = explode("-",$score_month);
	// var_dump($sm[1])."<br>";
	$hasil = $bln." ".$sm[1];
	$date = date("Y-m-d h:i:s");
	// $usr = $this->EUI_Session->_get_session('UserId');
	$usr =  $this -> EUI_Session -> _get_session('Username');
	$this->db->set('score', $values['score']);
	$this->db->set('score_month', $hasil);
	$this->db->set('deb_cardno', $values['CARDNO']);
	$this->db->set('create_date', $date);
	$this->db->set('uploadby', $usr);
	$this->db->insert('log_t_gn_debitur');
	// var_dump($bln."-".$sm[1]);
	//  die;
	$this->db->set('score', $values['score']);
	$this->db->set('score_month', $hasil);
	$this->db->where('deb_cardno', $values['CARDNO']);
	$update = $this->db->update('t_gn_debitur');




	$sql = "UPDATE `t_gn_debitur` SET `score` = ".$values['score'].", `score_month` = '".$hasil."'     WHERE `deb_cardno` =  '".$values['CARDNO']."'";
	// echo $sql;
	// die;
	if($this->db->affected_rows() == '1'){
	$this->_tots_success +=1;
	}else{
		$this->db->where('deb_cardno', $values['CARDNO']);
		$cek = $this->db->get('t_gn_debitur')->num_rows();
		if($cek == 0) {
			$this->_tots_failed +=1; 
			$this->logScoreQuery($sql);
			$this->logScoreCardno($values['CARDNO']);
		} else {
			$this->_tots_success +=1;
		}
	}
 }
 
}
 
public function _set_process_lama() 
{
 
  $AgentCode = null;
  
// @ pack : set process save to bucket here  
//   $this->_set_write_bucket();
 
// @ pack : set process save to debitur
  if( is_array($this->_upload_table_data) 
	AND !is_null($this->_upload_table_name) ) 
	foreach( $this->_upload_table_data as $n => $values ) 
 {
	$this->db->set('score', $values['score']);
	$this->db->where('deb_cardno', $values['CARDNO']);
	$this->db->update('t_gn_debitur');
	$sql = "UPDATE `t_gn_debitur` SET `score` = ".$values['score']." WHERE `deb_cardno` =  '".$values['CARDNO']."'";
	if($this->db->affected_rows() == '1'){
	$this->_tots_success +=1;
	}else{
		$this->db->where('deb_cardno', $values['CARDNO']);
		$cek = $this->db->get('t_gn_debitur')->num_rows();
		if($cek == 0) {
			$this->_tots_failed +=1; 
			$this->logScoreQuery($sql);
			$this->logScoreCardno($values['CARDNO']);
		} else {
			$this->_tots_success +=1;
		}
	}
 }
 
}

/*
 * @ package : set_assignment data **
 */
 
 private function _set_assignment( $DebiturId = 0 , $AgentCode=null )
 {
	$conds  = FALSE;
	$UserId =& $this->EUI_Session->_get_session('UserId');
	$Agent  =& $this->M_SysUser->_getUserByCode($AgentCode);
	
	if( $DebiturId ) 
	{
		$this->db->set('CustomerId',$DebiturId);
		$this->db->set('AssignAdmin',$UserId);
		$this->db->set('AssignLeader',$Agent['tl_id']);
		$this->db->set('AssignSelerId',$Agent['UserId']);
		$this->db->set('AssignAmgr',0);
		$this->db->set('AssignMgr',0);
		$this->db->set('AssignSpv',0);
		
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->insert('t_gn_assignment');
		if( $this->db->affected_rows() > 0 )
		{
		  // @ pack : set to assign history log  -----------------------------
			
			$AssignId = $this->db->insert_id();
			
		  // @ pack : AssignId ------------------------------------------------
		  
			$this->db->set('deb_id',$DebiturId);
			$this->db->set('assign_id',$AssignId);
			$this->db->set('assign_tl_id',$Agent['tl_id']);
			$this->db->set('assign_dc_id',$Agent['UserId']);
			$this->db->set('assign_status','DIS');
			$this->db->set('assign_type','ASSIGN.UPLOAD');
			$this->db->set('assign_log_created_ts', date('Y-m-d H:i:s'));
		  
		 // @ pack : if OK 	 -----------------------------------------
		 
			$this->db->insert('t_gn_assignment_log');
			if( $this->db->affected_rows() > 0 )
			{
				$conds = TRUE;
			}
		}
	}
	
	return $conds;
 }

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	// $this->_set_additional('deb_created_ts', date('Y-m-d H:i:s'));
	// $this->_set_additional('deb_cmpaign_id', $this->_campaignId);
	// $this->_set_additional('deb_upload_id', $this->_field_uploadId);
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
	
  }
}

}

// END CLASS 

?>