<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_AdditionalAddress extends EUI_Upload 
{

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

/*
 * @ pack : private static $instance  =null
 */

 private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_tots_nomaster = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/*
 * @ pack : cek duplicate in error result .
 */
 
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_NOMASTERDEB_FOUND' => $this->_tots_nomaster,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	// print_r($_call_back);
	// exit();
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
private function _is_duplicate_error()
{
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack  :  constructor  
 */
 
public function U_AdditionalAddress() 
{ 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
private function _set_write_bucket()
{
 if( is_array($this->_upload_table_data)) 
 {

 // @ pack : reset array -----------------------
	
	$stdClass = $this->_get_ProjectId();
	$array_assoc = array('deb_created_ts'=>'bucket_created_ts');
	
	foreach( $this->_upload_table_data as $n => $values )
	{
		$_array_select = null;
		foreach( $values as $field => $value )
		{ 
			if( in_array($field, array_keys($array_assoc) ) )  {
				$this->db->set($array_assoc[$field], $value);
			} else {
				$this->db->set($field, $value);	
			}
			
		// @ pack : additional field insert 
		
			$this->db->set('BukcetSourceId', $this->_field_uploadId);
			$this->db->set('BuketUploadId',$stdClass->ProjectId);
			$this->db->set('deb_cmpaign_id', $stdClass->CampaignId);	
			$this->db->set('deb_cardno', $values['deb_acct_no']);	
		}
		
		// insert here additional 
		
		$this->db->insert("t_gn_bucket_customers", $_array_select);
			
	}
	
 }

} 

/*
 * @ pack  : insert additional debitur address
 */
private function MasterDebitur($Accno = null){
	// $CampaignId = $this->_get_campaignId();
	$stdClass = array('DebId' => 0, 'CampaignId' => 0 );
	
	// if( $CampaignId ){
		$this->db->reset_select();
		$this->db->select("a.deb_id ", FALSE);
		$this->db->from("t_gn_debitur a ");
		$this->db->where("a.deb_acct_no", $Accno);
		// $this->db->where("a.deb_cmpaign_id", $CampaignId);
		// echo "<pre>";
		// echo $this->db->_get_var_dump();
		// echo "</pre>"; exit;
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'DebId' => $rows['deb_id'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	// }
	
	return (object)$stdClass;
}

private function UpdateAdditionalAddress( $xls= null ){
	$conds = 0; 
	if( !is_null($xls) AND is_array($xls) ){
		$this->db->set('ALAMAT',$xls['ALAMAT']);
		$this->db->set('PROP_NAME',$xls['PROP_NAME']);
		$this->db->set('KAB_NAME',$xls['KAB_NAME']);
		$this->db->set('KEC_NAME',$xls['KEC_NAME']);
		$this->db->set('NO_KEL_DUKCAPIL',$xls['NO_KEL_DUKCAPIL']);
		$this->db->set('KEL_NAME',$xls['KEL_NAME']);
		$this->db->set('NO_RT',$xls['NO_RT']);
		$this->db->set('NO_RW',$xls['NO_RW']);
		$this->db->set('UpdatedDate',date('Y-m-d H:i:s'));
		$this->db->where('deb_acct_no',$xls['CardNo']);

		$this->db->update('t_gn_debitur_additional_address');
		if($this->db->affected_rows()>0 ){
			$conds++;
		}
	}
	
	return $conds;
}

public function _set_process(){
	
	$AgentCode = null;
	$MasterFlag = null;
	// @ pack : set process save to bucket here  
	// $this->_set_write_bucket();

	// @ pack : set process save to debitur
	if( is_array($this->_upload_table_data) AND !is_null($this->_upload_table_name) )
		foreach( $this->_upload_table_data as $n => $values ){
			$DebId = $this->MasterDebitur($values['Accno']);
			if($DebId->DebId){
				$this->_set_additional('MasterFlag', 0);
				$MasterFlag = 0;
			}else{
				$this->_set_additional('MasterFlag', 1);
				$MasterFlag = 1;
				$this->_tots_nomaster +=1;
			}
			// print_r($values);
			// exit();
			$this->_set_additional('CreatedDate', date('Y-m-d H:i:s'));
			
			// look add additional
			// $AgentCode = ( $values['deb_agent'] ? $values['deb_agent'] : 0 );
			// look add field	
			if( is_array($this->_field_additional) AND count($this->_field_additional)>0 ){
				foreach( $this->_field_additional as $field => $value ){
					$values[$field] = $value;
				}
			}

			// @ pack : concate array 
			$this->db->insert( $this->_upload_table_name, $values );
			if($this->db->affected_rows()<1){
				$updateAddAddress = $this->UpdateAdditionalAddress($values);
				if( !$updateAddAddress ){
					$this->_tots_duplicate +=1;
				}
			}
			else{
				// @ pack : get insert assign ID
				// $conds = $this->_set_assignment( $this->db->insert_id(), $AgentCode);
				// if( $conds ) {
					if(!$MasterFlag){
						$this->_tots_success +=1;
					}else{
						$this->_tots_failed +=1;
					}
			}
	}
}

/*
 * @ package : set_assignment data **
 */
 
 private function _set_assignment( $DebiturId = 0 , $AgentCode=null )
 {
	$conds  = FALSE;
	$UserId =& $this->EUI_Session->_get_session('UserId');
	$Agent  =& $this->M_SysUser->_getUserByCode($AgentCode);
	
	if( $DebiturId ) 
	{
		$this->db->set('CustomerId',$DebiturId);
		$this->db->set('AssignAdmin',$UserId);
		$this->db->set('AssignLeader',$Agent['tl_id']);
		$this->db->set('AssignSelerId',$Agent['UserId']);
		$this->db->set('AssignAmgr',0);
		$this->db->set('AssignMgr',0);
		$this->db->set('AssignSpv',0);
		
		$this->db->set('AssignDate', date('Y-m-d H:i:s'));
		$this->db->insert('t_gn_assignment');
		if( $this->db->affected_rows() > 0 )
		{
		  // @ pack : set to assign history log  -----------------------------
			
			$AssignId = $this->db->insert_id();
			
		  // @ pack : AssignId ------------------------------------------------
		  
			$this->db->set('deb_id',$DebiturId);
			$this->db->set('assign_id',$AssignId);
			$this->db->set('assign_tl_id',$Agent['tl_id']);
			$this->db->set('assign_dc_id',$Agent['UserId']);
			$this->db->set('assign_status','DIS');
			$this->db->set('assign_type','ASSIGN.UPLOAD');
			$this->db->set('assign_log_created_ts', date('Y-m-d H:i:s'));
		  
		 // @ pack : if OK 	 -----------------------------------------
		 
			$this->db->insert('t_gn_assignment_log');
			if( $this->db->affected_rows() > 0 )
			{
				$conds = TRUE;
			}
		}
	}
	
	return $conds;
 }

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	// $this->_set_additional('deb_created_ts', date('Y-m-d H:i:s'));
	// $this->_set_additional('deb_cmpaign_id', $this->_campaignId);
	$this->_set_additional('UploadId', $this->_field_uploadId);
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
	
  }
}

}

// END CLASS 

?>