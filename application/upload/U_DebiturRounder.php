<?php
/**
	truncate t_gn_debitur;
	truncate t_gn_debitur_deleted;
	truncate t_gn_debitur_deleted_log;
	truncate t_gn_bucket_customers;
	truncate t_gn_callhistory;
	truncate t_gn_callhistory_test;
	truncate t_gn_siklus_data;
	truncate t_gn_siklus_report;
	truncate t_gn_siklus_user;
	truncate t_gn_assignment;
	truncate t_gn_assignment_log;
	
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
date_default_timezone_set("Asia/Jakarta"); 
class U_DebiturRounder extends EUI_Upload  {
	
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 
 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;
 private static $Instance = null;


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
public static function &Instance() {
  if( is_null(self::$Instance))  {
	 self::$Instance = new self();
  }
  return self::$Instance;
}


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
public function _reset_class_argvs()  {	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
private function _is_duplicate_error() {
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function U_DebiturAccount()  { 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 private function _get_ProjectId() {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _set_additional( $field = null, $values=null  ){
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 
function _get_campaignId()  {
 if( $this->URI->_get_have_post('CampaignId'))  {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 
function _set_uploadId( $_set_uploadId = 0 )  {
	if( $_set_uploadId ) {
		$this->_field_uploadId= $_set_uploadId;
	}	
 }

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
private function _set_write_bucket() {
	
 if(!is_array($this->_upload_table_data)){
	 return 0;
 }
 // get project data ID :
  $stdClass = $this->_get_ProjectId();
  $array_assoc = array('deb_created_ts'=>'bucket_created_ts');
  foreach($this->_upload_table_data as $n => $values ) {
		// on test process :
		if( !is_array($values)){
			continue; // skip 
		}
		
		// if data on array :
		$resultArray = null;
		$this->db->reset_write(); // cleaer : 
		foreach($values as $field => $value ) { 
			// finding key on array map: 
			if( @in_array($field, array_keys($array_assoc)))  {
				$this->db->set($array_assoc[$field], $value);
			} 
			// if finding process invalid :
			else if(!@in_array($field, array_keys($array_assoc)))  {
				$this->db->set($field, $value);	
			}
			
			$this->db->set('BukcetSourceId', $this->_field_uploadId);
			$this->db->set('BuketUploadId',$stdClass->ProjectId);
			$this->db->set('deb_cmpaign_id', $stdClass->CampaignId);	
			$this->db->set('deb_cardno', $values['deb_acct_no']);	
		}
		// then back up:
		$this->db->insert("t_gn_bucket_customers", $resultArray);	
  }
  
} 
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function reader( $param = null ){
	if(!is_array($param)){
	   $param = array(); // default :
	}
	// then test :
	if(class_exists('EUI_Object')){
		return new EUI_Object($param);
	}
	return 0;
} 
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function check_periode( $periode = null ){
	$tot = 0;
	$sql = sprintf("select count(a.id) as tot from t_gn_siklus_user a where a.periode = '%s'", $periode);
	$qry = $this->db->query($sql);
	if( $qry &&($row = $qry->result_first_assoc())){
		$tot = (int)$row['tot'];
	}
	return $tot;
} 

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function import_user( $periode = null ){
	// check data on periode 
	$resultArray = array();
	$resultData = 0;

	// $sql = sprintf("select a.id as user, 1 as rank, 1 as siklus, %s as periode 
	// 				from t_tx_agent a where a.profile_id = 4 and a.user_state = 1 order by a.UserId asc limit %d ", $periode, 10);

	
	// $sql = sprintf("select user, rank, 1 as siklus, %s as periode 
					// from t_gn_ranking order by id ASC ", $periode, 10);
					
	// $sql = sprintf("select user, rank, 1 as siklus, %s as periode 
	// 				from t_gn_ranking order by rank ASC, id ASC limit 5", $periode, 10);
	$sql = sprintf("select user, rank, 1 as siklus, %s as periode 
					from t_gn_ranking order by rank ASC, id ASC ", $periode, 10);
	//echo $sql;
	
	$qry = $this->db->query($sql);
	if($qry && $qry->num_rows()>0) 
	foreach( $qry->result_array() as $row ){
		$this->db->reset_write();
		$this->db->set('user', $row['user']);
		$this->db->set('rank', $row['rank']);
		$this->db->set('siklus', $row['siklus']);
		$this->db->set('periode', $row['periode']);
		if( $this->db->insert('t_gn_siklus_user') ){
			$resultData++;
		}
	}
	// return data : 
	return (int)$resultData;
}
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function round_size($param){
	 if(is_array($param) 
	 &&( $total = count($param)>0)){
		 return (int)$total;
	 }
	 return 0;
 }
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function round_header(){ 
	$header = array();
	$sql = "select a.top, a.low, a.`user` as stop, a.siklus from t_gn_siklus_report a";
	$qry = $this->db->query($sql);
	
	if($qry && $qry->num_rows()>0 
	&&($row = $qry->result_first_assoc())){
		$header['top'] = $row['top'];
		$header['low'] = $row['low'];
		$header['stop'] = $row['stop'];
		$header['siklus'] = $row['siklus'];
	}
	
	if(!$this->round_size($header)) return 0;
	else {
		return (array)$header;
	}
 }
 /**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function round_toplow(){ 
	$toplow = array();
	$sql = "select 
			(select t.user from t_gn_siklus_user t where t.id = min(a.id)) as top,
			(select t.user from t_gn_siklus_user t where t.id = max(a.id)) as low from t_gn_siklus_user a limit 1 ";	
	$qry = $this->db->query($sql);
	if($qry && $qry->num_rows()>0 
	&&($row = $qry->result_first_assoc())){
		$toplow['top'] = $row['top'];
		$toplow['low'] = $row['low'];
	}		
	// return data ""
	if(!$this->round_size($toplow)) return 0;
	else {
		return (array)$toplow;
	}
}
 /**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function round_setup( $stop = null ){
    $setup = array();	 
	$sql= "select 
		   (select b.siklus from t_gn_siklus_user b where b.id = (a.id-1)) as top,
		   (select t.siklus from t_gn_siklus_user t where t.id = (a.id+0)) as mdl,
		   (select t.siklus from t_gn_siklus_user t where t.id = (a.id+1)) as low
		   from t_gn_siklus_user a where a.`user` ='$stop'";
	$qry = $this->db->query($sql);
	if($qry && $qry->num_rows()>0 
	&&($row = $qry->result_first_assoc())){
		$setup['top'] = (int)$row['top'];
		$setup['mid'] = (int)$row['mdl'];
		$setup['low'] = (int)$row['low'];   
	}	
	// return data :
	return (array)$setup;
 }
 
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 
 function round_data(){ 
	$data = array();
	$sql = "select a.deb_id as data from t_gn_debitur a where a.deb_lock_rata = 1 order by a.deb_wo_amount desc";
	$qry = $this->db->query($sql);
	if( $qry && $qry->num_rows()>0 ) 
	foreach( $qry->result_assoc() as $row ){
		$data[] = (string)$row['data'];
	}
	return (array)$data;
 } 
 
 /**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function round_olddata( $newData = null ){
	
	$oldData = array();
	$sql = "select a.id, a.`data` as datas from t_gn_siklus_data a Order by a.id ASC";
	$qry = $this->db->query($sql);
	if( $qry ) foreach($qry->result_assoc() as $i => $row ){
		$oldData[] = $row['datas'];
	}
	
	// gabungkan datanya :
	$nextid = ((count($oldData)-1));
	foreach( $newData as $i => $val ){
		$nextid++; // nextdata index:
		$oldData[$nextid] = $val;
	}
	// then --------------------------- 
	return $oldData;
} 
 
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function round_user(){ 
	$user = array();
	$sql = "select a.user from t_gn_siklus_user a 
			where a.periode = date_format(curdate(),'%Y%m') 
			order by a.id asc";
			
	$qry = $this->db->query($sql);
	if( $qry && $qry->num_rows()>0 ) 
	foreach( $qry->result_assoc() as $row ){
		$user[] = (string)$row['user'];
	}
	return (array)$user;
}
 /**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function round_assign(){ 
	$ptr = 0;
	$sql = sprintf("select a.id, a.user, a.`data` from t_gn_siklus_data a where a.proc = %d", 0);
	$qry = $this->db->query($sql);
	if( $qry && $qry->num_rows()>0) 
	foreach( $qry->result_assoc() as $row ){
		// get detail user :
		$new = new stdClass();
		$new->p1 = (string)$row['user']; 
		$new->p2 = (int)$row['data'];
		$new->p3 = (int)$row['id'];
		
		// then 
		$src = $this->M_SysUser->_getUserByCode($new->p1);
		$src = $this->reader($src);
		if(!$src->find('UserId')){
		   continue;
		}
		
		// then :
		$this->db->reset_write();
		$this->db->set('AssignSelerId', $src->get('UserId'));
		$this->db->set('AssignLeader', $src->get('tl_id'));
		$this->db->where('CustomerId', $new->p2);
		if($this->db->update('t_gn_assignment')){
			
			// update debitur 
			$this->db->reset_write();
			$this->db->set('deb_agent',$new->p1);
			$this->db->set('deb_cmpaign_id',1);
			$this->db->where('deb_id', $new->p2);
			$this->db->update('t_gn_debitur');
			
			// update siklus data :
			$this->db->reset_write();
			$this->db->set('proc',1);
			$this->db->where('id', $new->p3);
			$this->db->update('t_gn_siklus_data');
			$ptr++;
		}
	}
	// return data OK:
	return(int)$ptr;
}
 
 
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function round_process( $round = null ){
 $cond = 0;
 
  // di assumsikan data dengan satu file walaupun beda file sehingga menjadi 
 // dist_data = ((old_data)+new_data);
 $round->data = $this->round_olddata($round->data); // overide _data with all data :
 $skl = new siklus($round->user);
 $fetch = $skl->Rounder($round->user, $round->data, $round->top, $round->low, $round->freq, $round->stop, $round->setup);
 if(!$fetch){
	 return 0;
 }
 
 // return 0;
 $dataAssoc = $fetch->data();
 foreach($dataAssoc as $index => $row){
	// skip if not data array:
	if(!$this->round_size($row)){
		continue;
	}
	
	// create_function object 
	$item = new stdClass();
	$item->user   = $row['d1'];
	$item->data   = $row['d0'];
	$item->siklus = $row['d2'];
	$item->date   = date('Y-m-d H:i:s');
	$item->type   = 'CD';
	$item->size  = 0;
	 
	 
	$this->db->reset_write();
	$this->db->set('user',$item->user );
	$this->db->set('data', $item->data);
	$this->db->set('type', $item->type);
	$this->db->set('date', $item->date );
	$this->db->set('siklus', $item->siklus);
	if($this->db->insert('t_gn_siklus_data')){
		// update deb_lock_rata ==0 
		$sql = array( sprintf("update t_gn_debitur a set a.deb_lock_rata=0 where a.deb_id=%d", $item->data),
				      sprintf("update t_gn_siklus_user set siklus=%d where user='%s'", $item->siklus, $item->user));
		foreach($sql as $i => $val ){
			$this->db->query($val);
			$item->size++;
		}	   
	}
	
 }
	
 // update report 
 $sql = sprintf("INSERT INTO t_gn_siklus_report(type, top, low, user, siklus, `update`) VALUES('CD', '%s', '%s', '%s', '%d', NOW())", $round->top, $round->low, $skl->user(), $skl->counter());
 if(!$this->db->query($sql)){
	$sql = sprintf("update siklus_report set user= '%s',  siklus = '%s', `update` = NOW()  where type='CD'", $skl->user(), $skl->counter());
	$this->db->query($sql);
 }
 
 // setelah process pembagian selesai baru di lakukan assign data 
 // ke table assignment 
  $this->round_assign($this);
 
}
 
/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _set_process()  {
	
  //$AgentCode = null;
  
  // import data untuk bcuket bagi rata dengan methode balance process : ini sampling aja 
  $periode = date('Ym');
  if(!$this->check_periode($periode)){
	 $this->import_user($periode);
  }
   
  // write to bucket if exist : 
  $this->_set_write_bucket();
  if(is_array($this->_upload_table_data) &&(!is_null($this->_upload_table_name))) 
  foreach($this->_upload_table_data as $n => $mArray){
	  
	// create new object :
	$itm = new stdClass();
	$src = $this->reader($mArray);
	if($src->find('deb_acct_no')){
	  $this->_set_additional('deb_cardno', $src->get('deb_acct_no'));
	}	
	
	// check Wo_Date 
	if($src->find('deb_wo_date')) {
		$this->_set_additional('deb_b_d', $src->get('deb_wo_date'));
	}	
	
	// check wo_amount:
	if($src->find('deb_amount_wo') &&($src->get('deb_amount_wo')>0)){
		$this->_set_additional('deb_wo_amount', $src->get('deb_amount_wo'));
	}	
	
	// look add additional	on string 
	if($src->find('deb_agent')){
	 // unset data agent karna tidak perlu :
		unset($mArray['deb_agent']);
	}
	
	//add lock table :
	if(!$src->find('deb_lock_rata')){
		$this->_set_additional('deb_lock_rata', 1);
	}
	
	// check additional data : 
	if(is_array($this->_field_additional) 
	&&(count($this->_field_additional)>0)){
		foreach( $this->_field_additional as $field => $value ){
			$mArray[$field] = $value;
		}
	}
	
	// then set data from  here [jika process insert berhasil]
	$this->db->insert($this->_upload_table_name, $mArray);
	if(is_object($this->db) &&($this->db->affected_rows() > 0)){
		if($itm->p0 = $this->db->insert_id()){
			$this->_set_assignment($itm->p0, null);
			$this->_tots_success +=1;
		}
	}
	// try catch error from mysql _respoonse : 
	else if( $err = @mysql_errno() &&(strcasecmp( @mysql_error(),'Duplicate'))){
		$this->_tots_duplicate+=1;
	}
	// other error :
	else if( $err = @mysql_errno() &&(!strcasecmp( @mysql_error(),'Duplicate'))){
		$this->_tots_failed+=1;
	}
 }
 
// kemudian process assignment dengan methode rounder :
   $round = new stdClass();
   $round->data  = array();
   $round->user  = array();
   $round->setup = array();
   $round->top   = null;
   $round->low   = null;
   $round->stop  = null;
   $round->freq  = 0;
   
 // ambil posisi report terakhir :
  if($r = $this->round_header()){
	$round->top  = $r['top'];
	$round->low  = $r['low'];
	$round->stop = $r['stop'];
	$round->freq = $r['siklus'];
  }
  
 // ambil posisi index taratas && terbawah
  if($r = $this->round_toplow()){
	$round->top  = $r['top'];
	$round->low  = $r['low'];
  }
  
 // untuk menetukan posisi naik / turun 
  if($r = $this->round_setup($round->stop)){
	$round->setup['top'] = $r['top'];
	$round->setup['mid'] = $r['mdl'];
	$round->setup['low'] = $r['low'];
  }
  
  
  // process on data rounder :
  $round->data = $this->round_data();
  if(count($round->data) &&($round->user = $this->round_user())){
	  $this->round_process($round);
  }
}


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 private function _set_assignment( $DebiturId = 0, $deskoll=null ) {
	 
// create new object :	 
	$src = new stdClass();
	$src->p0 = (int)$DebiturId;  // debitur
	$src->p1 = (string)$deskoll; // deskoll
	$src->p2 = (int)$this->EUI_Session->_get_session('UserId'); //user upload 
	$src->p3 = date('Y-m-d H:i:s');
	$src->p5 = 0;
	
// check data :
	if(!$src->p0 ){
	   return 0;
    }
	
	// reset cache data on after insert DB :
	$this->db->reset_write();
	$this->db->set('AssignDate',$src->p3);
	$this->db->set('CustomerId',$src->p0);
	$this->db->set('AssignAdmin',$src->p2);
	$this->db->set('AssignLeader', 0);
	$this->db->set('AssignSelerId',0);
	$this->db->set('AssignAmgr',0);
	$this->db->set('AssignMgr',0);
	$this->db->set('AssignSpv',0);
	
	$this->db->insert('t_gn_assignment');
	if(is_object($this->db) &&($this->db->affected_rows() > 0)){
		// insert into log  asssign :
		$src->p4 = $this->db->insert_id();
		if(!$src->p4) return 0;
		
		// then if insert OK :
		$this->db->reset_write();
		$this->db->set('deb_id',$src->p0);
		$this->db->set('assign_id',$src->p4);
		$this->db->set('assign_log_created_ts', $src->p3);
		$this->db->set('assign_tl_id',0);
		$this->db->set('assign_dc_id',0);
		$this->db->set('assign_status','DIS');
		$this->db->set('assign_type','ASSIGN.UPLOAD');
		$this->db->insert('t_gn_assignment_log');
		
		//check connection DB process :
		if(is_object($this->db) 
		&&($this->db->affected_rows()>0)){
			$src->p5++;
		}
	} 
	// return back :
	return (int)$src->p5;
 }


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
public function _get_is_complete()
{
	return $this->_is_complete;
}


/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
public function _set_content( $argv = null )
{
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	$this->_set_additional('deb_created_ts', date('Y-m-d H:i:s'));
	$this->_set_additional('deb_cmpaign_id', $this->_campaignId);
	$this->_set_additional('deb_upload_id', $this->_field_uploadId);
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
	
  }
}

}

// END CLASS 

?>