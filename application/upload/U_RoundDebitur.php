<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_RoundDebitur extends EUI_Upload 
{
	/* 
	 * @ pack : variable upload 
	 */
	 
	 var $_upload_table_name = null;
	 var $_upload_table_data = null;

	 
	/* 
	 * @ pack : variable upload 
	 */
	 
	 var $_tots_rowselect = 0;
	 var $_tots_success = 0;
	 var $_tots_failed = 0;
	 var $_tots_duplicate = 0;
	 
	/*
	 * @ pack : private static $instance  =null
	 */

	private $_field_additional = array(); 
	private $_field_uploadId = 0;
	private $_is_complete = FALSE;
	private $_campaignId = 0;
	private $_bucket_setup_id = null ;
	private $round_upload = 3;
	 
	private static $Instance = null;

	/*
	 * @ pack : private get $instance  =null
	 */
	 
	public static function &Instance()
	{
		if( is_null(self::$Instance) ) 
		{
			self::$Instance = new self();
		}

		return self::$Instance;
	}
	
	/*
	 * @ pack  :  constructor  
	 */
	 
	public function __construct() 
	{ 
		// $this->_get_campaignId();
		$this->load->model(array('M_MgtRandDeb'));
	}
	
	/*
	 * @ pack : _reset_argvs
	 */
	 
	public function _reset_class_argvs() 
	{	
	 $this->_tots_rowselect = 0;
	 $this->_tots_success = 0;
	 $this->_tots_failed = 0;
	 $this->_campaignId = 0;
	 $this->_field_uploadId= 0;
	 $this->_tots_duplicate = 0;
	 $this->_is_complete = false;
	 $this->_upload_table_name = null;
	 $this->_upload_table_data= null;
	 $this->_field_additional = array(); 
	 $this->_bucket_setup_id = null;
	 
	}
	
	 /*
	 * @ pack : cek duplicate in error result .
	 */
 
	private function _is_duplicate_error()
	{
		$_error = mysql_error();
		if(preg_match("/\Dup/i", $_error)) {
			return true;
		}	
		else{
			return false;
	  }
	}
	public function _get_class_callback(){
		$_call_back = array (
			'TOTAL_UPLOAD' => $this->_tots_rowselect,
			'TOTAL_SUCCES' => $this->_tots_success,
			'TOTAL_FAILED' => $this->_tots_failed,
			'TOTAL_DUPLICATE' => $this->_tots_duplicate 
		);
		
		return (object)$_call_back;
	}
	
	/*
	 * @ pack  : _set_templateid Ok 
	 */
 
	public function _set_uploadId( $_set_uploadId = 0 ) 
	{
		if( $_set_uploadId ) 
		{
			$this->_field_uploadId= $_set_uploadId;
		}	
	}
	/*
	 * @ pack  : _set_templateid Ok 
	 */
	 
	public function _set_additional( $field = null, $values=null  ) 
	 {
		if( !is_null($field) )
		{
			$this->_field_additional[$field] = $values;
		}	
	 }
	
	/*
	 * @ pack : insert moethode of distribute Ok 
	 */
	 
	public function _set_table( $table = null )
	{
	  if( !is_null( $table ) ) 
	  {
		 $this->_upload_table_name = $table;
	  }
	}
	/*
	 * @ pack : insert moethode of distribute Ok 
	 */
	 
	public function _get_is_complete()
	{
		return $this->_is_complete;
	}

	/*
	 * @ pack : insert moethode of distribute Ok 
	 */
	 
	public function _set_content( $argv = null )
	{
		//$CampaignId = self::_get_campaignId();
		
	  if( !is_null($argv) AND is_array($argv) )
	  {
		$this->_upload_table_data = $argv;
		$this->_tots_rowselect = count($argv);
		$this->_is_complete  = true;
	  }
	}
	
	/*
	 * @ pack  : insert moethode of distribute Ok 
	 */
	 
	public function _set_process() 
	{
		// insert setup
		$this->save_setup_round();
		
		// @ pack : set process save to debitur
   
		if( is_array($this->_upload_table_data) 
			AND !is_null($this->_upload_table_name) )
		{
			// print_r($this->_upload_table_data);
			foreach( $this->_upload_table_data as $n => $values ) 
			{
			
				// @ unset : DC ID then replace with its 
				$Debitur = $this->_getDebiturId(trim($values['acc_no']));
				// unset($values['deb_id']);
				$values['modul_setup_id'] = $this->_bucket_setup_id;
				$values['upload_file_id'] = $this->_field_uploadId;
				if( is_array($Debitur)){
					$values['deb_id'] = (int)$Debitur['deb_id'];
				} else {
					$values['deb_id'] = 0;
				}	
				
				// look add field 
				if( is_array($this->_field_additional) 
					AND count($this->_field_additional)>0 )
				{
					foreach( $this->_field_additional as $field => $value ){
						$values[$field] = $value;
					}
				}
			   // @ pack : concate array 
				if($values['deb_id'] != "0")
				{
					$this->db->insert( $this->_upload_table_name, $values );
					// echo $this->db->last_query()."\n";
					if($this->db->affected_rows()<1)  
					{
						if( $this->_is_duplicate_error() ){
							$this->_tots_duplicate+=1;
						} else {
							$this->_tots_failed +=1; 
						}
						
					} else {
						$this->_tots_success +=1;
						$TRX_ID[] = $this->db->insert_id();
						$this->_set_update_debitur($values['deb_id']);
					}
				}
				else
				{
					$this->_tots_failed +=1; 
				}
				
			}
		}
		
		if ( count($TRX_ID) > 0 )
		{
			$this->insert_assign_round($TRX_ID);
		}
	}
	private function save_setup_round()
	{
		$running = $this->RoundIsRunning();
		$user_create = _get_session('UserId');
		if( $this->URI->_get_have_post('user_create_setup')) {
			$user_create = $this -> URI->_get_post('user_create_setup');
		}
		if( $running == FALSE )
		{
			$this->db->set( 'mod_set_createdby', $user_create );
			$this->db->set( 'mod_set_createdts','now()',false );
			$this->db->set( 'mod_set_running', '1' );
			$this->db->set( 'modul_id_rnd', $this->round_upload );
			
			if( $this->db->insert('t_gn_modul_setup') )
			{
				$this->_bucket_setup_id = $this->db->insert_id();
			}
		}
	}
	
	//check access all already running
	private function RoundIsRunning()
	{
		$running = false;
		$ModulRunning = $this->M_MgtRandDeb->ModulRunning($this->round_upload);
		if( $ModulRunning['running'] == true )
		{
			$running = true;
			$this->_bucket_setup_id = $ModulRunning['modul_id'];
		}
		return $running;
	}
	
	/*
	 * @ pack : private get Deb+id 
	 */ 
	 
	 private function _getDebiturId( $AccountNo=0 )
	{
	   $DebiturId = null;
	   $this->db->reset_select();
	   $this->db->select("a.deb_id, a.deb_prev_call_status_code");
	   $this->db->from("t_gn_debitur a");
	   $this->db->where("a.deb_acct_no", $AccountNo);
	   
	   $qry = $this->db->get();
	   if( $qry->num_rows() > 0 )
	   {
		 if( $rows = $qry->result_first_assoc() ){
			$DebiturId = array('deb_id'=>$rows['deb_id'], 'prev_call_status'=> $rows['deb_prev_call_status_code']);
		 }
	   }
	   
	   return $DebiturId;
	   
	} 
	
	/*
	** update deb_access_all
	**
	*/
	
	private function _set_update_debitur($key)
	{
		
		$this->db->set("deb_is_access_all",1);
		$this->db->set("deb_is_lock",0);
		$this->db->where("deb_id", $key);
		$this->db->update("t_gn_debitur");
	}
	
	private function insert_assign_round($Trx_Id = null)
	{
		$duration = _get_post('round_time_upload');
		$split_round = _get_post('bool_round_team');
		$user_create = _get_session('UserId');
		$st_round_id = null;
		$user_round_rilter = array(
			'a.user_state'=>1,
			'a.logged_state'=>1
		);
		if( $this->URI->_get_have_post('user_create_setup')) {
			$user_create = $this -> URI->_get_post('user_create_setup');
		}
		
		$this->db->set('duration',$duration);
		$this->db->set('create_by',$user_create);
		$this->db->set('bool_running','1');
		$this->db->set('modul_setup_id',$this->_bucket_setup_id);
		$this->db->set('st_create_date_ts','now()',false);
		$this->db->set('bool_round_split',$split_round);
		$this->db->set('next_round_ts','SEC_TO_TIME(('.$duration.'*60)+TIME_TO_SEC(now()))',false);
		if( $this->db->insert('t_st_round') )
		{
			$st_round_id = $this->db->insert_id();
		}
		// echo $this->db->last_query();
		if(!is_null($st_round_id))
		{
			if($split_round==1)
			{
				$Tl_List = explode(',',_get_post('round_tl'));
				$user_round_rilter = array(
					'a.user_state'=>1,
					'a.logged_state'=>1,
					'a.tl_id'=>$Tl_List
				);
				foreach( $Tl_List as $index => $tl_id)
				{
					$this->db->set('st_round_id',$st_round_id);
					$this->db->set('user_handling',3);
					$this->db->set('userid',$tl_id);
					$this->db->insert('t_gr_user_round');
				}
			}
			$ActDeskoll =$this->M_SysUser->_get_teleamarketer($user_round_rilter);
			$random = $this->M_MgtRandDeb->_RandomAssign($ActDeskoll,$Trx_Id);
			foreach($random as $Agentid => $ArrayValue)
			{
				foreach($ArrayValue as $rows => $trx_id){
					$this->db->set('bucket_trx_id',$trx_id);
					$this->db->set('agent_id',$Agentid);
					$this->db->set('st_round_id',$st_round_id);
					if( $this->db->insert('t_gn_round_assign') )
					{
						$Msg['debitur']++;
					}
					
				}
				$Msg['agent']++;
			}
		}
		
		
	}
	
	
	
}

// END CLASS 

?>