<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_DebiturPayment extends EUI_Upload 
{

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 var $_pop_status = '107';
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

/*
 * @ pack : private static $instance  =null
 */

 private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/*
 * @ pack : cek duplicate in error result .
 */
 
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
private function _is_duplicate_error()
{
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack  :  constructor  
 */
 
public function U_DebiturAccount() 
{ 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
 public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
/*
 * @ pack : private get Deb+id 
 */ 
 
 private function _getPrev_call_status( $deb_id=0 )
{
   $deb_prev_call_status_code = null;
   $this->db->reset_select();
   $this->db->select("a.deb_prev_call_status_code");
   $this->db->from("t_gn_debitur a");
   $this->db->where("a.deb_id", $deb_id);
   
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 )
   {
	 if( $rows = $qry->result_first_assoc() )
	 {
		$deb_prev_call_status_code =  $rows['deb_prev_call_status_code'];
	 }
   }
   
   return $deb_prev_call_status_code;
   
 } 
 
 
/*
 * @ pack : private get Deb+id 
 */ 
 
 private function _getDebiturId( $AccountNo=0 )
{
   $DebiturId = null;
   $this->db->reset_select();
   $this->db->select("a.deb_id, a.deb_prev_call_status_code");
   $this->db->from("t_gn_debitur a");
   $this->db->where("a.deb_acct_no", $AccountNo);
   
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 )
   {
	 if( $rows = $qry->result_first_assoc() ){
		$DebiturId = array('deb_id'=>$rows['deb_id'], 'prev_call_status'=> $rows['deb_prev_call_status_code']);
	 }
   }
   
   return $DebiturId;
   
 } 
 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
public function _set_process() 
{
  $FilterArray = array('pay_dc_id');
  $AgentCode = null;
  
// @ pack : set process save to debitur
   
 if( is_array($this->_upload_table_data) 
	AND !is_null($this->_upload_table_name) ) 
	foreach( $this->_upload_table_data as $n => $values ) 
 {
	$pay_dc_id =& $this->M_SysUser->_getUserByCode($values['pay_dc_id']);
	
	$this->_set_additional('pay_account_no', $values['deb_id']);
	$this->_set_additional('pay_agent_id', strtoupper($values['pay_dc_id']));
	
	unset($values['pay_dc_id']);
	if( is_array($pay_dc_id)){
		$values['pay_dc_id'] = $pay_dc_id['UserId'];
	} else {
		$values['pay_dc_id'] = 0;
	}
	
	if(!empty($values['deb_id']))
	{
		$Debitur = $this->_getDebiturId($values['deb_id']);
		unset($values['deb_id']);
		if( is_array($Debitur)){
			$values['deb_id'] = (int)$Debitur['deb_id'];
		} else {
			$values['deb_id'] = 0;
		}
		
		if( is_array($this->_field_additional) 
			AND count($this->_field_additional)>0 )
		{
			foreach( $this->_field_additional as $field => $value ){
				$values[$field] = $value;
			}
		}
		
	$pay_date_eval = trim($values['pay_date']);
		
 // --- if invalid date -----		
	 if( is_null( EvaluateDate($pay_date_eval) )){
		 $this->_tots_failed +=1; 
	 }	
	 else
	 { 
		$this->db->insert( $this->_upload_table_name, $values );
		if($this->db->affected_rows()<1)  
		{
			if( $this->_is_duplicate_error() ){
				$this->_tots_duplicate+=1;
			} else {
				$this->_tots_failed +=1; 
			}
			
		} else {
			$this->_set_update_debitur($values);
			$this->_tots_success +=1;
		}
	 } // end check data 
		
	}
	
 }
 
}

/*
 * @ pack : private function  
 */
 
private function _set_update_debitur( $xls= null ) 
{
 $conds = 0; 
 if( !is_null($xls) )
 {
	$DebiturId = $xls['deb_id'];
	$Amount = $xls['pay_amount'];
	$DeskollId = $xls['pay_dc_id'];
	
	$this->db->set("deb_call_status_code", $this->_pop_status);
	$this->db->set("deb_last_trx_date", date('Y-m-d H:i:s'));
	$this->db->set("deb_last_paydate", $xls['pay_date']);
	$this->db->set("deb_afterpay", "IF(deb_afterpay IS NULL OR deb_afterpay='',deb_amount_wo-$Amount,deb_afterpay-$Amount)", FALSE);
	$this->db->where("deb_id", $DebiturId);
	if( $this->db->update("t_gn_debitur") )
	{
		$prev_call_status = $this->_getPrev_call_status($DebiturId);
		$Deskoll = $this->M_SysUser->_getUserDetail($DeskollId);
		if( is_array($Deskoll))
		{
			$this->db->set("CustomerId",$DebiturId);
			$this->db->set("CallAccountStatus",$this->_pop_status);
			$this->db->set("CallReasonId", $prev_call_status);
			$this->db->set("CreatedById",$Deskoll['UserId']);
			$this->db->set("TeamLeaderId",$Deskoll['tl_id']);
			$this->db->set("SupervisorId",$Deskoll['spv_id']);
			$this->db->set("CallHistoryNotes","POP-PROGRESS OF PAYMENT-Auto");
			$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s'));
			$this->db->set("CallHistoryUpdatedTs",date('Y-m-d H:i:s'));
			
			$this->db->insert("t_gn_callhistory");
			if( $this->db->affected_rows()> 0 )
			{
			  $conds++;
			}
			
		}	
	}
 }
 return $conds;
} 

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
  $CampaignId = self::_get_campaignId();
  
 // ------- on M_Upload ------------- 
 
  $out =& get_class_instance('M_Upload');
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	$this->_set_additional('pay_campaign_id', $CampaignId);
	$this->_set_additional('pay_uplod_file', $out->_read_file_name());
	$this->_set_additional('pay_upload_id', $this->_field_uploadId);
	$this->_set_additional('pay_created_ts', date('Y-m-d H:i:s'));
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = TRUE;
  }
}

}

// END CLASS 

?>