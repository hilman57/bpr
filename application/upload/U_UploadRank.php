<?php


/*
 * @ package : under upload model 
 * 
 */
 
class U_UploadRank extends EUI_Upload {

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

/*
 * @ pack : private static $instance  =null
 */

 private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/*
 * @ pack : cek duplicate in error result .
 */
 
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		// 'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_SUCCES' => $this->_tots_rowselect,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
private function _is_duplicate_error()
{
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack  :  constructor  
 */
 
public function U_DebiturAccount() 
{ 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
private function _set_write_bucket()
{
 $conds 	= FALSE;
 $ProductId = $this->URI->_get_post('CampaignId');
 if( is_array($this->_upload_table_data)) 
 {

 // @ pack : reset array -----------------------
	
	$stdClass = $this->_get_ProjectId();
	$array_assoc = array('deb_created_ts'=>'bucket_created_ts');
	$this->db->from('t_tx_agent_rank');
	$this->db->truncate();
	

	foreach( $this->_upload_table_data as $n => $values )
	{
		$_array_select = null;
		foreach( $values as $field => $value )
		{ 
			if( in_array($field, array_keys($array_assoc) ) )  {
				$this->db->set($array_assoc[$field], $value);
			} else {
				$this->db->set($field, $value);	
			}
			
		// @ pack : additional field insert 
		
			$this->db->set('updated_by', $this->EUI_Session->_get_session('UserId'));
		}
		
		// insert here additional 
		
			$this->db->insert("t_tx_agent_rank", $_array_select);
		
		// echo $this->db->last_query()."\n\r"; exit;
		if( $this->db->affected_rows() > 0 )
		{
			$conds = TRUE;
		}
	}
	// var_dump($this->conds);die;
	return $conds;
 }

} 

/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
public function _set_process() 
{
 
  $AgentCode = null;
  $ProductId = $this->URI->_get_post('CampaignId');
  
  
// @ pack : set process save to bucket here  
  $this->_set_write_bucket();
// var_dump($this->_upload_table_data);
if($ProductId == '1') {
	$Agent  =& $this->M_SysUser->_get_user_by_rank_asc($ProductId);
} elseif($ProductId == '2') {
	$Agent  =& $this->M_SysUser->_get_user_by_rank_desc($ProductId);
}

$conds = $this->_set_log_rank();
if( $conds ) {
	$this->_tots_success +=1;
}
 // var_dump($this->_tots_rowselect); exit;
}


/*
 * @ package : set_log_rank **
 */
private function _set_log_rank(){
	$conds 		= FALSE;
	$ProductId	= $this->URI->_get_post('CampaignId');

	if( is_array($this->_upload_table_data)) {
		foreach( $this->_upload_table_data as $n => $values ) {
			$_array_select = null;
			foreach( $values as $field => $value ) {
				if( in_array($field, array_keys($array_assoc) ) )  {
					$this->db->set($array_assoc[$field], $value);
				} else {
					$this->db->set($field, $value);	
				}
				// @ pack : additional field insert 
		
				$this->db->set('updated_by', $this->EUI_Session->_get_session('UserId'));
				$this->db->set('RankDate', date('Y-m-d H:i:s'));
				$this->db->set('CampaignId', $ProductId);
			}
			// insert here additional
			$this->db->insert("t_tx_rank_log", $_array_select);
			
			// echo $this->db->last_query()."\n\r"; exit;
			if( $this->db->affected_rows() > 0 )
			{
				$conds = TRUE;
			}
		}
	}
	return $conds;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
	// var_dump($argv);die();
	
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	$this->_set_additional('deb_created_ts', date('Y-m-d H:i:s'));
	$this->_set_additional('deb_cmpaign_id', $this->_campaignId);
	$this->_set_additional('deb_upload_id', $this->_field_uploadId);
	// echo $this->db->last_query()."\n\r"; exit;
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
	
  }
}

// static func untuk sort ascending
private static function sort_card($e1, $e2) {
	return ($e2['deb_amount_wo'] - $e1['deb_amount_wo']);
	
}

// static func untuk sort desc
private static function sort_cf($e1, $e2) {
	return ($e1['deb_amount_wo'] - $e2['deb_amount_wo']);
}

}