<?php
/*
 * @ package : under upload model 
 * 
 */
 
class U_AccountDeleted extends EUI_Upload 
{

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

/*
 * @ pack : private static $instance  =null
 */

 private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

/*
 * @ pack : cek duplicate in error result .
 */
 
 public function _get_class_callback()
{
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
private function _is_duplicate_error()
{
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack  :  constructor  
 */
 
public function U_DebiturAccount() 
{ 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
private function _set_write_bucket()
{
 if( is_array($this->_upload_table_data)) 
 {

 // @ pack : reset array -----------------------
	
	$stdClass = $this->_get_ProjectId();
	$array_assoc = array('deb_created_ts'=>'bucket_created_ts');
	
	foreach( $this->_upload_table_data as $n => $values )
	{
		$_array_select = null;
		foreach( $values as $field => $value )
		{ 
			if( in_array($field, array_keys($array_assoc) ) )  {
				$this->db->set($array_assoc[$field], $value);
			} else {
				$this->db->set($field, $value);	
			}
			
		// @ pack : additional field insert 
		
			$this->db->set('BukcetSourceId', $this->_field_uploadId);
			$this->db->set('BuketUploadId',$stdClass->ProjectId);
			$this->db->set('deb_cmpaign_id', $stdClass->CampaignId);	
			$this->db->set('deb_cardno', $values['deb_acct_no']);	
		}
		
		// insert here additional 
		
		$this->db->insert("t_gn_bucket_customers", $_array_select);
			
	}
	
 }

} 

/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
public function _set_process() 
{
 
  $arr_account = array();
  
// @ pack : set process save to bucket here  
  $this->_set_write_bucket();
 
// @ pack : set process save to debitur
  if( is_array($this->_upload_table_data) 
	AND !is_null($this->_upload_table_name) ) 
	foreach( $this->_upload_table_data as $n => $values ) 
 {
	if( strlen($values['deb_acct_no'])  > 1 )
	{
		if( is_array($this->_field_additional) 
			AND count($this->_field_additional)>0 )
		{
			foreach( $this->_field_additional as $field => $value ){
				$values[$field] = $value;
			}
		}
		
	   // @ pack : concate array 
		
		$this->db->insert( $this->_upload_table_name, $values );
		if($this->db->affected_rows()<1)  
		{
			if( $this->_is_duplicate_error() ){
				$this->_tots_duplicate+=1;
			} else {
				$this->_tots_failed +=1; 
			}
		}
		else 
		{
		 
		 // @ pack : get insert assign ID 
			
			$conds = $this->_set_delete_debitur($values['deb_acct_no']);
			// var_dump($conds);
			if( $conds ) {
				// echo "PDS is not Active";
				$this->_tots_success +=1;
			}else{
				// echo "PDS is Active";
				$this->_tots_failed +=1;
			}
		}
	} else {
		$this->_tots_failed +=1; 
	}
		
 }
 
}

/*
 * @ package : set_assignment data **
 */
 private function _get_StatusPDS($custno)
 {
	$flagpds = "kosong";
	if( $custno ){
		$this->db->reset_select();
		$this->db->select("a.Flag_Pds", FALSE);
		$this->db->from("t_gn_debitur a ");
		$this->db->where("a.deb_acct_no", $custno);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$flagpds = $rows['Flag_Pds'];
			}
		}else{
			$flagpds = "null";
		}
	}

		return $flagpds;
 }
 
 private function _set_delete_debitur( $custno = 0 )
{	
	if( $custno == 0 OR $custno =='' OR $custno == null ){
		return FALSE;
	}
	
	$FlagPDS = $this->_get_StatusPDS($custno);
	// echo $FlagPDS."\n";
	if($FlagPDS === '0'){
		$sql = "INSERT INTO t_gn_debitur_deleted ".
			   //"SELECT *, 0 as deb_lock_rata, NOW() FROM t_gn_debitur WHERE deb_acct_no ='$custno'
			   //ON DUPLICATE KEY UPDATE deb_deleted_date_ts=NOW()";
		"SELECT
		deb_id,
		deb_acct_no,
		deb_cardno,
		deb_id_number,
		deb_no_card,
		deb_agent,
		deb_mother_name,
		deb_name,
		deb_zip_h,
		deb_home_no1,
		deb_office_ph1,
		deb_mobileno1,
		deb_bp,
		deb_open_date,
		deb_entri_date,
		deb_pay_date,
		deb_firstbp_date,
		deb_wo_date,
		deb_wo_amount,
		deb_resource,	
		deb_dob,
		deb_limit,
		deb_last_pay,
		deb_no_pay,
		deb_last_paydate,
		deb_total_pay,
		deb_amount_wo,
		deb_fees,	
		deb_interest,
		deb_principal,
		deb_billing_add,
		deb_add_hm,
		deb_add_off,
		deb_ec_name_f,
		deb_ec_add_f,
		deb_ec_phone_f,
		deb_ec_mobile_f,
		deb_b_d,
		deb_ec_relationship,
		deb_reminder,
		deb_region,
		deb_ssv_no,
		deb_cmpaign_id,
		deb_created_ts,
		deb_updated_ts,
		deb_call_status_code,
		deb_prev_call_status_code,
		deb_call_activity_datets,
		deb_update_by_user,
		deb_upload_id,
		deb_afterpay,
		deb_bal_afterpay,
		deb_pri_afterpay,
		deb_spc,
		deb_rpc,
		deb_contact_type,
		deb_sms_count,
		deb_swap_count,
		deb_last_swap_ts,
		deb_last_trx_date,
		deb_is_kept,
		deb_is_lock,
		deb_is_call,
		deb_is_access_all,
		deb_perm_msg,
		deb_perm_msg_coll,
		deb_perm_msg_date,
		addhomephone,
		addhomephone2,
		addmobilephone,
		addmobilephone2,
		addofficephone,
		addofficephone2,
		other_agent,
		other_card_number,
		other_accstatus,
		addfax,
		addfax2,
		acc_mapping,
		infoptp,
		last_ptp_id,
		deb_lock_type,
		deb_lock_date_ts,
		NOW() 
		FROM t_gn_debitur WHERE deb_acct_no='$custno' 
		ON DUPLICATE KEY UPDATE deb_deleted_date_ts=NOW()";
		// echo $sql;
		if( $this->db->query($sql) ){
			$sql = "DELETE FROM t_gn_debitur WHERE deb_acct_no ='$custno'";
			if( $this->db->query($sql) ){
				// return "deleted";
				return 1;
			} else {
				// return "tidak ada data";
				return 0;
			}
		}else{
			return 0;
		}
	}
	else{
		// return "semua data pds \n";
		return 0;
	}
 }

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
  if( !is_null($argv) AND is_array($argv) )
  {
	$this->_upload_table_data = $argv;
	$this->_set_additional('deb_upload_datets', date('Y-m-d H:i:s'));
	$this->_set_additional('deb_upload_userid', _get_session('Username'));
	$this->_set_additional('deb_upload_id', $this->_field_uploadId);
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
	
  }
}

}

// END CLASS 

?>