<?php
/*
 * @ pack : U_DebiturLockAccount 
 */
 
class U_DebiturLockAccount extends EUI_Upload
{

/* 
 * @ pack : variable upload 
 */
 
 var $_upload_table_name = null;
 var $_upload_table_data = null;
 var $_pop_status = '107';
 var $_is_lock = 1;
 var $_is_auto_start = FALSE;
 var $_exist_agent_lock = array();
 
/* 
 * @ pack : variable upload 
 */
 
 var $_tots_rowselect = 0;
 var $_tots_success = 0;
 var $_tots_failed = 0;
 var $_tots_duplicate = 0;
 var $_HistoryId = array();
 
 private $_lock_list_id= null;
 
/*
 * @ pack : private static $instance  =null
 */

 private $_field_additional = array(); 
 private $_field_uploadId = 0;
 private $_is_complete = FALSE;
 private $_campaignId = 0;

 
 
/*
 * @ pack : private static $instance  =null
 */

private static $Instance = null;

/*
 * @ pack : private get $instance  =null
 */
 
public static function &Instance()
{
  if( is_null(self::$Instance) ) 
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 * @ pack  :  constructor  
 */
 
public function U_DebiturAccount() 
{ 
	$this->_get_campaignId();
	$this->load->model(array('M_SysUser'));
}

/*
 * @ pack : _reset_argvs
 */
 
 private function _set_delete_agent_lock_modul( $agent_id = null )
{
   $count = 0;
   $Agents = ( is_null($agent_id) ? $this->_exist_agent_lock : $agent_id );
   
	if( is_array($Agents) 
		AND count($Agents) > 0  )
	{
		foreach( $Agents as $AgentId => $value  )
	   {
		 $this->db->set("ModValueParameter", "NULL", FALSE);
		 $this->db->set("ModStartParameter","NULL", FALSE);
		 $this->db->set("ModEndParameter","NULL", FALSE);
		 $this->db->where_in("ModUser", array($value));
		 
		 if( $this->db->update("t_gn_lock_agent_modul") )
		 {
			$count++;
		 }
	  }
	}

// reset to default data ===========================================> 
	
	if( $count> 0){
		$this->_exist_agent_lock = array();
	}	
}

/*
 * @ pack : _reset_argvs
 */
 
public function _reset_class_argvs() 
{	
 $this->_tots_rowselect = 0;
 $this->_tots_success = 0;
 $this->_tots_failed = 0;
 $this->_campaignId = 0;
 $this->_field_uploadId= 0;
 $this->_tots_duplicate = 0;
 $this->_is_complete = false;
 $this->_upload_table_name = null;
 $this->_upload_table_data= null;
 $this->_field_additional = ARRAY(); 
 
}

// @ pack :  _get_select_expired

 private function _get_select_expired()
{

	 if( _get_have_post('ExpiredDate') 
		AND strlen(_get_post('ExpiredDate')) > 0 )
	{
		return date('Y-m-d', strtotime(_get_post('ExpiredDate'))) ." ".
			  (_get_post('ExpiredHour') ) .":". 
			  (_get_post('ExpiredMinute') ) .":".
			  (_get_post('ExpiredSecond') );
	} else {
		return NULL;
	}	

}

// @ pack :  _get_select_expired

 private function _get_select_start()
{
	if( _get_have_post('StartDate') 
		AND strlen(_get_post('StartDate')) > 0 )
	{
		return date('Y-m-d', strtotime(_get_post('StartDate'))) ." ".
			  (_get_post('StartHour') ) .":". 
			  (_get_post('StartMinute') ) .":".
			  (_get_post('StartSecond') );
	} else {
		return NULL;
	}
}

/*
 * @ pack : cek duplicate in error result .
 */
 
public function _get_class_callback(){
	$_call_back = array (
		'TOTAL_UPLOAD' => $this->_tots_rowselect,
		'TOTAL_SUCCES' => $this->_tots_success,
		'TOTAL_FAILED' => $this->_tots_failed,
		'TOTAL_DUPLICATE' => $this->_tots_duplicate 
	);
	
	return (object)$_call_back;
}

/*
 * @ pack : cek duplicate in error result .
 */
 
private function _is_duplicate_error()
{
	$_error = mysql_error();
	if(preg_match("/\Dup/i", $_error)) {
		return true;
	}	
	else{
		return false;
  }
}

/*
 * @ pack : Project Id 
 */
 
 private function _get_ProjectId()
 {
	$CampaignId = $this->_get_campaignId();
	$stdClass = array('ProjectId' => 0, 'CampaignId' => 0 );
	
	if( $CampaignId )
	{
		$this->db->reset_select();
		$this->db->select("a.ProjectId, a.CampaignId", FALSE);
		$this->db->from("t_gn_campaign_project a ");
		$this->db->where("a.CampaignId", $CampaignId);
		$res = $this->db->get();
		if( $res -> num_rows() > 0 )
		{
			if( $rows = $res->result_first_assoc() )
			{
				$stdClass = array(
					'ProjectId' => $rows['ProjectId'], 
					'CampaignId' => $rows['CampaignId'] 
				);
			}
		}
	}
	
	return (object)$stdClass;
 }

/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_additional( $field = null, $values=null  ) 
 {
	if( !is_null($field) )
	{
		$this->_field_additional[$field] = $values;
	}	
 }


 /*
 * @ pack  : _set_templateid Ok 
 */
 
public function _get_campaignId() 
{
 if( $this->URI->_get_have_post('CampaignId')) 
 {
	$this->_campaignId = $this->URI->_get_post('CampaignId');
  }	
	return $this->_campaignId;
 }

 
/*
 * @ pack  : _set_templateid Ok 
 */
 
public function _set_uploadId( $_set_uploadId = 0 ) 
{
	if( $_set_uploadId ) 
	{
		$this->_field_uploadId= $_set_uploadId;
	}	
 }
/*
 * @ pack : private get Deb+id 
 */ 
 
 private function _getPrev_call_status( $deb_id=0 )
{
   $deb_prev_call_status_code = null;
   $this->db->reset_select();
   $this->db->select("a.deb_prev_call_status_code");
   $this->db->from("t_gn_debitur a");
   $this->db->where("a.deb_id", $deb_id);
   
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 )
   {
	 if( $rows = $qry->result_first_assoc() )
	 {
		$deb_prev_call_status_code =  $rows['deb_prev_call_status_code'];
	 }
   }
   
   return $deb_prev_call_status_code;
   
 } 
 
 
/*
 * @ pack : private get Deb+id 
 */ 
 
 private function _getDebiturId( $AccountNo=0 )
{
   $DebiturId = null;
   $this->db->reset_select();
   $this->db->select("a.deb_id, a.deb_prev_call_status_code, a.deb_cmpaign_id");
   $this->db->from("t_gn_debitur a");
   $this->db->where("a.deb_acct_no", $AccountNo);
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 )
   {
	 if( $rows = $qry->result_first_assoc() ){
		$DebiturId = array(
			'deb_id'=>$rows['deb_id'], 
			'prev_call_status'=> $rows['deb_prev_call_status_code'],
			'deb_cmpaign_id' => $rows['deb_cmpaign_id'] );
	 }
   }
   
   return $DebiturId;
   
 } 

/*
 * @ pack : add auto start if null start date flags 
 *  ------------------------------------------------------>  
 */ 
 
 public function set_auto_start()
{


 if( _get_have_post('auto_start') 
    AND strlen(_get_post('auto_start'))>0  
	AND _get_post('auto_start') ==1 )
  {
	 $this->_is_auto_start = TRUE;
  } else {
	$this->_is_auto_start = FALSE;
  }
  
}

 
/*
 * @ pack  : insert moethode of distribute Ok 
 */
 
public function _set_process() 
{

$ListDebitur = array();

// @ pack :
 
  $AgentCode = null; 
// @ pack : set process save to debitur
   $this->set_setup_lock();

 if( is_array($this->_upload_table_data) 
	AND !is_null($this->_upload_table_name) ) 
	foreach( $this->_upload_table_data AS $n => $values ) 
 {
	
// @ unset : DC ID then replace with its 
    $Debitur = $this->_getDebiturId($values['Lock_AccountNo']);
	if( is_array($Debitur))
	{
		$values['Lock_DebiturId'] = (INT)$Debitur['deb_id'];
		$ListDebitur[$values['Lock_DebiturId']] = array(
			"deb_id" => $values['Lock_DebiturId'], 
			"deb_agent" => $values['Lock_AgentId']
		);
		
	} else {
		$values['Lock_DebiturId'] = 0;
	}	
	
 // look add field 
	
	if( is_array($this->_field_additional) 
		AND count($this->_field_additional)>0 )
	{
		foreach( $this->_field_additional AS $field => $value ){
			$values[$field] = $value;
		}
		$values['lock_list_id']=$this->_lock_list_id;
	}
	
 // look att campaign  field  	
 
	if( isset($values['Lock_CampaignId']) 
		AND (strlen($values['Lock_CampaignId']) == 0) )
	{
		$values['Lock_CampaignId'] = $Debitur['deb_cmpaign_id'];
	}	
	
 // @ pack : concate array 
	
	$this->db->insert( $this->_upload_table_name, $values );
	if($this->db->affected_rows()<1)  
	{
		if( $this->_is_duplicate_error() ){
			$this->_tots_duplicate+=1;
		} else {
			$this->_tots_failed +=1; 
		}
		
	} else {
		$this->_exist_agent_lock[$values['lock_agent_id']] = $values['lock_agent_id'];
		$this->_tots_success +=1;
	}
 }
 $this->_set_update_debitur($ListDebitur);
 
}

/*
 * @ pack : private function  
 */
 
 private function _set_update_debitur( $ListDebitur= null ) 
{
  $lock_id = 0;
  
/*
 * @ pack : update debitur jika debitur yang sebelmnya di lock kemudian diu upload 
 * 			maka lock menjadi unlock 
 */
  
  $arr_debitur = array();
  $arr_agents = array();
  
  $num = 0;
  foreach( $ListDebitur as $deb_id => $xls_row )
 {
	$arr_debitur[$num] = $xls_row['deb_id'];
	$arr_agents[$xls_row['deb_agent']][] = $xls_row['deb_id'];
	$this->db->set('deb_is_lock', 0);
    $this->db->where('a.deb_id',$xls_row['deb_id']);
    $this->db->where('a.deb_agent',$xls_row['deb_agent']);
    $this->db->update('t_gn_debitur');
	$num++;
 }
 
  
/*
 * @ pack : selain itu lock  maka lock menjadi unlock  maka lock menjadi unlock 
 * 
 */
  
  $this->db->reset_select();
  $this->db->select('a.deb_id, a.deb_agent, a.deb_acct_no', false);
  $this->db->from('t_gn_debitur a ');
  $this->db->where_in('a.deb_agent', array_map('strval', array_keys($arr_agents)));
  //$this->db->print_out();
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) 
	foreach( $qry->result_assoc() as $rows )
 {
	// ========= start 
	
	$arr_deb_loking = $arr_agents[$rows['deb_agent']];
	
	$User = $this->M_SysUser->getUserQuery(array("id"=>$rows['deb_agent']));
	
	if( is_array( $User ) 
		AND is_array($arr_deb_loking) AND count($arr_deb_loking) > 0 
		AND !in_array( $rows['deb_id'], $arr_deb_loking) ) foreach( $User as $Users )
	{
		$this->db->set('deb_id',$rows['deb_id']);
		$this->db->set('lock_agent_id',$Users['UserId']);
		$this->db->set('lock_created_by', _get_session('UserId'));
		$this->db->set('lock_upload_id', $this->_field_uploadId);
		$this->db->set('lock_auto_start',$this->_get_select_start());
		$this->db->set('lock_auto_end', $this->_get_select_expired());
		$this->db->set('lock_created_ts', date('Y-m-d H:i:s'));
		$this->db->set('lock_list_id', $this->_lock_list_id);
		
		if( TRUE == $this->_is_auto_start ) {
			$this->db->set('lock_auto_run', 0);
		}
		
		$this->db->insert('t_gn_lock_customer');
		
		if( $this->db->affected_rows()>0 )
		{
		   if( FALSE == $this->_is_auto_start ) 
		   {
			   
			$this->db->set("deb_is_lock",1);
			$this->db->set("deb_lock_type",101);
			$this->db->set("deb_lock_date_ts", date('Y-m-d H:i:s'));
			$this->db->where("deb_id", $rows['deb_id']);
			if( $this->db->update('t_gn_debitur') ) {
				 $this->_set_delete_agent_lock_modul();
			 }
			 
		   }
		  
		  $lock_id++;
		  
	  }  else {
		 // @pack : update t_gn_lock_customer
		 
			$this->db->set("lock_created_ts",date('Y-m-d H:i:s') );
			$this->db->set("lock_created_by",_get_session('UserId'));
			$this->db->set('lock_agent_id',$Users['UserId']);
			$this->db->set('lock_auto_start', $this->_get_select_start());
			$this->db->set('lock_auto_end', $this->_get_select_expired());
			$this->db->set('lock_upload_id', $this->_field_uploadId);
			$this->db->set('lock_list_id', $this->_lock_list_id);
			
		// cek of batch 
		
			if( TRUE == $this->_is_auto_start ) {
				$this->db->set('lock_auto_run', 0);
			}
		
			$this->db->where("deb_id", $rows['deb_id']);
			$this->db->where("lock_agent_id", $Users['UserId']);
			$this->db->update('t_gn_lock_customer');
			
		 // @pack :  update debitur 
		 
		  if( FALSE == $this->_is_auto_start ) {
			 $this->db->set("deb_is_lock",1);
			 $this->db->set("deb_lock_type",101);
			 $this->db->set("deb_lock_date_ts", date('Y-m-d H:i:s'));
			 $this->db->where("deb_id", $rows['deb_id']);
			 $this->db->where("deb_agent", $rows['deb_agent']);
			 
			 if( $this->db->update('t_gn_debitur') )
			 {
				 $this->_set_delete_agent_lock_modul();
			 }
			 
		  }
		  
		  $lock_id++;
		  
		}
	}
	
	// ===============> 
	
 }
 return $lock_id;
} 

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_table( $table = null )
{
  if( !is_null( $table ) ) 
  {
	 $this->_upload_table_name = $table;
  }
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _get_is_complete()
{
	return $this->_is_complete;
}

/*
 * @ pack : insert moethode of distribute Ok 
 */
 
public function _set_content( $argv = null )
{
  $CampaignId = self::_get_campaignId();
  if( !is_null($argv) AND is_array($argv) )
  {
  
  // cek if autostart will run by crontab ==================>
	$this->set_auto_start();
  // then insert all parameter all 
	$this->_upload_table_data = $argv;
	$this->_set_additional('Lock_CampaignId', $CampaignId);
	$this->_set_additional('Lock_UploadId', $this->_field_uploadId);
	$this->_set_additional('Lock_UserPrivilegesId', _get_session('HandlingType'));
	$this->_set_additional('Lock_CreateUserId', _get_session('UserId'));
	$this->_set_additional('Lock_CreateTs', date('Y-m-d H:i:s'));
	$this->_tots_rowselect = count($argv);
	$this->_is_complete  = true;
  }
}

private function set_setup_lock()
{
	$this->db->set('upload_id',$this->_field_uploadId);
	$this->db->set('lock_create_by', _get_session('UserId'));
	$this->db->set('lock_auto_start',$this->_get_select_start());
	$this->db->set('lock_auto_end', $this->_get_select_expired());
	$this->db->set('lock_create_ts', date('Y-m-d H:i:s'));
	
	$this->db->insert('t_gn_lock_list');
	if( $this->db->affected_rows()>0 )
	{
		$this->_lock_list_id = $this->db->insert_id();
	}
}



// END CLASS 

}


?>