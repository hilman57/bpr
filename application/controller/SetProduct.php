<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysMenu  
 * 			  extends under model class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysmenu/
 */
 
class SetProduct extends EUI_Controller 
{
	
/*
 * @ def : __construct
 * ---------------------------------------------------------------------
 *
 * @ param : -
 * @ param : -
 */

	
 function SetProduct() 
 {
	parent::__construct();
	$this -> load -> model(array(base_class_model($this),'M_Combo'));
 }

/*
 * @ def : index default of the page loader 
 * ---------------------------------------------------------------------
 *
 * @ param : controller & model ( object )
 * @ param : -
 */

function index()
{
	if( $this -> EUI_Session -> _have_get_session('UserId') ) 
	{
		$_EUI = array(
			'data'=> $this -> {base_class_model($this)},
			'gender' => $this -> M_Combo -> _getGender()
		); 
		
		$this -> load -> view('set_product_cores/view_product_nav',$_EUI); //load content data by pages 
	}
}

/*
 * @ def : ShowGrid
 * ---------------------------------------------------------------------
 *
 * @ param : $ProductAge < INT >
 * @ param : $ProductPlan < INT >
 */

function ShowGrid()
{
	$EUI['ProductPlan'] = (INT)$this -> URI ->_get_post('ProductPlan'); //load content data by pages 
	$EUI['ProductAge'] = (INT)$this -> URI ->_get_post('ProductAge'); // load content data by pages 
	
	$this -> load -> view('set_product_cores/view_age_band',$EUI); // load content data by pages 
}

// @ GridContent

function GridContent()
{
	$EUI['ProductPlan'] = (INT)$this -> URI -> _get_post('ProductPlan'); //load content data by pages 
	$EUI['ProductAge']  = (INT)$this -> URI -> _get_post('ProductAge'); // load content data by pages 
	$EUI['PayMode']  	= ($this -> URI-> _get_have_post('PayMode')?$this -> URI ->_get_array_post('PayMode'):null);
	$EUI['GroupPremi']  = ($this -> URI-> _get_have_post('GroupPremi')?$this -> URI ->_get_array_post('GroupPremi'):null);
	$EUI['Gender']		= ($this -> URI-> _get_have_post('Gender')?$this -> URI ->_get_array_post('Gender'):null);
	$EUI['GenderId']	= $this -> M_Combo -> _getGender();
	$EUI['data'] 		= $this -> M_SetProduct; // load content data by pages 		
	$this -> load -> view('set_product_cores/view_content_grid',$EUI); // load content data by pages
 }
 
/*
 * EUI .@ SaveProduct
 * -----------------------------------------
 *
 * @ func 	 set_level_one insert to database 
 * @ params  post & definition paymode 
 */
 
 function SaveProduct()
 {
	$_success = array('success'=> 0, 'error'=> ''); 
	$_post = array (
		'GroupPremi' 	=> $this -> URI -> _get_array_post('GroupPremi'),
		'PayMode' 		=> $this -> URI -> _get_array_post('PayMode'),
		'ProductPlan' 	=> $this -> URI -> _get_post('ProductPlan'),
		'RangeAge' 		=> $this -> URI -> _get_post('RangeAge'),
		'CreditShield'  => $this -> URI -> _get_post('CreditShield'),
		'ProductId' 	=> $this -> URI -> _get_post('ProductId'),
		'ProductName' 	=> $this -> URI -> _get_post('ProductName'),
		'ProductCores'	=> $this -> URI -> _get_post('ProductCores'),
		'ProductType'	=> $this -> URI -> _get_post('ProductType'),
		'Gender'		=> $this -> URI -> _get_array_post('Gender')
	);
	
	// echo $this -> URI -> _get_post('start_range_1');
	// print_r($this -> URI -> _get_array_post('post_data'));
	// exit(0);
	
	if( ( $_post['CreditShield']!=1) && ($_post['CreditShield']!='' )){
		$_conds = $this -> {base_class_model($this)} -> _set_not_credit_shield($_post, $_REQUEST);
	}
	else{
		$_conds = $this -> {base_class_model($this)} -> _set_credit_shield($_post, $_REQUEST);
	}
	
/** json parse **/
	
	if( $_conds )
	{
		$_success = array('success'=> 1, 'error'=> 'OK'); 
	}
	
	echo json_encode($_success);
 }
 
 
/*
 * EUI .@ SaveProduct
 * -----------------------------------------
 *
 * @ func 	 set_level_one insert to database 
 * @ params  post & definition paymode 
 */
 
}

?>