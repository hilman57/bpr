<?php

class DailyActivityReport extends EUI_Controller  
{

    public function index()
    {
        
        $this->load->view('dailyactivityreport/index');
    }

    public function tgl_indo($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
    }
	public function excel()
    {
      //$start_date = date("2019-08-08");
       $start_date = date("Y-m-d");
        //generate to excel
        // Excel() -> HTML_Excel();
      //cek mode
        ?>      
                <h1>REPORT DAILY ACTIVITY <?php echo $start_date; ?></h1>
                <table border="1px green" cellspacing="0">
                <thead>
                  <tr >                    
                    <th>TL</th>
                    <th>AGENT</th>
                    <th>DC</th>
                    <th>NEW</th>
                    <th>OS-ON PROCESS</th>
                    <th>OP-ON PROSPECT</th>
                    <th>VALID</th>
                    <th>ON NEGO</th>
                    <th>PTP-NEW</th>
                    <th>PTP-POP</th>
                    <th>PTP-BP</th>
                    <th>PAID OFF</th>
                    <th>BUSY LINE</th> 
                    <th>DECEASE</th>
                    <th>Grand Total</th>
                    <th>Atemp</th>
                  </tr>
                <thead>
                <tbody>                
                <?php                
                //  $start_date = date("Y-m-d");
                // $start_date = $_GET['start_date'];
                // $end_date = $_GET['end_date'];
                // test
                $data_attemp = $this->db->query("
                              SELECT 
                            ag.UserId AS TL_UID, ag.full_name as TL,  c.id AS AGENT,
                            c.full_name AS DC, a.CallAccountStatus AS ACCOUNT_STATUSID, st.CallReasonDesc as ACCOUNT_STATUS,
                            a.AccountNo AS 'ACCOUNTNO', a.CallHistoryCallDate 'TGLUPDATE'                            
                            FROM t_gn_callhistory a                            
                            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                            LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                            LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId                            
                            WHERE
                            CallHistoryCallDate BETWEEN '".$start_date." 00:00:00' AND '".$start_date." 23:00:00'
                            AND ag.userId!=''                            
                            ORDER BY TL_UID,a.ACCOUNTNO ASC
                ")->result();                
                // test
                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_attemp as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      #52_R014_OS-ON PROCESS

                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                      
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }
                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }

                #print_r($user_TL);
                #exit();
                $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){
                    
                    $data = $ON_STATUS;
                    $info = array();

                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }
                $arr = array();
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  // $ALL_ATTEMP = 0;  

                  foreach ($groups as $agent_id => $d){

                    $TOTAL_NEW = $TOTAL_NEW + $d['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $d['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $d['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $d['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $d['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $d['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $d['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $d['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $d['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $d['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $d['DECEASE'];

                    $ATTEMP = $d['NEW'] + $d['OS-ON PROCESS']+ $d['OP-ON PROSPECT']+ $d['VALID']+ $d['ON NEGO']+ 
                                  $d['PTP-NEW']+$d['PTP-POP']+$d['PTP-BP']+$d['PAID OFF']+$d['BUSY LINE']+$d['DECEASE'];
                    
                                  array_push($arr, $ATTEMP);
                  }
                }

                $no_agent = 1;
                $data_agent = $this->db->query("
                      SELECT	
                      ag.UserId AS TL_UID,
                      ag.full_name as TL,
                      c.id AS AGENT,
                      c.full_name AS DC,
                      a.CallAccountStatus AS ACCOUNT_STATUSID,
                      st.CallReasonDesc as ACCOUNT_STATUS,
                      a.AccountNo AS 'ACCOUNTNO',
                      a.CallHistoryCallDate 'TGLUPDATE'

                    FROM (
                      SELECT AccountNo,MAX(CallHistoryCallDate) AS GROUP_ID 
                      FROM t_gn_callhistory 	
                      WHERE
                        CallHistoryCallDate BETWEEN '".$start_date." 00:00:00' AND '".$start_date." 23:00:00' 
                        AND TeamLeaderId!=''
                        
                        
                      GROUP BY AccountNo,AgentCode
                    ) AS TBL_TMP
                    
                    INNER JOIN t_gn_callhistory a ON (a.CallHistoryCallDate=TBL_TMP.GROUP_ID AND a.AccountNo=TBL_TMP.AccountNo)

                    LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                    LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                    LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                    LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId 

                    WHERE
                      CallHistoryCallDate BETWEEN '".$start_date." 00:00:00' AND '".$start_date." 23:00:00'
                      AND ag.userId!=''
                      
                    ORDER BY TL_UID,a.ACCOUNTNO ASC
                ")->result();

                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_agent as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){

                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;                      
                    }

                    $user_TL[$tl_uid] = $agent;

                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();
                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }
               $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){                    
                    $data = $ON_STATUS;
                    $info = array();
                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                } 
                
                $STOP = 0;
                if( $STOP ) {
                $no = 0; $no2 = 0;
                  foreach ($user as $user_id => $groups) {                  
                    $first = true;
                    $TOTAL_NEW = 0;
                    $TOTAL_ON_PROCESS = 0;
                    $TOTAL_ON_PROSPECT = 0;
                    $TOTAL_VALID = 0;
                    $TOTAL_ON_NEGO = 0;
                    $TOTAL_PTP_NEW = 0;
                    $TOTAL_PTP_POP = 0;
                    $TOTAL_PTP_BP = 0;
                    $TOTAL_PAID_OFF = 0;
                    $TOTAL_BUSY_LINE = 0;
                    $TOTAL_DECEASE = 0;
                    $ALL_GRANT_TOTAL = 0;  
                    $ALL_ATTEMP = 0;  

                    foreach ($groups as $agent_id => $row){

                      $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
                      $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];

                      $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OP-ON PROSPECT'];
                      $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
                      $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
                      $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
                      $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
                      $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
                      $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
                      $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
                      $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];

                      $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                                    $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
                      
                      $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
                      $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 
                      
                      echo '<tr border="1px green" cellspacing="0">';

                      if($first){
                        echo '<td class="text-left">'.$row['TL'].'</td>';
                        $first=false;
                      }else{
                        echo '<td></td>';
                      }
                      echo '
                        <td>'.$row['AGENT'].'</td>
                        <td class="text-left">'.$row['DC'].'</td>
                        <td>'.$row['NEW'].'</td>
                        <td>'.$row['OS-ON PROCESS'].'</td>
                        <td>'.$row['OP-ON PROSPECT'].'</td>
                        <td>'.$row['VALID'].'</td>
                        <td>'.$row['ON NEGO'].'</td>
                        <td>'.$row['PTP-NEW'].'</td>
                        <td>'.$row['PTP-POP'].'</td>
                        <td>'.$row['PTP-BP'].'</td>
                        <td>'.$row['PAID OFF'].'</td>
                        <td>'.$row['BUSY LINE'].'</td>
                        <td>'.$row['DECEASE'].'</td>
                        <td>'.$GRANT_TOTAL.'</td>';                      
                        echo '<td>'.$arr[$no++].'</td>';
                        echo '</tr>';
                    }
                      
                    echo '
                    <tr border="1px green" cellspacing="0">
                      <td colspan="3" class="text-left"><strong>TOTAL</strong></td>
                      <td>'.$TOTAL_NEW.'</td>
                      <td>'.$TOTAL_ON_PROCESS.'</td>
                      <td>'.$TOTAL_ON_PROSPECT.'</td>
                      <td>'.$TOTAL_VALID.'</td>
                      <td>'.$TOTAL_ON_NEGO.'</td>
                      <td>'.$TOTAL_PTP_NEW.'</td>
                      <td>'.$TOTAL_PTP_POP.'</td>
                      <td>'.$TOTAL_PTP_BP.'</td>
                      <td>'.$TOTAL_PAID_OFF.'</td>
                      <td>'.$TOTAL_BUSY_LINE.'</td>
                      <td>'.$TOTAL_DECEASE.'</td>
                      <td>'.$ALL_GRANT_TOTAL.'</td>
                      <td>'.$ALL_ATTEMP.'</td>
                    </tr>';

                    echo '<tr border="1px green" cellspacing="0"><td colspan="14"></td></tr>';
                  
                  }
                } // # $STOP

                $path   = '/var/www/html/development/hsbc180619/';
                $fileName = "Daily Activity-".$start_date. "-".date('His').".xls";
                $fp = fopen("/var/www/html/development/hsbc180619/". $fileName,"a+");
                // $data = "nama \t alamat \t \n"; // header
                $data = "TL \t AGENT \t DC \t NEW \t OS-ON PROCESS \t OP-ON PROSPECT \t VALID \t ON NEGO \t PTP-NEW \t PTP-POP \t PTP-BP \t PAID OFF \t BUSY LINE \t DECEASE \t Grand Total \t Atemp \t \n";
                $no = 0; $no2 = 0;
                foreach ($user as $user_id => $groups) {                  
                      $first = true;
                      $TOTAL_NEW = 0;
                      $TOTAL_ON_PROCESS = 0;
                      $TOTAL_ON_PROSPECT = 0;
                      $TOTAL_VALID = 0;
                      $TOTAL_ON_NEGO = 0;
                      $TOTAL_PTP_NEW = 0;
                      $TOTAL_PTP_POP = 0;
                      $TOTAL_PTP_BP = 0;
                      $TOTAL_PAID_OFF = 0;
                      $TOTAL_BUSY_LINE = 0;
                      $TOTAL_DECEASE = 0;
                      $ALL_GRANT_TOTAL = 0;  
                      $ALL_ATTEMP = 0;  
                      foreach ($groups as $agent_id => $row){
    
                        $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
                        $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];
    
                        $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OP-ON PROSPECT'];
                        $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
                        $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
                        $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
                        $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
                        $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
                        $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
                        $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
                        $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];
    
                        $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                                      $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
                        
                        $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
                        $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 
                        if($first){
                          $data .= $row['TL']."\t";
                          $first=false;
                        }else{
                          $data .= "\t";
                        }
                          $data .= $row['AGENT']."\t";
                          $data .= $row['DC']."\t";
                          $data .= $row['NEW']."\t";
                          $data .= $row['OS-ON PROCESS']."\t";
                          $data .= $row['OP-ON PROSPECT']."\t";
                          $data .= $row['VALID']."\t";
                          $data .= $row['ON NEGO']."\t";
                          $data .= $row['PTP-NEW']."\t";
                          $data .= $row['PTP-POP']."\t";
                          $data .= $row['PTP-BP']."\t";
                          $data .= $row['PAID OFF']."\t";
                          $data .= $row['BUSY LINE']."\t";
                          $data .= $row['DECEASE']."\t";
                          $data .= $GRANT_TOTAL."\t";
                          $data .= $arr[$no++]."\t \n";
                      }
                      $data .= "TOTAL \t"; 
                      $data .= "\t"; 
                      $data .= "\t";
                        $data .= $TOTAL_NEW."\t";
                        $data .= $TOTAL_ON_PROCESS."\t";
                        $data .= $TOTAL_ON_PROSPECT."\t";
                        $data .= $TOTAL_VALID."\t";
                        $data .= $TOTAL_ON_NEGO."\t";
                        $data .= $TOTAL_PTP_NEW."\t";
                        $data .= $TOTAL_PTP_POP."\t";
                        $data .= $TOTAL_PTP_BP."\t";
                        $data .= $TOTAL_PAID_OFF."\t";
                        $data .= $TOTAL_BUSY_LINE."\t";
                        $data .= $TOTAL_DECEASE."\t";
                        $data .= $ALL_GRANT_TOTAL."\t";
                        $data .= $ALL_ATTEMP."\t \n";

                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t"; 
                        $data .= "\t \n"; 
                    
                    }
                  

                  fwrite($fp, $data);
                  fclose($fp);
              ?>
            </tbody>
          </table>
  <?php
      }

   // edit hilman
    public function detail()
    {
      $start_date = $_GET['start_date'];
      //$end_date = $_GET['end_date'];

        if ($_GET['button'] == 'Action') {
            
        // //generate to excel
        // Excel() -> HTML_Excel();
        // //cek mode
        
            $this->load->view('dailyactivityreport/detail');
        } else if ($_GET['button'] == 'Excel'){          
       //generate to excel
        Excel() -> HTML_Excel();
      //cek mode
        ?>      
                <h1>REPORT DAILY ACTIVITY <?php echo $start_date; ?></h1>
                <table border="1px green" cellspacing="0">
                <thead>
                  <tr >                    
                    <th>TL</th>
                    <th>AGENT</th>
                    <th>DC</th>
                    <th>NEW</th>
                    <th>OS-ON PROCESS</th>
                    <th>OP-ON PROSPECT</th>
                    <th>VALID</th>
                    <th>ON NEGO</th>
                    <th>PTP-NEW</th>
                    <th>PTP-POP</th>
                    <th>PTP-BP</th>
                    <th>PAID OFF</th>
                    <th>BUSY LINE</th> 
                    <th>DECEASE</th>
                    <th>Grand Total</th>
                    <th>Atemp</th>
                  </tr>
                <thead>
                <tbody>                
                <?php     
					
                $start_date = $_GET['start_date'];
              //  $end_date = $_GET['end_date'];
                // test
                $data_attemp = $this->db->query("
                              SELECT 
                            ag.UserId AS TL_UID, ag.full_name as TL,  c.id AS AGENT,
                            c.full_name AS DC, a.CallAccountStatus AS ACCOUNT_STATUSID, st.CallReasonDesc as ACCOUNT_STATUS,
                            a.AccountNo AS 'ACCOUNTNO', a.CallHistoryCallDate 'TGLUPDATE'                            
                            FROM t_gn_callhistory a                            
                            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                            LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                            LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId                            
                            WHERE
                            CallHistoryCallDate BETWEEN '".$start_date." 00:00:00' AND '".$start_date." 23:00:00'
                            AND ag.userId!=''                            
                            ORDER BY TL_UID,a.ACCOUNTNO ASC
                ")->result();                
                // test
                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_attemp as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      #52_R014_OS-ON PROCESS

                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                      
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }
                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }

                #print_r($user_TL);
                #exit();
                $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){
                    
                    $data = $ON_STATUS;
                    $info = array();

                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }
                $arr = array();
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  // $ALL_ATTEMP = 0;  

                  foreach ($groups as $agent_id => $d){

                    $TOTAL_NEW = $TOTAL_NEW + $d['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $d['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $d['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $d['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $d['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $d['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $d['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $d['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $d['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $d['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $d['DECEASE'];

                    $ATTEMP = $d['NEW'] + $d['OS-ON PROCESS']+ $d['OP-ON PROSPECT']+ $d['VALID']+ $d['ON NEGO']+ 
                                  $d['PTP-NEW']+$d['PTP-POP']+$d['PTP-BP']+$d['PAID OFF']+$d['BUSY LINE']+$d['DECEASE'];
                    
                                  array_push($arr, $ATTEMP);
                  }
                }


                $no_agent = 1;
                $data_agent = $this->db->query("
                      SELECT	
                      ag.UserId AS TL_UID,
                      ag.full_name as TL,
                      c.id AS AGENT,
                      c.full_name AS DC,
                      a.CallAccountStatus AS ACCOUNT_STATUSID,
                      st.CallReasonDesc as ACCOUNT_STATUS,
                      a.AccountNo AS 'ACCOUNTNO',
                      a.CallHistoryCallDate 'TGLUPDATE'

                    FROM (
                      SELECT AccountNo,MAX(CallHistoryCallDate) AS GROUP_ID 
                      FROM t_gn_callhistory 	
                      WHERE
                        CallHistoryCallDate BETWEEN '".$start_date." 00:00:00' AND '".$start_date." 23:00:00' 
                        AND TeamLeaderId!=''
                        
                        
                      GROUP BY AccountNo,AgentCode
                    ) AS TBL_TMP
                    
                    INNER JOIN t_gn_callhistory a ON (a.CallHistoryCallDate=TBL_TMP.GROUP_ID AND a.AccountNo=TBL_TMP.AccountNo)

                    LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                    LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                    LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                    LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId 

                    WHERE
                      CallHistoryCallDate BETWEEN '".$start_date." 00:00:00' AND '".$start_date." 23:00:00'
                      AND ag.userId!=''
                      
                    ORDER BY TL_UID,a.ACCOUNTNO ASC
                ")->result();

                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_agent as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){

                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;                      
                    }

                    $user_TL[$tl_uid] = $agent;

                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();
                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }
               $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){                    
                    $data = $ON_STATUS;
                    $info = array();
                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }                              
                  $no = 0; $no2 = 0;
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  $ALL_ATTEMP = 0;  
                  foreach ($groups as $agent_id => $row){

                    $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];

                    $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                                  $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
                    
                    $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
                    $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 
                    
                    echo '<tr border="1px green" cellspacing="0">';

                    if($first){
                      echo '<td class="text-left">'.$row['TL'].'</td>';
                      $first=false;
                    }else{
                      echo '<td></td>';
                    }
                    echo '
                      <td>'.$row['AGENT'].'</td>
                      <td class="text-left">'.$row['DC'].'</td>
                      <td>'.$row['NEW'].'</td>
                      <td>'.$row['OS-ON PROCESS'].'</td>
                      <td>'.$row['OP-ON PROSPECT'].'</td>
                      <td>'.$row['VALID'].'</td>
                      <td>'.$row['ON NEGO'].'</td>
                      <td>'.$row['PTP-NEW'].'</td>
                      <td>'.$row['PTP-POP'].'</td>
                      <td>'.$row['PTP-BP'].'</td>
                      <td>'.$row['PAID OFF'].'</td>
                      <td>'.$row['BUSY LINE'].'</td>
                      <td>'.$row['DECEASE'].'</td>
                      <td>'.$GRANT_TOTAL.'</td>';                      
                      echo '<td>'.$arr[$no++].'</td>';
                      echo '</tr>';
                  }
                    
                  echo '
                  <tr border="1px green" cellspacing="0">
                    <td colspan="3" class="text-left"><strong>TOTAL</strong></td>
                    <td>'.$TOTAL_NEW.'</td>
                    <td>'.$TOTAL_ON_PROCESS.'</td>
                    <td>'.$TOTAL_ON_PROSPECT.'</td>
                    <td>'.$TOTAL_VALID.'</td>
                    <td>'.$TOTAL_ON_NEGO.'</td>
                    <td>'.$TOTAL_PTP_NEW.'</td>
                    <td>'.$TOTAL_PTP_POP.'</td>
                    <td>'.$TOTAL_PTP_BP.'</td>
                    <td>'.$TOTAL_PAID_OFF.'</td>
                    <td>'.$TOTAL_BUSY_LINE.'</td>
                    <td>'.$TOTAL_DECEASE.'</td>
                    <td>'.$ALL_GRANT_TOTAL.'</td>
                    <td>'.$ALL_ATTEMP.'</td>
                  </tr>';

                  echo '<tr border="1px green" cellspacing="0"><td colspan="14"></td></tr>';
                
                }
              ?>
            </tbody>
          </table>
  <?php
      }
    }
    public function detailweekly()
    {
      $start_date = $_GET['start_date'];
      //$end_date = $_GET['end_date'];
      
      // test
      $tanggal = date('Y-m-d');
      $Pecah = explode( "-", $tanggal );
      //Menampilkan otomatis menggunakan for
      for ( $i = 0; $i < count( $Pecah ); $i++ ) {
      // echo $Pecah[$i] . "<br />";
      }
      $nilai = 7;
	  //by rangga
      $hari =  $Pecah[2];
      $hasil = $nilai - $hari;
      
      //Menampilkan secara manual dengan mengakses indexnya\
      $data = $Pecah[0] ."-". $Pecah[1] ."-".$hasil. "<br />";
      
     // echo "tanggal-fix : ".$tanggal. "<br />";
      
     // echo $Pecah[0] ."-". $Pecah[1] ."-".$hasil. "<br />";
     // echo "Tanggal : "  .$Pecah[2] . "<br />";
      
     // echo "Jumlah : " . $hasil . "<br />";
     // echo $data. "<br />";
      $arr = array($Pecah[0],$Pecah[1],$hasil);
      $strip = "-";
      $fungsi= implode($strip,$arr);
	  
      //echo $fungsi;
      // test

        if ($_GET['button'] == 'ActionWeekly') {
            
        // //generate to excel
        // Excel() -> HTML_Excel();
        // //cek mode
        
            $this->load->view('dailyactivityreport/detailweekly');
        } else if ($_GET['button'] == 'ExcelWeekly'){          
       //generate to excel
        Excel() -> HTML_Excel();
		$judul =  $this->db->query("select DATE(a.CallHistoryCallDate) AS callDate from t_gn_callhistory a where a.CallHistoryCallDate BETWEEN DATE_ADD('".$start_date."', INTERVAL -6 DAY) AND '".$start_date." 23:00:00' LIMIT 1 ")->row_Array();

      //cek mode
        ?>      
                <h1>REPORT WEEKLY ACTIVITY <?php echo $judul['callDate'] ?> - <?php echo $start_date ?> </h1>
                <table border="1px green" cellspacing="0">
                <thead>
                  <tr >                    
                    <th>TL</th>
                    <th>AGENT</th>
                    <th>DC</th>
                    <th>NEW</th>
                    <th>OS-ON PROCESS</th>
                    <th>OP-ON PROSPECT</th>
                    <th>VALID</th>
                    <th>ON NEGO</th>
                    <th>PTP-NEW</th>
                    <th>PTP-POP</th>
                    <th>PTP-BP</th>
                    <th>PAID OFF</th>
                    <th>BUSY LINE</th> 
                    <th>DECEASE</th>
                    <th>Grand Total</th>
                    <th>Atemp</th>
                  </tr>
                <thead>
                <tbody>                
                <?php     
					
                $start_date = $_GET['start_date'];
              //  $end_date = $_GET['end_date'];
                // test
                $data_attemp = $this->db->query("
                              SELECT 
                            ag.UserId AS TL_UID, ag.full_name as TL,  c.id AS AGENT,
                            c.full_name AS DC, a.CallAccountStatus AS ACCOUNT_STATUSID, st.CallReasonDesc as ACCOUNT_STATUS,
                            a.AccountNo AS 'ACCOUNTNO', a.CallHistoryCallDate 'TGLUPDATE'                            
                            FROM t_gn_callhistory a                            
                            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                            LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                            LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId                            
                            WHERE
                            CallHistoryCallDate BETWEEN DATE_ADD('".$start_date."', INTERVAL -6 DAY) AND '".$start_date." 23:00:00'
                            AND ag.userId!=''                            
                            ORDER BY TL_UID,a.ACCOUNTNO ASC
                ")->result();                
                // test
                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_attemp as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      #52_R014_OS-ON PROCESS

                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                      
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }
                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }

                #print_r($user_TL);
                #exit();
                $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){
                    
                    $data = $ON_STATUS;
                    $info = array();

                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }
                $arr = array();
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  // $ALL_ATTEMP = 0;  

                  foreach ($groups as $agent_id => $d){

                    $TOTAL_NEW = $TOTAL_NEW + $d['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $d['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $d['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $d['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $d['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $d['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $d['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $d['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $d['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $d['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $d['DECEASE'];

                    $ATTEMP = $d['NEW'] + $d['OS-ON PROCESS']+ $d['OP-ON PROSPECT']+ $d['VALID']+ $d['ON NEGO']+ 
                                  $d['PTP-NEW']+$d['PTP-POP']+$d['PTP-BP']+$d['PAID OFF']+$d['BUSY LINE']+$d['DECEASE'];
                    
                                  array_push($arr, $ATTEMP);
                  }
                }


                $no_agent = 1;
                $data_agent = $this->db->query("
                      SELECT	
                      ag.UserId AS TL_UID,
                      ag.full_name as TL,
                      c.id AS AGENT,
                      c.full_name AS DC,
                      a.CallAccountStatus AS ACCOUNT_STATUSID,
                      st.CallReasonDesc as ACCOUNT_STATUS,
                      a.AccountNo AS 'ACCOUNTNO',
                      a.CallHistoryCallDate 'TGLUPDATE'

                    FROM (
                      SELECT AccountNo,MAX(CallHistoryCallDate) AS GROUP_ID 
                      FROM t_gn_callhistory 	
                      WHERE
                        CallHistoryCallDate BETWEEN DATE_ADD('".$start_date."', INTERVAL -6 DAY) AND '".$start_date." 23:00:00' 
                        AND TeamLeaderId!=''
                        
                        
                      GROUP BY AccountNo,AgentCode
                    ) AS TBL_TMP
                    
                    INNER JOIN t_gn_callhistory a ON (a.CallHistoryCallDate=TBL_TMP.GROUP_ID AND a.AccountNo=TBL_TMP.AccountNo)

                    LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                    LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                    LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                    LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId 

                    WHERE
                      CallHistoryCallDate BETWEEN DATE_ADD('".$start_date."', INTERVAL -6 DAY) AND '".$start_date." 23:00:00'
                      AND ag.userId!=''
                      
                    ORDER BY TL_UID,a.ACCOUNTNO ASC
                ")->result();

                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_agent as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){

                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;                      
                    }

                    $user_TL[$tl_uid] = $agent;

                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();
                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }
               $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){                    
                    $data = $ON_STATUS;
                    $info = array();
                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }                              
                  $no = 0; $no2 = 0;
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  $ALL_ATTEMP = 0;  
                  foreach ($groups as $agent_id => $row){

                    $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];

                    $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                                  $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
                    
                    $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
                    $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 
                    
                    echo '<tr border="1px green" cellspacing="0">';

                    if($first){
                      echo '<td class="text-left">'.$row['TL'].'</td>';
                      $first=false;
                    }else{
                      echo '<td></td>';
                    }
                    echo '
                      <td>'.$row['AGENT'].'</td>
                      <td class="text-left">'.$row['DC'].'</td>
                      <td>'.$row['NEW'].'</td>
                      <td>'.$row['OS-ON PROCESS'].'</td>
                      <td>'.$row['OP-ON PROSPECT'].'</td>
                      <td>'.$row['VALID'].'</td>
                      <td>'.$row['ON NEGO'].'</td>
                      <td>'.$row['PTP-NEW'].'</td>
                      <td>'.$row['PTP-POP'].'</td>
                      <td>'.$row['PTP-BP'].'</td>
                      <td>'.$row['PAID OFF'].'</td>
                      <td>'.$row['BUSY LINE'].'</td>
                      <td>'.$row['DECEASE'].'</td>
                      <td>'.$GRANT_TOTAL.'</td>';                      
                      echo '<td>'.$arr[$no++].'</td>';
                      echo '</tr>';
                  }
                    
                  echo '
                  <tr border="1px green" cellspacing="0">
                    <td colspan="3" class="text-left"><strong>TOTAL</strong></td>
                    <td>'.$TOTAL_NEW.'</td>
                    <td>'.$TOTAL_ON_PROCESS.'</td>
                    <td>'.$TOTAL_ON_PROSPECT.'</td>
                    <td>'.$TOTAL_VALID.'</td>
                    <td>'.$TOTAL_ON_NEGO.'</td>
                    <td>'.$TOTAL_PTP_NEW.'</td>
                    <td>'.$TOTAL_PTP_POP.'</td>
                    <td>'.$TOTAL_PTP_BP.'</td>
                    <td>'.$TOTAL_PAID_OFF.'</td>
                    <td>'.$TOTAL_BUSY_LINE.'</td>
                    <td>'.$TOTAL_DECEASE.'</td>
                    <td>'.$ALL_GRANT_TOTAL.'</td>
                    <td>'.$ALL_ATTEMP.'</td>
                  </tr>';

                  echo '<tr border="1px green" cellspacing="0"><td colspan="14"></td></tr>';
                
                }
              ?>
            </tbody>
          </table>
  <?php
      }
    }
// test
public function detailmonthly()
    {
      $start_date = $_GET['start_date'];
      $date_statis = date('Y-m-01');
      
     
        if ($_GET['button'] == 'ActionMonthly') {
            
        // //generate to excel
        // Excel() -> HTML_Excel();
        // //cek mode
        
            $this->load->view('dailyactivityreport/detailmonthly');
        } else if ($_GET['button'] == 'ExcelMonthly'){          
       //generate to excel
	   
	    $start_date = $_GET['start_date'];
 //$end_date = $_GET['end_date'];
 
 // test
 $tanggal = date('Y-m-d');
 $Pecah = explode( "-", $start_date );
 //Menampilkan otomatis menggunakan for
 for ( $i = 0; $i < count( $Pecah ); $i++ ) {
 // echo $Pecah[$i] . "<br />";
 }
 $nilai = 7;
 //by rangga
 $hari 	=  $Pecah[2];
 $hasil = $nilai - $hari;
 $satu	= 1; 
 
 //Menampilkan secara manual dengan mengakses indexnya\
 $data = $Pecah[0] ."-". $Pecah[1] ."-".$satu. "<br />";
 
// echo "tanggal-fix : ".$tanggal. "<br />";
 
// echo $Pecah[0] ."-". $Pecah[1] ."-".$hasil. "<br />";
// echo "Tanggal : "  .$Pecah[2] . "<br />";
 
// echo "Jumlah : " . $hasil . "<br />";
// echo $data. "<br />";
 $arr = array($Pecah[0],$Pecah[1],$satu);
 $strip = "-";
 $fungsi= implode($strip,$arr);
 
 //echo $fungsi;
 // test
$date_statis = date('Y-m-01');
$judul =  $this->db->query("select DATE(a.CallHistoryCallDate) AS callDate from t_gn_callhistory a where a.CallHistoryCallDate BETWEEN DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND CURDATE() LIMIT 1 ")->row_Array();
//var_dump($this->db->last_query());
//var_dump($judul);

        Excel() -> HTML_Excel();
		$judul =  $this->db->query("select DATE(a.CallHistoryCallDate) AS callDate from t_gn_callhistory a where a.CallHistoryCallDate BETWEEN DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND CURDATE() LIMIT 1 ")->row_Array();

      //cek mode
        ?>      
                <h1>REPORT MONTHLY ACTIVITY <?php echo $fungsi ?> - <?php echo $start_date ?> </h1>
                <table border="1px green" cellspacing="0">
                <thead>
                  <tr >                    
                    <th>TL</th>
                    <th>AGENT</th>
                    <th>DC</th>
                    <th>NEW</th>
                    <th>OS-ON PROCESS</th>
                    <th>OP-ON PROSPECT</th>
                    <th>VALID</th>
                    <th>ON NEGO</th>
                    <th>PTP-NEW</th>
                    <th>PTP-POP</th>
                    <th>PTP-BP</th>
                    <th>PAID OFF</th>
                    <th>BUSY LINE</th> 
                    <th>DECEASE</th>
                    <th>Grand Total</th>
                    <th>Atemp</th>
                  </tr>
                <thead>
                <tbody>                
                <?php     
					
                $start_date = $_GET['start_date'];
              //  $end_date = $_GET['end_date'];
                // test
                $data_attemp = $this->db->query("
                              SELECT 
                            ag.UserId AS TL_UID, ag.full_name as TL,  c.id AS AGENT,
                            c.full_name AS DC, a.CallAccountStatus AS ACCOUNT_STATUSID, st.CallReasonDesc as ACCOUNT_STATUS,
                            a.AccountNo AS 'ACCOUNTNO', a.CallHistoryCallDate 'TGLUPDATE'                            
                            FROM t_gn_callhistory a                            
                            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                            LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                            LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId                            
                            WHERE
                            CallHistoryCallDate BETWEEN '".$fungsi."' AND '".$start_date." 23:00:00'
                            AND ag.userId!=''                            
                            ORDER BY TL_UID,a.ACCOUNTNO,AGENT ASC
                ")->result();                
                // test
                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_attemp as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      #52_R014_OS-ON PROCESS

                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                      
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }
                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }

                #print_r($user_TL);
                #exit();
                $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){
                    
                    $data = $ON_STATUS;
                    $info = array();

                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }
                $arr = array();
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  // $ALL_ATTEMP = 0;  

                  foreach ($groups as $agent_id => $d){

                    $TOTAL_NEW = $TOTAL_NEW + $d['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $d['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $d['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $d['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $d['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $d['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $d['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $d['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $d['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $d['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $d['DECEASE'];

                    $ATTEMP = $d['NEW'] + $d['OS-ON PROCESS']+ $d['OP-ON PROSPECT']+ $d['VALID']+ $d['ON NEGO']+ 
                                  $d['PTP-NEW']+$d['PTP-POP']+$d['PTP-BP']+$d['PAID OFF']+$d['BUSY LINE']+$d['DECEASE'];
                    
                                  array_push($arr, $ATTEMP);
                  }
                }


                $no_agent = 1;
                $data_agent = $this->db->query("
                      SELECT	
                      ag.UserId AS TL_UID,
                      ag.full_name as TL,
                      c.id AS AGENT,
                      c.full_name AS DC,
                      a.CallAccountStatus AS ACCOUNT_STATUSID,
                      st.CallReasonDesc as ACCOUNT_STATUS,
                      a.AccountNo AS 'ACCOUNTNO',
                      a.CallHistoryCallDate 'TGLUPDATE'

                    FROM (
                      SELECT AccountNo,MAX(CallHistoryCallDate) AS GROUP_ID 
                      FROM t_gn_callhistory 	
                      WHERE
                        CallHistoryCallDate BETWEEN '".$fungsi."' AND '".$start_date." 23:00:00' 
                        AND TeamLeaderId!=''
                        
                        
                      GROUP BY AccountNo,AgentCode
                    ) AS TBL_TMP
                    
                    INNER JOIN t_gn_callhistory a ON (a.CallHistoryCallDate=TBL_TMP.GROUP_ID AND a.AccountNo=TBL_TMP.AccountNo)

                    LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
                    LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
                    LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
                    LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId 

                    WHERE
                      CallHistoryCallDate BETWEEN '".$fungsi."' AND '".$start_date." 23:00:00'
                      AND ag.userId!=''
                      
                    ORDER BY TL_UID,a.ACCOUNTNO,AGENT ASC
                ")->result();

                $ON_STATUS = array(
                  'NEW' => 0,
                  'OS-ON PROCESS' => 0,
                  'OP-ON PROSPECT' => 0,
                  'VALID' => 0,
                  'ON NEGO' => 0,
                  'PTP-NEW' => 0,
                  'PTP-POP' => 0,
                  'PTP-BP' => 0,
                  'PAID OFF' => 0,
                  'BUSY LINE' => 0, 
                  'DECEASE' => 0
                );
                $user_TL = array();
                foreach ($data_agent as $r) {
                  $tl_uid = '#'.$r->TL_UID;
                  $tl_name = $r->TL;
                  $status = $r->ACCOUNT_STATUS;
                  $agent_id = $r->AGENT;

                  if ($user_TL[$tl_uid]){

                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;                      
                    }

                    $user_TL[$tl_uid] = $agent;

                  }else{
                    $user_TL[$tl_uid] = array();
                    $agent = array();
                    
                    // agent array
                    if($agent[$agent_id][$status]){
                      $agent[$agent_id][$status][] = $r;
                    }else{
                      $agent[$agent_id][$status] = array();
                      $agent[$agent_id][$status][] = $r;
                    }

                    $user_TL[$tl_uid] = $agent;
                    //print_r($user_TL);
                    //exit();
                  }
                  
                }
               $user = array();
                foreach ($user_TL as $user_id => $groups) {
                  foreach($groups as $agent_id=>$row){                    
                    $data = $ON_STATUS;
                    $info = array();
                    foreach($row as $status=>$r){
                      $info = (array)$r[0];
                      $data[trim($status)] = count($r);
                    }
                    unset($info['ACCOUNT_STATUS']);                    
                    $data = array_merge($info,$data);
                    $user[$user_id][$agent_id] = $data;                    
                  }
                }                              
                  $no = 0; $no2 = 0;
                foreach ($user as $user_id => $groups) {                  
                  $first = true;
                  $TOTAL_NEW = 0;
                  $TOTAL_ON_PROCESS = 0;
                  $TOTAL_ON_PROSPECT = 0;
                  $TOTAL_VALID = 0;
                  $TOTAL_ON_NEGO = 0;
                  $TOTAL_PTP_NEW = 0;
                  $TOTAL_PTP_POP = 0;
                  $TOTAL_PTP_BP = 0;
                  $TOTAL_PAID_OFF = 0;
                  $TOTAL_BUSY_LINE = 0;
                  $TOTAL_DECEASE = 0;
                  $ALL_GRANT_TOTAL = 0;  
                  $ALL_ATTEMP = 0;  
                  foreach ($groups as $agent_id => $row){

                    $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
                    $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];

                    $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OP-ON PROSPECT'];
                    $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
                    $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
                    $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
                    $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
                    $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
                    $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
                    $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
                    $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];

                    $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                                  $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
                    
                    $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
                    $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 
                    
                    echo '<tr border="1px green" cellspacing="0">';

                    if($first){
                      echo '<td class="text-left">'.$row['TL'].'</td>';
                      $first=false;
                    }else{
                      echo '<td></td>';
                    }
                    echo '
                      <td>'.$row['AGENT'].'</td>
                      <td class="text-left">'.$row['DC'].'</td>
                      <td>'.$row['NEW'].'</td>
                      <td>'.$row['OS-ON PROCESS'].'</td>
                      <td>'.$row['OP-ON PROSPECT'].'</td>
                      <td>'.$row['VALID'].'</td>
                      <td>'.$row['ON NEGO'].'</td>
                      <td>'.$row['PTP-NEW'].'</td>
                      <td>'.$row['PTP-POP'].'</td>
                      <td>'.$row['PTP-BP'].'</td>
                      <td>'.$row['PAID OFF'].'</td>
                      <td>'.$row['BUSY LINE'].'</td>
                      <td>'.$row['DECEASE'].'</td>
                      <td>'.$GRANT_TOTAL.'</td>';                      
                      echo '<td>'.$arr[$no++].'</td>';
                      echo '</tr>';
                  }
                    
                  echo '
                  <tr border="1px green" cellspacing="0">
                    <td colspan="3" class="text-left"><strong>TOTAL</strong></td>
                    <td>'.$TOTAL_NEW.'</td>
                    <td>'.$TOTAL_ON_PROCESS.'</td>
                    <td>'.$TOTAL_ON_PROSPECT.'</td>
                    <td>'.$TOTAL_VALID.'</td>
                    <td>'.$TOTAL_ON_NEGO.'</td>
                    <td>'.$TOTAL_PTP_NEW.'</td>
                    <td>'.$TOTAL_PTP_POP.'</td>
                    <td>'.$TOTAL_PTP_BP.'</td>
                    <td>'.$TOTAL_PAID_OFF.'</td>
                    <td>'.$TOTAL_BUSY_LINE.'</td>
                    <td>'.$TOTAL_DECEASE.'</td>
                    <td>'.$ALL_GRANT_TOTAL.'</td>
                    <td>'.$ALL_ATTEMP.'</td>
                  </tr>';

                  echo '<tr border="1px green" cellspacing="0"><td colspan="14"></td></tr>';
                
                }
              ?>
            </tbody>
          </table>
  <?php
      }
    }
// test
    function nama_tl($id)
    {
        $nama_tl = $this->db->query("SELECT full_name, code_user FROM t_tx_agent WHERE UserId='$id'")->result();

        return $nama_tl[0]->code_user . " - " . $nama_tl[0]->full_name;
    }

    public function crontab()
    {

      //$tglLike = '2019-06';
      $tglLike = date('Y-m');
      $this->db->query("truncate t_gn_ranking");
      $this->db->query("truncate t_gn_siklus_data");
      $this->db->query("truncate t_gn_siklus_report");
      $this->db->query("truncate t_gn_siklus_user");
      $data = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
      FROM t_gn_payment
      INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
      WHERE t_gn_payment.pay_date LIKE '%$tglLike%'
      GROUP BY tl_id
      ORDER BY amount_tl DESC")->result();

      foreach ($data as $row) {

          $nama_tl = $this->nama_tl($row->tl_id);
          $no = 0;
          $data_agent = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.id as id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl, t_gn_payment.pay_date
          FROM t_gn_payment
          INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
          WHERE t_tx_agent.tl_id='$row->tl_id' AND t_gn_payment.pay_date LIKE '%$tglLike%'
          GROUP BY t_tx_agent.id
          ORDER BY amount_tl DESC")->result();

          foreach ($data_agent as $row_agent) {
              $no++;
              $date = date('Y-m-d H:i:s');
              $pay_date = date('Ym');
              $this->db->query("INSERT into t_gn_ranking VALUES('','$no','$row_agent->code_user', '$nama_tl', '$row_agent->amount_tl','$pay_date','$date')");
              // $push['no'] = $no;
              // echo array_push($row_agent, $push);
              // echo json_encode($row_agent);

          }
      }
      echo "sukses";
    }



            /*
        * @ def 		: content / default pages controller 
        * -----------------------------------------
        *
        * @ params  	: post & definition paymode 
        * @ return 	: void(0)
        */
}

        // END OF CLASS 

        ?>