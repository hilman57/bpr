<?php
/*
 * @def 	: FaxContent
 * ------------------------------------
 * @param 	: array()
 */
 
class FaxContent extends EUI_Controller
{

// constructor 

function FaxContent()
{
	parent::__construct();	
	$this ->load->model(array(base_class_model($this),'M_Combo','M_Configuration'));
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function ResendFax()
{
  $_conds = array('success' => 0 );
  if( $this ->URI->_get_have_post('OutboxId') )
  {
	if( $rs = $this -> {base_class_model($this)}->_setResendFax( $this -> URI->_get_post('OutboxId')) )
	{
		$_conds = array('success' => 1);
	}
  }
  
  __(json_encode($_conds));
 
} 


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Preview()
{

 $this -> load->model('M_Configuration');
 $FTP = $this -> M_Configuration -> _getFTP();	

  
 if( $_faxmile = $this -> {base_class_model($this)} -> _getViewFaxFile( $this -> URI -> _get_post('OutboxId')) ) 
 {
	
	$this->load->library('Ftp');
	$this->Ftp->connect(array('hostname' => $FTP['FTP_SERVER1'],
		'port' => $FTP['FTP_PORT'],'username' => $FTP['FTP_USER1'],
		'password' => $FTP['FTP_PASSWORD1'])
	);
	
	if( $this -> Ftp->_is_conn() 
		AND !is_null($_faxmile['OutboxFaxPath']))
	{
		$_found   = null;
		
		$this->Ftp->changedir($_faxmile['OutboxFaxPath']); 
		$_ftplist = $this -> Ftp -> list_files('.');
			
		foreach($_ftplist as $k => $src )
		{
			if( ($src == $_faxmile['OutboxFaxFile'])) {
				$_found = $src;
			}
		}
			
		if(!defined('RECPATH') ) define('RECPATH',str_replace('system','application', BASEPATH)."temp");
		
			if( !is_null($_found) ) 
			{
				$FAX_FILE_DOCUMENT =  preg_replace("/\.tiff/i",".pdf", $_found);
				if($this -> Ftp -> download(RECPATH . "/$FAX_FILE_DOCUMENT", $FAX_FILE_DOCUMENT )) {
					$this -> load->view("mod_fax_content/view_fax_preview", array('file' => $FAX_FILE_DOCUMENT) );
				}
		  }
		}
		else
			exit('ftp connection is lose.');
			
	}
	
}	

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }
 	
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function index()
{
  if( $this -> EUI_Session->_get_session('UserId')) 
  {
	$UI = array(
		'page' => $this -> {base_class_model($this)}->_get_default(),
		'combo' => $this -> _getCombo() );
	 $this -> load -> view('mod_fax_content/view_fax_content_nav',$UI);
   }	
}
	
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

function content()
{
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array
		( 
		  'fax_status' => $this->{base_class_model($this)} ->_getFaxStatus(),
		  'page'  => $this->{base_class_model($this)} ->_get_resource(),
		  'num'   => $this->{base_class_model($this)} ->_get_page_number() 
		 );
		$this -> load -> view('mod_fax_content/view_fax_content_list',$UI);
	}
}	

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

function DeleteFax()
{
	$_conds = array('success' => 0);
	$_conds = array('success' => $this -> {base_class_model($this)} -> _setDeleteFax( $this -> URI->_get_array_post('DataId') ));
	echo json_encode($_conds);
}
 
}