<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SysUser extends EUI_Controller{

/*
 * load __construct
 */
 
function __construct()
{
	parent::__construct();
	$this -> load -> model(array('M_SysUser','M_Configuration')); // load model user
}
/*
 * load first page load of the nav data 
 */
 
function index() 
{
	if( $this -> EUI_Session -> _have_get_session('UserId')) 
	{
		$page['page'] = $this -> M_SysUser -> get_default();
		$this -> load -> view("sys_user/view_user_nav", $page);
	}
}


/*
 * load first page load of the nav data 
 * @ content of page 
 */
 
function content()
{
	$page['page'] = $this -> M_SysUser -> get_resource_query(); // load content data by pages 
	$page['num']  = $this -> M_SysUser -> get_page_number(); 	// load content data by pages 
	if( is_array($page) && is_object($page['page']) ) 
	{
		$this -> load -> view("sys_user/view_user_list", $page );
	}	
}

/*
 * reset password user 
 * @ return procedure 
 */
 
 function resign_user(){
	 $_success = array('success'=> 0);
	 $UserId = $this -> URI -> _get_array_post('UserId');
	 if (is_array( $UserId )){
		 if($this-> M_SysUser -> _set_user_resign( $UserId )){
			 $_success = array('success'=> 1);
		 }
	 }
	 echo json_encode($_success);
 }
 
function reset_password()
{
	$_success = array('success'=> 0);
	$UserId = $this -> URI -> _get_array_post('UserId');
	if( is_array( $UserId ) )
	{
		if( $this -> M_SysUser -> _reset_user_password( $UserId ) ) {
			$_success = array('success'=> 1);	
		}
	}
	
	echo json_encode($_success);
} 
/*
 * reset IP user 
 */
 
function reset_ip()
{
	$_success = array('success'=> 0);
	$UserId = $this -> URI -> _get_array_post('UserId');
	if( is_array( $UserId ) )
	{
		if( $this -> M_SysUser -> _reset_user_location( $UserId ) ) {
			$_success = array('success'=> 1);	
		}
	}
	echo json_encode($_success);
}

// disable_user 

function disable_user()
{
	$_success = array('success'=> 0);
	$UserId = $this -> URI -> _get_array_post('UserId');
	if( is_array( $UserId ) )
	{
		if( $this -> M_SysUser -> _disable_user( $UserId ) ) {
			$_success = array('success'=> 1);	
		}
	}
	echo json_encode($_success);
}

// enable_user 

function enable_user()
{
	$_success = array('success'=> 0);
	$UserId = $this -> URI -> _get_array_post('UserId');
	if( is_array( $UserId ) )
	{
		if( $this -> M_SysUser -> _enable_user( $UserId ) ) {
			$_success = array('success'=> 1);	
		}
	}
	echo json_encode($_success);
}

function enable_userAll(){
	
	$_success = array('success'=>0);
	if($this->M_SysUser->_enable_userAll()){
		$_success = array('success'=>1);
	}
	echo json_encode($_success);

}
// UserSession

public function UserSession()
{
	session_start(); $_conds = array();
	foreach( $this->EUI_Session->get_real_session() as $k => $v ) {
		if( !is_null($v) AND !is_array($v)) {
			$_conds[base64_encode($k)] = base64_encode($v);	
		}	
	}
	
	echo json_encode($_conds);
}


// remove_user

function remove_user()
{
	$_success = array('success'=> 0,'error'=> '' );
	$UserId = $this -> URI -> _get_array_post('UserId');
	if( is_array( $UserId ) )
	{
		if( $this -> M_SysUser -> _remove_user( $UserId ) ) {
			$_success = array('success'=> 1);	
		}
		else{
			$_success = array('success'=> 0, 'error'=> mysql_error() );	
		}
	}
	
	echo json_encode($_success);
}

// tpl_add_user

function tpl_add_user()
{
	if( $this -> EUI_Session -> _get_session('UserId'))
	{
		$_EUI['User'] = $this -> M_SysUser;
		$this -> load -> view('sys_user/view_add_user', $_EUI);
	}
}

// add tpl_edit_user

function tpl_edit_user()
{
	if( $this -> EUI_Session -> _get_session('UserId'))
	{
		$_EUI['User'] = $this -> M_SysUser;
		$_EUI['rows'] = $this -> M_SysUser -> _get_detail_user($this -> URI -> _get_post('UserId'));
		$this -> load -> view('sys_user/view_edit_user', $_EUI);
	}
}


// add set new user 

function add_user()
{
	$_success = array('success'=> 0,'error'=> '' );
	if( $this -> EUI_Session -> _have_get_session('UserId')) 
	{
		if(  $this -> M_SysUser -> _set_add_user() )
		{
			$_success = array( 'success' => 1, 'error' => mysql_error() );
		}
	}
	
	echo json_encode($_success);
}

// update_user

function update_user()
{
	$_success = array('success'=> 0,'error'=> '' );
	if( $this -> EUI_Session -> _have_get_session('UserId')) 
	{
		if(  $this -> M_SysUser -> _set_update_user() )
		{
			$_success = array( 'success' => 1, 'error' => mysql_error() );
		}
	}
	
	echo json_encode($_success);
}

// register_pbx

function register_pbx()
{
	$_success = array('success'=> 0,'error'=> '' );	
	$_array_list = $this -> URI -> _get_array_post('UserId'); //	1,2,5,6,9,10,23,24,25,26,27,28,29,30,31,32,33,34,35 
	
	if( $this -> EUI_Session -> _have_get_session('UserId')) 
	{
		$_error = $this -> M_SysUser -> _set_register_user_pbx( $_array_list );
		if($_error)
		{
			$_success = array( 'success' => 1, 'error' => $_error );
		}
		else{
			$_success = array( 'success' => 0, 'error' => $_error );
		}
	}
	
	echo json_encode($_success);
}

// User UserCapacity

function UserCapacity()
{
	$_result = array('success'=>0);
	
	$Configuration =& M_Configuration::get_instance();
	$Capacity = $this->M_SysUser->_getUserCapacity(1);
	$UserLimit = $Configuration->_getUserLimit();
	
	
	 
	if( is_array($UserLimit))
	{
		$CapacityCount = (((INT)$UserLimit['USER_LIMIT'])+1);
		
		if( $this -> EUI_Session->_get_session('HandlingType')!=USER_ROOT )
		{
			if( $Capacity < $CapacityCount )
			{
				$_result = array('success'=>1);
			}
		}
		else{
			$_result = array('success'=>1);
		}	
	}
	
	echo json_encode($_result);
}


// end function

}

// END OF FILE 
// location : ./application/controller/SysUser.php
?>