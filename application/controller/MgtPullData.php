<?php

/*
 * @ class : MgtPullData 
 * @ param : index 
 */

class MgtPullData
extends EUI_Controller
{


	// -------------------------------------
	/*
 * @ pack : constructor 
 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(base_class_model($this)));
		$this->load->helper(array('EUI_Object', 'EUI_Report'));
	}


	// -------------------------------------
	/*
 * @ pack : Instance of class  
 */

	public function SwapCombo()
	{
		$_serialize = array();
		$_combo = $this->M_Combo->_getSerialize();
		foreach ($_combo as $keys => $method) {
			if ((STRTOLOWER($keys) != 'serialize') and (STRTOLOWER($keys) != 'instance')
				and (STRTOLOWER($keys) != 'nstruct') and (STRTOLOWER($keys) != 't')
			) {
				if (STRTOLOWER($keys) == 'user') {
					$_serialize[$keys] = $this->SwapAgentAssign();
				} else {
					$_serialize[$keys] = $this->M_Combo->$method();
				}
			}
		}

		return $_serialize;
	}

	// -------------------------------------
	/*
 * @ pack : Instance of class  
 */

	public function SwapAgentAssign()
	{
		$_conds = array();
		$Agent = $this->M_SysUser->_get_user_by_login();

		$no = 0;
		foreach ($Agent as $k => $rows) {
			$_conds[$k] = $rows['full_name'];
		}

		return $_conds;
	}

	// -------------------------------------
	/*
 * @ pack : Instance of class  
 */

	protected function SwapPrivileges()
	{
		$datas = array();

		$Privileges = $this->M_SysUser->_get_handling_type();

		// USER_ROOT
		if ((_get_session('HandlingType') == USER_ROOT)) {
			$filter = array(USER_ROOT);
		}

		// USER_ADMIN
		if ((_get_session('HandlingType') == USER_ADMIN)) {
			$filter = array(USER_ROOT, USER_ADMIN, USER_QUALITY_HEAD, USER_QUALITY_STAFF);
		}

		// USER_ACCOUNT_MANAGER
		if ((_get_session('HandlingType') == USER_ACCOUNT_MANAGER)) {
			$filter = array(
				USER_ROOT, USER_ADMIN,
				USER_ACCOUNT_MANAGER, USER_QUALITY_HEAD,
				USER_QUALITY_STAFF
			);
		}

		// USER_MANAGER

		if ((_get_session('HandlingType') == USER_MANAGER)) {
			$filter = array(
				USER_ROOT, USER_ADMIN, USER_ACCOUNT_MANAGER,
				USER_MANAGER, USER_QUALITY_HEAD,
				USER_QUALITY_STAFF
			);
		}

		// USER_SUPERVISOR

		if ((_get_session('HandlingType') == USER_SUPERVISOR)) {
			$filter = array(
				USER_ROOT, USER_ADMIN, USER_ACCOUNT_MANAGER,
				USER_SUPERVISOR, USER_MANAGER,
				USER_QUALITY_HEAD,
				USER_QUALITY_STAFF
			);
		}

		// USER_LEADER

		if ((_get_session('HandlingType') == USER_LEADER)) {
			$filter = array(
				USER_ROOT, USER_ADMIN, USER_ACCOUNT_MANAGER,
				USER_SUPERVISOR, USER_MANAGER,
				USER_QUALITY_HEAD,
				USER_QUALITY_STAFF,
				USER_LEADER
			);
		}

		// USER_SENIOR_TL

		if ((_get_session('HandlingType') == USER_SENIOR_TL)) {
			$filter = array(
				USER_ROOT, USER_ADMIN, USER_ACCOUNT_MANAGER,
				USER_SUPERVISOR, USER_MANAGER,
				USER_QUALITY_HEAD,
				USER_QUALITY_STAFF,
				USER_LEADER, USER_SENIOR_TL
			);
		}


		foreach ($Privileges as $k => $User) {
			if (!@in_array($k, $filter)) {
				$datas[][$k] = $User;
			}
		}

		return $datas;
	}

	// -------------------------------------
	/*
 * @ pack : Instance of class  
 */
	public function index()
	{
		if (_have_get_session('UserId')) {
			$UI['page'] = $this->{base_class_model($this)}->_get_default();

			$UI['Deskoll'] = $this->M_SrcCustomerList->_getDeskollByLogin();
			$UI['GenderId'] = $this->M_SrcCustomerList->_getGenderId();
			$UI['CampaignId'] = $this->M_SrcCustomerList->_getCampaignByActive();
			$UI['AccountStatus'] = $this->M_SrcCustomerList->_getAccountStatus();
			$UI['LastCallStatus'] = $this->M_SrcCustomerList->_getLastCallStatus();
			$UI['privileges'] = $this->SwapPrivileges();
			$this->load->view('mgt_pull_data/view_mov_data_nav', $UI);
		}
	}



	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	public function Content()
	{
		if (_have_get_session('UserId')) {
			$UI['page'] = $this->{base_class_model($this)}->_get_resource();    // load content data by pages 
			$UI['num']  = $this->{base_class_model($this)}->_get_page_number(); // load content data by pages 

			if (is_array($UI)) {
				$this->load->view('mgt_pull_data/view_mov_data_list', $UI);
			}
		}
	}


	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	public function SwapUserLevel()
	{
		$UserPrivileges = (int)_get_post('LevelID');

		if (
			!is_null($UserPrivileges)
			and ($UserPrivileges == TRUE)
		) {
			$Level['LEVEL'] = $this->{base_class_model($this)}->_getUserLevel($UserPrivileges);
			$this->load->view('mgt_pull_data/view_privileges_list', $Level);
		} else {
			echo form()->combo('UserId', 'select auto', array());
		}
	}


	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	public function PageSwapData()
	{

		if (_have_get_session('UserId')) {
			$this->start_page = 0;
			$this->per_page   = _get_post('swp_from_page_record');
			$this->post_page  = (int)_get_post('page');

			// @ pack : set it 

			$this->result_swap = array();
			$this->result_post = new EUI_Object(_get_all_request());
			$this->content_swap = $this->{base_class_model($this)}->_get_swap_content($this->result_post);

			// @ pack : record 

			$this->total_records = count($this->content_swap);

			// @ pack : set its  
			if ($this->post_page) {
				$this->start_page = (($this->post_page - 1) * $this->per_page);
			} else {
				$this->start_page = 0;
			}

			// @ pack : set result on array
			if ((is_array($this->content_swap))
				and ($this->total_records > 0)
			) {
				$this->result_swap = array_slice($this->content_swap, $this->start_page, $this->per_page);
			}

			// @ pack : then set it to view 

			$SwapData['content_pages'] = $this->result_swap;
			$SwapData['total_records']  = $this->total_records;
			$SwapData['total_pages']  = ceil($this->total_records / $this->per_page);
			$SwapData['select_pages'] = $this->post_page;
			$SwapData['start_page']  = $this->start_page;

			if (TRUE == count($this->URI->_get_all_request()) > 0) {
				$this->load->view('mgt_pull_data/view_mov_page_data', $SwapData);
			}
		}
	} // ==> ListMoveData


	// -------------------------------------
	/*
 * @ pack : Instance of class  
 */

	public function SwapCountData()
	{
		$conds = array('total' => 100);
		__(json_encode($conds));
	}



	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */


	public function SwapData()
	{
		$_conds = array('success' => 0);
		if (_have_get_session('UserId')) {
			$SellerId = $this->URI->_get_array_post('UserId');
			$AssignId = $this->URI->_get_array_post('AssignId');

			if (count($SellerId) > 0 and count($AssignId) > 0) {
				$result = $this->{base_class_model($this)}->_setSwapData($SellerId, $AssignId);
				if ($result) {
					$_conds = array('success' => 1, 'message' => $result);
				}
			}
		}

		echo json_encode($_conds);
	}

	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	public function UploadSwapData()
	{
		if (_have_get_session('UserId')) {
			$Template = $this->M_MgtBucket->_get_template();
			foreach ($Template as $key => $val) {
				$tmptype = explode("_", $val);
				if ($tmptype[0] == "UPLOAD" && $tmptype[1] == "SWAP") {
					$Template['Template'][$key] = $val;
				}
			}

			// $Template['Template'] 	= $this->M_MgtBucket->_get_template();
			$Template['CampaignId'] = $this->M_SetCampaign->_get_campaign_name();
			if (is_array($Template)) {
				$this->load->view("mgt_pull_data/view_upload_swapdata", $Template);
			}
		}
	}


	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	public function SwapAmountData()
	{

		$_conds = array('data' => 0, 'agent' => 0);
		$param = array(
			'CallReasonId' => $this->URI->_get_array_post('CallReasonId'),
			'CampaignId' => $this->URI->_get_array_post('CampaignId'),
			'UserId' => $this->URI->_get_array_post('FromUserId'),
			'CallAttempt' => $this->URI->_get_array_post('CallAttempt'),
			'LimitAmount' => $this->URI->_get_post('LimitAmount'),
			'ToUserId' => $this->URI->_get_array_post('UserId'),
			'UserLevel' => $this->URI->_get_post('UserLevel')
		);

		$_conds = $this->{base_class_model($this)}->_setSwapAmountData($param);

		echo json_encode($_conds);
	}


	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	public function ShowAgentPerTL()
	{
		echo form()->combo('swp_from_deskoll_id', 'select long', AgentByLeader(_get_array_post('TL')));
	}

	public function ShowTLPerSTL()
	{
		echo form()->combo('ui_swp_from_tl_id', 'select long', TLBySTL(_get_array_post('STL')));
	}

	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */
	function ShowAllLeader()
	{
		echo form()->combo('swp_to_user_leader', 'select long', Teamleader(), null, array("change" => "new ShowDataToAgentByTL(this);"));
	}

	function ShowAllSeniorLeader()
	{
		echo form()->combo('swp_to_senior_leader', 'select long', Seniorleader(), null, array("change" => "new ShowDataToTLBySTL(this);"));
	}

	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */
	function ShowAllSupervisor()
	{
		echo form()->combo('swp_to_user_spv', 'select long', Supervisor(), null, array("change" => "new ShowAllLeader(13);"));
	}


	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */
	function ShowDataToAgentByTL()
	{
		echo form()->combo('swp_to_user_deskoll', 'select long', AgentByLeader(_get_array_post('TL')));
	}

	function ShowDataToTLBySTL()
	{
		echo form()->combo('swp_to_user_leader', 'select long', TLBySeniorLeader(_get_array_post('STL')));
	}

	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */
	function ShowAllAgent()
	{
		echo form()->combo('swp_to_user_deskoll', 'select long', Agent());
	}

	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	function SetSwapDataByAmount($out)
	{
		if (!is_object($out)) {
			return false;
		}

		return $this->{base_class_model($this)}->_setSwapDataByAmount($out);
	}

	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */

	function SetSwapDataByChecked($out)
	{
		if (!is_object($out)) {
			return false;
		}

		return $this->{base_class_model($this)}->_setSwapDataByChecked($out);
	}
	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
 * @ pack : Instance of class  
 *
 */
	function ActionSwapDataUser()
	{

		$out = new EUI_Object(_get_all_request());
		$conds = array('success' => 0);

		if ($out->get_value('swap_type') == 1) {
			$ok = $this->SetSwapDataByAmount($out);
			if ($ok) {
				$conds = array('success' => 1);
			}
		}

		if ($out->get_value('swap_type') == 2) {
			$ok = $this->SetSwapDataByChecked($out);
			if ($ok) {
				$conds = array('success' => 1);
			}
		}

		echo json_encode($conds);
	}



	// END CALSS 

}


// END FILE 
