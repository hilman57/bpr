<?php

/*
 * @ pack : ModAssignStatus -------------------
 * @ pack : under conttroller class 
 * @ pack : - 
 */
 
class ModAssignStatus extends EUI_Controller
{

/* 
 * @ pack : aksesor ModAssignStatus
 */
 
 public function ModAssignStatus()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
}

/* 
 * @ pack : superclass controller handle 
 */

 public function index()
{
	if($this->EUI_Session->_have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_default_acs();
		if( is_array($_EUI))
		{
			$this->load->view('mod_acs_view/view_acs_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ pack : _get_default
 */
 
 public function content()
{
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_resource_acs();    // load content data by pages 
		$_EUI['num']  = $this->{base_class_model($this)}->_get_page_number_acs(); // load content data by pages 
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('mod_acs_view/view_acs_list',$_EUI);
		}	
	}	
 }

 
/*
 * @ pack : _get_default
 */
 
 public function Add()
{
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		$this -> load -> view('mod_acs_view/view_acs_add',array (
		'FieldName' => $this->{base_class_model($this)}->_get_field_acs(),
		'HTML' => $this->{base_class_model($this)}->_get_component_acs(),
		'TitleLabel'=> $this->{base_class_model($this)}->_get_labels_acs()
		));
	}
 }
 
 

/*
 * @ pack : _get_default
 */
 
 public function Edit()
{
 $acsId = $this->URI->_get_post('Id');
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$UI = array(
		'IVR' => $this->{base_class_model($this)}->_get_detail_acs($acsId), 
		'FieldName' => $this->{base_class_model($this)}->_get_field_acs(),
		'HTML' => $this->{base_class_model($this)}->_get_component_acs(),
		'TitleLabel'=> $this->{base_class_model($this)}->_get_labels_acs()
	);
		
	 $this -> load -> view('mod_acs_view/view_acs_edit',$UI);
 }
 	
 }
 
/*
 * @ pack : _get_default
 */
 
 public function Save()
{
  $conds = array('success' => 0);
  if( $this->EUI_Session->_have_get_session('UserId') ) {
	if( $this->{base_class_model($this)}->_set_save_acs($this->URI->_get_all_request())) {
		$conds = array('success' => 1);
	}
  }
  
  echo json_encode($conds);
 }
 
/*
 * @ pack : _get_default
 */

 public function Update()
 {
	$conds = array('success' => 0);
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		if( $this->{base_class_model($this)}->_set_update_acs($this->URI->_get_all_request()))
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

 /*
 * @ pack : _get_default
 */
 
 public function Delete()
 {
	$conds = array('success' => 0);
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		if( $this->{base_class_model($this)}->_set_delete_acs($this->URI->_get_array_post('Id')))
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
 }


// END Class 

}

?>