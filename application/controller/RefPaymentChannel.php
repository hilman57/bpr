<?php 
/*
 * @ pack : payment channel 
 */
 
class RefPaymentChannel extends EUI_Controller
{

/* 
 * @ pack : superclass controller handle 
 */

 public function RefPaymentChannel()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
}

/* 
 * @ pack : superclass controller handle 
 */

 public function index()
{
	if($this->EUI_Session->_have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_default_pch();
		if( is_array($_EUI))
		{
			$this->load->view('ref_pch_view/view_pch_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ pack : _get_default
 */
 
 public function content()
{
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_resource_pch();    // load content data by pages 
		$_EUI['num']  = $this->{base_class_model($this)}->_get_page_number_pch(); // load content data by pages 
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('ref_pch_view/view_pch_list',$_EUI);
		}	
	}	
 }

 
/*
 * @ pack : _get_default
 */
 
 public function Add()
{
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		$this -> load -> view('ref_pch_view/view_pch_add',
			array
			(
				'FieldName' => $this->{base_class_model($this)}->_get_field_pch(),
				'HTML' => $this->{base_class_model($this)}->_get_component_pch(),
				'TitleLabel'=> $this->{base_class_model($this)}->_get_labels_pch()
			)
		);
	}
 }
 
 

/*
 * @ pack : _get_default
 */
 
 public function Edit()
{
 $pchId = $this->URI->_get_post('Id');
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$UI = array(
		'IVR' => $this->{base_class_model($this)}->_get_detail_pch($pchId), 
		'FieldName' => $this->{base_class_model($this)}->_get_field_pch(),
		'HTML' => $this->{base_class_model($this)}->_get_component_pch(),
		'TitleLabel'=> $this->{base_class_model($this)}->_get_labels_pch()
	);
		
	 $this -> load -> view('ref_pch_view/view_pch_edit',$UI);
 }
 	
 }
 
/*
 * @ pack : _get_default
 */
 
 public function Save()
{
  $conds = array('success' => 0);
  if( $this->EUI_Session->_have_get_session('UserId') ) {
	if( $this->{base_class_model($this)}->_set_save_pch($this->URI->_get_all_request())) {
		$conds = array('success' => 1);
	}
  }
  
  echo json_encode($conds);
 }
 
/*
 * @ pack : _get_default
 */

 public function Update()
 {
	$conds = array('success' => 0);
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		if( $this->{base_class_model($this)}->_set_update_pch($this->URI->_get_all_request()))
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

 /*
 * @ pack : _get_default
 */
 
 public function Delete()
 {
	$conds = array('success' => 0);
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		if( $this->{base_class_model($this)}->_set_delete_pch($this->URI->_get_array_post('Id')))
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
 }


// END Class 

}


?>