<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SMSInbox extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function SMSInbox() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));	
}

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
	if( $this->EUI_Session->_have_get_session('UserId') ) 
	{
		$_EUI  = array(
		 'page' => $this ->{base_class_model($this)} ->_get_default(),
		 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive()
		 );
		 
	   if(is_array($_EUI)) 
	   {
			$this -> load ->view('srv_sms_inbox_list/srv_sms_inbox_nav',$_EUI);
	   }	
	}
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('srv_sms_inbox_list/srv_sms_inbox_list',$_EUI);
	 }	
  }	
}


/*
 * @ pack : keep of the data 
 * -----------------------------------------------
 */
/*
 * @ pack : keep of the data for page lib 
 * -----------------------------------------------
 */
 
 public function SMSInboxCutomerId()
 {
	if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	 $this->start_page = 0;
	  $this->per_page   = 5;
	  $this->post_page  = (int)_get_post('page');
	 
	 // @ pack : set it 
	 
	  $this->result_sms = array();
	  $this->content_sms = $this->{base_class_model($this)}->_get_sms_inbox(_get_post('DebiturId'));
	  
	 // @ pack : record 

	  $this->total_records = count($this->content_sms);
		
	 // @ pack : set its  
	  if( $this->post_page) {
		$this->start_page = (($this->post_page-1)*$this->per_page);
	  } else {	
		$this->start_page = 0;
	  }

	 // @ pack : set result on array
	  if( (is_array($this->content_sms)) 
		AND ( $this->total_records > 0 ) )
	 {
		$this->result_sms = array_slice($this->content_sms, $this->start_page, $this->per_page);
	}	
	  
	 // @ pack : then set it to view 
	 
	  $smsinbox['content_pages']  = $this->result_sms;
	  $smsinbox['total_records']  = $this->total_records;
	  $smsinbox['total_pages']  = ceil($this->total_records/ $this->per_page);
	  $smsinbox['select_pages'] = $this->post_page;
	  $smsinbox['start_page']  = $this->start_page;
	  

	 // @ pack : sent to view 

	  $this->load->view("srv_sms_inbox_list/srv_sms_inbox_customer", $smsinbox);
  
  
  }
 }
 
 

}

// END OF CLASS 

?>