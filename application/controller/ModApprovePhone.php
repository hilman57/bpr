<?php

/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
 
class ModApprovePhone extends EUI_Controller 
{

/*
 * @ def 		: ModApprovePhone // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 	
function __construct()
{
	parent::__construct();
	$this->load->model(array( base_class_model($this),'M_PhoneType'));
	$this->load->helper(array('EUI_Object'));
}

/*
 * @ def 		: index
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 	
function index()
{
	if( $this ->EUI_Session ->_have_get_session('UserId') && class_exists('M_ModApprovePhone') )
	{
		$UI['DROP_DESKOLL']	= $this->M_SrcCustomerList->_getDeskollByLogin();
		$UI['DROP_CAMPAINGN'] = $this->M_SrcCustomerList->_getCampaignByActive();
		$UI['DROP_PHONETYPE'] = $this->M_RefAddPhoneType->_get_select_adp();
		$UI['page'] = $this -> {base_class_model($this)} -> _get_default();
		if( is_array($UI))
		{
			$this -> load ->view('mod_approval_phone/view_approve_phone_nav',$UI);
		}	
	}
}

/*
 * @ def 		: Content
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 	
public function Content() 
{
  if( $this -> EUI_Session -> _have_get_session('UserId') )
 {
	$_EUI['page'] = $this->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('mod_approval_phone/view_approve_phone_list',$_EUI);	
	}	
 }
}

/*
 * @ def 		: Content
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function ApproveItem()
{
	$ApproveItemId = null; $_conds = array("success"=>0);
	$ApproveItemId = $this ->URI->_get_array_post('ApproveItemId');
	
	if( is_array($ApproveItemId) )
	{	
		if( $this ->{base_class_model($this)}->_setApproveItem($ApproveItemId))
		{
			$_conds = array("success"=>1);
		}	
	}
	
	echo json_encode($_conds);
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function M_getSerilaizeCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }
 
 
/*
 * @ def 		: Content
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Detail()
{
 $ApproveItemId = $this ->URI->_get_post('ApproveId');
 if($ApproveItemId )
 {
	$CustomerId = $this ->{base_class_model($this)}->_getCustomerId($ApproveItemId);
	$UI = array(
		'Customers' => $this->M_SrcCustomerList->_getDetailCustomer($CustomerId),
		'Phones' => $this->M_SrcCustomerList->_getPhoneCustomer($CustomerId),
		'AddPhone' => $this->M_SrcCustomerList -> _getApprovalPhoneItems($CustomerId),
		'CallCategoryId' => $this->M_SetResultCategory->_getOutboundCategory(),
		'Combo' => $this->M_getSerilaizeCombo(),
		'ItemApprove' => $this->{base_class_model($this)}->_getAllApprovalItems(),
		'PostData' => $this->URI->_get_all_request()
	);
	
	$this->load->view('mod_approval_phone/approve_content_phone',$UI);
 }
 
}

// ViewAddPhone 

 public function ViewAddPhone()
{
 
 $_DATA_PHONE['Customer'] =  $this->URI->_get_all_request();
 $_DATA_PHONE['PhoneType'] = $this->M_PhoneType->_getPhoneTypeList();
 $_DATA_PHONE['Relationship'] = $this->M_PhoneType->_getRelationship();
  if( is_array($_DATA_PHONE) ) 
 {
	$this->load->view('mod_approval_phone/approve_view_addphone', $_DATA_PHONE );
  }
}

// ----------------------------------------------------------------------------
/*
 * @ packaage 		ViewMultipleAdd
 * 
 */
 
  public function ViewMultipleAdd()
{
	$this->load->view('mod_approval_phone/approve_view_multiple', array());
 }
 
// ----------------------------------------------------------------------------
/*
 * @ packaage 		PhoneNumber
 * 
 */

public function PhoneNumber()
{
	$_conds = array('phoneNumber' => '' );
	$fieldname = $this -> URI-> _get_post('FieldName');
	$this -> db -> select($fieldname); 
	$this -> db -> from('t_gn_debitur');
	$this -> db -> where('CustomerId',$this -> URI-> _get_post('CustomerId'));
	
	if( $rows =  $this -> db -> get() -> result_first_assoc() ){
		$_conds = array('phoneNumber' => $rows[$fieldname]);
	}

	echo json_encode($_conds);
}

// ----------------------------------------------------------------------------
/*
 * @ packaage 		SavePhone
 * 
 */
 public function SavePhone() 
 {
	$_arr = array('success' => 0 );
	if( $this -> EUI_Session->_have_get_session('UserId') )
	{
		$_E = $this -> URI->_get_all_request();
		$_conds = $this -> {base_class_model($this)} -> _SaveSubmitPhone($_E); 
		if( $_conds ) 
		{
			$_arr = array( 'success'=>1);
		}
	}
		
	echo json_encode($_arr);	
	
		
 }
 
// ----------------------------------------------------------------------------
/*
 * @ packaage SaveMultiplePhone
 * 
 */
 
 public function SaveMultiplePhone() 
{
	$cond = array("success" => 0 );
   if( !_have_get_session('UserId') ) 
  {
	echo json_encode(array('success' => 0 ));
	return false;	
  }	
  
  $var = new EUI_Object(_get_all_request() );
  $out =&get_class_instance(base_class_model($this));
  
   if( $out-> _SaveMultiplePhone($var) )
  {
	 echo json_encode(array('success' => 1 ));
	 return false;
  }  
  
  echo json_encode(array('success' => 0 ));
	  
  
}

// ======================= END CLASS =================================

}

?>