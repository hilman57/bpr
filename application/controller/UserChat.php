<?php
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
class UserChat extends EUI_Controller  {

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function UserChat()
{
	session_start();
	parent::__construct();	
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 */
 
function index()
{
	session_start();
	
	if ($_REQUEST['action'] == "chatheartbeat") { self::chatHeartbeat(); } 
	if ($_REQUEST['action'] == "sendchat") { self::sendChat(); } 
	if ($_REQUEST['action'] == "closechat") { self::closeChat(); } 
	if ($_REQUEST['action'] == "startchatsession") { self::startChatSession(); } 

	if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"])) {
		$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"] = array();	
	}

	if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"])) {
		$_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"] = array();	
	}
}

// chatHeartbeat

function chatHeartbeat() 
{
session_start();

	$this->db->select("a.*, b.init_name as AliasTo, c.init_name as AliasFrom ");
	$this->db->from("t_tx_agent_chat a");
	$this->db->join("t_tx_agent b ","a.to=b.id","LEFT");
	$this->db->join("t_tx_agent c ","a.from=c.id","LEFT");
	$this->db->where("a.to",$this ->EUI_Session->_get_session('Username'));
	$this->db->where("a.recd",0);
	$this->db->order_by("a.id","ASC");
	
	$items = '';

	$chatBoxes = array();

	foreach( $this->db->get()->result_assoc() as $chat ) 
	{
		if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"][$chat['from']]) 
			AND isset($_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chat['from']])) 
		{
			$items = $_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chat['from']];
		}

		$chat['message'] = self::sanitize($chat['message']);

		$items .= <<<EOD
					   {
			"s": "0",
			"f": "{$chat['from']}",
			"g": "{$this->SukuKataPertama($chat['AliasFrom'])}",
			"m": "{$chat['message']}"
	   },
EOD;

	if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chat['from']])) {
		$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chat['from']] = '';
	}

	$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chat['from']] .= <<<EOD
						   {
			"s": "0",
			"f": "{$chat['from']}",
			"g": "{$this->SukuKataPertama($chat['AliasFrom'])}",
			"m": "{$chat['message']}"
	   },
EOD;
		
		unset($_SESSION["{$this->EUI_Session->sess_prefix}tsChatBoxes"][$chat['from']]);
		$_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"][$chat['from']] = array('time' => $chat['sent'], 'aliasfrom' => self::SukuKataPertama($chat['AliasFrom']));
	}

	if (!empty($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"])) 
	{
	foreach ($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"] as $chatbox => $rows) {
	
		if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}tsChatBoxes"][$chatbox])) {
			$now = time()-strtotime($rows['time']);
			$time = date('g:iA M dS', strtotime($rows['time']));

			$message = "Sent at $time";
			if ($now > 180) {
				$items .= <<<EOD
{
	"s": "2",
	"f": "{$chatbox}",
	"g": "{$this->SukuKataPertama($rows['aliasfrom'])}",
	"m": "{$message}"
},
EOD;

	if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chatbox])) {
		$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chatbox] = '';
	}

	$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chatbox] .= <<<EOD
		{
"s": "2",
"f": "{$chatbox}",
"g": "{$this->SukuKataPertama($rows['aliasfrom'])}",
"m": "{$message}"
},
EOD;
			$_SESSION["{$this->EUI_Session->sess_prefix}tsChatBoxes"][$chatbox] = 1;
		}
		}
	}
}

// update flags if isread 

	$this->db->set('recd',1);
	$this->db->where('to',trim($this ->EUI_Session->_get_session('Username')));
	$this->db->where('recd',0);
	$this->db->update('t_tx_agent_chat');
	
	if ($items != '') 
	{
		$items = substr($items, 0, -1);
	}
header('Content-type: application/json');
?>
{
		"items": [
			<?php echo $items;?>
        ]
}
<?php
			exit(0);
}

// chatBoxSession

function chatBoxSession($chatbox) 
{
	$items = '';
	if (isset($_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chatbox])) {
		$items = $_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$chatbox];
	}

	return $items;	
}

// startChatSession

function startChatSession() 
{
	session_start();
	
	$items = '';
	if (!empty($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"])) 
	{
		foreach ($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"] as $chatbox => $void) 
		{
			$items.= self::chatBoxSession($chatbox);
		}
	}


	if ($items != '') {
		$items = substr($items, 0, -1);
	}
header('Content-type: application/json');
?>
{
	"username": "<?php __($this->EUI_Session->_get_session('Username'));?>",
	"codeuser" : "<?php __(self::SukuKataPertama($this->EUI_Session->_get_session('KodeUser')));?>",
	"items": [<?php __($items);?>]
}
<?php
	exit(0);
}

// SukuKataPertama

function SukuKataPertama( $Words = null )
{
 $_Words = null;
  if( !is_null($Words) ) 
  {
	if( $_array_list = explode(" ", $Words) ) {
		$_Words = strtoupper($_array_list[0]);
	 }
  }
	return $_Words;
}
//sendChat

function sendChat()
{

 $aliasname = self::SukuKataPertama($this->URI->_get_post('aliasname'));
 $message = $this->URI->_get_post('message');
 $from = $this->EUI_Session->_get_session('Username');
 $to = $this->URI->_get_post('to');
 
 $_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"][$to] = array( 'time' => date('Y-m-d H:i:s', time()), 'aliasto' => $aliasname );
 
 $messagesan = self::sanitize($message);
 if (!isset($_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$to])) {
	$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$to] = '';
 }

	$_SESSION["{$this->EUI_Session->sess_prefix}chatHistory"][$to] .= <<<EOD
					   {
			"s": "1",
			"f": "{$to}",
			"g": "{$aliasname}",
			"m": "{$messagesan}"
	   },
EOD;


	unset($_SESSION["{$this->EUI_Session->sess_prefix}tsChatBoxes"][$this->URI->_get_post('to')]);
	
	// send of chat 
	
	$this->db->set('from',$from);
	$this->db->set('to',$to);
	$this->db->set('message',$message);
	$this->db->set('sent',date('Y-m-d H:i:s'));
	$this->db->insert('t_tx_agent_chat');
	
	__("1");
	
	// print_r($_SESSION);
	exit(0);
}

//sendChat

function closeChat() 
{
	unset($_SESSION["{$this->EUI_Session->sess_prefix}openChatBoxes"][$this->URI->_get_post('chatbox')]);
	echo "1";
	exit(0);
}

// sanitize

function sanitize($text) 
{
	$text = htmlspecialchars($text, ENT_QUOTES);
	$text = str_replace("\n\r","\n",$text);
	$text = str_replace("\r\n","\n",$text);
	$text = str_replace("\n","<br>",$text);
	return $text;
}

}

?> 