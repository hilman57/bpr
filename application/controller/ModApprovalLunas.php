<?php
/*
 * @ pack : approval discount 
 */
 
class ModApprovalLunas extends EUI_Controller
{

/*
 * @ pack : approval discount 
 */
 
public function ModApprovalLunas()
{
  parent::__construct();
  $this->load->model(array(base_class_model($this)));
}

/*
 * @ pack : approval discount 
 */

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
	 'GenderId' => $this->{base_class_model($this)}->_getGenderId(),
	 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
	 'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus(),
	 'LastCallStatus' => $this->{base_class_model($this)}->_getLastCallStatus(),
	 'ApproveStatus' => $this->{base_class_model($this)}->_getRequestApproveStatus() 
	);
	
// @ pack : send to view_reqdiscount_list	 

   if(is_array($_EUI)) 
   {
	 $this -> load ->view('mod_view_reqlunas/view_reqlunas_nav',$_EUI);
   }	
 }
}

/*
 * @ pack : cotents 
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('mod_view_reqlunas/view_reqlunas_list',$_EUI);
	 }	
  }	
}

	 
/*
 * @ pack : Approve
 */
 
 public function Approve()
{

  $conds= array('success' => 0 );
  if(_have_get_session('UserId')) 
 {
	if( $_count = $this->{base_class_model($this)}->_setApprovalLunas() )
	{
		$conds= array('success' => 1); 
	}
 }
  echo json_encode($conds);
}

/*
 * @ pack : Approve
 */
 
 public function Reject()
{

  $conds= array('success' => 0 );
  if(_have_get_session('UserId')) 
 {
	if( $_count = $this->{base_class_model($this)}->_setRejectDiscount() )
	{
		$conds= array('success' => 1); 
	}
 }
  echo json_encode($conds);
}



/*
 * @ pack : Approve
 */
 
 public function Printed()
{
 
 $conds= array('success' => 0 , 'url' => '' );
 if(_have_get_session('UserId')) 
 {
    $DiscountId =_get_post('DiscountId');
	if( $result = $this->{base_class_model($this)}->_doPDF($DiscountId) )
	{	
		$this->db->set('printed_date', date('Y-m-d H:i:s'));
		$this->db->set('aggrement', '104');
		$this->db->where('id',$DiscountId, FALSE);
		$this->db->where_in('aggrement', array('101', '102','104','105'));
		$this->db->update('t_gn_discount_trx');
		
	// then show PDF 
		
		$ROOT_BASE_SERVER = str_replace(str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']),'', $result);
		$HTTP_BASE_SERVER = base_url() . $ROOT_BASE_SERVER;
		
		if( $HTTP_BASE_SERVER )
		{
			$conds = array('success' => 1,'url' => $HTTP_BASE_SERVER); 
		}	
	}
 }
 
  echo json_encode($conds);
} 

  
// END CLASS   
}

?>