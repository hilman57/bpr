<?php

class Callhistory extends EUI_Controller  
{

    public function index()
    {
        $this->load->view('callhistory/index');
    }
    public function excel()
    {

        //generate to excel

            // Excel() -> HTML_Excel();
            //cek mode
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=Callhistory" . $start_date . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            // $start_date = $_GET['start_date'];
            // $end_date = $_GET['end_date'];
            //echo "<h1>REPORT CALLHISTORY".$this->tgl_indo($_GET['start_date'])."</h1><br>";
            ?>

            <?php
            $start_date = date("Y-m-d");
            // $start_date = date("Y-m-d");
            // $end_date 	= date("Y-m-d"); 

            $no_agent = '1';
            $data_agent = $this->db->query("            
            SELECT
            # a.CallReasonId AS DC,
                c.id AS DC,
                ag.full_name as TL,
                a.AccountNo AS 'ACCOUNTNO',

                a.CallAccountStatus as ACCOUNT_STATUS,
                st.CallReasonDesc as ACCOUNT_STATUS,
                a.CallHistoryCallDate 'TGLUPDATE'

            FROM t_gn_callhistory a
            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
            LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
            LEFT join t_tx_agent ag on a.TeamLeaderId = ag.UserId WHERE
            # a.CallHistoryCallDate >= '2019-08-10   00:00:00'

            a.CallHistoryCallDate >= '" . $start_date . " 00:00:00'
            AND a.CallHistoryCallDate <= '" . $start_date . " 23:00:00'

            # AND st.CallReasonDesc = 'NEW';

             ")->result();
            // var_dump($data_agent);


            //foreach ($data_agent as $row_agent) {
            //var_dump($row_agent);

            //}
            // 			$data = array(
            //     array('Symbol','Company','Price'),
            //     array('GOOG','Google Inc.','800'),
            //     array('AAPL','Apple Inc.','500')
            // );
            $file = fopen('php://output', 'w');
            fputcsv($file, array('REPORT CALLHISTORY' . $this->tgl_indo($_GET['start_date'])));
            fputcsv($file, array('DC', 'TL', 'ACCOUNT NO', 'ACCOUNT STATUS', 'TGL UPDATE'));
            foreach ($data_agent as $row_agent) {
                //echo "<pre>";
                //print_r($row_agent);
                $data = array(
                    $row_agent->DC,
                    $row_agent->TL,
                    $row_agent->ACCOUNTNO,
                    $row_agent->ACCOUNT_STATUS, 
                    $row_agent->TGLUPDATE
                );
                //print_r($data);

                //echo "</pre>";

                fputcsv($file, $data);
            }
    }

    public function tgl_indo($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
    }

    // edit hilman
    public function detail()
    {

        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];

        if ($_GET['button'] == 'Action') {
            $bulan = $_GET['bulan'];


            //    echo"<br>";
            $data['data_agent'] = $this->db->query("
            SELECT
              c.id AS DC,
              ag.full_name as TL,
              a.AccountNo AS 'ACCOUNTNO',
          
              a.CallAccountStatus as ACCOUNT_STATUS,
              st.CallReasonDesc as ACCOUNT_STATUS,
              a.CallHistoryCallDate 'TGLUPDATE'
          
            FROM t_gn_callhistory a
            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
            LEFT join t_tx_agent ag on a.TeamLeaderId = ag.UserId WHERE# a.CallHistoryCallDate >= '2019-08-10   00:00:00'
          
            a.CallHistoryCallDate >= '" . $start_date . " 00:00:00'
            AND a.CallHistoryCallDate <= '" . $start_date . " 23:00:00'
                     ")->result();
            $this->load->view('callhistory/detail', $data);
        }  else if($_GET['button'] == 'Excel') {
            $this->load->view('callhistory/index2');
            
        }
    }

    function nama_tl($id)
    {
        $nama_tl = $this->db->query("SELECT full_name, code_user FROM t_tx_agent WHERE UserId='$id'")->result();

        return $nama_tl[0]->code_user . " - " . $nama_tl[0]->full_name;
    }

    public function crontab()
    {

        //$tglLike = '2019-06';
        $tglLike = date('Y-m');
        $this->db->query("truncate t_gn_ranking");
        $this->db->query("truncate t_gn_siklus_data");
        $this->db->query("truncate t_gn_siklus_report");
        $this->db->query("truncate t_gn_siklus_user");
        $data = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
    FROM t_gn_payment
    INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
    WHERE t_gn_payment.pay_date LIKE '%$tglLike%'
    GROUP BY tl_id
    ORDER BY amount_tl DESC")->result();

        foreach ($data as $row) {

            $nama_tl = $this->nama_tl($row->tl_id);
            $no = 0;
            $data_agent = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.id as id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl, t_gn_payment.pay_date
        FROM t_gn_payment
        INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
        WHERE t_tx_agent.tl_id='$row->tl_id' AND t_gn_payment.pay_date LIKE '%$tglLike%'
        GROUP BY t_tx_agent.id
        ORDER BY amount_tl DESC")->result();

            foreach ($data_agent as $row_agent) {
                $no++;
                $date = date('Y-m-d H:i:s');
                $pay_date = date('Ym');
                $this->db->query("INSERT into t_gn_ranking VALUES('','$no','$row_agent->code_user', '$nama_tl', '$row_agent->amount_tl','$pay_date','$date')");
                // $push['no'] = $no;
                // echo array_push($row_agent, $push);
                // echo json_encode($row_agent);

            }
        }
        echo "sukses";
    }
  
    public function csv()
    {

        //generate to excel

            // Excel() -> HTML_Excel();
            //cek mode
            //$start_date = date("2019-08-08");
             $start_date = date("Y-m-d");
            // $end_date 	= date("Y-m-d"); 

            $no_agent = '1';
            $data_agent = $this->db->query("            
            SELECT
            # a.CallReasonId AS DC,
                c.id AS DC,
                ag.full_name as TL,
                a.AccountNo AS 'ACCOUNTNO',

                a.CallAccountStatus as ACCOUNT_STATUS,
                st.CallReasonDesc as ACCOUNT_STATUS,
                a.CallHistoryCallDate 'TGLUPDATE'

            FROM t_gn_callhistory a
            LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
            LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
            LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
            LEFT join t_tx_agent ag on a.TeamLeaderId = ag.UserId WHERE
            # a.CallHistoryCallDate >= '2019-08-10   00:00:00'

            a.CallHistoryCallDate >= '" . $start_date . " 00:00:00'
            AND a.CallHistoryCallDate <= '" . $start_date . " 23:00:00'

            # AND st.CallReasonDesc = 'NEW';

             ")->result();
    
             // var_dump($data_agent);
            header("Content-type: text/csv");
            $fileName = "Callhistory-".$start_date. "-".date('His').".csv";
            header('Content-Disposition: attachment; filename='. $fileName);
            header("Pragma: no-cache");
            header("Expires: 0");
             //tentukan foldernya 
            //$path   = '/var/www/html/development/hsbc180619/';
			
			$path   = '/opt/enigma/webapps/hsbc/Export/';
            
			//cek apakah sudah ada direktorinya 
            //jika tdk ada bkin baru
            if(!is_dir($path)) {
                mkdir($path, 0777, TRUE);
            }
            

             $file = fopen($path.$fileName, "w");
            //  $file = fopen('php://output', 'w');
            fputcsv($file, array('REPORT CALLHISTORY' . $this->tgl_indo($_GET['start_date'])));
            fputcsv($file, array('DC', 'TL', 'ACCOUNT NO', 'ACCOUNT STATUS', 'TGL UPDATE'));
            foreach ($data_agent as $row_agent) {
                //echo "<pre>";
                //print_r($row_agent);
                $data = array(
                    $row_agent->DC,
                    $row_agent->TL,
                    $row_agent->ACCOUNTNO."\r",
                    $row_agent->ACCOUNT_STATUS, 
                    $row_agent->TGLUPDATE
                );
                //print_r($data);

                //echo "</pre>";

                fputcsv($file, $data);
                
            }
    }



    /*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
}

// END OF CLASS 

?>