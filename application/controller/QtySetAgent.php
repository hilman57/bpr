<?php
/*
 * @ def : Quality Set Agent 
 * ------------------------------------------
 * @ params : Unit test notes
 * @ params : 
 * @ params : 
 * ------------------------------------------
 */
 
class QtySetAgent extends EUI_Controller
{

/*
 * @ def : contants folder view 
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
private static $view_layout = null;
 
/*
 * @ def : constructor off class 
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
 function QtySetAgent() 
 {
	parent::__construct();
	self::$view_layout = array('qty_view_setagent');
	$this -> load-> model(array(base_class_model($this),'M_SysUser'));
 }

 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */

 public function getViewLayout() {
	return ( is_array(self::$view_layout) ? self::$view_layout[0] : null);
 }
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
public function index()
{
	$UI = array(
		'view_layout' 	=> self::getViewLayout(),
		'quality_staff' => $this -> M_SysUser->_get_quality_staff()
	);
	
	/* cannot acces forbidden && must be login **/
	
	if( !is_null($UI) AND $this -> EUI_Session->_have_get_session('UserId') )
	{
		$this -> load-> view( self::getViewLayout()."/view_setagent_nav", $UI );
	}	
	
}
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
  
  
public function getAgentReadyState()
{
	$UI = array ( 
		'view_page' 	=> $this ->{base_class_model($this)}->_getPageAgent(),
		'view_record' 	=> $this ->{base_class_model($this)}->_getRecordsAgent(),
		'view_agents' 	=> $this ->{base_class_model($this)}->_getAgentState(),
		'select_page'	=> $this -> URI->_get_post('page')
	);
	
	if( !is_null($UI) AND $this -> EUI_Session->_have_get_session('UserId') ) 
	{
		$this -> load-> view( self::getViewLayout()."/view_agent_layout", $UI );
	}	
}
 
/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
  
public function QualityAgentReady()
{
   // default page 
   
	$view_layout 	= array();
	$quality_scores = $this -> {base_class_model($this)}->_getQualitStaffScore();
	$quality_staff  = $this -> M_SysUser->_get_quality_staff();
	
	// seaarch data quality scores 
	
	foreach($quality_staff as $UserId => $rows )
	{
		if( in_array( $UserId, $quality_scores)){
			$view_layout[$UserId] = $rows;
		}		
	}
	
	// sent to view 
	
	$UI = array ( 
		'view_quality_agent'  => $this->{base_class_model($this)}->_getQualityReady(), 
		'view_quality_record' => $this->{base_class_model($this)}->_getRecordsQuality(),
		'view_quality_pages'  => $this->{base_class_model($this)}->_getPageQualityReady(),	
		'view_quality_page'	  => $this->URI->_get_post('page'),
		'view_quality_staff'  => $view_layout 
	);
	
	// show detial data 
	
	if( !is_null($UI) AND $this -> EUI_Session->_have_get_session('UserId'))
	{
		$this -> load-> view( self::getViewLayout()."/view_quality_ready",$UI);
	}	
} 


/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
  
  
public function AddAvailableAgent()
{
	$conds = array('success' => 0);
	
	if( $this -> URI->_get_have_post('UserId') 
		AND $this -> EUI_Session->_have_get_session('UserId') ) 
	{
		$result = $this -> {base_class_model($this)}->_setAddAvailableAgent( $this -> URI->_get_array_post('UserId')); 
		if($result)
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
  
public function UpdateQualityAgent()
{
	$conds = array('success' => 0);
	
	if( $this -> URI->_get_have_post('QualityAgentId') 
		AND $this -> EUI_Session->_have_get_session('UserId') ) 
	{
		$result = $this -> {base_class_model($this)}->_setUpdateQualityAgent( 
			$this -> URI->_get_array_post('QualityAgentId'),
			$this -> URI->_get_post('QulaityStaffId')
		); 
		if($result)
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
  
public function RemoveQualityAgent()
{
	$conds = array('success' => 0);
	
	if( $this -> URI->_get_have_post('QualityAgentId') 
		AND $this -> EUI_Session->_have_get_session('UserId') ) 
	{
		$result = $this -> {base_class_model($this)}->_setRemoveQualityAgent($this -> URI->_get_array_post('QualityAgentId')); 
		if($result)
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
  
public function EmptyQualityAgent()
{
	$conds = array('success' => 0);
	
	if( $this -> URI->_get_have_post('QualityAgentId') 
		AND $this -> EUI_Session->_have_get_session('UserId') ) 
	{
		$result = $this -> {base_class_model($this)}->_setEmptyQualityAgent($this -> URI->_get_array_post('QualityAgentId')); 
		if($result)
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

// 


}

?>