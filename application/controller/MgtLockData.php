<?php

/*
 * @ pack : manage lock data its
 */
 
class MgtLockData extends EUI_Controller
{
	
/*
 * @ pack : function data # ------------
 */ 

 public function MgtLockData()
{
	parent::__construct();
	$this->load->model(array( base_class_model($this)));
 }
 
/*
 * @ pack : index default load 
 */
 
 public function index() 
{
 
 if( _have_get_session('UserId') )
 {
	$dropdown['Dropdown'] = $this->{base_class_model($this)}->_get_dropdown();
	$dropdown['template'] = $this->M_MgtBucket->_get_template(4);
	$dropdown['product'] = $this->M_SetCampaign->_get_campaign_name();
	if( is_array( $dropdown ) )
	{
		$this->load->view("mgt_lock_data/view_lock_data", $dropdown );
	}
	/*Sementara fungsi di tutup pertanggal 18-05-2016*/
	// echo "Sorry Under Construction :'(";	
 }
	
}

/*
 * @ pack : index default load 
 */
 
 public function getUserLeader()
{
  
 $PrivilegeId  = (int)_get_post('PrivilegeId');
 $input = (string)_get_post('input');
 $UserLeader = $this->M_SysUser->_getUserLevelGroup($PrivilegeId);
 
// @ pack : if content is not valid # -------------------
 
 if( count($UserLeader) ==0 )
 {
   if( _get_session('HandlingType') == USER_LEADER ){
		$_inquery = $this->M_SysUser->getUserQuery(array(
			'handling_type' => $PrivilegeId,
			'UserId' => _get_session('UserId')
		));	
  }

// @ pack : if content is not valid # -------------------
	
	if(is_array($_inquery)) 
		foreach( $_inquery as $key => $rows )
	{
		 $UserLeader[$rows['UserId']] = $rows['full_name'];
	}
 }
 
 
 // @ pack : if content is not valid # -------------------
 
 if( is_array($UserLeader) ){
	echo form()->{$input}('lock_user_level2','select long', $UserLeader,
	NULL, array("change" => "Ext.DOM.UserLevelAgent({
			PrivilegeId : '4,6',
			LeaderId : this.value,
			input : 'listCombo'
		});"), array("dwidth" =>"220px", "event"=> "SetAllEventLeader"));
 } else {
	echo form()->combo('lock_user_level2','select auto', array() );
 }
 
}

/*
 * @ pack : index default load 
 */

 public function getUserAgent()
{
  
 $conds = array();
 $input = (string)_get_post('input');
 
 $PrivilegeId  = (array)_get_array_post('PrivilegeId');
 $LeaderId  = (array)_get_array_post('LeaderId');
 
 
 
// @pack : get user inquery 
 
 $UserDeskoll = $this->M_SysUser->getUserQuery(array(
	'tl_id' => $LeaderId,
	'handling_type' => $PrivilegeId )
 );
 
// @pack : get user inquery 
  
 if(is_array($UserDeskoll))
  foreach( $UserDeskoll as $k => $rows  )
 {
	$conds[$rows['UserId']] = "{$rows['id']} - {$rows['full_name']}";
  }
  
  if( count($conds)>0 ){
	echo form()->{$input}('lock_user_level3','select auto', $conds,null, array("click" => "Ext.Cmp('lock_user_level3').oneChecked(this);Ext.DOM.SetSelectedStatus(this);"));
	
  } else {
	echo form()->combo('lock_user_leve3','select long', $conds,NULL);
  }
  
}

// getUserStatusLock

public function getUserStatusLock()
{
	$select_status = array();
	$Dropdown = $this->{base_class_model($this)}->_get_dropdown();
	$status_lock = $this->{base_class_model($this)}->_get_select_active_value(
		array(
			'ModName' => _get_post('module'),
			'ModUser' => _get_post('UserId'),
			'ModStatus' => 'ACTIVE' )
	);
	
	if( is_array($status_lock) 
		AND $status_lock['ModValueParameter']!='' )
	{
		$result = explode(',', $status_lock['ModValueParameter']);
		foreach( $result as $keys => $value ) {
			if(!empty($value) ) {
				$select_status[$value] = $value;
			}	
		}
	}
	
	echo form()->listCombo('lock_account_status','select auto',$Dropdown['LOCK_DROPDOWN_STATUS'], array_keys($select_status)
		,null, array( "dwidth" => "200px", "height" => "100%"));
}

/*
 * @ pack : SetLoackDataByStatus #-------------------------------
 */ 
  
 public function SetLockDataByStatus()
{
  
  $cond  = array('success'=> 0);
  if( $this->EUI_Session->_have_get_session('UserId') )
  {
	 if( _get_post('LockModulType')=='status' )
	 {
		$call_back_client = $this->{base_class_model($this)}->_SetLockStatus();
		if( $call_back_client ) {
			$cond  = array('success'=> 1);
		}
		
	 }
  }
  
  echo json_encode($cond);
}

/*
 * @ pack : SetUnlock #-------------------------------
 */
 //http://192.168.1.52/hsbc/index.php/MgtLockData/SaveUnLockAccount/
 
 public function SaveUnLockAccount()
{

 $conds = array('success'=> 0 );
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	if( $count = $this->{base_class_model($this)}->_SaveUnLockAccount() )
	{
		$conds = array('success'=> $count );
	}	
 }
	echo json_encode($conds);
}


/*
 * @ pack  : SelectedDataAmount
 */  
 
public function SelectedDataAmount()
{
 
 $conds = array('success'=> 0,  'data'=> '' );
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	if( $data = $this->{base_class_model($this)}->_get_select_active_value(array(
		'ModUser' => _get_post('UserId'),
		'ModName' => 'amount',
		'ModStatus' => 'ACTIVE' )))
	{
		$conds = array('success'=> 1, 'data' => $data );
	}	
 }

 echo json_encode($conds);
} 
 
/*
 * @ pack : SetLockAmount
 */
 
public function SetLockAmount()
{
 $conds = array('success'=> 0 );
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	if( $count = $this->{base_class_model($this)}->_SetLockAmount() )
	{
		$conds = array('success'=> $count );
	}	
 }
	echo json_encode($conds);
} 

/*
 * @ pack : SetLockAmount
 */
 
 public function AccountLockPage()
{
  
  $this->start_page = 0;
  $this->per_page = 10;
  $this->post_page = (INT)_get_post('page');
 
// @ pack : set it 
 
  $this->result_lock = array();
  $this->content_lock = $this->{base_class_model($this)}->_getAccountLockPage(_get_post('DebiturId'));
  
 // @ pack : record 

  $this->total_records = count($this->content_lock);
	
 // @ pack : set its  
  if( $this->post_page) {
	$this->start_page = (($this->post_page-1)*$this->per_page);
  } else {	
	$this->start_page = 0;
  }

 // @ pack : set result on array
  if( (is_array($this->content_lock)) 
	AND ( $this->total_records > 0 ) )
 {
	$this->result_lock = array_slice($this->content_lock, $this->start_page, $this->per_page);
}	
  
 // @ pack : then set it to view 
 
  $lock['content_pages']  = $this->result_lock;
  $lock['total_records']  = $this->total_records;
  $lock['total_pages']  = ceil($this->total_records/ $this->per_page);
  $lock['select_pages'] = $this->post_page;
  $lock['start_page']  = $this->start_page;
  

 // @ pack : sent to view 

  $this->load->view("mgt_lock_data/view_lock_page", $lock);
}


// END CLASS  
}


?>