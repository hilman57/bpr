<?php
/*
 * @ modul : report call tracking controller 
 * @ object : superclass 
 * ----------------------------------------------------------------
  
 * @ param : - 
 * @ param : - 
 */
 
class CallTrackingReport extends EUI_Controller 
{

var $FilterGroupBy = null;
var $Mode = null;
var $R_Model = null;

/* @ param : aksesor of superclass **/

public function CallTrackingReport()
{
	parent::__construct();
	$this -> load -> model(array(base_class_model($this),'M_Report'));
    if( is_null($this -> R_Model) 
		AND !is_array($this -> R_Model) )
	{
		//$this->R_Model['filter_by_atm_group_campaign'] = array('class' => 'R_CallTrackAtmGroupByCampaign', 'title' => 'ATM Group By Campaign');
		$this->R_Model['filter_by_resource'] = array('class' => 'R_CallTrackGroupRecsource','title' => 'Group By Recsource');
		$this->R_Model['filter_by_spv_group_campaign'] = array('class' => 'R_CallTrackSpvGroupByCampaign','title' => 'SPV Group By Campaign');
		$this->R_Model['filter_by_agent_group_campaign']= array('class' => 'R_CallTrackTsrGroupByCampaign','title' => 'Caller Group By Campaign');
		$this->R_Model['filter_by_campaign'] 	= array('class' => 'R_CallTrackGroupCampaign', 'title' => 'Group By Campaign');
		//$this->R_Model['filter_by_atm'] = array('class' => 'R_CallTrackGroupAtm','title' => 'Group By ATM');
		$this->R_Model['filter_by_spv'] = array('class' => 'R_CallTrackGroupSpv','title' => 'Group By SPV'); 
		$this->R_Model['filter_by_agent'] = array('class' => 'R_CallTrackGroupTsr','title' => 'Group By Caller'); 
		$this->R_Model['filter_campaign_group_agent'] = array('class' => 'R_CallTrackCampaignGroupTsr','title' => 'Campaign Group By Caller '); 
		
	}	
	
	if( $this->URI->_get_post('Filter') 
		AND !is_null($this->URI->_get_post('Filter')) )
	{
		$this->FilterGroupBy = $this->URI->_get_post('Filter');
	}
	
	if( $this->URI->_get_post('mode') 
		AND !is_null($this->URI->_get_post('mode')) )
	{
		$this->Mode = $this -> URI->_get_post('mode');
	}
}

 
/* index of the page report modul **/
 
public function index()
{
	/* echo "<pre>";
	session_start();
	print_r($_SESSION);
	
	echo "</pre>"; */
	
	$UI = array(
		'agent_call_center' => $this -> {base_class_model($this)} -> getAgent(),
		'mode_call_center' => $this -> {base_class_model($this)} -> Mode(),
		'mode_call_type'  => $this -> M_Combo->_getCallDirection(),
		'group_filter_by'  => $this -> {base_class_model($this)}->GroupFilter()
	);  
	$this -> load -> view("rpt_call_tracking/report_call_track_nav", $UI);
 }


/* @ param : GroupFilterBy **/

public function GroupFilterBy() 
{

if( $this -> URI -> _get_have_post('method') )
{
	if( $method = $this -> URI -> _get_post('method') )
	{
		$object = $this -> URI -> _get_post('object');
		$properties = $this -> URI -> _get_post('properties');
		$styles = $this -> URI -> _get_post('styles');
		
		if( $object =='listCombo' ){
			__(form()->{$object}("{$properties}", $styles, $this->{$method}()));
		}
		else if( $object =='combo' ) 
		{
			if( $this ->URI->_get_post('GroupId') =='filter_by_agent'){
				__(form()->{$object}("{$properties}", $styles, $this->{$method}(),null,array("change" => "Ext.DOM.ShowAgentBySpvId(this.value,'listCombo','select long','Div2','label2','Caller Name');")));
			}
			
			if( $this ->URI->_get_post('GroupId')=='filter_by_spv'){
				__(form()->{$object}("{$properties}", $styles, $this->{$method}(),null,array("change" => "Ext.DOM.ShowSpvByAtm(this.value,'listCombo','null','Div2','label2','Spv Name');")));
			}
			
			if( $this ->URI->_get_post('GroupId')=='filter_by_atm_group_campaign'){
				__(form()->{$object}("{$properties}", $styles, $this->{$method}(),null, array("change" => "Ext.DOM.ShowDataCampaignId(this.value,'listCombo','Div2','label2');")));
			}
			
			if( $this ->URI->_get_post('GroupId')=='filter_by_spv_group_campaign'){
				__(form()->{$object}("{$properties}", $styles, $this->{$method}(),null, array("change" => "Ext.DOM.ShowDataCampaignId(this.value,'listCombo','Div2','label2');")));
			}
			
			if( $this ->URI->_get_post('GroupId')=='filter_by_agent_group_campaign'){
				__(form()->{$object}("{$properties}", $styles, $this->{$method}(),null, array("change" => "Ext.DOM.ShowAgentBySpvId(this.value,'combo','select','Div2','label2','Caller Name');")));
			}
			
			if( $this ->URI->_get_post('GroupId')=='filter_campaign_group_agent')
			{
				__(form()->{$object}("{$properties}", $styles, $this->{$method}(),null, array("change" => "Ext.DOM.ShowByAtm(this.value,'combo','select long','SPV Name');")));
			}
		}
	}
 }
 
} 

/**
 * @ def : group by showRecsource 
 *  ---------------------------------------------------------
 * @ param : recsource all 
 */
 
public function showRecsource()
{
	return $this -> {base_class_model($this)} -> _getRecsource();
	
}

 

/** 
 * @ def : GroupFilterBy 
 * ---------------------------------------------------------------
 *
 */
 
 public function showCampaign()
 {
	return $this -> {base_class_model($this)} -> _getCampaignReady();
 }


/* @ param : GroupFilterBy **/
/* ------------------------------------------------- */ 

 public function ShowDataCampaignId()
{
	if( $this -> URI->_get_have_post('UserId') )
	{
		__(form()->$_REQUEST['type']("CampaignId", $_REQUEST['class'],self::showCampaign() ));	
	}
	else{
		__(form()->combo("CampaignId",'input_text box',array()));	
	}	

}
/* @ param : GroupFilterBy **/
/* ------------------------------------------------- */ 

public function ShowByAtm() {

	__(form()->$_REQUEST['type']("SpvId", $_REQUEST['Class'], self::showATM(),null, array("change" => "Ext.DOM.ShowAgentBySpvId(this.value,'listCombo','select','Div3','label3','Caller Name');")));	
}

/* @ param : GroupFilterBy **/

public  function showATM() 
{
	$_conds = array();
	if( $rows  = $this -> M_SysUser -> _get_supervisor()) {
		$_conds = $rows;
	}
	
	return $_conds;
}


/* @ param : GroupFilterBy **/

public  function showSPV() 
{
	$_conds = array();
	if( $rows  = $this -> M_SysUser -> _get_supervisor()) {
		$_conds = $rows;
	}
	
	return $_conds;
}

/* @ param : GroupFilterBy **/
/* ------------------------------------------------- */ 

public function ShowAgentByAtm()
{
	if( $this -> URI -> _get_have_post('AtmId') 
		AND $this -> URI -> _get_post('AtmId')!='')
	{
		if( $this ->URI->_get_post('type')=='listCombo'){
			__(form()->listCombo('UserId', null, $this ->{base_class_model($this)}->_getAgentByAtm($this -> URI -> _get_post('AtmId')) ));
		}
		else{
			__(form()->combo('UserId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getAgentByAtm($this -> URI -> _get_post('AtmId')) ));
		}	
	}
	else{
		__(form()->combo('UserId', 'select box', array() ));
	}
}



/* @ param : ShowSpvByAtm spv is match of leader on realy privileges **/
/* -------------------------------------------------------------------*/ 

public function ShowAgentBySpv()
{
	if( $this -> URI -> _get_have_post('SpvId') 
		AND $this -> URI -> _get_post('SpvId')!='')
	{
		if( $this ->URI->_get_post('type')=='listCombo'){
			__(form()->listCombo('UserId', null, $this ->{base_class_model($this)}->_getAgentBySpv($this -> URI -> _get_post('SpvId')) ));
		}
		else
		{
			if( $this ->URI->_get_post('filterby')=='filter_by_agent_group_campaign'){
				__(form()->combo('UserId', $this ->URI->_get_post('Class'),  $this ->{base_class_model($this)}->_getAgentBySpv($this -> URI -> _get_post('SpvId')), 
					null, array("change" => "Ext.DOM.ShowDataCampaignId(this.value,'listCombo','Div4','label4');") ));
			}
			else{	
				__(form()->combo('UserId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getAgentBySpv($this -> URI -> _get_post('SpvId')) ));
			}
		}	
	}
	else{
		__(form()->combo('UserId', 'select box', array() ));
	}
}


/* @ param : ShowSpvByAtm **/
/* ------------------------------------------------- */ 
public function ShowSpvByAtm()
{
	if( $this -> URI -> _get_have_post('AtmId') 
		AND $this -> URI -> _get_post('AtmId')!='')
	{
		if( $this ->URI->_get_post('type')=='listCombo'){
			__(form()->listCombo('SpvId', null, $this ->{base_class_model($this)}->_getSpvByAtm($this -> URI -> _get_post('AtmId')) ));
		}
		else 
		{
			if( $this ->URI->_get_post('filterby')=='filter_by_agent') {
				__(form()->combo('SpvId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getSpvByAtm($this -> URI -> _get_post('AtmId')), null, array('change' => "Ext.DOM.ShowAgentBySpvId(this.value,'listCombo','null','Div3','label3','TSR Name');") ));
			}
			else if( $this ->URI->_get_post('filterby')=='filter_by_spv_group_campaign') {
				__(form()->combo('SpvId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getSpvByAtm($this -> URI -> _get_post('AtmId')), null, array('change' => "Ext.DOM.ShowDataCampaignId(this.value,'listCombo','Div3','label3');") ));
			}
			else if( $this ->URI->_get_post('filterby')=='filter_by_agent_group_campaign') 
			{
				__(form()->combo('SpvId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getSpvByAtm($this -> URI -> _get_post('AtmId')), null, array('change' => "Ext.DOM.ShowAgentBySpvId(this.value,'combo','select','Div3','label3','TSR Name');")));
			}
			else if( $this ->URI->_get_post('filterby')=='filter_campaign_group_agent') {
				__(form()->combo('SpvId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getSpvByAtm($this -> URI -> _get_post('AtmId')), null, array('change' => "Ext.DOM.ShowAgentBySpvId(this.value,'listCombo','select','Div4','label4','TSR Name');")));
			}
			
			else{
				__(form()->combo('SpvId', $this ->URI->_get_post('Class'), $this ->{base_class_model($this)}->_getSpvByAtm($this -> URI -> _get_post('AtmId')) ));
			}	
		}
	}
	else{
		__(form()->combo('SpvId', 'select box', array() ));
	}
}


/* @ param : CSS Atrribute by PHP setHedaerStyles **/
/* ------------------------------------------------- */ 

public function setHedaerStyles( $line = null )
{
	$CSS['first']  = array( 'html' => 'header first', 'excel' => 'xl6715526');
	$CSS['middle'] = array( 'html' => 'header middle','excel' => 'xl6715526');
	return $CSS[$line][strtolower($this -> URI->_get_post('mode'))]; 
}

/* @ param : CSS Atrribute by PHP  setContentStyles **/
/* ------------------------------------------------- */ 

public function setContentStyles( $line = null )
{
	$CSS['first']  = array( 'html' => 'content first left', 'excel' => 'xl6515526');
	$CSS['middle'] = array( 'html' => 'content middle right','excel' => 'xl6515526');
	return $CSS[$line][strtolower($this -> URI->_get_post('mode'))]; 
}


/* @ param : CSS Atrribute by PHP  setBottomStyles **/
/* ------------------------------------------------- */ 

public function setBottomStyles( $line = null )
{
	$CSS['first']  = array( 'html' => 'total first right', 'excel' => 'xl6615526');
	$CSS['middle'] = array( 'html' => 'total middle right','excel' => 'xl6615526');
	return $CSS[$line][strtolower($this -> URI->_get_post('mode'))]; 
}


/* @ param : GroupFilterBy **/
/* ------------------------------------------------- */ 

function ShowExcel()
 {
	Excel() -> HTML_Excel(get_class($this).''.time());
	if( is_array($this -> R_Model ) 
		AND !empty($this -> FilterGroupBy) )
	{
	
	  /* load instance of variable class **/
		
		if( $EUI_report_model = $this->R_Model[$this->FilterGroupBy] )
		{
		/* load header *******/
		
			$this->load->view("rpt_call_tracking/report_calltrack_excel_hader",  array('title' => $EUI_report_model['title']) );
			
		/* load model report */
			$this->load->report($EUI_report_model['class']);
		/* cek existing report */
			if( class_exists($EUI_report_model['class']) ) 
			{
				$UI = $this -> {$EUI_report_model['class']} ->_getResultReport();
				$this->load->view("rpt_call_tracking/{$UI['CALL_VIEWER']}",$UI);
			}
		}
		
		// load footer 
		$this->load->view("rpt_call_tracking/report_calltrack_html_footer");
	}
 
 }
 
 /* @ param : GroupFilterBy **/
/* ------------------------------------------------- */ 

 public function showHTML() 
 {
	
	if( is_array($this -> R_Model ) 
		AND !empty($this -> FilterGroupBy) )
	{
	
	  /* load instance of variable class **/
		
		if( $EUI_report_model = $this->R_Model[$this->FilterGroupBy] )
		{
		/* load header *******/
		
			$this->load->view("rpt_call_tracking/report_calltrack_html_hader",  array('title' => $EUI_report_model['title']) );
			
		/* load model report */
			$this->load->report($EUI_report_model['class']);
		/* cek existing report */
			if( class_exists($EUI_report_model['class']) ) 
			{
				$UI = $this -> {$EUI_report_model['class']} ->_getResultReport();
				$this->load->view("rpt_call_tracking/{$UI['CALL_VIEWER']}",$UI);
			}
		}
		
		// load footer 
		$this->load->view("rpt_call_tracking/report_calltrack_html_footer");
	}
	
 }
 
 
 
/* @ param : CtrByAgent **/
/* ------------------------------------------------- */ 
 
public function ShowReport()
{
	if( strtolower($this ->URI->_get_post('mode'))=='html')
	{
		self::showHTML(); 
	}	
	
	if( strtolower($this ->URI->_get_post('mode'))=='excel') 
	{
		self::ShowExcel(); 
	}	
}
 

/* @ param : CtrByAgent **/
/* ------------------------------------------------- */ 

 public function _POST()
 {
	$_reqs = array(); $filter = array('AgentId');
	
	$param =  $this -> URI -> _get_all_request();
	foreach( $param as $keys => $values ) 
	{ 
		if( in_array($keys, $filter) ){
			$_reqs[$keys] = $this -> URI->_get_array_post($keys);
		}
		else{
			$_reqs[$keys] = $values;
		}
	}

	return $_reqs; 	
 }
 

/* @ param : CtrByAgent **/
/* ------------------------------------------------- */ 
 public function _AGENT()
 {
	$_reqs = array(); 
	$filter = array('AgentId');
	
	$UserAgents = $this -> URI->_get_array_post('AgentId');
	foreach($UserAgents as $k => $v )
	{
		$_reqs[$v] = $this ->M_Report->_getAgentName($v); 	
	}
return $_reqs; 	
 }
 
 
}