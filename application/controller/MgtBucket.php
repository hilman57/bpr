<?php
/*
 * E.U.I 
 *
 
 * subject	: get SetCampaign modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
 
class MgtBucket extends EUI_Controller
{
	
/*
 * EUI :: __constructor() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
 function MgtBucket() {
	parent::__construct();
	// tambahan aja[modul_rounder]
	$this->load->helpers(array('EUI_Object','EUI_Round'));	
	//load model array
	$this -> load -> model(array(base_class_model($this), 'M_WorkArea', 'M_ModViewUpload',
							'M_SetCampaign','M_Upload', 'M_ModDistribusi','M_Template','M_User'));
 }
 
/*
 * EUI :: getCampaignName() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	

function SaveByCheckedATM()
{	
	$_success = array('success'=>0, 'mesages' => '0');
		
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{ 
		$post = array (
			'CampaignId' => $this -> URI->_get_post('campaign_id'),
			'BucketId' => $this -> URI -> _get_array_post('ftp_list_id'),
			'atm'=>	$this -> URI->_get_post('atm')
		);
		
		if(class_exists('M_ModDistribusi'))
		{
			$total_data = $this ->M_ModDistribusi ->_setDistribusi($post,'SaveByCheckedATM');
			if( $total_data > 0 )
			{
				$_success = array('success'=>1, 'mesages' => $total_data );
			} 
		}
	}
	
	echo json_encode($_success);
	
}
 
/*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
  
 public function getCampaignName()
 {
	$datas = array();
	if(class_exists('M_SetCampaign') ){
		$datas = $this->M_SetCampaign->_get_campaign_name();
	}
	echo json_encode($datas);	
}


/*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
public function getATMName() {
 	$datas = array();
	if(class_exists('M_User')){
		$datas = $this -> M_User -> _getATM();
	}
	echo json_encode($datas);
 } 
 
 /*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
public function index()  {
	$EUI = array(
		'WorkArea' => $this->M_WorkArea->_get_branch_work(),
		'Filename' => $this->M_ModViewUpload->_getModFilename(),
		'ProductId' => $this->M_SetCampaign->_get_campaign_name(),
		'page'=> $this ->{base_class_model($this)}->_get_default()
	);
	
	$this -> load -> view("mgt_bucket_data/view_bucket_nav.php", $EUI);
 }
 
/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
public function Content() {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$EUI = array();
		
		$_EUI['page'] = $this -> {base_class_model($this)} -> _get_resource();    // load content data by pages 
		$_EUI['num']  = $this -> {base_class_model($this)} -> _get_page_number(); // load content data by pages 
		
		if( is_array($_EUI) )
		{
		// sent to view data 
			$this -> load -> view("mgt_bucket_data/view_bucket_list.php", $_EUI);
		}	
	}	
 }
 

/*
 * EUI :: UploadBucket() 
 * -----------------------------------------
 *
 * @ def		will process on modul upload only 
 * @ param		not assign parameter
 */	

private function _getFileName()
{ 
	$_name = null; $_data = explode('.',$_FILES['fileToupload']['name'] );
	if(is_array($_data) )
	{
		$_name = strtoupper( $_data[count($_data)-1] );
	}
	
	return $_name;
} 
/*
 * EUI :: UploadBucket() 
 * -----------------------------------------
 *
 * @ def		will process on modul upload only 
 * @ param		not assign parameter
 * @ version 	update delegated data from type file 
 */	
 
 
 function UploadBucket()
 {
	
	$_success = array('success'=>0, 'mesages' => 'File Not Found ');
	
	if(isset($_FILES['fileToupload']['name']) && is_array($_FILES) 
		&& $this->URI->_get_have_post('TemplateId') )
	{
		$Template =&M_Template::_getDetailTemplate( $this->URI->_get_post('TemplateId'));
		
		$_file_extension = $this->_getFileName();
		
	// set global 
	
		if(!defined('EXCEL')) define('EXCEL','XLS');
		if(!defined('TEXT')) define('TEXT','TXT');
		
	// if extension is not null 
		if( !is_null($_file_extension) )
		{
			if( class_exists('M_Upload'))
			{
				$_callBack = null;
				
				if( ($_file_extension == EXCEL) 
					OR ($_file_extension == TEXT) ) 
				{
				
		/* ------------------------------------------------------------------------------------- 
		 * @ def :  if upload No bucket data 
		 * -------------------------------------------------------------------------------------
		*
		* @param : get on template setting 
		* @param : mode && bucket = not
		*
		*/		
			 $Template['TemplateBucket'] = ( is_null( $Template['TemplateBucket'] ) ? 'N' : $Template['TemplateBucket'] );
			 
			 if( isset($Template['TemplateBucket']) )  
				{
					switch( strtoupper($Template['TemplateMode']) )
					{
						case 'INSERT' : 
								$_callBack = $this -> M_Upload -> setInsertUpload( 
									array(
									'file_attribut' => $_FILES,
									'request_attribut' => $this -> URI -> _get_all_request() 
									), 
								array() );
								$_success  = array('success'=>1, 'mesages' => $_callBack );	
								
							break;	
								
							case 'UPDATE' : 
								$_callBack = $this -> M_Upload -> setUpdateUpload( array('file_attribut' => $_FILES,'request_attribut' => $this -> URI -> _get_all_request() ), 
									array() );
								$_success  = array('success'=>1, 'mesages' => $_callBack );	
							break;	
								
						}
					}
				}	
				else{
					$_success = array('success'=>0, 'mesages' => 'File extension no suported. ');
				}
			}
		}	
	}	
	
	echo json_encode($_success);
 }
 
 
 /*
 * EUI :: Upload Manual() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
 function ManualUpload()
 {
	
	$templatetype = $this -> URI->_get_post('templatetype');
	if( $this->EUI_Session->_have_get_session('UserId') ) 
	{
		// $Template = $this->{base_class_model($this)}->_get_template();
		// foreach($Template as $key => $val){
			// $tmptype = explode("_",$val);
			// if($tmptype[0] == "UPLOAD" && ($tmptype[1] != "SWAP" || $tmptype[1] != "ACCESS")){
				// $Template['Template'][$key] = $val;
			// }else if($tmptype[0] == "UPDATE"){
				// $Template['Template'][$key] = $val;
			// }
		// }
		
		$Template['Template'] = $this->{base_class_model($this)}->_get_template($templatetype);
		$Template['CampaignId'] = $this->M_SetCampaign->_get_campaign_name();
		if(is_array($Template) )
		{
			$this -> load -> view("mgt_bucket_data/view_bucket_upload", $Template);
		}	
	}	
 }

 /*
 * EUI :: Upload Rank() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
 function TemplateRank()
 {
	
	$templatetype = $this -> URI->_get_post('templatetype');
	if( $this->EUI_Session->_have_get_session('UserId') ) 
	{
		$Template['Template'] = $this->{base_class_model($this)}->_get_templaterank($templatetype);
		if(is_array($Template) )
		{
			$this -> load -> view("mgt_bucket_data/view_upload_rank", $Template);
		}	
	}	
 }
 
/*
 * EUI :: saveByAmount() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
  
 function saveByAmount()
 {
	$_success = array('success'=>0, 'mesages' => '0');
	
	$post = array
	(
		'AmountSize' => ($this -> URI->_get_have_post('amount_size') ? $this -> URI->_get_post('amount_size') : false ),
		'AmountAssign' => ($this -> URI->_get_have_post('amount_assign') ? $this -> URI->_get_post('amount_assign') : false ),
		'AssignStatus' => ($this -> URI->_get_have_post('assign_status') ? $this -> URI->_get_post('assign_status') : false ),
		'CampaignId' => ($this -> URI->_get_have_post('campaign_name') ? $this -> URI->_get_post('campaign_name') : false ),
		'FilenameId' => ($this -> URI->_get_have_post('fileupload') ? $this -> URI->_get_post('fileupload') : false ),
		'StartDate' => ($this -> URI->_get_have_post('start_date') ? $this -> URI->_get_post('start_date') : false ),
		'EndDate' => ($this -> URI->_get_have_post('end_date') ? $this -> URI->_get_post('end_date') : false )
	);
	
	if(class_exists('M_ModDistribusi'))
	{
		$total_data = $this ->M_ModDistribusi ->_setDistribusi($post,'saveByAmount');
		if( $total_data > 0 )
		{
			$_success = array('success'=>1, 'mesages' => $total_data );
		} 
	}
	
	echo json_encode($_success);
}
 
/*
 * EUI :: saveByAmount() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
  
function saveByAmountATM()
 {
	$_success = array('success'=>0, 'mesages' => '0');
	
	$post = array
	(
		'AmountSize' => ($this -> URI->_get_have_post('amount_size') ? $this -> URI->_get_post('amount_size') : false ),
		'AmountAssign' => ($this -> URI->_get_have_post('amount_assign') ? $this -> URI->_get_post('amount_assign') : false ),
		'AssignStatus' => ($this -> URI->_get_have_post('assign_status') ? $this -> URI->_get_post('assign_status') : false ),
		'CampaignId' => ($this -> URI->_get_have_post('campaign_name') ? $this -> URI->_get_post('campaign_name') : false ),
		'FilenameId' => ($this -> URI->_get_have_post('fileupload') ? $this -> URI->_get_post('fileupload') : false ),
		'StartDate' => ($this -> URI->_get_have_post('start_date') ? $this -> URI->_get_post('start_date') : false ),
		'EndDate' => ($this -> URI->_get_have_post('end_date') ? $this -> URI->_get_post('end_date') : false ),
		'atm' => ($this -> URI->_get_have_post('atm') ? $this -> URI->_get_post('atm') : false )
	);
	
	if(class_exists('M_ModDistribusi'))
	{
		$total_data = $this ->M_ModDistribusi ->_setDistribusi($post,'saveByAmountATM');
		if( $total_data > 0 )
		{
			$_success = array('success'=>1, 'mesages' => $total_data );
		} 
	}
	
	echo json_encode($_success);
}

 
/*
 * EUI :: SaveByChecked() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	

function SaveByChecked()
{	
	$_success = array('success'=>0, 'mesages' => '0');
		
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{ 
		$post = array (
			'CampaignId' => $this -> URI->_get_post('campaign_id'),
			'BucketId' => $this -> URI -> _get_array_post('ftp_list_id')	
		);
		
		if(class_exists('M_ModDistribusi'))
		{
			$total_data = $this ->M_ModDistribusi ->_setDistribusi($post,'saveByChecked');
			if( $total_data > 0 )
			{
				$_success = array('success'=>1, 'mesages' => $total_data );
			} 
		}
	}
	
	echo json_encode($_success);
	
}

// edit hilman 13-5-2022
function UploadBucketQuest()
 {
	//  var_dump($this->URI->_get_post('ktgr_name'));
	//  die;
	
	$_success = array('success'=>0, 'mesages' => 'File Not Found ');
	
	if(isset($_FILES['fileToupload']['name']) && is_array($_FILES) 
		&& $this->URI->_get_have_post('TemplateId') )
	{
		$Template =&M_Template::_getDetailTemplate( $this->URI->_get_post('TemplateId'));
		// echo '<pre>';
		// print_r($Template);
		// die;
		
		$_file_extension = $this->_getFileName();
		
	// set global 
	
		if(!defined('EXCEL')) define('EXCEL','XLS');
		if(!defined('TEXT')) define('TEXT','TXT');
		
	// if extension is not null 
		if( !is_null($_file_extension) )
		{
			if( class_exists('M_Upload'))
			{
				$_callBack = null;
				
				if( ($_file_extension == EXCEL) 
					OR ($_file_extension == TEXT) ) 
				{
				
		/* ------------------------------------------------------------------------------------- 
		 * @ def :  if upload No bucket data 
		 * -------------------------------------------------------------------------------------
		*
		* @param : get on template setting 
		* @param : mode && bucket = not
		*
		*/		
			//  $Template['TemplateBucket'] = ( is_null( $Template['TemplateBucket'] ) ? 'N' : $Template['TemplateBucket'] );
			//  echo "<pre>";
			//  print_r(strtoupper($Template['TemplateMode']));
			//  die;
			// insert

			 if( isset($Template['TemplateBucket']) )  
				{
					switch( strtoupper($Template['TemplateMode']) )
					{
						case 'INSERT' : 
								$_callBack = $this -> M_Upload -> setInsertUploadquestioner( 
									array(
									'file_attribut' => $_FILES,
									'request_attribut' => $this -> URI -> _get_all_request() 
									), 
								array() );
								$_success  = array('success'=>1, 'mesages' => $_callBack );	
								
							break;	
								
							case 'UPDATE' : 
								$_callBack = $this -> M_Upload -> setUpdateUpload( array('file_attribut' => $_FILES,'request_attribut' => $this -> URI -> _get_all_request() ), 
									array() );
								$_success  = array('success'=>1, 'mesages' => $_callBack );	
							break;	
								
						}
					}
				}	
				else{
					$_success = array('success'=>0, 'mesages' => 'File extension no suported. ');
				}
			}
		}	
	}	
	
	echo json_encode($_success);
 }
 
}

?>