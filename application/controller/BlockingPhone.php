<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class BlockingPhone extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function BlockingPhone() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
		
 }

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->M_SrcCustomerList->_getDeskollByLogin(),
	 'GenderId' => $this->M_SrcCustomerList->_getGenderId(),
	 'CampaignId' => $this->M_SrcCustomerList->_getCampaignByActive(),
	 'AccountStatus' => $this->M_SrcCustomerList->_getAccountStatus(),
	 'LastCallStatus' => $this->M_SrcCustomerList->_getLastCallStatus() );
	 
   if(is_array($_EUI)) {
	 $this -> load ->view('src_blocking_list/view_blocking_nav',$_EUI);
   }	
}
	
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('src_blocking_list/view_blocking_list',$_EUI);
	 }	
  }	
}

/*
 * @ pack : keep of the data 
 * -----------------------------------------------
 */
public function SetUnblockPhoneAll()
{
	 $_conds= array('success'=> 0);
	 if( $count = $this->{base_class_model($this)}->_SetUnblockPhoneAll() )
	 {
		$_conds= array('success'=> $count );	
	 }
	 echo json_encode($_conds);
}
 
public function SetUnblockPhone()
{
 $_conds= array('success'=> 0);
 if( $count = $this->{base_class_model($this)}->_SetUnblockPhone() )
 {
	$_conds= array('success'=> $count );	
 }
 echo json_encode($_conds);
 
}

}

// END OF CLASS 

?>