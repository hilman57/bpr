<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class ModAccountLock extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function ModAccountLock() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this),'M_SrcKeepList'));
		
 }

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
	 'GenderId' => $this->{base_class_model($this)}->_getGenderId(),
	 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
	 'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus(),
	 'LastCallStatus' => $this->{base_class_model($this)}->_getLastCallStatus() );
	 
   if(is_array($_EUI)) {
	 $this -> load ->view('src_accountlock_list/view_accountlock_nav',$_EUI);
   }	
}
	
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('src_accountlock_list/view_accountlock_list',$_EUI);
	 }	
  }	
}

 public function SetUnlockAccountAll(){
	 $conds = array('success' => 0 );
	 
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	 
	 //$count = $this ->{base_class_model($this)}->_SetUnlockAccountAll();
	if( $count = $this ->{base_class_model($this)}->_SetUnlockAccountAll() )
	{
		$conds = array('success' => $count );
	}
 } 

  echo json_encode($conds);
 }

/*
 * @ pack : keep of the data 
 * -----------------------------------------------
 */
 
 public function SetUnlockAccount()
{
 
 $conds = array('success' => 0 );
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	if( $count = $this ->{base_class_model($this)}->_SetUnlockAccount() )
	{
		$conds = array('success' => $count );
	}
 } 

  echo json_encode($conds);
}

// END OF CLASS 


}

?>