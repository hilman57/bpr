<?php
class Scaffolding extends EUI_Controller
{
	
protected $scaffold_tables = null; 
	
public function Scaffolding() 
{
	parent::__construct();	
	$this->load->model(base_class_model($this));	
	$this->scaffold_tables = $this->URI->_get_post('database');
	
/// set default by request ****/
	
	if( !is_null($this->scaffold_tables)) 
	{
		$this->{base_class_model($this)}->_set_scaffolding_tables($this->scaffold_tables);
		$this->{base_class_model($this)}->_set_scaffolding_postpage($this->URI->_get_post('v_page'));
		$this->{base_class_model($this)}->_set_scaffolding_attribute();
		$this->{base_class_model($this)}->_set_scaffolding_perpage(20);
	}
}


/**
 * @ def : defualt of page read first 
 * ------------------------------------------------------
 */
 
public function index()
{
	if( !is_null($this->scaffold_tables) )
	{
		$UI = array(
			'total_record' => $this->{base_class_model($this)}->_get_scaffolding_count(),
			'total_page' => $this->{base_class_model($this)}->_get_scaffolding_page(),
			'scaffolding_tables' => $this->scaffold_tables
		);
		
		$this->load->view("mod_view_scaffolding/scafolding_view_nav", $UI);
	}
}



/**
 * @ def : defualt of page read first 
 * ------------------------------------------------------
 */
 
public function content()
{
	$UI = array(
		'field_data' => $this->{base_class_model($this)}->_get_scaffolding_data(),
		'field_label' => $this->{base_class_model($this)}->_get_scaffolding_field(),
		'field_class' => $this->{base_class_model($this)}->_get_scaffolding_styles()
	);
	
	$this->load-> view('mod_view_scaffolding/scafolding_view_list',$UI);
}
	
}

?>