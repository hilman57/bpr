<?php 
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	//date_default_timezone_set('Asia/Bangkok');

	// php -q /var/www/html/hsbc_coll/index.php SMSVarAutomatic process (devel)
	// php -q /opt/enigma/webapps/hsbc/index.php SMSVarAutomatic process (asli)
	/*
	 * [ SMS BLAST HSBC COLLECTION STB]
	 * Created by : omens;
	 * maintance by :didi ganteng
	 * @param  [type] $CustomerId [description]
	 * @return [type]             [description]
	 */
	class SMSVarAutomatic extends EUI_Controller {

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */	
		var $dateinit = null; 
		var $timerinit = null;
		var $statusinit = null;
 

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */	
		function __construct()
		{
			parent::__construct();
			// biar di kirim sama sms server 
			// init tanggal process OK 	
			if( is_null($this->dateinit) ){
				$this->statusinit = 102; 
				$this->dateinit = date('Y-m-d');
				$this->timerinit = date('H:i');
			}
		}

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */	
		function passtru()
		{
			 
			$result_array = array();
			$sql = sprintf("
					select  
						a.SRV_Data_Id,  	
						a.SRV_Data_TemplateId, 
						a.SRV_Data_Status,  a.SRV_Data_Priority, 
						a.SRV_Data_Context as SRV_Data_Context, 
						time_format(a.SRV_Data_SentTime, '%%H:%%i') SRV_Data_SentTime,
						a.SRV_Data_SentDate as SRV_Data_SentDate,
						b.SmsTplContent as SERV_TemplateMessage 
					from serv_sms_automatic a
					left join serv_sms_tpl b on a.SRV_Data_TemplateId = b.SmsTplId
					where a.SRV_Data_Active=1 
					and a.SRV_Data_Process=0 
					and date(a.SRV_Data_SentDate)='%s'", $this->dateinit
				);
				// and date(a.SRV_Data_SentDate)='2019-12-06'", $this->dateinit
// and date(a.SRV_Data_SentDate)='%s'", $this->dateinit
				// printf("%s\n\r", $sql);
				$qry = $this->db->query( $sql);
				if( $qry && $qry->num_rows() > 0 ) 
					foreach( $qry->result_assoc() as $row ){
						$result_array[$row['SRV_Data_Id']] = $row;
					}
					// return data process on here OK 	
			return (array)$result_array;
		}

		function _getTemplateWODate()
		{
			 
			$result_array = array();
			$sql = sprintf("
					select  
						a.SRV_Data_Id,  	
						a.SRV_Data_TemplateId, 
						a.SRV_Data_Status,  
						a.SRV_Data_Priority, 
						a.SRV_Data_Context as SRV_Data_Context, 
						time_format(a.SRV_Data_SentTime, '%%H:%%i') SRV_Data_SentTime,
						a.SRV_Data_SentDate as SRV_Data_SentDate,
						b.SmsTplContent as SERV_TemplateMessage 
					from serv_sms_automatic a
					left join serv_sms_tpl b on a.SRV_Data_TemplateId = b.SmsTplId
					where a.SRV_Data_Active=1 
					and a.SRV_Data_Process=0
					and a.SRV_Data_Id =5"
				);
			
			//printf("%s\n\r", $sql);
			$qry = $this->db->query( $sql);
			
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $row ){
					$result_array[$row['SRV_Data_Id']] = $row;
				}
			// return data process on here OK 	
			return (array)$result_array;
		}
 
		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function index(){ }
		
		/*
		 [ SMS BLAST HSBC COLLECTION STB]
		 * Created by : omens;
		 * maintance by :didi ganteng
		 */
		function startrow( $Id = 0  )
		{
		 
		 	$startrow = 0;
		 	$sql = sprintf("update serv_sms_automatic a set a.SRV_Data_Process = 1 
						 where a.SRV_Data_Id ='%s'", $Id);
			if( $this->db->query( $sql ) ){
				$startrow++;
			}
			return $startrow;
		}

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		 
		function endrow( $nextid = 0  )
		{
		 
			$startrow = 0;
			$nextrow = @call_user_func_array('_getNextDate', array( $this->dateinit ));
			printf("next date process : %s\n\r", $nextrow);
			$sql = sprintf("
					update serv_sms_automatic a set 
							a.SRV_Data_SentDate = '%s',
							a.SRV_Data_Process = 0  
						where a.SRV_Data_Id ='%s'", $nextrow, $nextid
				);
			if( $this->db->query( $sql ) ){
				$startrow++;
			}
			// return callback 
			return (int)$startrow;
		}

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function nextmonth( $nextid = 0, $nextmonth  )
		{
		 
			//var_dump($nextmonth);
			$startrow = 0;
			$currdate =  explode("-", $nextmonth);
			$nextrow  = sprintf("%s-%s", NextMonth( $nextmonth ), $currdate[2]);
		 
		 	printf("next date process : %s\n\r", $nextrow);
		 	$sql = sprintf("
		 		update serv_sms_automatic a set 
					a.SRV_Data_SentDate = '%s',
					a.SRV_Data_Process = 0  
				where a.SRV_Data_Id ='%s'", $nextrow, $nextid
			);
			if( $this->db->query( $sql ) ){
				$startrow++;
			}
			// return callback 
			return (int)$startrow;
		}

		/*
		[ SMS BLAST HSBC COLLECTION STB]
		 * Created by : omens;
		 * maintance by :didi ganteng
		 */
		function next_month_new($nextid=0) 
		{
			$start = 0;
			$today = date('Y-m-d', strtotime('+1 month'));
			printf("next date process : %s\n\r", $today);

			$sql = "
		 		update serv_sms_automatic a set 
					a.SRV_Data_SentDate = '$today',
					a.SRV_Data_Process = '0'  
				where a.SRV_Data_Id ='$nextid'";

			if( $this->db->query( $sql ) ){
				$start++;
			}
			// return callback 
			return (int)$start;
		}
	 
		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function getbulan( $disposition = null, $row = null  )
		{
			 
		 	$result_array = array();
		 	$sql= sprintf("
		 			select  
		 				a.deb_id, 
		 				a.deb_acct_no, 
		 				a.deb_mobileno1, 
						a.deb_call_activity_datets,
						date(a.deb_call_activity_datets) as UpdateTs,
						a.deb_agent, b.AssignAdmin, 
						b.AssignAmgr, b.AssignMgr, 
						b.AssignSpv, b.AssignLeader,  b.AssignSelerId 
					from t_gn_debitur a 
					left join t_gn_assignment b on a.deb_id=b.CustomerId
					where a.deb_call_status_code ='%s'
					and a.deb_mobileno1 is not null", $disposition
				);
				/// yang ini masih saya limit 5 , nanti di ubah 
				// aja . karna cuma buat test jadi di limit 5 aja .. 
		 	$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
						// then will get data process 
						if( is_array( $result_row ) and isset( $result_row['UpdateTs'] )  ){
						// get dat dif date on last update 
						$daytot = call_user_func_array('_getDateDiff', 
										array( 
											$result_row['UpdateTs'], 
											$result_row['CreateTs']
										)
									);	
						$dayTotal = 0;			
						$dayTotal = ( isset($daytot['days_total']) ? $daytot['days_total'] : 0 );	
						// jika perbedaan data process sesuai setup
						$dayOfAccount = $result_row['deb_acct_no'];
				
						if( $dayTotal > 0 and $dayTotal < 10 ) {
						printf("find account : <%s> sys : %s <compare> data : %s\n\r", 
									$dayOfAccount, 
									$row->SRV_Data_Priority, 
									$dayTotal);
						}
						// abaikan priority -nya 
						if( !$row->SRV_Data_Priority ){
							printf("success account : <%s> sys : %s <compare> data : %s\n\r", 
									$dayOfAccount, 
									$row->SRV_Data_Priority, 
									$dayTotal);
									
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}						
					} 
		  		}
		 		// return back data customer.
		  		return (array)$result_array;
		}

		/**
		**WODATE - 5
		**/
		function getWOdate5() 
		{
			$today = date('l');
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
						
					"
				);
					
			$sql = "
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1, 
						d.deb_wo_date, 
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 1=1 and d.deb_mobileno1 !=''	
				";
					
			$qry = $this->db->query( $sql );

		 	if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						//printf("%s\n",  $result_row['deb_wo_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 5 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
				}
			// return back data customer.
			 // print_r($result_array);
			return (array)$result_array;
		}

		/**
		**WODATE - 20
		**/
		function getWOdate20() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
					"
				);
					
			$sql = "
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1, 
						d.deb_wo_date, 
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where  d.deb_mobileno1 !=''
				";
			$qry = $this->db->query( $sql );
		 	if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						//printf("%s\n",  $result_row['deb_wo_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 20 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
				}
			// return back data customer.
			//  print_r($result_array);
			return (array)$result_array;
		}

		/**
		**WODATE - 30
		**/
		function getWOdate30() 
		{
			$result_array = array();
			/* OLD 
			$sql = sprintf("
					select 
						DATEDIFF(curdate(), d.deb_wo_date) as totalDay,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
					"
				);
				*/
						/*
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_wo_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where  d.deb_mobileno1 !=''
			";
			*/
			
			// query untuk data yang WO date 30 hari   
			$sql = sprintf("
					SELECT 
						DATEDIFF(CURDATE(), d.deb_wo_date) as totalDay,
						d.deb_wo_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					FROM t_gn_debitur d
					INNER JOIN t_gn_assignment si on d.deb_id=si.CustomerId
					LEFT JOIN t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					WHERE 
						d.deb_mobileno1 !=''
						HAVING totalDay BETWEEN 30 and 30
					"
				);
					
		
			
					// echo $sql;
			$qry = $this->db->query( $sql );
		 	if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					// print_r($result_row);
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						//printf("%s\n",  $result_row['deb_wo_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						// $dayTotal = (int)$daytot['days_total'];
						$dayTotal = (int)$result_row['totalDay'];
echo "TotalARray ".$dayTotal;
						if( $dayTotal == 30 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
				}
		  
		 		// return back data customer.
				 print_r($result_array);
		  	return (array)$result_array;
		}

		/**
		**WODATE - 40
		**/
		function getWOdate40() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
					"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_wo_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where  d.deb_mobileno1 !=''
				;
			";
					
			$qry = $this->db->query( $sql );
		 	if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						//printf("%s\n",  $result_row['deb_wo_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];

						if( $dayTotal == 40 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
				}
		  
	 		// return back data customer.
			//  print_r($result_array);
		  	return (array)$result_array;
		}	

		/**
		**WODATE - 53
		**/
		function getWOdate53() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll !=''
						and d.deb_perm_msg_coll IN ('LP','VLP')
					"
				);
				//bua test di komen dulu aja
					
				$sql = "
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1, 
						d.deb_wo_date, 
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where  
						d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll !=''
						and d.deb_perm_msg_coll IN ('LP','VLP')
					"
				;
				$qry = $this->db->query( $sql );
				if( $qry && $qry->num_rows() > 0 ) 
					foreach( $qry->result_assoc() as $result_row ) {
						$result_row['CreateTs'] = $this->dateinit;
						// then will get data process 
						if( is_array( $result_row )&& isset( $result_row['deb_wo_date'] )  ) {
							//printf("%s\n",  $result_row['deb_wo_date'] );
							$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
							$dayTotal = (int)$daytot['days_total'];
							if( $dayTotal == 53 ){
								$result_array[$result_row['deb_id']] = (array)$result_row;
							}
						} 
					}
			// return back data customer.
			//  print_r($result_array);
		  	return (array)$result_array;
		}

		/**
		**WODATE - 75
		**/
		function getWOdate75() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll IN ('HP','MP','VHP')
					"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_wo_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where  
					d.deb_mobileno1 !=''
					and d.deb_perm_msg_coll IN ('HP','MP','VHP')
				"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						printf("%s\n",  $result_row['deb_wo_date'] );

						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 75 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
				}
				// return back data customer.
				//  print_r($result_array);
		  	return (array)$result_array;
		}

		/**
		**WODATE - 100
		**/
		function getWOdate100() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll IN ('HP','MP','VHP')
						and d.deb_perm_msg_coll != ''
					"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_wo_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where  
					d.deb_mobileno1 !=''
					and d.deb_perm_msg_coll IN ('HP','MP','VHP')
					and d.deb_perm_msg_coll != ''
				"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						//printf("%s\n",  $result_row['deb_wo_date'] );

						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 100 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
				}
				// return back data customer.
				//  print_r($result_array);
		  	return (array)$result_array;
		}

		/**
		**WODATE - 150
		**/
		function getWOdate150() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll IN ('HP','MP','VHP')
						and d.deb_perm_msg_coll != ''
					"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_wo_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where  
					d.deb_mobileno1 !=''
					and d.deb_perm_msg_coll IN ('HP','MP','VHP')
					and d.deb_perm_msg_coll != ''
				"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						printf("%s\n",  $result_row['deb_wo_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 100 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}

		/**
		**WODATE - 175
		**/
		function getWOdate175() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						month(d.deb_wo_date ) = month(CURRENT_DATE())
						and YEAR(d.deb_wo_date ) = YEAR(CURRENT_DATE())
						AND date(d.deb_wo_date) = curdate() -5
						and d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll IN ('HP','MP','VHP')
						and d.deb_perm_msg_coll != ''
					"
				);
					
			$sql = "
					select 
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1, 
						d.deb_wo_date, 
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where  
						d.deb_mobileno1 !=''
						and d.deb_perm_msg_coll IN ('HP','MP','VHP')
						and d.deb_perm_msg_coll != ''
					"
				;
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_wo_date'] )  ) {
						printf("%s\n",  $result_row['deb_wo_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_wo_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 175 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
		  		}
			// return back data customer.
			//  print_r($result_array);
			return (array)$result_array;
		}

		/**
		**Entri date - 8
		**/
		function getEd8() 
		{
			$result_array = array();

			$sql = sprintf("
					select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					where month(d.deb_entri_date ) = month(CURRENT_DATE())
					and YEAR(d.deb_entri_date ) = YEAR(CURRENT_DATE())
					AND date(d.deb_entri_date) = curdate() -5
					and d.deb_mobileno1 !=''
					and d.deb_entri_date is not null"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_entri_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				where  d.deb_mobileno1 !=''
				and d.deb_entri_date is not null;"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_entri_date'] )  ) {
						printf("%s\n",  $result_row['deb_entri_date'] );
						$daytot = call_user_func_array('_getDateDiff', 
							array(
								$result_row['CreateTs'],  
								$result_row['deb_entri_date']
							)
						);

						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 8 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
		  		}
		    // return back data customer.
			//print_r($result_array);
		    return (array)$result_array;
		}

		/**
		**Entri date - 90
		**/
		function getEd90() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					where month(d.deb_entri_date ) = month(CURRENT_DATE())
					and YEAR(d.deb_entri_date ) = YEAR(CURRENT_DATE())
					AND date(d.deb_entri_date) = curdate() -5
					and d.deb_mobileno1 !=''
					and d.deb_entri_date is not null"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_entri_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				where  d.deb_mobileno1 !=''
				and d.deb_entri_date is not null;"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_entri_date'] )  ) {
						printf("%s\n",  $result_row['deb_entri_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_entri_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 90 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}

		/**
		**Entri date - 150
		**/
		function getEd150() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					where month(d.deb_entri_date ) = month(CURRENT_DATE())
					and YEAR(d.deb_entri_date ) = YEAR(CURRENT_DATE())
					AND date(d.deb_entri_date) = curdate() -5
					and d.deb_mobileno1 !=''
					and d.deb_entri_date is not null"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_entri_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				where  d.deb_mobileno1 !=''
				and d.deb_entri_date is not null;"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_entri_date'] )  ) {
						printf("%s\n",  $result_row['deb_entri_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_entri_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 150 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}

		/**
		**Entri date - 175
		**/
		function getEd175() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					where month(d.deb_entri_date ) = month(CURRENT_DATE())
					and YEAR(d.deb_entri_date ) = YEAR(CURRENT_DATE())
					AND date(d.deb_entri_date) = curdate() -5
					and d.deb_mobileno1 !=''
					and d.deb_entri_date is not null"
				);
					
			$sql = "
				select 
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1, 
					d.deb_entri_date, 
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				where  d.deb_mobileno1 !=''
				and d.deb_entri_date is not null;"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) && isset( $result_row['deb_entri_date'] )  ) {
						printf("%s\n",  $result_row['deb_entri_date'] );
						$daytot = call_user_func_array('_getDateDiff', array($result_row['CreateTs'],  $result_row['deb_entri_date']));
						$dayTotal = (int)$daytot['days_total'];
						if( $dayTotal == 175 ){
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}
					} 
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}
		
		/**
		**Pop 04
		**/
		function getPtpOp04() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						#d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tas.CallReasonDesc,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_lk_account_status tas on tas.CallReasonCode = d.deb_call_status_code
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						d.deb_mobileno1 !=''
						and d.deb_call_status_code='107'
					"
				);		
			$sql = "
				select 
					#d.deb_entri_date,
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1,
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tas.CallReasonDesc,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_lk_account_status tas on tas.CallReasonCode = d.deb_call_status_code
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where 
					d.deb_mobileno1 !=''
					and d.deb_call_status_code='107'
				"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_array[$result_row['deb_id']] = (array)$result_row;
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}

		/**
		** Pop 25
		**/
		function getPtpOp25() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tas.CallReasonDesc,
						d.deb_call_status_code,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_lk_account_status tas on tas.CallReasonCode = d.deb_call_status_code
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						tas.CallReasonDesc = 'PTP-POP' 
						and d.deb_call_status_code ='107'
						and d.deb_mobileno1 !=''
					"
				);
					
			$sql = "
				select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tas.CallReasonDesc,
						d.deb_call_status_code,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_lk_account_status tas on tas.CallReasonCode = d.deb_call_status_code
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
						tas.CallReasonDesc = 'PTP-POP' 
						and d.deb_call_status_code ='107'
						and d.deb_mobileno1 !=''
				"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_array[$result_row['deb_id']] = (array)$result_row;
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}

		/**
		** Bp 04
		**/
		function getPtpbp04() 
		{
			$result_array = array();
			$sql = sprintf("
					select 
						d.deb_entri_date,
						d.deb_id,
						d.deb_acct_no,
						d.deb_mobileno1,
						d.deb_call_activity_datets,
						date(d.deb_call_activity_datets) as UpdateTs,
						d.deb_agent,
						si.AssignAdmin,
						si.AssignAmgr,
						si.AssignMgr,
						si.AssignSpv,
						si.AssignLeader,
						si.AssignSelerId,
						tas.CallReasonDesc,
						d.deb_call_status_code,
						tgf.PhoneNumber
					from t_gn_debitur d
					left join t_gn_assignment si on d.deb_id=si.CustomerId
					left join t_lk_account_status tas on tas.CallReasonCode = d.deb_call_status_code
					left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
					where 
					    d.deb_mobileno1 !=''
						and d.deb_call_status_code = '108'
					"
				);
					
			$sql = "
				select 
					d.deb_entri_date,
					d.deb_id,
					d.deb_acct_no,
					d.deb_mobileno1,
					d.deb_call_activity_datets,
					date(d.deb_call_activity_datets) as UpdateTs,
					d.deb_agent,
					si.AssignAdmin,
					si.AssignAmgr,
					si.AssignMgr,
					si.AssignSpv,
					si.AssignLeader,
					si.AssignSelerId,
					tas.CallReasonDesc,
					d.deb_call_status_code,
					tgf.PhoneNumber
				from t_gn_debitur d
				left join t_gn_assignment si on d.deb_id=si.CustomerId
				left join t_lk_account_status tas on tas.CallReasonCode = d.deb_call_status_code
				left join t_gn_validphone tgf on tgf.DebiturId = d.deb_id
				where 
				    d.deb_mobileno1 !=''
					and d.deb_call_status_code = '108'
				"
			;
					
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_array[$result_row['deb_id']] = (array)$result_row;
		  		}
		    // return back data customer.
			//  print_r($result_array);
		    return (array)$result_array;
		}

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function getrow( $disposition = null, $row = null  )
		{
			 
		 	$result_array = array();
		 	$sql= sprintf("
		 			select  
		 				a.deb_id, 
		 				a.deb_acct_no, 
		 				a.deb_mobileno1, 
						a.deb_call_activity_datets,
						date(a.deb_call_activity_datets) as UpdateTs,
						a.deb_agent, 
						b.AssignAdmin, 
						b.AssignAmgr, 
						b.AssignMgr, 
						b.AssignSpv, 
						b.AssignLeader,  
						b.AssignSelerId 
					from t_gn_debitur a 
					left join t_gn_assignment b on a.deb_id=b.CustomerId
					where a.deb_call_status_code ='%s'", $disposition
				);
				//printf("%s\n\r", $sql);
			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) and isset( $result_row['UpdateTs'] )  ){
						// get dat dif date on last update 
						$daytot = call_user_func_array('_getDateDiff',array( $result_row['UpdateTs'], $result_row['CreateTs']));	
						$dayTotal = 0;			
						$dayTotal = ( isset($daytot['days_total']) ?  $daytot['days_total'] : 0 );	
						// jika perbedaan data process sesuai setup
						$dayOfAccount = $result_row['deb_acct_no'];
				
						if( $dayTotal > 0 and $dayTotal < 10 ) {
							printf("find account : <%s> sys : %s <compare> data : %s\n\r", 
								$dayOfAccount,
								$row->SRV_Data_Priority, 
								$dayTotal
							);
						}
				
						if( $dayTotal == $row->SRV_Data_Priority ){
							printf("success account : <%s> sys : %s <compare> data : %s\n\r", 
								$dayOfAccount, 
								$row->SRV_Data_Priority, 
								$dayTotal
							);

							$result_array[$result_row['deb_id']] = (array)$result_row;
						}						
					} 
		  		}
				// return back data customer.
		  	return (array)$result_array;
		}
 
		 /*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function validphone( $ID = 0 )
		{
			
			$result_array = array();
			$sql = sprintf("
					select  
						a.ValidID, 
						a.PhoneNumber 
					from t_gn_validphone a  
					where a.DebiturId ='%s'", $ID 
				);

			$qry = $this->db->query( $sql );
			if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $row ){
					$result_array[$row['PhoneNumber']] = $row['PhoneNumber'];
				}
			// return back data process OK 
			return (array)$result_array;
		} 

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function getvalid( $disposition = null, $row = null  )
		{
			 
		 	$result_array = array();
			$sql= sprintf("
		 		select  
		 			a.deb_id, 
		 			a.deb_acct_no, 
		 			a.deb_mobileno1, 
					a.deb_call_activity_datets,
					date(a.deb_call_activity_datets) as UpdateTs,
					a.deb_agent, 
					b.AssignAdmin, 
					b.AssignAmgr, 
					b.AssignMgr, 
					b.AssignSpv, 
					b.AssignLeader,  
					b.AssignSelerId 
				from t_gn_debitur a 
				left join t_gn_assignment b on a.deb_id=b.CustomerId
				where a.deb_call_status_code ='%s'", $disposition
			);
			//printf("%s\n\r", $sql);
		 	$qry = $this->db->query( $sql );
		 	if( $qry && $qry->num_rows() > 0 ) 
				foreach( $qry->result_assoc() as $result_row ) {
					$result_row['CreateTs'] = $this->dateinit;
					// then will get data process 
					if( is_array( $result_row ) and isset( $result_row['UpdateTs'] )  ){
						// get dat dif date on last update 
						$daytot = call_user_func_array('_getDateDiff', array( $result_row['UpdateTs'], $result_row['CreateTs']));	
						$days_total = 0;			
						$dayTotal = ( isset($daytot['days_total']) ? $daytot['days_total'] : 0 );	
						
						// jika perbedaan data process sesuai setup
						$dayOfAccount = $result_row['deb_acct_no'];
						if( $dayTotal > 0 and $dayTotal < 10 ) {
							printf("find account : <%s> sys : %s <compare> data : %s\n\r", $dayOfAccount,  $row->SRV_Data_Priority,  $dayTotal);
						}

						if( $dayTotal == $row->SRV_Data_Priority ){
							
							printf("success account : <%s> sys : %s <compare> data : %s\n\r", 
								$dayOfAccount,  
								$row->SRV_Data_Priority,  
								$dayTotal
							);
									
							// kmudian check data no telpon nya jika 
							// tidak ada di table valid ambil dari yang ini saja 
							$validphone = null;
							$DebiturId = ( isset( $result_row['deb_id'] ) ? $result_row['deb_id'] : 0 );
							$validphone = @call_user_func_array( 
								array($this, 'validphone'), 
								array( $DebiturId )
							);

							// jika data berisi no telpon yang valid maka masukan no valid tersebut 
							// untuk di kirim sebagai No yang akan SMS 
							if( is_array( $validphone) && count($validphone) > 0 ){
								// ambil yang terakhir dari deret yang tersedia.
								$result_row['deb_mobileno1'] = end($validphone);
							}
							
							$result_array[$result_row['deb_id']] = (array)$result_row;
						}						
					} 
		  		}
		 	// return back data customer.
			return (array)$result_array;
		}

		//get wodate
		function WOdate5( $fld = null, $context = null )
		{
		 
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
		 
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 	$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
		 	if( !$startrow ){
				return false; 
		 	}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
		  	$this->getrow = null;
		  	$this->getrow = @call_user_func_array( 
		  		array( $this, 'getWOdate5' ), 
				array( $fld->SRV_Data_Status, $fld)
			); 
												
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
		 		foreach( $this->getrow as $result_row) {
					 // counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					print_r($row);
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2 ),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}

					// if( strcmp( substr($row->PhoneNumber,0,2 ),   '08' )){
					// 	@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					// 	continue;
					// }
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 
					$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;
					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$no_is_valid);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
					// "insert" t_gn_sms_approval
					// ini tadi masi di remark jadi gak insert ..
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
					//echo mysql_error();
		  		}
		 		// update untuk nex process disfer.
			@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
			return $totalProcess;
		}

		/**
		wodate20
		**/
		function WOdate20( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
		 
			// update start process flags  = 1 agara tidak 
			// melooping berulang
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
		  	$this->getrow = null;
		 	$this->getrow = @call_user_func_array( 
		 		array( $this, 'getWOdate20' ), 
		 		array( $fld->SRV_Data_Status, $fld)
		 	); 
												
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
			if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
					 // counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(
							array($this, 'endrow'), 
							array( $fld->SRV_Data_Id )
						);
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					print_r($row);
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 

					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 
					$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
			
					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;
					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$no_is_valid);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
					// "insert" t_gn_sms_approval
					// ini tadi masi di remark jadi gak insert ..
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
					//echo mysql_error();
		  		}
			 	// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		WODATE 30
		**/
		function WOdate30( $fld = null, $context = null )
		{
			echo "Start WoDate 30 Proccess";
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
			$this->getrow = null;
			$this->getrow = @call_user_func_array( 
				array( $this, 'getWOdate30' ), 
			    array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
			if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
					// counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					print_r($row);
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
			
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 
					$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;

					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;

					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$no_is_valid);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
					// "insert" t_gn_sms_approval
					// ini tadi masi di remark jadi gak insert ..
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
					echo mysql_error();
		  		}
				echo "TOTAL SMS YG AKAN DIKIRIM: ".$totalProcess;
		 		// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		WODATE 40
		**/
		function WOdate40( $fld = null, $context = null )
		{
		 
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
			$this->getrow = null;
			$this->getrow = @call_user_func_array( 
				array( $this, 'getWOdate40' ), 
			    array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
			if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
					// counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					print_r($row);
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
			
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 
					$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;

					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;

					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$no_is_valid);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
					// "insert" t_gn_sms_approval
					// ini tadi masi di remark jadi gak insert ..
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
					//echo mysql_error();
		  		}
		 		// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		wodate53
		**/
		function WOdate53( $fld = null, $context = null )
		{
		 
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
			$this->getrow = null;
			$this->getrow = @call_user_func_array( 
				array( $this, 'getWOdate53' ), 
			    array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
				// counting data untuk tiap rows process OK .
				if( !count($result_row) ){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}	
				// konvert data to object ;
				$row = (object)$result_row;
				print_r($row);
				if( !is_object( $row ) ){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}
			
				// check validasi no telpon mobile atau bukan 
				// jika prefix bukan '<08>' 
				if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}
				// then will get set process OK  then excep process  on here pages	
				// then will get set process OK  then excep process  on here pages 	
				$row->SmsCreateTs = date('Y-m-d H:i:s');
				$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
				$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
				$row->SmsUpdateTs = $row->SmsCreateTs;
				$row->SmsApprovalStatus = $this->statusinit; 
				$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
				// untuk process nya adalah  sebagai berikut;
				// untuk process nya adalah  sebagai berikut;

				$this->db->reset_write();
				$this->db->set('MasterId', 			$row->deb_id);
				$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
				$this->db->set('SmsDestination', 	$no_is_valid);
				$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
				$this->db->set('SmsLeaderId', 		$row->AssignLeader);
				$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
				$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
				$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
				$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
				$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
				$this->db->set('SmsLocation', 		_getIP());
				$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
				// "insert" t_gn_sms_approval
				// ini tadi masi di remark jadi gak insert ..
				if( $this->db->insert('t_gn_sms_approval') ){
					$totalProcess++;
				}
				//echo mysql_error();
			}
			// update untuk nex process disfer.
			@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
			return $totalProcess;
		}

		/**
		wodate75
		**/
		function WOdate75( $fld = null, $context = null )
		{
		 
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
			$this->getrow = null;
		  	$this->getrow = @call_user_func_array( 
		  		array( $this, 'getWOdate75' ), 
			    array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
				// counting data untuk tiap rows process OK .
				if( !count($result_row) ){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}	
			
				// konvert data to object ;
				$row = (object)$result_row;
				print_r($row);
				if( !is_object( $row ) ){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}
				// check validasi no telpon mobile atau bukan 
				// jika prefix bukan '<08>' 
				if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}
			
				// then will get set process OK  then excep process  on here pages	
				// then will get set process OK  then excep process  on here pages 	
				$row->SmsCreateTs = date('Y-m-d H:i:s');
				$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
				$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
				$row->SmsUpdateTs = $row->SmsCreateTs;
				$row->SmsApprovalStatus = $this->statusinit; 
				$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
				// untuk process nya adalah  sebagai berikut;
				// untuk process nya adalah  sebagai berikut;

				$this->db->reset_write();
				$this->db->set('MasterId', 			$row->deb_id);
				$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
				$this->db->set('SmsDestination', 	$row->no_is_valid);
				$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
				$this->db->set('SmsLeaderId', 		$row->AssignLeader);
				$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
				$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
				$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
				$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
				$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
				$this->db->set('SmsLocation', 		_getIP());
				$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
				// "insert" t_gn_sms_approval
				// ini tadi masi di remark jadi gak insert ..
				if( $this->db->insert('t_gn_sms_approval') ){
					$totalProcess++;
				}
			
				//echo mysql_error();
		  	}
		  
		 	// update untuk nex process disfer.
			@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
			return $totalProcess;
		}

		/**
		wodate100
		**/
		function WOdate100( $fld = null, $context = null )
		{
		 
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
			$this->getrow = null;
		  	$this->getrow = @call_user_func_array( 
		  		array( $this, 'getWOdate100' ), 
			    array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
				// counting data untuk tiap rows process OK .
				if( !count($result_row) ){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}	
			
				// konvert data to object ;
				$row = (object)$result_row;
				print_r($row);
				if( !is_object( $row ) ){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}
				// check validasi no telpon mobile atau bukan 
				// jika prefix bukan '<08>' 
				if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
					@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
					continue;
				}
			
				// then will get set process OK  then excep process  on here pages	
				// then will get set process OK  then excep process  on here pages 	
				$row->SmsCreateTs = date('Y-m-d H:i:s');
				$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
				$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
				$row->SmsUpdateTs = $row->SmsCreateTs;
				$row->SmsApprovalStatus = $this->statusinit;
				$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1; 
				// untuk process nya adalah  sebagai berikut;
				// untuk process nya adalah  sebagai berikut;

				$this->db->reset_write();
				$this->db->set('MasterId', 			$row->deb_id);
				$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
				$this->db->set('SmsDestination', 	$no_is_valid);
				$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
				$this->db->set('SmsLeaderId', 		$row->AssignLeader);
				$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
				$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
				$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
				$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
				$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
				$this->db->set('SmsLocation', 		_getIP());
				$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
				// "insert" t_gn_sms_approval
				// ini tadi masi di remark jadi gak insert ..
				if( $this->db->insert('t_gn_sms_approval') ){
					$totalProcess++;
				}
			
				//echo mysql_error();
		  	}
		  
		 	// update untuk nex process disfer.
			@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
			return $totalProcess;
		}

		/**
		wodate150
		**/
		function WOdate150( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
		 
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 	$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
		 	if( !$startrow ){
				return false; 
		 	}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
		 	$this->getrow = null;
		  	$this->getrow = @call_user_func_array( 
		  		array( $this, 'getWOdate150' ), 
				array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
			 	foreach( $this->getrow as $result_row) {
					// counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					print_r($row);
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit;
					$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1; 
			
					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;

					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$no_is_valid);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
					// "insert" t_gn_sms_approval
					// ini tadi masi di remark jadi gak insert ..
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
					//echo mysql_error();
		  		}
		 		// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
			return $totalProcess;
		}

		/**
		wodate175
		**/
		function WOdate175( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getWOdate175' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$no_is_valid);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Entri Date 8
		**/
		function Ed8( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
			$startrow = @call_user_func_array( 
				array( $this, 'startrow' ), 
				array( $fld->SRV_Data_Id ) 
			);

			if( !$startrow ){
				return false; 
			}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getEd8' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'endrow'), 
								array( $fld->SRV_Data_Id )
							);
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'endrow'), 
								array( $fld->SRV_Data_Id )
							);
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'endrow'), 
								array( $fld->SRV_Data_Id )
							);
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$row->deb_mobileno1);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Entri Date 90
		**/
		function Ed90( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getEd90' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$row->deb_mobileno1);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Entri Date 150
		**/
		function Ed150( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getEd150' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$row->deb_mobileno1);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Entri Date 175
		**/
		function Ed175( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getEd175' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$row->deb_mobileno1);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Pop 4
		**/
		function Pop4( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getPtpOp04' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$no_is_valid);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Pop 25
		**/
		function Pop25( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getPtpOp25' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$no_is_valid);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/**
		Bp 04
		**/
		function Bp04( $fld = null, $context = null )
		{
		 
		 	$totalProcess = 0;	
			// jika null data return false;	
		 	if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
				// ambilk detail berdasarkan kondisi data process 
				// status datanya .
			  	$this->getrow = null;
			  	$this->getrow = @call_user_func_array( 
			  		array( $this, 'getPtpbp04' ), 
					array( $fld->SRV_Data_Status, $fld)
				); 
				// data sms yang di kirim tidak memerlukan process approval akan tetapi 
				// agar process berjalan normal maka approval akan di  bypass 
				// oleh system.
		 		if( is_array( $this->getrow ) ) 
					foreach( $this->getrow as $result_row) {
						// counting data untuk tiap rows process OK .
						if( !count($result_row) ){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}	
						// konvert data to object ;
						$row = (object)$result_row;
						print_r($row);
						if( !is_object( $row ) ){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}
						// check validasi no telpon mobile atau bukan 
						// jika prefix bukan '<08>' 
						if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
							@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
							continue;
						}
			
					 	// then will get set process OK  then excep process  on here pages	
					 	// then will get set process OK  then excep process  on here pages 	
						$row->SmsCreateTs = date('Y-m-d H:i:s');
						$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
						$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
						$row->SmsUpdateTs = $row->SmsCreateTs;
						$row->SmsApprovalStatus = $this->statusinit; 
						$no_is_valid = ($row->PhoneNumber !="" OR $row->PhoneNumber !=null) ? $row->PhoneNumber : $row->deb_mobileno1;
						// untuk process nya adalah  sebagai berikut;
						// untuk process nya adalah  sebagai berikut;
						$this->db->reset_write();
						$this->db->set('MasterId', 			$row->deb_id);
						$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
						$this->db->set('SmsDestination', 	$no_is_valid);
						$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsLeaderId', 		$row->AssignLeader);
						$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
						$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
						$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
						$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
						$this->db->set('SmsLocation', 		_getIP());
						$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
						// "insert" t_gn_sms_approval
						// ini tadi masi di remark jadi gak insert ..
						if( $this->db->insert('t_gn_sms_approval') ){
							$totalProcess++;
						}
						//echo mysql_error();
		  			}
		 			// update untuk nex process disfer.
				@call_user_func_array(array($this, 'next_month_new'), array( $fld->SRV_Data_Id ));
				return $totalProcess;
		}

		/*
		 *
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function perbulan( $fld = null, $context = null )
		{
			  
			// var_dump($fld);
		 	$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
			if( !$startrow ){
				return false; 
			}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
		  	$this->getrow = null;
		  	$this->getrow = @call_user_func_array( 
		  		array( $this, 'getbulan' ), 
			    array( $fld->SRV_Data_Status, $fld)
			); 
		 	// print_r($this->getrow);
			// exit;

			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
					// counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'nextmonth'), array( $fld->SRV_Data_Id,  $fld->SRV_Data_SentDate));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'nextmonth'), array( $fld->SRV_Data_Id, $fld->SRV_Data_SentDate ));
						continue;
					}
			
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'nextmonth'), array( $fld->SRV_Data_Id, $fld->SRV_Data_SentDate));
						continue;
					}
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 

					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;
					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$row->deb_mobileno1);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
					
					// "insert" t_gn_sms_approval
					// ini tadi masi di remark jadi gak insert ..
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
		 		}
				// update untuk nex process disfer.
				@call_user_func_array(array($this, 'nextmonth'), array( $fld->SRV_Data_Id, $fld->SRV_Data_SentDate));
				// return back process data OK 
				return $totalProcess;
		}

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function validation( $fld = null, $context = null  )
		{
			$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
			}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
			$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
				if( !$startrow ){
					return false; 
				}
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
			$this->getvalid = null;
		  	$this->getvalid = @call_user_func_array( 
		  		array( $this, 'getvalid' ), 
				array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
				foreach( $this->getrow as $result_row) {
					// counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}

					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 
			
					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;

					$this->db->reset_write();
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$row->deb_mobileno1);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
					// "insert" t_gn_sms_approval
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
		  		}
		 		// update untuk nex process disfer.
		  		@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
		  		// return back process data OK 
			  	return $totalProcess;
		} 

		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function defaults( $fld = null, $context = null  )
		{
			  
		 	$totalProcess = 0;	
			// jika null data return false;	
			if( is_null( $fld ) ){
				return false;
		 	}
			// update start process flags  = 1 agara tidak 
			// melooping berulang
		 	$startrow = @call_user_func_array( array( $this, 'startrow' ), array( $fld->SRV_Data_Id ) );	
		 	if( !$startrow ){
				return false; 
		 	}
		 
			// ambilk detail berdasarkan kondisi data process 
			// status datanya .
		  	$this->getrow = null;
		  	$this->getrow = @call_user_func_array( 
		  		array( $this, 'getrow' ), 
				array( $fld->SRV_Data_Status, $fld)
			); 
			// data sms yang di kirim tidak memerlukan process approval akan tetapi 
			// agar process berjalan normal maka approval akan di  bypass 
			// oleh system.
		 	if( is_array( $this->getrow ) ) 
			 	foreach( $this->getrow as $result_row) {
					// counting data untuk tiap rows process OK .
					if( !count($result_row) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}	
					// konvert data to object ;
					$row = (object)$result_row;
					if( !is_object( $row ) ){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// check validasi no telpon mobile atau bukan 
					// jika prefix bukan '<08>' 
					if( strcmp( substr($row->deb_mobileno1,0,2),   '08' )){
						@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
						continue;
					}
					// then will get set process OK  then excep process  on here pages	
					// then will get set process OK  then excep process  on here pages 	
					$row->SmsCreateTs = date('Y-m-d H:i:s');
					$row->SmsTemplateId = $fld->SRV_Data_TemplateId;
					$row->SmsTextMessage = $fld->SERV_TemplateMessage; 
					$row->SmsUpdateTs = $row->SmsCreateTs;
					$row->SmsApprovalStatus = $this->statusinit; 
					// untuk process nya adalah  sebagai berikut;
					// untuk process nya adalah  sebagai berikut;
					$this->db->reset_write();
					$this->db->set('SmsLocation', 		_getIP());
					$this->db->set('MasterId', 			$row->deb_id);
					$this->db->set('SmsTemplateId', 	$row->SmsTemplateId); 
					$this->db->set('SmsDestination', 	$row->deb_mobileno1);
					$this->db->set('SmsCreateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsLeaderId', 		$row->AssignLeader);
					$this->db->set('SmsSupervisorId', 	$row->AssignSpv); 
					$this->db->set('SmsTextMessage', 	$row->SmsTextMessage);
					$this->db->set('SmsUpdateUserId', 	$row->AssignSelerId);
					$this->db->set('SmsCreateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsUpdateTs', 		$row->SmsCreateTs);
					$this->db->set('SmsApprovalStatus', $row->SmsApprovalStatus);
			
					// "insert" t_gn_sms_approval
					if( $this->db->insert('t_gn_sms_approval') ){
						$totalProcess++;
					}
		  		}
				// update untuk nex process disfer.
				@call_user_func_array(array($this, 'endrow'), array( $fld->SRV_Data_Id ));
				// return back process data OK 
				return $totalProcess;
		}

	 
		/*
		 * [SMS BLAST upload HSBC COLLECTION STB]
		 * @param  [type] $CustomerId [description]
		 * @return [type]             [description]
		 */
		function process()
		{ 
		 	//DEFINE date today
		 	$today = date('d');
		 	$day   = date('l');
		 
			$result_array = $this->passtru(); 
			print_r($row->result_array);

			// jika ketika di check tidak ada jadwal yang aktive maka 
			// di return 'false ' saja 
			 if( !$result_array ){
				exit( "no schedule active.\n\r" );
			}

			// kemudian jika schedule berisi datanya dan ada 
			// lakukan process berikut ini.
			if( is_array( $result_array ) )  
				foreach( $result_array as $key => $row ){
				// then if row "OK"  konvert to object 
				$row = (object)$row;
				if(!is_object( $row ) ){
					printf("%s\n", "please check object data process.");
					continue;
				} 
				
				//print_r($row);exit;
				// test data OK 	
				//	$row->SRV_Data_Context = 'perbulan';

				// jika data type adalah object maka process lanjutkan ke step 
				// berikutnya dengan check data jam nya .
				
			// sementara dibypass
				printf("time sistem : %s => time schedule : %s\n\r",$this->timerinit, $row->SRV_Data_SentTime);
				if( strcmp( $row->SRV_Data_SentTime,  $this->timerinit) ){
					printf("%s\n", "time process not a schedule.");
					continue;
				} 	 
				echo $row->SRV_Data_Context;
				// jika jam server dan jam schedule sama maka ambil source datanya 
				// kemudain update process =1  untuk process data Default adalah process 
				// ambil no telpon dengan cara yang simple langsung ambil no telpon mobile  pada tiap2 tiap account;
				if( !strcmp( $row->SRV_Data_Context, 'default') ){
					$this->callProccess = @call_user_func_array( 
						array( $this, 'defaults'), 
						array( $row, null )
					);
				} 
			
				// kshusus untuk data selain default seperti data "VALID"
				if( !strcmp( $row->SRV_Data_Context, 'valid') ){
					$this->callProccess = @call_user_func_array( 
						array($this, 'validation'),  
						array($row, null )
					);
				}

				// kalau ada process yang lain tambaahin dulu fungsi nya di sini 
				// misala 
				if( !strcmp( $row->SRV_Data_Context, 'perbulan') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'perbulan'),  
							array($row, null )
						);
					}
				}
			
				// get process wo date.
				// var_dump($row->SRV_Data_Context);
				if( !strcmp( $row->SRV_Data_Context, 'wodate5') ) {
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d') ) ) {
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate5'),  
							array($row, null )
						);
					}
				}

			
				if( !strcmp( $row->SRV_Data_Context, 'wodate20') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate20'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'wodate30') ){
					echo "// untuk data perbulan harus di check tglnya wo 30 ";
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
					// if( !strcmp(  $row->SRV_Data_SentDate, '2019-12-06')){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate30'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'wodate40') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate40'),  
							array($row, null )
						);
					}
				}
			
				if( !strcmp( $row->SRV_Data_Context, 'wodate53') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate53'),  
							array($row, null )
						);
					}
				}
			
				if( !strcmp( $row->SRV_Data_Context, 'wodate75') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate75'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'wodate100') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate100'),  
							array($row, null )
						);
					}
				}
			
				if( !strcmp( $row->SRV_Data_Context, 'wodate150') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate150'),  
							array($row, null )
						);
					}
				}
			
				if( !strcmp( $row->SRV_Data_Context, 'wodate175') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'WOdate175'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'ed8') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'Ed8'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'ed90') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'Ed90'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'ed150') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'Ed150'),  
							array($row, null )
						);
					}
				}

				if( !strcmp( $row->SRV_Data_Context, 'ed175') ){
					// untuk data perbulan harus di check tglnya 
					if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
						// akan di kirim tiap tgl yang di setting 
						$this->callProccess = @call_user_func_array( 
							array($this, 'Ed175'),  
							array($row, null )
						);
					}
				}

				//ini di kirim setiap tanggal 04
				//untuk sementara di set tanggal hari ini 
				//untuk UAT
				if($today == '04') {
					if( !strcmp( $row->SRV_Data_Context, 'pop4') )
						// untuk data perbulan harus di check tglnya 
						if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
							// akan di kirim tiap tgl yang di setting 
							$this->callProccess = @call_user_func_array( 
								array($this, 'Pop4'),  
								array($row, null )
							);
						}
				}

				//ini di kirim setiap tanggal 25
				//untuk sementara di set tanggal hari ini 
				//untuk UAT
				if($today == '25') {
					if( !strcmp( $row->SRV_Data_Context, 'pop25') )
						// untuk data perbulan harus di check tglnya 
						if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
							// akan di kirim tiap tgl yang di setting 
							$this->callProccess = @call_user_func_array( 
								array($this, 'Pop25'),  
								array($row, null )
							);
						}
				}

				//ini di kirim setiap tanggal 04
				//untuk sementara di set tanggal hari ini 
				//untuk UAT
				if($today == '04') {
					if( !strcmp( $row->SRV_Data_Context, 'bp4') )
						// untuk data perbulan harus di check tglnya 
						if( !strcmp(  $row->SRV_Data_SentDate, date('Y-m-d'))){
							// akan di kirim tiap tgl yang di setting 
							$this->callProccess = @call_user_func_array( 
								array($this, 'Bp04'),  
								array($row, null )
							);
						}
				}
		 	}
			// ended process 	 
			printf("%s\n\r", "done");
		}
	}
?>
