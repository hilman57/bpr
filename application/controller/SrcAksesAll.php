<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SrcAksesAll extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function SrcAksesAll() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
	$this->load->helper(array('EUI_Object'));
		
 }

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
	 'GenderId' => $this->{base_class_model($this)}->_getGenderId(),
	 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
	 'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus(),
	 'LastCallStatus' => $this->{base_class_model($this)}->_getLastCallStatus() );
	 
   if(is_array($_EUI)) {
	 $this -> load ->view('src_view_aksesall/view_aksesall_nav',$_EUI);
   }	
}
	
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('src_view_aksesall/view_aksesall_list',$_EUI);
	 }	
  }	
}

	/*
	* @ def 		: content / default pages controller 
	* -----------------------------------------
	*
	* @ params  	: post & definition paymode 
	* @ return 	: void(0)
	*/
	public function CheckAccountAccess()
	{
		$_conds = array ('Success'=> 0);
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			if($this ->{base_class_model($this)}->CheckAccessAll())
			{
				$_conds = array ('Success'=> 1);
			}
			// echo $this->db->last_query();
		}
		echo json_encode($_conds);

	}
	
	/*
	* @ def 		: content / default pages controller 
	* -----------------------------------------
	*
	* @ params  	: post & definition paymode 
	* @ return 	: void(0)
	*/
	public function RemoveKeys()
	{
		$_conds = array ('Success'=> 0);
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			if($this ->{base_class_model($this)}->UpdateAccessKeys())
			{
				$_conds = array ('Success'=> 1);
			}
		}
		echo json_encode($_conds);
	}
	
	// ---------------- ditunggu ya ----------
	
	 function DefaultRemoveKeys()
	{
		$this->{base_class_model($this)}->_DefaultRemoveKeys();
	}
	
}

// END OF CLASS 

?>