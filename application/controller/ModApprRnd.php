<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class ModApprRnd extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function ModApprRnd() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
		
 }

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
	 'GenderId' => $this->{base_class_model($this)}->_getGenderId(),
	 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
	 'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus(),
	 'LastCallStatus' => $this->{base_class_model($this)}->_getLastCallStatus() );
	 
   if(is_array($_EUI)) {
	 $this -> load ->view('mod_approval_random/view_appr_claim_nav',$_EUI);
   }	
}
	
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('mod_approval_random/view_appr_claim_list',$_EUI);
	}	
  }	
}

	/*
	 * @ def 		: content / default pages controller 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
 
	public function ApproveRandom() 
	{
		$_conds['Message']= 0;
		$_conds['success'] = 0;
		$_conds['fail'] = 0;
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$_conds = $this ->{base_class_model($this)}->approveRandom();
		}
		echo json_encode($_conds);
	}
	
	/*
	 * @ def 		: content / default pages controller 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
 
	public function RejectRandom() 
	{
		$_conds['Message']= 0;
		$_conds['success'] = 0;
		$_conds['fail'] = 0;
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{	
			
			// $ApproveCode = _get_post('ApprovalStatus');
			// $DebiturId = $this ->URI->_get_array_post('CustomerId');
			// if( is_array($DebiturId) )
			// {	
				// foreach($DebiturId as $index=> $deb_id)
				// {
					// if( $this ->{base_class_model($this)}->UpdateRejectStatus($deb_id,$ApproveCode))
					// {
						// $_conds['success']++;
						// $_conds['Message']=1;
					// }
					// else
					// {
						// $_conds['fail']++;
						// $_conds['Message']=1;
					// }
				// }
			// }
			$_conds = $this ->{base_class_model($this)}->rejectRandom();
		}
		echo json_encode($_conds);
	}

}

// END OF CLASS 

?>