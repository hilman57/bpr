<?php
/*
 * EUI Controller  
 *
 
 *@Section  : CallReason
 *@author 	: Omens  
 *@link		: http://www.razakitechnology.com/eui/controller 
 */
 
class CallReason extends EUI_Controller {
	
/**
 *@ constructor 
 *@ or aksesor class of trigers 
 **/
 
function CallReason()
{ 
	parent::__construct();
	$this -> load -> Model("M_CallReason");
}

/**
 * @ get store of the reaon 
 * @ return < array >;
 **/
 
 function store() 
 {
	$_datas = array();
	if( is_object( $this -> M_CallReason ) ) 
	{
		$datas = $this -> M_CallReason -> _get_CallReason();
	}
  
	echo json_encode($datas);
 }
}

//* END OF FILE 
//* location : /application/controller/ CallReason.php

?>