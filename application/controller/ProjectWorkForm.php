<?php
/*
 * E.U.I 
 *
 
 * subject	: ProductForm
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/ProductForm/
 */
 
class ProjectWorkForm extends EUI_Controller
{

private static $PolicyId = null;
private static $CustomerId = null;
private static $ViewLayout = null;

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : null
 * @ author : razaki team
 * 
 */
 
public function ProjectWorkForm() 
{
	parent::__construct();
	
	/* initialize load model **/
 	
	$this->load->model(array( base_class_model($this), 'M_Combo'));
	
	if( is_null(self::$PolicyId)) {
		self::$PolicyId  = $this->URI->_get_post('PolicyId');
	}
	
	if( is_null(self::$CustomerId)) {
		self::$CustomerId  = $this->URI->_get_post('CustomerId');
	}
	
	if( is_null(self::$ViewLayout)) {
		self::$ViewLayout  = $this->URI->_get_post('ViewLayout');
	}
} 

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
public function _getAttributes()
{	
	$conds = null;
	if( self::$PolicyId ) {
		$conds =  $this->{base_class_model($this)}->_getAttributes(self::$PolicyId, self::$CustomerId);
	}
	// print_r($conds);
	return $conds;
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
public function getPolicyHistory()
{	
	$conds = null;
	if( self::$PolicyId ) {
		$conds =  $this->{base_class_model($this)}->_getPolicyHistory(self::$PolicyId, self::$CustomerId);
	}
	
	return $conds;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
public function PolicyDataByPolicyId()
{
	$conds = array();
	if( $DataByUploadId  = $this->{base_class_model($this)}->_getPolicyHistory($this->URI->_get_post('PolicyId'), $this->URI->_get_post('CustomerId')) ) {
		$conds = $DataByUploadId;
	}
	
	echo json_encode($conds);
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function PolicyFollowByPolicyId()
{
	$conds = array();
	if( $DataByUploadId  = $this->{base_class_model($this)}->_getPolicyFollowByPolicyId($this->URI->_get_post('PolicyId'), $this->URI->_get_post('CustomerId')) ) {
		$conds = $DataByUploadId;
	}
	
	echo json_encode($conds);
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function getJavascript()
{
	$_arrs_js = '';
	$_arrs =& self::_getAttributes();
	
	if( !is_null($_arrs)) 
	{
		if( $arr_forms = $this->{base_class_model($this)}->_getWorkForm($_arrs['ProjectCode'])){
			$_arrs_js = $arr_forms['UrlViewJs'];
		}
	}
	
	return $_arrs_js;
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
public function index()
{
	if( $this -> URI->_get_have_post('PolicyId') 
		AND $this -> EUI_Session->_have_get_session('UserId')) 
	{
		switch(self::$ViewLayout)
		{
			case 'ADD_FORM': 
				$this->FormAddLayout();  
			break;
			
			case 'EDIT_FORM': 
				$this->FormEditLayout(); 
			break;
			
			case 'VIEW_FORM': 
				$this->FormViewLayout();  
			break;
		}
	}
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
 
function getCombo()
{	
	$_serialize = array();
	$_combo = $this -> M_Combo -> _getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((strtolower($keys)!='serialize') AND (strtolower($keys)!='instance') 
			AND (strtolower($keys)!='nstruct') AND (strtolower($keys)!='t'))
		{
			$_serialize[$keys] = $this->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function getFollowUpGroup( $ProjectId = 0 )
{
	return $this->{base_class_model($this)}->_getFollowUpGroup($ProjectId); 
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
public function FormAddLayout()
{
 $_arrs  = & self::_getAttributes();
 if( !is_null(self::$ViewLayout) AND !empty(self::$ViewLayout) ) 
 {
	if( $_arrs_layout = $this->{base_class_model($this)}->_getWorkForm($_arrs['ProjectCode']) )
	{
		$arr_forms['form'] 			= $_arrs_layout['AddView'];
		$arr_forms['JavaScript'] 	= $_arrs_layout['UrlViewJs'];
		$arr_forms['PolicyId'] 		= self::$PolicyId;
		$arr_forms['PolicyData'] 	= $this ->getPolicyHistory();
		$arr_forms['Combo'] 		= $this->getCombo();
		$arr_forms['ProjectId'] 	= $_arrs['ProjectId'];
		$arr_forms['FollowupGroup'] = $this->getFollowUpGroup($_arrs['ProjectId']);
		$arr_forms['FollowUpData']	= $this->{base_class_model($this)}->_getFollowUpExist($arr_forms['PolicyData']);
		$arr_forms['Province'] 		= $this->M_Combo->_getProvince();
		$this -> load -> form('add_form/'. $_arrs_layout['AddView'] .'/form_content',$arr_forms);
		
	}
}	

}

public function FormViewLayout()
{
 $_arrs  = & self::_getAttributes();
 if( !is_null(self::$ViewLayout) AND !empty(self::$ViewLayout) ) 
 {
	if( $_arrs_layout = $this->{base_class_model($this)}->_getWorkForm($_arrs['ProjectCode']) )
	{
		$arr_forms['form'] 			= $_arrs_layout['AddView'];
		$arr_forms['JavaScript'] 	= $_arrs_layout['UrlAddJs'];
		$arr_forms['PolicyId'] 		= self::$PolicyId;
		$arr_forms['PolicyData'] 	= $this ->getPolicyHistory();
		$arr_forms['Combo'] 		= $this->getCombo();
		$arr_forms['ProjectId'] 	= $_arrs['ProjectId'];
		$arr_forms['FollowupGroup'] = $this->getFollowUpGroup($_arrs['ProjectId']);
		$arr_forms['FollowUpData']	= $this->{base_class_model($this)}->_getFollowUpExist($arr_forms['PolicyData']);
		$arr_forms['Province'] 		= $this->M_Combo->_getProvince();
		
		$this -> load -> form('view_form/'. $_arrs_layout['AddView'] .'/form_content',$arr_forms);
		
	}
}	

}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function FormEditLayout()
 {
	$_arrs  = & self::_getAttributes();
 if( !is_null(self::$ViewLayout) AND !empty(self::$ViewLayout) ) 
 {
	if( $_arrs_layout = $this->{base_class_model($this)}->_getWorkForm($_arrs['ProjectCode']) )
	{
		$arr_forms['form'] 		 	= $_arrs_layout['AddView'];
		$arr_forms['JavaScript'] 	= $_arrs_layout['UrlViewJs'];
		$arr_forms['ProjectId'] 	= $_arrs['ProjectId'];
		$arr_forms['PolicyId'] 	 	= self::$PolicyId;
		$arr_forms['PolicyData'] 	= self::getPolicyHistory();
		$arr_forms['Combo'] 		= self::getCombo();
		$arr_forms['FollowupGroup'] = self::getFollowUpGroup($_arrs['ProjectId']);
		$arr_forms['FollowUpData']	= $this->{base_class_model($this)}->_getFollowUpExist($arr_forms['PolicyData']);
		
		$this -> load -> form('edit_form/'. $_arrs_layout['AddView'] .'/form_content',$arr_forms);
	}
}

 }
 
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
public function QustionByPolicyId()
{
	$QustionByPolicyId = $this->{base_class_model($this)}->_getQustionByPolicyId( $this->URI->_get_post('PolicyId') );
	if( $QustionByPolicyId )
		echo json_encode($QustionByPolicyId);
	else
	{
		echo json_encode( array(
			'CustField_Q1' => '',
			'CustField_Q10' => '',
			'CustField_Q2' => '',
			'CustField_Q3' => '',
			'CustField_Q4' => '',
			'CustField_Q5' => '',
			'CustField_Q6' => '',
			'CustField_Q7' => '',
			'CustField_Q8' => '',
			'CustField_Q9' => '' ) );
	}		
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

 
public function QustionByQualityPolicyId()
{
	$QustionByPolicyId = $this->{base_class_model($this)}->_getQustionByQualityPolicyId( $this->URI->_get_post('PolicyId') );
	if( $QustionByPolicyId )
		echo json_encode($QustionByPolicyId);
	else
	{
		echo json_encode( array(
			'QuestionId'  => '',
			'CustField_Q1' => '',
			'CustField_Q10' => '',
			'CustField_Q2' => '',
			'CustField_Q3' => '',
			'CustField_Q4' => '',
			'CustField_Q5' => '',
			'CustField_Q6' => '',
			'CustField_Q7' => '',
			'CustField_Q8' => '',
			'CustField_Q9' => '' ) );
	}		
}



 
}
?>