<?php
class FormLunas extends EUI_Controller
{

var $DebiturId = null;

/*
 * @ pack : index 
 */
 
public function __construct()
{
	parent::__construct();
	$this->DebiturId = base64_decode(_get_post('DebiturId'));
	$this->load->model(array(base_class_model($this),'M_SysUser','M_ModContactDetail'));
}

/*
 * @ pack : index 
 */
 
public function html()
{
  if( $this->DebiturId )
 {
	$content = array( 
		'discount' => $this->{base_class_model($this)}->_get_select_debitur($this->DebiturId),
		'campaign' => $this->{base_class_model($this)}->_get_select_campaign(),
		'cpa_reason' => $this->{base_class_model($this)}->_get_select_cpa_reason(),
		'payers' => $this->{base_class_model($this)}->_get_select_payer(),
		'occupation'=> $this->{base_class_model($this)}->_get_select_occupation(),
		'document_type' => $this->{base_class_model($this)}->_get_select_document_type(),
		'payment_periode' => $this->{base_class_model($this)}->_get_select_periode(),
		'debitur'=> $this->M_ModContactDetail->_get_select_debitur($this->DebiturId)
	);
		
	$this->load->form("form_html_lunas/view_form_index", $content);
 }
 
}

/*
 * @ pack : index 
 */
 
public function pdf()
{
  if( ($this->DebiturId) 
	AND !is_null( $this->DebiturId ) )
 {
	if( $DebiturId = $this->DebiturId ) 
	{
		$content = array( 
			'discount' => $this->{base_class_model($this)}->_get_select_debitur($DebiturId),
			'campaign' => $this->{base_class_model($this)}->_get_select_campaign() 
		);
		
	// @ pack : send data to view 
	
		if( is_array( $content ) ) {
			$this->load->form("form_pdf_lunas/view_form_index", $content);
		}	
		
	} else {
		exit('No debitur id ');
	}	
 }
 
}

/*
 * @ pack : SaveDiscount
 */
 
 public function SaveLunas()
{

  $response = array('success' => 0);
  if( _have_get_session('UserId') ) 
  {
	if( $cond = $this->{base_class_model($this)}->_SaveLunas() ) {
		$response = array('success' => 1);
	}
  }
  
  echo json_encode($response);

}

// @ pack : update form lunas 

 public function UpdateLunas()
{
   $response = array('success' => 0);
  if( _have_get_session('UserId') 
	AND _get_have_post('FormUpdateId') ) 
  {
	if( $cond = $this->{base_class_model($this)}->_UpdateLunas() ) {
		$response = array('success' => 1);
	}
  }
  
  echo json_encode($response);
}

/*
 * @ pack : index 
 */
 
public function PrintByDiscountId( )
{
 
  $DiscountId = _get_post('DiscountId');
  $_recsource = $this->{base_class_model($this)}->_get_select_discountId($DiscountId);
  $DebiturId  = ($this->DebiturId ? $this->DebiturId : $_recsource['deb_id']);
  
 if( ($DebiturId) 
	AND !is_null($DebiturId) )
 {
	if( $DebiturId ) 
	{
		$content = array( 
		'recsource' => $this->{base_class_model($this)}->_get_select_discountId($DiscountId),
		'discount' => $this->{base_class_model($this)}->_get_select_debitur($DebiturId),
		'campaign' => $this->{base_class_model($this)}->_get_select_campaign(),
		'cpa_reason' => $this->{base_class_model($this)}->_get_select_cpa_reason(),
		'payers' => $this->{base_class_model($this)}->_get_select_payer(),
		'occupation'=> $this->{base_class_model($this)}->_get_select_occupation(),
		'document_type' => $this->{base_class_model($this)}->_get_select_document_type(),
		'payment_periode' => $this->{base_class_model($this)}->_get_select_periode(),
		);
		
		$content['ex_doc'] = explode(',',$content['recsource']['doc']);
		$content['agent_detail'] = ($content['recsource']['agent_id']?$this->M_SysUser->_get_detail_user($content['recsource']['agent_id']):null);
		$content['LblApprHead'] = ( ( abs($content['recsource']['from_princ']) ) >= 30?"Approval by Head of Collection":"&nbsp;" );
	
	// @ pack : send data to view 
	
		if(( _get_post('action')!='edit' ) 
			and ( is_array($content) )) 
		{
			$this->load->form("form_pdf_lunas/view_form_index", $content); 
		}
		else {
			if( is_array( $content ) ) {
				$this->load->form("form_html_lunas/view_form_index", $content);
			}
		}	
		
	} else {
		exit('No debitur id ');
	}	
 }
 
}
 
 
// END CLASS 

}


?>