<?php
class ModSaveActivity extends EUI_Controller
{

/*
 * @ pack : __construct
 */
 
 function __construct()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
	$this->load->helper(array('EUI_Object'));
 }
 
/*
 * @ pack : moda save activity handle 
 */
 
 public function SaveActivityNote(){
     $_conds = array('success'=>0 );
     if( $this->EUI_Session->_get_session('UserId') )
     {
              if( $_is_conds = $this->{base_class_model($this)}->_SaveCallHistoryNote() )
              {
                $_conds = array('success'=>1);
              }	
      }	
	
	echo json_encode($_conds);
 }
 
 public function SaveActivity() 
{
	
  $_conds = array('success'=>0 );
  $out = new EUI_Object( _get_all_request());
  
  $AccountStatus = $out->get_value('select_account_status_code');
  if( strlen($AccountStatus) == 0  
	OR $AccountStatus == 0  ) 
 {
	 echo json_encode( array('success'=>0));  
	 return FALSE;
  } 
  
 // ---------- ( UPDATE bugs KOSONG  === 0 ) --------------------------------
 
  if( $this->EUI_Session->_get_session('UserId') )
  {
	if( $_is_conds = $this->{base_class_model($this)}->_SaveActivity() )
	{
	  $_conds = array('success'=>1);
	}	
  }	
	
	echo json_encode($_conds);
 }
 
/*
 * @ pack : SaveNotesActivity
 */ 
 public function SaveNotesActivity()
{
  $_conds = array('success'=>0 );
   if( $this->EUI_Session->_get_session('UserId') )
  {
	if( $_is_conds = $this->{base_class_model($this)}->_SaveNotesActivity() )
	{
	  $_conds = array('success'=>1);
	}	
  }	
  
  echo json_encode($_conds);
  
} 

/*
 * @ pack : SaveNewSms
 */
 
public function SaveSmsActivity() 
{
 $_conds = array('success'=>0 );
   if( $this->EUI_Session->_get_session('UserId') )
  {
	if( $_is_conds = $this->{base_class_model($this)}->_SaveSmsActivity() )
	{
	  $_conds = array('success'=>1);
	}	
  }	
  
  echo json_encode($_conds);
}

/*
 * @ pack : SaveClaimActivity
 */
 
public function SaveClaimActivity() 
{
  $_conds = array('success'=>0 );
  if( $this->EUI_Session->_get_session('UserId') )
  {
	
	if( $_is_conds = $this->{base_class_model($this)}->_SaveClaimActivity() )
	{
	  $_conds = array('success'=>1);
	}

  }	
  
  echo json_encode($_conds);
}
 
 
 
/*
 * @ pack : SaveBlocking
 */ 
 
 public function SaveBlocking()
{
  $_conds = array('success'=>0 );
  if( $this->EUI_Session->_get_session('UserId') )
  {
	if( $_is_conds = $this->{base_class_model($this)}->_SaveBlocking() ) {
	  $_conds = array('success'=>1);
	}
  }
  
  echo json_encode($_conds);
   
  
}

	public function save_prospect_note()
	{
		$_conds = array('success'=>0 );
		if( $this->EUI_Session->_get_session('UserId') )
		{
			if( $_is_conds = $this->{base_class_model($this)}->_SaveProspectNote() ) {
				$_conds = array('success'=>1);
			}
		}

		echo json_encode($_conds);
	}
	
	public function save_complain_note()
	{
		$_conds = array('success'=>0 );
		if( $this->EUI_Session->_get_session('UserId') )
		{
			$this->load->model('M_ModContactDetail');
			if( $_is_conds = $this->{base_class_model($this)}->_SaveComplainNote() ) {
				$_conds = array('success'=>1);
			}
		}
		echo json_encode($_conds);
	}
	
	public function save_vc_reason()
	{
		$_conds = array('success'=>0 );
		if( $this->EUI_Session->_get_session('UserId') )
		{
			$this->load->model('M_ModContactDetail');
			if( $_is_conds = $this->{base_class_model($this)}->_save_vc_reason() ) {
				$_conds = array('success'=>1);
			}
		}
		echo json_encode($_conds);
	}
	public function check_complain()
	{
		$status['valid_complain'] = 0;
		if( $this->EUI_Session->_get_session('UserId') )
		{
			$this->load->model('M_MgtComplainDebitur');
			$argv  =& $this->URI->_get_all_request();
			if( $this->M_MgtComplainDebitur->ComplainIsProgress($argv['CustomerId']) )
			{
				$status['valid_complain'] = 1;
			}
		}
		echo json_encode($status);
	}

	
	/*
	 * @ pack : SaveClaimActivity
	 */
	 
	public function SaveClaimPDSActivity() 
	{
	  $_conds = array('success'=>0 );
	  if( $this->EUI_Session->_get_session('UserId') )
	  {
		
		if( $_is_conds = $this->{base_class_model($this)}->_UpdatePds() )
		{
		  $_conds = array('success'=>1);
		}

	  }	
	  
	  echo json_encode($_conds);
	}



	public function setExpiredPVC(){
		// usage 00 07 * * * root /usr/bin/php -q /opt/enigma/webapps/devel/index.php ModSaveActivity setExpiredPVC
		$dayparameter	= array(1=>5, 2=>5, 3=>5, 4=>3, 5=>3);
		$expirydate		= date('Y-m-d');
		$timestamp 		= strtotime($expirydate);
		$daynumber		= date('w', $timestamp);
		$date			= strtotime($expirydate."-".$dayparameter[$daynumber]." day");
		
		$pvcdatestart	= date('Y-m-d', $date)." 00:00:00";
		$pvcdateend		= date('Y-m-d', $date)." 23:59:59";
		
		// $pvcdatestart	= "2021-05-04 13:00:00";
		// $pvcdateend		= "2021-05-04 23:59:59";
		// echo $pvcdateend;
		$debiturPVC = $this->{base_class_model($this)}->_setExpiredPVC($pvcdatestart, $pvcdateend);
		
		if(is_array($debiturPVC)){
			// echo "expired";
			// echo "<pre>";
			// print_r($debiturPVC);
			// echo "</pre>";
			$this->{base_class_model($this)}->_setAccountStatusBeforePVC($debiturPVC);
		}
		
		// echo "<pre>";
		// print_r($debiturPVC);
		// echo "</pre>";
	}
	

 // END CLASS
 
}
?>
