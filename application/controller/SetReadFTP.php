<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SetFTP 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SetReadFTP extends EUI_Controller
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function SetReadFTP()
 {
	parent::__construct();
	$this -> load -> model(array('M_SetReadFTP','M_SysUser'));
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session -> _have_get_session('UserId') )
	{
		
		$_EUI['page'] = $this -> M_SetReadFTP -> _get_default();
		if( is_array($_EUI)) 
		{
			$this -> load -> view('set_ftp_read/view_ftp_read_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
	  $_EUI['page'] = $this -> M_SetReadFTP -> _get_resource();    // load content data by pages 
	  $_EUI['num']  = $this -> M_SetReadFTP -> _get_page_number(); // load content data by pages 
	  $this -> load -> view('set_ftp_read/view_ftp_read_list',$_EUI);		
	}	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function FTPEdit()
 {
 if( $this -> EUI_Session -> _have_get_session('UserId') )
 {
	$UI['WorkProject']  	= $this->{base_class_model($this)}->_getWorkProjectCode(); 
	$UI['TemplateName'] 	= $this->{base_class_model($this)}->_getTemplateName();
	$UI['FtpReadData']		= $this->{base_class_model($this)}->_getFTPRead( $this -> URI-> _get_post('ftp_read_id'));
	$UI['UserPrivileges']   = $this->M_SysUser->_get_handling_type(); 
	$UI['UserId'] 			= $this->M_SysUser->_getUserRegistration();
		
	if( $UI )
	{
		$this ->load->view("set_ftp_read/view_edit_ftp", $UI);
	}	
 }	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function FTPAdd()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI['WorkProject']  	= $this->{base_class_model($this)}->_getWorkProjectCode(); 
		$UI['TemplateName'] 	= $this->{base_class_model($this)}->_getTemplateName();
		$UI['UserPrivileges']  	= $this->M_SysUser->_get_handling_type(); 
		$UI['UserId'] 			= $this->M_SysUser->_getUserRegistration();
		
		if( $UI )
		{
			$this ->load->view("set_ftp_read/view_add_ftp", $UI);
		}	
	}	
 }
   /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 public function WorkUserByLevel()
 {
	$User = array();
	if( $this->EUI_Session->_get_session('UserId') ) {
		$User = $this->M_SysUser->_getUserLevelGroup( $this -> URI->_get_post('ProfileId'));
	}
	
	__(form()->combo('ftp_read_userid', 'select', $User ));
 }
 
 
 /** http://192.168.10.236/collection/index.php/SetReadFTP/FTPUpdate/ **/

 
 public function FTPUpdate()
 {
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setFTPUpdate( $this -> URI->_get_all_request() ))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
 }
 
/* http://192.168.10.236/collection/index.php/SetReadFTP/FTPSave/ **/

 public function FTPSave()
 {
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setFTPSave( $this -> URI->_get_all_request() ))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
 }
 

/* FTPDelete **/

public function FTPDelete()
{
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setFTPDelete( $this -> URI->_get_array_post('Id')))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
} 
 
}
?>