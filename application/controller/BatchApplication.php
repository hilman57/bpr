<?php
/*
 * @ pack : BatchApplication
 * @ note : this data for crontab action from here .
 * ------------------------------------------------------------------
 * @ example : $> php -q /opt/enigma/webapps/hsbc/index.php BatchApplication index param
 */
 
 class BatchApplication extends EUI_Controller
{

/*
 * @ pack : __construct
 * ------------------------------------------------------------------
 */ 
 
 var $batch_error = array();
 var $batch_param = array();

/*
 * @ pack : __construct
 * ------------------------------------------------------------------
 */ 
 
 public function __construct()
{
  parent::__construct();
  $this->load->model(base_class_model($this));	
 
// START :: parameter =========================================>

  $this->_set_batch_param("absensi","SetAgentAbsensi"); 			// $c -> cronjob for agent absensi 
  $this->_set_batch_param("absensiblock","SetAgentBlocking"); 		// $c -> cronjob for agent absensi 
  $this->_set_batch_param("absensiunblock","SetUnblockAgent"); 		// $c -> cronjob for agent absensi 
  //$this->_set_batch_param("ptp_proses","SetPtpProses");
  $this->_set_batch_param("open_phone_block","SetOpenBlockPhone"); 	// $c -> cronjob open block 
  $this->_set_batch_param("open_auto_unlock","SetAutoUnlock"); 		// $c -> open lock account 
  $this->_set_batch_param("start_auto_lock","SetStartLock"); 		// $c -> start lock account 
  $this->_set_batch_param("update_discount","SetUpdateDiscount"); 	// $c -> cek discount payment 
  $this->_set_batch_param("clear_chat","SetClearChat"); 			// $c -> clear chat 3 month ago 
  $this->_set_batch_param("clear_broadcast","SetClearBrodcast"); 	// $c -> clear broadcast 3 month ago 
  $this->_set_batch_param("check_bp_status","SetCheckBPStatus"); 	// $c -> check status BP 
  $this->_set_batch_param("check_pop_status","SetCheckPOPStatus"); 	// $c -> check status BP 
  $this->_set_batch_param("excel_dumper","SetExcelDumper"); 		// $c -> for dump excel every day 
  $this->_set_batch_param("five_assigment", "SetFiveDayAssigment"); // $c -> for re - Assigment on 5 day 
  $this->_set_batch_param("reset_approval", "SetResetApproval");	// $c -> for re -  reset approval on 30 day 
  $this->_set_batch_param("compile_sdr", "SetCompileSdr");			// $c -> for generate SDR report table 
  $this->_set_batch_param("excel_deleted", "DownloadExcelDeleted");			// $c -> for generate SDR report table 
  $this->_set_batch_param("auto_round_account", "AutoRound");			// $c -> for Round Process 
  $this->_set_batch_param("log_inventory", "SetInventoryLog");			// $c -> for Inventory Log
  
  
  // STOP :: parameter =========================================>
 
}

/*
 * @ pack : set _set_batch_param 
 */
 
 public function _set_batch_param($key = null, $value = null )
{
	if(!is_null($key) )
	{
		$this->batch_param[$key] = $value;
	}
} // STOP :: _set_batch_param ================>


/*
 * @ pack : set _set_batch_param 
 */
 
 public function _set_batch_error($key = null, $value = null )
{
	if(!is_null($key) ) {
		$this->batch_error[$key] = $value ."\n\r";
	}
} // STOP :: _set_batch_param ================>



/*
 * @ pack : index =================> default 
 */
 
 public function index()
{
   global $argv, $argc;
   echo "argument => ".$argv[3];
   if( count($argc) > 0 AND isset($argv[3]) )
   {
	  $_POST_VARS = $this->batch_param[$argv[3]];
	  if( isset( $_POST_VARS ) )
	  {
		$call_function = $this->batch_param[$argv[3]];
		call_user_func(array(get_class($this),$call_function));
		// print_r(array(get_class($this),$call_function));
		// exit();
	  }
	  
   } else  {
		$this->_set_batch_error('1001', "argument is not valid !");
   }
   
   if(is_array($this->batch_error))
	foreach($this->batch_error as $k => $err)
  {
		print $err;
   }
   
}
 
/*
 * @ pack 	 : locked Descoll if not login with interval definition before 
 * 			   lock on table configuration 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAgentAbsensi
 * ------------------------------------------------------------------
 */ 

 public function SetAgentAbsensi()
{
	// blocking agent telat db cronjob 8:15
	$cond = $this->{base_class_model($this)}->_SetAgentAbsensi();
	if( $cond > 0 ) {
		$this->_set_batch_error('1002', "process lock is done.!");
	}
 }
 
 public function SetAgentBlocking()
{
	// blocking seluruh agent dg cronjob jam 20
	$cond = $this->{base_class_model($this)}->_SetAgentBlocking();
	if( $cond > 0 ) {
		$this->_set_batch_error('1002', "Blocking agent is done.!");
	}
}

public function SetUnblockAgent()
{
	// unblocking seluruh agent dg cronjob jam 7:59
	$cond = $this->{base_class_model($this)}->_SetUnblockAgent();
	if( $cond > 0 ) {
		$this->_set_batch_error('1002', "Unblocking agent is done.!");
	}
}
 //================> end batch event SetAgentAbsensi


/*
 * @ pack 	 : open block by phone Acoount if user forget open manual
 * 			   lock on table configuration 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetOpenBlockPhone
 * ------------------------------------------------------------------
 */ 
 
 public function SetOpenBlockPhone()
{
	$cond = $this->{base_class_model($this)}->_SetOpenBlockPhone();
	if( $cond > 0 ) {
		$this->_set_batch_error('1003', "process Block Phone is done.!");
	}
}
//================> end batch event SetOpenBlockPhone


/*
 * @ pack 	 : unlock data if time is expired from here look its 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAutoUnlock
 * ------------------------------------------------------------------
 */ 
 
 public function SetAutoUnlock()
{
	$cond = $this->{base_class_model($this)}->_SetAutoUnlock();
	if( $cond > 0 ) {
		$this->_set_batch_error('1004', "process auto unlock account is done.!");
	}
}
//================> end batch event SetAutoUnlock

// END CLASSS =================>  

/*
 * @ pack 	 : unlock data if time is expired from here look its 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAutoUnlock
 * ------------------------------------------------------------------
 */ 
 
 public function SetStartLock()
{
	$cond = $this->{base_class_model($this)}->_SetStartLock();
	if( $cond > 0 ) {
		$this->_set_batch_error('1005', "process auto unlock account is done.!!");
	}
}
//================> end batch event SetAutoUnlock


/*
 * @ pack 	 : unlock data if time is expired from here look its 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAutoUnlock
 * ------------------------------------------------------------------
 */ 
 
 public function SetUpdateDiscount()
{
	$cond = $this->{base_class_model($this)}->_SetUpdateDiscount();
	if( $cond > 0 ) {
		$this->_set_batch_error('1006', "process update Request is done.!");
	} else {
		$this->_set_batch_error('1006', "process update Request is failed.!");
	}
}
//================> end batch event SetAutoUnlock


/*
 * @ pack 	 : Clear of SetClearChat 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetClearChat
 * ------------------------------------------------------------------
 */ 
 
 public function SetClearChat()
{
	$cond = $this->{base_class_model($this)}->_SetClearChat();
	if( $cond > 0 ) {
		$this->batch_error[] = "process clear chat done.!\r\n";
	} else {
		$this->batch_error[] = "process clear chat failed.!\r\n";
	}
} //================> end batch event SetAutoUnlock


/*
 * @ pack 	 : clear broadcast message
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAutoUnlock
 * ------------------------------------------------------------------
 */ 
 
 public function SetClearBrodcast()
{
	$cond = $this->{base_class_model($this)}->_SetClearBroadcast();
	if( $cond > 0 ) {
		$this->batch_error[] = "process clear broadcast is done.!\r\n";
	} else {
		$this->batch_error[] = "process clear broadcast failed.!\r\n";
	}
} //================> end batch event SetAutoUnlock


/*
 * @ pack 	 : set SetCheckBPStatus 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAutoUnlock
 * ------------------------------------------------------------------
 */ 
 
 public function SetCheckBPStatus()
{
	$cond = $this->{base_class_model($this)}->_SetCheckBPStatus();
	if( $cond > 0 ) {
		$this->batch_error[] = "process Check BP Status is done.!\r\n";
	} else {
		$this->batch_error[] = "process Check BP Status failed.!\r\n";
	}
} 
// SetCheckBPStatus ================> 

/*
 * @ pack 	 : set SetCheckBPStatus 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php BatchApplication SetAutoUnlock
 * ------------------------------------------------------------------
 */ 
 
 public function SetCheckPOPStatus()
{
	$cond = $this->{base_class_model($this)}->_SetCheckPOPStatus();
	if( $cond > 0 ) {
		$this->batch_error[] = "process Check POP Status is done.!\r\n";
	} else {
		$this->batch_error[] = "process Check POP Status failed.!\r\n";
	}
} 
// SetCheckBPStatus ================> 

// -----------------------------------------------------
/*
 * @ pack   : call setProsessDumperExcel  
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 public function SetExcelDumper()
{
	$this->batch_error[] = "Start Process .\n\r";
	$cond = $this->{base_class_model($this)}->_SetExcelDumper();
	if( $cond > 0 ) {
		$this->batch_error[] = "process excel dump is done.!\n\r";
	} else {
		$this->batch_error[] = "process excel dump failed.!\n\r";
	}
	
} // SetExcelDumper ======================> 

// -----------------------------------------------------
/*
 * @ pack   : call setProsessDumperExcel  
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 public function DownloadExcelDeleted()
{
	$this->batch_error[] = "Start Process .\n\r";
	$cond = $this->{base_class_model($this)}->_DownloadExcelDeleted();
	if( $cond > 0 ) {
		$this->batch_error[] = "process excel deleted dump is done.!\n\r";
	} else {
		$this->batch_error[] = "process excel deleted  failed.!\n\r";
	}
	
} // SetExcelDumper ======================> 




// -----------------------------------------------------
/*
 * @ pack   : call SetFiveDayAssigment  
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 public function SetFiveDayAssigment()
{
	$this->batch_error[] = "Start Process .\n\r";
	$cond = $this->{base_class_model($this)}->_SetFiveDayAssigment();
	if( $cond > 0 ) {
		$this->_set_batch_error("111", "process re- Assigment is done.!");
	} else {
		$this->_set_batch_error("111", "process re- Assigment is failed.!");
	}
	
} // SetFiveDayAssigment ======================> 

// -----------------------------------------------------
/*
 * @ pack   : call SetResetApproval  
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 public function SetResetApproval()
{
	$this->batch_error[] = "Start Process .\n\r";
	$cond = $this->{base_class_model($this)}->_SetResetApproval();
	if( $cond > 0 ) {
		$this->_set_batch_error("111", "process reset approval is done.!");
	} else {
		$this->_set_batch_error("111", "process reset approval is failed.!");
	}
	
} // SetResetApproval ======================> 


// -----------------------------------------------------
/*
 * @ pack   : call SetCompileSdr  
 * @ return : void(0)
 */
 // -----------------------------------------------------
 
 public function SetCompileSdr()
{
	$this->batch_error[] = "Start Process Generate SDR .\n\r";
	$cond = $this->{base_class_model($this)}->_SetCompileSdr();
	if( $cond > 0 ) {
		$this->_set_batch_error("111", "process generate SDR is done.!");
	} else {
		$this->_set_batch_error("111", "process generate SDR is failed.!");
	}
	
} // SetCompileSdr ======================> 

public function AutoRound()
{
	// $this->batch_error[] = "Round process is running.!\n\r";
	// echo "Round process is running.!\n\r";
	// $cond = $this->{base_class_model($this)}->_RoundAccount();
	// if( $cond['conds'] ) {
		// $this->_set_batch_error("1009", $cond['message']);
	// } else {
		// $this->_set_batch_error("1009", $cond['message']);
	// }
	$cond = "\n".$this->{base_class_model($this)}->_RoundAccount();
	$cond .= "\n".$this->{base_class_model($this)}->expire_access_all();
	$this->_set_batch_error("1009", $cond);
	/*$msg = $cond."\n";
	if($msg!=""){
		$file_msg = "random_log_".date("d-m-Y").".txt";
		$path_file_msg = FCPATH."application/temp/".$file_msg;
		if(file_exists($path_file_msg)){
			//echo "file ada";
			$handle = fopen($path_file_msg, 'a') or die('Cannot open file:  '.$path_file_msg);
			fwrite($handle, $msg);
			fclose($handle);
		}
		else{
			//echo "file ga ada";
			$handle = fopen($path_file_msg, 'w') or die('Cannot open file:  '.$path_file_msg); 
			fwrite($handle, $msg);
			fclose($handle);
		}
	}*/ 
	// print_r($cond);
} // Start Round Process ======================> 


// Fungsi ambil data untuk log data inventory
public function SetInventoryLog()
{
	$this->batch_error[] = "Start Process Log Inventory .\n\r";
	$cond = $this->{base_class_model($this)}->_SetInventoryLog();
	if( $cond > 0 ) {
		$this->_set_batch_error("111", "process Log Inventory is done.!");
	} else {
		$this->_set_batch_error("111", "process Log Inventory is failed.!");
	}
	
} 



// END CLASSS =================>  

}

?>