<?php

class CallOutboundReport extends EUI_Controller
{

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CallOutboundReport()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
	if( class_exists(base_class_model($this)) )
	{
		$this->M_CallOutboundReport->setDisplayParams( $this->URI->_get_all_request() );
	}	
}

/**
 * @def  : page of index read first class 
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function index() 
{
	$Initialize = $this->{base_class_model($this)}->getInitialize();
	$Initialize["ReportMonth"] = $this->{base_class_model($this)}->getRptMonth();
	if( $Initialize )
	{
		$this->load->view("rpt_call_outbound/view_iframe_outbound", $Initialize);
	}
}

/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function display()
{
	$this->load->report('R_CallOutboundReport');
	
	if( $UI = array( 
		'display' => $this->R_CallOutboundReport->display()  ) AND is_array( $UI ) )
	{
		$this->load->view("rpt_call_outbound/view_call_outbound", $UI);
	}
}



/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
 function DisplayParentDetail()
 {
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
	
		if( $param = $this->URI->_get_all_request() )
		{
			$view  = $this->M_CallOutboundReport->DetailByParents();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}


/**
 * @ def  : DisplayContactDetail
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
public function DisplayContactDetail()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
	
		if( $param = $this->URI->_get_all_request() )
		{
			$view  = $this->M_CallOutboundReport->DisplayContactDetail();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}
 

/**
 * @ def  : DisplayContactDetail
 * ------------------------------------------------
 
 * @ param : - 
 * @ param : -
 * 
 */
 
 
public function DisplayNotPtpDetail()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
	
		if( $param = $this->URI->_get_all_request() )
		{
			$view  = $this->M_CallOutboundReport->DisplayNotPtpDetail();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}

 
/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
 
 
public function DisplayPtpDetail()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ) {
			$view  = $this->M_CallOutboundReport->DisplayPtpDetail();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}
 

/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */

function DisplayDetailReturnList()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DisplayDetailReturnList();
		}
	} 
  $this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
} 

/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
function DisplayDetailByAtempt()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DisplayDetailByAtempt();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}

/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 

public function DisplayCallAtempDetail()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
	
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DisplayCallAtempDetail();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}

/**
 * @def  : -
 * ------------------------------------------------
 
 * @param : - 
 * @param : -
 * 
 */
 
public function DisplayDetailByAtemptAndContact()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DisplayDetailByAtemptAndContact();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}


/**
 * @ def  	: DisplayDetailAtemptAndContact
 * ------------------------------------------------
 
 * @ param :  
 * @ param : 
 * 
 */
 
public function DisplayDetailAtemptAndContact()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DisplayDetailAtemptAndContact();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}


/**
 * @ def  	: DisplayDetailAtemptAndContact
 * ------------------------------------------------
 
 * @ param :  
 * @ param : 
 * 
 */
 
public function DetailOptionCompleted()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DetailOptionCompleted();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}


/**
 * @ def  	: DisplayDetailAtemptAndContact
 * ------------------------------------------------
 
 * @ param :  
 * @ param : 
 * 
 */
 
public function DetailDeadTone()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DetailDeadTone();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}

/**
 * @ def  	: DisplayDetailAtemptAndContact
 * ------------------------------------------------
 
 * @ param :  
 * @ param : 
 * 
 */
 
public function DetailUnconnected()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DetailUnconnected();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}

/**
 * @ def  	: DisplayDetailAtemptAndContact
 * ------------------------------------------------
 
 * @ param :  
 * @ param : 
 * 
 */
public  function DetailConnected()
{
	static $view = array();
	if( $this->URI->_get_have_post('action') )
	{
		if( $param = $this->URI->_get_all_request() ){
			$view  = $this->M_CallOutboundReport->DetailConnected();
		}
	} 
	
	$this->load->view("rpt_call_outbound/view_call_detailbyparents", $view);
}




}
?>