<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class QtyApprovalData extends EUI_Controller
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function QtyApprovalData() 
 {
	parent::__construct();	
	$this -> load ->model
	( 
		array
		(
			base_class_model($this),
			'M_SetResultQuality','M_SysUser',
			'M_SrcCustomerList','M_SetCallResult',
			'M_SetProduct','M_SetCampaign'
		)
	);	
 }
 
 
  
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCallResult()
 {
	$_conds = array();
	
	 foreach
	 ( 
		$this ->M_SetCallResult ->_getCallReasonId()  
			as $k  => $call  )
	{
		$_conds[$k] = $call['name'];
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getProductId()
 {
	$_conds = array();
	foreach
	( 
		$this ->M_SetProduct ->_getProductId()  
		as $k  => $call )
	{
		$_conds[$k] = $call['name'];
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getQtyResult()
 {
	$_conds = array();
	if(class_exists('M_SetResultQuality'))
	{
		foreach
		( 
			$this -> M_SetResultQuality ->_getQualityResult()  
			as $k  => $call )
		{
			$_conds[$k] = $call['name'];
		}
	}	
	
	return $_conds;
 }
 
 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function index()
{
 if( $this ->EUI_Session ->_have_get_session('UserId') && class_exists('M_QtyApprovalData') )
 {
	$_EUI  = array
	( 
		'page' 			=> $this -> M_QtyApprovalData -> _get_default(),
		'CampaignId' 	=> $this -> M_SetCampaign -> _get_campaign_name(),
		'CardType' 		=> $this -> M_SrcCustomerList ->_getCardType(), 
		'GenderId' 		=> $this -> M_SrcCustomerList ->_getGenderId(),
		'UserId' 		=> $this -> M_SysUser -> _get_teleamarketer(), 
		'CallResult' 	=> $this -> _getCallResult(), 
		'QtyResult'		=> $this -> _getQtyResult(),
		'ProductId' 	=> $this -> _getProductId()
	);	
		
	if( is_array($_EUI))
	{
		$this -> load ->view('qty_approval_data/view_approval_data_nav',$_EUI);
	}	
 }
 
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Content()
{

  if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->M_QtyApprovalData ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->M_QtyApprovalData ->_get_page_number(); // load content data by pages 
		
		// sent to view data 
		
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('qty_approval_data/view_approval_data_list',$_EUI);
		}	
	}	
}
 
}