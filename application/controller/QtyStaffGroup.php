<?php
class QtyStaffGroup extends EUI_Controller 
{
/*
 * @def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
private static $view_layout = null;
	
/*
 * @def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

function QtyStaffGroup() 
{
	parent::__construct();
	$this -> load ->model(array(base_class_model($this)));
	
	if(is_null(self::$view_layout) ) {
		self::$view_layout = "qty_view_staffgroup"; 
	}	
}

	
/*
 * @def : constructor off class 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

 public function getViewLayout() {
	return self::$view_layout;
 }

/*
 * @def : index default classes 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
 
public function index()
{
	if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) )
	{
		$this->load->view("{$this -> getViewLayout()}/view_staff_group_nav");
		
	}	
}


/*
 * @def : index default classes 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
 
public function StaffAvailable()
{
  if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) )
  {
	$UI = array('view_staff_available' => $this -> {base_class_model($this)} ->_getStaffAvailable()); 
	$this->load->view("{$this -> getViewLayout()}/view_staff_available",$UI);
  }
  
}

/*
 * @def : index default classes 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
 
public function StaffByGroup()
{
 if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) )
  {
	$UI = array(
		'view_staff_group' => $this -> {base_class_model($this)} ->_getStaffByGroup(),
		'view_staff_skill' => $this -> {base_class_model($this)} ->_getStaffSkill() ); 
	$this->load->view("{$this -> getViewLayout()}/view_staff_groups",$UI);
  }
}

/*
 * @def : AddAvailableSkill
 * ----------------------------------
 *
 * @param : QualityStaffId ( array)
 * @param : -
 */
 
public function AddAvailableSkill()
{
	$_conds = array('success' => 0);
	
	if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) 
		AND $this -> URI -> _get_have_post('QualityStaffId') )
  {
		if( $result = $this -> {base_class_model($this)} -> _setAddAvailableSkill( $this -> URI -> _get_array_post('QualityStaffId') )) 
		{
			$_conds = array('success' => 1);
		}
  }
  
  __(json_encode($_conds));
}

/*
 * @ def : AssignQualitySkill
 * ----------------------------------
 *
 * @ param : Quality_Group_Id
 * @ param : Quality_Skill_Id
 */
 public function AssignQualitySkill()
{
	$_conds = array('success' => 0);
	if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) 
		AND $this -> URI -> _get_have_post('Quality_Group_Id') 
		AND $this -> URI -> _get_have_post('Quality_Skill_Id') )
	{
		if( $result = $this -> {base_class_model($this)} -> _setAssignQualitySkill( 
			$this -> URI -> _get_array_post('Quality_Group_Id'), 
			$this -> URI -> _get_post('Quality_Skill_Id')
		)) 
		{
			$_conds = array('success' => 1);
		}
	}
  
  __(json_encode($_conds));
	
}

/*
 * @ def : RemoveQualitySkill
 * ----------------------------------
 *
 * @ param : Quality_Group_Id
 * @ param : -
 */

 public function RemoveQualitySkill()
{
	$_conds = array('success' => 0);
	if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) 
		AND $this -> URI -> _get_have_post('Quality_Group_Id') )
	{
		if( $result = $this -> {base_class_model($this)} -> _setRemoveQualitySkill(  $this -> URI -> _get_array_post('Quality_Group_Id') )) 
		{
			$_conds = array('success' => 1);
		}
	}
  
  __(json_encode($_conds));
	
}
 

/*
 * @ def : ClearQualitySkill
 * ----------------------------------
 *
 * @ param : Quality_Group_Id
 * @ param : -
 */

 public function ClearQualitySkill()
{
	$_conds = array('success' => 0);
	if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) 
		AND $this -> URI -> _get_have_post('Quality_Group_Id') )
	{
		if( $result = $this -> {base_class_model($this)} -> _setClearQualitySkill(  $this -> URI -> _get_array_post('Quality_Group_Id') )) 
		{
			$_conds = array('success' => 1);
		}
	}
  
  __(json_encode($_conds));
	
}
 
 
 /*
 * @def : index default classes 
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
 
 
 


}

?>