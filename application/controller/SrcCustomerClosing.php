<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SrcCustomerClosing extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function SrcCustomerClosing()
 {
	parent::__construct();
	$this -> load -> model
	( 
		array
		(
			'M_SrcCustomerClosing', 'M_SetCallResult',
			'M_SetProduct','M_SetCampaign','M_SrcCustomerList',
			'M_SetResultQuality','M_SetResultCategory',
			'M_Combo'
		)
	);
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function setCallResult() 
{
	$_result = array( "setCallResult" => $this -> getCallResult( $this -> URI ->_get_post('CategoryId')) );
	if( is_array($_result)) {
		$this -> load ->view('src_closing_list/view_call_result',$_result);
	}
}
  
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCallResult($CategoryId = null )
 {
	$_conds = array();
	
	 foreach
	 ( 
		$this ->M_SetCallResult ->_getCallReasonId($CategoryId)  
			as $k  => $call  )
	{
		$_conds[$k] = $call['name'];
	}
	
	return $_conds;
 }
 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getProductId()
 {
	// $_conds = array();
	// foreach
	// ( 
		// $this ->M_SetProduct ->_getProductId()  
		// as $k  => $call )
	// {
		// $_conds[$k] = $call['name'];
	// }
	
	// return $_conds;
 }
 
  
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getResultQuality()
 {
	$_conds = array();
	foreach
	( 
		$this ->M_SetResultQuality ->_getQualityResult()  
		as $k  => $call )
	{
		$_conds[$k] = $call['name'];
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session ->_have_get_session('UserId') )
	{
		$_EUI  = array
		(
			'page' 			=> $this->M_SrcCustomerClosing->_get_default(), 
			'CampaignId' 	=> $this->M_SetCampaign->_get_campaign_name(),
			'GenderId' 		=> $this->M_SrcCustomerList->_getGenderId(),
			'Agent'			=> $this ->M_SrcCustomerList->_getAgent(),
			'cmp_active'	=> $this ->M_SrcCustomerList ->_get_active_campaign(),
			'ProductId' 	=> $this ->M_SrcCustomerClosing ->_get_product_category(), 
			'CallResult' 	=> self::_getCallResult(), 
			'ResultQuality' => self::_getResultQuality(),
			'QaStatus'		=> $this ->{base_class_model($this)}->_getQaStatus()
		);
		if( is_array($_EUI))
		{
			$this -> load ->view('src_closing_list/view_closing_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->M_SrcCustomerClosing ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->M_SrcCustomerClosing ->_get_page_number(); // load content data by pages 
		$_EUI['BackLevel'] = $this->M_SetResultQuality ->_getQualityBackLevel(); //
		
		
		// sent to view data 
		
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('src_closing_list/view_closing_list',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function ContactDetail()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$CustomerId = $this -> URI -> _get_post('CustomerId');
		if( !is_null($CustomerId) 
			&& $result_customers = $this->{base_class_model($this)}->_getDetailCustomer($CustomerId) )
		{
			$UI = array
			(
				'Customers' 		=> $result_customers,
				'Phones'			=> $this->{base_class_model($this)}->_getPhoneCustomer($CustomerId),
				'AddPhone' 			=> $this->{base_class_model($this)}->_getApprovalPhoneItems($CustomerId),
				'Product' 			=> $this->{base_class_model($this)}->_getAvailProduct($CustomerId),
				'CallNumber'		=> $this->{base_class_model($this)}->_getLastCallPhone($CustomerId),
				'CallCategoryId' 	=> $this->M_SetResultCategory->_getOutboundCategory(),
				'CallResultId' 		=> $this->_getCallResult($result_customers['CallReasonCategoryId']),
				'Combo' 			=> $this->_getCombo()
			);
			
			$this -> load -> view('src_closing_list/view_contact_main_detail',$UI);
		}
	}	
  }	
  
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function ViewQuestion()
 {
	if( $CustomerId = $this->URI->_get_64post('CustomerId') )
	{
		if( $UI = array('question'=> $this->{base_class_model($this)}->getViewQuestion($CustomerId) ) )
		{
			$this->load->view('src_closing_list/view_contact_question', $UI);
		}	
	}
 }
 
 
 
}
?>