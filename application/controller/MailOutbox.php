<?php
/*
 * @def 	: FaxContent
 * ------------------------------------
 * @param 	: array()
 */
 
class MailOutbox extends EUI_Controller
{

// constructor 

function MailOutbox()
{
	parent::__construct();	
	$this ->load->model(array(base_class_model($this),'M_Combo','M_Configuration'));
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function ResendMail()
{
  $_conds = array('success' => 0 );
  if( $this ->URI->_get_have_post('EmailOutboxId') )
  {
	if( $rs = $this -> {base_class_model($this)}->_setResendMail( $this -> URI->_get_array_post('EmailOutboxId')) )
	{
		$_conds = array('success' => 1);
	}
  }
  
  __(json_encode($_conds));
 
} 


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function PrintPreview()
{
  if( $this -> EUI_Session->_get_session('UserId') )
	{
		$rows = $this ->{base_class_model($this)} ->_getPrintPriview($this -> URI->_get_post('EmailOutboxId'));
		$this -> load ->view('mod_mail_outbox/view_mail_outbox_preview',array('content' => $rows));
	}
}	


public function DownloadAttachment()
{
	$this->db->select('*');
	$this->db->from('email_attachment_url');
	$this->db->where('EmailReffrenceId', $this->URI->_get_post('OutboxId'));
	if( $rows  = $this->db->get()->result_first_assoc() )
	{
		define('PATH_BATCH_EXPORT', str_replace('system','application', BASEPATH) );
		$BASE_EXCEL_FILE_NAME = PATH_BATCH_EXPORT ."temp/". basename($rows['EmailAttachmentPath']);
		
		if( copy($rows['EmailAttachmentPath'], $BASE_EXCEL_FILE_NAME)) 
		{
		  header("Pragma: public");
		  header("Expires: 0");
		  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		  header("Cache-Control: public");
		  header("Content-Description: File Transfer");
		  header("Content-Type: ". $rows['EmailAttachmentType']);
		  header("Content-Disposition: attachment; filename=". basename($BASE_EXCEL_FILE_NAME));
		  header("Content-Transfer-Encoding: binary");
		  header("Content-Length: " . filesize($BASE_EXCEL_FILE_NAME));
		  readfile($BASE_EXCEL_FILE_NAME); 
		  @unlink($BASE_EXCEL_FILE_NAME);
		}
		
	}
	else{
		exit('no file.');
	}
}

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }
 	
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function index()
{
  if( $this -> EUI_Session->_get_session('UserId')) 
  {
	$UI = array(
		'page' => $this -> {base_class_model($this)}->_get_default(),
		'combo' => $this -> _getCombo() );
	 $this -> load -> view('mod_mail_outbox/view_mail_outbox_nav',$UI);
   }	
}
	
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

function content()
{
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array
		( 
		  'fax_status' => $this->{base_class_model($this)} ->_getFaxStatus(),
		  'page'  => $this->{base_class_model($this)} ->_get_resource(),
		  'num'   => $this->{base_class_model($this)} ->_get_page_number() 
		 );
		$this -> load -> view('mod_mail_outbox/view_mail_outbox_list',$UI);
	}
}	

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

function DeleteFax()
{
	$_conds = array('success' => 0);
	$_conds = array('success' => $this -> {base_class_model($this)} -> _setDeleteFax( $this -> URI->_get_array_post('DataId') ));
	echo json_encode($_conds);
}
 
}