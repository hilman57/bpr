<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class MgtAssignment extends EUI_Controller
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function __construct()
 {
	parent::__construct();
	$this -> load -> model(array(base_class_model($this),
		'M_SetCallResult',
		'M_SysUser',
		'M_Combo',
		'M_SetCampaign'));
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this -> {base_class_model($this)} -> _get_default();
		if( is_array($_EUI) ) 
		{
			$this -> load -> view('mgt_assignment/view_assignment_nav',$_EUI);
		}	
	}	
 }
 

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((strtolower($keys)!='serialize') AND (strtolower($keys)!='instance') 
			AND (strtolower($keys)!='nstruct') AND (strtolower($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }

 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI = array (
			'Model' => $this -> {base_class_model($this)} ,
			'page' => $this -> {base_class_model($this)}->_get_resource(),
			'num' => $this -> {base_class_model($this)}->_get_page_number()
		);
		
		// sent to view data 
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('mgt_assignment/view_assignment_list',$_EUI);
		}	
	}	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function getAssignContent()
 {
	if( $this -> URI -> _get_have_post('CampaignId') )
	{
		$this->{base_class_model($this)}->_set_CampaignId($this->URI->_get_post('CampaignId') );
		$UI['CampaignNumber'] = $this->{base_class_model($this)}->getCampaignNumber();
		$UI['CampaignName'] = $this->{base_class_model($this)}->getCampaignName();
		$UI['CampaignId']	= $this->{base_class_model($this)}->getCampaignId();
		$UI['UserLevel'] = $this->{base_class_model($this)}->getLevelUser();
		$UI['JumlahData'] = $this->{base_class_model($this)}->JumlahData();
		$UI['SizePolicyData'] = $this->{base_class_model($this)}->SizePolicyData();
		$UI['DistribusiMode'] = $this->{base_class_model($this)}->DistribusiMode();
		$UI['ProductCategory'] = $this->{base_class_model($this)}->_get_product_category(0);
		$UI['DistribusiType'] = $this->{base_class_model($this)}->DistribusiType();
		$UI['CallResult'] = $this->_getReasultAssign();
		$UI['Users'] = $this->_getAgentAssign();
		$UI['params'] = $this -> URI->_get_all_request();
		
		
		if(is_array($UI)) 
		{
			$this -> load -> view('mgt_assignment/view_assignment_content',$UI);
		}	
	}
 }

/*
 * @ unit test  : cek CampaignId inbound Or OutboundGoals
 * 
 */
 
function CampaignType()
{
	$_conds = array('type' => 0 );
	if( $this -> EUI_Session-> _have_get_session('UserId') )
	{
		$_conds = array('type' => $this->{base_class_model($this)}->_getCampaignType($this -> URI->_get_post('CampaignId')));
	}
	
	__(json_encode($_conds));
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function getShowByLevel()
 {
	switch($this -> URI -> _get_post('DistribusiType') )
	{
		case 1 : self::_getShowManual();  break;
		case 2 : self::_getShowAutomatic();  break;
	}
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getShowManual()
 {
	if( $this -> URI -> _get_have_post('UserLevel') )
	{
		$LevelUser = $this -> URI -> _get_post('UserLevel');
		if( $LevelUser!=FALSE )
		{
			$EUI = array( 'Manual' => $this -> {base_class_model($this)} -> _get_manual_distribusi($LevelUser));
			$this -> load -> view('mgt_assignment/view_assignment_manual',$EUI);
		}	
	}	
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getShowAutomatic()
 {
	if( $this -> URI -> _get_have_post('UserLevel') )
	{
		$LevelUser = $this -> URI -> _get_post('UserLevel');
		if( $LevelUser!=FALSE )
		{
			$EUI = array( 'Manual' => $this -> {base_class_model($this)} -> _get_automatic_distribusi($LevelUser));
			$this -> load -> view('mgt_assignment/view_assignment_automatic',$EUI);
		}	
	}	
 }
 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 private function _getJSON( $JSON=null )
	{
		if( !is_null($JSON) )
		{
			return json_decode(str_replace("\\","", $JSON ),true);
		}	
	} 
	
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function AgentDistribusi()
 {
	$_conds = array('success' => 0, 'message' => '');
	
	if( $this -> URI -> _get_have_post('UserLevel') )
	{
		$data_post = array
		(
			'AssignData' 	 => $this->URI->_get_post('AssignData'),
			'CampaignId' 	 => $this->URI->_get_post('CampaignId'),
			'CampaignNumber' => $this->URI->_get_post('CampaignNumber'),
			'DistribusiMode' => $this->URI->_get_post('DistribusiMode'),
			'DistribusiType' => $this->URI->_get_post('DistribusiType'),
			'JumlahData' 	 => $this->URI->_get_post('JumlahData'),
			'UserLevel' 	 => $this->URI->_get_post('UserLevel'),
			'CustomerCity'	 => $this->URI->_get_post('CustomerCity'),
			'GenderId'		 => $this->URI->_get_post('GenderId'),
			'StartAge'		 => $this->URI->_get_post('StartAge'),
			'StartDate' 	 => $this->URI->_get_post('StartDate'),
			'EndDate' 		 => $this->URI->_get_post('EndDate'),
			'ProductCategory'=> $this->URI->_get_post('ProductCategory'),
			'UserSelectId' 	 => $this->URI->_get_array_post('UserSelect'),
			'UserSelect'	 => $this->_getJSON($this->URI->_get_post('UserSelectId'))
		);
		
		$_getMsg = $this->{base_class_model($this)}->_setAgentDistribusi($data_post);
		if( is_array($_getMsg) 
			AND !is_null($_getMsg))
		{
			$_conds = array('success' =>1, 'message' => $_getMsg );
		}
	}
	
	__(json_encode($_conds));
 }
 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 
function AgentReAssignment()
{
	
	$_conds = array('success' => 0, 'message' => '');
	if( $this -> URI -> _get_have_post('UserLevel') ) 
	{
		$data_post = array
		(
			'fltCallResultId'=> $this->URI->_get_array_post('CallResultId'),
			'fltUserId'	 	 => $this->URI->_get_array_post('UserId'),
			'AssignData' 	 => $this->URI->_get_post('AssignData'),
			'CampaignId' 	 => $this->URI->_get_post('CampaignId'),
			'DistribusiMode' => $this->URI->_get_post('DistribusiMode'),
			'DistribusiType' => $this->URI->_get_post('DistribusiType'),
			'JumlahData' 	 => $this->URI->_get_post('JumlahData'),
			'UserLevel' 	 => $this->URI->_get_post('UserLevel'),
			'UserSelectId' 	 => $this->URI->_get_array_post('UserSelect'),
			'UserSizeData'	 => $this->_getJSON($this->URI->_get_post('UserSizeData'))
		);
		
		$_getMsg = $this -> {base_class_model($this)} -> _setReAssignment($data_post);
		
		if( is_array($_getMsg) 
			AND !is_null($_getMsg))
		{
			$_conds = array('success' =>1, 'message' => $_getMsg );
		}
	}
	
	__(json_encode($_conds));
} 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getAgentAssign()
{
	$_conds = array();
	$Agent = $this -> M_SysUser->_get_user_by_login();
	
	$no=0;
	foreach( $Agent as $k => $rows ) 
	{
		$_conds[$k] = $rows['full_name'];
	}
	
	return $_conds;
} 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getReasultAssign()
{
	$_conds = array();
	$CallResult = $this -> M_SetCallResult->_getCallReasonId(null);
	$Sale = $this -> M_SetCallResult->_getInterestSale();
	
	$_conds['0'] = 'New Data';
	foreach($CallResult as $k => $rows )
	{
		if( !in_array($k,array_keys($Sale)) ){
			$_conds[$k] = $rows['name'];
		}
	}
	
	return $_conds;
} 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
function _getReasultAll()
{
	$_conds = array();
	$CallResult = $this -> M_SetCallResult->_getCallReasonId(null);
	
	$_conds['0'] = 'New Data';
	foreach($CallResult as $k => $rows )
	{
		$_conds[$k] = $rows['name'];
	}
	
	return $_conds;
} 

  
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function ShowData(){
	
 $_data_array = array('success' => 0, 'counter' =>0);
 
 if( $this -> URI->_get_have_post('UserId') AND	$this -> URI->_get_have_post('CampaignId') )
{
	$UI = array( 
		'UserId' => $this ->URI ->_get_array_post('UserId'),
		'CallResultId' => $this -> URI ->_get_array_post('CallResultId'),
		'CampaignId' => $this -> URI ->_get_post('CampaignId')
	);
	
	$_data_array = array('success' => 1, 
		'counter' => $_data_array = $this -> {base_class_model($this)}->_getShowData($UI) );
}

	
	__(json_encode($_data_array));
}
  
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function ViewAgentData()
 {
	if( $this -> URI -> _get_have_post('CampaignId') )
	{
		$this->{base_class_model($this)}->_set_CampaignId($this->URI->_get_post('CampaignId') );
		$UI['CampaignNumber'] = $this->{base_class_model($this)}->getCampaignNumber();
		$UI['CampaignName'] = $this->{base_class_model($this)}->getCampaignName();
		$UI['CampaignId']	= $this->{base_class_model($this)}->getCampaignId();
		$UI['UserLevel'] = $this->{base_class_model($this)}->getLevelUser();
		$UI['JumlahData'] = $this->{base_class_model($this)}->JumlahData();
		$UI['SizePolicyData'] = $this->{base_class_model($this)}->SizePolicyData();
		$UI['DistribusiMode'] = $this->{base_class_model($this)}->DistribusiMode();
		$UI['ProductCategory'] = $this->{base_class_model($this)}->_get_product_category(0);
		$UI['DistribusiType'] = $this->{base_class_model($this)}->DistribusiType();
		$UI['CallResult'] = $this->_getReasultAssign();
		$UI['Users'] = $this->_getAgentAssign();
		$UI['params'] = $this -> URI->_get_all_request();
		
		// $this->{base_class_model($this)}->_set_CampaignId($this->URI->_get_post('CampaignId') );
		// $UI['CampaignNumber'] = $this->{base_class_model($this)}->getCampaignNumber();
		// $UI['CampaignName'] = $this->{base_class_model($this)}->getCampaignName();
		// $UI['UserLevel'] = $this->{base_class_model($this)}->getLevelUser();
		// $UI['JumlahData'] = $this->{base_class_model($this)}->JumlahData();
		// $UI['DistribusiMode'] = $this->{base_class_model($this)}->DistribusiMode();
		// $UI['ProductCategory'] = $this->{base_class_model($this)}->_get_product_category(0);
		// $UI['DistribusiType'] = $this->{base_class_model($this)}->DistribusiType();
		// $UI['CallResult'] = $this->_getReasultAssign();
		// $UI['Users'] = $this->_getAgentAssign();
		
		if(is_array($UI))
		{
			$this -> load -> view('mgt_assignment/view_agent_content',$UI);
		}	
	}	
 }
 
  
 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 function OutboundGoals()
 {
	__(json_encode( 
		$this -> {base_class_model($this)} -> _getOutboundGoals() 
	));
	
 } 
 
 
 // ShowDataDetail
 
 function ShowDataDetail()
 {
	$UI = array( 
		'UserId' => $this ->URI ->_get_array_post('UserId'),
		'CallResultId' => $this -> URI ->_get_array_post('CallResult'),
		'CampaignId' => $this -> URI ->_get_post('CampaignId')
	);
	
	
	$_list_views = $this -> {base_class_model($this)}->_getShowDataDetail($UI);
	$_list_field = array_keys($this -> {base_class_model($this)}->_getHideTables());
	array_push($_list_field, 'CallReasonId','SellerId');
	
	$viewer  = array("views_data" => $_list_views, "views_field" => $_list_field,'combo'=>self::_getCombo(),'User' => $this ->M_SysUser);
	$this -> load -> view("mgt_assignment/view_show_data_detail", $viewer);
		
 }
 
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function getUserByLevelLogin()
{
  $_user_login = array();
  if( $this -> URI -> _get_have_post('UserLevel'))
  {
	if( $_user_login = $this -> M_SysUser->_getUserLevelGroup( $this -> URI->_get_post('UserLevel')))
	{
	  __(form()->listCombo('ListUserId',null, $_user_login, NULL, array('click' => 'Ext.DOM.AssignPageContent();'),NULL));
	}
 }
 
} 
 
 
 /*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CheckAmountOfData()
{
	if( $this->EUI_Session->_have_get_session('UserId')==FALSE ){
		die('No parmater.');
	}	

	$UserId = $this->URI->_get_array_post('UserId');
	$HandlingType = $this->URI->_get_post('HandlingType');
	$CampaignId = $this->URI->_get_post('CampaignId');
	$CampaignNumber = $this->URI->_get_post('CampaignNumber');

	if(($UserId==TRUE) AND (is_array($UserId)) )
	{
		$recsouce = array
		( 
			'VarsUserId' 	 => $UserId,
			'UserId' 		 => $this->M_SysUser->_getUserLevelGroup($HandlingType),
			'view_rows_PPerCampaign' => $this->{base_class_model($this)}->_getCheckAmountOfDataPerCampaign($UserId, $HandlingType, $CampaignNumber),
			'view_rows_CPerCampaign' => $this->{base_class_model($this)}->_getCheckAmountCustomerPerCampaign($UserId, $HandlingType, $CampaignNumber),
			'CallReasonId' 	 => $this->_getReasultAll(),
			'CampaignCode'	 => $this->{base_class_model($this)}->getCampaignCode($CampaignId)
		);
		
		$this->load->view("mgt_assignment/view_amount_data", $recsouce);
	}
}

/** ReleaseAssignment **/

public function ReleaseAssignment()
{
 
 $conds = array( "success" => 0, "counter"=>0 );
 
 $UserId = $this->URI->_get_array_post('UserId');
 $AsgData = (INT)$this->URI->_get_post('AssgData');
 
 if( is_array($UserId) 
	AND !is_null($UserId) AND ($AsgData > 0) )
 {
	$this->db->select("c.*");
	$this->db->from("t_gn_campaign a");
	$this->db->join("t_gn_debitur b "," b.CampaignId=a.CampaignId");
	$this->db->join("t_gn_assignment c "," c.CustomerId=b.CustomerId");
	$this->db->where('( b.CallReasonId = 0 OR b.CallReasonId IS NULL )', '', FALSE);
	$this->db->where("a.CampaignId", $this->URI->_get_post('CampaignId') );
	$this->db->where_in("c.AssignSelerId", $UserId ); 
	$this->db->order_by('c.AssignId','DESC');
	$this->db->limit($AsgData,0);
	
	$count = 0;
	
	foreach( $this->db->get() -> result_assoc() as $rows  )
	{
		if( $rows['AssignId'] > 0 )
		{
			$this->db->set('LogAssignmentId', $rows['AssignId']);
			$this->db->set('LogUserId',$rows['AssignSpv']); 
			$this->db->set('LogAssignUserId',$rows['AssignSelerId']);
			$this->db->set('LogCreatedDate',date('Y-m-d H:i:s')); 
			$this->db->set('LogTrfMenu','ASSIGNMENT_DATA.GET_CALLER_DATA.UNASSIGN_DATA');
			$this->db->insert('t_gn_distribusi_log');
			
			if( $this->db->affected_rows() > 0 )
			{
				$this->db->set('AssignSelerId', 'NULL', FALSE);
				$this->db->set('AssignDate',date('Y-m-d H:i:s') );
				$this->db->set('AssignMode', 'RLD');
				$this->db->where('AssignId',$rows['AssignId']);
				$this->db->update('t_gn_assignment');
				if( $this->db->affected_rows() > 0 )
				{
					$count++;
				}
			}
		}
	}
 }
 	
/// ------------------------------------------
	
 if( $count > 0 ){
	$conds = array( "success" => 1, "counter"=> $count );
 }	
		
echo json_encode($conds);

//STOP::HERE 

}


// end of class 
 
}
?>