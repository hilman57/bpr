<?php
// 08 Juni 2020 - Luqmanul Hakim	
/*
 * @ package	Class Controller ReportInventory
 * @ auth 		Unknown User
 */
class ReportInventory extends EUI_Controller 
{

	/*
	 * @ package function construct
	 */

	function __construct()
	{
		
		parent::__construct();
		$this->load->model(array(base_class_model($this),'M_Report'));
		$this->load->helper(array('EUI_Object','EUI_Report'));
	}


	/*
	 * function index
	 */

	public function index()
	{
		$this->load-> view("rpt_inventory/page/report_inventory_nav", array());

	}

	public function ShowReport() 
	 {
		
	 $out = new EUI_Object( _get_all_request() );
	 
	 
	 $data['start_date1'] = $out->get_value('start_date1');	
	 $data['end_date1'] = $out->get_value('end_date1');  
	 $data['start_date2'] = $out->get_value('start_date2');	
	 $data['end_date2'] = $out->get_value('end_date2');  
	 $this->load->view('rpt_inventory/html/report_inventory',$data);
		
		
	 }
	  public function ShowExcel() 
	{

	 Excel() -> HTML_Excel(urlencode("ReportInventory").''.time());
		
	 $out = new EUI_Object( _get_all_request() );
	 $data['start_date1'] = $out->get_value('start_date1');	
	 $data['end_date1'] = $out->get_value('end_date1');  
	 $data['start_date2'] = $out->get_value('start_date2');	
	 $data['end_date2'] = $out->get_value('end_date2');  
	 $this->load->view('rpt_inventory/html/report_inventory',$data);
		
		
	 }
 

}
?>