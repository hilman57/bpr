<?php
if( count($argv) < 3 ) die('Console only');

/**
 * # definer static public of path directory 
 * # process this class . this run via Console .sh 
 * # no handle manual process by humman .
 * # author	 : omens < jombi_par@yahoo.com>
 * # create  : 20141003
 
 Additional :
 
 20-11-2014
 
 - line : 261 function _getPolicyNo(), param start 9 => 8
 
 **/
 
define('FTP_PROCESS', '/opt/enigma/webapps/collection/index.php FTPSchedule Process');

/**
 * @ def 		: M_FTPSchedule
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: M_FTPSchedule --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 

class FTPSchedule extends EUI_Controller
{

/* @ private parameter this class **/

private $primary_keys = null;

/* @ aksessor / __constructor -> the class 	**/

public function FTPSchedule() 
{
	parent::__construct();
	$this->primary_keys ='System_id';
	$this->load->model(array('M_FTPSchedule','M_Template','M_Tools','M_SysUser'));
}

/**
 * @ def 		: default read by controller EUI_Framework::PHP
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: index --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
 public function index() 
 {
	$rs = $this->{base_class_model($this)}->getFTP();
	if( $rs = $this->{base_class_model($this)}->getFTP()) 
	{
		foreach( $rs as $FTP_ID => $rows )
		{
			if( $FTP_ID )
			{
				$command = "php ". FTP_PROCESS . " $FTP_ID";
				system($command);
			}
		}
	}
 }
 
/**
 * @ def 		: MimeType & get type extenion from 
 *				  grabing file on directry 
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: M_FTPSchedule --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
public function MimeType($file = null )
{	
 $nulls = null;
 if( !is_null( $file) ) 
 {
	$file = explode(".", $file);
	if( is_array($file) ) 
		$nulls = $file[count($file)-1]; 
 }
  return $nulls; 
 }
 
/**
 * @ def 		: Process will run argv by index on runner 
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: $ global parameter on PHP 
 * @ params 	: -
 * @ example    : -
 **/
 
 public function Process()
 {
	global $argv;
	
	$cnt = count($argv);
	$lstftp = null;
	
	if( ($cnt > 3 ) AND ($FTP_Id = $argv[3]) ) 
	{
		if( $FTProws = $this->{base_class_model($this)}->getFTP($FTP_Id) )
		{
			
			$d = $FTProws[$FTP_Id];
			
			if( is_array($d) 
				AND !is_null($d) )
			{
				$ls_ftp  = (isset($d['incoming']) ? scandir($d['incoming']) : null );
				
				$i = 0;
				foreach($ls_ftp as $k => $listname ) 
				{
					if( !is_dir($listname) 
						AND preg_match("/{$d['project']}/i",$listname) ) 
					{
						if( (self::MimeType($d['controll']) == self::MimeType($listname))  
							AND !file_exists($d['history'] .'/'. $listname) ) 
						{
							$lstftp[$FTP_Id][$i]['ctl']= $listname; 
						}
						$i++;
					}						
				}
				
				if(is_array($lstftp) ) {
					self::FTP_Will_Read($d, $lstftp[$FTP_Id]);
				}	
			}
		}
	}
	
	exit;
 }
 
/**
 * @ def 		: FTPMoving
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: FTPMoving --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/

public function FTPMoving($from = null, $dstn = null ) 
{
	$command = " mv $from $dstn";
	if( system($command) ){
		@unlink($from);
	}
} 
 
/**
 * @ def 		: FTPLibrary
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: FTPLibrary --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
public function FTPLibrary($flc = null, $Tpl=0 ) 
{
	$spr = "|";
	if(class_exists('M_Template') ) 
	{
		if( $trs = $this->M_Template->_getDetailTemplate($Tpl) ) {
			$spr = $trs['TemplateSparator'];	
		}
	}	
	
	TextImport()->ReadText($flc);
	TextImport()->setDelimiter($spr);	
} 

/**
 * @ def 		: FTPSource
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: FTPSource --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/
 
protected function _get_source_explode( $system_id = NULL )
{
	$config = NULL;
	$explodes = array();
	
	if( !is_null($system_id) )
	{
		$system_id = explode("-", $system_id);
		foreach($system_id as $i => $values ) 
		{
			$explodes[$i] =  trim($values);
		}
	}
	
	if( is_array($explodes) )
	{
		$config['link_id'] = $explodes[0];
		$config['cif_number'] = $explodes[1];
		$config['policy_number'] = $explodes[2];
		
	}
	
	
	return $config;
}

/**
 * @ def 		: FTPSource
 * ---------------------------------------------------------------------------------------------------
 * 
 * @ params  	: FTPSource --> class
 * @ params 	: $autonumber
 * @ example    : get by fields ID / Campaign ID
 **/

public function FTPSource($flc  = null ) 
{
	$SourceId = 0;
	
	$this->db->set('FTP_UploadStartDateTs', date('Y-m-d H:i:s'));
	$this->db->set('FTP_UploadDateTs', date('Y-m-d H:i:s'));
	$this->db->set('FTP_UploadFilename', $flc);
	$this->db->insert('t_gn_upload_report_ftp');
	if( $this->db->affected_rows() > 0 ) {
		$SourceId = $this->db->insert_id();
	}
	return $SourceId;
}

/*
 * @ def 	: get client number from link-id + policy number  on maxleng  =   
 * ------------------------------------------------------------------------------------------
 *
 * @ system Id : 3400649025634726
    $ link ( 3 ) 
    $ CIF  ( 4006490 )
	$ policy ( 25634726 )
	
 * @ param	: attribute data  : array() ..
 * @ param	: controll file will process array()..
 */

private function _getLinkID( $SystemId = null , $start = 0, $end =1 )
{
	$source = self::_get_source_explode( $SystemId );
	$conds = FALSE;
	if( !is_null($SystemId) AND ($SystemId!='') )  
	{
		//$conds = substr($SystemId, $start, $end);
		$conds = $source['link_id'];
		
	}
	return $conds;
}
 
/*
 * @ def 	: get client number from link-id + policy number  on maxleng  =  
 * ------------------------------------------------------------------------------------------
 *
 * @ param	: attribute data  : array() ..
 * @ param	: controll file will process array()..
 */  
 
private function _getClientNo( $SystemId = null )
{
	$conds = NULL;
	if( !is_null($SystemId) AND ($SystemId!='') ) 
	{
		$conds = $this->_getLinkID($SystemId);	
		$conds.= $this->_getCIFNumber($SystemId);	
		
	}
	
	return $conds;
}
 
/*
 * @ def 	: get client number from link-id + policy number  on maxleng  =  
 * ------------------------------------------------------------------------------------------
 *
 * @ param	: attribute data  : array() ..
 * @ param	: controll file will process array()..
 */  
 
private function _getPolicyNo( $SystemId = null, $start = 8 )
{
	$source = self::_get_source_explode( $SystemId );
	$conds = FALSE;
	if( !is_null($SystemId) AND ($SystemId!='') )  
	{
	  /* $conds = substr($SystemId, $start, strlen($SystemId)); **/
	  
		$conds = $source['policy_number'];
	}	
	
	return $conds;
}
 
/*
 * @ def 	: get customers number / CIF NUmber  = from system ID   
 * ------------------------------------------------------------------------------------------
 *
 * @ param	: attribute data  : array() ..
 * @ param	: controll file will process array()..
 */

private function _getCIFNumber( $SystemId = null, $start = 1, $end = 7 )
{
	$source = self::_get_source_explode( $SystemId );
	$conds = FALSE;
	if(!is_null($SystemId) AND ($SystemId!='')) 
	{
	
	/** $conds = substr( $SystemId, $start, $end); **/
		
		$conds = $source['cif_number'];	
	}
	
	return $conds;
}
 
/*
 * @ def 	: FTP_Will_Read with assum data from text field 
 * ------------------------------------------------------------------------------------------
 *
 * @ param	: attribute data  : array() ..
 * @ param	: controll file will process array()..
 */
 
public function FTP_Will_Read( $fs = null, $ls )
 {
	$as = self::MimeType($fs['filetype']);
	$fd = $this->M_Template->_getTemplateRows($fs['template']);
	$ts = $this->M_Template->_getDetailTemplate($fs['template']);
	
	foreach( $ls as $ky => $rs ) 
	{ 
		$compile = array(); 
		$bs = self::MimeType($rs['ctl']);
		$cs = str_replace($bs, $as, $rs['ctl']);
		
		if((self::MimeType($cs) == self::MimeType($fs['filetype'])) AND ( file_exists($fs['incoming'] .'/'. $cs )==TRUE )  
			AND ( file_exists( $fs['history'] .'/'. $cs )!=TRUE ) )
	   {
			self::FTPLibrary($fs['incoming'] .'/'. $cs, $fs['template']);
			self::FTPMoving($fs['incoming'] .'/'. $rs['ctl'] , $fs['history'] .'/'. $rs['ctl'] );
			
			$SourceId = $this->FTPSource($fs['history'] .'/'. $cs);
			$fs['sourceid'] = $SourceId;
			$chd = TextImport()->getHeader();
			
			if( COUNT($fd) == COUNT($chd))
			{
				$ln = 2; $si = 1; 
				$sn = TextImport()->getCount(); 
				
				/* ************** standar cek && ricek ********************************************************/	
				
				while( $ln<=$sn ) // ---> next process 
				{
					if(in_array($this->primary_keys, array_values($fd)) )
					{
						$col = 0;
						foreach( $fd as $n => $labels )
						{
							$field_values  = trim(TextImport()->getValue($ln, $n));
						    if($field_values!=='') 
							{
							 
						/* ************** standar cek && ricek ********************************************************/	
						
								$vs = $this->M_Tools->__tools_format( $labels, $field_values );
								if( !is_null($vs) ){
									$compile[$si][$labels] = $this->M_Tools->__tools_format( $labels, $field_values);
								}
								
						/* ************** standar cek && ricek ********************************************************/
								
								if($labels == $this->primary_keys )
								{
									$compile[$si]['CIFNumber'] = $this->_getCIFNumber($field_values);
									$compile[$si]['CustomerNumber'] = $this->_getCIFNumber($field_values);
									$compile[$si]['Policy_Sales_Number'] = $this->_getPolicyNo($field_values);
									//$compile[$si]['Client_No'] = $this->_getClientNo($field_values);
								}
							}
						   
						 /* ************** add field not in template ********************************************************/
						 
						   $compile[$si]['BukcetSourceId'] = $SourceId;
						   $compile[$si]['UploadId'] = $fs['uploadid'];
						   $compile[$si]['CustomerUploadedTs'] = date('Y-m-d H:i:s');
						}
						$si++; $ln++; 
					}
				}
				
				/** then Proces send to model **/
				$fs['tablename'] = $ts['TemplateTableName'];
				
				if( is_array($fs) 
					AND $rs = $this -> {base_class_model($this)}->_setWillUpload($compile, $fs) ) 
				{
				
				/** set to assignment && customers **/
				
					$this->{base_class_model($this)}->FTPCopyCustomers($fs, $rs);	
					$this->{base_class_model($this)}->FTPCopyPolicyDetail($fs, $rs);
					
				/** then insert to ftp report procees **/
				
					$this->db->set("FTP_UploadRows", $rs['totals']);
					$this->db->set("FTP_UploadSuccess",$rs['success']);
					$this->db->set("FTP_UploadFailed",$rs['failed']);
					$this->db->set("FTP_UploadType",'FTP');
					$this->db->set("FTP_UploadEndDateTs",date('Y-m-d H:i:s'));
					
				/** set source ID ***/
					
					$this->db->where("FTP_UploadId", $SourceId);
					$this->db->update("t_gn_upload_report_ftp");
					if( $this->db->affected_rows() > 0 ){
						self::FTPMoving($fs['incoming'] .'/'. $cs , $fs['history'] .'/'. $cs);
					}


					$this->load->model(array('M_WriteReportUpload'));
					$this->M_WriteReportUpload->doSendUploadReport($fs['uploadid'], $SourceId);
				}
			}	
		}
	}
	// stop end data 
 }
 
 
 
}
?>