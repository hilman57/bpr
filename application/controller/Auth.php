<?php

if( !defined('WCALL_PROJECT_ID') ) define('WCALL_PROJECT_ID', 1);

/*
 * EUI Controller  
 *
 
 * Section  : < Auth > first load vail web home  website application enjoy its.
 * author 	: razaki team  
 * link		: http://www.razakitechnology.com/eui/controller 
 */
 
class Auth extends EUI_Controller
{


function __construct(){
	parent::__construct();
	$this->load->helper(array('EUI_Object'));
}
/*
 *# Auth Controller 
 *# main index Auth login Users
 */


/*
 *# Auth Controller 
 *# main index Auth login Users
 */
 
 public function index() 
{
  /** handle of session expired password  **/
  if( $this->EUI_Session->_get_session('UserId')) {
	 redirect("Main/?login=(true)");	
  }
 /** handle of session expired password  **/
  else if($this->EUI_Session->_get_session('old_password')) {
	$this->load->Model('M_Website');
	$form=EUI_Form::get_instance(); 
	$this->load->layout( $this->Layout->base_layout().'/UserLogExpired',$this->M_Website->_web_get_data());	
  }
  /** handle of session expired password  **/
  else {
	$this->load->Model('M_Website');
	$form = EUI_Form::get_instance(); 
	$this->load->layout($this->Layout->base_layout().'/UserLogin',$this -> M_Website -> _web_get_data());
	// print_r($this->Layout->base_layout().'/UserLogin');
  }
  
}

// ------------ match  deb_agent = assign_selerid  ------------------------------------
/* updaet data yang tidak macth antara debitur dan Assignment 
 * kepentingan All data .
 * @omens / 2016-04-26
 */

 
function SMSSpam() 
{
 $sql = " DELETE FROM serv_sms_inbox WHERE Sender IN ( 
          SELECT serv_sms_blocking.block_no FROM serv_sms_blocking )";
 if( $this->db->query($sql) ){
	return TRUE;
	}
 return false;
 
}

// ------------ match  deb_agent = assign_selerid  ------------------------------------
/* updaet data yang tidak macth antara debitur dan Assignment 
 * kepentingan All data .
 * @omens / 2016-04-26
 */
 
 function MatchAccount()
{
	$sql  = "select b.deb_id, b.deb_acct_no, a.CustomerId, a.AssignSelerId,
			 (select tg.id from t_tx_agent tg where tg.UserId=a.AssignSelerId) as AsgUser,
			 b.deb_agent
			 from t_gn_assignment a inner join t_gn_debitur b on a.CustomerId=b.deb_id
			 where b.deb_agent NOT IN('AGUS','COMPLAIN','CITRA','GABY','LUNAS','nyoto')
			 having AsgUser <> deb_agent ";
			
	$rs = $this->db->query($sql);
	if( $rs->num_rows() > 0 ) 
	foreach( $rs->result_assoc() as $rows )
	{
		if( is_array($rows) 
			AND isset($rows['AsgUser']) ) 
		{
			
			$sql = " UPDATE t_gn_debitur a SET a.deb_agent = '{$rows['AsgUser']}'
					 WHERE a.deb_id={$rows['deb_id']} AND a.deb_agent='{$rows['deb_agent']}' LIMIT 1";	
			if( $this->db->query($sql) ){
				echo ".";
			}
		}		
	}	
}

// ------------ reset jika lupa logout ------------------------------------
/*
 * Update jika user lupa logout dari appliaksi .
 * @omens / 2016-04-26
 */
 
function ResetLogin()
{
	$sql ="UPDATE t_tx_agent a SET a.logged_state=0, a.ip_address=NULL WHERE a.logged_state=1";
	if( $this->db->query($sql) ){
		return true;
	}
	return false;
}

 // ------------ reset blocked by system ------------------------------------
/*
 * logout by system
 * @author : didi gantengs
 * @date : 2019-01-24
 * @link( http://it-underground.web.id)
 */
 
function Blockeds()
{
	session_start();
	$id = $this -> EUI_Session -> _get_session('UserId');
	
	session_start();
	
	$this -> load -> model('M_User');
	
	if($this -> EUI_Session -> _get_session('UserId') )
	{
		$this -> M_User -> _setUpdeteBlocked($id); 
		$this -> M_User -> _set_update_activity('LOGOUT BY SYSTEM', $this -> EUI_Session -> _get_session('UserId') );
		$this -> EUI_Session -> _destroy_session();
	}

	if( $this -> EUI_Session -> _get_session('UserId') !=TRUE )
	{
		redirect("Auth/?login=(false)");
	}
}

/*
 *# action login if OK  testing Only
 *# Login Method not constructor 
 */
 
function Login()
{
	$_conds = array('success' => 0);
	
	if( $this->URI->_get_have_post('password') 
		OR $this->URI->_get_have_post('username'))
	{
		$this->load->model('M_User');
		$_sent_array = array ( 
			'username'=> $this -> URI -> _get_64post('username'), 
			'password'=> $this -> URI -> _get_64post('password') 
		);
		
	/*
     * @ pack : cek object model  --> 
	 */
	 
		if(is_object($this->M_User)) 
		{
		 
		 $_IP_REAL_ADDRESS = _getIP();
		 $rows = $this->M_User->getLoginUser($_sent_array); 	
		 $ExpiredPassword = $this->M_User->_get_expired_password($_sent_array);
	
	/*
     * @ pack : cek user ON DB --> 
	 */
		if( ( FALSE == $rows) and !is_array($rows) ){
			echo json_encode(array('success' => 0));
			return false;
		}
			
	/*
     * @ pack : cek object model  --> 
	 */
		if((empty($rows['ip_address']) 
			OR $rows['ip_address']==_getIP() OR $rows['handling_type']==USER_ROOT )) 
		{
			
	/*
     * @ pack : cek user on project setting if project more then one project
	 */		
			$this->M_User->setWorkProject($rows['UserId']); 
			if(!in_array( WCALL_PROJECT_ID, _get_session('ProjectId'))){
				$this->EUI_Session->_destroy_session();
				echo json_encode(array('success' => 3)); // domain diffren in WCALL
				return false;	
			}
			
	/*
     * @ pack : cek if user user_state is  zero = user block 
	 */		
			if( (isset($rows['user_state'])) AND ($rows['user_state'] == 0) ){
				echo json_encode(array('success' => 5 )); // is user blocked OR user_state = 0
				return false;
			 }
			 
	/*
     * @ pack : expired password 
	 */				 
			if(!is_null($ExpiredPassword) AND is_array($ExpiredPassword) ){
				$this->EUI_Session->_destroy_session();
				$this->EUI_Session->_set_session('old_password', $ExpiredPassword['old_password']);
				$this->EUI_Session->_set_session('old_user_agent', $ExpiredPassword['user_agent']);
				$this->EUI_Session->_set_session('old_real_password', $ExpiredPassword['real_password']);
				echo json_encode(array('success' => 4));
				return false;
			}
	 /*
     * @ pack : sesion start with succes to login 
	 */			  
			session_start();  
			$this->EUI_Session->_set_session('UserId',$rows['UserId']);
			$this->EUI_Session->_set_session('Username',$rows['id']);
			$this->EUI_Session->_set_session('KodeUser',$rows['code_user']);
			$this->EUI_Session->_set_session('Fullname',$rows['full_name']);
			$this->EUI_Session->_set_session('OnlineName',$rows['init_name']);
			$this->EUI_Session->_set_session('ProfileId',$rows['profile_id']);
			$this->EUI_Session->_set_session('GroupId',$rows['group_id']);
			$this->EUI_Session->_set_session('GroupName',$rows['GroupName']);
			$this->EUI_Session->_set_session('HandlingType',$rows['handling_type']);
			$this->EUI_Session->_set_session('AgencyId',$rows['agency_id']);
			$this->EUI_Session->_set_session('SupervisorId',$rows['spv_id']);
			$this->EUI_Session->_set_session('TeamLeaderId',$rows['tl_id']);
			$this->EUI_Session->_set_session('SeniorLeaderId',$rows['stl_id']);
			$this->EUI_Session->_set_session('ManagerId',$rows['mgr_id']);
			$this->EUI_Session->_set_session('Password',$rows['password']);
			$this->EUI_Session->_set_session('LoginIP',$_IP_REAL_ADDRESS);
			$this->EUI_Session->_set_session('UserState',$rows['user_state']);
			$this->EUI_Session->_set_session('Telphone',$rows['telphone']);
			$this->EUI_Session->_set_session('LastUpdate',$rows['last_update']);
			$this->EUI_Session->_set_session('AccountManager',$rows['act_mgr']);
			$this->EUI_Session->_set_session('QualityHead',$rows['quality_id']);
			
	/* 
	 * @ pack : cek again sesion exist Or create session TRUE 
	 */
			if( $this->EUI_Session->_have_get_session('UserId') )
			{
				$this->EUI_Session->_set_session('LastLogin', $this->M_User->_get_last_login());
				if($this->M_User->_set_update_activity('LOGIN',_get_session('UserId'))){
					$this->M_User->_setUpdateLastLogin(1);
				}
				
				$_conds = array('success' => 1); // succes pass parameter 
				
			 }
			 
	  // ok set session 
	  
		 } else {
			$_conds = array('success' =>2, 'location'=>$rows['ip_address']); // detected login on other location 
		  }
		  
		// end of pack login logic ..  
	  }
	}
	
	echo json_encode($_conds);
}	

/**
 ** user logout methode  if succces  logout then 
 ** redirect to index for cek session data available
 **/
 
function Logout()
{
	session_start();
	
	$this -> load -> model('M_User');
	
	if($this -> EUI_Session -> _get_session('UserId') )
	{
		$this -> M_User -> _setUpdateLastLogin(0); 
		$this -> M_User -> _set_update_activity('LOGOUT', $this -> EUI_Session -> _get_session('UserId') );
		$this -> EUI_Session -> _destroy_session();
	}
	
	if( $this -> EUI_Session -> _get_session('UserId') !=TRUE )
	{
		redirect("Auth/?login=(false)");
	}
}


/* update password user if user click **/


 public function UpdatePassword()
{
 
 $this->load->model('M_User'); 

 $_conds = array('success'=>0);
 $User = array();
 $params = $this->URI->_get_all_request(); 
 
 if( is_array($params) )
 {
	$old_password = MD5($params['curr_password']); 
	$new_password = MD5($params['new_password']);
	
	if( $old_password == $new_password ){
		$_conds = array('success'=>2); // password must be diffrent 
	}
	else 
	{
		$is_password_used = $this->M_User->_get_select_used_password(_get_session('Username'), $new_password);
		if( $is_password_used ){
			$_conds = array('success'=>3);	// pass is has used 
		} else {
		
			$User['Username'] = _get_session('Username');
			$User['curr_password'] = $old_password;
			$User['new_password'] = $new_password;
			if( $this->M_User->_setUpdatePassword($User) ) 
			{
				$_conds = array('success'=>1); // OK 
			}
		}
	}
 }
	
  __(json_encode($_conds));
}

/* update password user if user click **/

 public function UpdateExpiredPassword()
{
  $conds = array('success'=>0 );
  
 /* get all compare string **/
  $compare_password = MD5($this->URI->_get_64post('password'));
  
  if( _get_session('old_password') == $compare_password ){
	 $conds = array('success'=>2);
  } else {
	
	
// @ pack : load list user password 
	
	$this->load->model('M_User'); 
	$User['Username'] = $this->URI->_get_64post('username');
	$User['curr_password'] =  $this->EUI_Session->_get_session('old_password');
	$User['new_password'] = MD5($this->URI->_get_64post('password'));
	 
	 
// @pack :  get has been used of password  

	$is_password_used = $this->M_User->_get_select_used_password($User['Username'], $User['new_password'] );
	if( $is_password_used ) 
	{
		$conds = array('success'=>3);	
	}
	else 
	{
		 if($this->M_User->_setUpdatePassword($User))
		 {
			$this->EUI_Session->_destroy_session();
			$conds = array('success'=>1);
		 }
	 }
  }

	echo json_encode($conds);
  
} 

/*
 * @ pack : Refresh write log user 
 */
 
 public function Refresh()
{
  $conds = array('success'=> 0);
  $_IP_REAL_ADDRESS = _getIP();
  
/* pack : create log user **/
  
  $this->load->model(array('M_Loger','M_SrcAksesAll'));
  
  $Loger =& M_Loger::Instance();
  $callback = $Loger->set_activity_log("RELOAD BROWSER FROM $_IP_REAL_ADDRESS ");
  
  // -------- remove key --------------------------
  
  $this->M_SrcAksesAll->_DefaultRemoveKeys();
  
  if( $callback ) {
	$conds = array('success'=> 1);
  }
  
  echo json_encode($conds);
} //==> Refresh 
 
 // new line 20190211
function getUserAbsenStatus()
{
	$_conds = array('success' => 0);
	
	if( $this->URI->_get_have_post('password') 
		OR $this->URI->_get_have_post('username'))
	{
		$this->load->model('M_User');
		$conds = $this->M_User->_getUserAbsenStatus($this -> URI -> _get_64post('username'));
	}
	
	echo json_encode($_conds = array('success' => $conds));
}


function getHandlingType()
{
	$sql  = "select a.handling_type from t_tx_agent a where a.UserId = ".$this->EUI_Session->_get_session('UserId');
			
	$rs = $this->db->query($sql);
	if( $rs->num_rows() > 0 ) 
	foreach( $rs->result_assoc() as $rows )
	{
		if( is_array($rows) 
			AND isset($rows['handling_type']) ) 
		{
			echo json_encode($rows);
		}		
	}	
}

function getHandlingTypeOnLogin()
{
	$sql  = "select a.handling_type from t_tx_agent a where a.Id = '".$this->URI->_get_post('userlogins')."'";
		// echo $sql;	
	$rs = $this->db->query($sql);
	if( $rs->num_rows() > 0 ) 
	foreach( $rs->result_assoc() as $rows )
	{
		if( is_array($rows) 
			AND isset($rows['handling_type']) ) 
		{
			echo json_encode($rows);
		}		
	}	
}








}
// END OF FILE
// location : ./application/controller/Auth.php
?>