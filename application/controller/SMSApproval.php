<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SMSApproval extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function SMSApproval() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
		
 }

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
	 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
	 'ApproveStatus' => $this->{base_class_model($this)}->_getApproveStatus(),
	 'SMSType' => $this->{base_class_model($this)}->_getSMSType() );
	 
   if(is_array($_EUI)) {
	 $this -> load ->view('srv_sms_approval/view_srv_approval_nav',$_EUI);
   }	
}
	
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('srv_sms_approval/view_srv_approval_list',$_EUI);
	 }	
  }	
}

/*
 * @ pack : keep of the data 
 * -----------------------------------------------
 */
 public function SMSApprove()
{
  $_conds = array('success' => 0);
	if( _have_get_session('UserId') ){
		if( $this->{base_class_model($this)}->_setSMSApprove() ) {
			$_conds = array('success' => 1);
		}
	}	
	echo json_encode($_conds);
}

/*
 * @ pack : keep of the data 
 * -----------------------------------------------
 */
 
 public function SMSRejected()
{
	$_conds = array('success' => 0);
	if( _have_get_session('UserId') ){
		if( $this->{base_class_model($this)}->_setSMSRejected() ) {
			$_conds = array('success' => 1);
		}
	}	
	echo json_encode($_conds);
}

// END OF CLASS
}

 

?>