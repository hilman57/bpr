<?php 
	error_reporting(E_ALL ^ E_DEPRECATED);
	set_time_limit(0);
	
	class RptDialerDetail extends EUI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->helper(array('EUI_Object','EUI_Report'));
		}
		
		public function index(){
			$this->load->view("rpt_dialer_detail/rpt_call_incoming");
		}

		/*
		*  SHOW report 
		*  @param calldate = start_date & calldate = end_date
		*  @param mode = daily,weekly,monthly
		*  @return url =  mode&start_date&end_date
		*  author : didi gantengs
		*/
		
		Public Function ShowReport()
		{
			//get mode
			// $mode = $_REQUEST['mode'];
			$type = $_REQUEST['type'];
			$agents = $_REQUEST['user_agent'];
			
			// echo $agents;

			//prepare pass paramater
			// $data['mode'] 	 = $mode;
			$data['type'] 	 = $type;
			// $data['daily']   = $this->get_daily();
			$data['detail_daily'] = $this->_get_detail_daily();
			// $data['weekly']  = $this->get_weekly();
			$data['monthly'] = $this->get_monthly();
			//print_r($data['datas']);exit;
			//cek mode
			if ( $type == 'daily') {
				// $this->load->view('rpt_dialer_detail/rpt_show_daily', $data);
			// } else if( $type == 'daily' && $mode == 'detail'){
				$this->load->view('rpt_dialer_detail/rpt_show_daily_detail', $data);
			// } else if ( $type == 'weekly') {
				// $this->load->view('rpt_dialer_detail/rpt_show_weekly', $data);
			} else {
				$this->load->view('rpt_dialer_detail/rpt_show_monthly', $data);
			}
			
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
		}
		
		/*
		*  SHOW Excel 
		*  @param calldate = start_date & calldate = end_date
		*  @param mode = daily,weekly,monthly
		*  @return Excel Name dinamis by mode parameter
		*          array
		*  author : didi gantengs
		*/
		public function ShowExcel() {
			//get mode
			// $mode = $_REQUEST['mode'];
			$type = $_REQUEST['type'];
			$agents = $_REQUEST['user_agent'];
			// $types = strtoupper($type);
			// $modes = strtoupper($mode);//change to font capital

			// $mode_name = ($modes != "") ? $modes : "";

			//prepare pass paramater
			// $data['mode'] 	 = $mode;
			$data['type'] 	 = $type;
			$data['detail_daily'] = $this->_get_detail_daily();
			// $data['daily']   = $this->get_daily();
			// $data['weekly']  = $this->get_weekly();
			$data['monthly'] = $this->get_monthly();
			//print_r($data['datas']);exit;
			
			//generate to excel
			Excel() -> HTML_Excel(urlencode(_get_post('report_title')).'DialerDetail_'.$type);
			//cek mode
			if ( $type == 'daily') {
				// $this->load->view('rpt_dialer_detail/rpt_show_daily', $data);
			// } else if( $type == 'daily' && $mode == 'detail'){
				$this->load->view('rpt_dialer_detail/rpt_show_daily_detail', $data);
			// } else if ( $type == 'weekly') {
				// $this->load->view('rpt_dialer_detail/rpt_show_weekly', $data);
			} else {
				$this->load->view('rpt_dialer_detail/rpt_show_monthly', $data);
			}
		}

		/*
		* GET DAILY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function get_daily()
		{
			//get parameter date
			$start 	= $_REQUEST['start_date'];
			$end 	= $_REQUEST['end_date'];

			//parameter incoming call
			$param_inc = 'from-pstn';

			//get connection database
			$conn = $this->_connect_to_db();
		
			$query = mysql_query("
				SELECT 
					COUNT(cd.clid) AS total,
					DATE(cd.calldate) as call_date , 
					SUM(cd.duration) as durasi,
					ROUND(AVG(cd.duration)) as av
				FROM cdr cd 
				WHERE 
					cd.dcontext = '".$param_inc."' AND
					cd.calldate >='".$start." 00:00:00' 
					AND cd.calldate <= '".$end." 23:59:59' 
				GROUP BY 
					date(cd.calldate)"
			);
			//echo $query;
			
			while ($rows = mysql_fetch_assoc($query)) {
				$data[] = $rows;
			}
			return $data;
		}

		/*
		* GET WEEKLY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function get_weekly()
		{
			//get parameter date
			$start = $_REQUEST['start_date'];
			$end   = $_REQUEST['end_date'];

			//parameter incoming call
			$param_inc = 'from-pstn';

			//get connection database
			$conn = $this->_connect_to_db();

			$query = mysql_query("
				SELECT 
					COUNT(cd.clid) AS total,
					DATE_FORMAT(cd.calldate, '%M') AS bulan,
					WEEK(cd.calldate) AS call_date , 
					SUM(cd.duration) AS durasi,
					WEEK(DATE(cd.calldate), 3) -
				  	WEEK(DATE(cd.calldate) - INTERVAL DAY(DATE(cd.calldate))-1 DAY, 3) + 1 AS week_number,
				  	ROUND(AVG(cd.duration)) AS av
				FROM cdr cd
				WHERE 
					cd.dcontext = '".$param_inc."' AND
					cd.calldate >='".$start." 00:00:00' 
					AND cd.calldate <= '".$end." 23:59:59' 
				GROUP BY 
					WEEK(cd.calldate)"
			);

			while ($rows = mysql_fetch_assoc($query)) {
				$data[] = $rows;
			}

			return $data;
		}

		/*
		* GET MONTHLY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function get_monthly() 
		{
			//get parameter date
			$start = $_REQUEST['start_date'];
			$end   = $_REQUEST['end_date'];
			$agents = ($_REQUEST['user_agent']?$_REQUEST['user_agent']:0);
			$date_from = strtotime($start);
			$date_to = strtotime($end);
			
			for ($i=$date_from; $i<=$date_to; $i+=86400) {
				$data[date("F", $i)]['strategy_volume'] = 0;
				$data[date("F", $i)]['right_party_contact'] = 0;
				$data[date("F", $i)]['promise_to_pay'] = 0;
				$data[date("F", $i)]['adjusted_operation_hour'] = 0;
				$data[date("F", $i)]['dials_completed'] = 0;
				$data[date("F", $i)]['compliance_attempt'] = 0;
				$data[date("F", $i)]['abandons'] = 0;
				$data[date("F", $i)]['uniqe_report_dial_completed'] = 0;
				$data[date("F", $i)]['connect'] = 0;
				$data[date("F", $i)]['uniqe_report_dial_completed_percent'] = 0;
				$data[date("F", $i)]['overall_penetration'] = 0;
				$data[date("F", $i)]['predictive_presentation'] = 0;
				$data[date("F", $i)]['RPC_connect_to_collector'] = 0;
				$data[date("F", $i)]['RPC_strategy_volume'] = 0;

			}

			$query1 = ("SELECT /*DATE(a.deb_created_ts)*/ DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.deb_id) AS strategy_volume  FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >='".$start." 00:00:00'
			AND a.deb_created_ts <= '".$end." 23:00:00'
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(a.deb_created_ts);");
			// echo $query1;
			$qry1 = mysql_query($query1);
			while ($rows = mysql_fetch_array($qry1)) {
				$data[$rows['dateq']]['strategy_volume'] = ($rows['strategy_volume']?$rows['strategy_volume']:0);
			}

			$query2 = ("SELECT DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.deb_id) AS right_party_contact FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_rpc IN (SELECT rpcs.rpc_code FROM t_lk_remote_place rpcs where rpcs.rpc_code in (1,2) )
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(a.deb_created_ts);");
			// echo $query2;
			$qry2 = mysql_query($query2);
			while ($rows = mysql_fetch_array($qry2)) {
				$data[$rows['dateq']]['right_party_contact'] = ($rows['right_party_contact']?$rows['right_party_contact']:0);
			}

			$query3 = ("SELECT DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.deb_id) AS promise_to_pay FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IN (106,107,108)
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(a.deb_created_ts);");
			// echo $query3;
			$qry3 = mysql_query($query3);
			while ($rows = mysql_fetch_array($qry3)) {
				$data[$rows['dateq']]['promise_to_pay'] = ($rows['promise_to_pay']?$rows['promise_to_pay']:0);
			}
			
			// khusus ini langsung isi 0
			$querySecond = "SELECT date(actlog.start_time) as dates, actlog.id, DATE_FORMAT(actlog.start_time, '%M') AS dateq,
			TIMESTAMPDIFF(second,actlog.start_time,actlog.end_time) as workload
			FROM cc_agent_activity_log actlog 
			LEFT JOIN t_tx_agent f ON f.userid = actlog.agent
			WHERE actlog.start_time >='".$start." 08:00:00'
			AND actlog.start_time <= '".$end." 23:30:00'
			and actlog.`status` in (1,4,3)
			AND actlog.agent in (".$agents.")
			group by date(actlog.start_time), actlog.id;";
			// echo $querySecond;
			$qrys = mysql_query($querySecond);
			while ($rows = mysql_fetch_array($qrys)) {
				$data[$rows['dateq']]['adjusted_operation_hour'] += ($rows['workload']?$rows['workload']:0);
			}
			// $data[$rows['dateq']]['adjusted_operation_hour'] = 0;

			$query4 = ("SELECT DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.CallHistoryId) AS dials_completed FROM t_gn_callhistory a
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			LEFT JOIN t_gn_debitur b ON a.CustomerId = b.deb_id
			LEFT JOIN t_gn_assignment v ON b.deb_id = v.CustomerId
			WHERE b.deb_created_ts >= '".$start." 00:01:58'
			AND b.deb_created_ts <= '".$end." 23:00:00'
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(b.deb_created_ts);");
			// echo $query4;
			$qry4 = mysql_query($query4);
			while ($rows = mysql_fetch_array($qry4)) {
				$data[$rows['dateq']]['dials_completed'] = ($rows['dials_completed']?$rows['dials_completed']:0);
			}

			$query5 = ("SELECT DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.deb_id) AS compliance_attempt FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IN (102,103,104,105,106,107,108,109,114)
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(a.deb_created_ts);");
			// echo $query5;
			$qry5 = mysql_query($query5);
			while ($rows = mysql_fetch_array($qry5)) {
				$data[$rows['dateq']]['compliance_attempt'] = ($rows['compliance_attempt']?$rows['compliance_attempt']:0);
			}
			
			// khusus ini langsung isi 0
			$data[$rows['dateq']]['abandons'] = 0;

			$query6 = ("SELECT DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.deb_id) AS uniqe_report_dial_completed FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IS NOT NULL AND a.deb_call_status_code NOT IN (100)
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(a.deb_created_ts);");
			// echo $query6;
			$qry6 = mysql_query($query6);
			while ($rows = mysql_fetch_array($qry6)) {
				$data[$rows['dateq']]['uniqe_report_dial_completed'] = ($rows['uniqe_report_dial_completed']?$rows['uniqe_report_dial_completed']:0);
			}

			$query7 = ("SELECT DATE_FORMAT(a.deb_created_ts, '%M') AS dateq ,COUNT(a.deb_id) AS connect FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IN (SELECT ac.CallReasonCode FROM t_lk_account_status ac WHERE ac.CallReasonCategoryId = 14)
			AND f.UserId in (".$agents.")
			GROUP BY MONTH(a.deb_created_ts);");
			// echo $query7;
			$qry7 = mysql_query($query7);
			while ($rows = mysql_fetch_array($qry7)) {
				$data[$rows['dateq']]['connect'] = ($rows['connect']?$rows['connect']:0);
			}
			
			
			return $data;
		}

		/*
		* GET REPORT DETAIL DAILY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function _get_detail_daily()
		{
			$param_inc = 'from-pstn';
			//get parameter date
			$start = $_REQUEST['start_date'];
			$end   = $_REQUEST['end_date'];
			$agents = ($_REQUEST['user_agent']?$_REQUEST['user_agent']:0);
			// $days = $this->get_day_interval($start, $end);
			// echo $days;
			// echo "<br>";
			// echo $start."s/d".$end;
			
			$date_from = strtotime($start);
			$date_to = strtotime($end);
			
			for ($i=$date_from; $i<=$date_to; $i+=86400) {
				$data[date("Y-m-d", $i)]['strategy_volume'] = 0;
				$data[date("Y-m-d", $i)]['right_party_contact'] = 0;
				$data[date("Y-m-d", $i)]['promise_to_pay'] = 0;
				$data[date("Y-m-d", $i)]['adjusted_operation_hour'] = 0;
				$data[date("Y-m-d", $i)]['dials_completed'] = 0;
				$data[date("Y-m-d", $i)]['compliance_attempt'] = 0;
				$data[date("Y-m-d", $i)]['abandons'] = 0;
				$data[date("Y-m-d", $i)]['uniqe_report_dial_completed'] = 0;
				$data[date("Y-m-d", $i)]['connect'] = 0;
				$data[date("Y-m-d", $i)]['uniqe_report_dial_completed_percent'] = 0;
				$data[date("Y-m-d", $i)]['overall_penetration'] = 0;
				$data[date("Y-m-d", $i)]['predictive_presentation'] = 0;
				$data[date("Y-m-d", $i)]['RPC_connect_to_collector'] = 0;
				$data[date("Y-m-d", $i)]['RPC_strategy_volume'] = 0;

			}

			$query1 = ("SELECT DATE(a.deb_created_ts) AS dateq ,COUNT(a.deb_id) AS strategy_volume  FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >='".$start." 00:00:00'
			AND a.deb_created_ts <= '".$end." 23:00:00'
			AND f.UserId in (".$agents.")
			GROUP BY date(a.deb_created_ts);");
			// echo $query1;
			$qry = mysql_query($query1);
			while ($rows = mysql_fetch_array($qry)) {
				$data[$rows['dateq']]['strategy_volume'] = ($rows['strategy_volume']?$rows['strategy_volume']:0);
			}

			$query2 = mysql_query("SELECT date(a.deb_created_ts) AS dateq ,COUNT(a.deb_id) AS right_party_contact FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_rpc IN (SELECT b.rpc_code FROM t_lk_remote_place b)
			AND f.UserId in (".$agents.")
			GROUP BY date(a.deb_created_ts);");
			while ($rows = mysql_fetch_array($query2)) {
				$data[$rows['dateq']]['right_party_contact'] = ($rows['right_party_contact']?$rows['right_party_contact']:0);
			}

			$query3 = mysql_query("SELECT date(a.deb_created_ts) AS dateq ,COUNT(a.deb_id) AS promise_to_pay FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IN (106,107,108)
			AND f.UserId in (".$agents.")
			GROUP BY date(a.deb_created_ts);");
			while ($rows = mysql_fetch_array($query3)) {
				$data[$rows['dateq']]['promise_to_pay'] = ($rows['promise_to_pay']?$rows['promise_to_pay']:0);
			}
			
			// khusus ini langsung isi 0
			$querySecond = "SELECT date(actlog.start_time) as dates, actlog.id, DATE_FORMAT(actlog.start_time, '%M') AS dateq,
			TIMESTAMPDIFF(second,actlog.start_time,actlog.end_time) as workload
			FROM cc_agent_activity_log actlog 
			LEFT JOIN t_tx_agent f ON f.userid = actlog.agent
			WHERE actlog.start_time >='".$start." 08:00:00'
			AND actlog.start_time <= '".$end." 23:30:00'
			and actlog.`status` in (1,4,3)
			AND actlog.agent in (".$agents.")
			group by date(actlog.start_time), actlog.id;";
			$qrys = mysql_query($querySecond);
			while ($rows = mysql_fetch_array($qrys)) {
				$data[$rows['dates']]['adjusted_operation_hour'] += ($rows['workload']?$rows['workload']:0);
			}
			// $data[$rows['dateq']]['adjusted_operation_hour'] = 0;

			$query4 = mysql_query("SELECT date(b.deb_created_ts) AS dateq ,COUNT(a.CallHistoryId) AS dials_completed FROM t_gn_callhistory a
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			LEFT JOIN t_gn_debitur b ON a.CustomerId = b.deb_id
			LEFT JOIN t_gn_assignment c ON b.deb_id = c.CustomerId
			WHERE b.deb_created_ts >= '".$start." 00:01:58'
			AND b.deb_created_ts <= '".$end." 23:00:00'
			AND f.UserId in (".$agents.")
			GROUP BY date(b.deb_created_ts);");
			while ($rows = mysql_fetch_array($query4)) {
				$data[$rows['dateq']]['dials_completed'] = ($rows['dials_completed']?$rows['dials_completed']:0);
			}

			$query5 = mysql_query("SELECT date(a.deb_created_ts) AS dateq ,COUNT(a.deb_id) AS compliance_attempt FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IN (102,103,104,105,106,107,108,109,114)
			AND f.UserId in (".$agents.")
			GROUP BY date(a.deb_created_ts);");
			while ($rows = mysql_fetch_array($query5)) {
				$data[$rows['dateq']]['compliance_attempt'] = ($rows['compliance_attempt']?$rows['compliance_attempt']:0);
			}
			
			// khusus ini langsung isi 0
			$data[$rows['dateq']]['abandons'] = 0;

			$query6 = mysql_query("SELECT DATE(a.deb_created_ts) AS dateq ,COUNT(a.deb_id) AS uniqe_report_dial_completed FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IS NOT NULL AND a.deb_call_status_code NOT IN (100)
			AND f.UserId in (".$agents.")
			GROUP BY date(a.deb_created_ts);");
			while ($rows = mysql_fetch_array($query6)) {
				$data[$rows['dateq']]['uniqe_report_dial_completed'] = ($rows['uniqe_report_dial_completed']?$rows['uniqe_report_dial_completed']:0);
			}

			$query7 = mysql_query("SELECT date(a.deb_created_ts) AS dateq ,COUNT(a.deb_id) AS connect FROM t_gn_debitur a
			LEFT JOIN t_gn_assignment b ON a.deb_id = b.CustomerId
			LEFT JOIN t_tx_agent f ON f.userid = b.AssignSelerId
			WHERE a.deb_created_ts >= '".$start." 00:01:58'
			AND a.deb_created_ts <= '".$end." 23:00:00' AND a.deb_call_status_code IN (SELECT ac.CallReasonCode FROM t_lk_account_status ac WHERE ac.CallReasonCategoryId = 14)
			AND f.UserId in (".$agents.")
			GROUP BY date(a.deb_created_ts);");
			while ($rows = mysql_fetch_array($query7)) {
				$data[$rows['dateq']]['connect'] = ($rows['connect']?$rows['connect']:0);
			}

			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			return $data;
		}

		/*
		* function connect to dababase asterisk 
		* @param HOSTNAME, USER ,PASSWORD
		* @return connect or lose
		* author : didi gantengs
		*/
		function get_day_interval($start=null, $end=null) 
		{
			// $diff = abs(strtotime($end) - strtotime($start));

			// $years = floor($diff / (365*60*60*24));
			// $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			// $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			
			// return $days;
			
			$start = new DateTime($start);
			$end = new DateTime($end);
			$interval = $start->diff($end);
			// echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "; 

			// shows the total amount of days (not divided into years, months and days like above)
			// echo "difference " . $interval->days . " days ";
			
			return $interval->days;
		}
	}
?>