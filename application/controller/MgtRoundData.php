<?php 
/*
 * @ pack : manage round data its
 */
 
class MgtRoundData extends EUI_Controller
{

	/*
	 * @ pack : function data # ------------
	 */ 

	public function MgtRoundData()
	{
		parent::__construct();
		$this->load->model(array( base_class_model($this)));
	}
	
	public function index()
	{		
		
	}
	
	public function UIRoundStatus()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$dropdown['Dropdown'] = $this->{base_class_model($this)}->_get_dropdown();
			$dropdown['RandomBy'] = _get_post('random_by');
			if( is_array( $dropdown ) )
			{
				$this -> load -> view('mgt_random_debitur/view_round_status',$dropdown);
			}
		}
	}
	
	public function UIRoundUpload()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			
			$dropdown['template'] = $this->M_MgtBucket->_get_template();
			$dropdown['product'] = $this->M_SetCampaign->_get_campaign_name();
			if( is_array( $dropdown ) )
			{
				$this -> load -> view('mgt_random_debitur/view_round_upload',$dropdown);
			}
		}
	}
	
	/** fungsi memanggil model untuk menyimpan pengaturan perputaran **/
	public function RoundByStatus()
	{
		$call_back_client=array();
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$call_back_client = $this->{base_class_model($this)}->_SetRoundStatus();
		}

		echo json_encode($call_back_client);
	}
	
	
	
}
//End Of Class
?>