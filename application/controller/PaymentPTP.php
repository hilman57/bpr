<?php
class PaymentPTP extends EUI_Controller
{
/*
 * @ pack : __construct 
 */
 
function __construct()
{
	parent::__construct();
	$this->load->model(base_class_model($this));
}

/*
 * PaymentHistoryPage
 */
 public function PaymentHistoryPage()
{

/*
 * @ pack : set default of pproperties 
 */
 
  $this->post_page  = _get_post('page');
  $this->per_page   = 5;
  $this->start_page = 0;
  $this->total_page = 0 ;
  $this->total_record = 0;
  $this->result_page = array();
  $this->result_source = array();
  
/*
 * @ pack : of list data 
 */
 
 if( _have_get_session('UserId') )
{
 
 // @ pack : set its  
  
  if( $this->post_page) {
	$this->start_page = (($this->post_page-1)*$this->per_page);
  } else {	
	$this->start_page = 0;
  }
	
	$this->result_source = $this->{base_class_model($this)}->_getPaymentDebitur();
	if( sizeof($this->result_source) > 0  ){
		$this->total_record  = count($this->result_source);
	}
	
// @ pack : set total page 
	
    if( $this->total_record ){
		$this->total_page = ceil($this->total_record/ $this->per_page);
	}
	
 // @ pack : set to page list 
 
	$this->result_page = array_slice($this->result_source, $this->start_page, $this->per_page);
 } 
 
 // @ pack : sent to view 

  $isPaymentContent['content_pages'] = $this->result_page;
  $isPaymentContent['total_records'] = $this->total_record;
  $isPaymentContent['total_pages']   = $this->total_page;
  $isPaymentContent['select_pages']  = $this->post_page;
  $isPaymentContent['start_page']    = $this->start_page; 
 
 // @ pack : sent to view 
 
  if( $isPaymentContent )
  {
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->load->view("mod_payment_ptp/view_payment_ptp",  $isPaymentContent);	
	}
	else
	{
		$this->load->view("mod_payment_ptp/view_payment_ptp_manage",  $isPaymentContent);
	}
  }
}

// @ pack : END CLASS 

}


?>