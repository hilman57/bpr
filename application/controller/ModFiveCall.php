<?php 

// @ pack : ModFiveCall
class ModFiveCall extends EUI_Controller 
{

//@ pack : __construct

public function __construct()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this),'M_CallWithArea'));
}
/*
 * @ pack : SaveBlocking
 */ 
 
 public function SaveFiveCall()
{
  $_conds = array('success'=>0 );
  if( $this->EUI_Session->_get_session('UserId') )
  {
	if( $_is_conds = $this->{base_class_model($this)}->_SaveFiveCall() )
	{
	  $_conds = array('success'=>1);
	}	
  }
  
  echo json_encode($_conds);
  
}

// CounterFiveCall

public function CounterFiveCall()
{
  $_conds = array('count'=>0, 'lock' => 0 );
  
  if( $this->EUI_Session->_get_session('UserId') )
  {
  
	$db_is_lock  = $this->{base_class_model($this)}->_get_select_db_is_lock();
	$db_is_block = $this->{base_class_model($this)}->_getCounterFiveCall();
	$db_is_calltime = $this->M_CallWithArea->_select_operation_time();
	
 // lock data ========================> 
 
	if( $db_is_lock ) 
		$_conds['lock'] = 1;

		
 // lock data ========================> 
	if( $db_is_block ) 
		$_conds['count'] = 1;
	
 // lock data ========================> 	
	if( isset($db_is_calltime['call']) )  
		$_conds['calltime'] = $db_is_calltime['call'];
		
  }
  
  echo json_encode($_conds);
		
}
// END CLASS 
}
