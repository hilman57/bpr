<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
 
 class SetField extends EUI_Controller 
 {

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function SetField() 
 { 
	parent::__construct();
	$this -> load ->model(array(
		base_class_model($this),'M_SetCampaign','M_ManageDatabase')
	);
 }	
 
 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )  
	{
		$UI = array('page' => $this ->{base_class_model($this)} ->_get_default());
		$this -> load -> view("mod_view_field/view_setfield_nav.php", $UI);
	}
 }
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array(
			'page' => $this ->{base_class_model($this)} ->_get_resource(),
			'num' => $this ->{base_class_model($this)} ->_get_page_number()
		);
		
		// sent to view data 
		$this -> load -> view('mod_view_field/view_setfield_list',$UI);	
	}
	
 }
 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Create() 
{	
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array( 
			'view_field_size' => $this ->{base_class_model($this)} ->_getFieldSize(),  
			'CampaignId' => $this ->M_SetCampaign->_getCampaignGoals(2),
			'Status' => array('0' => 'Not Active','1'=>'Active')
		);  // outbound 
		$this -> load -> view('mod_view_field/view_setfield_layout',$UI);	
	}	
} 
 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function ViewLayoutGenerate() 
{	
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array( 
			'view_field_start' => (INT)$this -> URI->_get_post('FldStart'), 
			'view_field_end' => (INT)$this -> URI->_get_post('FldEnd'),
			'fields' => $this ->{base_class_model($this)} ->_getCols()
		);
			
		$this -> load -> view('mod_view_field/view_setfield_generator',$UI);	
	}	
} 

 
public function SaveGenerate()
{

 $_conds = array('success' => 0);
 
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		if( $result =  $this ->{base_class_model($this)} -> _setSaveGenerate( $this -> URI->_get_all_request() ) )
		{
			$_conds = array('success' => 1);	
		}
	}
	
	__(json_encode($_conds));
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Delete()
{
	$_conds = array('success' => 0);
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		if( $result =  $this ->{base_class_model($this)} -> _setDeleted( $this -> URI->_get_array_post('Field_Id') ) )
		{
			$_conds = array('success' => 1);	
		}
	}
	
	__(json_encode($_conds));

}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function ViewLayout()
{
 // @ pack : $this ->{base_class_model($this)} ->_getFieldByCampaignId(10);
		
	if( $this -> EUI_Session -> _have_get_session('UserId') ) 
	{
		$LayoutData = array(base64_decode(_get_post('LayoutId')));  
		if(is_array($LayoutData))
		{
			$this -> load -> view('mod_view_field/view_layout_content');		
		}
		
	}

}

		
 }