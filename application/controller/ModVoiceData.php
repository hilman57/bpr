<?php

/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class ModVoiceData Extends EUI_Controller
{
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function ModVoiceData ()
 {
	parent::__construct();
	$this -> load -> model(array(base_class_model($this),
	'M_Pbx','M_Configuration',
	'M_SetCampaign','M_SrcCustomerList','M_SysUser'
	));
 }
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function getCallResult($CategoryId = null )
 {
	$_conds = array();
	$_arr_callresult = $this->M_SetCallResult->_getCallReasonId($CategoryId);
		
	if(is_array($_arr_callresult))foreach( $_arr_callresult as $k =>$call ) {
		$_conds[$k] = $call['name'];
	}
	
	return $_conds;
 }
 
public function _getCallInterest()
{
	$interest = array();
	
	if( $res = $this->M_SetCallResult->_getRealInterest() )
	{
		foreach( $res as $id  => $rows ) {
			$interest[$id] = $rows['name'];
		}
	}
	
	return $interest;
}
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function index()
{
 if( $this ->EUI_Session ->_have_get_session('UserId') && class_exists('M_ModVoiceData') )
 {
	$_EUI  = array ( 
		'page' 		 => $this->M_ModVoiceData->_get_default(),
		'CampaignId' => $this->M_SetCampaign->_get_campaign_name(),
		//'CardType' 	 => $this->M_SrcCustomerList->_getCardType(), 
		'GenderId' 	 => $this->M_SrcCustomerList->_getGenderId(),
		'UserId' 	 => $this->M_SysUser->_get_teleamarketer(),
		'CallResult' => $this->getCallResult()
	);
		
		
	if( is_array($_EUI))
	{
		$this -> load ->view('mod_voice_data/view_mod_voice_nav',$_EUI);
	}	
 }
 
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Content()
{

  if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->M_ModVoiceData ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->M_ModVoiceData ->_get_page_number(); // load content data by pages 
		
		// sent to view data 
		
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('mod_voice_data/view_mod_voice_list',$_EUI);
		}	
	}	
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function SetVoicePlay()
{
	$_success = array('success'=>0);
	
	// M_Configuration
	
	
	
	$FTP = $this -> M_Configuration -> _getFTP();	
	$voice  = $this -> {base_class_model($this)}->_getVoiceData($this->URI->_get_post('RecordId') );
	
	// if exist data then cek in PBX Server ...
	if( is_array($voice) ) 
	{
	
	 // include library FTP 
	 
		$this->load->library('Ftp');
		
	// set parameter attribute 
	
		$PBX = $this->M_Pbx ->InstancePBX($voice['agent_ext']);
		$this->Ftp->connect(array(
			'hostname' => $FTP["FTP_SERVER{$PBX}"],
			'port' => $FTP["FTP_PORT{$PBX}"],
			'username' => $FTP["FTP_USER{$PBX}"],
			'password' => $FTP["FTP_PASSWORD{$PBX}"])
		);
		
		
		
		// cek connection ID 
		
		if( ($this -> Ftp->_is_conn()!=FALSE) 
			AND (isset($voice['file_voc_loc'])) )
		{
			$_found   = null;
			
		// change directory on server remote ...
		
			$this->Ftp->changedir($voice['file_voc_loc']); 
			
		// show file on spesific location ..
		
			$_ftplist = $this -> Ftp -> list_files('.');
			foreach($_ftplist as $k => $src )
			{
				if( ($src == $voice['file_voc_name'])) {
					$_found = $src;
				}
			}
			
		// def location to local download 
		
			if(!defined('RECPATH') ) 
				define('RECPATH',str_replace('system','application', BASEPATH)."temp");
				
		// if match fil then download 
			
			if( !is_null($_found) ) 
			{
				$_original_path = RECPATH;
				
				if($this -> Ftp -> download(RECPATH . '/' . $_found, $_found ) ) 
				{
					exec("sox {$_original_path}/{$_found} {$_original_path}/{$_found}.wav");
					$voice['file_voc_name'] = "{$_found}.wav";
					@unlink( $_original_path ."/". $_found );
					$_success = array('success'=>1, 'data' => $voice );
				}
			}
		}
	}
	
	echo json_encode($_success);
}


/*
 * @ def 		: --
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function DownloadVoice()
 {
	$_success = array('success'=>0);
	
	// M_Configuration
	  $FTP = $this -> M_Configuration -> _getFTP();	
	
	// voice data by click user ...
	$voice  = $this -> {base_class_model($this)}->_getVoiceData($this->URI->_get_post('RecordId') );
	
	// if exist data then cek in PBX Server ...
	if( is_array($voice) ) 
	{
	
	// include library FTP 
		$this->load->library('Ftp');
		
	// set parameter attribute 
		
		$PBX = $this->M_Pbx ->InstancePBX($voice['agent_ext']);
		$this->Ftp->connect(array(
			'hostname' => $FTP["FTP_SERVER{$PBX}"],
			'port' => $FTP["FTP_PORT{$PBX}"],
			'username' => $FTP["FTP_USER{$PBX}"],
			'password' => $FTP["FTP_PASSWORD{$PBX}"])
		);
		
		
	// cek connection ID 
		if( ($this -> Ftp->_is_conn()!=FALSE) 
			AND (isset($voice['file_voc_loc'])) )
		{
			$_found   = null;
			
	// change directory on server remote ...
	
			$this->Ftp->changedir($voice['file_voc_loc']); 
			
	// show file on spesific location ..
	
			$_ftplist = $this -> Ftp -> list_files('.');
			foreach($_ftplist as $k => $src )
			{
				if( ($src == $voice['file_voc_name'])) {
					$_found = $src; 
				}
			}
			
	// def location to local download 
	
			if(!defined('RECPATH') ) 
				define('RECPATH',str_replace('system','application', BASEPATH)."temp");
				
	// if match fil then download 
			if( !is_null($_found) ) 
			{
				$_wavvoice  = preg_replace('/gsm/i','wav',$_found);
				$_original_path = RECPATH;
				if($this -> Ftp -> download(RECPATH . '/' . $_found, $_found ) ) 
				{
					$_wavvoice = urlencode($_wavvoice);
					exec("sox \"{$_original_path}/{$_found}\" {$_original_path}/{$_wavvoice}");
					if(@unlink( $_original_path .'/'. $_found ))
					{
						$voice['file_voc_name'] = $_wavvoice;
						$_success = array('success'=>1, 'data' => $voice );
					}	
				}
			}
		}
	}
	
	echo json_encode($_success);
	
 }
 
 // DeletedVoice
 
 function DeletedVoice()
 {
	$_success = array('success'=>0);
	
	if(!defined('RECPATH')) {
		define('RECPATH',str_replace('system','application', BASEPATH)."temp");	
	}
	
	if( $this -> URI ->_get_have_post('filename') )
	{
		$file_voice = RECPATH . '/' . $this -> URI->_get_post("filename");
		if(file_exists($file_voice) )
		{
			if( @unlink($file_voice) )
			{
				$_success = array('success'=>1);
			}
		}
	}

	echo json_encode($_success);	
				
 }
 
 
 
 // wgetDownload
 
 public function WgetDownload()
 {
	if( $this -> URI->_get_have_post('VoiceName'))
	{
		$VoiceName = base64_decode($this -> URI->_get_post('VoiceName'));
		if( !is_null($VoiceName) )
		{
			if(!defined('RECPATH') )  define('RECPATH',str_replace('system','application', BASEPATH)."temp/". $VoiceName );
			
			if( !file_exists(RECPATH))  exit('Voice not found.');
			else
			{
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Type: audio/x-gsm");
				header("Content-Disposition: attachment; filename=". basename(RECPATH));
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: " . filesize(RECPATH));
				readfile(RECPATH); 
				@unlink(RECPATH);
			}
		}
	}
 }
 
 /**
  * @ def 		: play audio on window open 
  * -------------------------------------------------------------------------
  
  * @ author 	: omens  
  * @ date  	: 20140930 
  */
  
 public function NewPlayWindow() 
 {
 
	$FTP = $this -> M_Configuration -> _getFTP();	
	$voice  = $this -> {base_class_model($this)}->_getVoiceData($this->URI->_get_post('RecordId') );

	
	// if exist data then cek in PBX Server ...
	if( is_array($voice) ) 
	{
	
	 // include library FTP 
	 
		$this->load->library('Ftp');
		
	// set parameter attribute 
	
		$PBX = $this->M_Pbx->InstancePBX($voice['agent_ext']);
		$FTP_connect = $this->Ftp->connect(array( 'hostname'=>$FTP["FTP_SERVER{$PBX}"], 'port'=>$FTP["FTP_PORT{$PBX}"], 'username'=> $FTP["FTP_USER{$PBX}"],'password' => $FTP["FTP_PASSWORD{$PBX}"],'debug' => true ) );
		
		if( ($FTP_connect==TRUE) 
			AND (isset($voice['file_voc_loc'])) )
		{
			
			$_found = NULL;
			
			if( FALSE == $this->Ftp->changedir($voice['file_voc_loc']) )
			{
				echo "<pre>". reset($this->Ftp->_get_error()) ."</pre>";
			}
			
			$_ftplist = $this->Ftp->list_files('.');
			if( !is_array($_ftplist) ) {
				echo "<pre>". reset($this->Ftp->_get_error()) ."</pre>";
			}
			
			foreach($_ftplist as $k => $src ) 
			{
				if( ($src == $voice['file_voc_name'])) 
				{
					$_found = $src;
				}
			}
				
			if(!defined('RECPATH') ) 
			{
				define('RECPATH',str_replace('system','application', BASEPATH)."temp");
			}		
			
			  if( !is_null( $_found ) ) 
			 {
				$_original_path = RECPATH;
				if($this -> Ftp -> download(RECPATH . '/' . $_found, $_found ))
				{
					$_wavvoice = urlencode($_found);
					exec("sox \"{$_original_path}/{$_found}\" {$_original_path}/{$_wavvoice}.wav && rm -f \"{$_original_path}/{$_found}\"");
					$voice['file_voc_name'] = "{$_wavvoice}.wav";
					$this->load->view("mod_voice_data/voice_window_popup", array('rows' => $voice));
				}
				
			  }
		}
		else  
		{
			echo "<pre>". reset($this->Ftp->_get_error()) ."</pre>";
		}
		
	}
 }
 
/* 
 * @ def 		: play audio on window open 
 * -------------------------------------------------------------------------
  
 * @ author 	: omens  
 * @ date  	: 20140930 
 */
 
 public function VioicePage()
{
  // @ pack : sent to view 
  if( in_array(_get_session('HandlingType'), 
	 array(
	  USER_ROOT, USER_ADMIN,
	  USER_SENIOR_TL,
	  USER_SUPERVISOR, 
	  USER_ACCOUNT_MANAGER, 
	  USER_LEADER, 
	  USER_MANAGER) ))
  {	
	 $this->start_page = 0;
	  $this->per_page   = 5;
	  $this->post_page  = (int)_get_post('page');
	 
	 // @ pack : set it 
	 
	  $this->result_voice = array();
	  $this->content_voice = $this->{base_class_model($this)}->_get_voice_content(_get_post('DebiturId'));
	  
	  
	 // @ pack : record 

	  $this->total_records = count($this->content_voice);
		
	 // @ pack : set its  
	  if( $this->post_page) {
		$this->start_page = (($this->post_page-1)*$this->per_page);
	  } else {	
		$this->start_page = 0;
	  }

	 // @ pack : set result on array
	  if( (is_array($this->content_voice)) 
		AND ( $this->total_records > 0 ) )
	 {
		$this->result_voice = array_slice($this->content_voice, $this->start_page, $this->per_page);
	}	
	  
	 // @ pack : then set it to view 
	 
	  $VoiceLoger['content_pages']  = $this->result_voice;
	  $VoiceLoger['total_records']  = $this->total_records;
	  $VoiceLoger['total_pages']  = ceil($this->total_records/ $this->per_page);
	  $VoiceLoger['select_pages'] = $this->post_page;
	  $VoiceLoger['start_page']  = $this->start_page;
	  
	  $this->load->view("mod_voice_data/view_mod_voice_content", $VoiceLoger);
  }
} 
 
 // NewPlayWindow::STOP
 
}
?>