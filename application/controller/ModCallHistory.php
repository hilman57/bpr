<?php

/*
 * @ pack : ModCallHistory
 */
 
 class ModCallHistory extends EUI_Controller 
{

/*
 * @ pack : __construct
 */
 
 function ModCallHistory()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
	
 }

/*
 * @ pack : __construct
 */
 
public function CustomerHistory()
{
 if( _have_get_session('UserId') )
 {	
	$content['CallHistory'] = $this->{base_class_model($this)}->_get_call_history_customer(_get_post('DebiturId'));
	if( is_array( $content )) 
	{
		$this->load->view("mod_call_history/view_call_history", $content);
	} 
  }
}

/*
 * @ pack :  call history with page 
 */

 /*
 * @ pack : __construct
 */
 
public function CustomerHistoryPage()
{
  $this->start_page = 0;
  $this->per_page   = 5;
  $this->post_page  = (int)_get_post('page');
 
 // @ pack : set it 
 
  $this->result_history = array();
  $this->content_history = $this->{base_class_model($this)}->_get_call_history_customer(_get_post('DebiturId'));
  
 // @ pack : record 

  $this->total_records = count($this->content_history);
	
 // @ pack : set its  
  if( $this->post_page) {
	$this->start_page = (($this->post_page-1)*$this->per_page);
  } else {	
	$this->start_page = 0;
  }

 // @ pack : set result on array
  if( (is_array($this->content_history)) 
	AND ( $this->total_records > 0 ) )
 {
	$this->result_history = array_slice($this->content_history, $this->start_page, $this->per_page);
}	
  
 // @ pack : then set it to view 
 
  $CallHistory['content_pages']  = $this->result_history;
  $CallHistory['total_records']  = $this->total_records;
  $CallHistory['total_pages']  = ceil($this->total_records/ $this->per_page);
  $CallHistory['select_pages'] = $this->post_page;
  $CallHistory['start_page']  = $this->start_page;
  

 // @ pack : sent to view 

  $this->load->view("mod_call_history/view_call_history_page", $CallHistory);
}

// END CLASS 
 
}

?>