<?php
class MenuWorkProject extends EUI_Controller
{
	
	
function MenuWorkProject()
 {
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
 }
 

/*
 * load first page load of the nav data 
 */
 
public function index() 
{
	if( $this->EUI_Session->_have_get_session('UserId')) 
	{
		$page['page'] = $this -> {base_class_model($this)} -> get_default();
		$this -> load -> view("mod_menu_project/view_menuproject_nav", $page);
	}
}


/*
 * load first page load of the nav data 
 * @ content of page 
 */
 
public function content()
{
	$page['page'] = $this->{base_class_model($this)}->get_resource_query(); // load content data by pages 
	$page['num']  = $this->{base_class_model($this)}->get_page_number(); 	// load content data by pages 
	if( is_array($page) && is_object($page['page']) ) 
	{
		$this -> load -> view("mod_menu_project/view_menuproject_list", $page );
	}	
}

/**
 * @ def : add menu layout user navigation HHH 
 *
 */
 
 public function AddMenuWorkProject()
 {
	if( $this->EUI_Session->_get_session('UserId') )
	{
		$UI = $this->{base_class_model($this)}->_AddMenuWorkProject();
		if( $UI ) 
		{
			$this -> load -> view("mod_menu_project/view_add_menuproject", $UI );
		}	
	}
 }
 

/**
 * @ def : add menu layout user navigation HHH 
 *
 */ 
 
  public function ApplicationMenuByProject()
 {
	if( $this->EUI_Session->_get_session('UserId') )
	{
		$UI = array( 'ApplicationMenuByProject' => $this->{base_class_model($this)}->_WorkMenuApplication( $this->URI->_get_post('WorkProjectId') )  );
		if( $UI ) 
		{
			$this -> load -> view("mod_menu_project/view_load_menuproject", $UI );
		}	
	}
 }
 
/** 
 * @ def  : delete menu on work project detected 
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/DeleteApplicationMenuByProject/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array )
 * @ param : $ProjectId ( array ) 
 */

public function DeleteApplicationMenuByProject()
{
	$conds = array('success' => 0);
	
	if( $this->EUI_Session->_get_session('UserId') )
	{
		$UI = $this->{base_class_model($this)}->_DeleteApplicationMenuByProject();
		if( $UI )
		{
			$conds = array('success' => 1);	
		}
	}
	
	echo json_encode($conds);
}

/** 
 * @ def  : delete menu on work project detected 
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/AssignMenuWorkProjectById/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array )
 * @ param : $ProjectId ( array ) 
 */
 
public function AssignMenuWorkProjectById()
{
	$conds = array('success' => 0);
	
	if( $this->EUI_Session->_get_session('UserId') )
	{
		$UI = $this->{base_class_model($this)}->_AssignMenuWorkProjectById();
		if( $UI )
		{
			$conds = array('success' => 1);	
		}
	}
	
	echo json_encode($conds);
}
 
/** 
 * @ def  : delete menu on work project by rows in grid table  
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/RemoveApplicationMenuById/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array ) 
 */

public function RemoveApplicationMenuById()
{
	$conds = array('success' => 0);
	
	if( $this->EUI_Session->_get_session('UserId') )
	{
		$UI = $this->{base_class_model($this)}->_RemoveApplicationMenuById();
		if( $UI )
		{
			$conds = array('success' => 1);	
		}
	}
	
	echo json_encode($conds);
}

/** 
 * @ def  : delete menu on work project by rows in grid table  
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/ActiveWorkApplicationMenu/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array ) 
 */

public function ActiveWorkApplicationMenu()
{
	$conds = array('success' => 0);
	
	if( $this->EUI_Session->_get_session('UserId') )
	{	
		$UI = $this->{base_class_model($this)}->_ActiveWorkApplicationMenu
		(
			$this->URI->_get_array_post('WorkMenuId'), 
			$this->URI->_get_post('Activate') 
		);
		
		if( $UI )
		{
			$conds = array('success' => 1);	
		}
	}
	
	echo json_encode($conds);
}




 
 
 
}
?>