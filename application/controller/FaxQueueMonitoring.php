<?php
class FaxQueueMonitoring extends EUI_Controller
{

function FaxQueueMonitoring() {
	parent::__construct();
	$this -> load -> model(array(base_class_model($this)));
}
	
function index() {
	if( $this -> EUI_Session->_get_session('UserId') ){
		$this -> load -> view("fax_queue_monitoring/view_fax_queue_nav",array(null));
	}
}

/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function Content()
{
	if( $this -> EUI_Session->_get_session('UserId') )
	{
		$UserActivity = array('fax_list_queue' => $this -> {base_class_model($this)} -> _getFaxqueueMonitoring() );
		$this -> load -> view("fax_queue_monitoring/view_fax_queue_list",$UserActivity);
	}	
}

/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */


public function FaxMonCancel()
{
	$_conds = array('success' => 0);
	if( $this -> EUI_Session->_have_get_session('UserId') )
	{
		if( $result = $this -> {base_class_model($this)} -> _setFaxMonCancel() )
		{
			$_conds = array('success' => 1);
		}
	}
	
	__(json_encode($_conds));
}

/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function FaxMonResend()
{
	$_conds = array('success' => 0);
	if( $this -> EUI_Session->_have_get_session('UserId') )
	{
		if( $result = $this -> {base_class_model($this)} -> _setFaxMonResend() )
		{
			$_conds = array('success' => 1);
		}
	}
	
	__(json_encode($_conds));
}

/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function FaxMonView()
{

 $this -> load->model('M_Configuration');
 $FTP = $this -> M_Configuration -> _getFTP();	

 if( $_faxmile = $this -> {base_class_model($this)} -> _getViewFaxFile( $this -> URI -> _get_post('QueueId')) ) 
 {
	$this->load->library('Ftp');
	$this->Ftp->connect(array('hostname' => $FTP['FTP_SERVER1'],
		'port' => $FTP['FTP_PORT'],'username' => $FTP['FTP_USER1'],
		'password' => $FTP['FTP_PASSWORD1'])
	);
	
	
	if( $this -> Ftp->_is_conn() 
		AND !is_null($_faxmile['QueueFaxPath']))
	{
		
		$_found   = null;
		
		$this->Ftp->changedir($_faxmile['QueueFaxPath']); 
		$_ftplist = $this -> Ftp -> list_files('.');
			
		foreach($_ftplist as $k => $src )
		{
			if( ($src == $_faxmile['QueueFaxFile'])) {
				$_found = $src;
			}
		}
			
		if(!defined('RECPATH') ) 
			define('RECPATH',str_replace('system','application', BASEPATH)."temp");
		
			if( !is_null($_found) )
			{
				$FAX_FILE_DOCUMENT =  preg_replace("/\.tiff/i",".pdf", $_found);
				if($this -> Ftp -> download(RECPATH . "/$FAX_FILE_DOCUMENT", $FAX_FILE_DOCUMENT))
				{
					$this -> load->view("fax_queue_monitoring/view_fax_preview", array('file' => $FAX_FILE_DOCUMENT) );
				}
			}
		}
		else
			exit('ftp connection is lose.');
			
	}

}





}
?>