<?php

/*
 * @ class : Dstquestioner 
 * @ param : index 
 */
 
 class Dstquestioner 
	extends EUI_Controller
{


// -------------------------------------
/*
 * @ pack : constructor 
 */
 
 public function __construct()
{
	parent::__construct();	
	$this->load->model(array(base_class_model($this)));
	$this->load->helper(array('EUI_Object','EUI_Report'));
	
}
 
 
// -------------------------------------
/*
 * @ pack : Instance of class  
 */

 public function SwapCombo()
{
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			if(STRTOLOWER($keys)=='user')
			{
				$_serialize[$keys] = $this->SwapAgentAssign();
			}
			else{
				$_serialize[$keys] = $this ->M_Combo->$method();
			}
		}
	}
	
	return $_serialize;
 } 
 
// -------------------------------------
/*
 * @ pack : Instance of class  
 */
 
 

// -------------------------------------
/*
 * @ pack : Instance of class  
 */
 public function index() 
{ 
	// echo "halloo";
	// die;
  if(_have_get_session('UserId'))
  {
	//  $UI['page'] = $this->{base_class_model($this)}->_get_default();
	
	//  $UI['Deskoll'] = $this->M_SrcCustomerList->_getDeskollByLogin();
	//  $UI['GenderId'] = $this->M_SrcCustomerList->_getGenderId();
	//  $UI['CampaignId'] = $this->M_SrcCustomerList->_getCampaignByActive();
	//  $UI['AccountStatus'] = $this->M_SrcCustomerList->_getAccountStatus();
	//  $UI['LastCallStatus'] = $this->M_SrcCustomerList->_getLastCallStatus();
	//  $UI =  $this->{base_class_model($this)}->kategori();
	//  var_dump($UI);
	 
	 $this->load->view('mgt_dstquestioner/view_mov_data_nav', $UI);
  }
}
 


// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
 public function Content() 
{
  if( _have_get_session('UserId') ) 
  {
	$UI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$UI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($UI) )
	{
		$this->load->view('mgt_dstquestioner/view_mov_data_list',$UI);
	 }	
  }	
}
 

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
 public function SwapUserLevel()
{
 $UserPrivileges =(INT)_get_post('LevelID');
  
  if( !is_null($UserPrivileges) 
	AND ($UserPrivileges == TRUE))
 {
	$Level['LEVEL'] = $this->{base_class_model($this)}->_getUserLevel($UserPrivileges);
	$this->load->view('mgt_dstquestioner/view_privileges_list', $Level);
 } else {
	echo form()->combo('UserId','select auto', array());
 }
 
 }
 

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
 public function PageSwapData()
{
	// $out = $this -> URI->_get_all_request();
	// var_dump($out->get_array_value('category'));
	
 if(_have_get_session('UserId') )
 {
   $this->start_page = 0;
   $this->per_page   = _get_post('swp_from_page_record');
   $this->post_page  = (INT)_get_post('page');
 
 // @ pack : set it 
 
   $this->result_swap = array();
   $this->result_post = new EUI_Object(_get_all_request());
//  echo '<pre>';  
//    var_dump( $this->result_post);

   $this->content_swap = $this->{base_class_model($this)}->_get_swap_content( $this->result_post );
  
 // @ pack : record 

  $this->total_records = count($this->content_swap);
//   var_dump( $this->total_records);
   
 // @ pack : set its  
  if( $this->post_page) {
	$this->start_page = (($this->post_page-1)*$this->per_page);
  } else {	
	$this->start_page = 0;
  }

 // @ pack : set result on array
  if( (is_array($this->content_swap)) 
	AND ( $this->total_records > 0 ) )
 {
	$this->result_swap = array_slice($this->content_swap, $this->start_page, $this->per_page);
}	
  
 // @ pack : then set it to view 
//  print_r($this->total_records);
//  print_r($this->per_page);
  $SwapData['content_pages'] = $this->result_swap;
  $SwapData['total_records']  = $this->total_records;
  $SwapData['total_pages']  = ceil($this->total_records/ $this->per_page);
  $SwapData['select_pages'] = $this->post_page;
  $SwapData['start_page']  = $this->start_page;
//  echo "<pre>"; 
//   var_dump($SwapData);
//   die;
  
  if( TRUE == count( $this->URI->_get_all_request() ) > 0)
  {
	 $this -> load -> view('mgt_dstquestioner/view_mov_page_data', $SwapData );
   }
   
 }
 
} // ==> ListMoveData
 

// -------------------------------------
/*
 * @ pack : Instance of class  
 */ 
 
public function SwapCountData()
 {
	$conds = array( 'total'=> 100 );
	__(json_encode($conds));
 }
 


// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
 
 public function SwapData()
{
  $_conds = array('success' => 0);
 if( _have_get_session('UserId') ) 
 {
	$SellerId = $this->URI->_get_array_post('UserId');
	$AssignId = $this->URI->_get_array_post('AssignId');
	
	if( count($SellerId)> 0 AND count($AssignId) > 0 ) 
	{
		$result = $this -> {base_class_model($this)} ->_setSwapData($SellerId,$AssignId);
		if( $result ) {
			$_conds = array('success' => 1,'message' => $result);
		}	
	 }
 }

 echo json_encode($_conds);
 
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
public function UploadSwapData()
{
 if(_have_get_session('UserId') ) 
 {
	$Template = $this->M_MgtBucket->_get_template();
		foreach($Template as $key => $val){
			$tmptype = explode("_",$val);
			if($tmptype[0] == "UPLOAD" && $tmptype[1] == "SWAP"){
				$Template['Template'][$key] = $val;
			}
		}
	
	// $Template['Template'] 	= $this->M_MgtBucket->_get_template();
	$Template['CampaignId'] = $this->M_SetCampaign->_get_campaign_name();
	if(is_array($Template)){
		$this->load->view("mgt_dstquestioner/view_upload_swapdata", $Template);
	}	
 }
}

 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
public function SwapAmountData()
{

$_conds = array('data'=>0,'agent'=>0);
$param = array( 
 'CallReasonId' => $this -> URI ->_get_array_post('CallReasonId'),
 'CampaignId' => $this -> URI ->_get_array_post('CampaignId'),
 'UserId' => $this -> URI ->_get_array_post('FromUserId'),
 'CallAttempt' => $this -> URI ->_get_array_post('CallAttempt'),
 'LimitAmount' => $this -> URI ->_get_post('LimitAmount'),
 'ToUserId' => $this -> URI ->_get_array_post('UserId'),
 'UserLevel' => $this -> URI ->_get_post('UserLevel')
);	
	
$_conds = $this -> {base_class_model($this)} ->_setSwapAmountData($param);

echo json_encode($_conds);

}


// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
 
 public function ShowAgentPerTL() 
{
	echo form()->combo('swp_from_deskoll_id','select long', AgentByLeader(_get_array_post('TL')));
}

public function ShowTLPerSTL() 
{
	echo form()->combo('ui_swp_from_tl_id','select long', TLBySTL(_get_array_post('STL')));
}
 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
function ShowAllLeader(){ 
	echo form()->combo('swp_to_user_leader','select long', Teamleader(), null, array("change" => "new ShowDataToAgentByTL(this);") );
}

function ShowAllSeniorLeader(){ 
	echo form()->combo('swp_to_senior_leader','select long', Seniorleader(), null, array("change" => "new ShowDataToTLBySTL(this);") );
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
function ShowAllSupervisor(){ 
	echo form()->combo('swp_to_user_spv','select long', Supervisor(), null, array("change" => "new ShowAllLeader(13);") );
}


// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
function ShowDataToAgentByTL(){
	echo form()->combo('swp_to_user_deskoll','select long', AgentByLeader(_get_array_post('TL')));
}

function ShowDataToTLBySTL(){
	echo form()->combo('swp_to_user_leader','select long', TLBySeniorLeader(_get_array_post('STL')));
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
function ShowAllAgent(){
	echo form()->combo('swp_to_user_deskoll','select long', Agent());
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
 
 function SetSwapDataByAmount( $out ) 
{
 if(!is_object( $out )) {
	return false;
 }
 
  return $this->{base_class_model($this)}->_setSwapDataByAmount( $out );
  
}
 
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */
 
 function SetSwapDataByChecked( $out ) 
{
  if(!is_object( $out )) {
	return false;
  }
  
  return $this->{base_class_model($this)}->_setSwapDataByChecked( $out );
  
 }
// -------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------
/*
 * @ pack : Instance of class  
 *
 */ 
function ActionSwapDataUser()
{
	
  $out = new EUI_Object( _get_all_request() );
  $conds = array('success' => 0);
//   print_r($out->get_value('swap_type'));
//   die;
  
//   if( $out->get_value('swap_type') == 1 ) 
//  {
// 	$ok = $this-> SetSwapDataByAmount( $out );
// 	if( $ok ) { 
// 		 $conds = array('success' => 1);
// 	}
//  }
 
  if( $out->get_value('swap_type') == 2 ) 
 {
	$ok = $this-> SetSwapDataByChecked( $out );
	if( $ok ) { 
		 $conds = array('success' => 1);
	}
 }
 
 echo json_encode($conds);
 
 
} 

// test

function FTPAdd()
{
	
   if( $this -> EUI_Session -> _have_get_session('UserId') )
   {
	  
		   $this ->load->view("mgt_dstquestioner/view_add_kategori");
	   	
   }	
}

function FTPEdit()
 {
	//  var_dump($this -> URI-> _get_post('ftp_read_id'));
	//  die;
	$UI['FtpReadData']	= $this->{base_class_model($this)}->_getFTPRead( $this -> URI-> _get_post('ftp_read_id'));
 	// $UI	= $this->{base_class_model($this)}->_getWorkProjectCode(); 

	
	$this ->load->view("mgt_dstquestioner/view_edit_kategori", $UI); 	
 }

 /* FTPDelete **/

public function FTPDelete()
{
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setFTPDelete( $this -> URI->_get_array_post('Id')))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
} 

public function FTPSave()
 {
	
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setFTPSave( $this -> URI->_get_all_request() ))
		{
			$_conds = array('success' => 1 );
		}
	}
	// var_dump($_conds);
	// die;
	
	__(json_encode($_conds));
 }

 public function FTPUpdate()
 {
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setFTPUpdate( $this -> URI->_get_all_request() ))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
 }

 public function ShowDataKategori()
 {
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_showDataKategori())
		{
			$_conds = array('success' => 1,'data' =>$rs );
		}
	}
	// var_dump($rs);
	echo json_encode($_conds);	
 }
 
 function ShowDataKategori2(){
	$rs = $this -> {base_class_model($this)}->_showDataKategori();
	echo '<select type="combo" name="swp_to_kategori" id="swp_to_kategori" class="select long" onchange="pickCategory(this.value);">
	<option value=""> --choose --</option>';
	foreach ($rs as $value) {
		// print_r($value);
		echo "<option value=".$value['Id'].">".$value['ktgr_name']."</option>";
	}	
	echo '</select>';
	
	// echo form()->combo('swp_to_kategori','select long', $value);
}

// test 
// END CALSS 
}

?>