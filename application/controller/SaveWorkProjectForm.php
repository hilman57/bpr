<?php
class SaveWorkProjectForm extends EUI_Controller 
{

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
public function SaveWorkProjectForm() 
{
	parent::__construct();
	$this -> load->model(array(base_class_model($this)));
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
 public function SaveWorkProject()
 {	
	$_conds = array('success' => 0);
	if( $param = $this ->URI->_get_all_request() ) 
	{
		if( $rows = $this ->{base_class_model($this)}->_setSaveWorkProject($param) ) {
			$_conds = array('success' => 1);
		}
	}
	
	__(json_encode($_conds));
 }
 
 
 /*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
 public function UpdateWorkProject()
 {	
	$_conds = array('success' => 0);
	if( $param = $this ->URI->_get_all_request() ) 
	{
		if( $rows = $this ->{base_class_model($this)}->_setUpdateWorkProject($param) ) {
			$_conds = array('success' => 1);
		}
	}
	
	__(json_encode($_conds));
 }
 
 
 
 /*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

	
}
	
?>