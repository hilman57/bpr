<?php

	Class SetSmsBlast Extends EUI_Controller
	{
		Public Function __Construct()
		{
			parent::__construct();
			$this -> load -> model('M_SetSmsBlast');
		}
		
		Public Function Index()
		{
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				$_EUI['page'] = $this -> M_SetSmsBlast -> _get_default();
				
				if( is_array($_EUI) ) 
				{
					$this -> load -> view('set_sms_blast/set_sms_blast_nav',$_EUI);
				}
			}
		}
		
		function Content()
		{
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				$_EUI['page'] = $this -> M_SetSmsBlast -> _get_resource();
				$_EUI['num']  = $this -> M_SetSmsBlast -> _get_page_number();

				if( is_array($_EUI) && is_object($_EUI['page']) )  
				{
					$this -> load -> view('set_sms_blast/set_sms_blast_list',$_EUI);
				}	
			}
		}
		
		function AddSetSMS()
		{
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				$EUI = array(
								'Template'		=> $this -> M_SetSmsBlast -> _get_template(),
								'Dropdown'		=> $this -> M_SetSmsBlast -> _get_select_dropdown(),
								'DataStatus'	=> $this -> M_SetSmsBlast -> _get_data_status()
							);
				if( $EUI )
				{
					$this -> load -> view('set_sms_blast/set_sms_blast_add',$EUI);
				}
			}
		} 
		
		function SaveSetSMS()
		{
			$_conds = array('success' => 0);
			$_data = $this -> URI->_get_all_request();
			if( is_array($_data) )
			{
				if( $this -> M_SetSmsBlast -> _setSaveSMS() ){
					 $_conds = array('success' => 1);
				}
			}

			echo json_encode($_conds);
		}
		
		function EditSetSMS()
		{
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				$EUI = array(
								'isi'			=> $this -> M_SetSmsBlast -> _get_set_sms_detail($this -> URI -> _get_post('sms_id')),
								'Template'		=> $this -> M_SetSmsBlast -> _get_template(),
								'DataStatus'	=> $this -> M_SetSmsBlast -> _get_data_status()
							);
				$this -> load -> view('set_sms_blast/set_sms_blast_edit', $EUI);
			}
		}
		
		function UpdateSetSMS()
		{
			$_conds  = array('success'=>0);

			if( $this ->URI->_get_have_post('SRV_Data_Id')) 
			{
				$post = $this -> URI->_get_all_request();
				if( is_array($post))
				{
					if( $this -> M_SetSmsBlast ->_setUpdateSMS())
					{
						$_conds  = array('success'=>1);	
					}
				}
			}

			echo json_encode($_conds);
		}
		
		function DeleteSetSMS()
		{
			$success = array('success'=> 0, 'error'=> 'NO');
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				if( $this -> M_SetSmsBlast -> _setDeleteSMS($this -> URI->_get_post('ID'))) 
				{
					$success = array('success'=> 1, 'error'=> 'OK');	
				}
			}

			echo json_encode($success);
		}
	}

?>