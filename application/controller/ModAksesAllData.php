<?php

/*
 * E.U.I 
 *
 
 * subject	: get SetCampaign modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
class ModAksesAllData extends EUI_Controller
{
	
/*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
 public function ModAksesAllData() 
{
	parent::__construct();
	$this -> load -> model(array(base_class_model($this)));
}
	
/*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
function index()
{

 if( $this -> EUI_Session -> _have_get_session('UserId'))
 {
	$EUI = array('page'=> $this -> {base_class_model($this)} -> _get_default(),
				 'New_Agent'=>( array('NotAssign'=>'Not Assign','AccessAll'=>'Access All') +
								$this -> {base_class_model($this)} ->get_all_DC() + 
								$this -> {base_class_model($this)} ->get_all_leader() ),
				 'Exist_Agent'=> ( $this -> {base_class_model($this)} ->get_all_DC() ),
				 'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus()
				);
	$this -> load -> view('mod_akses_all/view_akses_all_nav', $EUI );
 }
 
}

/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */

function Content()
{
	if( $this -> EUI_Session -> _have_get_session('UserId'))
	{
		$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
		if( is_array($_EUI) && is_object($_EUI['page']) ) {
			$this -> load -> view('mod_akses_all/view_akses_all_list',$_EUI);
		}

	}
}

function ManageDistribute()
{
	$Content = array('Assign_Type'=>array( 1=>'Access All', 2=>'Access Per Team', 3=>'Access Per DC' ));
	// echo "<pre>";
	// print_r($Content);
	// echo "</pre>";
	if(is_array($Content) )
	{
		$this -> load -> view("mod_akses_all/view_manage_akses_all", $Content);
	}

	
}

function getAssignTo()
{
	$AssignType  = (int)_get_post('AssignType');
	$combo_value = array();
	if($AssignType==1)
	{
		$combo_value = array();
	}
	elseif($AssignType==2)
	{
		$combo_value = $this -> {base_class_model($this)} ->get_all_leader();
	}
	elseif($AssignType==3)
	{
		$combo_value = $this -> {base_class_model($this)} ->get_all_DC();
	}
	
	echo form()->combo('Assign_To','select auto', $combo_value );
}

function AssignAccessCheck()
{
	$success = 0;
	$fail = 0;
	$msg = array('Message'=>0,'Success'=>$success,'Fail'=>$fail);
	$ex_debitur = explode(',',$this->URI->_get_post('CustomerId'));
	
	foreach ($ex_debitur as $index => $deb_id)
	{
		$key['akses_deb_id']=$deb_id;
		$data['akses_type']= $this->URI->_get_post('AssignType');
		$data['akses_target_id']= $this->URI->_get_post('AssignTo');
		if($this->{base_class_model($this)}->UpdateAccessAssign($data,$key))
		{
			$success++;
		}
		else
		{
			$fail++;
		}
	}
	$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
	//print_r($msg);
	echo json_encode($msg);
	
}
function AssignAccessAll()
{
	$success = 0;
	$fail = 0;
	$msg = array('Message'=>0,'Success'=>$success,'Fail'=>$fail);
	$debitur = $this->{base_class_model($this)}->get_deb_not_assign();
	foreach ($debitur as $index => $deb_id)
	{
		$key['akses_deb_id']=$deb_id;
		$data['akses_type']= $this->URI->_get_post('AssignType');
		$data['akses_target_id']= $this->URI->_get_post('AssignTo');
		if($this->{base_class_model($this)}->UpdateAccessAssign($data,$key))
		{
			$success++;
		}
		else
		{
			$fail++;
		}
	}
	$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
	echo json_encode($msg);
	
	
}
function ResetAccessAll()
{
	$success = 0;
	$fail = 0;
	$msg = array('Message'=>0,'Success'=>$success,'Fail'=>$fail);
	if($this->{base_class_model($this)}->UpdateAccessFlagAll())
	{
		$success++;
	}
	else
	{
		$fail++;
	}
	$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
	echo json_encode($msg);
}
function ResetAccessByCheck()
{
	$success = 0;
	$fail = 0;
	$msg = array('Message'=>0,'Success'=>$success,'Fail'=>$fail);
	$ex_debitur = explode(',',$this->URI->_get_post('CustomerId'));
	// print_r($ex_debitur);
	
	foreach ($ex_debitur as $index => $deb_id)
	{
		$key['akses_deb_id']=$deb_id;
		if($this->{base_class_model($this)}->UpdateAccessFlag($key))
		{
			$success++;
		}
		else
		{
			$fail++;
		}
	}
	// echo $this->db->last_query();
	$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
	echo json_encode($msg);
}

	
}

?>