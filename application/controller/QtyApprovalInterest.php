<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class QtyApprovalInterest extends EUI_Controller
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function QtyApprovalInterest() 
 {
	parent::__construct();
	$this -> load ->model(array(
		base_class_model($this),
		'M_Combo','M_SrcCustomerList', 
		'M_SetCallResult','M_SetProduct', 
		'M_SetCampaign','M_SetResultCategory',
		'M_ModSaveActivity', 'M_SetResultQuality',
		'M_Payers','M_Benefiecery','M_Insured',
		'M_QtyPoint','M_Configuration','M_Underwriting',
		'M_Pbx'	
		)
	);	
 }
 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function index()
{
 if( $this ->EUI_Session ->_have_get_session('UserId') && class_exists('M_QtyApprovalInterest') )
 {
	$_EUI  = array( 
		'page' => $this ->{base_class_model($this)} ->_get_default(),
		'Combo' => $this -> _getCombo()
	);
		
	if( is_array($_EUI))
	{
		$this -> load ->view('qty_approval_interest/view_approval_interest_nav',$_EUI);
	}	
 }
 
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Content()
{

  if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->{base_class_model($this)} ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
		
		// sent to view data 
		
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('qty_approval_interest/view_approval_interest_list',$_EUI);
		}	
	}	
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function QualityResult()
{
	$_conds= array();
	$_conds = $this->M_SetResultQuality->_getQualityResult();
	
	foreach( $_conds as $k => $rows )
	{
		$_conds[$k] = $rows['name']; 
	}
	
	return $_conds;
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function LastCallHistory( $CustomerId = 0 )
{	
	$_conds = array();
	
	if( $this -> EUI_Session->_have_get_session('UserId') )
	{
		$_conds = $this -> {base_class_model($this)}->_getLastCallHistory( $CustomerId );
	}
	
	return $_conds;
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function CallResultId()
{
	$_conds = array();
	$datas = $this -> M_SetCallResult->_getCallReasonId();
	foreach($datas as $k => $rows )
	{
		$_conds[$k] = $rows['name'];
	}
	
	return $_conds;
}
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function QualityDetail()
{
	$CustomerId = $this ->URI->_get_post('CustomerId');
	$UI = array
	(
			'Customers' 	=> $this->M_SrcCustomerList->_getDetailCustomer($CustomerId),
			'Phones' 		=> $this->M_SrcCustomerList->_getPhoneCustomer($CustomerId),
			'AddPhone' 		=> $this->M_SrcCustomerList->_getApprovalPhoneItems($CustomerId),
			'Payers' 		=> $this->M_Payers->_getPayerReady($CustomerId),
			'Insured' 		=> $this->M_Insured->_getInsuredById($CustomerId),
			'Benefiecery' 	=> $this->M_Benefiecery->_getBeneficiaryCustomerId($CustomerId),
			'Callhistory' 	=> $this->LastCallHistory($CustomerId),
			'Policy' 		=> $this->M_Payers->_getPayersInformation($CustomerId),
			'ResultPoints' 	=> $this->M_QtyPoint->_getApprovalPoint($CustomerId),
			'QtyScoring' 	=> $this->M_QtyPoint->_getQualityScoring($CustomerId),
			'Assesment' 	=> $this->M_QtyPoint->_getAssesment(), 
			'jsonAssesment' => $this->M_QtyPoint->_jsonAssesment(),
			'QualityScoring'=> $this->M_QtyPoint->_getContentScoring(),
			'ComboScoring' 	=> $this->M_QtyPoint->_comboScoring(),
			'CallCategoryId'=> $this->M_SetResultCategory->_getOutboundCategory(),
			'CallResultId' 	=> $this->CallResultId(),
			'QualityApprove'=> $this->QualityResult(),
			'Combo' 		=> $this->_getCombo()
		);
			
			
	if($this ->URI->_get_have_post('CustomerId')) 
	{
		$this -> load -> view('qty_approval_interest/view_quality_detail',$UI);
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Recording()
{
	$param = array(
		'CustomerId' => $this->URI->_get_post('CustomerId'),
		'pages' => $this->URI->_get_post('Pages'),
	);
	
	$ListView =  array( 
		'data'  => $this->{base_class_model($this)}->_getListVoice($param), 
		'page'  => $this->{base_class_model($this)}->_getPages($param),
		'records' => $this->{base_class_model($this)}->_getCountVoice($param),
		'current' => $this->URI->_get_post('Pages')
		);
	
	
	if( $ListView )
	{
		$this -> load -> view('qty_approval_interest/view_quality_recording',$ListView);
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function CallHistory()
{
	$CallHistory = null;
	if( $this -> URI->_get_have_post('CustomerId') )
	{
		$CustomerId = $this -> URI->_get_post('CustomerId');
		if( $CustomerId )
		{
			$CallHistory= array('CallHistory' => $this->M_ModSaveActivity->_getCallHistory($CustomerId) );
		}
		
		$this -> load -> view("qty_approval_interest/view_quality_history_content",$CallHistory);
	}
}
/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function SaveQualityActivity()
{
	$conds = array('success' => 0);
	if( $this -> EUI_Session->_get_session('UserId') ) {
		$result = $this -> {base_class_model($this)}->_SaveQualityAssesment();
		if( $result )
		{
			$conds = array('success' => 1);
		}
		
	}
	__(json_encode($conds));
}


/*
 * @ def 		: approval save point 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function DetailInsured()
{
  
  if( $param = $this -> URI->_get_all_request() 
	AND is_array($param)) 
 {	
	$results = $this ->M_Insured->_getInsureId($param['InsuredId']);
	
	if( $result = array
	(
		'Insured' => $results,
		'Combo'=> $this->_getCombo(),
		'Question' => $this->M_Underwriting->_getUnderwriting($results['ProductId']),
		'Benefiecery'=> $this->M_Benefiecery->_getBeneficiaryInsuredId($results['InsuredId'])
	))
	{
		$this -> load -> view("qty_approval_interest/view_detail_insured", $result);
	}
	
 }
	
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function PlayBySessionId() 
{
  $_success = array('success'=>0);
	
  $FTP = $this -> M_Configuration -> _getFTP();
  $sql ="SELECT b.id as RecId FROM cc_call_session a  
		 LEFT JOIN cc_recording b ON a.session_id=b.session_key 
		 WHERE a.session_id='{$this -> URI->_get_post('RecordId')}'";
			 
  $qry = $this -> db ->query($sql);
  if( $rows = $qry -> result_first_assoc() )
  {
	 $voice  = $this -> {base_class_model($this)}->_getVoiceResult($rows['RecId']);
	 if( is_array($voice) )
	 {
		$this->load->library('Ftp');
		
		$PBX = $this->M_Pbx ->InstancePBX($voice['agent_ext']);
		$this->Ftp->connect(array(
			'hostname' => $FTP["FTP_SERVER{$PBX}"],
			'port' => $FTP["FTP_PORT{$PBX}"],
			'username' => $FTP["FTP_USER{$PBX}"],
			'password' => $FTP["FTP_PASSWORD{$PBX}"]
		));
		
		if( ($this -> Ftp->_is_conn()!=FALSE) 
			&& (isset($voice['file_voc_loc'])) )
		{
			$_found   = null;
			$this->Ftp->changedir($voice['file_voc_loc']); 
			
			$_ftplist = $this -> Ftp -> list_files('.');
			foreach($_ftplist as $k => $src )
			{
				if( ($src == $voice['file_voc_name'])) {
					$_found = $src;
				}
			}
			
	    /** def location to local download **/
		
			if(!defined('RECPATH') ) 
				define('RECPATH',str_replace('system','application', BASEPATH)."temp");
				
		/** if match fil then download **/
		
			if( !is_null($_found) )
			{
				$_original_path = RECPATH;
				if($this -> Ftp -> download(RECPATH . '/' . $_found, $_found))
				{	
					exec("sox {$_original_path}/{$_found} {$_original_path}/{$_found}.wav");
					$voice['file_voc_name'] = "{$_found}.wav";
					
					@unlink( $_original_path ."/". $_found );
					
					$_success = array('success'=>1, 'data' => $voice );
				}
			}
		}
	}
	
	
	}
	
	echo json_encode($_success);
	
} 



/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function VoicePlay()
{
	$_success = array('success'=>0);
	// M_Configuration
	  $FTP = $this -> M_Configuration -> _getFTP();	
	  
	// voice data by click user ...
	
	$voice  = $this -> {base_class_model($this)}->_getVoiceResult($this->URI->_get_post('RecordId') );
	
	// if exist data then cek in PBX Server ...
	if( is_array($voice) ) 
	{
	
	 // include library FTP 
	 
		$this->load->library('Ftp');
		
	// set parameter attribute 
	
		
		$PBX = $this->M_Pbx ->InstancePBX($voice['agent_ext']);
		$this->Ftp->connect(array(
			'hostname' => $FTP["FTP_SERVER{$PBX}"],
			'port' => $FTP["FTP_PORT{$PBX}"],
			'username' => $FTP["FTP_USER{$PBX}"],
			'password' => $FTP["FTP_PASSWORD{$PBX}"])
		);
		
		
		// cek connection ID 
		
		if( ($this -> Ftp->_is_conn()!=FALSE) 
			AND (isset($voice['file_voc_loc'])) )
		{
			$_found   = null;
			
		// change directory on server remote ...
		
			$this->Ftp->changedir($voice['file_voc_loc']); 
			
		// show file on spesific location ..
		
			$_ftplist = $this -> Ftp -> list_files('.');
			
			foreach($_ftplist as $k => $src )
			{
				if( ($src == $voice['file_voc_name'])) {
					$_found = $src;
				}
			}
			
		// def location to local download 
		
			if(!defined('RECPATH') ) 
				define('RECPATH',str_replace('system','application', BASEPATH)."temp");
				
		// if match fil then download 
		
			if( !is_null($_found) ) 
			{
				$_original_path = RECPATH;
				if($this -> Ftp -> download(RECPATH . '/' . $_found, $_found))
				{
					exec("sox {$_original_path}/{$_found} {$_original_path}/{$_found}.wav");
					$voice['file_voc_name'] = "{$_found}.wav";
					@unlink( $_original_path ."/". $_found );
					$_success = array('success'=>1, 'data' => $voice );
				}
			}
		}
	}
	
	echo json_encode($_success);
}

// update payer 

public function UpdatePayer() 
{
  $_conds = array('success' => 0 );
	if( $this -> M_Payers->_SaveDataPayers( $this -> URI->_get_all_request() ) ) 
	{
		$_conds = array('success' => 1);	
	}
	
  __(json_encode($_conds));	
}


//UpdateInsured
public function UpdateInsured() 
{
  $_conds = array('success' => 0 );
	if( $this -> M_Insured->_UpdateDataInsured( $this -> URI->_get_all_request() ) ) 
	{
		$_conds = array('success' => 1);	
	}
	
  __(json_encode($_conds));	
}



//UpdateBenefiacery
public function UpdateBenefiacery() 
{

  $_conds = array('success' => 0 );
	$BeneficiaryId = $this -> URI->_get_array_post('BeneficiaryId');
	if( $param = $this -> URI->_get_all_request() 
		AND is_array($BeneficiaryId) )
	{
		//BeneficiaryAge_13	20
		
		$n = 0;
		foreach( $BeneficiaryId as $key => $num )
		{
			$_SET_POST['GenderId'] = $param["BenefGender_{$num}"];
			$_SET_POST['SalutationId'] = $param["BenefSalutationId_{$num}"];
			$_SET_POST['BeneficiaryDOB'] = date('Y-m-d',strtotime($param["eneficiaryDOB_{$num}"]));
			$_SET_POST['BeneficiaryFirstName'] = $param["BeneficiaryFirstName_{$num}"];
			$_SET_POST['RelationshipTypeId'] = $param["BenefRealtionship_{$num}"];
			$_SET_POST['BeneficiaryAge'] = $param["BeneficiaryAge_{$num}"];
			$_SET_POST['BeneficiaryUpdatedTs'] = date('Y-m-d H:i:s');
			$_SET_POST['BeneficiaryId'] = $num;
			
			if( $res= $this -> M_Benefiecery -> _UpdateDataBeneficiary($_SET_POST) ){
				$n++;
			}
		}	
		if( $n > 0 ) {
			$_conds = array('success' => 1);
		}	
	}	
	
  __(json_encode($_conds));	
}


	
 	

 
}