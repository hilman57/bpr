<?php 


class PvcvcReport extends EUI_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model(
			array(
				'M_Report',
				'M_MgtRandDeb',
				base_class_model($this)
			)
		);

	}
	
	public function index(){
		$random_modul = $this->{base_class_model($this)}->modul_random();
		$UI['cmb_random_modul'] = $random_modul;

		$this->load-> view("rpt_pvcvc/report_pvcvc_nav",$UI);
	}
	
	public function showReport()
	{
		$UI_Mode = strtolower($this -> URI->_get_post('type_show'));
		switch( $UI_Mode )
		{
			case 'showhtml' 	: $this -> showHtmlReport(); 	break;
			case 'showexcel' 	: $this -> showExcelReport(); 	break;
		}
	}
	
	private function showHtmlReport()
	{
		$reportType = strtolower($this -> URI->_get_post('report_type'));
		switch($reportType){
			case 'pvc'	: $this->showReportPVC();	break;
			case 'vct'	: $this->showReportVCT();	break;
			case 'vcp'	: $this->showReportVCP();	break;
			case 'vct_category'	: $this->showReportVCTPerCategory();	break;
			case 'vcp_category'	: $this->showReportVCPPerCategory();	break;
		}
	}
	
	private function showExcelReport()
	{
		Excel() -> HTML_Excel(urlencode(_get_post('report_title')).''.time());
		$reportType = strtolower($this -> URI->_get_post('report_type'));
		switch($reportType){
			case 'pvc'	: $this->showReportPVC();	break;
			case 'vct'	: $this->showReportVCT();	break;
			case 'vcp'	: $this->showReportVCP();	break;
			case 'vct_category'	: $this->showReportVCTPerCategory();	break;
			case 'vcp_category'	: $this->showReportVCPPerCategory();	break;
		}
	}

	private function showReportPVC()
	{
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataPVC();
		$REPORT['title'] = $this -> URI->_get_post('report_title');
		
		$this->load-> view("rpt_pvcvc/report_pvc_html",$REPORT);
	}
	
	private function showReportVCT()
	{
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCT();
		$REPORT['title'] = $this -> URI->_get_post('report_title');
		
		$this->load-> view("rpt_pvcvc/report_vct_html",$REPORT);
	}
	
	private function showReportVCP()
	{
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCP();
		$REPORT['title'] = $this -> URI->_get_post('report_title');
		
		$this->load-> view("rpt_pvcvc/report_vcp_html",$REPORT);
	}
	
	private function showReportVCTPerCategory()
	{
		$reportType = strtolower($this -> URI->_get_post('report_type'));
		// echo $reportType;
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCTPerCategory();
		$REPORT['vccategory'] = $this->{base_class_model($this)}->getVCReason($this->{base_class_model($this)}->getAccStatusVC($reportType));
		$REPORT['title'] = $this -> URI->_get_post('report_title');
		// echo "<pre>";
		// print_r($REPORT);
		// echo "</pre>";
		$this->load-> view("rpt_pvcvc/report_vct_category_html",$REPORT);
	}
	
	private function showReportVCPPerCategory()
	{
		$reportType = strtolower($this -> URI->_get_post('report_type'));
		// echo $reportType;
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCPPerCategory();
		$REPORT['vccategory'] = $this->{base_class_model($this)}->getVCReason($this->{base_class_model($this)}->getAccStatusVC($reportType));
		$REPORT['title'] = $this -> URI->_get_post('report_title');
		// echo "<pre>";
		// print_r($REPORT);
		// echo "</pre>";
		$this->load-> view("rpt_pvcvc/report_vcp_category_html",$REPORT);
	}

	
	
}