<?php 
//--------------------------------------------------------------------------
/*
 * @ package 		class conroller ReportCpaSummary  
 * @ auth 			uknown User 
 */

 class RptSms extends EUI_Controller 
{


	//--------------------------------------------------------------------------
	/*
	 * @ package 		EUI_Report_helper
	 */

	function __construct() 
	{
		parent::__construct();
		// $this->load->model(array('M_RptSms'));
		// $this->load->model(array(base_class_model($this),'M_RptSms'));
		$this->load->helper(array('EUI_Object','EUI_Report'));
	}
	
	public function index()
	{
		$this->load-> view("rpt_sms/report_sms_nav");
	}

	Public Function ShowReport()
	{
		$this->load->view('rpt_sms/rpt_show');
	}
	
	public function ShowExcel() {
		Excel() -> HTML_Excel(urlencode(_get_post('report_title')).'REPORT_SMS_BLAST-');
		$this->load->view('rpt_sms/rpt_excel');
	}
 }