<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class QtyCheckedList extends EUI_Controller
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function QtyCheckedList() 
 {

	parent::__construct();
		$this -> load ->model(array
		(
			base_class_model($this),
			'M_Combo','M_SrcCustomerList', 
			'M_SetCallResult','M_SetProduct', 
			'M_SetCampaign','M_SetResultCategory',
			'M_ModSaveActivity', 'M_SetResultQuality',
			'M_Pbx','M_FieldPolicy'
		)
   );	
 }
 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
function getAgentReady()
{
	$UserList = array();
	$UserState = $this -> _getCombo();
	
	if( $rows = $this -> {base_class_model($this)}->_getAgentReady())
	{
		foreach( $UserState['User'] as $UserId => $UserName )
		{
			if(in_array($UserId, array_keys($rows))){
				$UserList[$UserId] = $UserName;
			}	
		}
	}
return $UserList;
	
} 


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function index()
{
 if( $this ->EUI_Session ->_have_get_session('UserId') && class_exists('M_QtyCheckedList') )
 {
	$_EUI  = array( 
		'page' => $this->{base_class_model($this)} ->_get_default(),
		'Combo' => $this->_getCombo(),
		'UserState' => $this->getAgentReady(),
		'qulity_array' => array(1=> 'Checked', 11=>'Checked All')
	);
		
	if( is_array($_EUI))
	{
		$this -> load ->view('qty_approval_scoring/view_approval_checkedlist_nav',$_EUI);
	}	
 }
 
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Content()
{
  if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->{base_class_model($this)} ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
		$_EUI['quality'] = $this->M_SetResultQuality->_getQualityResult();
		$_EUI['qulity_array'] = array(1=> 'Checked', 11=>'Checked All');
		
		// sent to view data 
		
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('qty_approval_scoring/view_approval_checkedlist_list',$_EUI);
		}	
	}	
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function QualityResult()
{
	$_conds= array();
	$_conds = $this->M_SetResultQuality->_getQualityResult();
	
	foreach( $_conds as $k => $rows )
	{
		$_conds[$k] = $rows['name']; 
	}
	
	return $_conds;
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function LastCallHistory( $CustomerId = 0 )
{	
	$_conds = array();
	
	if( $this -> EUI_Session->_have_get_session('UserId') )
	{
		$_conds = $this -> {base_class_model($this)}->_getLastCallHistory( $CustomerId );
	}
	
	return $_conds;
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function CallResultId()
{
	$_conds = array();
	$datas = $this -> M_SetCallResult->_getCallReasonId();
	foreach($datas as $k => $rows ) {
		$_conds[$k] = $rows['name'];
	}
	
	return $_conds;
}
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function QualityDetail()
{
	$CustomerId = $this ->URI->_get_post('CustomerId');
	$UI = array
	( 
		/** get on model other reference **/
		
		'Customers' 	 => $this->M_SrcCustomerList->_getDetailCustomer($CustomerId),
		'Phones' 		 => $this->M_SrcCustomerList->_getPhoneCustomer($CustomerId),
		'AddPhone' 		 => $this->M_SrcCustomerList->_getApprovalPhoneItems($CustomerId),
		'RowPolicyData'  => $this->M_SrcCustomerList->_getRowPolicyByCustomer($CustomerId),
		'PolicyNumber'	 => $this->M_SrcCustomerList->_getPolicyCustomer($CustomerId),
		
		/** get on model this controller **/
		
		'Callhistory' 	 => $this->LastCallHistory($CustomerId),
		'CallCategoryId' => $this->M_SetResultCategory->_getOutboundCategory(),
		'CallResultId' 	 => $this->CallResultId(),
		'QualityApprove' => $this->QualityResult(),
		'Combo' 		 => $this->_getCombo(),
		
		'ScoringCategory' => $this->{base_class_model($this)}->_getScoringCategory($CustomerId),
		'ScoringQuestion' => $this->{base_class_model($this)}->_getScoringQuestion(),
		'CheckedList' => 'Checked'
	);
			
	// sen view 
	
	if($this ->URI->_get_have_post('CustomerId')) {
		$this -> load -> view('qty_approval_scoring/view_quality_detail',$UI);
	}
}

// update PolicyList if update by QA 

public function PolicyList()
{
	$CustomerId = $this ->URI->_get_post('CustomerId');
	$UI = array ( 
		'RowPolicyData'  => $this->M_SrcCustomerList->_getRowPolicyByCustomer($CustomerId),
		'CallCategoryId' => $this->M_SetResultCategory->_getOutboundCategory(),
		'CallResultId' 	 => $this->CallResultId()
	);
			
	// sen view 
	
	if($this ->URI->_get_have_post('CustomerId')) {
		$this -> load -> view('qty_approval_scoring/view_quality_update_policylist',$UI);
	}	
}





/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function SaveScoreQuality()
{
  $_conds = array('success' => 0 );
  
  if( $this ->EUI_Session->_have_get_session('UserId') 
	AND is_array($this->URI->_get_all_request()) )
 {
	if( $this->{base_class_model($this)}->_setSaveScoreQuality($this ->URI->_get_all_request())) {
		$_conds = array('success' => 1);
	}
 }
	
	__(json_encode($_conds));
}

 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function PolicyDetailById()
{
	$PolicyField = M_FieldPolicy::get_instance();
	$PolicyData = array();	
	if($this->EUI_Session->_have_get_session('UserId')) 
	{  
		$PolicyId =(INT)$this->URI->_get_post('PolicyId');	
		if($PolicyId) 
		{
			if( $PolicyData  = $this->{base_class_model($this)}->_getPolicyDataById($PolicyId) ) 
			{
				$Project = $this->M_SetCampaign->getAttribute($PolicyData['CampaignId']);
				if($Project)
				{
					$this->load->view('mod_contact_detail/view_contact_policydata', 
					array('PolicyLabel' => $PolicyField->_getFieldCompiler($Project['ProjectId'], $PolicyData )) ) ;			
				}
			}
		}
		
		
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Recording()
{
	$param = array(
		'CustomerId' => $this->URI->_get_post('CustomerId'),
		'pages' => $this->URI->_get_post('Pages'),
	);
	
	$ListView =  array( 
		'data'  => $this->{base_class_model($this)}->_getListVoice($param), 
		'page'  => $this->{base_class_model($this)}->_getPages($param),
		'records' => $this->{base_class_model($this)}->_getCountVoice($param),
		'current' => $this->URI->_get_post('Pages')
		);
	
	
	if( $ListView )
	{
		$this -> load -> view('qty_approval_scoring/view_quality_recording',$ListView);
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CallHistory()
{
	$CallHistory = null;
	if( $this -> URI->_get_have_post('CustomerId') )
	{
		$PollicyId = $this -> URI->_get_post('PolicyId');
		
		$CustomerId = $this -> URI->_get_post('CustomerId');
		if( $CustomerId ) {
			$CallHistory= array('CallHistory' => $this->M_ModSaveActivity->_getCallHistory($CustomerId, $PollicyId) );
		}
		
		$this -> load -> view("qty_approval_scoring/view_quality_history_content",$CallHistory);
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function ReloadQuestion()
{
	$CallHistory = null;
	if( $this -> URI->_get_have_post('CustomerId') )
	{
		$CustomerId = $this -> URI->_get_post('CustomerId');
		$PolicyId = $this->URI->_get_post('PolicyId');
		
		$UI = array (
			'Customers' => $this->M_SrcCustomerList->_getDetailCustomer($CustomerId),
			'PolicyId' => $this->URI->_get_post('PolicyId')
		);	
		$this -> load -> view("qty_approval_scoring/view_quality_reload_question",$UI);
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function VoicePlay()
{
	$_success = array('success'=>0);
	
	// voice data by click user ...
	
	$voice  = $this -> {base_class_model($this)}->_getVoiceResult($this->URI->_get_post('RecordId') );
	
	// if exist data then cek in PBX Server ...
	if( is_array($voice) ) 
	{
		$this->load->library('Ftp');
		
	// set parameter attribute 
	
		$PBX = $this->M_Pbx ->InstancePBX($voice['agent_ext']);
		$this->Ftp->connect(array(
			'hostname' => $FTP["FTP_SERVER{$PBX}"],
			'port' => $FTP["FTP_PORT{$PBX}"],
			'username' => $FTP["FTP_USER{$PBX}"],
			'password' => $FTP["FTP_PASSWORD{$PBX}"]) );
	
	// cek connection ID 
		
		if( ($this -> Ftp->_is_conn()!=FALSE) 
			AND (isset($voice['file_voc_loc'])) )
		{
			$_found   = null;
			
		// change directory on server remote ...
		
			$this->Ftp->changedir($voice['file_voc_loc']); 
			
		// show file on spesific location ..
		
			$_ftplist = $this -> Ftp -> list_files('.');
			foreach($_ftplist as $k => $src )
			{
				if( ($src == $voice['file_voc_name'])) {
					$_found = $src;
				}
			}
			
		// def location to local download 
		
			if(!defined('RECPATH') ) 
				define('RECPATH',str_replace('system','application', BASEPATH)."temp");
				
		// if match fil then download 
		
			if( !is_null($_found) ) {
				if($this -> Ftp -> download(RECPATH . '/' . $_found, $_found ) ) {
					$_success = array('success'=>1, 'data' => $voice );
				}
			}
		}
	}
	
	echo json_encode($_success);
}


// ApprovalAll 
public function UnApprovalAll()
{
	$conds = array('success'=>0);
	
	if( $this -> URI->_get_have_post('CustomerId') ){
		$CustomerId = $this -> URI->_get_array_post('CustomerId');
		if( is_array($CustomerId) )
		{
			if( $_bools = $this->{base_class_model($this)}->_setUnApprovalAll( $CustomerId ) )
			{
				$conds = array('success'=>1);
			}	
		}
		
	}
	
	__(json_encode($conds));
}
 
 
}