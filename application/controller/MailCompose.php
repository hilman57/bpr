<?php
/**
 * MailCompose - PHP creation and transport class.
 *
 * @package EUI_Controller
 *
 * @author omens ( rohmattullah ) < jombi.par@yahoo.com >
 * 
 */
 
class MailCompose extends EUI_Controller
{
 
private $mkdir_path = null;
	

/*
 * @ def 		: MailCompose contructor 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function MailCompose()
{
	parent::__construct();
	
	$this->load->model(array(base_class_model($this)));
	$this->mkdir_path = date('Y')."/".date('m') ."/". date('d');
  }	
  
/*
 * @ def 		: index
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index()
  {
	 if( $this ->EUI_Session -> _get_session('UserId') ) 
	 {
		 $this -> load-> view('mod_mail_compose/view_mail_compose');
	 }	
  }
  
/*
 * @ def 		: FileWriteAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function FileWriteAttachment( $OutboxId  = 0) 
{
 $var_log_path = array();
 
 if( (isset($_FILES)) AND (count($_FILES)>0) AND ($OutboxId > 0) )
 {
	$Config = $this->M_Configuration->_getMail(); // require mail 
	if( $Config )
	{ 
		$path   = $Config['outbox.path'];
		$mkdir  = $path . '/'. $this->mkdir_path .'/'. $OutboxId;
		
		// creation directory 
		if(!is_dir($mkdir)) {
			mkdir($mkdir, 0777,true);	
			system("chmod -R 0777 {$path}/{$this->mkdir_path}");
		}
		
		 $sn= 0;
		 foreach( $_FILES as $name => $rows ) 
			{
				if( move_uploaded_file( $rows['tmp_name'], $mkdir . '/' . $rows['name']))
				{
					$var_log_path[$sn]['path'] = $mkdir .'/'. $rows['name'];
					$var_log_path[$sn]['size'] = $rows['size'];
					$var_log_path[$sn]['mime'] = $rows['type'];
				}
				$sn++;
			}
		 
	 }	 
  }	
  
  return $var_log_path;
  
}
   
/*
 * @ def 		: FileSizeUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function FileSizeUpload ()
 {
	$max_file_upload = 0;
	
	if( isset($_FILES) 
		AND count($_FILES) > 0 )
	{
		$size = 0; 
		foreach( $_FILES as $key => $rows ) {
			$size +=(int)$rows['size'];
		}
		
		$max_file_upload =(INT)round(($size/1048576), 2);
	}
	
	return $max_file_upload;
 } 
  
 
/*
 * @ def 		: SaveMailToQueue
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function SaveMailToQueue()
 {
	 $conds = array('success' => 0, 'message' => '' );
	 if( $dataSize = self::FileSizeUpload() 
		AND  $dataSize  > 5 ) 
	{
		$conds = array('success' => 2, 'message' => 'Maximum Size 5 MB ' );
	}
	else 
	{
		if( $param = $this->URI->_get_all_request() )
		{
			$OutboxId =  $this->{base_class_model($this)}->_setSaveMailToQueue( $param );
			
			// set to save data CC / BCC / To
			
			if( $OutboxId ) 
			{
				$this->{base_class_model($this)}->_setMailDestination( $this->URI->_get_array_post('MailTo'), $OutboxId);
				$this->{base_class_model($this)}->_setMailCC( $this->URI->_get_array_post('MailCC'), $OutboxId );
				$this->{base_class_model($this)}->_setMailBCC( $this->URI->_get_array_post('MailBCC'), $OutboxId );
				
				$varlog = self::FileWriteAttachment($OutboxId);
				if(is_array($varlog)) {
					$this->{base_class_model($this)}->_setSaveAttachment($varlog, $OutboxId); 
				}
			}
			
			// set to Queue 
			
			if( $OutboxId ){
				if( $res = $this->{base_class_model($this)}->_setSaveQueue($OutboxId) )
				   $conds = array('success' => 1, 'message' => 'OK' );
			}
			
		}
	}
	 
	__(json_encode($conds));
	
  }
  
  
}

?>