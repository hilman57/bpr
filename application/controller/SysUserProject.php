<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SysUserProject extends EUI_Controller
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function SysUserProject()
 {
	parent::__construct();
	$this -> load -> model(array(base_class_model($this), 'M_SysUser','M_SetWorkProject'));
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this -> {base_class_model($this)} -> _get_default();
		if( is_array($_EUI) ) 
		{
			$this -> load -> view('sys_user_project/view_user_project_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this->{base_class_model($this)}->_get_page_number(); // load content data by pages 
		
		// sent to view data 
		
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('sys_user_project/view_user_project_list',$_EUI);
		}	
	}	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function AddUserWorkProject()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array
		(
			'UserPrivileges' => $this->M_SysUser->_get_handling_type(),
			'UserWorkProject' => $this ->M_SetWorkProject->_getWorkProjectId(), 
			'UserRegistration' => $this -> M_SysUser -> _getUserRegistration()
		);
		
		$this -> load -> view('sys_user_project/view_user_project_add', $UI );
	}
 }
 
  /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 public function WorkUserByLevel()
 {
	$User = array();
	if( $this->EUI_Session->_get_session('UserId') ) {
		$User = $this->M_SysUser->_getUserLevelGroup( $this -> URI->_get_post('ProfileId'));
	}
	
	__(form()->combo('UserId', 'select', $User ));
 }
 
 
 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function SaveUserWorkProject()
 {
	$_conds = array('success'=>0);
	
	if( $this -> EUI_Session -> _have_get_session('UserId') ) {
		if( $this -> {base_class_model($this)}->_setSaveUserWorkProject( $this ->URI-> _get_all_request() ))
		{
			$_conds = array('success'=>1);
		}
	}
	
	echo json_encode($_conds);
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 function EditUserWorkProject()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$UI = array
		(
			'UserPrivileges' => $this->M_SysUser->_get_handling_type(),
			'UserWorkProject' => $this ->M_SetWorkProject->_getWorkProjectId(), 
			'WorkUser' => $this ->{base_class_model($this)}->_getUserWorkProject( $this -> URI->_get_post('WorkId') ),
			'UserRegistration' => $this -> M_SysUser -> _getUserRegistration()
		);
		
		$this -> load -> view('sys_user_project/view_user_project_edit', $UI );
	}
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 function UpdateUserWorkProject()
 {
	$_conds = array('success'=>0);
	if( $this -> EUI_Session -> _have_get_session('UserId') ) 
	{
		if( $this -> {base_class_model($this)}->_setUpdateUserWorkProject( $this ->URI-> _get_all_request() ))
		{
			$_conds = array('success'=>1);
		}
	}
	__(json_encode($_conds));
 }
 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 function DeleteWorkUserProject()
 {
	$_conds = array('success'=>0);
	if( $this -> EUI_Session -> _have_get_session('UserId') ) 
	{
		if( $this->{base_class_model($this)}->_setDeleteWorkUserProject($this ->URI-> _get_array_post('WorkId')))
		{
			$_conds = array('success'=>1);
		}
	}
	echo json_encode($_conds);	
 }
 
 
}
?>