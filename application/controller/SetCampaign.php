<?php
ini_set("memory_limit", "2048M");
/*
 * E.U.I 
 *
 
 * subject	: get SetCampaign modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
class SetCampaign extends EUI_Controller
{
	
/*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
 public function SetCampaign() 
{
	parent::__construct();
	$this -> load -> model(array("M_SetCampaign","M_Utility","M_ModOutBoundGoal"));
}
	
/*
 * EUI :: index() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
function index()
{

 if( $this -> EUI_Session -> _have_get_session('UserId'))
 {
	// $EUI['page'] = $this -> M_SetCampaign -> _get_default();
	$EUI['javascript'] = $this->ManageJS();
	$this -> load -> view('set_campaign/view_campaign_nav', $EUI );
 }
 
}

/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */

function Content()
{
 if( $this -> EUI_Session -> _have_get_session('UserId'))
 {
	$EUI['page'] = $this->{base_class_model($this)}->_get_resource_query(); // load content data by pages 
	$EUI['num']  = $this->{base_class_model($this)}->_get_page_number(); 	// load content data by pages 
	$EUI['size'] = $this->{base_class_model($this)}->_get_size_campaign();
	
	$this -> load -> view('set_campaign/view_campaign_list', $EUI );
 }
}

/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
function AddTpl()
{
	if( $this -> EUI_Session -> _have_get_session('UserId')) 
	{
		$UI = array
		(
			'Utility'=> $this -> M_Utility,
			'OutboundGoals' => $this -> M_ModOutBoundGoal->_getOuboundGoals(),
			'Action' => $this->{base_class_model($this)}->_getMethodAction(), 
			'Method' => $this->{base_class_model($this)}->_getMethodDirection(),
			'Avail' => $this->{base_class_model($this)}->_getCampaignGoals(2)
		); 
		// 2=outbound 1/inbound
		$this -> load -> view('set_campaign/view_campaign_add',$UI);
	}
}


/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
function EditTpl()
{
	if( $this -> EUI_Session -> _have_get_session('UserId')) 
	{
		$UI = array
		(
			'Utility'=> $this -> M_Utility,
			'OutboundGoals' => $this -> M_ModOutBoundGoal->_getOuboundGoals(),
			'ProductCampaign' => array(),
			'Campaign' => $this->{base_class_model($this)}->getAttribute( $this -> URI->_get_post('CampaignId')),
			'Action' => $this->{base_class_model($this)}->_getMethodAction(), 
			'Method' => $this->{base_class_model($this)}->_getMethodDirection(),
			'Avail' => $this->{base_class_model($this)}->_getCampaignGoals(2)
		); 
		$this -> load -> view('set_campaign/view_campaign_edit',$UI);

	}
}

/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
	
function Update()
{
	$_conds = array("success"=>0);
	$_post_data = $this -> URI -> _get_all_request();
	if( isset($_post_data) 
		AND count($_post_data)> 0 )
	{
		if( $this -> {base_class_model($this)}-> _setUpdate($_post_data) )
		{
			$_conds = array("success"=>1);
		}
	}
	
	echo json_encode($_conds);
}

/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */

function View()
{
	$_post_data = $this -> URI -> _get_array_post("CampaignId");
	if(is_array($_post_data)) 
	{
		$data = array("result" => $this ->{base_class_model($this)}-> _getDataCampaignId($_post_data) );
		$this -> load -> view('set_campaign/view_campaign_views',$data);
	}
}


/*
 * EUI :: Content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */

function Manage()
{
	if( $this -> EUI_Session -> _have_get_session('UserId')) {
		$UI = array(
			'Method' => $this->{base_class_model($this)}->_getMethodDirection(),
			'AvailOut' => $this->{base_class_model($this)}->_getCampaignGoals(2),
			'AvailIn' => $this->{base_class_model($this)}->_getCampaignGoals(1)
		); 
		$this -> load -> view('set_campaign/view_manage_campaign',$UI);
	}
}


/*
 * EUI :: Export() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
	
function Export()
{

 $_post_data = $this -> URI -> _get_array_post("CampaignId");
 
 if(is_array($_post_data) AND is_array($_post_data))
 {
	foreach( $_post_data as $keys => $campaignid )
	{
		$campaign = $this->{base_class_model($this)}->getAttribute( $campaignid );
		$data = array(	"campaign" => $campaign['CampaignDesc'],"campaignId" => array(0=>$campaign['CampaignId']));
		$this -> load -> view('set_campaign/view_campaign_export',$data);
	}
	
	
	// echo "<pre>";
	// print_r($_post_data);
	// echo "</pre>";
	// $this -> load -> view('set_campaign/view_campaign_export',$data);
 }
 
}

/*
 * EUI :: Export() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
function ExportDeleted(){
	
 $_post_data = $this -> URI -> _get_array_post("CampaignId");
 
 if(is_array($_post_data) AND is_array($_post_data))
 {
	foreach( $_post_data as $keys => $campaignid )
	{
		$campaign = $this->{base_class_model($this)}->getAttribute( $campaignid );
		$data = array(	"campaign" => $campaign['CampaignDesc'],"campaignId" => array(0=>$campaign['CampaignId']));
		$this -> load -> view('set_campaign/view_campaign_export_deleted',$data);
	}
 }
}


/*
 * EUI :: SaveCampaign() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
function SaveCampaign()
{
	$_success = array('success'=> 0, 'error'=> ''); 
	$_post = array();
	if( $this -> EUI_Session -> _have_get_session('UserId') ) 
	{
		$_post_data = $this -> URI -> _get_all_request();


		if( isset($_post_data['CampaignNumber']) )
		{
			if( $this -> M_SetCampaign -> _set_save_campaign()) 
			{
				$_success = array('success'=>1, 'error'=> ''); 
			}
		}
	}
	
	echo json_encode($_success);
}
	
/*
 * EUI :: getDataInbound() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
	
function getDataInbound()
{
	$_success = array('success'=> 0, 'data'=> 0 ); 
	
	if( $this -> EUI_Session->_have_get_session('UserId'))
	{
		$data = $this -> {base_class_model($this)}->_getDataInbound($this -> URI->_get_post('CampaignId'));
		if( !is_null($data) 
			AND (INT)$data > 0 )
		{
			$_success = array('success'=> 1, 'data'=> $data ); 
		}	
	}
	
	echo json_encode($_success);
}

/*
 * EUI :: ManageCampaign() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
function ManageCampaign()
{
	$_success = array('success'=> 0); 
		
	if( $this -> EUI_Session->_have_get_session('UserId'))
	{
		$parameter = $this->URI->_get_all_request();
		if( is_array($parameter))
		{
			$jumlah = $this->{base_class_model($this)}->_setManageCampaign($parameter);
			
			if($jumlah)
			{
				$_success = array('success'=>1, 'data' => $jumlah);
			}
		}	
	}
	
	echo json_encode($_success);
}

 private function ManageJS()
 {
	$Pre_js = array ( 
						'page' =>$this -> {base_class_model($this)} -> _get_default() 
					);
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_ROOT) ) )
	{
		$js = $this -> load -> view('set_campaign/js_campaign_root',$Pre_js,TRUE);
	}
	else
	{
		$js = $this -> load -> view('set_campaign/js_campaign_public',$Pre_js,TRUE);
	}
	
	return $js;
 }
 
}

?>