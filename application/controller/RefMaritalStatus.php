<?php 
/* http://localhost:8080/AIA/index.php/RefMaritalStatus?_=1410451932057 */

class RefMaritalStatus extends EUI_Controller
{

/* superclass controller handle **/

public function RefMaritalStatus(){
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
}

	
/*** index:: handle ***/

function index()
{
	if($this->EUI_Session->_have_get_session('UserId') ) 
	{
		$UI['page'] = $this->{base_class_model($this)}->_get_default();
		$this->load->view('ref_marital_status/view_marital_nav',$UI);
		
	}	
}

/*** content data of list page **/

function content()
 {
	if($this->EUI_Session->_have_get_session('UserId'))
	{
		$UI['page'] = $this->{base_class_model($this)}->PageResouce(); // load content data by pages 
		$UI['num']  = $this->{base_class_model($this)}->PageNumber();  // load content data by pages 
		
		$this -> load -> view('ref_marital_status/view_marital_list',$UI);	
	}	
 }

 

}


