<?php

class Report extends EUI_Controller {

	function index()
	{
		$action 				  = _get_post('action');
		$data['start_date']		  = _get_post('start_date');
		$data['end_date'] 		  = _get_post('end_date');
		$data['GroupCallCenter']  = _get_post('GroupCallCenter');
		$data['AgentId'] 		  = _get_array_post('AgentId');
		$data['TlId']			  = _get_post('user_tl');
		
		switch($action){
		case 'tracking-report':
			$this->load->view('rpt_summary/tracking_report',$data);
			break;
		case 'ptp-report':
			$this->load->view('rpt_summary/report_ptp',$data);
			break;
		case 'skill-report':
			$this->load->view('rpt_summary/skill_dev',$data);
			break;
		case 'data-keep':
			$this->load->view('rpt_summary/data_keep',$data);
			break;
		case 'data-review':
			$this->load->view('rpt_summary/data_review',$data);
			break;
		case 'summary-cpa':
			$this->load->view('rpt_summary/report_cpa_summary',$data);
			break;
		case 'list-lunas':
			$this->load->view('rpt_summary/report_lunas_summary',$data);
			break;
		case 'cpa_status_report':
			$this->load->view('rpt_summary/report_cpa_status',$data);
			break;
		case 'account_lunas':
			$this->load->view('rpt_summary/report_account_lunas_cf',$data);
			break;
		case 'account_batal':
			$this->load->view('rpt_summary/report_account_batal_cf',$data);
			break;
		}
	}
}

?>