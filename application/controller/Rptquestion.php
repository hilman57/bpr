<?php

/*
 * @ class : Rptquestion 
 * @ param : index 
 */
 
 class Rptquestion 
	extends EUI_Controller
{


// -------------------------------------
/*
 * @ pack : constructor 
 */
 
 public function __construct()
{
	parent::__construct();	
	$this->load->model(array(base_class_model($this)));
	$this->load->helper(array('EUI_Object','EUI_Report'));
	
}
 
 public function index() 
{ 
	$UI = $this->{base_class_model($this)}->_reportquestion();
	$this->load->view('mgt_dstquestioner/detailrpt', $UI);  
}
 



 
}
?>