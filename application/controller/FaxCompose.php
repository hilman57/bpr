<?php

class FaxCompose extends EUI_Controller
{

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function FaxCompose()
{
	parent::__construct();
	$this -> load->model(array(base_class_model($this),'M_Configuration'));
}
	
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 	
public function index()
{
	if( $this -> EUI_Session->_have_get_session('UserId'))
	{
		$this -> load -> view("mod_fax_compose/view_fax_compose");
	}
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 	
public function FaxFile()
{
  if( $this -> EUI_Session->_have_get_session('UserId'))
  {
	$this -> load -> view("mod_fax_compose/view_fax_file");
  }
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 	
public function FaxText()
{
  if( $this -> EUI_Session->_have_get_session('UserId'))
  {
	$this -> load -> view("mod_fax_compose/view_fax_text");
  }
}


/*
 * @ def 		: UploadDataExcel
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _setMoving( $files = null, $path='', $ux = 0 )
{

 $_conds = false;
 if(isset($files['name']))
 {
	// create dir if not exits 
	
	$_file_arrays = explode('.', $files['name']);
	$_md5 = md5(md5(date('H:i:s'))).$ux;
	$_file_extension = $_md5 .'.'.$_file_arrays[count($_file_arrays)-1];
	$_dir_fax = $path['upload.base_path']. date('Y') .'/'. date('m').'/'. date('d');
	
	if( count($_file_arrays) > 1)
	{
		if(!is_dir($_dir_fax))
		{
			mkdir($_dir_fax, 0777, true);
			if( copy($files['tmp_name'], $_dir_fax .'/'. $_file_extension )) 
			{
				$_conds = array( 
					'FULL_PATH' => $_dir_fax .'/'.$_file_extension,
					'FILE_NAME' => $_file_extension
				);
			}
		}
		else 
		{
			if( copy($files['tmp_name'], $_dir_fax .'/'. $_file_extension )) 
			{
					$_conds = array( 
						'FULL_PATH' => $_dir_fax .'/'.$_file_extension,
						'FILE_NAME' => $_file_extension
					);
			}
		}
	}
 }

 return $_conds;
} 

/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */


function _getFileType( $fileType ) 
{
	$_preg = explode("/", $fileType);
	return ( (count($_preg)> 1 ? strtoupper($_preg[1]) : null ));	
}


/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _getDestination( $FaxNumber = 0 ) 
{
    $results = $FaxNumber;
	$_FAX =& M_Configuration::get_instance();
	if(is_array($_FAX_VALUES = $_FAX -> _getFaximile()) )
	{
		if(preg_match("/^{$_FAX_VALUES['FAX_LOCAL_NUMBER']}/", $FaxNumber))
		{
			$results = substr($FaxNumber, strlen($_FAX_VALUES['FAX_LOCAL_NUMBER']), strlen($FaxNumber)); 
		}
	}
	
	return $results;
}

/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function FaxFileUpload()
{

  $_conds = array('success' => 0 );
  if( $this -> EUI_Session -> _have_get_session('UserId')) 
  {
	 $_FAX_USERS = $this -> EUI_Session->_get_session('Username');
	 $_FAX =& M_Configuration::get_instance();
	 
	 if(is_array($_FAX_VALUES = $_FAX -> _getFaximile()) )
	 {
		$_FAX_NUMBER = $this -> _getDestination( $this -> URI->_get_post('FaxNumber')); $UX = 0;
		if(is_array($_FILES) 
			AND ($_FAX_NUMBER) ) foreach( $_FILES as $_FAX_UPLOAD)
		{
			if( $_FAX_PATH = $this -> _setMoving( $_FAX_UPLOAD, $_FAX_VALUES, $UX ) )
			{
				$FAX_CONTENT_DATA['DataDestNo'] 	  = $_FAX_NUMBER;
				$FAX_CONTENT_DATA['DataCreatedBy'] 	  = $_FAX_USERS;
				$FAX_CONTENT_DATA['DataPath'] 		  = $_FAX_PATH['FULL_PATH']; 
				$FAX_CONTENT_DATA['DataFilename'] 	  = $_FAX_PATH['FILE_NAME']; 
				$FAX_CONTENT_DATA['DataOrigFilename'] = $_FAX_UPLOAD['name'];
				$FAX_CONTENT_DATA['DataType'] 		  = $this -> _getFileType($_FAX_UPLOAD['type']);
				$FAX_CONTENT_DATA['DataDateCreated']  = date('Y-m-d H:i:s');
				
				print_r($FAX_CONTENT_DATA);
				// sent to model 
				
				if( $this -> {base_class_model($this)} -> _setSaveFaxData($FAX_CONTENT_DATA))
				{
					$UX++;
				}
			}
		}
	 }	
	 
	 if( $UX > 0 ) 
		$_conds = array('success' => 1);
  }
  
 // call back 
 
__(json_encode($_conds));

}


/*
 * @ def 		: FaxFileUpload
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function FaxFileText() 
{
	$_conds = array('success' => 0);
	
	$_FAX_CONTENT 	= addslashes($this->URI->_get_post('FaxContent'));
	$_FAX_USERNAME 	= $this->EUI_Session->_get_session('Username');
	if( $_FAX_NUMBER 	= $this->_getDestination( $this -> URI->_get_post('FaxNumber') ) )
	{
		$FAX_CONTENT_DATA['DataDestNo'] = $_FAX_NUMBER;
		$FAX_CONTENT_DATA['DataContent'] = $_FAX_CONTENT;
		$FAX_CONTENT_DATA['DataCreatedBy'] = $_FAX_USERNAME;
		$FAX_CONTENT_DATA['DataType'] = 'TEXT';
		$FAX_CONTENT_DATA['DataSource'] =  0;
		$FAX_CONTENT_DATA['DataDateCreated'] = date('Y-m-d H:i:s');
		
		if( $this -> {base_class_model($this)} -> _saveCreateFax($FAX_CONTENT_DATA) )
		{
			$_conds = array('success' => 1);
		}
	}
	
	__(json_encode($_conds));
}

	
}

?>