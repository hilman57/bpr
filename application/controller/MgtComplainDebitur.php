<?php
/*
 * E.U.I 
 *
 
 * subject	: Manage complain debitur 
 * 			  extends under Controller class
 */
 
class MgtComplainDebitur extends EUI_Controller
{
	/*
	 * @ def 		: __construct // constructor class 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function __construct() 
	{
		parent::__construct();
		$this->load->model(array(base_class_model($this),'M_SetCampaign',
				'M_SysUser','M_Combo'));
	}
	
	public function index()
	{
		if( $this->EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI  = array(
				'page' => $this ->{base_class_model($this)} ->_get_default(),
				'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
				'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
			);
			 
			if(is_array($_EUI)) 
			{
				$this -> load ->view('mgt_complain_deb/complain_deb_nav',$_EUI);
			}	
		}
	}
	
	/*
	 * @ def 		: content / default pages controller 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function Content() 
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
			$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 

			if( is_array($_EUI) && is_object($_EUI['page']) ) 
			{
				$this -> load -> view('mgt_complain_deb/complain_deb_list',$_EUI);
			}	
		}	
	}
	
	
	public function EditComplain()
	{
	
		$data_complain = $this ->{base_class_model($this)}->_getComplain(
			array("IdComplain"=>_get_post('IdComplain'))
		);
		$UI['SOURCE_COMPLAIN'] = $this->M_Combo->_getSourceComplain();
		$UI['KASUS_KOMPLEN'] = $this->M_Combo->_getKasusKomplen();
		$UI['IdComplain'] = _get_post('IdComplain');
		$UI['COMPLAIN'] = $data_complain[0];
		$UI['RESPON'] = $this->M_Combo->_getResponseComplain();
		// echo "<pre>";
		// print_r($UI);
		// echo "<pre>";
		$this->load->view('mgt_complain_deb/form_edit_complain',$UI);
	}
	
	public function update_complain_note()
	{
		$_conds = array('success'=>0 );
		if( $this->EUI_Session->_get_session('UserId') )
		{
			if( $this ->{base_class_model($this)}->_UpdateComplain() )
			{
				$_conds = array('success'=>1);
			}
		}	

		echo json_encode($_conds);
		
	}
}

//end class