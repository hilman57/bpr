<?php 
/*
 * @ pack : manage round data its
 */
 
class MgtRandDeb extends EUI_Controller
{
	/*
	 * @ pack : function data # ------------
	 */ 

	public function MgtRandDeb()
	{
		parent::__construct();
		$this->load->model(array( base_class_model($this)));
	}
	
	public function index()
	{	
		if( $this->EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI  = array(
						'page' => $this ->{base_class_model($this)} ->_get_default()
					);
			$this->load->view("mgt_random_debitur/mgt_randdeb_nav",$_EUI);
		}
		
	}
	
	/*
	 * @ def 		: content / default pages controller 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	 public function Content() 
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
			$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 

			if( is_array($_EUI) && is_object($_EUI['page']) )
			{
				$this -> load -> view('mgt_random_debitur/mgt_randdeb_list',$_EUI);
			}	
		}	
	}

	public function get_random_by()
	{
		$modul = $this->{base_class_model($this)}->get_modul_random();
		echo json_encode($modul);
	}
	
	public function PanelAccessAll()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$this -> load -> view('mgt_random_debitur/pnl_accessall_nav');
		}
	}
	
	public function PanelAccessAllPage()
	{ 
		// echo "tes";
		if(_have_get_session('UserId') )
		{
			$this->load->helper(array('EUI_Object'));	
			$this->start_page = 0;
			$this->per_page   = 10;
			$this->post_page  = 0;
			if(_get_have_post('page') )  {
				$this->post_page  = (INT)_get_post('page');
			}
			
			
			// @ pack : set it 

			$this->result_swap = array();
			// $post = new EUI_Object(_get_all_request());
			// echo "<pre>";
			// print_r($this->result_post);
			// echo "</pre>";
			$this->panel_access_all = $this->{base_class_model($this)}->_get_detail_access_all();

			// @ pack : record 

			$this->total_records = count($this->panel_access_all);

			// @ pack : set its  
			if( $this->post_page) {
				$this->start_page = (($this->post_page-1)*$this->per_page);
			} else {	
				$this->start_page = 0;
			}

			// @ pack : set result on array
			if( (is_array($this->panel_access_all)) 
			AND ( $this->total_records > 0 ) )
			{
				$this->result_swap = array_slice($this->panel_access_all, $this->start_page, $this->per_page);
			}	

			// @ pack : then set it to view 

			$SwapData['content_pages'] = $this->result_swap;
			$SwapData['total_records']  = $this->total_records;
			$SwapData['total_pages']  = ceil($this->total_records/ $this->per_page);
			$SwapData['select_pages'] = $this->post_page;
			$SwapData['start_page']  = $this->start_page;

			if( TRUE == count( $this->URI->_get_all_request() ) > 0)
			{
				$this -> load -> view('mgt_random_debitur/pnl_accessall_page', $SwapData );
			}

		}
	}
	
	public function PanelDetilRound()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$this -> load -> view('mgt_random_debitur/pnl_round_nav');
		}
	}
	
	public function PanelRoundPage()
	{ 
		// echo "tes";
		if(_have_get_session('UserId') )
		{
			$this->load->helper(array('EUI_Object'));	
			$this->start_page = 0;
			$this->per_page   = 10;
			$this->post_page  = 0;
			if(_get_have_post('page') )  {
				$this->post_page  = (INT)_get_post('page');
			}
			
			
			// @ pack : set it 

			$this->result_swap = array();
			// $post = new EUI_Object(_get_all_request());
			// echo "<pre>";
			// print_r($this->result_post);
			// echo "</pre>";
			$this->panel_round = $this->{base_class_model($this)}->_get_detail_round();
			// echo "<pre>";
			// print_r($this->panel_round);
			// echo "</pre>";
			// @ pack : record 

			$this->total_records = count($this->panel_round);

			// @ pack : set its  
			if( $this->post_page) {
				$this->start_page = (($this->post_page-1)*$this->per_page);
			} else {	
				$this->start_page = 0;
			}

			// @ pack : set result on array
			if( (is_array($this->panel_round)) 
			AND ( $this->total_records > 0 ) )
			{
				$this->result_swap = array_slice($this->panel_round, $this->start_page, $this->per_page);
			}	

			// @ pack : then set it to view 

			$SwapData['content_pages'] = $this->result_swap;
			$SwapData['total_records']  = $this->total_records;
			$SwapData['total_pages']  = ceil($this->total_records/ $this->per_page);
			$SwapData['select_pages'] = $this->post_page;
			$SwapData['start_page']  = $this->start_page;

			if( TRUE == count( $this->URI->_get_all_request() ) > 0)
			{
				$this -> load -> view('mgt_random_debitur/pnl_round_page', $SwapData );
			}

		}
	}
	// public function DetailRound()
	// {
		// if( $this -> EUI_Session->_have_get_session('UserId') ) 
		// {
			// $this -> load -> Model('M_Website');
			// $data_webs = $this -> M_Website -> _web_get_data();
			// $ui_detail_setup['website'] = $data_webs['website'];
			// $this -> load -> view('mgt_random_debitur/view_detail_setup',$ui_detail_setup);
		// }
	// }
	
	public function UIRoundStatus()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$dropdown['Dropdown'] = $this->{base_class_model($this)}->_get_dropdown();
			$dropdown['spv'] = $this->M_SysUser->_get_supervisor();
			// $dropdown['tl'] = $this->M_SysUser->_get_teamleader(array(
				// 'user_state'=>1
			// ));
			$dropdown['RandomBy'] = _get_post('random_by');
			if( is_array( $dropdown ) )
			{
				$this -> load -> view('mgt_random_debitur/view_round_status',$dropdown);
			}
		}
	}
	
	public function UIRoundUpload()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			
			$dropdown['template'] = $this->M_MgtBucket->_get_template(5);
			$dropdown['product'] = $this->M_SetCampaign->_get_campaign_name();
			$dropdown['spv'] = $this->M_SysUser->_get_supervisor();
			if( is_array( $dropdown ) )
			{
				$this -> load -> view('mgt_random_debitur/view_round_upload',$dropdown);
			}
		}
	}
	
	/** fungsi memanggil model untuk menyimpan pengaturan perputaran **/
	public function RoundByStatus()
	{
		$call_back_client=array('success'=>0);
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$call_back_client = $this->{base_class_model($this)}->_SetRoundStatus();
			// if($call_back_client['success'] == 1)
			// {
				// $round_batch = FCPATH ."application/batch";
				// exec("cd ".$round_batch." && ./round_process start",$batch_message);
			// }
		}

		echo json_encode($call_back_client);
		
		// echo FCPATH;
		// /var/www/html/hsbc_20160511/
	}
	
	public function RunModul()
	{
		
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$alert = array('success'=>1, 'msg'=>"");
			$randomby = $this->URI->_get_post('random_by');
			$modul_random = $this->{base_class_model($this)}->get_modul_random();
			$running_modul = $this->{base_class_model($this)}->ModulRunning(array_keys($modul_random['combo']));
			if($running_modul['running'] === true )
			{
				if($running_modul['random_id'] != $randomby)
				{
					$alert['success'] = 0;
					$alert['msg'] = $modul_random['combo'][$running_modul['random_id']]." is running, please stop it first";
				}
			}
			echo json_encode($alert);
		}
		
		
	}
	
	/*public function ForceRound()
	{
		// $cond  = array('success'=> 0);
		// $call_back_client = $this->{base_class_model($this)}->_RoundDebitur();
		// if($call_back_client)
		// {
			// $cond  = array('success'=> 1);
		// }
		// echo json_encode($cond);
		$cond  = array('success'=> 0);
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$call_back_client = $this->{base_class_model($this)}->_force_process_round();
			if($call_back_client)
			{
				$cond  = array('success'=> 1);
			}
		}

		echo json_encode($cond);
	}*/
	
	public function StopRandomModule()
	{
		$_conds=array();
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$round_setup = $this ->M_MgtRandDeb->_get_detail_round( $this->URI->_get_post('SetupId') );
			$_conds = $this->{base_class_model($this)}->_StopRandomModule( $this->URI->_get_post('SetupId') );
			// if($_conds['Message']==1)
			// {
				// $round = array(2,3) ;
				// if (in_array($round_setup['modul_id_rnd'], $round))
				// {
					// $round_batch = FCPATH ."application/batch";
					// exec("cd ".$round_batch." && ./round_process stop",$batch_message);
				// }
			// }
		}
		echo json_encode($_conds);
	}
	
	public function CheckClaimReq()
	{
		$conds = array();
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$conds = $this->{base_class_model($this)}->_CheckClaimReq();
		}
		echo json_encode($conds);
		
	}
	
	public function LockAgentRandom()
	{
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			$dropdown['Dropdown'] = $this->{base_class_model($this)}->_get_dropdown();
			if( is_array( $dropdown ) )
			{
				$this -> load -> view('mgt_random_debitur/view_setuplock_random',$dropdown);
				// echo "tes";
			}
		}
	}
	
	public function lock_agent_nav()
	{
		if( $this->EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI  = array(
						'page' => $this ->{base_class_model($this)} ->_get_default_lock_random()
					);
			// print_r($_EUI);
			$this->load->view("mgt_random_debitur/view_agentlock_nav",$_EUI);
		}
	}
	
	public function lock_agent_list()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource_lock_random();    // load content data by pages 
			$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number_lock_random(); // load content data by pages 
			// print_r($_EUI);
			// var_dump(is_array($_EUI) && is_object($_EUI['page']));
			if( is_array($_EUI) && is_object($_EUI['page']) )
			{
				$this -> load -> view('mgt_random_debitur/view_agentlock_list',$_EUI);
				// echo "cetak";
			}	
		}
	}
	
	public function ManageAgentLock()
	{
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$ActDeskoll =$this->M_SysUser->_get_teleamarketer(array('user_state'=>1,
																'logged_state'=>1));
			$Content['agent_list'] = $ActDeskoll;
			if(is_array($Content) )
			{
				$this -> load -> view("mgt_random_debitur/view_agentlock_random", $Content);
			}
		}
		
	}
	
	public function SetAgentLock()
	{
		$cond = array('status'=>0,'message'=>"");
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$cond = $this ->{base_class_model($this)}-> save_lock_agent();
		}
		echo json_encode($cond);
	}
	
	public function LockAgentByCheck()
	{
		$cond = array('status'=>0,'message'=>"");
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$cond = $this ->{base_class_model($this)}-> setlockbycheck();
		}
		echo json_encode($cond);
	}
	
	public function UnlockAgentByCheck()
	{
		$cond = array('status'=>0,'message'=>"");
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$cond = $this ->{base_class_model($this)}-> setunlockbycheck();
		}
		echo json_encode($cond);
	}
	
	public function UploadPanelAccessAll()
	{
		$this->load->model(array('M_MgtBucket'));
		$templatetype = $this -> URI->_get_post('templatetype');
		if( $this->EUI_Session->_have_get_session('UserId') ) 
		{
			// $Template = $this->{base_class_model($this)}->_get_template();
			// foreach($Template as $key => $val){
				// $tmptype = explode("_",$val);
				// if($tmptype[0] == "UPLOAD" && ($tmptype[1] != "SWAP" || $tmptype[1] != "ACCESS")){
					// $Template['Template'][$key] = $val;
				// }else if($tmptype[0] == "UPDATE"){
					// $Template['Template'][$key] = $val;
				// }
			// }
			
			$Template['Template'] = $this->M_MgtBucket->_get_template($templatetype);
			$Template['CampaignId'] = $this->M_SetCampaign->_get_campaign_name();
			$Template['auto'] = array('1'=>"YES",'0'=>"NO");
			$Template['timedefault'] = $this->LoadEndExp();
			$Template['spv'] = $this->M_SysUser->_get_supervisor();
			
			if(is_array($Template) )
			{
				$this -> load -> view("mgt_random_debitur/access_panel_upload", $Template);
			}	
		}
		// echo var_dump( method_exists($this,'LoadEndExp'));		
	}
	public function LoadTimeExp()
	{
		if( $this->EUI_Session->_have_get_session('UserId') ) 
		{
			$auto = array("1"=>'LoadStartEndExp',"0"=>'LoadEndExp');
			// echo '<table align="left" border=0 cellpadding="5px">
			// <tr>
				// <td class="left text_caption bottom" style="height:24px;"># Start Time&nbsp;</td>
				// <td class="center text_caption bottom">:</td>	
				// <td class="bottom" nowrap>'
						// .form()->combo('access_start_hour','select box', array(11=>11), '17', null, array('style'=>'width:50px;') ).'&nbsp;:'
						// .form()->combo('access_start_min','select box', array(11=>11), '59', null, array('style'=>'width:50px;')).'&nbsp;:'
						// .form()->combo('access_start_sec','select box', array(11=>11), '59', null, array('style'=>'width:50px;')).
					// '</td>
			// </tr>
			
			// <tr>
				// <td class="left text_caption bottom" style="height:24px;"># End Time&nbsp;</td>
				// <td class="center text_caption bottom">:</td>	
				// <td class="bottom" nowrap>'
						// .form()->combo('access_end_hour','select box', array(11=>11), '17', null, array('style'=>'width:50px;') ).'&nbsp;:'
						// .form()->combo('access_end_min','select box', array(11=>11), '59', null, array('style'=>'width:50px;')).'&nbsp;:'
						// .form()->combo('access_end_sec','select box', array(11=>11), '59', null, array('style'=>'width:50px;')).
					// '</td>
			// </tr></table>';
			$choice = $this -> URI->_get_post('autostart');
			if(isset($auto[$choice]))
			{
				if( method_exists($this,$auto[$choice]))
				{
					echo $this->$auto[$choice]();
				}
			}
			
		}
		
	}
	private function LoadStartEndExp()
	{
		$hours = $this->{base_class_model($this)}->_get_select_hour(date("H"));
		$men = $this->{base_class_model($this)}->_get_select_minute();
		$sec = $this->{base_class_model($this)}->_get_select_seconds();
		$combo = '<table align="left" border=0 cellpadding="5px">
			<tr>
				<td class="left text_caption bottom" style="height:24px;"># Start Time&nbsp;</td>
				<td class="center text_caption bottom">:</td>	
				<td class="bottom" nowrap>'
						.form()->combo('access_start_hour','select box', $hours, date("H"), null, array('style'=>'width:50px;') ).'&nbsp;:'
						.form()->combo('access_start_min','select box', $men, date("i"), null, array('style'=>'width:50px;')).'&nbsp;:'
						.form()->combo('access_start_sec','select box', $sec, date("s"), null, array('style'=>'width:50px;')).
					'</td>
			</tr>
			
			<tr>
				<td class="left text_caption bottom" style="height:24px;"># End Time&nbsp;</td>
				<td class="center text_caption bottom">:</td>	
				<td class="bottom" nowrap>'
						.form()->combo('access_end_hour','select box', $hours, date("H"), null, array('style'=>'width:50px;') ).'&nbsp;:'
						.form()->combo('access_end_min','select box', $men, date("i"), null, array('style'=>'width:50px;')).'&nbsp;:'
						.form()->combo('access_end_sec','select box', $sec, date("s"), null, array('style'=>'width:50px;')).
					'</td>
			</tr></table>';
		return $combo;
	}
	private function LoadEndExp()
	{
		$hours = $this->{base_class_model($this)}->_get_select_hour(date("H"));
		$men = $this->{base_class_model($this)}->_get_select_minute();
		$sec = $this->{base_class_model($this)}->_get_select_seconds();
		$combo = '<table align="left" border=0 cellpadding="5px">
			<tr>
				<td class="left text_caption bottom" style="height:24px;"># End Time&nbsp;</td>
				<td class="center text_caption bottom">:</td>	
				<td class="bottom" nowrap>'
						.form()->combo('access_end_hour','select box', $hours, date("H"), null, array('style'=>'width:50px;') ).'&nbsp;:'
						.form()->combo('access_end_min','select box', $men, date("i"), null, array('style'=>'width:50px;')).'&nbsp;:'
						.form()->combo('access_end_sec','select box', $sec, date("s"), null, array('style'=>'width:50px;')).
					'</td>
			</tr></table>';
		return $combo;
	}
	
	public function StopAccessallPanel()
	{
		$cond = array('Message'=>0);
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$cond = $this ->{base_class_model($this)}-> _stopPanelAcessAll();
		}
		echo json_encode($cond);
	}
	
	public function RoundperTeam()
	{
		if($this -> URI->_get_post('bool_round_team') == 1)
		{
			$tl = $this->M_SysUser->_get_teamleader(array(
				'user_state'=>1
			));
			$combo = '<table align="left" border="0" cellpadding="5px">
						<tr>
							<td class="text_caption bottom"  valign="top"># Teamleader &nbsp;:</td>
							<td class="bottom" valign="top">'.form()->listCombo("round_tl","select auto",$tl).'</td>
						</tr>
					</table>';
		}
		else
		{
			$combo="&nbsp;";
		}
		
		echo $combo;
		
	}
	
	public function StopRoundPanel()
	{
		$cond = array('Message'=>0);
		if( $this -> EUI_Session->_have_get_session('UserId') ) 
		{
			$cond = $this ->{base_class_model($this)}-> _stopPanelRound();
		}
		echo json_encode($cond);
	}
}
//END OF Class
?>