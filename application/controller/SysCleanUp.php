<?php
class SysCleanUp extends EUI_Controller
{
 
 var $argv = array();
 var $swaps = array();
 
 public function __construct()
 {
	global $argv;
	parent::__construct();
	
	foreach( $argv as $key => $value )
	{
		$this->argv[$key] = $value;
	}
	
	$this->logs=& EUI_WriteLog::get_instance();
	$this->logs->_relative_write_filename( dirname(__FILE__),'clean.txt');
 
 }

/*
 * @ pack: clean system 
 */ 
 
 public function Clean()
{
  
 if(isset($this->argv[3]) )
 {
    $filter = trim($this->argv[3]);
	$qry = $this->db->query("SHOW TABLES"); 
	if( $qry->num_rows()>0 )
	  foreach( $qry->result_assoc() as $rows )
	{
		foreach( $rows as $key => $name )
		{
			if( preg_match("/^$filter/i", $name) ){
				$this->swaps[$name] = $name;
			}
		}	
	}
	
// @ pack : line of reset drop its 	
	if((count($this->swaps)>0)) 
	 foreach( $this->swaps as $i_s => $i_n )
	{
		if( $conds = $this->db->query("DROP TABLES $i_n") )
		{
			$this->logs->_write_content_log("$i_n\r");
			echo "success drop table -> $i_n -> done.\n\r";
		}
	}
	
	exit("done");
	
 } else {
	exit("No filter detected.");
 }

}



}
?>