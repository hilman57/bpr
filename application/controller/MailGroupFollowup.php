<?php

/*
 * @def 	: FaxContent
 * ------------------------------------
 * @param 	: array()
 */
 
class MailGroupFollowup extends EUI_Controller
{

/*
 * @def 	: __construct
 * ------------------------------------
 * @param 	: array()
 */
 
public function __construct()
{
	parent::__construct();
	$this->load->model(array( base_class_model($this),'M_SetWorkProject'));
}

/*
 * @def 	: index
 * ------------------------------------
 * @param 	: array()
 */
 
public function index()
{
  if($this->EUI_Session->_get_session('UserId'))
  {
	 $UI = array('page'=>$this -> {base_class_model($this)}->_get_default() );	
	 $this->load->view('mod_mail_group/view_mail_group_nav',$UI);
  }
  
}

/*
 * @def 	: content
 * ------------------------------------
 * @param 	: array()
 */
 
public function content()
{
	if($this->EUI_Session->_have_get_session('UserId'))
	{
		$UI = array(
		  'page'=> $this->{base_class_model($this)}->_get_resource(),
		  'num' => $this->{base_class_model($this)}->_get_page_number() 
		);
		
		$this->load->view('mod_mail_group/view_mail_group_list',$UI);
	}
}


/*
 * @def 	: content
 * ------------------------------------
 * @param 	: array()
 */
 
public function AddMailGroup()
{
	if($this->EUI_Session->_have_get_session('UserId')) 
	{
	  $UI = array( 
		'WorkProject' => $this->M_SetWorkProject->_getWorkProjectId(),
		'WorkCode' => $this->{base_class_model($this)}->_getFollowGroupCode() 
	  );
		
	  $this->load->view('mod_mail_group/view_mail_group_add',$UI );
	}
}

/* SaveAddMailGroup */

public function SaveAddMailGroup()
{
	$conds = array('success' => 0);
	if( is_array($this->URI->_get_all_request() ) )
	{
		if( $this->{base_class_model($this)}->_setSaveAddMailGroup() ) { 
			$conds = array('success'=>1);
		}
	}
	
	echo json_encode($conds);
}

/** UpdateAddMailGroup **/

public function UpdateAddMailGroup()
{
	$conds = array('success' => 0);
	if( is_array($this->URI->_get_all_request() ) )
	{
		if( $this->{base_class_model($this)}->_setUpdateAddMailGroup() ) { 
			$conds = array('success'=>1);
		}
	}
	
	echo json_encode($conds);
}


/*
 * @def 	: content
 * ------------------------------------
 * @param 	: array()
 */
 
public function EditMailGroup()
{
	if($this->EUI_Session->_have_get_session('UserId'))  {
	  $UI = array( 
		'WorkProject' => $this->M_SetWorkProject->_getWorkProjectId(),
		'WorkCode' => $this->{base_class_model($this)}->_getFollowGroupCode(),
		'WorkData' => $this->{base_class_model($this)}->_getFollowData(),	
	  );
		
	  $this->load->view('mod_mail_group/view_mail_group_edit',$UI );
	}
}


/* DeleteMailGroup */

public function DeleteMailGroup()
{
	$conds = array('success' => 0);
	if( is_array($this->URI->_get_all_request() ) )
	{
		if( $this->{base_class_model($this)}->_setDeleteMailGroup() ) { 
			$conds = array('success'=>1);
		}
	}
	
	echo json_encode($conds);
}


	
}

?>