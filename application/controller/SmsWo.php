<?php 
//--------------------------------------------------------------------------
/*
 * @ package 		class conroller ReportCpaSummary  
 * @ auth 			uknown User 
 */

 class SmsWo extends EUI_Controller 
{


	//--------------------------------------------------------------------------
	/*
	 * @ package 		EUI_Report_helper
	 */

	function __construct() 
	{
		parent::__construct();
		// $this->load->model(array('M_RptSms'));
		// $this->load->model(array(base_class_model($this),'M_RptSms'));
		$this->load->helper(array('EUI_Object','EUI_Report'));
	}
	
	public function index()
	{
		$this->load-> view("rpt_sms_wo/report_sms_nav");
    }
    
    public function ShowExcel() {
		//Excel() -> HTML_Excel(urlencode(_get_post('report_title')).'REPORT_SMS_BLAST-');
		//$this->load->view('rpt_sms_wo/rpt_excel_cront');
		$this->exportExcelNew();
	}

	public function ShowReportWo() {
		// Excel() -> HTML_Excel('REPORT_SMS_BLAST');
		$this->load->view('rpt_sms_wo/rpt_excel_crontab');

	}
		public function exportExcelNew()
	{
		$tanggal = $_REQUEST['start_date'];
		$sqltanggal = "
			SELECT 
		r.deb_cardno,r.deb_cardno,r.deb_mobileno1,r.deb_name,

		r.deb_wo_date - INTERVAL 5 DAY AS WO5, 
		r.deb_wo_date - INTERVAL 20 DAY AS WO20,
		r.deb_wo_date - INTERVAL 30 DAY AS WO30,
		r.deb_wo_date - INTERVAL 53 DAY AS WO53,
		r.deb_wo_date - INTERVAL 75 DAY AS WO75,
		r.deb_wo_date - INTERVAL 145 DAY AS WO145,
		r.deb_wo_date - INTERVAL 175 DAY AS WO175,
		datediff(r.deb_wo_date - INTERVAL 5 DAY,  r.deb_wo_date) as selisihWO5,
		datediff(r.deb_wo_date - INTERVAL 20 DAY,  r.deb_wo_date) as selisihWO20,
		datediff(r.deb_wo_date -INTERVAL 30 DAY,  r.deb_wo_date) as selisihWO30,
		datediff(r.deb_wo_date - INTERVAL 53 DAY,  r.deb_wo_date) as selisihWO53,
		datediff(r.deb_wo_date - INTERVAL 75 DAY,  r.deb_wo_date) as selisihWO75,
		datediff(r.deb_wo_date - INTERVAL 30 DAY,  r.deb_wo_date) as selisihWO145,
		datediff(r.deb_wo_date - INTERVAL 30 DAY,  r.deb_wo_date) as selisihWO175
		
		FROM t_gn_debitur r
		WHERE r.deb_wo_date IN ('" . $tanggal . "') 
		GROUP BY WO5,WO20,WO30,WO53,WO75,WO145,WO175";
		$query =  $this->db->query($sqltanggal)->row_array();
		$results = "SELECT ts.deb_cardno,ts.deb_cardno,ts.deb_mobileno1,ts.deb_name,ts.deb_wo_date,ts.deb_perm_msg FROM t_gn_debitur ts 
		WHERE 
		(ts.deb_perm_msg IN ('LP','VLP','VHP','HP','MP') OR ts.deb_wo_date IN ('" . $query["WO5"] . "','" . $query["WO30"]  . "'))
		AND ts.deb_wo_date != '0000-00-00'
		and ts.deb_wo_date IN ('" . $query["WO5"] . "','" . $query["WO20"] . "','" . $query["WO30"] . "','" . $query["WO53"] . "','" . $query["WO75"] . "','" . $query["WO145"] . "','" . $query["WO175"] . "')";
		$result = $this->db->query($results)->result();
		$this->load->library("Excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'Card No');
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'Customer Name');
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'Approval Date of Last approver');
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'No Handphone');
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'Narasi SMS');
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'No Inbound');
		$object->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'WO');
		$excel_row = 3;
		foreach ($result as $row){
			if ($row->deb_wo_date == $query['WO5']) {
				$wo = 'WO5';
			} elseif ($row->deb_wo_date == $query['WO20']) {
				$wo = "WO20";
			} elseif ($row->deb_wo_date == $query['WO30']) {
				$wo = "WO30";
			} elseif ($row->deb_wo_date == $query['WO53']) {
				$wo = "WO53";
			} elseif ($row->deb_wo_date == $query['WO75']) {
				$wo = "WO75";
			} elseif ($row->deb_wo_date == $query['WO150']) {
				$wo = "WO150";
			} else {
				$wo = "WO175";
			}
				if(strlen( $row->deb_cardno)==12){
                $cardno = '0000000'.$row->deb_cardno;
            }else{
                $cardno= '000'.$row->deb_cardno;
            } 
			$excel_row++;
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $cardno);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->deb_name);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, 0);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->deb_mobileno1, PHPExcel_Cell_DataType::TYPE_STRING);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $this->narasi($wo));
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, '02150811241');
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $wo);
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="REPORT SMS BLAST_'.$_REQUEST['start_date'].'.xls"');
		$object_writer->save('php://output');
	}

	public function narasi($wo)
	{
		switch ($wo) {
			case 'WO5':
				$narasi = "Nasabah HSBC yth, Sesuai dengan surat terakhir yang dikirimkan, saat ini tagihan Bapak/Ibu ditangani oleh pihak ketiga yang menjadi rekanan HSBC Indonesia";
				break;
			case 'WO20':
				$narasi = "Nasabah HSBC Yth, Manfaatkan DISKON menarik dgn CICILAN & hapus nama anda dari daftar Hitam BI. Hubungi kami di 021-50811241.";
				break;
			case 'WO30':
				$narasi = "Nasabah HSBC Yth. Pulihkan kualitas kredit & hapus daftar hitam BI. Segera lakukan pembayaran.Hubungi kami di 021-50811241.";
				break;
			case 'WO40':
				$narasi = "Nasabah HSBC Yth. Manfaatkan DISKON menarik dgn CICILAN & hapus nama anda dari daftar Hitam BI. Hubungi kami di 021-50811241.";
				break;
			case 'WO53':
				$narasi = "Nasabah HSBC Yth.Utk menghindari langkah lanjut PENAGIHAN LAPANGAN & memperbaiki kualitas kredit anda. Hubungi kami di 021-50811241.";
				break;
			case 'WO75':
				$narasi = "Nasabah HSBC Yth. Pulihkan Segera lakukan pembayaran & hapus nama anda dari daftar hitam BI.Hubungi kami di 021-50811241.";
				break;
			case 'WO150':
				$narasi = "Nasabah HSBC Yth,manfaatkan DISKON menarik dgn CICILAN & hapus nama anda dari daftar Hitam BI.Hubungi kami di 021-50811241.";
				break;
			case 'WO175':
				$narasi = "Nasabah HSBC Yth.Utk menghindari langkah lanjut PENAGIHAN LAPANGAN & perbaikan kualitas kredit anda, segera lakukan pembayaran info 021-50811241.";
				break;
			default:
				$narasi = '';
				break;
		}
		return $narasi;
	}

	
 }