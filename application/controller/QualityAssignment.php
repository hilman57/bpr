<?php
/*
 * E.U.I 
 * --------------------------------------------------------------------------
 *
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/QualityAssignment/index/?
 * ----------------------------------------------------------------------------
 */
 
class QualityAssignment extends EUI_Controller
{
/*
 * @def : QualityAssignment aksesor 
 * ----------------------------------
 *
 * @param : -
 * @param : - 
 */
private static $view_layout = null;
/*
 * @def : QualityAssignment aksesor 
 * ----------------------------------
 *
 * @param : -
 * @param : - 
 */
public function QualityAssignment()
{
	parent::__construct();
	$this -> load -> model(array(base_class_model($this),'M_SetCampaign'));
	if( is_null( $view_layout ) )
	{
		self::$view_layout = "qty_view_assignment"; 
	}
}
/*
 * @def : getViewLayout
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */

 public function ViewLayout() 
 {
	return self::$view_layout;
 }
/*
 * @def : default page
 * ----------------------------------
 *
 * @param : Unit Test 
 * @param : Unit Test 
 */
 
public function index()
{
	if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) )
	{
		$this->load->view("{$this ->ViewLayout()}/view_assignment_nav");
	}	
 }
 
/*
 * @def : get layput panel left content 
 * ----------------------------------
 *
 * @param : - 
 * @param : -
 */
 
public function FilterAssignment()
{
	if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) )
	{
		$UI = array('CampaignOutbound' => $this -> M_SetCampaign -> _getCampaignGoals(2) );
		$this->load->view("{$this->ViewLayout()}/view_layout_left", $UI);
		
	}	

}

/*
 * @def : get layput panel left content 
 * ----------------------------------
 *
 * @param : - 
 * @param : -
 */
 
public function ContentFilter()
{
	if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) )
	{
		
		$UI = array('QualityGroup' => $this -> {base_class_model($this)} -> _getQualityGroup() );
		$this->load->view("{$this->ViewLayout()}/view_layout_right",$UI);
	}	

}

/*
 * @ def : get layput panel left content 
 * --------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
 
function ShowDataByChecked()
{

  if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) ) 
  {
	   $UI = array
	   (
			'result' => $this -> {base_class_model($this)} -> _getDataByChecked(),
			'view_page_layout' => $this -> {base_class_model($this)} ->_getPage(), 
			'selected_page' => $this -> URI->_get_post('start_page')
		);
			
	   $this->load->view("{$this->ViewLayout()}/view_layout_bychecked", $UI);		
  }
  
}

/*
 * @ def : get layput panel left content 
 * ---------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
 
function ShowDataByAmount()
{
   if( $this ->EUI_Session ->_have_get_session('UserId') 
	AND class_exists(base_class_model($this)) ) 
  {
	  $this->load->view("{$this->ViewLayout()}/view_layout_byamount", 
		array('view_size_data' => $this -> {base_class_model($this)} ->_getDataByAmount() )
	  );		
  }
}
 
/*
 * @ def : get layput panel left content 
 * ---------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
 
 
function AssignByChecked()
{
  $_conds = array('success' => 0 , 'message' => '' );
  
  
	if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) ) 
	  {
	    if( $result  = $this -> {base_class_model($this)} ->_setAssignByChecked($this -> URI->_get_array_post('AssignId'), 
			$this -> URI->_get_array_post('QualityStaffId'))) 
		{
			$_conds = array('success' => 1 , 'message' => $result );
		}
	}
	
	__(json_encode($_conds));
}
 
/*
 * @ def : get layput panel left content 
 * ---------------------------------------
 *
 * @ param : - 
 * @ param : -
 */
 
 
function AssignByAmount()
{
  $_conds = array('success' => 0 , 'message' => '' );
	if( $this ->EUI_Session ->_have_get_session('UserId') 
		AND class_exists(base_class_model($this)) ) 
	  {
	    if( $result  = $this -> {base_class_model($this)} ->_setAssignByAmount($this -> URI->_get_post('AssignSizeData'), 
			$this -> URI->_get_array_post('QualityStaffId'))) 
		{
			$_conds = array('success' => 1 , 'message' => $result );
		}
	}
	
	__(json_encode($_conds));
}
 
 
 
 




}
?>