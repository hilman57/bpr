<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class ModCustomerReview extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function ModCustomerReview() 
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
		
 }

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function index() 
{ 
  if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$_EUI  = array(
	 'page' => $this ->{base_class_model($this)} ->_get_default(), 
	 'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
	 'GenderId' => $this->{base_class_model($this)}->_getGenderId(),
	 'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
	 'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus(),
	 'LastCallStatus' => $this->{base_class_model($this)}->_getLastCallStatus() );
	 
   if(is_array($_EUI)) {
	 $this -> load ->view('mod_customer_review/view_customer_review_nav',$_EUI);
   }	
}
	
}
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function Content() 
{
  if( $this -> EUI_Session->_have_get_session('UserId') ) 
  {
	$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
	$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	
	if( is_array($_EUI) && is_object($_EUI['page']) ) {
		$this -> load -> view('mod_customer_review/view_customer_review_list',$_EUI);
	 }	
  }	
}


// @ pack : Preview 

 public function Preview()
{
 
 $this->load->model(array('M_ModContactDetail'));
 
 $FavouriteId =& _get_post('FavouriteId'); 
 if( $FavouriteId )
 {
	$_result = $this->{base_class_model($this)}->_get_select_favourite_call($FavouriteId);	
	$ModContactDetail = & M_ModContactDetail::Instance();
	
	if( $_result )
	{	
		$result['dropdown'] = $ModContactDetail->_get_select_dropdown();
		$result['contact_data'] =$ModContactDetail->_get_select_debitur($_result['deb_id']);	 
		$result['data_coll'] = $this->{base_class_model($this)}->_get_select_favourite_call($FavouriteId);	
	}
 }
 
 $this->load->view('mod_customer_review/view_customer_preview', $result);
 
}


/*
 * @ pack : Unblock 
 */
 public function UnblockAll()
 {
	 $conds = array('success' => 0 );
	 if( $this->EUI_Session->_have_get_session('UserId') ) 
	{
		if( $OK_ERROR = $this->{base_class_model($this)}->_setUnblockAll()  ){
			$conds = array('success' => 1);
		}
	}
	echo json_encode($conds);
 }
 
 public function Unblock() 
{

 $conds = array('success' => 0 );
 
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$FavouriteId = _get_array_post('FavouriteId'); 	
	if( count($FavouriteId )> 0 )
	{
		if( $OK_ERROR = $this->{base_class_model($this)}->_setUnblock($FavouriteId)  ){
			$conds = array('success' => 1);
		}
	}
	
 }
 
 echo json_encode($conds);
 
} 



// END OF CLASS 

}



?>