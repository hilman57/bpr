<?php


class PvcvcReport extends EUI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(
			array(
				'M_Report',
				'M_MgtRandDeb',
				base_class_model($this)
			)
		);
	}

	public function index()
	{
		$random_modul = $this->{base_class_model($this)}->modul_random();
		$UI['cmb_random_modul'] = $random_modul;

		$this->load->view("rpt_pvcvc/report_pvcvc_nav", $UI);
	}

	public function showReport()
	{
		$UI_Mode = strtolower($this->URI->_get_post('type_show'));
		switch ($UI_Mode) {
			case 'showhtml':
				$this->showHtmlReport();
				break;
			case 'showexcel':
				$this->showExcelReport();
				break;
		}
	}

	private function showHtmlReport()
	{
		$reportType = strtolower($this->URI->_get_post('report_type'));
		switch ($reportType) {
			case 'pvc':
				$this->showReportPVC();
				break;
			case 'vct':
				$this->showReportVCT();
				break;
			case 'vcp':
				$this->showReportVCP();
				break;
			case 'vct_category':
				$this->showReportVCTPerCategory();
				break;
			case 'vcp_category':
				$this->showReportVCPPerCategory();
				break;
		}
	}

	private function showExcelReport()
	{
		$reportType = strtolower($this->URI->_get_post('report_type'));
		switch ($reportType) {
			case 'pvc':
				$this->showReportPVC();
				break;
			case 'vct':
				$this->showReportVCT();
				break;
			case 'vcp':
				$this->showReportVCP();
				break;
			case 'vct_category':
				$this->showReportVCTPerCategory();
				break;
			case 'vcp_category':
				$this->showReportVCPPerCategory();
				break;
		}
		Excel()->HTML_Excel(urlencode(_get_post('report_title')) . '' . time());
	}

	private function showReportPVC()
	{
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataPVC();
		$REPORT['data_tabel2'] = $this->{base_class_model($this)}->getDataPVC2();
		$REPORT['openPVC'] = $this->{base_class_model($this)}->getDataPVCopenPVC();
		// $REPORT['openPVC2'] = $this->{base_class_model($this)}->getDataPVCopenPVC2();
		$REPORT['NewPVC'] = $this->{base_class_model($this)}->getDataPVCopenPVC();
		$REPORT['NewExit'] = $this->{base_class_model($this)}->getDataPVCopenPVC2();
		$REPORT['NewPVC2'] = $this->{base_class_model($this)}->getDataNewPVC();
		$REPORT['NewExit2'] = $this->{base_class_model($this)}->getDataExitPVC();
		$REPORT['title'] = $this->URI->_get_post('report_title');

		$start_date = $this->URI->_get_post('start_date_claim');
		$openPvc = array();
		// foreach new - exit
		$newMore = 0;
		$newIsExit = 0;
		foreach($REPORT['NewPVC'][$start_date] as $key => $item) {
			if(count($REPORT['NewExit'][$start_date]) != 0) {
				foreach($REPORT['NewExit'][$start_date] as $item2) {
					$date1 = date($item['tgl_new_pvc']);
					$date2 = date($item2['tgl_new_pvc']);
					if($item2['deb_id'] == $item['deb_id'] && $date2 > $date1) {
						// array_push($openPvc, $item);
						// break;
						unset($REPORT['NewPVC'][$start_date][$key]);
					}
				}
			} else {
				array_push($openPvc, $item);
			}
		}
		// echo '<pre>';
		// print_r($REPORT['NewPVC']);
		// print_r($REPORT['NewExit']);
		// echo count($REPORT['NewPVC'][$start_date]).'<br/>';
		// echo count($REPORT['NewExit'][$start_date]).'<br/>';
		// echo count($openPvc);
		// echo '</pre>';
		$REPORT['OpenPvc'] = $REPORT['NewPVC'][$start_date];
		$this->load->view("rpt_pvcvc/report_pvc_html", $REPORT);
	}

	private function showReportVCT()
	{
		// VC Temporary
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCT();
		$REPORT['data_tabel2'] = $this->{base_class_model($this)}->getDataVCT2();
		$REPORT['openVCT'] = $this->{base_class_model($this)}->getDataVCTopenVCT();
		$REPORT['NewVCT'] = $this->{base_class_model($this)}->getDataNewVCT();
		$REPORT['NewExit'] = $this->{base_class_model($this)}->getDataExitVCT();

		$REPORT['title'] = $this->URI->_get_post('report_title');

		$this->load->view("rpt_pvcvc/report_vct_html", $REPORT);
	}

	private function showReportVCP()
	{
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCP();
		$REPORT['data_tabel2'] = $this->{base_class_model($this)}->getDataVCP2();
		$REPORT['openVCP'] = $this->{base_class_model($this)}->getDataVCPopenVCP();
		$REPORT['NewVCP'] = $this->{base_class_model($this)}->getDataNewVCP();
		$REPORT['NewExit'] = $this->{base_class_model($this)}->getDataExitVCP();
		$REPORT['title'] = $this->URI->_get_post('report_title');

		$this->load->view("rpt_pvcvc/report_vcp_html", $REPORT);
	}

	private function showReportVCTPerCategory()
	{
		$reportType = strtolower($this->URI->_get_post('report_type'));
		// echo $reportType;
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCTPerCategory();
		$REPORT['vccategory'] = $this->{base_class_model($this)}->getVCReason($this->{base_class_model($this)}->getAccStatusVC($reportType));
		$REPORT['title'] = $this->URI->_get_post('report_title');
		// echo "<pre>";
		// print_r($REPORT);
		// echo "</pre>";
		$this->load->view("rpt_pvcvc/report_vct_category_html", $REPORT);
	}

	private function showReportVCPPerCategory()
	{
		$reportType = strtolower($this->URI->_get_post('report_type'));
		// echo $reportType;
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->getDataVCPPerCategory();
		$REPORT['vccategory'] = $this->{base_class_model($this)}->getVCReason($this->{base_class_model($this)}->getAccStatusVC($reportType));
		$REPORT['title'] = $this->URI->_get_post('report_title');
		// echo "<pre>";
		// print_r($REPORT);
		// echo "</pre>";
		$this->load->view("rpt_pvcvc/report_vcp_category_html", $REPORT);
	}
}
