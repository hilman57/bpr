<?php 
/*
 * @ pack : BatchApplication
 * @ note : this data for crontab action from here .
 * ------------------------------------------------------------------
 * @ example : $> php -q /opt/enigma/webapps/hsbc/index.php BatchHistoryTmp index 2016-01-01 2016-08-01 
 */
 
class BatchHistoryTmp extends EUI_Controller
{
 var $start_date = null;
 var $end_date = null;
	


//--------------------------------------------------------------------------
/*
 * @ package 		ShowDetailReportByTL
 */
 
 function __construct()
{
	global $argv;
	parent::__construct();
	
	$this->start_date = date('Y-m-d');
	$this->end_date = date('Y-m-d');
	
	if( isset( $argv[3] ) ){
		$this->start_date = date("Y-m-d", strtotime($argv[3]));
	}
	
	if( isset( $argv[4] ) ){
		$this->end_date = date("Y-m-d", strtotime($argv[4]));
	}
	
 }	
 
//--------------------------------------------------------------------------
/*
 * @ package 		ShowDetailReportByTL
 */
 
 public function Index()
{
	$sql = sprintf("INSERT INTO t_gn_callhistory_report(
					CallHistoryId,
					CustomerId, 
					CallSessionId,
					AccountNo, 
					CallReasonId, 
					CallAccountStatus, 
					CreatedById, 
					AgentCode, 
					TeamLeaderId, 
					SupervisorId, 
					CallHistoryCreatedTs 
				) SELECT 
					a.CallHistoryId,
					a.CustomerId, 
					a.CallSessionId,
					a.AccountNo, 
					a.CallReasonId, 
					a.CallAccountStatus, 
					a.CreatedById, 
					a.AgentCode, 
					a.TeamLeaderId, 
					a.SupervisorId, 
					a.CallHistoryCreatedTs 
			FROM t_gn_callhistory a
			WHERE  a.CallHistoryCreatedTs >='%s 00:00:00'
			AND a.CallHistoryCreatedTs <='%s 23:59:59'
			AND a.ApprovalStatusId IS NULL
			ON DUPLICATE KEY UPDATE CallHistoryCreatedTs = a.CallHistoryCreatedTs",
			$this->start_date, $this->end_date);
	if( $this->db->query( $sql ) ){
		echo ".";
	}
}

// =============== END CLASS BATCH TMP ====================

}			

?>
