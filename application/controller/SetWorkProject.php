<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SetFTP 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SetWorkProject extends EUI_Controller
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function SetWorkProject()
 {
	parent::__construct();
	$this -> load -> model('M_SetWorkProject');
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session -> _have_get_session('UserId') )
	{
		
		$_EUI['page'] = $this->{base_class_model($this)}->_get_default();
		if( is_array($_EUI)) 
		{
			$this -> load -> view('set_work_project/view_work_project_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
	  $_EUI['page'] = $this->{base_class_model($this)}->_get_resource();    // load content data by pages 
	  $_EUI['num']  = $this->{base_class_model($this)}->_get_page_number(); // load content data by pages 
	  $this -> load -> view('set_work_project/view_work_project_list',$_EUI);		
	}	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function WorkProjectEdit()
 {
 if( $this -> EUI_Session -> _have_get_session('UserId') )
 {
	$UI['WorkProject']	= $this->{base_class_model($this)}->_getWorkProject( $this -> URI-> _get_post('ProjectId'));
	if( $UI )
	{
		$this ->load->view("set_work_project/view_edit_work_project", $UI);
	}	
 }	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 function WorkProjectAdd()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$this ->load->view("set_work_project/view_add_work_project");
	}	
 }
 
 /** http://192.168.10.236/collection/index.php/SetReadFTP/FTPUpdate/ **/

 
 public function WorkProjectUpdate()
 {
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setWorkProjectUpdate( $this -> URI->_get_all_request() ))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
 }
 
/* http://192.168.10.236/collection/index.php/SetReadFTP/FTPSave/ **/

 public function WorkProjectSave()
 {
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setWorkProjectSave( $this -> URI->_get_all_request() ))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
 }
 

/* WorkProjectDelete **/

public function WorkProjectDelete()
{
	$_conds = array('success' => 0 );
	if( $this->EUI_Session->_get_session('UserId'))
	{
		if( $rs = $this -> {base_class_model($this)}->_setWorkProjectDelete( $this -> URI->_get_array_post('ProjectId')))
		{
			$_conds = array('success' => 1 );
		}
	}
	
	__(json_encode($_conds));
} 
 
}
?>