<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class ModContactView extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function ModContactView()
 {
	parent::__construct();
	$this -> load -> model
	( 
		array
		(
			'M_ModContactView', 'M_SetCallResult',
			'M_SetProduct', 'M_SetCampaign','M_ProjectWorkForm',
			'M_SetResultCategory', 'M_Combo', 'M_FieldPolicy'
		)
	);
 }
 
 
/*
 * @ def 		: CekPolicyForm
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CekPolicyForm()
{
	$_conds = array('success' => 0 );
	if( $this -> URI->_get_have_post('CustomerId') )
	{
		if( $rows = $this -> {base_class_model($this)} ->_getCekPolicyForm( $this -> URI->_get_post('CustomerId')) )
		{
			$_conds = array('success' => 1 );	
		}
	}
	
	__(json_encode($_conds));
} 

/*
 * @ def 		: CekPolicyForm
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

public function PolicyStatusData()
{
	$array_result = array('success'=> 0, 'data' => array() );
	$PolicyId = $this->URI->_get_post('PolicyId');
	if( $PolicyId  AND ( $result = $this->{base_class_model($this)}->_getPolicyStatusData($PolicyId) ))
	{
		$array_result = array('success'=> 1, 'data' => $result );	
	}
	
	__(json_encode($array_result));
}


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function getCallResult($CategoryId = null )
 {
	$_conds = array();
	$_arr_callresult = $this->M_SetCallResult->_getCallReasonId($CategoryId);
		
	if( is_null($CategoryId) AND $this -> URI ->_get_have_post('CategoryId')!=TRUE){ 
		$_conds['NEW'] = 'New ';  	
	}	
	
	if(is_array($_arr_callresult))foreach( $_arr_callresult as $k =>$call ) {
		$_conds[$k] = $call['name'];
	}
	
	return $_conds;
 }

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function setCallResult() 
{
	$_result = array( "setCallResult" => $this -> getCallResult( $this -> URI ->_get_post('CategoryId')) );
	if( is_array($_result)) {
		$this -> load ->view('mod_contact_view/view_call_result',$_result);
	}
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getProductId() {
	
 }

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function index()
 { 
	if( $this ->EUI_Session ->_have_get_session('UserId') )
	{
		$_EUI= array(
			'CallResult' 	=> $this->getCallResult(), 
			'CampaignId' 	=> $this->M_SetCampaign->_get_campaign_name(),
			'page' 			=> $this->{base_class_model($this)}->_get_default(),
			'GenderId' 		=> $this->{base_class_model($this)}->_getGenderId(),
			'Agent'			=> $this->{base_class_model($this)}->_getAgent(),
			'cmp_active'	=> $this->{base_class_model($this)}->_get_active_campaign(),    // load content data by pages
			'ProductId' 	=> $this->{base_class_model($this)}->_getProductCategory()			
		);
		$handling = (in_array(_get_session('HandlingType'), array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND) )? 'caller' : NULL );
		if( is_array($_EUI) && $handling != 'caller')
		{
			$this -> load ->view('mod_contact_view/view_customer_nav',$_EUI);
		}else{
			// $_EUIS  = array
			// (
				// 'page' 			=> $this->M_SrcCustomerClosing->_get_default(), 
				// 'CampaignId' 	=> $this->M_SetCampaign->_get_campaign_name(),
				// 'GenderId' 		=> $this->M_SrcCustomerList->_getGenderId(),
				// 'Agent'			=> $this ->M_SrcCustomerList->_getAgent(),
				// 'cmp_active'	=> $this ->M_SrcCustomerList ->_get_active_campaign(),
				// 'ProductId' 	=> $this ->M_SrcCustomerClosing ->_get_product_category(), 
				// 'CallResult' 	=> self::_getCallResult(), 
				// 'ResultQuality' => self::_getResultQuality(),
				// 'QaStatus'		=> $this ->{base_class_model($this)}->_getQaStatus()
			// );
			// if( is_array($_EUIS))
			// {
				$this -> load ->view('src_closing_list/view_closing_nav',$_EUI);
			// }	
		}
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->{base_class_model($this)} ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->{base_class_model($this)} ->_get_page_number(); // load content data by pages 
		if( is_array($_EUI) && is_object($_EUI['page']) )
		{
			$this -> load -> view('mod_contact_view/view_customer_list',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: NextActivity
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function NextActivity() 
{
	$next_activity_list = array('list'=> $this->{base_class_model($this)}->_NextActivity());
	__(json_encode($next_activity_list));
} 

 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCombo()
 {
	$_serialize = array();
	$_combo = $this ->M_Combo->_getSerialize();
	foreach( $_combo as $keys => $method )
	{
		if((STRTOLOWER($keys)!='serialize') AND (STRTOLOWER($keys)!='instance') 
			AND (STRTOLOWER($keys)!='nstruct') AND (STRTOLOWER($keys)!='t'))
		{
			$_serialize[$keys] = $this ->M_Combo->$method(); 	
		}
	}
	
	return $_serialize;
 }
 
 /** 
  * @ param : ContactDetail 
  * --------------------------------------------------------------------
  * @ akses : public on data save 
  */
  
 public function ContactDetail()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$CustomerId = $this -> URI -> _get_post('CustomerId');
		if(!is_null($CustomerId))
		{
			if( $result_data_customers = $this->{base_class_model($this)}->_getDetailCustomer($CustomerId)) 
			{
				$param = array( 'CampaignId' => $result_data_customers['CampaignId'] );
				$UI = array(
					'Customers' 		=> $result_data_customers,
					'Phones'			=> $this->{base_class_model($this)}->_getPhoneCustomer($CustomerId),
					'AddPhone' 			=> $this->{base_class_model($this)}->_getApprovalPhoneItems($CustomerId),
					'PolicyNumber'		=> $this->{base_class_model($this)}->_getPolicyCustomer($CustomerId),
					'RowPolicyData'     => $this->{base_class_model($this)}->_getRowPolicyByCustomer($CustomerId),
					'Disabled'			=> $this->{base_class_model($this)}->_getOnWorkProject($CustomerId),
					'CallCategoryId' 	=> $this->M_SetResultCategory ->_getOutboundCategoryByProject($param),
					'CallResultId' 		=> $this->getCallResult($result_data_customers['CallReasonCategoryId']),
					'Combo' 			=> $this->_getCombo()
				);
				$this -> load -> view('mod_contact_view/view_contact_main_detail',$UI);
			}
		}
	}	
  }	
  
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function ReloadPolicyData()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$CustomerId = $this -> URI -> _get_post('CustomerId');
		if( !is_null($CustomerId))
		{
			if( $result_data_customers = $this->{base_class_model($this)}->_getDetailCustomer($CustomerId))
			{
				$param = array( 'CampaignId' => $result_data_customers['CampaignId'] );
				$UI = array(
					'RowPolicyData'     => $this->{base_class_model($this)}->_getRowPolicyByCustomer($CustomerId),
					'CallCategoryId' 	=> $this->M_SetResultCategory ->_getOutboundCategoryByProject(),
					'CallResultId' 		=> $this->getCallResult($result_data_customers['CallReasonCategoryId']),
					'Combo' 			=> $this->_getCombo()
				);
				
				$this -> load -> view('mod_contact_view/view_contatc_updatepolicy',$UI);
			}
		}
	}	
 }
 
/*
 * @ def 		: reload question by Policy Data 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function ReloadQuestion()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$CustomerId = $this -> URI -> _get_post('CustomerId');
		$PolicyId = $this -> URI -> _get_post('PolicyId');
		if(!is_null($CustomerId))
		{
			if( $result_data_customers = $this->{base_class_model($this)}->_getDetailCustomer($CustomerId)) 
			{
				$param = array( 'CampaignId' => $result_data_customers['CampaignId'] );
				$UI = array(
					'Customers' => $result_data_customers,
					'PolicyId' => $PolicyId,
					'PolicyNumber'=> $this->{base_class_model($this)}->_getPolicyCustomer($CustomerId),
					'Combo' => $this->_getCombo()
				);
				
				$this -> load -> view('mod_contact_view/view_contact_reload_question',$UI);
			}
		}
	}	
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function PolicyStatus()
 {
	$_conds = array('PolicyReady' => 0 );
	if( $this -> URI ->_get_post('CustomerId'))
	{
		$this->db->select("COUNT(a.PolicyAutoGenId) AS jumlah", FALSE);
		$this->db->from("t_gn_policyautogen a ");
		$this->db->where("a.CustomerId", $this->URI->_get_post('CustomerId'));
		
		if( $rows = $this->db->get()->result_first_assoc() )
		{
			$_conds = array('PolicyReady' => $rows['jumlah'] );	
		}
	}
	
	__(json_encode($_conds));	
 }
 
/*
 * @ def 		: FollowUpCollection
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function FollowUpCollection()
 {
	$_conds = array('PolicyReady' => 0 );
	if( $this -> URI ->_get_post('CustomerId'))
	{
		$this->db->select("COUNT(a.FuId) AS jumlah", FALSE);
		$this->db->from("t_gn_followup a ");
		$this->db->where("a.FuCustId", $this->URI->_get_post('CustomerId'));
		
		if( $rows = $this->db->get()->result_first_assoc() )
		{
			$_conds = array('PolicyReady' => $rows['jumlah'] );	
		}
	}
	
	__(json_encode($_conds));	
 }


/*
 * @ def 		: OtherSaveActivity
 * -----------------------------------------
 *
 * @ params  	: this save for SPV privilege with project Wcall attribute
 * @ return 	: void(0)
 */
public function SaveActivity()
{
 
 $_conds = array('success' => 0);
 if( $this -> EUI_Session -> _get_session('UserId'))
 {
	$array = array (
		'CustomerId'		=> ( $this->URI->_get_have_post('CustomerId') ? $this -> URI->_get_post('CustomerId') : null),
		'PolicyId'			=> ( $this->URI->_get_have_post('PolicyId') ? $this -> URI->_get_post('PolicyId') : null),
		'PolicyNumber'		=> ( $this->URI->_get_have_post('PolicyNumber') ? $this -> URI->_get_post('PolicyNumber') : null),
		'date_call_later' 	=> ( $this->URI->_get_have_post('date_call_later') ? $this -> URI->_get_post('date_call_later') : null),
		'hour_call_later' 	=> ( $this->URI->_get_have_post('hour_call_later') ? $this -> URI->_get_post('hour_call_later') : null),
		'minute_call_later' => ( $this->URI->_get_have_post('minute_call_later') ? $this -> URI->_get_post('minute_call_later') : null),
		'PhoneNumber' 		=> ( $this->URI->_get_have_post('PhoneNumber') ? $this -> URI->_get_post('PhoneNumber') : null),
		'AddPhoneNumber' 	=> ( $this->URI->_get_have_post('AddPhoneNumber') ? $this -> URI->_get_post('AddPhoneNumber') : null),
		'CallStatus' 		=> ( $this->URI->_get_have_post('CallStatus') ? $this -> URI->_get_post('CallStatus') : null),
		'CallResult' 		=> ( $this->URI->_get_have_post('CallResult') ? $this -> URI->_get_post('CallResult') : null),
		'call_remarks' 		=> ( $this->URI->_get_have_post('call_remarks') ? $this -> URI->_get_post('call_remarks') : null),
		'CallSessionId'		=> ( $this->URI->_get_have_post('CallSessionId') ? $this -> URI->_get_post('CallSessionId') : null),
		'CallComplete'		=> ( $this->URI->_get_have_post('CallComplete') ? $this -> URI->_get_post('CallComplete') : null)
	);
		
		if( is_array($array))
		{
			if($this->{base_class_model($this)}->_setSaveActivity($array) )
			{
				$_conds = array('success'=>1);
			}
		}
	}
	
	echo json_encode($_conds);
 }
 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function PolicyDetailById()
{
	$PolicyField = M_FieldPolicy::get_instance();
	$PolicyData = array();	
	if($this->EUI_Session->_have_get_session('UserId')) 
	{  
		$PolicyId =(INT)$this->URI->_get_post('PolicyId');	
		if($PolicyId) 
		{
			if( $PolicyData  = $this->{base_class_model($this)}->_getPolicyDataById($PolicyId) ) 
			{
				$Project = $this->M_SetCampaign->getAttribute($PolicyData['CampaignId']);
				if($Project)
				{
					$this->load->view('mod_contact_view/view_contact_policydata', 
						array('PolicyLabel' => $PolicyField->_getFieldCompiler($Project['ProjectId'], $PolicyData)));			
				}
			}
		}
	}
}

}
?>