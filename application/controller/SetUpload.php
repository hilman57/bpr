<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SetUpload extends EUI_Controller
{

 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function SetUpload()
 {
	parent::__construct();
	$this -> load -> model('M_SetUpload');
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session -> _have_get_session('UserId') )
	{	
		// $this -> {base_class_model($this)} -> _get_list_tables();
		// $_EUI =array
		// (
			// 'page' 	=> $this -> {base_class_model($this)} -> _get_default(),
			// 'plist' => $this -> {base_class_model($this)} -> _get_list_tables(),
			// 'ModeType' 	=> $this ->{base_class_model($this)}-> _getTypeUpload(),
			// 'FileType' 	=> $this ->{base_class_model($this)}-> _getTypeFile()
		// );
		$_EUI =array
		(
			'javascript' => $this->ManageJS()
		);
		
		if( is_array($_EUI) ) 
		{
			$this -> load -> view('set_temp_upload/view_template_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this -> {base_class_model($this)} -> _get_resource();    // load content data by pages 
		$_EUI['num']  = $this -> {base_class_model($this)} -> _get_page_number(); // load content data by pages 
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('set_temp_upload/view_template_list',$_EUI);
		}	
	}	
 }
 
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function setTemplate()
 {
	if( $this -> URI -> _get_have_post('tables') )
	{
		$_fields = array ( 
			'fields' => $this ->{base_class_model($this)}-> _get_field_column($this -> URI -> _get_post('tables'))
		);
		
		if( strtolower($this -> URI->_get_post('method'))=='insert' )
		{
			$this -> load -> view('set_temp_upload/view_layout_upload',$_fields);
		}
		
		if( strtolower($this -> URI->_get_post('method'))=='update' )
		{
			$this -> load -> view('set_temp_upload/view_layout_kupload',$_fields);
		}
	}
		
 }


/*
 * @ def 		: create template views 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Create()
{
	if( $this ->EUI_Session->_have_get_session('UserId') )
	{
		$this -> {base_class_model($this)} -> _get_list_tables();
		$ui =array
		(
			'page' => $this ->{base_class_model($this)}-> _get_default(),
			'plist' => $this ->{base_class_model($this)}-> _get_list_tables(),
			'ModeType' => $this ->{base_class_model($this)}-> _getTypeUpload(),
			'FileType' => $this ->{base_class_model($this)}-> _getTypeFile(),
			'LimitDays' => $this ->{base_class_model($this)}-> _LimitDays(),
			'BlackList' => $this ->{base_class_model($this)}-> _BlackList(),
			'UplodModul' => $this ->{base_class_model($this)}-> _UploadModul()
		);	
		
		
		$this -> load -> view('set_temp_upload/view_add_template',$ui);
	} 
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function saveTemplate()
 {
	$_maps_data = array(); $_conds = array('success'=> 0);
	
	switch( $this->URI->_get_post('mode_input') ) 
	{
		case 'insert':  // insert template 
		
		  if( $this -> URI -> _get_have_post('list_check') ) 
		  {
				foreach( $this -> URI -> _get_array_post('list_check') as $k => $n ) {
					$_maps_data[$n] = $this -> URI->_get_post($n);			
				}
			
				$mode_input 	= ( $this -> URI -> _get_have_post('mode_input') ? $this -> URI -> _get_post('mode_input') : null );
				$table_name		= ( $this -> URI -> _get_have_post('table_name') ? $this -> URI -> _get_post('table_name') : null );
				$templ_name		= ( $this -> URI -> _get_have_post('templ_name') ? $this -> URI -> _get_post('templ_name') : null ); 
				$file_type  	= ( $this -> URI -> _get_have_post('file_type') ? $this -> URI -> _get_post('file_type') : null );
				$order_by   	= ( $this -> URI -> _get_have_post('order_by') ? $this -> URI -> _get_array_post('order_by') : null );
				$delimiter  	= ( $this -> URI -> _get_have_post('delimiter') ? $this -> URI -> _get_post('delimiter') : null );
				$expired_days  	= ( $this -> URI -> _get_have_post('expired_days') ? $this -> URI -> _get_post('expired_days') : null );
				$black_list  	= ( $this -> URI -> _get_have_post('black_list') ? $this -> URI -> _get_post('black_list') : null );
				$bucket_data    = ( $this -> URI -> _get_have_post('bucket_data') ? $this -> URI -> _get_post('bucket_data') : null );
				$upload_modul   = ( $this -> URI -> _get_have_post('upload_modul') ? $this -> URI -> _get_post('upload_modul') : null );
				
				// next process  ----------------
				
				if( $this->{base_class_model($this)}->_set_saveInsert( 
					array ( 
						'mode' => $mode_input, 'table' => $table_name,
						'tplname' => $templ_name, 'filetype' => $file_type,
						'content' => $_maps_data, 'order_by' => $order_by, 
						'delimiter' => $delimiter, 'black_list' => $black_list, 
						'expired_days' => $expired_days, 'bucket_data' => $bucket_data,
						'upload_modul' => $upload_modul
				)))
				{
					$_conds = array('success'=> 1);
				}
			}
		break;

		case 'update': // update template 
		
		  if( $this -> URI -> _get_have_post('list_check') ) 
		  {
				foreach( $this->URI->_get_array_post('list_check') as $k => $n ){
					$_maps_data[$n] = $this->URI->_get_post($n);			
				}
				
				foreach( $this->URI->_get_array_post('list_keys') as $ik => $in ){
					$_maps_keys[$in] = $this->URI->_get_post($in);			
				}
			
				$mode_input 	= ( $this -> URI ->_get_have_post('mode_input') ? $this -> URI -> _get_post('mode_input') : null );
				$table_name		= ( $this -> URI ->_get_have_post('table_name') ? $this -> URI -> _get_post('table_name') : null );
				$templ_name		= ( $this -> URI ->_get_have_post('templ_name') ? $this -> URI -> _get_post('templ_name') : null ); 
				$file_type  	= ( $this -> URI ->_get_have_post('file_type') ? $this -> URI -> _get_post('file_type') : null );
				$order_by   	= ( $this -> URI ->_get_have_post('order_by') ? $this -> URI -> _get_array_post('order_by') : null );
				$delimiter  	= ( $this -> URI -> _get_have_post('delimiter') ? $this -> URI -> _get_post('delimiter') : null );
				$expired_days  	= ( $this -> URI ->_get_have_post('expired_days') ? $this -> URI -> _get_post('expired_days') : null );
				$black_list  	= ( $this -> URI ->_get_have_post('black_list') ? $this -> URI -> _get_post('black_list') : null );
				$bucket_data    = ( $this -> URI -> _get_have_post('bucket_data') ? $this -> URI -> _get_post('bucket_data') : null );
				$upload_modul   = ( $this -> URI -> _get_have_post('upload_modul') ? $this -> URI -> _get_post('upload_modul') : null );
				
				if( $this ->{base_class_model($this)} -> _set_saveUpdate( 
					array 
					( 
						'mode' => $mode_input, 'table' => $table_name,
						'tplname' => $templ_name, 'filetype' => $file_type,
						'content' => $_maps_data, 'keys' => $_maps_keys,
						'black_list' => $black_list, 'expired_days' => $expired_days,
						'bucket_data' => $bucket_data, 'upload_modul' => $upload_modul
				)))
				{
					$_conds = array('success'=> 1);
				}
			}
		break;
	}		
	
	echo json_encode($_conds);
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function DownloadTemplate()
 {
	if( $this -> URI -> _get_have_post('TemplateId'))
	{
		$tmp_result = $this ->{base_class_model($this)}-> _get_name_template($this -> URI -> _get_post('TemplateId'));
		
		$data = array
		(
			'template' => $this ->{base_class_model($this)}-> _get_download_template($this -> URI -> _get_post('TemplateId')),
			'filename' => $tmp_result['TemplateName'],
			'filetype' => $tmp_result['TemplateFileType'],
			'sparator' => $tmp_result['TemplateSparator'],
		);
		
		if($tmp_result['TemplateFileType']=='txt')
			$this -> load -> view('set_temp_upload/view_download_data_txt',$data);
		else{
			$this -> load -> view('set_temp_upload/view_download_data_xls',$data);
		}	
	}
	else{
		show_error("No have template ID ");
	}
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function Enable()
 {
	$_conds = array('success' => 0 ); 
	if( $this -> URI -> _get_have_post('TemplateId'))
	{
		if( $this ->{base_class_model($this)}-> _set_active_template( 1, $this -> URI -> _get_post('TemplateId') ))
		{
			$_conds = array('success' => 1);	
		} 
	}
	
	echo json_encode($_conds);
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
  
 function Disable()
 {
	$_conds = array('success' => 0 ); 
	if( $this -> URI -> _get_have_post('TemplateId'))
	{
		if( $this ->{base_class_model($this)}-> _set_active_template(0, $this -> URI -> _get_post('TemplateId') ))
		{
			$_conds = array('success' => 1);	
		} 
	}
	
	echo json_encode($_conds);
 }
 
 
 // delete template 
 function Delete()
 {
	$_conds = array('success' => 0 ); 
	
	$params = $this -> URI -> _get_array_post('TemplateId');
	
	if( count($params)> 0 )
	{
		if( $this ->{base_class_model($this)}-> _set_delete_template($params) )
		{
			$_conds = array('success' => 1);	
		} 
	}
	
	echo json_encode($_conds);
 
 }
 
  private function ManageJS()
 {
	$Pre_js = array ( 
						'page' =>$this -> {base_class_model($this)} -> _get_default() 
					);
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_ROOT) ) )
	{
		$js = $this -> load -> view('set_temp_upload/js_template_root',$Pre_js,TRUE);
	}
	else
	{
		$js = $this -> load -> view('set_temp_upload/js_template_public',$Pre_js,TRUE);
	}
	
	return $js;
 }
 
 
 
  
}
?>
