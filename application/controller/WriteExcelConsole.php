<?php

class WriteExcelConsole extends EUI_Controller
{	

private $mkdir_path = null;

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 	
function __construct()
{
	parent::__construct();
	$this->load->model(array(base_class_model($this)));
	$this->mkdir_path = date('Y')."/".date('m') ."/". date('d');
}
	
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
public function getGroup()
{
	$res = array();

	$this->db->select("*");
	$this->db->from("t_lk_followupgroup_header a");
	$this->db->order_by("a.HeaderGroup");
	$this->db->order_by("a.HeaderOrder");
	$i = 0;
	// echo $this->db->_get_var_dump();
	foreach ($this->db->get()->result_assoc() as $key => $value) 
	{
		$res[$i] = $value;
		$i++;
	}
	
	return $res;
}

public function index()
{
// echo "string";
// die();
 $all_group = $this->getGroup();
 $this->load->helper('EUI_ExcelWorksheet');
 $group = $this->{base_class_model($this)}->_getGroupByActive();
	// echo '<pre>';
	// print_r($group);
	// echo '</pre>';
 foreach( $group as $_key_group => $groups )
 {
	$conds = $this->{base_class_model($this)}->_getCountByGroup( $_key_group, date('Y-m-d'));
	// echo $_key_group;
	if($conds)
	{
		$BASE_DIR_EXCEL = date('Ymd');
		if(!is_dir( FCPATH . APPPATH .'temp/'. $BASE_DIR_EXCEL)) {
			mkdir( FCPATH . APPPATH .'temp/'. $BASE_DIR_EXCEL, 0777, true);
		}
		if($_key_group=='999'){
			$BASE_PATH_EXCEL = FCPATH . APPPATH .'temp/'. $BASE_DIR_EXCEL;
			$BASE_FILE_NAME1  = 'process_followup_'. $_key_group .'_Agency_'. date('Ymd').'.xls';
			$BASE_FILE_NAME2  = 'process_followup_'. $_key_group .'_Bancass_'. date('Ymd').'.xls';
			$BASE_FILE_EXCEL1 = $BASE_PATH_EXCEL . '/' .  $BASE_FILE_NAME1 ;
			$BASE_FILE_EXCEL2 = $BASE_PATH_EXCEL . '/' .  $BASE_FILE_NAME2 ;

			if(is_dir($BASE_PATH_EXCEL)!=FALSE) {
				system("cd ". $BASE_PATH_EXCEL ." && rm -f $BASE_FILE_NAME2 ");
				system("cd ". $BASE_PATH_EXCEL ." && rm -f $BASE_FILE_NAME1 ");
			}
			
			$workbook 	=& new writeexcel_workbook($BASE_FILE_EXCEL1);
			$worksheet =& $workbook -> addworksheet();
			$header 	=& $workbook -> addformat();
			$fontsize  =& $workbook -> addformat();
				$workbook1 	=& new writeexcel_workbook($BASE_FILE_EXCEL2);
				$worksheet1 =& $workbook1 -> addworksheet();
				$header1 	=& $workbook1 -> addformat();
				$fontsize1  =& $workbook1 -> addformat();

			$header->set_bold();
			$header->set_size(11);
			$header->set_color('white');
			$header->set_align('center');
			$header->set_align('vcenter');
			$header->set_pattern();
			$header->set_fg_color('black');
			$fontsize->set_bold();
			$fontsize->set_size(14);
				$header1->set_bold();
				$header1->set_size(11);
				$header1->set_color('white');
				$header1->set_align('center');
				$header1->set_align('vcenter');
				$header1->set_pattern();
				$header1->set_fg_color('black');
				$fontsize1->set_bold();
				$fontsize1->set_size(14);

			$fields = $this ->db->list_fields('t_gn_followup');
			$x_i = 0;

			foreach ($all_group as $value) {
				if ($value['HeaderGroup']==$_key_group) {
					$worksheet->write(0, $x_i, $value['HeaderAlias'], $header);
					$worksheet1->write(0, $x_i, $value['HeaderAlias'], $header1);
					$x_i++;
				}
			}
			
			$this->db->select('a.*');
			$this->db->from('v_fu_daily a');
			$this->db->where('a.FuGroupCode',$_key_group);
			$this->db->where('date(a.FuCreatedDate)',date('Y-m-d'));
			$this->db->where('a.FuCreateExcel','N');
			// $this->db->_get_var_dump();
			
			$num = 1;$nums = 1;
			foreach($this->db->get()->result_assoc() as $rows ){
				$cols = 0;
				foreach( $all_group as $value ) {	// loop untuk semua kolom header dari report followup
					if ($value['HeaderGroup']==$_key_group) {
						if($rows['CampaignTypeId']==1){
							$worksheet->write_string($num, $cols, $rows[$value['HeaderColumn']]);	
						}else if($rows['CampaignTypeId']==2){
							$worksheet1->write_string($nums, $cols, $rows[$value['HeaderColumn']]);	
						}
					$cols++;
					}
				}
				if($rows['CampaignTypeId']==1){
					$num++;
				}else{
					$nums++;
				}
			}
			$workbook->close();
			$workbook1->close();
		}else if($_key_group=='700'){
			$BASE_PATH_EXCEL = FCPATH . APPPATH .'temp/'. $BASE_DIR_EXCEL;
			$BASE_FILE_NAME1  = 'process_followup_'. $_key_group .'_Agency_'. date('Ymd').'.xls';
			$BASE_FILE_NAME2  = 'process_followup_'. $_key_group .'_Bancass_'. date('Ymd').'.xls';
			$BASE_FILE_EXCEL1 = $BASE_PATH_EXCEL . '/' .  $BASE_FILE_NAME1 ;
			$BASE_FILE_EXCEL2 = $BASE_PATH_EXCEL . '/' .  $BASE_FILE_NAME2 ;

			if(is_dir($BASE_PATH_EXCEL)!=FALSE) {
				system("cd ". $BASE_PATH_EXCEL ." && rm -f $BASE_FILE_NAME2 ");
				system("cd ". $BASE_PATH_EXCEL ." && rm -f $BASE_FILE_NAME1 ");
			}
			
			$workbook 	=& new writeexcel_workbook($BASE_FILE_EXCEL1);
			$worksheet =& $workbook -> addworksheet();
			$header 	=& $workbook -> addformat();
			$fontsize  =& $workbook -> addformat();
				$workbook1 	=& new writeexcel_workbook($BASE_FILE_EXCEL2);
				$worksheet1 =& $workbook1 -> addworksheet();
				$header1 	=& $workbook1 -> addformat();
				$fontsize1  =& $workbook1 -> addformat();

			$header->set_bold();
			$header->set_size(11);
			$header->set_color('white');
			$header->set_align('center');
			$header->set_align('vcenter');
			$header->set_pattern();
			$header->set_fg_color('black');
			$fontsize->set_bold();
			$fontsize->set_size(14);
				$header1->set_bold();
				$header1->set_size(11);
				$header1->set_color('white');
				$header1->set_align('center');
				$header1->set_align('vcenter');
				$header1->set_pattern();
				$header1->set_fg_color('black');
				$fontsize1->set_bold();
				$fontsize1->set_size(14);

			$fields = $this ->db->list_fields('t_gn_followup');
			$x_i = 0;

			foreach ($all_group as $value) {
				if ($value['HeaderGroup']==$_key_group) {
					$worksheet->write(0, $x_i, $value['HeaderAlias'], $header);
					$worksheet1->write(0, $x_i, $value['HeaderAlias'], $header1);
					$x_i++;
				}
			}
			
			$this->db->select('a.*');
			$this->db->from('v_fu_daily a');
			$this->db->where('a.FuGroupCode',$_key_group);
			$this->db->where('date(a.FuCreatedDate)',date('Y-m-d'));
			$this->db->where('a.FuCreateExcel','N');
			// $this->db->_get_var_dump();
			
			$num = 1;$nums = 1;
			foreach($this->db->get()->result_assoc() as $rows ){
				$cols = 0;
				foreach( $all_group as $value ) {	// loop untuk semua kolom header dari report followup
					if ($value['HeaderGroup']==$_key_group) {
						if($rows['CampaignTypeId']==1){
							$worksheet->write_string($num, $cols, $rows[$value['HeaderColumn']]);	
						}else if($rows['CampaignTypeId']==2){
							$worksheet1->write_string($nums, $cols, $rows[$value['HeaderColumn']]);	
						}
					$cols++;
					}
				}
				if($rows['CampaignTypeId']==1){
					$num++;
				}else{
					$nums++;
				}
			}
			$workbook->close();
			$workbook1->close();
		}else{
			 $BASE_PATH_EXCEL = FCPATH . APPPATH .'temp/'. $BASE_DIR_EXCEL;
			 $BASE_FILE_NAME  = 'process_followup_'. $_key_group .'_'. date('Ymd').'.xls';
			 $BASE_FILE_EXCEL = $BASE_PATH_EXCEL . '/' .  $BASE_FILE_NAME ;
			 
			 if(is_dir($BASE_PATH_EXCEL)!=FALSE) {
				system("cd ". $BASE_PATH_EXCEL ." && rm -f $BASE_FILE_NAME ");
			 }
			 
			 $workbook 	=& new writeexcel_workbook($BASE_FILE_EXCEL);
			 $worksheet =& $workbook -> addworksheet();
			 $header 	=& $workbook -> addformat();
			 $fontsize  =& $workbook -> addformat();
			 
			 $header->set_bold();
			 $header->set_size(11);
			 $header->set_color('white');
			 $header->set_align('center');
			 $header->set_align('vcenter');
			 $header->set_pattern();
			 $header->set_fg_color('black');
			 $fontsize->set_bold();
			 $fontsize->set_size(14);

			 $fields = $this ->db->list_fields('t_gn_followup');
			 $x_i = 0;

			 // menulis heder report ke xl sesuai group report, fu-pos, fu-agency dsb 
			 foreach ($all_group as $value) {
				if ($value['HeaderGroup']==$_key_group) {
					$worksheet->write(0, $x_i, $value['HeaderAlias'], $header);
					$x_i++;
				}
			 }

			  $this->db->select('a.*');
			  $this->db->from('v_fu_daily a');
			  $this->db->where('a.FuGroupCode',$_key_group);
			  $this->db->where('date(a.FuCreatedDate)',date('Y-m-d'));
			  $this->db->where('a.FuCreateExcel','N');
			  
		// echo "q => ";
		// echo $this->db->_get_var_dump();
		// echo "\n\n";
		// exit;
			
			$num = 1;
			foreach($this->db->get()->result_assoc() as $rows ) 	// loop untuk semua baris/row hasil query 
			{
				$cols = 0;
				foreach( $all_group as $value ) {	// loop untuk semua kolom header dari report followup
					if ($value['HeaderGroup']==$_key_group) {
						$worksheet->write_string($num, $cols, $rows[$value['HeaderColumn']]);	
						$cols++;
					}
				}
				$num++;
			}
			 
				$workbook->close();

				/** 
				* @ insert this to table data transaction if process done 
				* ----------------------------------------------------------------------------------------
				**/

				$tomorrow_date = date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d'))));
				if( !empty($tomorrow_date) AND file_exists($BASE_FILE_EXCEL))
				{
					$FileSize = _getFormatSize(filesize($BASE_FILE_EXCEL));

					$this->db->set('file_excel_name', $BASE_FILE_NAME);
					$this->db->set('file_excel_size',$FileSize);
					$this->db->set('file_excel_path', $BASE_PATH_EXCEL);
					$this->db->set('file_excel_create', date('Y-m-d H:i:s'));
					$this->db->set('file_excel_send', $tomorrow_date);
					$this->db->set('file_excel_group',$_key_group); 
					$this->db->insert('t_gn_followup_excel');

					if( $this->db->affected_rows() > 0 ){
						$IDrows = $this->db->insert_id();
						if( $IDrows ){
							$this->SendDataViaEmail($IDrows);
							$_conds++;
						}
					}else{
						$this->db->set('file_excel_size', $FileSize);
						$this->db->where('file_excel_name',$BASE_FILE_NAME);
						$this->db->update('t_gn_followup_excel');	
					}
				}
			}
		}
	}
}

public function setHeader(){
	
}

/*
 * @ def 		: SAVE file excel to mail server to sending ... 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function SendDataViaEmail( $ID=0 )
{
	if( $ID==0) exit(0);
	
/* get detail **/
	
	$_arrs_followup = $this->{base_class_model($this)}->_getDetailFollowupExcel( $ID );
	
	foreach( $_arrs_followup as $rows ) 
	{
		$param = array('MailContent' => 'Send Daily Report Followup', 'MailSubject' => $rows['FuGroupDesc'], 'AssignDataId' => $rows['file_excel_id']);
		if( $OutboxId= $this->{base_class_model($this)}->_setSavaMailOutgoing( $param ) )
		{
			// addess to only 
			
			$_AddressTo = $this->{base_class_model($this)}->_AddressTo($rows['file_excel_group']);
			if( is_array($_AddressTo)!=FALSE){
				$this->{base_class_model($this)}->_setMailDestination($_AddressTo, $OutboxId );
			}
			
			// $_AddressCC
			
			$_AddressCC = $this->{base_class_model($this)}->_AddressCC($rows['file_excel_group']);
			if( is_array($_AddressCC)!=FALSE){
				$this->{base_class_model($this)}->_setMailCC($_AddressCC, $OutboxId );
			}
			
			// then set to "Antrian "
			
			$this->{base_class_model($this)}->_setSaveQueue($OutboxId);
			
			// then save file write attachment file 
			
			$varlog =  $this->FileWriteAttachment($rows, $OutboxId );
			if(is_array($varlog))
			{
				$this->{base_class_model($this)}->_setSaveAttachment($varlog, $OutboxId); 
			}
		}
	}
  }
  
/*
 * @ def 		: FileWriteAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function FileWriteAttachment( $rows = null , $OutboxId  = 0) 
{
 $var_log_path = array();
 if(($OutboxId > 0) )
 {
	$Config = $this->M_Configuration->_getMail(); // require mail 
	if( $Config )
	{ 
		$path  = $Config['outbox.path'];
		$mkdir = $path . '/'. $this->mkdir_path .'/'. $OutboxId;
		if(!is_dir($mkdir)){
			mkdir($mkdir, 0777,true); 	
		}
		
		$sn = 0;
		if(copy( $rows['file_excel_path'] .'/'. $rows['file_excel_name'],  $mkdir . '/' . $rows['file_excel_name']))
		{
			$var_log_path[$sn]['path'] = $mkdir .'/'. $rows['file_excel_name'];
			$var_log_path[$sn]['size'] = $rows['file_excel_size'];
			$var_log_path[$sn]['mime'] = 'application/vnd.ms-excel';
			$sn++;
		 }
	 }	 
  }	
  
  return $var_log_path;
  
} 

 
 
 
}
?>