<?php 
class SysAllDebitur extends EUI_Controller {


// ------------------------------------------------------------------
/* 
 * @ package 		construct 
 */
 
 function __construct()
{
	parent::__construct();
	$this->load->model(array('M_SysAllDebitur'));
	$this->load->helper(array('EUI_Object'));	
}

// ------------------------------------------------------------------
/* 
 * @ package 		index 
 */
 
 function index() 
 {
	 if( strtoupper(_get_session('Username') ) == $this->URI->segment(3) )
	{	
		$this->load->view("mod_view_alldebitur/view_alldebitur_nav", array());	
    }
}

// ------------------------------------------------------------------
/* 
 * @ package 		index 
 */

 function SysPageAllDebitur()
 { 
		
 if(_have_get_session('UserId') )
 {
   $outModel=& get_class_instance('M_SysAllDebitur');
   
   $this->start_page = 0;
   $this->per_page   = 10;//_get_post('swp_from_page_record');
   $this->post_page  = (INT)_get_post('page');
 
 // @ pack : set it 
 
   $this->result_swap = array();
   $this->result_post = new EUI_Object(_get_all_request());
   $this->content_debitur = $outModel->_DebiturContent( $this->result_post );
  
 // @ pack : record 

  $this->total_records = count($this->content_debitur);
   
 // @ pack : set its  
  if( $this->post_page) {
	$this->start_page = (($this->post_page-1)*$this->per_page);
  } else {	
	$this->start_page = 0;
  }

 // @ pack : set result on array
  if( (is_array($this->content_debitur)) 
	AND ( $this->total_records > 0 ) )
 {
	$this->result_swap = array_slice($this->content_debitur, $this->start_page, $this->per_page);
}	
  
 // @ pack : then set it to view 
 
  $SwapData['content_pages'] = $this->result_swap;
  $SwapData['total_records']  = $this->total_records;
  $SwapData['total_pages']  = ceil($this->total_records/ $this->per_page);
  $SwapData['select_pages'] = $this->post_page;
  $SwapData['start_page']  = $this->start_page;
  
  if( TRUE == count( $this->URI->_get_all_request() ) > 0)
  {
	 $this -> load -> view('mod_view_alldebitur/view_alldebitur_page', $SwapData );
   }
   
 }
 }
 

// =========== END CLASS ======================
}

?>
