<?php
class SaveMemo extends EUI_Controller
{
	function SaveMemo() {
		parent::__construct();
		$this -> load -> model(array(base_class_model($this)));
	}
	
	function Add(){
		if( $this -> EUI_Session -> _have_get_session('UserId') ){
			$UI = array(
				'Notes' => $this->{base_class_model($this)}->_getNotes()
			);
		$this -> load -> view('savememo/view_save_memo',$UI);
		}
		
		$formSubmit = _get_post('submitForm');
		$Note 		= _get_post('Note');
		$NoteId		= _get_post('NoteId');
		$UserId 	= $this -> EUI_Session -> _get_session('UserId');
		
		if( $formSubmit == 'formSave' ){
			if(empty($NoteId)){
				$this->{base_class_model($this)}->_insertMemo($Note, $UserId);
			} else {
				$this->{base_class_model($this)}->_updateMemo($NoteId, $Note, $UserId);
				// $this->{base_class_model($this)}->_updateMemo($update, $where, 't_gn_note');
			}
		}
 	}

}

?>