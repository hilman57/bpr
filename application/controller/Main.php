<?php
/*
 * EUI Controller  
 *
 
 * Section  : < Main > first load vail web home  if login true .
 * author 	: razaki team  
 * link		: http://www.razakitechnology.com/eui/controller 
 */

class Main extends EUI_Controller
{


	function __construct()
	{
		parent::__construct();
		$this->load->model("M_ModCtiLogin");
	}
	/*
 * main of home 
 * if user login is true 
 * notes : not validate session handle by coocies cause handle 
 * by session  
 */

	function index()
	{
		// echo "haloo";
		// die;
		$_header_website  = '';

		if ($this->EUI_Session->_have_get_session('UserId')) {
			$this->load->Model('M_Website');
			$this->load->Model('M_Menu');
			// print_r($_header_website) . '<br><br><br>';
			if (!is_array($_header_website)) {

				$data_webs = $this->M_Website->_web_get_data();
				$_header_website['website'] = $data_webs['website'];
				$_header_website['menu'] = $this->M_Menu->_get_acess_menu();
				$_header_website['CTI'] = $this->M_ModCtiLogin;
				$this->load->layout($this->Layout->base_layout() . '/UserMain', $_header_website);
			}
		} else {
			redirect("Auth/?login=(false)");
		}
	}
}

// END OF FILE
// location : ../application/controller/Main.php *
