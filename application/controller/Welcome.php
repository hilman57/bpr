<?php
/*
 * EUI Controller  
 *
 
 * Section  : Welcome first load content on HOME
			  website application enjoy its.
 * author 	: Omens  
 * link		: http://www.razakitechnology.com/eui/controller 
 */
 
class Welcome extends EUI_Controller
{

/*
 @ Constructor 
 */
 
function Welcome() 
{
	parent::__construct();	
	$this -> load -> Model('M_Website');
}	

/*
 @ method index look on URI Segment /index.php/welcome/index 
 @ loading first call on your application 
 */

function index()
{
	if( is_object( $this -> M_Website ) ) {
		$this -> load -> view("welcome/Welcome",  $this -> M_Website -> _web_get_data() );
	}	
}
	
}

// END OF FILE
// location : ./application/controller/Welcome.php

?>