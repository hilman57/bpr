<?php

	Class ValidAddPhone Extends EUI_Controller
	{
		Public Function __Construct()
		{
			parent::__construct();
			$this->load->model(array(base_class_model($this),'M_ValidAddPhone'));
		}
		
		Public Function Index()
		{
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				$_EUI['page'] = $this -> {base_class_model($this)} -> _get_default();
				
				if( is_array($_EUI) ) 
				{
					$this -> load -> view('valid_add_phone/valid_add_phone_nav',$_EUI);
				}
			}
		}
		
		public function Content() 
		{
			if( $this -> EUI_Session->_have_get_session('UserId') ) 
			{
				$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
				$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 

				if( is_array($_EUI) && is_object($_EUI['page']) ) {
					$this -> load -> view('valid_add_phone/valid_add_phone_list',$_EUI);
				}
			}
		}
		
		Public Function ApproveNumber()
		{
			$success = array('success'=> 0, 'error'=> 'NO');
			if( $this -> EUI_Session -> _have_get_session('UserId') )
			{
				if( $this -> {base_class_model($this)} -> _set_approve_number()) 
				{
					$success = array('success'=> 1, 'error'=> 'OK');	
				}
			}
			
			echo json_encode($success);
		}
	}

?>