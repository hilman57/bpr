<?php

/*
 * E.U.I 
 *
 
 * subject	: get SetCampaign modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
class ModAccessAll extends EUI_Controller
{
	/*
	 * EUI :: index() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */	
	 
	public function ModAccessAll() 
	{
		parent::__construct();
		$this -> load -> model(array(base_class_model($this)));
	}
	
	/*
	 * EUI :: index() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */	
	 
	function index()
	{

		if( $this -> EUI_Session -> _have_get_session('UserId'))
		{
			$AccessType = $this->AccessType();
			$EUI = array(
							'page'=> $this -> {base_class_model($this)} -> _get_default(),
							'Assign_Type'=>$AccessType['combo'],
							'New_Agent'=> ( $this -> {base_class_model($this)} ->get_all_DC() + 
										    $this -> {base_class_model($this)} ->get_all_leader() ),
							'Exist_Agent'=> ( $this -> {base_class_model($this)} ->get_all_DC() ),
							'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus()
			);
			$this -> load -> view('mgt_random_debitur/view_access_all_nav', $EUI );
		}
	 
	}
	
	/*
	 * EUI :: Content() 
	 * -----------------------------------------
	 *
	 * @ def		function get detail content list page 
	 * @ param		not assign parameter
	 */

	public function Content()
	{
		if( $this -> EUI_Session -> _have_get_session('UserId'))
		{
			$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
			$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
		
			if( is_array($_EUI) && is_object($_EUI['page']) ) {
				$this -> load -> view('mgt_random_debitur/view_access_all_list',$_EUI);
			}

		}
	}
	
	public function ManageDistribute()
	{
		$AccessType = $this->AccessType();
		$Content['Assign_Type'] = $AccessType['combo'];
		if(is_array($Content) )
		{
			$this -> load -> view("mgt_random_debitur/view_access_all_manage", $Content);
		}

		
	}
	
	private function AccessType()
	{
		$AccessType = $this->{base_class_model($this)}->getAccessType();
		return $AccessType;
	}
	
	public function linkAccessType()
	{
		$AccessType = $this->AccessType();
		echo json_encode($AccessType['link']);
	}
	
	public function AccessAllDeskol()
	{
		$name = "Assign_To";
		if($this->URI->_get_have_post('control'))
		{
			$name = $this->URI->_get_post('control')."_".$name;
		}
		echo form()->combo($name,'select auto', array() );
	}
	
	public function AccessTeamDeskol()
	{
		$name = "Assign_To";
		$input = "";
		if($this->URI->_get_have_post('control'))
		{
			$name = $this->URI->_get_post('control')."_".$name;
		}
		
		$combo_value = $this -> {base_class_model($this)} ->get_all_leader();
		$input =  form()->listCombo($name,'select auto', $combo_value );
		if($this->URI->_get_have_post('input_type') && $this->URI->_get_post('input_type')=='combo')
		{
			$input = form()->combo($name,'select auto', $combo_value );
		}
		echo $input;
	}
	
	public function AccessDeskol()
	{
		$name = "Assign_To";
		$input = "";
		if($this->URI->_get_have_post('control'))
		{
			$name = $this->URI->_get_post('control')."_".$name;
		}
		
		$combo_value = $this -> {base_class_model($this)} ->get_all_DC();
		$input =  form()->listCombo($name,'select auto', $combo_value );
		if($this->URI->_get_have_post('input_type') && $this->URI->_get_post('input_type')=='combo')
		{
			$input = form()->combo($name,'select auto', $combo_value );
		}
		echo $input;
	}
	
	public function AccessDefault()
	{
		$name = "Assign_To";
		if($this->URI->_get_have_post('control'))
		{
			$name = $this->URI->_get_post('control')."_".$name;
		}
		
		$combo_value = ( $this -> {base_class_model($this)} ->get_all_leader() + $this -> {base_class_model($this)} ->get_all_DC() );
		
		echo form()->combo('Assign_To','select auto', $combo_value );
	}
	
	public function AssignAccessCheck()
	{
		$msg=array();
		$msg = array('Message'=>0,'Success'=>0,'Fail'=>0);
		if( $this -> EUI_Session -> _have_get_session('UserId'))
		{
			$msg = $this -> {base_class_model($this)} ->UpdateAccessAssign();
		}
		echo json_encode($msg);
	}
	
	public function ResetAccessByCheckAll(){
		$msg = array('Message'=>0,'Success'=>0,'Fail'=>0);
		if ($this->EUI_Session->_have_get_session('UserId')){
			$msg = $this -> {base_class_model($this)} ->UpdateAccessFlagAll();
		}
		echo json_encode($msg);
	}
	public function ResetAccessByCheck()
	{
		$msg = array('Message'=>0,'Success'=>0,'Fail'=>0);
		if( $this -> EUI_Session -> _have_get_session('UserId'))
		{
			$msg = $this -> {base_class_model($this)} ->UpdateAccessFlag();
		}
		echo json_encode($msg);
	}
	
	public function AssignAccessAll()
	{
		$msg = array('Message'=>0,'Success'=>0,'Fail'=>0);
		if( $this -> EUI_Session -> _have_get_session('UserId'))
		{
			$msg = $this -> {base_class_model($this)} ->UpdateAssignAccessAll();
		}
		echo json_encode($msg);		
	}

	
}

?>