<?php
class FormDiscount extends EUI_Controller
{

var $DebiturId = null;

/*
 * @ pack : index 
 */
 
public function __construct()
{
	parent::__construct();
	$this->DebiturId = base64_decode(_get_post('DebiturId'));
	$this->load->model(array(base_class_model($this),'M_SysUser','M_ModContactDetail'));
}

/*
 * @ pack : index 
 */
 
 public function html()
{
 
 if( $this->DebiturId )
 {
	 $status = false;
	$content = array( 
		'discount' => $this->{base_class_model($this)}->_get_select_debitur($this->DebiturId),
		'campaign' => $this->{base_class_model($this)}->_get_select_campaign(),
		'cpa_reason' => $this->{base_class_model($this)}->_get_select_cpa_reason(),
		'payers' => $this->{base_class_model($this)}->_get_select_payer(),
		'occupation'=> $this->{base_class_model($this)}->_get_select_occupation(),
		'document_type' => $this->{base_class_model($this)}->_get_select_document_type(),
		'payment_periode' => $this->{base_class_model($this)}->_get_select_periode(),
		'status'=> $this->{base_class_model($this)}->_CheckingAgreement($this->DebiturId),
		'debitur'=> $this->M_ModContactDetail->_get_select_debitur($this->DebiturId),
	);
	//$status = $this->{base_class_model($this)}->_CheckingAgreement($this->DebiturId);
	//if ($status == true){
		//$this->load->form("form_html_discount/view_form_index_error", $content);
	//}else{
		$this->load->form("form_html_discount/view_form_index", $content);
	//}
	//$this->load->form("form_html_discount/view_form_index", $content);
 }
 
}

/*
 * @ pack : index 
 */

 
public function pdf()
{
  if( ($this->DebiturId) 
	AND !is_null( $this->DebiturId ) )
 {
	if( $DebiturId = $this->DebiturId ) 
	{
		$content = array( 
			'discount' => $this->{base_class_model($this)}->_get_select_debitur($DebiturId),
			'campaign' => $this->{base_class_model($this)}->_get_select_campaign() 
		);
		
	// @ pack : send data to view 
	
		if( is_array( $content ) ) {
			$this->load->form("form_pdf_discount/view_form_index", $content);
		}	
		
	} else {
		exit('No debitur id ');
	}	
 }
}

/*
 * @ pack : index 
 */
 
public function PrintByDiscountId( )
{
 
  $DiscountId = _get_post('DiscountId');
  $_recsource = $this->{base_class_model($this)}->_get_select_discountId($DiscountId);
  $DebiturId  = ($this->DebiturId ? $this->DebiturId : $_recsource['deb_id']);
  
 if( ($DebiturId) 
	AND !is_null($DebiturId) )
 {
	if( $DebiturId ) 
	{
		$content = array( 
		'recsource' => $this->{base_class_model($this)}->_get_select_discountId($DiscountId),
		'discount' => $this->{base_class_model($this)}->_get_select_debitur($DebiturId),
		'campaign' => $this->{base_class_model($this)}->_get_select_campaign(),
		'cpa_reason' => $this->{base_class_model($this)}->_get_select_cpa_reason(),
		'payers' => $this->{base_class_model($this)}->_get_select_payer(),
		'occupation'=> $this->{base_class_model($this)}->_get_select_occupation(),
		'document_type' => $this->{base_class_model($this)}->_get_select_document_type(),
		'payment_periode' => $this->{base_class_model($this)}->_get_select_periode(),
		'assigndata'=> $this->{base_class_model($this)}->_get_name_owner_data($DiscountId),
		);
		//'assigndata' => $this->{base_class_model($this)}->_get_name_owner_data($DebiturId),
		$content['ex_doc'] = explode(',',$content['recsource']['doc']);
	
		$content['agent_detail'] = ($content['recsource']['agent_id']?$this->M_SysUser->_get_detail_user($content['recsource']['agent_id']):null);
		$content['LblApprHead'] = ( ( abs($content['recsource']['from_princ']) ) >= 30?"Approval by Head of Collection":"&nbsp;" );
	// @ pack : send data to view 
	
		if(( _get_post('action')!='edit' ) 
			and ( is_array($content) )) 
		{
			$this->load->form("form_pdf_discount/view_form_index", $content); 
		}
		else {
			if( is_array( $content ) ) {
				$this->load->form("form_html_discount/view_form_index", $content);
			}
		}	
		
	} else {
		exit('No debitur id ');
	}	
 }
 
}

public function FileExtension( $filename  = '' )
{
	$dsp = explode(".", $filename);
	return $dsp[count($dsp)-1];
}

function _is_type( $t )
{
	$_conds = false;
	
	$_type = explode('.', $t);
	$_array = array ('txt','pdf','doc', 'docs','jpg','png');
	
	if( in_array( strtolower($_type[count($_type)-1]), $_array )) 
		$_conds = true;
		
	return $_conds;	
} 

/*
 * @ pack : SaveDiscount
 */
 
 public function SaveDiscount()
{

  $response = array('success' => 0);
  if( _have_get_session('UserId') ) 
  {
	if( $cond = $this->{base_class_model($this)}->_SaveDiscount() ) {
		$response = array('success' => 1);
	}
  }
 echo json_encode($response);
}


// @ pack : update form lunas 


public function DeleteAttach_1()
{
	$response	= array('success' => 0);
	if(_have_get_session('UserId')) {
		if( $cond = $this->{base_class_model($this)}->DeleteAttach_1() ) {
			$response = array('success' => 1);
		}
	}
	echo json_encode($response);
}

public function DeleteAttach_2()
{
	$response	= array('success' => 0);
	if(_have_get_session('UserId')) {
		if( $cond = $this->{base_class_model($this)}->DeleteAttach_2() ) {
			$response = array('success' => 1);
		}
	}
	echo json_encode($response);
}

public function DeleteAttach_3()
{
	$response	= array('success' => 0);
	if(_have_get_session('UserId')) {
		if( $cond = $this->{base_class_model($this)}->DeleteAttach_3() ) {
			$response = array('success' => 1);
		}
	}
	echo json_encode($response);
}

public function DeleteAttach_4()
{
	$response	= array('success' => 0);
	if(_have_get_session('UserId')) {
		if( $cond = $this->{base_class_model($this)}->DeleteAttach_4() ) {
			$response = array('success' => 1);
		}
	}
	echo json_encode($response);
}

public function DeleteAttach_5()
{
	$response	= array('success' => 0);
	if(_have_get_session('UserId')) {
		if( $cond = $this->{base_class_model($this)}->DeleteAttach_5() ) {
			$response = array('success' => 1);
		}
	}
	echo json_encode($response);
}

public function DeleteAttach_6()
{
	$response	= array('success' => 0);
	if(_have_get_session('UserId')) {
		if( $cond = $this->{base_class_model($this)}->DeleteAttach_6() ) {
			$response = array('success' => 1);
		}
	}
	echo json_encode($response);
}

public function UpdateDiscount()
{
	
	 $img = array();
	
	$response = array('success' => 0);
	
	$img  = $_FILES; $num = 0;
	if( is_array( $img ) && count($img) ) {
		foreach( $img as $row )
		{
			// print_r($img);
			$date = strtotime(date('Y-m-d H:i:s'));
			$md5 = sprintf("%s.%s",  md5($date), $row['name']);
			if( $this -> _is_type($row['name']) )  {
				if( $cond = $this->{base_class_model($this)}->_UpdateDiscount($img) ) {
					$cond = move_uploaded_file($row['tmp_name'], APPPATH .'script/'. $row['name'] ); 
					if( $cond ){
						$num++;
					}
				}				
			}	
		}
	} else {
		$this->{base_class_model($this)}->_UpdateDiscount();
		$num++;
	}
	// return callback data 
	if( $num ){
		$response = array('success' => 1);
	}
	echo json_encode($response);
}

public function reArrayFiles($file)
{
	$file_ary = array();
	$file_count = count($file['name']);
	$file_key = array_keys($file);
	
	for($i=0;$i<$file_count;$i++)
	{
		foreach($file_key as $val)
		{
			$file_ary[$i][$val] = $file[$val][$i];
		}
	}
	return $file_ary;
}

 
 
// END CLASS 

}


?>