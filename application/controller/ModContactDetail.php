<?php
/*
 * @ pack : UserContactDetail class parent controller 
 */
class ModContactDetail extends EUI_Controller
{

/*
 * @ pack : iundex function 
 */
 
 public function __construct() 
{
   parent::__construct();
   $this->load->model(array(base_class_model($this),'M_PaymentHistory','M_PaymentPTP'));
}

/*
 * @ pack : iundex function 
 */
 
 public function index()
{
 /*
  * @ pack : cek session exist 
  */
  
if(_have_get_session('UserId') )
{
  $content['Customer']  = $this->{base_class_model($this)}->_get_select_debitur();
  $content['AccOthers'] = $this->{base_class_model($this)}->_get_select_other_account();
  $content['Dropdown']  = $this->{base_class_model($this)}->_get_select_dropdown();
  $content['BucketRandomId']  = ( $this->URI->_get_post('BucketRandomId')?$this->URI->_get_post('BucketRandomId') : 0 );
  if( is_array( $content) ){
	$this->load->view("mod_contact_detail/view_contact_main_detail", $content);
  }	
 } 
}

/*
 * @ pack : PaymentSchedule
 */
public function PaymentSchedule()
{
 if(_have_get_session('UserId') ) 
 {
	 $content['payment'] = $this->{base_class_model($this)}->_get_select_payment();
	 if( is_array($content) )
	 {
		$this->load->view("mod_contact_detail/view_contact_payment_schedule", $content);
	 }
 }
 
}

/*
 * @ pack : PaymentSchedule
 */
 
public function CreateNewSms()
{
	
 if(_have_get_session('UserId') ) 
 {
	$content['Customer']  = $this->{base_class_model($this)}->_get_select_debitur();
	$content['Dropdown']  = $this->{base_class_model($this)}->_get_select_dropdown();
	$this->load->view("mod_contact_detail/view_contact_create_sms", $content);
 }
 
}

/*
 * @ pack : PaymentSchedule
 */
 
public function Template()
{

 $_conds = array( 'text' => '' );	
 $TemplateId =_get_post('TemplateId');
 if(_have_get_session('UserId') ) 
 {
	$Customer = $this->{base_class_model($this)}->_get_select_debitur();
	$TextMessage = $this->{base_class_model($this)}->_get_textmessage_sms($TemplateId,$Customer['deb_call_status_code']);
	if( $TextMessage )
	{	
	  $TextMessage = str_replace("{DC_NAME}", strtoupper(_get_session('Fullname')),  $TextMessage);
	  $TextMessage = str_replace("{NAMA_CH}", $Customer['deb_name'],  $TextMessage);
	  $TextMessage = str_replace("{NO_KARTU}",$Customer['deb_acct_no'],  $TextMessage);
	  $TextMessage = str_replace("{AMOUNT_WO}",_getCurrency($Customer['deb_amount_wo']),  $TextMessage);
	  $_conds = array( 'text' => $TextMessage );
	}
 }
 
 echo json_encode($_conds);
 
}

// @ pack : DataAccountMaping

 public function DataAccountMaping()
{
 
 if( $this->EUI_Session->_get_session('UserId') )
 {
	
// @ on pack : get data user 
	
	if( _get_post('action') =='onDetail' )
	{
		$result_data = $this->{base_class_model($this)}->_get_select_data_maping();
		if( is_array($result_data)) {
			echo json_encode( array("success"=> 1, 'data'=>  $result_data) );
		} else {
			echo json_encode( array("success"=> 0) );
		}
	}
	
// @ on pack : get data user 
	
	if( _get_post('action') =='onShow' )
	{
		$data['OtherAccount'] = $this->URI->_get_all_request();
		if( is_array($data['OtherAccount']) )
		{
			$this->load->view("mod_contact_detail/view_contact_accountmap", $data);
		}
	}	
  }
}

// @ pack : mod content index 

 public function setUdateCall()
{
  $conds = array('success' => 0 );
  
  if( $this->EUI_Session->_have_get_session('UserId') )
  {
	 $callback = $this->{base_class_model($this)}->_setUdateCall();
	 if($callback) {
		$conds = array('success' => 1); 
	 }
  }
  
  echo json_encode($conds);
}
 
// @ pack : mod content index 

 public function IsCall()
{
  $conds = array('success' => 0 );
  
  if( $this->EUI_Session->_have_get_session('UserId') )
  {
	 $callback = $this->{base_class_model($this)}->_getIsCall();
	 if($callback) {
		$conds = array('success' => 1); 
	 }
  }
  
  echo json_encode($conds);
}

	/***
	**** payment history page
	**** 02/04/2015
	***/
	 
	public function PaymentHistoryPage()
	{
	  $this->start_page = 0;
	  $this->per_page   = 20;
	  $this->post_page  = (int)_get_post('page');
	 
	 // @ pack : set it 
	 
	  $this->result_payment_history = array();
	  $this->content_payment_history = $this->M_PaymentHistory->_get_payment_history_customer(_get_post('DebiturId'));
	 
	 // @ pack : record 

	  $this->total_records = count($this->content_payment_history);
		
	 // @ pack : set its  
	  /* if( $this->post_page) {
		$this->start_page = (($this->post_page-1)*$this->per_page);
	  } else {	
		$this->start_page = 0;
	  } */

	 // @ pack : set result on array
	  if( (is_array($this->content_payment_history)) 
		AND ( $this->total_records > 0 ) )
	 {
		// $this->result_payment_history = array_slice($this->content_payment_history, $this->start_page, $this->per_page);
		$this->result_payment_history = $this->content_payment_history;
	}	
	  
	 // @ pack : then set it to view 
	 
	  $CallHistory['content_pages']  = $this->result_payment_history;
	  $CallHistory['total_records']  = $this->total_records;
	  // $CallHistory['total_pages']  = ceil($this->total_records/ $this->per_page);
	  // $CallHistory['select_pages'] = $this->post_page;
	  $CallHistory['start_page']  = $this->start_page;

	 // @ pack : sent to view 

	 $this->load->view("mod_contact_detail/view_payment_history_page", $CallHistory);
	}
	
	public function PaymentHistoryXO()
	{
		$history_xo = array ();
		$contentPH = array();
		$X_O = array();
		$listPH = $this->M_PaymentHistory->_get_payment_detail(_get_post('DebiturId'));
		$Month = $this->M_PaymentHistory->_get_select_month();
		$customer = $this->{base_class_model($this)}->_get_select_debitur(_get_post('DebiturId'));
		//print_r($customer);
		//$inputdate = $this->M_PaymentHistory->_get_class_entry($customer['deb_open_date']);
		
		if(is_array($listPH) AND count($listPH)>0)
		{
			foreach ($listPH as $index => $value)
			{
				$expl_index = explode('-',$index);
				
				$contentPH[$expl_index[0]][$expl_index[1]]= $value;
				
			}
		}
        foreach($contentPH as $year => $arrayval){
			foreach( $Month as $i => $n ){
				$X_O[$year][$i] =  $this->_class_html( (array_key_exists($n,$arrayval)?1:0) );
			}
		}
		$history_xo = array ('contentPH'=>$contentPH,
							 'Month'=>$Month,
							 'x_o_value'=> $X_O,
							 'Input_Date' => date( 'd-m-Y', strtotime($customer['deb_open_date']) ) );
		$this->load->view("mod_contact_detail/view_payment_history_xo", $history_xo);
	}
	
	private function _class_html( $conds )
	{
		$array_html = array(
		  1 => "<div class='assign' style='color:green;'>Pay</div>",	
		  0 => "<div class='close' style='color:red;'>No Pay</div>"
		);
		 
		return  $array_html[$conds];
	}
	
	
	public function deletePayment()
	{
		$conds = array('success' => 0 );
  
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			if($this->M_PaymentHistory->DeletePayment())
			{
				$conds = array('success' => 1);
			}
		}

		echo json_encode($conds);
	}
	
	public function deletePTP()
	{
		$conds = array('success' => 0 );
  
		if( $this->EUI_Session->_have_get_session('UserId') )
		{
			if($this->M_PaymentPTP->DeletePTP())
			{
				$conds = array('success' => 1);
			}
		}
		echo json_encode($conds);
	}
 // end of class
	
	public function CheckAdditionalPhone(){
		$content['Dropdown']  = $this->{base_class_model($this)}->_get_select_dropdown();
		// print_r($content['Dropdown']['COLL_DROPDOWN_ADD']);
		echo form()->combo('select_add_phone','select_auto auto', $content['Dropdown']['COLL_DROPDOWN_ADD'], 
				$content['Dropdown']['COLL_CUSTOMER_PHN'], array("change"=>"Ext.DOM.SetCallDial(this);"));
	}
 
	// -------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------
	/*
	 * @ pack : Instance of class  
	 *
	 */ 
	 
	 public function PageProspectNote()
	{
		if(_have_get_session('UserId') )
		{
			$per_page = 10;
			$page_select = (INT)_get_post('page');
			$start_page = 0;
			
			// @ pack : set its  
			if( $page_select ) {
				$start_page = (($page_select-1)*$per_page);
			} else {	
				$start_page = 0;
			}
		
			$this->load->helper(array('EUI_Object'));
			$result_post = new EUI_Object(_get_all_request());
			$result_data = $this->{base_class_model($this)}->ListGrid( $result_post );
			$total_records =  $this->{base_class_model($this)}->TotalGrid( $result_post );
			$grid['content_pages'] = $result_data;
			$grid['total_records'] = $total_records;
			$grid['total_pages']  = ceil($total_records / $per_page);
			$grid['select_pages'] = $page_select;
			$grid['start_page']  = $start_page;
			if( TRUE == count( $this->URI->_get_all_request() ) > 0)
			{
				$this -> load -> view('mod_contact_detail/view_prospect_note_page', $grid );
			}
		}	 
	}
	
	public function FormComplain()
	{
		$UI['SOURCE_COMPLAIN'] = $this->M_Combo->_getSourceComplain();
		$UI['KASUS_KOMPLEN'] = $this->M_Combo->_getKasusKomplen();
		$UI['DEBITUR'] = base64_decode(_get_post('DebiturId'));
		$this->load->view('mod_contact_detail/view_form_complain',$UI);
	}
	public function FormReasonVC()
	{
		$UI['REASON_VC'] = $this->M_Combo->_getReasonVC(_get_post('CallStatus'));
		// $UI['KASUS_KOMPLEN'] = $this->M_Combo->_getKasusKomplen();
		$UI['DEBITUR'] = base64_decode(_get_post('DebiturId'));
		$this->load->view('mod_contact_detail/view_form_vc_reason',$UI);
	}

}

?>