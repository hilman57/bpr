<?php 
	error_reporting(E_ALL ^ E_DEPRECATED);
	set_time_limit(0);
	//--------------------------------------------------------------------------
	/*
	 * @ package 		class conroller RptCallIncoming  
	 * @ auth 			Didi 
	 */
	//constants
	define('HOSTNAME', '192.168.0.11');
	define('USER', 'enigma');
	define('PASSWORD', 'enigma');
	define('DATABASE', 'asteriskcdrdb');

	class RptCallIncoming extends EUI_Controller 
	{


		//--------------------------------------------------------------------------
		/*
		 * @ package 		EUI_Report_helper
		 */

		function __construct() 
		{
			parent::__construct();
			$this->load->helper(array('EUI_Object','EUI_Report'));
		}
		
		/*
		* Get View report 
		* and get parameter 
		*/
		public function index()
		{
			$this->load-> view("rpt_call_inc/rpt_call_incoming");
		}

		/*
		*  SHOW report 
		*  @param calldate = start_date & calldate = end_date
		*  @param mode = daily,weekly,monthly
		*  @return url =  mode&start_date&end_date
		*  author : didi gantengs
		*/
		Public Function ShowReport()
		{
			//get mode
			$mode = $_REQUEST['mode'];
			$type = $_REQUEST['type'];

			//prepare pass paramater
			$data['mode'] 	 = $mode;
			$data['type'] 	 = $type;
			$data['daily']   = $this->get_daily();
			$data['detail_daily'] = $this->_get_detail_daily();
			$data['weekly']  = $this->get_weekly();
			$data['monthly'] = $this->get_monthly();
			//print_r($data['datas']);exit;
			//cek mode
			if ( $type == 'daily' && $mode == 'summary' ) {
				$this->load->view('rpt_call_inc/rpt_show_daily', $data);
			} else if( $type == 'daily' && $mode == 'detail')  {
				$this->load->view('rpt_call_inc/rpt_show_daily_detail', $data);
			} else if ( $type == 'weekly') {
				$this->load->view('rpt_call_inc/rpt_show_weekly', $data);
			} else {
				$this->load->view('rpt_call_inc/rpt_show_monthly', $data);
			}
		}
		
		/*
		*  SHOW Excel 
		*  @param calldate = start_date & calldate = end_date
		*  @param mode = daily,weekly,monthly
		*  @return Excel Name dinamis by mode parameter
		*          array
		*  author : didi gantengs
		*/
		public function ShowExcel() {
			//get mode
			$mode = $_REQUEST['mode'];
			$type = $_REQUEST['type'];
			$types = strtoupper($type);
			$modes = strtoupper($mode);//change to font capital

			$mode_name = ($modes != "") ? $modes : "";

			//prepare pass paramater
			$data['mode'] 	 = $mode;
			$data['detail_daily'] = $this->_get_detail_daily();
			$data['daily']   = $this->get_daily();
			$data['weekly']  = $this->get_weekly();
			$data['monthly'] = $this->get_monthly();
			//print_r($data['datas']);exit;
			
			//generate to excel
			Excel() -> HTML_Excel(urlencode(_get_post('report_title')).'REPORT_INCOMING_CALL_'.$types.'_'.$mode_name);
			//cek mode
			if ( $type == 'daily' && $mode == 'summary' ) {
				$this->load->view('rpt_call_inc/rpt_show_daily', $data);
			} else if( $type == 'daily' && $mode == 'detail')  {
				$this->load->view('rpt_call_inc/rpt_show_daily_detail', $data);
			} else if ( $type == 'weekly') {
				$this->load->view('rpt_call_inc/rpt_show_weekly', $data);
			} else {
				$this->load->view('rpt_call_inc/rpt_show_monthly', $data);
			}
		}

		/*
		* GET DAILY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function get_daily()
		{
			//get parameter date
			$start 	= $_REQUEST['start_date'];
			$end 	= $_REQUEST['end_date'];

			//parameter incoming call
			$param_inc = 'from-pstn';

			//get connection database
			$conn = $this->_connect_to_db();
		
			$query = mysql_query("
				SELECT 
					COUNT(cd.clid) AS total,
					DATE(cd.calldate) as call_date , 
					SUM(cd.duration) as durasi,
					ROUND(AVG(cd.duration)) as av
				FROM cdr cd 
				WHERE 
					cd.dcontext = '".$param_inc."' AND
					cd.calldate >='".$start." 00:00:00' 
					AND cd.calldate <= '".$end." 23:59:59' 
				GROUP BY 
					date(cd.calldate)"
			);
			//echo $query;
			
			while ($rows = mysql_fetch_assoc($query)) {
				$data[] = $rows;
			}
			return $data;
		}

		/*
		* GET WEEKLY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function get_weekly()
		{
			//get parameter date
			$start = $_REQUEST['start_date'];
			$end   = $_REQUEST['end_date'];

			//parameter incoming call
			$param_inc = 'from-pstn';

			//get connection database
			$conn = $this->_connect_to_db();

			$query = mysql_query("
				SELECT 
					COUNT(cd.clid) AS total,
					DATE_FORMAT(cd.calldate, '%M') AS bulan,
					WEEK(cd.calldate) AS call_date , 
					SUM(cd.duration) AS durasi,
					WEEK(DATE(cd.calldate), 3) -
				  	WEEK(DATE(cd.calldate) - INTERVAL DAY(DATE(cd.calldate))-1 DAY, 3) + 1 AS week_number,
				  	ROUND(AVG(cd.duration)) AS av
				FROM cdr cd
				WHERE 
					cd.dcontext = '".$param_inc."' AND
					cd.calldate >='".$start." 00:00:00' 
					AND cd.calldate <= '".$end." 23:59:59' 
				GROUP BY 
					WEEK(cd.calldate)"
			);

			while ($rows = mysql_fetch_assoc($query)) {
				$data[] = $rows;
			}

			return $data;
		}

		/*
		* GET MONTHLY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function get_monthly() 
		{
			//get parameter date
			$start = $_REQUEST['start_date'];
			$end   = $_REQUEST['end_date'];

			//parameter incoming call
			$param_inc = 'from-pstn';

			//get connection database
			$conn = $this->_connect_to_db();

			$query = mysql_query("
				SELECT 
					COUNT(cd.clid) AS total,
					DATE_FORMAT(cd.calldate, '%M') AS bulan,
					MONTH(cd.calldate) AS call_date , 
					SUM(cd.duration) AS durasi,
				  	cd.calldate,
				  	ROUND(AVG(cd.duration)) AS av
				FROM cdr cd
				WHERE 
					cd.dcontext = '".$param_inc."' AND
					cd.calldate >='".$start." 00:00:00' 
					AND cd.calldate <= '".$end." 23:59:59' 
				GROUP BY 
					MONTH(cd.calldate);"
			);

			while ($rows = mysql_fetch_assoc($query)) {
				$data[] = $rows;
			}

			return $data;
		}

		/*
		* GET REPORT DETAIL DAILY 
		* @param calldate >= start_date & calldate = end_date
		* @return array 2 dimensional 
		* author : didi gantengs 
		*/
		function _get_detail_daily()
		{
			$param_inc = 'from-pstn';
			//get parameter date
			$start = $_REQUEST['start_date'];
			$end   = $_REQUEST['end_date'];

			$conn = $this->_connect_to_db();

			$query = mysql_query("
				SELECT 
				    cr.calldate AS CALLDATE, 
				    cr.clid AS PHONENUMBER, 
				    cr.duration AS TOTAL_DURATION  
				FROM cdr cr 
				WHERE 
					cr.dcontext='".$param_inc."' AND
					cr.calldate >='".$start." 00:00:00' AND
					cr.calldate <= '".$end." 23:59:59' 
				ORDER BY cr.calldate DESC
			");
			//echo $query;
			while ($rows = mysql_fetch_array($query)) {
				$data[] = $rows;
			}

			return $data;
		}

		/*
		* function connect to dababase asterisk 
		* @param HOSTNAME, USER ,PASSWORD
		* @return connect or lose
		* author : didi gantengs
		*/
		function _connect_to_db() 
		{
			$connect = mysql_connect(HOSTNAME, USER, PASSWORD) or die('Could not connect to server.' );

			mysql_select_db(DATABASE, $connect);
			
			if( !$connect )
			{
				die( 'kaga konek gans');
			}

			return $connect;
		}
	}
?>