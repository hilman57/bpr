<?php
/**
 * MailQueue - PHP creation and transport class.
 *
 * @package EUI_Controller
 *
 * @author omens ( rohmattullah ) < jombi.par@yahoo.com >
 * @update bugfix cache data **
 * 
 */
 
class MailQueue extends EUI_Controller
{

public function MailQueue()
{
	parent::__construct();
	$this -> load -> model(array('M_Configuration'));
}

// get to 

function CC( $OutboxId = 0 )
{
  
  $rows = array();
  if( $OutboxId ) 
  {
	$this->db->select("*");
	$this->db->from("email_copy_carbone");
	$this->db->where("EmailReffrenceId", $OutboxId);
	$this->db->where("EmailDirection",2);
	
	if( $OutboxId ){
		$rows = $this->db->get();
	}
  }
  
  return $rows;
}


/*
 * @ def 		: _getAttach
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function BCC( $OutboxId = 0 )
{
  $rows = array();
  if( $OutboxId ) 
  {
	$this->db->select("*");
	$this->db->from("email_blindcopy_carbone");
	$this->db->where("EmailReffrenceId", $OutboxId);
	$this->db->where("EmailDirection",2);
	
	if( $OutboxId ){
		$rows = $this->db->get();
	}
  }
  
  return $rows;
}

/*
 * @ def 		: _getAttach
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Destination( $OutboxId = 0 )
{
  $rows = array();
  if( $OutboxId ) 
  {
	$this ->db->select("*");
	$this ->db->from("email_destination");
	$this ->db->where("EmailReffrenceId", $OutboxId);
	$this->db->where("EmailDirection",2);
	
	if( $OutboxId ){
		$rows =$this->db->get();
	}
  }
  
  return $rows;
}

 
/*
 * @ def 		: _getAttach
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function Attachment($OutboxId = 0)
{
  $rows = array();
  if( $OutboxId ) 
  {
	$this ->db->select("*");
	$this ->db->from("email_attachment_url");
	$this ->db->where("EmailReffrenceId", $OutboxId);
	
	if( $OutboxId ){
		$rows =$this->db->get();
	}
  }
  
  return $rows;
		
}


 
/*
 * @ def 		: SaveMailToQueue
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Queue()
{

  $Config =& $this->M_Configuration->_getMail();
  $TryOut = (INT)$Config['outbox.retry'];
  if( $TryOut ) 
  {
	$this->db->select("*");
	$this->db->from("email_queue");
	$this->db->where('QueueTrying<=', $TryOut, FALSE);
	$this->db->where('date(QueueCreateTs)=', date('Y-m-d'));
	$this->db->where_not_in('QueueStatus', array(1005));
	$this->db->order_by('QueueId','ASC');
	
	return $this->db->get();
  }
  else
	return null;
}

 
/*
 * @ def 		: SaveMailToQueue
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function Outbox( $OutboxId= 0)
{
  $rows = array();
  if( $OutboxId ) 
  {
	$this ->db->select("*");
	$this ->db->from("email_outbox");
	$this ->db->where("EmailOutboxId", $OutboxId);
	
	if( $OutboxId ){
		$rows =$this->db->get();
	}
  }
  
  return $rows;
}



/*
 * @ def 		: Process Mail Queue 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function Process() 
{
 
 $this->load->library('EUI_PHPMailer'); // load file 
 $Config = $this->M_Configuration->_getMail(); // require mail 
 if( class_exists('EUI_PHPMailer') AND is_array($Config) ) 
 {
	$this->EUI_PHPMailer->SMTPDebug = $Config['smtp.debug'];
	$this->EUI_PHPMailer->isSMTP();  						
	$this->EUI_PHPMailer->SMTPAuth = true;
	$this->EUI_PHPMailer->Port = $Config['smtp.port'];
	$this->EUI_PHPMailer->Host = $Config['smtp.host']; 	
	$this->EUI_PHPMailer->Username = $Config['smtp.auth']; 	 
	$this->EUI_PHPMailer->Password = $Config['smtp.password'];  
	
 /*------ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< OPTIONAL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-----*/	
   
	if(($Config['smtp.secure']!='')){
		$this->EUI_PHPMailer->SMTPSecure = $Config['smtp.secure'];
	}
	
 /*------ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< MAIN PROC >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-----*/	

 $qry = $this->Queue();
 $i = 0;
 foreach( $qry->result_assoc() as $rows  )
 {
	$QueueId = $rows['QueueId'];		
	$QueueTrying =(int)$rows['QueueTrying'];
	$CountTrying =(int)$Config['outbox.retry'];
	
	if( $QueueTrying < $CountTrying )
	{
		/** global attributes **/
		$this->EUI_PHPMailer->setFrom($Config['smtp.auth'],$Config['smtp.name']);
		$OutboxId =(INT)$rows['QueueMailId'];
		
		/** from Outbox Content data **/
		
		$Outbox = $this->Outbox($OutboxId);
		if( is_object($Outbox) AND ($Content = $Outbox ->result_first_assoc()) )
		{
			$this->EUI_PHPMailer->Subject = $Content['EmailSubject'];
			
			if((isset($Content['EmailContent'])) AND ($Content['EmailContent']!='') )
				$this->EUI_PHPMailer->msgHTML($Content['EmailContent']);
			else
				$this->EUI_PHPMailer->msgHTML("To view the message, please use an HTML compatible email viewer!");
				
			$this->EUI_PHPMailer->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		}
				
		/** get destination - to **/
		
		$Destination = $this->Destination($OutboxId);
		if( is_object($Destination) ){foreach( $Destination->result_assoc() as $rows )
		{
			if( trim($rows['EmailDestination'])!='' ){
				$this->EUI_PHPMailer->addAddress($rows['EmailDestination'],$rows['EmailDestination']);
			}
		}}
				
		/** get CC address **/
				
		$CC = $this->CC($OutboxId);
		if(is_object($CC) ){foreach( $CC->result_assoc() as $rows )
		{
			if( trim($rows['EmailCC'])!='' ){
				$this->EUI_PHPMailer->addCC($rows['EmailCC'],$rows['EmailCC']);
			}
		}}
				
		/** get BCC address **/
			  
		$BCC = $this->BCC($OutboxId);
		if(is_object($BCC)){foreach( $BCC->result_assoc() as $rows )
		{
			if( trim($rows['EmailBCC'])!='' ){
				$this->EUI_PHPMailer->addBCC($rows['EmailBCC'],$rows['EmailBCC']);
			}
		}}
				
				
		 /** get Attachment  **/
		 $Attachment = $this->Attachment($OutboxId);
		 if( is_object($Attachment) ){ foreach( $Attachment->result_assoc() as $rows )
		 {
			if( trim($rows['EmailAttachmentPath'])!=''){
				$this->EUI_PHPMailer->addAttachment($rows['EmailAttachmentPath']);
			}
		  }}
		
		/** try to execption **/
		
		try 
		{
			echo "$i\n\r";
			
			/*** self::SetSaveError ****/
			self::SetSaveError($QueueId, $OutboxId, 'On Progreess', 1005);
			if( $this->EUI_PHPMailer->send() ) 
			{
				$this->db->set('EmailStatus',1003);
				$this->db->set('EmaiUpdateTs',date('Y-m-d H:i:s'));
				$this->db->where('EmailOutboxId',$OutboxId);
				$this->db->update('email_outbox');
				
				if( $this->db->affected_rows() > 0 )
				{
					$this->db->set('HistoryRefferenceId',$OutboxId); 
					$this->db->set('HistoryStatus',1003);
					$this->db->set('HistoryDirection',2); 
					$this->db->set('HistoryReason', "SENT OK");
					$this->db->set('HistoryCreateTs',date('Y-m-d H:i:s'));
					$this->db->insert('email_history');
					
					if( $this->db->affected_rows() > 0 ) 
					{
						$this->db->where("QueueId", $QueueId);
						$this->db->delete('email_queue');
					}	
				}
				
				/** reset all receiption **/
				$this->EUI_PHPMailer->clearAllRecipients();
				
				/* reset attachment **/
				$this->EUI_PHPMailer->clearAttachments();
				
			}
			else {
				self::SetSaveError($QueueId, $OutboxId, $this->EUI_PHPMailer->getError());
			}
				
		 } catch (phpmailerException $e){
			self::SetSaveError($QueueId, $OutboxId, $e->errorMessage());
		 } catch (Exception $e) {
			 self::SetSaveError($QueueId, $OutboxId, $e->errorMessage());
		 }
	   }
	   else{
			self::SetDeleteQueueError($QueueId); 	
	   }
	   
	   $i++;
    }
  } 
} 
 
/*
 * @ def 		: SetSaveError
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function SetSaveError($QueueId = 0 , $OutboxId = 0 , $message = '', $error_code=1004 )
{
	if( $QueueId )
	{
		$this->db->set('QueueStatus', $error_code);
		$this->db->set('QueueReason', $message);
		$this->db->set('QueueTrying','((QueueTrying)+1)', FALSE);
		$this->db->set('QueueStatusTs', date('Y-m-d H:i:s'));
		$this->db->where('QueueId',$QueueId);
		$this->db->update('email_queue');
		
		if( $this->db->affected_rows() > 0 )
		{
			$this->db->set('EmailStatus',$error_code);
			$this->db->set('EmaiUpdateTs',date('Y-m-d H:i:s'));
			$this->db->where('EmailOutboxId',$OutboxId);
			$this->db->update('email_outbox');
			
			if( $this->db->affected_rows() > 0 )
			{
				$this->db->set('HistoryRefferenceId',$OutboxId); 
				$this->db->set('HistoryStatus', $error_code);
				$this->db->set('HistoryDirection',2); 
				$this->db->set('HistoryReason', $message);
				$this->db->set('HistoryCreateTs',date('Y-m-d H:i:s'));
				$this->db->insert('email_history');
			}
		}
    }
}


/*
 * @ def 		: SetSaveError
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function SetDeleteQueueError($QueueId = 0 )
{
	$count = 0;
	if( $QueueId )
	{
		$this->db->where('QueueId',$QueueId);
		$this->db->delete('email_queue');
		
		if( $this->db->affected_rows() > 0 )
		{
			$count++;
		}
    }
	
	echo $this->db->last_query();
	
	return $count;
}


}	
	
?>
