<?php
class Questioner extends EUI_Controller
{


	function Questioner()
	{
		parent::__construct();
		$this->load->model(array(base_class_model($this)));
	}

	protected function encrypt_decrypt($string = null, $action = 'encrypt', $string_encrypt = null)
	{
		// $encrypt_method = "AES-256-CBC";
		// $secret_key = 'hsbc-collection';
		// $secret_iv = 'hsbc-collection';
		// $key = hash('sha256', $secret_key);
		// $iv = substr(hash('sha256', $secret_iv), 0, 16);
		if ($action == 'encrypt') {
			$output = md5($string);
		} else if ($action == 'decrypt') {
			// $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
			if (md5($string) === $string_encrypt)
				$output = $string;
			else
				$output = null;
		}
		return $output;
	}

	public function login()
	{
		$checkLogin = $this->M_Questioner->checkLogin($_POST['username'], md5($_POST['password']));
		// print_r($checkLogin);die;
		if ($checkLogin != null) {
			echo json_encode(array(
				'success'		=> 1,
				'message'		=> 'Login Successfully!',
				'data'			=> array(
					'login'		=> $checkLogin['UserId'],
					'token'		=> $this->encrypt_decrypt($checkLogin['UserId'], 'encrypt')
				)
			));
		} else {
			echo json_encode(array(
				'success'		=> 0,
				'message'		=> 'Login Unsuccessfully!',
				'data'			=> null
			));
		}
	}

	public function getQuestionerCategory()
	{
		if ($_POST['login'] == $this->encrypt_decrypt($_POST['login'], 'decrypt', $_POST['token'])) {
			$data = $this->M_Questioner->getQuestionerCategory();
			echo json_encode(array(
				'success'		=> 1,
				'message'		=> 'Get data question successfully!',
				'data'			=> $data
			));
		} else {
			echo json_encode(array(
				'success'		=> 0,
				'message'		=> 'Invalid token or token not match!',
				'data'			=> null
			));
		}
	}

	public function getQuestioner()
	{
		if ($_POST['login'] == $this->encrypt_decrypt($_POST['login'], 'decrypt', $_POST['token'])) {
			$arrData = array();
			$data = $this->M_Questioner->getQuestioner($_POST['id_category']);
			// print_r($data);die;
			if (count($data) > 0) {
				foreach ($data as $val) {
					unset($val['jawabanbenar']);
					$userid = explode(',', $val['UserId']);
					if (in_array($_POST['login'], $userid)) {
						array_push($arrData, $val);
					}
				}
			}
			echo json_encode(array(
				'success'		=> 1,
				'message'		=> 'Get data question successfully!',
				'data'			=> $arrData
			));
		} else {
			echo json_encode(array(
				'success'		=> 0,
				'message'		=> 'Invalid token or token not match!',
				'data'			=> null
			));
		}
	}

	public function checkSubmitQuestioner()
	{
		if ($_POST['login'] == $this->encrypt_decrypt($_POST['login'], 'decrypt', $_POST['token'])) {
			$data = array();
			$checkResultQuestioner = $this->M_Questioner->getResultQuestioner($_POST['login']);
			foreach ($checkResultQuestioner as $val) {
				array_push($data, $val['category_id']);
			}
			echo json_encode(array(
				'success'		=> 1,
				'message'		=> 'Get data question successfully!',
				'data'			=> $data
			));
		} else {
			echo json_encode(array(
				'success'		=> 0,
				'message'		=> 'Invalid token or token not match!',
				'data'			=> null
			));
		}
	}

	public function submitQuestioner()
	{
		if ($_POST['login'] == $this->encrypt_decrypt($_POST['login'], 'decrypt', $_POST['token'])) {
			$arrData = array();
			$true = 0;
			$false = 0;
			$data = $this->{base_class_model($this)}->getQuestioner($_POST['id_category']);
			if (count($data) > 0) {
				foreach ($data as $val) {
					$userid = explode(',', $val['UserId']);
					if (in_array($_POST['login'], $userid)) {
						array_push($arrData, $val);
					}
				}
				for ($i = 0; $i < count($arrData); $i++) {
					if ($arrData[$i]['jawabanbenar'] == $_POST['answer'][$i]['value']) {
						$true++;
					} else {
						$false++;
					}
				}
				$dataResultQuestioner = array(
					'deskoll'		=> $_POST['login'],
					'answer_true'	=> $true,
					'answer_false'	=> $false,
					'category_id'	=> $_POST['id_category']
				);
				$this->M_Questioner->insertResultQuestioner($dataResultQuestioner);
			}
			echo json_encode(array(
				'success'		=> 1,
				'message'		=> 'Get data question successfully!',
				// 'data'			=> $arrData
				'data'			=> $dataResultQuestioner
			));
		} else {
			echo json_encode(array(
				'success'		=> 0,
				'message'		=> 'Invalid token or token not match!',
				'data'			=> null
			));
		}
	}

	public function enableMenu()
	{
		$data = $this->{base_class_model($this)}->m_enableMenu();
		echo json_encode(array('success' => 1, 'response' => 201, 'message' => 'Success Data', 'data' => $data));
	}

	public function index()
	{

		if ($this->EUI_Session->_have_get_session('UserId')) {
			$page['page'] = $this->{base_class_model($this)}->get_default();
			$this->load->view("mod_questioner/view_menuproject_nav", $page);
		}
	}



	public function content()
	{
		$page['page'] = $this->{base_class_model($this)}->get_resource_query(); // load content data by pages 
		$page['num']  = $this->{base_class_model($this)}->get_page_number(); 	// load content data by pages 
		if (is_array($page) && is_object($page['page'])) {

			$this->load->view("mod_questioner/view_menuproject_list", $page);
		}
	}

	/**
	 * @ def : add menu layout user navigation HHH 
	 *
	 */

	public function AddMenuWorkProject()
	{
		if ($this->EUI_Session->_get_session('UserId')) {
			$UI = $this->{base_class_model($this)}->_AddMenuWorkProject();
			if ($UI) {
				$this->load->view("mod_questioner/view_add_menuproject", $UI);
			}
		}
	}


	/**
	 * @ def : add menu layout user navigation HHH 
	 *
	 */

	public function ApplicationMenuByProject()
	{
		if ($this->EUI_Session->_get_session('UserId')) {
			$UI = array('ApplicationMenuByProject' => $this->{base_class_model($this)}->_WorkMenuApplication($this->URI->_get_post('WorkProjectId')));
			if ($UI) {
				$this->load->view("mod_questioner/view_load_menuproject", $UI);
			}
		}
	}

	/** 
	 * @ def  : delete menu on work project detected 
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/DeleteApplicationMenuByProject/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array )
	 * @ param : $ProjectId ( array ) 
	 */

	public function DeleteApplicationMenuByProject()
	{
		$conds = array('success' => 0);

		if ($this->EUI_Session->_get_session('UserId')) {
			$UI = $this->{base_class_model($this)}->_DeleteApplicationMenuByProject();
			if ($UI) {
				$conds = array('success' => 1);
			}
		}

		echo json_encode($conds);
	}

	/** 
	 * @ def  : delete menu on work project detected 
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/AssignMenuWorkProjectById/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array )
	 * @ param : $ProjectId ( array ) 
	 */

	public function AssignMenuWorkProjectById()
	{
		$conds = array('success' => 0);


		if ($this->EUI_Session->_get_session('UserId')) {
			$UI = $this->{base_class_model($this)}->_AssignMenuWorkProjectById();
			if ($UI) {
				$conds = array('success' => 1);
			}
		}

		echo json_encode($conds);
	}

	/** 
	 * @ def  : delete menu on work project by rows in grid table  
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/RemoveApplicationMenuById/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array ) 
	 */

	public function RemoveApplicationMenuById()
	{
		$conds = array('success' => 0);
		// var_dump($this->URI->_get_post('WorkMenuId'));
		// die;
		if ($this->EUI_Session->_get_session('UserId')) {
			$UI = $this->{base_class_model($this)}->_RemoveApplicationMenuById();
			if ($UI) {
				$conds = array('success' => 1);
			}
		}

		echo json_encode($conds);
	}

	/** 
	 * @ def  : delete menu on work project by rows in grid table  
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/ActiveWorkApplicationMenu/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array ) 
	 */

	public function ActiveWorkApplicationMenu()
	{
		$conds = array('success' => 0);

		if ($this->EUI_Session->_get_session('UserId')) {
			$UI = $this->{base_class_model($this)}->_ActiveWorkApplicationMenu(
				$this->URI->_get_array_post('WorkMenuId'),
				$this->URI->_get_post('Activate')
			);

			if ($UI) {
				$conds = array('success' => 1);
			}
		}

		echo json_encode($conds);
	}
}
