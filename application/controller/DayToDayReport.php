<?php

class DayToDayReport extends EUI_Controller  
{

    public function index()
    {
        $this->load->view('daytodayreport/index');
    }

    public function tgl_indo($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
    }
    public function detailcsv()
    {
        
            $this->load->view('daytodayreport/detailcsv', $data);
          }

    // edit hilman
    public function detail()
    {
        if ($_GET['button'] == 'Action') {
        
            $this->load->view('daytodayreport/detail', $data);
        } else {


      //generate to excel
           Excel() -> HTML_Excel();
      //cek mode
      ?>
     
     <h1>REPORT DAY TO DAY</h1>  
  <?php
function load_report($current,$awal,$akhir){

  //$awal = '2019-08-08 00:00:00';
  //$akhir = '2019-08-08 23:00:00';

  $no_agent = 1;
  $data_agent = $current->db->query("
        SELECT	
        ag.UserId AS TL_UID,
        ag.full_name as TL,
        c.id AS AGENT,
        c.full_name AS DC,
        a.CallAccountStatus AS ACCOUNT_STATUSID,
        st.CallReasonDesc as ACCOUNT_STATUS,
        a.AccountNo AS 'ACCOUNTNO',
        a.CallHistoryCallDate 'TGLUPDATE'

      FROM (
        SELECT AccountNo,MAX(CallHistoryCallDate) AS GROUP_ID 
        FROM t_gn_callhistory 	
        WHERE
          CallHistoryCallDate BETWEEN '".$awal."' AND '".$akhir."' 
          AND TeamLeaderId!=''
          
          
        GROUP BY AccountNo,AgentCode
      ) AS TBL_TMP
      
      INNER JOIN t_gn_callhistory a ON (a.CallHistoryCallDate=TBL_TMP.GROUP_ID AND a.AccountNo=TBL_TMP.AccountNo)

      LEFT JOIN t_tx_agent c ON c.id = a.AgentCode
      LEFT JOIN t_gn_debitur b ON b.deb_acct_no = a.AccountNo
      LEFT JOIN t_lk_account_status st on a.CallAccountStatus = st.CallReasonCode
      LEFT JOIN t_tx_agent ag on a.TeamLeaderId = ag.UserId 

      WHERE
        a.CallHistoryCallDate BETWEEN '".$awal."' AND '".$akhir."' 
        AND ag.userId!=''
        
      ORDER BY TL_UID,a.ACCOUNTNO ASC
  ")->result();
  
  $user_TL = array();
  

  foreach ($data_agent as $r) {
    $tl_uid = '#'.$r->TL_UID;
    $tl_name = $r->TL;
    $status = $r->ACCOUNT_STATUS;
    $agent_id = $r->AGENT;

    if ($user_TL[$tl_uid]){

      // agent array
      if($agent[$agent_id][$status]){
        $agent[$agent_id][$status][] = $r;
      }else{

        $agent[$agent_id][$status] = array();
        $agent[$agent_id][$status][] = $r;
        
      }

      $user_TL[$tl_uid] = $agent;

      //print_r($user_TL);
      //exit();

    }else{
      $user_TL[$tl_uid] = array();
      $agent = array();
      
      // agent array
      if($agent[$agent_id][$status]){
        $agent[$agent_id][$status][] = $r;
      }else{
        $agent[$agent_id][$status] = array();
        $agent[$agent_id][$status][] = $r;
      }

      $user_TL[$tl_uid] = $agent;
      //print_r($user_TL);
      //exit();
    }
    
  }

  //print_r($user_TL);
  //exit();

  $ON_STATUS = array(
    'NEW' => 0, 
    'OS-ON PROCESS' => 0,
    'OP-ON PROSPECT' => 0,
    'VALID' => 0,
    'ON NEGO' => 0,
    'PTP-NEW' => 0,
    'PTP-POP' => 0,
    'PTP-BP' => 0,
    'PAID OFF' => 0,
    'BUSY LINE' => 0, 
    'DECEASE' => 0
  );

  $user = array();
  foreach ($user_TL as $user_id => $groups) {
    foreach($groups as $agent_id=>$row){
      
      $data = $ON_STATUS;
      $info = array();

      foreach($row as $status=>$r){
        $info = (array)$r[0];
        $data[trim($status)] = count($r);
      }
      unset($info['ACCOUNT_STATUS']);
      
      $data = array_merge($info,$data);

      $user[$user_id][$agent_id] = $data;
      
    }
  }
   

  $USERS = array();  
  $arr = array();

  foreach ($user as $user_id => $groups) {

    $ROWS = array();
    
    $first = true;
    $TOTAL_NEW = 0;
    $TOTAL_ON_PROCESS = 0;
    $TOTAL_ON_PROSPECT = 0;
    $TOTAL_VALID = 0;
    $TOTAL_ON_NEGO = 0;
    $TOTAL_PTP_NEW = 0;
    $TOTAL_PTP_POP = 0;
    $TOTAL_PTP_BP = 0;
    $TOTAL_PAID_OFF = 0;
    $TOTAL_BUSY_LINE = 0;
    $TOTAL_DECEASE = 0;
    $ALL_GRANT_TOTAL = 0;  
    $ALL_ATTEMP = 0;  

    foreach ($groups as $agent_id => $row){

      $TOTAL_NEW = $TOTAL_NEW + $row['NEW'];
      $TOTAL_ON_PROCESS = $TOTAL_ON_PROCESS + $row['OS-ON PROCESS'];

      $TOTAL_ON_PROSPECT = $TOTAL_ON_PROSPECT + $row['OS-ON PROSPECT'];
      $TOTAL_VALID = $TOTAL_VALID + $row['VALID'];
      $TOTAL_ON_NEGO = $TOTAL_ON_NEGO + $row['ON NEGO'];
      $TOTAL_PTP_NEW = $TOTAL_PTP_NEW + $row['PTP-NEW'];
      $TOTAL_PTP_POP = $TOTAL_PTP_POP + $row['PTP-POP'];
      $TOTAL_PTP_BP = $TOTAL_PTP_BP + $row['PTP-BP'];
      $TOTAL_PAID_OFF = $TOTAL_PAID_OFF + $row['PAID OFF'];
      $TOTAL_BUSY_LINE = $TOTAL_BUSY_LINE + $row['BUSY LINE'];
      $TOTAL_DECEASE = $TOTAL_DECEASE + $row['DECEASE'];

      $GRANT_TOTAL = $row['NEW'] + $row['OS-ON PROCESS']+ $row['OP-ON PROSPECT']+ $row['VALID']+ $row['ON NEGO']+ 
                    $row['PTP-NEW']+$row['PTP-POP']+$row['PTP-BP']+$row['PAID OFF']+$row['BUSY LINE']+$row['DECEASE'];
      
      $ALL_GRANT_TOTAL  = $ALL_GRANT_TOTAL + $GRANT_TOTAL; 
      $ALL_ATTEMP       = $ALL_ATTEMP + $arr[$no2++]; 

      // ambil dari sini
      array_push($arr, $GRANT_TOTAL);
      
      $ROWS[$row['AGENT']] = array(
        'AGENT' => $row['AGENT'],
        'DC' => $row['DC'],
        'TOTAL'=> $GRANT_TOTAL
      );

      $USERS[$user_id] = array(
        'TL'=> $row['TL'],
        'rows' => $ROWS
      );

    }
  }

  //print_r($USERS);
  //exit();

  return $USERS;
}

$report = array();

$start_date = $_GET['start_date'];
$total_day = date('d',strtotime($start_date));


for($i=1;$i<=$total_day;$i++){
  $day = str_pad($i, 2, "0", STR_PAD_LEFT);
  $ym = date('Y-m',strtotime($start_date));

  $awal = $ym.'-'.$day.' 00:00:00';
  $akhir = $ym.'-'.$day.' 23:00:00';
  
  $r = load_report($this,$awal,$akhir);
  
  $report[] = $r;
   
}

// print_r($report);
// exit();

$report_agent = array();
$user_TL = array();

foreach($report as $rows){
  
  foreach($rows as $user_id => $user){
    
    $user_TL[$user_id] = $user['TL'];

    foreach($user['rows'] as $agent_id => $agent){
      
      $report_agent[$user_id][$agent_id]= $agent['DC'];    
      
    }
     
  }
  
}

?>

<table border="1px green" cellspacing="0">
  <thead>
    <tr>
      
      <th class="text-left">TL</th>
      <th>AGENT</th>
      <th class="text-left">DC</th>
     <?php

      $no=1;
      for($no=1;$no<=$total_day;$no++){
        echo '<th>'.$no.'</th>';
      }
      
      ?>
      <th>TOTAL</th>
    </tr>
  <thead>

  <tbody>
    <?php
      echo '<br>';
      // print_r($arrtl);
      
      foreach($report_agent as $user_id => $agents){
        $f=true;
        
        $TOTAL_STATUS_PERDAY = array();
        $TL=1; 

        foreach($agents as $agent_id => $agent_name){
          // print_r($arrtl);
          echo '<tr>'; 
          
          if($f){
           echo '<td class="text-left">'.$user_TL[$user_id].'</td>';
           $f=false;
          }else{
            echo '<td></td>';
          }

          echo '<td>'.$agent_id.'</td>';
          echo '<td class="text-left">'.$agent_name.'</td>';

          # TOTAL HORIZONTAL /AGENT
          $xx=0;
          $TOTAL_STATUS_AGENT = 0;

          while($xx < $total_day){
            $agent = $report[$xx][$user_id]['rows'][$agent_id];
            
            $total_status = (int)$agent['TOTAL'];
            echo '<td>'.(($total_status) ? $total_status : '-').'</td>';

            $xx++;

            $TOTAL_STATUS_AGENT = $TOTAL_STATUS_AGENT + (int)$agent['TOTAL'];

            $TOTAL_STATUS_PERDAY[$xx] = $TOTAL_STATUS_PERDAY[$xx] + (int)$agent['TOTAL'];
          }

          echo '<td>'.$TOTAL_STATUS_AGENT.'</td>';
          
          echo '</tr>';
        }

        # TOTAL VERTICAL /TL
        echo '<tr class="bg-grey">';
        echo '<td colspan="3" class="text-left"><strong>TOTAL</strong></td>';

        $ALL_TOTAL = 0;
        foreach($TOTAL_STATUS_PERDAY as $TOTAl_PERDAY){
          echo '<td>'.(($TOTAl_PERDAY) ? $TOTAl_PERDAY : '-').'</td>';
          $ALL_TOTAL = $ALL_TOTAL + $TOTAl_PERDAY;
          
        }
        
        echo '<td>'.$ALL_TOTAL.'</td>';
        echo '</tr>';        

        echo '<tr><td colspan="'.($total_day+3).'"></td></tr>';

      }
    ?>
  </tbody>
  
</table>       
<?php
        }
    }

    function nama_tl($id)
    {
        $nama_tl = $this->db->query("SELECT full_name, code_user FROM t_tx_agent WHERE UserId='$id'")->result();

        return $nama_tl[0]->code_user . " - " . $nama_tl[0]->full_name;
    }

    public function crontab()
    {

        //$tglLike = '2019-06';
        $tglLike = date('Y-m');
        $this->db->query("truncate t_gn_ranking");
        $this->db->query("truncate t_gn_siklus_data");
        $this->db->query("truncate t_gn_siklus_report");
        $this->db->query("truncate t_gn_siklus_user");
        $data = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
    FROM t_gn_payment
    INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
    WHERE t_gn_payment.pay_date LIKE '%$tglLike%'
    GROUP BY tl_id
    ORDER BY amount_tl DESC")->result();

        foreach ($data as $row) {

            $nama_tl = $this->nama_tl($row->tl_id);
            $no = 0;
            $data_agent = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.id as id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl, t_gn_payment.pay_date
        FROM t_gn_payment
        INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
        WHERE t_tx_agent.tl_id='$row->tl_id' AND t_gn_payment.pay_date LIKE '%$tglLike%'
        GROUP BY t_tx_agent.id
        ORDER BY amount_tl DESC")->result();

            foreach ($data_agent as $row_agent) {
                $no++;
                $date = date('Y-m-d H:i:s');
                $pay_date = date('Ym');
                $this->db->query("INSERT into t_gn_ranking VALUES('','$no','$row_agent->code_user', '$nama_tl', '$row_agent->amount_tl','$pay_date','$date')");
                // $push['no'] = $no;
                // echo array_push($row_agent, $push);
                // echo json_encode($row_agent);

            }
        }
        echo "sukses";
    }



    /*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
}

// END OF CLASS 

?>