<?php
/*
 * @ pack : Lsit of PTP 
 */
 
class SrcPtpList extends EUI_Controller
{
	/*
	* @ pack : approval discount 
	*/

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(base_class_model($this)));
	}
	
	public function index()
	{
		if( $this->EUI_Session->_have_get_session('UserId') ) 
		{
			$_EUI  = array(
				'page' => $this ->{base_class_model($this)} ->_get_default(), 
				'Deskoll' => $this->{base_class_model($this)}->_getDeskollByLogin(),
				'GenderId' => $this->{base_class_model($this)}->_getGenderId(),
				'CampaignId' => $this->{base_class_model($this)}->_getCampaignByActive(),
				'AccountStatus' => $this->{base_class_model($this)}->_getAccountStatus(),
				'LastCallStatus' => $this->{base_class_model($this)}->_getLastCallStatus() 
			);
		   if(is_array($_EUI)) {
			 $this -> load ->view('src_ptp_list/view_ptp_nav',$_EUI);
		   }	
		}
	}
	
	/*
	 * @ def 		: content / default pages controller 
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition paymode 
	 * @ return 	: void(0)
	 */
	 
	public function Content() 
	{
	  if( $this -> EUI_Session->_have_get_session('UserId') ) 
	  {
		$_EUI['page'] = $this ->{base_class_model($this)}->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->{base_class_model($this)}->_get_page_number(); // load content data by pages 
		
		if( is_array($_EUI) && is_object($_EUI['page']) ) {
			$this -> load -> view('src_ptp_list/view_ptp_list',$_EUI);
		 }	
	  }	
	}
}
//End class