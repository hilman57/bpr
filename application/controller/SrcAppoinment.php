<?php
class SrcAppoinment extends EUI_Controller
{

function SrcAppoinment()
{
  parent::__construct();
  $this ->load ->model(array('M_SrcAppoinment','M_SrcCustomerList','M_SetCampaign','M_SetCallResult'));
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 public function getCallResult($CategoryId = null )
{
	$_conds = array();
	$_arr_callresult = $this->M_SetCallResult->_getCallback(null);
	
	if(is_array($_arr_callresult))foreach( $_arr_callresult as $k =>$call ) {
		$_conds[$call['code']] = $call['name'];
	}
	
	return $_conds;
 }
 
/*
 * @ pack : index of page 
 */ 
 
 public function index()
{
  if( $this ->EUI_Session ->_have_get_session('UserId') )
 {
	$_EUI  = array(
		'page' 			=> $this->M_SrcAppoinment->_get_default(),
		'CampaignId' 	=> $this->M_SetCampaign->_get_campaign_name(),
		'GenderId' 		=> $this->M_SrcCustomerList->_getGenderId(),
		'CallResult' 	=> $this->getCallResult()
	);
	
	if( is_array($_EUI)) {
		$this->load->view('src_appointment_list/view_appoinment_nav',$_EUI);
	}	
 }
 
}
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this ->M_SrcAppoinment ->_get_resource();    // load content data by pages 
		$_EUI['num']  = $this ->M_SrcAppoinment ->_get_page_number(); // load content data by pages 
		
		// sent to view data 
		// var_dump($_EUI['page']);
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('src_appointment_list/view_appoinment_list',$_EUI);
		}	
	}	
 }
 
 // udpate 
function Update()
{
	$callback = array('success' => 0);
	
	if( $this -> URI -> _get_have_post('CustomerId') )
	{
		$this -> db -> set('ApointmentRead', 1);
		$this -> db -> where('CustomerId', $this->URI->_get_post('CustomerId') , FALSE);
		$this -> db -> where('UserId',$this->EUI_Session->_get_session('UserId') , FALSE);
		// var_dump($this->db);
		if( $this -> db -> update('t_gn_appoinment') )
		{
			$callback = array('success' => 1);
		}
	}
	
	echo json_encode($callback);
}	
 
}

?>