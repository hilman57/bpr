<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for SysUser 
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class SetCampaignScript extends EUI_Controller
{
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function SetCampaignScript()
 {
	parent::__construct();
	$this -> load -> model( array( base_class_model($this),'M_SetPrefix') );
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function index()
 {
	if( $this ->EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this -> {base_class_model($this)} -> _get_default();
		if( is_array($_EUI) ) 
		{
			$this -> load -> view('set_campaign_script/view_campaign_script_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)} -> _get_resource();    // load content data by pages 
		$_EUI['num']  = $this->{base_class_model($this)} -> _get_page_number(); // load content data by pages 
		
		if( is_array($_EUI) 
		   AND is_object($_EUI['page']) )  
		{
			$this -> load -> view('set_campaign_script/view_campaign_script_list',$_EUI);
		}	
	}	
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

private function _getCampaignName()
{
	$this->db->select('a.CampaignId, a.CampaignCode, a.CampaignDesc');
	$this->db->from('t_gn_campaign a');
	$this->db->order_by('a.CampaignId','ASC');
	
	foreach( $this->db->get()->result_assoc() as $rows ) {
		$_conds[$rows['CampaignId']] = $rows['CampaignCode'] ." / ". $rows['CampaignDesc']; 
	}
	return $_conds;
}
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 function AddScript()
 {
	$UI  = array
	(
		'CampaignName' => $this->_getCampaignName(),
		'Active'=> $this ->M_SetPrefix->_get_status_prefix()
	);
	
	$this -> load -> view("set_campaign_script/view_campaign_script_add",$UI);
 }


 /*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function FileExtension( $filename  = '' )
 {
	$dsp = explode(".", $filename);
	return $dsp[count($dsp)-1];
 }
 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _is_type( $t )
{
	$_conds = false;
	
	$_type = explode('.', $t);
	$_array = array ('txt','pdf','doc', 'docs');
	
	if( in_array( strtolower($_type[count($_type)-1]), $_array )) 
		$_conds = true;
		
	return $_conds;	
} 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Upload()
{
	$_result = array('success'=>0);
	if( isset($_FILES['ScriptFileName'])) 
	{
		$date = strtotime(date('Y-m-d H:i:s'));
		
		$md5 = md5($date) .".". $this->FileExtension($_FILES['ScriptFileName']['name']);
		$_Data = $this -> URI -> _get_all_request();
		if( $this -> _is_type($_FILES['ScriptFileName']['name']) ) 
		{
			if( move_uploaded_file($_FILES['ScriptFileName']['tmp_name'], APPPATH .'script/'. $md5 ))
			{
				$_Data['md5file'] = $md5;
				if( $this->{base_class_model($this)}->_setUpload( array('post_data' => $_Data, 'post_files' => $_FILES ) )) 
				{
					$_result = array( 'success' => 1 );
				}
			}
		}	
	}
	
	echo json_encode($_result);
	
}
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function SetActive()
{
	$_result = array('success'=>0); $_Data = array();
	if( $this -> URI->_get_have_post('ScriptId') ) 
	{
		$_Data['ScriptId'] = $this -> URI -> _get_array_post('ScriptId');
		$_Data['Flags'] = $this -> URI -> _get_post('Active');
		if( isset($_Data['ScriptId']))
		{
			if( $this ->{base_class_model($this)}->_setActive($_Data) )
			{
				$_result = array('success'=>1);
			}
		}
	}
	
	echo json_encode($_result);
	
} 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Delete()
{
	$_result = array('success'=>0); $_Data = array();
	if( $this -> URI->_get_have_post('ScriptId') ) 
	{
		$_Data['ScriptId'] = $this -> URI -> _get_array_post('ScriptId');
		if( isset($_Data['ScriptId']))
		{
			if( $this ->{base_class_model($this)}->_setDelete($_Data) )
			{
				$_result = array('success'=>1);
			}
		}
	}
	
	echo json_encode($_result);
	
} 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function getScript()
{
	$_conds = array();
	if( $this -> EUI_Session->_get_session('UserId') ) {
		$_DataScript= $this -> {base_class_model($this)}->_getScript();
		if( is_array( $_DataScript )) {
			$_conds = $_DataScript;
		}
	}
	
	echo json_encode($_conds);
}

// ShowProductScript
function ShowProductScript()
{
	$ScriptId = $this -> URI->_get_64post('ScriptId');	
	if( $ScriptId )
	{
		$UI =  array( 'Data' => $this ->{base_class_model($this)}->_getDataScript($ScriptId) );
		if( is_array($UI) ) {
			$this -> load -> view("set_campaign_script/view_campaign_show",$UI);
		}
	}
}

function ShowCampaignScript()
{
	$ScriptId = $this -> URI->_get_64post('ScriptId');
	if( $ScriptId )
	{
		$UI =  array( 'Data' => $this ->{base_class_model($this)}->_getDataScript($ScriptId) );
		if( is_array($UI) )
		{
			$this -> load -> view("set_campaign_script/view_campaign_show",$UI);
		}
	}
}

function ShowScriptByProject()
{
	$script = $this -> {base_class_model($this)}->_getScript();
	$id = 0;
	
	foreach($script as $key => $value) {
		$id = $key;
	}
	
	$UI =  array( 'Data' => $this ->{base_class_model($this)}->_getDataScript($id) );
	if( is_array($UI) )
	{
		$this -> load -> view("set_campaign_script/view_campaign_show",$UI);
	}
}
 
}
?>