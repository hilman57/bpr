<?php
/**
 * @ def : FindCustomerDetail class 
 * -------------------------------------------
 
 * @ package : controller 
 * @ subject : operation collection ( AIA )
 *
 */
 
class FindCustomerDetail extends EUI_Controller
{

/** set global variable **/

var $PolicyNumber = NULL;
var $CIFNumber = NULL;
var $CustomerName = NULL;

/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
public function __construct()
{
 parent::__construct();
 $this->load->model(array(base_class_model($this),
	'M_Combo',
	'M_SrcCustomerList', 
	'M_SetCallResult','M_SetProduct', 
	'M_SetCampaign','M_SetResultCategory',
	'M_ModSaveActivity', 'M_SetResultQuality',
	'M_QtyScoring', 'M_Pbx',
	'M_FieldPolicy' 
));

/** set key cif number **/

 if( $this->URI->_get_have_post('cif_number') 
	AND is_null($this->CIFNumber) )
 {
	$this->CIFNumber = $this->URI->_get_post('cif_number');
 }

/** set key cif customer_name **/

 if( $this->URI->_get_have_post('customer_name') 
	AND is_null($this->CustomerName) )
{
	$this->CustomerName = $this->URI->_get_post('customer_name');
 }
 
/** set key cif policy_number **/
	
 if( $this->URI->_get_have_post('policy_number') 
	AND is_null($this->PolicyNumber))
 {
	$this->PolicyNumber = $this->URI->_get_post('policy_number');
 }
 
}

/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
public function index()
{


	if( $this->EUI_Session->_have_get_session('UserId') ) 
	{
		$UI = array(
		'cif_number' => $this->URI->_get_post('cif_number'),
		'customer_name' => $this->URI->_get_post('customer_name'),
		'page' => $this->URI->_get_post('page'),
		'policy_number' => 	$this->URI->_get_post('policy_number') 
	);
	
	if( is_array($UI))
	{
		$this->load->view('mod_find_customer_detail/UserContent',$UI);
	}
	}
}


/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
 
public function FindByKeywords()
{
	$UI = array( 
		'record_assoc' => $this->{base_class_model($this)}->recsource_page_data(),
		'total_record' => $this->{base_class_model($this)}->get_record_data(),
		'total_page' => $this->{base_class_model($this)}->total_page(),
		'current' => $this->URI->_get_post('page')	
		
	);
	if( is_array($UI)) 
	{ 
		$this->load->view('mod_find_customer_detail/UserIndex',$UI);
	}
}	

/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 
public function SelectDetail()
{
	if( $this->URI->_get_have_post('CustomerId') )
	{
		$CustomerId = $this->URI->_get_post('CustomerId');
		$PolicyId = $this->URI->_get_post('PolicyId');
		
		$UI = array ( 
			'Customers' 	 => $this->{base_class_model($this)}->_getDetailCustomer($CustomerId),
			'PolicyNumber'	 => $this->{base_class_model($this)}->_getPolicyCustomer($PolicyId), 
			'Callhistory' 	 => $this->{base_class_model($this)}->_getLastCallHistory($PolicyId),
			'page' 			 => $this->URI->_get_post('page')
		);
		
		$this->load->view('mod_find_customer_detail/UserDetail', $UI);
	}
}
/**
 * @ def : __construct parent methode 
 * -------------------------------------------
 
 * @ param : 3 posted parameter 
 */
 

public function Callhistory()
{
	$conds = array();
	if( $this->URI->_get_have_post('PolicyId') )
	{
		$conds = $this->{base_class_model($this)}->_getLastCallHistory($PolicyId);
	}
	
	return $conds;
}




// END OF CLASS 

}
?>