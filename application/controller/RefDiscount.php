<?php
class RefDiscount extends EUI_Controller
{

 function RefDiscount() 
 {
	parent::__construct();
	$this -> load -> model(array(base_class_model($this)));
 }

/*
 * @ pack : _get_default
 */
 
function index()
 {
	if($this->EUI_Session->_have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_default_disc();
		if( is_array($_EUI))
		{
			$this->load->view('ref_disc_view/view_disc_nav',$_EUI);
		}	
	}	
 }
 
/*
 * @ pack : _get_default
 */
 
function content()
 {
	if( $this -> EUI_Session -> _have_get_session('UserId') )
	{
		$_EUI['page'] = $this->{base_class_model($this)}->_get_resource_disc();    // load content data by pages 
		$_EUI['num']  = $this->{base_class_model($this)}->_get_page_number_disc(); // load content data by pages 
		if( is_array($_EUI) && is_object($_EUI['page']) )  
		{
			$this -> load -> view('ref_disc_view/view_disc_list',$_EUI);
		}	
	}	
 }

 
/*
 * @ pack : _get_default
 */
 
 function Add()
 {
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		$this -> load -> view('ref_disc_view/view_disc_add',
			array
			(
				'FieldName' => $this -> {base_class_model($this)}-> _get_field_disc(),
				'HTML' 		=> $this -> {base_class_model($this)}-> _get_component_disc()
			)
		);
	}
 }
 
 

/*
 * @ pack : _get_default
 */
 
 public function Edit()
{
 $discId = $this->URI->_get_post('Id');
 if( $this->EUI_Session->_have_get_session('UserId') ) 
 {
	$UI = array(
		'IVR' => $this->{base_class_model($this)}->_get_detail_disc($discId), 
		'FieldName' => $this->{base_class_model($this)}->_get_field_disc(),
		'HTML' => $this->{base_class_model($this)}->_get_component_disc()
	);
		
	 $this -> load -> view('ref_disc_view/view_disc_edit',$UI);
 }
 	
 }
 
/*
 * @ pack : _get_default
 */
 
 public function Save()
{
  $conds = array('success' => 0);
  if( $this->EUI_Session->_have_get_session('UserId') ) {
	if( $this->{base_class_model($this)}->_set_save_disc($this->URI->_get_all_request())) {
		$conds = array('success' => 1);
	}
  }
  
  echo json_encode($conds);
 }
 
/*
 * @ pack : _get_default
 */

 public function Update()
 {
	$conds = array('success' => 0);
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		if( $this->{base_class_model($this)}->_set_update_disc($this->URI->_get_all_request()))
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
}

 /*
 * @ pack : _get_default
 */
 
 public function Delete()
 {
	$conds = array('success' => 0);
	if( $this->EUI_Session->_have_get_session('UserId') )
	{
		if( $this->{base_class_model($this)}->_set_delete_disc($this->URI->_get_array_post('Id')))
		{
			$conds = array('success' => 1);
		}
	}
	
	echo json_encode($conds);
 }
 
}

?>