<?php
class MailMonitoring extends EUI_Controller
{

function MailMonitoring() 
{
	parent::__construct();
	$this -> load -> model(array(base_class_model($this)));
}
	
	
function index() 
{
	if( $this -> EUI_Session->_get_session('UserId') )
	{
		$this -> load -> view("mod_mail_monitoring/view_mail_queue_nav",array(null));
	}
}

	
public function Content()
{
	if( $this -> EUI_Session->_get_session('UserId') )
	{
		$UserActivity = array
		(
			'mail_list_queue' => $this->{base_class_model($this)}->_getMailqueueMonitoring(),
			'config' => $this->{base_class_model($this)}->_getConfig()  
		);
		$this -> load -> view("mod_mail_monitoring/view_mail_queue_list",$UserActivity);
	}	
}

public function Retry()
{
	$_conds = array('success' => 0 );
	$this -> db -> set('QueueTrying',0);
	$this -> db -> set('QueueStatus',1001);
	$this -> db -> set('QueueStatusTs',date('Y-m-d H:i:s'));
	$this -> db -> where('QueueId', $this -> URI->_get_post('QueueId'));
	$this -> db ->update('email_queue');
	if( $this -> db ->affected_rows() > 0 ) 
	{
		$this->db->select('QueueMailId');
		$this->db->from('email_queue');
		$this->db->where('QueueId', $this -> URI->_get_post('QueueId'));
		if( $rs = $this->db->get() -> result_first_assoc() )
		{
			$this->db->set('HistoryRefferenceId',$rs['QueueMailId']); 
			$this->db->set('HistoryStatus', 1001);
			$this->db->set('HistoryDirection',2); 
			$this->db->set('HistoryReason', 'Retry Send Email');
			$this->db->set('HistoryCreateTs',date('Y-m-d H:i:s'));
			$this->db->insert('email_history');
		}	
		
		$_conds = array('success' => 1 );
	}
	
	echo json_encode($_conds);
	
}

public function Cancel()
{
	$_conds = array('success' => 0 );
	
	$this->db->select('QueueMailId');
	$this->db->from('email_queue');
	$this->db->where('QueueId', $this -> URI->_get_post('QueueId'));
	
	if( $rs = $this->db->get() -> result_first_assoc() )
	{
		$this->db->set('EmailStatus',1006); // cancel
		$this->db->set('EmaiUpdateTs',date('Y-m-d H:i:s')); // cancel
		$this->db->where('EmailOutboxId', $rs['QueueMailId']);
		$this->db->update('email_outbox');
		
		if( $this->db->affected_rows() > 0  )
		{
			$this->db->set('HistoryRefferenceId',$rs['QueueMailId']); 
			$this->db->set('HistoryStatus', 1006);
			$this->db->set('HistoryDirection',2); 
			$this->db->set('HistoryReason', 'Cancel by user');
			$this->db->set('HistoryCreateTs',date('Y-m-d H:i:s'));
			$this->db->insert('email_history');
			if( $this->db->affected_rows() > 0  )
			{
				$this -> db -> where('QueueId',$this->URI->_get_post('QueueId'));
				if($this -> db ->delete('email_queue') ) 
				{
					$_conds = array('success' => 1 );
				}
			}	
		}
	}
	
	
	echo json_encode($_conds);
	
	
	
}


}
?>