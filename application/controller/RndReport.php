<?php 
//--------------------------------------------------------------------------
/*
 * @ package 		class conroller ReportCpaSummary  
 * @ auth 			uknown User 
 */

 class RndReport extends EUI_Controller 
{


	//--------------------------------------------------------------------------
	/*
	 * @ package 		EUI_Report_helper
	 */

	function __construct() 
	{
		parent::__construct();
		$this->load->model(
			array(
				'M_Report',
				'M_MgtRandDeb',
				base_class_model($this)
			)
		);

	}
	
	public function index()
	{
		$random_modul = $this->{base_class_model($this)}->modul_random();
		$UI['cmb_random_modul'] = $random_modul['combo'];
		// echo "<pre>";
		// var_dump($this -> URI->_get_array_post('start_date_claim'));
		// echo "</pre>";
		$this->load-> view("rpt_random/report_random_nav",$UI);

	}
	
	public function get_random_report()
	{
		$random_modul = $this->{base_class_model($this)}->modul_random();
		echo json_encode($random_modul['ui_report']);
	}
	
	public function report_access_all()
	{
		$UI_Mode = strtolower($this -> URI->_get_post('type_show'));
		switch( $UI_Mode )
		{
			case 'showhtml' 	: $this -> access_all_html(); 	break;
		}
	}
	
	private function access_all_html()
	{
		$REPORT['data_tabel'] = $this->{base_class_model($this)}->get_claim_access_all();
		$REPORT['title'] = $this -> URI->_get_post('report_title');
		
		$this->load-> view("rpt_random/access_all_html",$REPORT);
	}
 }