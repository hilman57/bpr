<?php
 
class Ranking extends EUI_Controller
{
 
 public function index($bulan=NULL) 
{ 
    $data['data'] = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
    FROM t_gn_payment
    INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
    GROUP BY tl_id
    ORDER BY amount_tl DESC")->result();
    $this -> load ->view('ranking/index', $data);	
}

public function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

public function detail() {

    if ($_GET['button'] == 'Action') {
        $bulan = $_GET['bulan'];
        $data['data'] = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
        FROM t_gn_payment
        INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
        WHERE t_gn_payment.pay_date LIKE '%$bulan%'
        GROUP BY tl_id
        ORDER BY amount_tl DESC")->result();
        $data['bulan'] = $bulan;
        $this -> load ->view('ranking/detail', $data);
    } else {	

    //generate to excel
    Excel() -> HTML_Excel();
    //cek mode
?>
<h1>REPORT RANKING <?php echo $this->tgl_indo($_GET['bulan']) ?></h1>
<table>
    <tr>
        <th>#</th>
        <th>ID TL</th>
        <th>Amount</th>
        <th>Detail</th>
    </tr>
<?php
$bulan = $_GET['bulan'];
$data = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
FROM t_gn_payment
INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
WHERE t_gn_payment.pay_date LIKE '%$bulan%'
GROUP BY tl_id
ORDER BY amount_tl DESC")->result();
$data['bulan'] = $bulan;
$no = 1;
foreach($data as $row)  {
    $nama_tl = $this->db->query("SELECT full_name, code_user FROM t_tx_agent WHERE UserId='$row->tl_id'")->result()
?>
    <tr>
        <td align="center"><?php echo $no++ ?></td>
        <td style="width:300px;"><?php echo $nama_tl[0]->code_user ?> [ <?php echo $nama_tl[0]->full_name ?> ]</td>
        <td style="width:200px;">Rp. <?php echo number_format($row->amount_tl,0,'.','.') ?></td>
        <td style="width:450px;">
        <table class="table_dalam">
            <tr>
                <th>#</th>
                <th>ID Agent</th>
                <th>Amount</th>
            </tr>
            <?php
            $no_agent = '1';
            $data_agent = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.id as id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
            FROM t_gn_payment
            INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
            WHERE t_tx_agent.tl_id='$row->tl_id' AND t_gn_payment.pay_date LIKE '%$bulan%'
            GROUP BY t_tx_agent.id
            ORDER BY amount_tl DESC")->result();

            foreach ($data_agent as $row_agent) {
            ?>
            <tr>
                <td align="center"><?php echo $no_agent++ ?></td>
                <td style="width:300px;"><?php echo $row_agent->code_user ?> [ <?php echo $row_agent->nama_tl ?> ]</td>
                <td style="width:150px;">Rp. <?php echo number_format($row_agent->amount_tl,0,'.','.') ?></td>
            </tr>
            <?php } ?>
        </table>
        </td>
    </tr>
<?php
}
?>
</table>
<?php
}

}

function nama_tl($id) {
    $nama_tl = $this->db->query("SELECT full_name, code_user FROM t_tx_agent WHERE UserId='$id'")->result();

    return $nama_tl[0]->code_user." - ".$nama_tl[0]->full_name;
}

public function crontab() {

    //$tglLike = '2019-10';
    $tglLike = date('Y-m');
    $pay_date = date('Ym');
    //$pay_date = '201910';
    $datemin = date('m')-1;

    if (date('d') >= 6 && date('d') <=25) {
        echo "forbidden";
        return false;
    }

    if (date('d') >= 1 && date('d') <= 5) {
        $tglLike = date('Y').'-'.$datemin;
        $pay_date= date('Y').$datemin;
    }

    //echo $tglLike;
    $this->db->query("truncate t_gn_ranking");
    $this->db->query("truncate t_gn_siklus_data");
	$this->db->query("truncate t_gn_siklus_report");
	$this->db->query("truncate t_gn_siklus_user");
    $data = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.tl_id as tl_id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl
    FROM t_gn_payment
    INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
    WHERE t_gn_payment.pay_date LIKE '%$tglLike%'
    GROUP BY tl_id
    ORDER BY amount_tl DESC")->result();

    foreach ($data as $row) {

        $nama_tl = $this->nama_tl($row->tl_id);
        $no = 0;
        $data_agent = $this->db->query("SELECT SUM(t_gn_payment.pay_amount) as amount_tl, t_tx_agent.id as id, t_tx_agent.code_user as code_user, t_tx_agent.full_name as nama_tl, t_gn_payment.pay_date
        FROM t_gn_payment
        INNER JOIN t_tx_agent ON t_tx_agent.id=t_gn_payment.pay_agent_id
        WHERE t_tx_agent.tl_id='$row->tl_id' AND t_gn_payment.pay_date LIKE '%$tglLike%'
        GROUP BY t_tx_agent.id
        ORDER BY amount_tl DESC")->result();

        foreach ($data_agent as $row_agent) {
            $no++;
            $date=date('Y-m-d H:i:s');
            // $pay_date='201910';
            // $pay_date=date('Ym');
            $this->db->query("INSERT into t_gn_ranking VALUES('','$no','$row_agent->code_user', '$nama_tl', '$row_agent->amount_tl','$pay_date','$date')");
            // $push['no'] = $no;
            // echo array_push($row_agent, $push);
            // echo json_encode($row_agent);
            
        }

    }
    echo "sukses";
    
}


 
/*
 * @ def 		: content / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 

}

// END OF CLASS 

?>