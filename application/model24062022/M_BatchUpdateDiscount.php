<?php 

/*
 * @ pack : modul update log request discount && lunas 
 * --------------------------------------------------------
 * --------------------------------------------------------
 */

class M_BatchUpdateDiscount extends EUI_Model 
{

/*
 * @ pack : private static $Instance 
 */
 
 private static $Instance = NULL;

/*
 * @ pack : M_BatchClearLog
 */
 
 public static function &Instance() 
{
 if(is_null(self::$Instance) ) {
  self::$Instance = new self();	
 }
  return self::$Instance;
  
}

/*
 * @ pack : update _SetUpdateDiscount  loook on Payment 
 *			 HIstory with interval that like history 
 * ---------------------------------------------------------------------			 
 */ 
 
 public function _setBatchUpdateDiscount()
{
	$Mbatch =&get_class_instance('M_BatchApplication');
	
	$conds = 0;
	/* @ pack : define its . ========================> */
	 $AGREE_STATUS  = array(102, 104,105); // approve , printed, approve by spv \
	/* @ pack : define its . ========================> */
	 $ON_ACCOUNT_STATUS = array('107'=>'PTP-POP','108'=>'PTP-BP');
	 $ACCOUNT_PAIDOFF = array('109'=>'PAID OFF');
	 $ON_STATUS_PROCESS = array('PROGRESS'); // approve , printed, approve by spv \
	 $ON_STATUS_LUNAS   = array('LUNAS'); // approve , printed, approve by spv \
	 $ON_STATUS_BATAL   = array('BATAL'); // approve , printed, approve by spv \
	/* @ pack : then selected all data like here . **/
	 $ON_ACCOUNT_NO = NULL;
	/* @ pack : then selected all data like here . **/
	 $this->db->reset_select();
	 $this->db->select("a.id as UpdateId, a.* ", false);
	 $this->db->from("t_gn_discount_trx a ");
	 $this->db->where_in("a.aggrement",$AGREE_STATUS);
	 $this->db->where_in("a.cpa_status", $ON_STATUS_PROCESS);
	 $this->db->where("a.approval_date IS NOT NULL", "", FALSE);
	 $this->db->where("( DATE(a.last_checked) < CURDATE() OR DATE(a.last_checked) IS NULL ) ", "", FALSE);
	 $qry = $this->db->get();
	//echo $this->db->last_query()."\n\r";
	/* set num of here =================================> */
	 $num = 0; 
	 if( $qry->num_rows()> 0 ) 
		foreach( $qry->result_assoc() as $rows ) 
	{
		if( isset($rows['deb_acctno']) )
		{
			$ON_ACCOUNT_NO[$rows['id']] = array ( 
			  'deb_acctno' => $rows['deb_acctno'], 
			  'deb_id' => $rows['deb_id'],
			  'agent_id' => $rows['agent_id'],
			  'tl_id' => $rows['tl'],
			  'spv_id' => $rows['spv'] 
			);
			
		}
	 }
	 //print_r($ON_ACCOUNT_NO);
	/* process from here  =================================> */ 
	 if( is_array($ON_ACCOUNT_NO) ) 
		foreach( $ON_ACCOUNT_NO as $UpdateId => $rows)
	 {
		 //echo $UpdateId;
		$this->db->reset_select(); // on default 
		$this->db->select("a.expired_date,a.totalpayment_byspv as pay_sync, curdate() as is_now, DATE(a.approval_date) AS approval_date",FALSE);
		$this->db->from("t_gn_discount_trx a ");
		// $this->db->where("a.deb_acctno",$rows['deb_acctno']);
		$this->db->where("a.id",$UpdateId);
		$qry = $this->db->get();
		//echo $this->db->last_query()."\n\r";
	 /* set on rows field **/
		$rows_field = null;
		if( $qry->num_rows() > 0 ){
			$rows_field = $qry->result_first_assoc();
		}
	 /* @pack : process from here  =================================> */ 
		$conds++;
		
		if( !is_null($rows_field) ) 
		{
			$expired_date  = $rows_field['expired_date'];
			$promise_pay   = $rows_field['pay_sync'];
			$process_date  = $rows_field['is_now'];
			$approval_date = $rows_field['approval_date'];	
		/* @ pack : then process its =================================>  */
			$total_payment = 0;
			$this->db->reset_select(); // on default 
			$this->db->select("SUM(a.pay_amount) as total_payment",FALSE);
			$this->db->from("t_gn_payment a");
			$this->db->where("a.deb_id", $rows['deb_id']);
			$this->db->where("DATE(a.pay_date)>='$approval_date'", "", FALSE);
			$this->db->where("DATE(a.pay_date)<='$expired_date'", "", FALSE);
			$this->db->group_by('a.deb_id');
			$tot_pay = $this->db->get();
			// echo $this->db->last_query()."\n\r";
			if( ( $tot_pay ) 
				AND ($tot_pay->num_rows()>0) )
			{
				$total_payment = (INT)$tot_pay->result_singgle_value();
				// echo "total payment = ".$total_payment."\n\r";
			}
		/* @ pack :  next step its  =============================================> */
			$on_process_update = FALSE;
	 // ===================> PTP - OP ========================> 
			if( ( $process_date <= $expired_date) 
				AND ( $total_payment >= $promise_pay ) )
			{
				//echo "PTP - OP (".end($ON_STATUS_LUNAS).") \n\r";
				//echo $process_date ."<=". $approval_date ."AND".$total_payment.">=". $promise_pay;
				//echo $UpdateId;
				$this->db->set('cpa_status', end($ON_STATUS_LUNAS));
				$this->db->set('last_checked', date('Y-m-d H:i:s'));				
				$on_accout_status = reset(array_keys($ACCOUNT_PAIDOFF)); // PAID OFF
				$on_process_update = true;
				
			}
	  // ===================> BATAL / PTP - BP ========================> 
			if( ( $process_date > $expired_date) 
				AND ( $total_payment <= $promise_pay ) )
			{
				//echo end($ON_STATUS_BATAL)."\n\r>";
				//echo $process_date .">". $approval_date ."AND".$total_payment."<=". $promise_pay;
				//echo $UpdateId;
					//			$data = array("cpa_status"=>end($ON_STATUS_BATAL));
					//			$where = "id =".$UpdateId;
				//$str = $this->db->update_string('t_gn_discount_trx', $data, $where);
				//echo $str;
				$this->db->set('cpa_status', end($ON_STATUS_BATAL));
				$this->db->set('last_checked', date('Y-m-d H:i:s'));
				//$this->db->where("id", $UpdateId);
				$on_accout_status = end(array_keys($ON_ACCOUNT_STATUS)); // PTP-BP
				$on_process_update = true;
			}
	// ===================> ON PROGRESS / ON PROGRESS / PTP - OP ========================> 
			if( ( $process_date < $expired_date) 
				AND ( ($total_payment ==0) OR ($total_payment < $promise_pay) ) )
			{
				//echo  end($ON_STATUS_PROCESS)."\n\r>";
				//echo $process_date ."<". $approval_date ."AND".$total_payment."== 0 OR ".$total_payment."<".$promise_pay;
				//echo $UpdateId;
				$this->db->set('cpa_status', end($ON_STATUS_PROCESS));
				$this->db->set('last_checked', date('Y-m-d H:i:s'));
				//$this->db->where("id", $UpdateId);
				$on_accout_status = reset(array_keys($ON_ACCOUNT_STATUS)); // PTP-POP 
				$on_process_update = true;
			}
			
 	
	// =================== On PROGRESS THEN ITS =========================================> 
			if( $on_process_update )
			{
				
				$prev_call_status = $Mbatch->_getPrev_call_status($rows['deb_id']);
				$this->db->where('id', $UpdateId);
				if( $this->db->update("t_gn_discount_trx") ) 
				{
					//echo $this->db->last_query();
					
					$this->db->set('deb_call_status_code', $on_accout_status);
					$this->db->where('deb_id', $rows['deb_id']);
					if( $this->db->update('t_gn_debitur') )
					{
						$this->db->set("CustomerId",$rows['deb_id']);
						$this->db->set("CallAccountStatus", $on_accout_status);
						$this->db->set("CallReasonId", $prev_call_status);
						$this->db->set("CreatedById",$rows['agent_id']);
						$this->db->set("TeamLeaderId",$rows['tl_id']);
						$this->db->set("SupervisorId",$rows['spv_id']);
						$this->db->set("CallHistoryNotes","POP-PROGRESS OF REQUEST DISCOUNT -Auto");
						$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s'));
						$this->db->insert("t_gn_callhistory");
						if( $this->db->affected_rows()> 0 ) { 
							$conds++;
						}	
					}
				}
			}
		}	
	 }
 return $conds;
	 
}
// END CLASS 

}

?>	 
 
