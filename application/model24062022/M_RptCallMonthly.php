<?php
class M_RptCallMonthly extends EUI_Model
{

var $config  = NULL;
/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
protected function set_config() 
{

// set config array select 

$this->config = array (
	array( "no"=>"1.&nbsp;", "title" => "In Progress", "code" => "tot_call_inprogress", "color" => "", "background"=>"", "call_rate_func" =>"rate_call_inprogress" ),
	array( "no"=>"2.&nbsp;", "title" => "Untouched","code" => "tot_untouched", "color" => "", "background"=>"", "call_rate_func" =>"rate_untouched"),
	array( "no"=>"3.&nbsp;", "title" => "Conducted/Attempt/Dialed( A + B + C)","code" => "tot_conducted_attempt_dialed", "color" => "", "background"=>"", "call_rate_func" =>"rate_conducted_attempt_dialed"),
	array( "no"=>"", "title" => "Total ( 1 + 2 + 3 )","code" => "tot_all_size", "color" => "", "background"=>""),
	array( "no"=>"A.&nbsp;", "title" => "Connected & Answered ( I + II ) - CLEAN DATA","code" => "tot_answer_clean_data", "color" => "", "background"=>"","call_rate_func" =>"rate_answer_clean_data",
			"child" => array
			(
				array("no"=>"I", "title" => "Contacted ( Ia + Ib )", "code" => "tot_call_contacted", "call_rate_func"=>"rate_call_contacted",
					  "child" => array (
					    array( "no"=>"I.a", "title" => "Surveyed", "code" => "tot_call_surveys", "color"=>"", "background" => "", "call_rate_func" =>"rate_call_surveys" ),
					    array( "no"=>"I.b", "title" => "No Interest to Talk", "code" => "tot_call_not_interest_talk", "color"=>"", "background" => "", "call_rate_func" =>"rate_call_not_interest_talk")
					)),
				array( "no"=>"II", "title" => "Non Contacted","code"=>"tot_non_contacted", "color"=>"", "background" => "", "call_rate_func" =>"rate_non_contacted")	
			)
		),
	array( "no"=>"B.&nbsp;", "title" => "Connected & Not Answered", "code" => "tot_connected_and_notanaswer", "color" => "","background"=>"",  "call_rate_func" =>"rate_connected_and_notanaswer"),
	array( "no"=>"C.&nbsp;", "title" => "Not Connected - Unclean Data","code" => "tot_connected_unclean_data", "color" => "", "background"=>"","call_rate_func" =>"rate_connected_unclean_data", 
			"child" => array(
					array( "no"=>"1", "title" => "Busy", "code" =>"tot_call_busy", "color"=>"", "background" => "",  "call_rate_func" =>"rate_call_busy"),
					array( "no"=>"2", "title" => "Dead Tone", "code"=>"tot_call_deadtone",  "color"=>"", "background" => "",   "call_rate_func" =>"rate_call_deadtone"),
					array( "no"=>"3", "title" => "Left Message/Voice Mail", "code"=>"tot_call_leftmesage", "color"=>"", "background" => "", "call_rate_func" =>"rate_call_leftmesage"),
					array( "no"=>"4", "title" => "Return List", "color"=>"", "code" =>"tot_call_returnlist", "background" => "", "call_rate_func" =>"rate_call_returnlist"),
					array( "no"=>"5", "title" => "Wrong Number", "color"=>"", "code"=>"tot_call_wrongnumber", "background" => "", "call_rate_func" =>"rate_call_wrongnumber") 
				)),
	array( "no"=>"D.&nbsp;", "title" => "In Progress","code" => "tot_call_inprogress", "color" => "", "background"=>"", "call_rate_func" =>"rate_call_inprogress",
				"child" => array(
					array( "no"=>"1", "title" => "Busy","code" =>"tot_inpr_call_busy", "color"=>"", "background" => "", "call_rate_func" =>"rate_inpr_call_busy"),
					array( "no"=>"2", "title" => "Call Back Later", "code"=>"tot_inpr_call_back_later", "color"=>"", "background" => "", "call_rate_func" =>"rate_inpr_call_back_later"),
					array( "no"=>"3", "title" => "Can Not Be Reached", "code"=>"tot_inpr_call_not_bereach", "color"=>"", "background" => "", "call_rate_func" =>"rate_inpr_call_not_bereach"),
					array( "no"=>"4", "title" => "Dead Tone", "code"=>"tot_inpr_call_deadtone", "color"=>"", "background" => "", "call_rate_func" =>"rate_inpr_call_deadtone"),
					array( "no"=>"5", "title" => "Left Message/Voice Mail", "code"=>"tot_inpr_call_leftmesage", "color"=>"", "background" => "", "call_rate_func" =>"rate_inpr_call_leftmesage"),
					array( "no"=>"6", "title" => "No Answer", "color"=>"", "code"=>"tot_inpr_call_not_answer",  "background" => "", "call_rate_func" =>"rate_inpr_call_not_answer"),
					array( "no"=>"7", "title" => "Wrong Number", "color"=>"", "code"=>"tot_inpr_call_wrongnumber",  "background" => "", "call_rate_func" =>"rate_inpr_call_wrongnumber") 
				)),
	array( "no"=>"", "title" => "Contacted Ratio (Successful Contacted to All data) ","code" => "tot_contact_ratio", "color" => "", "background"=>"", "call_user_func" =>"percent_data"),		
	array( "no"=>"", "title" => "Successful Surveyed to Contacted rate","code" => "", "color" => "", "code"=>"tot_success_survey", "background"=>"", "call_user_func" =>"percent_data")
 );
	
}
 
 
/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
public function _get_config() {
	return $this->config;
}
 
/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
public function M_RptCallMonthly() 
{ 
	$this->set_config();
}

/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 		
public static function & get_instance() 
{
	if( is_null(self::$instance))  {
		self::$instance = new self();
	}
	return self::$instance;
}

/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
 
public function _getCampaignByWorkProject()
{


$CampaignId = array();
$this->db->reset_select();
$this->db->select(" a.CampaignId, b.CampaignDesc, b.CampaignCode ", FALSE );
$this->db->from("t_gn_campaign_project a ");
$this->db->join("t_gn_campaign b "," a.CampaignId=b.CampaignId","LEFT");
$this->db->where_in("a.ProjectId", array(1));

// if have post campaignid // 

if( $this->URI->_get_have_post('CampaignId')) {
	$this->db->where_in("a.CampaignId", $this->URI->_get_array_post('CampaignId') );
}

$rec = $this->db->get();

if($rec->num_rows() > 0 )
	foreach( $rec->result_assoc() as $rows )
{
	$CampaignId[$rows['CampaignId']]['code']= $rows['CampaignCode'];
	$CampaignId[$rows['CampaignId']]['name']= $rows['CampaignDesc'];
}

return $CampaignId;

}


/*
 * @ def  : source control form HTML Surce data 
 * --------------------------------------------------
 *
 * @ param : start_date 
 * @ param : end_date 
 *
 */
 
public function _getWorkProjectAnswers()
{
	$_works = array();
	$this->db->select('a.AnswerCode');
	$this->db->from('t_gn_project_answers a');
	$this->db->where('a.ProjectId', 1);
	$this->db->group_by('a.AnswerCode');
	$this->db->order_by('a.AnswerNo','ASC');
	
	foreach( $this->db->get()->result_assoc() as $rows  )  {
		$_works[$rows['AnswerCode']] = $rows['AnswerCode'];
	}
	return $_works;
}


}
?>