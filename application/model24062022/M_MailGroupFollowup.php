<?php
class M_MailGroupFollowup extends EUI_Model
{

/* M_MailGroupFollowup **/

public function M_MailGroupFollowup(){ }
 
/* M_MailGroupFollowup */
 
public function _get_default()
{
	
 // set pages 
 
 $this->EUI_Page->_setPage(10); 
 $this->EUI_Page->_setSelect("a.FuEMaiId");
 $this->EUI_Page->_setFrom(" t_lk_followup_mailgroup a");
 $this->EUI_Page->_setJoin("t_lk_followupgroup b ", "a.FuEMailFollowupCode=b.FuGroupCode","LEFT", TRUE);
 $this->EUI_Page->_setWhereIn('a.FuEMailProjectId',$this->EUI_Session->_get_session('ProjectId'));// project wcall 
 
  // filter 
 if( $this->URI->_get_have_post('keywords') )
 {
	$this->EUI_Page->_setAnd(" ( 
			a.FuEMaiId LIKE '%{$this->URI->_get_post('keywords')}%'
			OR b.FuGroupDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailFollowupCode LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEmailToCc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailAddr LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailDesc LIKE '%{$this->URI->_get_post('keywords')}%'
		)");
 }
 
 if($this -> EUI_Page -> _get_query() ) {
   return $this -> EUI_Page;
  }
  
}	

// _get_content
public function _get_content() 
{
  $this->EUI_Page->_postPage((INT)$this -> URI -> _get_post('v_page'));
  $this->EUI_Page->_setPage(10); 
  $this->EUI_Page->_setArraySelect(array(
	'a.FuEMaiId AS FuEMaiId' => array('FuEMaiId', 'ID', 'Primary'),
	'a.FuEMailFollowupCode AS FuEMailFollowupCode' => array('FuEMailFollowupCode','* Group Code'),
	'b.FuGroupDesc AS FuGroupDesc' => array('FuGroupDesc','* Group Name'),
	'a.FuEmailToCc AS FuEmailToCc' => array('FuEmailToCc','* Ref'),
	'a.FuEMailAddr AS FuEMailAddr' => array('FuEMailAddr','* Address'),
	'a.FuEMailDesc AS FuEMailDesc' => array('FuEMailDesc','Description'),
	'a.FuEMailCreatedTs AS FuEMailCreatedTs' => array('FuEMailCreatedTs','Create Date'),
	'IF( a.FuEMailShow="Y","Active","Not Active") AS IsActive'=> array('IsActive','Status') 
 ));
 
 $this->EUI_Page->_setFrom("t_lk_followup_mailgroup a ");
 $this->EUI_Page->_setJoin("t_lk_followupgroup b ", "a.FuEMailFollowupCode=b.FuGroupCode","LEFT", TRUE);
 $this->EUI_Page->_setWhereIn('a.FuEMailProjectId',$this->EUI_Session->_get_session('ProjectId'));
 
 // filter 
 if( $this->URI->_get_have_post('keywords') )
 {
	$this->EUI_Page->_setAnd(" ( 
			a.FuEMaiId LIKE '%{$this->URI->_get_post('keywords')}%'
			OR b.FuGroupDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailFollowupCode LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEmailToCc LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailAddr LIKE '%{$this->URI->_get_post('keywords')}%'
			OR a.FuEMailDesc LIKE '%{$this->URI->_get_post('keywords')}%'
			
		)");
 }
 
 
 if( $this->URI->_get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
  }	
  else{
	$this->EUI_Page->_setOrderBy("a.FuEMailCreatedTs","DESC");
  }
  //echo $this->EUI_Page->_getCompiler();
  $this -> EUI_Page ->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_resource()
{
  self::_get_content();
  if( $this->EUI_Page->_get_query()!='') {
	return $this -> EUI_Page -> _result();
  }
 
}
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 
/* get group followup Under Project **/

public function _getFollowGroupCode()
{
  $work_code = array();
  
  $this->db->select("a.FuGroupCode, a.FuGroupDesc");
  $this->db->from("t_lk_followupgroup a ");
  $this->db->where_in("a.FuProjectId", $this->EUI_Session->_get_session('ProjectId'));
  $res = $this->db->get();
  
  if($res->num_rows()>0 ) foreach( $res->result_assoc() as $rows ) 
  {
	$work_code[$rows['FuGroupCode']] = "{$rows['FuGroupCode']} - {$rows['FuGroupDesc']}";
  }
  
  return $work_code;

}

/** getIDByCode **/
private function getIDByCode( $code = null )
{
	$ID = 0;
	if(!is_null($code)) 
	{
		$this->db->reset_select();
		$this->db->select("a.FuGroupId",FALSE);
		$this->db->from("t_lk_followupgroup a ");
		$this->db->where("a.FuGroupCode", $code);
		//echo $this->db->_get_var_dump();
		
		$rs = $this->db->get();
		if( $rs->num_rows()>0 AND $rows=$rs->result_first_assoc() )
		{
			$ID = $rows['FuGroupId'];
		}
	}
	
	return $ID;
}

/* _setSaveAddMailGroup **/

public function _setSaveAddMailGroup()
{
	$conds = 0;
	
	$params = $this->URI->_get_all_request();
	
	if(is_array($params))
	 foreach( $params as $field => $values )
	{
	  $this->db->set($field, $values);
	  if( $field=='FuEMailFollowupCode' )
	  {
		$ID = self::getIDByCode($values);
		if( $ID )
		{
			$this->db->set('FUEMailFollowupId', $ID);
		}
	  }
	}
	
	$this->db->set('FuEMailCreatedBy',$this->EUI_Session->_get_session('UserId'));
	$this->db->set('FuEMailCreatedTs',date('Y-m-d H:i:s'));
	$this->db->insert("t_lk_followup_mailgroup");
	
	if( $this->db->affected_rows() > 0  ) {
		$conds++;
	}
	
	return $conds;
	
}

/* _setUpdateAddMailGroup **/

public function _setUpdateAddMailGroup()
{
	$conds = 0;
	
	$params = $this->URI->_get_all_request();
	
	if(is_array($params))
	 foreach( $params as $field => $values ) 
	 {
	   if( $field!='FuEMaiId' )
			$this->db->set($field, $values);
	   else
			$this->db->where($field, $values);	
	}
	
	$this->db->set('FuEMailUpdatedBy',$this->EUI_Session->_get_session('UserId'));
	$this->db->set('FuEMaiUpdatedTs',date('Y-m-d H:i:s'));
	$this->db->update("t_lk_followup_mailgroup");
	
	if( $this->db->affected_rows() > 0  ) {
		$conds++;
	}
	
	return $conds;
	
}

/* _setDeleteMailGroup **/

public function _setDeleteMailGroup()
{

 $conds = 0;
 $params = $this->URI->_get_all_request();
 if(is_array($params))
	foreach( $params as $field => $values )
 {
	$this->db->where($field, $values);
 }
 
 $this->db->delete("t_lk_followup_mailgroup");
 if( $this->db->affected_rows()>0) {	
	$conds++;
 }
 
 return $conds;
}

/* _getFollowData **/

public function _getFollowData() 
{
 $work_data = array();
 
	$this->db->select('*');
	$this->db->from('t_lk_followup_mailgroup a ');
	$this->db->where('a.FuEMaiId', $this->URI->_get_post('FuEMaiId'));
	if( $rows = $this->db->get() )
	{
		foreach( $rows->result_first_assoc() as $field => $values ) {
			$work_data[$field] = $values;
		}
	}
	
	return $work_data;
}

 
}


?>