<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_ModApprRnd extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_ModApprRnd() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser','M_SetCampaign','M_ModDistribusi'));
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.claim_id");

	$this->EUI_Page->_setFrom("t_gn_claim_debitur a");
	$this->EUI_Page->_setJoin("t_gn_buckettrx_debitur b","a.bucket_trx_id=b.bucket_trx_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_debitur c","b.deb_id=c.deb_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign d","c.deb_cmpaign_id=d.CampaignId","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent e","a.claim_by=e.UserId","INNER");
	$this->EUI_Page->_setJoin("t_gn_assignment f","c.deb_id=f.CustomerId","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent g","f.AssignSelerId=g.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_lk_aprove_status h","a.approval_status=h.AproveCode","LEFT",TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('a.approval_status','104');
 	
	// $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	 
	 
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('c.deb_acct_no', 'claim_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_cmpaign_id', 'claim_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('c.deb_call_status_code', 'claim_account_status', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_call_status_code', 'claim_call_status', TRUE);
	// $this->EUI_Page->_setAndCache('ag.UserId', 'claim_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_resource', 'claim_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'claim_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("c.deb_call_activity_datets>='". _getDateEnglish(_get_post('claim_start_date')) ."'", 'claim_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("c.deb_call_activity_datets<='". _getDateEnglish(_get_post('claim_end_date')) ."'", 'claim_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("c.deb_amount_wo>=". (INT)_get_post('claim_start_amountwo') ."", 'claim_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("c.deb_amount_wo<=". (INT)_get_post('claim_end_amountwo')."", 'keep_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	// $this->EUI_Page->_setGroupBy('c.deb_id');
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
	/** default of query ***/
	$this->EUI_Page->_setArraySelect(array(
		"a.claim_id AS ClaimId"=> array('ClaimId','ID','primary'),
		"b.deb_id AS CustomerId"=> array('CustomerId','CustomerId','hidden'),
		"d.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Product'),
		"c.deb_acct_no AS AccountNumber "=> array('AccountNumber','Customer ID'),
		"c.deb_name AS CustomerName"=> array('CustomerName','Customer Name'),
		"e.id AS AgentClaim"=>array('AgentClaim','Agent Claim'),
		"g.id AS OldAgent"=>array('OldAgent','Old Agent'),
		"a.claim_date_ts AS DateClaim"=>array('DateClaim','Date Claim'),
		"c.deb_reminder AS History "=> array('History','History'),
		"h.AproveName AS ApprovalStatus"=>array('ApprovalStatus','Approval Status')
	));
	
	$this->EUI_Page->_setFrom("t_gn_claim_debitur a");
	$this->EUI_Page->_setJoin("t_gn_buckettrx_debitur b","a.bucket_trx_id=b.bucket_trx_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_debitur c","b.deb_id=c.deb_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign d","c.deb_cmpaign_id=d.CampaignId","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent e","a.claim_by=e.UserId","INNER");
	$this->EUI_Page->_setJoin("t_gn_assignment f","c.deb_id=f.CustomerId","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent g","f.AssignSelerId=g.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_lk_aprove_status h","a.approval_status=h.AproveCode","LEFT",TRUE);

	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('a.approval_status','104');
 	
	// $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	 
	 
// @ pack : set cache on page **/
	
	// $this->EUI_Page->_setAndCache('c.deb_acct_no', 'claim_cust_id', TRUE);
	// $this->EUI_Page->_setAndCache('c.deb_cmpaign_id', 'claim_campaign_id',TRUE);
	// $this->EUI_Page->_setAndCache('c.deb_call_status_code', 'claim_account_status', TRUE);
	// $this->EUI_Page->_setAndCache('c.deb_call_status_code', 'claim_call_status', TRUE);
	// $this->EUI_Page->_setLikeCache('c.deb_resource', 'claim_recsource', TRUE);
	// $this->EUI_Page->_setLikeCache('c.deb_name', 'claim_cust_name', TRUE);
	// $this->EUI_Page->_setAndCache('ag.UserId', 'claim_agent_id', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	// $this->EUI_Page->_setAndOrCache("c.deb_call_activity_datets>='". _getDateEnglish(_get_post('claim_start_date')) ."'", 'claim_start_date', TRUE);
	// $this->EUI_Page->_setAndOrCache("c.deb_call_activity_datets<='". _getDateEnglish(_get_post('claim_end_date')) ."'", 'claim_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	// $this->EUI_Page->_setAndOrCache("c.deb_amount_wo>=". (INT)_get_post('claim_start_amountwo') ."", 'claim_start_amountwo', TRUE);
	// $this->EUI_Page->_setAndOrCache("c.deb_amount_wo<=". (INT)_get_post('claim_end_amountwo')."", 'keep_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	// $this->EUI_Page->_setGroupBy('c.deb_id');
		
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) 
	{
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}
	else
	{
		$this->EUI_Page->_setOrderBy('a.claim_date_ts','DESC');
	}
	// echo $this ->EUI_Page->_getCompiler();
	$this->EUI_Page->_setLimit();
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
	
	/*
	 * @ def 		: update_approval_status //  
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition approveRandom 
	 * @ return 	: void(0)
	 */
	public function approveRandom()
	{
		$_conds['Message']= 1;
		$_conds['success'] = 0;
		$_conds['fail']=0;
		
		$ClaimId = $this ->URI->_get_array_post('ApprovalId');
		$this->db->set( "approval_status", "101" );
		$this->db->set( "approval_by", $this -> EUI_Session -> _get_session('UserId') );
		$this->db->set( "approval_date_ts", date('Y-m-d H:i:s') );
		if( is_array($ClaimId) ) {
			$this->db->where_in("claim_id", $ClaimId);
		} else {
			$this->db->where_in("claim_id", array($ClaimId));
		}
		$this->db->where("approval_status","104");
		$this->db->update("t_gn_claim_debitur");
		// echo $this->db->last_query();
		if( $this->db->affected_rows()>0 )
		{
			$detailClaim = $this->getClaimDetail();
			if( count($detailClaim) > 0 )
			{
				foreach($detailClaim as $claimId => $detail )
				{
					$this->db->set("AssignSelerId",$detail['AgentId']);
					$this->db->set("AssignLeader",$detail['tl_id']);
					$this->db->set("AssignMode",'CLM');
					$this->db->set("AssignDate",date('Y-m-d H:i:s'));
					$this->db->where( "AssignId", $detail['AssignId'] );
					$this->db->update("t_gn_assignment");
					if( $this->db->affected_rows()>0 )
					{
						if($this->M_ModDistribusi->_setSaveLog( array( 
							'AssignId' => $detail['AssignId'], 
							'UserId' => $detail['AgentId'], 
							'tl_id' => $detail['tl_id'], 
							'assign_status'=>'CLM'
						 ),'ASSIGN.CLAIM' )) 
						{
							$this->db->set( "deb_agent", $detail['AgentCode'] );
							$this->db->set( "deb_is_access_all", "0" );
							$this->db->where( "deb_id", $detail['DebiturId'] );
							$this->db->update("t_gn_debitur");
							
							if( $this->db->affected_rows()>0 )
							{
								$_conds['success']++;
							}
							else 
							{
								$_conds['fail']++;
							}
						}
					}
					else
					{
						$this->db->set("deb_is_access_all","0");
						$this->db->where("deb_id",$detail['DebiturId']);
						$this->db->update("t_gn_debitur");
						if( $this->db->affected_rows()>0 )
						{
							$_conds['success']++;
						}
						else 
						{
							$_conds['fail']++;
						}
					}
				}
			}
		}
		
		// if( count($detailClaim) > 0 )
		// {
			// foreach($detailClaim as $claimId => $detail )
			// {
				// $this->db->set( "approval_status", "101" );
				// $this->db->set( "approval_by", $this -> EUI_Session -> _get_session('UserId') );
				// $this->db->set( "approval_date_ts", date('Y-m-d H:i:s') );
				// $this->db->where("claim_id",$claimId);
				// $this->db->update("t_gn_claim_debitur");
				// if( $this->db->affected_rows()>0 )
				// {
					// $this->db->set("AssignSelerId",$detail['AgentId']);
					// $this->db->set("AssignLeader",$detail['tl_id']);
					// $this->db->set("AssignMode",'CLM');
					// $this->db->set("AssignDate",date('Y-m-d H:i:s'));
					// $this->db->where( "AssignId", $detail['AssignId'] );
					// $this->db->update("t_gn_assignment");
					// if( $this->db->affected_rows()>0 )
					// {
						
						// if($this->M_ModDistribusi->_setSaveLog( array( 
							// 'AssignId' => $detail['AssignId'], 
							// 'UserId' => $detail['AgentId'], 
							// 'tl_id' => $detail['tl_id'], 
							// 'assign_status'=>'CLM'
						 // ),'ASSIGN.CLAIM' )) 
						// {
							// $this->db->set( "deb_agent", $detail['AgentCode'] );
							// $this->db->set( "deb_is_access_all", "0" );
							// $this->db->where( "deb_id", $detail['DebiturId'] );
							// $this->db->update("t_gn_debitur");
							
							// if( $this->db->affected_rows()>0 )
							// {
								// $_conds['success']++;
							// }
							// else 
							// {
								// $_conds['fail']++;
							// }
						// }
					// }
					// else
					// {
						// $this->db->set("deb_is_access_all","0");
						// $this->db->where("deb_id",$detail['DebiturId']);
						// $this->db->update("t_gn_debitur");
						// if( $this->db->affected_rows()>0 )
						// {
							// $_conds['success']++;
						// }
						// else 
						// {
							// $_conds['fail']++;
						// }
					// }
				// }
				
			// }
			
		// }
		return $_conds;
	}
	
	/*
	 * @ def 		: update_approval_status //  
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition detail claim 
	 * @ return 	: void(0)
	 */
	 public function rejectRandom()
	{
		$_conds['Message']= 1;
		$_conds['success'] = 0;
		$_conds['fail']=0;
		
		$ClaimId = $this ->URI->_get_array_post('ApprovalId');
		$this->db->set( "approval_status", "102" );
		$this->db->set( "approval_by", $this -> EUI_Session -> _get_session('UserId') );
		$this->db->set( "approval_date_ts", date('Y-m-d H:i:s') );
		if( is_array($ClaimId) ) {
			$this->db->where_in("claim_id", $ClaimId);
		} else {
			$this->db->where_in("claim_id", array($ClaimId));
		}
		$this->db->where("approval_status","104");
		$this->db->update("t_gn_claim_debitur");
		if( $this->db->affected_rows()>0 )
		{
			$detailClaim = $this->getClaimDetail();
			if( count($detailClaim) > 0 )
			{
				foreach($detailClaim as $claimId => $detail )
				{
					$this->db->set( "claim_flag", "0" );
					$this->db->where("bucket_trx_id",$detail['bucket_trx_id']);
					$this->db->update("t_gn_buckettrx_debitur");
					if( $this->db->affected_rows()>0 )
					{
						$_conds['success']++;
					}
					else 
					{
						$_conds['fail']++;
					}
				}
			}
		}
		// $detailClaim = $this->getClaimDetail();
		// if( count($detailClaim) > 0 )
		// {
			// foreach($detailClaim as $claimId => $detail )
			// {
				// $this->db->set( "approval_status", "102" );
				// $this->db->set( "approval_by", $this -> EUI_Session -> _get_session('UserId') );
				// $this->db->set( "approval_date_ts", date('Y-m-d H:i:s') );
				// $this->db->where("claim_id",$claimId);
				// $this->db->update("t_gn_claim_debitur");
				// if( $this->db->affected_rows()>0 )
				// {
					// $this->db->set( "claim_flag", "0" );
					// $this->db->where("bucket_trx_id",$detail['bucket_trx_id']);
					// $this->db->update("t_gn_buckettrx_debitur");
					// if( $this->db->affected_rows()>0 )
					// {
						// $_conds['success']++;
					// }
					// else 
					// {
						// $_conds['fail']++;
					// }
				// }
			// }
		// }
		
		return $_conds;
	}
	 
	/*
	 * @ def 		: update_approval_status //  
	 * -----------------------------------------
	 *
	 * @ params  	: post & definition detail claim 
	 * @ return 	: void(0)
	 */
	private function getClaimDetail()
	{
		$data = array();
		$ClaimId = $this ->URI->_get_array_post('ApprovalId');
		
		$this->db->reset_select();
		$this->db->select("	a.claim_id as Claimid,
							c.bucket_trx_id,
							a.claim_by as AgentId,
							b.id as AgentCode,
							b.tl_id AS tl_id,
							c.deb_id,
							d.AssignId");
		$this->db->from("t_gn_claim_debitur a");
		$this->db->join('t_tx_agent b', 'a.claim_by=b.UserId', 'INNER');
		$this->db->join('t_gn_buckettrx_debitur c', 'a.bucket_trx_id=c.bucket_trx_id', 'INNER');
		$this->db->join('t_gn_assignment d', 'c.deb_id=d.CustomerId', 'INNER');
		
		if( !is_null($ClaimId) )
		{	
			if( is_array($ClaimId) ) {
				$this->db->where_in("a.claim_id", $ClaimId);
			} else {
				$this->db->where_in("a.claim_id", array($ClaimId));
			}	
		}
		 
		$qry = $this->db->get();
		if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) 
		{
			$data[$rows['Claimid']]['AgentId'] = $rows['AgentId'];
			$data[$rows['Claimid']]['tl_id'] = $rows['tl_id'];
			$data[$rows['Claimid']]['AgentCode'] = $rows['AgentCode'];
			$data[$rows['Claimid']]['DebiturId'] = $rows['deb_id'];
			$data[$rows['Claimid']]['AssignId'] = $rows['AssignId'];
			$data[$rows['Claimid']]['bucket_trx_id'] = $rows['bucket_trx_id'];
		}
		return $data;
	}

}

// END OF CLASS 



?>