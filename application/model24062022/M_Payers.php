<?php
class M_Payers extends EUI_Model
{

 
/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 	
 private function _getPayerField() 
 {
	return $this->db->list_fields('t_gn_payer');
 }
 
 
/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
private function _getFilterDate() 
{
	$filters = array('PayerDOB' => 'PayerDOB' );
	return $filters;
}

/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */

private function _getFilterField() 
{
	$filters = array(
		'PayerIdentificationTypeId' => 'IdentificationTypeId',
		'PayerGenderId' => 'GenderId',
		'PremiumGroupId' => 'PremiumGroupId',
		'PayerSalutationId' => 'SalutationId',
		'PayerProvinceId' =>'ProvinceId',
		'PayerOfficePhoneNum' => array('PayerOfficePhoneNum','PayerWorkPhoneNum'),
		'CreditCardTypeId' => array('PaymentTypeId','CreditCardTypeId'),
		'OptOut' => 'PayerOptOut'
	);
	
	return $filters;
}

/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
public function _getCountPayer($CustomerId=0)
{
	$_conds = 0;
	
	$sql = " select count(a.PayerId) as Qty from t_gn_payer a where a.CustomerId= '{$CustomerId}' ";
	$qry = $this -> db->query($sql);
	if( $rows = $qry -> result_first_assoc() ) {
		$_conds =(INT)$rows['Qty']; 
	}
	
	return $_conds;
}




/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
private function _getPayerExist($CustomerId=0)
{
	$_payers = array();
	
	$sql = " select * from t_gn_payer a where a.CustomerId= '{$CustomerId}' ";
	$qry = $this -> db->query($sql);
	foreach( $qry -> result_first_assoc() as $key => $values ){
		$_payers[$key] = $values; 
	}
	
	return $_payers;
}


/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
private function _getPayerNotExist($CustomerId=0)
{
	$_payers = array();
	
	$this->db->select('*');
	$this->db->from('t_gn_debitur a');
	$this->db->where('a.CustomerId',$CustomerId);
	
	if( $rows = $this -> db->get()->result_first_assoc() )
	{
		$_payers['SalutationId'] = $rows['SalutationId'];
		$_payers['PayerFirstName'] = $rows['CustomerFirstName'];
		$_payers['PayerLastName'] = $rows['CustomerLastName'];
		$_payers['GenderId'] = $rows['GenderId'];
		$_payers['PayerDOB'] = $rows['CustomerDOB'];
		$_payers['PayerAddressLine1'] = $rows['CustomerAddressLine1'];
		$_payers['PayerAddressLine2'] = $rows['CustomerAddressLine2'];
		$_payers['PayerAddressLine3'] = $rows['CustomerAddressLine3'];
		$_payers['PayerAddressLine4'] = $rows['CustomerAddressLine4'];
		$_payers['IdentificationTypeId'] = $rows['IdentificationTypeId'];
		$_payers['PayerIdentificationNum'] = $rows['CustomerIdentificationNum'];
		$_payers['PayerMobilePhoneNum'] = $rows['CustomerMobilePhoneNum'];
		$_payers['PayerMobilePhoneNum2'] = $rows['CustomerMobilePhoneNum2'];
		$_payers['PayerCity'] = $rows['CustomerCity'];
		$_payers['PayerHomePhoneNum'] = $rows['CustomerHomePhoneNum'];
		$_payers['PayerHomePhoneNum2'] = $rows['CustomerHomePhoneNum2'];
		$_payers['PayerZipCode'] = $rows['CustomerZipCode'];
		$_payers['PayerOfficePhoneNum'] = $rows['CustomerWorkPhoneNum'];
		$_payers['PayerOfficePhoneNum2'] = $rows['CustomerWorkPhoneNum2'];
		$_payers['PayerProvinceId'] = $rows['ProvinceId'];
		$_payers['CreditCardTypeId'] = $rows['CardTypeId'];
		$_payers['PayerEmail'] = $rows['CustomerEmail'];
		$_payers['PayerCreditCardNum'] = ($rows['CustomerCreditCardNum'] ? $rows['CustomerCreditCardNum'] : '');
		$_payers['PayerFaxNum'] = '';
		$_payers['PayerAddrType'] = '1';
		$_payers['PayerAge'] = 0;
		$_payers['PayerCreditCardExpDate'] = $rows['CustomerCreditCardExpDate'];
		$_payers['PayersBankId'] = '0';
	}	
	
	return $_payers;
}




/*
 * @ def    : get payer data from customer table not in transaction
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
 function _getAddPayers($CustomerId=0)
 {
	$Qty = $this ->_getCountPayer($CustomerId);
	$_payers = array();
	if( $Qty !=FALSE )
		$_payers = $this -> _getPayerExist($CustomerId);
	else
		$_payers = $this -> _getPayerNotExist($CustomerId);
		
	return $_payers;
 }
 
/*
 * @ def    : save data payers in sale
 * ------------------------------------------------------
 *
 * @ param  : $form  
 * @ akses  : public 
 * @ author : razaki team
 */
 
 function _SaveDataPayers( $_payers = null )
 {
	$_conds = 0;
	$clear_leaves = array();
  
  // filter map with parameter 
  
	$filters = $this->_getFilterField();
	if(is_array($_payers))foreach( $_payers as $ky => $fs ) 
	{
		if( in_array($ky, array_keys($filters)) )
		{
			if( is_array($filters[$ky]) )
			{
				foreach( $filters[$ky] as $fld => $fld_values )
				{
					$clear_leaves[$fld_values] = $fs; 
				}
			}
			else{
				$clear_leaves[$filters[$ky]] = $fs; 
			}
		}	
		else {
			$filter_dates = $this -> _getFilterDate();
			if( in_array($ky,array_keys($filter_dates)))
				$clear_leaves[$ky] = $this -> EUI_Tools -> _date_english($fs);
			else {
				$clear_leaves[$ky] = $fs;
			}	
		}
	}
	
	// filter map with columns 
	
	$fields = $this->_getPayerField();
	
	if(is_array($clear_leaves) ) foreach( $fields as $key => $values ) 
	{
		if( in_array( $values, array_keys($clear_leaves))) 
		{
			if( !empty($clear_leaves[$values]) ){
				$this -> db -> set($values, $clear_leaves[$values]);		
			}
		}
	}
	
	$this -> db -> insert('t_gn_payer');
	if( $this -> db ->affected_rows() > 0 ){
		$_conds++;
	}
	else 
	{
		// update if dulicate 
		if(is_array($clear_leaves) ) foreach( $fields as $key => $values ) 
		{
			if( in_array( $values, array_keys($clear_leaves))) 
			{
				if( !empty($clear_leaves[$values]) ){
					$this -> db -> set($values, $clear_leaves[$values]);		
				}
			}
		}
		
		$this->db->where('CustomerId',$clear_leaves['CustomerId']);
		$this->db->update('t_gn_payer');
		$_conds++;
	}
	
	return $_conds;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 public function _getPayerReady($where = array() )
 {
	$_payers = array();
	
    if( is_array($where)) $this -> db -> where($where);
	else {
		$this -> db -> where('CustomerId', $where);
	}
	
	$this -> db -> select('*');
	$this -> db -> from('t_gn_payer a');
	if( $rows_result_value = $this -> db -> get()-> result_first_assoc() )
	{
		$_payers = $rows_result_value;
	}
	
	return $_payers;

 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 public function _getPayersInformation($where = array() )
 {
	$_payers = array();
	
	$this->db->select(" 
				b.PolicyNumber, a.PolicySalesDate,  
				a.PolicyEffectiveDate, c.ProductName, 
				e.CampaignName,f.full_name" );
				
	$this->db->from('t_gn_policy a');
	$this->db->join("t_gn_policyautogen b ", "a.PolicyNumber=b.PolicyNumber","LEFT");
	$this->db->join("t_gn_product c "," b.ProductId=c.ProductId","LEFT");
	$this->db->join("t_gn_debitur d "," b.CustomerId=d.CustomerId","LEFT");
	$this->db->join("t_gn_campaign e "," d.CampaignId=e.CampaignId","LEFT");
	$this->db->join("t_tx_agent f "," d.SellerId=f.UserId","LEFT");
	
    if( is_array($where)) $this -> db -> where($where);
	else {
		$this -> db -> where('b.CustomerId', $where);
	}
	if( $rows_result_value = $this -> db -> get()-> result_first_assoc() )
	{
		$_payers = $rows_result_value;
	}
	
	return $_payers;

 }
 
 
}