<?php
/*
 * @ sunject : M_BatchApplication
 * ------------------------------------------------------------------------
 *
 * @ pack 	 : #
 *	 all model for bacth class cronjob for easy maintenace 
 *   please create on here . then run with sh file on ../batch/...
 *	 thanks veri much 
 *
 */

 class M_BatchApplication extends EUI_Model
{

/*
 * @ pack : instance 
 */
 
private static $Instance = null;
 
/*
 * @ pack : instance 
 */
 
 public static function &Instance()
{
 
 if(is_null(self::$Instance) ){
	self::$Instance = new self();
 }
 return self::$Instance;
 
} 
 
/*
 * @ pack : aksesor 
 */
 
 public function M_BatchApplication()
{
	$this->load->model(
	  array(
		'M_Configuration','M_SetLockMonitoring','M_Loger',
		'M_BatchClearLog', 'M_BatchUpdateDiscount','M_BatchBrokenPromise',
		'M_BatchPOP','M_BatchExcelDumper','M_BatchAssign5Day',
		'M_BatchResetApproval','M_BatchCompileSdr','M_BatchExcelDeleted','M_MgtRandDeb',
		'M_BatchLogInventory'
		)
	);
	
  // run with default aksesor 
}

/*
 * @ pack : _SetAgentAbsensi with parameter on argv cronjob 
	 willl cek on config if true will run if not true 
	 stop Its .		
 */
 
 public function _SetAgentAbsensi()
{
 
// @ pack : set callback ----------------------------->

 $conds = 0;
 
// @ pack : $class ---------------------------------->

 $ClsConf=& M_Configuration::get_instance(); 
 $ClsLock=& M_SetLockMonitoring::Instance();
 $Auth = $ClsConf->_get_auth_rule_modul();

 $USER_LOGIN_CHECK = (INT)$Auth['USER_LOGIN_CHECK'];
 
 // pack : $Auth ---------------------------------> 
 
  if(is_array($Auth) AND $USER_LOGIN_CHECK > 0 )
 {
	$this->db->select("UserId",FALSE);
	$this->db->from("t_tx_agent");
	$this->db->where("t_tx_agent.UserId NOT IN(
			SELECT t_tx_agent_activity.UserId 
				FROM t_tx_agent_activity 
			WHERE 
				DATE(t_tx_agent_activity.ActivityDateTs) = DATE(NOW()) )", "",  FALSE);
			
	$this->db->where_in('t_tx_agent.handling_type',
		array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 )
	 foreach( $qry->result_assoc() as $rows ) 
	{
		$this->db->set('user_state',0);
		$this->db->where('UserId',$rows['UserId']);
		if( $this->db->update('t_tx_agent')) 
		{
			$conds++;
			$ClsLock->_setLockHistory($rows['UserId'], 'ABSENSI');
		}
		
	 }
  }
  
  return $conds;
}

public function _SetAgentBlocking(){
	$conds = 0;
	$ClsConf=& M_Configuration::get_instance(); 
	$ClsLock=& M_SetLockMonitoring::Instance();
	$Auth = $ClsConf->_get_auth_rule_modul();

	$USER_LOGIN_CHECK = (INT)$Auth['USER_LOGIN_CHECK'];
	if(is_array($Auth) AND $USER_LOGIN_CHECK > 0 ){
		$this->db->set('user_state',0);
		$this->db->set('ip_address',NULL);
		$this->db->set('logged_state',0);
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		if( $this->db->update('t_tx_agent')){
			$conds++;
			// $ClsLock->_setLockHistory($rows['UserId'], 'ABSENSI');
		}
	}
  
  return $conds;
}

public function _SetUnblockAgent(){
	$conds = 0;
	$ClsConf=& M_Configuration::get_instance(); 
	$ClsLock=& M_SetLockMonitoring::Instance();
	$Auth = $ClsConf->_get_auth_rule_modul();

	$USER_LOGIN_CHECK = (INT)$Auth['USER_LOGIN_CHECK'];
	if(is_array($Auth) AND $USER_LOGIN_CHECK > 0 ){
		$this->db->set('user_state',1);
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->where("user_resign <> 1","",FALSE);
		if( $this->db->update('t_tx_agent')){
			$conds++;
			// $ClsLock->_setLockHistory($rows['UserId'], 'ABSENSI');
		}
	}
  
  return $conds;
}

/*
 * @ pack : 
	_SetOpenBlockPhone with parameter on argv cronjob 
	willl cek on config if true will run if not true 
	stop Its .		
 */
 
 public function _SetOpenBlockPhone()
{
 $conds = 0;
 
 $this->db->reset_select();
 $this->db->select("a.id as UnixID, a.deb_id, b.deb_is_lock", FALSE);
 $this->db->from("t_gn_favourite_call a ");
 $this->db->join("t_gn_debitur b "," a.deb_id=b.deb_id ", "LEFT");
 $this->db->where("date_format(a.LastCallDateTs,'%Y-%m-%d') = date_format( (now() - interval +1 day),'%Y-%m-%d')", "", FALSE);
 $this->db->where("a.LastCounterCall", 5);
 $this->db->where_in("b.deb_is_lock", array(0,1) );
 $qry = $this->db->get();
 if( $qry->num_rows() >0 )
	foreach($qry->result_assoc() as $rows )
 {
 // # step 1 :: update set of tgn_favourite lock on to 0, 
 
	$this->db->set('BlockingStatus',0);
	$this->db->set('LastCounterCall', 0);
	$this->db->set('PoolingById','SYSTEM');
	$this->db->set('PoolingUpdate', date('Y-m-d H:i:s'));
	$this->db->where('id', $rows['UnixID']);
	
	if( $this->db->update('t_gn_favourite_call') )
	{
	
 // # step 2 :: update on Debitur OK 
 
		$this->db->set('deb_is_lock', 0);
		$this->db->where('deb_is_lock', 1);
		$this->db->where('deb_id',$rows['deb_id']);
		if( $this->db->update('t_gn_debitur') )
		{
			$conds++;
		}
	}
 }
 
 return $conds;

}

/*
 * @ pack : 
	_SetAutoUnlock with parameter on argv cronjob 
	willl cek on config if true will run if not true 
	stop Its .		
 */
 
 public function _SetAutoUnlock()
{

//@ reseting all account is expired OK 
  
  $count = 0;
  $this->db->reset_select();
  
//@ get select ==========================>  
 
  $this->db->select("
	 a.lock_id,a.deb_id, a.lock_upload_id as UnixId, 
	 a.lock_created_by, a.lock_created_ts, a.lock_auto_end,
	(unix_timestamp(a.lock_auto_end)-unix_timestamp(now())) AS Jumlah ", FALSE);
  
   $this->db->from("t_gn_lock_customer a ");
   $this->db->group_by("a.lock_id");
   $this->db->having("Jumlah <=", 0, FALSE);
   
   $qry = $this->db->get();
   
   if( $qry->num_rows() > 0 ) 
	foreach( $qry->result_assoc() as $rows )
  {
  
    // @ step 1 : lock open on debitur 
   
	 $this->db->set("deb_is_lock", 0);
	 $this->db->set("deb_lock_type", "0");
	 $this->db->where("deb_id", $rows['deb_id']);
	 if( $this->db->update('t_gn_debitur') )
	 {
	 
	  // @ step 2 : delete from  t_gn_lock_customer
		$this->db->where("lock_id", $rows['lock_id']);
		if( $this->db->delete("t_gn_lock_customer") )
		{
			$this->M_Loger->set_activity_log("RESET ACCOUNT
				->DEBITUR[{$rows[deb_id]}]
				->CREATE_DATE[{$rows[lock_created_ts]}]
				->CREATE_BYID[{$rows[lock_created_by]}]
				->EXPIRED_DATE[{$rows[lock_auto_end]}] 
			");
				
			$count++;
		}
	 }
  } 
  
 // @ pack : set callback to client  
 
  return $count;
	 
} // _SetAutoUnlock ==========================> 


/*
 * @ pack : start lock data if data Ok To lock its ,here 
 * ---------------------------------------------------------------
 */
 
 public function _SetStartLock()
{
 
 $Agents = array();
 
 $this->db->reset_select(); 
 $this->db->select("
	a.lock_id,a.deb_id, a.lock_upload_id as UnixId, a.lock_agent_id, 
	a.lock_created_by, a.lock_created_ts, a.lock_auto_end,
	(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(a.lock_auto_start)) AS TimeStart,
	(UNIX_TIMESTAMP(a.lock_auto_end)-UNIX_TIMESTAMP(NOW())) AS TimeEnd

	", FALSE);
		
 $this->db->from("t_gn_lock_customer a ");
 $this->db->where("a.lock_auto_run",0);
 $this->db->group_by("a.lock_id");
 $this->db->having("TimeStart>=",0,FALSE);
 $this->db->having("TimeEnd>=",0,FALSE);
 
 //echo $this->db->_get_var_dump();
 
 $qry = $this->db->get();
 
 if( $qry->num_rows() > 0 ) 
	foreach( $qry->result_assoc() as $rows )
 {

 // @ step 1 : lock open on debitur 
	 $this->db->set("deb_is_lock", 1);
	 $this->db->set("deb_lock_type", 101);
	 $this->db->set("deb_lock_date_ts", date('Y-m-d H:i:s'));
	 $this->db->where("deb_id", $rows['deb_id']);
	 
	 if( $this->db->update('t_gn_debitur') )
	 {
		$this->db->set("lock_auto_run",1);
		$this->db->where("lock_id", $rows['lock_id']);
		if( $this->db->update("t_gn_lock_customer") )
		{
			$Agents[$rows['lock_agent_id']] = $rows['lock_agent_id'];
			
			// @ pack : reset modul in OK 
			
			$this->M_Loger->set_activity_log("START ACCOUNT LOCK
				->DEBITUR[{$rows[deb_id]}]
				->CREATE_DATE[{$rows[lock_created_ts]}]
				->CREATE_BYID[{$rows[lock_created_by]}]
				->EXPIRED_DATE[{$rows[lock_auto_end]}] 
			");
				
			$count++;
		}
	 }
 }
 
 
// @ pack : reset modul yang di set oleh TL =====================> 
 
 if( count($Agents) > 0 ) 
	foreach( $Agents as $AgentId => $value )
 {
	$this->db->set("ModValueParameter", "NULL", FALSE);
	$this->db->set("ModStartParameter","NULL", FALSE);
	$this->db->set("ModEndParameter","NULL", FALSE);
	$this->db->where_in("ModUser", array($value));
	if( $this->db->update("t_gn_lock_agent_modul") )
	{
		$count++;
	}	
 }
 
// @ pack : set callback to client =============================> 
 
 return $count; 
 

} // _SetStartLock =====================>


/*
 * @ pack : update _SetUpdateDiscount  loook on Payment 
 *			 HIstory with interval that like history 
 * ---------------------------------------------------------------------			 
 */ 
 
 public function _getPrev_call_status( $deb_id=0 )
{
   $deb_prev_call_status_code = null;
   $this->db->reset_select();
   $this->db->select("a.deb_prev_call_status_code");
   $this->db->from("t_gn_debitur a");
   $this->db->where("a.deb_id", $deb_id);
   
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 )
   {
	 if( $rows = $qry->result_first_assoc() )
	 {
		$deb_prev_call_status_code =  $rows['deb_prev_call_status_code'];
	 }
   }
   
   return $deb_prev_call_status_code;
   
 } 
 
/*
 * @ pack : update _SetUpdateDiscount  loook on Payment 
 *			 HIstory with interval that like history 
 * ---------------------------------------------------------------------			 
 */ 
 public function _SetUpdateDiscount()
{
  $clsDiscount =& get_class_instance('M_BatchUpdateDiscount');
  return $clsDiscount->_setBatchUpdateDiscount();
  
} 
// _SetUpdateDiscount ====================>


/*
 * @ pack 	 : Clear of _SetClearChat 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch clear_chat
 * ------------------------------------------------------------------
 */ 
 
 public function _SetClearChat()
{
  $clsChat =& get_class_instance('M_BatchClearLog');
  return $clsChat->_setBatch_Clear_Chat();
  

} 
// _SetClearChat ============================>


/*
 * @ pack 	 : Clear of _SetClearBroadcast 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch clear_broadcast
 * ------------------------------------------------------------------
 */ 
 
 public function _SetClearBroadcast()
{
  $mgsChat =& get_class_instance('M_BatchClearLog');
  return $mgsChat->_setBatch_Clear_Broadcast();
  

} 
// _SetClearBroadcast ============================>

/*
 * @ pack 	 : cron checker bp status 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch clear_broadcast
 * ------------------------------------------------------------------
 */ 
 
 public function _SetCheckBPStatus()
{
   $BrokenPromise =& get_class_instance('M_BatchBrokenPromise');
   return $BrokenPromise->_SetUpdateBrokenPromise();
}  
// _SetCheckBPStatus ====================>
 
 
/*
 * @ pack 	 : cron checker bp status 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch clear_broadcast
 * ------------------------------------------------------------------
 */ 
 
 public function _SetCheckPOPStatus()
{
   $POP=& get_class_instance('M_BatchPOP');
   return $POP->_SetUpdatePOP();
} 
// _SetCheckPOP ========================>
 
 
/*
 * @ pack 	 : cron for dummper excel like it then you must look on t_lk_configuration 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch excel_dumper
 * ------------------------------------------------------------------
 */ 
 
 public function _SetExcelDumper()
{
	$ExcelDumper =& get_class_instance('M_BatchExcelDumper');
	return $ExcelDumper->_setProsessDumperExcel();
} 
 
/*
 * @ pack 	 : cron for dummper excel like it then you must look on t_lk_configuration 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch excel_dumper
 * ------------------------------------------------------------------
 */ 
 
 public function _DownloadExcelDeleted()
{
	$ExcelDumper =& get_class_instance('M_BatchExcelDeleted');
	return $ExcelDumper->_setProsessDumperExcel();
} 
 
/*
 * @ pack 	 : cron for re- Assignment 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch five_assigment
 * ------------------------------------------------------------------
 */ 
 
 public function _SetFiveDayAssigment()
{
	$Day5Assign =& get_class_instance('M_BatchAssign5Day');
	return $Day5Assign->_SetProcessAssign();
} 


/*
 * @ pack 	 : cron for re- Assignment 
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch five_assigment
 * ------------------------------------------------------------------
 */ 
 
 public function _SetResetApproval()
{
	$ResetApproval =& get_class_instance('M_BatchResetApproval');
	return $ResetApproval->_SetResetApproval();
} 



 
/*
 * @ pack 	 : cron for re- compile sdr  
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch compile_sdr 
 * ------------------------------------------------------------------
 */ 
  public function _SetCompileSdr()
{
	$BatchCompileSdr =& get_class_instance('M_BatchCompileSdr');
	return $BatchCompileSdr->_SetCompileSdr();
} 
 
 public function _RoundAccount()
 {
	$callback = array( 'conds'=>false,'message'=>"No Round Proccess");
	$pesan = "No Round Proccess";
	$Random =& get_class_instance('M_MgtRandDeb');
	$row_round_setup = $Random ->get_round_setup();
	$temp_modul_id = null;
	if( count($row_round_setup) > 0 )
	{
		$time = "23:00:00";
		list($hours,$mins,$secs) = explode(':',$time);
		$second_limit_round = mktime($hours,$mins,$secs) - mktime(0,0,0);
		
		$time = date("H:i:s");
		list($hours,$mins,$secs) = explode(':',$time);
		$second_now = mktime($hours,$mins,$secs) - mktime(0,0,0);
		
		foreach($row_round_setup as $round_setup)
		{
			if($round_setup['modul_setup_id'] != $temp_modul_id )
			{
				$temp_modul_id = $round_setup['modul_setup_id'];
			}
		}
		// stop crontab sementara
		if($second_now >= $second_limit_round)
		{
			if(!is_null($temp_modul_id))
			{
				$pesan = "Round Setup Running";
				$Random -> _StopRandomModule($round_setup['modul_setup_id']);
				$pesan .= "\n-->stop because max time per day";
			}
			return $pesan;
			exit();
		}
		
		$pesan = "Round Setup Checking";
		foreach($row_round_setup as $round_setup)
		{
			if(isset($round_setup['st_round_id']))
			{
				$time = $round_setup['next_round_ts'];
				list($hours,$mins,$secs) = explode(':',$time);
				$second_next = mktime($hours,$mins,$secs) - mktime(0,0,0);
				
				$time = date("H:i:s");
				list($hours,$mins,$secs) = explode(':',$time);
				$second_now = mktime($hours,$mins,$secs) - mktime(0,0,0);
				if($second_now >= $second_next)
				{
					$pesan .= "\n-->second now (".$second_now.") >= second next (".$second_next.") \n";
					if( $Random ->_RoundDebitur($round_setup) )
					{
						$this->db->set('next_round_ts','SEC_TO_TIME(('.$round_setup['duration'].'*60)+TIME_TO_SEC(now()))',false);
						$this->db->where("st_round_id", $round_setup['st_round_id']);
						if( $this->db->update("t_st_round") )
						{
							$callback = array( 'conds'=>true,'message'=>"success round debitur at ".date('Y-m-d H:i:s') );
							$pesan .= "\n-->success round debitur at ".date('Y-m-d H:i:s');
						}
					}
					else
					{
						$callback = array( 'conds'=>false,'message'=>"fail update round assign at ".date('Y-m-d H:i:s') );
						$pesan .= "\n-->fail update round assign at ".date('Y-m-d H:i:s');
					}
				}
				else
				{
					$callback = array( 'conds'=>false,'message'=>"not this time ".date('Y-m-d H:i:s') );
					$pesan .= "\n-->not this time ".date('Y-m-d H:i:s');
				}
			}
			else
			{
				$callback = array( 'conds'=>false,'message'=>"No Setup Round Proccess" );
				$pesan .= "\n-->No Setup Round Proccess at ".date('Y-m-d H:i:s');
			}
		}
		
	}
	return $pesan;
 }
 
 public function expire_access_all()
 {
	$Random =& get_class_instance('M_MgtRandDeb');
	$access_all_setup = $Random ->get_access_all_setup();
	$pesan .= "\nAccess All Auto Start and Stop Checking";
	// echo "<pre>";
	// print_r($access_all_setup);
	// echo "</pre>";
	// var_dump(count($access_all_setup) > 0);
	if(count($access_all_setup) > 0)
	{
		// $auto = array_column($access_all_setup,'bool_auto_start','st_access_all_id');
		// print_r($auto);
		$stoped=0;
		foreach($access_all_setup as $row_access_setup)
		{
			// if(isset($row_access_setup['st_access_all_id']))
			// {
				// break;
			// }
			// var_dump($row_access_setup['bool_auto_start']=="1" && $row_access_setup['bool_running']=="0");
			if($row_access_setup['bool_auto_start']=="1" && $row_access_setup['bool_running']=="0")
			{
				$time = $row_access_setup['st_access_start'];
				list($hours,$mins,$secs) = explode(':',$time);
				$second_st = mktime($hours,$mins,$secs) - mktime(0,0,0);
				
				$time = date("H:i:s");
				list($hours,$mins,$secs) = explode(':',$time);
				$second_now = mktime($hours,$mins,$secs) - mktime(0,0,0);
				if($second_now >= $second_st)
				{
					$this->db->set("bool_auto_start","0");
					$this->db->set("bool_running","1");
					$this->db->where("st_access_all_id", $row_access_setup['st_access_all_id']);
					if( $this->db->update("t_st_access_all") )
					{
						$callback = array( 'conds'=>true,'message'=>"success auto start (".$row_access_setup['st_access_start'].") access all at ".date('Y-m-d H:i:s') );
						$pesan .= "\n-->success auto start (".$row_access_setup['st_access_start'].") access all at ".date('Y-m-d H:i:s');
					}
				}
				else
				{
					$pesan .= "\n-->No this time auto start access all at ".date('Y-m-d H:i:s') ." ( ". $row_access_setup['full_name'] ." )";
				}
			}
			
			if($row_access_setup['bool_running']=="1")
			{
				$time = $row_access_setup['st_access_end'];
				list($hours,$mins,$secs) = explode(':',$time);
				$second_st = mktime($hours,$mins,$secs) - mktime(0,0,0);
				
				$time = date("H:i:s");
				list($hours,$mins,$secs) = explode(':',$time);
				$second_now = mktime($hours,$mins,$secs) - mktime(0,0,0);
				if($second_now >= $second_st)
				{
					$this->db->set("bool_running","0");
					$this->db->where("st_access_all_id", $row_access_setup['st_access_all_id']);
					if( $this->db->update("t_st_access_all") )
					{
						$callback = array( 'conds'=>true,'message'=>"success auto stop access all at ".date('Y-m-d H:i:s') );
						$pesan .= "\n-->success auto stop access all at ".date('Y-m-d H:i:s');
					}
				}
				else
				{
					$pesan .= "\n-->No this time auto stop access all at ".date('Y-m-d H:i:s') ." ( ". $row_access_setup['full_name'] ." )";
				}
			}
			else
			{
				$stoped++;
			}
		}
		// echo "terakhir".$stoped;
		// if(count($access_all_setup) == $stoped+1)
		// {
			// $Random -> _StopRandomModule($access_all_setup[$stoped]['modul_setup_id']);
		// } 
	}
	else
	{
		$pesan .= "\n-->No Setup Access All Proccess at ".date('Y-m-d H:i:s');
	}
	return $pesan;
 }
 
 /*
 * @ pack 	 : cron for re- log inventory  
 * @ cronjob : 
				$> 05 08 * * * php -q [root_directory]/hsbc/index.php ModApplicationBatch log_inventory 
 * ------------------------------------------------------------------
 */ 
  public function _SetInventoryLog()
{
	$BatchInventoryLog =& get_class_instance('M_BatchLogInventory');
	return $BatchInventoryLog->_SetLogInventory();
} 


// END CLASS  

}

?>