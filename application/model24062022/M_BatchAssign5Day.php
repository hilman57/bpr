<?php
// -------------------------------------------------------------------
/*
 * @ pack   : batch for re-Asign to spv if more then 5 days
 * @ notes  : will run by scheduler 
 * @ aksess : 0777 
 */
// ---------------------------------------------------------------------

class M_BatchAssign5Day extends EUI_Model
{

 private static $Instance = null;
 public function M_BatchAssign5Day(){ }

// -----------------------------------------------------
/*
 * @ pack : instance of class 
 */
 // -----------------------------------------------------

 public static function &Instance() {
  if(is_null(self::$Instance)) {
	self::$Instance = new self();
  }
  return self::$Instance;
 
} 

// -----------------------------------------------------
/*
 * @ pack 	: _SetProcessAssign
 * @ return : bool true/false 
 */
 // -----------------------------------------------------

 
 public function _SetProcessAssign()
{
  $default_admin = mysql_real_escape_string("admin");
  $default_spvid = mysql_real_escape_string("AGUS");
  
  $sql = " SELECT  ".
		 " a.deb_id as deb_id, c.AssignId as AssignId, ".
		 " c.AssignAdmin as AssignAdmin, c.AssignSpv as AssignSpv,".
		 " c.AssignLeader as AssignLeader, c.AssignSelerId as AssignSelerId, ".
		 " a.deb_agent as AgentId,(NOW()) as datets ".
		 " FROM t_gn_debitur a ".
		 " INNER JOIN t_tx_agent b ON  a.`deb_agent` = b.id ".
		 " INNER JOIN t_gn_assignment c ON a.deb_id = c.CustomerId ".
		 " WHERE DATE(a.deb_last_swap_ts) <= IF(DATE_FORMAT(NOW(),'%w')=0,DATE(NOW()) - INTERVAL 6 DAY,DATE(NOW()) - INTERVAL 7 DAY) ".
		 " AND b.handling_type NOT IN (1,3,8) ";
		
  $qry = $this->db->query($sql);
  if( $qry->num_rows() > 0) 
	  foreach( $qry->result_assoc() as $row )
  {
	// ------- insert into loger ------------------------------------------------------------------
	// --------------------------------------------------------------------------------------------
	
	 $this->db->reset_write();
	 $this->db->set("assign_id", $row['AssignId']);
	 $this->db->set("deb_id", $row['deb_id']);
	 $this->db->set("assign_spv_id", $row['AssignSpv']);
	 $this->db->set("assign_type", "ASSIGN.CRON");
	 $this->db->set("assign_log_created_ts",$row['datets'] );
	 $this->db->insert("t_gn_assignment_log");
		
	// ------- insert into loger ------------------------------------------------------------------
	// --------------------------------------------------------------------------------------------
	
	 $this->db->reset_write();
	 $this->db->set("AssignAdmin", $row['AssignAdmin']);
	 $this->db->set("AssignDate",$row['datets']);
	 $this->db->set("AssignMode",'GET');
	 $this->db->set("AssignLeader","NULL", false);
	 $this->db->set("AssignSelerId","NULL", false);
	 $this->db->set("AssignAmgr", 0);
	 $this->db->set("AssignMgr", 0);
	 $this->db->set("AssignSpv",0);
	 $this->db->where("AssignId",$row['AssignId']);
	 $this->db->where("CustomerId",$row['deb_id']);
	  if( $this->db->update("t_gn_assignment") )
	 {
		 $this->db->reset_write();
		 $this->db->set("deb_agent", $default_spvid);
		 $this->db->set("deb_last_swap_ts", $row['datets']);
		 $this->db->where("deb_id", $row['deb_id']);
		 $this->db->update("t_gn_debitur");
	  }
	  
	echo ".";  
  }
  
 return TRUE;
 
}  

// ==================================== END CLASS =========================================
	
}

?>