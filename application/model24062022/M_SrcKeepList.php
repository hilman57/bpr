<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SrcKeepList extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_SrcKeepList() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser','M_SetProduct', 
	'M_SetCampaign','M_ProjectWorkForm',
	'M_SetResultCategory', 'M_Combo', 
	'M_MaskingNumber'));
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.deb_id");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.deb_cmpaign_id=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.deb_call_status_code = f.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.deb_cmpaign_id=g.CampaignId","LEFT", TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	$this->EUI_Page->_setAnd('a.deb_is_kept',1);
	
 // @ pack : filter by default session ---------------------------------------------------------------------------------
	$this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'keep_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'keep_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'keep_account_status', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'keep_call_status', TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'keep_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'keep_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'keep_cust_name', TRUE);
	
 // @ pack : start date ------------------------------------------------------
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets>='". _getDateEnglish(_get_post('keep_start_date')) ."'", 'keep_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets<='". _getDateEnglish(_get_post('keep_end_date')) ."'", 'keep_end_date', TRUE);
	
 // @ pack : amount wo  ---------------------------------------------------------------------------------
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo>=". (INT)_get_post('keep_start_amountwo') ."", 'keep_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo<=". (INT)_get_post('keep_end_amountwo')."", 'keep_end_amountwo', TRUE);
	
 // @ pack : group_by 
 	$this->EUI_Page->_setGroupBy('a.deb_id');
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.deb_id AS CustomerId"=> array('CustomerId','ID','primary'),
		"d.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Product'),
		"a.deb_acct_no AS AccountNumber "=> array('AccountNumber','Costumer ID'),
		"a.deb_name AS CustomerName"=> array('CustomerName','Customer Name'),
		"ag.id AS UserId "=> array('UserId','Agent ID'),
		"f.CallReasonDesc AS AccountStatus "=> array('AccountStatus','Account Status'),
		"f.CallReasonDesc AS CallStatus "=> array('CallStatus','Last Call'),
		"a.deb_call_activity_datets AS LastCallDate "=> array('LastCallDate','Last Call Date'),
		"a.deb_resource AS Recsource "=> array('Recsource','Recsource'),
		"a.deb_amount_wo AS AmountWO "=> array('AmountWO','Amount WO'),
		"a.deb_bal_afterpay AS BalanceAffterPay "=> array('BalanceAffterPay','Bal. Afterpay'),
		"a.deb_id AS History "=> array('History','History')
	));
	
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.deb_id=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.deb_cmpaign_id=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.deb_call_status_code = f.CallReasonCode ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.deb_cmpaign_id=g.CampaignId","LEFT", TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
 	 $this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
 	 $this->EUI_Page->_setAnd('a.deb_is_kept',1);
	 $this->EUI_Page->_setWherein('g.ProjectId',_get_session('ProjectId') );
	 
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 		
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('b.AssignSelerId IS NOT NULL');
	}		
	
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'keep_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'keep_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'keep_account_status', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'keep_call_status', TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'keep_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_resource', 'keep_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'keep_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets>='". _getDateEnglish(_get_post('keep_start_date')) ."'", 'keep_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_call_activity_datets<='". _getDateEnglish(_get_post('keep_end_date')) ."'", 'keep_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo>=". (INT)_get_post('keep_start_amountwo') ."", 'keep_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("a.deb_amount_wo<=". (INT)_get_post('keep_end_amountwo')."", 'keep_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('a.deb_id');
		
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
	
	public function _save_keep_history($data)
	{
		$_conds = 0;
		if( ($this ->EUI_Session->_have_get_session('UserId') !=FALSE) )
		{
			// $this->db->set("keept_deb_id", $this->URI->_get_post('CustomerId'));
			// $this->db->set("keept_userid",$this -> EUI_Session -> _get_session('UserId'));
			// $this->db->set("keept_create_datets",$this->EUI_Tools->_date_time());
			// $this->db->set("keept_deb_type",$this->URI->_get_post('IsKeep'));
			
			$this->db->insert("t_gn_keept_history",$data);
			if( $this->db->affected_rows() > 0 )
			{
				$_conds++;
			}
		}
		return $_conds;
	}
	
	public function _update_keep_deb($data)
	{
		$_conds = 0;
		
		$this->db->set('deb_is_kept',$data['keept_deb_type']);
		
		 
		// @ pack : t_gn_debitur where update on here 
		 
		$this->db->where('deb_id', $data['keept_deb_id']);
		if( $this->db->update('t_gn_debitur') )
		{
			$_conds++;
		}
		return $_conds;
	}
	

}

// END OF CLASS 



?>