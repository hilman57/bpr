<?php
class M_RptUpload extends EUI_Model
{
	function M_RptUpload()
	{
	
	}
	
	function _getAgentBySpv($spv_id)
	{
		$datas = array();
		
		$sql = "select 
					a.UserId, 
					a.id, 
					a.full_name 
				from t_tx_agent a 
				where 1=1 
				AND a.spv_id = '".$spv_id."'
				AND a.handling_type = '".USER_AGENT_OUTBOUND."'
				order by a.full_name asc";
		$qry = $this->db->query($sql);
		
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['UserId']] = $rows['full_name'];
			}
		}
		
		return $datas;
	}
	
	function _getCampaignFilter()
	{
		$datas = array();
		
		$sql = "select 
					a.CampaignId, 
					a.CampaignNumber, 
					a.CampaignName 
				from t_gn_campaign a 
				left join t_gn_campaign_project b on a.CampaignId = b.CampaignId
				where 1=1
				AND a.CampaignStatusFlag = 1 ";
		
		if($this->EUI_Session->_have_get_session('ProjectId'))
		{
			$ProjectId = implode(',',$this->EUI_Session->_get_session('ProjectId'));
			$sql .= " AND b.ProjectId in (".$ProjectId.") ";
		}
		$sql .= " order by a.CampaignName asc ";
		// echo $sql;
		$qry = $this->db->query($sql);
		
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0))
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['CampaignId']] = $rows['CampaignName'];
			}
		}
		else{
			die('Data Unavaliable for this criteria.');
		}
		
		return $datas;
	}
	
	function _getDatasReport($param)
	{
		$datas = array();
		
		/* $sql = "select 
					c.CampaignId,
					date(b.LogCreatedDate) as LogCreatedDate,
					b.LogUserId,
					count(a.AssignId) as DataCustomer
				from t_gn_assignment a
				left join t_gn_distribusi_log b on a.AssignId = b.LogAssignmentId
				left join t_gn_debitur c on a.CustomerId = c.CustomerId
				where 1=1
				and a.AssignSelerId is not null
				and b.LogAssignUserId = '".$param['SpvId']."'
				and date(b.LogCreatedDate) >= '".$param['start_date']."'
				and date(b.LogCreatedDate) <= '".$param['end_date']."'
				and b.LogUserId in (".$param['AgentId'].")
				and c.CampaignId in (".$param['AgentId'].")
				group by c.CampaignId, b.LogCreatedDate, b.LogUserId";
		echo $sql; */
		
		$sql = "SELECT 
					c.CampaignId,
					DATE(a.AssignDate) AS LogCreatedDate, 
					a.AssignSelerId as LogUserId,
					count(a.AssignId) AS DataCustomer
				FROM t_gn_assignment a
				LEFT JOIN t_gn_debitur c ON a.CustomerId = c.CustomerId
				WHERE 1=1
				AND a.AssignSelerId IS NOT NULL
				AND a.AssignSpv = '".$param['SpvId']."'
				AND DATE(a.AssignDate) >= '".$param['start_date']."'
				AND DATE(a.AssignDate) <= '".$param['end_date']."'
				AND a.AssignSelerId IN (".$param['AgentId'].") 
				AND c.CampaignId IN (".$param['CampaignId'].")
				GROUP BY c.CampaignId, date(a.AssignDate), a.AssignSelerId 
				";

		// var_dump($sql);
		
		$qry = $this->db->query($sql);
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
		{
			foreach($qry->result_assoc() as $rows)
			{
				// $datas[$rows['CampaignId']][$rows['LogCreatedDate']][$rows['LogUserId']] = $rows['DataCustomer'];
				$datas[$rows['CampaignId']][$rows['LogCreatedDate']][$rows['LogUserId']] = $rows['DataCustomer'];
			}
		}
		// else
		// {
			// die('Data Unavaliable for this criteria.');
		// }
		
		
		return $datas;
	}
	
	function _getPolicyReport($param)
	{
		$datas = array();
		
		$sql = "SELECT 
					c.CampaignId,
					DATE(a.AssignDate) AS LogCreatedDate, 
					a.AssignSelerId as LogUserId,
					count(a.AssignId) AS DataCustomer
				FROM t_gn_assignment a
				LEFT JOIN t_gn_debitur c ON a.CustomerId = c.CustomerId
				LEFT JOIN t_gn_policy_detail d on c.CustomerNumber = d.EnigmaId
				WHERE 1=1
				AND a.AssignSelerId IS NOT NULL
				AND a.AssignSpv = '".$param['SpvId']."'
				AND DATE(a.AssignDate) >= '".$param['start_date']."'
				AND DATE(a.AssignDate) <= '".$param['end_date']."'
				AND a.AssignSelerId IN (".$param['AgentId'].") 
				AND c.CampaignId IN (".$param['CampaignId'].")
				GROUP BY c.CampaignId, date(a.AssignDate), a.AssignSelerId";

		// var_dump($sql);
		
		$qry = $this->db->query($sql);
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['CampaignId']][$rows['LogCreatedDate']][$rows['LogUserId']] = $rows['DataCustomer'];
			}
		}
		// else
		// {
			// die('Data Unavaliable for this criteria.');
		// }
		
		
		return $datas;
	}
	
	function _getAgentName($param)
	{
		$datas = array();
		
		$sql = "select a.UserId, a.id, a.full_name from t_tx_agent a where a.UserId in (".$param['AgentId'].") order by a.full_name asc";
		$qry = $this->db->query($sql);
		
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0))
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['UserId']] = $rows['id']." - ".$rows['full_name'];
			}
		}
		
		return $datas;
	}
	
	function _getCampaignName($param)
	{
		$datas = array();
		
		$sql = "select a.CampaignId, a.CampaignNumber, a.CampaignName from t_gn_campaign a where a.CampaignId in (".$param['CampaignId'].")";
		$qry = $this->db->query($sql);
		
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0))
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['CampaignId']] = $rows['CampaignNumber']." - ".$rows['CampaignName'];
			}
		}
		
		return $datas;
	}
}
?>