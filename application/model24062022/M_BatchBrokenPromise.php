<?php 

/**
 * @ pack : Periksa data coll_debitur yang statusnya POP dan sudah lebih dari 7 hari
 *          update status debitur menjadi BP (Broken Promise) jika ketemu
 */
 
 class M_BatchBrokenPromise extends EUI_Model 
{
  
 var $PTP_STATUS_CODE= 106; // t_lk_account_status
 var $BP_STATUS_CODE = 108; // t_lk_account_status
 
/*
 * @ pack : private static $Instance 
 */
 
 private static $Instance = NULL;

/*
 * @ pack : M_BatchClearLog
 */
 
 public static function &Instance() 
{
 if(is_null(self::$Instance) ) {
  self::$Instance = new self();	
 }
  return self::$Instance;
  
}

/*
 * @ pack : function _set model this 
 */ 
 
 public function M_BatchBrokenPromise()  
{	 
  /** @ rerun null **/ 
}

 
/*
 * @ pack : function _set model this 
 */ 
 
 public function _SetUpdateBrokenPromise() 
{	

 /* get class this **/ 
  $ClsBatch =& get_class_instance('M_BatchApplication');
  
  $this->db->reset_select();
  $this->db->select("a.*, 
	c.CreatedById as CreatedById, 
	c.CustomerId as CustomerId,
	c.TeamLeaderId, c.SupervisorId",FALSE);

  $this->db->from("t_tx_ptp a ");
  $this->db->join("t_gn_debitur b ","a.deb_id=b.deb_id", "LEFT");
  $this->db->join("t_gn_callhistory c "," a.ptp_callhistory_id=c.CallHistoryId","INNER");
  $this->db->where("a.ptp_last_status", $this->PTP_STATUS_CODE);
  $this->db->where("b.deb_call_status_code <> 109");
  
// filter ini berdaarakan data crontab yang lama .
  
  $this->db->where("a.ptp_date<=(NOW()-INTERVAL 7 DAY)", "", FALSE);
  $this->db->where("a.is_delete<>1","", FALSE);
  
 
 //echo $this->db->_get_var_dump();
/* @ pack : load its **/
 
  $qry = $this->db->get();
  if( $qry->num_rows() > 0 ) 
	foreach( $qry->result_assoc() as $rows ) 
  {
	$CustomerId = $rows['CustomerId'];
	$agent 	= $rows['CreatedById'];
	$ptp_id = $rows['ptp_id'];
	$TeamLeaderId = $rows['TeamLeaderId']; 
	$SupervisorId = $rows['SupervisorId'];
	
	$this->db->set("deb_call_status_code", $this->BP_STATUS_CODE);
	$this->db->set("deb_last_trx_date", date('Y-m-d H:i:s'));
	$this->db->where("deb_id", $rows['deb_id']);
	$this->db->update("t_gn_debitur");
	
// next for checked *
	
	$conds = false;
	
	if($this->db->affected_rows() > 0) {
		$conds = true;
	}
	
// @ if true update on PTP transaction  this for not checked again **/	
	if( TRUE == $conds )
	{
		$this->db->set("ptp_last_status", $this->BP_STATUS_CODE);
		$this->db->where("ptp_id", $ptp_id);
		$this->db->update('t_tx_ptp');
		if( $this->db->affected_rows() > 0 ){
			$conds = true;
		}
	}
	
// @ pack : set to history its **/

    $call_prev_status  = $ClsBatch->_getPrev_call_status($CustomerId); // get last statun on debitur 
	$call_times_status = date('Y-m-d H:i:s'); // call time  = now ()
	$call_notes_status = "BP-Broken Promise-Auto"; // notes call status 

// @ pack : set cond ---------------------
	
	if( TRUE == $conds )
	{
		$this->db->set("CustomerId",$CustomerId);
		$this->db->set("CallAccountStatus",$this->BP_STATUS_CODE);
		$this->db->set("CallReasonId",$call_prev_status);
		$this->db->set("CreatedById",$agent);
		$this->db->set("CallHistoryCreatedTs",$call_times_status);
		$this->db->set("CallHistoryNotes",$call_notes_status);
		$this->db->set("TeamLeaderId",$TeamLeaderId);
		$this->db->set("SupervisorId",$SupervisorId);
		$this->db->insert('t_gn_callhistory');
		
		if( $this->db->affected_rows() > 0 )
		{
			$conds = true; 
		}
	}
	
  }
  
  return $conds;
}

// END CLASS  

} 

?>