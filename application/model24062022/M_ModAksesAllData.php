<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_ModAksesAllData extends EUI_Model
{
	/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 var $ToAgent;
 
private static $Instance = null;

/*
 * @ pack : Instance 
 */
 
 public static function &Instance()
{
  if(is_null(self::$Instance) )
 {
	self::$Instance = new self();
  }
 
 return self::$Instance;
 
}

/*
 * @ pack : Aksesor  
 */
 
 public function M_ModAksesAllData() 
{
	$this->load->model(array('M_SetCallResult'));
	$this->ToAgent = array(	'NotAssign'=>'b.akses_target_id IS NULL',
							'AccessAll'=> 'b.akses_target_id=0'
					);
				
 }

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.deb_id");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_akses_all b","a.deb_id= b.akses_deb_id","INNER");
	$this->EUI_Page->_setJoin("t_lk_account_status c","a.deb_call_status_code = c.CallReasonCode","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent d","b.akses_existing_agent_id = d.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent e","b.akses_target_id = e.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project f "," a.deb_cmpaign_id=f.CampaignId","LEFT", TRUE);
	
	// access all filter
	$this->EUI_Page->_setAnd('a.deb_is_access_all',1);
	$this->EUI_Page->_setAnd('b.akses_all_flag',1);
	
	 // @ pack : filter by default session ---------------------------------------------------------------------------------
	$this->EUI_Page->_setWherein('f.ProjectId',_get_session('ProjectId') );
	
	// @ pack : set cache on page **/
	// var_dump(_get_post('ModAccess_New_Agent'));
	// var_dump(array_key_exists(_get_post('ModAccess_New_Agent'),$this->ToAgent));
	if(_get_post('ModAccess_New_Agent'))
	{
		
		if( array_key_exists(_get_post('ModAccess_New_Agent'),$this->ToAgent) )
		{
			$this->EUI_Page->_setAnd($this->ToAgent[_get_post('ModAccess_New_Agent')]);
			// echo $this->ToAgent[_get_post('ModAccess_New_Agent')];
		}
		else
		{
			$this->EUI_Page->_setAnd('b.akses_target_id', _get_post('ModAccess_New_Agent'));
		}
	}
	
	
	// $this->EUI_Page->_setAndCache('a.akses_type', 'ModAccess_New_Agent', TRUE);
	$this->EUI_Page->_setAndCache('b.akses_existing_agent_id', 'ModAccess_Exist_Agent', TRUE);
	// $this->EUI_Page->_setAndCache('a.deb_cmpaign_id', 'ModAccess_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'ModAccess_cust_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'ModAccess_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'ModAccess_account_status', TRUE);
	
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	// $this->EUI_Page->_setArraySelect(array(
		// 'a.akses_id AS akses_id'=> array('akses_id','ID','primary'),
		// 'a.akses_deb_id AS akses_deb_id '=> array('akses_deb_id','Debitur Id')
	// ));
	$this->EUI_Page->_setArraySelect(array(
		'a.deb_id AS CustomerId'=> array('CustomerId','ID','primary'),
		'a.deb_acct_no AS CustomerNumber'=>array('CustomerNumber','Customer Id'),
		'a.deb_name AS CustomerName'=>array('CustomerName','Customer Name'),
		'c.CallReasonDesc AS AccountStatus'=>array('AccountStatus','Account Status'),
		'd.full_name AS ExistAgentName'=>array('ExistAgentName','Exist Agent Name'),
		'IF(b.akses_type=1,"All",e.full_name) AS NewAgentName'=>array('NewAgentName','New Agent Name'),
		'a.deb_last_trx_date AS LastCallDate'=>array('LastCallDate','Last Call Date'),
		'b.akses_upload_ts AS UploadDate'=>array('UploadDate','Upload Date')
	));
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_akses_all b","a.deb_id= b.akses_deb_id","INNER");
	$this->EUI_Page->_setJoin("t_lk_account_status c","a.deb_call_status_code = c.CallReasonCode","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent d","b.akses_existing_agent_id = d.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent e","b.akses_target_id = e.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project f "," a.deb_cmpaign_id=f.CampaignId","LEFT", TRUE);
	
	// access all filter
	$this->EUI_Page->_setAnd('a.deb_is_access_all',1);
	$this->EUI_Page->_setAnd('b.akses_all_flag',1);
	
	
	 // @ pack : filter by default session ---------------------------------------------------------------------------------
	$this->EUI_Page->_setWherein('f.ProjectId',_get_session('ProjectId') );
	
	// @ pack : set cache on page **/
	if(_get_post('ModAccess_New_Agent'))
	{
		
		if( array_key_exists(_get_post('ModAccess_New_Agent'),$this->ToAgent) )
		{
			$this->EUI_Page->_setAnd($this->ToAgent[_get_post('ModAccess_New_Agent')]);
		}
		else
		{
			$this->EUI_Page->_setAnd('b.akses_target_id', _get_post('ModAccess_New_Agent'));
		}
	}
	
	$this->EUI_Page->_setAndCache('b.akses_existing_agent_id', 'ModAccess_Exist_Agent', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_acct_no', 'ModAccess_cust_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_name', 'ModAccess_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.deb_call_status_code', 'ModAccess_account_status', TRUE);
	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	// echo $this ->EUI_Page->_getCompiler(); 
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }

 public function get_all_leader()
 {
	$data = array();
	$this->db->reset_select();
	$this->db->select('a.UserId, a.full_name');
	$this->db->from('t_tx_agent a');
	$this->db->where(array(	'a.user_state'=>1,
							'a.handling_type'=>USER_LEADER));
 
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 ){
		foreach( $qry->result_assoc() as $rows )
		{
			$data[$rows['UserId']] = $rows['full_name'];
		}
   }
   return $data;
 }
 
 public function get_all_DC()
 {
	$data = array();
	$this->db->reset_select();
	$this->db->select('a.UserId, a.full_name,a.code_user');
	$this->db->from('t_tx_agent a');
	$this->db->where(array(	'a.user_state'=>1,
							'a.handling_type'=>USER_AGENT_OUTBOUND));
 
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 ){
		foreach( $qry->result_assoc() as $rows )
		{
			$data[$rows['UserId']] = $rows['code_user'] ." - ". $rows['full_name'];
		}
   }
   return $data;
 }
 
 
 
 // update t_akses_all by deb id
 
 public function UpdateAccessAssign($data,$key)
 {
	$this->db->where($key);
	$this->db->update('t_gn_akses_all', $data);
	if($this->db->affected_rows()>0)
	{
		return true;
	}
 }
 
 // update access all flag (Reset)
 public function UpdateAccessFlag($key)
 {
	
	$this->db->where($key);
	$this->db->update('t_gn_akses_all', array('akses_all_flag'=>'0'));
	if($this->db->affected_rows()>0)
	{
		return true;
	}
 }
 
 //Update access all flag (Reset All)
 
 public function UpdateAccessFlagAll()
 {
		$this->db->set('akses_all_flag','0');
		$this->db->where('(approve_status != 101 OR approve_status IS NULL) AND akses_all_flag = 1');
		//$this->db->where('akses_all_flag', 1);
		$this->db->update('t_gn_akses_all');
		if($this->db->affected_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
 }
 // get all deb not assign in access all
 
 public function  get_deb_not_assign()
 {
 
	$data = array();
	$this->db->reset_select();
	$this->db->select('a.akses_deb_id');
	$this->db->from('t_gn_akses_all a');
	$this->db->where(array('a.akses_all_flag'=>1,'a.akses_type' => 0));
   $qry = $this->db->get();
   
   if( $qry->num_rows() > 0 ){
		foreach( $qry->result_assoc() as $rows )
		{
			$data[]=$rows['akses_deb_id'];
		}
		return $data;
   }
   else 
   {
		return FALSE;
   }
  
 }
/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */
 
 public function _get_data_aksess_all( $DebiturId = null )
{
	$this->db->reset_select();
	$this->db->select("a.akses_deb_id AS deb_id", FALSE);
	$this->db->from("t_gn_akses_all a ");
	$this->db->where("akses_all_flag",1);
	
// @ cek params type : 
  
  if( !is_null($DebiturId) )
  {	
	if( is_array($DebiturId) ) {
		$this->db->where_in("a.akses_deb_id", $DebiturId);
	} else {
		$this->db->where_in("a.akses_deb_id", array($DebiturId));
	}	
  }	
  
// @ set query 	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ) {
		return $qry->result_assoc();
	} else {
		return FALSE;
	}
	
} // ==> _get_data_aksess_all


/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */
 
 public function _get_is_aksess_all( $DebiturId )
{
  $result = $this->_get_data_aksess_all( $DebiturId);
  if( is_array($result) )
  {
	 $deb_id = reset($result);
	 return (INT)$deb_id['deb_id'];
  } else {
	return FALSE;
  }
  
 } // ==> _get_is_aksess_all
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 
 // END CLASS 
}

?>