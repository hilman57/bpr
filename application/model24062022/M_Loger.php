<?php
/*
 * @ pack : M_Loger handle activity user 
 * @ auth : omens 
 * @ date : 2015-03-03 
 */
 
class M_Loger extends EUI_Model
{

var $_arr_sesion_id = array();
/*
 * @ pack : set_activity_log 
 */ 
 
private static $Instance = null;



/*
 * @ pack : set_activity_log 
 */ 
 
function M_Loger() { }

/*
 * @ pack : Instance 
 */ 
 
 public static function &Instance()
{
 if( is_null(self::$Instance) ) {
	self::$Instance = new self();
 }
 return self::$Instance;
}

/*
 * @ pack : set_activity_log 
 */ 
 
 public function _set_session_id($key, $value)
{
  if(!is_array($key) ) {
	$this->_arr_sesion_id[$key] = $value;
  } 
  else{
	 foreach( $key as $field => $value )
	 {
		$this->_arr_sesion_id[$field] = $value;
	 }
  }
}

/*
 * @ pack : set_activity_log 
 */ 
 public function set_activity_log( $_comment = null )
{
 $conds = 0;
 
 if( !is_null($_comment) )
 {
   if( $this->EUI_Session->_have_get_session('UserId') ){ 
	 $this->db->set('ActivityUserId',_get_session('UserId'));	
   } else {
	  $this->db->set('ActivityUserId',$this->_arr_sesion_id['UserId']);
   }
	
	$this->db->set('ActivityDate', date('Y-m-d H:i:s'));
	$this->db->set('ActivityEvent',$_comment);
	$this->db->insert('t_gn_activitylog');
	
	if( $this->db->affected_rows() > 0  ) {
		$conds++;
	}
	
  }
  
  return $conds;
  
}

 // END CLASS 
 
}

?>