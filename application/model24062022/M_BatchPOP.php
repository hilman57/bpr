<?php 

/**
 * @ pack : Periksa data coll_debitur yang statusnya POP dan sudah lebih dari 7 hari
 *          update status debitur menjadi BP (Broken Promise) jika ketemu
 */
 
class M_BatchPOP extends EUI_Model
{
  
 var $PTP_STATUS_CODE = 106; // t_lk_account_status
 var $BP_STATUS_CODE  = 108; // t_lk_account_status
 var $POP_STATUS_CODE = 107; // t_lk_account_status
 
/*
 * @ pack : private static $Instance 
 */
 
 private static $Instance = NULL;

/*
 * @ pack : M_BatchClearLog
 */
 
 public static function &Instance() 
{
 if(is_null(self::$Instance) ) {
  self::$Instance = new self();	
 }
  return self::$Instance;
  
}
 
/*
 * @ pack : function _set model this 
 */ 
 
public function M_BatchPOP() 
{	
	// return null 
}

/*
 * function _SetUpdatePOP
 */
 
 public function _SetUpdatePOP()
{
  $conds = 0;
 
  $this->_setPaymentProgress1();
  $this->_setPaymentProgress2();
  $this->_setPaymentProgress3();
  $this->_setPaymentProgress4();
  $this->_setPaymentProgress5();
  
  return $conds;
  
}


/*
 * @ pack : step Process one  ------------------>
 * -------------------------------------------------
 * * @ notes : interval 1 - 2 month -> pop1 
 */
 
 private function _setPaymentProgress1()
{
  
  $POP_STATUS1 =& $this->POP_STATUS_CODE;
  $update_account = null;
  
/* @pack : set reset select to default query **/
 
  $this->db->reset_select();
  
  $this->db->select("MAX(a.pay_date) as trxdate, b.deb_id as AccountId, b.deb_call_status_code as accstatus ", FALSE);
  $this->db->from("t_gn_payment a");
  $this->db->join("t_gn_debitur b "," a.deb_id=b.deb_id", "INNER");
  $this->db->group_by("b.deb_id");
  $this->db->having("
	DATE_FORMAT(trxdate,'%Y-%m-%d')<= date_format((NOW() - INTERVAL 1 MONTH - INTERVAL 2 DAY),'%Y-%m-%d') 
	AND DATE_FORMAT(trxdate,'%Y-%m-%d')>= date_format((NOW() - INTERVAL 2 MONTH - INTERVAL 1 DAY),'%Y-%m-%d')
	AND accstatus IN({$this->POP_STATUS_CODE}) ","", FALSE);

 $qry = $this->db->get();
 
 if( $qry->num_rows() >0) 
	foreach( $qry ->result_assoc() as $rows )
 {
	$update_account[$rows['deb_id']] = $POP_STATUS1;
 }
 
 // cek of validation data ***/
 
 if( is_null($update_account)  
	AND !is_array($update_account) ) 
{
	print("Invalid Data POP-1 :". __LINE__ );
	print("\r\n");	
 }
 
 
// then next process  

 foreach( $update_account as $AccountId => $Status )
 {
	$this->db->set("deb_call_status_code", $Status);
	$this->db->where("deb_id", $AccountId);
	$this->db->update("t_gn_debitur");
 }
 
}// _setPaymentProgress1 =======================>
 
 
/*
 * @ pack : step Process one  ------------------>
 * -------------------------------------------------
 * @ notes : interval 2 - 3 month --> POP 2 
 */
 
 private function _setPaymentProgress2()
{
  
  $POP_STATUS2 =& $this->POP_STATUS_CODE;
  $update_account = null;
  
/* @pack : set reset select to default query **/
 
  $this->db->reset_select();
  
  $this->db->select("MAX(a.pay_date) as trxdate, b.deb_id as AccountId, b.deb_call_status_code as accstatus ", FALSE);
  $this->db->from("t_gn_payment a");
  $this->db->join("t_gn_debitur b "," a.deb_id=b.deb_id", "INNER");
  $this->db->group_by("b.deb_id");
  $this->db->having("
	DATE_FORMAT(trxdate,'%Y-%m-%d')<= date_format((NOW() - INTERVAL 2 MONTH - INTERVAL 2 DAY),'%Y-%m-%d') 
	AND DATE_FORMAT(trxdate,'%Y-%m-%d')>= date_format((NOW() - INTERVAL 3 MONTH - INTERVAL 1 DAY),'%Y-%m-%d')
	AND accstatus IN({$this->POP_STATUS_CODE}) ","", FALSE);
		
 $qry = $this->db->get();
 if( $qry->num_rows() >0) 
	foreach( $qry ->result_assoc() as $rows )
 {
	$update_account[$rows['deb_id']] = $POP_STATUS2;
 }
 
 // cek of validation data ***/
 
 if( is_null($update_account)  
	AND !is_array($update_account) ) 
{
	print("Invalid Data POP-2 :". __LINE__ );
	print("\r\n");	
 }
 
 
// then next process  

 foreach( $update_account as $AccountId => $Status )
 {
	$this->db->set("deb_call_status_code", $Status);
	$this->db->where("deb_id", $AccountId);
	$this->db->update("t_gn_debitur");
 }
 
}
// _setPaymentProgress2 =======================>
 
 
 
/*
 * @ pack : step Process one  ------------------>
 * -------------------------------------------------
 * @ notes : interval 2 - 3 month --> POP 3
 */
 
 private function _setPaymentProgress3()
{
  
  $POP_STATUS3 =& $this->POP_STATUS_CODE;
  $update_account = null;
  
/* @pack : set reset select to default query **/
 
  $this->db->reset_select();
  
  $this->db->select("MAX(a.pay_date) as trxdate, b.deb_id as AccountId, b.deb_call_status_code as accstatus ", FALSE);
  $this->db->from("t_gn_payment a");
  $this->db->join("t_gn_debitur b "," a.deb_id=b.deb_id", "INNER");
  $this->db->group_by("b.deb_id");
  $this->db->having("
	DATE_FORMAT(trxdate,'%Y-%m-%d')<= date_format((NOW() - INTERVAL 2 MONTH - INTERVAL 2 DAY),'%Y-%m-%d') 
	AND DATE_FORMAT(trxdate,'%Y-%m-%d')>= date_format((NOW() - INTERVAL 3 MONTH - INTERVAL 1 DAY),'%Y-%m-%d')
	AND accstatus IN({$this->POP_STATUS_CODE}) ","", FALSE);
		
 $qry = $this->db->get();
 if( $qry->num_rows() >0) 
	foreach( $qry ->result_assoc() as $rows )
 {
	$update_account[$rows['deb_id']] = $POP_STATUS3;
 }
 
 // cek of validation data ***/
 
 if( is_null($update_account)  
	AND !is_array($update_account) ) 
{
	print("Invalid Data POP-3 :". __LINE__ );
	print("\r\n");	
 }
 
 
// then next process  

 foreach( $update_account as $AccountId => $Status )
 {
	$this->db->set("deb_call_status_code", $Status);
	$this->db->where("deb_id", $AccountId);
	$this->db->update("t_gn_debitur");
 }
 
}
// _setPaymentProgress3 =======================>
 
 
/*
 * @ pack : step Process one  ------------------>
 * -------------------------------------------------
 * @ notes : interval 3  month --> POP -4
 */
 
 private function _setPaymentProgress4()
{
  
  $POP_STATUS4 =& $this->POP_STATUS_CODE;
  $update_account = null;
  
/* @pack : set reset select to default query **/
 
  $this->db->reset_select();
  
  $this->db->select("MAX(a.pay_date) as trxdate, b.deb_id as AccountId, b.deb_call_status_code as accstatus ", FALSE);
  $this->db->from("t_gn_payment a");
  $this->db->join("t_gn_debitur b "," a.deb_id=b.deb_id", "INNER");
  $this->db->group_by("b.deb_id");
  $this->db->having("
	DATE_FORMAT(trxdate,'%Y-%m-%d')<= date_format((NOW() - INTERVAL 3 MONTH - INTERVAL 2 DAY),'%Y-%m-%d') 
	AND accstatus IN({$this->POP_STATUS_CODE}) ", "", FALSE);
		
 $qry = $this->db->get();
 if( $qry->num_rows() >0) 
	foreach( $qry ->result_assoc() as $rows )
 {
	$update_account[$rows['deb_id']] = $POP_STATUS4;
 }
 
 // cek of validation data ***/
 
 if( is_null($update_account)  
	AND !is_array($update_account) ) 
{
	print("Invalid Data POP-4 :". __LINE__ );
	print("\r\n");	
		
 }
 
 
// then next process  

 foreach( $update_account as $AccountId => $Status )
 {
	$this->db->set("deb_call_status_code", $Status);
	$this->db->where("deb_id", $AccountId);
	$this->db->update("t_gn_debitur");
 }
 
}
// _setPaymentProgress4 =======================>  
 

 
/*
 * @ pack : step Process one  ------------------>
 * -------------------------------------------------
 * @ notes : interval 3  month --> POP-5
 */
 
 private function _setPaymentProgress5()
{
  
  $POP_STATUS5 =& $this->POP_STATUS_CODE;
  $update_account = null;
  
/* @pack : set reset select to default query **/
 
  $this->db->reset_select();
  
  $this->db->select("MAX(a.pay_date) as trxdate, b.deb_id as AccountId, b.deb_call_status_code as accstatus ", FALSE);
  $this->db->from("t_gn_payment a");
  $this->db->join("t_gn_debitur b "," a.deb_id=b.deb_id", "INNER");
  $this->db->group_by("b.deb_id");
  $this->db->having("
	DATE_FORMAT(trxdate,'%Y-%m-%d')<= date_format((NOW() - INTERVAL 3 MONTH - INTERVAL 2 DAY),'%Y-%m-%d') 
	AND accstatus IN({$this->POP_STATUS_CODE}) ","",FALSE);
		
 $qry = $this->db->get();
 if( $qry->num_rows() >0) 
	foreach( $qry ->result_assoc() as $rows )
 {
	$update_account[$rows['deb_id']] = $POP_STATUS5;
 }
 
 // cek of validation data ***/
 
 if( is_null($update_account)  
	AND !is_array($update_account) ) 
{
	print("Invalid Data POP-5 :". __LINE__ );
	print("\r\n");	
 }
 
 
// then next process  

 foreach( $update_account as $AccountId => $Status )
 {
	$this->db->set("deb_call_status_code", $Status);
	$this->db->where("deb_id", $AccountId);
	$this->db->update("t_gn_debitur");
 }
 
}
// _setPaymentProgress4 =======================>  

// END CLASS 
 
} 