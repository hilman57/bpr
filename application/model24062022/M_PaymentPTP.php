<?php
class M_PaymentPTP extends EUI_Model
{

/*
 * @ pack : aksesor M_PaymentPTP
 */
 
function M_PaymentPTP(){

}

/*
 * @ pack : get all data 
 */
 
public function _getPaymentDebitur()
{
  $DebiturId = $this->URI->_get_post('DebiturId');
  
  
  $this->db->reset_select(); // if cache 
  $this->db->select("a.ptp_id,a.ptp_date, a.ptp_amount, b.pch_name", FALSE);
  $this->db->from("t_tx_ptp a ");
  $this->db->join("t_lk_payment_channel b "," a.ptp_chanel = b.pch_code","LEFT");
  $this->db->where("a.deb_id", $DebiturId);
  $this->db->where("a.is_delete", "0");
  $this->db->order_by("a.ptp_date","DESC");
  // echo $this->db->print_out();
  $qry = $this->db->get();
  if( $qry->num_rows() ){
	return $qry ->result_assoc();
  } else {
	return array();
  }
  
  
}

public function DeletePTP()
{
	$conds = FALSE;
	$PtpId = $this->URI->_get_post('ptpid');
	// var_dump(!empty($PtpId));
	// echo $PtpId;
	if( !empty($PtpId) )
	{
		$this->db->reset_select();
		$this->db->set("is_delete","1");
		$this->db->set("delete_by", _get_session('UserId') );
		$this->db->set("delete_date",date('Y-m-d H:i:s'));
		$this->db->where("ptp_id",$PtpId);
		$this->db->update("t_tx_ptp");
		if($this->db->affected_rows()>0)
		{
			$conds = TRUE;
		}
	}
	
	return $conds;
}
 
 // END CLASS 
 
}

?>