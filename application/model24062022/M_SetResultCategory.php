<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SetResultCategory extends EUI_Model
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery("
		SELECT * FROM t_lk_customer_status a 
		LEFT JOIN t_lk_outbound_goals b 
		ON a.CallOutboundGoalsId=b.OutboundGoalsId 
	"); 
	
	$flt = '';
	
	if( $this->URI->_get_have_post('keywords') ) 
	{
		$flt .=" AND ( 
						a.CallReasonCategoryCode LIKE '%{$this->URI->_get_post('keywords')}%'  
						OR a.CallReasonCategoryName LIKE '%{$this->URI->_get_post('keywords')}%'  
						OR a.CallReasonInterest LIKE '%{$this->URI->_get_post('keywords')}%' 
						OR a.CallReasonCategoryFlags LIKE '%{$this->URI->_get_post('keywords')}%' 
						OR a.CallReasonCategoryOrder LIKE '%{$this->URI->_get_post('keywords')}%' 
						OR a.CallOutboundGoalsId LIKE '%{$this->URI->_get_post('keywords')}%' 
						
					)";
	}				
			
	$this -> EUI_Page -> _setWhere($flt);   
	if( $this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
 
  $sql =" SELECT * FROM t_lk_customer_status a 
		  LEFT JOIN t_lk_outbound_goals b 
		  ON a.CallOutboundGoalsId=b.OutboundGoalsId ";
			
  $this -> EUI_Page ->_setQuery($sql);
  
  $flt = '';
  if( $this->URI->_get_have_post('keywords') )
  {
	$flt .=" AND ( 
				a.CallReasonCategoryCode LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.CallReasonCategoryName LIKE '%{$this->URI->_get_post('keywords')}%'  
				OR a.CallReasonInterest LIKE '%{$this->URI->_get_post('keywords')}%' 
				OR a.CallReasonCategoryFlags LIKE '%{$this->URI->_get_post('keywords')}%' 
				OR a.CallReasonCategoryOrder LIKE '%{$this->URI->_get_post('keywords')}%' 
				OR a.CallOutboundGoalsId LIKE '%{$this->URI->_get_post('keywords')}%' 
			)";
  }
  
  $this -> EUI_Page->_setWhere($flt);
  if( $this -> URI ->_get_have_post('order_by'))
  {
	$this -> EUI_Page->_setOrderBy($this ->URI->_get_post('order_by'),$this ->URI->_get_post('type'));
  }
  $this -> EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _getOutboundCategory( $CategoryId = null )
{ 
	$_conds = array();
	
	$this -> db ->select("*");
	$this -> db ->from("t_lk_customer_status a");
	$this -> db ->join("t_lk_outbound_goals b","a.CallOutboundGoalsId=b.OutboundGoalsId","LEFT");
	$this -> db ->where('b.Name','outbound');
	$this -> db ->where('a.CallActivityShow','1');
	
	
	// if true
	if(!is_null($CategoryId)) {
		$this -> db->where("a.CallReasonCategoryId", $CategoryId );
	}
	
	//echo $this -> db->_get_var_dump();
	
	foreach( $this ->db ->get() ->result_assoc() as $rows)
	{
		$_conds[$rows['CallReasonCategoryId']] = $rows['CallReasonCategoryName'];
	}
	
	return $_conds;
}

function _getCategoryByProject( $ProjectWork = NULL )
{
	$_conds = array();
	
	$this -> db ->select("*");
	$this -> db ->from("t_lk_customer_status a");
	$this -> db ->join("t_lk_outbound_goals b","a.CallOutboundGoalsId=b.OutboundGoalsId","LEFT");
	$this -> db ->where('b.Name','outbound');
	$this -> db ->where('a.CallActivityShow','1');
	
	if($ProjectWork!=NULL){
		$this -> db->where("a.CallReasonProjectId", $ProjectWork );
	}
	
	foreach( $this ->db ->get() ->result_assoc() as $rows){
		$_conds[$rows['CallReasonCategoryId']] = $rows['CallReasonCategoryName'];
	}
	
	return $_conds;
}

function _getOutboundCategoryByProject( $param = array() )
{ 
	$_conds = array();
	
	$this -> db ->select("*");
	$this -> db ->from("t_lk_customer_status a");
	$this -> db ->join("t_lk_outbound_goals b","a.CallOutboundGoalsId=b.OutboundGoalsId","LEFT");
	$this -> db ->where('b.Name','outbound');
	$this -> db ->where('a.CallActivityShow','1');
	
	
	// if true
	if(isset($param['CategoryId'])) {
		$this -> db->where("a.CallReasonCategoryId", $param['CategoryId'] );
	}
	
	if(isset($param['CampaignId']))
	{
		$ProjectWork = $this->getProjectByCampaign($param['CampaignId']);
		$this -> db->where("a.CallReasonProjectId", $ProjectWork );
	}
	
	//echo $this -> db->_get_var_dump();
	
	foreach( $this ->db ->get() ->result_assoc() as $rows)
	{
		$_conds[$rows['CallReasonCategoryId']] = $rows['CallReasonCategoryName'];
	}
	
	return $_conds;
}

function getProjectByCampaign($id)
{
	$projectId = 0;
	
	$sql = "select a.ProjectId from t_gn_campaign_project a where a.CampaignId = '".$id."'";
	$qry = $this->db->query($sql);
	
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$projectId = $rows['ProjectId'];
		}
	}
	
	return $projectId;
}
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
function _getInboundCategory()
{
	$_conds = array();
	
	$this -> db -> select("*");
	$this -> db -> from("t_lk_customer_status a");
	$this -> db -> join("t_lk_outbound_goals b","a.CallOutboundGoalsId=b.OutboundGoalsId","LEFT");
	$this -> db -> where('b.Name','inbound');
	$this -> db ->where('a.CallActivityShow','1');
	
	foreach( $this ->db ->get() ->result_assoc() as $rows)
	{
		$_conds[$rows['CallReasonCategoryId']] = $rows['CallReasonCategoryName'];
	}
	
	return $_conds;
} 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _setActive($data=array())
 {
	$_conds = 0;
	if(is_array($data))
	{
		foreach( $data['CategoryId'] as $keys => $CategoryId )
		{
			if( $this -> db ->update('t_lk_customer_status', 
				array('CallReasonCategoryFlags'=> $data['Active']), 
				array('CallReasonCategoryId'=>$CategoryId)
			))
			{
				$_conds++;
			}	
		}
	}	
		
	
	return $_conds;
 }
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _setDelete( $data=array() )
 {
	$_conds = 0;
	if(is_array($data))
	{
		foreach( $data as $keys => $CategoryId )
		{
			if( $this -> db ->delete('t_lk_customer_status', 
				array('CallReasonCategoryId'=>$CategoryId)
			)){
				$_conds++;
			}	
		}
	}	
		
	
	return $_conds;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _getOrder( $param=0 )
 {
	$_order = array();
	for($i = 0; $i <=(INT)$param; $i++) {
		$_order[$i] = $i;
	}
	
	return $_order;
 }
 
 function _getOuboundGoals() 
 {
	$_conds = array();
	$this -> db -> select('*');
	$this -> db -> from('t_lk_outbound_goals');
	foreach( $this -> db ->get()->result_assoc() as $rows ) 
	{
		$_conds[$rows['OutboundGoalsId']] = $rows['Description'];
	}
	
	return $_conds;
}
  
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _getCategoryInterest()
 {
	$_order = array( '1'=> 'YES','0' => 'NO');
	return $_order;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _getDataCategory( $CategoryId=0 )
 {
	$this ->db ->select('*');
	$this ->db ->from('t_lk_customer_status');
	$this ->db ->where('CallReasonCategoryId',$CategoryId);
	
	$_conds = array();
	if( $rows = $this -> db -> get() -> result_first_assoc() ) {
		$_conds = $rows;
	}
	return $_conds;
}
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _setSaveCategory($data = array() )
 {
	$_conds = 0;
	if( $this -> db -> insert('t_lk_customer_status', $data ))
	{
		$_conds++;
	}
	
	return $_conds;
 }
  /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */	
 
 function _setUpdateCategory($data = array() )
 {
	$_conds = 0; $_update = array(); $_where = array();
	foreach( $data as $fields => $values ) {
		if( ( $fields!='CallReasonCategoryId') )
			$_update[$fields] = $values; 
		else
			$_where[$fields]= $values;
	}
	
	if( $this -> db->update('t_lk_customer_status',$_update, $_where ))
	{
		$_conds++;
	}	
	
	return $_conds;
 }
 
 
}

?>