<?php
//--------------------------------------------------------------------------
/*
 * @ package 		class model report object 
 * @ auth 			uknown User 
 */
 
class M_RndReport extends EUI_Model
{
	public function modul_random()
	{
		return $this->M_MgtRandDeb->get_modul_random();
	}
	
	public function get_claim_access_all()
	{
		/*EXPLAIN
SELECT a.claim_id,
b.deb_id,
b.acc_no,
c.deb_id,
c.deb_acct_no,
d.CampaignDesc,
f.id AS OldAgent,
a.claim_date_ts,
e.id AS AgentClaim,
a.approval_date_ts,
g.AproveName AS ApprovalStatus,
a.bucket_trx_id,
i.bucket_trx_id,
i.access_all_id,
j.*
FROM t_gn_claim_debitur a 
INNER JOIN t_gn_buckettrx_debitur b ON a.bucket_trx_id=b.bucket_trx_id
LEFT JOIN t_gn_debitur c ON b.deb_id=c.deb_id
LEFT JOIN t_gn_campaign d ON c.deb_cmpaign_id =d.CampaignId
LEFT JOIN t_tx_agent e ON a.claim_by=e.UserId
LEFT JOIN t_tx_agent f ON a.from_owner=f.UserId
LEFT JOIN t_lk_aprove_status g ON a.approval_status=g.AproveCode
INNER JOIN t_gn_modul_setup h ON b.modul_setup_id=h.modul_setup_id
INNER JOIN t_gn_access_all i ON a.bucket_trx_id = i.bucket_trx_id
LEFT JOIN t_st_access_all j ON i.st_access_all_id=j.st_access_all_id

WHERE a.claim_date_ts like '2016-04-19%'*/
		
		$data=array();
		$start_claim_date = null;
		$end_claim_date = null;
		$report_type=array(0);
		if($this -> URI->_get_have_post("report_type"))
		{
			$report_type = $this -> URI->_get_post('report_type');
		}
		if(!$this -> URI->_get_array_post())
		{
			$start_claim_date = _getDateEnglish($this -> URI->_get_post('start_date_claim'));
			$end_claim_date = _getDateEnglish($this -> URI->_get_post('end_date_claim'));
			$this->db->reset_select();		
			$this->db->select(	"a.claim_id,
								b.deb_id,
								b.acc_no,
								c.deb_id,
								c.deb_acct_no,
								d.CampaignDesc,
								f.id AS OldAgent,
								a.claim_date_ts,
								e.id AS AgentClaim,
								a.approval_date_ts,
								g.AproveName AS ApprovalStatus,
								a.bucket_trx_id,
								i.bucket_trx_id,
								i.access_all_id"
			);
			$this->db->from("t_gn_claim_debitur a");
			$this->db->join("t_gn_buckettrx_debitur b ","a.bucket_trx_id=b.bucket_trx_id", "INNER");
			$this->db->join("t_gn_debitur c","b.deb_id=c.deb_id", "LEFT");
			$this->db->join("t_gn_campaign d","c.deb_cmpaign_id =d.CampaignId", "LEFT");
			$this->db->join("t_tx_agent e","a.claim_by=e.UserId", "LEFT");
			$this->db->join("t_tx_agent f","a.from_owner=f.UserId", "LEFT");
			$this->db->join("t_lk_aprove_status g","a.approval_status=g.AproveCode", "LEFT");
			$this->db->join("t_gn_modul_setup h","b.modul_setup_id=h.modul_setup_id", "INNER");
			$this->db->join("t_gn_access_all i","a.bucket_trx_id = i.bucket_trx_id", "INNER");
			$this->db->join("t_st_access_all j","i.st_access_all_id=j.st_access_all_id", "LEFT");
			$this->db->where("a.claim_date_ts BETWEEN '".$start_claim_date."' AND '".$end_claim_date."'");
			$this->db->where_in("h.modul_id_rnd",$report_type);
			$rs = $this->db->get();
			// echo $this->db->last_query();
			if( $rs->num_rows() > 0 )
			{
				$data = $rs->result_assoc();
			}
		}
		return $data;
	}
	
	public function get_claim_round()
	{
	}
}