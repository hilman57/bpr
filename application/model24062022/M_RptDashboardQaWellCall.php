<?php
class M_RptDashboardQaWellCall extends EUI_Model
{
	function M_RptDashboardQaWellCall()
	{
	
	}
	
	function _getReportType()
	{
		return array(
			'type_user_qa' => 'By QA'
			// 'type_followup' => 'By Follow Up'
		);
	}
	
	/* ===================================================================================================== */
	
	
	
	function _get_datas_fu($prm)
	{
		$datas = array();
		
		$sql = "SELECT 
					DATE(a.CustomerRejectedDate) AS CustomerRejectedDate, 
					if(d.FuGroupCode,d.FuGroupCode,'NULL') as FuGroupCode, 
					COUNT(a.CustomerId) AS TotalData, 
					SUM(IF(b.CampaignCode='WCDS1WC11411001',1,0)) AS BCA, 
					SUM(IF(b.CampaignCode='WCDS2WC11411001',1,0)) AS CIMB, 
					SUM(IF(b.CampaignCode='WCDS3WC11411001',1,0)) AS OTHERS, 
					SUM(IF(b.CampaignCode='WCDS4WC11411001',1,0)) AS CITIBANK, 
					SUM(IF(b.CampaignCode='WCDS5WC11411001',1,0)) AS AGENCY, 
					SUM(IF(b.CampaignCode='WCDS6WC01410001',1,0)) AS TEMPORELL
				FROM t_gn_debitur a
				LEFT JOIN t_gn_campaign b ON a.CampaignId = b.CampaignId
				LEFT JOIN t_gn_policy_detail c ON a.CustomerNumber = c.EnigmaId
				LEFT JOIN t_gn_followup d on a.CustomerId = d.FuCustId
				WHERE 1=1 
				AND c.CallComplete = 1 
				AND a.CallReasonQue IS NOT NULL 
				AND a.QueueId IS NOT NULL 
				AND DATE(a.CustomerRejectedDate) >= '2014-11-18' 
				AND DATE(a.CustomerRejectedDate) <= '2014-11-18'
				GROUP BY DATE(a.CustomerRejectedDate), d.FuGroupCode";
				
		$qry = $this->db->query($sql);
		
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['CustomerRejectedDate']][$rows['FuGroupCode']] = $rows;
			}
		}
		
		return $datas;
	}
}
?>