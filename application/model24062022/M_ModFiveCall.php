<?php
 /*
 * @ pack : _SaveFiveCall
 */ 

 
class M_ModFiveCall extends EUI_Model
{

/*
 * @ pack : _SaveFiveCall
 */ 

 function M_ModFiveCall()
{

}

/*
 * @ pack : _SaveFiveCall
 */ 
 
public function _getCounterFiveCall()
{
 
 $conds = 0;
 
 $this->db->reset_select(); //set default 
 $this->db->select('LastCounterCall', FALSE);
 $this->db->from('t_gn_favourite_call');
 $this->db->where('deb_id',_get_post('CustomerId'));
 $this->db->where('PhoneNumber',_get_post('PhoneNumber'));
 $this->db->where('LastAgentId',_get_session('UserId'));
 $this->db->where('LastTeamLeaderId',_get_session('TeamLeaderId'));
 $this->db->where('LastSupervisorId',_get_session('SupervisorId'));
 $this->db->where('LastCallDate',date('Y-m-d'));

/** @ pack : query result data **/
 
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 )
 {
	if( $rows = $qry->result_first_assoc() )
	{
		if((INT)$rows['LastCounterCall'] >= 5 )
		{
			$conds++;
		}	
	}
 }
 
 return $conds;
 
}

/* @ pack : _get_select_is_lock **/

 public function _get_select_db_is_lock()
{

 $conds  = 0;
 
 $this->db->reset_select(); //set default 	 
 $this->db->select('a.deb_is_lock',FALSE);
 $this->db->from('t_gn_debitur a ');
 $this->db->where('a.deb_id',_get_post('CustomerId')); 

 $qry = $this->db->get();
 if( $qry->num_rows() > 0 )
 {
	if( $rows = $qry->result_first_assoc() )
	{
		if((INT)$rows['deb_is_lock'] == 1 ) {
			$conds++;
		}	
	}
 }
 
 return $conds;
	 
}


/*
 * @ pack : _SaveFiveCall
 */ 
 
 public function _SaveFiveCall()
{
  $conds =0;
  
  $this->db->set('deb_id',_get_post('CustomerId'));
  $this->db->set('PhoneNumber',_get_post('PhoneNumber'));
  $this->db->set('LastCounterCall',1);
  $this->db->set('LastAgentId',_get_session('UserId'));
  $this->db->set('LastTeamLeaderId',_get_session('TeamLeaderId'));
  $this->db->set('LastSupervisorId',_get_session('SupervisorId'));
  $this->db->set('LastCallDate',date('Y-m-d H:i:s'));
  $this->db->set('BlockingStatus',0);
	
// @ pack : insert 

  $this->db->insert('t_gn_favourite_call' );
  if( $this->db->affected_rows()>0 )
  {
	if( $InsertId = $this->db->insert_id() ) 
	{
		$this->db->set('FavouriteId',$InsertId);
		$this->db->set('CounterCall',1);
		$this->db->set('AgentId',_get_session('UserId'));
		$this->db->set('TeamLeaderId',_get_session('TeamLeaderId'));
		$this->db->set('SupervisorId',_get_session('SupervisorId'));
		$this->db->set('CallDateTs',date('Y-m-d H:i:s'));
		$this->db->set('LastCallDateTs', date('Y-m-d H:i:s'));
		$this->db->insert('t_gn_favourite_history');
		if( $this->db->affected_rows()>0 ) {
			$conds++;
		}
	}
  } 
  else  
  {
	
	$this->db->reset_select(); // reset select 
	
 /* select data if duplicate **/
 	
	$this->db->select('id, deb_id, LastCounterCall');
	$this->db->from('t_gn_favourite_call');
	$this->db->where('deb_id',_get_post('CustomerId'));
	$this->db->where('PhoneNumber',_get_post('PhoneNumber'));
	$this->db->where('LastAgentId',_get_session('UserId'));
	$this->db->where('LastTeamLeaderId',_get_session('TeamLeaderId'));
	$this->db->where('LastSupervisorId',_get_session('SupervisorId'));
	$this->db->where('LastCallDate',date('Y-m-d'));
	
	
 /* @ pack : result query **/
	
	$qry = $this->db->get();
	foreach( $qry->result_assoc() 
		AS $rows )
	{
		$counter = (((INT)$rows['LastCounterCall'])+1); // set counter 
		$blocking = ( $counter==5 ? 1 : 0 );
		
		if( $counter )
		{
			$this->db->set('FavouriteId',$rows['id']);
			$this->db->set('CounterCall', $counter);
			$this->db->set('AgentId',_get_session('UserId'));
			$this->db->set('TeamLeaderId',_get_session('TeamLeaderId'));
			$this->db->set('SupervisorId',_get_session('SupervisorId'));
			$this->db->set('CallDateTs',date('Y-m-d H:i:s'));
			$this->db->insert('t_gn_favourite_history');
			if( $this->db->affected_rows()>0 ) 
			{
				$this->db->set('LastCounterCall',$counter);
				$this->db->set('LastCallDateTs', date('Y-m-d H:i:s'));
				$this->db->set('BlockingStatus', $blocking);
				$this->db->where('id', $rows['id']);
				
				if( $this->db->update('t_gn_favourite_call') )
				{
					$this->db->set('deb_is_lock', $blocking );
					$this->db->set('deb_lock_type', 104);
					$this->db->set("deb_lock_date_ts", date('Y-m-d H:i:s'));
					$this->db->where('deb_id', $rows['deb_id']);
					$this->db->update('t_gn_debitur');
				}
			}
			
			$conds++; 
		 }
			
	 }
 }
	
	return $conds;
} //===> _SaveFiveCall


// END CLASS 

}

?>