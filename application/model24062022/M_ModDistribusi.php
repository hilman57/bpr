<?php
/*
 * E.U.I 
 *
 
 * subject	: get SetCampaign modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 * 
 
 * @ notes  : update and add function method for distribute of remain data
 * @ author : omens < jombi_par@yahoo.com > 
 * @ line 	: # 1154
 */
 
 
class M_ModDistribusi extends EUI_Model
{

 private static $params = NULL;

/*
 * EUI :: _setDistribusi () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
 function M_ModDistribusi()
{
	$this -> load -> model(array('M_SysUser'));
 }
 
/*
 * EUI :: _setDistribusi () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	 
  
public function _setDistribusi( $params = null, $action = null )
{
	$_conds = 0;
	if( !is_null($params)) self::$params = $params;
	if(!is_null($action) ) 
	{
		switch($action)
		{
			case 'saveByAmount' : 
				$_conds =& self::SaveByAmount();  
			break;  

			case 'saveByAmountATM' : 
				$_conds =& self::SaveByAmountATM($params['atm']);  
			break;  
			
			case 'saveByChecked' : 
				$_conds =& self::SaveByChecked(); 
			break;	

			case 'SaveByCheckedATM' : 
				$_conds =& self::SaveByCheckedATM($params['atm']); 
			break;	
			
			case 'ManualDistribusi' :
				$_conds =& self::ManualDistribusi();  
			break;
			
			case 'AutomaticDistribusi' : 
				$_conds =& self::AutomaticDistribusi();  
			break;	
		}
	}
	
	return $_conds;
}

/*
 * EUI :: setRand 
 * -----------------------------------------
 *
 * @ def	 : function get detail content list page 
 * @ param	 : not assign parameter
 * -----------------------------------------
 
 * @ update  : acak berlaku jika jumlah yang akan di acak lebih dari satu 
 *			   maka jika tidak  maka biarkan default.	
 */	 
 
protected function setArrayRandom( $arrays = null )
{
	static $is_rand = array();
	if( is_array($arrays) )
	{
		$rands = array_rand($arrays, count($arrays));
		if( is_array($rands) AND count($arrays) > 1 ) {
			$num = 0;
			foreach($rands as $k => $stack ) {
				$is_rand[$k] = $arrays[$stack];
				$num++;
			}
		}
		else {
			$rands = $arrays; $num = 0; 
			foreach($rands as $k => $stack ) {
				$is_rand[$k] = $arrays[$k];
				$num++;
			}
		}
	}
	
	return $is_rand;
	
}

 
/*
 * EUI :: _setReAssigment () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	 
  
public function _setReAssigment( $params = null, $action = null )
{
	$_conds = 0;
	if( !is_null($params)) self::$params = $params;
	if(!is_null($action) ) 
	{
		switch($action)
		{
			case 'ManualReassignment' :
				$_conds =& self::ManualReassignment();  
			break;
			
			case 'AutomaticReassignment' : 
				$_conds =& self::AutomaticReassignment();  
			break;	
		}
	}
	
	return $_conds;
}

/*
 * EUI :: _get_bucket_list_sintax() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
private function AutomaticReassignment()
{
/** set array method **/
 
 $UserIsReady = array(); $UserData = array();
 
 if((self::$params['DistribusiMode']==2) ) {
	 $UserIsReady = self::setArrayRandom(self::$params['UserSelectId']);
  }
  else{
	 $UserIsReady = self::$params['UserSelectId'];
  }
  
 
 if(( count($UserIsReady)!=0) 
    AND (self::$params['AssignData']!='') 
	AND (self::$params['AssignData']!=0))
  {
	$assign_limit 	= array_slice(self::$params['Data'], 0, self::$params['AssignData']);
	$QtyPerUser 	= (INT)(self::$params['AssignData']/count($UserIsReady)); 
	if( $QtyPerUser > 0 )
	{
		$start = 0;
		foreach( $UserIsReady as $k => $vUser )
		{
			$start_data = (($start)*($QtyPerUser));
			$limit_data = (($QtyPerUser)-1);
			$post_data = $start_data;
			$limit_assign = 0;
			
			/* @ def : set on limit data **/
			
			while(true) 
			{
				$UserData[$vUser][] = $assign_limit[$post_data]; 
				if(($limit_assign==$limit_data)) break;
					$post_data+=1;	
					$limit_assign+=1;
			}
			
			$start++;	
		}
	}
  }
  
  
/** bagikan data sisanya ke user  secara acak OK ***/ 
  
  $start_assign  = (($QtyPerUser)* count($UserIsReady));
  $assign_offset = (count($assign_limit) - $start_assign);
  $assign_divider = NULL;
  if( $assign_offset > 0 )
  {
	$assign_divider = array_slice($assign_limit,$start_assign, $assign_offset);
	if(is_array($assign_divider))
	{
		if( $CallerId = $UserIsReady )
		{
			if( $_call_user_array = $this->_getSelectCallerAgentByRandom( $CallerId ) )
			{
				$rec_assign_user = array_slice($_call_user_array, 0, count($assign_divider));	
				foreach($rec_assign_user as $k => $UserAssignId ) 
				{
					$UserData[$UserAssignId][] = $assign_divider[$k]; 
				}
			}	
		}
	}
  }
  
  /*
   * @ def 	   : action start distribusi data 
   * -----------------------------------------------
   *
   * @ param   : then will update && save to log 
   * @ aksess  : look here 
   */
 //  print_r($UserData);

  $_success = 0;
  
  if( is_array($UserData) AND count($UserData) > 0 )
  {
	foreach($UserData as $UserId => $Datas )
	{	
		foreach($Datas as $key => $AssignId )
		{
			$UserDetail = $this -> M_SysUser -> _getUserDetail($UserId);
			if( $UserDetail )
			{
				if( self::$params['UserLevel']==USER_ACCOUNT_MANAGER ){
					$_update = array
					(
						'AssignAmgr'  => $UserDetail['act_mgr'], 
						'AssignMode' => 'MOV', 
						'AssignDate' => date('Y-m-d H:i:s')
					);
				}
				
				if( self::$params['UserLevel']==USER_MANAGER ){
					$_update = array
					(
						'AssignMgr'  => $UserDetail['mgr_id'], 
						'AssignAmgr' => $UserDetail['act_mgr'], 
						'AssignMode' => 'MOV', 
						'AssignDate' => date('Y-m-d H:i:s')
					);
				}
				
				if( self::$params['UserLevel']==USER_SUPERVISOR ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignMode' 	=> 'MOV', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
				
				if( self::$params['UserLevel']==USER_LEADER ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignLeader' 	=> $UserDetail['tl_id'],
						'AssignMode' 	=> 'MOV', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
				
				
				if( self::$params['UserLevel']==USER_AGENT_OUTBOUND ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignLeader' 	=> $UserDetail['tl_id'],
						'AssignSelerId' => $UserDetail['UserId'],
						'AssignMode' 	=> 'MOV', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
						
				if( self::$params['UserLevel']==USER_AGENT_INBOUND ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignSelerId' => $UserDetail['UserId'],
						'AssignLeader' 	=> $UserDetail['tl_id'],
						'AssignMode' 	=> 'MOV', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
			}
			
			/* @ def : update assignment data **/
			
				if( $this -> db -> update('t_gn_assignment',$_update, 
					array('AssignId' => $AssignId)
				))
				{
					if( self::_setSaveLog( array('AssignId'=>$AssignId,'UserId'=> $UserDetail['UserId']) , 'REASSIGNMENT.AUTOMATIC'))
					{ 
						$_success++;
					}	
				}
		}
	}	
  }
  
  return array( 
	'SizeData'  => $_success, 
	'SizeUsers' => count($UserIsReady)
  );
  
}

/*
 * EUI :: _get_bucket_list_sintax() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	

private function ManualReassignment()
{

/** set array method **/

 $_success = 0;  $UserData = array();
 $UserIsReady = array();
 
 if((self::$params['DistribusiMode']==2) ) {
	 $UserIsReady = self::setArrayRandom(self::$params['UserSizeData']);
  }
  else{
	 $UserIsReady = self::$params['UserSizeData'];
  }
  
  
  if( is_array($UserIsReady) 
		AND !is_null($UserIsReady))
	{
		foreach( $UserIsReady as $key => $rows )
		{
			$array_user_id[$key] = $rows['userid']; 
			$array_size_id[$key] = $rows['size'];	
		}
				
		/* @ def : urutkan size_data secra ASC ex: 0,1,2,3,4 */
		$array_multisort = array_multisort($array_size_id, SORT_ASC, $array_user_id, SORT_ASC, $UserIsReady); 
		if( $array_multisort )
		{
			$QtyDataAsg = self::$params['Data'];
			$start_post= 0;
				
			foreach($UserIsReady as $rows )
			{
				if( $start_post==0 )
				{
					$start =0;
					$post_size = ($rows['size']-1);
					while(true)
					{
						$UserData[$rows['userid']][] = $QtyDataAsg[$start]; 
						if( $start==$post_size ) BREAK;
							$start+=1;
					}
				}	
				else
				{
					$post_size = ($rows['size']+$start);
					$start = $start+1;
					while(true) 
					{
						$UserData[$rows['userid']][] = $QtyDataAsg[$start]; 
						if( $start==$post_size ) BREAK;
						$start+=1;
					}
				}
						
				$start_post+=1;
			}	
			
		/* @ def 	   : action start distribusi data 
		 * -----------------------------------------------
		 *
		 * @ param   : then will update && save to log 
		 * @ aksess  : look here 
		 */
		  
		if( is_array($UserData) AND count($UserData) > 0 )
		{
			foreach($UserData as $UserId => $Datas )
			{	
				foreach($Datas as $key => $AssignId )
				{
					$UserDetail = $this -> M_SysUser -> _getUserDetail($UserId);
					if( $UserDetail )
					{
						if( self::$params['UserLevel']==USER_ACCOUNT_MANAGER){
							$_update = array (
								'AssignMgr'  => $UserDetail['mgr_id'], 
								'AssignAmgr' => $UserDetail['act_mgr'], 
								'AssignMode' => 'MOV', 
								'AssignDate' => date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_MANAGER ){
							$_update = array
							(
								'AssignMgr'  => $UserDetail['mgr_id'], 
								'AssignMode' => 'MOV', 
								'AssignDate' => date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_SUPERVISOR ){
							$_update = array
							(
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignMode' 	=> 'MOV', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_LEADER ){
							$_update = array
							(
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignLeader' 	=> $UserDetail['tl_id'],
								'AssignMode' 	=> 'MOV', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_AGENT_OUTBOUND ){
							$_update = array
							(
								
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignLeader' 	=> $UserDetail['tl_id'],
								'AssignSelerId' => $UserDetail['UserId'],
								'AssignMode' 	=> 'MOV', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_AGENT_INBOUND ){
							$_update = array
							(
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignLeader' 	=> $UserDetail['tl_id'],
								'AssignSelerId' => $UserDetail['UserId'],
								'AssignMode' 	=> 'MOV', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
					}
					
					/* @ def : update assignment data **/
					
						if( $this -> db -> update('t_gn_assignment',$_update, 
							array('AssignId' => $AssignId)
						))
						{
							if( self::_setSaveLog( array('AssignId'=>$AssignId,'UserId'=> $UserDetail['UserId']) , 'REASSIGNMENT.MANUAL' ))
							{ 
								$_success++;
							}	
						}
				}
			}	
		}}
	}
	
	return array( 
		'SizeData' => $_success, 
		'SizeUsers' => count($UserIsReady) 
	);
}


/*
 * EUI :: _get_bucket_list_sintax() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
  
 private function _get_bucket_list_sintax()
 {
	$_conds = array();
	
	$this -> db -> select('*');
	$this -> db -> from('t_gn_bucket_customers');
	
	if( self::$params['FilenameId']!=FALSE)
		$this -> db -> where('FTP_UploadId', self::$params['FilenameId']);
		
	if( !is_null(self::$params['AssignStatus']))
		$this -> db -> where('AssignCampaign', self::$params['AssignStatus']);
	
	if( self::$params['StartDate'] AND self::$params['EndDate'] )
	{
		$this -> db -> where( "date(CustomerUploadedTs)>=", $this ->EUI_Tools ->_date_english( self::$params['StartDate']) );
		$this -> db -> where( "date(CustomerUploadedTs)<=", $this ->EUI_Tools ->_date_english( self::$params['EndDate']) );
	}	
	
	$this -> db -> limit(self::$params['AmountAssign']);
	foreach( $this -> db -> get() -> result_assoc() as $rows){
		$_conds[$rows['CustomerId']]= $rows;
	}
	
	return $_conds;
 } 
 
/*
 * EUI :: saveByAmount () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
  
function _get_field_customers()
{ 
	$field = $this->db->list_fields('t_gn_debitur');
	foreach( $field as $k => $v )
	{
		$_conds[$v] = $v;
	}
	
	return $_conds;
}		


		
/*
 * EUI :: saveByAmount () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 private function _select_count_data($array = null )
 {
	$_totals = 0;
	if(!is_null($array))
	{
		$this ->db ->select('CustomerId');
		$this ->db ->from('t_gn_debitur');
		$this ->db ->where('CustomerNumber',$array['CustomerNumber']);
		$this ->db ->where('CampaignId',$array['CampaignId']);
		
		$i = 1;
		foreach($this ->db ->get() -> result_assoc() as $rows )
		{
			$_conds[$i] = (INT)$rows['CustomerId'];
			$i++;
		}
		
		$_totals = count($_conds);
	}
	
	return $_totals;
	
 }
  
 
/*
 * EUI :: saveByAmount () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
private function _get_bucket_list_checked()
{
	$_bucket = array();
	if(!is_null(self::$params))
	{
		$this ->db ->select('*');
		$this ->db ->from('t_gn_bucket_customers');
		$this ->db ->where_in('CustomerId',self::$params['BucketId']);
		
		foreach( $this -> db -> get() -> result_assoc() as $rows)
		{
			$_bucket[$rows['CustomerId']]= $rows;
		}
	}
	
	return $_bucket;
}

/*
 * EUI :: saveByAmount () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	

private function SaveByCheckedATM($atm=''){
  
  $UserDetail = $this -> M_SysUser -> _getUserDetail($atm);

  $_totals = 0;	$_duplicate = 0;
  if( !is_null(self::$params) )
  {
	$_data_customer =& $this->_get_field_customers();
	$_data_bucket =& $this->_get_bucket_list_checked();
	
	if( is_array($_data_bucket) )
	{
		/* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		
		$_list_data = null; $_bucket_id = null;
		$i = 1;
		foreach( $_data_bucket as $k => $rows ) 
		{
			foreach($rows as $keys => $values ) 
			{
				if( in_array($keys,$_data_customer) 
					AND $keys!='CustomerId' )
				{
					$_list_data[$i][$keys] = $values;
				}
				$_list_data[$i]['CampaignId'] = self::$params['CampaignId']; 
				$_bucket_id[$i]['BucketId'] = $rows['CustomerId']; 
			}
			$i++;
		}
		
		 /* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		if( count($_list_data)> 0 AND !is_null($_list_data))
		{
			foreach($_list_data as $k => $_insert_data )
			{
				if( $this->_select_count_data( 
					array
					(
						'CustomerNumber' => $_insert_data['CustomerNumber'] ,
						'CampaignId' => $_insert_data['CampaignId'] 
					))!=TRUE)
				{ 
					if( $this -> db -> insert('t_gn_debitur', $_insert_data) )
					{
						$insertId = $this->db->insert_id();
						if( $insertId )
						{
								$this -> db -> insert('t_gn_assignment',
								array(
									'CustomerId'=> $insertId, 
									'AssignAdmin'=> $this -> EUI_Session -> _get_session('UserId'),
									'AssignAmgr' => $UserDetail['act_mgr'], 
									'AssignMgr' 	=> $UserDetail['mgr_id'], 
									'AssignSpv' 	=> $UserDetail['spv_id'],
									'AssignMode' 	=> 'DIS', 
									'AssignDate' 	=> date('Y-m-d H:i:s')
								));

						}
						
						if( $this -> db -> update('t_gn_bucket_customers', 
							array('AssignCampaign'=>1),
							array('CustomerId' => $_bucket_id[$k]['BucketId'])
						))
						{
							$this -> db -> insert('t_gn_bucket_assigment', 
							array
							(
								'CustomerBucketId' => $_bucket_id[$k]['BucketId'],
								'CustomerCampaignId' => $_insert_data['CampaignId'],
								'CreatedDateTs' => date('Y-m-d H:i:s'),
								'CreateUserId' => $this -> EUI_Session->_get_session('UserId')
							));
							
							$_totals++;
						}
					}
				}
				else{
					$_duplicate++;
				}
			}
		}
	}
  }
    // return data in array 
  
  return array
  (
	'_success' => $_totals, 
	'_duplicate'=> $_duplicate
  );
} 

  
private function SaveByChecked(){
  
  $_totals = 0;	$_duplicate = 0;
  if( !is_null(self::$params) )
  {
	$_data_customer =& $this->_get_field_customers();
	$_data_bucket =& $this->_get_bucket_list_checked();
	
	if( is_array($_data_bucket) )
	{
		/* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		
		$_list_data = null; $_bucket_id = null;
		$i = 1;
		foreach( $_data_bucket as $k => $rows ) 
		{
			foreach($rows as $keys => $values ) 
			{
				if( in_array($keys,$_data_customer) 
					AND $keys!='CustomerId' )
				{
					$_list_data[$i][$keys] = $values;
				}
				$_list_data[$i]['CampaignId'] = self::$params['CampaignId']; 
				$_bucket_id[$i]['BucketId'] = $rows['CustomerId']; 
			}
			$i++;
		}
		
		 /* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		if( count($_list_data)> 0 AND !is_null($_list_data))
		{
			foreach($_list_data as $k => $_insert_data )
			{
				if( $this->_select_count_data( 
					array
					(
						'CustomerNumber' => $_insert_data['CustomerNumber'] ,
						'CampaignId' => $_insert_data['CampaignId'] 
					))!=TRUE)
				{ 
					if( $this -> db -> insert('t_gn_debitur', $_insert_data) )
					{
						$insertId = $this->db->insert_id();
						if( $insertId )
						{
								$this -> db -> insert('t_gn_assignment',
								array(
									'CustomerId'=> $insertId, 
									'AssignAdmin'=> $this -> EUI_Session -> _get_session('UserId')
								));
						}
						
						if( $this -> db -> update('t_gn_bucket_customers', 
							array('AssignCampaign'=>1),
							array('CustomerId' => $_bucket_id[$k]['BucketId'])
						))
						{
							$this -> db -> insert('t_gn_bucket_assigment', 
							array
							(
								'CustomerBucketId' => $_bucket_id[$k]['BucketId'],
								'CustomerCampaignId' => $_insert_data['CampaignId'],
								'CreatedDateTs' => date('Y-m-d H:i:s'),
								'CreateUserId' => $this -> EUI_Session->_get_session('UserId')
							));
							
							$_totals++;
						}
					}
				}
				else{
					$_duplicate++;
				}
			}
		}
	}
  }
  
   
  // return data in array 
  
  return array
  (
	'_success' => $_totals, 
	'_duplicate'=> $_duplicate
  );
} 

 
/*
 * EUI :: saveByAmount () 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
  
private function SaveByAmount(){
  $_totals = 0;	$_duplicate = 0;
  
  if( !is_null(self::$params) )
  {
	$_data_customer =& $this->_get_field_customers();
	$_data_bucket =& $this->_get_bucket_list_sintax();
	if( is_array($_data_bucket) )
	{
	
	   /* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		
		$_list_data = null; $_bucket_id = null;
		$i = 1;
		foreach( $_data_bucket as $k => $rows ) 
		{
			foreach($rows as $keys => $values ) 
			{
				if( in_array($keys,$_data_customer) 
					AND $keys!='CustomerId' )
				{
					$_list_data[$i][$keys] = $values;
				}
				$_list_data[$i]['CampaignId'] = self::$params['CampaignId']; 
				$_bucket_id[$i]['BucketId'] = $rows['CustomerId']; 
			}
			$i++;
		}
		
	   /* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		if( count($_list_data)> 0 AND !is_null($_list_data))
		{
			foreach($_list_data as $k => $_insert_data )
			{
				if( $this->_select_count_data( 
					array
					(
						'CustomerNumber' => $_insert_data['CustomerNumber'] ,
						'CampaignId' => $_insert_data['CampaignId'] 
					))!=TRUE)
				{ 
					if( $this -> db -> insert('t_gn_debitur', $_insert_data) )
					{
						$insertId = $this->db->insert_id();
						if( $insertId )
						{
								$this -> db -> insert('t_gn_assignment',
								array(
									'CustomerId'=> $insertId, 
									'AssignAdmin'=> $this -> EUI_Session -> _get_session('UserId')
								));
						}
						
						if( $this -> db -> update('t_gn_bucket_customers', 
							array('AssignCampaign'=>1),
							array('CustomerId' => $_bucket_id[$k]['BucketId'])
						))
						{
							$this -> db -> insert('t_gn_bucket_assigment', 
							array
							(
								'CustomerBucketId' => $_bucket_id[$k]['BucketId'],
								'CustomerCampaignId' => $_insert_data['CampaignId'],
								'CreatedDateTs' => date('Y-m-d H:i:s'),
								'CreateUserId' => $this -> EUI_Session->_get_session('UserId')
							));
							
							$_totals++;
						}
					}
				}
				else{
					$_duplicate++;
				}
			}
		}
	}	
  }
  
  // return data in array 
  
  return array
  (
	'_success' => $_totals, 
	'_duplicate'=> $_duplicate
  );
  
}


private function SaveByAmountATM($atm='') {
	$UserDetail = $this -> M_SysUser -> _getUserDetail($atm);

  $_totals = 0;	$_duplicate = 0;
  
  if( !is_null(self::$params) )
  {
	$_data_customer =& $this->_get_field_customers();
	$_data_bucket =& $this->_get_bucket_list_sintax();
	if( is_array($_data_bucket) )
	{
	
	   /* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		
		$_list_data = null; $_bucket_id = null;
		$i = 1;
		foreach( $_data_bucket as $k => $rows ) 
		{
			foreach($rows as $keys => $values ) 
			{
				if( in_array($keys,$_data_customer) 
					AND $keys!='CustomerId' )
				{
					$_list_data[$i][$keys] = $values;
				}
				$_list_data[$i]['CampaignId'] = self::$params['CampaignId']; 
				$_bucket_id[$i]['BucketId'] = $rows['CustomerId']; 
			}
			$i++;
		}
		
	   /* 
		* @ def 	: masp columns to data customer 
		* ------------------------------------------
		*
		* @ param  : no comment 
		* @ aksess : try again
		*/
		if( count($_list_data)> 0 AND !is_null($_list_data))
		{
			foreach($_list_data as $k => $_insert_data )
			{
				if( $this->_select_count_data( 
					array
					(
						'CustomerNumber' => $_insert_data['CustomerNumber'] ,
						'CampaignId' => $_insert_data['CampaignId'] 
					))!=TRUE)
				{ 
					if( $this -> db -> insert('t_gn_debitur', $_insert_data) )
					{
						$insertId = $this->db->insert_id();
						if( $insertId )
						{
								$this -> db -> insert('t_gn_assignment',
								array(
									'CustomerId'	=> $insertId, 
									'AssignAdmin'	=> $this -> EUI_Session -> _get_session('UserId'),
									'AssignAmgr' 	=> $UserDetail['act_mgr'], 
									'AssignMgr' 	=> $UserDetail['mgr_id'], 
									'AssignSpv' 	=> $UserDetail['spv_id'],
									'AssignMode' 	=> 'DIS', 
									'AssignDate' 	=> date('Y-m-d H:i:s')
								));
						}
						
						if( $this -> db -> update('t_gn_bucket_customers', 
							array('AssignCampaign'=>1),
							array('CustomerId' => $_bucket_id[$k]['BucketId'])
						))
						{
							$this -> db -> insert('t_gn_bucket_assigment', 
							array
							(
								'CustomerBucketId' => $_bucket_id[$k]['BucketId'],
								'CustomerCampaignId' => $_insert_data['CampaignId'],
								'CreatedDateTs' => date('Y-m-d H:i:s'),
								'CreateUserId' => $this -> EUI_Session->_get_session('UserId')
							));
							
							$_totals++;
						}
					}
				}
				else{
					$_duplicate++;
				}
			}
		}
	}	
  }
  
  // return data in array 
  
  return array
  (
	'_success' => $_totals, 
	'_duplicate'=> $_duplicate
  );
  
}

/*
 * @def		 : _setDistribusi from assigment 
 *				agent look on agent assigment () 
 * -----------------------------------------
 *
 * @ def	 : function get detail content list page 
 * @ param	 : not assign parameter
 */	
  
  

protected function _getSelectCallerAgentByRandom( $User = null ) {
	static $config = null;
	
	if(!is_null($User) AND is_array($User) )
	{
		$this->db->reset_select(); // clear of begin select query .
		$this->db->select('a.UserId');
		$this->db->from('t_tx_agent a');
		$this->db->where_in('a.UserId', $User);
		$this->db->order_by('RAND()');
		foreach( $this->db->get()->result_assoc() as $rows ){
			$config[$rows['UserId']] = $rows['UserId'];
		}
	}
	
	return $config;
}



/*
 * @def		 : set User Agent to random method if have remain of data  
 * -----------------------------------------
 *
 * @ param	 : - 	
 * @ param	 : - 
 */	
 
public function ManualDistribusi() {

 $_success = 0;  $UserData = array(); $UserIsReady = array();
 
 if((self::$params['DistribusiMode']==2) ) {
	 $UserIsReady = self::setArrayRandom(self::$params['UserSelect']);
  }
  else{
	 $UserIsReady = self::$params['UserSelect'];
  }
  
	if( is_array( $UserIsReady ) 
		AND !is_null( $UserIsReady ) )
	{
		foreach( $UserIsReady as $key => $rows )
		{
			$array_user_id[$key] = $rows['userid']; 
			$array_size_id[$key] = $rows['size'];	
		}
				
		/* @ def : urutkan size_data secara ASC ex: 0,1,2,3,4 */
		$array_multisort = array_multisort($array_size_id, SORT_ASC, $array_user_id, SORT_ASC, $UserIsReady ); 
		if( $array_multisort )
		{
			$QtyDataAsg = self::$params['Data'];
			$start_post= 0;
				
			foreach($UserIsReady as $rows )
			{
				if( $start_post==0 )
				{
					$start =0;
					$post_size = ($rows['size']-1);
					while(true)
					{
						$UserData[$rows['userid']][] = $QtyDataAsg[$start]; 
						if( $start==$post_size ) BREAK;
							$start+=1;
					}
				}	
				else
				{
					$post_size = ($rows['size']+$start);
					$start = $start+1;
					while(true) 
					{
						$UserData[$rows['userid']][] = $QtyDataAsg[$start]; 
						if( $start==$post_size ) BREAK;
						$start+=1;
					}
				}
						
				$start_post+=1;
			}	
		/* @ def 	   : action start distribusi data 
		 * -----------------------------------------------
		 *
		 * @ param   : then will update && save to log 
		 * @ aksess  : look here 
		 */
		  
		if( is_array($UserData) AND count($UserData) > 0 )
		{
			foreach($UserData as $UserId => $Datas )
			{	
				foreach($Datas as $key => $AssignId )
				{
					$UserDetail = $this -> M_SysUser -> _getUserDetail($UserId);
					if( $UserDetail )
					{
						if( self::$params['UserLevel']==USER_ACCOUNT_MANAGER ){
							$_update = array
							(
								'AssignAmgr'  => $UserDetail['act_mgr'], 
								'AssignMode' => 'DIS', 
								'AssignDate' => date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_MANAGER ){
							$_update = array
							(
								'AssignAmgr'  => $UserDetail['act_mgr'], 
								'AssignMgr'  => $UserDetail['mgr_id'], 
								'AssignMode' => 'DIS', 
								'AssignDate' => date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_SUPERVISOR ){
							$_update = array
							(
								'AssignAmgr'  	=> $UserDetail['act_mgr'], 
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignMode' 	=> 'DIS', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_LEADER ){
							$_update = array
							(
								'AssignAmgr' 	=> $UserDetail['act_mgr'], 
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignLeader' 	=> $UserDetail['tl_id'],
								'AssignMode' 	=> 'DIS', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_AGENT_OUTBOUND ){
							$_update = array
							(
								'AssignAmgr'  	=> $UserDetail['act_mgr'], 	
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignLeader' 	=> $UserDetail['tl_id'],
								'AssignSelerId' => $UserDetail['UserId'],
								'AssignMode' 	=> 'DIS', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
						
						if( self::$params['UserLevel']==USER_AGENT_INBOUND ){
							$_update = array
							(
								'AssignAmgr'  	=> $UserDetail['act_mgr'], 
								'AssignMgr' 	=> $UserDetail['mgr_id'], 
								'AssignSpv' 	=> $UserDetail['spv_id'],
								'AssignLeader' 	=> $UserDetail['tl_id'],
								'AssignSelerId' => $UserDetail['UserId'],
								'AssignMode' 	=> 'DIS', 
								'AssignDate' 	=> date('Y-m-d H:i:s')
							);
						}
					}
					
					/* @ def : update assignment data **/
					
						if( $this -> db -> update('t_gn_assignment',$_update, 
							array('AssignId' => $AssignId)
						))
						{
							if( self::_setSaveLog( array('AssignId'=>$AssignId,'UserId'=> $UserDetail['UserId']), 'ASSIGNMENT.MANUAL' ) )
							{ 
								$_success++;
							}	
						}
				}
			}	
		}}
	}
	
	return array( 
		'SizeData' => $_success, 
		'SizeUsers' => count($UserIsReady) 
	);
}


/*
 * @def		 : _setDistribusi from assigment 
 *				agent look on agent assigment () 
 * -----------------------------------------
 *
 * @ def	 : function get detail content list page 
 * @ param	 : not assign parameter
 */	
  
public function AutomaticDistribusi() {
  
  $UserData = array(); $UserIsReady = array();
  
/** with methode Random array user position **/

  if( self::$params['DistribusiMode']==2 ){
	 $UserIsReady = self::setArrayRandom(self::$params['UserSelectId']);
  }
  else{
	 $UserIsReady = self::$params['UserSelectId'];
  }
  
  if(( count(self::$params['UserSelectId'])!=0) 
    AND (self::$params['AssignData']!='') 
	AND (self::$params['AssignData']!=0))
  {
  
  $assign_limit = array_slice(self::$params['Data'], 0, self::$params['AssignData']);
  $QtyPerUser 	= (INT)(self::$params['AssignData']/count($UserIsReady)); 
	
	if( $QtyPerUser > 0 )
	{
		$start = 0;
		foreach( $UserIsReady as $k => $vUser )
		{
			$start_data = (($start)*($QtyPerUser));
			$limit_data = (($QtyPerUser)-1);
			$post_data = $start_data;
			$limit_assign = 0;
			
			/* @ def : set on limit data **/
			
			while(true) 
			{
				$UserData[$vUser][] = $assign_limit[$post_data]; 
				if(($limit_assign==$limit_data)) break;
					$post_data+=1;	
					$limit_assign+=1;
			}
			
			$start++;	
		}
	}
  }
  
/** bagikan data sisanya ke user  secara acak OK ***/ 
  
  $start_assign  = (($QtyPerUser)* count($UserIsReady));
  $assign_offset = (count($assign_limit) - $start_assign);
  $assign_divider = NULL;
  if( $assign_offset > 0 )
  {
	$assign_divider = array_slice($assign_limit,$start_assign, $assign_offset);
	if(is_array($assign_divider))
	{
		if( $CallerId = $UserIsReady )
		{
			if( $_call_user_array = $this->_getSelectCallerAgentByRandom( $CallerId ) )
			{
				$rec_assign_user = array_slice($_call_user_array, 0, count($assign_divider));	
				foreach($rec_assign_user as $k => $UserAssignId ) 
				{
					$UserData[$UserAssignId][] = $assign_divider[$k]; 
				}
			}	
		}
	}
  }	
  
  /*
   * @ def 	   : action start distribusi data 
   * -----------------------------------------------
   *
   * @ param   : then will update && save to log 
   * @ aksess  : look here 
   */
   
  $_success = 0;
  
  if( is_array($UserData) AND count($UserData) > 0 )
  {
	foreach($UserData as $UserId => $Datas )
	{	
		foreach($Datas as $key => $AssignId )
		{
			$UserDetail = $this -> M_SysUser -> _getUserDetail($UserId);
			if( $UserDetail )
			{
				if( self::$params['UserLevel']==USER_MANAGER ){
					$_update = array
					(
						'AssignMgr'  => $UserDetail['mgr_id'], 
						'AssignAmgr'  	=> $UserDetail['act_mgr'], 
						'AssignMode' => 'DIS', 
						'AssignDate' => date('Y-m-d H:i:s')
					);
				}
				
				if( self::$params['UserLevel']==USER_SUPERVISOR ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignAmgr'  	=> $UserDetail['act_mgr'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignMode' 	=> 'DIS', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
				
				if( self::$params['UserLevel']==USER_LEADER ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignLeader'	=> $UserDetail['tl_id'],
						'AssignAmgr'  	=> $UserDetail['act_mgr'], 
						'AssignMode' 	=> 'DIS', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
				
				if( self::$params['UserLevel']==USER_AGENT_OUTBOUND ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignAmgr'  	=> $UserDetail['act_mgr'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignLeader'	=> $UserDetail['tl_id'],
						'AssignSelerId' => $UserDetail['UserId'],
						'AssignMode' 	=> 'DIS', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
						
				if( self::$params['UserLevel']==USER_AGENT_INBOUND ){
					$_update = array
					(
						'AssignMgr' 	=> $UserDetail['mgr_id'], 
						'AssignAmgr'  	=> $UserDetail['act_mgr'], 
						'AssignSpv' 	=> $UserDetail['spv_id'],
						'AssignLeader'	=> $UserDetail['tl_id'],
						'AssignSelerId' => $UserDetail['UserId'],
						'AssignMode' 	=> 'DIS', 
						'AssignDate' 	=> date('Y-m-d H:i:s')
					);
				}
			}
			
			/* @ def : update assignment data **/
				if( $this -> db -> update('t_gn_assignment',$_update, 
					array('AssignId' => $AssignId)
				))
				{
					if( self::_setSaveLog( array('AssignId'=>$AssignId,'UserId'=> $UserDetail['UserId']), 'ASSIGNMENT.AUTOMATIC') ) { 
						$_success++;
					}	
				}
		}
	}	
  }
  
  return array( 
	'SizeData'  => $_success, 
	'SizeUsers' => count($UserIsReady)
  );
  
}

/*
 * @def		 : _setDistribusi from assigment 
 *				agent look on agent assigment () 
 * -----------------------------------------
 *
 * @ def	 : function get detail content list page 
 * @ param	 : not assign parameter
 */	

 public function _setSaveLog($array = array(), $action = NULL ) 
{
  $_conds = FALSE;
  if( is_array($array) 
	AND !is_null($array) ) 
  {
  
    $DebId = $this->_get_debitur_assign_id($array['AssignId']);
	
	if( $DebId !=FALSE ){
		$this->db->set('deb_id', $DebId); 
	}
	
 /* @ pack : ------------------------------- */
	
	 if( isset($array['AssignId']) ){
		$this->db->set('assign_id',$array['AssignId'] );
	 }
	 
	 if( isset($array['tl_id'])){
		$this->db->set('assign_tl_id', $array['tl_id']);
	 }
	 
/* @ pack : */

	  if( isset($array['spv_id'])){
		$this->db->set('assign_spv_id', $array['spv_id']);
	 }
	 

/* @ pack : ------------------------------- */
	 
	 if( isset($array['UserId']) ){
		$this->db->set('assign_dc_id', $array['UserId'] );
	 }
	 
 /* @ pack : ------------------------------- */	 
	 
	 if( isset($array['assign_status']) ){
		 $this->db->set('assign_status', $array['assign_status']);
	 }
	 
/* @ pack : ------------------------------- */
	 
	 $this->db->set('assign_log_created_ts', date('Y-m-d H:i:s'));
	 $this->db->set('assign_log_create_by', _get_session('UserId'));
	 
/* @ pack : ------------------------------- */	 
	 
	 if( !is_null($action) ) {
		$this->db->set('assign_type', $action );
	 }
	 
	 
	 $this->db->insert('t_gn_assignment_log');
	 
	 if( $this->db->affected_rows() > 0 )
	 {
	
		if(isset($array['agent_code']) 
			AND $array['agent_code']!='' )
		{
		  //$this->db->set("deb_is_lock",0);
		  $this->db->set("deb_is_kept",0);
		  $this->db->set("deb_swap_count","(deb_swap_count)+1", FALSE);
		  $this->db->set('deb_agent', strtoupper($array['agent_code']));
		  $this->db->set("deb_last_swap_ts",date('Y-m-d H:i:s')); 
		  $this->db->where('deb_id',$DebId);
		  $this->db->update('t_gn_debitur');
		}
		
		$_conds = TRUE;
	 }
  }	
  
  return $_conds;
  
}

/*
 * @ pack : get list data 
 */

private function _get_debitur_assign_id( $AssignId = 0 )
{
 $this->db->reset_select();
 $this->db->select("a.CustomerId", FALSE);
 $this->db->from('t_gn_assignment a');
 $this->db->where('a.AssignId', $AssignId );
 
 $qry = $this->db->get();
 if( $qry->num_rows() > 0) {
	return (INT)$qry->result_singgle_value();
 } else {
	return 0;
 }
 
}
 
 
 // END CLASS
  
}
?>