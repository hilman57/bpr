<?php
class M_FieldPolicy extends EUI_Model
{

private static $instance = null;


/* instance class **/
 
public static function &get_instance()
{
	if( is_null(self::$instance) )
	{
		self::$instance = new self();
	}
 return self::$instance;
}

/** 
 * @ def   : M_FieldPolicy
 * ----------------------------------------------------
 * @ param : -
 **/
  
 public function M_FieldPolicy() {
	$this->load->model(array('M_Tools'));
 }
 
 
/** 
 * @ def : _getPolicyCompiler 
 * ----------------------------------------------------
 * @ param : label def by ProjectId 
 * @ data  : push of data 
 * @ n 	: sum of columns view 	
 **/
  
 public function _getFieldCompiler( $ProjectId = 0 , $data = null, $n = 2)
 {
	$constructors = null;
	$nBaris = 0; 
	 if( $Label = self::_getFieldLabel($ProjectId) )
	 {
		foreach( $Label as $key => $rows )
		{
			if( in_array($rows['FieldName'], array_keys($data)) ) 
			{
				$constructors[$rows['FieldName']]['label']  = $rows['FieldLabel'];
				$constructors[$rows['FieldName']]['value']  = $data[$rows['FieldName']];
				$constructors[$rows['FieldName']]['class']  = $rows['FieldStyle'];
				$constructors[$rows['FieldName']]['type']   = $rows['FieldType'];
				$constructors[$rows['FieldName']]['format'] = $rows['FieldFormat'];
			}
		}
	 }
	 
	 
	 if( !is_array($constructors) ) die('No data defined.');
	 
/** data testing **/

	 $nBaris = ceil(count($constructors)/$n);
	 $nResult = array(); 
	 $r = 0; $start = 0;
	 while( $r <= $nBaris ){
			$start++;
			$nds  = ($start-1)*$n;
			$rsd  = array_slice($constructors, $nds, $n );
			if( count( $rsd )> 0 ){
				$nResult[$r] = $rsd;
			}
			
		$r++;
	 }
	 return $nResult;
 }
 
 
 /** 
  * @ def : _getPolicyCompiler 
  * ----------------------------------------------------
  * @ param : label def by ProjectId 
  * @ data  : push of data 
  * @ n 	: sum of columns view 	
  **/
  
 private function _getFieldLabel($ProjectId = null )
 {
 
	$arrs_label = null;
	
	$this->db->select("a.FieldName, a.FieldLabel, a.FieldFormat, a.FieldType, a.FieldStyle ");
	$this->db->from("t_gn_field_policy a");
	$this->db->where_in("a.ProjectId", $ProjectId);
	$this->db->where("a.FieldActive",1);
	$this->db->order_by('a.FieldOrder', 'ASC');
	$i = 0;
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{
		$arrs_label[$i] = $rows;
		$i++;
	}
	return $arrs_label;
	
 }
 
 
}

?>