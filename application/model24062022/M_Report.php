<?php


//--------------------------------------------------------------------------
/*
 * @ package 		class model report object 
 * @ auth 			uknown User 
 */
 
class M_Report extends EUI_Model
{

protected $ar_report_type = array();
protected $ar_report_mode = array();
protected $ar_report_group  = array();

private static $instance = null;	

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
  function __construct() 
{
  $this->load->model(array('M_Combo','M_SysUser'));
  
 // ----------------------------------------------------------------------
  
  $this->ar_report_type = array(
		"tracking-report" => "Data Tracking Report",
		"campaign-detail" => "Campaign Detail",
		"ptp-report" => "Promise to Pay Report",
		"skill-report" => "Skill Development Report",
    "data-keep" => "Data Keep",
		"data-review" => "Report CallMon Review",
		"summary-cpa" => "Report Request Discount",
		//"list-lunas" => "List Lunas",
		"cpa_status_report" => "CPA Status Report",
		"account_lunas" => "Account Lunas",
		"account_batal" => "Account Batal",
		"account_complain" => "Account Complain",
		"cpa_list_lunas"=>"CPA List Lunas",
		"full_balance_lunas"=>"List Lunas Full Balance",
    "acc_dc" => "Review Account per DC"
  );
  
// ----------------------------------------------------------------------
  
$this->ar_report_mode = array(
	'detail' => 'Detail', 
	'summary' => 'Summary'
);

// ----------------------------------------------------------------------

$this->ar_report_group  = array(
	'report_group_per_tl' => 'Group Per Team Leader',
	'report_group_per_deskoll' => 'Group Per Deskoll',
	
);
  
}
 
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
 public static function & Instance() 
{
  if( is_null(self::$instance)) {
	self::$instance = new self();
  }
	return self::$instance;
 }
 

 
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
 public static function & get_instance() 
{
  if( is_null(self::$instance)) {
	self::$instance = new self();
  }
	return self::$instance;
 }
 

 
// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 
 public function _select_report_type() 
 {
	return (array)$this->ar_report_type;
} 


// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
  
  public function _select_report_mode() 
{
	return (array)$this->ar_report_mode;
} 

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
  
  public function _select_report_group() 
{
	return (array)$this->ar_report_group;
}  
 
 

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 public function _select_user_spv() 
 {
  
  $this->spv = array();
  $this->db->reset_select();		
  $this->db->select("a.UserId, a.id, a.full_name ");
  $this->db->from("t_tx_agent a");
  $this->db->join("t_gn_agent_profile b ","a.handling_type=b.id", "LEFT");
 
  
  if(in_array( _get_session('HandlingType'), array(USER_LEADER)) ) {
	$this->db->where("b.id", 99);
  } else {
	$this->db->where("b.id", USER_SUPERVISOR);
  }
  
  $this->db->order_by("a.id", "ASC");
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows )
  {
	$this->spv[$rows['UserId']] = join(" - ", array($rows['id'], $rows['full_name']));
  }  
  
  return (array)$this->spv;
   
 }

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 public function _select_user_tl() 
{
  $this->tl = array();	
  $this->db->reset_select();		
  $this->db->select("a.UserId, a.id, a.full_name ");
  $this->db->from("t_tx_agent a");
  $this->db->join("t_gn_agent_profile b ","a.handling_type=b.id", "LEFT");
  $this->db->where("b.id", USER_LEADER);
  $this->db->where("(a.user_resign is null or a.user_resign=0)");  
  if(in_array( _get_session('HandlingType'), array(USER_LEADER)) ) {
	$this->db->where("a.UserId", _get_session('UserId'));  
  }
  $this->db->order_by("a.id", "ASC");
  
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows )
  {
	$this->tl[$rows['UserId']] = join(" - ", array($rows['id'], $rows['full_name']));
  }  
  
  return (array)$this->tl;
	
}

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown
 * @ for senior leader
 */
 
 public function _select_senior_tl() 
{
  $this->stl = array();	
  $this->db->reset_select();		
  $this->db->select("a.UserId, a.id, a.full_name ");
  $this->db->from("t_tx_agent a");
  $this->db->join("t_gn_agent_profile b ","a.handling_type=b.id", "LEFT");
  $this->db->where("b.id", USER_SENIOR_TL);
  
  if(in_array( _get_session('HandlingType'), array(USER_SENIOR_TL)) ) {
	$this->db->where("a.UserId", _get_session('UserId'));  
  }
  $this->db->order_by("a.id", "ASC");
  
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows )
  {
	$this->stl[$rows['UserId']] = join(" - ", array($rows['id'], $rows['full_name']));
  }  
  
  return (array)$this->stl;
	
}


// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
  public function _select_user_agent( ) 
{
  $this->agent = array();	
  $this->db->reset_select();		
  $this->db->select("a.UserId, a.id, a.full_name ");
  $this->db->from("t_tx_agent a");
  $this->db->join("t_gn_agent_profile b ","a.handling_type=b.id", "LEFT");
  $this->db->where("b.id", USER_AGENT_OUTBOUND);
  $this->db->where("(a.user_resign is null or a.user_resign=0)");  
  if(in_array( _get_session('HandlingType'), array(USER_LEADER)) ) {
	$this->db->where("a.tl_id", _get_session('UserId'));  
  }
  $this->db->order_by("a.id", "ASC");
  
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows )
  {
	$this->agent[$rows['UserId']] = join(" - ", array($rows['id'], $rows['full_name']));
  }  
  
  return (array)$this->agent;
	
} 

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 public function _select_agent_by_spv( $spv_id = 0 ) 
{
	$this->agent = array();
	return $this->agent;
}

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 public function _select_agent_by_tl( $tl_id  = 0 ) 
{
	$this->agent = array();
	
	if( !is_array($tl_id) ){
		$tl_id = array($tl_id);
	}	
	
  $this->db->reset_select();		
  $this->db->select("a.UserId, a.id, a.full_name ");
  $this->db->from("t_tx_agent a");
  $this->db->join("t_gn_agent_profile b ","a.handling_type=b.id", "LEFT");
  $this->db->where("b.id", USER_AGENT_OUTBOUND);
  $this->db->where_in("a.tl_id", $tl_id);  
  $this->db->order_by("a.id", "ASC");
  
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows )
  {
	$this->agent[$rows['UserId']] = join(" - ", array($rows['id'], $rows['full_name']));
  }  
  
  return (array)$this->agent;
}



// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
public function _select_agent_by_tl_tlincluded( $tl_id  = 0 ) 
{
	$this->agent = array();
	
	if( !is_array($tl_id) ){
		$tl_id = array($tl_id);
	}	
	
  $this->db->reset_select();		
  $this->db->select("a.UserId, a.id, a.full_name ");
  $this->db->from("t_tx_agent a");
  $this->db->join("t_gn_agent_profile b ","a.handling_type=b.id", "LEFT");
  // $this->db->where("b.id", USER_AGENT_OUTBOUND);
  $this->db->where_in("a.tl_id", $tl_id);  
  $this->db->where("(a.user_resign is null or a.user_resign=0)");  
  $this->db->order_by("a.id", "ASC");
  
  $rs = $this->db->get();
  if( $rs->num_rows() > 0 ) 
	  foreach( $rs->result_assoc() as $rows )
  {
	$this->agent[$rows['UserId']] = join(" - ", array($rows['id'], $rows['full_name']));
  }  
  
  return (array)$this->agent;
}

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
public function _select_tl_by_spv( $spvid = 0 ) {
	$this->tl = array();
	return $this->tl;
}


// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
 public function _select_row_cc_agent( $UserId  = 0 )
{
	
}	

// ----------------------------------------------------------------------------------

/*
 * @ package 	construct 
 * @ auth 		uknown 
 */
 
public function _select_user_level()
{
 $this->arr_user_level = array();
 
 $this->db->reset_write();
 $this->db->select("a.id, a.name", FALSE);
 $this->db->from("t_gn_agent_profile a ");
 $this->db->where_in("a.id", array(USER_SUPERVISOR, USER_LEADER, USER_AGENT_OUTBOUND));
 $this->db->order_by("a.name","ASC");
 
 $rs = $this->db->get();
 if( $rs->num_rows() > 0 ) 
	foreach( $rs->result_assoc() as $rows )
 {
	$this->arr_user_level[$rows['id']] = join(" - ", array($rows['name']));
 }
 return $this->arr_user_level;
 
}

public function campaign_outbound()
{
	$this->load->model(array('M_SetCampaign'));
	$outbound = 2;
	return $this->M_SetCampaign->_getCampaignGoals($outbound);
} 
 
 // ================= END CLASS =========================
 
}
?>