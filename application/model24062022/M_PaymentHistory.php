<?php
class M_PaymentHistory extends EUI_Model {

var $DebiturId = null;
var $PaymentHistory = array();

/*
 * @ pack : instance of 
 */

private static $Instance = null;
 
/*
 * @ pack : contsruct 
 */

function M_PaymentHistory(){ }

/* 
 * @ pack : singgleeton  
 */
 
 public static function &Instance()
{
 if( is_null(self::$Instance) ) {	
	self::$Instance = new self();
 }
 
 return self::$Instance;
}

/*
 * @ pack : _get_select_month
 */

 public function _get_select_month()
{
  $argc = null;
  if( is_null($argc))
	for($argv = 1; $argv<=12; $argv++)
  {
	 if( $argc_vars = (string)sprintf('%02d',$argv) )
	 {
		$argc[$argc_vars]=$argc_vars;	
	 }
  }
  
  return $argc;
}

/*
 * @ pack : 
 */

 public function _get_payment_detail( $DebiturId )
{
  $this->DebiturId =(int)$DebiturId;
  $this->PaymentHistory = array();
  
// @ pack : DebiturId

  if( $this->DebiturId ) 
  {
    $this->db->reset_select();
	$this->db->select("date_format(a.pay_date,'%Y-%m') as tgl", FALSE);
	$this->db->from("t_gn_payment a");
	$this->db->where("a.deb_id",$this->DebiturId);
	$this->db->group_by("tgl");
	// echo $this->db->print_out();
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ) 
	 foreach( $qry->result_assoc() as $rows )
	{
	 $this->PaymentHistory[$rows['tgl']] = $rows['tgl'];
	}
  }
  
  return $this->PaymentHistory;
  
 }
 
/*
 * @ pack : class entry / recsource 
 */ 
public function _get_class_entry( $entry_date =null )
{
	$entry_date = (string)preg_replace('/[^\0-9]/i','', $entry_date);
	
	if( !is_null($entry_date) ){
		return substr($entry_date,0,2) .'-'. substr($entry_date,2,2) .'-'. substr( date('Y'), 0,2) . substr( date('Y'), -2);
	} else {
		return null;
	}
} 

/** get payment summery by DebitUr Id **/

 public function _getSummaryPayment( $DebiturId =0 )
{
	$result = 0;
	
	$this->db->reset_select();
	$this->db->select("SUM(a.pay_amount) as payment ", FALSE);
	$this->db->from("t_gn_payment a ");
	$this->db->where("a.deb_id", $DebiturId);
	
	$qry = $this->db->get();
	if( $qry ->num_rows() > 0 )
	{
		$result =(int)$qry->result_singgle_value();
	}
	
	return $result;
}

// get lasy payment amount **/

 public function _getSummaryLastPayment( $DebiturId =0 )
{
	$result = NULL;
	$this->db->reset_select();
	$this->db->select("a.pay_amount AS payment ", FALSE);
	$this->db->from("t_gn_payment a ");
	$this->db->where("a.deb_id", $DebiturId);
	$this->db->order_by("a.pay_date","DESC");
	$this->db->limit(1);
	
	$qry = $this->db->get();
	if( $qry ->num_rows() > 0 )
	{
		$result =(int)$qry->result_singgle_value();
	}
	
	return $result;
}

/*
 * @ pack : get payment history by customer id
 *	06/04/2015
 */
 
 public function _get_payment_history_customer( $CustomerId = 0 )
{
	$this->db->reset_select();	
	$this->db->select("
		a.pay_id,
		DATE_FORMAT(a.pay_date,'%d-%m-%Y') AS pay_date,
		b.deb_name,
		c.code_user,
		a.delete_dc_id,
		a.update_created_ts,
		a.pay_amount",
	FALSE);
	
	$this->db->from("t_gn_payment a");
	$this->db->join("t_gn_debitur b","a.deb_id = b.deb_id","LEFT");
	$this->db->join("t_tx_agent c","a.pay_dc_id = c.UserId","LEFT");
	
    $this->db->where("a.deb_id",$CustomerId);
    $this->db->where("a.pay_status","1");
	$this->db->order_by("DATE(a.pay_date)", "DESC");
	
	// echo $this->db->_get_var_dump();
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_assoc();
	} else { 
		return FALSE;
	}
}


/*
 * @ pack : delete payment history by payment id
 *	08/04/2015
 */
public function DeletePayment()
{
	$Success = FALSE;
	$payid = _get_post('PayId');
	$payment = array();
	$this->db->reset_select();	
	$this->db->select("*",FALSE);
	$this->db->from("t_gn_payment a");
    $this->db->where("a.pay_id",$payid);
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ){
		foreach ($qry->result_assoc() as $row)
		{
			$payment['pay_id']=$row['pay_id'];
			$payment['deb_id']=$row['deb_id'];
			$payment['pay_amount']=$row['pay_amount'];
			$payment['pay_created_ts']=$row['pay_created_ts'];
			$payment['log_created_ts']=date('Y-m-d H:i:s');
			$payment['log_desc']="Delete payment by ".$this->EUI_Session->_get_session('Fullname');
		}
	}
	
	if(count($payment>0))
	{
		$this->db->set('pay_status',0);
		$this->db->set('delete_dc_id',$this->EUI_Session->_get_session('Fullname'));
		$this->db->set('update_created_ts',date('Y-m-d H:i:s'));
		$this->db->where('pay_id', $payid);
		$this->db->update('t_gn_payment');
		if($this->db->affected_rows()>0);
		{
			$this->db->insert('t_gn_payment_log', $payment );
			$Success = TRUE;
		}
	}
	return $Success;
	
}
 
// END CLASS 
}

?>