<?php

class M_FormDiscount extends EUI_Model
{

var $Customers = array();

/*
 * @ pack : instance 
 */
  
 private static $Instance = null;

/*
 * @ pack : instance 
 */
 
 public static function &Instance()
{
 if( is_null(self::$Instance) )
 {
	self::$Instance = new self();
  }
  return self::$Instance;
  
}
 
/*
 * get all reference debitur 
 */
 
 
 public function M_FormDiscount()
{
	$this->load->model(array('M_ModContactDetail'));
}

/*
 * get all reference debitur 
 */
 
 public function _get_name_owner_data($CustomerId){
	 $this->db->reset_select();
	 $this->db->select('a.*', false);
	 $this->db->from('t_gn_assignment a');
	 //$this->db->join('t_tx_agent','AssignSellerId = UserId');
	 //$this->db->where('CustomerId',$CustomerId);
	 $this->db->join('t_gn_discount_trx b','a.CustomerId = b.deb_id');
	 $this->db->where('b.id',$CustomerId);
	 
	 $qry = $this->db->get();
	 
	if( $qry->num_rows() > 0 )
	{
		return $qry->result_first_assoc();
		//return array('AssignSelerId'=>$CustomerId);
		//return $this->db->last_query();
	}
	 
 }
 
 public function _get_select_debitur( $CustomerId )
{
	if( class_exists('M_ModContactDetail') )
	{
		$ClsModel =& get_class_instance('M_ModContactDetail');
		$this->Customers = $ClsModel->_get_select_debitur( $CustomerId );
		
		if( !is_null($this->Customers) )
		{
			return $this->Customers;
		}
		
	} else {
		return array();
	}	
}

/*
 * get all reference debitur 
 */
 
 public function _get_select_campaign()
{
  if( !is_null($this->Customers) )
  {
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_gn_campaign');
	$this->db->where('CampaignId', $this->Customers['deb_cmpaign_id']);
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 )
	{
		return $qry->result_first_assoc();
	}	
	
 } else {
	return null;
 }
} 
 
/*
 * @ pack : get occupation 
 */ 
 
public function _get_select_occupation()
{
   $conds = array();
 
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_lk_occupation_code');
	$this->db->where('OccFlags', 1);
	foreach( $this->db->get()->result_assoc() as $rows ){
		$conds[$rows['OccCode']] = $rows['OccIndonesian'];
	}
	
	return $conds;
}
/*
 * @ pack : get cpa reason  
 */  
 
 public function _get_select_cpa_reason()
{
	$conds = array();
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_lk_cpa_reason');
	$this->db->where('reason_flags', 1);
	
	foreach( $this->db->get()->result_assoc() as $rows ){
		$conds[$rows['reason_code']] = $rows['reason_desc'];
	}
	
	return $conds;
}
 
 /*
 * @ pack : get cpa payer
 */  
 public function _get_select_payer()
{
	$conds = array();
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_lk_payers');
	$this->db->where('PayerFlags', 1);
	
	foreach( $this->db->get()->result_assoc() as $rows ){
		$conds[$rows['PayerCode']] = $rows['PayerDescription'];
	}
	
	return $conds;
} 
// _get_select_payer =====================>

/*
 * @pack : periode 
 */
 
 public function _get_select_periode( $n = 12 ) 
 {
   $conds = array();
   for( $i=1; $i<=$n; $i++){
	$conds[$i] = $i;
   }
   
   return $conds;
	
 }
 
// _get_select_periode =====================>

 /*
 * @ pack : document type
 */  
 
 public function _get_select_document_type()
{	
	$conds = array();
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_lk_document_type');
	$this->db->where('DocTypeFlags', 1);
	
	foreach( $this->db->get()->result_assoc() as $rows ){
		$conds[$rows['DocTypeCode']] = $rows['DocTypeDescription'];
	}
	
	return $conds;
}
// _get_select_document_type =====================>

/*
 * @ SaveDiscount 
 */
 
 public function _getInteger( $float = 0 )
{	
	
	return preg_replace('/\./i','', $float);
} 
// _getInteger =====================>

/* 
 * @ pack : select_expired_date 
 *  --------------------------------------------
 */

 private function select_expired_date( $date = null )
{
	$PaymentPeriod = ( _get_post('PaymentPeriod') * 30);
	return _getPrevDate($date, $PaymentPeriod);
}  
// select_expired_date ========================>


/*
 * @ SaveDiscount 
 */

public function _CheckingAgreement($DebId){
	$this->db->select('*');
	$this->db->from('t_gn_discount_trx');
	$this->db->where('deb_id',$DebId);
	$this->db->where_not_in('aggrement','103');
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 )
	{
		return 'isi';
	}else{
		return 'kosong';
	}	
}
 
public function _SaveDiscount()
{

 $conds = 0;
 
 /* @ pack : expired date of month periode **/
 
 $ExpiredDate = self::select_expired_date(date('Y-m-d'));
 
/* @ pack : expired date of month periode **/

 $this->db->set('deb_id', _get_post('DebiturId'));
 $this->db->set('deb_acctno',_get_post('CFNo'));
 $this->db->set('justification', _get_post('justification_text'));
 $this->db->set('reff_no',_get_post('RefNo'));
 $this->db->set('placement',_get_post('Placement'));
 $this->db->set('card_status',_get_post('CardStatus'));
 $this->db->set('region',_get_post('Region'));
 $this->db->set('payment_periode',_get_post('PaymentPeriod'));
 $this->db->set('occupation',_get_post('Occupation'));
 $this->db->set('reason',_get_post('Reason'));
 $this->db->set('pay_handled_by',_get_post('PaymentHandledBy'));
 $this->db->set('arrangement', _get_post('Arrangement'));
 $this->db->set('principle_byspv',self::_getInteger(_get_post('Principal')));
 $this->db->set('futurepay_byspv',self::_getInteger(_get_post('FuturePayment')));
 $this->db->set('outstanding_byspv',self::_getInteger(_get_post('OutstandingBalance')));
 $this->db->set('downpayment_byspv',self::_getInteger(_get_post('DownPayment')));
 $this->db->set('totalpayment_byspv',self::_getInteger(_get_post('TotalPayment')));
 $this->db->set('charges_byspv',self::_getInteger(_get_post('ChargesFeeInterest')));
 $this->db->set('discountamo_byspv', self::_getInteger(_get_post('ExceptDiscountAmount')));
 $this->db->set('product', _get_post('Product'));
 $this->db->set('from_balance', _get_post('FromOSbal'));
 $this->db->set('from_princ',_get_post('FromPrincipal'));
 $this->db->set('justification_byspv',_get_post('justification_text'));
 $this->db->set('ExceptionalLevel', _get_post('ExceptionalLevel'));
 $this->db->set('ExceptProcessTo', _get_post('ExceptProcessTo'));
 $this->db->set('except1',_get_post('except1'));
 $this->db->set('except2',_get_post('except2'));
 $this->db->set('except3',_get_post('except3'));
 $this->db->set('except4',_get_post('except4'));
 $this->db->set('form_type','D');
 $this->db->set('doc',_get_post('document'));
 $this->db->set('sid_status',_get_post('sid'));
 $this->db->set('agent_id',_get_session('UserId'));
 $this->db->set('tl', _get_session('TeamLeaderId'));
 $this->db->set('spv', _get_session('SupervisorId'));
 $this->db->set('create_datets', date('Y-m-d H:i:s'));
 $this->db->set('expired_date', $ExpiredDate);
 $this->db->set('create_date', date('Y-m-d'));
 
 // @ pack : run update data ----------------------------->
  
 $this->db->insert("t_gn_discount_trx");
 if( $this->db->affected_rows() > 0 ){
	 $conds++;
 }
 
 return $conds; 
}  
// _SaveDiscount ================================>


/*
 * @ pack : update  form lunas if edit && update 
 */
 
public function DeleteAttach_1()
{
	$DebiturId = _get_post('FormUpdateId');
	$conds = 0;
	
	$this->db->set('File1', null);
	$this->db->where('id', $DebiturId);
	
	if( $this->db->update("t_gn_discount_trx") )
	{
		$conds++;
	}
	return $conds;
}

public function DeleteAttach_2()
{
	$DebiturId = _get_post('FormUpdateId');
	$conds = 0;
	
	$this->db->set('File2', null);
	$this->db->where('id', $DebiturId);
	
	if( $this->db->update("t_gn_discount_trx") )
	{
		$conds++;
	}
	return $conds;
}

public function DeleteAttach_3()
{
	$DebiturId = _get_post('FormUpdateId');
	$conds = 0;
	
	$this->db->set('File3', null);
	$this->db->where('id', $DebiturId);
	
	if( $this->db->update("t_gn_discount_trx") )
	{
		$conds++;
	}
	return $conds;
}

public function DeleteAttach_4()
{
	$DebiturId = _get_post('FormUpdateId');
	$conds = 0;
	
	$this->db->set('File4', null);
	$this->db->where('id', $DebiturId);
	
	if( $this->db->update("t_gn_discount_trx") )
	{
		$conds++;
	}
	return $conds;
}

public function DeleteAttach_5()
{
	$DebiturId = _get_post('FormUpdateId');
	$conds = 0;
	
	$this->db->set('File5', null);
	$this->db->where('id', $DebiturId);
	
	if( $this->db->update("t_gn_discount_trx") )
	{
		$conds++;
	}
	return $conds;
}

public function DeleteAttach_6()
{
	$DebiturId = _get_post('FormUpdateId');
	$conds = 0;
	
	$this->db->set('File6', null);
	$this->db->where('id', $DebiturId);
	
	if( $this->db->update("t_gn_discount_trx") )
	{
		$conds++;
	}
	return $conds;
}
 
 public function _UpdateDiscount($_post =array())
{

  $FormUpdateId =(int)_get_post('FormUpdateId');
  
  if( $FormUpdateId == FALSE ) {
	return FALSE;
  }	
  // then if OK Update 
  
/* @ pack : expired date of month periode **/
 
 $ExpiredDate = self::select_expired_date(_get_post('create_date'));
 
/* @ pack : expired date of month periode **/

 $conds  = 0; // user callback 
 
 $this->db->set('deb_id', _get_post('DebiturId'));
 $this->db->set('deb_acctno',_get_post('CFNo'));
 $this->db->set('justification', _get_post('justification_text'));
 $this->db->set('reff_no',_get_post('RefNo'));
 $this->db->set('placement',_get_post('Placement'));
 $this->db->set('card_status',_get_post('CardStatus'));
 $this->db->set('region',_get_post('Region'));
 $this->db->set('payment_periode',_get_post('PaymentPeriod'));
 $this->db->set('occupation',_get_post('Occupation'));
 $this->db->set('reason',_get_post('Reason'));
 $this->db->set('pay_handled_by',_get_post('PaymentHandledBy'));
 $this->db->set('arrangement', _get_post('Arrangement'));
 $this->db->set('principle_byspv',self::_getInteger(_get_post('Principal')));
 $this->db->set('futurepay_byspv',self::_getInteger(_get_post('FuturePayment')));
 $this->db->set('outstanding_byspv',self::_getInteger(_get_post('OutstandingBalance')));
 $this->db->set('downpayment_byspv',self::_getInteger(_get_post('DownPayment')));
 $this->db->set('totalpayment_byspv',self::_getInteger(_get_post('TotalPayment')));
 $this->db->set('charges_byspv',self::_getInteger(_get_post('ChargesFeeInterest')));
 $this->db->set('discountamo_byspv', self::_getInteger(_get_post('ExceptDiscountAmount')));
 $this->db->set('product', _get_post('Product'));
 $this->db->set('from_balance', _get_post('FromOSbal'));
 $this->db->set('from_princ',_get_post('FromPrincipal'));
 $this->db->set('justification_byspv',_get_post('justification_text'));
 $this->db->set('ExceptionalLevel', _get_post('ExceptionalLevel'));
 $this->db->set('ExceptProcessTo', _get_post('ExceptProcessTo'));
 $this->db->set('except1',_get_post('except1'));
 $this->db->set('except2',_get_post('except2'));
 $this->db->set('except3',_get_post('except3'));
 $this->db->set('except4',_get_post('except4'));
 $this->db->set('form_type','D');
 $this->db->set('doc',_get_post('document'));
 $this->db->set('agent_id',_get_session('UserId'));
 $this->db->set('tl', _get_session('TeamLeaderId'));
 $this->db->set('spv', _get_session('SupervisorId'));
 $this->db->set('expired_date', $ExpiredDate);
 $this->db->set('udate_date', date('Y-m-d H:i:s'));
 if($_post) {
 $this->db->set('File1', $_post[0]['name']);
 $this->db->set('File2', $_post[1]['name']);
 $this->db->set('File3', $_post[2]['name']);
 $this->db->set('File4', $_post[3]['name']);
 $this->db->set('File5', $_post[4]['name']);
 $this->db->set('File6', $_post[5]['name']);
 }
 // $this->db->set('sid_status',_get_post('sid'));
 //$this->db->set('updated_by', _get_session('UserId'));
 
 // @ pack : run update data ----------------------------->
 
 $this->db->where('id', $FormUpdateId);
 
 if( $this->db->update("t_gn_discount_trx") )
 {
	 $conds++;
 }
 // $this->db->last_query();
 return $conds;
	
}
// _SaveDiscount ================================>




/*
 * @ pack : function _get_select_discountId 
 */
 
 public function _get_select_discountId( $id )
{
	$this->db->select('a.*, a.id as UpdateId');
	$this->db->from('t_gn_discount_trx a');
	$this->db->where('a.id',$id);
	return $this->db->get()->result_first_assoc();
} 

// _get_select_discountId =================================>
 
// END CLASS 

 
}
?>