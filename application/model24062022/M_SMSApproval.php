<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SMSApproval extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_SMSApproval() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser',
	'M_SetCampaign','M_SetResultCategory', 
	'M_Combo','M_MaskingNumber'));
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getApproveStatus()
{
 $_account_status = null;
 
 $this->db->reset_select();
 $this->db->select("a.StatusCode, a.StatusName ");
 $this->db->from("t_lk_sms_approval_status a");
 $this->db->where('a.StatusFlags', 1);
 foreach( $this->db->get()->result_assoc() as $rows )
 {
	$_account_status[$rows['StatusCode']] = $rows['StatusName'];
 }
 return $_account_status;
 
}
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getSMSType() 
{
  $_last_call_status = null;
  return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
  $this->EUI_Page->_setPage(10); 
  $this->EUI_Page->_setSelect("a.SmsId"); 
  
  $this->EUI_Page->_setFrom("t_gn_sms_approval a");
  $this->EUI_Page->_setJoin("t_lk_sms_approval_status b ","a.SmsApprovalStatus=b.StatusCode","LEFT");
  $this->EUI_Page->_setJoin("t_gn_debitur c ","a.MasterId=c.deb_id","LEFT");
  $this->EUI_Page->_setJoin("serv_sms_tpl d ","a.SmsTemplateId=d.SmsTplId","LEFT");
  $this->EUI_Page->_setJoin("t_tx_agent e "," a.SmsCreateUserId=e.UserId","LEFT");
  $this->EUI_Page->_setJoin("serv_sms_group_tpl g ","d.GroupTplId=g.GroupTplId","LEFT");
  $this->EUI_Page->_setJoin("t_gn_campaign f ","c.deb_cmpaign_id=f.CampaignId","LEFT", TRUE);

 // @ pack : filter by default session ---------------------------------------------------------------------------------
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		 $this->EUI_Page->_setAnd('e.UserId',$this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
	   $this->EUI_Page->_setAnd('e.tl_id',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		$this->EUI_Page->_setAnd('a.SmsCreateUserId IS NOT NULL');
	}		
	$this->EUI_Page->_setAnd('a.SmsApprovalStatus = 101');
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setLikeCache('c.deb_acct_no', 'apprv_cust_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_cmpaign_id', 'apprv_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.SmsApprovalStatus','apprv_sms_status',TRUE);
	$this->EUI_Page->_setAndCache('e.UserId', 'apprv_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'apprv_cust_name', TRUE);
	
 // @ pack : start date ------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.SmsCreateTs>='". _getDateEnglish(_get_post('apprv_start_date')) ."'", 'apprv_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.SmsCreateTs<='". _getDateEnglish(_get_post('apprv_end_date')) ."'", 'apprv_end_date', TRUE);
	
 // @ pack : group_by 
 
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
  $this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
  $this->EUI_Page->_setPage(10);
	
/** default of query ***/
  $this->EUI_Page->_setArraySelect(array(
	  "a.SmsId AS SmsId"=> array('SmsId','ID','primary'),
	  "c.deb_id as DebId" => array("DebId", "DebId","hidden"),
	  "f.CampaignCode AS CampaignCode "=> array('CampaignCode','Product'),
	  "c.deb_acct_no AS AccountNumber "=> array('AccountNumber','Acc. No'),
	  "c.deb_name AS Customer " =>array('Customer','Customer Name'),
	  "a.SmsDestination AS Destination "=> array('Destination','Destination'),
	  "g.GroupTplName AS GroupName" => array("GroupName","Sms Type"),
	  "a.SmsTextMessage AS SmsTplContent"=> array('SmsTplContent','Text Message'),
	  "date_format(a.SmsCreateTs,'%d-%m-%Y %H:%i:%s') as CreatedTs" => array("CreatedTs","Create Date"),
	  "date_format(a.SmsUpdateTs,'%d-%m-%Y %H:%i:%s') as UpdateTs" => array("UpdateTs","Approve Date"),
	  "b.StatusName as StatusCode" => array("StatusCode","Status"),
	  "e.code_user AS Usercode "=> array('Usercode','Use ID'),
	  "e.full_name AS Username "=> array('Username','User Name'),
  ));
	
	
 $this->EUI_Page->_setFrom("t_gn_sms_approval a");
 $this->EUI_Page->_setJoin("t_lk_sms_approval_status b ","a.SmsApprovalStatus=b.StatusCode","LEFT");
 $this->EUI_Page->_setJoin("t_gn_debitur c ","a.MasterId=c.deb_id","LEFT");
 $this->EUI_Page->_setJoin("serv_sms_tpl d ","a.SmsTemplateId=d.SmsTplId","LEFT");
 $this->EUI_Page->_setJoin("t_tx_agent e "," a.SmsCreateUserId=e.UserId","LEFT");
 $this->EUI_Page->_setJoin("serv_sms_group_tpl g ","d.GroupTplId=g.GroupTplId","LEFT");
 $this->EUI_Page->_setJoin("t_gn_campaign f ","c.deb_cmpaign_id=f.CampaignId","LEFT", TRUE);
	
	
// @ pack : filter by default session ---------------------------------------------------------------------------------
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		 $this->EUI_Page->_setAnd('e.UserId',$this->EUI_Session->_get_session('UserId'));
	}	

// @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
	   $this->EUI_Page->_setAnd('e.tl_id',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		$this->EUI_Page->_setAnd('a.SmsCreateUserId IS NOT NULL');
	}		
	$this->EUI_Page->_setAnd('a.SmsApprovalStatus = 101');
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setLikeCache('c.deb_acct_no', 'apprv_cust_id', TRUE);
	$this->EUI_Page->_setLikeCache('a.deb_cmpaign_id', 'apprv_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('a.SmsApprovalStatus','apprv_sms_status',TRUE);
	$this->EUI_Page->_setAndCache('e.UserId', 'apprv_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'apprv_cust_name', TRUE);
	
// @ pack : start date ------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("a.SmsCreateTs>='". _getDateEnglish(_get_post('apprv_start_date')) ."'", 'apprv_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("a.SmsCreateTs<='". _getDateEnglish(_get_post('apprv_end_date')) ."'", 'apprv_end_date', TRUE);
		
// @ pack : set order by ----------------------------------------------------------

	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
 
/*
 * @ pack : _setSMSApprove 
 */
 
public function _setSMSApprove()
{
 $conds =0;
 $SmsId = _get_array_post('SmsId');
 if( $SmsId )
 {
	foreach( $SmsId as $id => $value )
	{
		$this->db->set("SmsApprovalStatus",_get_post('Code'));
		$this->db->set("SmsUpdateUserId",_get_session('UserId'));
		$this->db->set("SmsUpdateTs",date('Y-m-d H:i:s'));
		$this->db->where("SmsId", $value);
		if( $this->db->update("t_gn_sms_approval")){
			$conds++;
		}
	}	
  }
  
  return $conds;
  
}
/*
 * @ pack : _setSMSRejected 
 */
 
public function _setSMSRejected()
{
 $conds =0;
 $SmsId = _get_post('SmsId');
 if( $SmsId )
 {
	$this->db->set("SmsApprovalStatus",_get_post('Code'));
	$this->db->set("SmsUpdateUserId",_get_session('UserId'));
	$this->db->set("SmsUpdateTs",date('Y-m-d H:i:s'));
	$this->db->where("SmsId", $SmsId);
	if( $this->db->update("t_gn_sms_approval")){
		$conds++;
	}
  }
  return $conds;
}

}

// END OF CLASS 



?>