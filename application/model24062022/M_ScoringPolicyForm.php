<?php
class M_ScoringPolicyForm extends EUI_Model
{
	function M_ScoringPolicyForm()
	{
		// echo "XXX";
	}
	
	function _SaveScore($param)
	{
		$sql = " select a.* from t_gn_qa_scoring a where a.PolicyId = '".$param['PolicyId']."' ";
		$qry = $this->db->query($sql);
		// echo $sqlX;
		if($qry->num_rows() < 1)
		{
			$conds = array('success'=>0);
			$sql = array();
			
			$sql['CustomerId'] = $param['CustomerId'];
			$sql['PolicyId'] = $param['PolicyId'];
			$sql['ProjectId'] = $param['ProjectId'];
			$sql['ScoringRemark1'] = $param['RemarkScore'];
			$sql['ScoringTotal1'] = $param['FinalScore'];
			$sql['ScroingQualityId'] = $this->EUI_Session->_get_session('UserId');
			$sql['ScoringCreateTs'] = date('Y-m-d H:i:s');
			
			$this->db->insert('t_gn_qa_scoring',$sql);
			// var_dump($this->db);
			if( $this->db->affected_rows() > 0 )
			{
				$InsertId = $this -> db->insert_id();
				$bebek = array();
				
				for($x=1;$x<=8;$x++){
					if( isset($param['chk_'.$x]) ){
						$bebek['chk_'.$x] = $param['chk_'.$x];
					}else{
						$bebek['chk_'.$x] = 0;
					}
				}
				
				$anoa['ScoringId'] = $InsertId;
				$anoa['ScoringQuestionId'] = implode(',',array_keys($bebek));
				$anoa['ScoringQuestionValue'] = implode(',',$bebek);
				$anoa['ScoringQuestionDates'] = date('Y-m-d H:i:s');
				$anoa['ScoringTotalScore1'] = $param['FinalScore'];
				$anoa['ScoringByUserId'] = $this->EUI_Session->_get_session('UserId');
				
				$this->db->insert('t_gn_scoring_point',$anoa);
				if( $this->db->affected_rows() > 0 )
				{
					$conds = array('success'=>1);
					// echo "3";
				}
				// var_dump($this->db);
			}
			return $conds;
			
		}
		else{
			$conds = array('success'=>0);
			
			// $this->db->where('CustomerId'] = $param['CustomerId'];
			$this->db->where('PolicyId', $param['PolicyId']);

			$this->db->set('ScoringRemark1', $param['RemarkScore']);
			$this->db->set('ScoringTotal1', $param['FinalScore']);
			$this->db->set('ScroingQualityId', $this->EUI_Session->_get_session('UserId'));
			$this->db->set('ScoringCreateTs', date('Y-m-d H:i:s'));
			
			$this->db->update('t_gn_qa_scoring');
			
			if( $this->db->affected_rows() > 0 OR $this->db->affected_rows() < 0 ){
				$this->db->select('a.Id',FALSE);
				$this->db->from('t_gn_qa_scoring a');
				$this->db->where('a.PolicyId', $param['PolicyId']);
				
				$rows = $this->db->get();
				$ScoringId = $rows->result_first_assoc();
				
				if($rows->num_rows() > 0){
					$bebek = array();
				
					for($x=1;$x<=8;$x++){
						if( isset($param['chk_'.$x]) ){
							$bebek['chk_'.$x] = $param['chk_'.$x];
						}else{
							$bebek['chk_'.$x] = 0;
						}
					}
					
					$this->db->where('ScoringId', $ScoringId['Id']);
					
					$this->db->set('ScoringQuestionId', implode(',',array_keys($bebek)));
					$this->db->set('ScoringQuestionValue', implode(',',$bebek));
					$this->db->set('ScoringQuestionDates', date('Y-m-d H:i:s'));
					$this->db->set('ScoringTotalScore1', $param['FinalScore']);
					$this->db->set('ScoringByUserId', $this->EUI_Session->_get_session('UserId'));
					
					$this->db->update('t_gn_scoring_point');
					if( $this->db->affected_rows() > 0 )
					{
						$conds = array('success'=>1);
					}
				}
			}
			
			return $conds;
		}
		
	}
	
	function _SaveScoreBCU($param)
	{
		$conds = array('success'=>0);
		
		$sql['CustomerId'] = $param['CustomerId'];
		$sql['PolicyId'] = $param['PolicyId'];
		$sql['ProjectId'] = $param['ProjectId'];
		$sql['ScoringRemark1'] = $param['Remark1'];
		$sql['ScoringRemark2'] = $param['Remark2'];
		$sql['ScoringTotal1'] = $param['FinalScore'];
		$sql['ScroingQualityId'] = $this->EUI_Session->_get_session('UserId');
		$sql['ScoringCreateTs'] = date('Y-m-d H:i:s');
		
		// print_r($sql);
		
		$this->db->insert('t_gn_qa_scoring',$sql);
		
		if( $this->db->affected_rows() > 0 )
		{
			$InsertId = $this -> db->insert_id();
			
			$bebek = array();
			
			for($x=1;$x<=25;$x++)
			{
				if( isset($param['chk_'.$x]) )
				{
					$bebek['chk_'.$x] = $param['chk_'.$x];
				}
				else{
					$bebek['chk_'.$x] = 0;
				}
			}
			
			$anoa['ScoringId'] = $InsertId;
			$anoa['ScoringQuestionId'] = implode(',',array_keys($bebek));
			$anoa['ScoringQuestionValue'] = implode(',',$bebek);
			$anoa['ScoringQuestionDates'] = date('Y-m-d H:i:s');
			$anoa['ScoringTotalScore1'] = $param['FinalScore'];
			$anoa['ScoringByUserId'] = $this->EUI_Session->_get_session('UserId');
			
			// print_r($anoa);
			
			$this->db->insert('t_gn_scoring_point',$anoa);
			// var_dump($this->db);
			if( $this->db->affected_rows() > 0 )
			{
				$conds = array('success'=>1);
			}
			// var_dump($this->db);
		}
		
		return $conds;
	}
	
	function _SaveScoreAttempt($param)
	{
		$conds = array('success'=>0);
		
		$sql['PolicyNo'] = $param['No_Policy'];
		$sql['HolderName'] = $param['Policy_Name'];
		$sql['Value'] = $param['Nilai'];
		$sql['Note'] = $param['Catatan'];
		$sql['CreatedDateTs'] = date('Y-m-d H:i:s');
		$sql['CreatedBy'] = $this->EUI_Session->_get_session('UserId');
		$sql['UpdatedDateTs'] = date('Y-m-d H:i:s');
		$sql['UpdatedByTs'] = $this->EUI_Session->_get_session('UserId');
		
		$this->db->insert('t_gn_scoring_call_attempt',$sql);
		
		if( $this->db->affected_rows() > 0 )
		{
			$conds = array('success'=>1);
		}
		
		return $conds;
	}
	
	function _SaveScoreEager($param)
	{
		$conds = array('success'=>0);
		
		$sql['PolicyNo'] = $param['No_Policy'];
		$sql['HolderName'] = $param['Policy_Name'];
		$sql['Score1'] = $param['Nilai1'];
		$sql['Score2'] = $param['Nilai2'];
		$sql['Score3'] = $param['Nilai3'];
		// $sql['Notes'] = date('Y-m-d H:i:s');
		$sql['CreatedDateTs'] = date('Y-m-d H:i:s');
		$sql['CreatedBy'] = $this->EUI_Session->_get_session('UserId');
		$sql['UpdatedDateTs'] = date('Y-m-d H:i:s');
		$sql['UpdatedBy'] = $this->EUI_Session->_get_session('UserId');
		
		$this->db->insert('t_gn_scoring_eager',$sql);
		
		// var_dump($this->db);
		
		if( $this->db->affected_rows() > 0 )
		{
			$conds = array('success'=>1);
		}
		
		return $conds;
	}
	
	function _GetData($CustomerId,$PolicyId,$ProjectId)
	{
		$datas = array();
		
		$sql = "select * from t_gn_qa_scoring a
				left join t_gn_scoring_point b on a.Id = b.ScoringId
				where a.CustomerId = '".$CustomerId."' 
				and a.PolicyId = '".$PolicyId."' 
				and a.ProjectId = '".$ProjectId."'";
		$qry = $this->db->query($sql);
		// echo $sql;
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas = $rows;
			}
		}
		
		return $datas;
	}
	
	function _GetDataAttempt($CustomerId,$PolicyId,$ProjectId)
	{
		$datas = array();
		$sql = "SELECT
					a.PolicyId,
					a.PolicyNumber,
					a.EnigmaId,
					a.CIFNumber,
					a.PolicyFirstName,
					b.CustomerId,
					b.CustomerFirstName,
					e.Value,
					e.Note
				FROM t_gn_policy_detail a
				LEFT JOIN t_gn_debitur b ON a.CIFNumber = b.CIFNumber
				LEFT JOIN t_gn_campaign c ON b.CampaignId = c.CampaignId
				LEFT JOIN t_gn_campaign_project d ON c.CampaignId = d.CampaignId
				LEFT JOIN t_gn_scoring_call_attempt e ON a.PolicyNumber = e.PolicyNo
				WHERE b.CustomerId = ".$CustomerId."
				AND a.PolicyId = ".$PolicyId."
				AND d.ProjectId = ".$ProjectId." ";
		$qry = $this->db->query($sql);
		// echo $sql;
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas = $rows;
			}
		}
		
		return $datas;
	}
	
	function _GetDataEager($CustomerId,$PolicyId,$ProjectId)
	{
		$datas = array();
		
		$sql = "SELECT
					a.PolicyId,
					a.PolicyNumber,
					a.EnigmaId,
					a.CIFNumber,
					a.PolicyFirstName,
					b.CustomerId,
					b.CustomerFirstName,
					e.Score1,
					e.Score2,
					e.Score3
				FROM t_gn_policy_detail a
				LEFT JOIN t_gn_debitur b ON a.CIFNumber = b.CIFNumber
				LEFT JOIN t_gn_campaign c ON b.CampaignId = c.CampaignId
				LEFT JOIN t_gn_campaign_project d ON c.CampaignId = d.CampaignId
				LEFT JOIN t_gn_scoring_eager e ON a.PolicyNumber = e.PolicyNo
				WHERE b.CustomerId = ".$CustomerId."
				AND a.PolicyId = ".$PolicyId."
				AND d.ProjectId = ".$ProjectId." ";
		$qry = $this->db->query($sql);
		// echo $sql;
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas = $rows;
			}
		}
		
		return $datas;
	}
	
	function _getProjectCode($projectId)
	{
		$datas = array();
		
		$sql = "SELECT
					a.ProjectId,
					a.ProjectCode,
					a.ProjectName
				FROM t_lk_work_project a
				WHERE 1=1
					AND a.ProjectId = '".$projectId."'";
		
		$qry = $this->db->query($sql);
		// echo $sql;
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas = $rows;
			}
		}
		return $datas;
	}
	
	function _CheckExisting($param)
	{
		$conds = array('result' => 0);
		
		$sql = " select a.Id from t_gn_qa_scoring a where a.CustomerId = '".$param['CustomerId']."' ";
		$qry = $this->db->query($sql);
		
		if($qry->num_rows() > 0)
		{
			$conds = array('result' => 1);
		}
		
		return $conds;
	}
}
?>