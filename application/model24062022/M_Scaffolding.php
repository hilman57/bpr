<?php
class M_Scaffolding extends EUI_Model
{
	
var $_scafollding_tables = null; 	
var $_scafollding_perpage = 0;
var $_scafollding_postpage = 0;	
var $_scafollding_start = 0;
var $_scafollding_order = null;
var $_scafollding_type = null;
var $_scafollding_styles = null;


/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
  
 	
function _set_scaffolding_tables( $tables  = null ) {
	if( !is_null($tables) ){
		$this->_scafollding_tables = $tables;
	}
}


/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
function _set_scaffolding_attribute()
{
	$this->_scafollding_order = $this->URI->_get_post('order_by');
	$this->_scafollding_type = $this->URI->_get_post('type');
	
	if( !is_null($this->_scafollding_order) 
		AND  $this->_scafollding_type = $this->URI->_get_post('type') )
	{
		
		$field = $this->_get_scaffolding_field();	
		if( in_array($this->URI->_get_post('order_by'), $field )  )
		{
			$this->_scafollding_styles[$this->URI->_get_post('order_by')] = strtolower($this->URI->_get_post('type'));
		}
	}
}

/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 
function _get_scaffolding_styles()
{
	if( is_array($this->_scafollding_styles) ){
		return $this->_scafollding_styles;
	}
	else
		return null;
}

/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 
 
function _get_scaffolding_field()
{
	$fields = $this->db->list_fields($this->_scafollding_tables);
	if( !is_array($fields) ) return FALSE;
		
		
	return $fields;	
}


/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 
function _set_scaffolding_perpage( $perpage = 0 ) 
{
	$this->_scafollding_perpage = $perpage;	
}

/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 
function _set_scaffolding_postpage( $postpage = 0 ) 
{
	$this->_scafollding_postpage = $postpage;	
}

/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 
function _get_scaffolding_page()
{
	$_scf_record = $this->_get_scaffolding_count();
	
	$scf_page 	 = 0;
	
	if( $_scf_record )
	{
		$scf_page = ceil($_scf_record/ $this->_scafollding_perpage);
	}
	
	return (INT)$scf_page;
}

/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 
function _get_scaffolding_count() 
{
	$scf_count = 0;
	
	$this->db->select('count(*) as size ', FALSE);
	$this->db->from($this->_scafollding_tables);
	
	if( $size  = $this->db->get()->result_first_assoc() ){
		$scf_count = $size['size'];
	}
	return $scf_count;
}

/** 
 * @ def : _set_scaffolding_perpage 
 * -------------------------------------------------------------------------
  
 * @ param : integer  
 */
 

function _get_scaffolding_data() 
{
	
	$scf_data = array();
	
	if( $this->_scafollding_postpage ) 
		$this ->_scafollding_start = (($this->_scafollding_postpage-1)*$this->_scafollding_perpage);
	else {	
		$this ->_scafollding_start = 0;
	}
	
	$this->db->select('*');
	$this->db->from($this->_scafollding_tables);
	
	if( $this->URI->_get_have_post('order_by')){
		$this->db->order_by($this->URI->_get_post('order_by'), $this->URI->_get_post('type'));
	}
	$this->db->limit($this->_scafollding_perpage, $this ->_scafollding_start);
	
	$num = $this ->_scafollding_start+1;
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		foreach( $rows as $fields => $values )
		{
			$scf_data[$num][$fields] = $values;
		}
		
		$num++;
	}
	
	return $scf_data;
}


	
	
}
?>