<?php

/*
 * @ pack  : _aksessor 
 * @ model : contact detail 
 * @ notes : perlu di compare **
 */ 
 
 class M_ModContactDetail extends EUI_Model 
{
	
var $CustomerId = null;
var $DebiturId  = null;
var $Block  = null;

/*
 * @ pack : _aksessor 
 */ 
private static $Instance = null;
 
/*
 * @ pack : _aksessor 
 */ 
 
 public static function &Instance()
{
  if( is_null(self::$Instance) ) {
		self::$Instance = new self();
	}
	
  return self::$Instance;	
  
}
/*
 * @ pack : _aksessor 
 */ 
 
 public function M_ModContactDetail()
{
  $this->load->model(array(
	 'M_Combo', 
	 'M_ModAssignStatus', 
	 'M_RefAddPhoneType',
	 'M_PaymentHistory',
	 'M_Configuration',
	 'M_BlockingPhone',
	 'M_RefDiscount' ));
   
  if( is_null($this->DebiturId)){
	$this->DebiturId = $this->URI->_get_post('CustomerId');
	$this->Block =& M_BlockingPhone::Instance();
   }
 
}	

/*
 * @ selected accoun status if OK 
 */
 
 protected function & _get_selected_last_account_status( $debitur_id = 0 )
{
 $_last_account_status = array();
 $this->db->reset_select();
 $this->db->select('b.CallReasonCode, b.CallReasonDesc', FALSE);
 $this->db->from('t_gn_debitur a');
 $this->db->join('t_lk_account_status b ','a.deb_call_status_code = b.CallReasonCode', 'LEFT');
 $this->db->where('a.deb_id', $debitur_id);
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 ) 
 {
	foreach( $qry->result_assoc() as $rows ){
	 $_last_account_status[$rows['CallReasonCode']] = $rows['CallReasonDesc'];
	}
 }
 
 return $_last_account_status;
}

/*
 * @ pack : _aksessor 
 */ 

 public function _get_select_debitur( $CustomerId = 0 )
{

 $select_array = array();	
 $this->CustomerId = (int)(($CustomerId ? $CustomerId : $this->DebiturId));
 
 // reset select cache : upadte 2019/08/12 
 $this->db->reset_select();
 $this->db->select("a.* ", FALSE);
 $this->db->from("t_gn_debitur a");
 $this->db->where('a.deb_id',  $this->CustomerId);
 $qry = $this->db->get();
 if( $qry &&($qry ->num_rows() > 0 ))  {
	$select_array = $qry->result_first_assoc();
 }
 
 return $select_array;
 
} 
/* 
 * @ pack : function _get_account_status_level
 * @ auth : omen 
 * @ date : 2015-02-24 
 */
 
 public function _get_account_status_level()
{
   if( class_exists('M_ModAssignStatus') )
 {
	$AccountStatus =& self::_get_select_debitur();
	
	$Default = $AccountStatus['deb_call_status_code'];
	if( strlen($Default) < 2 ){
		$Default = $AccountStatus['deb_prev_call_status_code'];
	}
	
	$_level_account_status = $this->M_ModAssignStatus->_get_level_status_privileges( $Default );
	if( !is_array($_level_account_status) )  {	
		return array();
	}else if(count($_level_account_status) > 0) {
		return (array)$_level_account_status;
	} else {
		$las_acc_status =&self::_get_selected_last_account_status($AccountStatus['deb_id']); 
		return (array)$las_acc_status;
	}
	
 } else {
	return array();
 }
 
 
}

/* 
 * @ pack : function _get_call_status_level
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
public function _get_call_status_level()
{
  $conds_callstatus = array(); 
  $NotNedded = & $this->M_SetCallResult->_getNotInterest();
  $call_status =& $this->M_Combo->_getCallResult();
  
  if(is_array($call_status)) 
	foreach($call_status as $code => $values )
  {
	if( !in_array($code,  array_keys($NotNedded) ) ) 
	 {
		$conds_callstatus[$code] = $values;	
	 }
  }
  
  return $conds_callstatus;
}

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 

 public function _get_select_hours() 
{
  $Ih = array(); for( $i=0; $i<=23; $i++) 
  {
	$i_s = (string)sprintf('%02d', $i);
	$Ih[$i_s] = $i_s;
  }
	
 return $Ih;
  
} 

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 

 public function _get_select_minutes() 
{
  $iM = array();
  for( $i = 0; $i<=59; $i++) 
  {
	$iM[sprintf('%02d',$i)] = (string)sprintf('%02d', $i);
  }
  
 return $iM;
  
} 

/* 
 * @ pack : function get Phone Number spesific
 * @ auth : omens 
 * @ date : 2015-02-24 
 *
 */
 
 public function _get_select_primary_phone()
{

 $select_phone = array();
 $rows =& self::_get_select_debitur();
 $block = $this->Block->_get_select_bloking( $this->CustomerId );
 
 
 if(is_array($rows) )
{
 foreach( array('deb_home_no1'=>'HOME', 'deb_office_ph1'=>'OFF', 'deb_mobileno1'=>'PH' ) 
	AS $label => $values ) 
 {
	if(isset($rows[$label]) AND !empty($rows[$label]))
	{
	  if(!in_array($rows[$label], array_keys($block)) )
	  {	 
		if( $phoneNumber=(string)_getMasking($rows[$label]) ){
			$select_phone[base64_encode($rows[$label])] = (string)"{$values} - {$phoneNumber}";
		}
	  }
	}	
 }
 
 }
 
 return $select_phone;
 
} 
 
/* 
 * @ pack : function _get_select_emergency_phone
 * @ auth : omens 
 * @ date : 2015-02-24 
 *
 */
 
 public function _get_select_emergency_phone()
{

 $select_phone = array();
 $rows =& self::_get_select_debitur();
 $block = $this->Block->_get_select_bloking( $this->CustomerId );
 
 if(is_array($rows) )
{
    foreach( array('deb_ec_phone_f'=>'EC1','deb_ec_mobile_f'=>'EC2') 
		as $label => $values ) 
	{
		if(isset($rows[$label]) AND !empty($rows[$label]))
		{
		   //if(!in_array($rows[$label], array_keys($block)) )
		   //{		
				$phoneNumber = (string)_getMasking($rows[$label]);
				if( !empty($phoneNumber) ) {
					$select_phone[base64_encode($rows[$label])] = (string)"{$values} - {$phoneNumber}";
				}	
		   //} 	
		}	
	}
 }
 
 return $select_phone;
 
} 

/* 
 * @ pack : function Base On 
 * @ auth : omens 
 * @ date : 2015-02-24 
 *
 */
 
 public function _get_baseon_payment()
{

 $rows =& self::_get_select_debitur();
 $conds = array();
 
  if(is_array($rows)) 
 {
   $tots_amount_wo =(INT)$rows['deb_amount_wo'];
   $tots_balance_py =(INT)$rows['deb_bal_afterpay'];
   $tots_lastpayment = 0;
   
   if( $tots_amount_wo ) 
   {
	 $tots_affterpay = (($tots_amount_wo) - $tots_lastpayment);
	 if(  $tots_lastpayment )
	 {
		$conds[$tots_affterpay] = 'Afterpay';
	 }
   }
    
  $conds[$tots_amount_wo] = 'Amount WO';
  $conds[$tots_balance_py] = 'Balace Afterpay';
  
 }
 
 return $conds;
 
}

/* 
 * @ pack : function Base On 
 * @ auth : omens 
 * @ date : 2015-02-24 
 *
 */
 
 public function _get_select_tenor()
{
 
 $tenors = array();
 $config =& M_Configuration::get_instance(); // @ pack : instance 
 $result = $config->_getTenor();
 if( is_array($result)) 
	foreach($result as $rows )	 
 {
	$tenors[$rows['ConfigValue']]= $rows['ConfigName'];	
 }
 
 return $tenors;
 
}

/* 
 * @ pack : function _get_select_emergency_phone
 * @ auth : omens 
 * @ date : 2015-02-24 
 *
 */
 
 public function _get_select_additional_phone()
{
  $add_setphone = array();	
  $add_phones = $this->M_RefAddPhoneType->_get_detail_add( $this->DebiturId );
  $block = $this->Block->_get_select_bloking( $this->CustomerId );
  
  foreach( $add_phones as $phone => $values )
  {
	if(!in_array( $phone, array_keys($block)) )
	{
		$add_setphone[base64_encode($phone)] = $values;
	}
  }
  
  if(is_array($add_setphone) ){
	return $add_setphone;
  } else {
	return array();
  }
}   

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 

 public function _get_select_payment()
{
  $params = $this->URI->_get_all_request();
  $result = array();
  
  if( is_array($params) ) 
  {
	$month = $params['month'];
	$payment = $params['payment'];
	
// @ pack : get all date its 
	
	$end_month = date("Y-m-d", strtotime("$month month"));
	$start_month = date('Y-m');
	$avg_payment = round( ($payment/$month),0);
	
// @ pack : then set its.

	while(true)
	{
	  $d_s = (string)$start_month . '-' . date('d');
	  $result[$d_s] = $avg_payment;
	 
	  if($d_s==$end_month ) break;
		$start_month = NextMonth($start_month);
	}
  }
  array_shift($result);
  return $result;
  
}

/*
 * @ pack : get last phone to dial 
 */
 
public function _get_last_call_phone()
{
  $LastCallNumber = NULL;
  
  $this->db->reset_select();
  $this->db->select("a.CallNumber",FALSE);
  $this->db->from("t_gn_callhistory a");
  $this->db->where("a.CustomerId", $this->DebiturId);
  $this->db->order_by('a.CallHistoryId','DESC');
  $this->db->limit(1);
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0 ) 
  {
	if( $rows = $qry->result_first_assoc() )
	{
		$base64_encode = (string)$rows['CallNumber'];
		if( !empty($base64_encode) ){
			$LastCallNumber = base64_encode($base64_encode);
		}
	}
  }
  return $LastCallNumber;	
}

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _get_select_rit_maping( $AccountID  = 0 )
{

 $data_result = array();
 $this->db->reset_select();
 
 $this->db->select("
	b.deb_acct_no as CardNo, 
	a.Card as AccountNo,
	a.ClassCard as ClassCard,
	NULL as Deskoll,
	NULL as LastCallDate,
	NULL as AccountStatus,
	'RIT' as DataFrom,
	c.deb_acct_no as DataExist,
	(select ch.deb_amount_wo from t_gn_debitur ch where ch.deb_cardno = a.Card) as AmountWO", FALSE);
 
 
 $this->db->from ("t_gn_account_mapping a ");
 $this->db->join("t_gn_debitur b "," a.CardNo=b.deb_acct_no", "LEFT");
 $this->db->join("t_gn_debitur c "," a.Card=c.deb_acct_no", "LEFT");
 $this->db->where("c.deb_acct_no is null", "", FALSE);
 $this->db->where("a.CardNo", $AccountID);
 
// echo $this->db->_get_var_dump();

 $qry = $this->db->get();
 if( $qry->num_rows() > 0 ) 
	foreach( $qry->result_assoc() as $rows )
 {
   $data_result[] = $rows;
 }
 	
 return $data_result;
	
}


/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
public function _get_select_other_account()
{
  $recsource = array();
  
  $_Account =& $this->_get_select_debitur();
  $_AccountID =(string)$_Account['deb_acct_no'];
  $_IdentitasID =(string)$_Account['deb_id_number'];
  $_DebiturId =(string)$_Account['deb_id'];
  $_CampaignId =(int)$_Account['deb_cmpaign_id'];
  
  // get data RIT ======================>  
  $_Data_RIT = $this->_get_select_rit_maping($_AccountID);
  
// reset select data array ======================>
 
  $this->db->reset_select();
  
   $this->db->select("
	a.deb_id_number as IdNumber, 
	a.deb_acct_no as AccountNo, 
	a.deb_agent as Deskoll, 
		a.deb_call_activity_datets as LastCallDate, 
	NULL as ClassCard, 
	b.CallReasonDesc as AccountStatus,
	'AKS' as DataFrom ", 
	FALSE);
	
 $this->db->from("t_gn_debitur a ");
 $this->db->join("t_lk_account_status b "," a.deb_call_status_code=b.CallReasonCode","LEFT");
 $this->db->where_in('a.deb_id_number',$_IdentitasID);
 $this->db->where_not_in('a.deb_id',$_DebiturId);
 
 // @ pack : result assoc 
 // echo "<br />".$this->db->_get_var_dump();
  
  $account_results = array();
  


  $qry = $this->db->get();
  if( $qry->num_rows()>0 ) 
	foreach($qry->result_assoc() as $rows )
  {
	$account_results[] = $rows; 

  }
 
 // resulting data join array ==================> 
 
  foreach( array($_Data_RIT,$account_results ) as  
	$kesy => $values )
 {
	foreach( $values as $_ux => $_uv ){
		 $recsource[] = $_uv;	
	}
 }
	// echo "<pre>";
	// print_r($recsource);
	// echo "</pre>";
	

  return $recsource;
}

/*
 * @pack : _get_last_payment
 */
 
 public function _get_last_payment( $DebiturId = 0 )
{
  $last_payment = 0;
  
  $this->db->reset_select();
  $this->db->select("
		IF( (b.pay_amount IS NULL OR b.pay_amount=0 OR b.pay_amount='' ), 
		a.deb_last_pay, b.pay_amount ) as LastPayment ", FALSE);
  
  $this->db->from("t_gn_debitur a");
  $this->db->join("t_gn_payment b ","a.deb_id=b.deb_id","LEFT");
  $this->db->where("a.deb_id",$DebiturId);
  $this->db->order_by("b.pay_created_ts","DESC");
  $this->db->limit(1);
  
  $qry = $this->db->get();
  if( $qry->num_rows()>0 )
  {
	 if( $rows = $qry->result_first_assoc() )
	 {
		$last_payment = $rows['LastPayment'];
	 }
  }
  
  return $last_payment;
}  

/*
 * @pack : _get_total_payment
 */
 
 public function _get_total_payment( $DebiturId = 0 )
{
  $total_payment = 0;
  
  $this->db->reset_select();
  $this->db->select("
		(SUM(IF(a.deb_last_pay is not null, a.deb_last_pay,0))+
		 SUM(IF(b.pay_amount is not null, b.pay_amount,0))) as TotalPayment", 
  FALSE);
  
  $this->db->from("t_gn_debitur a");
  $this->db->join("t_gn_payment b ","a.deb_id=b.deb_id","LEFT");
  $this->db->where("a.deb_id",$DebiturId);
  $this->db->group_by("a.deb_id");
  
  $qry = $this->db->get();
  echo "<!-- ".$this->db->last_query()." -->";
  if( $qry->num_rows()>0 )
  {
	 if( $rows = $qry->result_first_assoc() )
	 {
		$total_payment = $rows['TotalPayment'];
	 }
  }
  
  return $total_payment;
}  

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _get_template_sms()
{
  $tpl_sms = array();
  $this->db->reset_select();
  $this->db->select("a.SmsTplId, a.SmsTplSubject");
  $this->db->from("serv_sms_tpl a");
  $this->db->where("a.SmsTplFlags",1);
  
  // echo $this->db->_get_var_dump();
  foreach( $this->db->get()->result_assoc() as $rows )
  {
	 $tpl_sms[$rows['SmsTplId']] = $rows['SmsTplSubject'];	
  }
  
  return $tpl_sms;
} 

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */
 
 public function _get_is_aksess_all()
{
	$is_access_all = false;
	if( $this->URI->_get_have_post('BucketRandomId') )
	{
		$this->load->model('M_MgtRandDeb');
		$Rand_all =& M_MgtRandDeb::Instance();
		$deb_random = $Rand_all->CheckDebiturIsRandom( $this->URI->_get_post('BucketRandomId') );
		if(isset($deb_random['access_all_id']) AND count($deb_random['access_all_id']) > 0 AND !empty($deb_random['access_all_id']) )
		{
			$is_access_all = true;
		}
		
		if(isset($deb_random['round_id']) AND count($deb_random['round_id']) > 0 AND !empty($deb_random['round_id']) )
		{
			$is_access_all = true;
		}
	}
	
	
	return $is_access_all;
} // ==> _get_is_aksess_all
 

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _get_textmessage_sms( $SmsTplId = 0, $CallStatus = 0 )
{
	// echo $CallStatus;
  $tpl_sms = null;
  $this->db->reset_select();
  $this->db->select("a.SmsTplContent");
  $this->db->from("serv_sms_tpl a");
  $this->db->where("a.SmsTplId",$SmsTplId);
  if($CallStatus>0){
	$this->db->where("a.SmsTplCallStatus like '%".$CallStatus."%'");
  }
  // echo $this->db->_get_var_dump();
  if( $rows = $this->db->get()->result_first_assoc() ) 
  {
	 $tpl_sms = (string)$rows['SmsTplContent'];
  }
  
  return $tpl_sms;
  
} 

/* 
 * @ pack : function _get_select form
 * @ auth : omen 
 * @ date : 2015-02-24 
 */
 
public function _get_user_form()
{
	return array(
		'FormLunas' => 'Form Lunas',
		'FormDiscount' => 'Form Discount' 
	);
} 

 
/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _get_select_discount()
{
	$M_RefDiscount = M_RefDiscount::Instance();
	if( is_object($M_RefDiscount) )
	{
		return (array)$M_RefDiscount->_get_select_disc();
	} else {
		return array();
	}	
}	
 
/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _get_select_dropdown()
{
	$phone = $this->_get_select_primary_phone();
	$dropdown = array(
		'COLL_DROPDOWN_SPC' => $this->M_Combo->_getSPC(),
		'COLL_DROPDOWN_RPC' => $this->M_Combo->_getRPC(),
		'COLL_DROPDOWN_PTP' => $this->M_Combo->_getInfoPTP(),
		'COLL_DROPDOWN_PCH' => $this->M_Combo->_getPaymentChannel(),
		'COLL_DROPDOWN_CLS' => $this->_get_call_status_level(),
		'COLL_DROPDOWN_ACS' => $this->_get_account_status_level(),
		'COLL_DROPDOWN_HRS' => $this->_get_select_hours(),
		'COLL_DROPDOWN_MNT' => $this->_get_select_minutes(),
		'COLL_DROPDOWN_PMR' => $this->_get_select_primary_phone(),
		'COLL_DROPDOWN_EMC' => $this->_get_select_emergency_phone(),
		'COLL_DROPDOWN_ADD' => $this->_get_select_additional_phone(),
		'COLL_DROPDOWN_BON' => $this->_get_baseon_payment(),
		'COLL_DROPDOWN_TNR' => $this->_get_select_tenor(),
		'COLL_CUSTOMER_PHN' => $this->_get_last_call_phone(),
		'COLL_TEMPLATE_SMS'	=> $this->_get_template_sms(),
		'COLL_CUSTOMER_FRM' => $this->_get_user_form(),
		'COLL_DROPDOWN_DIS' => $this->_get_select_discount(),
		'COLL_AKSESS_ALL'	=> $this->_get_is_aksess_all()
	);
	
	return $dropdown;
} 
 
/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _setUdateCall()
{
  $cond = 0;
  
  $CustomerId= _get_post('CustomerId');
  if( $CustomerId )
  {
  
 /*
  * @ pack : if is Handle if User Followup Level not Deskoll Require True Only Like Its 
  */
	$ActiveIn = array(
		USER_AGENT_INBOUND, 
		USER_AGENT_OUTBOUND 
	);
		
	if( ! in_array( _get_session('HandlingType'), $ActiveIn) ) {
		return TRUE;
	}	
		 
	// @ pack : this level deskoll block ITS 	
	
	$this->db->set('deb_is_call', 0);
	$this->db->set('deb_call_activity_datets', date('Y-m-d H:i:s'));
    $this->db->where('deb_is_call', 1);
    $this->db->where('deb_id', $CustomerId );
	$this->db->update('t_gn_debitur');
	
	if($this->db->affected_rows()> 0) 
	{
		$cond++;
	}
  }
   
  return $cond;
}
 
/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _getIsCall()
{
   $cond = 0;
   $CustomerId = _get_post('CustomerId');
   if( $CustomerId )
   {
	 
	 /*
	  * @ pack : if is Handle if User Followup Level not 
				Deskoll Require True Only Like Its 
	  */
		 $ActiveIn = array(
			USER_AGENT_INBOUND, 
			USER_AGENT_OUTBOUND ,

		);
		
		if( ! in_array( _get_session('HandlingType'), $ActiveIn) ) {
			return TRUE;
		}	
		 
	// @ pack : this level deskoll block ITS 	

	 $this->db->set('deb_is_call', 1);
	 $this->db->set('deb_call_activity_datets', date('Y-m-d H:i:s'));
     
	 $this->db->where('deb_is_call', 0);
	 $this->db->where('deb_is_lock', 0);
     $this->db->where('deb_id', $CustomerId );
	 $this->db->update('t_gn_debitur');
	 
	 if(  $this->db->affected_rows()> 0 ) {
		$cond++;
	 }
   }
   
   return $cond;
}

/* 
 * @ pack : function _get_select_dropdown
 * @ auth : omen 
 * @ date : 2015-02-24 
 */ 
 
 public function _get_select_data_maping()
{
 
 $this->db->select('a.deb_id, a.deb_cmpaign_id', FALSE);
 $this->db->from('t_gn_debitur a');
 $this->db->where('a.deb_acct_no', _get_post('Account'));
 $qry = $this->db->get();
 if( $qry->num_rows()>0 ) 
 {
	return $qry->result_first_assoc();
 } else {
	return FALSE;
 }
}

	public function ListGrid( $out )
	{
		$per_page = 10;
		$page_select = (INT)_get_post('page');
		$start_page = 0;
		
		// @ pack : set its  
		if( $page_select ) {
			$start_page = (($page_select-1)*$per_page);
		} else {	
			$start_page = 0;
		}
		
		$array_result = array();
		$this->db->reset_select();
		
		$this->db->select("
			a.id_prospect_note as id,
			b.code_user,
			b.full_name,
			a.note,
			a.create_date", 
		FALSE); 
		
		$this->db->from("t_gn_prospect_note a");
		$this->db->join("t_tx_agent b "," a.user_id=b.UserId","INNER");
		$this->db->where('a.debitur_id',$out->get_value('CustomerId','intval'));
		
		if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
			array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND, USER_SENIOR_TL) ) )
		{
			$this->db->where('a.user_id',_get_session('UserId'));	
		}
		
		if( _get_have_post('orderby') ){
			$this->db->order_by(_get_post('orderby'),_get_post('type'));
		}
		
		
		$this->db->limit($per_page, $start_page);
		// echo $this->db->print_out();
		$qry = $this->db->get();
		if( $qry->num_rows() > 0 ) 
		{
			$array_result  = $qry->result_assoc();
		}
		return $array_result;
	}
	
	public function TotalGrid( $out )
	{
		$total = 0;
		$this->db->from("t_gn_prospect_note a");
		$this->db->join("t_tx_agent b "," a.user_id=b.UserId","INNER");
		$this->db->where('a.debitur_id',$out->get_value('CustomerId','intval'));
		
		if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
			array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND, USER_SENIOR_TL) ) )
		{
			$this->db->where('a.user_id',_get_session('UserId'));	
		}
		
		$total = $this->db->count_all_results();
		return $total;
	}
 
// END OF CLASS 
 
}

?>