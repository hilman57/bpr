<?php
class M_SaveWorkProjectForm extends EUI_Model
{


protected $FollowGroup  = null;
protected $FollowName   = array();
protected $IsReadyGroup = array();
protected $QuestionId  = 0;

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */ 
 
public function __construct()
{
	$this->setFollowupGroupCode();
	$this->FollowName = array 
	(
		'700'=> array('name' => 'FU_POST', 'call_func' => '_setSavePost', 'user_update' =>'_setUpdatePost' ),
		'800'=> array('name' => 'FU_AGENCY_SUPPORT','call_func' => '_setSaveAgencySuport','user_update' => '_setUpdateAgencySuport'),
		'900'=> array('name' => 'FU_BANCA_SUPPORT','call_func' => '_setSaveBancassSuport', 'user_update' => '_setUpdateBancassSuport'),
		'999'=> array('name' => 'FU_VISIT_TO_BRANCH','call_func' => '_setSaveVisitToBranch', 'user_update' => '_setUpdateVisitToBranch'), ''
	);
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function setFollowupGroupCode()
 {
	static $config = array();
	if( $this->URI->_get_have_post('FollowupGroupCode') ) 
	foreach( $this->URI->_get_array_post('FollowupGroupCode') as $k => $GroupCode )
	{	
		$this->FollowGroup[$GroupCode] = $GroupCode;
	}
	 
	 
	// LOOK OF READY GROUP THEN DELETED 
	
	$this->IsReadyGroup = $this->URI->_get_array_post('IsReadyGroup');	
	 
 }
 
 
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */ 
protected function field()
{
	$fields = array
	(
		'CustomerId' 			=> 'FuCustId', 
		'CIFNumber'				=> 'FuCIF',
		'PolicyNumber' 			=> 'FuPolicyNo',
		'Address1'				=> 'FuAddr1',
		'Address2'				=> 'FuAddr2',
		'Address3' 				=> 'FuAddr3',
		'Address4' 				=> 'FuAddr4',
		'City' 					=> 'FuCity',
		'ZIP' 					=> 'FuZip',
		'Province' 				=> 'FuProvince',
		'Country' 				=> 'FuCountry', 
		'Email' 				=> 'FuEmail',
		'PaymentFrequency' 		=> 'FuPaymentFreq',
		'Home'					=> 'FuHomePhone',
		'Office'				=> 'FuOfficePhone',
		'Mobile'				=> 'FuMobilePhone',
		'FuQuestionId'			=> 'FuQuestionId'
	);
	
	return $fields;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
public function _set_insert_history($param = array('GroupCode' => 0, 'PolicyNumber' => 0, 'QuestionId' => 0 ))
{
	$this->db->select('*');
	$this->db->from('t_gn_followup');
	$this->db->where('FuCreatedDate', date('Y-m-d'));
	$this->db->where('FuGroupCode',$param['GroupCode']);
	$this->db->where('FuPolicyNo', $param['PolicyNumber']);
	$this->db->where('FuQuestionId', $param['QuestionId']);
	foreach( $this->db->get()->result_first_assoc() as $field => $values )
	{
		$this->db->set($field, $values);	
	}
	
	$this->db->insert('t_gn_followup_history');	
	
	if( $this->db->affected_rows() > 0 ){
		return true;
	}	
	else
		return false;
} 

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
 protected function _detail_policy_id( $PolicyId  = 0)
 {
	static $config = array();
	
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_gn_policy_detail');
	$this->db->where('PolicyId', $PolicyId);
	
	foreach( $this->db->get()->result_assoc() as $rows )  
	{
		foreach( $rows as $field => $value ) {
			$config[$field] = $value;
		}
	}
	
	return $config;
 }

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
 
public function _setSavePost( $FuGroupCode ) 
{

	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuGroupCode', $FuGroupCode);
		$this->db->set('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->set('FuCIF', $PolicyData['CIFNumber']);
		$this->db->set('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->set('FuAddr1', strtoupper($this->URI->_get_post('ADDRESS1_FU_POST')));
		$this->db->set('FuAddr2', strtoupper($this->URI->_get_post('ADDRESS2_FU_POST')));
		$this->db->set('FuAddr3', strtoupper($this->URI->_get_post('ADDRESS3_FU_POST')));
		$this->db->set('FuAddr4', strtoupper($this->URI->_get_post('ADDRESS4_FU_POST')));
		$this->db->set('FuCity', strtoupper($this->URI->_get_post('CITY_FU_POST')));
		$this->db->set('FuZip', strtoupper($this->URI->_get_post('ZIP_FU_POST')));
		$this->db->set('FuProvince', strtoupper($this->URI->_get_post('PROVINCE_FU_POST')));
		$this->db->set('FuEmail', strtoupper($this->URI->_get_post('EMAIL_FU_POST')));
		$this->db->set('FuCountry', strtoupper($this->URI->_get_post('COUNTRY_FU_POST')));
		$this->db->set('FuPaymentFreq', strtoupper($this->URI->_get_post('PAYMENTFREQUENCY_FU_POST')));
		$this->db->set('FuHomePhone', strtoupper($this->URI->_get_post('HOME_FU_POST')));
		$this->db->set('FuOfficePhone', strtoupper($this->URI->_get_post('OFFICE_FU_POST')));
		$this->db->set('FuMobilePhone', strtoupper($this->URI->_get_post('MOBILE_FU_POST')));
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_POST')));
		$this->db->set('FuCreatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuCreatedDate', date('Y-m-d'));
		$this->db->set('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
		$this->db->set('FuQuestionId', $this->QuestionId);
		$this->db->insert('t_gn_followup');
		
		if( $this->db->affected_rows() > 0 ) { 
			$conds++;
		} 
		else 
		{
			$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_BANCA_SUPPORT')));
			$this->db->set('FuAddr1', strtoupper($this->URI->_get_post('ADDRESS1_FU_POST')));
			$this->db->set('FuAddr2', strtoupper($this->URI->_get_post('ADDRESS2_FU_POST')));
			$this->db->set('FuAddr3', strtoupper($this->URI->_get_post('ADDRESS3_FU_POST')));
			$this->db->set('FuAddr4', strtoupper($this->URI->_get_post('ADDRESS4_FU_POST')));
			$this->db->set('FuCity', strtoupper($this->URI->_get_post('CITY_FU_POST')));
			$this->db->set('FuZip', strtoupper($this->URI->_get_post('ZIP_FU_POST')));
			$this->db->set('FuProvince', strtoupper($this->URI->_get_post('PROVINCE_FU_POST')));
			$this->db->set('FuEmail', strtoupper($this->URI->_get_post('EMAIL_FU_POST')));
			$this->db->set('FuCountry', strtoupper($this->URI->_get_post('COUNTRY_FU_POST')));
			$this->db->set('FuPaymentFreq', strtoupper($this->URI->_get_post('PAYMENTFREQUENCY_FU_POST')));
			$this->db->set('FuHomePhone', strtoupper($this->URI->_get_post('HOME_FU_POST')));
			$this->db->set('FuOfficePhone', strtoupper($this->URI->_get_post('OFFICE_FU_POST')));
			$this->db->set('FuMobilePhone', strtoupper($this->URI->_get_post('MOBILE_FU_POST')));
			$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_POST')));
			$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
			
			// where 
			
			$this->db->where('FuGroupCode', $FuGroupCode);
			$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
			$this->db->where('FuCIF', $PolicyData['CIFNumber']);
			$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
			$this->db->where('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
			$this->db->update('t_gn_followup');
		}
	}
		
}



/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function _setSaveBancassSuport($FuGroupCode)
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuGroupCode', $FuGroupCode);
		$this->db->set('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->set('FuCIF', $PolicyData['CIFNumber']);
		$this->db->set('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_BANCA_SUPPORT')));
		$this->db->set('FuCreatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuCreatedDate', date('Y-m-d'));
		$this->db->set('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
		$this->db->set('FuQuestionId', $this->QuestionId);
		$this->db->insert('t_gn_followup');
		
		if( $this->db->affected_rows() > 0 ) { 
			$conds++;
		} 
		else 
		{
			$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_BANCA_SUPPORT')));
			$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
			$this->db->where('FuGroupCode', $FuGroupCode);
			$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
			$this->db->where('FuCIF', $PolicyData['CIFNumber']);
			$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
			$this->db->where('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
			
			$this->db->update('t_gn_followup');
		}
	}
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

 
protected function _setSaveAgencySuport($FuGroupCode)
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuGroupCode', $FuGroupCode);
		$this->db->set('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->set('FuCIF', $PolicyData['CIFNumber']);
		$this->db->set('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_AGENCY_SUPPORT')));
		$this->db->set('FuCreatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuCreatedDate', date('Y-m-d'));
		$this->db->set('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
		$this->db->set('FuQuestionId', $this->QuestionId);
		$this->db->insert('t_gn_followup');
		
		if( $this->db->affected_rows() > 0 ) { 
			$conds++;
		} 
		else 
		{
			$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_AGENCY_SUPPORT')));
			$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
			$this->db->where('FuGroupCode', $FuGroupCode);
			$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
			$this->db->where('FuCIF', $PolicyData['CIFNumber']);
			$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
			$this->db->where('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
			$this->db->update('t_gn_followup');
		}
	}
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

 
public function _setSaveVisitToBranch( $FuGroupCode )
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuGroupCode', $FuGroupCode);
		$this->db->set('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->set('FuCIF', $PolicyData['CIFNumber']);
		$this->db->set('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_VISIT_TO_BRANCH')));
		$this->db->set('FuCreatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuCreatedDate', date('Y-m-d'));
		$this->db->set('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
		$this->db->set('FuQuestionId', $this->QuestionId);
		$this->db->insert('t_gn_followup');
		
		if( $this->db->affected_rows() > 0 ) { 
			$conds++;
		} 
		else 
		{
			$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_VISIT_TO_BRANCH')));
			$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
			$this->db->where('FuGroupCode', $FuGroupCode);
			$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
			$this->db->where('FuCIF', $PolicyData['CIFNumber']);
			$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
			$this->db->where('FuCreatedBy', $this->EUI_Session->_get_session('UserId'));
			$this->db->update('t_gn_followup');
		}
	}
	
	// echo $this->db->last_query();
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function SetSaveFollowupProject( $QuestionId )
{


 $this->QuestionId = $QuestionId;
 if( is_array($this->FollowGroup) )
 {
	foreach( $this->FollowGroup as $key => $code )
	{
		if( in_array($code, array_keys( $this->FollowName)) )
		{
			if( $InstanceOf = $this->FollowName[$code] ) {
				$this->{$InstanceOf['call_func']}($code);
			}
		}
	}
	
	$this->QuestionId = 0;
 }
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function _setUpdatePost( $FuGroupCode ) 
{

	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	
	$this->_set_insert_history(array('GroupCode' => $FuGroupCode, 
									  'PolicyNumber' => $PolicyData['PolicyNumber'], 
									  'QuestionId' => $this->QuestionId ));
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_BANCA_SUPPORT')));
		$this->db->set('FuAddr1', strtoupper($this->URI->_get_post('ADDRESS1_FU_POST')));
		$this->db->set('FuAddr2', strtoupper($this->URI->_get_post('ADDRESS2_FU_POST')));
		$this->db->set('FuAddr3', strtoupper($this->URI->_get_post('ADDRESS3_FU_POST')));
		$this->db->set('FuAddr4', strtoupper($this->URI->_get_post('ADDRESS4_FU_POST')));
		$this->db->set('FuCity', strtoupper($this->URI->_get_post('CITY_FU_POST')));
		$this->db->set('FuZip', strtoupper($this->URI->_get_post('ZIP_FU_POST')));
		$this->db->set('FuProvince', strtoupper($this->URI->_get_post('PROVINCE_FU_POST')));
		$this->db->set('FuEmail', strtoupper($this->URI->_get_post('EMAIL_FU_POST')));
		$this->db->set('FuCountry', strtoupper($this->URI->_get_post('COUNTRY_FU_POST')));
		$this->db->set('FuPaymentFreq', strtoupper($this->URI->_get_post('PAYMENTFREQUENCY_FU_POST')));
		$this->db->set('FuHomePhone', strtoupper($this->URI->_get_post('HOME_FU_POST')));
		$this->db->set('FuOfficePhone', strtoupper($this->URI->_get_post('OFFICE_FU_POST')));
		$this->db->set('FuMobilePhone', strtoupper($this->URI->_get_post('MOBILE_FU_POST')));
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_POST')));
		$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuUpdatedBy',$this->EUI_Session->_get_session('UserId'));
			// where 
			
		$this->db->where('FuGroupCode', $FuGroupCode);
		$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->where('FuCIF', $PolicyData['CIFNumber']);
		$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->where('FuQuestionId', $this->QuestionId);
		$this->db->update('t_gn_followup');
	}
		
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function _setUpdateBancassSuport($FuGroupCode)
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	$this->_set_insert_history(array('GroupCode' => $FuGroupCode, 
									  'PolicyNumber' => $PolicyData['PolicyNumber'], 
									  'QuestionId' => $this->QuestionId ));
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_BANCA_SUPPORT')));
		$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuUpdatedBy',$this->EUI_Session->_get_session('UserId'));
		$this->db->where('FuGroupCode', $FuGroupCode);
		$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->where('FuCIF', $PolicyData['CIFNumber']);
		$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->where('FuQuestionId', $this->QuestionId);
		$this->db->update('t_gn_followup');
		
	}
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

protected function _setUpdateAgencySuport($FuGroupCode)
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	$this->_set_insert_history(array('GroupCode' => $FuGroupCode, 
									  'PolicyNumber' => $PolicyData['PolicyNumber'], 
									  'QuestionId' => $this->QuestionId ));
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_AGENCY_SUPPORT')));
		$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuUpdatedBy',$this->EUI_Session->_get_session('UserId'));
		$this->db->where('FuGroupCode', $FuGroupCode);
		$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->where('FuCIF', $PolicyData['CIFNumber']);
		$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);;
		$this->db->update('t_gn_followup');
	}
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function _setUpdateVisitToBranch( $FuGroupCode )
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	$this->_set_insert_history(array('GroupCode' => $FuGroupCode, 
									  'PolicyNumber' => $PolicyData['PolicyNumber'], 
									  'QuestionId' => $this->QuestionId ));
	if( !is_null($FuGroupCode) 
		AND is_array($PolicyData) )
	{
		$this->db->set('FuNotes', strtoupper($this->URI->_get_post('NOTES_FU_VISIT_TO_BRANCH')));
		$this->db->set('FuUpdatedTs',date('Y-m-d H:i:s'));
		$this->db->set('FuUpdatedBy',$this->EUI_Session->_get_session('UserId'));
		$this->db->where('FuGroupCode', $FuGroupCode);
		$this->db->where('FuCustId', $this->URI->_get_post('CustomerId'));
		$this->db->where('FuCIF', $PolicyData['CIFNumber']);
		$this->db->where('FuPolicyNo', $PolicyData['PolicyNumber']);
		$this->db->where('FuQuestionId', $this->QuestionId);
		$this->db->update('t_gn_followup');
	}
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

public function _SetUpdateFollowupProject( $QuestionId )
{
  $this->QuestionId = $QuestionId;	
  if( is_array($this->FollowGroup) ) 
  {
	
	foreach( $this->FollowGroup as $key => $code )
	{
		// UPDATE OR INSERT
		$array_insert = array();
		$array_update = array();
		
		if( in_array($code, array_keys( $this->FollowName)) )
		{
			if( $InstanceOf = $this->FollowName[$code] ) 
			{
				if( $this->cek_question_by_group($QuestionId, $code ) ) {
					$this->{$InstanceOf['user_update']}($code);
					$array_update[$code] = $code;
				}else {
					$this->{$InstanceOf['call_func']}($code);
					$array_insert[$code] = $code;  
				}	
			}		
		}
		
		// DELETED DATA FOLLOWGROUP 
		// print_r($this->IsReadyGroup);
		// echo $code;
		
		$arrs = array();
		
		if(is_array($this->IsReadyGroup))
		foreach( $this->IsReadyGroup as $ky => $ready_code )
		{
			if( !in_array($ready_code,$array_update) 
				AND in_array($ready_code,$array_insert) )
			{
				self::DeleteWorkProjectByCode($ready_code);
			}
		}
		
		
	}
	
	
	
  }
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
protected function DeleteWorkProjectByCode( $ready_code )
{
	$conds = 0;
	$PolicyData =& $this->_detail_policy_id( $this->URI->_get_post('PolicyId') );
	
	$this->_set_insert_history(array('GroupCode' => $ready_code, 
									  'PolicyNumber' => $PolicyData['PolicyNumber'], 
									  'QuestionId' => $this->QuestionId ));
	
	$this->db->where("FuGroupCode", $ready_code);
	$this->db->where("FuPolicyNo", $PolicyData['PolicyNumber']);
	$this->db->where("FuCustId", $this->URI->_get_post('CustomerId'));
	$this->db->delete('t_gn_followup');
	if( $this->db->affected_rows() > 0 ){
		return TRUE;
	}
	else
		return FALSE;
}


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

protected function cek_question_by_group( $QuestionId=NULL, $GroupCode=NULL)
{
    $conds = false;
	
	if( (!is_null($QuestionId)) AND ($QuestionId!=0) 
		AND !is_null($GroupCode) )
	{
		$this->db->reset_select();
		$this->db->select('count(a.FuId) as jumlah', false);
		$this->db->from('t_gn_followup a');
		$this->db->where('a.FuQuestionId', $QuestionId);
		$this->db->where('a.FuGroupCode', $GroupCode);
		if( $rows = $this->db->get()->result_first_assoc() )
		{
			$conds =(INT)$rows['jumlah'];
		}
	}
	
	return $conds;

}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
public function _getFollowGroupByPolicyId( $PolicyId )
{
	$config['code']= array();
	$this->db->select('b.PolicyId, a.FuGroupCode',FALSE);
	$this->db->from('t_gn_followup a ');
	$this->db->join('t_gn_followup_question b ', 'a.FuQuestionId=b.QuestionId','LEFT');
	$this->db->where('b.PolicyId',$PolicyId);
	
	$rec = $this->db->get();
	$num = 0;
	if( $rec ->num_rows() > 0 ) foreach( $rec->result_assoc() as $rows ) {
		$config['code'][$num]= $rows['FuGroupCode'];	
		$num++;
	}
	
	return $config;
}

 
 // END OF CLASS 
}

?>