<?php
/*
 * E.U.I 
 *
 
 * subject	: get model data for user modul 
 * 			  extends under model class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
 class M_SysUser extends EUI_Model 
{

/* 
 * pack : instance of class 
 */
 
private static $Instance = null;

/* 
 * pack : instance of class 
 */

public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
}

/*
 *@ get default nav of query data 
 *@ will return to nav data 
 */
 
 function get_default() 
 {
  
//@ set pages 
	
	$this -> EUI_Page -> _setPage(20); 
	
//@ set pages  condition 
	$filter = ' AND a.user_resign = 0';
	
	if( $this -> URI -> _get_have_post('UserId') ) 
	{ 
		$filter = " AND ( a.UserId LIKE '%".$_REQUEST['UserId']."%'  
			 OR a.ip_address LIKE '%".$this -> URI -> _get_post('UserId')."%'  
			 OR a.full_name LIKE '%".$_REQUEST['UserId']."%'  OR a.id LIKE '".$_REQUEST['UserId']."')";
	}
	
//@ set query position default 
 
	$this -> EUI_Page -> _setQuery(
		 "SELECT  a.UserId as UserId FROM t_tx_agent a 
		  LEFT JOIN cc_agent b on a.id=b.userid 
		  LEFT JOIN t_gn_agent_profile d on a.profile_id=d.id" ); 
	
// cek login root atau bukan	

	if( $this -> EUI_Session->_get_session('HandlingType')!=USER_ROOT){
		$filter .= " AND d.id<>'" . USER_ROOT . "'";
	}
	
	$this -> EUI_Page -> _setWhere( $filter );   
	// echo $this->EUI_Page->_getCompiler();
	
	return $this -> EUI_Page;
 }
 
 
/*
 *@ get page content return to content list 
 *@ get content data 
 */
 
//  public function get_content()
//  {
// 	$this->EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
// 	$this->EUI_Page->_setPage(20);
	
//   /** cek if have the post userid **/
 
//    $this->EUI_Page->_setArraySelect(array( 
// 				"a.UserId as chk_menu" => array("chk_menu","ID", "primary"),
// 				"a.id as UserIds" => array("UserIds","* User Id"), 
// 				"a.init_name as init_name" => array("init_name","Online Name"), 
// 				"a.full_name as full_name" => array("full_name","Full Name"), 
// 				"d.name as profile_id" => array("profile_id","Privileges"),
// 				"IF((SELECT concat(f.id,'- ',f.full_name) FROM t_tx_agent f WHERE f.UserId=a.mgr_id) is null,'-',(SELECT f.full_name  from t_tx_agent f where f.UserId=a.mgr_id)) as Manager" => array("Manager","Manager."),
// 				"IF((SELECT concat(f.id,'-',f.full_name) FROM t_tx_agent f WHERE f.UserId=a.spv_id) is null,'-',(SELECT f.full_name  from t_tx_agent f where f.UserId=a.spv_id)) as Spv" => array("Spv","Supervisor."),
// 				"IF((SELECT concat(f.id,'-',f.full_name) FROM t_tx_agent f WHERE f.UserId=a.tl_id) is null,'-',(SELECT f.full_name  from t_tx_agent f where f.UserId=a.tl_id)) as Leader" => array("Leader","Leader."),
// 				"IF(a.telphone=1,'Yes','No') as telphone" => array("telphone","Telephone "),
// 				"IF(logged_state=1,'Sign In','Sign Out') as logged_state" => array("logged_state","User Login"),
// 				"IF(a.user_state=1,'Active','UnActive') as user_state" => array("user_state","User Active"),
// 				"a.ip_address as ip_address" => array("ip_address","IP Location")
// 		));
		
// 		$this->EUI_Page->_setFrom("t_tx_agent a");
// 		$this->EUI_Page->_setJoin("cc_agent b "," a.id=b.userid","LEFT");
// 		$this->EUI_Page->_setJoin("t_gn_agent_profile d "," a.profile_id=d.id","LEFT"); 
// 		$this->EUI_Page->_setJoin("cc_agent_group e "," e.id=b.agent_group","LEFT");
// 		$this->EUI_Page->_setJoin("cc_agent_skill g "," b.id=g.agent","LEFT");
// 		$this->EUI_Page->_setJoin("cc_skill h "," g.skill=h.id ","LEFT", TRUE);
				
// 		$this->EUI_Page->_setAnd("a.user_resign is null OR a.user_resign = 0");
	
//  /** cek if have the post userid **/
 
// 	$filter = null;
	
// 	if(($this->URI->_get_have_post('UserId')))
// 	{ 
// 		$this->EUI_Page->_setAnd("( a.UserId LIKE '%".$this->URI->_get_post('UserId')."%'  
// 					OR a.full_name LIKE '%".$this->URI->_get_post('UserId')."%'  
// 					OR a.ip_address LIKE '%".$this->URI->_get_post('UserId')."%' 
// 					OR a.id LIKE '".$this->URI->_get_post('UserId')."')
// 			", FALSE);
// 	}
	
//   /** cek login root atau bukan	**/
  
// 	if(($this->EUI_Session->_get_session('HandlingType')!=USER_ROOT))
// 	{
// 		$this->EUI_Page->_setAnd('d.id!', USER_ROOT);
// 	}
		
//  //** set where if true **/
 
// 	$this->EUI_Page->_setWhere($filter);
	
//  //** set order by ***/
 	
// 	if( $this->URI->_get_have_post('order_by')) 
// 		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	
	
//  //@ set limit query on content 
// 	$this->EUI_Page->_setLimit();
// 	// echo "<pre>". $this ->EUI_Page->_getCompiler()."</pre>";
// } 	

 public function get_content()
 {
	$this->EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this->EUI_Page->_setPage(20);
	
  /** cek if have the post userid **/
 
   $this->EUI_Page->_setArraySelect(array( 
				"a.UserId as chk_menu" => array("chk_menu","ID", "primary"),
				"a.id as UserIds" => array("UserIds","* User Id"), 
				"a.init_name as init_name" => array("init_name","Online Name"), 
				"a.full_name as full_name" => array("full_name","Full Name"), 
				"d.name as profile_id" => array("profile_id","Privileges"),
				"IF((SELECT concat(f.id,'- ',f.full_name) FROM t_tx_agent f WHERE f.UserId=a.mgr_id) is null,'-',(SELECT f.full_name  from t_tx_agent f where f.UserId=a.mgr_id)) as Manager" => array("Manager","Manager."),
				"IF((SELECT concat(f.id,'-',f.full_name) FROM t_tx_agent f WHERE f.UserId=a.spv_id) is null,'-',(SELECT f.full_name  from t_tx_agent f where f.UserId=a.spv_id)) as Spv" => array("Spv","Supervisor."),
				"IF((SELECT concat(f.id,'-',f.full_name) FROM t_tx_agent f WHERE f.UserId=a.tl_id) is null,'-',(SELECT f.full_name  from t_tx_agent f where f.UserId=a.tl_id)) as Leader" => array("Leader","Leader."),
				"IF(a.telphone=1,'Yes','No') as telphone" => array("telphone","Telephone "),
				"IF(logged_state=1,'Sign In','Sign Out') as logged_state" => array("logged_state","User Login"),
				"IF(a.user_state=1,'Active','UnActive') as user_state" => array("user_state","User Active"),
				"a.ip_address as ip_address" => array("ip_address","IP Location")
				// "ra.rank" => array("rank", "Ranking")
		));
		
		$this->EUI_Page->_setFrom("t_tx_agent a");
		$this->EUI_Page->_setJoin("cc_agent b "," a.id=b.userid","LEFT");
		$this->EUI_Page->_setJoin("t_gn_agent_profile d "," a.profile_id=d.id","LEFT"); 
		$this->EUI_Page->_setJoin("cc_agent_group e "," e.id=b.agent_group","LEFT");
		$this->EUI_Page->_setJoin("cc_agent_skill g "," b.id=g.agent","LEFT");
		//$this->EUI_Page->_setJoin("t_tx_agent_rank r "," r.dc_code = a.id","LEFT");
		//$this->EUI_Page->_setJoin("t_lk_rank ra "," r.dc_rank = ra.rank_id","LEFT");
		$this->EUI_Page->_setJoin("cc_skill h "," g.skill=h.id ","LEFT", TRUE);
				
		$this->EUI_Page->_setAnd("a.user_resign = 0");
	
 /** cek if have the post userid **/
 
	$filter = null;
	
	if(($this->URI->_get_have_post('UserId')))
	{ 
		$this->EUI_Page->_setAnd("( a.UserId LIKE '%".$this->URI->_get_post('UserId')."%'  
					OR a.full_name LIKE '%".$this->URI->_get_post('UserId')."%'  
					OR a.ip_address LIKE '%".$this->URI->_get_post('UserId')."%' 
					OR a.id LIKE '".$this->URI->_get_post('UserId')."')
			", FALSE);
	}
	
  /** cek login root atau bukan	**/
  
	if(($this->EUI_Session->_get_session('HandlingType')!=USER_ROOT))
	{
		$this->EUI_Page->_setAnd('d.id!', USER_ROOT);
	}
		
 //** set where if true **/
 
	$this->EUI_Page->_setWhere($filter);
	
 //** set order by ***/
 	
	if( $this->URI->_get_have_post('order_by')) 
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	
	
 //@ set limit query on content 
	$this->EUI_Page->_setLimit();

	// echo $this->EUI_Page->_getCompiler();
}  
/*
 *@ get buffering query from database
 *@ then return by object type ( resource(link) ); 
 */
 
function get_resource_query()
 {
	self::get_content();
	
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 *@ get number record & start of number every page 
 *@ then result ( INT ) type 
 */
 
function get_page_number()
  {
	if( $this -> EUI_Page -> _get_query()!='' )
	{
		return $this -> EUI_Page -> _getNo();
	}	
  }
  
  
/*@ reset user password data */

function _reset_user_password( $data = null )
{
	$tot = 0;
	if( !is_null( $data ) && is_array($data) )
	{
		foreach( $data as $k => $UserId ) 
		{
			$this -> db -> where( 'UserId', $UserId );
			if ( $this -> db -> update( 't_tx_agent',array('password' => md5('1234')) ) ) {
				$tot++;
			}
		}	
	}
	
	if( $tot > 0 )  return true;
	else
		return false;
}


/*@ _reset_user_location */

function _reset_user_location( $data = null )
{
	$tot = 0;
	if( !is_null( $data ) && is_array($data) )
	{
		foreach( $data as $k => $UserId ) 
		{
			$sql = " UPDATE t_tx_agent a SET a.ip_address = null, a.logged_state=0 
					 WHERE a.UserId='$UserId'";
					 
			if( $this -> db -> query($sql) ) {
				$tot++;
			}
		}	
	}
	
	if( $tot > 0 )  return true;
	else
		return false;
}


//@ _disable_user
function _set_user_resign( $data = null){
		$tot = 0;
		if(!is_null($data) && is_array($data)){
			foreach($data as $k => $UserId){
				$this -> db -> where( 'UserId', $UserId );
				$this -> db -> update( 't_tx_agent',array('user_resign' => 1,'user_state'=> 0 ));				
				$tot++;
			}
		}
		
		if( $tot > 0 ) {
			return true;
		}			
		else{
			return false;
		}
			
 }
 
function _disable_user( $data = null )
{
	$tot = 0;
	if( !is_null( $data ) && is_array($data) )
	{
		foreach( $data as $k => $UserId ) 
		{
			$this -> db -> where( 'UserId', $UserId );
			if ( $this -> db -> update( 't_tx_agent',array('user_state' => 0 )))
			{
				$tot++;
			}
		}	
	}
	
	if( $tot > 0 )  return true;
	else
		return false;
} 

//@ _enable_user
 
function _enable_user( $data = null )
{
	$tot = 0;
	if( !is_null( $data ) && is_array($data) )
	{
		foreach( $data as $k => $UserId ) 
		{
			$this -> db -> where( 'UserId', $UserId );
			if ( $this -> db -> update( 't_tx_agent',array('user_state' => 1 )))
			{
				$tot++;
			}
		}	
	}
	
	if( $tot > 0 )  return true;
	else
		return false;
} 

function _enable_userAll(){
	$tot = 0;
	$this->db->where('user_resign',0);
	if($this->db->update('t_tx_agent',array('user_state'=>1))){
		$tot++;
	}
	if($tot > 0){
		return true;
	}else{
		return false;
	}
}
//@ _get_user

function _get_user( $UserId )
{
	$sql = " select a.UserId, b.id as cc_agent_id from t_tx_agent a 
			 left join cc_agent b on a.id=b.userid
			 WHERE a.UserId ='$UserId' 
			 AND b.userid is not null  ";
			 
	$qry = $this -> db -> query($sql);
	if( !$qry -> EOF() )
	{
		return $qry -> result_first_assoc();
	}	
	else
		return false;
}


//@ _remove_user # --------------------------------------------

function _remove_user( $data = null )
{
	$tot = 0;
	if( !is_null( $data ) && is_array($data) )
	{
		
		foreach( $data as $k => $UserId ) 
		{
			$_avail_user = self::_get_user($UserId);
			if( $_avail_user !=FALSE )
			{
				//@ remove from coll_aagent
				if ( $this -> db -> delete('t_tx_agent', array('UserId' => $_avail_user['UserId'] ), $limit =1 ) ) 
				{
					//@ remove from cc_agent 
					if( $this -> db -> delete('cc_agent', array('id' => $_avail_user['cc_agent_id'] ), $limit =1 )) 
					{
						$tot++;
					}
				}
			}
			else
			{
				if ( $this -> db -> delete('t_tx_agent', array('UserId' => $UserId  ), $limit =1 ) ) 
				{
					 //@ remove from coll_aagent  only not by cc_agent cause not found user in 
					 //@ cc_agent table
					$tot++;
				}
			}	
		}	
	}
	
	if( $tot > 0 )  return true;
	else
		return false;
} 

// @ pack: add new user # --------------------------------------------

public function _set_add_user() {

 $_conds = 0;
 $USER_LOGIN_PRIVILEGE  = $this->URI->_get_post('profile');
 if( ($this ->EUI_Session->_have_get_session('UserId') !=FALSE) )
 {
	$this->db->set("id", $this->URI->_get_post('userid'));
	$this->db->set("code_user", $this->URI->_get_post('textAgentcode'));
	$this->db->set("full_name", $this->URI->_get_post('fullname'));
	$this->db->set("init_name", $this->URI->_get_post('textOnlineCode'));
	$this->db->set("profile_id", $this->URI->_get_post('profile'));
	$this->db->set("handling_type", $this->URI->_get_post('profile'));
	$this->db->set("spv_id", $this->URI->_get_post('user_spv'));
	$this->db->set("mgr_id", $this->URI->_get_post('user_mgr'));
	$this->db->set("admin_id", $this->URI->_get_post('user_admin'));
	$this->db->set("act_mgr", $this->URI->_get_post('account_manager'));
	$this->db->set("quality_id", $this->URI->_get_post('quality_head'));
	$this->db->set("tl_id", $this->URI->_get_post('user_leader'));
	$this->db->set("stl_id", $this->URI->_get_post('senior_leader'));
	$this->db->set("telphone", $this->URI->_get_post('user_telphone'));
	$this->db->set("updated_by", $this->EUI_Session->_get_session('UserId'));
	$this->db->set("update_password", $this->EUI_Tools->_date_time());
	$this->db->set("user_create_date", $this->EUI_Tools->_date_time());
	$this->db->set("password", $this->EUI_Tools->_set_md5('1234'));
	$this->db->set("agency_id",'EUI');
	$this->db->set("user_state",1);
	$this->db->set("logged_state",0);
 
 // @ pack : create User Its. 
	
	$this->db->insert("t_tx_agent");
	if( $this->db->affected_rows() > 0 )
	{
		$UserId = $this->db->insert_id();
		if( $UserId )
		{
			if( $USER_LOGIN_PRIVILEGE == USER_ADMIN ) $this->db->set("admin_id",$UserId);
			if( $USER_LOGIN_PRIVILEGE == USER_ACCOUNT_MANAGER ) $this->db->set("act_mgr",$UserId);
			if( $USER_LOGIN_PRIVILEGE == USER_MANAGER ) $this->db->set("mgr_id",$UserId);
			if( $USER_LOGIN_PRIVILEGE == USER_SUPERVISOR ) $this->db->set("spv_id",$UserId);
			if( $USER_LOGIN_PRIVILEGE == USER_LEADER ) $this->db->set("tl_id",$UserId);
			if( $USER_LOGIN_PRIVILEGE == USER_SENIOR_TL ) $this->db->set("stl_id",$UserId);
			
			// @ pack : compare string 
			
			if( in_array( $USER_LOGIN_PRIVILEGE, array(
				USER_ADMIN, 
				USER_ACCOUNT_MANAGER, 
				USER_MANAGER, 
				USER_SUPERVISOR, 
				USER_LEADER,
				USER_SENIOR_TL)))
			{
				 $this->db->where("UserId", $UserId);
				 $this->db->update("t_tx_agent");
			}
		}
		
	  // @ pack : insert to cc_agent table for aksess to CTI 	
		
		$this->db->set("userid", $this->URI->_get_post('userid'));
		$this->db->set("name", $this->URI->_get_post('fullname'));
		$this->db->set("agent_group", $this->URI->_get_post('cc_group'));
		$this->db->set("password", $this->EUI_Tools->_set_md5('1234'));
		$this->db->set("login_status", 1);
		$this->db->set("occupancy", 1);
		
		$this->db->insert("cc_agent");
		if( $this->db->affected_rows() > 0 )
		{
			$_conds++;
		}
	}
 }
	
 return $_conds;

}

//@ pack : _set_update_user # --------------------------------------------

 public function _set_update_user()
{
	
  $_conds = FALSE;
 
  $this->db->set('name',$this->URI->_get_post('fullname'));
  $this->db->set('agent_group',$this->URI->_get_post('cc_group'));
  $this->db->set('userid',$this->URI->_get_post('userid'));
 
 // @ pack : cc_agent where update on here 
 
  $this->db->where('userid', $this->URI->_get_post('CcUserId'));
  if( $this->db->update('cc_agent') )
  {
	 $this->db->set('full_name', $this->URI->_get_post('fullname'));
	 $this->db->set('id', $this->URI->_get_post('userid'));
	 $this->db->set('init_name', $this->URI->_get_post('textOnlineCode'));
	 $this->db->set('code_user', $this->URI->_get_post('textAgentcode'));
	 $this->db->set('telphone', $this->URI->_get_post('user_telphone'));
	 $this->db->set('profile_id', $this->URI->_get_post('privilges'));
	 $this->db->set('handling_type', $this->URI->_get_post('privilges'));
	 $this->db->set('admin_id', $this->URI->_get_post('user_admin'));
	 $this->db->set('tl_id', $this->URI->_get_post('user_leader'));
	 $this->db->set('stl_id', $this->URI->_get_post('senior_leader'));
	 $this->db->set('spv_id', $this->URI->_get_post('spvid'));
	 $this->db->set('mgr_id', $this->URI->_get_post('mgrid'));
	 $this->db->set('act_mgr',$this->URI->_get_post('account_manager'));
	 $this->db->set('quality_id',$this->URI->_get_post('quality_head'));
	 $this->db->set("last_update", $this->EUI_Tools->_date_time());
	 
 // @ pack : set update data t_tx_agent its. 
 
	 $this->db->where('UserId',$this->URI->_get_post('hiddenUserId'));
	 if( $this->db->update('t_tx_agent') ) {
		$_conds = TRUE;
	 }
	 
  }
  
  return $_conds;
  
}

// @ pack : _set_register_user_pbx

function _set_register_user_pbx( $_UserId=null )
{
	$_conds = FALSE;
	
	$this -> load -> Model('M_Pbx');
	if( class_exists('M_Pbx') )
	{
		if( (count($_UserId)!=0) && ($_UserId!='')) 
		{
			$_conds = $this -> M_Pbx -> _set_register_user( $_UserId );
		}
	}
	
	return $_conds;
}

//@ _get_detail_user

function _get_detail_user( $UserId )
{
	$_data = array();
	
	$this -> db -> select('a.UserId, a.id as Username, a.full_name, a.handling_type, a.tl_id, a.spv_id, a.mgr_id, a.admin_id,a.act_mgr, a.quality_id,
		a.code_user, a.init_name, b.agent_group, a.telphone');
	$this -> db -> from('t_tx_agent a left join cc_agent b on a.id=b.userid');
	$this -> db -> where('a.UserId', $UserId);
	$_data = $this -> db -> get() -> result_assoc();
	if( is_array($_data) ){
		return $_data[0];
	}	
	else
		return false;
		
}

function _get_name_user ( $UserId ){
	$this->db->reset_select();
	$this->db->select('full_name');
	$this->db->from('t_tx_agent');
	$this->db->where('UserId',$UserId);
	$qry = $this->db->get();
	if($qry->num_rows >0){
		return $qry->result_first_assoc();
	}else{
		return false;
	}
	
}
//@ function 

function _get_admin(){
	
$_avail = array();
	
	$this->db->select('a.UserId, a.full_name, a.id');
	$this->db->from("t_tx_agent a");
	$this->db->join("t_gn_agent_profile b", "a.handling_type=b.id","left");
	$this->db->where("b.id",USER_ADMIN);
	
	$this->db->order_by("a.full_name","ASC");
	foreach( $this -> db->get()->result_assoc() as $rows )
	{
		$_avail[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_avail;
	
}


//@ function 

function _getQualityControll(){
	
$_avail = array();
	
	$this->db->select('a.UserId, a.full_name, a.id');
	$this->db->from("t_tx_agent a");
	$this->db->join("t_gn_agent_profile b", "a.handling_type=b.id","left");
	$this->db->where("b.id",USER_QUALITY);
	$this->db->order_by("a.full_name","ASC");
	foreach( $this -> db->get()->result_assoc() as $rows )
	{
		$_avail[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_avail;
	
}


//@ _get_detail_user

function _getUserDetail( $UserId )
{
	$_data = array();
	
	$this -> db -> select('
			a.UserId, a.id as Username, 
			a.full_name, a.tl_id, a.handling_type, 
			a.spv_id, a.mgr_id, a.act_mgr, 
			a.quality_id, a.admin_id, a.code_user, 
			a.init_name, a.telphone', FALSE);
			
	$this->db->from('t_tx_agent a');
	$this->db->where('a.UserId', $UserId);
	$this->db->where('a.user_state', 1);
	$this->db->order_by("a.full_name","ASC");
	$_data = $this -> db -> get() -> result_assoc();
	if( is_array($_data) ){
		return $_data[0];
	}	
	else
		return false;
		
}


//@ _get_detail_user

function _getUserByCode( $UserCode = null, $state =null )
{
	$_data = array();
	$this -> db -> select('
			a.UserId, a.id as Username, 
			a.full_name, a.tl_id, a.handling_type, 
			a.spv_id, a.mgr_id, a.act_mgr, 
			a.quality_id, a.admin_id, a.code_user, 
			a.init_name, a.telphone', FALSE);
			
  $this->db-> from('t_tx_agent a');
	
// @ pack : customize query // default by code 
	
  if(!is_array($UserCode)){	
	$this->db->where('a.id', $UserCode);
  }
  
// @ pack : customize query 
	  
 if(is_array($UserCode)) 
	foreach( $UserCode as $key => $val )
 {
    if(!is_array($val) ){
		$this->db->where_in($key, array($val));
	} else{
		$this->db->where_in('a.id', $val);
	}
 }
 
// @ pack : default is null ----------------------------> 
	
 if(is_null($state) ){
	$this->db->where_in('a.user_state', array(0,1));
 }
	
// @ pack : default is null 	
 if( is_array( $state )){
	$this->db->where_in('a.user_state', array(0,1));
  }
	
// @ pack : default is null 	
  if( !is_array( $state ) AND !is_null($state) ){
	$this->db->where_in('a.user_state',array($state));
  }
	
// @ pack : default is null 	
	$this->db->order_by("a.full_name","ASC");
	$_data = $this->db->get();
	if( $_data->num_rows()> 0 ) {
		return $_data->result_first_assoc();
	}	
	else
		return false;
		
}


//@ select get profile id

function _get_handling_type() 
{
	$_data = array();
	$this -> db -> select('a.id, a.name');
	$this -> db -> from('t_gn_agent_profile  a');
	$this -> db -> where('a.IsActive',1);
	
	if( $this -> EUI_Session->_get_session('HandlingType')!=USER_ROOT){
		$this -> db -> where('a.id!=',USER_ROOT,FALSE);	
	}
	
	$this->db->order_by("a.name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) {
		$_data[$rows['id']] = $rows['name'];
	}


	
	return $_data;
}

//@ select get profile id

function _get_administrator() 
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('handling_type', USER_ADMIN );
	$this->db->order_by("full_name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}

//@ select get profile id

function _get_manager() 
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('handling_type', USER_MANAGER );
	
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}

//@ select get profile id

function _get_account_manager() 
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('handling_type', USER_ACCOUNT_MANAGER );
	
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}


//@ select get profile id

function _get_quality_head() 
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('handling_type', USER_QUALITY_HEAD );
	$this->db->order_by("full_name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}



//@ select get profile id

function _get_quality_staff($UserId = null ) 
{
	$_data = array();
	$this -> db -> select('a.UserId, a.id, a.full_name');
	$this -> db -> from('t_tx_agent a');
	$this -> db -> join('t_gn_user_project_work b ',' a.UserId=b.UserId','LEFT');
	$this -> db -> where('a.handling_type', USER_QUALITY_STAFF);
	$this -> db -> where('a.user_state',1);
	
	$this -> db -> where_in('b.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
	
	// if have leader 
	
	if( !is_null($UserId) ){
		$this -> db -> where('quality_id',$UserId);		
	}
	$this->db->order_by("a.full_name","ASC");
	// echo $this->db->_get_var_dump();
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		//$_data[$rows['UserId']] = $rows['id'] ." - ". $rows['full_name'];
		$_data[$rows['UserId']] = $rows['full_name'];
		
	}
	
	return $_data;
}

//@ select get profile id

function _get_user_by_login() 
{
	$_data = array();
	$this -> db -> select('UserId, id, full_name');
	$this -> db -> from('t_tx_agent');
	
	if( $this->EUI_Session->_get_session('HandlingType') == USER_ROOT ){
		$this->db->where_in('handling_type', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	}
	else if( $this->EUI_Session->_get_session('HandlingType') == USER_ADMIN ){
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));	
	}
	else if( $this->EUI_Session->_get_session('HandlingType') == USER_MANGER ){
		$this->db->where_in('handling_type', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->where('mgr_id',$this -> EUI_Session->_get_session('UserId'));	
	}
	else if( $this -> EUI_Session->_get_session('HandlingType') == USER_SUPERVISOR ){
		$this->db->where_in('handling_type', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	}
	else if($this->EUI_Session->_get_session('HandlingType') == USER_AGENT_OUTBOUND ){
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->where('UserId',$this -> EUI_Session->_get_session('UserId'));
	}
	else if($this->EUI_Session->_get_session('HandlingType') == USER_AGENT_INBOUND  ){
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->where('UserId',$this -> EUI_Session->_get_session('UserId'));
	}
	else if($this->EUI_Session->_get_session('HandlingType') == USER_LEADER  ){
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
		$this->db->where('tl_id',$this -> EUI_Session->_get_session('UserId'));
	}
	else if($this->EUI_Session->_get_session('HandlingType') == USER_SENIOR_TL  ){
		$this->db->where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND,USER_LEADER));
		$this->db->where('stl_id',$this -> EUI_Session->_get_session('UserId'));
	}
	else{
		$this -> db -> where_in('handling_type',array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	}
	
	
	$this->db->order_by("full_name","ASC");
	
	// list agent 
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows;
	}
	
	return $_data;
}


//@ select get profile id

function _get_teleamarketer($param = null ) 
{
	$_data = array();
	$this -> db -> select('a.UserId, a.id, a.full_name, a.init_name');
	$this -> db -> from('t_tx_agent a');
	$this -> db -> join('t_gn_user_project_work b ',' a.UserId=b.UserId','LEFT');
	$this -> db -> where_in('a.handling_type', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	$this -> db -> where_in('b.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
  // cheked list 
	if(!is_null($param) 
		AND ($param) 
		)
	{
		foreach($param as $cols => $value )
		{
			if(is_array($value))
			{
				$this -> db ->where_in($cols,$value);
			}
			else
			{
				$this -> db ->where($cols,$value);
			}	
		}
	}

	$this->db->order_by("a.full_name","ASC");
	foreach( $this->db->get()->result_assoc() as $rows ) 
	{
		//$_data[$rows['UserId']] = $rows['id']." - ".$rows['full_name'];
		$_data[$rows['UserId']] = $rows['full_name'];
		
	}
	

	return $_data;
}

function _get_init_name($param = null ) 
{
	$_data = array();
	$this -> db -> reset_select();
	$this -> db -> select('a.UserId, a.id, a.init_name');
	$this -> db -> from('t_tx_agent a');
	$this -> db -> join('t_gn_user_project_work b ',' a.UserId=b.UserId','LEFT');
	$this -> db -> where_in('a.handling_type', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	$this -> db -> where_in('b.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
  // cheked list 
	if(!is_null($param) 
		AND ($param) 
		)
	{
		$this -> db ->where($param); 	
	}

	$this->db->order_by("a.init_name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		//$_data[$rows['UserId']] = $rows['id']." - ".$rows['full_name'];
		$_data[$rows['UserId']] = $rows['init_name'];
		
	}
	

	return $_data;
}

//@ select get profile id

 function _get_supervisor() 
{
	$_data = array();
	$this->db->select('a.UserId, a.full_name');
	$this->db->from('t_tx_agent a');
	$this->db->join('t_gn_user_project_work b ',' a.UserId=b.UserId','LEFT');
	$this->db->where('a.handling_type', USER_SUPERVISOR );
	$this->db->order_by("full_name","ASC");
	
	$qry = $this->db->get();
	if($qry->num_rows() > 0 ) 
		foreach( $qry->result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}

//@ select get profile id

function _get_seniorleader( $param = null ) 
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('handling_type', USER_SENIOR_TL );
	if(!is_null($param) 
		AND ($param) 
	)
	{
		$this -> db ->where($param); 	
	}
	$this->db->order_by("full_name","ASC");
	// echo $this->db->_get_var_dump();
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}

function _get_teamleader( $param = null ) 
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('handling_type', USER_LEADER );
	if(!is_null($param) 
		AND ($param) 
	)
	{
		$this -> db ->where($param); 	
	}
	$this->db->order_by("full_name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
}


//@ cc_agent_group

function _getJoinUser()
{
	$_conds = array();
	
	$this->db->select('b.id, b.userid, b.name');
	$this->db->from('t_tx_agent a');
	$this->db->join('cc_agent b','a.id=b.userid','LEFT');
	$this->db->join('t_gn_agent_profile c','a.profile_id=c.id','LEFT');
	$this->db->where('c.IsActive',1);
	$this->db->where('b.userid is not null');
	$this->db->order_by("b.name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows )
	{
		$_conds[$rows['id']] = $rows['name'];
	}
	
	return $_conds;

}

//@ cc_agent_group

function _get_agent_group() 
{
	$_data = array();
	$this -> db -> select('id, description');
	$this -> db -> from('cc_agent_group');

	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['id']] = $rows['description'];
	}
	
	return $_data;
}

//@ cc_agent_group

function _get_telephone() 
{
	$_data = array('1'=>'YES','0'=>'NO');
	return $_data;
}


// _getUserCapacity

function _getUserCapacity( $Status=1 )
{
	$_Capacity = 0;
	
	$this -> db -> select("COUNT('a.UserId') as Jumlah",FALSE);
	$this -> db -> from("t_tx_agent a");
	$this -> db -> where("a.user_state", $Status);
	$this -> db -> where_in("a.profile_id", array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
	
	$rows = $this -> db -> get()->result_first_assoc();
	if( is_array($rows) )
	{
		$_Capacity = (INT)$rows['Jumlah'];
	}
	
	return $_Capacity;
	
}

// _getUserCapacity

function _getUserRegistration( $Status=1 )
{
	$_data = array();
	$this -> db -> select('UserId, full_name');
	$this -> db -> from('t_tx_agent');
	$this -> db -> where('user_state',1);
	$this -> db -> where('handling_type!=', USER_ROOT, FALSE);
	$this->db->order_by("full_name","ASC");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$_data[$rows['UserId']] = $rows['full_name'];
	}
	
	return $_data;
	
}

// get group of the user under privilges 

function _getUserLevelGroup( $Level = null )
{
	$_conds = array();
  
	$this->db->select('b.UserId, b.full_name, a.id as UserLevelId, b.id as UserCode');
	$this->db->from('t_gn_agent_profile a');
	$this->db->join('t_tx_agent b','a.id=b.handling_type','LEFT');
	
	
	if(!is_null($Level) )
	{
		if(is_array($Level) )
		{
			$this->db->where_in('a.id', $Level);
		} else {
			$this->db->where_in('a.id', array($Level));
		}
	}
	
	// level USER_ROOT
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_ROOT ) {
		$this->db->where_not_in('b.handling_type',array(USER_ROOT));
	}
	
	// level USER_ADMIN
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_ADMIN ) {
		$this->db->where_not_in('b.handling_type',array(USER_ROOT));
	}
	
	// level USER_MANAGER
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_MANAGER ) {
		$this->db->where_in('b.mgr_id',$this -> EUI_Session->_get_session('UserId'));
		$this->db->where_not_in('b.handling_type',array(
			USER_ROOT,
			USER_ADMIN,
			USER_MANAGER
		));
	}
	
	// level USER_ACCOUNT_MANAGER 
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_ACCOUNT_MANAGER ) {
		$this->db->where_in('b.act_mgr',$this -> EUI_Session->_get_session('UserId'));
		$this->db->where_not_in('b.handling_type',array(
			USER_ROOT,
			USER_ADMIN,
			USER_MANAGER,
			USER_ACCOUNT_MANAGER
		));
	}
	
	// level USER_SUPERVISOR
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_SUPERVISOR ) {
		$this->db->where_in('b.spv_id',$this -> EUI_Session->_get_session('UserId'));
		$this->db->where_not_in('b.handling_type',array(
			USER_ROOT,
			USER_ADMIN,
			USER_MANAGER,
			USER_ACCOUNT_MANAGER,
			USER_SUPERVISOR
		));
	}
	
	// level team USER_LEADER 
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_LEADER ) {
		$this->db->where_in('b.tl_id',$this -> EUI_Session->_get_session('UserId'));
		$this->db->where_not_in('b.handling_type',array(
			USER_ROOT,
			USER_ADMIN,
			USER_MANAGER,
			USER_ACCOUNT_MANAGER,
			USER_SUPERVISOR,
			USER_LEADER
		));
	}
	
	// level team USER_SENIOR_TL 
	
	if( $this -> EUI_Session->_get_session('HandlingType')== USER_SENIOR_TL ) {
		$this->db->where_in('b.tl_id',$this -> EUI_Session->_get_session('UserId'));
		$this->db->where_not_in('b.handling_type',array(
			USER_ROOT,
			USER_ADMIN,
			USER_MANAGER,
			USER_ACCOUNT_MANAGER,
			USER_SUPERVISOR,
			USER_LEADER,
			USER_SENIOR_TL
		));
	}
	
	// LOOK OF RESULT DATA 
	$this->db->order_by("b.full_name","ASC");
	foreach( $this -> db->get() -> result_assoc() as $rows ) {
		$_conds[$rows['UserId']] = strtoupper($rows['UserCode']);
	}
	
	return $_conds;
}	

/* NAMBAH FUNGSI */

function _getUserInDetail( $UserId )
{
	$_data = array();
	
	if(trim($UserId) != '')
	{
		$sql = "SELECT `a`.`UserId`, `a`.`id` as Username, `a`.`full_name`, `a`.`tl_id`,
			`a`.`handling_type`, `a`.`spv_id`, `a`.`mgr_id`, `a`.`act_mgr`, 
			`a`.`quality_id`, `a`.`admin_id`, `a`.`code_user`, 
			`a`.`init_name`, `a`.`telphone`
				FROM (`t_tx_agent` a)
				WHERE `a`.`user_state` =  1
				AND `a`.`UserId` in (".$UserId.")
				ORDER BY `a`.`full_name` ASC";
		
		$qry = $this -> db -> query($sql);
		
		if($qry->num_rows() > 0)
		{
			foreach($qry->result_assoc() as $rows)
			{
				$_data[] = $rows;
			}
		}
	
	}
	
	return $_data;
}

/*
 * function get User customize inQuery()
 */
 
public function getUserQuery( $where = null )
{
	$_conds = array();
	
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_tx_agent');
	if(is_array($where) )
	{
		foreach( $where as $field => $values ){
			if( is_array($values) ){
				$this->db->where_in($field, $values);	
			} else {
				$this->db->where($field, $values);
			}
		}
	}
	
	
// @ pack : of result its.	

	if( !is_null($where) ) { 
		
		$qry = $this->db->get();
		if( $qry -> num_rows() > 0 )
		{
			$_conds = $qry->result_assoc();
		}
	}
	
	return $_conds;
} 


}
?>