<?php
/*
 * @def 	: FaxContent
 * ------------------------------------
 * @param 	: array()
 */
 
class M_MailOutbox extends EUI_Model
{

// constructor 

function M_MailOutbox() {
	// run
}
	
// index 

function _get_default()
{
	
 // set pages 
 
 $this->EUI_Page->_setPage(10); 
 $this->EUI_Page->_setSelect("a.EmailOutboxId");
 $this->EUI_Page->_setFrom("email_outbox a");
 $this->EUI_Page->_setJoin("email_status_refference b ","a.EmailStatus=b.EmailStatusCode",'LEFT');
 $this->EUI_Page->_setJoin("t_tx_agent c","a.EmailCreateById=c.UserId","LEFT",TRUE);
 
 $this->EUI_Page->_setWhereIn('a.EmailAssignDataId',$this->EUI_Session->_get_session('ProjectId'));// project wcall 

/** filter ***/
	if($this->URI->_get_have_post('agent_id')){
		$this->EUI_Page->_setAnd("c.id", $this -> URI->_get_post('agent_id'));
	}	

	if( $this -> URI->_get_have_post('agent_group') ){
		$this->EUI_Page->_setAnd("c.agent_group", $this -> URI->_get_post('agent_group'));
	}

	if($this ->URI->_get_have_post('start_time')){
		$this->EUI_Page->_setAnd("a.OutboxStatusTime >" , _getDateEnglish($this -> URI->_get_post('start_time'))." 00:00:00"); 
		$this->EUI_Page->_setAnd("a.OutboxStatusTime <" , _getDateEnglish($this -> URI->_get_post('end_time'))." 23:59:59");
	}

	
	if( $this -> URI->_get_have_post('faxnumber') ){
		$this->EUI_Page->_setAnd("a.OutboxFaxNumber",  $this -> URI->_get_post('faxnumber'));
	}
	
	if($this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}	

// voice content display 

function _get_content()
{

 // set pages 

 $this->EUI_Page->_postPage((INT)$this -> URI -> _get_post('v_page'));
 $this->EUI_Page->_setPage(10); 
 
 // $this->EUI_Page->_setSelect("*");
 // $this->EUI_Page->_setFrom("email_outbox a "); 
 // $this->EUI_Page->_setJoin("email_status_refference b ", " a.EmailStatus=b.EmailStatusCode","LEFT");
 // $this->EUI_Page->_setJoin("t_tx_agent c ","a.EmailCreateById=c.UserId","LEFT", FALSE);
 
 
 $this->EUI_Page->_setArraySelect(array(
	'a.EmailOutboxId as EmailOutboxId' => array('EmailOutboxId', 'ID', 'Primary'),
	'a.EmailSubject as Subject' => array('Subject','Subject'),
	'date_format(a.EmailCreateTs,"%d/%m/%Y %H:%i:%s") as CreateDate' => array('CreateDate', 'Create Date'),
	'date_format(a.EmaiUpdateTs,"%d/%m/%Y %H:%i:%s") as QueueDate' => array('QueueDate','Queue Date'),
	'c.full_name as EmailCreateById' => array('EmailCreateById','Create By'),
	"IF( d.EmailAttachmentPath IS NULL,'', 'Download') as Attachment" => array('Attachment', 'Attachment'),
	'b.EmailStatusName as EmailStatusName' => array('EmailStatusName','Status')
	
 ));
 
 $this->EUI_Page->_setFrom("email_outbox a "); 
 $this->EUI_Page->_setJoin("email_status_refference b ", " a.EmailStatus=b.EmailStatusCode","LEFT");
 $this->EUI_Page->_setJoin("t_tx_agent c ","a.EmailCreateById=c.UserId","LEFT");
 $this->EUI_Page->_setJoin("email_attachment_url d ", " a.EmailOutboxId=d.EmailReffrenceId","LEFT",TRUE);
 
 
			
// filter
 $this->EUI_Page->_setWhereIn('a.EmailAssignDataId',$this->EUI_Session->_get_session('ProjectId'));

 
  if( $this -> URI->_get_have_post('agent_id') ){
	$this->EUI_Page->_setAnd("c.id", $this -> URI->_get_post('agent_id'));
  }

  if( $this -> URI->_get_have_post('agent_group') ){
	$this->EUI_Page->_setAnd("c.agent_group", $this -> URI->_get_post('agent_group'));
  }
	
  if( $this -> URI->_get_have_post('start_time'))
  {
	$this->EUI_Page->_setAnd("a.OutboxStatusTime >", _getDateEnglish($this->URI->_get_post('start_time'))." 00:00:00"); 
	$this->EUI_Page->_setAnd("a.OutboxStatusTime <", _getDateEnglish($this->URI->_get_post('end_time'))." 23:59:59");
  }
 
  if( $this -> URI->_get_have_post('faxnumber') ){
	$this->EUI_Page->_setAnd("a.OutboxFaxNumber", $this -> URI->_get_post('faxnumber'));
  }
 
   // look order by 
  
  if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
  }	
  else{
	$this->EUI_Page->_setOrderBy("a.EmailCreateTs","DESC");
  }
	
  
  
  $this -> EUI_Page ->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
 
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 
 // vget all status 
 
 function _getFaxStatus()
 {
	$FAX_STATUS = array();
	
	$this ->db -> select("*");
	$this ->db -> from("cc_app_reference a");
	$this ->db -> where("a.ref_group", 'FAX_STATUS');
	foreach( $this ->db ->get() -> result_assoc() as $rows ) {
		$FAX_STATUS[$rows['ref_value']] = $rows['ref_desc'];
	} 
	
	return $FAX_STATUS;
}	
 
 
 // _getOutbox
 function _getOutbox($DataId=0 )
 {
	$rs = array();
	$sql = " SELECT MAX(a.OutboxId), a.*, b.ref_desc FROM fax_outbox  a 
			 LEFT JOIN cc_app_reference b ON ( a.OutboxStatus = b.ref_value AND b.ref_group='FAX_STATUS') WHERE a.OutboxDataId='$DataId'";
			 
	$qry = $this -> db ->query($sql);
	if( $rows = $qry -> result_first_assoc() ) {
		$rs = ( is_array($rows) ? $rows : null ); 
	}
	
	return $rs;
 }
 

 // _setDeleteFax
 
 function _setDeleteFax( $param = null )
 {
	$_conds = 0;
	
	if( !is_null($param) ) 
	{
		foreach($param as $key => $DataId )
		{
			$this -> db -> select('*');
			$this -> db -> from('fax_data');
			$this -> db -> where('DataId', $DataId);
			
			foreach($this -> db ->get() -> result_assoc() as $rows ) 
			{
				if( $rows ) 
				{
					$_path_file = $rows['DataPath'];
					if( file_exists( $_path_file ) ){
						@unlink($_path_file);
					}
					
					// delete on table a 
					$this -> db -> where('DataId', $DataId);
					if( $this -> db -> delete('fax_data') ) 
					{
						$this -> db -> where('QueueDataId', $DataId);
						if( $this -> db -> delete('fax_queue') ){
							$_conds++;
						}
					}
				}
			}
		}
	}
	
	return $_conds;
 }
 

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function CC( $OutboxId = 0 )
{
	$cc = '';
	
	$this ->db->select('*');
	$this ->db->from('email_copy_carbone');
	$this ->db->where('EmailReffrenceId', $OutboxId);
	$this ->db->where('EmailDirection', 2);
	
	$i = 0;
	foreach(  $this ->db->get()->result_assoc() as $rows) {
		$cc[$i]= $rows['EmailCC'];
		$i++;
	}
	
	if( is_array($cc) )
		return implode(";", $cc);
	else
		return NULL;
}


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function BCC( $OutboxId = 0 )
{
	$bcc = '';
	$this ->db->select('*');
	$this ->db->from('email_blindcopy_carbone');
	$this ->db->where('EmailReffrenceId', $OutboxId);
	$this ->db->where('EmailDirection', 2);
	$i= 0;
	foreach(  $this ->db->get()->result_assoc() as $rows)
	{
		$bcc[$i]= $rows['EmailBCC'];
		$i++;
	}
	if( is_array($bcc) )
		return implode(";", $bcc);
	else
		return NULL;
	
}

/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function TO( $OutboxId = 0 )
{
 $to = '';
 
	$this ->db->select('*');
	$this ->db->from('email_destination');
	$this ->db->where('EmailReffrenceId', $OutboxId);
	$this ->db->where('EmailDirection', 2);
	
	$i=0;
	foreach(  $this ->db->get()->result_assoc() as $rows)
	{
		$to[]= $rows['EmailDestination'];
		$i++;
	}
	if( is_array($to) )
		return implode(";", $to);
	else
		return NULL;
		
}

 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getPrintPriview( $OutboxId = null ) 
{

 $content = array();
 if(!is_null($OutboxId) ) 
 {
	$this->db->select('*');
	$this->db->from("email_outbox a ");
	$this->db->where("a.EmailOutboxId", $OutboxId);
	
	foreach(  $this ->db->get()->result_assoc() as $rows)
	{
		if( $OutboxId = (INT)$rows['EmailOutboxId'] )
		{
			$content['email_id']  = $rows['EmailOutboxId'];
			$content['from'] 	  = $rows['EmailSender'];
			$content['sent_date'] = $rows['EmailCreateTs'];
			$content['subject']   = $rows['EmailSubject'];
			$content['body'] 	  = $rows['EmailContent'];
			$content['to'] 		  = $this->TO($OutboxId);
			$content['cc'] 		  = $this->CC($OutboxId);
			$content['bcc'] 	  = $this->BCC($OutboxId);
		}	
	 }
	 
  }

return $content;
		
} 
/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _setResendMail( $EmailOutboxId =0 )
 {
	$_conds = 0;
	
	foreach( $EmailOutboxId as $key => $OutboxId )
	{
		$this->db->set('EmailStatus', 1001);
		$this->db->where('EmailOutboxId', $OutboxId);
		$this->db->update('email_outbox');
		
		if( $this->db->affected_rows() > 0 )
		{
			$this ->db->set('QueueMailId', $OutboxId);
			$this ->db->set('QueueCreateTs', date('Y-m-d H:i:s'));
			$this ->db->set('QueueStatus',1001);
			$this ->db->set('QueueReason','SMTP ready sent to email'); 
			$this ->db->set('QueueTrying',0);
			$this ->db->insert('email_queue');
			if( $this ->db->affected_rows() > 0 )
			{
				$this->db->set('HistoryRefferenceId',$OutboxId); 
				$this->db->set('HistoryStatus', 1001);
				$this->db->set('HistoryDirection',2); 
				$this->db->set('HistoryReason', 'SMTP ready sent to email');
				$this->db->set('HistoryCreateTs',date('Y-m-d H:i:s'));
				$this->db->insert('email_history');
				if( $this->db->affected_rows()>0 ) {
					$_conds++;
				}
			}
		}
	}	
	
	return $_conds;
	
}	


}