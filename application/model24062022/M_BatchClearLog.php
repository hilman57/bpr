<?php
/*
 * @ pack : modul clear log of the batch log 
 * 			extends class model parent::EUI_Model
 *          this modul for delete on database table *
 * --------------------------------------------------------
 * --------------------------------------------------------
 */

class M_BatchClearLog 
	extends EUI_Model 
{
	
 private static $Instance = null;

/*
 * @ pack : M_BatchClearLog
 */
 
 public static function &Instance() 
{
 if(is_null(self::$Instance) ) {
  self::$Instance = new self();	
 }
 
 return self::$Instance;
 
}
 
/*
 * @ pack : M_BatchClearLog
 */ 
 
 public function M_BatchClearLog()
{ 
	/* return null **/ 
}

/*
 * @ pack : _setBatch_Clear_Chat
 *  ---------------------------------------------------------
 * @ note :  delete data more 3 month ago 
 */
 
 public function _setBatch_Clear_Chat()
{
   $cond = 0;
   
   $this->db->where("DATE(sent)>= DATE_ADD(CURDATE(), INTERVAL -3 MONTH) ", "",FALSE);
   $this->db->where("DATE(sent)<= DATE_ADD(CURDATE(), INTERVAL -1 MONTH) ", "",FALSE);
   if( $this->db->delete("t_tx_agent_chat") ) {
	  $cond++;
   }
   
   return $cond;
}  // _setBatch_Clear_Chat ====================================> 

/*
 * @ pack : _setBatch_Clear_Broadcast
 *  ---------------------------------------------------------
 * @ note :  delete data more 3 month ago 
 */
 
 public function _setBatch_Clear_Broadcast()
{
   $cond = 0;
   $this->db->where("DATE(sent)>= DATE_ADD(CURDATE(), INTERVAL -3 MONTH) ", "",FALSE);
   $this->db->where("DATE(sent)<= DATE_ADD(CURDATE(), INTERVAL -1 MONTH) ", "",FALSE);
   if( $this->db->delete("t_tx_agent_msgbox") ) 
   {
	  $cond++;
   }
   
   return $cond;
} // _setBatch_Clear_Broadcast ================================> 

	
}


?>