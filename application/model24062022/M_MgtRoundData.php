<?php

/*
 * @ pack : manage round data its
 */
 
class M_MgtRoundData extends EUI_Model 
{

	/* static variable */
	private static $Instance = null;
/*
 * @ pack : constractor of class
 */	

	public function M_MgtRoundData()
	{
		$this->load->model(array('M_SysUser','M_SetCallResult','M_MgtBucket'));
	}
	
	
	/*
	 * @ pack : manage round data its
	 */	
	 
	 public static function &Instance()
	{
		if( is_null(self::$Instance) ) {
			self::$Instance = new self();
		}
		
		return self::$Instance;
	}
	/*
	 * @ pack : manage round data its
	 */
	 
	public function _get_dropdown()
	{	
		$dropdown = array(
		'DROPDOWN_ROUND_STATUS' => $this->_get_select_status()
		);
		
		return $dropdown;
	}
	
	/*
	 * @ pack : get Call status
	 */
	 
	 private function _get_select_status()
	{
	 $conds = array();
	 
	 $recs = $this->M_SetCallResult->_getCallReasonId();
	 foreach( $recs as $code => $values ) {
		$conds[$code] = $values['name']; 
	 }
		
	 return $conds;
	 
	}
	
	public function _SetRoundStatus()
	{
		$Msg = array('success'=>0,'Agent'=>0,'debitur'=>0);
		
		if( $this->URI->_get_have_post('CallAccountStatus') AND
			$this->URI->_get_have_post('Duration') AND
			$this->URI->_get_have_post('RandomBy')
		  )
		  {
			$duration = _get_post('Duration');
			$status_to_round = explode(',',_get_post('CallAccountStatus'));
			$RandomBy = _get_post('RandomBy');
			
			$deb = $this->get_debitur_status($status_to_round);
			$ActDeskoll =$this->M_SysUser->_get_teleamarketer(array('user_state'=>1,
																'logged_state'=>1));
			
			// echo $this->db->last_query();
			if( count($deb) <= 0 )
			{
				return $Msg;
				exit;
			}
			
			if( count($ActDeskoll) <= 0 )
			{
				$Msg = array('success'=>0,'Agent'=>0,'debitur'=>count($deb));
				return $Msg;
				exit;
			}
			
			if ( count($deb) < count($ActDeskoll) )
			{
				$Msg = array('success'=>0,'Agent'=>count($ActDeskoll),'debitur'=>count($deb));
				return $Msg;
				exit;
			}
			
			$this->db->set('modul_id_rnd',$RandomBy);
			$this->db->set('mod_set_createdby',_get_session('UserId'));
			$this->db->set('mod_set_running',1);
			$this->db->set('mod_set_createdts','now()',false);
			if( $this->db->insert('t_gn_modul_setup') )
			{
				$modul_setup_id = $this->db->insert_id();
				
				$this->db->set('modul_setup_id',$modul_setup_id);
				$this->db->set('duration',$duration);
				$this->db->set('next_round_ts','SEC_TO_TIME(('.$duration.'*60)+TIME_TO_SEC(now()))',false);
				if( $this->db->insert('t_st_round') )
				{
					$st_round_id = $this->db->insert_id();
					$i=0;
					foreach($deb as $rows => $ArrayValue)
					{
						$this->db->set('deb_id',$ArrayValue['deb_id']);
						$this->db->set('acc_no',$ArrayValue['deb_acct_no']);
						
						$this->db->set('modul_setup_id',$modul_setup_id);
						if( $this->db->insert('t_gn_buckettrx_debitur') )
						{
							$TRX_ID[$i] = $this->db->insert_id();
							$i++;
						}
					}
				}
				
				if ( count($TRX_ID) > 0 )
				{
					$random = $this->_RandomAssign( $ActDeskoll,$TRX_ID );
					foreach($random as $Agentid => $ArrayValue)
					{
						foreach($ArrayValue as $rows => $trx_id){
							$this->db->set('bucket_trx_id',$trx_id);
							$this->db->set('agent_id',$Agentid);
							if( $this->db->insert('t_gn_round_assign') )
							{
								$Msg['debitur']++;
							}
							
						}
						$Msg['Agent']++;
					}
					$Msg['success']=1;
				}
			}
		  }
		  
		  return $Msg;
		
	}
	
	private function get_debitur_status($status)
	{
		$var_argv = array();
		$this->db->reset_select();
		$this->db->select('a.deb_id,a.deb_acct_no');
		$this->db->from('t_gn_debitur a');
		$this->db->join('t_gn_assignment b ',' a.deb_id = b.CustomerId','INNER');
		$this->db->where_in('a.deb_call_status_code', $status );
		$this->db->where('b.AssignSelerId IS NOT NULL');
		$this->db->order_by("a.deb_id", "RANDOM");
		
		$qry = $this->db->get();
		if( $qry->num_rows() > 0 ) {
			$i=0;
			foreach($qry->result_assoc() as $rows)
			{
				$var_argv[$i] = $rows;
				$i++;
			}
		}
		// echo $this->db->last_query();
		return $var_argv;
	}
	
	/*** Acak Kepemilikan data agent yang login (Pertama Kali)
	**** _RandomAssign()
	**** param : AgentId, DebiturId
	**** return : array()
	***/
	
	public function _RandomAssign($AgentId,$Debitur)
	{
	
		//inisialisasi awal
		$SumAssign=0;
		$i=0;
		$j=0;
		$k=0;
		(int)$SigmaDebitur = count($Debitur);
		(int)$SigmaDeskoll = count($AgentId);
		
		//hitung sisa data
		(int)$ModDeb = $SigmaDebitur % $SigmaDeskoll;
		
		//hitung perolehan data yang akan didapat deskoll
		$SumAssign = (INT)($SigmaDebitur/$SigmaDeskoll);
		
		$split_deb = array_chunk($Debitur,$SumAssign);
		
		//membagi rata debitur ke deskol
		foreach($AgentId as $deskollid => $deskollname){
			// $RandomAssign[$deskollid] = $split_deb[$i];
			
			
			foreach($split_deb[$i] as $index => $deb_id_split){
				$RandomAssign[$deskollid][$j]=$deb_id_split;
				$j++;
			}
			$i++;
		}
		
		if($ModDeb>0)
		{
			/**acak deskolll untuk membagi sisa debitur
			** pengacak berdasarkan sisa hasil bagi data
			**/
			$rand_deskoll=array_rand($AgentId,$ModDeb);
			
			/**
			** Membagi sisa data debitur ke deskoll yang terpilih			
			**/
			if(is_array($rand_deskoll))
			{
				foreach($rand_deskoll as $ind=>$desk_id){
					$RandomAssign[$desk_id][$j+$k]=$split_deb[$i][$k];
					$k++;
				}
			}
			else
			{
				foreach(array($rand_deskoll) as $ind=>$desk_id){
					$RandomAssign[$desk_id][$j+$k]=$split_deb[$i][$k];
					$k++;
				}
			}
		}
	
		return $RandomAssign;
	}
	
	public function _get_round_by()
	{
		return array('round_by_check'=>'Round By Check','round_by_upload'=>'Round By Upload' );
	}
}
//END OF Class M_MgtRoundData
?>