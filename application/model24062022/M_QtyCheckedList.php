<?php

define('APPROVAL_ALL', 11);
define('APPROVAL', 1);
define('UnAPPROVAL_ALL', 9);

/*
 * E.U.I 
 * -----------------------------------------------
 *
 * subject	: M_QtyApprovalInterest
 * 			  extends under Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/controller/sysuser/
 */
 
class M_QtyCheckedList extends EUI_Model
{


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function M_QtyCheckedList()
 {
	$this -> load -> model(array('M_SetCallResult','M_SetResultQuality','M_ModOutBoundGoal'));
 }
 
 
 
/*
 * @ def 		: _getEventSale
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

function _getAgentReady()
{
	$AgentId = array();
	$this -> db->select("a.Agent_User_Id ");
	$this -> db->from("t_gn_quality_agent a ");
	$this -> db->where("a.Quality_Staff_Id",$this -> EUI_Session->_get_session('UserId'));
	foreach( $this -> db->get() -> result_assoc()  as $rows )
	{
		$AgentId[$rows['Agent_User_Id']] = $rows['Agent_User_Id'];
	}
	
	return $AgentId;
	
} 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Sale()
{
	$_a = array(); $_b = array();
	if( class_exists('M_SetCallResult'))
	{
		$_a = $this -> M_SetCallResult -> _getInterestSale(); 
		foreach( $_a as $_k => $_v )
		{
			$_b[$_k] = $_k;  
		}	
	}
	
	return $_b;
} 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function _getAgentByQualityStaff()
{
	$_list_agents = false;
	
	if( $this -> EUI_Session->_have_get_session('UserId') )
	{
		$this -> db -> select("b.UserId");
		$this -> db -> from("t_gn_quality_agent a");
		$this -> db -> join("t_tx_agent b","a.Agent_User_Id=b.UserId","LEFT");
		$this -> db -> where("a.Quality_Staff_Id", $this -> EUI_Session->_get_session('UserId') );
		foreach( $this -> db ->get() -> result_assoc() as $rows )
		{
			$_list_agents[$rows['UserId']] = $rows['UserId'];
		}	
	}
	
	return $_list_agents;
} 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 private function _getApprovalInterest()
 {
	$_conds = array();
	if(class_exists('M_SetCallResult'))
	{
		$i = 0;
		foreach( $this -> M_SetCallResult -> _getEventSale() as $k => $rows )
		{
			$_conds[$i] = $k;
			$i++;
		}
	}
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
 private function _getQualityConfirm()
 {
	$_conds = array();
	if(class_exists('M_SetResultQuality'))
	{
		$i = 0;
		foreach( $this -> M_SetResultQuality -> _getQualityConfirm() as $k => $rows )
		{
			$_conds[$i] = $k;
			$i++;
		}
	}
	return $_conds;
 }
 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_default()
{
	/* get instance class outbound **/	
	
	$CallDirection =& M_ModOutBoundGoal::get_instance();
	$this -> EUI_Page->_setPage(10);
	
	$this->EUI_Page->_setSelect(" c.*,  d.full_name, d.id, IF( e.CallReasonDesc IS NULL, 'New', e.CallReasonDesc) as CallReasonDesc, i.CampaignName as CampaignName ",FALSE);
	$this->EUI_Page->_setFrom('t_gn_assignment a ');
	$this->EUI_Page->_setJoin('t_gn_quality_agent b ',' a.AssignSelerId=b.Agent_User_Id','LEFT');
	$this->EUI_Page->_setJoin('t_gn_debitur c ',' a.CustomerId=c.CustomerId','LEFT');
	$this->EUI_Page->_setJoin('t_tx_agent d ',' a.AssignSelerId=d.UserId','LEFT');
	$this->EUI_Page->_setJoin('t_lk_account_status e ',' c.CallReasonId=e.CallReasonId','LEFT');
	$this->EUI_Page->_setJoin('t_gn_quality_group f ',' b.Quality_Staff_Id=f.Quality_Staff_id','LEFT'); 
	$this->EUI_Page->_setJoin('t_tx_agent h ',' b.Quality_Staff_Id=h.UserId','LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign i ',' c.CampaignId=i.CampaignId', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign_project j','c.CampaignId=j.CampaignId', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_policy_detail k ','c.CustomerNumber=k.EnigmaId', 'LEFT', TRUE);
	
/* default for method scoring quality **/

	$this->EUI_Page->_setAnd('i.OutboundGoalsId', $CallDirection->_getOutboundId());
	$this->EUI_Page->_setAnd('c.CallComplete', 1);
	$this->EUI_Page->_setWhereIn("c.CallReasonQue", array(APPROVAL, APPROVAL_ALL));
	
/* get status in sale only  **/

	if($this->EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD  
	 OR $this->EUI_Session->_get_session('HandlingType')==USER_QUALITY_STAFF ) 
	{
		$this->EUI_Page->_setAnd("f.Quality_Skill_Id", QUALITY_SCORES );
	}
	
/* level user quality Head **/

	if($this->EUI_Session->_get_session('HandlingType')== USER_QUALITY_HEAD )
	{
		$this->EUI_Page->_setAnd("h.quality_id", $this -> EUI_Session->_get_session('UserId'));
	}

	/* 	level login quality staff **/
	
	if( $this->EUI_Session->_get_session('HandlingType')== USER_QUALITY_STAFF )
	{
		$this->EUI_Page->_setAnd("b.Quality_Staff_Id", $this -> EUI_Session->_get_session('UserId'));
	}
	
	
	/* filter next data if not empty filter **/
	
	$this->EUI_Page->_setAnd("a.AssignAdmin IS NOT NULL");
	$this->EUI_Page->_setAnd("a.AssignMgr IS NOT NULL ");
	$this->EUI_Page->_setAnd("a.AssignSpv IS NOT NULL");
	$this->EUI_Page->_setAnd("a.AssignBlock", "0");
	
  /** cache filter **/
	$this->EUI_Page->_setLikeCache('c.CustomerFirstName', 'chk_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('c.CampaignId', 'chk_campaign_id', TRUE);
	$this->EUI_Page->_setAndCache('c.CIFNumber', 'chk_cust_number', TRUE);
	$this->EUI_Page->_setAndCache('k.CallReasonId', 'chk_call_result', TRUE);
	$this->EUI_Page->_setAndCache('a.AssignSelerId', 'chk_user_id', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyProductName', 'chk_category_id', TRUE);
	$this->EUI_Page->_setAndCache('c.CallReasonQue', 'chk_QaulityStatusId', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyNumber', 'chk_PolicyNumber', TRUE);
	$this->EUI_Page->_setAndOrCache("date(c.CustomerUpdatedTs)>='". date('Y-m-d',strtotime($this -> URI->_get_post('chk_start_date')))."'", "chk_start_date", TRUE);
	$this->EUI_Page->_setAndOrCache("date(c.CustomerUpdatedTs)<='". date('Y-m-d',strtotime($this -> URI->_get_post('chk_end_date')))."'", "chk_end_date", TRUE);
	
	
// run filter
	
	$this->EUI_Page->_setGroupBy('c.CustomerId');   
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}	 
	
	
 }
 
 /*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
public function _get_content()
{
  /** instance of class **/
  
	$CallDirection =& M_ModOutBoundGoal::get_instance();
	$this->EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this->EUI_Page->_setPage(10);
	
	$this->EUI_Page->_setArraySelect(array(
		'c.CustomerId as chk_cust_call' => array('chk_cust_call','chk_cust_call','primary'),
		'c.CustomerFirstName as CustomerFirstName' => array('CustomerFirstName','Owner Name'),
		'c.CIFNumber as CIFNumber' => array('CIFNumber','Customer Number'),
		'k.PolicyNumber as PolicyNumber' => array('PolicyNumber','Policy Number'),
		'k.PolicyProductName as PolicyProductName' => array('PolicyProductName','Product Name'),
		'date_format(k.PolicyIssDate,"%d/%m/%Y") as PolicyIssDate' => array('PolicyIssDate','Issued Date'),
		'date_format(k.PolicyUploadDate,"%d/%m/%Y") as PolicyUploadDate' => array('PolicyUploadDate','Upload Date'),
		'd.full_name as full_name' => array('full_name','Caller Name'),
		'e.CallReasonDesc as CallReasonDesc' => array('CallReasonDesc','Last Call Status'),
		'date_format(c.CustomerUpdatedTs, "%d/%m/%Y %H:%i:%s") as CustomerUpdatedTs' => array('CustomerUpdatedTs','Last Call Date'),
		'(select count(plt.PolicyId) from t_gn_policy_detail plt where plt.EnigmaId=c.CustomerNumber ) as SumPolicy' => array('SumPolicy','Sum Of Policy'),
		'c.CallAttempt as CallAttempt' => array('CallAttempt','Atempt'),
		'l.AproveName as CallReasonQue' => array('CallReasonQue','QA Status')
	));
	
	$this->EUI_Page->_setFrom('t_gn_assignment a ');
	$this->EUI_Page->_setJoin('t_gn_quality_agent b ',' a.AssignSelerId=b.Agent_User_Id','LEFT');
	$this->EUI_Page->_setJoin('t_gn_debitur c ',' a.CustomerId=c.CustomerId','LEFT');
	$this->EUI_Page->_setJoin('t_tx_agent d ',' a.AssignSelerId=d.UserId','LEFT');
	$this->EUI_Page->_setJoin('t_lk_account_status e ',' c.CallReasonId=e.CallReasonId','LEFT');
	$this->EUI_Page->_setJoin('t_gn_quality_group f ',' b.Quality_Staff_Id=f.Quality_Staff_id','LEFT'); 
	$this->EUI_Page->_setJoin('t_tx_agent h ',' b.Quality_Staff_Id=h.UserId','LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign i ',' c.CampaignId=i.CampaignId', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign_project j','c.CampaignId=j.CampaignId', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_policy_detail k ','c.CustomerNumber=k.EnigmaId', 'LEFT');
	$this->EUI_Page->_setJoin('t_lk_aprove_status l ','c.CallReasonQue=l.ApproveId', 'LEFT', TRUE);
	
	
/* default for method scoring quality **/

	$this->EUI_Page->_setAnd('i.OutboundGoalsId', $CallDirection->_getOutboundId());
	$this->EUI_Page->_setAnd('c.CallComplete', 1);
	
/* get status in sale only  **/
	$this->EUI_Page->_setWhereIn("c.CallReasonQue", array(APPROVAL, APPROVAL_ALL));
	
// tipe handling 

	if($this->EUI_Session->_get_session('HandlingType')==USER_QUALITY_HEAD  
	 OR $this->EUI_Session->_get_session('HandlingType')==USER_QUALITY_STAFF ) 
	{
		$this->EUI_Page->_setAnd("f.Quality_Skill_Id", QUALITY_SCORES );
	}
	
	/* level user quality Head **/
	
	if($this->EUI_Session->_get_session('HandlingType')== USER_QUALITY_HEAD){
		$this->EUI_Page->_setAnd("h.quality_id", $this -> EUI_Session->_get_session('UserId'));
	}

	/* 	level login quality staff **/
	
	if( $this->EUI_Session->_get_session('HandlingType')== USER_QUALITY_STAFF )
	{
		$this->EUI_Page->_setAnd("b.Quality_Staff_Id", $this -> EUI_Session->_get_session('UserId'));
	}
	
	
	/* filter next data if not empty filter **/
	
	$this->EUI_Page->_setAnd("a.AssignAdmin IS NOT NULL");
	$this->EUI_Page->_setAnd("a.AssignMgr IS NOT NULL ");
	$this->EUI_Page->_setAnd("a.AssignSpv IS NOT NULL");
	$this->EUI_Page->_setAnd("a.AssignBlock", "0");
	
  // filtring by login 
  
	if( $this -> EUI_Session -> _get_session('HandlingType')==SUPERVISOR ){			 
		$this->EUI_Page->_setAnd("a.AssignSpv", $this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if($this -> EUI_Session -> _get_session('HandlingType')==TELESALES) {
		$this->EUI_Page->_setAnd("a.AssignSelerId", $this -> EUI_Session -> _get_session('UserId'));
	}	
	
 /** filtring by keep session **/
 
	$this->EUI_Page->_setLikeCache('c.CustomerFirstName', 'chk_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('c.CampaignId', 'chk_campaign_id', TRUE);
	$this->EUI_Page->_setAndCache('c.CIFNumber', 'chk_cust_number', TRUE);
	$this->EUI_Page->_setAndCache('k.CallReasonId', 'chk_call_result', TRUE);
	$this->EUI_Page->_setAndCache('a.AssignSelerId', 'chk_user_id', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyProductName', 'chk_category_id', TRUE);
	$this->EUI_Page->_setAndCache('c.CallReasonQue', 'chk_QaulityStatusId', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyNumber', 'chk_PolicyNumber', TRUE);
	$this->EUI_Page->_setAndOrCache("date(c.CustomerUpdatedTs) >='". date('Y-m-d',strtotime($this -> URI->_get_post('chk_start_date')))."'", "chk_start_date", TRUE);
	$this->EUI_Page->_setAndOrCache("date(c.CustomerUpdatedTs)<='". date('Y-m-d',strtotime($this -> URI->_get_post('chk_end_date')))."'", "chk_end_date", TRUE);
	
/** filtring by keep session **/
	
	$this->EUI_Page->_setGroupBy('c.CustomerId');
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
 }
 
 
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getCountVoice($CustomerId=0)
 {
	$_count = 0;
	$this -> db -> select("count(a.id) as jumlah",FALSE);
	$this -> db -> from("cc_recording a");
	
	if( $rows = $this -> db -> get() -> result_first_assoc() ){
		$_count = (INT)$rows['jumlah']; 
	}
	
	return $_count;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getPages($CustomerId=0)
 {
	$PagesList = array();
	
	$record = $this -> _getCountVoice();
	$counts = ceil($record/5);
	
	for($p = 1; $p <= (INT)$counts; $p++) {
		$PagesList[$p] = $p;
	}
	
	return $PagesList;
	
 }
 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getLastCallHistory($CustomerId)
 {
	$_conds = array();
	
	$this -> db -> select('*');
	$this -> db -> from('t_gn_callhistory');
	$this -> db -> where('CustomerId',$CustomerId);
	
	if( $avail = $this -> db -> get()->result_first_assoc() )
	{
		$_conds = $avail;
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
  function _getListVoice($param = array() )
 {
	$_voice = array();
	
 // $start
	$start = 0; $perpages = 5; 
	
 // total page 
	
	$record = $this -> _getCountVoice();
	$pages = ceil($record/$perpages);
	
	//  get start pages 
	
	if( isset($param['pages']) ){
		if( (INT)$param['pages'] > 0 )
			$start = ( (($param['pages'])-1) * $perpages); 
		else
			$start = 0;
	}
	
	// run data 
	
	$this -> db -> select("*");
	$this -> db -> from("cc_recording a");
	$this -> db -> limit($perpages,$start);
	
	// get query result
	
	$qry = $this->db->get();
	$num = $start+1;
	foreach($qry -> result_assoc() as $rows )
	{
		$_voice[$num] = $rows;	
		$num++;
	}	
	return $_voice;
 }
 
 
 
 /* get rows data **/
 
 function _getVoiceResult($VoiceId=0 )
 {
	$this -> db -> select("*");
	$this -> db -> from("cc_recording a");
	$this -> db -> where('id',$VoiceId);
	
	$_result =  array();
	
	if( $_conds = $this -> db->get() -> result_first_assoc() )
	{
		foreach($_conds as $fld => $values )
		{
			if( $fld=='file_voc_size' ) 
				$_result[$fld] = $this->EUI_Tools->_get_format_size($values);
				
			else if( $fld=='duration' ) 
				$_result[$fld] = $this->EUI_Tools->_set_duration($values);
				
			else if( $fld=='anumber' ) 
				$_result[$fld] = $this->EUI_Tools->_getPhoneNumber($values);	
				
			else if( $fld=='start_time' ) 
				$_result[$fld] = $this->EUI_Tools->_datetime_indonesia($values);	
				
			else 
				$_result[$fld] = $values;
		}
		
		return $_result;
	}
	else
		return null;
 }
 

 
public function _getQtyCount( $CustomerId = 0 )
{
	$count = 0;
	
	$this->db->select("COUNT(a.Id) as jumlah",FALSE);
	$this->db->from("t_gn_scoring_point a ");
	$this->db->where("a.CustomerId",$CustomerId); 
	
	if( $rows = $this->db->get()->result_first_assoc() ) 
	{
		$count = (INT)$rows['jumlah'];
	}
	
	return $count;
} 



 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 

 public function _saveQualityValues( $param = null  )
 {
   $InsertId = 0;
   if( is_array($param) ) 
   {
		$this->db->set('CustomerId',$param['CustomerId']);
		$this->db->set('ScoringRemark',strtoupper($param['remarks']));
		$this->db->set('ScroingQualityId',$this -> EUI_Session->_get_session('UserId'));
		$this->db->set('ScoringCreateTs',date('Y-m-d H:i:s'));
		$this->db->insert('t_gn_qa_scoring');
		if( $this->db->affected_rows() > 0 )
			$InsertId = $this -> db->insert_id();
	}	
		
	return $InsertId;
 }
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 public function _setSaveScoreQuality($param = NULL )
 {

 $conds = 0;
 if(!is_null($param)  AND is_array($param))
 {
	$this->db->where('CustomerId', $param['CustomerId']);
	$this->db->set('CallReasonQue', $param['CallReasonQue']);
	$this->db->set('QueueId', $this->EUI_Session->_get_session('UserId'));
	$this->db->set('CustomerUpdatedTs', date('Y-m-d H:i:s'));
	$this->db->set('CustomerRejectedDate',date('Y-m-d H:i:s'));	
	$this->db->update('t_gn_debitur'); // --> update customer OK
	
	if( $this->db->affected_rows() > 0  
		OR $this->db->affected_rows() < 0 )
	{
		$this->db->select('*'); // --> update last insert rows call history
		$this->db->from('t_gn_callhistory');
		$this->db->where('CustomerId',$param['CustomerId']);
		$this->db->order_by("CallHistoryId","DESC");
		$this->db->limit(1);
		
		if($rows = $this->db->get()->result_first_assoc()) 
		{
			$this->db->set("CallSessionId",$rows['CallSessionId']);
			$this->db->set("CustomerId",$rows['CustomerId']);
			$this->db->set("CallReasonId",$rows['CallReasonId']);
			$this->db->set("PolicyId",$rows['PolicyId']);
			$this->db->set("ApprovalStatusId",$param['CallReasonQue'],FALSE);
			$this->db->set("CallHistoryCallDate",$rows['CallHistoryCallDate']);
			$this->db->set("CallNumber",$rows['CallNumber']);
			$this->db->set("CallHistoryNotes",strtoupper($param['CallHistoryNotes']));
			$this->db->set("CreatedById",$this->EUI_Session->_get_session('UserId'));
			$this->db->set("UpdatedById",$this->EUI_Session->_get_session('UserId'));
			$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s'));
			$this->db->set("CallHistoryUpdatedTs",date('Y-m-d H:i:s'));
			$this->db->insert('t_gn_callhistory'); // --> insert to call history by QA 
			
			if($this->db->affected_rows()> 0 )
			{
				$this->db->set('QualityStatusId', $param['CallReasonQue']);
				$this->db->set('QualityUserId', $this->EUI_Session->_get_session('UserId'));
				$this->db->set('QualityUpdateTs', date('Y-m-d H:i:s'));
				$this->db->where('PolicyId', $param['PolicyId']);
				$this->db->update('t_gn_policy_detail');
				$conds++;
			}	
		 }
	 }
}
   return $conds;
 }
 
 
  
 /*
 * @ def 		: get policy data by customer ID AND by Upload ID **
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 

 
 public function _getPolicyDataById( $PolicyId = null )
 {
	$datas = array();
	$this->db->select('
			a.CustomerId, a.Currency, a.Nama_Dc, a.CampaignId,
			a.Branch_Name,a.CustomerCountry, 
			a.Bank_Issuer, c.PayMode as PaymentModeAs, b.*', 
	FALSE);
	
	$this->db->from('t_gn_debitur a');
	$this->db->join('t_gn_policy_detail b ',' a.CustomerNumber=b.EnigmaId', 'LEFT');
	$this->db->join('t_lk_paymode c', 'c.PayModeId=b.PaymentMode','LEFT');
	$this->db->where('b.PolicyId', $PolicyId);
	$rows = $this->db->get();
	
	if($rows->num_rows() > 0 ) {
		$datas = $rows->result_first_assoc();
	}
	
	return $datas;
 }
 
 function _getProjectIdByCustomerId($cust_id)
 {
	$id = 0;
	
	$sql = "select b.ProjectId from t_gn_debitur a
			left join t_gn_campaign_project b on a.CampaignId = b.CampaignId
			where 1=1 and a.CustomerId = '".$cust_id."'";
	$qry = $this->db->query($sql);
	
	if($qry->num_rows() > 0)
	{
		$id = $qry->result_singgle_value();
	}
	
	return $id;
 }
 
 public function _getScoringCategory($cust_id)
 {
	$datas = array();
	
	$projectId = $this->_getProjectIdByCustomerId($cust_id);
	
	$sql = "select a.CategoryNo, a.QuestionCategory from t_lk_scoring_category a 
			where a.ScoringProjectId = '".$projectId."'";

	$qry = $this->db->query($sql);
	
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$datas[$rows['CategoryNo']] = $rows;
		}
		// print_r($datas);
	}
	
	return $datas;
 }
 
 
 public function _getScoringQuestion()
 {
	$datas = array();
	
	// $QuestionId = $this->_getProjectIdByCustomerId($cust_id);
	
	$sql = "SELECT
				 a.Id, a.ScoringCategoryNo, a.ScoringQuestion
			FROM t_lk_scoring_question a
			LEFT JOIN t_lk_scoring_category b ON a.ScoringCategoryNo = b.CategoryNo
			WHERE 1=1 AND b.ScoringProjectId = 2";
	
	$qry = $this->db->query($sql);
	$i = 0;
		foreach( $qry->result_assoc() as $rows)
		{
			$datas[$rows['ScoringCategoryNo']][$rows['Id']]=$rows['ScoringQuestion'];
			$i++;
		}
		
	return $datas;
 }
 
 public function _getScoringValue()
 {
	
 }
 
 
 /** _setApprovalAll***/
 
 public function _setUnApprovalAll( $Id  = null )
 {
	$conds = 0;
	if( !is_null($Id) )
	{
		
		foreach( $Id as $keys => $CustomerId)
		{
			$this -> db->select('*');
			$this -> db->from('t_gn_debitur ');
			$this -> db->where('CustomerId', $CustomerId);
			
			if( $rows = $this -> db->get()->result_first_assoc() )
			{
				$this->db->set('CallReasonQue', UnAPPROVAL_ALL); 
				$this->db->set('QueueId', $this->EUI_Session->_get_session('UserId'));
				$this->db->set('CustomerUpdatedTs', date('Y-m-d H:i:s'));
				$this->db->set('CustomerRejectedDate', date('Y-m-d H:i:s'));
				$this->db->where('CustomerId', $CustomerId );
				$this->db->update('t_gn_debitur');
				
				
				// then next --> 
				
				if( $CustomerNumber = $rows['CustomerNumber'] )
				{
					$this->db->set('QualityStatusId', UnAPPROVAL_ALL );
					$this->db->set('QualityUserId', $this->EUI_Session->_get_session('UserId'));
					$this->db->set('QualityUpdateTs', date('Y-m-d H:i:s'));
					$this->db->where('EnigmaId', $CustomerNumber );
					$this->db->update('t_gn_policy_detail');
					
					if($CustomerId)
					{
						$this ->db->select('*');
						$this ->db->from('t_gn_callhistory');
						$this ->db->where('CustomerId', $CustomerId);
						$this ->db->order_by('CallHistoryId', 'DESC');
						$this ->db->limit(1);
						if( $rs = $this->db->get() -> result_first_assoc() )
						{
							$this->db->set('CallSessionId',$rs['CallSessionId']); 
							$this->db->set('CustomerId',$rs['CustomerId']);
							$this->db->set('PolicyId',$rs['PolicyId']);
							$this->db->set('CallReasonId',$rs['CallReasonId']);
							$this->db->set('ApprovalStatusId',UnAPPROVAL_ALL);
							$this->db->set('CreatedById',$this->EUI_Session->_get_session('UserId'));
							$this->db->set('UpdatedById',$this->EUI_Session->_get_session('UserId'));
							$this->db->set('CallHistoryCallDate',$rs['CallHistoryCallDate']);
							$this->db->set('CallNumber',$rs['CallNumber']);
							$this->db->set('CallHistoryNotes','UNAPPROVAL ALL / UNCHECKED ALL');
							$this->db->set('CallHistoryCreatedTs',date('Y-m-d H:i:s'));
							$this->db->set('CallHistoryUpdatedTs',date('Y-m-d H:i:s'));
							$this->db->set('ScriptId', $rs['ScriptId']);
							$this->db->insert('t_gn_callhistory');
							
							if( $this->db->affected_rows() > 0 )
							{
								$conds++;
							}
						}
					}
				}	
			}
		}	
	}	
	
	return $conds;
 }
 
}
?>