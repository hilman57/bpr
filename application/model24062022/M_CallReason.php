<?php
/*
 * EUI Model  
 *
 
 *@Section  : M_CallReason
 *@author 	: Omens  
 *@link		: http://www.razakitechnology.com/eui/controller 
 */
 
class M_CallReason extends EUI_Model
{
	
/**
 *@ constructor 
 **/
 
function M_CallReason()
{ 
	// run & first load class
}

/**
 * @ get store of the reaon 
 **/
 
function _get_CallReason()
 {
	$_conds = array();
	$sql = " SELECT a.reasonid, a.reason_desc from cc_reasons a ";
	$qry = $this -> db -> query($sql);
	foreach( $qry -> result_assoc() as $rows ) {
		$_conds[$rows['reasonid']] = $rows['reason_desc'];
	}
	
	return $_conds;
 }
 
}

// END OF FILE 
// location : /application/model/M_CallReason.php
?>