<?php
/*
 * E.U.I 
 * --------------------------------------------------------------
 * 
 * subject	: get model data for M_SetPrefix modul 
 * 			  extends under model class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
class M_SetPrefix extends EUI_Model 
{

/*
 * EUI :: _get_default() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_default() 
 {
  
/*
 | @ set _set default show page on list record/pages 
 */
	
	$this -> EUI_Page -> _setPage(20); 

/*
 | @ set _set default show page on list record/pages 
 */
 
	$this -> EUI_Page -> _setQuery
	(
		" SELECT b.ProductCode 
		  FROM t_gn_productprefixnumber a
		  LEFT JOIN t_gn_product b on a.ProductId=b.ProductId
		  LEFT JOIN t_gn_formlayout c ON a.PrefixNumberId=c.PrefixId"  
	); 
	
	$flt = null;  
	if( $this -> URI ->_get_have_post('keywords'))
	{
		$flt .= " AND ( 
					a.PrefixNumberId LIKE '%{$this->URI->_get_post('keywords')}%'
					OR b.ProductCode LIKE '%{$this->URI->_get_post('keywords')}%'
					OR b.ProductName LIKE '%{$this->URI->_get_post('keywords')}%'
					OR a.PrefixChar  LIKE '%{$this->URI->_get_post('keywords')}%' 
					OR a.PrefixLength LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.AddView LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.EditView LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.Handler LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.Model LIKE '%{$this->URI->_get_post('keywords')}%'
					OR a.PrefixMethod LIKE '%{$this->URI->_get_post('keywords')}%'
			 
			)"; 
	} 
	
	$this -> EUI_Page -> _setWhere($flt);   
	return $this -> EUI_Page;
 }
 
 
/*
 * EUI :: _get_content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_content()
 {
 
/*
 | @ set _setdefauult show page on list record/pages 
 */
 
	$this -> EUI_Page -> _postPage( $this -> URI -> _get_post('v_page') );
	$this -> EUI_Page -> _setPage(20);
	
/*
 | @ set _setQuery
 */
  
	$this -> EUI_Page -> _setQuery
	(
		" SELECT 
			a.PrefixNumberId,  b.ProductCode,  b.ProductName, a.PrefixChar, 
			a.PrefixLength, c.AddView, c.EditView,  c.`Handler`, c.Model,
			 a.PrefixMethod,
			IF( a.PrefixFlagStatus<>1, 'Not Active','Active') as Status 
		  FROM t_gn_productprefixnumber a
		  LEFT JOIN t_gn_product b ON a.ProductId=b.ProductId 
		  LEFT JOIN t_gn_formlayout c ON a.PrefixNumberId=c.PrefixId" 
	 );
	 
	$flt = null;  
	if( $this -> URI ->_get_have_post('keywords'))
	{
		$flt .= " AND ( 
					a.PrefixNumberId LIKE '%{$this->URI->_get_post('keywords')}%'
					OR b.ProductCode LIKE '%{$this->URI->_get_post('keywords')}%'
					OR b.ProductName LIKE '%{$this->URI->_get_post('keywords')}%'
					OR a.PrefixChar  LIKE '%{$this->URI->_get_post('keywords')}%' 
					OR a.PrefixLength LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.AddView LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.EditView LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.Handler LIKE '%{$this->URI->_get_post('keywords')}%'
					OR c.Model LIKE '%{$this->URI->_get_post('keywords')}%'
					OR a.PrefixMethod LIKE '%{$this->URI->_get_post('keywords')}%'
			 
			)"; 
	} 
	
/*
 | @ set _setWhere
 */
 
	$this -> EUI_Page ->  _setWhere($flt);
	
/*
 | @ set order by ----
 */
	
	if( $this -> URI -> _get_have_post('order_by') ) 
		$this -> EUI_Page -> _setOrderBy( $this -> URI -> _get_post('order_by'),$this -> URI -> _get_post('type'));
	
/*
 | @ set _setLimit 
 */
	
	$this -> EUI_Page ->  _setLimit();
} 


/*
 * EUI :: _get_resource_query() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _get_resource_query()
 {
	$res = false;
	
	self::_get_content();
	
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		$res = $this -> EUI_Page -> _result();
		if($res) return $res;
		else
		{
			exit("Error :". mysql_error());
		}
	}	
 }
 
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _get_page_number()
  {
	if( $this -> EUI_Page -> _get_query()!='' )
	{
		return $this -> EUI_Page -> _getNo();
	}	
  }
  
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_avail_product()
 {
	$datas = array();
	$sql = "select a.ProductId, a.ProductName FROM t_gn_product a";
	$qry = $this -> db -> query($sql);
	foreach( $qry -> result_assoc() as $rows )
	{
		$datas[$rows['ProductId']] = $rows['ProductName'];
	}
	return $datas;
 }
 
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_method_prefix()
 {
	return array
	(
		'one-to-one' => 'One to One ',
		'one-to-many' => 'One to Many',
		'take-customize' => 'Customize'
	);
 } 
 
 
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _getPrefixId($ProductId=0 )
{
	$PrefixId = null;
	
	$this -> db->select('a.PrefixNumberId');
	$this -> db->from('t_gn_productprefixnumber a');
	$this -> db->where('a.ProductId',$ProductId);
	
	if( $avail = $this -> db->get()->result_first_assoc() ){
		$PrefixId = $avail['PrefixNumberId'];
	}
	
	return $PrefixId;
} 
 
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_status_prefix()
 {
	return array(
		'0' => 'Not Active',
		'1' => 'Active'
	);
 }
   
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _get_avail_form( $prefix = null )
 {
	$form = array();
	$form_avail_list = array();
	$_list_strips = array();
	
	if( isset($form_avail_list) )
	{
		if( !is_null($prefix) )
		{
			$form_avail_list = $this -> EUI_Tools -> _ls_get_dir(array("form/$prefix"), true);
			foreach( $form_avail_list as $k => $v ) 
			{
				$_list_strips = explode('.', $v);
				if(is_array($_list_strips) ) 
				{
					$form[$_list_strips[0]] = $_list_strips[0];
				}	
			}
		}
	}

	return $form;
 }
 
/*
 * EUI :: _get_char_prefix() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 
public function _seDelete() 
{
  $_conds = 0;
	if($this -> URI->_get_array_post('PrefixId'))foreach( 
		$this -> URI->_get_array_post('PrefixId') as  $key => $PrefixId ) 
	{
		$this ->db ->select('*');
		$this ->db ->from('t_gn_productprefixnumber a ');
		$this ->db ->where('a.PrefixNumberId', $PrefixId);
		if( $rows = $this ->db -> get() -> result_first_assoc() )
		{
			$this -> db->where('PrefixId', $rows['PrefixNumberId']);
			if( $this -> db->delete('t_gn_formlayout') )
			{
				$this -> db->where('PrefixNumberId', $rows['PrefixNumberId']);
				if( $this -> db->delete('t_gn_productprefixnumber') )
				{
					$_conds++;
				}
			}
		}
	}

	return $_conds;	
} 
 /*
 * EUI :: _get_char_prefix() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 private function _save_product_form($_post=null, $_insertid=0 )
 {
	$_conds = false;
	if( (!is_null($_post)) && ($_insertid!=0)) 
	{
		if( $this -> db -> insert("t_gn_formlayout", 
		array(
			'PrefixId' => $_insertid, 
			'EditView' => $_post['form_edit'], 
			'AddView' => $_post['form_input'], 
			'UrlView' => base_url()
			)
		)){
			$_conds = true;
		}
	}
	
	return $_conds;
}
   
 
 /*
 * EUI :: _get_char_prefix() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 private function _get_char_prefix($_code=null, $_length=0)
 {
	$_ret = null;
	if( (!is_null($_code)) && ($_length!=0))
	{
		$P = null;
		for( $n=1; $n<=$_length; $n++ ){
			$P.='0'; 
		}
		
		$L = null;
		if(!is_null($P) ){
			$L = $_code.substr($P,strlen($_code), strlen($P)); 	
		}
		
		if( !is_null($L)) 
			$_ret = $L;
	}
	
	return $_ret;
}
   
/*
 * EUI :: _set_save_prefix_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _set_save_prefix_number( $_prefix_post=null )
 {
	$tot = 0;
	$_get_chars = null;
	if(!is_null($_prefix_post) )
	{
		$_get_chars = self::_get_char_prefix( $_prefix_post['result_code'],  $_prefix_post['result_length'] );
		if( !is_null($_get_chars) )
		{
			if( $this -> db -> insert('t_gn_productprefixnumber', 
				array
				(
					'PrefixChar' => $_get_chars,
					'PrefixMethod' => $_prefix_post['result_method'],
					'ProductId' => $_prefix_post['result_head_level'],
					'PrefixLength' => $_prefix_post['result_length'], 
					'PrefixFlagStatus' => $_prefix_post['status_active'], 
				)
			)){
				$InsertId = $this -> db -> insert_id();
				if( $InsertId )
				{
					if( self::_save_product_form($_prefix_post, $InsertId) );
						$tot++;	
				}
			}
		}
	}
	
	return $tot;
 } 
 
// function PrefixId

function _getPrefix()
{
	$_conds = array();
 
	$this->db->select("*");
	$this->db->from("t_gn_productprefixnumber");
	$this->db->where("PrefixNumberId", $this -> URI->_get_post('PrefixId'));
	
	$i = 0;
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		foreach($rows as $k => $v ){
			$_conds[$k] = $v;
		}
		$i++;
	}
	
	return $_conds;
}


// function update 
function _setUpdate( $_prefix_post = null )
{
	$_conds = false;
	$_get_chars = self::_get_char_prefix( $_prefix_post['result_code'],  $_prefix_post['result_name'] );
	if( $this -> db ->update("t_gn_productprefixnumber",
	array
	(
		'PrefixChar' => $_get_chars,
		'ProductId' => $_prefix_post['result_head_level'],
		'PrefixLength' => $_prefix_post['result_name'], 
		'PrefixFlagStatus' => $_prefix_post['status_active'], 
					
	), array("PrefixNumberId"=>$_prefix_post['PrefixNumberId'])))
	{
		$_conds = true;
	}
	
	return $_conds;
}

// _setActive

function _setActive($_params = null )
{
	$_conds = 0;
	if( !is_null($_params) 
		AND is_array($_params) )
	{
		foreach($_params['PrefixId'] as $PrefixNumberId )
		{
			if( $this -> db -> update("t_gn_productprefixnumber",
				array("PrefixFlagStatus"=> $_params['Active']), 
				array("PrefixNumberId"=> $PrefixNumberId)
			))
			{
				$_conds+=1;
			}
		}
	}
	
	return $_conds;
}


}
?>