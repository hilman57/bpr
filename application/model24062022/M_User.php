<?php
/*
 * EUI Model  
 *
 
 * Section  : < M_User > get information user on table 
 * author 	: razaki team  
 * link		: http://www.razakitechnology.com/eui/controller 
 */
 
class M_User extends EUI_Model
{

/* 
 * constructor only 
 */

function M_User() 
{
	$this->load->model(array('M_Loger',
	  'M_SetWorkProject','M_SysUser',
	  'M_Configuration','M_SysUserProject'
	));
}


/*
 * get login detail every user on coll agent 
 * return < array >
 */

function _getATM()
{
	$datas = array();
	
	$this->db->select('UserId, full_name');
	$this->db->from('t_tx_agent');
	$this->db->where('handling_type',3);
	
	foreach ($this->db->get()->result_assoc() as $rows) {
		$datas[$rows['UserId']]=$rows['full_name'];
	}

	return $datas;
}
 
 
function _set_update_activity( $event = 'LOGIN', $UserId )
{
	$_conds = false;
	
	if( (!is_null( $UserId)) AND ($UserId!=FALSE) )
	{
		$SQL_insert['UserId'] = $UserId;
		$SQL_insert['ActivityAction']  =  $event;
        $SQL_insert['ActivityDateTs'] = $this -> EUI_Tools -> _date_time();
		$SQL_insert['ActivitySessionId'] = $this -> EUI_Session -> _get_session_id();
		$SQL_insert['ActivityLocation'] =  $this -> EUI_Tools -> _get_real_ip();
		
		if( $this -> db -> insert('t_tx_agent_activity', $SQL_insert) ){
			$_conds = true;
		}
		else
			$_conds = false;
	}
	
	return $_conds;
}

/*
 * get login detail every user on coll agent 
 * return < array >
 */

 function _get_last_login()
 {
	$_conds = null;
	
	$UserId = $this -> EUI_Session -> _get_session('UserId');
	
	$sql = " SELECT a.ActivityDateTs FROM t_tx_agent_activity a  WHERE a.UserId='$UserId' 
			 AND a.ActivityAction='LOGIN' ORDER BY a.ActivityId DESC LIMIT 1 ";
			 
	$qry = $this -> db -> query($sql);
	if( !$qry -> EOF() )
	{
		$rows = $qry -> result_first_assoc();
		if( ($rows['ActivityDateTs']!='') )
		{
			$_conds = $rows['ActivityDateTs'];
		}
	}
	
	return $_conds;
		
 }
 
/*
 * get login detail every user on coll agent 
 * return < array >
 */
 
function getLoginUser( $_User )
{
	$_conds  = FALSE;
	if( is_array( $_User ) ) 
	{
		$this->db->select("a.*, b.menu_group, b.menu, b.name as GroupName ", FALSE);
		$this->db->from("t_tx_agent a");
		$this->db->join("t_gn_agent_profile b ","a.handling_type=b.id ","LEFT");
		$this->db->where("a.id",$_User['username']);
		$this->db->where("a.password='".md5($_User['password'])."'", "", FALSE);
		// echo $this->db->_get_var_dump();
		$qry = $this -> db->get();
		if( $qry->num_rows() > 0 )
		{
			$_conds = $qry -> result_first_assoc();
		}	
	}
	
	return $_conds;
 }
 
 
 /*
 * get login detail every user on coll agent 
 * return < array >
 */
 function _setUpdateLastLogin( $Login=1 )
 {
	$_conds= 0;
	
	if( $Login==1) 
	{
		$this ->db -> set('login_count','(login_count)+1', false );
		$this ->db -> where('UserId', $this -> EUI_Session ->_get_session('UserId'));
		$this ->db -> update('t_tx_agent');
	}
	
	if( $this -> EUI_Session -> _get_session('UserId') )
	{
		if( $Login==0 )
		{
			$this -> db ->set('ip_address','NULL',FALSE);	
			$this -> db ->set('last_update',$this -> EUI_Tools -> _date_time());
			$this -> db ->set('logged_state',$Login);
		}	
		else
		{
			$this -> db ->set('ip_address', $this -> EUI_Tools -> _get_real_ip() );
			$this -> db ->set('last_update',$this -> EUI_Tools -> _date_time() );
			$this -> db ->set('logged_state',$Login);
		}
		
		//set update testing 
		
		$this -> db -> where('UserId',$this -> EUI_Session ->_get_session('UserId'));
		$this -> db -> update('t_tx_agent');
		
		if( $this -> db ->affected_rows() > 0 ){
			$_conds++;
		}
	}					
		
	return $_conds;
 }
 

 function _setUpdeteBlocked($id = null)
 {
 	$sql ="UPDATE t_tx_agent a SET a.logged_state=0,a.user_state=0, a.ip_address=NULL WHERE a.UserId='".$id."'";

 	if( $this->db->query($sql)) {
 		return  true;
 	}
 	return false;
 }
 
/*
 * @ pack : update User password 
 *
 */
 
 public function _setUpdatePassword($param=null)
{
	$_conds = 0;
	if( !is_null($param) )
	{
		$this->db->where('id', $param['Username']);
		$this->db->where('password', $param['curr_password']);
		$this->db->set('password',$param['new_password']);
		$this->db->set('update_password',date('Y-m-d H:i:s'));
		 
		
		if($this->db->update('t_tx_agent')) 
		{
			$M_SysUser =& M_SysUser::Instance();
			$rows = $M_SysUser->getUserQuery( array('id' => $param['Username'] ) );
			foreach( $rows as $key => $values ){
				$this->M_Loger->_set_session_id('UserId', $values['UserId']);
				$this->db->set('UserId', $values['UserId']);
			}
			
			$this->db->set('UserName', $param['Username']);
			$this->db->set('UserPassword', $param['curr_password']);
			$this->db->set('UserCreatedTs', date('Y-m-d H:i:s'));
			$this->db->insert('t_gn_password_history');
			
			if( $this->db->affected_rows() > 0 )
			{
				$this->M_Loger->set_activity_log("change password from {$param['curr_password']} to {$param['new_password']}");
				$_conds++;
			}
		}
	}
	
	return $_conds;
 }

 
/*
 * @ pack : update User password 
 *
 */
 
 public function _get_select_used_password( $Username = null, $Password = null )
{

 $conds = 0;
 $this->db->reset_select();
 $this->db->select('count(a.HistoryId) as jumlah', FALSE );
 $this->db->from('t_gn_password_history a');
 $this->db->where('a.UserName', $Username);
 $this->db->where('a.UserPassword', $Password);
 
 $qry = $this->db->get();
 if( $qry->num_rows()> 0) {
	$conds = (INT)$qry->result_singgle_value();
 }
 
 return $conds;

} 
 
 /** 
  * @ def : handle user on work group data 
  * ------------------------------------------------------------------------
  
  * @ param  : UserId (INT) get by login 
  * @ return : void(0);
  */
  
 public function setWorkProject( $UserId = null )
{
   $_conds = true;
   
   $M_SetWorkProject =& M_SetWorkProject::Instance();
   $M_SysUserProject=& M_SysUserProject::Instance(); 
   
   if(!is_null($UserId)) 
   {
	
	/*
     * @ pack : set auto regiter if project is only one 
	 */
	
	 $is_user_exist = $M_SysUserProject->_getUserExistOnProject($UserId);
	 
	 if((class_exists('M_SetWorkProject')) 
		AND ($is_user_exist == 0 )){ 
		$M_SetWorkProject->_setAutomaticRegister($UserId);
	 }
	  
	// @ pack : exactly root 
 	
	 $workset = array(USER_ROOT);
	  if( class_exists('M_SysUser') 
		 AND ($Users = $this->M_SysUser->_get_detail_user($UserId)) )
	  {
			$result_session = array();
			if( in_array( $Users['handling_type'], $workset) ) 
			{
				$this->db->select('a.ProjectId');	
				$this->db->from('t_lk_work_project a');
				$this->db->where('a.ProjectFlags',1);
				foreach( $this->db->get() -> result_assoc() as $rows ) 
				{
					$result_session[$rows['ProjectId']] = $rows['ProjectId'];
				}
			}
			else
			{
				$this->db->select('a.ProjectId');	
				$this->db->from('t_gn_user_project_work a');
				$this->db->where('a.Status',1);
				$this->db->where('a.UserId', $Users['UserId']);
				
				foreach( $this->db->get() -> result_assoc() as $rows ){
					$result_session[$rows['ProjectId']] = $rows['ProjectId'];
				}
			}
			
		/**<< set in session >> ****/
		
			if( is_array($result_session) ) 
			{
				session_start();
				
				if( count($result_session) > 0 )
					$this->EUI_Session->_set_session('ProjectId', array_keys($result_session));
				else
					$this->EUI_Session->_set_session('ProjectId', array(0));
			}
		}
   }
 }
 

 
/** 
 * @ def : handle user on work group data 
 * ------------------------------------------------------------------------
  
 * @ param  : UserId (INT) get by login 
 * @ return : void(0);
 */
 
 public function _get_expired_password( $User )
{
  $result_conds = null;
  
  $clas_config =& M_Configuration::get_instance(); 
  $cass_auth_rule =& $clas_config->_get_auth_rule_modul();
 
  if( (isset($cass_auth_rule['PASSWORD_CHECK'])) 
	AND ($cass_auth_rule['PASSWORD_CHECK']==1 
	OR $cass_auth_rule['PASSWORD_CHECK']==TRUE ) )
 {
	 $this->db->reset_select();
	 $this->db->select("a.password as old_password, id as user_agent, a.update_password ",FALSE);
	 $this->db->from("t_tx_agent a");
	 $this->db->where("a.id", $User['username']);
	 $this->db->where("a.password", md5($User['password']));
	 
	 $qry = $this->db->get();
	 if( $qry->num_rows() > 0 ) 
	 {
		$rows_result_assoc = $qry->result_first_assoc();
		$diff_days = _getDateDiff(date('Y-m-d'), date('Y-m-d', strtotime($rows_result_assoc['update_password'])));
		
		if((INT)$diff_days['days_total'] >= (INT)$cass_auth_rule['PASSWORD_EXPIRED'] ) {
			$rows_result_assoc['real_password'] = $User['password'];
			$result_conds = $rows_result_assoc;
		}
	 }
  }
   return $result_conds;
}

// new line 20190211
function _getUserAbsenStatus($username){
	$_conds = null;
	
	$sql = " SELECT a.user_state FROM t_tx_agent a WHERE a.Id='$username'";
	// echo $sql;
	
	$qry = $this -> db -> query($sql);
	if( !$qry -> EOF() )
	{
		$rows = $qry -> result_first_assoc();
		if( ($rows['user_state']!='') )
		{
			$_conds = $rows['user_state'];
		}
	}
	
	return $_conds;
}

// END CLASS 

 
}

// END OF FILE
// location : ./application/controller/Auth.php

?>