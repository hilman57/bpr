<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_ModAccessAll extends EUI_Model
{
	/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
private static $Instance = null;

/*
 * @ pack : Instance 
 */
 
 public static function &Instance()
{
  if(is_null(self::$Instance) )
 {
	self::$Instance = new self();
  }
 
 return self::$Instance;
 
}

/*
 * @ pack : Aksesor  
 */
 
 public function M_ModAccessAll() 
{
	$this->load->model(array('M_SetCallResult'));
				
 }

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("b.bucket_trx_id");
	$this->EUI_Page->_setFrom("t_gn_modul_setup a");
	$this->EUI_Page->_setJoin("t_gn_buckettrx_debitur b","a.modul_setup_id=b.modul_setup_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_debitur c","b.deb_id = c.deb_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_access_all g","b.bucket_trx_id = g.bucket_trx_id","INNER");
	$this->EUI_Page->_setJoin("t_lk_account_status d","c.deb_call_status_code=d.CallReasonCode","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment e","c.deb_id=e.CustomerId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent f","e.AssignSelerId = f.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent h","g.access_to = h.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_st_access_all i ","g.st_access_all_id = i.st_access_all_id","LEFT");
	$this->EUI_Page->_setJoin("t_lk_assign_access_type j ","g.access_type_id = j.access_type_id","LEFT", TRUE);
	
	// access all filter
	$this->EUI_Page->_setAnd('a.mod_set_running',1);
	$this->EUI_Page->_setAnd('c.deb_is_access_all',1);
	$this->EUI_Page->_setAnd('i.bool_running',1);
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_SPV) ) )
	{
		$this->EUI_Page->_setAnd('i.st_access_create_id',_get_session('UserId'));
	}
	
	 
	
	// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('c.deb_acct_no', 'ModAccess_cust_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'ModAccess_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_call_status_code', 'ModAccess_account_status', TRUE);
	$this->EUI_Page->_setAndCache('f.UserId', 'ModAccess_Exist_Agent', TRUE);
	$this->EUI_Page->_setAndCache('g.access_type_id', 'ModAccess_AssignType', TRUE);
	$this->EUI_Page->_setAndCache('g.access_to', 'nav_Assign_To', TRUE);
	
	// echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/
	$this->EUI_Page->_setArraySelect(array(
		'b.deb_id as deb_id' => array('deb_id', 'debid', 'hidden'),
		'b.bucket_trx_id AS bucket_trx_id'=> array('bucket_trx_id','ID','primary'),
		'c.deb_acct_no AS CustomerNumber'=>array('CustomerNumber','Customer Id'),
		'c.deb_name AS CustomerName'=>array('CustomerName','Customer Name'),
		'd.CallReasonDesc AS AccountStatus'=>array('AccountStatus','Account Status'),
		'f.full_name AS ExistAgentName'=>array('ExistAgentName','Exist Agent Name'),
		'j.access_type_name AS AccessType'=>array('AccessType','Assign Type'),
		'h.full_name AS NewAgentName'=>array('NewAgentName','New Agent Name'),
		'c.deb_call_activity_datets AS LastCallDate'=>array('LastCallDate','Last Call Date'),
		'i.st_create_date_ts AS Create_Time'=>array('Create_Time','Upload Date')
	));
	
	$this->EUI_Page->_setFrom("t_gn_modul_setup a");
	$this->EUI_Page->_setJoin("t_gn_buckettrx_debitur b","a.modul_setup_id=b.modul_setup_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_debitur c","b.deb_id = c.deb_id","INNER");
	$this->EUI_Page->_setJoin("t_gn_access_all g","b.bucket_trx_id = g.bucket_trx_id","INNER");
	$this->EUI_Page->_setJoin("t_lk_account_status d","c.deb_call_status_code=d.CallReasonCode","LEFT");
	$this->EUI_Page->_setJoin("t_gn_assignment e","c.deb_id=e.CustomerId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent f","e.AssignSelerId = f.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent h","g.access_to = h.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_st_access_all i ","g.st_access_all_id = i.st_access_all_id","LEFT");
	$this->EUI_Page->_setJoin("t_lk_assign_access_type j ","g.access_type_id = j.access_type_id","LEFT", TRUE);
	
	// access all filter
	$this->EUI_Page->_setAnd('a.mod_set_running',1);
	$this->EUI_Page->_setAnd('c.deb_is_access_all',1);
	$this->EUI_Page->_setAnd('i.bool_running',1);
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_SPV) ) )
	{
		$this->EUI_Page->_setAnd('i.st_access_create_id',_get_session('UserId'));
	}
	
	
	
	// @ pack : filter by default session ---------------------------------------------------------------------------------
	// $this->EUI_Page->_setWherein('f.ProjectId',_get_session('ProjectId') );
	
	$this->EUI_Page->_setAndCache('c.deb_acct_no', 'ModAccess_cust_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'ModAccess_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_call_status_code', 'ModAccess_account_status', TRUE);
	$this->EUI_Page->_setAndCache('f.UserId', 'ModAccess_Exist_Agent', TRUE);
	$this->EUI_Page->_setAndCache('g.access_type_id', 'ModAccess_AssignType', TRUE);
	$this->EUI_Page->_setAndCache('g.access_to', 'nav_Assign_To', TRUE);
	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	// echo $this ->EUI_Page->_getCompiler(); 
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
 
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }

 public function get_all_leader()
 {
	$data = array();
	$this->db->reset_select();
	$this->db->select('a.UserId, a.full_name');
	$this->db->from('t_tx_agent a');
	$this->db->where(array(	'a.user_state'=>1,
							'a.handling_type'=>USER_LEADER));
 
   $qry = $this->db->get();
   if( $qry->num_rows() > 0 ){
		foreach( $qry->result_assoc() as $rows )
		{
			$data[$rows['UserId']] = $rows['full_name'];
		}
   }
   return $data;
 }
 
	public function get_all_DC()
	{
		$data = array();
		$this->db->reset_select();
		$this->db->select('a.UserId, a.full_name,a.code_user');
		$this->db->from('t_tx_agent a');
		$this->db->where(array(	'a.user_state'=>1,
			'a.handling_type'=>USER_AGENT_OUTBOUND));

		$qry = $this->db->get();
		if( $qry->num_rows() > 0 )
		{
			foreach( $qry->result_assoc() as $rows )
			{
				$data[$rows['UserId']] = $rows['code_user'] ." - ". $rows['full_name'];
			}
		}
		return $data;
	}
 
 
 
	 // update t_akses_all by deb id
	 
	public function UpdateAccessAssign()
	{
		$msg=array();
		$data=array();
		$success=0;
		$fail=0;
		$AssignType = $this->URI->_get_post('AssignType');
		$AssignTo = $this->URI->_get_array_post('AssignTo');
		$bucket_trx = $this->URI->_get_array_post('bucket_trx_id');
		
		$data = $this->get_access_all_data(array(
			'bucket_trx' => $bucket_trx,
			'access_type'=> null
		));
		foreach($data as $index => $access_all_id)
		{
			if(is_array($AssignTo))
			{
				foreach($AssignTo as $index => $userid)
				{
					$this->db->set( 'userid', $userid );
					$this->db->set( 'access_all_id', $access_all_id );
					if( $this->db->insert('t_gr_user_access') )
					{
						$this->db->set("access_type_id",$AssignType);
						$this->db->where("access_all_id", $access_all_id);
						$this->db->where("access_type_id IS NULL");
						$this->db->update("t_gn_access_all");
						if($this->db->affected_rows()>0)
						{
							$success++;
						}
					}
					else
					{
						$fail++;
					}
				}
			}
			else
			{
				$this->db->set("access_type_id",$AssignType);
				$this->db->where("access_all_id", $access_all_id);
				$this->db->where("access_type_id IS NULL");
				$this->db->update("t_gn_access_all");
				if($this->db->affected_rows()>0)
				{
					$success++;
				}
				else
				{
					$fail++;
				}
			}
		}
		$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
		return $msg;
	}
 
	public function UpdateAccessFlagAll(){
		$modtype = $this->URI->_get_post('modtype');
		$assignto = $this->URI->_get_post('assignto');
		$success = 0;
		$fail = 0;
		$msg = array('Message'=>0,'Success'=>$success,'Fail'=>$fail);
		/*$sql = "update t_gn_buckettrx_debitur a 
					inner join t_gn_debitur b on a.deb_id = b.deb_id
					inner join t_gn_access_all c on a.bucket_trx_id = c.bucket_trx_id
					SET b.deb_is_access_all = 0";
					
		// if($modtype == 1 ){
			// $conds = " where c.access_type_id = '".$modtype."'";
		// }else {
			// $conds = " where c.access_type_id = '".$modtype."' and c.access_to = '".$assignto."'";
		// }
		$conds = " where c.access_type_id = '".$modtype."'";
		$sql .= $conds;
		
		$this->db->query($sql);
		// echo $this->db->last_query();
		if($this->db->affected_rows() >0){
			$success++;
		}else{
			$fail++;
		}*/
		$data = $this->get_access_all_data(array(
			'access_type'=> $modtype
		));
		if(count($data) > 0)
		{
			// $this->db->set('b.deb_is_access_all',0);
			// $this->db->set('d.bool_running',0);
			// $this->db->join('t_gn_debitur b ','a.deb_id = b.deb_id','INNER');
			// $this->db->join('t_gn_access_all c ','a.bucket_trx_id = c.bucket_trx_id','INNER');
			// $this->db->join('t_st_access_all d ','c.st_access_all_id = d.st_access_all_id','INNER');
			// $this->db->where_in('c.access_all_id',$data);
			// $this->db->update('t_gn_buckettrx_debitur a');
			// echo $this->db->last_query();
			foreach($data as $index => $access_all_id)
			{
				$this->db->query("UPDATE t_gn_buckettrx_debitur a
							  INNER JOIN t_gn_debitur b ON a.deb_id=b.deb_id
							  INNER JOIN t_gn_access_all c ON a.bucket_trx_id = c.bucket_trx_id
							  SET b.deb_is_access_all = 0
							  WHERE c.access_all_id =".$access_all_id);
				if($this->db->affected_rows()>0)
				{
					$success++;
					
				}
				else
				{
					$fail++;
				}
			}
			$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
		}
		return $msg;
		
	}
	 // update access all flag (Reset)
	public function UpdateAccessFlag()
	{
		
		$msg=array();
		$success=0;
		$fail=0;
		$bucket_trx = explode(',',$this->URI->_get_post('bucket_trx_id'));
		foreach($bucket_trx as $index => $bucket_trx_id)
		{
			$this->db->query("UPDATE t_gn_buckettrx_debitur a
							  INNER JOIN t_gn_debitur b ON a.deb_id=b.deb_id
							  SET b.deb_is_access_all = 0
							  WHERE a.bucket_trx_id= ".$bucket_trx_id);
			if($this->db->affected_rows()>0)
			{
				$success++;
			}
			else
			{
				$fail++;
			}
		}
		$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
		return $msg;
		
	}
	
	public function UpdateAssignAccessAll()
	{
		$data = array();
		$msg=array();
		$success=0;
		$fail=0;
		$AssignType = $this->URI->_get_post('AssignType');
		$AssignTo = $this->URI->_get_array_post('AssignTo');
		// var_dump($AssignTo);
		$data = $this->get_access_all_data(array(
			'access_type'=> null
		));
		if(count($data)>0)
		{
			foreach($data as $idx => $access_all_id)
			{
				if(is_array($AssignTo))
				{
					foreach($AssignTo as $index => $userid)
					{
						$this->db->set( 'userid', $userid );
						$this->db->set( 'access_all_id', $access_all_id );
						if( $this->db->insert('t_gr_user_access') )
						{
							$this->db->set("access_type_id",$AssignType);
							$this->db->where("access_all_id", $access_all_id);
							$this->db->update("t_gn_access_all");
							if($this->db->affected_rows()>0)
							{
								$success++;
							}
						}
						else
						{
							$fail++;
						}
					}
				}
				else
				{
					$this->db->set("access_type_id",$AssignType);
					$this->db->where("access_all_id", $access_all_id);
					$this->db->update("t_gn_access_all");
					if($this->db->affected_rows()>0)
					{
						$success++;
					}
					else
					{
						$fail++;
					}
				}
				
			}
			$msg = array('Message'=>1,'Success'=>$success,'Fail'=>$fail);
		}
		return $msg;
	}

 
 /*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 public function getAccessType()
 {
	$data=array();
	$this->db->reset_select();
	$this->db->select("a.access_type_id,a.access_type_name,a.access_type_link");
	$this->db->from("t_lk_assign_access_type a");
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ){
		foreach( $qry->result_assoc() as $rows )
		{
			$data['combo'][$rows['access_type_id']]=$rows['access_type_name'];
			$data['link'][$rows['access_type_id']]=$rows['access_type_link'];
		}
		
   }
   return $data;
 }
 
 public function CheckDebiturAccessAll($DebiturId=null)
{
	$_conds = array();
	if(!is_null($DebiturId))
	{
		$this->db->reset_select();
		$this->db->select(" a.deb_id,b.bucket_trx_id,c.mod_upload_id,d.access_all_id",FALSE);
		$this->db->from("t_gn_debitur a");
		$this->db->join("t_gn_buckettrx_debitur b","a.deb_id=b.deb_id","INNER");
		$this->db->join("t_gn_modul_setup c","b.modul_setup_id=c.modul_setup_id","INNER");
		$this->db->join("t_gn_access_all d","b.bucket_trx_id=d.bucket_trx_id","INNER");
		$this->db->where("a.deb_id",$DebiturId);
		$this->db->where("c.modul_id_rnd","1");
		$this->db->where("c.mod_set_running","1");
		$this->db->where("a.deb_is_access_all","1");
   
		$qry = $this->db->get();
		if( $qry->num_rows() > 0) 
		{
			foreach( $qry->result_assoc() as $rows ) {
				$_conds['DebiturId'] = $rows['deb_id'];
				$_conds['bucket_trx_id'] = $rows['bucket_trx_id'];
				$_conds['access_all_id'] = $rows['access_all_id'];
			}
		}
	}
	return $_conds;
}

private function get_access_all_data($where=array())
{
	$data = array();
	$this->db->reset_select();
	$this->db->select('c.access_all_id');
	$this->db->from('t_gn_modul_setup a');
	$this->db->join('t_gn_buckettrx_debitur b','a.modul_setup_id=b.modul_setup_id','INNER');
	$this->db->join('t_gn_access_all c','b.bucket_trx_id=c.bucket_trx_id','INNER');
	$this->db->join('t_gn_debitur d','b.deb_id=d.deb_id','INNER');
	$this->db->join('t_st_access_all e','c.st_access_all_id=e.st_access_all_id','INNER');
	$this->db->where('a.mod_set_running','1');
	
	$this->db->where('d.deb_is_access_all','1');
	$this->db->where('e.bool_running','1');
	if(isset($where['bucket_trx']))
	{
		if(is_array($where['bucket_trx']))
		{
			$this->db->where_in('c.bucket_trx_id',$where['bucket_trx']);
		}
		else
		{
			$this->db->where('c.bucket_trx_id',$where['bucket_trx']);
		}
	}
	if(isset($where['access_type']))
	{
		if(!empty($where['access_type']))
		{
			$this->db->where('c.access_type_id',$where['access_type']);
		}
	}
	elseif(is_null($where['access_type']))
	{
		$this->db->where('c.access_type_id IS NULL');
	}
	
	$qry = $this->db->get();
	$i=0;
	if( $qry->num_rows() > 0 )
	{
		foreach( $qry->result_assoc() as $rows )
		{
			$data[$i]=$rows['access_all_id'];
			$i++;
		}
	}
	return $data;
} 
 // END CLASS 
}

?>