<?php
/*
 * @ pack : M_ModCallHistory
 */
 
class M_ModCallHistory extends EUI_Model {


/*
 * @ pack : M_ModCallHistory
 */
 
private static $Instance  = null;


/*
 * @ pack : M_ModCallHistory
 */
public static function &Instance()
{
  if(is_null(self::$Instance)) 
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
} 
/*
 * @ pack : M_ModCallHistory
 */
 
 public function M_ModCallHistory()
{

}

/*
 * @ pack : _get_call_history_user
 */
 
 public function _get_call_history_user( $UserId = 0 )
{
	$this->db->reset_select();
	$this->db->select("
		a.CallHistoryCallDate, 
		a.CallNumber,
		a.CallHistoryNotes,
		d.spc_name as SPCName,
		d.spc_description as SPCDesc,
		e.rpc_code as RPCName,
		e.rpc_description as RPCDesc,
		b.deb_name as CustomerName, 
		c.id as UserName, 
		c.code_user as UserCode, 
		c.init_name as UserOnline, 
		c.full_name as UserFullname,
		f.CallReasonDesc as AccountStatusName,
		g.CallReasonDesc as CallStatusName", 
	FALSE);
	
	$this->db->from("t_gn_callhistory a ");
	$this->db->join("t_gn_debitur b "," a.CustomerId=b.deb_id","LEFT");
	$this->db->join("t_tx_agent c "," a.CreatedById=c.UserId","LEFT");
	$this->db->join("t_lk_speach_with d "," a.CallHistorySpc = d.spc_code","LEFT");
	$this->db->join("t_lk_remote_place e "," a.CallHistoryRpc = e.rpc_code","LEFT");
	$this->db->join("t_lk_account_status f "," a.CallAccountStatus=f.CallReasonCode","LEFT");
	$this->db->join("t_lk_account_status g "," a.CallReasonId=g.CallReasonCode","LEFT");
    
// @ pack : user handle ID 

	if( in_array( _get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) )) 
	{
		$this->db->where("a.CreatedById",$UserId);
	}
	
// @ pack : 	
	
	if( in_array( _get_session('HandlingType'), 
		array(USER_SUPERVISOR) )) 
	{
		$this->db->where("a.SupervisorId",$UserId);
	}
// @ pack : 
	
	if( in_array( _get_session('HandlingType'), 
		array(USER_LEADER) )) 
	{
		$this->db->where("a.TeamLeaderId",$UserId);
	}
	
	$this->db->order_by("a.CallHistoryId", "DESC");
	// echo $this->db->_get_var_dump();
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_assoc();
	} else { 
		return FALSE;
	}
}

/*
 * @ pack : get last call history by ID 
 */
 
 public function _get_last_call_history_customer( $CustomerId = 0  )
{
  $result = null;
  
	$this->db->reset_select();
	$this->db->select('*');
	$this->db->from('t_gn_callhistory a ');
	$this->db->where('a.CustomerId', 0 );
	$this->db->order_by('a.CallHistoryId', 'DESC');
	$this->db->limit(1);
	$qry = $this->db->get();
	if( $qry->num_rows()> 0 ) {
		$result = (array)$qry->result_first_assoc();
	}
	
	return $result;
} 
// _get_last_call_history_customer =====================> 
 

/*
 * @ pack : get call history by customer id
 */
 
 public function _get_call_history_customer( $CustomerId = 0 )
{
	$this->db->reset_select();
	
	$this->db->select("
		a.CallHistoryCreatedTs, 
		a.CallNumber,
		a.CallHistoryNotes,
		d.spc_name as SPCName,
		d.spc_description as SPCDesc,
		e.rpc_code as RPCName,
		e.rpc_description as RPCDesc,
		b.deb_name as CustomerName, 
		c.id as UserName, 
		c.code_user as UserCode, 
		c.init_name as UserOnline, 
		c.full_name as UserFullname,
		f.CallReasonDesc as AccountStatusName,
		g.CallReasonDesc as CallStatusName", 
	FALSE);
	
	$this->db->from("t_gn_callhistory a ");
	$this->db->join("t_gn_debitur b "," a.CustomerId=b.deb_id","LEFT");
	$this->db->join("t_tx_agent c "," a.CreatedById=c.UserId","LEFT");
	$this->db->join("t_lk_speach_with d "," a.CallHistorySpc = d.spc_code","LEFT");
	$this->db->join("t_lk_remote_place e "," a.CallHistoryRpc = e.rpc_code","LEFT");
	$this->db->join("t_lk_account_status f "," a.CallAccountStatus=f.CallReasonCode","LEFT");
	$this->db->join("t_lk_account_status g "," a.CallReasonId=g.CallReasonCode","LEFT");
    $this->db->where("a.CustomerId",$CustomerId);
	$this->db->order_by("a.CallHistoryCreatedTs", "DESC");
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_assoc();
	} else { 
		return FALSE;
	}
}

//END CLASS  

}
?>