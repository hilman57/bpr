<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
 
class M_ModCustomerReview extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_ModCustomerReview() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser','M_SetCampaign',));
}


// _NextActivity 

public function _NextActivity()
{
    $next_activity = array();
	$this->_get_default();
	$sql = $this->EUI_Page->_getCompiler();
	if( $sql ) 
	{
		$qry = $this->db->query($sql);
		$num = 0;
		foreach( $qry->result_assoc() as $rows ){
			$next_activity[] = $rows['CustomerId'];  
			$num++;
		}
	}
	
	return $next_activity;
	
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
  public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	$this->EUI_Page->_setSelect("a.deb_id");
	$this->EUI_Page->_setFrom("t_gn_favourite_call a ");
	$this->EUI_Page->_setJoin('t_gn_debitur b ','a.deb_id=b.deb_id', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_assignment c','b.deb_id=c.CustomerId', 'LEFT');
	$this->EUI_Page->_setJoin('t_tx_agent d ','a.LastAgentId=d.UserId', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign e ','b.deb_cmpaign_id=e.CampaignId', 'LEFT', TRUE);
	
 //	@ pack : filtering key of the session  --------------------------------------------------------------------------------- 

	$this->EUI_Page->_setAnd('c.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('c.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('c.AssignSpv IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('a.LastCounterCall',5);
	$this->EUI_Page->_setAnd('e.CampaignStatusFlag',1);

 // @ pack : filter by default session ---------------------------------------------------------------------------------
	
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('c.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}	

  // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignSelerId IS NOT NULL');
	}		
	
 // @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'cust_rev_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'cust_rev_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'cust_rev_account_status', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'cust_rev_call_status', TRUE);
	$this->EUI_Page->_setAndCache('d.UserId', 'cust_rev_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_resource', 'cust_rev_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'cust_rev_cust_name', TRUE);
	
 // @ pack : start date ------------------------------------------------------
	$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets>='". _getDateEnglish(_get_post('cust_rev_start_date')) ."'", 'cust_rev_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets<='". _getDateEnglish(_get_post('cust_rev_end_date')) ."'", 'cust_rev_end_date', TRUE);
	
 // @ pack : amount wo  ---------------------------------------------------------------------------------
	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo>=". (INT)_get_post('cust_rev_start_amountwo') ."", 'cust_rev_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo<=". (INT)_get_post('cust_rev_end_amountwo')."", 'cust_rev_end_amountwo', TRUE);
	
 // @ pack : group_by 
	// echo "<!-- ". $this ->EUI_Page->_getCompiler()." -->";
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
	
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.id AS FavouriteId"=> array('FavouriteId','ID','primary'),
		"e.CampaignDesc AS CampaignDesc "=> array('CampaignDesc','Product'),
		"b.deb_acct_no AS AccountNumber "=> array('AccountNumber','Costumer ID'),
		"b.deb_name AS CustomerName"=> array('CustomerName','Customer Name'),
		"UPPER(d.id) AS UserId "=> array('UserId','Agent ID'),
		"f.CallReasonDesc AS AccountStatus "=> array('AccountStatus','Account Status'),
		"b.deb_call_activity_datets AS LastCallDate "=> array('LastCallDate','Last Call Date'),
		"a.LastCallDateTs AS BlockDate "=> array('BlockDate','Block Date'),
		"a.PhoneNumber AS PhoneNumber "=> array('PhoneNumber','Block Number'),
		"b.deb_resource AS Recsource "=> array('Recsource','Recsource'),
		"b.deb_amount_wo AS AmountWO "=> array('AmountWO','Amount WO'),
		"b.deb_bal_afterpay AS BalanceAffterPay "=> array('BalanceAffterPay','Bal. Afterpay'),
		"b.deb_reminder AS History "=> array('History','History')
	));
	
	$this->EUI_Page->_setFrom("t_gn_favourite_call a ");
	$this->EUI_Page->_setJoin('t_gn_debitur b ','a.deb_id=b.deb_id', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_assignment c','b.deb_id=c.CustomerId', 'LEFT');
	$this->EUI_Page->_setJoin('t_tx_agent d ','a.LastAgentId=d.UserId', 'LEFT');
	$this->EUI_Page->_setJoin('t_gn_campaign e ','b.deb_cmpaign_id=e.CampaignId', 'LEFT');
	$this->EUI_Page->_setJoin('t_lk_account_status f ','b.deb_call_status_code=f.CallReasonCode','LEFT', TRUE);
	
// pack : process ---------------------------------------------------------------------------------
	 
	$this->EUI_Page->_setAnd('c.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('c.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('c.AssignSpv IS NOT NULL', FALSE);
 	$this->EUI_Page->_setAnd('a.LastCounterCall',5);
	$this->EUI_Page->_setAnd('e.CampaignStatusFlag',1);
	 
// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 		
	if(in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		$this->EUI_Page->_setAnd('c.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
 // @ pack : filter by default is leader ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignLeader',$this->EUI_Session->_get_session('UserId'));
	}		

// @ pack : filter by default is admin, spv , amgr, mgr ---------------------------------------------------------------------------------
 	
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_SUPERVISOR, USER_MANAGER, USER_ACCOUNT_MANAGER, USER_ADMIN) ) )
	{
		 $this->EUI_Page->_setAnd('c.AssignSelerId IS NOT NULL');
	}		
	
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('b.deb_acct_no', 'cust_rev_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_cmpaign_id', 'cust_rev_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'cust_rev_account_status', TRUE);
	$this->EUI_Page->_setAndCache('b.deb_call_status_code', 'cust_rev_call_status', TRUE);
	$this->EUI_Page->_setAndCache('d.UserId', 'cust_rev_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_resource', 'cust_rev_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('b.deb_name', 'cust_rev_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets>='". _getDateEnglish(_get_post('cust_rev_start_date')) ."'", 'cust_rev_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("b.deb_call_activity_datets<='". _getDateEnglish(_get_post('cust_rev_end_date')) ."'", 'cust_rev_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo>=". (INT)_get_post('cust_rev_start_amountwo') ."", 'cust_rev_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("b.deb_amount_wo<=". (INT)_get_post('cust_rev_end_amountwo')."", 'cust_rev_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	//$this->EUI_Page->_setGroupBy('a.deb_id');
		
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	// echo "<!-- ". $this ->EUI_Page->_getCompiler()." -->";
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _get_select_favourite_call( $FavouriteId = 0 )
{
 $result = array();
 
 if( $FavouriteId )
 {
	$this->db->reset_select();	
	$this->db->select('a.id as FavouriteId, a.*, b.*');
	$this->db->from('t_gn_favourite_call a ');
	$this->db->join('t_tx_agent b','a.LastAgentId=b.UserId', 'LEFT');
	$this->db->where('a.id', $FavouriteId );
	
	$qry = $this->db->get();
	if(($qry->num_rows()>0))
	{
		$result = (array)$qry->result_first_assoc(); 		
	 }
	 
  }
   return $result;	
}


// _setUnblock

public function _setUnblockAll()
{
	$this->load->model('M_Loger');
	$this->db->reset_select();
	$this->db->select('a.id as FavouriteId, a.*, b.*');
	$this->db->from('t_gn_favourite_call a ');
	$this->db->join('t_tx_agent b','a.LastAgentId=b.UserId', 'LEFT');
	$this->db->join('t_gn_debitur c','a.deb_id=c.deb_id', 'LEFT');
	$this->db->where('a.LastCounterCall >=',5);
	$this->db->where('c.deb_is_lock',1);
	
	$query = $this->db->get();
	foreach ($query->result_array() as $rows)
	{
			$FavouriteId = $rows['FavouriteId'];
			
			$this->db->set('deb_is_lock', 0);
			$this->db->set('deb_lock_type', 0);
			$this->db->where('deb_id',$rows['deb_id']);
			$this->db->update('t_gn_debitur');
			
				$this->db->set('LastCounterCall','0');
				$this->db->set('BlockingStatus','0');
				$this->db->set('PoolingUpdate', date('Y-m-d H:i:s'));
				$this->db->set('PoolingById', strtoupper(_get_session('Username')));
				$this->db->where('id', $FavouriteId);
				
				if( $this->db->update('t_gn_favourite_call') )
				{
					$Loger=& M_Loger::Instance();
					$Loger->set_activity_log("UNBLOCK PHONE FROM CUSTOMER REVIEW ID[$FavouriteId]");				
					
				}
				$this->db->set("CustomerId",$rows['deb_id']);
				$this->db->set("CreatedById",$rows['LastAgentId']);
				$this->db->set("TeamLeaderId",$rows['LastTeamLeaderId']);
				$this->db->set("SupervisorId",$rows['LastSupervisorId']);
				$this->db->set("CallHistoryNotes","UNBLOCK ALL CUSTOMER REVIEW -".strtoupper(_get_session('Username')));
				$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s'));
				$this->db->insert("t_gn_callhistory");
				 
			
			$cond++;
	}
	return $cond;
}

 public function _setUnblock( $id = null )
{
 $this->load->model('M_Loger');

// @ pack : loger data 
 
 $cond = 0 ; 
 if( !is_null($id) )
 {
	foreach( $id as $key => $FavouriteId )
	{
		$coll_data = $this->_get_select_favourite_call( $FavouriteId );
	
		if( count($coll_data) > 0 )
		{
			$this->db->set('deb_is_lock', 0);
			$this->db->set('deb_lock_type', 0);
			
			$this->db->where('deb_id',$coll_data['deb_id']);
			
			if( $this->db->update('t_gn_debitur') ) 
			{	
				$this->db->set('LastCounterCall','0');
				$this->db->set('BlockingStatus','0');
				$this->db->set('PoolingUpdate', date('Y-m-d H:i:s'));
				$this->db->set('PoolingById', strtoupper(_get_session('Username')));
				$this->db->where('id', $FavouriteId);
				
				if( $this->db->update('t_gn_favourite_call') )
				{
					$Loger=& M_Loger::Instance();
					$Loger->set_activity_log("UNBLOCK PHONE FROM CUSTOMER REVIEW ID[$FavouriteId]");
					
					
					$cond++;
				}
				
				$this->db->set("CustomerId",$coll_data['deb_id']);
				$this->db->set("CreatedById",$coll_data['LastAgentId']);
				$this->db->set("TeamLeaderId",$coll_data['LastTeamLeaderId']);
				$this->db->set("SupervisorId",$coll_data['LastSupervisorId']);
				$this->db->set("CallHistoryNotes","UNBLOCK CUSTOMER REVIEW -".strtoupper(_get_session('Username')));
				$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s'));
				$this->db->insert("t_gn_callhistory");
				 
			}
		}
	}
}
return $cond;

}
 // END OF CLASS 

}
?>