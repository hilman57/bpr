<?php
/*
 * @ pack : under model M_ModulRPC
 */ 
 
class M_ModActiveMenu extends EUI_Model {

/*
 * @ pack : under model M_InfoPTP
 */ 
 
var $_table_space = array();

/*
 * @ pack : under model M_InfoPTP
 */ 
 
 public function M_ModActiveMenu()
{
  $this->_table_space = array('t_gn_active_menu');
  $this->load->model(array('M_SysPrivileges','M_SetCampaign','M_Loger'));
  
}


/*
 * @ pack : under model M_InfoPTP
 */ 
 public function _get_system_menu( $ID= null )
{
$config = array();

 $this->db->reset_select();
 $this->db->select("*");
 $this->db->from('t_gn_application_menu');
 
 if( !is_null($ID) ){
	$this->db->where_in('id',$ID);
 }
 $this->db->where("flag","1");
	
 foreach( $this->db->get()->result_assoc() as $rows )
 {
	$config[$rows['id']] = $rows['menu'];			
 }
 
 return $config;
 
	
}

/*
 * @ pack : get status by Level && exist status on here 
	SER_LEVEL 	USER_AGENT_INBOUND 	6 	Active
	2 	USER_LEVEL 	USER_ROOT 		8 	Active
	3 	USER_LEVEL 	USER_ADMIN 		1 	Active
	4 	USER_LEVEL 	USER_SUPERVISOR 	3 	Active
	5 	USER_LEVEL 	USER_QUALITY_STAFF 	11 	Active
	6 	USER_LEVEL 	USER_QUALITY_HEAD 	5 	Active
	7 	USER_LEVEL 	USER_ACCOUNT_MANAGER 	9 	Active
	8 	USER_LEVEL 	USER_MANAGER 	2 	Active
	9 	USER_LEVEL 	USER_AGENT_OUTBOUND 	4 	Active
	10 	USER_LEVEL 	USER_LEADER
	
 */
 
public function _get_level_status_privileges( $level = 0 )
{

 $_account_status_level = array();
 
 if( $this->EUI_Session->_have_get_session('UserId') )
 {
	$this->db->reset_select();
	
	$this->db->select(" 
		a.acs_code_level1 as Level1, c.CallReasonDesc as DescLeve1, 
		a.acs_code_level2 as Level2, b.CallReasonDesc as DescLeve12", FALSE);
	
	$this->db->from("t_gn_assign_acount_status a");
	$this->db->join("t_lk_account_status b "," a.acs_code_level2=b.CallReasonCode","LEFT");
	$this->db->join("t_lk_account_status c  "," a.acs_code_level1=c.CallReasonCode","LEFT");
	
	
/*
 * @ pack : root & am, acm admin 
 */ 
	$USER_LEVEL = _get_session('HandlingType');
	if( in_array(
		$USER_LEVEL, array(USER_ROOT,USER_ADMIN,USER_ACCOUNT_MANAGER)))
	{
		$this->db->where("a.acs_code_level1", $level);
	}
	
/*
 * @ pack : if login by TL 
 */ 
	if( in_array(
		$USER_LEVEL, array(USER_LEADER,USER_AGENT_OUTBOUND,USER_AGENT_INBOUND)))
	{
		$this->db->where("a.acs_code_level1", $level);
		$this->db->where("a.acs_code_level1", $level);
	}
	
	$this->db->where("a.acs_code_flags",1);
	
	$qry = $this->db->get();
	if( $qry->num_rows()>0)
		foreach( $qry->result_assoc() as $rows )
	{
		$_account_status_level[$rows['Level1']] = $rows['DescLeve1'];
		$_account_status_level[$rows['Level2']] = $rows['DescLeve12'];	
	}
	
	
	
	
	
  }
  
  return $_account_status_level;
  
}

/*
 * @ pack : _get_level_account_status
 */
 
public function _get_user_privileges()
{
	$select_user_privilege = null;
	if( is_null( $select_user_privilege ))
	{
		if( $rows = $this->M_SysPrivileges->_get_select_user_privilege(array(1)) ) {
			$select_user_privilege = $rows;
		}
	}
		
	return $select_user_privilege;
}

// @ pack : (acs_code_id, acs_code_level1, acs_code_level2, acs_code_privileges, 
// @ pack : acs_code_campaign, acs_code_created, 
// @ pack : acs_code_createby, acs_code_flags)


/*
 * @ pack : select all 
 */
 
 public function _get_labels_acs()
{
	$_acs_label = array(
		'ActiveUserLevel' => 'User Level',
		'ActiveMenuId' => 'Menu ID',
		'ActiveMenuName' => 'Menu Name',
		'ActiveMenuTitle' => 'Title'
	);
	
	return $_acs_label;
}


/*
 * @ pack : select all 
 */
 
 public function _get_select_acs()
{

  $_acs_label = array();
  return $_acs_label;
   
}

/*
 * @ pack : select all 
 */
 
 public function _get_detail_acs( $argv_vars = null )
{
  $conds = array();	
  
  $this->db->reset_select();
  $this->db->select("*",FALSE);
  $this->db->from(reset($this->_table_space));
  $this->db->where('ActiveId', $argv_vars);
  
  $qry = $this->db->get();
  if( $qry->num_rows()>0 ) {
	$conds = $qry->result_first_assoc();
  }
  
  return $conds;
	
}


/*
 * @ pack : select all 
 */
 
 public function _get_field_acs()
{
	return $this->db->list_fields(reset($this->_table_space));
}

/*
 * @ pack : select all 
 */
 
 public function _get_component_acs() 
{
	return array 
	( 
		'primary' => array('ActiveId'),
		'input' => array('ActiveMenuName','ActiveMenuTitle'),
		'combo' => array('ActiveMenuId','ActiveUserLevel'),
		'option'=> array(
			'ActiveMenuId'=>$this->_get_system_menu(),
			'ActiveUserLevel'=> $this->_get_user_privileges()
		)
	);
}

/*
 * @ pack : _set_save_spc
 */
 
public function _set_save_acs( $arg_vars = null )
{
  $conds = 0;	
  if( is_array($arg_vars) )
  {
	foreach( $arg_vars as $field => $value ) {
	  $this->db->set($field, $value);
	}
	$this->db->set('ActiveMenuCreateTs', date('Y-m-d H:i:s'));
	
	// @ pack : set save data 
	$this->db->insert(reset($this->_table_space));
	if( $this->db->affected_rows() > 0 ) 
	{
		$this->M_Loger->set_activity_log("Add Assign Account Status ::".implode(',',$arg_vars));
		$conds++;
	}
  }
 
 return $conds;
  
} 


/*
 * @ pack : _set_save_spc
 */
 
 public function _set_update_acs( $argv_vars = null )
{

 $conds = 0;
 
 // @ pack : reset all select query ..
 
 $this->db->reset_select(); 
 
 // @ pack : cek argument set vars ..
  
 if( is_array($argv_vars) )	
	foreach( $argv_vars as $field => $value )
 {
	if(in_array($field,array('ActiveId') ) ) {
		$this->db->where($field, $value);
	} else {
		$this->db->set($field, $value);
	}
	
	$this->db->set('ActiveMenuCreateTs', date('Y-m-d H:i:s'));
  }
  
  // @ pack : execute query from here ..
   
  $this->db->update(reset($this->_table_space));
  
  if($this->db->affected_rows()>0){
    $this->M_Loger->set_activity_log("Edit Assign Account Status ::".implode(',',$argv_vars));
	$conds++;
  }
 
  return $conds;
 
}

/*
 * @ pack : _set_save_spc
 */
 
public function _set_delete_acs( $acsId = NULL )
{
  $conds = 0;
  if( is_array($acsId) ) 
	foreach( $acsId as $PrimaryId => $value )
  {
	  $this->db->where('ActiveId', $value);	
      $this->db->delete(reset($this->_table_space));
	  
	  if( $this->db->affected_rows() > 0 )
	  {
		 $this->M_Loger->set_activity_log("Delete Active Menu ::".implode(',',$acsId));
		 $conds++;
	  }
  }
  
  return $conds;
}

/*
 * @ pack : _get_default
 */
 
 public function _get_default_acs()
{
  $this->EUI_Page->_setPage(10); 
  $this->EUI_Page->_setSelect("a.ActiveId");
  $this->EUI_Page->_setFrom(reset($this->_table_space) ." a", TRUE); 
  
  // @ pack : set filter --------------------
	
   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd(" 
			a.ActiveId LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveUserLevel LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveMenuId LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveMenuName LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveMenuTitle LIKE '%{$this->URI->_get_post('keywords')}%'"
		);
  }		
	
 // @ pack : process its  
 
  if( $this->EUI_Page->_get_query() ) {
		return $this->EUI_Page;
  }
}

/*
 * @ pack : _get_default
 */
 
 public function _get_content_acs()
{
  $this->EUI_Page->_postPage($this->URI->_get_post('v_page') );
  $this->EUI_Page->_setPage(10);
  
// @ pack : set filter --------------------
	  
  $this->EUI_Page->_setArraySelect(array(
		"a.ActiveId AS ActiveId" => array("ActiveId","ID","primary"),
		"a.ActiveId AS ID" => array("ID","ID"),
		"b.name AS PrivilegeName" => array("PrivilegeName","User Level"),
		"a.ActiveMenuId AS ActiveMenuId" => array("ActiveMenuId","Menu ID"),
		"a.ActiveMenuName AS ActiveMenuName" => array("ActiveMenuName","Menu Name"),
		"a.ActiveMenuTitle AS ActiveMenuTitle" => array("ActiveMenuTitle","Menu Title") 
	));
  
   $this->EUI_Page->_setFrom(reset($this->_table_space) ." a ");
   $this->EUI_Page->_setJoin("t_gn_agent_profile b ", "a.ActiveUserLevel=b.id", "LEFT");
  
  // @ pack : set filter --------------------
	
   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd(" 
			a.ActiveId LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveUserLevel LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveMenuId LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveMenuName LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR a.ActiveMenuTitle LIKE '%{$this->URI->_get_post('keywords')}%'"
		);
  }	
	
  if( $this->URI->_get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy($this ->URI->_get_post('order_by'),$this ->URI->_get_post('type'));
  }
  
 // echo $this->EUI_Page->_getCompiler();
  
  $this -> EUI_Page->_setLimit();
}

/*
 * @ pack : _get_default
 */
 
 public function _get_resource_acs()
{
	self::_get_content_acs();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
}

/*
 * @ pack : _get_default
 */
 
 public function _get_page_number_acs() 
{
	if( $this->EUI_Page->_get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ pack : _getSelectMenuByLevel
 */ 
 
 public function _getSelectMenuByLevel()
{
 $this->db->reset_select();
 $this->db->select("a.menu");
 $this->db->from("t_gn_agent_profile a ");
 $this->db->where("a.id", _get_post("UserLevelId"));
 
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 ){
	$menu_id = (array)explode(",", $qry->result_singgle_value() );
	return $this->_get_system_menu($menu_id);
	
 } else {
	return array();
 }
} 
 
/*
 * @ pack  : _getSelectMenuDetail 
 */ 
 
 public function _getSelectMenuDetail()
{
 $this->db->reset_select();
 $this->db->select('*');
 $this->db->from('t_gn_application_menu');
 $this->db->where('id', _get_post('ActiveMenuId'));
 $qry = $this->db->get(); 
 if( $qry ->num_rows() > 0 ){
	return $qry->result_first_assoc();
 } else {
	return array();
 }
 

	
} // _getSelectMenuDetail ==============================> 


/*
 * @ pack  : _getSelectMenuDetail 
 */ 
 

public function _getActiveMenu()
{
  static $config = array();
  
  $this->db->reset_select();	
  $this->db->select("
		a.ActiveMenuName as id, 
		a.ActiveMenuName as name,
		a.ActiveMenuTitle as title ", FALSE);
		
  $this->db->from("t_gn_active_menu a");
  $this->db->where("ActiveUserLevel", _get_session('HandlingType') );
  $qry = $this->db->get();
  if( $qry->num_rows() > 0 ){
	$num = 0;
	foreach( $qry->result_assoc() as $rows ) {
		$config[$num] = $rows;
		$num++;
	}	
  }
  
  return $config;

} // _getActiveMenu ==============================> 



// END CLASS 
 
}



?>