<?php
/*
 * @ pack : under model M_ModulRPC
 */ 
 
class M_RefAddPhoneType extends EUI_Model {

/*
 * @ pack : under model M_InfoPTP
 */ 
 
var $_table_space = array();

/*
 * @ pack : under model M_InfoPTP
 */ 
 
public function M_RefAddPhoneType()
{
  $this->_table_space = array('t_lk_additional_phone_type');
}

//id, code, name, max_phone, description

/*
 * @ pack : select all 
 */
 
 public function _get_labels_adp()
{
	$_adp_label = array(
		'adp_code' => 'Code',
		'adp_name' => 'Name',
		'adp_description' => 'Description',
		'adp_max_phone' => 'Max Phone',
		'adp_flags' => 'Status'
	);
	
	return $_adp_label;
}


/*
 * @ pack : select all 
 */
 
 public function _get_select_adp()
{

  $_adp_label = array();
  $this->db->reset_select();	
  $this->db->select('*',FALSE);
  $this->db->from(reset($this->_table_space));
  $this->db->where('adp_flags', 1);
 
  $qry = $this->db->get();
  if( $qry->num_rows() > 0 ) 
   foreach( $qry ->result_assoc() as $rows ) 
  {
	$_adp_label[$rows['adp_code']] = $rows['adp_name'];
  }
  
 return $_adp_label;
   
}

/*
 * @ pack : get slect phone number by Deb ID 
 */
 
 public function _get_detail_add( $DebiturId = 0 )
{
 $select_phone = array();
 
// @ pack : select its
  
  $this->db->reset_select();
  $this->db->select("a.addphone_no, b.adp_name", FALSE);
  $this->db->from(" t_gn_additional_phone a ");
  $this->db->join(reset($this->_table_space) . " b "," a.addphone_type=b.adp_code","LEFT");
  $this->db->where("a.deb_id",$DebiturId);
  $this->db->where_in("a.addphone_approve_status",array('101') );
  $this->db->order_by("a.addphone_id","DESC");
  
  $qry = $this->db->get();
  if($qry->num_rows()>0 )
   foreach( $qry->result_assoc() as $rows )
 {
	$select_phone[$rows['addphone_no']] = (string)"$rows[adp_name] - ". _getMasking($rows[addphone_no]);
  }
  
  return $select_phone;
 
}

/*
 * @ pack : select all 
 */
 public function _get_detail_adp( $argv_vars = null )
{
  $conds = array();	
  
  $this->db->reset_select();
  $this->db->select('*',FALSE);
  $this->db->from(reset($this->_table_space));
  $this->db->where('id', $argv_vars);
  $qry = $this->db->get();
  if( $qry->num_rows()>0 )
  {
	$conds = $qry->result_first_assoc();
  }
  
  return $conds;
	
}


/*
 * @ pack : select all 
 */
 
 public function _get_field_adp()
{
	return $this->db->list_fields(reset($this->_table_space));
}

/*
 * @ pack : select all 
 */
 
 public function _get_component_adp() 
{
	return array ( 
		'primary' => array('id'),
		'combo' => array('adp_flags'),
		'input' => array('adp_code', 'adp_name', 'adp_description','adp_max_phone'),
		'option' => array('1'=>'Active','0'=>'Not Active')
	);
}

/*
 * @ pack : _set_save_spc
 */
 
public function _set_save_adp( $arg_vars = null )
{
  $conds = 0;	
  if( is_array($arg_vars) )
  {
	foreach( $arg_vars as $field => $value ) {
		$this->db->set($field, $value);
	}
	
	// @ pack : set save data 
	$this->db->insert(reset($this->_table_space));
	if( $this->db->affected_rows() > 0 ) {
		$conds++;
	}
  }
 
 return $conds;
  
} 


/*
 * @ pack : _set_save_spc
 */
 
 public function _set_update_adp( $argv_vars = null )
{

 $conds = 0;
 
 // @ pack : reset all select query ..
 
 $this->db->reset_select(); 
 
 // @ pack : cek argument set vars ..
  
 if( is_array($argv_vars) )	
	foreach( $argv_vars as $field => $value )
 {
	if(in_array($field,array('id') ) ) {
		$this->db->where($field, $value);
	} else {
		$this->db->set($field, $value);
	}
  }
  
  // @ pack : execute query from here ..
   
  $this->db->update(reset($this->_table_space));
  if($this->db->affected_rows()>0){
	$conds++;
  }
 
  return $conds;
 
}

/*
 * @ pack : _set_save_spc
 */
 
public function _set_delete_adp( $adpId = NULL )
{
  $conds = 0;
  if( is_array($adpId) ) 
	foreach( $adpId as $PrimaryId => $value )
  {
	  $this->db->where('id', $value);	
      $this->db->delete(reset($this->_table_space));
	  
	  if( $this->db->affected_rows() > 0 )
	  {
		 $conds++;
	  }
  }
  
  return $conds;
}

/*
 * @ pack : _get_default
 */
 
 public function _get_default_adp()
{
  $this->EUI_Page->_setPage(10); 
  $this->EUI_Page->_setSelect("*");
  $this->EUI_Page->_setFrom(reset($this->_table_space), TRUE); 
  
  // @ pack : set filter --------------------
	
   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd(" 
			id LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_code LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_max_phone LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_name LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_description LIKE '%{$this->URI->_get_post('keywords')}%'");
  }	
	
 // @ pack : process its  
 
  if( $this->EUI_Page->_get_query() ) {
		return $this->EUI_Page;
  }
}

/*
 * @ pack : _get_default
 */
 
 public function _get_content_adp()
{
  $this->EUI_Page->_postPage($this->URI->_get_post('v_page') );
  $this->EUI_Page->_setPage(10);
  
// @ pack : set filter --------------------
	  
  $this->EUI_Page->_setArraySelect(array(
		"id AS AdpId" => array("AdpId","ID","primary"),
		"id AS ID" => array("ID","ID"),
		"adp_code AS AdpCode" => array("AdpCode","Code"),
		"adp_name AS AdpName" => array("AdpName","Name"),
		"adp_description as AdpDescription" => array("AdpDescription","Description"),
		"adp_max_phone as AdpLimit" => array("AdpLimit","Max Phone"),
		"IF(adp_flags=1,'Active','Not Active') AS AdpFlags" => array("AdpFlags","Status")
   ));
  
   $this->EUI_Page->_setFrom(reset( $this->_table_space), TRUE);
   
 // @ pack : set filter --------------------
	
   if( $this->URI->_get_have_post('keywords')){
	   $this->EUI_Page->_setAnd(" 
			id LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_code LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_max_phone LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_name LIKE '%{$this->URI->_get_post('keywords')}%'  
			OR adp_description LIKE '%{$this->URI->_get_post('keywords')}%'");
  }	
	
  
  if( $this->URI->_get_have_post('order_by')) {
	$this->EUI_Page->_setOrderBy($this ->URI->_get_post('order_by'),$this ->URI->_get_post('type'));
  }
  
  
  $this -> EUI_Page->_setLimit();
}


/*
 * @ pack : _get_default
 */
 
public function _get_resource_adp()
 {
	self::_get_content_adp();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 

/*
 * @ pack : _get_default
 */
public function _get_page_number_adp() 
 {
	if( $this->EUI_Page->_get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }

// END CLASS 
 
}



?>