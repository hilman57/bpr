<?php
/*
 * @ pack : M_ModCallHistory
 */
 
class M_ModOldCallHistory extends EUI_Model {


/*
 * @ pack : M_ModCallHistory
 */
 
private static $Instance  = null;


/*
 * @ pack : M_ModCallHistory
 */
public static function &Instance()
{
  if(is_null(self::$Instance)) 
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
} 
/*
 * @ pack : M_ModCallHistory
 */
 
 public function M_ModOldCallHistory()
{

} 

/*
 * @ pack : get call history by customer id
 */
 
 public function _get_old_call_history_customer( $CustomerId = 0 )
{
	$this->db->reset_select();
	
	$this->db->select("
		a.CallHistoryCreatedTs, 
		a.CallNumber,
		a.CallHistoryNotes,
		d.spc_name as SPCName,
		d.spc_description as SPCDesc,
		e.rpc_code as RPCName,
		e.rpc_description as RPCDesc,
		b.deb_name as CustomerName, 
		c.id as UserName, 
		c.code_user as UserCode, 
		c.init_name as UserOnline, 
		c.full_name as UserFullname,
		f.CallReasonDesc as AccountStatusName,
		g.CallReasonDesc as CallStatusName", 
	FALSE);
	
	$this->db->from("t_gn_callhistory a ");
	$this->db->join("t_gn_debitur_deleted b "," a.CustomerId=b.deb_id","LEFT");
	$this->db->join("t_tx_agent c "," a.CreatedById=c.UserId","LEFT");
	$this->db->join("t_lk_speach_with d "," a.CallHistorySpc = d.spc_code","LEFT");
	$this->db->join("t_lk_remote_place e "," a.CallHistoryRpc = e.rpc_code","LEFT");
	$this->db->join("t_lk_account_status f "," a.CallAccountStatus=f.CallReasonCode","LEFT");
	$this->db->join("t_lk_account_status g "," a.CallReasonId=g.CallReasonCode","LEFT");
    $this->db->where("b.deb_acct_no",$CustomerId);
	$this->db->order_by("a.CallHistoryCreatedTs", "DESC");
	
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 ){
		return $qry->result_assoc();
	} else { 
		return FALSE;
	}
}

//END CLASS  

}
?>