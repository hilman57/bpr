<?php
class M_PhoneType extends EUI_Model
{


// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */

private static $meta = null; 
private static $Instance  = null;

// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */
 
 
public static function &Instance()
{
	if( is_null(self::$Instance) ){
		self::$Instance = new self();
	}
	return self::$Instance;
}


// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */

function __construct()
{
	if( is_null(self::$meta) ) {
		self::$meta = 't_lk_additional_phone_type'; 	
	}
}


// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */

function _getHideData()
{
	$rowshide = array();
	
	$this -> db -> select('a.table_field_name');
	$this -> db -> from('t_gn_hide_tables a ');
	$this -> db -> where('a.table_name', 't_gn_bucket_customers');
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$rowshide[$rows['table_field_name']] = $rows['table_field_name'];
	}
	
	return $rowshide;
}

// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */

 public function _getRelationship()
{
 
 $conds = array();
 $this->db->reset_select();
 $this->db->select(" a.RelationshipTypeCode, a.RelationshipTypeName, a.RelationshipTypeDesc", 
	FALSE);
	
 $this->db->from("t_lk_relationshiptype a");
 $this->db->where("a.RelationshipTypeFlags", 1);
 foreach( $this->db->get() ->result_assoc() as $rows ) {
	$conds[$rows['RelationshipTypeCode']] = $rows['RelationshipTypeDesc'];
 }
 
 return $conds;
 
}
	

// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */
 
 
 public function _getPhoneTypeList() 
{

 $phone = array();
 $this->db->reset_select();
 $this->db->select('*', FALSE);
 $this->db->from(self::$meta);
 $this->db->where('adp_flags',1);
 foreach( $this -> db -> get() -> result_assoc() as $rows ) 
 {
	$phone[$rows['adp_code']] = $rows['adp_description'];
  }
  
 return $phone;
}

// ----------------------------------------------------------------------------
/*
 * @ packaage 		: _get_page_number // constructor class 
 * 
 */
 
 public function _getMultiplePhone( $CustomerId = 0 )
{
	$arr_list_data = array();
	
	$this->db->reset_select();
	$this->db->select("
		a.addphone_type, 
		a.addphone_family_code, 
		a.addphone_no ", 
	FALSE);
	$this->db->from("t_gn_additional_phone a");
	$this->db->where("a.deb_id", $CustomerId);
	$this->db->where_not_in("a.addphone_approve_status", array('101', '102'));
	$rs = $this->db->get();
	if( $rs->num_rows() > 0 ) 
		foreach( $rs->result_assoc() as $row )
	{
		$arr_list_data[$row['addphone_type']] = array(
			'rel_phone' => $row['addphone_family_code'], 
			'num_phone' => $row['addphone_no'] 
		);
	}
	return $arr_list_data;
 }
 

// -----------------------------------------------------

	
	

}

?>