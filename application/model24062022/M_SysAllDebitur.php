<?php 
 class M_SysAllDebitur extends EUI_Model 
{
	
// ----------------------------------------------------------
/*
 * @ package 	@Instance
 */
 
private static $Instance = null;
	
	
// ----------------------------------------------------------
/*
 * @ package 	@construct
 */
 
 
function __construct(){ }

// ----------------------------------------------------------
/*
 * @ package 	&Instance
 */
 
 
 public static function &Instance()
{
	if( is_null( self::$Instance ) ){
		self::$Instance = new self();	
	}	
	return self::$Instance;
}

// ----------------------------------------------------------
/*
 * @ package 	&Instance
 */
 
 function _DebiturContent( $out  = null ) 
{
  
 $this->array_result = array();
 $this->db->reset_select();
 $this->db->select(" 
		b.AssignId as AssignId,
		a.deb_id as DebiturId, 
		a.deb_acct_no as AccountNo, 
		a.deb_name as CustomerName, 
		a.deb_amount_wo as AmountWO, 
		upper(a.deb_agent) as AgentId,
		c.full_name as Fullname, 
		d.CallReasonDesc as AccountStatus, 
		a.deb_call_activity_datets  as CallDate,
		e.CampaignCode as Product,
		ts.CallReasonDesc as LastCallStatus,
		a.deb_bal_afterpay as BalAfterPay,
		a.deb_id as HistoryId,
		(SELECT tg.UserId  FROM t_tx_agent tg 
		 WHERE tg.id=a.deb_agent) as DebAgentId", 
 FALSE); 
 
 
 $this->db->from("t_gn_debitur a");
 $this->db->join("t_gn_assignment b "," a.deb_id=b.CustomerId","LEFT");
 $this->db->join("t_tx_agent c "," b.AssignSelerId=c.UserId","LEFT");
 $this->db->join("t_lk_account_status d", "a.deb_call_status_code=d.CallReasonCode","LEFT");
 $this->db->join("t_gn_campaign e", "a.deb_cmpaign_id=e.CampaignId","LEFT");
 $this->db->join("t_gn_campaign_project f ","f.CampaignId=e.CampaignId","LEFT");
 $this->db->join("t_lk_account_status ts ","a.deb_prev_call_status_code=ts.CallReasonCode","LEFT");
 
 
// ----------- filter by Account  -----------
if(_get_have_post('deb_acct_no') )  {
	$this->db->like("a.deb_acct_no", $out->get_value('deb_acct_no'));
}	

if(_get_have_post('deb_campaignid') )  {
	$this->db->where("a.deb_cmpaign_id", $out->get_value('deb_campaignid'));
}

if(_get_have_post('deb_name') )  {
	$this->db->where("a.deb_name", $out->get_value('deb_name'));
}


if(_get_have_post('orderby') )  {
	$this->db->order_by($out->get_value('orderby'), $out->get_value('type'));
} else {
	$this->db->order_by("a.deb_id", "ASC");
}
//echo $this->db->print_out();
 
$qry = $this->db->get();
if( $qry->num_rows() > 0 ) {
	$this->array_result  = $qry->result_assoc();
 }
 
 return $this->array_result;		
} 


	
// ========== END CLASS MODEL  ==================	
}
?>