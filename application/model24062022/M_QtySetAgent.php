<?php

/*
 * @ def : default read of controller  
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */
 
class M_QtySetAgent extends EUI_Model
{

 var $perpage = 10;

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
function M_QtySetAgent(){
	
	
}


/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
 
function _getWasAssignAgent()
{
    $UserAssign = null;
  
	$this -> db -> select("a.Agent_User_Id");
	$this -> db -> from("t_gn_quality_agent a");
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$UserAssign[$rows['Agent_User_Id']] = $rows['Agent_User_Id']; 
	}
	
	return $UserAssign;
	
}	

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
function _getRecordsAgent()
{
	$counter  = 0;
	$simples = $this -> _getWasAssignAgent();
	
/** cek valid is array **/

	$UserAvail = FALSE;
	if(is_array($simples)){
		$UserAvail = array_keys($simples);
	}
	
	$this -> db-> select("count(a.UserId) as Jumlah", FALSE);
	$this -> db-> from("t_tx_agent a");
	$this -> db-> join("t_gn_agent_profile b ", "a.profile_id=b.id","LEFT");
	$this -> db-> join("t_gn_user_project_work c ","a.UserId=c.UserId", "LEFT");
	$this -> db-> where_in("b.id", array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) );
	$this -> db-> where("a.user_state",1);
	
	$this -> db-> where_in("c.ProjectId", $this->EUI_Session->_get_session('ProjectId'));
	
	if( $UserAvail !=FALSE){
		$this -> db -> where_not_in('a.UserId',$UserAvail);
	}
	
	if( $rows = $this -> db -> get()->result_first_assoc() )
	{
		$counter = $rows['Jumlah'];
	}
	
	return $counter;
}


/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
function _getRecordsQuality()
{
	$QualityReady = 0;
	
	$this->db->select("COUNT(a.Id) as Qty ", FALSE);
		
	$this->db->from("t_gn_quality_agent a");
	$this->db->join("t_tx_agent b", "a.Agent_User_Id=b.UserId","LEFT");
	$this->db->join("t_tx_agent c", "c.UserId=b.spv_id","LEFT");
	$this->db->join("t_tx_agent d", "d.UserId=a.Quality_Staff_Id","LEFT");
	$this->db->join("t_gn_user_project_work e ","c.UserId=e.UserId", "LEFT");
	
	// cek data available 
	
	if( $this -> URI->_get_have_post('QulityId') 
		AND $this -> URI->_get_post('QulityId') !='' )
	{
		$this -> db ->where("a.Quality_Staff_Id",$this -> URI->_get_post('QulityId'));	
	}
	
	
	if( $this -> URI -> _get_have_post('Hide') 
		AND $this -> URI -> _get_post('Hide')==1)
	{
		$this -> db ->where("a.Quality_Staff_Id IS NULL");
	}
	
	if( $this -> URI -> _get_have_post('TsrName')) {
		$this -> db ->like("b.full_name",$this -> URI -> _get_post('TsrName'));
	}
	
	$this->db->where_in("e.ProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->order_by('b.full_name','ASC');
	
	
	if( $rows = $this -> db -> get() -> result_first_assoc() ) {
		$QualityReady = (INT)$rows['Qty'];
	}
	
	return $QualityReady;
}

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
function _getPageAgent()
{
	$pager = array();
	$agent = $this -> _getRecordsAgent();
	$page = ceil($agent/$this -> perpage);
	for($i = 1; $i<=$page; $i++) {
		$pager[$i] = $i;
	}
	
	return $pager;
}



/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
function _getPageQualityReady()
{
	$pager = array();
	$agent = $this -> _getRecordsQuality();
	$page = ceil($agent/$this -> perpage);
	for($i = 1; $i<=$page; $i++) {
		$pager[$i] = $i;
	}
	
	return $pager;
}
 
/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	
 
 
function _getAgentState()
{
	$AgentStates = array();
	$simples = $this -> _getWasAssignAgent();
	
	// cek valid is array 
	$UserAvail = false;
	if( is_array($simples) ){
		$UserAvail = array_keys($simples);
	}
	
	$this -> db -> select("a.*");
	$this -> db -> from("t_tx_agent a");
	$this -> db -> join("t_gn_agent_profile b ", "a.profile_id=b.id","LEFT");
	$this -> db -> where_in("b.id", array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) );
	$this -> db -> where("a.user_state",1);
	$this -> db -> order_by('a.full_name', 'ASC');
	// cek available data
		
	if( $UserAvail !=FALSE){
		$this -> db -> where_not_in('a.UserId',$UserAvail);
	}
	
	$start = 0;
	if( $this -> URI -> _get_have_post('page') )
	{
		$_page = (INT)$this -> URI -> _get_post('page'); 
		if( $_page > 0 )
			$start = ( (($_page) -1) * $this -> perpage);
		else
			$start = 0;
	}
	
	$this -> db -> limit($this -> perpage,$start);
	$num = $start+1;
	foreach( $this -> db -> get() -> result_assoc() as $rows ) 
	{
		$AgentStates[$num] = $rows;
		$num++;
	}
	
	return $AgentStates;
}

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	

function _getQualityReady()
{
	$QualityReady = array();
	
	$this -> db ->select("
		a.Id as GroupQualityId, c.full_name as SpvName, c.id as SpvId,  b.* , 
		d.full_name as QualityName, d.id as QualityCode, d.UserId as QualityId", FALSE);
		
	$this -> db ->from("t_gn_quality_agent a");
	$this -> db ->join("t_tx_agent b", "a.Agent_User_Id=b.UserId","LEFT");
	$this -> db ->join("t_tx_agent c", "c.UserId=b.spv_id","LEFT");
	$this -> db ->join("t_tx_agent d", "d.UserId=a.Quality_Staff_Id","LEFT");
	
	// cek data available 
	if( $this -> URI->_get_have_post('QulityId') 
		AND $this -> URI->_get_post('QulityId') !='' )
	{
		$this -> db ->where("a.Quality_Staff_Id",$this -> URI->_get_post('QulityId'));	
	}
	
	
	if( $this -> URI -> _get_have_post('Hide') 
		AND $this -> URI -> _get_post('Hide')==1)
	{
		$this -> db ->where("a.Quality_Staff_Id IS NULL");
	}
	
	if( $this -> URI -> _get_have_post('TsrName'))
	{
		$this -> db ->like("b.full_name",$this -> URI -> _get_post('TsrName'));
	}
	
	$start = 0;
	if( $this -> URI -> _get_have_post('page') )
	{
		$_page = (INT)$this -> URI -> _get_post('page'); 
		if( $_page > 0 )
			$start = ( (($_page) -1) * $this -> perpage);
		else
			$start = 0;
	}
	
	$this -> db -> limit($this -> perpage,$start);
	$num = $start+1;
	
	$this -> db ->order_by('b.id','ASC');
	foreach( $this -> db -> get() -> result_assoc() as $rows )
	{
		$QualityReady[$num] = $rows;
		$num++;
	}
	
	return $QualityReady;
}

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	

function _setAddAvailableAgent($UserId = null )
{
	$conds = 0;
	if( !is_null($UserId))
	{
		foreach( $UserId as $key => $UsersId )
		{
			$this -> db -> set('Agent_User_Id',$UsersId);
			$this -> db -> set('Create_Group_Ts',date('Y-m-d H:i:s'));
			$this -> db -> set('Create_Group_By', $this ->EUI_Session -> _get_session('UserId'));
			$this -> db -> insert('t_gn_quality_agent');
			
			if($this -> db ->affected_rows() < 1 ) 
			{
				$this -> db -> set('Create_Group_Ts',date('Y-m-d H:i:s'));
				$this -> db -> set('Create_Group_By', $this ->EUI_Session -> _get_session('UserId'));
				$this -> db -> where('Agent_User_Id',$UsersId);
				$this -> db -> update('t_gn_quality_agent');
			}
			$conds++;
		}
	}
	
	return $conds;
}

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	

function _setUpdateQualityAgent($QualityAgentId = null, $QualityStaffId = null )
{
	$conds = 0;
	if( !is_null($QualityAgentId) AND !is_null($QualityStaffId) )
	{
		foreach( $QualityAgentId as $key => $Id )
		{
			$this -> db -> where('Id',$Id);
			$this -> db -> set('Quality_Staff_Id',$QualityStaffId);
			$this -> db -> set('Create_Group_Ts',date('Y-m-d H:i:s'));
			$this -> db -> set('Create_Group_By', $this ->EUI_Session -> _get_session('UserId'));
			if( $this -> db -> Update('t_gn_quality_agent') ){
				$conds++;
			}	
		}
	}
	
	return $conds;
}

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	

function _setRemoveQualityAgent($QualityAgentId = null)
{
	$conds = 0;
	if( !is_null($QualityAgentId))
	{
		foreach( $QualityAgentId as $key => $Id )
		{
			$this -> db -> where('Id',$Id);
			$this -> db -> set('Create_Group_Ts',date('Y-m-d H:i:s'));
			$this -> db -> set('Create_Group_By', $this ->EUI_Session -> _get_session('UserId'));
			if( $this -> db -> delete('t_gn_quality_agent') ){
				$conds++;
			}	
		}
	}
	
	return $conds;
}

/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	

function _getQualitStaffScore()
{
	$Quality = array();
	
	$this -> db ->select("a.Quality_Staff_id");
	$this -> db ->from("t_gn_quality_group a");
	$this -> db ->where("a.Quality_Skill_Id",QUALITY_SCORES);
	foreach( $this -> db -> get() -> result_assoc() as $rows ){
		$Quality[$rows['Quality_Staff_id']]= $rows['Quality_Staff_id'];  
	}
	
	return $Quality;
}


/*
 * @ def : default super class   
 * ------------------------------------------
 *
 * @ params : Unit test notes
 * @ params : 
 */	

function _setEmptyQualityAgent($QualityAgentId = null)
{
	$conds = 0;
	if( !is_null($QualityAgentId))
	{
		foreach( $QualityAgentId as $key => $Id )
		{
			$this -> db -> where('Id',$Id);
			$this -> db -> set('Quality_Staff_Id','NULL',FALSE);
			
			if( $this -> db -> update('t_gn_quality_agent') ){
				$conds++;
			}	
		}
	}
	
	return $conds;
}

// 




}

?>