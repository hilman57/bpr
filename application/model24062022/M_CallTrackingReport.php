<?php

class M_CallTrackingReport extends EUI_Model
{

/* ------------------# aksesor of model class #------------------------------ */ 

  function M_CallTrackingReport() {
	 $this -> load -> model(array('M_SetCampaign','M_SysUser'));
	 
  }
  
  
 // GroupFilter
/* ------------------------------------------------- */ 

function GroupFilter()
{
	return array(
		'filter_by_resource' => 'Group By Recsource',
		'filter_by_campaign' => 'Group By Campaign',
		//'filter_by_atm' => 'Group By ATM',
		'filter_by_spv' => 'Group By SPV',
		'filter_by_agent' => 'Group Caller',
		//'filter_by_atm_group_campaign' => 'ATM Group By Campaign',
		'filter_by_spv_group_campaign' => 'SPV Group By Campaign',
		'filter_by_agent_group_campaign' => 'Caller Group By Campaign',
		'filter_campaign_group_agent' => 'Campaign Group By Caller'
	);
} 

/* getAgent **/
/* ------------------------------------------------- */ 

function getAgent()
{
  $_conds = array();	
	if( class_exists('M_SetCampaign')) 
	{
		$_conds  = $this -> M_SetCampaign -> _getCampaignGoals(2);
	}	
	
  return $_conds;
}


/* @ param : _getCampaignReady **/
/* ------------------------------------------------- */ 

function _getCampaignReady() 
{
  $_conds = array();	
  if( class_exists('M_SetCampaign'))
  {
	$_conds  = $this -> M_SetCampaign -> _getCampaignGoals(2);
  }	
	
  return $_conds;	
}

/* @ param : ShowSpvByAtm **/
/* ------------------------------------------------- */ 

public function _getAgentByAtm( $AtmId= 0 ) 
{
  $_conds = array();
  $param = array('spv_id' => $AtmId, 'user_state' =>1);
  
  if( $rows = $this -> M_SysUser -> _get_teleamarketer($param) ) 
  {
	$_conds = $rows;
  }	
	
	return $_conds;
 }
 
/* @ param : ShowSpvByAtm **/
/* ------------------------------------------------- */ 

public function _getSpvByAtm( $AtmId= 0  )
{
 $_conds = array();
 $this->db->select('a.UserId, a.full_name as SpvName ');
 $this->db->from('t_tx_agent a');
 $this->db->where('a.spv_id',$AtmId);
 $this->db->where_in('a.handling_type', array(USER_LEADER));
 $this->db->not_like("a.full_name","BUCK");
 $this->db->order_by('a.full_name','ASC');
 
 foreach( $this->db-> get() -> result_assoc() as $rows ) 
 {
	$_conds[$rows['UserId']] = $rows['SpvName'];
  }
 return $_conds;	
} 


/* @ param : ShowAgentBySpv **/
/* ------------------------------------------------- */ 

public function _getAgentBySpv( $SpvId= 0  )
{
  $_conds = array();
  $this->db->select('a.UserId, a.full_name as AgentName ');
  $this->db->from('t_tx_agent a');
  $this->db->where('a.spv_id',$SpvId);
  $this->db->where_in('a.handling_type', array(USER_AGENT_OUTBOUND,USER_AGENT_INBOUND));
  $this->db->not_like("a.full_name","BUCK");
  $this->db->order_by('a.full_name','ASC');
  
  foreach( $this->db-> get() -> result_assoc() as $rows )
  {
	$_conds[$rows['UserId']] = $rows['AgentName'];
  }
  
  return $_conds;	
 
} 

 
/* @ param : ShowSpvByAtm **/
/* --*/

function Mode() {
	return array
	(
		'summary' => 'Summary'
	);
}
 
 /* @ param : ShowSpvByAtm **/
 /* -------------------------------------------------------- */
 
function _getCategory()
{
	$conds = array();
	
	$sql = "select a.CallReasonCategoryId, a.CallReasonCategoryName from t_lk_customer_status a
			where a.CallReasonCategoryFlags = 1";
	$qry = $this->db->query($sql);
	
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$conds[$rows['CallReasonCategoryId']] = $rows['CallReasonCategoryName'];
		}
	}
	
	return $conds;
}

/**
 * @ def : _getRecsource
 * -----------------------------------------------------------
 
 * @ param : - 
 */
 
 
public function _getRecsource()
{
	$recsource = array();
	
	$this->db->select("a.FTP_UploadId, a.FTP_UploadFilename");
	$this->db->from("t_gn_upload_report_ftp a ");
	$this->db->join("t_gn_bucket_customers b "," a.FTP_UploadId = b.BukcetSourceId","LEFT");
	$this->db->join("t_lk_ftp_read c ","b.BuketUploadId=c.ftp_read_id","LEFT");
	$this->db->join("t_lk_work_project d "," c.ftp_read_project_code=d.ProjectCode", "LEFT");
	$this->db->where_in("d.ProjectId", $this->EUI_Session->_get_session('ProjectId'));
	$this->db->group_by("a.FTP_UploadId");
	
	foreach( $this->db->get()->result_assoc() as $rows ){
		$recsource[$rows['FTP_UploadId']] = basename($rows['FTP_UploadFilename']);
	}
	
	return $recsource;
}


/**
 * @ def : _getRecsource
 * -----------------------------------------------------------
 
 * @ param : - 
 */
 
 
function _getUserATM() {
	//if( $this -> )
}

/* @ param : ShowSpvByAtm **/
/* -------------------------------------------------------- */

}
?>