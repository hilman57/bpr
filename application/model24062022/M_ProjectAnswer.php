<?php
class M_ProjectAnswer extends EUI_Model {

var $ProjectId = array();

/**
 *
 * @ def : constructor 
 * 
 */
 
 public function M_ProjectAnswer()
{
	if( $this->EUI_Session->_get_session('UserId') ) 
	{
		$this->ProjectId = $this->EUI_Session->_get_session('ProjectId');
	}
}


/**
 *
 * @ def : _getAnswerQuestion 
 * ------------------------------------------------------
 * 
 */
 
 
public function _getAnswerQuestion()
{
	
    $arrs_answers = array();
	// $this->db->select('a.AnswerCode, a.AnswerValue, b.AnswerDesc');
	$this->db->select('a.AnswerCode, a.AnswerValue, b.AnswerNote, AnswerDesc');
	$this->db->from('t_gn_project_answers a');
	$this->db->join('t_lk_answer_question b ','a.AnswerValue=b.AnswerId','LEFT');
	$this->db->where_in('a.ProjectId', $this->ProjectId );
	// echo 
	$i = 1;
	
	foreach( $this->db->get()->result_assoc() as $rows ){
		$arrs_answers[$rows['AnswerCode']][$rows['AnswerValue']] = $rows['AnswerValue'];
		$i++;
	}
	
	return $arrs_answers;
	
} 

/**
 *
 * @ def : _getAnswerQuestion 
 * ------------------------------------------------------
 * 
 */
 

public function _getAnswerKey($CustomerId = 0 ,$PolicyId=0 )
{
	$field = array ( 'CustField_Q1','CustField_Q2','CustField_Q3','CustField_Q4','CustField_Q5', 
					 'CustField_Q6','CustField_Q7','CustField_Q8','CustField_Q9','CustField_Q10');
	$this->db->select(implode(",",$field));
	$this->db->from('t_gn_followup_question');
	$this->db->where('CustomerId', $CustomerId);
	$this->db->where('PolicyId',$PolicyId);
	
	return $this->db->get()->result_first_assoc();
	
}
			
}
?>