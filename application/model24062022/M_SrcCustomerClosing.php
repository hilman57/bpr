<?php
/*
 * E.U.I 
 *
 * ---------------------------------------------------------------------------- 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 * 
 */
 
class M_SrcCustomerClosing extends EUI_Model
{

var $SurveyStatus = NULL;

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function M_SrcCustomerClosing() 
{
	$this -> load -> model('M_SetCallResult');
	$this -> load -> model('M_MaskingNumber');
	
	// set object pointer data on static 
	
	if( class_exists('M_SetCallResult') 
		AND is_null($this->SurveyStatus)) 
	{
		$this->SurveyStatus = & $this->M_SetCallResult->_getCallReasonEvent();
	}	
	
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Sale()
{
	$_a = array(); $_b = array();
	if( class_exists('M_SetCallResult'))
	{
		$_a = $this -> M_SetCallResult -> _getInterestSale(); 
		foreach( $_a as $_k => $_v )
		{
			$_b[$_k] = $_k;  
		}	
	}
	
	return $_b;
} 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _get_product_category()
 {
	$datas = array();
	
	$sql = "SELECT 
				distinct(k.PolicyProductCategory) as PolicyProductName
			FROM t_gn_debitur a 
			INNER JOIN t_gn_assignment b ON a.CustomerId=b.CustomerId 
			LEFT JOIN t_gn_campaign d ON a.CampaignId=d.CampaignId 
			LEFT JOIN t_lk_account_status f ON a.CallReasonId = f.CallReasonId 
			LEFT JOIN t_gn_campaign_project g ON a.CampaignId=g.CampaignId 
			LEFT JOIN t_gn_policy_detail k ON a.CustomerNumber=k.EnigmaId 
			WHERE 1=1 
			AND b.AssignAdmin IS NOT NULL 
			AND b.AssignMgr IS NOT NULL 
			AND b.AssignSpv IS NOT NULL 
			AND b.AssignBlock='0' 
			AND d.CampaignStatusFlag='1' 
			AND g.ProjectId IN(".IMPLODE(',',$this->EUI_Session->_get_session('ProjectId')).") 
			AND a.CallComplete='1' ";
	
	
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$sql .= " AND b.AssignSelerId = '".$this->EUI_Session->_get_session('UserId')."' ";
	}
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$sql .= " AND b.AssignSelerId = '".$this->EUI_Session->_get_session('UserId')."' ";
	}		
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$sql .= " AND b.AssignSpv = '".$this->EUI_Session->_get_session('UserId')."' ";
	}
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$sql .= " AND b.AssignMgr = '".$this->EUI_Session->_get_session('UserId')."' ";
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER) {
		$sql .= " AND b.AssignAmgr = '".$this->EUI_Session->_get_session('UserId')."' ";
	}		
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_ADMIN){
		$sql .= " AND b.AssignAmgr = '".$this->EUI_Session->_get_session('UserId')."' ";
	}
	
	
	$qry = $this->db->query($sql);
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$datas[$rows['PolicyProductName']] = $rows['PolicyProductName'];
		}
	}
	
	return $datas;
 }
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this->EUI_Page->_setSelect("a.CustomerId");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.CustomerId=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.CampaignId=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.CallReasonId = f.CallReasonId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.CampaignId=g.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_policy_detail k ","a.CustomerNumber=k.EnigmaId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent l ","b.AssignSelerId=l.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_followup_question m ","(a.CustomerId=m.CustomerId AND k.PolicyId = m.PolicyId)","LEFT");
	$this->EUI_Page->_setJoin("t_lk_aprove_status o ","a.CallReasonQue=o.ApproveId","LEFT", TRUE);
	
/**	filtering key of the session  **/

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignBlock', '0');
	$this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	
	$this->EUI_Page->_setAnd(" ( a.CallReasonQue IS NULL OR a.CallReasonQue IN(9))");
	
	/*
	 * 
	 * handle callresult on survey Only dan ini berlaku hanya login agent 
	 * All QA ( PDR / W CALL ) 
	 *
	 */
    
	// if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ) {
		// $this->EUI_Page->_setWhereIn('a.CallReasonId', array_keys($this->SurveyStatus));
	// }
	/*** filtering ***/
	
	$this->EUI_Page->_setWherein('g.ProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->EUI_Page->_setAnd('a.CallComplete','1');

	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_SUPERVISOR ){
		$this->EUI_Page->_setAnd('b.AssignSpv', $this->EUI_Session->_get_session('UserId'));
	}		
				 
	if( $this->EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$this->EUI_Page->_setAnd('b.AssignMgr', $this->EUI_Session->_get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER) {
		$this->EUI_Page->_setAnd('b.AssignAmgr', $this->EUI_Session->_get_session('UserId'));
	}		
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_ADMIN){
		$this->EUI_Page->_setAnd('b.AssignAmgr', $this->EUI_Session->_get_session('UserId'));
	}		
	
	/* START FILTERING */
	
	$this->EUI_Page->_setLikeCache('a.CustomerFirstName', 'cls_cust_name', TRUE);
	$this->EUI_Page->_setLikeCache('a.CIFNumber', 'cls_cif_number', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyNumber', 'cls_policy_number', TRUE);
	$this->EUI_Page->_setAndCache('a.CallReasonQue', 'cls_qa_status', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyProductCategory', 'cls_product_name', TRUE);
	$this->EUI_Page->_setAndCache('a.CallAttempt', 'cls_attempt_count', TRUE);
	$this->EUI_Page->_setAndCache('a.CallReasonId', 'cls_call_reason_id', TRUE);
	$this->EUI_Page->_setAndCache('a.CampaignId', 'cls_campaign', TRUE);
	$this->EUI_Page->_setAndCache('b.AssignSelerId', 'cls_caller_name', TRUE);
	
	/* END OF START FILTERING */

	$this->EUI_Page->_setGroupBy('a.CustomerNumber');
	
	if($this->EUI_Page->_get_query()) 
	{
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/
	
	// $this->EUI_Page->_setSelect("
				// a.CustomerFirstName, a.CustomerDOB, a.CIFNumber, 
				// d.CampaignName, a.CustomerCity, a.CustomerUpdatedTs,
				// IF(f.CallReasonDesc is null,'New',f.CallReasonDesc) as CallResult, f.CallReasonCategoryId, q.CallReasonCategoryName, k.PolicyProductCategory, k.PolicyProductName AS PolicyProductName,
				// k.PolicyIssDate, k.PolicyUploadDate, a.CallAttempt, k.PolicyNumber, l.*, m.*, a.CustomerId , o.AproveName ");
	$this->EUI_Page->_setArraySelect(array(
		"a.CustomerId AS chk_cust_call"=> array('chk_cust_call','ID','primary'),
		"a.CustomerFirstName AS CustomerFirstName"=> array('CustomerFirstName','Owner Name'),
		"k.PolicyNumber AS PolicyNumber"=> array('PolicyNumber','Policy Number'),
		"k.PolicyProductName AS PolicyProductName" => array('PolicyProductName','Product Name'),
		"k.PolicyIssDate AS PolicyIssDate" => array('PolicyIssDate','Issued Date'),
		"k.PolicyUploadDate AS PolicyUploadDate" => array('PolicyUploadDate','Upload Date'),
		"q.CallReasonCategoryName AS CallReasonCategoryName" => array('CallReasonCategoryName','Call Status'),
		"IF(f.CallReasonDesc is null,'New',f.CallReasonDesc) as CallResult" => array('CallResult','Call Result'),
		"a.CustomerUpdatedTs AS CallDate" => array('CallDate','Call Date'),
		"l.full_name AS full_name" => array('full_name','Caller'),
		"a.CallAttempt AS CallAttempt" => array('CallAttempt','Attempt'),
		"o.AproveName AS AproveName" => array('AproveName','QA Status')
	));
		
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.CustomerId=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.CampaignId=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.CallReasonId = f.CallReasonId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_customer_status q "," f.CallReasonCategoryId = q.CallReasonCategoryId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.CampaignId=g.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_policy_detail k ","a.CustomerNumber=k.EnigmaId","LEFT");
	$this->EUI_Page->_setJoin("t_tx_agent l ","b.AssignSelerId=l.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_followup_question m ","(a.CustomerId=m.CustomerId AND k.PolicyId = m.PolicyId)","LEFT");
	$this->EUI_Page->_setJoin("t_lk_aprove_status o ","a.CallReasonQue=o.ApproveId","LEFT", TRUE);
/** process **/
	
	 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignBlock', 0);
 	 $this->EUI_Page->_setAnd('d.CampaignStatusFlag','1');
	 
	/** filtering  **/
	
	$this->EUI_Page->_setAnd(" ( a.CallReasonQue IS NULL OR a.CallReasonQue IN(9))");
	$this->EUI_Page->_setWherein('g.ProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->EUI_Page->_setAnd('a.CallComplete','1');

	/** 
	 * handle callresult on survey Only dan ini berlaku hanya login agent 
	 * All QA ( PDR / W CALL ) 
	**/
	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}

	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}			 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ){
		$this->EUI_Page->_setAnd('b.AssignSpv',$this->EUI_Session->_get_session('UserId'));
	}		
				 
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_MANAGER ){
		$this->EUI_Page->_setAnd('b.AssignAdmin',$this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER ){
		$this->EUI_Page->_setAnd('b.AssignAmgr', $this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ADMIN ){
		$this->EUI_Page->_setAnd('b.AssignMgr', $this -> EUI_Session -> _get_session('UserId'));
	}
	
	/* START FILTERING */
	
	$this->EUI_Page->_setLikeCache('a.CustomerFirstName', 'cls_cust_name', TRUE);
	$this->EUI_Page->_setLikeCache('a.CIFNumber', 'cls_cif_number', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyNumber', 'cls_policy_number', TRUE);
	$this->EUI_Page->_setAndCache('a.CallReasonQue', 'cls_qa_status', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyProductCategory', 'cls_product_name', TRUE);
	$this->EUI_Page->_setAndCache('a.CallAttempt', 'cls_attempt_count', TRUE);
	$this->EUI_Page->_setAndCache('a.CallReasonId', 'cls_call_reason_id', TRUE);
	$this->EUI_Page->_setAndCache('a.CampaignId', 'cls_campaign', TRUE);
	$this->EUI_Page->_setAndCache('b.AssignSelerId', 'cls_caller_name', TRUE);
	
	/* END OF START FILTERING */
	
	$this->EUI_Page->_setGroupBy('a.CustomerNumber');
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}
	$this->EUI_Page->_setLimit();
	
	
}



/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 public function _getDetailCustomer($CustomerId=null)
 {
	$_conds = array();
	
	$this -> db -> select('a.*, b.Gender, c.Salutation, d.CampaignName, d.CampaignNumber, e.CallReasonDesc, e.CallReasonCategoryId ', FALSE);
	$this -> db -> from('t_gn_debitur a');
	$this -> db -> join('t_lk_gender b','a.GenderId=b.GenderId','LEFT');
	$this -> db -> join('t_lk_salutation c','a.SalutationId=c.SalutationId','LEFT');
	$this -> db -> join('t_gn_campaign d','a.CampaignId=d.CampaignId','LEFT');
	$this -> db -> join('t_lk_account_status e','a.CallReasonId=e.CallReasonId','LEFT');
	$this -> db -> where('a.CustomerId',$CustomerId);
	// echo $this -> db ->_get_var_dump();
	
	foreach($this -> db ->get() -> result_assoc() as $rows )
	{
		foreach($rows as $k => $v ){
			$_conds[$k] = $v;
		}
	}
	
	return $_conds;
 }
 
  
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getPhoneCustomer($CustomerId=null)
 {
	$_conds = array();
	
	$this ->db ->select('CustomerHomePhoneNum, CustomerMobilePhoneNum, CustomerWorkPhoneNum, CustomerWorkFaxNum, CustomerWorkExtPhoneNum, CustomerFaxNum');
	$this ->db ->from('t_gn_debitur');
	$this ->db ->where('CustomerId',$CustomerId);
	
	foreach($this -> db ->get() -> result_assoc() as $rows )
	{
		if( !is_null($rows['CustomerHomePhoneNum']) 
			AND $rows['CustomerHomePhoneNum']!='' )
		{
			$_conds[$rows['CustomerHomePhoneNum']] = " Home ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerHomePhoneNum']); 
		}
		
		if( !is_null($rows['CustomerMobilePhoneNum']) 
			AND $rows['CustomerMobilePhoneNum']!='' )
		{
			$_conds[$rows['CustomerMobilePhoneNum']] = " Mobile ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerMobilePhoneNum']); 
		}
		
		if( !is_null($rows['CustomerWorkPhoneNum']) 
			AND $rows['CustomerWorkPhoneNum']!='' )
		{
			$_conds[$rows['CustomerWorkPhoneNum']] = " Office ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerWorkPhoneNum']); 
		}
		
		if( !is_null($rows['CustomerWorkFaxNum']) 
			AND $rows['CustomerWorkFaxNum']!='' )
		{
			$_conds[$rows['CustomerWorkFaxNum']]= " Work Fax ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerWorkFaxNum']); 
		}
		
		//
		if( !is_null($rows['CustomerWorkExtPhoneNum']) 
			AND $rows['CustomerWorkExtPhoneNum']!='' )
		{
			$_conds[$rows['CustomerWorkExtPhoneNum']] = " Work Ext ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerWorkExtPhoneNum']); 
		}
		
		
		if( !is_null($rows['CustomerFaxNum']) 
			AND $rows['CustomerFaxNum']!='' )
		{
			$_conds[$rows['CustomerFaxNum']]= " Fax ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerFaxNum']); 
		}
	}
	
	return $_conds;
 }
 
 

 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getApprovalPhoneItems($CustomerId = 0 )
 {
	$_conds = array();
	
	$this ->db ->select('a.ApprovalNewValue, b.ApprovalItem');
	$this ->db ->from('t_gn_approvalhistory a');
	$this ->db ->join('t_lk_approvalitem b','a.ApprovalItemId=b.ApprovalItemId','LEFT');
	$this ->db ->where('a.CustomerId',$CustomerId);
	
	foreach($this ->db -> get()->result_assoc() as $rows )
	{
		$_avail = explode(' ', $rows['ApprovalItem']);
		if( count($_avail) > 0 ){
			$_conds[$rows['ApprovalNewValue']] = $_avail[0] .' '. $this->M_MaskingNumber->MaskingBelakang($rows['ApprovalNewValue']); 	
		}
	}
	
	return $_conds;
 } 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getPolicyAutogen( $CustomerId =null  )
{
	$Product = array();
	
	$this->db->select('a.ProductId');
	$this->db->from('t_gn_policyautogen a');
	$this->db->where('a.CustomerId',$CustomerId);
	foreach( $this->db->get() -> result_assoc()  as $rows ) {
		$Product[$rows['ProductId']] = $rows['ProductId'];
	}
	
	return $Product;
	
} 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAvailProduct( $CustomerId = 0 )
 {
	$_product = array(); 
	$ProductList = $this -> _getPolicyAutogen($CustomerId);
	
	
	$this ->db->select('d.ProductId, d.ProductName');
	$this ->db->from('t_gn_debitur a ');
	$this ->db->join('t_gn_campaign b ',' a.CampaignId=b.CampaignId');
	$this ->db->join('t_gn_campaignproduct c ',' b.CampaignId=c.CampaignId');
	$this ->db->join('t_gn_product d ',' c.ProductId=d.ProductId');
	$this ->db->where('a.CustomerId',$CustomerId);
	
	foreach( $this ->db-> get() -> result_assoc() as $rows )
	{
		if( in_array($rows['ProductId'], $ProductList) ){
			$_product[$rows['ProductId']] = $rows['ProductName'];
		}	
	}
	
	return $_product;
 }
 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function _getLastCallPhone($CustomerId = 0 )
 {
	$CallPhone = null;
	
	$this -> db -> select('a.CallNumber');
	$this -> db -> from("t_gn_callhistory a");
	$this -> db -> where("a.CustomerId", $CustomerId);
	$this -> db -> where("a.ApprovalStatusId IS NULL");
	$this -> db -> where("a.CreatedById", $this -> EUI_Session->_get_session('UserId'));
	$this -> db -> order_by('a.CallHistoryId', 'DESC');
	$this -> db -> limit(1);

	if( $rows  = $this -> db->get()->result_first_assoc() )
	{
		$CallPhone = $rows['CallNumber'];
	}
	
	return $CallPhone;
	
 }
 
 function _getQaStatus()
 {
	$datas = array();
	
	$sql = "select a.ApproveId, a.AproveName from t_lk_aprove_status a where a.AproveFlags = 1";
	// echo $sql;
	$qry = $this->db->query($sql);
	
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$datas[$rows['ApproveId']] = $rows['AproveName'];
		}
	}
	
	return $datas;
 }
 
/** ViewQuestion ***/
public function getViewQuestion( $CustomerId = 0 )
{
  static $arrs  = array();
  
  $this->db->select("
	a.PolicyId, b.PolicyNumber,
	a.CustField_Q1, a.CustField_Q2, 
	a.CustField_Q3, a.CustField_Q4, 
	a.CustField_Q5, a.CustField_Q6, 
	a.CustField_Q7, a.CustField_Q8,
	a.CustField_Q9, a.CustField_Q10", 
	FALSE);
	
	$this->db->from("t_gn_followup_question a ");
	$this->db->join("t_gn_policy_detail b "," a.PolicyId=b.PolicyId","LEFT");
	$this->db->where("a.CustomerId", $CustomerId);
	$this->db->group_by("a.PolicyId");
	foreach( $this->db->get() ->result_assoc() as $rows )
	{
		foreach( $rows as $field => $values )
		{
			if( !in_array($field, array('PolicyNumber','PolicyId') ) )
			{
				$arrs[$rows['PolicyNumber']][$field]= $values; 
			}
		}
	}
	
	return $arrs;

}
 

}

?>