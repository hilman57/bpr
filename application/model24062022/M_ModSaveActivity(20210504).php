<?php

/*
 * @ pack   : M_ModSaveActivity
 * @ change : 2015-02-25  
 */
 
 class M_ModSaveActivity extends EUI_Model
{

/*
 * @ pack : constructor class 
 */
 
 var $argv_vars = array();

/*
 * @ pack : constructor class 
 */
 
 public function __construct()
{
  parent::__construct();
  $this->load->model(array('M_SetCallResult','M_Loger','M_ValidAddPhone','M_ModPVCReport'));
  if( is_array($this->URI->_get_all_request()))
  {
	$this->argv_vars = (array)$this->URI->_get_all_request();
  }
}
/*
 * @ pack : save notes activity 
 */
 
public function _SaveValidPhone()
{
	$Valid = FALSE;
	$Status		= _get_post('status_valid');
	$AddPhone	= _get_post('AddPhone');
	$Jenis		= preg_match("/^08/", $this->argv_vars['CallNumber']);
	$Duplicate	= $this -> duplicate($this->argv_vars['CallNumber'], $this->argv_vars['DebiturId']);
	$Data		= $this -> getData($this->argv_vars['DebiturId']);
	
	if( empty($Data) AND $Status == 1 AND $AddPhone != '' AND $Jenis == true ) {
		$this->db->set("DebiturId", $this->argv_vars['DebiturId']);	
		$this->db->set("PhoneNumber", $this->argv_vars['CallNumber']);
		$this->db->set("AgentID", _get_session('UserId'));
		$this->db->set("Status", null);
		$this->db->set("ValidDate", date('Y-m-d H:i:s'));
		$this->db->insert('t_gn_validphone');
	} elseif( $Status == 1 AND $AddPhone != '' AND $Jenis == true ) {
		$this->db->set("PhoneNumber", $this->argv_vars['CallNumber']);
		$this->db->where("DebiturId", $this->argv_vars['DebiturId']);
		$this->db->update("t_gn_validphone");
	}
	// echo $this -> db -> last_query();
	return $Valid;
}

function duplicate($hp, $deb_id)
{
	$Filter = array();
	$this -> db -> select("a.*");
	$this -> db -> from("t_gn_validphone a");
	$this -> db -> where("a.PhoneNumber",$hp);
	$this -> db -> where("a.DebiturId",$deb_id);
	// echo $this -> db -> _get_var_dump();
	foreach($this -> db -> get() -> result_assoc() as $rows)
	{
		$Filter[$rows['ValidID']] = $rows['PhoneNumber'];
	}
	return $Filter;
}

function getData($deb_id)
{
	$Filter = array();
	$this -> db -> select("a.*");
	$this -> db -> from("t_gn_validphone a");
	$this -> db -> where("a.DebiturId",$deb_id);
	// echo $this -> db -> _get_var_dump();
	foreach($this -> db -> get() -> result_assoc() as $rows)
	{
		$Filter[$rows['ValidID']] = $rows['PhoneNumber'];
	}
	return $Filter;
}
 
 public function _SaveNotesActivity()
{
	$conds = 0;
	$Addphone = $this->argv_vars['select_add_phone'];
	if( is_array($this->argv_vars) AND $DebiturId = $this->argv_vars['DebiturId'] )
	{
		$this->db->set("deb_reminder", $this->argv_vars['notes_reminder']);
		$this->db->where("deb_id",$DebiturId);
		if( $this->db->update("t_gn_debitur")) 
		{
			if($this->db->affected_rows() > 0) {
				// if(!empty($Addphone)) {
					// $this->db->set("DebiturId", $this->argv_vars['DebiturId']);
					// $this->db->insert('t_gn_validphone');
					$conds++;
					// echo $this->db->last_query();
				// }
			}
		}
	}

	return $conds;
	// echo $this->db->last_query();
}

/*
 * @ pack : Save Activity here 
 */
 
 public function _SaveCallHistoryNote(){
     
      $CallHistoryId = FALSE;	
  $CustomerNo = $this->CustomerNo($this->argv_vars['DebiturId']);
    
    if( (is_array($this->argv_vars)) AND ($CallHistoryId==FALSE))    
    {
        	
			$this->db->set("CustomerId", $this->argv_vars['DebiturId']);	
			$this->db->set("CallHistoryNotes", strtoupper($this->argv_vars['text_activity_notes']));
			$this->db->set("CreatedById", _get_session('UserId'));
			//$this->db->set("AgentCode", _get_session('Username'));
			//$this->db->set("TeamLeaderId", _get_session('TeamLeaderId'));
			//$this->db->set("SupervisorId", _get_session('SupervisorId'));
				$this->db->set("SupervisorId", _get_session('UserId'));
			$this->db->set("CallHistoryCallDate", date('Y-m-d H:i:s'));
			$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s')); 
				$this->db->insert('t_gn_callhistory');
				//echo $this->db->last_query();
			if( $this->db->affected_rows() > 0 )
			{
				$CallHistoryId = $this->db->insert_id();
			}
    }
    return $CallHistoryId;
 }
 
 
 public function _SaveActivity()
{
 $_conds = FALSE;
 
 if( is_array($this->argv_vars) )
 {
	$_conds = $this->_SaveCallHistory();
	if( $_conds ) 
	{
		// var_dump(!isset($this->argv_vars['cb_ptp_reminder']));
		if(isset($this->argv_vars['cb_ptp_reminder']))
		{
			if(empty($this->argv_vars['cb_ptp_reminder']))
			{
				if(!empty($this->argv_vars['text_payment']) &&  $this->argv_vars['text_payment'] != "0" )
				{
					$this->_SavePaymentPTP( $_conds );
					// $this->_SaveValidPhone();
					// if($this->argv_vars['text_payment']>0){
						$this->argv_vars['select_account_status_code']='106';
					// }
				}
			}
		}
		$this->_SaveValidPhone();
		$this->_SaveCallBackLater();	
		$this->_UpdateDebitur(); 
	}	
	
 }
 
 return $_conds;
 
}

/* 
 * @ payment PTP 
 */
 
 public function _SavePaymentPTP( $CallHistoryId = 0 )
{
 $PTP_LastId = FALSE;
 if( (is_array($this->argv_vars)) )
 {
   $ptp_account = $this->CustomerNo($this->argv_vars['DebiturId']);	 // add by oments 
   $ptp_status = (array)$this->M_SetCallResult->_getInterestSale();
   $account_status = $this->argv_vars['select_account_status_code'];
   
   /*
    * yang ini memang sengaja di remark ya ? kok validation di buang 
	* tolong di cek lagi .
	*/
	
	
   // if( in_array( $account_status, array_keys($ptp_status)) )
   // {
	 
	 
	 $this->db->set("ptp_account",$ptp_account);
	 $this->db->set("deb_id",$this->argv_vars['DebiturId']);
	 $this->db->set("ptp_date",date('Y-m-d', strtotime($this->argv_vars['text_ptp_date'])));
	 $this->db->set("ptp_amount",$this->argv_vars['text_payment']);
	 $this->db->set("ptp_last_status",$this->argv_vars['select_account_status_code']);
	 $this->db->set("ptp_discount",$this->argv_vars['text_discount']);
	 $this->db->set("ptp_chanel",$this->argv_vars['select_channel_payment']);
	 $this->db->set("ptp_tenor",$this->argv_vars['select_tenor']);	
	 $this->db->set("ptp_type", $this->argv_vars['select_info_ptp']);
	 $this->db->set("ptp_create_ts",date('Y-m-d H:i:s'));
	 $this->db->set("ptp_callhistory_id", $CallHistoryId);
	 
  // @ pack : insert to ptp 
	
	 $this->db->insert("t_tx_ptp");
	 if($this->db->affected_rows()> 0)
	 {
		$PTP_LastId = $this->db->insert_id();
	 }
   // }
   
 }

 // @ pack : call back result PTP
 
 return $PTP_LastId;
 
}


//------------- get CustomerNo @ ad by omen 20160417-------
 
 function CustomerNo( $CustomerId = 0 )
{
	$account_no = 0;
	
	$sql = "SELECT a.deb_acct_no 
			FROM t_gn_debitur a 
			WHERE a.deb_id ='$CustomerId'";
			
	$qry = $this->db->query($sql);
	if( $qry->num_rows() > 0 ) { 
		$row = $qry->result_first_assoc();
		if( is_array($row) ){
			$account_no = $row['deb_acct_no'];
		}
	}	
	
	return $account_no;
 }
 
/*
 * @ pack : save call history 
 */ 
 
 public function _SaveCallHistory()
{
  $CallHistoryId = FALSE;	
  $CustomerNo = $this->CustomerNo($this->argv_vars['DebiturId']);
  $this->M_ModPVCReport->setpvcvcStatus($this->argv_vars);
  
  if( (is_array($this->argv_vars)) AND ($CallHistoryId==FALSE))
  {
	$this->db->set("AccountNo", $CustomerNo);  
	$this->db->set("CallSessionId",$this->argv_vars['CallSessionId']);
	$this->db->set("CallNumber", $this->argv_vars['CallNumber']);
	$this->db->set("CustomerId", $this->argv_vars['DebiturId']);
	$this->db->set("CallReasonId",$this->argv_vars['select_prev_call_status_code']);
	$this->db->set("CallAccountStatus", $this->argv_vars['select_account_status_code']);
	$this->db->set("CallHistorySpc",$this->argv_vars['select_spc']); 
	$this->db->set("CallHistoryRpc",$this->argv_vars['select_rpc']);
	$this->db->set("CallHistoryNotes", strtoupper($this->argv_vars['text_activity_notes']));
	$this->db->set("CreatedById", _get_session('UserId'));
	$this->db->set("AgentCode", _get_session('Username'));
	$this->db->set("TeamLeaderId", _get_session('TeamLeaderId'));
	$this->db->set("SupervisorId", _get_session('SupervisorId'));
	$this->db->set("CallHistoryCallDate", date('Y-m-d H:i:s'));
	$this->db->set("CallHistoryCreatedTs",date('Y-m-d H:i:s')); 
	
	// $this->db->set("PhoneType" ,$this->argv_vars['DebiturId']);
	// $this->db->set("ApprovalStatusId", $this->argv_vars['DebiturId']);
	
	// @ pack : insert to history then reseult last insert ID 
	
	$this->db->insert('t_gn_callhistory');
	if( $this->db->affected_rows() > 0 )
	{
		$CallHistoryId = $this->db->insert_id();
		
		// $ModPVCReport =& M_ModPVCReport::Instance();
		// $ModPVCReport->setpvcvcStatus($argv['DebiturId']);
		
		
		//var_dump($this->argv_vars['CallSessionId']);
		// if(!empty($this->argv_vars['CallSessionId']))
		// {
			 $this->db->set('deb_attempt',1);
		// }
		$this->db->set("Last_CallhistoryId",$CallHistoryId);
		$this->db->set("deb_id", $this->argv_vars['DebiturId']);
		$this->db->set("deb_acct_no", $CustomerNo);
		$this->db->insert('call_history_min');
		if( $this->db->affected_rows() < 0 )
		{
			// if(!empty($this->argv_vars['CallSessionId']))
			// {
				 $this->db->set('deb_attempt','deb_attempt+1',false);
			// }
			$this->db->set("Last_CallhistoryId",$CallHistoryId);
			$this->db->where("deb_id", $this->argv_vars['DebiturId']);
			$this->db->update('call_history_min');
			// echo $this->db->last_query();
		}
	}
 }
 
 return $CallHistoryId;
	
}

/*
 * @ pack : upodate t_gn_debitur with status its.
 *
 */
 public function _UpdateDebitur() 
{
 
 $_conds = 0;
  if( (is_array($this->argv_vars)) 
	AND ($_conds==FALSE)) 
  {
   
   // @ pack : where 
   
	 $this->db->where('deb_id', $this->argv_vars['DebiturId']);
	 
   // @ pack : parameter	
	
	 $this->db->set('deb_spc', $this->argv_vars['select_spc']);
	 $this->db->set('deb_rpc', $this->argv_vars['select_rpc']);
	 $this->db->set('deb_update_by_user', _get_session('UserId'));
	 
	 if( in_array( _get_session('HandlingType'), array(USER_AGENT_OUTBOUND)) ){
		$this->db->set('deb_agent', _get_session('Username')); 
		// tidak merubah apapun selama tidak dari uploadan;
		// kalau yang followup agent maka deb_agent - nya di update 
		// gimna pak.
     } 
	 
	 $this->db->set('deb_call_status_code', $this->argv_vars['select_account_status_code']);
	 $this->db->set('deb_prev_call_status_code', $this->argv_vars['select_prev_call_status_code']);
	 
	 $this->db->set('deb_call_activity_datets',date('Y-m-d H:i:s'));
	 
  // @ pack : execute here.. (^_^); 
  
	if( $this->db->update('t_gn_debitur') ) {
		$_conds++;
	 } 
  }
  
  return $_conds;
  
}

/*
 * @ pack : upodate t_gn_debitur with status its.
 *
 */
 
 public function _SaveCallBackLater() 
{

 $CallBackId = 0;
 $_call_back_date = NULL;
 
 if( ($CallBackId==FALSE) AND (is_array($this->argv_vars))  )
 {
	if( strlen( $this->argv_vars['text_call_back_date'] ) > 0 
		AND  strlen( $this->argv_vars['text_call_hour'] ) )
	{
		$_call_back_date = _getDateEnglish($this->argv_vars['text_call_back_date'])." ".$this->argv_vars['text_call_hour'].":".$this->argv_vars['text_call_minute'].":00"; 
		// echo $_call_back_date;
		if(!is_null($_call_back_date)){				   
			$_call_back_date = (string)$_call_back_date;
			if( $_call_back_date )
			{	
				$this->db->set('CustomerId',$this->argv_vars['DebiturId']);
				$this->db->set('UserId',_get_session('UserId'));
				$this->db->set('ApoinmentDate',$_call_back_date);
				$this->db->set('ApoinmentCreate',date('Y-m-d H:i:s'));
				$this->db->insert('t_gn_appoinment');
				
				if( $this->db->affected_rows() > 0 ){
					 $CallBackId = $this->db->insert_id();
				}	
			}
		}	
	}
  }
  
  return $CallBackId;
  
 }
 
/*
 * @ pack : _SaveSmsActivity # -----------------------
 */ 
 public function _SaveSmsActivity()
{
  $this->db->set('MasterId',_get_post('CustomerId') );
  $this->db->set('SmsTemplateId', _get_post('sms_tempalte_id') );
  $this->db->set('SmsDestination',_get_post('SmsDestination'));
  $this->db->set('SmsCreateUserId',_get_session('UserId'));
  $this->db->set('SmsLeaderId',_get_session('TeamLeaderId'));
  $this->db->set('SmsSupervisorId',_get_session('SupervisorId'));	
  $this->db->set('SmsTextMessage',_get_post('sms_message_text'));
  $this->db->set('SmsLocation',_get_session('LoginIP'));
  $this->db->set('SmsCreateTs',date('Y-m-d H:i:s'));
  
  
  $this->db->insert('t_gn_sms_approval');
  if( $this->db->affected_rows() > 0 ){
		return true;
  } else {
		return false;
  }
  
}

/*
 * @ pack : save blocking phone number 
 */
 
 public function _SaveBlocking()
{
 
 $Loger =& M_Loger::Instance();
 $argv  =& $this->URI->_get_all_request();
 
 $conds = 0;
 if( is_array($argv))
 {
	$text_argv = null;
	if(preg_match('/-/i', $argv['PhoneText'])) {
		$text_argv = explode('-', $argv['PhoneText']);
		$text_argv = ( isset($text_argv[0] )? trim($text_argv[0]) : null ); 
	}
	
 // @ pack : cek is null 
 
	if(!is_null( $text_argv ) )
	{
		$this->db->set("Blocking_CustomerId",$argv['CustomerId']);
		$this->db->set("Blocking_phone_no",$argv['PhoneNumber']);
		$this->db->set("Blocking_phone_type",$text_argv);
		$this->db->set("Blocking_remarks",$argv['MessageText']);
		$this->db->set("Blocking_date",date('Y-m-d H:i:s'));
		$this->db->set("Blocking_UserId",_get_session('UserId') );
		$this->db->insert("t_tx_blocking");
		if( $this->db->affected_rows() > 0 )
		{
			 $Loger->set_activity_log("BLOCKING PHONE 
				-> PHONE[{$argv['PhoneNumber']}]
				-> USER[{$this->EUI_Session->_get_session('UserId')}]
				-> CUSTOMER[{$argv['CustomerId']}]");
			$conds++;
		} else {
			 $Loger->set_activity_log("BLOCKING PHONE 
				-> PHONE[{$argv['PhoneNumber']}]
				-> USER[{$this->EUI_Session->_get_session('UserId')}]
				-> CUSTOMER[{$argv['CustomerId']}]");
				
		// @ pack : update if duplicate 
 		
			$this->db->set("Blocking_phone_type",$text_argv);
			$this->db->set("Blocking_remarks",$argv['MessageText']);
			$this->db->set("Blocking_date",date('Y-m-d H:i:s'));
			$this->db->set("Blocking_UserId",_get_session('UserId') );
			$this->db->where('Blocking_phone_no', $argv['PhoneNumber']);
			
			$this->db->update('t_tx_blocking');
			$conds++;
		}
	}
 }
 
 return $conds;
 
 }
 
 

/*
 * @ pack : _SaveClaimActivity # -----------------------
 */ 
 public function _SaveClaimActivity()
{
	$Loger =& M_Loger::Instance();
	$conds=false;
	
	$this->db->set("claim_flag","1");
	$this->db->where("bucket_trx_id",$this->argv_vars['BucketRandomId']);
	$this->db->update("t_gn_buckettrx_debitur");
	if($this->db->affected_rows()>0)
	{
		$this->db->set( 'bucket_trx_id',$this->argv_vars['BucketRandomId'] );
		$this->db->set( 'from_owner',$this->get_Owner_debitur() ); 
		$this->db->set( 'claim_by',$this->EUI_Session->_get_session('UserId') );
		$this->db->set( 'claim_date_ts',date('Y-m-d H:i:s') );
		$this->db->set( 'approval_status','104' );
		if( $this->db->insert('t_gn_claim_debitur') )
		{
			$Loger->set_activity_log("CLAIM DEBITUR FROM RANDOM MODUL 
				-> USER[{$this->EUI_Session->_get_session('UserId')}]
				-> CUSTOMER[{$this->argv_vars['DebiturId']}]
				-> BUCKET RANDOM[{$this->argv_vars['BucketRandomId']}]");
			$conds=true;
		}
	}
	return $conds;
}

private function get_Owner_debitur()
{
	$data = null;
	
	$this->db->reset_select();
	$this->db->select("*");
	$this->db->from("t_gn_assignment a");
	$this->db->where("a.CustomerId", $this->argv_vars['DebiturId']);
		
	 
	$qry = $this->db->get();
	if( $qry->num_rows() > 0 )
	{
		if( $rows = $qry->result_first_assoc() )
		{
			$data = $rows['AssignSelerId'];
		}
	}
	return $data;
}

 private function _get_keys()
{
	$result = preg_replace("/[!^a-zA-Z']+s/", "", mysql_error());
	if( $result ){
		return explode("-", $result);
	} else {
		return null;
	}
}

	/*
	 * @ pack : save prospect note
	 */
	 
	public function _SaveProspectNote()
	{
	 
		$argv  =& $this->URI->_get_all_request();

		$conds = 0;
		if( is_array($argv))
		{
			$this->db->set("debitur_id",$argv['CustomerId']);
			$this->db->set("note",$argv['text_prospect_notes']);
			$this->db->set("create_date",date('Y-m-d H:i:s'));
			$this->db->set("user_id",_get_session('UserId') );
			$this->db->insert("t_gn_prospect_note");
			if( $this->db->affected_rows() > 0 )
			{
				$conds++;
			}

		}
		return $conds;
	}
	
	 public function _SaveComplainNote()
	 {
		$argv  =& $this->URI->_get_all_request();
		$debitur = $this->M_ModContactDetail->_get_select_debitur($argv['DebiturId']);
		$conds = 0;
		if( is_array($argv) && is_array($debitur) )
		{
			$this->db->set("debitur_id",$argv['DebiturId']);
			$this->db->set("call_status_code",$debitur['deb_prev_call_status_code']);
			$this->db->set("account_status_code",$debitur['deb_call_status_code']);
			$this->db->set("userid",_get_session('UserId') );
			$this->db->set("id_source_complain",$argv['source_complain'] );
			$this->db->set("id_kasus_komplen",$argv['jenis_kasus'] );
			$this->db->set("action_taken",$argv['text_action_taken'] );
			$this->db->set("result",$argv['text_result'] );
			$this->db->set("PhoneBlock",$argv['PhoneBlock'] );
			$this->db->set("create_date",date('Y-m-d H:i:s') );
			$this->db->insert("t_gn_complain_review");
			if( $this->db->affected_rows() > 0 )
			{
				$conds++;
			}
			
		}
		 
		return $conds;
	 }
	 
	 public function _save_vc_reason()
	 {
		$argv  =& $this->URI->_get_all_request();
		$debitur = $this->M_ModContactDetail->_get_select_debitur($argv['DebiturId']);
		$conds = 0;
		if( is_array($argv) && is_array($debitur) )
		{
			$this->db->set("debitur_id",$argv['DebiturId']);
			$this->db->set("call_status_code",$debitur['deb_prev_call_status_code']);
			$this->db->set("account_status_code",$debitur['deb_call_status_code']);
			$this->db->set("userid",_get_session('UserId') );
			$this->db->set("vc_reason_id",$argv['vcreason'] );
			$this->db->set("wo_date",$debitur['deb_wo_date'] );
			$this->db->set("wo_bal",$debitur['deb_wo_amount'] );
			$this->db->set("result",$argv['text_result'] );
			$this->db->set("PhoneBlock",$argv['PhoneBlock'] );
			$this->db->set("create_date",date('Y-m-d H:i:s') );
			$this->db->insert("t_gn_vc_review");
			if( $this->db->affected_rows() > 0 )
			{
				$conds++;
			}
			
		}
		 
		return $conds;
	 }
 
// END CLASS 

}

?>
