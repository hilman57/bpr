<?php
class M_BatchResetApproval extends EUI_Model
{
/*
 * @ pack : instance 
 */
 
private static $Instance = null;
 
/*
 * @ pack : instance 
 */
 
 public static function &Instance()
{
 
 if(is_null(self::$Instance) ){
	self::$Instance = new self();
 }
 return self::$Instance;
 
}
 
// -----------------------------------------------------
/*
 * @ pack : instance of class 
 */
 // -----------------------------------------------------

 public function _SetResetApproval()
{

 $this->db->where("create_date <= NOW() - INTERVAL 30 DAY","", FALSE);
 if( $this->db->delete("t_tx_cust_approval") ){
	return TRUE;
 } else {
	return FALSE;
 }
 
}


// END CLASS 
 
}

?>