<?php
class M_MenuWorkProject extends EUI_Model
{

public function get_default()
{
 $this -> EUI_Page -> _setPage(20); 
	
 $this->EUI_Page->_setSelect('a.Id as WorkMenuId');
 
 $this->EUI_Page->_setFrom("t_gn_menu_project a");
 $this->EUI_Page->_setJoin("t_lk_work_project b "," a.ProjectId=b.ProjectId","LEFT"); 
 $this->EUI_Page->_setJoin("t_gn_application_menu c "," a.MenuId=c.id","LEFT");
 $this->EUI_Page->_setJoin("t_gn_group_menu d "," c.group_menu=d.GroupId","LEFT", TRUE);
		
 /** set if have filter data ***/
 
 if(($this->URI->_get_have_post('keyword'))) 
 { 
	$this->EUI_Page->_setAnd("( b.ProjectName LIKE '%".$this->URI->_get_post('keyword')."%'  
		OR d.GroupName LIKE '%".$this->URI->_get_post(keyword)."%'  
		OR c.menu LIKE '%".$this->URI->_get_post('keyword')."%' 
		OR a.CreateDateTs LIKE '".$this->URI->_get_post('keyword')."') ", FALSE);
	}
	
 
 return $this -> EUI_Page;
	
 }
 
 /**
  * @ def : get default result of content 
  * ---------------------------------------------------------
  * 
  */
  
 public function get_content()
 {
	$this->EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this->EUI_Page->_setPage(20);
	
	 $this->EUI_Page->_setArraySelect(array(
			'a.Id as WorkMenuId' => array('WorkMenuId','ID'), 
			'b.ProjectName as ProjectName' => array('ProjectName','Project Name'), 
			'd.GroupName as GroupName'  => array('GroupName','Menu Group'), 
			'c.menu as menu' => array('menu','Menu Name'), 
			'a.CreateDateTs as CreateDateTs' => array('CreateDateTs','Create Date'), 
			'IF( a.MenuActive=1, "Active","Not Active") as MenuActive' => array('MenuActive','Active') )
		);	
		
	$this->EUI_Page->_setFrom("t_gn_menu_project a");
	$this->EUI_Page->_setJoin("t_lk_work_project b "," a.ProjectId=b.ProjectId","LEFT"); 
	$this->EUI_Page->_setJoin("t_gn_application_menu c "," a.MenuId=c.id","LEFT");
	$this->EUI_Page->_setJoin("t_gn_group_menu d "," c.group_menu=d.GroupId","LEFT", TRUE);
		
  /** set if have filter data ***/

	 
	if(($this->URI->_get_have_post('keyword'))) { 
		$this->EUI_Page->_setAnd("( b.ProjectName LIKE '%".$this->URI->_get_post('keyword')."%'  
			OR d.GroupName LIKE '%".$this->URI->_get_post(keyword)."%'  
			OR c.menu LIKE '%".$this->URI->_get_post('keyword')."%' 
			OR a.CreateDateTs LIKE '".$this->URI->_get_post('keyword')."') ", FALSE);
	}
	
	if( $this->URI->_get_have_post('order_by')) 
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	
	$this->EUI_Page->_setLimit();
	// echo "<pre>". $this ->EUI_Page->_getCompiler()."</pre>";	
} 	


/*
 *@ get buffering query from database
 *@ then return by object type ( resource(link) ); 
 */
 
function get_resource_query()
 {
	self::get_content();
	
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 *@ get number record & start of number every page 
 *@ then result ( INT ) type 
 */
 
function get_page_number()
  {
	if( $this -> EUI_Page -> _get_query()!='' )
	{
		return $this -> EUI_Page -> _getNo();
	}	
  }
  
 
/**
 * @ def : add menu layout user navigation HHH 
 *
 */
 
public function _MenuApplication()
{
	static $config =array();
	
	$this->db->select('a.id, a.menu');
	$this->db->from('t_gn_application_menu a ');
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$config[$rows['id']] = $rows['menu'];
	}
	
	return $config;
}	

/**
 * @ def : add menu layout user navigation HHH 
 * ---------------------------------------------------------
 */
  
  
public function _WorkProjectId()
{
 
  static $config =array();
	
	$this->db->select('a.ProjectId, a.ProjectName');
	$this->db->from('t_lk_work_project a ');
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$config[$rows['ProjectId']] = $rows['ProjectName'];
	}
	
	return $config;
}

/**
 * @ def : add menu layout user navigation HHH 
 * ---------------------------------------------------------
 */
  
public function _WorkMenuApplication( $ProjectId = null )
{
	static $config =array();
	
	$this->db->select('*');
	
	$this->db->from('t_gn_menu_project a ');
	$this->db->join('t_gn_application_menu b ', ' a.MenuId=b.id','LEFT');
	$this->db->where('a.ProjectId', $ProjectId);
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$config[$rows['id']] = $rows['menu'];
	}
	
	return $config;
}	
  
/**
 * @ def : add menu layout user navigation HHH 
 * ---------------------------------------------------------
 */
  
public function  _AddMenuWorkProject() {
	return array('WorkProjectId' => self::_WorkProjectId(), 'ApplicationMenu' =>self::_MenuApplication() );
}


/** 
 * @ def  : delete menu on work project detected 
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/DeleteApplicationMenuByProject/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array )
 * @ param : $ProjectId ( array ) 
 */
 
public function _DeleteApplicationMenuByProject()
{
	$MenuId = $this->URI->_get_array_post('MenuId');
	$ProjectId = $this->URI->_get_post('ProjectId');
	
	$conds = 0;
	if( is_array($MenuId) AND ($ProjectId) ==TRUE )
	{
		foreach( $MenuId as $key => $Id )
		{
			if( $on = $this->db->delete('t_gn_menu_project', array('MenuId' =>$Id, 'ProjectId'=>$ProjectId ) ) )
			{
				$conds++;
			}	
		}
	}
	
	return $conds;
}
 

/** 
 * @ def  : delete menu on work project detected 
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/AssignMenuWorkProjectById/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array )
 * @ param : $ProjectId ( array ) 
 */
 
public function _AssignMenuWorkProjectById()
{
	$MenuId = $this->URI->_get_array_post('MenuId');
	$ProjectId = $this->URI->_get_post('ProjectId');
	
	$conds = 0;
	if( is_array($MenuId) AND ($ProjectId)==TRUE )
	{
		foreach( $MenuId as $key => $Id )
		{
			$this->db->set('MenuId', $Id, FALSE);
			$this->db->set('ProjectId',$ProjectId, FALSE);
			$this->db->set('MenuActive', 1, FALSE);
			$this->db->set('CreateDateTs',date('Y-m-d H:i:s'));
			
			$this->db->insert('t_gn_menu_project');
		
			if( $this->db->affected_rows() > 0 )
			{
				$conds++;
			}	
		}
	}
	
	return $conds;
}
 
 
/** 
 * @ def  : delete menu on work project by rows in grid table  
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/RemoveApplicationMenuById/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array ) 
 */

public function _RemoveApplicationMenuById()
{
	$conds =0;
	if( $this->URI->_get_have_post('WorkMenuId') )
	{
		$WorkMenuId = $this->URI->_get_array_post('WorkMenuId');
		if( is_array($WorkMenuId) )
		foreach( $WorkMenuId as $key => $value ) 
		{
			if( $this->db->delete('t_gn_menu_project', array('id' =>$value) ) ){
				$conds++;
			}
		}
	}

	return $conds;
}



/** 
 * @ def  : delete menu on work project by rows in grid table  
 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/_ActiveWorkApplicationMenu/
 *
 * ------------------------------------------------------------------------------------------------- 
 * @ param : $MenuId ( array ) 
 */

public function _ActiveWorkApplicationMenu( $param = null, $activated = 0  )
{
	$conds =0;	
	
	if( !is_null($param) )
	{
		if( is_array($param) )
		foreach( $param as $key => $value ) 
		{	
			$this->db->set('MenuActive', $activated);
			$this->db->set('UpdateDateTs', date('Y-m-d H:i:s'));
			$this->db->where('Id', $value);
			
			$this->db->update('t_gn_menu_project');
			if( $this->db->affected_rows() > 0 )
			{
				$conds++;
			}
		}
	}

	return $conds;
	
}






 
  
  
 	
	
	
}

?>