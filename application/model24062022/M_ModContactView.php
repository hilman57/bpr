<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_ModContactView extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function M_ModContactView()
{
	$this->load->meta('_cc_extension_agent');
	$this->load->model(array(
		'M_SetResultQuality', 'M_SetCallResult', 
		'M_MaskingNumber', 'M_SaveWorkProjectForm',
		'M_ModSaveActivity'	));
	
}


function _getCekPolicyForm( $CustomerId = 0 )
{
	$_conds = 0;
	
	$this -> db -> select('COUNT(a.PolicyAutoGenId) as jumlah');
	$this -> db -> from("t_gn_policyautogen a ");
	$this -> db -> where("a.CustomerId", $CustomerId);
	
	if(  $rows = $this -> db -> get() -> result_first_assoc()  )
	{
		$_conds = (INT)$rows['jumlah'];
	}
	
	return $_conds;
}

 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function NotInterest()
{
	$_a = array(); $_b = array();
	if( class_exists('M_SetCallResult'))
	{
		$_a = $this -> M_SetCallResult -> _getNotInterest(); 
		foreach( $_a as $_k => $_v )
		{
			$_b[$_k] = $_k;  
		}	
	}
	
	return $_b;
} 
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function Sale()
{
	$_a = array(); $_b = array();
	if( class_exists('M_SetCallResult'))
	{
		$_b = $this -> M_SetCallResult -> _getPendingInfo();
		$_a = $this -> M_SetCallResult -> _getInterestSale(); 
		foreach( $_a as $_k => $_v )
		{
			if( !in_array($_k, array_keys($_b))){
				$_b[$_k] = $_k;  
			}
		}	
	}
	
	return $_b;
} 



// _NextActivity 

public function _NextActivity()
{
    $next_activity = array();
	$this->_get_default();
	$sql = $this->EUI_Page->_getCompiler();
	if( $sql ) 
	{
		$qry = $this->db->query($sql);
		$num = 0;
		foreach( $qry->result_assoc() as $rows ){
			$next_activity[] = $rows['CustomerId'];  
			$num++;
		}
	}
	
	return $next_activity;
	
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getProductCategory()
 {
	$datas = array();
	
	$sql = "SELECT 
				distinct(k.PolicyProductCategory) as PolicyProductName 
			FROM t_gn_debitur a 
			INNER JOIN t_gn_assignment b ON a.CustomerId=b.CustomerId 
			LEFT JOIN t_tx_agent ag ON ag.UserId=b.AssignSelerId 
			LEFT JOIN t_gn_campaign d ON a.CampaignId=d.CampaignId 
			LEFT JOIN t_lk_account_status f ON a.CallReasonId = f.CallReasonId 
			LEFT JOIN t_gn_campaign_project g ON a.CampaignId=g.CampaignId 
			LEFT JOIN t_gn_policy_detail k ON a.CustomerNumber=k.EnigmaId 
			WHERE 1=1 
			AND b.AssignAdmin IS NOT NULL 
			AND b.AssignMgr IS NOT NULL 
			AND b.AssignSpv IS NOT NULL 
			AND b.AssignBlock='0' 
			AND d.CampaignStatusFlag='1' 
			AND g.ProjectId IN(".implode(',',$this->EUI_Session->_get_session('ProjectId')).") 
			AND a.CallComplete='0' ";
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$sql .= " AND b.AssignSelerId = '".$this->EUI_Session->_get_session('UserId')."' ";
	}
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$sql .= " AND b.AssignSelerId = '".$this->EUI_Session->_get_session('UserId')."' ";
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ){
		$sql .= " AND b.AssignSpv = '".$this->EUI_Session->_get_session('UserId')."' ";
		$sql .= " AND b.AssignSelerId is not null ";
	}	
				 
	if( $this->EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$sql .= " AND b.AssignMgr = '".$this->EUI_Session->_get_session('UserId')."' ";
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER) {
		$sql .= " AND b.AssignAmgr = '".$this->EUI_Session->_get_session('UserId')."' ";
	}		
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_ADMIN){
		$sql .= " AND b.AssignAmgr = '".$this->EUI_Session->_get_session('UserId')."' ";
	}	
	
	$qry = $this->db->query($sql);
	
	if($qry->num_rows() > 0)
	{
		foreach($qry->result_assoc() as $rows)
		{
			$datas[$rows['PolicyProductName']] = $rows['PolicyProductName'];
		}
	}
	
	return $datas;
 }
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this->EUI_Page->_setSelect("a.CustomerId");
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.CustomerId=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.CampaignId=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.CallReasonId = f.CallReasonId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.CampaignId=g.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_policy_detail k ","a.CustomerNumber=k.EnigmaId","LEFT", TRUE);
	
/**	filtering key of the session  **/

	$this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	$this->EUI_Page->_setAnd('b.AssignBlock', '0');
	$this->EUI_Page->_setAnd('d.CampaignStatusFlag',1);
	

	/*** filtering ***/
	
	$this->EUI_Page->_setWherein('g.ProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->EUI_Page->_setAnd('a.CallComplete','0');

	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}		
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ){
		$this->EUI_Page->_setAnd('b.AssignSpv',$this->EUI_Session->_get_session('UserId'));
		$this->EUI_Page->_setAnd('b.AssignSelerId is not null');
	}	
				 
	if( $this->EUI_Session->_get_session('HandlingType')==USER_MANAGER ){
		$this->EUI_Page->_setAnd('b.AssignMgr', $this->EUI_Session->_get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER) {
		$this->EUI_Page->_setAnd('b.AssignAmgr', $this->EUI_Session->_get_session('UserId'));
	}		
	
	if( $this->EUI_Session->_get_session('HandlingType')==USER_ADMIN){
		$this->EUI_Page->_setAnd('b.AssignAmgr', $this->EUI_Session->_get_session('UserId'));
	}		
	
/** simple cache handle on page ***/

	$this->EUI_Page->_setLikeCache('a.CustomerFirstName', 'flw_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.CIFNumber', 'flw_cif_number', TRUE);
	$this->EUI_Page->_setAndCache('a.CallAttempt', 'flw_attempt_count', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyNumber', 'flw_policy_number', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyProductCategory', 'flw_product_name', TRUE);
	$this->EUI_Page->_setAndCache('d.CampaignId', 'flw_campaign',TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'flw_dc', TRUE);	
	
/** customize queryes ***/

	if(_get_post('flw_call_reason')=='NEW')
		$this->EUI_Page->_setAndOrCache("( f.CallReasonId IS NULL OR a.CallReasonId=0 )", 'flw_call_reason', TRUE);
	else 
		$this->EUI_Page->_setAndCache('f.CallReasonId', 'flw_call_reason', TRUE);
		
	
/** set filtering **/

	$this->EUI_Page->_setGroupBy('a.CustomerNumber');
	
	if($this->EUI_Page->_get_query()) 
	{
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
/** default of query ***/

	$this->EUI_Page->_setArraySelect(array(
		"a.CustomerId AS CustomerId"=> array('CustomerId','ID','primary'),
		"a.CustomerFirstName AS CustomerFirstName"=> array('CustomerFirstName','Owner Name'),
		"k.PolicyNumber AS PolicyNumber"=> array('PolicyNumber','Policy Number'),
		"k.PolicyProductCategory AS PolicyProductName"=> array('PolicyProductName','Product Category'),
		"DATE_FORMAT(k.PolicyIssDate,'%d/%m/%Y') AS PolicyIssDate"=> array('PolicyIssDate','Issued Date'),
		"d.CampaignCode AS CampaignCode"=> array('CampaignCode','Campaign Code'),
		"DATE_FORMAT(a.CustomerUpdatedTs,'%d/%m/%Y %H:%i:%s') AS CustomerUpdatedTs"=> array('CustomerUpdatedTs','Last Call Date '),
		"IF(m.CallReasonCategoryName is null, 'New',m.CallReasonCategoryName)  AS CallReasonCategoryName" => array('CallReasonCategoryName','Call Result'),
		"IF(f.CallReasonDesc is null,'New',f.CallReasonDesc) AS CallResult"=> array('CallResult','Call Status'),
		"m.CallReasonCategoryId"=> array('CallReasonId','Call Result'),
		"ag.full_name AS full_name"=> array('full_name','Caller Name'),
		"a.CallAttempt AS CallAttempt"=> array('CallAttempt','Attempt'),
		"COUNT(distinct k.PolicyNumber) AS SumPolicy"=> array('SumPolicy','Sum of Policy')
	));
	
	$this->EUI_Page->_setFrom("t_gn_debitur a");
	$this->EUI_Page->_setJoin("t_gn_assignment b","a.CustomerId=b.CustomerId ","INNER");
	$this->EUI_Page->_setJoin("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign d "," a.CampaignId=d.CampaignId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_account_status f "," a.CallReasonId = f.CallReasonId ","LEFT");
	$this->EUI_Page->_setJoin("t_lk_customer_status m "," m.CallReasonCategoryId = f.CallReasonCategoryId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.CampaignId=g.CampaignId","LEFT");
	$this->EUI_Page->_setJoin("t_gn_policy_detail k ","a.CustomerNumber=k.EnigmaId","LEFT", TRUE);

/** process **/
	 $this->EUI_Page->_setAnd('b.AssignAdmin IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignMgr IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignSpv IS NOT NULL', FALSE);
	 $this->EUI_Page->_setAnd('b.AssignBlock', 0);
 	 $this->EUI_Page->_setAnd('d.CampaignStatusFlag','1');
	 
	/** filtering  **/
	
	$this->EUI_Page->_setWherein('g.ProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->EUI_Page->_setAnd('a.CallComplete','0');
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}

	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this->EUI_Page->_setAnd('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}			 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ){
		$this->EUI_Page->_setAnd('b.AssignSpv',$this->EUI_Session->_get_session('UserId'));
		$this->EUI_Page->_setAnd('b.AssignSelerId is not null');
	}		
				 
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_MANAGER ){
		$this->EUI_Page->_setAnd('b.AssignAdmin',$this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER ){
		$this->EUI_Page->_setAnd('b.AssignAmgr', $this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ADMIN ){
		$this->EUI_Page->_setAnd('b.AssignMgr', $this -> EUI_Session -> _get_session('UserId'));
	}
	
/** set cache on page **/
	
	$this->EUI_Page->_setLikeCache('a.CustomerFirstName', 'flw_cust_name', TRUE);
	$this->EUI_Page->_setAndCache('a.CIFNumber', 'flw_cif_number', TRUE);
	$this->EUI_Page->_setAndCache('a.CallAttempt', 'flw_attempt_count', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyNumber', 'flw_policy_number', TRUE);
	$this->EUI_Page->_setAndCache('k.PolicyProductCategory', 'flw_product_name', TRUE);
	$this->EUI_Page->_setAndCache('d.CampaignId', 'flw_campaign',TRUE);
	$this->EUI_Page->_setAndCache('ag.UserId', 'flw_dc', TRUE);	
	
/** customize queryes ***/
	
	if(_get_post('flw_call_reason')=='NEW')
		$this->EUI_Page->_setAndOrCache("( f.CallReasonId IS NULL OR a.CallReasonId=0 )", 'flw_call_reason', TRUE);
	else 
		$this->EUI_Page->_setAndCache('f.CallReasonId', 'flw_call_reason', TRUE);
		
/* set group by **/
	
	$this->EUI_Page->_setGroupBy('a.CustomerNumber');
	
/* set order by **/
	
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
}


function _get_active_campaign()
{
	$this->db->distinct();
	$this->db->select(" d.CampaignId,  d.CampaignName ");
	$this->db->from("t_gn_debitur a");
	$this->db->join("t_gn_assignment b","a.CustomerId=b.CustomerId ","INNER");
	$this->db->join("t_tx_agent ag","ag.UserId=b.AssignSelerId ","LEFT");
	$this->db->join("t_gn_campaign d "," a.CampaignId=d.CampaignId ","LEFT");
	$this->db->join("t_gn_campaign_project g "," a.CampaignId=g.CampaignId","LEFT");

/** process **/
	 $this->db->where('b.AssignAdmin IS NOT NULL');
	 $this->db->where('b.AssignMgr IS NOT NULL');
	 $this->db->where('b.AssignSpv IS NOT NULL');
	 $this->db->where('b.AssignBlock', 0);
 	 $this->db->where('d.CampaignStatusFlag','1');
	 
	/** filtering  **/
	
	$this->db->where_in('g.ProjectId',$this->EUI_Session->_get_session('ProjectId') );
	$this->db->where('a.CallComplete','0');
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_AGENT_OUTBOUND ){
		$this->db->where('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}

	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_AGENT_INBOUND ){
		$this->db->where('b.AssignSelerId', $this->EUI_Session->_get_session('UserId'));
	}			 
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_SUPERVISOR ){
		$this->db->where('b.AssignSpv',$this->EUI_Session->_get_session('UserId'));
		$this->db->where('b.AssignSelerId is not null');
	}		
				 
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_MANAGER ){
		$this->db->where('b.AssignAdmin',$this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ACCOUNT_MANAGER ){
		$this->db->where('b.AssignAmgr', $this -> EUI_Session -> _get_session('UserId'));
	}	
	
	if( $this -> EUI_Session -> _get_session('HandlingType')==USER_ADMIN ){
		$this->db->where('b.AssignMgr', $this -> EUI_Session -> _get_session('UserId'));
	}

	$res = array();
	foreach ($this->db->get()->result_assoc() as $value) {
		$res[$value['CampaignId']] = $value['CampaignName'];
	}
	
	return $res;
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getGenderId()
{
  $_conds = array();
  $sql = " SELECT a.GenderId, a.Gender FROM  t_lk_gender a";
  $qry = $this -> db -> query($sql);
  if( !$qry -> EOF() )
  {
	foreach( $qry -> result_assoc() as $rows ) 
	{
		$_conds[$rows['GenderId']] = $rows['Gender'];
	}
  }	
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _getCardType()
{
  $_conds = array();
  $sql = " SELECT a.CardTypeId, a.CardTypeDesc FROM t_lk_cardtype  a WHERE a.CardTypeFlag=1";
  $qry = $this -> db -> query($sql);
  if( !$qry -> EOF() )
  {
	foreach( $qry -> result_assoc() as $rows ) 
	{
		$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
	}
  }	
  
  return $_conds;
} 


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_data_template()
{
	$data_template = $this -> _cc_extension_agent -> _get_meta_colums();
 	if( $data_template )
	{
		return $data_template;
	}
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 public function _getDetailCustomer($CustomerId=null)
 {
	$_conds = array();
	
	$this -> db -> select('a.*, b.Gender, f.id, f.full_name, c.Salutation, d.CampaignName, d.CampaignNumber, e.CallReasonDesc, e.CallReasonCategoryId ', FALSE);
	$this -> db -> from('t_gn_debitur a');
	$this -> db -> join('t_lk_gender b','a.GenderId=b.GenderId','LEFT');
	$this -> db -> join('t_lk_salutation c','a.SalutationId=c.SalutationId','LEFT');
	$this -> db -> join('t_gn_campaign d','a.CampaignId=d.CampaignId','LEFT');
	$this -> db -> join('t_lk_account_status e','a.CallReasonId=e.CallReasonId','LEFT');
	$this -> db -> join('t_gn_assignment g','a.CustomerId=g.CustomerId','LEFT');
	$this -> db -> join('t_tx_agent f','g.AssignSelerId=f.UserId','LEFT');
	$this -> db -> where('a.CustomerId',$CustomerId);
	// echo $this -> db ->_get_var_dump();
	
	foreach($this -> db ->get() -> result_assoc() as $rows )
	{
		foreach($rows as $k => $v ){
			$_conds[$k] = $v;
		}
	}
	
	return $_conds;
 }
 
 /*
 * @ def 		: _getOriginalData
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
public function _getOriginalData( $CustomerId = 0 )
{
	$data = array();
	
	$this ->db ->select('*');
	$this ->db ->from('t_gn_debitur');
	$this ->db ->where('CustomerId',$CustomerId);
	
	if( $CustomerData = $this -> db->get()-> result_first_assoc() ) {
		$data = $CustomerData;
	}
	
	return $data;
} 
 



 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getPhoneCustomer($CustomerId=null)
 {
	$_conds = array();
	
	$this ->db ->select('CustomerHomePhoneNum, CustomerMobilePhoneNum, CustomerWorkPhoneNum, CustomerWorkFaxNum, CustomerWorkExtPhoneNum, CustomerFaxNum');
	$this ->db ->from('t_gn_debitur');
	$this ->db ->where('CustomerId',$CustomerId);
	
	foreach($this -> db ->get() -> result_assoc() as $rows )
	{
		if( !is_null($rows['CustomerHomePhoneNum']) 
			AND $rows['CustomerHomePhoneNum']!='' )
		{
			$_conds[$rows['CustomerHomePhoneNum']] = " Home ".$this->M_MaskingNumber->MaskingBelakang($rows['CustomerHomePhoneNum']); 
		}
		
		if( !is_null($rows['CustomerMobilePhoneNum']) 
			AND $rows['CustomerMobilePhoneNum']!='' )
		{
			$_conds[$rows['CustomerMobilePhoneNum']] = " Mobile ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerMobilePhoneNum']); 
		}
		
		if( !is_null($rows['CustomerWorkPhoneNum']) 
			AND $rows['CustomerWorkPhoneNum']!='' )
		{
			$_conds[$rows['CustomerWorkPhoneNum']] = " Office ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerWorkPhoneNum']); 
		}
		
		if( !is_null($rows['CustomerWorkFaxNum']) 
			AND $rows['CustomerWorkFaxNum']!='' )
		{
			$_conds[$rows['CustomerWorkFaxNum']]= " Work Fax ". $this->M_MaskingNumber->MaskingBelakang($rows['CustomerWorkFaxNum']); 
		}
		
		//
		if( !is_null($rows['CustomerWorkExtPhoneNum']) 
			AND $rows['CustomerWorkExtPhoneNum']!='' )
		{
			$_conds[$rows['CustomerWorkExtPhoneNum']] = " Work Ext ". $rows['CustomerWorkExtPhoneNum']; 
		}
		
		
		if( !is_null($rows['CustomerFaxNum']) 
			AND $rows['CustomerFaxNum']!='' )
		{
			$_conds[$rows['CustomerFaxNum']]= " Fax ". $rows['CustomerFaxNum']; 
		}
	}
	
	return $_conds;
 }
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getApprovalPhoneItems($CustomerId = 0 )
 {
	$_conds = array();
	
	$this ->db ->select('a.ApprovalNewValue, b.ApprovalItem');
	$this ->db ->from('t_gn_approvalhistory a');
	$this ->db ->join('t_lk_approvalitem b','a.ApprovalItemId=b.ApprovalItemId','LEFT');
	$this ->db ->where('a.CustomerId',$CustomerId);
	
	foreach($this ->db -> get()->result_assoc() as $rows )
	{
		$_avail = explode(' ', $rows['ApprovalItem']);
		if( count($_avail) > 0 ){
			$_conds[$rows['ApprovalNewValue']] = $_avail[0] .' '. $this->M_MaskingNumber->MaskingBelakang($rows['ApprovalNewValue']); 	
		}
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function _cti_extension_upload( $data = null )
 {
	$_totals = 0;
	$_conds = false;
	if( !is_null( $data) )
	{
		if($this -> URI -> _get_post('mode') =='truncate') //empty table if truncate mode  
			$this -> db -> truncate( $this -> _cc_extension_agent-> _get_meta_index() );
			
		// then request 
		
		foreach( $data as $rows ) 
		{
			if( $this -> db -> insert( 
				$this -> _cc_extension_agent-> _get_meta_index(),
				$rows
			)){
				$_totals+=1;
			}
		}
		
		if( $_totals > 1)
			$_conds = true;
		
	}
	
	return $_conds;	
 }
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAvailProduct( $CustomerId = 0 ) {
	$_product = array();
	return $_product;
 }
 
/*
 * @ def 		: get policy data by customer ID AND by Upload ID **
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 

 public function _getPolicyCustomer($CustomerId = 0 )
 {
	$datas = array();
	
	$this->db->select('b.PolicyId, b.PolicyNumber');
	$this->db->from('t_gn_debitur a');
	$this->db->join('t_gn_policy_detail b ',' a.CustomerNumber=b.EnigmaId', 'LEFT');
	$this->db->where('a.CustomerId', $CustomerId);
	foreach($this->db->get()->result_assoc() as $rows){
		$datas[$rows['PolicyId']] = $rows['PolicyNumber'];
	}
	
	return $datas;
 }
 
 /*
 * @ def 		: _getRowPolicyByCustomer
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 

 public function _getRowPolicyByCustomer($CustomerId = 0 )
 {
	$datas = array();
	
	$this->db->select('b.PolicyId,a.CustomerId,a.CustomerFirstName, b.CIFNumber, 
			b.PolicyFirstName, b.PolicyNumber, b.PolicyPremi, b.PolicyIssDate, b.CallReasonId, 
			c.CallReasonCategoryId, b.CallComplete,b.UpdateDateTs, d.full_name, a.CallAttempt');
	$this->db->from('t_gn_debitur a');
	$this->db->join('t_gn_policy_detail b ',' a.CustomerNumber=b.EnigmaId', 'LEFT');
	$this->db->join('t_lk_account_status e ',' a.CallReasonId=e.CallReasonId', 'LEFT');
	$this->db->join('t_lk_customer_status c ',' e.CallReasonCategoryId=c.CallReasonCategoryId', 'LEFT');
	$this->db->join('t_tx_agent d ',' d.UserId=b.CreateByUserId', 'LEFT');
	$this->db->where('a.CustomerId', $CustomerId);
	$rows = $this->db->get();
	if($rows->num_rows() > 0 ) 
	{
		$datas = $rows -> result_assoc();
	}
	
	return $datas;
 }
 
 
 /*
 * @ def 		: _getPolicyDataById
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 

 
 public function _getPolicyDataById( $PolicyId = null )
 {
	$datas = array();
	$this->db->select('a.*, b.*',FALSE);
	$this->db->from('t_gn_debitur a');
	$this->db->join('t_gn_policy_detail b ',' a.CustomerNumber=b.EnigmaId', 'LEFT');
	$this->db->join('t_lk_paymode c', 'c.PayModeId=b.PaymentMode','LEFT');
	$this->db->where('b.PolicyId', $PolicyId);
	
	$rows = $this->db->get();
	
	if($rows->num_rows() > 0 ) {
		foreach( $rows->result_first_assoc() as $field => $value ){
			$datas[$field] = $value;
		}
	}
	
	return $datas;
 }
 
 
 /*
 * @ def 		: get policy data by customer ID AND by Upload ID **
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
public function _getPolicyStatusData( $PolicyId = null )
{
	$conds = false;
	if( !is_null($PolicyId) ) 
	{
		$this->db->select('b.*, a.CallComplete');
		$this->db->from('t_gn_policy_detail a');
		$this->db->join('t_lk_account_status b ','a.CallReasonId=b.CallReasonId','LEFT');
		$this->db->where('b.CallReasonId IS NOT NULL');
		$this->db->where('PolicyId', $PolicyId);
		
		if( $rows = $this->db->get()->result_first_assoc() )
		{
			$conds = $rows;
		}
	}
 
 return $conds;
}

 /*
 * @ def 		: _getAgent
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 public function _getAgent()
 {
 	$ret = array();

 	$this->db->distinct();
 	$this->db->select("ag.UserId, ag.id, ag.full_name");
 	$this->db->from("t_gn_debitur a");
 	$this->db->join("t_gn_assignment b","a.CustomerId=b.CustomerId ","INNER");
 	$this->db->join("t_tx_agent ag","ag.UserId=b.AssignSelerId ","INNER");
 	$this->db->join("t_gn_campaign d "," a.CampaignId=d.CampaignId ","LEFT");
 	$this->db->join("t_lk_account_status f "," a.CallReasonId = f.CallReasonId ","LEFT");
 	$this->db->join("t_lk_customer_status m "," m.CallReasonCategoryId = f.CallReasonCategoryId ","LEFT");
 	$this->db->join("t_gn_campaign_project g "," a.CampaignId=g.CampaignId","LEFT");
 	$this->db->join("t_gn_policy_detail k ","a.CustomerNumber=k.EnigmaId","LEFT", "TRUE");

/** process **/

	$this->db->where('b.AssignAdmin IS NOT NULL');
	$this->db->where('b.AssignMgr IS NOT NULL');
	$this->db->where('b.AssignSpv IS NOT NULL');
	$this->db->where('b.AssignBlock', 0);
	$this->db->where('d.CampaignStatusFlag','1');
	$this->db->where_in('g.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	$this->db->order_by("ag.full_name","ASC");
	
	foreach ($this->db->get()->result_assoc() as $value) {
		$ret[$value['UserId']] = $value['full_name'];
		
	}

	return $ret;

 }
 
/*
 * @ def 		: _validSellerId
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 public function _validSellerId($param)
 {
	$_conds = false;
	$sql = " select a.AssignSelerId from t_gn_assignment a where a.CustomerId = '".$param['CustomerId']."' ";
	$qry = $this->db->query($sql);
	if($qry->result_singgle_value() == '')
	{
		$_conds = true;
	}
	
	return $_conds;
 }
 
 public function _getCallCompleteStatus($CustomerId)
 {
	$_conds = false;
	
	$this->db->select('a.CallComplete');
	$this->db->from('t_gn_debitur a');
	$this->db->where('a.CustomerId', $CustomerId);
	
	if( $rows = $this->db->get()->result_first_assoc() )
		{
			if($rows['CallComplete']==1)$_conds=true;
		}
	
	return $_conds;
 }
 
/**
 * @ aksess : public  
 * 	 get handle Project on site 
 *   if match on configuration  
	 \ P002 = BCU , P001 = WCAL \
	LIKE THIS 
**/
public function _getOnWorkProject( $CustomerId  = 0 )
{
	$array_config = array('disabled' => 'disabled');
	// if( !in_array( $this->EUI_Session->_get_session('HandlingType'), 
	// array( USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) )) {
	if($this->_getCallCompleteStatus($CustomerId)==false){
		$array_config = NULL;
	}
	return $array_config;
}
 
/**
 * @ def 	: save point data array
 * ---------------------------------------------------------
 * @ params : -
 * @ params : -
 */
 
public function _setAnswerWorkProject()
{
	$flds = 0; 
	$field = array (
		'CustField_Q1','CustField_Q2','CustField_Q3','CustField_Q4','CustField_Q5', 
		'CustField_Q6','CustField_Q7','CustField_Q8','CustField_Q9','CustField_Q10');
	
	for($n =0; $n<count($field); $n++){
		$values = $this->URI->_get_post($field[$n]);
		if(($values!='')) {
			$this->db->set($field[$n], $this->URI->_get_post($field[$n]));
			$flds++;
		}	
	}
	
/** set data point answer ***/

	if( $flds > 0 ) 
	{
		$this->db->set('CustomerId', $this->URI->_get_post('CustomerId')); 
		$this->db->set('EnigmaId',$this->URI->_get_post('CustomerNumber'));
		$this->db->set('PolicyId',$this->URI->_get_post('PolicyId'));
		$this->db->set('UserLevel1', $this->EUI_Session->_get_session('UserId'));
		$this->db->set('UserLevel2', $this->EUI_Session->_get_session('SupervisorId'));
		$this->db->set('UserLevel3', $this->EUI_Session->_get_session('ManagerId'));
		$this->db->set('CreateTs', date('Y-m-d H:i:s'));
		
		$this->db->insert('t_gn_followup_question'); // inserting to tbls 
		
		if(!$this->db->affected_rows() )
		{
			for( $s_i =0; $s_i<count($field); $s_i++) 
			{
				$values = $this->URI->_get_post($field[$s_i]);
				if( !empty($values) ) 
				{
					$this->db->set($field[$s_i], $this->URI->_get_post($field[$s_i]));
					$flds++;
				}	
			}
			
		  $this->db->set('UserLevel1', $this->EUI_Session->_get_session('UserId'));
		  $this->db->set('UserLevel2', $this->EUI_Session->_get_session('SupervisorId'));
		  $this->db->set('UserLevel3', $this->EUI_Session->_get_session('ManagerId'));
		  $this->db->set('UpdateTs', date('Y-m-d H:i:s'));
		  $this->db->set('UpdateBy',$this->EUI_Session->_get_session('UserId'));
		  $this->db->where('CustomerId', $this->URI->_get_post('CustomerId'));
		  $this->db->where('PolicyId', $this->URI->_get_post('PolicyId'));
		  $this->db->update('t_gn_followup_question');
		}
		
		$QestionId = $this->db->insert_id();
		
		$param = array(
			'EnigmaId' 	 => $this->URI->_get_post('CustomerNumber'),
			'PolicyId' 	 => $this->URI->_get_post('PolicyId'),
			'UserLevel1' => $this->EUI_Session->_get_session('UserId'),
			'UserLevel2' => $this->EUI_Session->_get_session('SupervisorId'),
			'UserLevel3' => $this->EUI_Session->_get_session('ManagerId'),
			'CreateTs' 	 => date('Y-m-d H:i:s'),
			'CreateBy' 	 => $this->EUI_Session->_get_session('UserId')
		);
		
		for($n =0; $n<count($field); $n++)
		{
			$values = $this->URI->_get_post($field[$n]);
			if(!empty($values)) 
			{
				$param[$field[$n]] = $this->URI->_get_post($field[$n]);
			}	
		}
		
		self::set_var_log($param);
		
		/** save followup data **/
		
		$this->M_SaveWorkProjectForm->SetSaveFollowupProject( $QestionId ); 
		
		/** array data set ***/
	}

} 



/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: procedure function
 * @ return 	: void(0)
 */
 
protected function set_var_log($param)
{
	
 $QuestionId = $this->M_ModSaveActivity->_getQuestionId(array( 'EnigmaId' => $param['EnigmaId'], 'PolicyId' => $param['PolicyId']));
 $this->db->set('QuestionId',$QuestionId);
 
 foreach($param as $field => $value) 
 {
	if( $field != 'EnigmaId' 
		AND $field != 'PolicyId' )
	{
		$this->db->set($field, $value);
	}
 }
	
 $this->db->insert('t_gn_followup_question_log'); // inserting to tbls 
 
 return $this->db->affected_rows();
	
}


/*
 * @ def 		: index / default pages controller 
 * -----------------------------------------
 *
 * @ params  	: procedure function
 * @ return 	: void(0)
 */
 
public function SaveDataByAppoinment($_post_array = null)
{
	$_conds = 0;
	if(is_array($_post_array)
		AND ($_post_array['date_call_later']!='') 
		AND ($_post_array['date_call_later']!='')
		AND ($_post_array['hour_call_later']!='')
		AND ($_post_array['hour_call_later']!='')) 
	{
		$this->db->set("CustomerId",$_post_array['CustomerId']);
		$this->db->set("UserId",$this->EUI_Session->_get_session('UserId'));
		$this->db->set("ApoinmentDate",$this->M_ModSaveActivity->_getAppointmentDate($_post_array));
		$this->db->set("ApoinmentCreate",$this->EUI_Tools->_date_time());
		$this->db->set("ApoinmentFlag",0);
		$this->db->insert('t_gn_appoinment');
		if( $this->db->affected_rows()> 0 )
			$_conds++;
	}	
	
	return $_conds;
}



/*
 * @ def 		: _setSaveActivity
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _setSaveActivity( $_vars = null )
{
  $_conds = 0;
  
  if($this->EUI_Session->_have_get_session('UserId'))
  {
	$_call_atempt = $this->URI->_get_post('CallAtempt');
	if(is_array($_vars)) 
	{
		self::SaveDataByAppoinment($_vars);	
		
/* cek approval status ID **/

		$ApprovalStatusId = 0;
		$Interest = $this->M_SetCallResult->_getInterestSale();
		$PendingInfo = $this->M_SetCallResult->_getPendingInfo();
		
		if( in_array($_vars['CallResult'], array_keys($Interest)))
		{
			if( $ApprovalStatusId = $this -> M_SetResultQuality->_getQualityStatusByCode('505')){
				if(!in_array($_vars['CallResult'], array_keys($PendingInfo)) ){
					$this -> db ->set('ApprovalStatusId',$ApprovalStatusId);
				}
			}
		}
		
  /* save to call history :: get insert data call **/
		
		$PhoneType = $this->M_ModSaveActivity->_getPhoneType($_vars['CustomerId']);
			
		$this->db->set('PhoneType', $PhoneType[$_vars['PhoneNumber']]);
		$this->db->set('CustomerId',$_vars['CustomerId']);
		$this->db->set('PolicyId',$_vars['PolicyId']);
		$this->db->set('CallReasonId',$_vars['CallResult']); 
		$this->db->set('CallNumber', $_vars['PhoneNumber']);
		$this->db->set('CallHistoryNotes', $_vars['call_remarks']); 
		$this->db->set('CallComplete', $_vars['CallComplete']);
		$this->db->set('ScriptId',$this->URI->_get_post('ScriptId'));
		$this->db->set('CreatedById',$this->EUI_Session->_get_session('UserId'));
		$this->db->set('UpdatedById',$this->EUI_Session->_get_session('UserId')); 
		$this->db->set('CallHistoryCallDate',date('Y-m-d H:i:s'));
		$this->db->set('CallHistoryCreatedTs',date('Y-m-d H:i:s'));
		$this->db->set('CallHistoryUpdatedTs',date('Y-m-d H:i:s'));
		
		
		$this->db->set('CallSessionId',($_vars['CallSessionId']?$_vars['CallSessionId'] : 'NULL'),FALSE);
		$this->db->insert('t_gn_callhistory');
		
		if($this->db->affected_rows()>0 )
		{
			if( $QUALITY_STATUS = $this->M_SetResultQuality->_getQualityBackLevel() 
			  AND in_array( $_vars['QualityStatus'], $QUALITY_STATUS ) )
			{
				$this->db->set('CallReasonQue',$this->M_ModSaveActivity->QualityReconfirm() );
			}
			
	/** set ke kustomer jika data sale **/
	
			if($ApprovalStatusId)
			{
				if( !in_array($_vars['CallResult'], array_keys($PendingInfo)) ) {
					$this->db->set('CallReasonQue',$ApprovalStatusId);
				}	
			}
			
	/** if is complete status  ***/
		
        if($_vars['CallComplete'] > 0  ) {
			$this->db->set('CallReasonQue',9);
		}		
			
		// TO CUSTOMER BY CUSTOMER ID
		
		 $this->db->set('SellerId', $this->EUI_Session->_get_session('UserId'));
		 $this->db->set('CallReasonId', $_vars['CallResult']);
		 $this->db->set('CustomerUpdatedTs', date('Y-m-d H:i:s'));
		 $this->db->set('CallComplete', $_vars['CallComplete']);
		 $this->db->set('CallAttempt', "((CallAttempt)+$_call_atempt)", FALSE);
		 $this->db->where('CustomerId',$_vars['CustomerId']);
		 $this->db->update('t_gn_debitur');
		 
		 if( $this->db->affected_rows()>0 )
		 { 
				self::_setAnswerWorkProject();
				
				$this->db->reset_select();
				$this->db->set('CallReasonId', $_vars['CallResult']);	
				$this->db->set('CallComplete', $_vars['CallComplete']);
				$this->db->set('UpdateDateTs', date('Y-m-d H:i:s'));
				$this->db->set('PolicyUpdatedTs', date('Y-m-d H:i:s'));
				$this->db->set('CreateByUserId', $this->EUI_Session->_get_session('UserId'));
				$this->db->set('CallAttempt',  "((CallAttempt)+$_call_atempt)", FALSE);
				$this->db->where('PolicyId', $_vars['PolicyId']);
				
				if( $this->db->update('t_gn_policy_detail')) 
				{
					$_conds++;
				}		
			}
			
		  }
	 }
 }
	
	return $_conds;
 }
 
 
 // END OF CLASS 
 
}

?>