<?php
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 
class M_Combo extends EUI_Model
{

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
 
private static $Intance = null;

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
 
public function M_Combo()
{
	$this->load->model(array(
		'M_SetCampaign', 'M_SetProduct', 
		'M_SetCallResult','M_SysUser', 
		'M_SetResultQuality','M_SetResultCategory',
		'M_RefRPC','M_RefSPC', 'M_RefPTP','M_RefDiscount',
		'M_RefPaymentChannel'
	));
}

 
public function	&get_instance()
{
	if(is_null(self::$Intance) )
	{
		self::$Intance = new self();
	}
	
	return self::$Intance;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
 

function _getMaried()
{
	$_conds = array();
	
	$this -> db -> select('MaritalStatusCode, MaritalStatusDesc');
	$this -> db -> from('t_lk_maritalstatus');
	foreach($this -> db ->get()->result_assoc() as $rows ) {
		$_conds[$rows['MaritalStatusCode']] = $rows['MaritalStatusDesc'];
	}
	
	return $_conds;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
 

function _getSmoking()
{
	$_conds = array();
	return $_conds;
	
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	

function _getComunication()
{
	$_conds = array();
	
	$this -> db -> select('CommChannelId,CommChannelDesc');
	$this -> db -> from('t_lk_communications_channel');
	foreach($this -> db ->get()->result_assoc() as $rows )
	{
		$_conds[$rows['CommChannelId']] = $rows['CommChannelDesc'];
	}
	
	return $_conds;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
function _getWorkType()
{
	$_conds = array();
	return $_conds;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
function _getCountry()
{
	$_conds = array();
	$this -> db -> select('CountryCode, CountryName');
	$this -> db -> from('t_lk_country');
	$this -> db -> where('CountryFlagStatus',1);
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['CountryCode']] = $rows['CountryName'];
	}
	
	return $_conds;
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
function _getOutboundCategory()
{
	$_conds = array();
	$_conds = $this -> M_SetResultCategory->_getOutboundCategory();
	if( is_array($_conds) ){
		return $_conds;
	}
	else
		return null;
}	
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
 
function _getInboundCategory()
{
	$_conds = array();
	$_conds = $this -> M_SetResultCategory->_getInboundCategory();
	if( is_array($_conds) )
	{
		return $_conds;
	}
	else
		return null;
}	

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */	
 
function _getGender()
{
	$_conds = array();
	$this -> db -> select('GenderId,Gender');
	$this -> db -> from('t_lk_gender');
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['GenderId']] = $rows['Gender'];
	}
	
	return $_conds;
}	

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getCallResultInbound()
{ 
	$_conds = array();
	$this -> db -> select("a.CallReasonCode,a.CallReasonDesc"); 
	$this -> db -> from("t_lk_account_status  a ");
	$this -> db -> join("t_lk_customer_status b ","a.CallReasonCategoryId=b.CallReasonCategoryId","LEFT");
	$this -> db -> join("t_lk_outbound_goals c ","b.CallOutboundGoalsId=c.OutboundGoalsId","LEFT");
	$this -> db -> where("c.Name","inbound");
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['CallReasonCode']] = $rows['CallReasonDesc'];
	}
	
	return $_conds;
}	

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getCallResultOutbound()
{ 
	$_conds = array();
	$this -> db -> select("a.CallReasonCode,a.CallReasonDesc"); 
	$this -> db -> from("t_lk_account_status  a ");
	$this -> db -> join("t_lk_customer_status b ","a.CallReasonCategoryId=b.CallReasonCategoryId","LEFT");
	$this -> db -> join("t_lk_outbound_goals c ","b.CallOutboundGoalsId=c.OutboundGoalsId","LEFT");
	$this -> db -> where("c.Name","outbound");
	
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['CallReasonId']] = $rows['CallReasonDesc'];
	}
	
	return $_conds;
}	


/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getPaymentMode()
{
	$_conds = array();
	return $_conds;
}	

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getPaymentType()
{
	$_conds = array();
	return $_conds;
}	
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getCardType()
{
	$_conds = array();
	$this -> db -> select('CardTypeId, CardTypeDesc');
	return $_conds;
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getPremiGroup()
{
	$_conds = array();
	return $_conds;
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getBank()
{
	$_conds = array();
	return $_conds;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getProvince()
{
	$_conds = array();
	return $_conds;
}

/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getRealtionship()
{
	$_conds = array();
	return $_conds;
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getSalutation()
{
	$_conds = array();
	return $_conds;
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getDirectionType()
{
	$_conds = array();
	return $_conds;
}

// get UserAgent 

function _getUserr()
{
	$_conds = array();
	if(class_exists('M_SysUser')){
		$_conds = $this ->M_SysUser->_get_init_name();
	}
	
	return $_conds;
}


function _getUser() {
	$_conds = array();
	if(class_exists('M_SysUser')){
		$_conds = $this ->M_SysUser->_get_teleamarketer();
	}
	
	return $_conds;
}


// get _getProduct 

function _getProduct() {
	$_conds = array();
	return $_conds;
}


// get _getCampaign 

function _getCampaign()
{
	$_conds = array();
	if( class_exists('M_SetCampaign')) {
		$_conds = $this->M_SetCampaign->_get_campaign_name();
	}
	return $_conds;
}


/*
 * @ pack : get _getCallResult 
 * @ auth : omens 
 * @ comment : update migrate of code call code 
 *
 */
 
 public function _getCallResult()
{
  $_conds = array();
  if( class_exists('M_SetCallResult') )
  {
	
	$_list = $this->M_SetCallResult->_getCallReasonId(null);
	if( is_array($_list) ) 
	{
		foreach($_list as $k => $rows )
		{
			$_conds[$rows['code']] = $rows['name'];
		}
	}
  }
	
	return $_conds;
}

/*
 * @ pack : get _getCallResult 
 * @ auth : omens 
 * @ comment : update migrate of code call code 
 *
 */
 
 public function _getAccountStatus()
{
  $_conds = array();
  if( class_exists('M_SetCallResult') )
  {
	
	$_list = $this->M_SetCallResult->_getCallReasonId(null);
	if( is_array($_list) ) 
	{
		foreach($_list as $k => $rows )
		{
			$_conds[$rows['code']] = $rows['name'];
		}
	}
  }
	
	return $_conds;
}

/*
 * @ pack : get _getCallResult 
 * @ auth : omens 
 * @ comment : update migrate of code call code 
 *
 */
 
 public function _getQualityResult()
{
	$_conds = array();
	if( class_exists('M_SetResultQuality') ){
		$_list = $this->M_SetResultQuality->_getQualityResult();
		if( is_array($_list) ) {
			foreach($_list as $k => $rows ){
				$_conds[$k] = $rows['name'];
			}
		}
	}
	
	return $_conds;
}

function _getQualityResultForm()
{
	$_conds = array();
	if( class_exists('M_SetResultQuality') ){
		$_list = $this->M_SetResultQuality->_getQualityResultForm();
		if( is_array($_list) ) {
			foreach($_list as $k => $rows ){
				$_conds[$k] = $rows['name'];
			}
		}
	}
	
	return $_conds;
}
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 function _getSerialize(){
	$data = array();
	$list = get_class_methods($this);
	foreach($list as $key => $Method ) {
		$_key= substr($Method,4,strlen($Method));
		$data[$_key] = $Method;
	}
	
	return $data;
 }
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 function _getIdentification()
 {
	$_conds = array();
	$this -> db -> select('*');
	$this -> db -> from('t_lk_identificationtype');
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['IdentificationTypeId']] = $rows['IdentificationType'];
	}
	
	return $_conds;
 }	
  
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */

 function _getBillingAddress()
 {
	$_conds = array();
	return $_conds;
	
 } 
 
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getCallDirection()
 {
	$_conds = array();
	$this->db->select('OutboundGoalsId, Description');
	$this->db->from('t_lk_outbound_goals');
	$this->db->where('Flags',1);
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['OutboundGoalsId']] = $rows['Description'];
	}
	return $_conds;
 } 
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 function _getAgentGroup()
 {
	$_conds = array();
	$this->db->select('id, description');
	$this->db->from('cc_agent_group');
	$this->db->where('status_active',1);
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['id']] = $rows['description'];
	}
	
	return $_conds;
	
 }  
 
/*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
function _getAgentId()
 {
	$_conds = array();
	$this->db->select('a.id, a.name');
	$this->db->from('cc_agent a');
	$this->db->join('t_tx_agent b', 'a.userid = b.id','INNER');
	$this->db->where_not_in('b.handling_type', array(USER_ROOT));
	$this->db->order_by('a.name', 'ASC');
	
	foreach($this -> db ->get()->result_assoc() as $rows ){
		$_conds[$rows['id']] = $rows['name'];
	}
	
	return $_conds;
	
 }  
 
 /*
 * @ def 	: index / default page on this controller
 * ----------------------------------------------------
 * @ param  : $_POST
 * @ author : razaki team
 * 
 */
 function _getAnswerQuestion()
 {
	$_conds = array();
	return $_conds;
	
 }  
 
 /**
  * @def  : get follow_up group by project handle 
  * @date : 20141204
  *
  */
  
 function _getFollUpGroup() {
	$conds = array();
	return $conds;
 }
 
 
 // get phone type add by omens 
  
 
public function CallPhoneType( $CustomerId = null )
{
	static $config = array();
	$CallPhonetype = null;
	
	$this->db->reset_select();
	$this->db->select(" *", FALSE);
	$this->db->from('t_gn_debitur a');
	$this->db->where('a.deb_id', $CustomerId);
	
	$rec = $this->db->get();
	if( !$rec) { 
		exit(mysql_error());
	}
	
	if( $rec ->num_rows() > 0 )
	{
		if( $rows = $rec->result_first_assoc() )
		{
			foreach($rows as $field => $numbers ){
				if($numbers!='' )
				{
					$config[trim($numbers)] = $field;
				}
			}
		}
	}
	
	// then get query on Call History 
	$this->db->select('a.CallNumber');
	$this->db->from('t_gn_callhistory a');
	$this->db->where('a.CustomerId', $CustomerId);
	$this->db->where('a.UpdatedById IS NULL','', FALSE);
	$this->db->order_by('CallHistoryId','DESC');
	$this->db->limit(1);
	
	$hisorys = $this->db->get();
	if( $hisorys->num_rows() > 0)
	{
		if( $rows = $hisorys->result_first_assoc() )
		{
			if( in_array($rows['CallNumber'], array_keys($config) ) ) {
				return $config[$rows['CallNumber']];
			}
			else
			{
				if( $rows['CallNumber'] !='' )
					return 'Add Phone';
				else
					return NULL;
			}	
			
		}	
	}
} 

/*
 * @ pack : get SPC Reference 
 * @ auth : Omens 
 * @ Date : 2015-02-24 
 */
 
public function _getSPC()
{
  $select_spc = array();
  if( class_exists('M_RefSPC') )
  {
	 if( $_collect = $this->M_RefSPC->_get_select_spc() 
		AND is_array( $_collect ) ) 
	{
		$select_spc = (array)$_collect;
	}
	 
  }  
  
  return $select_spc;
}

/*
 * @ pack : get RPC Reference 
 * @ auth : Omens 
 * @ Date : 2015-02-24 
 */
 
public function _getRPC()
{
  $select_rpc = array();
  if( class_exists('M_RefRPC') )
  {
	 if( $_collect = $this->M_RefRPC->_get_select_rpc() 
		AND is_array( $_collect ) ) 
	{
		$select_rpc = (array)$_collect;
	}
	 
  }  
  
  return $select_rpc;
}

/*
 * @ pack : get Discount Reference 
 * @ auth : Omens 
 * @ Date : 2015-02-24 
 */
 
public function _getDiscount()
{
  $select_discount = array();
   
   if( class_exists('M_RefDiscount') )
  {

	 if( $_collect = $this->M_RefDiscount->_get_select_disc() 
		AND is_array( $_collect ) ) 
	{
		$select_discount = (array)$_collect;
	}
  }  
  
  return $select_discount;
} 

/*
 * @ pack : get INFO PTP Reference 
 * @ auth : Omens 
 * @ Date : 2015-02-24 
 */
 
public function _getInfoPTP()
{
  $select_info_ptp = array();
   
   if( class_exists('M_RefPTP') )
  {
	 if( $_collect = $this->M_RefPTP->_get_InfoPTP() 
		AND is_array( $_collect ) ) 
	{
		$select_info_ptp = (array)$_collect;
	}
  }  
  return $select_info_ptp;
} 

/*
 * @ pack : get payment Channel 
 * @ auth : Omens 
 * @ Date : 2015-02-24 
 */
 
public function _getPaymentChannel()
{
  $select_pch= array();
   
   if( class_exists('M_RefPaymentChannel') )
  {
	 if( $_collect = $this->M_RefPaymentChannel->_get_select_pch() 
		AND is_array( $_collect ) ) 
	{
		$select_pch = (array)$_collect;
	}
  }  
  return $select_pch;
}

/*
 * @ pack : get source coplain 
 * @ auth : ASEANINDOOOOOOOO 
 * @ Date : 2016-09-19
 */
 
public function _getSourceComplain()
{
	$source_complain= array();
	$this->db->select('a.id_source_complain,a.source_complain');
	$this->db->from('t_lk_source_complain a');
	$this->db->where('a.status_source',1);

	$query = $this->db->get();
	if( $query->num_rows() > 0)
	{
		foreach( $query->result_assoc() as $rows )
		{
			$source_complain[$rows['id_source_complain']] = $rows['source_complain'];
		}
	}
	return $source_complain;
	
}

public function _getReasonVC($CallStatus)
{
	$source_complain= array();
	$this->db->select('a.Id, a.VCReasonCode, a.VCReasonDesc');
	$this->db->from('t_lk_vc_reason a');
	$this->db->where('a.AccountStatusCode',$CallStatus);
	$this->db->where('a.Flag',1);
	// echo $this->db->_get_var_dump();
	$query = $this->db->get();
	if( $query->num_rows() > 0)
	{
		foreach( $query->result_assoc() as $rows )
		{
			$source_complain[$rows['Id']] = "(".$rows['VCReasonCode'].") ".$rows['VCReasonDesc'];
		}
	}
	return $source_complain;
} 

/*
 * @ pack : get source coplain 
 * @ auth : ASEANINDOOOOOOOO 
 * @ Date : 2016-09-19
 */
 
public function _getKasusKomplen()
{
	$kasus_complain= array();
	$this->db->select('a.id_kasus_komplain,a.kasus_komplain');
	$this->db->from('t_lk_kasus_komplain a');
	$this->db->where('a.status_kasus',1);

	$query = $this->db->get();
	if( $query->num_rows() > 0)
	{
		foreach( $query->result_assoc() as $rows )
		{
			$kasus_complain[$rows['id_kasus_komplain']] = $rows['kasus_komplain'];
		}
	}
	return $kasus_complain;
	
}

public function _getResponseComplain()
{
	$respon_complain= array();
	$this->db->select('a.id_respon,a.nama_respon');
	$this->db->from('t_lk_complain_respon a');

	$query = $this->db->get();
	if( $query->num_rows() > 0)
	{
		foreach( $query->result_assoc() as $rows )
		{
			$respon_complain[$rows['id_respon']] = $rows['nama_respon'];
		}
	}
	return $respon_complain;
}

// END CLASS 

}

?>