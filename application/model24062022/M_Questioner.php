<?php
class M_Questioner extends EUI_Model
{

	public function getQuestionerCategory()
	{
		return $this->db->get('t_lk_kategori_question')->result_array();
	}

	public function getQuestioner($id_category)
	{
		$recsource = array();
		$this->db->reset_select();
		$query = 'SELECT a.questioner_id ,b.survey_question ,a.questioner_desc ,c.a,c.b,c.c,c.d,c.jawabanbenar, a.UserId
			   FROM (t_gn_questioner a)
			   INNER JOIN t_lk_question_survey b ON a.questioner_id=b.quest_id
			   INNER JOIN t_lk_type_ans_survey c ON a.questioner_id=c.quest_id
			   WHERE a.questioner_flag = 1
				 AND a.kategori_id = ' . $id_category . ' GROUP BY a.questioner_id';

		$qry = $this->db->query($query);

		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row => $key) {
				$data[] = $key;
			}
		}

		return $data;
	}

	public function checkLogin($id, $password)
	{
		$this->db->where('id', $id);
		$this->db->where('password', $password);
		$qry = $this->db->get('t_tx_agent');
		if ($qry->num_rows() > 0) {
			return $qry->row_array();
		} else {
			return null;
		}
	}

	public function getResultQuestioner($deskoll)
	{
		$this->db->select('category_id');
		$this->db->where('deskoll', $deskoll);
		$qry = $this->db->get('t_gn_result_questioner');
		if ($qry->num_rows() > 0) {
			return $qry->result_array();
		} else {
			return null;
		}
	}

	public function insertResultQuestioner($data)
	{
		$this->db->insert('t_gn_result_questioner', $data);
	}

	public function m_enableMenu($param = null, $activated = 0)
	{
		$conds = 0;
		if (!is_null($param)) {
			if (is_array($param))
				foreach ($param as $key => $value) {
					$this->db->set('questioner_flag', $activated);
					$this->db->where('questioner_id', $value);
					$this->db->update('t_gn_questioner');
					if ($this->db->affected_rows() > 0) {
						$conds++;
					}
				}
		}

		return $conds;
	}

	public function get_default()
	{
		$this->EUI_Page->_setPage(20);
		$flt .= "WHERE a.questioner_flag = 1 ";

		$this->EUI_Page->_setSelect('a.questioner_id as WorkMenuId');
		$this->EUI_Page->_setFrom("t_gn_questioner a");
		$this->EUI_Page->_setJoin("t_gn_product b ", " a.product_id=b.ProductId", "LEFT");
		$this->EUI_Page->_setJoin("t_lk_questioner_type c ", " a.questioner_type=c.quest_type_id", "LEFT");
		//  $this -> EUI_Page -> _setWhere( $flt ); 

		/** set if have filter data ***/

		//  if(($this->URI->_get_have_post('keyword'))) 
		//  { 
		// 	$this->EUI_Page->_setAnd("( b.ProjectName LIKE '%".$this->URI->_get_post('keyword')."%'  
		// 		OR d.GroupName LIKE '%".$this->URI->_get_post(keyword)."%'  
		// 		OR c.menu LIKE '%".$this->URI->_get_post('keyword')."%' 
		// 		OR a.CreateDateTs LIKE '".$this->URI->_get_post('keyword')."') ", FALSE);
		// 	}


		return $this->EUI_Page;
	}


	/**
	 * @ def : get default result of content 
	 * ---------------------------------------------------------
	 * 
	 */

	public function get_content()
	{
		$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
		$this->EUI_Page->_setPage(20);
		$flt .= "WHERE a.questioner_flag = 1 ";
		$this->EUI_Page->_setArraySelect(
			array(
				'a.questioner_id as WorkMenuId' => array('WorkMenuId', 'ID', 'hidden'),
				'b.survey_question as ProjectName' => array('ProjectName', 'Questioner Description'),
				'a.questioner_desc as GroupName'  => array('GroupName', 'Questioner Type'),
				'c.jawabanbenar as menu' => array('menu', 'Questioner Description'),
				'IF(a.questioner_flag=1,"Active","Not Active") as MenuActive' => array('MenuActive', 'Questioner Status')
			)

		);

		$this->EUI_Page->_setFrom("t_gn_questioner a");
		$this->EUI_Page->_setJoin("t_lk_question_survey b ", " a.questioner_id=b.quest_id", "inner");
		$this->EUI_Page->_setJoin("t_lk_type_ans_survey c ", " a.questioner_id=c.quest_id", "inner");
		// $this -> EUI_Page -> _setWhere( $flt ); 
		// $this->EUI_Page->_setWhere('a.questioner_flag', '1');



		// if(($this->URI->_get_have_post('keyword'))) { 
		// 	$this->EUI_Page->_setAnd("( b.ProjectName LIKE '%".$this->URI->_get_post('keyword')."%'  
		// 		OR d.GroupName LIKE '%".$this->URI->_get_post(keyword)."%'  
		// 		OR c.menu LIKE '%".$this->URI->_get_post('keyword')."%' 
		// 		OR a.CreateDateTs LIKE '".$this->URI->_get_post('keyword')."') ", FALSE);
		// }

		// if( $this->URI->_get_have_post('order_by')) 
		// 	$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));

		$this->EUI_Page->_setLimit();
		// echo "<pre>". $this ->EUI_Page->_getCompiler()."</pre>";	
	}


	/*
 *@ get buffering query from database
 *@ then return by object type ( resource(link) ); 
 */

	function get_resource_query()
	{
		self::get_content();

		if ($this->EUI_Page->_get_query() != '') {
			return $this->EUI_Page->_result();
		}
	}

	/*
 *@ get number record & start of number every page 
 *@ then result ( INT ) type 
 */

	function get_page_number()
	{
		if ($this->EUI_Page->_get_query() != '') {
			return $this->EUI_Page->_getNo();
		}
	}


	/**
	 * @ def : add menu layout user navigation HHH 
	 *
	 */

	public function _MenuApplication()
	{
		static $config = array();

		$this->db->select('a.id, a.menu');
		$this->db->from('t_gn_application_menu a ');
		foreach ($this->db->get()->result_assoc() as $rows) {
			$config[$rows['id']] = $rows['menu'];
		}

		return $config;
	}

	/**
	 * @ def : add menu layout user navigation HHH 
	 * ---------------------------------------------------------
	 */


	public function _WorkProjectId()
	{

		static $config = array();

		$this->db->select('a.ProjectId, a.ProjectName');
		$this->db->from('t_lk_work_project a ');
		foreach ($this->db->get()->result_assoc() as $rows) {
			$config[$rows['ProjectId']] = $rows['ProjectName'];
		}

		return $config;
	}

	/**
	 * @ def : add menu layout user navigation HHH 
	 * ---------------------------------------------------------
	 */

	public function _WorkMenuApplication($ProjectId = null)
	{
		static $config = array();

		$this->db->select('*');

		$this->db->from('t_gn_menu_project a ');
		$this->db->join('t_gn_application_menu b ', ' a.MenuId=b.id', 'LEFT');
		$this->db->where('a.ProjectId', $ProjectId);
		foreach ($this->db->get()->result_assoc() as $rows) {
			$config[$rows['id']] = $rows['menu'];
		}

		return $config;
	}

	/**
	 * @ def : add menu layout user navigation HHH 
	 * ---------------------------------------------------------
	 */

	public function  _AddMenuWorkProject()
	{
		return array('WorkProjectId' => self::_WorkProjectId(), 'ApplicationMenu' => self::_MenuApplication());
	}


	/** 
	 * @ def  : delete menu on work project detected 
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/DeleteApplicationMenuByProject/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array )
	 * @ param : $ProjectId ( array ) 
	 */

	public function _DeleteApplicationMenuByProject()
	{
		$MenuId = $this->URI->_get_array_post('MenuId');
		$ProjectId = $this->URI->_get_post('ProjectId');

		$conds = 0;
		if (is_array($MenuId) and ($ProjectId) == TRUE) {
			foreach ($MenuId as $key => $Id) {
				if ($on = $this->db->delete('t_gn_menu_project', array('MenuId' => $Id, 'ProjectId' => $ProjectId))) {
					$conds++;
				}
			}
		}

		return $conds;
	}


	/** 
	 * @ def  : delete menu on work project detected 
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/AssignMenuWorkProjectById/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array )
	 * @ param : $ProjectId ( array ) 
	 */

	public function _AssignMenuWorkProjectById()
	{
		$product 	= $this->URI->_get_post('product');
		$question 	= $this->URI->_get_post('question');
		$answer 	= $this->URI->_get_post('answer');
		// print_r($this->URI->_get_post('product'));
		// print_r($this->URI->_get_post('question'));
		// print_r($this->URI->_get_post('answer'));
		// die;
		$this->db->reset_write();
		$this->db->set('product_id', $product);
		$this->db->set('questioner_type', '1');
		$this->db->set('questioner_flag', '1');
		$this->db->insert('t_gn_questioner');
		if ($this->db->affected_rows() > 0)
			$InsertId_quest = $this->db->insert_id();
		// print_r($InsertId);

		$this->db->reset_write();
		$this->db->set('survey_question', $InsertId_quest);
		$this->db->set('survey_quest_order', $product);
		$this->db->set('question_mandatory', '0');
		$this->db->insert('t_lk_question_survey');

		if ($this->db->affected_rows() > 0)
			$InsertId_qs = $this->db->insert_id();
		// print_r($InsertId_qs);

		$this->db->reset_write();
		$this->db->set('type_survey_id', $InsertId_quest);
		$this->db->set('ans_label', $product);
		$this->db->set('ans_rule_id', '0');
		$this->db->set('ans_order', '0');
		$this->db->insert('t_lk_type_ans_survey');

		if ($this->db->affected_rows() > 0)
			$InsertId_tas = $this->db->insert_id();
		// print_r($InsertId_tas);

		$this->db->reset_write();
		$this->db->set('survey_quest_id', $InsertId_qs);
		$this->db->set('type_ans_id', $InsertId_tas);
		$this->db->set('survey_flag', '1');
		$this->db->insert('t_lk_survey');

		$this->db->reset_write();
		$this->db->set('questioner_id', $InsertId_quest);
		$this->db->set('product_id', $product);
		$this->db->set('survey_id', '0');
		$this->db->insert('t_gn_prod_survey');



		// $insert_lbl = array(
		// 	'type_survey_id'=>$array_value['type_ans'],
		// 	'ans_label'=>$val_ans,
		// 	'ans_rule_id'=>$rule[$order_quest][$order_ans],
		// 	'ans_order'=>$order_ans
		// 	);
		// 	$ans_id = 0;
		// 	$_conds_ans = $this -> set_mysql_insert('t_lk_type_ans_survey', $insert_lbl);





		// die;



		// $conds = 0;
		// if( is_array($MenuId) AND ($ProjectId)==TRUE )
		// {
		// 	foreach( $MenuId as $key => $Id )
		// 	{
		// 		$this->db->set('MenuId', $Id, FALSE);
		// 		$this->db->set('ProjectId',$ProjectId, FALSE);
		// 		$this->db->set('MenuActive', 1, FALSE);
		// 		$this->db->set('CreateDateTs',date('Y-m-d H:i:s'));

		// 		$this->db->insert('t_gn_menu_project');

		// 		if( $this->db->affected_rows() > 0 )
		// 		{
		// 			$conds++;
		// 		}	
		// 	}
		// }

		return $conds;
	}


	/** 
	 * @ def  : delete menu on work project by rows in grid table  
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/RemoveApplicationMenuById/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array ) 
	 */

	public function _RemoveApplicationMenuById()
	{
		// print_r($this->URI->_get_post('WorkMenuId'));
		// die;

		$conds = 0;
		if ($this->URI->_get_post('WorkMenuId')) {
			$WorkMenuId = $this->URI->_get_array_post('WorkMenuId');
			if (is_array($WorkMenuId))
				foreach ($WorkMenuId as $key => $value) {
					// print_r($value);
					$this->db->delete('t_gn_questioner', array('questioner_id' => $value));
					// print_r($this->db->last_query());
					$this->db->delete('t_lk_question_survey', array('quest_id' => $value));
					$this->db->delete('t_lk_type_ans_survey', array('quest_id' => $value));
					// die;

					$conds++;
				}
			// die;
		}

		return $conds;
	}



	/** 
	 * @ def  : delete menu on work project by rows in grid table  
	 * @ url  : http://192.168.1.95/operation/index.php/MenuWorkProject/_ActiveWorkApplicationMenu/
	 *
	 * ------------------------------------------------------------------------------------------------- 
	 * @ param : $MenuId ( array ) 
	 */

	public function _ActiveWorkApplicationMenu($param = null, $activated = 0)
	{
		$conds = 0;
		// var_dump($param);
		// die;
		if (!is_null($param)) {
			if (is_array($param))
				foreach ($param as $key => $value) {
					// print_r($value)."<br>";
					// print_r($activated)."<br>";
					$this->db->set('questioner_flag', $activated);
					$this->db->where('questioner_id', $value);

					$this->db->update('t_gn_questioner');
					if ($this->db->affected_rows() > 0) {
						$conds++;
					}
				}
		}

		return $conds;
	}
}
