<?php
	Class M_RptStaffActivity extends EUI_Model
	{
		function M_RptStaffActivity()
		{
		
		}
		
		public static function & get_instance() 
		{
			if( is_null(self::$instance)) 
			{
				self::$instance = new self();
			}
			return self::$instance;
		}
		
		function NavMgr()
		{
			$mgr = array();
			$sql = "SELECT
						a.UserId,
						a.full_name
					FROM t_tx_agent a
					WHERE 1=1
						AND a.handling_type = 2
						AND a.mgr_id = 2
					GROUP BY a.UserId";
			$qry = $this -> db -> query($sql);
			foreach($qry -> result_assoc() as $rows)
			{
				$mgr[$rows['UserId']] = $rows['full_name'];
			}
			return $mgr;
		}
		
		function NavSpv($mgrId)
		{
			$spv = array();
			$sql = "SELECT
						a.UserId,
						a.full_name
					FROM t_tx_agent a
					WHERE 1=1 
						AND a.handling_type = 3
						AND a.mgr_id = ".$mgrId."
					GROUP BY a.UserId";
			$qry = $this -> db -> query($sql);
			foreach($qry -> result_assoc() as $rows)
			{
				$spv[$rows['UserId']] = $rows['full_name'];
			}
			return $spv;
		}
		
		function NavCCO($spvId)
		{
			$spv = array();
			$sql = "SELECT
						a.UserId,
						a.full_name
					FROM t_tx_agent a
					WHERE 1=1 
						AND a.handling_type = 4
						AND a.spv_id = ".$spvId."
					GROUP BY a.UserId";
			$qry = $this -> db -> query($sql);
			foreach($qry -> result_assoc() as $rows)
			{
				$spv[$rows['UserId']] = $rows['full_name'];
			}
			return $spv;
		}
	}
?>