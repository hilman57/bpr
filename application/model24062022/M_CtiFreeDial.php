<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_CtiFreeDial extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function M_CtiFreeDial(){
	$this -> load -> meta('_cc_extension_agent');
}
 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery
			("SELECT 
			 a.*, b.ProviderName, b.ProviderCode , c.full_name 
			 FROM coll_testcall_report a 
			 LEFT JOIN coll_misdn_provider b on a.ProviderId=b.ProviderId
			 LEFT JOIN t_tx_agent c on a.CallByUser=c.UserId "); 
	
	
	$this -> EUI_Page -> _setWhere();   
	if( $this -> EUI_Page -> _get_query() )
	{
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

	$this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
	$this -> EUI_Page->_setPage(10);
 
	$this -> EUI_Page -> _setQuery
			("select a.*, b.ProviderName, b.ProviderCode , c.full_name 
			 from coll_misdn_report a 
			 left join coll_misdn_provider b on a.ProviderId=b.ProviderId
			 left join t_tx_agent c on a.CallByUser=c.UserId"); 
	
			
	$this -> EUI_Page -> _setWhere();   
	$this -> EUI_Page -> _setOrderBy('a.CallId','DESC');
	$this -> EUI_Page -> _setLimit();
} 


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
}

?>