<?php
class M_MailMonitoring extends EUI_Model
{

function M_MailMonitoring() {
	$this -> load -> model(array('M_Configuration'));
}


function _getConfig()
{
	$config =& M_Configuration::get_instance();
	return $config -> _getMail(); // require mail 
}
// _getFaxqueueMonitoring

function _getMailqueueMonitoring()
{
	$_fax_queues = array();
	$this -> db -> select(" a.*, b.EmailStatusName, c.EmailSubject, 
		unix_timestamp(now()) - unix_timestamp(a.QueueCreateTs) as MailDuration, 
		d.full_name",FALSE);
		
	$this -> db -> from("email_queue a ");
	$this -> db -> join("email_status_refference b ", "a.QueueStatus = b.EmailStatusCode","LEFT");
	$this -> db -> join("email_outbox c "," a.QueueMailId=c.EmailOutboxId","LEFT");
	$this -> db -> join("t_tx_agent d "," d.UserId=c.EmailCreateById","LEFT");
	$this -> db -> order_by('a.QueueId','ASC');
	$num = 0;
	foreach( $this -> db ->get() -> result_assoc() as $rows )  
	{
		$_fax_queues[$num] = $rows;
		$num++;
	}
	
	return $_fax_queues;
}

}

?>