<?php
// -------------------------------------------------------------------------------------------
/*
 * @ pack : class compile / generate SDR report 
 * @ step process :
 * --------------------------------------------------------------------------------------------
 
	# Desc: Inisialisasi coll_sdr setiap tanggal 1
	# Update DIALER HOURS ke table cc_agent_activity_log untuk status >0
	# Update jumlah yang RPC
	# Update jumlah yang PTP
	# Update Jumlah yang bayar
	# Hitung jumlah broken promise(PTP-Bayar) rumus saja
	# Update jumlah uang pembayaran 
	
 */ 
// -------------------------------------------------------------------------------------------


class M_BatchCompileSdr extends EUI_Model
{

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack : property instance object 
 */

protected $arr_sdr_command = null;

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack : property instance object 
 */
 
private static $Instance = null;
 

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack : construct 
 */
 
 public function M_BatchCompileSdr()
{
  $this->arr_sdr_command = array ( 
	"_day" => date('d'), 
	"_periode" => date('Y-m') 
  );
  
  // end data 
  
 }

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : Instance of class 
 * @ aksess : public static methode   
 */
 
 public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
}


// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _Inialize will generate first date  on every month 
 * @ aksess : protected static methode   
 */
 
 protected function _Inialize()
{
 
 $_Inialize = FALSE;
 if( strcmp( $this->arr_sdr_command['_day'], '01') == 0 ){
	$_Inialize = TRUE;
 }
 return (bool)$_Inialize;

  
} 

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : Instance of class 
 * @ aksess : public static methode   
 */
 
 public function _SetCompileSdr()
{

  // @ pack : cek date on every month if schedular run --------------------------------------------
  
  $this->Periode =$this->arr_sdr_command['_periode'];
 
  if( $this->_Inialize() == TRUE ) 
  {
    
	$sql = "INSERT INTO t_gn_sdr(yearmonth, userid, agent_id, paidhours,workhours) 
            ( SELECT '{$this->Periode}', a.userid, a.id, 176, 176
				FROM cc_agent a LEFT JOIN t_tx_agent b on a.userid=b.id
				WHERE b.handling_type IN(4)  ) 
			ON DUPLICATE KEY UPDATE userid=a.userid, agent_id=a.id ";
			
	
	$this->db->query($sql);
	if( $this->db->affected_rows()<1 ) {
		if( preg_match("/\Dup/i", mysql_error() ) ) {
			print('Invalid query to inisialisasi data : ' . mysql_error()."\n");
			print($sql."\n");
	    }
	}// end  
	
	
  } 
  else 
  {
	// @ pack  : update if not date  = 01 --------------------------------------------
	// @ notes : please cek before generate its.
	$this->_SetUpdateSdr();
	
  }
  
  return TRUE; //--------------------- IS CALLBACK ONLY 
  
}
// _setCompileSdr --------------------



// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _LoginTime of on indetification 
 * @ aksess : public static methode   
 */
 
 protected function _LoginTime( $AgentId = 0 )
{
 
//@ pack : login time every user 
 $_LoginTime = 0;
 
 $this->db->reset_select();
 $this->db->select("SUM(UNIX_TIMESTAMP(end_time)- UNIX_TIMESTAMP(start_time)) as LOGIN_TIME", FALSE);
 $this->db->from("cc_agent_activity_log");
 $this->db->where("agent", "$AgentId");
 $this->db->where("status>0","", FALSE);
 $this->db->where("DATE_FORMAT(`start_time`,'%Y-%m')='{$this->Periode}'", "",  FALSE);
 $this->db->group_by("agent");
 // $this->db->print_out();
 $qry = $this->db->get();
 if( $qry->num_rows() > 0 ){
	$_LoginTime = $qry->result_singgle_value();
 }
 return $_LoginTime;
 
} // _LoginTime ==================> 

 
// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _RPC of on indetification 
 * @ aksess : protected static methode   
 */
 
 protected function _RPC( $AgentId = 0 )
{
 
//@ pack : counte RPC on Debitur on CH only 

 $_RPC = 0;
 
 $this->db->reset_select();
 $this->db->select("deb_agent, COUNT(deb_rpc) as TOTAL_RPC ", FALSE);
 $this->db->from("t_gn_debitur");
 $this->db->where("DATE_FORMAT(deb_call_activity_datets,'%Y-%m')='{$this->Periode}'", "",  FALSE);
 $this->db->where("deb_rpc", 1); // ketemu CH 
 $this->db->where("deb_agent",$AgentId);// filter on agent name not agent ID 
 $this->db->group_by("deb_agent");
 
 $qry = $this->db->get();
 if( ($qry->num_rows() > 0) 
	AND ( $rows = $qry->result_first_assoc()) )
 {
	$_RPC = (INT)$rows[TOTAL_RPC];	
}

 
 return $_RPC;
 
} // _RPC ==================> 


// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _PTP of on indetification 
 * @ aksess : protected static methode   
 */
 
 protected function _PTP( $AgentId = 0 )
{

//@ pack : counte RPC on Debitur on CH only 

 $_PTP = 0;
 
 $this->db->reset_select();
 $this->db->select("c.userid , count(a.ptp_id) as TOTAL_PTP ", FALSE);
 $this->db->from("t_tx_ptp a");
 $this->db->join("t_tx_agent b ","a.ptp_create_by_id=b.UserId","LEFT");
 $this->db->join("cc_agent c "," b.id=c.userid","LEFT");
 $this->db->where("DATE_FORMAT(ptp_date,'%Y-%m')='{$this->Periode}'","",FALSE);
 $this->db->where("c.userid", "$AgentId");
 $this->db->group_by("c.userid");
 
 $qry = $this->db->get();
 if( ($qry->num_rows() > 0) 
	AND ( $rows = $qry->result_first_assoc()) )
 {
	$_PTP = (INT)$rows['TOTAL_PTP'];	
}

 return $_PTP;
 
} // _PTP ==================> 

 

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _Payment of on indetification 
 * @ aksess : protected static methode   
 */
 
 protected function _SumPayment( $AgentId = 0 )
{

//@ pack : counte Payment on Debitur on CH only 

 $_Payment = 0;
 $this->db->reset_select();
 $this->db->select("c.userid, sum(a.pay_amount) as TOT_AMOUNT", FALSE);
 $this->db->from("t_gn_payment a");
 $this->db->join("t_tx_agent b ","a.pay_dc_id=b.UserId","LEFT");
 $this->db->join("cc_agent c ","b.id=c.userid","LEFT");
 $this->db->where("DATE_FORMAT(a.pay_date,'%Y-%m')='{$this->Periode}'","",FALSE);
 $this->db->where("c.userid", "$AgentId");
 $this->db->group_by("c.userid");
 
 $qry = $this->db->get();
 if( ($qry->num_rows() > 0) 
	AND ( $rows = $qry->result_first_assoc()) )
 {
	$_Payment = (INT)$rows['TOT_AMOUNT'];	
}
 return $_Payment;
 
} // _Payment ==================> 



// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _Payment of on indetification 
 * @ aksess : protected static methode   
 */
 
 protected function _TotPayment( $AgentId = 0 )
{

//@ pack : counte Payment on Debitur on CH only 

 $_Payment = 0;
 $this->db->reset_select();
 $this->db->select("c.userid, count(a.pay_id) as TOT_PAYMENT", FALSE);
 $this->db->from("t_gn_payment a");
 $this->db->join("t_tx_agent b ","a.pay_dc_id=b.UserId","LEFT");
 $this->db->join("cc_agent c ","b.id=c.userid","LEFT");
 $this->db->where("DATE_FORMAT(a.pay_date,'%Y-%m')='{$this->Periode}'","",FALSE);
 $this->db->where("c.userid", "$AgentId");
 $this->db->group_by("c.userid");
 
 $qry = $this->db->get();
 if( ($qry->num_rows() > 0) 
	AND ( $rows = $qry->result_first_assoc()) )
 {
	$_Payment = (INT)$rows['TOT_PAYMENT'];	
}
 return $_Payment;
 
} // _Payment ==================> 

 

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _setUpdateSdr of on indetification 
 * @ aksess : public static methode   
 */
 
 protected function _SetUpdateSdr()
{

 $this->arr_data = NULL;
 
// @ pack : start proces update --------------------------------------
	
 $this->db->reset_select();
 $this->db->select("a.agent_id,a.userid", FALSE);
 $this->db->from("t_gn_sdr a");
 $this->db->join("cc_agent b ","a.userid = b.userid", "INNER");
 $qry = $this->db->get();
 
// @ pack : get data query --------------------------------------------->
 
 $i = 0;
 if($qry->num_rows()>0)
	foreach( $qry->result_assoc() as $rows )
 {
	$this->arr_data[$i]["agentid"] = $rows["agent_id"];
	$this->arr_data[$i]["userid"] = $rows["userid"];
	$i++;	
 }
 
 

// @ pack : cek validation data type ------------------------------------>
 
 if( is_array($this->arr_data)  AND count($this->arr_data)> 0 ) 
	foreach( $this->arr_data as $_key => $_case) 
{
	
	$Username = $_case['userid'];
	if( strlen($Username)>0 )
	{
	
	// @ pack : login time every agent ---------------------------------------
	   $tot_logtime_agent = $this->_LoginTime( $_case['agentid'] );
	   
	// @ pack : agent RPC  ketemu sama CH ---------------------------------------
	   $tot_rpc_agent = $this->_RPC( $Username );
	   
	// @ pack :  total data yang PTP ---------------------------------------
	   $tot_ptp_agent = $this->_PTP( $Username );
	   
	// @ pack :  total data yang bayar  ---------------------------------------
	   $tot_payment_agent = $this->_TotPayment( $Username );
	   $sum_payment_agent = $this->_SumPayment( $Username );
	   
    // @ pack : total data BP ( broken PTP ) ---------------------------------------
	   $tot_bp_agent = 0;
	   if( ($tot_ptp_agent) 
			AND ($tot_payment_agent<= $tot_ptp_agent ) )
	  {
			$tot_bp_agent = $tot_ptp_agent - $tot_payment_agent;
	   }
	   
	// then update t_gn_sdr  ---------------------------------------   
	
	   $this->db->set("dialer_hours", $tot_logtime_agent, FALSE);
	   $this->db->set("rpc", $tot_rpc_agent);
	   $this->db->set("ptp", $tot_ptp_agent);
	   $this->db->set("bp", $tot_bp_agent);
	   $this->db->set("payment", $tot_payment_agent);
	   $this->db->set("collected", $sum_payment_agent);
	   $this->db->where("agent_id", $_case['agentid']);
	   $this->db->where("yearmonth", $this->Periode);
	   $this->db->update("t_gn_sdr");
	 
	   
	}
	
 } // end foreach ------------------------------------------------------>
 
 return TRUE; // ---------------------- is callback 

} 
// _setUpdateSdr  ================================>


// END CLASS 
}

?>