<?php
/*
 * @ pack  : M_SetLockMonitoring extends Model class 
 * @ date  : 2015-03-24 
 * ----------------------------------------------- 
 * @ param : class model 
 *
 */
 
class M_SetLockMonitoring extends EUI_Model
{

private $_lock_reason = array();

/*
 * @ pack : aksesor class model 
 * -------------------------------------------------
 */

private static $Instance = null;


/*
 * @ pack : aksesor class model 
 * -------------------------------------------------
 */
 
 public static function &Instance()
{
  if( is_null(self::$Instance) ) {
		self::$Instance = new self();
  }
  return self::$Instance;
}
 
 
/*
 * @ pack : _setLockHistory
 * -------------------------------------------------
 */
 
 public function _setLockHistory( $userid=0, $reason='ABSENSI', $count = 1 )
{
 
 $CallBack = 0;
 $ClsSysUser =& M_SysUser::Instance();
 $User = $ClsSysUser->getUserQuery(array('UserId'=> $userid ));
	
 if( is_array($User) )
	foreach( $User as $row )
 {	
   $this->db->set('User_id', $row['UserId']);
   $this->db->set('Leader_Id', $row['tl_id']);
   $this->db->set('Log_count', $count);
   $this->db->set('Log_reason', $reason);
   $this->db->set('Log_created_ts', date('Y-m-d H:i:s') );
   
   $this->db->insert('t_tx_dc_locking_log');
   
   if( $this->db->affected_rows() > 0 )  
   {
		$CallBack = $this->db->insert_id();
   }
 }
 
 return $CallBack;
	
}
 
/*
 * @ pack : aksesor class model 
 * -------------------------------------------------
 */
 
public function M_SetLockMonitoring() 
{ 
 
 $this->load->model(array('M_SysUser'));
 $this->_lock_reason = array('ABSENSI','CALL_LIMIT');	
}

/*
 * @ pack : aksesor class model 
 * -------------------------------------------------
 */
 
 public function _getSetLockMonitoring()
{
 // @ pack : reset select process cache 
 
  $this->db->reset_select();
  $this->db->select('COUNT(*) AS jml ', FALSE);
  $this->db->from("cc_call_session cs ");
  $this->db->where('cs.agent_id', _get_session('agentId'), FALSE);
  $this->db->where("((cs.status=3004 AND cs.start_time >= NOW() - INTERVAL 70 MINUTE) OR (cs.status IN (3005, 3003)  AND cs.end_time >= NOW() - INTERVAL 3 MINUTE
					  AND cs.start_time >= now() - interval 73 minute ) OR ( cs.status IN (3001, 3002) AND cs.start_time >= now() - interval 3 minute) )", "", FALSE);

  $qry = $this->db->get();
  if( $qry->num_rows() > 0 )
  {  
	return TRUE;
  } 
  else { 
	return FALSE; 
  }
  
  	
} // ==> _getSetLockMonitoring
 

/*
 * @ pack : aksesor class model 
 * -------------------------------------------------
 */
 
 public function _setLockUser()
{
  $this->db->set('user_state',0);
  $this->db->where('UserId',_get_session('UserId'));
  if( $this->db->update('t_tx_agent') ){
	 $this->_setLockHistory( _get_session('UserId'), 'CALL_LIMIT' );
	 return TRUE;
  } else {
	 return FALSE;
  }
  
} // ===> _setLockUser
 
// END CLASS 

}
?>