<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SetCampaignScript extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery(" SELECT a.CampaignCode, a.CampaignDesc FROM t_gn_campaign a
			 LEFT JOIN t_gn_campaign_script b on a.CampaignId=b.CampaignId
			 LEFT JOIN t_tx_agent c on b.UploadBy=c.UserId"); 
	
	$flt = '';
	if( $this -> URI -> _get_have_post('keywords'))
	{
		$flt.= " AND (
				b.ProductCode LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR b.ProductName LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.ScriptFlagStatus LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR c.full_name LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.ScriptFileName LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.Description LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.ScriptUpload LIKE '%{$this -> URI->_get_post('keywords')}%' 
				OR a.UploadDate	LIKE '%{$this -> URI->_get_post('keywords')}%' 
			)";	
	}				
  
	$this -> EUI_Page->_setWhere($flt);	

	// echo $this->EUI_Page->_getCompiler();	

	if($this -> EUI_Page -> _get_query())
	{
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
  $this -> EUI_Page ->_setQuery
		(
			"SELECT  b.CampaignCode,  b.CampaignDesc,
			IF( a.ScriptFlagStatus=1,'Active','Not Active') as ScriptFlagStatus, c.id, c.full_name, a.ScriptFileName, 
			a.Description, a.ScriptUpload, a.UploadDate, a.ScriptId
			FROM t_gn_campaign_script  a
			LEFT JOIN t_gn_campaign b on a.CampaignId=b.CampaignId
			LEFT JOIN t_tx_agent c on a.UploadBy=c.UserId" );
  
  $flt = '';
  if( $this -> URI -> _get_have_post('keywords') ) {
	$flt.= " AND ( a.ScriptFlagStatus LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR c.full_name LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.ScriptFileName LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.Description LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.ScriptUpload LIKE '%{$this -> URI->_get_post('keywords')}%' 
			OR a.UploadDate	LIKE '%{$this -> URI->_get_post('keywords')}%' 
		)";	
  }				
  
  $this -> EUI_Page->_setWhere($flt);
  
  if($this -> URI -> _get_have_post('order_by') )
  {
	$this -> EUI_Page->_setOrderBy($this -> URI -> _get_post('order_by'),$this -> URI -> _get_post('type'));	
  }
  $this -> EUI_Page->_setLimit();

  // echo $this->EUI_Page->_getCompiler();	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _setUpload( $_post =array() )
 {
	$this->db->set('CampaignId', $_post['post_data']['CampaignId']);
	$this->db->set('ScriptFileName', $_post['post_data']['md5file']);
	$this->db->set('Description', $_post['post_data']['ScriptTitle']);
	$this->db->set('ScriptFlagStatus', $_post['post_data']['Active']);
	$this->db->set('UploadDate', $this -> EUI_Tools ->_date_time());
	$this->db->set('UploadBy', $this -> EUI_Session ->_get_session('UserId'));
	
	$this ->db->insert('t_gn_campaign_script');
	
	if( $this ->db->affected_rows() > 0 ) 
		return true;
	else
		return false;
 }
 
  
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setActive($_post = null)
 {
	$_conds = 0;
	if(!is_null($_post))
	{
		foreach($_post['ScriptId'] as $keys => $ScriptId )
		{
			if( $this -> db -> update('t_gn_campaign_script', 
			 array( 'ScriptFlagStatus'=>$_post['Flags']), 
			 array( 'ScriptId' => $ScriptId)))
			{
				$_conds+=1;	
			}
		}
	}
	
	return $_conds;
 }

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _setDelete($_post = null)
 {
	$_conds = 0;
	if(!is_null($_post))
	{
		foreach($_post['ScriptId'] as $keys => $ScriptId )
		{
			if( $this -> db -> delete('t_gn_campaign_script',array('ScriptId'=>$ScriptId))) {
				$_conds+=1;	
			}
		}
	}
	
	return $_conds;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getScript()
 {
	$_conds = array();
	
	// $this ->db->select('a.*, b.CampaignName');
	// $this ->db->from('t_gn_campaign_script  a');
	// $this ->db->join('t_gn_campaign b ','a.CampaignId=b.CampaignId','LEFT');
	// $this ->db->join('t_tx_agent c ','a.UploadBy=c.UserId','LEFT');
	// $this ->db->join('t_gn_debitur d ','a.CampaignId=d.CampaignId','LEFT');
	// $this ->db->join('t_gn_campaign_project e ','a.CampaignId=e.CampaignId','LEFT');
	
	//handle by project ID **/
	
	// $this->db->where_in('e.ProjectId', $this->EUI_Session->_get_session('ProjectId'));
	
    // if( $this ->URI->_get_have_post('CampaignId') )	
		// $this ->db->where('a.CampaignId', $this->URI->_get_post('CampaignId')); 
	
	 // if( $this ->URI->_get_have_post('CustomerId') )	
		// $this ->db->where('d.CustomerId', $this->URI->_get_post('CustomerId'));
	
	// if( $this ->URI->_get_have_post('ProjectId') )	
		// $this ->db->where('e.ProjectId', $this->URI->_get_post('ProjectId'));
		
	
	// foreach( $this -> db -> get()->result_assoc()  as $rows )
	// {
		// $_conds[$rows['ScriptId']] = $rows['Description'];
	// }
	
	return $_conds;
 }
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _getDataScript($ScriptId=0)
 {	
	$_conds = array();
	
	$this -> db-> select('a.*');
	$this -> db-> from('t_gn_campaign_script a');
	$this -> db-> where('a.ScriptId',$ScriptId);
	if( $rows = $this -> db->get()-> result_first_assoc() )
	{
		$_conds = $rows;
	}
	
	return $_conds;
 }
 
 
 
 
}

?>