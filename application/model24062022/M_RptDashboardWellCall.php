<?php
class M_RptDashboardWellCall extends EUI_Model
{
	function M_RptDashboardWellCall()
	{
		
	}
	
	function _getReportType()
	{
		return array(
			'type_perform' => 'Performance Caller',
			'type_campaign' => 'Campaign Productivity',
			'type_status' => 'Utilize Customer'
		);
	}
	
	/* --------------------------------------------------------------------------------------------------------------------------- */
	
	function _getActivityAgent($param, $action)
	{
		$datas = array();
		
		$sql = "select 
					date(a.ActivityDateTs) as ActivityDateTs,
					a.UserId,
					a.ActivityAction,
					max(a.ActivityDateTs) as TimeAgent 
				from t_tx_agent_activity a
				where a.ActivityAction = '".$action."'
				AND DATE(a.ActivityDateTs) >= '".$param['start_date']."'
				AND DATE(a.ActivityDateTs) <= '".$param['end_date']."'
				AND a.UserId IN (".$param['AgentId'].") 
				group by date(a.ActivityDateTs), a.UserId";
		
		$qry = $this->db->query($sql);
		if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
		{
			foreach($qry->result_assoc() as $rows)
			{
				$datas[$rows['ActivityDateTs']][$rows['UserId']] = date("H:i:s", strtotime($rows['TimeAgent']));
			}
		}
		else
		{
			die('Data Unavaliable for this criterias.');
		}
		
		
		return $datas;
	}
	
	function _getTotalData($param){
		$datas = array();
		$agentID = explode(",",$param['AgentId']);
		foreach($agentID as $agent){
			$sql = "select COUNT(a.PolicyId) as totaldata from t_gn_policy_detail a
					left join t_gn_debitur b ON a.EnigmaId = b.CustomerNumber
					left join t_gn_assignment c ON c.CustomerId = b.CustomerId
					where c.AssignSelerId IN (".$agent.")
					";
					// AND DATE(a.ActivityDateTs) >= '".$param['start_date']."'
					// AND DATE(a.ActivityDateTs) <= '".$param['end_date']."'
			
			$qry = $this->db->query($sql);
			if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
			{
				foreach($qry->result_assoc() as $rows)
				{
					$datas['totaldata'][$agent] = $rows['totaldata'];
				}
			}
		}
		return $datas;
	}
	
	function _getNewData($param){
		$datas = array();
		$agentID = explode(",",$param['AgentId']);
		foreach($agentID as $agent){
			$sql = "select COUNT(a.PolicyId) as newdata from t_gn_policy_detail a
					left join t_gn_debitur b ON a.EnigmaId = b.CustomerNumber
					left join t_gn_assignment c ON c.CustomerId = b.CustomerId
					where 1=1 AND (a.CallReasonId is null OR a.CallReasonId = '0')
					AND b.CallReasonId = '0' AND c.AssignSelerId IN (".$agent.")
					";
					// AND DATE(a.ActivityDateTs) >= '".$param['start_date']."'
					// AND DATE(a.ActivityDateTs) <= '".$param['end_date']."'
			
			$qry = $this->db->query($sql);
			if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
			{
				foreach($qry->result_assoc() as $rows)
				{
					$datas['newdata'][$agent] = $rows['newdata'];
				}
			}
		}
		return $datas;
	}
	
	function _getPendingData($param){
		$datas = array();
		$agentID = explode(",",$param['AgentId']);
		foreach($agentID as $agent){
			$sql = "select count(a.PolicyId) as Pending from t_gn_policy_detail a
					left join t_gn_debitur b ON a.EnigmaId = b.CustomerNumber
					left join t_gn_assignment c ON c.CustomerId = b.CustomerId
					where 1=1 AND (a.CallComplete = '0' OR b.CallComplete = '0')
					AND b.CallReasonId <> '0' AND c.AssignSelerId IN (".$agent.")
					";
					// AND DATE(a.ActivityDateTs) >= '".$param['start_date']."'
					// AND DATE(a.ActivityDateTs) <= '".$param['end_date']."'
			
			$qry = $this->db->query($sql);
			if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
			{
				foreach($qry->result_assoc() as $rows)
				{
					$datas['Pending'][$agent] = $rows['Pending'];
				}
			}
		}
		return $datas;
	}
	
	function _getCompleteData($param){
		$datas = array();
		$agentID = explode(",",$param['AgentId']);
		foreach($agentID as $agent){
			$sql = "select count(a.PolicyId) as Complete from t_gn_policy_detail a
					left join t_gn_debitur b ON a.EnigmaId = b.CustomerNumber
					left join t_gn_assignment c ON c.CustomerId = b.CustomerId
					where 1=1 AND a.CallComplete = '1' AND b.CallComplete = '1'
					AND b.CallReasonId <> '0' AND c.AssignSelerId IN (".$agent.")
					";
					// AND DATE(a.ActivityDateTs) >= '".$param['start_date']."'
					// AND DATE(a.ActivityDateTs) <= '".$param['end_date']."'
			
			$qry = $this->db->query($sql);
			if( ($qry!==FALSE) AND ($qry->num_rows() > 0) )
			{
				foreach($qry->result_assoc() as $rows)
				{
					$datas['Complete'][$agent] = $rows['Complete'];
				}
			}
		}
		return $datas;
	}
}
?>