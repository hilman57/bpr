<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetBenefit modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
 
class M_SrcRound extends EUI_Model
{

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function M_SrcRound() 
{
	$this->load->model(array(
	'M_SetCallResult','M_SysUser','M_SetCampaign','M_MgtRandDeb'));
}


// _NextActivity 

public function _NextActivity()
{
    $next_activity = array();
	$this->_get_default();
	$sql = $this->EUI_Page->_getCompiler();
	if( $sql ) 
	{
		$qry = $this->db->query($sql);
		$num = 0;
		foreach( $qry->result_assoc() as $rows ){
			$next_activity[] = $rows['CustomerId'];  
			$num++;
		}
	}
	
	return $next_activity;
	
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAccountStatus()
{
 $_account_status = null;
 if( is_null($_account_status) )
 {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) {
		$_account_status[$AccountStatusCode] = $rows['name'];
	}	
 }  
	return $_account_status;
 }
 
 

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getLastCallStatus() 
{
  $_last_call_status = null;
  if( is_null($_account_status) )
  {
	$result = $this->M_SetCallResult->_getCallReasonId();
	foreach( $result as $AccountStatusCode => $rows ) 
	{
		$_last_call_status[$AccountStatusCode] = $rows['name'];
	}	
  }   

   return $_last_call_status;
	
} 


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_default() 
{
	$this->EUI_Page->_setPage(10); 
	
	$this->EUI_Page->_setSelect("b.deb_id");
	$this->EUI_Page->_setFrom("t_gn_modul_setup a");
	$this->EUI_Page->_setJoin("t_gn_buckettrx_debitur b","a.modul_setup_id=b.modul_setup_id","INNER"); 
	$this->EUI_Page->_setJoin("t_gn_debitur c","b.deb_id = c.deb_id","INNER"); 
	$this->EUI_Page->_setJoin("t_gn_round_assign d","b.bucket_trx_id = d.bucket_trx_id","INNER"); 
	$this->EUI_Page->_setJoin("t_gn_campaign e","c.deb_cmpaign_id=e.CampaignId","LEFT"); 
	$this->EUI_Page->_setJoin("t_gn_assignment f","c.deb_id=f.CustomerId","INNER"); 
	$this->EUI_Page->_setJoin("t_tx_agent g","f.AssignSelerId=g.UserId","LEFT"); 
	$this->EUI_Page->_setJoin("t_lk_account_status h","c.deb_call_status_code = h.CallReasonCode","LEFT"); 
	$this->EUI_Page->_setJoin("t_lk_account_status i","c.deb_prev_call_status_code = i.CallReasonCode","LEFT"); 
	$this->EUI_Page->_setJoin("t_tx_agent j","d.agent_id=j.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_st_round k","d.st_round_id=k.st_round_id","LEFT",TRUE);
	
	
	// @ pack : filter by default is agent ---------------------------------------------------------------------------------
	if( in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		//$akses="(d.access_type_id = 1 OR IF(d.access_type_id = 2,agt_to.tl_id ={$this -> EUI_Session ->_get_session('TeamLeaderId')},NULL) OR IF(d.access_type_id=3,agt_to.UserId={$this -> EUI_Session ->_get_session('UserId')},NULL))";
		$this->EUI_Page->_setAnd( "d.agent_id", $this -> EUI_Session ->_get_session('UserId') );
	}
	
	// @ pack : filter by default is leader ---------------------------------------------------------------------------------
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		//$akses="(d.access_type_id = 1 OR IF(d.access_type_id = 2,agt_to.tl_id ={$this -> EUI_Session ->_get_session('UserId')}, NULL) OR IF(d.access_type_id=3,agt_to.tl_id={$this -> EUI_Session ->_get_session('UserId')}, NULL))";
		$this->EUI_Page->_setAnd( "j.tl_id", $this -> EUI_Session ->_get_session('UserId') );
	}
	
	$this->EUI_Page->_setAnd("a.mod_set_running","1");
	$this->EUI_Page->_setAnd("c.deb_is_access_all","1");
	$this->EUI_Page->_setAnd("k.bool_running","1");
	$this->EUI_Page->_setAnd("b.claim_flag","0");
	
	
// @ pack : set cache on page **/

	$this->EUI_Page->_setAndCache('c.deb_acct_no', 'aksesall_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_cmpaign_id', 'aksesall_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('c.deb_call_status_code', 'aksesall_account_status', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_prev_call_status_code', 'aksesall_call_status', TRUE);
	$this->EUI_Page->_setAndCache('f.AssignSelerId', 'aksesall_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_resource', 'aksesall_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'aksesall_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("DATE(c.deb_call_activity_datets)>='". _getDateEnglish(_get_post('aksesall_start_date')) ."'", 'aksesall_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("DATE(c.deb_call_activity_datets)<='". _getDateEnglish(_get_post('aksesall_end_date')) ."'", 'aksesall_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("c.deb_amount_wo>=". _get_post('aksesall_start_amountwo') ."", 'aksesall_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("c.deb_amount_wo<=". _get_post('aksesall_end_amountwo')."", 'aksesall_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('b.deb_id');
	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	// $this->EUI_Page->_setLimit();
	 // echo $this ->EUI_Page->_getCompiler();
	if($this->EUI_Page->_get_query()) {
		return $this->EUI_Page;
	}
}


/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_content()
{
	
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(10);
	
	/** default of query ***/

	
	$this->EUI_Page->_setArraySelect(array(
		'b.deb_id AS CustomerId'=> array('CustomerId','ID','primary'),
		'b.bucket_trx_id AS BucketRandomId'=> array('BucketRandomId','BucketRandomId','hidden'),
		'e.CampaignDesc AS CampaignDesc'=>array('CampaignDesc','Product'),
		'c.deb_acct_no AS AccountNumber'=>array('AccountNumber','Customer ID'),
		'c.deb_name AS CustomerName'=>array('CustomerName','Customer Name'),
		'g.id AS AgentId'=>array('AgentId','Agent Id'),
		'h.CallReasonDesc AS AccountStatus'=>array('AccountStatus','Account Status'),
		'i.CallReasonDesc AS CallStatus'=>array('CallStatus','Last Call'),
		'c.deb_call_activity_datets AS LastCallDate'=>array('LastCallDate','Last Call Date'),
		'c.deb_resource AS Recsource'=>array('Recsource','Recsource'),
		'c.deb_amount_wo AS AmountWO'=>array('AmountWO','Amount WO'),
		'c.deb_bal_afterpay AS BalanceAffterPay'=>array('BalanceAffterPay','Bal. After Pay'),
		'c.deb_reminder AS History'=>array('History','History')
	)); 
	$this->EUI_Page->_setFrom("t_gn_modul_setup a");
	$this->EUI_Page->_setJoin("t_gn_buckettrx_debitur b","a.modul_setup_id=b.modul_setup_id","INNER"); 
	$this->EUI_Page->_setJoin("t_gn_debitur c","b.deb_id = c.deb_id","INNER"); 
	$this->EUI_Page->_setJoin("t_gn_round_assign d","b.bucket_trx_id = d.bucket_trx_id","INNER"); 
	$this->EUI_Page->_setJoin("t_gn_campaign e","c.deb_cmpaign_id=e.CampaignId","LEFT"); 
	$this->EUI_Page->_setJoin("t_gn_assignment f","c.deb_id=f.CustomerId","INNER"); 
	$this->EUI_Page->_setJoin("t_tx_agent g","f.AssignSelerId=g.UserId","LEFT"); 
	$this->EUI_Page->_setJoin("t_lk_account_status h","c.deb_call_status_code = h.CallReasonCode","LEFT"); 
	$this->EUI_Page->_setJoin("t_lk_account_status i","c.deb_prev_call_status_code = i.CallReasonCode","LEFT"); 
	$this->EUI_Page->_setJoin("t_tx_agent j","d.agent_id=j.UserId","LEFT");
	$this->EUI_Page->_setJoin("t_st_round k","d.st_round_id=k.st_round_id","LEFT",TRUE);
	
	
	// @ pack : filter by default is agent ---------------------------------------------------------------------------------
	if( in_array( $this->EUI_Session->_get_session('HandlingType'), 
		array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
	{
		//$akses="(d.access_type_id = 1 OR IF(d.access_type_id = 2,agt_to.tl_id ={$this -> EUI_Session ->_get_session('TeamLeaderId')},NULL) OR IF(d.access_type_id=3,agt_to.UserId={$this -> EUI_Session ->_get_session('UserId')},NULL))";
		$this->EUI_Page->_setAnd( "d.agent_id", $this -> EUI_Session ->_get_session('UserId') );
	}
	
	// @ pack : filter by default is leader ---------------------------------------------------------------------------------
	if( in_array( $this -> EUI_Session -> _get_session('HandlingType'),
		array(USER_LEADER) ) )
	{
		//$akses="(d.access_type_id = 1 OR IF(d.access_type_id = 2,agt_to.tl_id ={$this -> EUI_Session ->_get_session('UserId')}, NULL) OR IF(d.access_type_id=3,agt_to.tl_id={$this -> EUI_Session ->_get_session('UserId')}, NULL))";
		$this->EUI_Page->_setAnd( "j.tl_id", $this -> EUI_Session ->_get_session('UserId') );
	}
	
	$this->EUI_Page->_setAnd("a.mod_set_running","1");
	$this->EUI_Page->_setAnd("c.deb_is_access_all","1");
	$this->EUI_Page->_setAnd("k.bool_running","1");
	$this->EUI_Page->_setAnd("b.claim_flag","0");
	
		
	
// @ pack : set cache on page **/
	
	$this->EUI_Page->_setAndCache('c.deb_acct_no', 'aksesall_cust_id', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_cmpaign_id', 'aksesall_campaign_id',TRUE);
	$this->EUI_Page->_setAndCache('c.deb_call_status_code', 'aksesall_account_status', TRUE);
	$this->EUI_Page->_setAndCache('c.deb_prev_call_status_code', 'aksesall_call_status', TRUE);
	$this->EUI_Page->_setAndCache('f.AssignSelerId', 'aksesall_agent_id', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_resource', 'aksesall_recsource', TRUE);
	$this->EUI_Page->_setLikeCache('c.deb_name', 'aksesall_cust_name', TRUE);
	
// @ pack : start date ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setAndOrCache("DATE(c.deb_call_activity_datets)>='". _getDateEnglish(_get_post('aksesall_start_date')) ."'", 'aksesall_start_date', TRUE);
	$this->EUI_Page->_setAndOrCache("DATE(c.deb_call_activity_datets)<='". _getDateEnglish(_get_post('aksesall_end_date')) ."'", 'aksesall_end_date', TRUE);
	
// @ pack : amount wo  ---------------------------------------------------------------------------------

	$this->EUI_Page->_setAndOrCache("c.deb_amount_wo>=". _get_post('aksesall_start_amountwo') ."", 'aksesall_start_amountwo', TRUE);
	$this->EUI_Page->_setAndOrCache("c.deb_amount_wo<=". _get_post('aksesall_end_amountwo')."", 'aksesall_end_amountwo', TRUE);
	
// @ pack : group by  ---------------------------------------------------------------------------------
	
	$this->EUI_Page->_setGroupBy('b.deb_id');
	
/* set order by **/
	if( $this->URI->_get_have_post('order_by')) {
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}	
	
	$this->EUI_Page->_setLimit();
	// echo $this ->EUI_Page->_getCompiler();
}
/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getCampaignByActive() 
{
 $_is_active = null;
 if( is_null($_is_active)) {
	$_is_active = $this->M_SetCampaign->_get_campaign_name();
 }
	
 return $_is_active;
	
}

/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _get_resource()
{
	self::_get_content();
	if(($this->EUI_Page->_get_query()!=="" )) 
	{
		return $this->EUI_Page->_result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getGenderId()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select("a.GenderId, a.Gender",FALSE);
  $this->db->from("t_lk_gender a");
  
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['GenderId']] = $rows['Gender'];
  }
  
  return $_conds;
} 

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _getCardType()
{
  $_conds = array();
  
  $this->db->reset_select();
  $this->db->select(" a.CardTypeId, a.CardTypeDesc",FALSE);
  $this->db->from("t_lk_cardtype  a");
  $this->db->where("a.CardTypeFlag",1);
   
  $qry = $this->db->get();
  if( $qry->num_rows() > 0) foreach( $qry->result_assoc() as $rows ) {
	$_conds[$rows['CardTypeId']] = $rows['CardTypeDesc'];
  }
  
  return $_conds;
} 


/*
 * @ def : _getCustomerByOtherNumberCIF 
 * --------------------------------------------------
 */
 
 public function _getDeskollByLogin() 
{
  $Deskoll = array();
  $_array_select = $this->M_SysUser->_get_user_by_login();
  foreach( $_array_select as $UserId => $rows ) 
  {
	$Deskoll[$UserId] = $rows['id'] ." - ". $rows['full_name'];
  }
  
  return $Deskoll;
  
 }
	/*
	 * @ def : CheckAccessAll 
	 * --------------------------------------------------
	 */
	public function CheckRound()
	{
		$conds_back = false;
		if($this->URI->_get_have_post('CustomerId'))
		{
			if( in_array( $this->EUI_Session->_get_session('HandlingType'), 
				array(USER_AGENT_INBOUND, USER_AGENT_OUTBOUND) ) )
			{
				$DebiturId = $this->URI->_get_post('CustomerId');
				$bucket_trx_id = $this->URI->_get_post('BucketRandomId');
				$deb_random = $this->M_MgtRandDeb->CheckDebiturIsRandom($bucket_trx_id);
				if(isset($deb_random['round_id']) AND count($deb_random['round_id']) > 0 AND !empty($deb_random['round_id']))
				{
					$this->db->reset_select();
					$this->db->where('round_id', $deb_random['round_id'] );
					$this->db->where('agent_id', $this -> EUI_Session ->_get_session('UserId') );
					$this->db->from('t_gn_round_assign');
					//echo $this->db->last_query();
					if ( $this->db->count_all_results() > 0 )
					{
						$this->db->set("deb_is_call",1);
						$this->db->where("deb_id", $DebiturId );
						if( $this->db->update("t_gn_debitur") )
						{
							$conds_back = true;
						}
					}	
				}
			}
			else
			{
				$conds_back = true;
			}
			
		}
		return $conds_back;
	}
	
	// private function expiredDebitur()
	// {
		// $this->db->where('round_id', );
		// $this->db->from('t_gn_round_assign');
		// echo $this->db->count_all_results();
	// }
	
	/*
	 * @ def : UpdateAccessKeys 
	 * --------------------------------------------------
	 */
	 
	public function UpdateAccessKeys()
	{
		$conds_back = false;
		if($this->URI->_get_have_post('CustomerId'))
		{
			$DebiturId = $this->URI->_get_post('CustomerId');
			$bucket_trx_id = $this->URI->_get_post('BucketRandomId');
			$deb_access_all = $this->M_MgtRandDeb->CheckDebiturIsRandom($bucket_trx_id);
			if(isset($deb_access_all['access_all_id']) AND count($deb_access_all['access_all_id']) > 0)
			{
				$where= array ( 'access_all_id'=>$deb_access_all['access_all_id'] );
				$this->db->set('access_follow', NULL);
				$this->db->where($where);
				$this->db->update('t_gn_access_all');
				if($this->db->affected_rows()>0)
				{
					$this->db->set("deb_is_call",0);
					$this->db->where("deb_id", $DebiturId);
					if( $this->db->update("t_gn_debitur") )
					{
						$conds_back = true;
					}
				}
			}
		}		
		return $conds_back;
	}

}

// END OF CLASS 



?>