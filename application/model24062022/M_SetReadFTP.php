<?php
/*
 * E.U.I 
 *
 
 * subject	: M_SetLastCall modul 
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/M_Utility/
 */
 
class M_SetReadFTP extends EUI_Model
{


 
/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(10); 
	
	$this -> EUI_Page -> _setQuery("SELECT * FROM t_lk_ftp_read a"); 
	
	$filter = '';
	
	if( $this->URI->_get_have_post('key_words') ) 
	{
		$filter ="";
	}				
			
	$this -> EUI_Page -> _setWhere( $filter );   
	if( $this -> EUI_Page -> _get_query() ) {
		return $this -> EUI_Page;
	}
}

/*
 * @ def 		: __construct // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_content()
{

  $this -> EUI_Page->_postPage( $this -> URI -> _get_post('v_page') );
  $this -> EUI_Page->_setPage(10);
  $this -> EUI_Page ->_setQuery( "
				SELECT * 
				FROM t_lk_ftp_read a 
				LEFT JOIN t_lk_work_project b ON a.ftp_read_project_code=b.ProjectCode
				LEFT JOIN t_gn_template c ON a.ftp_read_template_Id=c.TemplateId" );
  $filter = '';
  if( $this -> URI -> _get_have_post('key_words') ) {
	$filter = " ";	
  }				
		
  $this -> EUI_Page->_setWhere();
  $this -> EUI_Page->_setLimit();
}


/*
 * @ def 		: _get_resource // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_resource()
 {
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') 
	{
		return $this -> EUI_Page -> _result();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_page_number() 
 {
	if( $this -> EUI_Page -> _get_query()!='' ) {
		return $this -> EUI_Page -> _getNo();
	}	
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 
/** get detail data **/

public function _getFTPRead( $ftp_read_id = 0  ) 
{
	$conds = array();
	
	$this ->db ->select('*');
	$this ->db ->from('t_lk_ftp_read');
	$this ->db ->where('ftp_read_id', $ftp_read_id);
	if( $rs = $this ->db->get() -> result_first_assoc() ) {
		$conds = $rs;
	}
	
	return $conds;
}
 
 
 /* function get Product code **/
 
 public  function _getWorkProjectCode()
 {
	$works = array();
	
	$this->db->select('*');
	$this->db->from('t_lk_work_project');
	$this->db->where('ProjectFlags',1);
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$works[$rows['ProjectCode']] = $rows['ProjectName'];
	}
	
	return $works;
	
 }
 
 
  /* function get TemplateName **/
 
 public function _getTemplateName() 
 {
	$works = array();
	
	$this->db->select('*');
	$this->db->from('t_gn_template');
	$this->db->where('TemplateFlags',1);
	
	foreach( $this->db->get()->result_assoc() as $rows )
	{
		$works[$rows['TemplateId']] = $rows['TemplateName'];
	}
	
	return $works;
	
 }
 
 /* _setFTPSave **/
 
 public function _setFTPUpdate( $params = null )
 {
	$conds = 0;
	if( !is_null($params) )
	{
		foreach( $params as $fieldname => $fieldvalue ) 
		{
			if( $fieldname!='ftp_read_id' ){
				$this->db->set($fieldname, $fieldvalue);
			}
			else{
				$this->db->where($fieldname, $fieldvalue);
			}
		}
		
		$this ->db->update('t_lk_ftp_read');
		if($this ->db->affected_rows() > 0)
			$conds++;
	}
	
	return $conds;
 }
 
 
 /* _setFTPSave **/
 
 public function _setFTPSave( $params = null )
 {
	$conds = 0;
	if( !is_null($params) )
	{
		foreach( $params as $fieldname => $fieldvalue ) {
			$this->db->set($fieldname, $fieldvalue);
		}
		
		$this->db->set('ftp_read_createts', date('Y-m-d H:i:s'));
		$this->db->insert('t_lk_ftp_read');
		
		if($this ->db->affected_rows() > 0)
			$conds++;
	}
	
	return $conds;
 }
 
 
 // ** _setFTPDelete **/
 
 public function _setFTPDelete($Id  = null )
 {
	$conds = 0;
	if( !is_null($Id) ) 
	{
		foreach( $Id  as $k => $fieldId )
		{
			$this->db->where('ftp_read_id', $fieldId);
			$this->db->delete('t_lk_ftp_read');
			if( $this->db->affected_rows() > 0 ){
				$conds++;
			}
		}
	}
	
	return $conds;
	
 } 
 
}

?>