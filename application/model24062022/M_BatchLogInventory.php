<?php
// -------------------------------------------------------------------------------------------
/*
 * @ pack : class compile / Inventory Log
 * @ step process :
 * --------------------------------------------------------------------------------------------
 
	# Desc: Inisialisasi coll_sdr setiap tanggal 1

	
 */ 
// -------------------------------------------------------------------------------------------


class M_BatchLogInventory extends EUI_Model
{

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack : property instance object 
 */

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack : property instance object 
 */
 
private static $Instance = null;
 

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack : construct 
 */
 
 public function M_BatchLogInventory()
{

  
}

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : Instance of class 
 * @ aksess : public static methode   
 */
 
 public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
}


// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : _Inialize will generate first date  on every month 
 * @ aksess : protected static methode   
 */
 
 protected function _Inialize()
{
 
	$_Inialize = TRUE;
	return (bool)$_Inialize;
  
} 

// ------------------------------------------------------------------------------------------- 
/*
 * @ pack   : Instance of class 
 * @ aksess : public static methode   
 */
 
 public function _SetLogInventory()
{

  // @ pack : cek date on every month if schedular run --------------------------------------------
  $this->arr_data = NULL;
  if( $this->_Inialize() == TRUE ) 
  {
  	$date = date('Y-m-d');
  	echo $date." ";
  	$sql = "SELECT COUNT(a.deb_id) AS inventory, SUM(a.deb_wo_amount) AS balance
  				FROM t_gn_debitur a";

			
	$dt = $this->db->query($sql);
	foreach ($dt->result_assoc() as $key) {
		$this->arr_data["inventory"] = $key["inventory"];
		$this->arr_data["balance"] = $key["balance"];
	}


	$sql = "SELECT COUNT(a.CallHistoryId) AS attempt, COUNT(IF(a.CallAccountStatus = '103' OR a.CallAccountStatus = '104' OR a.CallAccountStatus = '105' OR a.CallAccountStatus = '106' OR a.CallAccountStatus = '107' OR a.CallAccountStatus = '108', 1, NULL)) AS rpc FROM t_gn_callhistory a where DATE_FORMAT(a.CallHistoryCreatedTs, '%Y-%m-%d') = '$date' AND a.AgentCode <> ''";

			
	$dt = $this->db->query($sql);
	foreach ($dt->result_assoc() as $key) {
		$this->arr_data["attempt"] = $key["attempt"];
		$this->arr_data["rpc"] = $key["rpc"];
	}

	$sql = "SELECT COUNT(a.ptp_id) as PTP, SUM(a.ptp_amount) AS jml_ptp
  				FROM t_tx_ptp a
  				where DATE_FORMAT(a.ptp_create_ts, '%Y-%m-%d') = '$date'";

			
	$dt = $this->db->query($sql);
	foreach ($dt->result_assoc() as $key) {
		$this->arr_data["PTP"] = $key["PTP"];
		$this->arr_data["jml_ptp"] = $key["jml_ptp"];
	}

	$sql = "SELECT SUM(a.pay_amount) AS payment, COUNT(a.pay_id) AS acc
  				FROM t_gn_payment a
  				where DATE_FORMAT(a.pay_created_ts, '%Y-%m-%d') = '$date'";

			
	$dt = $this->db->query($sql);
	foreach ($dt->result_assoc() as $key) {
		$this->arr_data["payment"] = $key["payment"];
		$this->arr_data["acc"] = $key["acc"];
	}


	print_r($this->arr_data);

	$sql = "INSERT INTO t_gn_inventory_log(inventory, balance, attempt, rpc, ptp, jml_ptp, payment, acc, log_ts) VALUES (
			'".$this->arr_data["inventory"]."','".$this->arr_data["balance"]."','".$this->arr_data["attempt"]."','".$this->arr_data["rpc"]."','".$this->arr_data["PTP"]."','".$this->arr_data["jml_ptp"]."','".$this->arr_data["payment"]."','".$this->arr_data["acc"]."','".date('Y-m-d H:i:s')."')"; //".date('Y-m-d H:i:s')."
	$this->db->query($sql);
	if( $this->db->affected_rows()<1 ) {
		if( preg_match("/\Dup/i", mysql_error() ) ) {
			print('Invalid query to inisialisasi data : ' . mysql_error()."\n");
			print($sql."\n");
	    }          
	}// end  
	
	
  } 
  
  return TRUE; //--------------------- IS CALLBACK ONLY 
  
}
// _SetLogInventory --------------------


// END CLASS 
}

?>