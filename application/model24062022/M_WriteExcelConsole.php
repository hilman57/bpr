<?php
class M_WriteExcelConsole extends EUI_Model
{


/** constructor  of class ****/

public function M_WriteExcelConsole() {
	$this->load->model(array('M_Configuration'));
 }
 
// STOP::INDEX	

/**
 *
 * @ def : get atcive group only to generateed report daily source 
 * -----------------------------------------------------------------------
 * @ param : -
 * @ param : -
 *
 **/
 
public function _getGroupByActive()
{
	$_arrs_groups = array();
	
	$this ->db->select('a.FuGroupCode, a.FuMailAddress');
	$this ->db->from('t_lk_followupgroup a');
	$this ->db->where('a.FuGroupShow', 'Y');
	foreach( $this->db->get()->result_assoc() as $rows ){
		$_arrs_groups[$rows['FuGroupCode']] = array( 'code' => $rows['FuGroupCode']);
	}
	
	return $_arrs_groups;
}

/**
 *
 * @ def : get counter size data if true generateed 
 * -----------------------------------------------------------------------
 * @ param : -
 * @ param : -
 *
 **/
 
public function _getCountByGroup($GroupCode =0, $date = '0000-00-00')
{
	$_arrs_groups = 0;
	
	$this ->db->select('count(a.FuId) as jumlah', FALSE);
	$this ->db->from('t_gn_followup a');
	$this ->db->where('a.FuGroupCode',$GroupCode);
	$this ->db->where('date(a.FuCreatedDate)',$date );
	// echo $this->db->_get_var_dump();
	if( $rows = $this->db->get()->result_first_assoc() ){
		$_arrs_groups = (INT)$rows['jumlah'];
	}
	
	return $_arrs_groups;
}
 
/*
 * @ def 		: _setMailDestination
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function _setMailDestination($param=null, $OutboxId=0 )
{

  $conds = 0;
  if( (!is_null($param)) AND ($OutboxId!=FALSE) )
  {
	foreach( $param as $keys => $anAddress )
	{
		$this->db->set('EmailDestination',$anAddress); 
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2);
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_destination');
		
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
	return $conds;
} 


/*
 * @ def 		: _setMailCC
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function _setMailCC($param=null, $OutboxId=0 )
{

  $conds = 0;
  if( (!is_null($param)) AND ($OutboxId!=FALSE) )
  {
	foreach( $param as $keys => $anAddress )
	{
		$this->db->set('EmailCC',$anAddress); 
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2);
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_copy_carbone');
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
	return $conds;
} 

/*
 * @ def 		: _setSaveMailToQueue
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setSavaMailOutgoing( $param  = null )
{
   $OutboxId = 0;
   $Config = $this->M_Configuration->_getMail(); // require mail 
   
   if( !is_null($param) )
   {
	 $this->db->set('EmailSender', $Config['smtp.auth']); // on configuratio 
	 $this->db->set('EmailContent', $param['MailContent']); // html or text 
	 $this->db->set('EmailSubject', $param['MailSubject']); // subject 
	 $this->db->set('EmailAssignDataId', $param['AssignDataId']); // subject 
	 $this->db->set('EmailStatus', '1001'); // Ready status
	 $this->db->set('EmailCreateTs', date('Y-m-d H:i:s')); 
	 
	 
	 $this->db->insert('email_outbox');
	 if( $this->db->affected_rows() > 0 )
	 {
		$OutboxId = $this->db->insert_id();
	 }
	 
	}
	
	return $OutboxId;
} 

// get spesifik to address 

public function _AddressTo($code = 0 )
{
	$_arrs_to = array();
	
	$this->db->select('a.FuEMailAddr');
	$this->db->from('t_lk_followup_mailgroup a');
	$this->db->where('a.FuEMailFollowupCode', $code);
	$this->db->where('a.FuEmailToCc', 'to');
	
	$i = 0;
	foreach( $this->db-> get() -> result_assoc() as $rows )
	{
		$_arrs_to[$i] = $rows['FuEMailAddr']; 
		$i++;
	}
	
	return $_arrs_to;
}


// get spesifik _AddressCC

public function _AddressCC($code = 0 )
{
	$_arrs_to = array();
	
	$this->db->select('a.FuEMailAddr');
	$this->db->from('t_lk_followup_mailgroup a');
	$this->db->where('a.FuEMailFollowupCode', $code);
	$this->db->where('a.FuEmailToCc', 'cc');
	
	$i = 0;
	foreach( $this->db-> get() -> result_assoc() as $rows )
	{
		$_arrs_to[$i] = $rows['FuEMailAddr']; 
		$i++;
	}
	
	return $_arrs_to;
}
/*
 * @ def 		: _setSaveMailToQueue
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getDetailFollowupExcel($ID)
 {
	$this->db->select('a.*, b.FuGroupDesc, b.FuMailAddress');
	$this->db->from('t_gn_followup_excel a ');
	$this->db->join('t_lk_followupgroup b','a.file_excel_group=b.FuGroupCode','LEFT');
	$this->db->where('b.FuGroupShow', 'Y');
	$this->db->where('a.file_excel_id', $ID);
	$rows = $this->db->get()->result_assoc();
	return $rows;
	
	
 } 
 

/*
 * @ def 		: _setSaveAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 
public function _setSaveAttachment( $param = null, $OutboxId = 0 )
{
 $conds = 0;
 if( (!is_null($param)) AND ($OutboxId!=FALSE) )
 {
	foreach( $param as $keys => $rows )
	{
		$this->db->set('EmailAttachmentPath',$rows['path']); 
		$this->db->set('EmailAttachmentSize',$rows['size']);
		$this->db->set('EmailAttachmentType',$rows['mime']);
		$this->db->set('EmailReffrenceId', $OutboxId); 
		$this->db->set('EmailDirection', 2); 
		$this->db->set('EmailCreateTs', date('Y-m-d H:i:s'));	
		$this->db->insert('email_attachment_url');
		
		if( $this->db->affected_rows() > 0) {
			$conds++;
		}
	}
 }
	
return $conds;
}

/*
 * @ def 		: _setSaveAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

public function _setSaveQueue( $OutboxId = 0 )
{
  $send_to_morows = date('Y-m-d H:i:s', strtotime('+1 day', strtotime(date('Y-m-d H:i:s')))); 
  $conds = 0;
  if( $OutboxId )
  {
	$this->db->set('QueueStatusTs',$send_to_morows);
	$this->db->set('QueueCreateTs',$send_to_morows); 
	$this->db->set('QueueMailId', $OutboxId );
	$this->db->set('QueueStatus', '1001');
	$this->db->set('QueueTrying',0);
	$this->db->insert('email_queue');
	
	if( $this->db->affected_rows()> 0 ) {
		$conds++;
	}
	
  }
  
  return $conds;
	
}
	
 
}
?>