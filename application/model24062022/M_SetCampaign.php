<?php
/*
 * E.U.I 
 *
 
 * subject	: get SetCampaign modul 
 * 			  extends under EUI_Controller class
 * author   : razaki team	
 * link		: http://www.razakitechnology.com/eui/model/sysuser/
 */
 
class M_SetCampaign extends EUI_Model
{


 var $set_rows_headers  = array(
	'debiturid'				=> array('field'=>'deb_id'),
	'cardno'				=> array('field'=>'deb_cardno'),
	'custno'				=> array('field'=>'deb_acct_no'),
	'accountno'				=> array('field'=>'deb_acct_no'),
	'idno'					=> array('field'=>'deb_id_number'),
	'nbrcards'				=> array('field'=>'deb_no_card'),
	'fullname'				=> array('field'=>'deb_name'),
	'mothername'			=> array('field'=>'deb_mother_name'),
	'dateofbirth'			=> array('field'=>'deb_dob'),
	'homeaddress'			=> array('field'=>'deb_add_hm'),
	'billingaddress'		=> array('field'=>'deb_billing_add'),
	'officeaddress'			=> array('field'=>'deb_add_off'),
	'homezip'				=> array('field'=>'deb_zip_h'),
	'homephone'				=> array('field'=>'deb_home_no1'),
	'homephone2'			=> array('field'=>NULL),
	'mobilephone'			=> array('field'=>'deb_mobileno1'),
	'mobilephone2'			=> array('field'=>NULL),
	'otheraddress'			=> array('field'=>NULL),
	'otheraddress2'			=> array('field'=>NULL),
	'officephone'			=> array('field'=>'deb_office_ph1'),
	'officefax'				=> array('field'=>NULL),
	'ecname'				=> array('field'=>'deb_ec_name_f'),
	'ecaddress'				=> array('field'=>'deb_ec_add_f'),
	'ecphone'				=> array('field'=>'deb_ec_phone_f'),
	'ecmobile'				=> array('field'=>'deb_ec_mobile_f'),
	'ecrelationship'		=> array('field'=>NULL),
	// 'branch'				=> array('field'=>NULL),
	'area'					=> array('field'=>'deb_region'),
	'opendate'				=> array('field'=>'deb_open_date'),
	'clss_entry'			=> array('field'=>'deb_resource'),
	'creditlimit'			=> array('field'=>'deb_limit'),
	'currentbalance'		=> array('field'=>NULL),
	'feeandcharge'			=> array('field'=>'deb_fees'),
	'interest'				=> array('field'=>'deb_interest'),
	'principalbalance'		=> array('field'=>'deb_principal'),
	'lastpaymentdate'		=> array('field'=>'deb_last_paydate'),
	'lastpaymentamount'		=> array('field'=>'deb_last_pay'),
	'currentpaymentdue'		=> array('field'=>NULL),
	'bpcount'				=> array('field'=>'deb_bp'),
	'bpfirstdate'			=> array('field'=>NULL),
	'wo_date'				=> array('field'=>'deb_b_d'),
	'wo_amount'				=> array('field'=>'deb_wo_amount'),
	'wo_open'				=> array('field'=>NULL),
	'wo_lpd'				=> array('field'=>NULL),
	'curr_coll_id'			=> array('field'=>'deb_update_by_user'),
	'dlq'					=> array('field'=>NULL),
	'perm_message'			=> array('field'=>'deb_perm_msg'),
	'perm_date'				=> array('field'=>'deb_perm_msg_date'),
	'accstatus'				=> array('field'=>'deb_call_status_code'),
	'accstatusdate'			=> array('field'=>NULL),
	'prevaccstatus'			=> array('field'=>'deb_prev_call_status_code'),
	'prevaccstatusdate'		=> array('field'=>NULL),
	'lastresponse'			=> array('field'=>NULL),
	'contacthistoryid'		=> array('field'=>NULL),
	'agent'					=> array('field'=>'deb_agent'),
	'remarks'				=> array('field'=>NULL),
	'reminder'				=> array('field'=>'deb_reminder'),
	'uploadbatch'			=> array('field'=>'deb_upload_id'),
	'ptp_discount'			=> array('field'=>NULL),
	'ptp_amount'			=> array('field'=>NULL),
	'ptp_date'				=> array('field'=>NULL),
	'ptp_channel'			=> array('field'=>NULL),
	'ssv_no'				=> array('field'=>NULL),
	'callstatus'			=> array('field'=>NULL),
	'calldate'				=> array('field'=>'deb_call_activity_datets'),
	'prevcallstatus'		=> array('field'=>NULL),
	'infoptp'				=> array('field'=>NULL),
	'prevcalldate'			=> array('field'=>NULL),
	'tenor'					=> array('field'=>NULL),
	'addhomephone'			=> array('field'=>NULL),
	'addhomephone2'			=> array('field'=>NULL),
	'addmobilephone'		=> array('field'=>NULL),
	'addmobilephone2'		=> array('field'=>NULL),
	'addofficephone'		=> array('field'=>NULL),
	'addofficephone2'		=> array('field'=>NULL),
	'addfax'				=> array('field'=>NULL),
	'addfax2'				=> array('field'=>NULL),
	'swap_count'			=> array('field'=>'deb_swap_count'),
	'sms_count'				=> array('field'=>'deb_sms_count'),
	'other_agent'			=> array('field'=>NULL),
	'other_card_number' 	=> array('field'=>NULL),
	'other_accstatus'		=> array('field'=>NULL),
	'contacttype'			=> array('field'=>'deb_contact_type'),
	'isrpc'					=> array('field'=>'deb_rpc'),
	'spc_with'				=> array('field'=>'deb_spc'),
	'namahomephone1'		=> array('field'=>NULL),
	'relativehomephone1'	=> array('field'=>NULL),
	'namahomephone2'		=> array('field'=>NULL),
	'relativehomephone2'	=> array('field'=>NULL),
	'namaofficephone1'		=> array('field'=>NULL),
	'relativeofficephone1'	=> array('field'=>NULL),
	'namaofficephone2'		=> array('field'=>NULL),
	'relativeofficephone2'	=> array('field'=>NULL),
	'namatlpphone1'			=> array('field'=>NULL),
	'relativetlpphone1'		=> array('field'=>NULL),
	'namatlpphone2'			=> array('field'=>NULL),
	'relativetlpphone2'		=> array('field'=>NULL),
	'namamobilephone1'		=> array('field'=>NULL),
	'relativemobilephone1'	=> array('field'=>NULL),
	'namamobilephone2'		=> array('field'=>NULL),
	'relativemobilephone2'	=> array('field'=>NULL),
	'keep_data'				=> array('field'=>'deb_is_kept'),
	'afterpay'				=> array('field'=>'deb_afterpay'),
	'attempt_call'			=> array('field'=>NULL),
	'isblock'				=> array('field'=>'deb_is_lock'),
	'bal_afterpay'			=> array('field'=>'deb_bal_afterpay'),
	'pri_afterpay'			=> array('field'=>'deb_pri_afterpay'),
	'tenor_value'			=> array('field'=>NULL),
	'tenor_amnt'			=> array('field'=>NULL),
	'tenor_dates'			=> array('field'=>NULL),
	'flag'					=> array('field'=>NULL),
	'current_lock'			=> array('field'=>NULL),
	'lock_modul'			=> array('field'=>NULL),
	'cpa_count'				=> array('field'=>NULL),
	'lunas_flag'			=> array('field'=>NULL),
	'last_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'max_swap_ts'			=> array('field'=>'deb_last_swap_ts'),
	'acc_mapping'			=> array('field'=>NULL),
	'class_mapping'			=> array('field'=>NULL),
	'accstatus_name'		=> array('field'=>NULL),
	'SPC_DESC'				=> array('field'=>'SPC_DESC'),
	'RPC_DESC'				=> array('field'=>'RPC_DESC'),
	'other_ch_office'		=> array('field'=>'other_ch_office'),
	'other_ch_home'			=> array('field'=>'other_ch_home'),
	'other_ch_mobile1'		=> array('field'=>'other_ch_mobile1'),
	'other_ch_mobile2'	 	=> array('field'=>'other_ch_mobile2'),
	'family'				=> array('field'=>'family'),
	'neighbour'				=> array('field'=>'neighbour'),
	'rel_person'			=> array('field'=>'rel_person'),
	'other_ec'				=> array('field'=>'other_ec')
);

/*
 * EUI :: _get_default() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
function M_SetCampaign()
{
	$this -> load->model('M_SysUser');
}

/*
 * EUI :: _get_default() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
function _get_campaign_name()
{
	$datas = array();
	
	$this->db->select("a.CampaignId, a.CampaignName");
	$this->db->from("t_gn_campaign a ");
	$this->db->join("t_lk_outbound_goals b", "a.OutboundGoalsId=b.OutboundGoalsId","LEFT");
	$this->db->join("t_gn_campaign_project c "," a.CampaignId=c.CampaignId", "LEFT");
	$this->db->where("a.CampaignStatusFlag",1);
	$this->db->where("Name", "outbound");
	$this->db->where_in('c.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
	$this->db->order_by('a.CampaignCode','ASC');
	
	foreach( $this -> db ->get() ->result_assoc() as $rows )
	{
		$datas[$rows['CampaignId']] = $rows['CampaignName'];
	}
	
	return $datas;
}

/*
 * EUI :: _get_default() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */	
 
function _get_default()
{
	$this -> EUI_Page -> _setPage(20); 
	
 /* set default of query ***/
 
	$this->EUI_Page->_setSelect(" DISTINCT(a.CampaignId) ",FALSE);
	$this->EUI_Page->_setFrom("t_gn_campaign a ");
	
 /* join tables **/
	$this->EUI_Page->_setJoin("t_gn_campaign_project f "," a.CampaignId=f.CampaignId","LEFT",TRUE);
	
/* set filter **/
	
	if($this->URI->_get_have_post('status_campaign')) {
		$this->EUI_Page->_setAnd("a.CampaignStatusFlag", $this->URI->_get_post('status_campaign'));
	}
	//echo $this->EUI_Page->_getCompiler();
	
	return $this -> EUI_Page;
}

/*
 * EUI :: _get_content() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */		
 
public function _get_content()
{
	$this->EUI_Page->_postPage($this->URI->_get_post('v_page'));
	$this->EUI_Page->_setPage(20);
	
/* default of select **/
	$this->EUI_Page->_setArraySelect(array(
		
		"DISTINCT a.CampaignNumber AS CampaignNumber" => array("CampaignNumber","Campaign Number"), 
		"a.CampaignId AS CampaignId" => array("CampaignId", "CampaignId","primary"),
		"(SELECT COUNT(dbs.deb_id) FROM t_gn_debitur dbs  WHERE dbs.deb_cmpaign_id=a.CampaignId ) AS DataSize" => array("DataSize","Data Size"),
		"a.CampaignCode as CampaignCode" => array("CampaignCode","Campaign Code"),
		"a.CampaignDesc as CampaignDesc" => array("CampaignDesc","Campaign Desc"),
		"IF( (a.CampaignReUploadFlag is null OR a.CampaignReUploadFlag=0),'N','Y') AS ReUploadReasonId" => array("ReUploadReasonId","Upload ReasonId"),
		"a.CampaignEndDate AS CampaignEndDate" => array("CampaignEndDate","End Date"), 
		"a.CampaignExtendedDate AS CampaignExtendedDate" => array("CampaignExtendedDate","Extend Date"),
		"IF( a.CampaignStatusFlag=0,'Not Active','Active') AS CmpStatus" => array("CmpStatus","Status Active"),
		"f.Description AS CallTypeName" => array("CallTypeName","Call Type"), 
		"h.ProjectName AS ProjectName" => array("ProjectName","Work Project")
	));
		
/* set from query select */
	
	$this->EUI_Page->_setFrom("t_gn_campaign a ");
	$this->EUI_Page->_setJoin("t_lk_outbound_goals f "," a.OutboundGoalsId=f.OutboundGoalsId ","LEFT");
	$this->EUI_Page->_setJoin("t_gn_campaign_project g "," a.CampaignId=g.CampaignId ", "LEFT");
	$this->EUI_Page->_setJoin("t_lk_work_project h "," g.ProjectId=h.ProjectId", "LEFT",TRUE);
	
/** set filtering **/

	if( $this->URI->_get_have_post('status_campaign')){
		$this->EUI_Page->_setAnd("a.CampaignStatusFlag", $this->URI->_get_post('status_campaign'));
	}
	
/* set order **/

	if( $this->URI->_get_have_post('order_by')){
		$this->EUI_Page->_setOrderBy($this->URI->_get_post('order_by'),$this->URI->_get_post('type'));
	}else{
		$this->EUI_Page->_setOrderBy('a.CampaignCode','ASC');
	}
	
	// echo "<pre>".$this->EUI_Page->_getCompiler()."</pre>";
	
	$this->EUI_Page->_setLimit();
		
}	

/*
 * EUI :: _get_resource_query() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
public function _get_resource_query()
 {
	$res = false;
	self::_get_content();
	if( $this -> EUI_Page -> _get_query()!='') {
		$res = $this -> EUI_Page -> _result();
		if($res) return $res;
	}	
 }
 
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _get_page_number()
  {
	if( $this -> EUI_Page -> _get_query()!='' )
	{
		return $this -> EUI_Page -> _getNo();
	}	
  }
  
   
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _set_save_campaign( $post=array() )
 {
	$_conds = false;
	$_serialize = $this -> URI -> _get_all_request();
	$_productid = $this -> URI -> _get_array_post('ProjectId');
	
	$sql = array
	(
		'OutboundGoalsId'	 => (isset($_serialize['OutboundGoalsId'])?$_serialize['OutboundGoalsId']:null), 
		'CampaignNumber'	 => (isset($_serialize['CampaignNumber'])?$_serialize['CampaignNumber']:null), 
		'CampaignName'		 => (isset($_serialize['CampaignName'])?$_serialize['CampaignName']:null), 
		'CampaignDesc'		 => (isset($_serialize['CampaignDesc'])?$_serialize['CampaignDesc']:null),
		'CampaignCode'		 => (isset($_serialize['CampaignCode'])?$_serialize['CampaignCode']:null),
		'CampaignStatusFlag' => (isset($_serialize['StatusActive'])?$_serialize['StatusActive']:null),
		'BuildTypeId'		 => (isset($_serialize['BuildTypeId'])?intval($_serialize['BuildTypeId']):0),
		'CampaignEndDate' 	 => (isset($_serialize['ExpiredDate'])?$this->EUI_Tools->_date_english($_serialize['ExpiredDate']):null), 
		'CampaignStartDate'  => (isset($_serialize['StartDate'])?$this->EUI_Tools->_date_english($_serialize['StartDate']):null)
	);
	
	if( $this -> db -> insert('t_gn_campaign',$sql)){
		$_insertId = $this -> db -> insert_id();
		if( $_insertId )
		{

			if(isset($_productid) && $_productid)
			{	
				foreach($_productid as $k => $ProjectId)
				{
					$this->db->set('CampaignId',$_insertId);
					$this->db->set('ProjectId',$ProjectId);
					$this->db->set('CreateTs',date('Y-m-d H:i:s'));
					$this->db->set('CreateUserId', $this->EUI_Session->_get_session('UserId'));
					
					$this->db->insert('t_gn_campaign_project');
					if( $this ->db->affected_rows() > 0 )
					{
						$_conds =true;
					}
				}
			}
		}
	}
	
	return $_conds;
		
 }
 
 
 
 /*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
function _get_size_campaign()
{ 
	$_conds = array();
	
	// $sql = " SELECT COUNT(a.CustomerId) as SizeCount, a.CampaignId FROM t_gn_debitur a GROUP BY a.CampaignId ";
	// $qry = $this->db->query($sql);
	// foreach($qry -> result_assoc() as $rows ) {
		// $_conds[$rows['CampaignId']] = ( $rows['SizeCount'] ? $rows['SizeCount'] : 0 );	
	// }
	
	return $_conds;
}	
/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */

function getAttribute($CampaignId=null )
{
	$_conds = array();
	if( isset($_conds))
	{
		$sql = "select a.*, b.ProjectId from t_gn_campaign a left join t_gn_campaign_project b on a.CampaignId=b.CampaignId
				left join t_lk_work_project c on b.ProjectId=b.ProjectId  WHERE a.CampaignId = '$CampaignId'";
		$qry = $this -> db -> query($sql);
		// echo $sql; exit;
		if($qry !=FALSE) 
		{
			$_conds = $qry -> result_first_assoc();
		}	
	}
	
	return $_conds;
}

/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */


function _setUpdate($_post_data= null)
{
	//var_dump($_post_data['CampaignNumber']);exit;
	
	$_conds = false;
	if(!is_null($_post_data))
	{
		if( $this ->db -> update("t_gn_campaign",array
		(
			"CampaignName" => $_post_data['CampaignName'],
			"CampaignCode" => $_post_data['CampaignCode'],
			"CampaignDesc" => $_post_data['CampaignDesc'],
			"OutboundGoalsId" => $_post_data['OutboundGoalsId'],
			"CampaignEndDate" => $this -> EUI_Tools -> _date_english($_post_data['ExpiredDate']),
			"CampaignStartDate" => $this -> EUI_Tools -> _date_english($_post_data['StartDate']),
			"CampaignStatusFlag" => $_post_data['StatusActive'],
			"BuildTypeId" => $_post_data['BuildTypeId'] ), 
				array("CampaignNumber"=> $_post_data['CampaignNumber'])
		))
		{
			$this->db->set('ProjectId', $_post_data['ProjectId']);
			$this->db->where('CampaignId', $_post_data['CampaignId']);
			$this->db->update('t_gn_campaign_project');
			$_conds =true;
		} 
	}
	
	return $_conds;
}

/*
 * EUI :: _get_page_number() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
function _getDataCampaignId($CampaignId = array())
{
	$_conds = array();
	$this->db->reset_select();
	$this->db->select("
	a.deb_id,
	a.deb_cardno,
	a.deb_acct_no,
	a.deb_acct_no,
	a.deb_id_number,
	a.deb_no_card,
	a.deb_name,
	a.deb_mother_name,
	a.deb_dob,
	a.deb_add_hm,
	a.deb_billing_add,
	a.deb_add_off,
	a.deb_zip_h,
	a.deb_home_no1,
	a.deb_mobileno1,
	a.deb_office_ph1,
	a.deb_ec_name_f,
	a.deb_ec_add_f,
	a.deb_ec_phone_f,
	a.deb_ec_mobile_f,
	a.deb_region,
	a.deb_open_date,
	a.deb_resource,
	a.deb_limit,
	a.deb_fees,
	a.deb_interest,
	a.deb_principal,
	a.deb_last_paydate,
	a.deb_last_pay,
	a.deb_bp,
	a.deb_b_d,
	a.deb_wo_amount,
	a.deb_update_by_user,
	a.deb_perm_msg,
	a.deb_perm_msg_date,
	a.deb_call_status_code,
	a.deb_prev_call_status_code,
	a.deb_agent,
	a.deb_reminder,
	a.deb_upload_id,
	a.deb_swap_count,
	a.deb_sms_count,
	a.deb_contact_type,
	a.deb_rpc,
	a.deb_spc,
	a.deb_is_kept,
	a.deb_afterpay,
	a.deb_is_lock,
	a.deb_bal_afterpay,
	a.deb_pri_afterpay,
	a.deb_last_swap_ts,
	b.CallReasonDesc as accstatus_name, 
	d.spc_description as SPC_DESC, 
	e.rpc_description as RPC_DESC, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=103) as other_ch_office, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=101) as other_ch_home, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=102) as other_ch_mobile1, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=109) as other_ch_mobile2, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=104) as family, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=105) as neighbour,
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id AND c.addphone_type=107) as rel_person, 
	(SELECT group_concat(c.addphone_no) FROM t_gn_additional_phone c WHERE c.deb_id=a.deb_id  AND c.addphone_type=108) as other_ec",FALSE);
	$this->db->from("t_gn_debitur a");
	$this->db->join("t_lk_account_status b ","a.deb_call_status_code=b.CallReasonCode", "LEFT");
	$this->db->join("t_lk_speach_with d "," a.deb_spc=d.spc_code", "LEFT");
	$this->db->join("t_lk_remote_place e "," a.deb_rpc=e.rpc_code","LEFT");
	$this->db->join("t_gn_campaign f", "a.deb_cmpaign_id=f.CampaignId", "LEFT");
	
	if( is_array($CampaignId) 
		AND count($CampaignId) > 0 )
	{
		$this -> db -> where_in("a.deb_cmpaign_id",$CampaignId);
	}
	
	$rs = $this->db->get(); 
	$i = 0;
	 if( $rs->num_rows() > 0)
		 foreach( $rs->result_assoc() as $rows ) 
	{
		if(is_array($this->set_rows_headers) )
			foreach( $this->set_rows_headers as $hd => $row ) 
		{
			$_conds[$i][$hd] = ( is_null( $row['field'] ) ? "" : $rows[$row['field']]);
		}
		$i++;
	}
	
	return $_conds;
}

/*
 * EUI :: _getMethodDirection() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _getMethodDirection()
 {
	$_conds = array();
	
	// $this->db->select('a.MethodCode, a.MethodName'); 
	// $this->db->from('t_lk_direct_method a');
	// $this->db->where('a.MenthodFlags',1);
	
	// foreach( $this ->db->get()->result_assoc() as $rows)
	// {
		// $_conds[$rows['MethodCode']] = $rows['MethodName'];
	// }
	
	// return $_conds;
	
	
 }
 
/*
 * EUI :: _getMethodDirection() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _getMethodAction()
 {
	$_conds = array();
	
	// $this->db->select('a.ActionCode, a.ActionName'); 
	// $this->db->from('t_lk_direction_action a');
	// $this->db->where('a.ActionFlags',1);
	
	// foreach( $this ->db->get()->result_assoc() as $rows)
	// {
		// $_conds[$rows['ActionCode']] = $rows['ActionName'];
	// }
	
	return $_conds;
 }
 
 
 /*
 * EUI :: _getOutboundGoals() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
 function _getCampaignGoals( $OutboundGoalsId = 0 )
 {
	$_conds = array();
	
	$this->db->select('a.CampaignId, a.CampaignName');
	$this->db->from('t_gn_campaign a');
	$this -> db -> join('t_gn_campaign_project b ',' a.CampaignId=b.CampaignId','LEFT');
	$this->db->where('a.OutboundGoalsId', $OutboundGoalsId);
	$this -> db -> where_in('b.ProjectId',$this->EUI_Session->_get_session('ProjectId'));
	
	
	foreach( $this ->db->get()->result_assoc() as $rows)
	{
		$_conds[$rows['CampaignId']] = $rows['CampaignName'];
	}
	
	return $_conds;
	
 }
 

 
/*
 * EUI :: _getDataInbound() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
  
 function _getDataInbound($CampaignId)
 {
	$_counter = 0;
	
	$this -> db -> select('COUNT(a.CustomerId) as Counter ');
	$this -> db -> from('t_gn_debitur a');
	$this -> db -> join('t_gn_assignment b ', 'a.CustomerId=b.CustomerId','INNER');
	$this -> db -> where('a.CampaignId',$CampaignId);
	
	if( $rows = $this -> db -> get()->result_first_assoc() )
	{
		$_counter = $rows['Counter'];
	}
	
	return $_counter;
 }
 
/*
 * EUI :: _setManageCampaign() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */
 
private function _getDataByCampaign($post)
{
	$_assign_customer = array();
	
	$this ->db ->select('*');
	$this ->db ->from('t_gn_debitur a');
	$this ->db ->join('t_gn_assignment b', 'a.CustomerId=b.CustomerId', 'INNER');
	$this ->db ->where('a.CampaignId',$post['InboundCampaignId']);	
	$this ->db ->limit($post['AssignData']);
	
	$i = 0;
	foreach( $this ->db ->get() -> result_assoc() as $rows )
	{
		$_assign_customer[$i] = $rows;
		$i++;
	}
	
	return $_assign_customer;
}
 
/*
 * EUI :: _setManageCampaign() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */ 
private function _getAssignByCampaign($CampaignId)
{
	$_assign_data = array();
	
	$this ->db ->select('*');
	$this ->db ->from('t_gn_debitur a');
	$this ->db ->join('t_gn_assignment b', 'a.CustomerId=b.CustomerId', 'INNER');
	$this ->db ->where('a.CampaignId',$CampaignId);	
	
	$i = 0;
	foreach( $this ->db ->get() -> result_assoc() as $rows )
	{
		$_assign_data[$i] = $rows;
		$i++;
	}
	
	return $_assign_data;
}
 
/*
 * EUI :: _setManageCampaign() 
 * -----------------------------------------
 *
 * @ def		function get detail content list page 
 * @ param		not assign parameter
 */ 
function _setManageCampaign($post=null)
{

	$_conds = 0;
	
 // define of parameter
	if(!defined('DIRECT')) define('DIRECT',2);
	if(!defined('DUPLICATE')) define('DUPLICATE',1);
	if(!defined('REPLACE')) define('REPLACE',2);
	
  // then process copy data 
	
	if(!is_null($post))
	{
		// method duplicate 
			
			if($post['DirectAction']==DUPLICATE)
			{
				// filtering data 
				
				$_array_filter = array
				(
					'CustomerId', 'UpdatedById','CampaignId','CallReasonId',
					'CallReasonQue','QueueId', 'SellerId','QaProsess',
					'InitDays','CustomerUpdatedTs','AssignId','AssignAdmin',
					'AssignMgr', 'AssignSpv', 'AssignSelerId','AssignDate',
					'AssignMode', 'AssignBlock'
				);
				
				// maping data 
				
				$avail_columns = ARRAY(); $assign_columns = ARRAY();
				$DataCustomers = ARRAY_VALUES(self::_getDataByCampaign($post));
				$nums = 0;
				
				foreach( $DataCustomers as $k => $values ) 
				{
					foreach($values as $fieldname => $fielvalues ) 
					{
						if(!in_array($fieldname, $_array_filter) ) 
						{
							$avail_columns[$nums][$fieldname] = $fielvalues;
						}
						else{
							$assign_columns[$nums][$fieldname] = $fielvalues;
						}
						
						$avail_columns[$nums]['CampaignId'] = $post['OutboundCampaignId'];
					}
					
				/* 
				 * @ def 	: insert to customer data 
				 * -------------------------------------
				 * @ param  : array()
				 * @ aksess : public
				 */	
				 
				 $_UserDetail = array_keys($this ->M_SysUser ->_get_administrator());
				 if( $this ->db->insert('t_gn_debitur',$avail_columns[$nums]))
				 {
					$CustomerId = $this ->db->insert_id();
					
					/* insert to assign data  */
						
					if( $this -> db->insert('t_gn_assignment',
							array
							(
								'CustomerId' 	=> $CustomerId, 
								'AssignAdmin' 	=> $_UserDetail[count($_UserDetail)-1],
								'AssignMode' 	=> 'DIS'
							)
						))
					{
						/* insert to log data  */
						if( $this -> db -> insert("t_gn_direct_campaign",
							array
							(
								'CustomerIdNew' 	 => $CustomerId, 
								'DirectCampaignFrom' => $post['InboundCampaignId'], 
								'DirectCampaignTo' 	 => $post['OutboundCampaignId'],  
								'CustomerIdOld' 	 => $assign_columns[$nums]['CustomerId'], 
								'SellerId' 			 => $assign_columns[$nums]['SellerId'],  
								'CallReasonId'		 => $assign_columns[$nums]['CallReasonId'],  
								'CreateByUserId' 	 => $this -> EUI_Session ->_get_session('UserId'),
								'CreateDateTs' 		 => date('Y-m-d H:i:s'),
								'DirectAction' 		 => DUPLICATE,
								'DirectMethode' 	 => DIRECT
							)
						)){
							$_conds++;
						}
					 }
				   }
					$nums++;
				}
				
			}
			
		// method replace 	
			
			if($post['DirectAction']==REPLACE)
			{
				// filtering data 
				
				$_array_filter = array (
					'CustomerId','UpdatedById','CampaignId','CallReasonId',
					'CallReasonQue','QueueId', 'SellerId','QaProsess',
					'InitDays','CustomerUpdatedTs','AssignId','AssignAdmin',
					'AssignMgr', 'AssignSpv', 'AssignSelerId','AssignDate',
					'AssignMode', 'AssignBlock'
				);
				
				// maping data 
				
				$_avc = ARRAY(); 
				$_mls = ARRAY(); 
				$where_avails 	= ARRAY();
				$DataCustomers  = ARRAY_VALUES(self::_getDataByCampaign($post));
				
				
				$_UserDetail = array_keys($this ->M_SysUser ->_get_administrator());
				
				$nums = 0;
				foreach( $DataCustomers as $k => $values ) 
				{
					$_mls['DirectCampaignFrom'] = $post['InboundCampaignId'];
					$_mls['DirectCampaignTo']   = $post['OutboundCampaignId'];
					$_mls['CustomerIdOld']	 	= $values['CustomerId'];
					$_mls['SellerId']			= $values['SellerId'];
					$_mls['CustomerIdNew'] 	 	= $values['CustomerId'];
					$_mls['CallReasonId']		= $values['CallReasonId'];
					$_mls['DirectAction'] 		= REPLACE;
					$_mls['DirectMethode'] 	 	= DIRECT;
					$_mls['CreateByUserId'] 	= $this -> EUI_Session ->_get_session('UserId');
					$_mls['CreateDateTs']		= date('Y-m-d H:i:s');
					
					foreach($values as $fieldname => $fielvalues ) 
					{
						if(!in_array($fieldname, $_array_filter) ) 
						{
							$this->db->set($fieldname,$fielvalues,true);
							$this->db->set('CallReasonId','NULL',FALSE);
							$this->db->set('SellerId','NULL',FALSE);
							$this->db->set('CampaignId', $post['OutboundCampaignId'],FALSE);							
						}
						else 
						{
							if( $fieldname=='CustomerId') {
								$this->db->where($fieldname,$fielvalues,FALSE);
							}
						}
					}
					
					$this -> db -> update('t_gn_debitur'); 
					if( $this->db->affected_rows() > 0 )
					{
						$this->db->set('AssignMgr','NULL',FALSE);
						$this->db->set('AssignSpv','NULL',FALSE);
						$this->db->set('AssignSelerId','NULL',FALSE);
						$this->db->set('AssignAdmin',$_UserDetail[count($_UserDetail)-1],FALSE);
						$this->db->set('AssignMode','DIS',TRUE);
						$this->db->set('AssignDate',date('Y-m-d H:i:s'),TRUE);
						$this->db->where('CustomerId',$values['CustomerId']);
						$this->db->update('t_gn_assignment');
						if( $this->db->affected_rows() > 0)
						{
							$_conds++;
							if( $this -> db ->insert('t_gn_direct_campaign', $_mls) )
							{
								$_conds++;
							}
						}
					}	
					
				  $nums++;
				}
				
			}
		
	}
	
	return $nums;
	
}
 
}

?>