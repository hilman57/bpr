<table align='center' border=0 width='100%' class="reposition">
	<tr>
		<td colspan=2>
			<?php echo form()->textarea('justification_text','textarea-justification',$recsource['justification_byspv']);?>
		</td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
	<tr>
		<td class='text_caption left current'>&nbsp;EMPOWERMENT MATRIX CONTROL</td>
		<td class='text_caption left current'>&nbsp;EXCEPTION ( if any, need approval Head of Collection )</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td class='text_caption'>Exceptional Level</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('ExceptionalLevel','input_text long',__print($recsource['ExceptionalLevel']));?></td>
				</tr>
				<tr>
					<td class='text_caption'>Discount Amount</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('ExceptDiscountAmount','input_text long',__print($recsource['discountamo_byspv']));?></td>
				</tr>
				<tr>
					<td class='text_caption'>&nbsp;</td>
					<td class='text_content'>:</td>
					<td class='text_caption'><div id="LblApprHead" name="LblApprHead">&nbsp;</div></td>
				</tr>
			</table>
		</td>
		<td valign='top'>
			
			<table width='100%'>
				<tr>
					<td class='text_caption' width='10%' nowrap>
						<?php echo form()->input('ExceptProcessTo','input_text date',__print('Cols SPV'), null, 
						array('style'=>'border:0px solid #000000;text-align:right;width:60px;') );?></td>
					<td class='center text_content' width='4%'>:</td>
					<td class='left'><?php echo form()->input('except1','input_text ',__print($recsource['except1']));?></td>
				</tr>
				<tr>
					<td class='text_caption left'></td>
					<td class='center text_content' width='4%'>:</td>
					<td class='left'><?php echo form()->input('except2','input_text long',__print($recsource['except2']));?></td>
				</tr>
				<tr>
					<td class='text_caption left'></td>
					<td class='center text_content' width='4%'>:</td>
					<td class='left'><?php echo form()->input('except3','input_text long',__print($recsource['except3']));?></td>
				</tr>
				
				<tr>
					<td class='text_caption left'></td>
					<td class='center text_content' width='4%'>:</td>
					<td class='left'><?php echo form()->input('except4','input_text long',__print($recsource['except4']));?></td>
				</tr>
			</table>
			
		</td>
		
	</tr>
</table>

		