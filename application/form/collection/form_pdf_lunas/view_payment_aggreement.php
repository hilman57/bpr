<table border=0 width='100%' class="reposition">
<tr>

	<td  width='50%' class="kolom_content">
		<table border=0 width='70%'>
		   <tr>
				<td class='text_caption current' width='12%'>DEBIT .</td>
				<td class='text_content' width='1%'>&nbsp;</td>
				<td class='text_content' width='20%'>&nbsp;</td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>Principal </td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print(_getCurrency($recsource['principle_byspv'])); ?> </td>
		   </tr>
			<tr>
				<td nowrap class='text_caption precision'>Charges (Fee & Interest)</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print(_getCurrency($recsource['charges_byspv'])); ?> </td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption precision'>Outstanding Balance</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print(_getCurrency($recsource['outstanding_byspv'])); ?> </td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>&nbsp;</td>
				<td class="text_content">&nbsp; </td>
				<td class="text_content">&nbsp; </td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption current'>PAYMENT</td>
				<td class="text_content"></td>
				<td class="text_content right"></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Down Payment</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print(_getCurrency($recsource['downpayment_byspv'])); ?></td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption precision'>Future Payment</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print(_getCurrency($recsource['futurepay_byspv'])); ?></td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption bottom precision'>Total Payment</td>
				<td class="text_content center">:</td>
				<td class='bottom text_content right' style='font-weight:bold;'><?php echo __print(_getCurrency($recsource['totalpayment_byspv'])); ?> </td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>Discount Amount</td>
				<td class="text_content center">:</td>
				<td class="text_content right" style='font-weight:bold;'><?php echo __print(_getCurrency($recsource['discountamo_byspv'])); ?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>&nbsp;</td>
				<td class="text_content">&nbsp; </td>
				<td class="text_content">&nbsp; </td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Payment Period</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print(_getCurrency($recsource['payment_periode'])); ?> Month</td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'> +/- From O/S bal (%)</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print( $recsource['from_balance']); ?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>+/- From Principal (%)</td>
				<td class="text_content center">:</td>
				<td class="text_content right"><?php echo __print($recsource['from_princ']); ?></td>
		   </tr>
		  
		   
		</table>
	</td>
	<td  class="kolom_content" width='50%'>
		<table border=0 width='100%'>
		   <tr >
				<td width='25%' class='text_caption'>Entry Date</td>
				<td width='2%'>:</td>
				<td width='73%' class="text_content"><?php echo __print($discount['deb_resource']); ?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>Open Date </td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getDateIndonesia($discount['deb_open_date'])); ?></td>
		   </tr>
			<tr>
				<td nowrap class='text_caption precision'>Wo Date</td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getDateIndonesia($discount['deb_wo_date'])); ?></td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption precision'>Last Pay Date</td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getDateIndonesia($discount['deb_last_paydate'])); ?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Last Pay Amount</td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getCurrency($discount['deb_last_pay'] )); ?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption  '>Last Pay date from payment</td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getLastPaymentDate($discount['deb_id'])); ?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Last Pay amount from payment</td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getCurrency(getLastPaymentHistory($discount['deb_id']))); ?></td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption precision'>DOB(yyyy-mm-dd)</td>
				<td>:</td>
				<td class="text_content"><?php echo __print(_getDateIndonesia($discount['deb_dob'])); ?></td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption precision'>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp; </td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision current' colspan=3>* DOC</td>
		   </tr>
		   
		   
		   <tr>
				<td nowrap class='text_caption precision' colspan=3>
					<table width='80%'>
						<tr>
							<td><div class="checkbox"><?php echo __print(getDocumentType($recsource, '101'))?></div></td>
							<td>&nbsp;Faxed</td>
							<td><div class="checkbox"><?php echo __print(getDocumentType($recsource, '102'))?></div></td>
							<td>&nbsp;Surper</td>
						</tr>
						<tr>
							<td><div class="checkbox"><?php echo __print(getDocumentType($recsource, '103'))?></div></td>
							<td>&nbsp;When Taking Sulrun</td>
							<td><div class="checkbox"><?php echo __print(getDocumentType($recsource, '104'))?></div></td>
							<td>&nbsp;Billings</td>
						</tr>
						<tr>
							<td><div class="checkbox"><?php echo __print(getDocumentType($recsource, '105'))?></div></td>
							<td>&nbsp;KTP</td>
							<td><div class="checkbox"><?php echo __print(getDocumentType($recsource, '106'))?></div></td>
							<td>&nbsp;Others</td>
						</tr>	
					</table>
				</td>
		   </tr>
		   
		</table>
	</td>
</tr>
</table>
	