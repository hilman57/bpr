<!-- load function --> 

<?php $this->load->form("form_pdf_lunas/view_form_function"); ?>

<!-- load function --> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>FORM LUNAS</title>
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.0.2.js?time=<?php echo time();?>"></script> 

<!-- load style : css style color --> 
<?php $this->load->form("form_pdf_lunas/view_form_styles"); ?>
<!-- load style : css style color<< --> 

</head>
<body>
<div id="container" style="border:0px solid #dddddd;padding:25px 1px 1px 1px;">
<table  border=0 width='90%' align='center'>
	<tr>
		<td><div class="logo-text"> FORM LUNAS</div></td>
		<td><div class="logo-hsbc">&nbsp;</div></td>
	</tr>	
	<tr>
		<td  style="border-top:2px solid #5f6f59; " colspan=2></td>
	</tr>
	
	<tr>
		<td colspan=2><?php $this->load->form("form_pdf_lunas/view_form_customer");?></td>
	</tr>
	
	<tr>
		<td class='header' colspan=2>BACKGROUND</td>
	</tr>
	
	<tr>
		<td colspan=2><?php $this->load->form("form_pdf_lunas/view_form_background");?></td>
	</tr>
	
	<tr>
		<td class='header' colspan=2>ACCOUNT STATUS</td>
	</tr>
	<tr>
		<td><?php $this->load->form("form_pdf_lunas/view_form_accountstatus");?></td>
	</tr>
	<tr>
		<td class='header' colspan=2>PAYMENT ARRANGEMENT</td>
	</tr>
	<tr>
		<td colspan=2><?php $this->load->form("form_pdf_lunas/view_payment_aggreement");?></td>
	</tr>
	<tr>
		<td class='header' colspan=2>JUSTIFICATION</td>
	</tr>
	<tr>
		<td align='center' colspan=2><?php $this->load->form("form_pdf_lunas/view_form_justification");?></td>
	</tr>
	<tr>
		<td align='center' colspan=2>&nbsp;</td>
	</tr>
	<tr>
		<td align='center' colspan=2><?php $this->load->form("form_pdf_lunas/view_form_approved");?></td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
</table>

	
	
</body>
</html>