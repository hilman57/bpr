<style type="text/css">
::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }
body { background-color: #FFFFFF; margin: 0px; color: #4F5155; }
a {color: #003399;background-color: transparent; font-weight: normal; }
h1 { color: #444; background-color: transparent; border-bottom: 1px solid #D0D0D0; font-size: 12px; font-weight: normal; margin: 0 0 14px 0; padding: 14px 15px 10px 15px; }

div#container { 
	margin: 12px; 
	padding : 4px 4px 90px 4px ; 
	text-align:justify; 
	border: 1px solid #D0D0D0; 
	line-height:22px; 
	background-color:#FFFFFF;
}

td {
	border-top:0px solid #dddddd;
	padding : 0px;
}
td.header{
	background-color:#00ff00;
	color:#000000;
	font-family:Arial;
	font-size:12px;
	font-weight:bold;
	padding-left:2px;
	border-bottom:0px solid #dddddd;
}
td.headermost{
	background-color:#000000;
	color:#FFFFFF;
	font-weight:bold;
	padding:2px;
}
td.text_caption{
	font-family:Arial;
	font-size:12px;
	color:#153b0b;
	padding-left: -2px;
}
td.kolom_content{
	padding : 0px;
}
td.bottom {
	border-left: 0px dashed #000;
	border-right: 0px dashed #000;
	border-top: 0px dashed #000;
	border-bottom : 1px dashed #000;
}
td.current{
	background-color:#f2fcef;
	font-family:Arial;
	font-weight:bold;
	
}
table.reposition{
	margin-left:-5px;
	cellspacing:0px;
}

div.checkbox{
	width:10px;
	height:15px;
	padding-top:-1px;
	padding-right:3px;
	padding-bottom:3px;
	padding-left:3px;
	border:1px solid #000;
	text-align:center;
	vertical-align: middle;
}
div.textarea-justification{
	border:1px solid #dddddd;
	width:100%;
	font-family:Arial;
	line-height:16px;
	font-size:11px;
	padding : 4px;
	background-color:#FFFFEE;
	height:50px;
}
td.center{
	text-align: center;
}
td.center{
	text-align: center;
}
div.ttd-box{
	width:120px;
	height:70px;
	border:0px solid #dddddd;
}

td.box-left{
	border-left:1px solid #000000;
	border-left:1px solid #000000;
}
td.box-top{
	border-top:1px solid #000000;
}
td.box-bottom{
	border-bottom:1px solid #000000;
}

td.box-right{
	border-right:1px solid #000000;
}

.lowercase{
	font-size:11px;
	color:red;
}
div.logo-hsbc{
	float:right;
	margin-right:-20px;
	background:url('<?php echo base_library();?>/gambar/hsbc_logo.png') no-repeat top right; width:250px; height:70px;
}
div.logo-text{
	font-family:Arial;
	font-size:18px;
	font-weight:bold;
}
td.text_content{
	font-family:Arial;
	font-size:12px;
	padding:0px;
	color:#000000;
}
.right {
 text-align:right;
}
.center {
 text-align:center;
}
</style>