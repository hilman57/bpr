<table align='center' border=0 width='100%' class="reposition">
	<tr>
		<td colspan=2>
			<div class="textarea-justification"> <?php echo __print($recsource['justification_byspv']);?></div>
		</td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;</td>
	</tr>
	<tr>
		<td class='text_caption left current'>&nbsp;EMPOWERMENT MATRIX CONTROL</td>
		<td class='text_caption left current'>&nbsp;EXCEPTION ( if any, need approval Head of Collection )</td>
	</tr>
	<tr>
		<td>
			<table border=0 width='100%'>
				<tr>
					<td class='text_caption' nowrap>Exceptional Level</td>
					<td class='center' width='5%'>:</td>
					<td class='text_content'>&nbsp;<?php echo __print($recsource['ExceptionalLevel']);?></td>
				</tr>
				<tr>
					<td class='text_caption'>Discount Amount</td>
					<td class='center' width='5%'>:</td>
					<td class='text_content' style='font-weight:bold;'>&nbsp;<?php echo __print(_getCurrency($recsource['discountamo_byspv'])); ?></td>
				</tr>
				<tr>
					<td class='text_caption'>&nbsp;</td>
					<td class='center'>:</td>
					<td class='text_caption' nowrap><?php echo $LblApprHead; ?></td>
				</tr>
			</table>
		</td>
		<td valign='top'>
			<table width='100%' cellspacing=0>
				<tr>
					<td class='text_caption' width='10%' nowrap><?php echo __print($recsource['ExceptProcessTo']);?>&nbsp;</td>
					<td class='center' width='4%'>:</td>
					<td class='text_content left'>&nbsp;<?php echo __print($recsource['except1']);?></td>
				</tr>
				
				<tr>
					<td class='text_caption left'></td>
					<td class='center' width='4%'>:</td>
					<td class='text_content left'>&nbsp;<?php echo __print($recsource['except2']);?></td>
				</tr>
				<tr>
					<td class='text_caption left'></td>
					<td class='center' width='4%'>:</td>
					<td class='text_content left'>&nbsp;<?php echo __print($recsource['except3']);?></td>
				</tr>
				<tr>
					<td class='text_caption left'></td>
					<td class='center' width='4%'>:</td>
					<td class=' text_content left'>&nbsp;<?php echo __print($recsource['except4']);?></td>
				</tr>
			</table>
			
		</td>
		
	</tr>
</table>

		