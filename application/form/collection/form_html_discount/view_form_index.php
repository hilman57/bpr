<!-- load function --> 
<?php $this->load->form("form_html_discount/view_form_function"); ?>
<!-- load function --> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>

<title>COLLECTION PAYMENT ARRANGEMENT (CPA)</title>
<link type="text/css" rel="stylesheet" href="<?php echo base_layout_style();?>/styles.icon.css?time=<?php echo time();?>" />
<script type="text/javascript" src="<?php echo base_jquery();?>/ui/jquery-2.0.0.js?time=<?php echo time();?>"></script>  
<script type="text/javascript" src="<?php echo base_jquery();?>/plugins/accounting.js?time=<?php echo time();?>"></script> 
<script type="text/javascript" src="<?php echo base_enigma();?>/cores/EUI_1.1.3_dep.js?time=<?php echo time();?>"></script> 
<?php $this->load->form("form_html_discount/view_form_javascript");?>
<?php $this->load->form("form_html_discount/view_form_styles");?>
</head>
<body>
<div id="container" style="border:0px solid #dddddd;padding:25px 1px 1px 1px;">

<form name="frmDiscount">
<?php echo form()->hidden('DebiturId',null, $discount['deb_id']);?>
<?php echo "\r\n";?>
<?php echo form()->hidden('debitur_campaign_id',null, $debitur['deb_cmpaign_id']);?>
<?php echo "\r\n";?>
<?php echo form()->hidden('FormUpdateId',null, $recsource['UpdateId']);?>
<?php echo "\r\n";?>
<?php echo form()->hidden('create_date',null, $recsource['create_date']);?>
<?php 

if ($status == "isi"){
	echo "On Progress, Contact Your Leader";
} else {
	
?>
<table  border=0 width='90%' align='center'>
	<tr>
		<td><div class="logo-text"> COLLECTION PAYMENT ARRANGEMENT (CPA) </div></td>
		<td><div class="logo-hsbc">&nbsp;</div></td>
		
	</tr>	
	<tr>
		<td  style="border-top:2px solid #5f6f59; " colspan=2></td>
	</tr>
	
	<tr>
		<td colspan=2><?php $this->load->form("form_html_discount/view_form_customer");?></td>
	</tr>
	
	<tr>
		<td class='header' colspan=2>BACKGROUND</td>
	</tr>
	
	<tr>
		<td colspan=2><?php $this->load->form("form_html_discount/view_form_background");?></td>
	</tr>
	
	<tr>
		<td class='header' colspan=2>ACCOUNT STATUS</td>
	</tr>
	<tr>
		<td><?php $this->load->form("form_html_discount/view_form_accountstatus");?></td>
	</tr>
	<tr>
		<td class='header' colspan=2>PAYMENT ARRANGEMENT</td>
	</tr>
	<tr>
		<td colspan=2><?php $this->load->form("form_html_discount/view_payment_aggreement");?></td>
	</tr>
	<tr>
		<td class='header' colspan=2>JUSTIFICATION</td>
	</tr>
	
	<tr>
		<td align='center' colspan=2><?php $this->load->form("form_html_discount/view_form_justification");?></td>
	</tr>
	
	<tr>
		<td align='center' colspan=2>&nbsp;</td>
	</tr>
	
	<tr>
		<td align='center' colspan=2><?php $this->load->form("form_html_discount/view_form_approved");?></td>
	</tr>
	<tr>
		<td colspan=2><?php $this->load->form("form_html_discount/view_form_footer");?></td>
	</tr>
</table>
<?php
}
?>
</form>
</div>
</body>
</html>