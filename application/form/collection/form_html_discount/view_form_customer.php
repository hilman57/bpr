<table border=0 width='100%' class="reposition" cellspacing=0>
<tr>
	<td width='50%'>
		<table border=0 width='100%' cellspacing=0>
		   <tr >
				<td width='32%' class='text_caption'>Ref No.</td>
				<td width='2%' class='text_caption'>:</td>
				<td width='70%' class="text_content"><?php echo form()->combo('RefNo','input_text long',array('D'=>'D','X'=>'X'),($recsource['reff_no']?$recsource['reff_no']:'D'));?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption'>Proposal Date</td>
				<td class='text_middle'>:</td>
				<td class="text_content"><?php echo form()->input('ProposalDate','input_text long', date('d-m-Y')); ?></td>
		   </tr>
			<tr>
				<td nowrap class='text_caption'>CF No</td>
				<td class='text_middle'>:</td>
				<td class="text_content"><?php echo form()->input('CFNo','input_text long', $discount['deb_acct_no']);?></td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption'>Customer Name</td>
				<td class='text_middle'>:</td>
				<td class="text_content"><?php echo form()->input('CustomerName','input_text long', $discount['deb_name']);?></td>
		   </tr>
		</table>
	</td>
	<td width='50%'>
		<table border=0 width='100%' cellspacing=0>
		   <tr >
				<td width='25%' class='text_caption'>&nbsp;</td>
				<td width='2%'>&nbsp;</td>
				<td width='73%'>&nbsp;</td>
		   </tr>
		   <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp; </td>
		   </tr>
			<tr>
				<td nowrap class='text_caption'> Product </td>
				<td class='text_middle'>:</td>
				<td class="text_content"><?php echo form()->input('Product','input_text long', $campaign['CampaignCode']);?></td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption'>Arrangement</td>
				<td class='text_middle'>:</td>
				<td class='text_content'><?php echo form()->input('Arrangement','input_text long',__print(null));?></td>
		   </tr>
		</table>
	</td>
</tr>
</table>
	