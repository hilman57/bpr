<table border=0 width='100%' class="reposition">
	<tr>
		<td  width='50%' class="kolom_content">
			<table  width='90%' border=0>
			   <tr>
					<td class='text_caption current' colspan='4' width='100%'>DEBIT</td>
			   </tr>
			   <tr>
					<td nowrap class='text_caption precision' width='20%'>Principal </td>
					<td class='text_content'  width='2%'>:</td>
					<td class='text_content' width='35%'><?php echo form()->input('Principal','input_text',
					($recsource['principle_byspv']?
					__print(_getCurrency($recsource['principle_byspv'])):
					__print(_getCurrency($discount['deb_principal']))
					), null,
						array('style'=>'width:100%;text-align:right;') );?></td>
					<td class='text_content' width='40%'>&nbsp;</td>	
			   </tr>
				<tr>
					<td nowrap class='text_caption precision'>Charges (Fee & Interest)</td>
					<td class='text_content'>:</td>
					<td class='text_content' width='35%'><?php echo form()->input('ChargesFeeInterest','input_text',
					($recsource['charges_byspv']?
					__print(_getCurrency($recsource['charges_byspv'])):
					__print(_getCurrency($discount['deb_fees']) )
					),null,
						array('style'=>'width:100%;text-align:right;') );?></td>
					<td  class='text_content' width='35%'>&nbsp;</td>	
			   </tr>
			   
				<tr>
					<td nowrap class='text_caption precision'>Outstanding Balance</td>
					<td class='text_content'>:</td>
					<td class='text_content' >
						<?php echo form()->input('OutstandingBalance','input_text ',
						($recsource['outstanding_byspv']?
						__print(_getCurrency($recsource['outstanding_byspv'])):
						__print(_getCurrency($discount['deb_amount_wo']))
						), null, 
							array('style'=>'width:100%;text-align:right;') );?></td>
					<td class='text_content'>&nbsp;</td>
					
			   </tr>
			   
			   <tr>
					<td nowrap class='text_caption precision' colspan=4>&nbsp;</td>
			   </tr>
			   
			   <tr>
					<td nowrap class='text_caption current' colspan=4>PAYMENT</td>
			   </tr>
			   
			   <tr>
					<td nowrap class='text_caption precision'>DownPayment</td>
					<td class='text_content'>:</td>
					<td class='text_content' width='35%'><?php echo form()->input('DownPayment','input_text long',
					($recsource['downpayment_byspv']?
					__print(_getCurrency($recsource['downpayment_byspv'])):
					__print(0)
					),
					null, array('style'=>'width:100%;text-align:right;') );?></td>
					<td class='text_content' width='35%'>&nbsp;</td>
					
			   </tr>
				<tr>
					<td nowrap class='text_caption precision'>Future Payment</td>
					<td class='text_content'>:</td>
					<td class='text_content' ><?php echo form()->input('FuturePayment','input_text long',
					($recsource['futurepay_byspv']?
					__print(_getCurrency($recsource['futurepay_byspv'])):
					__print(_getCurrency(getSumPaymentHistory($discount['deb_id'])))
					),
					null, array('style'=>'width:100%;text-align:right;') );?></td>
					<td class='text_content'>&nbsp;</td>
			   </tr>
				<tr>
					<td nowrap class='text_caption bottom precision'>Total Payment</td>
					<td class='text_content'>:</td>
					
					<td class='text_content'><?php echo form()->input('TotalPayment','input_text long',
					($recsource['totalpayment_byspv']?
					__print(_getCurrency($recsource['totalpayment_byspv'])):
					__print(_getCurrency(getSumPaymentHistory($discount['deb_id'])))
					),
					NULL, array('style'=>'width:100%;text-align:right;') );
						//_getCurrency(getSumPaymentHistory($discount['deb_id']))
					?></td>
					<td class='text_content'>&nbsp;</td>
			   </tr>
			   <tr>
					<td nowrap class='text_caption precision'>Discount Amount</td>
					<td class='text_content' >:</td>
					
					<td class='text_content'><?php echo form()->input('DiscountAmount','input_text long',
					($recsource['discountamo_byspv']?
					__print(_getCurrency($recsource['discountamo_byspv'])):
					__print(_getCurrency(getDiscountAmount($discount)))
					),
					NULL, array('style'=>'width:100%;text-align:right;') );?></td>
					<td class='text_content'>&nbsp;</td>
			   </tr>
			   
			   <tr>
					<td nowrap class='text_caption precision' colspan=4>&nbsp;</td>
			   </tr>
			   
			   <tr>
					<td nowrap class='text_caption precision'>Payment Period</td>
					<td class='text_content'>:</td>
					<td class='text_content' ><?php echo form()->combo('PaymentPeriod','select long',$payment_periode, 
					($recsource['payment_periode']?
					__print($recsource['payment_periode']):
					__print(0)
					) , 
					NULL, array('style'=>'width:100%;text-align:right;padding-right:8px;'));?></td>
					<td class='text_content'>&nbsp;Month</td>
			   </tr>
			   
			   <tr>
					<td nowrap class='text_caption precision'> +/- From O/S bal (%)</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('FromOSbal','input_text long',

					($recsource['from_balance']?
					__print($recsource['from_balance']):
					__print('-100')
					),
					NULL, array('style'=>'width:100%;text-align:right;') );?></td>
					<td class='text_content'>&nbsp;</td>
					
			   </tr>
			   <tr>
					<td nowrap class='text_caption precision'>+/- From Principal (%)</td>
					<td class='text_content'>:</td>
					<td class='text_content' ><?php echo form()->input('FromPrincipal','input_text long',
					($recsource['from_princ']?
					__print($recsource['from_princ']):
					__print(0)
					),
					NULL, array('style'=>'width:100%;text-align:right;'));?></td>
					<td class='text_content'>&nbsp;</td>
			   </tr>
			</table>
			
		</td>
		
		<td  class="kolom_content" width='50%'>
			<table border=0 width='100%'>
				<tr >
					<td width='25%' class='text_caption'>Entry Date</td>
					<td width='2%' class='text_content'>:</td>
					<td width='73%' class='text_content'><?php echo form()->input('EntryDate','input_text long',
					__print($discount['deb_resource']));?></td>
				</tr>
				<tr>
					<td nowrap class='text_caption precision'>Open Date </td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('OpenDate','input_text long',
					__print(_getDateIndonesia($discount['deb_open_date'])));?></td>
				</tr>
				<tr>
					<td nowrap class='text_caption precision'>Wo Date</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('WoDate','input_text long',
					__print(_getDateIndonesia($discount['deb_wo_date'])));?></td>
				</tr>

				<tr>
					<td nowrap class='text_caption precision'>Last Pay Date</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('LastPayDate','input_text long',
					__print(_getDateIndonesia($discount['deb_pay_date'])));?></td>
				</tr>

				<tr>
					<td nowrap class='text_caption precision'>Last Pay Amount</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('LastPayAmount','input_text long',  
					__print(_getCurrency($discount['deb_last_pay'] )),
					NULL, array('style'=>'text-align:left;') );?></td>
				</tr>
				<tr>
					<td nowrap class='text_caption precision'>DOB(dd-mm-yyyy)</td>
					<td class='text_content'>:</td>
					<td class='text_content'><?php echo form()->input('DOB','input_text long',
					_getDateIndonesia($discount['deb_dob']));?></td>
				</tr>
				<tr>
					<td nowrap class='text_caption precision'>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp; </td>
				</tr>
				<tr>
					<td nowrap class='text_caption precision current' colspan=3>* DOC</td>
				</tr>
				<tr>
					<td nowrap class='text_caption' colspan=3>
						<table border=0>
							<tr>
								<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'101',null,(find_array('101',$ex_doc)?array('checked'=>'checked'):null) );?></td>
								<td class='text_content'>&nbsp;Faxed</td>
								<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'102',null,(find_array('102',$ex_doc)?array('checked'=>'checked'):null) );?></td>
								<td class='text_content'>&nbsp;Surper</td>
							</tr>
							<tr>
								<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'103',null,(find_array('103',$ex_doc)?array('checked'=>'checked'):null) );?></td>
								<td class='text_content'>&nbsp;When Taking Sulrun</td>
								<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'104',null,(find_array('104',$ex_doc)?array('checked'=>'checked'):null) );?></td>
								<td class='text_content'>&nbsp;Billings</td>
							</tr>
							<tr>
								<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'105',null,(find_array('105',$ex_doc)?array('checked'=>'checked'):null) );?></td>
								<td class='text_content'>&nbsp;KTP</td>
								<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'106',null,(find_array('106',$ex_doc)?array('checked'=>'checked'):null) );?></td>
								<td class='text_content'>&nbsp;Others</td>
							</tr>	
						</table>
					</td>
				</tr>
				<?php
					// for($a=1;$a<=6;$a++) {
				?>
						<tr>
							<td class='text_content'>
							<form action="javascript:void(0);" method="post" onsubmit="return false;" enctype="multipart/form-data">  
								<input type="file" multiple="multiple" name="ScriptFileName" id="ScriptFileName">
								</form>
							</td>
						</tr>
				<?php
					// }
				?>
				<tr>
					<td>
						<input type="reset" value="Cancel">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td width='90%' class="text_content" colspan=2>
			<table border = 0>
				<tr align = "center">
					<?php
						if(!empty($recsource['File1'])){
					?>
					<td>
						<img src="<?=base_url().APPPATH .'script/'.$recsource['File1']?>" height="150" width="194" alt="">
						<br><input type="button" value="Delete" onclick="DeleteAttach_1();">
					</td>
					<?php
						}
					?>
					
					<?php
						if(!empty($recsource['File2'])){
					?>
					<td>
						<img src="<?=base_url().APPPATH .'script/'.$recsource['File2']?>" height="150" width="194" alt="">
						<br><input type="button" value="Delete" onclick="DeleteAttach_2();">
					</td>
					<?php
						}
					?>
					
					<?php
						if(!empty($recsource['File3'])){
					?>
					<td>
						<img src="<?=base_url().APPPATH .'script/'.$recsource['File3']?>" height="150" width="194" alt="">
						<br><input type="button" value="Delete" onclick="DeleteAttach_3();">
					</td>
					<?php
						}
					?>
					
					<?php
						if(!empty($recsource['File4'])){
					?>
					<td>
						<img src="<?=base_url().APPPATH .'script/'.$recsource['File4']?>" height="150" width="194" alt="">
						<br><input type="button" value="Delete" onclick="DeleteAttach_4();">
					</td>
					<?php
						}
					?>
					
					<?php
						if(!empty($recsource['File5'])){
					?>
					<td>
						<img src="<?=base_url().APPPATH .'script/'.$recsource['File5']?>" height="150" width="194" alt="">
						<br><input type="button" value="Delete" onclick="DeleteAttach_5();">
					</td>
					<?php
						}
					?>
					
					<?php
						if(!empty($recsource['File6'])){
					?>
					<td>
						<img src="<?=base_url().APPPATH .'script/'.$recsource['File6']?>" height="150" width="194" alt="">
						<br><input type="button" value="Delete" onclick="DeleteAttach_6();">
					</td>
					<?php
						}
					?>
				</tr>
			</table>
		</td>
	</tr>
	
</table>
	