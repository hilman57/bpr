
<style type="text/css">
.button{ padding-left:20px; padding-right:5px; padding-bottom:22px; padding-top:5px; border:1px solid #c6d0e6; vertical-align:middle;margin:8px 8px 8px 0px;  background-color:#f2f6f4;color:#067346;font-family:Arial;font-size:11px; font-weight:bold;cursor:pointer; background-position: 4px 5px;}
body { background-color: #FFFFFF; margin: 0px; color: #4F5155; }
div#container { 
	margin: 12px; 
	padding : 4px 4px 90px 4px ; 
	text-align:justify; 
	border: 1px solid #D0D0D0; 
	line-height:22px; 
	background-color:#FFFFFF;
}
td {
	border-top:0px solid #dddddd;
	padding : 0px;
}
td.header{
	background-color:#90ee90;
	color:#000000;
	font-family:Arial;
	font-size:12px;
	font-weight:bold;
	padding-left:2px;
	border-bottom:0px solid #dddddd;
}
td.headermost{
	background-color:#000000;
	color:#FFFFFF;
	font-weight:bold;
	padding:2px;
}
td.text_caption{
	font-family:Arial;
	font-size:12px;
	color:#000000;
	padding-left: -2px;
}
td.kolom_content{
	padding : 0px;
}
td.bottom {
	border-left: 0px dashed #000;
	border-right: 0px dashed #000;
	border-top: 0px dashed #000;
	border-bottom : 1px dashed #000;
}
td.current{
	background-color:#ffffff;
	font-family:Arial;
	font-weight:bold;
	
}
table.reposition{
	margin-left:-5px;
}

div.checkbox{
	width:10px;
	height:10px;
	padding:2px 2px 2px 2px;
	border:1px solid #000000;
}
.textarea-justification{
	border:1px dashed #dddddd;
	width:100%;
	font-family:Arial;
	font-size:12px;
	margin-top:10px;
	height:60px;
}
.textarea-justification:hover{
	background-color:#fffCCC;
}
td.center{
	text-align: center;
}
td.center{
	text-align: center;
}
div.ttd-box{
	width:120px;
	height:70px;
	border:0px solid #dddddd;
}

td.box-left{
	border-left:1px solid #000000;
	border-left:1px solid #000000;
}
td.box-top{
	border-top:1px solid #000000;
}
td.box-bottom{
	border-bottom:1px solid #000000;
}

td.box-right{
	border-right:1px solid #000000;
}

.lowercase{
	font-size:11px;
	color:red;
}

div.logo-hsbc{
	float:right;
	margin-right:-20px;
	background:url('http://192.168.1.52/hsbc/library/gambar/hsbc_logo.png') no-repeat top right; width:250px; height:70px;
}
div.logo-text{
	font-family:Arial;
	font-size:18px;
	font-weight:bold;
}

td.text_content{
	font-family:Arial;
	font-size:12px;
	padding:1px;
	color:#000000;
}
.input_text{
	height : 18px;
	font-size:12px;
	background-color:#FFFFFF;
	color:blue;
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-right:1px solid #FFFFFF;
	border-bottom:1px dashed #000000;
}

.long { width: 180px; }
.select {
	color:blue;
	font-size:12px;
	height:20px;
	font-family:Arial;
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-right:1px solid #FFFFFF;
	border-bottom:1px dashed #000000;
}
.select:hover{
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-right:1px solid #FFFFFF;
	border-bottom:1px solid #DDDDD;
	background-color:#FFFCCC;
}
.input_text:hover{
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-right:1px solid #FFFFFF;
	border-bottom:1px solid #DDDDD;
	background-color:#FFFCCC;
}

.form-required {
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-right:1px solid #FFFFFF;
	border-bottom:1px solid red;
}
</style>