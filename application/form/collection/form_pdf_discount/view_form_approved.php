<table id = "justify" width="100%" cellspacing=0>
	<tr>
		<td colspan="7" class='text_caption current center'>APPROVED BY (only for holder of limit letter)</td>
	</tr>
	
	<tr>
		<td class="text_caption center box-top box-left " style="border-bottom:0px solid #000;">Coll. Supervisor</td>
		<td class="text_caption center box-top box-left " style="border-bottom:0px solid #000;">Coll. Band 6 <br>(AM/MGR)</td>
		<td class="text_caption center box-top box-left" style="border-bottom:0px solid #000;">Coll. Band 5 <br> (AVP/VP)</td>
		<td class="text_caption center box-top box-left" style="border-bottom:0px solid #000;">Coll. Band 4 <br> (SPV)</td>
		<td class="text_caption center box-top box-left" style="border-bottom:0px solid #000;">Head of <br>Coll.</td>
		<td class="text_caption center box-top box-left" style="border-bottom:0px solid #000;">Head of <br>CRM CCC</td>
		<td class="text_caption center box-top box-left box-right" style="border-bottom:0px solid #000;">Head of<br>CRM</td>
	</tr>
	
	<tr>
		<td class="text_caption center box-left box-bottom"><div class="ttd-box">&nbsp;</div></td>
		<td class="text_caption center box-left box-bottom"><div class="ttd-box">&nbsp;</div></td>
		<td class="text_caption center box-left box-bottom"><div class="ttd-box">&nbsp;</div></td>
		<td class="text_caption center box-left box-bottom"><div class="ttd-box">&nbsp;</div></td>
		<td class="text_caption center box-left box-bottom"><div class="ttd-box">&nbsp;</div></td>
		<td class="text_caption center box-left box-bottom"><div class="ttd-box">&nbsp;</div></td>
		<td class="text_caption center box-left box-bottom box-right"><div class="ttd-box">&nbsp;</div></td>
	</tr>
</table>