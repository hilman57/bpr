<table border=0 width='100%' class="reposition">
<tr>

	<td class='kolom_content'>
		<table border=0 width='100%'>
		   <tr >
				<td width='30%' class='text_caption'>Occupation .</td>
				<td width='2%' class='text_content'>:</td>
				<td width='70%' class='text_content'><?php echo form()->combo('Occupation','select long',$occupation, $recsource['occupation']);?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption'>Reason </td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->combo('Reason','select long',$cpa_reason, $recsource['reason']);?></td>
		   </tr>
			<tr>
				<td nowrap class='text_caption'>No. Of Other DLQ Debt </td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('NoOfOtherDLQDebt','input_text long',__print('>=3'));?></td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption'>Payment Handled By</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->combo('PaymentHandledBy','select long', $payers, $recsource['pay_handled_by']);?></td>
		   </tr>
		</table>
	</td>
	<td class='kolom_content'>
		<table border=0 width='100%'>
		   <tr>
				<td width='25%'>&nbsp;</td>
				<td width='2%'>&nbsp;</td>
				<td width='73%'>&nbsp;</td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption'>Mapping Acount </td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('MappingAcount','input_text long',__print('N'));?></td>
		   </tr>
			<tr>
				<td class='text_caption'>Placement </td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('Placement','input_text long',($recsource['placement']?$recsource['placement']:__print('RIT')));?></td>
		   </tr>
		   
			<tr>
				<td class='text_caption'>Agency Name</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('AgencyName','input_text long','AKS');?></td>
		   </tr>
		</table>
	</td>
</tr>
</table>
	