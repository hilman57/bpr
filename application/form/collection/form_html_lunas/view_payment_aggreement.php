<table border=0 width='100%' class="reposition">
<tr>

	<td  width='50%' class="kolom_content">
		<table  width='90%' border=0>
		   <tr>
				<td class='text_caption current' colspan='4' width='100%'>DEBIT</td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision' width='20%'>Principal </td>
				<td class='text_content'  width='2%'>:</td>
				<td class='text_content' width='35%'><?php echo form()->input('Principal','input_text',__print(
					_getCurrency($discount['deb_principal'])), null,
					array('style'=>'width:100%;text-align:right;') );?></td>
				<td class='text_content' width='40%'>&nbsp;</td>	
		   </tr>
			<tr>
				<td nowrap class='text_caption precision'>Charges (Fee & Interest)</td>
				<td class='text_content'>:</td>
				<td class='text_content' width='35%'><?php echo form()->input('ChargesFeeInterest','input_text',__print(
					_getCurrency($discount['deb_fees']) ),null,
					array('style'=>'width:100%;text-align:right;') );?></td>
				<td  class='text_content' width='35%'>&nbsp;</td>	
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption precision'>Outstanding Balance</td>
				<td class='text_content'>:</td>
				<td class='text_content'>&nbsp;</td>
				<td class='text_content' >
					<?php echo form()->input('OutstandingBalance','input_text ',__print( 
						_getCurrency($discount['deb_amount_wo'])), null, 
						array('style'=>'width:100%;text-align:right;') );?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision' colspan=4>&nbsp;</td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption current' colspan=4>PAYMENT</td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>DownPayment</td>
				<td class='text_content'>:</td>
				<td class='text_content' width='35%'><?php echo form()->input('DownPayment','input_text long',
				__print(0),
				null, array('style'=>'width:100%;text-align:right;') );?></td>
				<td class='text_content' width='35%'>&nbsp;</td>
				
		   </tr>
		    <tr>
				<td nowrap class='text_caption precision'>Future Payment</td>
				<td class='text_content'>:</td>
				<td class='text_content' ><?php echo form()->input('FuturePayment','input_text long',
				__print(_getCurrency(getSumPaymentHistory($discount['deb_id']))),
				null, array('style'=>'width:100%;text-align:right;') );?></td>
				<td class='text_content'>&nbsp;</td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption bottom precision'>Total Payment</td>
				<td class='text_content'>:</td>
				<td class='text_content bottom'>&nbsp;</td>
				<td class='text_content'><?php echo form()->input('TotalPayment','input_text long',
				__print(_getCurrency(getSumPaymentHistory($discount['deb_id']))),
				NULL, array('style'=>'width:100%;text-align:right;') );
					//_getCurrency(getSumPaymentHistory($discount['deb_id']))
				?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>Discount Amount</td>
				<td class='text_content' >:</td>
				<td class='text_content'>&nbsp;</td>
				<td class='text_content'><?php echo form()->input('DiscountAmount','input_text long',
				__print(_getCurrency(getDiscountAmount($discount))),
				NULL, array('style'=>'width:100%;text-align:right;') );?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision' colspan=4>&nbsp;</td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Payment Period</td>
				<td class='text_content'>:</td>
				<td class='text_content' ><?php echo form()->combo('PaymentPeriod','select long',$payment_periode, 
				($recsource['payment_periode']?$recsource['payment_periode']:__print(0)) , 
				NULL, array('style'=>'width:100%;text-align:right;padding-right:8px;'));?></td>
				<td class='text_content'>&nbsp;Month</td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'> +/- From O/S bal (%)</td>
				<td class='text_content'>:</td>
				<td class='text_content' colspan=2><?php echo form()->input('FromOSbal','input_text long',
				($recsource['from_balance']?$recsource['from_balance']:__print('-100')),
				NULL, array('style'=>'width:100%;text-align:right;') );?></td>
				
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>+/- From Principal (%)</td>
				<td class='text_content'>:</td>
				<td class='text_content' colspan=2><?php echo form()->input('FromPrincipal','input_text long',
				($recsource['from_princ']?$recsource['from_princ']:__print(0)),
				NULL, array('style'=>'width:100%;text-align:right;'));?></td>
				
		   </tr>
		  
		   
		</table>
	</td>
	<td  class="kolom_content" width='50%'>
		<table border=0 width='100%'>
		   <tr >
				<td width='25%' class='text_caption'>Entry Date</td>
				<td width='2%' class='text_content'>:</td>
				<td width='73%' class='text_content'><?php echo form()->input('EntryDate','input_text long',
				__print($discount['deb_resource']));?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision'>Open Date </td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('OpenDate','input_text long',
				__print(_getDateIndonesia($discount['deb_open_date'])));?></td>
		   </tr>
			<tr>
				<td nowrap class='text_caption precision'>Wo Date</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('WoDate','input_text long',
				__print(_getDateIndonesia($discount['deb_wo_date'])));?></td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption precision'>Last Pay Date</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('LastPayDate','input_text long',
					__print(_getDateIndonesia($discount['deb_last_paydate'])));?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Last Pay Amount</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('LastPayAmount','input_text long',  
					__print(_getCurrency($discount['deb_last_pay'] )),
					NULL, array('style'=>'text-align:left;') );?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption  '>Last Pay date from payment</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('LastPayDateFromPayment','input_text right long',
					__print(_getLastPaymentDate($discount['deb_id'])), 
					NULL, array('style'=>'text-align:left;') );?></td>
		   </tr>
		   
		   <tr>
				<td nowrap class='text_caption precision'>Last Pay amount from payment</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('LastPayAmountFromPayment','input_text long',
					__print(_getCurrency(getLastPaymentHistory($discount['deb_id']))), 
					NULL, array('style'=>'text-align:left;') );?></td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption precision'>DOB(yyyy-mm-dd)</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('DOB','input_text long',
					_getDateIndonesia($discount['deb_dob']));?></td>
		   </tr>
		    <tr>
				<td nowrap class='text_caption precision'>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp; </td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption precision current' colspan=3>* DOC</td>
		   </tr>
		   
		   
		   <tr>
				<td nowrap class='text_caption precision' colspan=3>
					<table width='80%'>
						<tr>
							<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'101',null,(find_array('101',$ex_doc)?array('checked'=>'checked'):null) );?></td>
							<td class='text_content'>&nbsp;Faxed</td>
							<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'102',null,(find_array('102',$ex_doc)?array('checked'=>'checked'):null) );?></td>
							<td class='text_content'>&nbsp;Surper</td>
						</tr>
						<tr>
							<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'103',null,(find_array('103',$ex_doc)?array('checked'=>'checked'):null) );?></td>
							<td class='text_content'>&nbsp;When Taking Sulrun</td>
							<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'104',null,(find_array('104',$ex_doc)?array('checked'=>'checked'):null) );?></td>
							<td class='text_content'>&nbsp;Billings</td>
						</tr>
						<tr>
							<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'105',null,(find_array('105',$ex_doc)?array('checked'=>'checked'):null) );?></td>
							<td class='text_content'>&nbsp;KTP</td>
							<td class='text_content'><?php echo form()->checkbox('sendOfDocument',null,'106',null,(find_array('106',$ex_doc)?array('checked'=>'checked'):null) );?></td>
							<td class='text_content'>&nbsp;Others</td>
						</tr>	
					</table>
				</td>
		   </tr>
		   
		</table>
	</td>
</tr>
</table>
	