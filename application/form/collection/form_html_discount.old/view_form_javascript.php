<script>

// Define all 

var $curs = accounting, $isaprv = 0,
	$except= [[0,""],
			  [1,"Coll SPV"],
			  [2,"Coll Band 6"],[3,"Coll Band 5"],
			  [4,"Coll Band 4"],[5,"Head of Coll"],
			  [6,"Head of CCC"],[7,"Head of CRM"] ];
			  
var $argm = { "D":"SETTLEMENT",
			  "R":"RESCHEDULE",
			  "X":"RESCHEDULE",
			  "":"-" };
			 
// efine url 
 
 var URL = function () 
{
	return window.opener.Ext.DOM.INDEX;
}

// @ pack : UpdateDiscount

 // var UpdateDiscount = function()
// {
  // var argv_vars = [], conds = Ext.Serialize('frmDiscount').Complete(['FormUpdateId','create_date'])
  // if( conds == false ){
	 // Ext.Msg('Form not completed.').Info();
	 // return false;
 // }

 // argv_vars['document'] = Ext.Cmp('sendOfDocument').getChecked();
 // argv_vars['FormUpdateId'] = Ext.Cmp('FormUpdateId').getValue();
 
 
 
  // Ext.Ajax
 // ({
	 // url 	: URL() +"/FormDiscount/UpdateDiscount/",
	 // method : 'POST',
	 // param 	: Ext.Join([ 
				// Ext.Serialize('frmDiscount').getElement(), 
				// argv_vars 
			  // ]).object(),
	// ERROR 	: function(e){
	 // Ext.Util(e).proc(function(response){
		// if( response.success ){
			// Ext.Msg("Update CPA").Success();
		 // } else {
			// Ext.Msg("Update CPA").Success();
		 // }
	  // });
	// }
 // }).post();
 
// }

var UpdateDiscount = function()
{
  var argv_vars = [], conds = Ext.Serialize('frmDiscount').Complete(['FormUpdateId','create_date'])

 argv_vars['document'] = Ext.Cmp('sendOfDocument').getChecked();
 argv_vars['FormUpdateId'] = Ext.Cmp('FormUpdateId').getValue();
 
	var $fileUpload = $("input[type='file']");
	if (parseInt($fileUpload.get(0).files.length)>6){
		alert("You can only upload a maximum of 6 files");
		return true;
	} else {
		Ext.Ajax
		({
			url 	: URL() +"/FormDiscount/UpdateDiscount/",
			method : 'POST',
			file 	: 'ScriptFileName',
			param 	: Ext.Join([ 
						Ext.Serialize('frmDiscount').getElement(), 
						argv_vars 
						]).object(),
			complete : function(fn){
				try {
					var ERR = JSON.parse(fn.target.responseText);
					if( ERR.success ){
						Ext.Msg("Update CPA").Success();
						location.reload();
					}
					else{
						Ext.Msg("Update CPA").Failed();
					}
				}
				catch(e){
					Ext.Msg(e).Error();
				}
			}
		}).MultipleUpload();	
	}
}


// @ pack : SaveDiscount

 var SaveDiscount = function()
{
	var argv_vars = [];
	var conds;
  var FormUpdateId = Ext.Cmp('FormUpdateId').getValue();
  if (FormUpdateId == ""){
	  conds = Ext.Serialize('frmDiscount');
  } else {
	  conds = Ext.Serialize('frmDiscount').Complete(['FormUpdateId','create_date']);
  } 
   
	  
  if( conds == false ){
	 Ext.Msg('Form not completed.').Info();
	 return false;
 }

 argv_vars['document'] = Ext.Cmp('sendOfDocument').getChecked();
 
 // @ pack its : save to discount
 
  Ext.Ajax
 ({
	 url 	: URL() +"/FormDiscount/SaveDiscount/",
	 method : 'POST',
	 param 	: Ext.Join([ 
				Ext.Serialize('frmDiscount').getElement(), 
				argv_vars 
			  ]).object(),
	ERROR 	: function(e){
	 Ext.Util(e).proc(function(response){
		if( response.success ){
			Ext.Msg("Save CPA").Success();
		 } else {
			Ext.Msg("CPA Alerdy Exist").Info();
		 }
	  });
	}
 }).post();
 
}

/*
 * @ pack : histung discount 
 */
 
 var Arrangment  = function()
{
  var $RefNo = $('#RefNo').val();
  for( var i in $argm ) {
	if( i == $RefNo ){
		$('#Arrangement').val( $argm[i] );
	}
 }
	
}

/*
 * @ pack : histung discount 
 */

 var UpdateProduct = function ()
{
 var cfno = $("#CFNo").val();
 var str = cfno.substring(0,1);
 var str2 = cfno.substring(0,3);
 
 var prod = "";
  if(str == '2'){ prod = "CF"; }
  else if(str == '4'){ prod = "CARD"; }
  else if(str == '5'){ prod = "CARD"; }
  else if(str2 == '080'){ prod = "PIL"; }
  else if(str2 == '001'){ prod = "PIL"; }
  else if(str2 == '071'){ prod = "GRF"; }
  else if(str2 == '073'){ prod = "GRF"; }
  else{ prod = "-"; }
 
 $('#Product').val(prod);
}


/*
 * @ pack : histung discount 
 */
 
 var ClearDiscount = function () 
{
	Ext.Serialize('frmDiscount').Clear();
}

/*
 *
 */
var UpdateException = function()
{
 var strDate = $("#OpenDate").val();
 var n = new Date();
 var m = new Date(strDate);
 var monthss = new Date(n-m).getMonth();
	
  if( $('#Arrangement').val() == "SETTLEMENT" && $('#CardStatus').val() == "WO" && $('#Product').val() == "CF" && $('#FromPrincipal').val() <= 0.4){
	except1 = "Discount on Principal > 40%"; }
  else if( $('#Arrangement').val() == "SETTLEMENT" && $('#CardStatus').val()== "WO" && $('#Product').val() == "CARD" && $('#FromPrincipal').val() <= 0.3){
	except1 = "Discount on Principal > 30%"; }
  else if($('#Arrangement').val()== "SETTLEMENT" && $('#CardStatus').val() == "WO" && $('#Product').val() == "PIL" && $('#FromPrincipal').val() <= 0.3){
	except1 = "Discount on Principal > 30%"; }
  else if($('#Arrangement').val()== "SETTLEMENT" && $('#CardStatus').val() == "WO" && $('#Product').val() == "GRF" && $('#FromPrincipal').val() <= 0.3){
	except1 = "Discount on Principal > 30%"; }
  else{
	except1 = "-";
 }
 
/*
 * @ pack : histung discount 
 */
 
 if($('#Arrangement').val() == "RESCHEDULE" 
	 || $('#Arrangement').val() == "SETTLEMENT")
{
	if(monthss < 6){ except2 = "Time since open date < 6 month"; }
	else{ except2 = "-"; } 
 }
else{ except2 = "-"; }

/*
 * @ pack : histung discount 
 */

if($('#Arrangement').val()== "SETTLEMENT"){ 
 if($('#Product').val() == "CARD"){ except3 = "T52";}
 else if($('#Product').val() == "PIL"){ except3 = "T53"; }
 else if($('#Product').val() == "CF"){ except3 = "T54"; }
 else if($('#Product').val() == "GRF"){ except3 = "T55"; }
 else{ except3 = "-"; }
}

/*
 * @ pack : histung discount 
 */

if($('#Arrangement').val() == "SETTLEMENT"){
 if($('#Product').val()== "CARD"){ except4 = "Q60"; }
 else if($('#Product').val()== "PIL"){ except4 = "Q61"; }
 else if($('#Product').val()== "CF"){ except4 = "Q62"; }
 else if($('#Product').val()== "GRF"){ except4 = "Q63"; }
 else{except4 = "-"; }
 
}
	
	
	$('#except1').val(except1);
	$('#except2').val(except2);
	$('#except3').val(except3);
	$('#except4').val(except4);
	
} 
/*
 * @ pack : histung discount 
 */
 
 var Discount = function ()
{

 var totBalance = $curs.toFixedNumber($('#OutstandingBalance').val()),
     totPayment= $curs.toFixedNumber($('#TotalPayment').val()),
	 totDisc = ((totBalance)- (totPayment));
var totPrincipal  = $curs.toFixedNumber($('#Principal').val());	 
var FuturePayment = $curs.toFixedNumber($('#TotalPayment').val()) - $curs.toFixedNumber($('#DownPayment').val());
var FromOSbal = - (totDisc / $curs.toFixedNumber($('#OutstandingBalance').val())) * 100;
var FromPrincipal = ( ( $curs.toFixedNumber($('#TotalPayment').val()) - $curs.toFixedNumber($('#Principal').val()) ) / $curs.toFixedNumber($('#Principal').val()) ) * 100;
/*
var FromOSbal = 100 - ((totPayment / totBalance) * 100);
var FromPrincipal = 100 - ((totPayment/totPrincipal) * 100);
*/	
 // limit approve user 
 
var $i = 0;
  if(totDisc <= 0){  $i = 0; } 
  else if(totDisc <= 5000000){  $i = 1; }
  else if(totDisc <= 10000000){ $i = 2; }
  else if(totDisc <= 20000000){ $i = 3; }
  else if(totDisc <= 30000000){ $i = 4; }
  else if(totDisc <= 50000000){ $i = 5; }
  else if(totDisc <= 200000000){ $i = 6; }
  //else if(totDisc <= 100000000){ $i = 6; }
  //else if(totDisc <= 2300000000){ $i = 7; }
  else{ $i = 0; }
  
// render to object --> 
  if($i == 6 ){
	  $('#except1').val("Coll. Band 6 (AM/MGR)");
  }else if($i == 5){
	  $('#except1').val("Coll. Band 5 (AVP/VP)");
  }else if($i == 4){
	  $('#except1').val("Coll. Band 3 (SPV)");
  }else if($i==3){
	  $('#except1').val("Head of Coll");
  }else if($i == 2){
	  $('#except1').val("Head of CRM CCC");
  }else {
	  $('#except1').val("Head of CRM");
  }
  
  $('#ExceptProcessTo').val($except[$i][1]);
  
  $('#DiscountAmount').val($curs.formatMoney(totDisc, "", 0, ".", ","));
  $('#ExceptDiscountAmount').val($curs.formatMoney(totDisc, "", 0, ".", ","));
  $('#FuturePayment').val($curs.formatMoney(FuturePayment, "", 0, ".", ","));
  $('#FromOSbal').val(FromOSbal.toFixed(2));
  $('#ExceptionalLevel').val($i);
  $('#FromPrincipal').val(FromPrincipal.toFixed(2));
  $('#TotalPayment').val($curs.formatMoney(totPayment, "", 0, ".", ","));
 if(AbsPrincipal>=30){$('#LblApprHead').text('Approval by Head of Collection');}
 else{$('#LblApprHead').text('');}
}


// render jquery function body 

$(document).ready(function(){
	$('#TotalPayment').bind('keyup', function(){
		window.Discount();	
		window.Arrangment();
		window.UpdateProduct();
		window.UpdateException();
	});
	
	$('.input_text').bind('mouseout', function(){
		window.Discount();
		window.Arrangment();
		window.UpdateProduct();
		window.UpdateException();
	});
});

 
 
</script>