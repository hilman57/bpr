<table border=0 width='100%' class="reposition">
<tr>

	<td class='kolom_content'>
		<table border=0 width='100%'>
		   <tr>
				<td width='40%' class='text_caption'>Card Open Date</td>
				<td width='2%' class='text_content'>:</td>
				<td class='text_content' width='60%'><?php echo form()->input('CardOpenDate','input_text long',__print(_getDateIndonesia($discount['deb_open_date'])));?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption'>Card Status</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('CardStatus','input_text long',($recsource['card_status']?__print($recsource['card_status']):__print('WO')));?></td>
		   </tr>
			<tr>
				<td nowrap class='text_caption'>Cycle Delinquent</td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('CycleDelinquent','input_text long',__print('8'));?></td>
		   </tr>
		   
			<tr>
				<td nowrap class='text_caption'>Region </td>
				<td class='text_content'>:</td>
				<td class='text_content'><?php echo form()->input('Region','input_text long',($recsource['region']?strtoupper($recsource['region']):strtoupper($discount['deb_region'])));?></td>
		   </tr>
		   <tr>
				<td nowrap class='text_caption'>Responsesible Collector or Negotiator  </td>
				<td class='text_content'>:</td>
				<!-- <td class='text_content'><?php #echo form()->input('CollectorOrNegotiator','input_text long', ($agent_detail['full_name']?getOwnerData($assigndata['AssignSelerId']) :_get_session('Fullname')) );?></td> -->
				<td class='text_content'><?php echo form()->input('CollectorOrNegotiator','input_text long', __print(getOwnerData($assigndata['AssignSelerId']) ));?></td>
				
		   </tr>
		</table>
	</td>
</tr>
</table>
	