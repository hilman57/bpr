#!/bin/sh
# check connection for mysqlport 3306 
# shell comand test
# author <omens>

# --------------------------------------------
# base of root dir  
# --------------------------------------------

root_event_dir=/opt/enigma/webapps/hsbc/index.php
root_event_handler=BatchApplication
root_event_index=index

# set rgc to parameter 
argc=$#

# set rgc to branching condition 
if [ $argc > 0 ] ; then
	php -q $root_event_dir $root_event_handler $root_event_index $1 $2
else 
	echo "Inavlid argument"
	exit 0
fi;	

# then you can write crontab on /etc/crontab like here

# run of absensi cronjob 
# -------------------------------------------------------
# $> 15 08 * * * root /opt/enigma/webapps/hsbc/applictaion/batch/application_batch.sh excel_deleted cf/card/ALL = Kosong  

# run of absensi cronjob 
# -------------------------------------------------------
# $> 15 08 * * * root /opt/enigma/webapps/hsbc/applictaion/batch/application_batch.sh absensi 

# run of cek PTP procees 
# -------------------------------------------------------
# $> 15 22 * * * root /opt/enigma/webapps/hsbc/applictaion/batch/application_batch.sh ptp_process 

# run of cek PTP request discount  
# -------------------------------------------------------
# $> 20 22 * * * root /opt/enigma/webapps/hsbc/applictaion/batch/application_batch.sh check_payment

# run of cek PTP open block phone number  
# -------------------------------------------------------
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/applictaion/batch/application_batch.sh open_phone_block

# run of cek PTP open block phone number  
# -------------------------------------------------------
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/applictaion/batch/application_batch.sh update_discount

# then you can write crontab on /etc/crontab like here
# $> 15 08 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh absensi 
# $> 15 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh ptp_process 
# $> 20 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh check_payment
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh open_phone_block
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh update_discount
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh compile_sdr
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh five_assigment
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh check_bp_status
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh check_pop_status
# $> 25 22 * * * root /opt/enigma/webapps/hsbc/application/batch/application_batch.sh excel_dumper