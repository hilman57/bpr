#!/bin/sh
# pack   : cek auto reset lock unlock from php   
# shell  : bash script spooler process 
# chmod  : 0777 
# -----------------------------------------------------------------------------

root_event_dir="/opt/enigma/webapps/hsbc/application/batch"
root_event_procces_pid="./application_batch.sh open_auto_unlock"
root_event_procces_auto="./application_batch.sh start_auto_lock"

#let it dump core
ulimit -c unlimited
#don't die by  SIGPIPE
trap '' PIPE
# cek is alive of PID 
ALIVE=
isalive()
{	
	ALIVE=
	for psnum in `ps -ef | grep -v grep | grep $1 | awk '{print $1}'`; do
	   echo $psnum
	  if kill -0 $psnum > /dev/null 2>&1 ; then
		ALIVE=1  
	  fi
	 done
	 return
}
# cek is alive of PID 
 autostart() 
{
   while :; do 
		cd ${root_event_dir}/&& ${root_event_procces_auto}
	sleep 2
  done
}

# cek is alive of PID 

 process() 
{
   while :; do 
		cd ${root_event_dir}/&& ${root_event_procces_pid}
	sleep 5
  done
}

autostart &
process &

