#!/bin/sh

# this will deleted data tmp on web application like GSM FILE 

BASE_PATH_ROOT="hsbc"
BASE_PATH_HOME="/opt/enigma/webapps/${BASE_PATH_ROOT}"
BASE_PATH_VOICE="${BASE_PATH_HOME}/application/temp/"
BASE_PATH_RESET="${BASE_PATH_HOME}/index.php Auth ResetLogin"
BASE_PATH_MATCH="${BASE_PATH_HOME}/index.php Auth MatchAccount"
BASE_PATH_SPAM="${BASE_PATH_HOME}/index.php Auth SMSSpam"
BASE_PATH_TEMP="${BASE_PATH_HOME}/index.php BatchHistoryTmp Index"


# this will deleted GSM on tmp every day 
# 10 02 * * * root /opt/enigma/webapps/hsbc/application/batch/start_deletion.sh deleted_tmp_gsm

if [ "$1" == "deleted_tmp_gsm" ]; then
  cd $BASE_PATH_VOICE && rm -rf *.gsm
  cd $BASE_PATH_VOICE && rm -rf *.wav
fi

# this will reset agent login on t_lk_user 
# 15 03 * * * root /opt/enigma/webapps/hsbc/application/batch/start_deletion.sh reset_agent_login

if [ "$1" == "reset_agent_login" ]; then
   php -q $BASE_PATH_RESET
fi
# this will reset agent login on t_lk_user 
# 15 03 * * * root /opt/enigma/webapps/hsbc/application/batch/start_deletion.sh match_account

if [ "$1" == "match_account" ]; then
   php -q $BASE_PATH_MATCH
fi

# this will delete on SMS Spam  
# 15 03 * * * root /opt/enigma/webapps/hsbc/application/batch/start_deletion.sh sms_spam

if [ "$1" == "sms_spam" ]; then
   php -q $BASE_PATH_SPAM
fi

# this will delete on SMS Spam  
# 15 03 * * * root /opt/enigma/webapps/hsbc/application/batch/start_deletion.sh temp_call_history

if [ "$1" == "temp_call_history" ]; then
   BASE_PATH_DATE=`date +%Y-%m-%d`  
   php -q $BASE_PATH_TEMP $BASE_PATH_DATE $BASE_PATH_DATE
fi



