<?php

$con = mysqli_connect("192.168.0.11","enigma","enigma","enigmahsbcdb");
// $tglReport = date("d-m-Y");
$tglReport = date("d-m-Y");
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
// Load plugin PHPExcel nya
include 'PHPExcel.php';

// PHPExcel_Writer_Excel2007
include 'PHPExcel/Writer/Excel2007.php';

//call class PHPexcel
echo date('H:i:s') . " Create new PHPExcel object\n";
$excel = new PHPexcel();

//settingan phpexcel
echo date('H:i:s') . " Set properties\n";
$excel->getProperties()->setCreator('Aseanindo')
		->setLastModifiedBy('HSBC-ritz2')
		->setTitle("SMSReport")
		->setSubject("SMSReport")
		->setDescription("SMSReport")
		->setKeywords("SMS Report");

//variable style header table
echo date('H:i:s') . " Add Style\n";
$style_col = array(
	'font'		=> array(
		'bold' => true,
		 'color' => array('rgb' => 'FFFFFF'),
        'size'  => 13
	),
	'alignment'	=> array(
		'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'	=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	),
	'fill'	=> array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '3366FF')
     ),
	'borders'	=> array(
		'top'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'right'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'bottom'=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'left'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
	)
);

//variable style body table
$style_row = array(
	'alignment'	=> array(
		'vertical'	=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	),
	'borders'	=> array(
		'top'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'right'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'bottom'=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
		'left'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
	)
);

$excel->setActiveSheetIndex(0)->setCellValue('A1', "Report SMS");
$excel->getActiveSheet()->mergeCells('A1:F1');
$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
$excel->getActiveSheet()->SetCellValue('A2', 'Date Range '.date('Y-m-d'));
$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1

 //header tabel nya pada baris ke 3
 $excel->setActiveSheetIndex(0)->setCellValue('A4', "No"); // Set kolom A3 dengan tulisan "NO"
 $excel->setActiveSheetIndex(0)->setCellValue('B4', "Customerid"); // Set kolom B3 dengan tulisan "Customerid"
 $excel->setActiveSheetIndex(0)->setCellValue('C4', "DEBITUR NAME"); // Set kolom C3 dengan tulisan "DEBITUR NAME"
 $excel->setActiveSheetIndex(0)->setCellValue('D4', "SEGMENT"); // Set kolom D3 dengan tulisan "SEGMENT"
 $excel->setActiveSheetIndex(0)->setCellValue('E4', "RECEPIENT"); // Set kolom E3 dengan tulisan "RECEPIENT"
 $excel->setActiveSheetIndex(0)->setCellValue('F4', "SEND DATE"); // Set kolom F3 dengan tulisan "SEND DATE"
 $excel->setActiveSheetIndex(0)->setCellValue('G4', "UPLOAD DATE"); // Set kolom G3 dengan tulisan "UPLOAD DATE"
 $excel->setActiveSheetIndex(0)->setCellValue('H4', "WO DATE"); // Set kolom H3 dengan tulisan "WO DATE"
 $excel->setActiveSheetIndex(0)->setCellValue('I4', "SMS STATUS"); // Set kolom I3 dengan tulisan "SMS STATUS"
 $excel->setActiveSheetIndex(0)->setCellValue('J4', "MESSAGE"); // Set kolom J3 dengan tulisan "MESSAGE"
 $excel->setActiveSheetIndex(0)->setCellValue('K4', "DC"); // Set kolom K3 dengan tulisan "DC"
 $excel->setActiveSheetIndex(0)->setCellValue('L4', "TL"); // Set kolom L3 dengan tulisan "TL"
 $excel->setActiveSheetIndex(0)->setCellValue('M4', "Template"); // Set kolom M3 dengan tulisan "Template"

 //set style header
 $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('H4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('I4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('J4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('K4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('L4')->applyFromArray($style_col);
 $excel->getActiveSheet()->getStyle('M4')->applyFromArray($style_col);

 //set height baris 1-3
 $excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
 $excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
 $excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);

 $date = date("Y-m-d");
 $result = "SELECT d.deb_id AS cust_id,d.deb_name,DATE_FORMAT(d.deb_wo_date,'%d-%m-%Y') AS wo_date,
				d.deb_perm_msg_coll,s.Recipient,DATE_FORMAT(s.SendDate,'%d-%m-%Y %H:%i:%s') AS senddate,ss.StatusCode,
				s.TemplateId,s.Message,a.id AS agent,b.id AS tl,DATE_FORMAT(d.deb_created_ts, '%d-%m-%Y') AS tglupload,
					CASE
						WHEN s.TemplateId = 13 THEN 'Fresh WO (+5)'
						WHEN s.TemplateId = 15 THEN 'Fresh WO (+20)'
						WHEN s.TemplateId = 14 THEN 'Fresh WO (+30)'
						WHEN s.TemplateId = 17 THEN 'Fresh WO (+53)'
						WHEN s.TemplateId = 18 THEN 'Fresh WO (+75)'
						WHEN s.TemplateId = 23 THEN 'Fresh WO (+100)'
						WHEN s.TemplateId = 19 THEN 'Fresh WO (+150)'
						WHEN s.TemplateId = 20 THEN 'Fresh WO (+175)'						
					END AS SMStype						
				FROM t_gn_debitur d
				INNER JOIN serv_sms_outbox s ON d.deb_id=s.MasterId
				INNER JOIN t_tx_agent a ON d.deb_update_by_user=a.UserId
				INNER JOIN t_tx_agent b ON a.tl_id=b.UserId
				INNER JOIN serv_sms_status ss ON s.SmsStatus=ss.StatusId
				WHERE 1=1
				AND s.TemplateId IN (13,15,14,17,18,23,19,20)
				AND s.SendDate >= '".$date." 00:00:00'
				AND s.SendDate <= '".$date." 23:00:00'
				GROUP BY d.deb_id;";
 $qry =mysqli_query($con,$result);

 $no 	= 1; // Untuk penomoran tabel, di awal set dengan 1
 $numrow= 5;
 while($sql = mysqli_fetch_array($qry)) {
 	$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
 	$excel->setActiveSheetIndex(0)->setCellValueExplicit('B'.$numrow, $sql['cust_id'],PHPExcel_Cell_DataType::TYPE_STRING);
 	$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $sql['deb_name']);
 	$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $sql['deb_perm_msg_coll']);
 	$excel->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, $sql['Recipient'],PHPExcel_Cell_DataType::TYPE_STRING);
 	$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $sql['senddate']);
 	$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $sql['tglupload']);
 	$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $sql['wo_date']);
 	$excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $sql['StatusCode']);
 	$excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $sql['Message']);
 	$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $sql['agent']);
 	$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $sql['tl']);
 	$excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $sql['SMStype']);

 	//APPLY STYLE ROW
 	$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
 	$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);

 	$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);

 	$no++;
 	$numrow++;
 }

 //set width kolom
 $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
 $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
 $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
 $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
 $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
 $excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
 $excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
 $excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
 $excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
 $excel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
 $excel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
 $excel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
 $excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

 // Set orientasi kertas jadi LANDSCAPE
 $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

 // Set judul file excel nya
 echo date('H:i:s') . " Rename sheet\n";
 $excel->getActiveSheet(0)->setTitle("Laporan Data SMS");
 $excel->setActiveSheetIndex(0);

 // Proses file excel
 /*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
 header('Content-Disposition: attachment; filename="Data Siswa.xlsx"'); // Set nama file excel nya
 header('Cache-Control: max-age=0');
 $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
 $write->save('php://output');*/
 echo date('H:i:s') . " Write to Excel2007 format\n";
 PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
 $objWriter = new PHPExcel_Writer_Excel2007($excel);
 $objWriter->save("/opt/enigma/webapps/hsbc/Export/ReportSMS-".date('Ymd').".xlsx");
 echo date('H:i:s') . " Done writing file.\r\n";


?>