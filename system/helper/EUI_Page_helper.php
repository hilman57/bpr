<?php 

/*
 * @ def 	:  under lib EUI_Page Libraries 
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
 
 if( !function_exists('page_font_family') )
 {
	function page_font_family( $font = null  ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) {
			return $UI->EUI_Page->_set_font_family($font);	
		}
	}
	
 }


 
 /*
 * @ def 	:  under lib EUI_Page Libraries 
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: $field
 * @ param  : $rows 
 * @ param  : $func  
 * @ akses 	: public function 
 */
 
 if(!function_exists('page_call_function'))
 {
	function page_call_function($field=null, $rows = NULL, $func = NULL )
	{	
		
		$result = NULL;
		if( in_array($field, array_keys($func) ) )
		{
			if( in_array( $func[$field], array('_getCurrency')) )
			{
				if( function_exists($func[$field]) ){
					$result = call_user_func($func[$field],$rows[$field]); 
					$result = ( $result ? 'Rp.&nbsp;'. $result : '-&nbsp;'); 
				} 
			}
			else
			{
				if( function_exists($func[$field]) ){
					$result = call_user_func($func[$field],$rows[$field]); 
				}
			}
		}		
		else{
			$result = ( $rows[$field] ? $rows[$field] : '-');
		}
		
		return $result;
	}
 }
/*
 * @ def 	:  font_size
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
  
  if( !function_exists('page_font_size') )
 {
	function page_font_size( $size = null  ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			$UI->EUI_Page->_set_font_size($size);	
		}
	}
	
 }
 
/*
 * @ def 	:  font_color
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
  
  if( !function_exists('page_font_color') )
 {
	function page_font_color( $color = null  ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			$UI->EUI_Page->_set_font_color($color);	
		}
	}
	
 }
 
/*
 * @ def 	:  background_color
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
  
  if( !function_exists('page_background_color') )
 {
	function page_background_color( $color = null  ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			$UI->EUI_Page->_set_background_color($color);	
		}
	}
 }
 
/*
 * @ def 	:  select_header
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
  
   if( !function_exists('page_header') )
 {
	function page_header( $header = null  ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_selected_header($header);	
		}
	}
 }

/*
 * @ def 	:  select_column
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
 
  if( !function_exists('column') )
 {
	function page_column( $column = null  ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_selected_columns($column);	
		}
	}
 }
 
 /*
 * @ def 	:  select_column
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
 
  if( !function_exists('page_labels') )
 {
	function page_labels() 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_getLabels();	
		}
	}
 }
 
  /*
 * @ def 	:  select_column
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
 
  if( !function_exists('page_style') )
 {
	function page_style() 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_get_order_style();	
		}
	}
 }

/*
 * @ def 	:  select_column
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
  if( !function_exists('page_set_style') )
 {
  function page_set_style( $key, $value ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_set_style($key, $value);	
		}
	}
 }
 
 /*
 * @ def 	:  SELECT PRIMARY SHOWING 
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
  if( !function_exists('page_primary') )
 {
	function page_primary() 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_getPrimary();	
		}
	}
 }
 
 
/*
 * @ def 	:  SELECT PRIMARY SHOWING 
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
  if( !function_exists('page_get_align') )
 {
	function page_get_align() 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_getAlign();	
		}
	}
 } 
 
/*
 * @ def 	:  SELECT PRIMARY SHOWING 
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
  if( !function_exists('page_set_align') )
 {
	function page_set_align( $a, $b ) 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_setAlign($a, $b);	
		}
	}
 }
 
 
 
  /*
 * @ def 	:  SELECT PRIMARY SHOWING 
 * --------------------------------------------------------------
 *
 * @ notes  : get local json functionality if nout found on PHP
 * @ param 	: array data 
 * @ akses 	: public function 
 */
 
  if( !function_exists('page_hidden') )
 {
	function page_hidden() 
	{
		$UI = & get_instance();
		if( class_exists('EUI_Page') ) 
		{
			return $UI->EUI_Page->_getHidden();	
		}
	}
 }
 
 
?>