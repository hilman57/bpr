<?php
/*
 * E.U.I Frame work 
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		E.U.I
 * @author		razaki team deplovment
 * @copyright	Copyright (c) 2007 - 2014, SALAM RAHMAT SEMESTA, Inc.
 * @since		Version 1.0
 */
 
// ------------------------------------------------------------------------

/* 
 * @ def 		: class poster data on class extends mailer on libraries mfunction 
 * --------------------------------------------------------------------------
 *
 * @ packaage  	: helper / libraries  
 * @ author 	: razaki team deplovment
 * @ date 		: 2014-12-13
 *
 */
 
// ------------------------------------------------------------------------

class EUI_SMLGenerator
{

/*
 * @ def : var global data 
 */ 
  
 protected $_post_to  = array();
 protected $_post_cc  = array();
 protected $_post_bcc = array();
 protected $_post_title = NULL;
 protected $_post_header = NULL;
 protected $_post_body = NULL;
 protected $_post_date = NULL;
 protected $_post_attachment = array();
 protected $_post_config = NULL;
 protected $_post_assign_id = NULL;
 protected $_post_status = NULL;
 protected $_post_path = NULL; //date('Y')."/".date('m') ."/". date('d');
 
// --------------------------------------------------------------------
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 private static $instance = NULL;
 
/*
 * @ def : patern simple akses pointer class  
 * --------------------------------------------------------------------
 */
 
public static function &get_instance()
{
  if( is_null(self::$instance)) 
  {
	self::$instance = new self();
  }
  
  return self::$instance;
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
public function __construct()
{
  $UI =&get_instance();
  
  $UI->load->model(array('M_Configuration'));  
  if( class_exists('M_Configuration') )
  {
	$this->_post_config =& M_Configuration::get_instance(); 
  }
  
  $this->_post_path = date('Y')."/".date('m') ."/". date('d');
  $this->_post_status = 1001;
  log_message('debug', "SMLGenerator Class Initialized");
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 public function set_send_date( $date = NULL )
 {
	$this->_post_date = date('Y-m-d H:i:s');
	
	/* @ override **/
	
	if( !is_null($date) ) 
	{
		$this->_post_date = $date;	
	}
 }
 

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 public function set_add_assign( $id = NULL ) {
	$this->_post_assign_id = $id;
 }
 
 
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
protected function reset_all_component()
{
 $this->_post_to = array();
 $this->_post_cc = array();
 $this->_post_bcc = array();
 $this->_post_title = NULL;
 $this->_post_header = NULL;
 $this->_post_body = NULL;
 $this->_post_assign_id = NULL;
 $this->_post_attachment = array();
 
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
protected function _set_on_destination($UI, $OutboxId = 0 )
{

 if(is_array($this->_post_to) ) 
	foreach( $this->_post_to as $keys => $add_address )
 {
	$UI->db->set('EmailDestination',$add_address); 
	$UI->db->set('EmailReffrenceId', $OutboxId); 
	$UI->db->set('EmailCreateTs', $this->_post_date);	
	$UI->db->set('EmailDirection', 2);
	$UI->db->insert('email_destination');
	if( $UI->db->affected_rows() > 0) 
	{
		$conds++;
	}
 }
 
 return $conds;
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
protected function _set_on_copycarbone($UI, $OutboxId = 0 )
{
 if(is_array($this->_post_cc) ) 
  foreach( $this->_post_cc as $keys => $add_address )
  {
	$UI->db->set('EmailCC',$add_address); 
	$UI->db->set('EmailReffrenceId', $OutboxId); 
	$UI->db->set('EmailCreateTs', $this->_post_date);	
	$UI->db->set('EmailDirection', 2);
	$UI->db->insert('email_copy_carbone');
	if( $UI->db->affected_rows() > 0) 
	{
		$conds++;
	}
 }
 
 return $conds;
 
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
protected function _set_on_blindcarbone($UI, $OutboxId = 0 )
{
 if(is_array($this->_post_bcc) ) 
  foreach( $this->_post_bcc as $keys => $add_address )
  {
	if( $add_address!='' )
	{
		$UI->db->set('EmailBCC',$add_address); 
		$UI->db->set('EmailReffrenceId', $OutboxId); 
		$UI->db->set('EmailDirection', 2);
		$UI->db->set('EmailCreateTs', $this->_post_date);	
		$UI->db->insert('email_blindcopy_carbone');
		if( $UI->db->affected_rows() > 0)  
		{
			$conds++;
		}
	}	
 }
 
 return $conds;
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
protected function _set_on_attachment($UI, $OutboxId = 0 ) 
{

$conds  = 0;

if(is_array($this->_post_attachment) ) 
  foreach( $this->_post_attachment as $attachment ) 
{
	$attach = $this->FileWriteAttachment($attachment, $OutboxId);
	
	if( $attach )
	{
		$UI->db->set('EmailAttachmentPath',$attach['path']); 
		$UI->db->set('EmailAttachmentSize',$attach['size']);
		$UI->db->set('EmailAttachmentType',$attach['mime']);
		$UI->db->set('EmailReffrenceId', $OutboxId); 
		$UI->db->set('EmailCreateTs', $this->_post_date);	
		$UI->db->set('EmailDirection', 2); 
		$UI->db->insert('email_attachment_url');
		
		if( $UI->db->affected_rows() > 0) 
		{
			$conds++;
		}
	}
}
 return $conds;
}


/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
protected function _set_on_queue( $UI, $OutboxId = 0 )
{
  $conds = 0;
  if(($OutboxId) AND ($this->_post_date))
  {
	$UI->db->set('QueueMailId', $OutboxId );
	$UI->db->set('QueueStatusTs',$this->_post_date);
	$UI->db->set('QueueCreateTs',$this->_post_date); 
	$UI->db->set('QueueStatus', $this->_post_status);
	$UI->db->set('QueueTrying',0);
	$UI->db->insert('email_queue');
	
	if( $UI->db->affected_rows()> 0 ) 
	{
		$conds++;
	}
  }
  
  return $conds;
 } 
	

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
public function set_compile()
{
  static $config = NULL;
 /** get mail sender on configuration db **/
  $conds = 0;
  $config =& $this->_post_config->_getMail();
 if( $config )
 {
	$UI =& get_instance();
	$UI->db->set('EmailSender', $config['smtp.auth']); // on configuratio 
	$UI->db->set('EmailContent', $this->_post_body ); // html or text 
	$UI->db->set('EmailSubject', $this->_post_title ); // subject 
	$UI->db->set('EmailAssignDataId', $this->_post_assign_id); // subject 
	$UI->db->set('EmailStatus', $this->_post_status); // Ready status
	$UI->db->set('EmailCreateTs', $this->_post_date); 
	
	// then insert to db 
	
	$UI->db->insert('email_outbox');
	
	if( $UI->db->affected_rows() > 0 )
	{
		$OutboxId = $UI->db->insert_id();
		if( $OutboxId )
		{
		
		/** post destination **/
			$this->_set_on_destination($UI, $OutboxId);
			
		/** post copy carbone **/
			$this->_set_on_copycarbone($UI, $OutboxId);
			
		/** post blind copy carbone **/
			$this->_set_on_blindcarbone($UI, $OutboxId);
			
		/** _set_on_attachment **/
			$this->_set_on_attachment($UI, $OutboxId);
			
		/** _set_on_queue **/
			$this->_set_on_queue($UI, $OutboxId);
		}
		
		$conds++;
	}
 }
 else
 {
	log_message('debug', "Configuration is not found.");
 }
 
 // reset all component set variable 
 
 $this->reset_all_component();
 
 // OK 
 
 
 return $conds;
 
}

/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
 
 public function set_add_to( $_address =NULL, $_name = null )
 {
	if( !is_array($_address) 
		AND !is_null($_address) AND !is_null($_name) ){
		$this->_post_to[trim($_address)] = $_name;
	}	
	else if( !is_array($_address) 
		AND is_null($_address)==FALSE  AND is_null($_name) )
	{
		$this->_post_bcc[trim($_address)] = $_address;
	}
	else 
	{
		if( !is_null($_address))
		{
			foreach($_address as $add_address => $add_name ) 
			{
				$this->_post_to[trim($add_address)] = $add_name;	
			}
		}	
	}
 }
 
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */

 public function set_add_cc( $_address =NULL, $_name = null )
 {
	if( !is_array($_address) 
		AND is_null($_address)==FALSE )
		$this->_post_cc[trim($_address)] = $_name;
	else 
	{
		if( is_null($_address)==FALSE )
		{
			foreach($_address as $add_address => $add_name ) {
				$this->_post_cc[trim($add_address)] = $add_name;	
			}
		}	
	}
 }
 
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */

 public function set_add_bcc( $_address =NULL, $_name = null )
 {
	if( !is_array($_address) 
		AND is_null($_address)==FALSE  AND !is_null($_name) )
	{
		$this->_post_bcc[trim($_address)] = $_name;
	}	
	else if( !is_array($_address) 
		AND is_null($_address)==FALSE  AND is_null($_name) )
	{
		$this->_post_bcc[trim($_address)] = $_address;
	}
	else 
	{
		if( is_null($_address)==FALSE )
		{
			foreach($_address as $add_address => $add_name ) {
				$this->_post_bcc[trim($add_address)] = $add_name;	
			}
		}	
	}
 }
 
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */

 public function set_add_title( $title = NULL )
 {
	if(!is_array($title) 
		AND is_null($title) ==FALSE ) 
	{
		$this->_post_title = $title;
	}
 } 
  
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
public function set_add_header( $header = null )
{
	if(!is_array($header) 
		AND is_null($header) ==FALSE ) 
	{
		$this->_post_header = $header;
	}
 }  
 
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
public function set_add_body( $set_add_body =NULL )
{
	$this->_post_body = $set_add_body;
 }  

 
/*
 * @ def : var global data 
 * --------------------------------------------------------------------
 */
public function set_add_attachment( $_add_atachment = NULL )
 {
	if( !is_array($_add_atachment) 
		AND !is_null($_add_atachment) )
		$this->_post_attachment[trim($_add_atachment)] = $_add_atachment;
	else 
	{
		if( !is_null( $_add_atachment) )
		{
			foreach( $_add_atachment as $attach_url => $attach_name ) 
			{
				$this->_post_attachment[trim($attach_name)] = $attach_name;	
			}
		}	
	}
 }  
 
  
/*
 * @ def 		: FileWriteAttachment
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
private function FileWriteAttachment( $BASE_PATH = NULL , $OutboxId  = 0) 
{

 $files = array();
 if(($OutboxId > 0) )
 {
	$config =& $this->_post_config->_getMail();
	if( $config )
	{ 
	
	 /** get file of name **/
	 
		$BASE_PATH_OUTBOX  = $config['outbox.path'];
		$BASE_PATH_ATTACHMENT = "{$BASE_PATH_OUTBOX}/{$this->_post_path}/{$OutboxId}";
		$BASE_FILE_NAME = basename($BASE_PATH);
		
		if(!is_dir($BASE_PATH_ATTACHMENT)){
			mkdir($BASE_PATH_ATTACHMENT, 0777,TRUE); 	
		}
		
		$sn = 0;
		$BASE_FILE_EXIST = $BASE_PATH_ATTACHMENT ."/". $BASE_FILE_NAME;
		
		if(copy( $BASE_PATH,  $BASE_FILE_EXIST ))
		{
			if( FILE_EXISTS($BASE_FILE_EXIST) )
			{
				$BASE_FILE_SIZE =  FILESIZE($BASE_FILE_EXIST);
			
				$files['path'] = $BASE_FILE_EXIST;
				$files['size'] = $BASE_FILE_SIZE;
				$files['mime'] = 'application/vnd.ms-excel';
			}	
		}
	 }	 
  }	
  
  return $files;
  
} 

 
// END OF CLASS 
 
}


/*
 * return with function 
 */


if(!function_exists('SML_Generator') )
{  function SML_Generator()  
{
	if(!class_exists('EUI_SMLGenerator')) {
		return FALSE;
	}
	
	return EUI_SMLGenerator::get_instance();
	
  }
}

// EXAMPLE 

// $this->load->helper('EUI_SMLGenerator');
// SML_Generator()->set_send_date(date('Y-m-d H:i:s'));
// SML_Generator()->set_add_assign('10');
// SML_Generator()->set_add_title('hello world');
// SML_Generator()->set_add_body('test html');
// SML_Generator()->set_add_to('jombi_par@yahoo.com');
// SML_Generator()->set_add_bcc('jombi_par@yahoo.com');
// SML_Generator()->set_add_bcc('wisnu@yahoo.com');
// SML_Generator()->set_add_attachment('/opt/enigma/www/test1.xls');
// SML_Generator()->set_add_attachment('/opt/enigma/www/test2.xls');
// SML_Generator()->set_add_attachment('/opt/enigma/www/test3.xls');
// SML_Generator()->set_compile();
// print_r(SML_Generator())
	

?>