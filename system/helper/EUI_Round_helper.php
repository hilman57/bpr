<?php 
/**
 * @Project [SUA Process rounder ]
 * @version [1.0.1]
 * @author 	[razaki team]
 * 
 * @class  [Booking / apointment ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	

class siklus{
 var $p0  = array();  		// ---- result data olahan  
 var $p1  = array();		// ---- data on saving process temp 
 var $p2  = array();  		// ---- users on temporary 
 var $p6  = array();   		// ---- dynamic user 
 var $p7  = array();   		// ---- dynamic data process 
 var $p4  = 0;   			// ---- frequency process 
 var $p3  = 0; 	 	 		// ---- total data ALL
 var $p5  = 0;		 		// ---- total save data on temp
 var $p8  = null;    		// ---- select top and bottom 
 var $p9  = null;    		// ---- select top and bottom 
 var $p12 = null; 			// user_paling_atas 
 var $p13 = null; 			// user_paling_bawah:
 var $topToDown = array(); 
 var $downToTop = array();
	
/**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	

 private static $instance= null;

/**
 * @Project [SUA Process __construct ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function __construct( $user = null ){ 
	$dataSorter = array();
	$this->topToDown = $user; 
	$this->downToTop = $user;
	if(count($this->downToTop)){
	   krsort($this->downToTop);
	   foreach($this->downToTop as $i => $newUser ){
		   $dataSorter[] = $newUser;
	   }
	}
	// revers data index:
	$this->downToTop = (array)$dataSorter;  
 }

/**
 * @Project [SUA Process Instance ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public static function &Instance(){
	if(is_null(self::$instance)){
	   self::$instance = new self();
	}
	return self::$instance;
 } 
 
 /**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function preset( $data = null ){
	$preset = array(); $i = 0;
	if(is_array($data))
	foreach($data as $key => $val ){
		$preset[$i] = $val;
		$i++;
	}
	// reset back to data :
	if( count($preset) == 0) {
		return 0;
	}
	// overider :
	$this->p7 = (array)$preset;
	return $this->p7;
 }
 

  
/**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function Rounder($user = null, $data=null, $top= null, $low = null, $freq = 0, $stop = null, $setup = null ){
	$this->p6  = (array)$user;   
	$this->p7  = (array)$data;    
	$this->p4  = (int)$freq;   	 	 
	$this->p12 = $top; 	// user_paling_atas 
	$this->p13 = $low; 	// user_paling_bawah:
	$this->p14 = true; 	// skip index :
	 
	// testing
	if(!$this->p4){
	   $this->p3+= count($this->p7);
	}
	
	// batas atas :
	if(!strcmp($this->p8, $this->p12)){
		$this->p6  = (array)$this->topToDown;
	} 
	
	// batas atas :
	if(!strcmp($this->p8, $this->p13)){
		$this->p6  = (array)$this->downToTop;
	} 
	
	// starting process on here :
	$i = 0; 
	foreach($this->p6 as $key => $val ){
		if($data = $this->p7[$key]){
			$this->p0[] = array(
				'd0' => $data,
				'd1' => $val,
				'd2' => $this->p4 
			);
			
			$this->p8 = $val;
			if( $this->p8 ){
				unset($this->p7[$key]);
			}
		} 
	}  

	// set data on here :
	if($this->preset($this->p7)){ 
		$this->p4++;
		siklus::Rounder($this->p6, $this->p7, $this->p12, $this->p13, $this->p4, $this->p8);
	} 
	return $this;
 }
 
 /**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function data(){
	 return (array)$this->p0;
 }
 /**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function user(){
	 return (string)$this->p8;
 }
 /**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function counter(){
	 return $this->p4;
 }
/**
 * @Project [SUA Process rounder ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
} 
 

