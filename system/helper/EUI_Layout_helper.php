<?php
/*
 * @ def	: Layout Helpers 
 * ----------------------------------------- 
 * 
 * @ notes 	: Please use in view mode for && if 
 *			  you use in frame class You can Used 
			  $this ->Layout -> (optional);
 * @ param  : paramter helper			  
 */
 
if( !function_exists('base_layout'))
{
 function base_layout() 
 {
	$LYT =& get_instance();
	return $LYT ->Layout ->base_layout();
 }
}

/*
 * @ def	: set base_enigma 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_enigma'))
{  function base_enigma()
 {
	$LYT =& get_instance();
	$URI = $LYT ->Layout ->base_layout_enigma();
	return ( $URI ? $URI : null );
 }
}	


if( !function_exists('base_web_editor')){
	
	function base_web_editor()
	{
		$LYT =& get_instance();
		return $LYT ->Layout ->base_web_editor();
	}
}
/*
 * @ def	: set base_layout_jquery 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if(!function_exists('base_jquery'))
{ 
 function base_jquery()
 {
	$LYT =& get_instance();
	return $LYT ->Layout ->base_layout_jquery();
 }
}	

/*
 * @ def	: set base_js_layout 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_js_layout'))
{ 
 function base_js_layout()
 {
	$LYT =& get_instance();
	return $LYT ->Layout ->base_js_layout();
 }
}	

/*
 * @ def	: set base_image_layout 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_image_layout'))
{
	function base_image_layout() 
	{
		$LYT =& get_instance(); 
		return $LYT ->Layout ->base_image_layout();
	}
}

/*
 * @ def	: set base_themes_style 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_themes_style'))
{
 function base_themes_style($style=null)
 {
	$LYT =& get_instance(); 
	return $LYT->Layout->base_themes_style($style);
 }
}

/*
 * @ def	: set base_themes_style 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_style'))
{
  function base_style() 
  {
	$LYT =& get_instance(); 
	return $LYT->Layout->base_style();
  }  
}
 
/*
 * @ def	: set base_menu_model 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_menu_model') ) 
{
 function base_menu_model()
 {
	$_conds = null;
	$EUI =& get_instance();
	$EUI -> load -> Model('M_Menu');
	
	if( class_exists('M_Menu') )
	{
		$data= $EUI -> M_Menu -> _get_acess_menu();
		if( is_array($data) )
		{
			$_conds = $data;
		}
	}
	
	return $_conds;	
 }
}  

/*
 * @ def	: set base_layout_style 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_layout_style'))
{
  function base_layout_style() 
  {
	$_URI = base_url() ."library/styles/". base_layout() ."/default";
	if( $_URI )
	{
		return $_URI;
	}	
  }
}

/*
 * @ def	: set base_library 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_library')) 
{ 
  function base_library() 
  {
	$LYT =& get_instance(); 
	return $LYT -> Layout ->base_library();
 }
 
}


/*
 * @ def	: set base_menu_layout 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */

if( !function_exists('base_menu_layout') ) {
 function base_menu_layout( $m = null) 
 {
   $_compile = ''; 
  if(is_array($m) && !is_null($m))
  {
    if( isset($m['data']) && is_array($m) )
   {
	 $_compile .= '<div ';
	 
	// create class div menu 
	 if( isset($m['container']['class']) && !is_null($m['container']['class'])){
		$_compile .= ( $m['container']['class'] ? 'class="'. $m['data']['container']['class'] .'"':'');
	 }	
	
	// create class div id 	
	 if( isset($m['container']['id']) && !is_null($m['container']['id']) ){
		$_compile .= ( $m['container']['id'] ? 'id="'. $m['container']['id'] .'" ':'');
	 }
	
	// create class div extra 	
	 if( isset($m['container']['extra']) && !is_null($m['container']['extra']) ){
		$_compile .= ( $m['container']['extra'] ? $m['container']['extra'] : '' );
	 }
	
	 $_compile .= ">\n";
	
	if( isset($m['data']) && ($m['data']) )
	 {
		foreach( $m['data'] as $c => $r )
		{
			if( $c )
			{
				$_compile.= str_replace('{title}', $c, $m['parent']['ahref'] ) ."\n";
				
				$_compile.= '<ul'; // create ull
				
				if( isset($m['child']['show']) && ( $m['child']['show'] )){
					$_compile.= ( !is_null( $m['child']['class']['ul']) ? $m['child']['class']['ul'] : '' );
				}	
				
				$_compile.= ">";
				foreach( $r as $k => $d )
				{
					$_compile .= '<li';
					$_compile .= ( !is_null($m['child']['class']['li']) ? "class=\"{$m['child']['class']['li']}\"" : "" );  
					$_compile .= '>';
					$_compile .= "<a href=\"javascript:void(0);\" id=\"{$d['id']}\" class=\"{$d['style']}\"";
					$_compile .= (!is_null($m['click']['action'])? "onclick=\"javascript:{$m['click']['action']}('{$d['file_name']}','{$d['menu']}');\"" : "x")." >{$d['menu']}</a>";
					$_compile .= "</li>";	
				}
				
				$_compile .= "</ul>\n";	
			}
		}
	  }
	}
	
	$_compile .= "</div>\n";
 }
 
 return $_compile;
}

/*
 * @ def	: base_chat_layout 
 * ----------------------------
 * @ param 	: keys , values 
 * @ return : procedure 
 */
 
if( !function_exists('base_chat_layout') ) {
 function base_chat_layout( $c = null ) {
  $_compile = '<div';
  if( !is_null($c) && is_array($c) )
  {
	if( isset($c['container'])) {
		$_compile .= ( !is_null( $c['container']['id'] )?' id="'.$c['container']['id'].'"': null);
		$_compile .= ( !is_null( $c['container']['class'])?' class="'.$c['container']['class'].'"': null);
		$_compile .= ( !is_null( $c['container']['extra'])?' style="'.$c['container']['extra'].'"': null);
	}
	
	$_compile.= '>';
	if( isset($c['parent'])){
		$_compile .= ( !is_null($c['parent']['title'] )? $c['parent']['title'] : null);
	}
			
	$_compile.= '<ul';
	if( isset($c['ul'])) {
		$_compile .= ( !is_null( $c['ul']['id'] )?'id="'.$c['ul']['id'].'"': null);
		$_compile .= ( !is_null( $c['ul']['class'])?'class="'.$c['ul']['class'].'"': null);
		$_compile .= ( !is_null( $c['ul']['extra'])?'style="'.$c['ul']['extra'].'"': null);
	}
			
	$_compile.= '>'; $_compile.= '</ul>'; 
  }
   
   $_compile.= '</div>';
   
   return $_compile;
   
 }} 
}	



// ----------------------------------------------------------------------------------------------------
/*
 * @ package : base window_header  
 */
 
 
if( !function_exists('base_window_header') )
{
	function base_window_header( $title = "Window Title" )
	{
		$arr_helper = "<!DOCTYPE html> \n".
			"<html>\n".
				"<head>\n".
				"<title>$title</title>\n".
					"<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_themes_style($website['_web_themes']) ."/ui.all.css?time=".time()."\">\n".
					"<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_layout_style() ."/styles.overwriter.css?time=". time() ."\">\n".
					"<script type=\"text/javascript\" src=\"". base_jquery() ."/ui/jquery-2.0.0.js?time=". time() ."\"></script>\n".
					"<script type=\"text/javascript\" src=\"". base_enigma() ."/cores/EUI_1.1.3.js?time=". time() ."\"></script>\n".
					"<script type=\"text/javascript\" src=\"". base_jquery() ."/plugins/jquery.seletdown.js?time=".time() ."\"></script>\n".
					"<script type=\"text/javascript\" src=\"". base_jquery() ."/plugins/jquery.customizetab.js?time=".time() ."\"></script>\n".
					"<script type=\"text/javascript\" src=\"". base_jquery() ."/plugins/jquery.spiner.js?time=".time() ."\"></script>\n";
					
			
			
		return $arr_helper;	
	}
}


?>