<?php
/* @ def 	: E.U.I Session Helper base on EUI_Session libraries 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
// ------------------------------------------------------------
 
/* @ def 	: _have_get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_have_get_session') )
{
	function _have_get_session($param)
	{
		$EUI =& get_instance();
		return $EUI -> EUI_Session -> _have_get_session($param);
	}
} 


// ------------------------------------------------------------
 
/* @ def 	: _get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_get_session') )
{
	function _get_session($param)
	{
		$EUI =& get_instance();
		return $EUI -> EUI_Session -> _get_session($param);
	}
} 

// ------------------------------------------------------------
 
/* @ def 	: _get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 if( !function_exists('_deleted_session') )
{
	function _deleted_session($param)
	{
		$EUI =& get_instance();
		return $EUI -> EUI_Session -> _unset_session($param);
	}
} 
 
 
// @ def : get_restore_session(); 
 if( !function_exists('_get_exist_session') )
{
	
	function _get_exist_session($param)
	{
		$EUI =& get_instance();
		return (_have_get_session($param)?_get_session($param):$EUI->URI->_get_post($param));
	}
}
 
 