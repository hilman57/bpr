<?php
/* 
 * @ def 	  : helper data from lib EUI_tools 
 * ---------------------------------------
 * @ param 	  : EUI_Tools_helper 
 * @ location : ../system/helpers/EUI_Tools_helper
 */

if(!function_exists('Spliter') ) 
 {
   function Spliter( $data = null,  $argc=",", $arr_map = null ) 
 {
	$out =& get_instance();
	if( !class_exists('Spliters') ){
		$out->load->helper('EUI_Spliter');	
	}
	$spl =&Spliters::Instance();
	$spl->inialize( $data, $argc, (array)$arr_map );
	return ( is_object($spl)  ? $spl : false );
	
  }
  
}

// ----------------------------------------------------------------------------
/*
 * @ pack  model directed akess  
 * 
 */

 if( !function_exists('Dropdown') )
{
	 function Dropdown() 
	{
		$CI= &get_instance();
		$CI->load->model(array('M_Combo'));
		if( !class_exists('M_Combo') )   {
			return NULL;
		}
		
		return M_Combo::get_instance();
	}
}

// ----------------------------------------------------------------------------
/*
 * @ pack  model directed akess  Phone type 
 * 
 */

 if( !function_exists('Phone') )
{
	 function Phone() 
	{
		$CI= &get_instance();
		$CI->load->model(array('M_PhoneType'));
		if( !class_exists('M_PhoneType') ) {
			return NULL;
		}
		return get_class_instance('M_PhoneType');
	}
}

// ----------------------------------------------------------------------------
/*
 * @ pack  model directed akess  Phone type 
 * 
 */
 if( !function_exists('PhoneRelation') )
{
	function PhoneRelation() {
		$out =&Phone(); 
		return $out->_getRelationship();
	}
}


// ----------------------------------------------------------------------------
/*
 * @ pack  model directed akess  Phone type 
 * 
 */
 if( !function_exists('PhoneType') )
{
	function PhoneType() {
		$out =&Phone(); 
		return $out->_getPhoneTypeList();
	}
}


// ----------------------------------------------------------------------------
/*
 * @ pack  model directed akess  Phone type 
 * 
 */
 if( !function_exists('PhoneMultiple') )
{
	function PhoneMultiple( $CustomerId = 0 ) 
   {
		$out =&Phone(); 
		return $out->_getMultiplePhone( $CustomerId );
	}
}

/* 
 * @ def 	  : helper data from lib EUI_tools 
 * ---------------------------------------
 * @ param 	  : EUI_Tools_helper 
 * @ location : ../system/helpers/EUI_Tools_helper
 */

if( !function_exists('is') )
{
	function is( $data = array(), $field = 0 )
	{
		if( isset( $data[$field] ) ) 
		{
			return $data[$field];
		}
		return null;
	}
} 

/* 
 * @ def 	  : helper data from lib EUI_tools 
 * ---------------------------------------
 * @ param 	  : EUI_Tools_helper 
 * @ location : ../system/helpers/EUI_Tools_helper
 */

 if( !function_exists('evalute') ) 
 {
    function evalute( $val = '' )
	{
		if( strlen($val) == 0 ){ 
			return 0; 
		} else {
			return $val;
		}
    }
} 
 
/* 
 * @ def 	  : helper data from lib EUI_tools 
 * ---------------------------------------
 * @ param 	  : EUI_Tools_helper 
 * @ location : ../system/helpers/EUI_Tools_helper
 */

 if(!function_exists('Objective') ) 
 {
   function & Objective( $data = null ) 
 {
	$out =& get_instance();
	if( !class_exists('EUI_Object') ){
		$out->load->helper(array('EUI_Object'));	
	}
	
	$Objective = new EUI_Object( $data );
	return $Objective;
	
  }
}
/* 
 * @ def : _getVersion of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getVersion') )
 {
	function _getVersion()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _version();
		}	
	}
}



/* 
 * @ def : get name of month 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('NameMonth') )
 {
	function NameMonth( $n = null )
	{
	   $UI =& get_instance();
	   if( !is_null($n) )
	   {
		 $n = (int)$n;
		 $month =& $UI->EUI_Tools->_getBulan('in'); // indonesia
		 return $month[$n];
	   }
	   else{
		return null;
	   }
	   
	}
 }
/* 
 * @ def : _getBrowser of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 if(!function_exists('NextMonth') )
 {
	function NextMonth( $date ) {
		$dates = explode("-", $date);
		$yyyy = $dates[0]; 
	$mm   = $dates[1];
	$mm++;
	
	if($mm>12){
		$mm = 1;
		$yyyy++;
	}

	if (strlen($mm)==1)$mm="0".$mm;
	return $yyyy."-".$mm;
	
	}
 }

/* 
 * @ def : _getBrowser of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getBrowser') )
 {
	function _getBrowser()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _os_browser();
		}	
	}
}
 
/* 
 * @ def :_getOS UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getOS') )
 {
	function _getOS()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _os_detected();
		}	
	}
} 

/* 
 * @ def : _getDuration instance of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDuration') )
 {
	function _getDuration($integer = 0 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_duration( $integer );
		}	
	}
} 


/* 
 * @ def : _getFormatSize of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getFormatSize') )
 {
	function _getFormatSize($integer = 0 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _get_format_size( $integer );
		}	
	}
} 

/* 
 * @ def : _getIP  of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getIP') )
 {
	function _getIP()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _get_real_ip();
		}	
	}
} 


/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getCurrency') )
 {
	function _getCurrency($Integer=0)
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_rupiah($Integer);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDateEnglish') )
 {
	function _getDateEnglish($Date=0)
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _date_english($Date);
		}	
	}
} 


/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getOptionDate') )
 {
	function _getOptionDate( $dates = null, $lang = 'en', $mod ='-')
	{
		$UI =& get_instance(); 
		$_dates = null; 
		if( !is_null($dates) && $dates > 0)
		{
			$_lives = explode("{$mod}", $dates);
			switch( $lang ) 
			{
				case 'en' : 
					$_curdate = "{$_lives[0]}-{$_lives[1]}-{$_lives[2]}";
					$_dates = _getDateEnglish($_curdate);
				break;
				
				case 'in' : 
					$_curdate = "{$_lives[0]}-{$_lives[1]}-{$_lives[2]}";
					$_dates = _getDateIndonesia($_curdate);
				break;
			}
		} else {
			$_dates = '-';
		}
		
		return $_dates;
	}
} 


/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getPhoneNumber') )
 {
	function _getPhoneNumber($String='')
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _getPhoneNumber($String);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDateIndonesia') )
 {
	function _getDateIndonesia($Date=0)
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _date_indonesia($Date);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getMasking') )
 {
	function _getMasking($v=null, $t='x')
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_masking($v,$t);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if( ! function_exists('_setMasking') )
{
	function _setMasking($v=null, $t='x') 
	{
	    $UI =& get_instance();
		if( $UI ) {
			return $UI->EUI_Tools->_setToMasking($v,$t);
		}	
	}
} 




/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getNextDate') )
 {
	function _getNextDate( $Date = null )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _NextDate($Date);
		}	
	}
} 


/* 
 * @ def : _getIP of UI // get range in date next 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getNextCurrDate') )
 {
	function _getNextCurrDate( $Date = null, $n=0 )
	{
		$UI =& get_instance();
		if( $UI ) {
			return $UI -> EUI_Tools -> _NextCurrDate( $Date, $n );
		}	
	}
} 

/* 
 * @ def : _getIP of UI // get range in date next 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if( !function_exists('_getPrevDate') )
{ 
 function _getPrevDate( $Date=NULL, $n) 
 {
	$UI =& get_instance();
	if( $UI )
	{
		return $UI -> EUI_Tools -> _PrevDate( $Date, $n );
	}	
 }
 
}	

/* 
 * @ def : _getIP of UI // get range in date next 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if( ! function_exists('_getSizeDayMonth') )
{ 
	function _getSizeDayMonth( $Month = NULL, $Year = NULL ) 
 {
	$UI =& get_instance();
	if( $UI )
	{
		return $UI->EUI_Tools->_getSizeDayMonth( $Month, $Year );
	}	
 }
 
}




/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDateDiff') )
 {
	function _getDateDiff( $d1, $d2 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _DateDiff( $d1, $d2 );
		}	
	}
}

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_getSortDate') )
{
	function _getSortDate( $bulan = 0 )
	{
		$_bulan = null;
		
		$UI =& get_instance();
		if( $UI )
		{
			$_list = $UI -> EUI_Tools ->_getBulan();
			$_bulan = $_list[(INT)$bulan];
		}
		
		return $_bulan;
	}
}	

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_setPassword') )
{
	function _setPassword( $pwd = '' )
	{
		$_pwd= null;
		
		$UI =& get_instance();
		if( $UI )
		{
			$_pwd = $UI->EUI_Tools->_setPassword($pwd);
			
		}
		
		return $_pwd;
	}
}	

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_getDateTime') )
{
  function _getDateTime( $date = null )
  {
	
	if(!is_null($date) and strlen($date) >1 )
	{
		return date('d-m-Y H:i:s', strtotime($date) );
	} else {
		return "-";
	}	
  }
}
/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('StartDate') )
 {
	function StartDate( $Date =0 ) {
		return  join(' ', array(_getDateEnglish($Date), '00:00:00'));	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('EndDate') )
 {
	function EndDate( $Date =0 ) {
		return join(' ', array(_getDateEnglish($Date),'23:59:59'));	
	}
} 

/** @ pack : print empty string **/

if(!function_exists('__print') ) 
{ 
  function __print( $argv = null )  
{
	if( is_null($argv) ) {
		return "-";
	}
	else if( strlen($argv)==0 ) {
		return "-";
	}	
	else {
		return $argv;
	}
  } 
}


// eval_date --------------- cek true or note 
if( !function_exists('EvaluateDate') )
{
	function EvaluateDate( $date='' )
	{
		if( strlen($date)<6 ){
			return null;
		}
		
		$arr_default = array('1970-01-01','01-01-1970');
		$evaldate = date('Y-m-d', strtotime($date) );
		if( !in_array( $evaldate, $arr_default ) )
		{
			return $date;
		}
		return null;
	}
} 


/** @ pack : print empty string **/

if(!function_exists('History') ) 
{ 
   function History( $CustomerId = 0 )   
  {
	 $last_notes = ""; 
	 $CI =& get_instance();
	 $CI->db->reset_select();
	 $CI->db->select("hs.CallHistoryNotes", FALSE);
	 $CI->db->from("t_gn_callhistory hs");
	 $CI->db->where("hs.CallHistoryId = 
		(
			select  MAX(a.CallHistoryId) 
			from t_gn_callhistory a 
			where a.CustomerId = '$CustomerId' ) ", "", 
	 FALSE);
	 
	// $CI->db->print_out();
	 
	 $rs = $CI->db->get();
	 if( $rs->num_rows() > 0 
		AND $row = $rs->result_first_assoc() ) 
	 {
		 $last_notes =(string)$row['CallHistoryNotes'];
	 }	
	 return $last_notes;
  }
}

// ==================== END CLASS ================