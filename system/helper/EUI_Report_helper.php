<?php 

//--------------------------------------------------------------------------
/*
 * @ package 		EUI_Report_helper
 */

 if( !function_exists('Report') ) 
{
	function Report() 
	{
		$CI =& get_instance();
		$CI->load->model(array('M_Report'));
		if(class_exists('M_Report') )
		{
			return M_Report::Instance();
		} 
		
		return null;
	}	
 }
 
//--------------------------------------------------------------------------
/*
 * @ package 	ReportType
 */
 
 if( !function_exists('ReportType') ) {
	function ReportType()
	{
		$Out =& Report();
		return $Out->_select_report_type();
	}	
} 

//--------------------------------------------------------------------------
/*
 * @ package 		ReportMode
 */
 
 if( !function_exists('ReportMode') ) {
	function ReportMode()
	{
		$Out =& Report();
		return $Out->_select_report_mode();
	}	
} 
 
//--------------------------------------------------------------------------
/*
 * @ package 		ReportGroup
 */
 
 if( !function_exists('ReportGroup') ) {
	function ReportGroup()
	{
		$Out =& Report();
		return $Out->_select_report_group();
	}	
} 
 
//--------------------------------------------------------------------------
/*
 * @ package 		Supervisor
 */
 
 if( !function_exists('Supervisor') ) {
	function Supervisor()
	{
		$Out =& Report();
		return $Out->_select_user_spv();
	}	
}  

//--------------------------------------------------------------------------
/*
 * @ package 		Teamleader
 */
 
 if( !function_exists('Teamleader') ) {
	function Teamleader()
	{
		$Out =& Report();
		return $Out->_select_user_tl();
	}	
}  

//--------------------------------------------------------------------------
/*
 * @ package 		SeniorLeader
 */
 
 if( !function_exists('Seniorleader') ) {
	function Seniorleader()
	{
		$Out =& Report();
		return $Out->_select_senior_tl();
	}	
}

//--------------------------------------------------------------------------
/*
 * @ package 		Agent
 */
 
 if( !function_exists('Agent') ) {
	function Agent()
	{
		$Out =& Report();
		return $Out->_select_user_agent();
	}	
}  

//--------------------------------------------------------------------------
/*
 * @ package 		AgentByTL
 */
 
 if( !function_exists('AgentByTL') ) {
	function AgentByLeader( $tl_id = 0 )
	{
		$Out =& Report();
		return $Out->_select_agent_by_tl( $tl_id );
	}	
} 
//--------------------------------------------------------------------------
/*
 * @ package 		AgentByTL
 */


if( !function_exists('AgentByLeaderInclude') ) {
	function AgentByLeaderInclude( $tl_id = 0 )
	{
		$Out =& Report();
		return $Out->_select_agent_by_tl_tlincluded( $tl_id );
	}	
}

//--------------------------------------------------------------------------
/*
 * @ package 		AgentBySpv
 */
 
 if( !function_exists('AgentBySpv') ) {
	function AgentBySpv( $spv_id = 0 )
	{
		$Out =& Report();
		return $Out->_select_agent_by_spv( $spv_id );
	}	
}   
//--------------------------------------------------------------------------
/*
 * @ package 		TlBySpv
 */
 
 if( !function_exists('TlBySpv') ) 
{
	function TlBySpv( $spv_id = 0 )
	{
		$Out =& Report();
		return $Out->_select_tl_by_spv( $spv_id );
	}	
}  

//--------------------------------------------------------------------------
/*
 * @ package 		TlBySpv
 */
 
 if( !function_exists('RowCcAgent') ) 
{
	function RowCcAgent( $UserId = 0 )
	{
		$Out =& Report();
		return $Out->_select_row_cc_agent( $UserId );
	}	
}  

//--------------------------------------------------------------------------
/*
 * @ package 		TlBySpv
 */
 
 if( !function_exists('Level') ) 
{
	function Level()
	{
		$Out =& Report();
		return $Out->_select_user_level();
	}	
}

//--------------------------------------------------------------------------
/*
 * @ package 		TlBySpv
 */
 
 if( !function_exists('Product_Outbound') ) 
{
	function Product_Outbound()
	{
		$Out =& Report();
		return $Out->campaign_outbound();
	}	
}    




// =============== END HELPER ===============================
 
?>