<?php

/**
 * E.U.I Frame work 
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		E.U.I
 * @author		razaki team deplovment
 * @copyright	Copyright (c) 2007 - 2014, SALAM Rahmat SEMESTA, Inc.
 * @since		Version 1.0
 */
 
 
// ------------------------------------------------------------------------

/**
 * SFTP Class
 *
 * @package		E.U.I Frame work 
 * @subpackage	Libraries
 * @category	Libraries
 * @author		razaki team deplovment
 */ 
class EUI_Sftp 
{


// --------------------------------------------------------------------
/**
 * @ def : global variable and public parameter 
 *
 */
 
	var $_host 		 = null;
	var $_user		 = null;
	var $_pwd 	 	 = null;
	var $_timeout 	 = 90;
	var $_port 		 = 22;
	var $_ssl 		 = FALSE;
	var $system_type = NULL;
	var $passive 	 = FALSE;
	var $_conn_id	 = FALSE;
	
// --------------------------------------------------------------------
/**
 * @ def : private parameter on class SFTP 
 *
 */	
	
	private $_errors	 = NULL;
	private $_last_error = NULL;
	private $_crlf		= "\n\r";
	
// --------------------------------------------------------------------

// --------------------------------------------------------------------
/**
 * @ def : constructor class 
 * --------------------------------------------------------------------
 * @ param : $_host ( string )
 * @ param : $_user ( string )
 * @ param : $_pwd ( string )
 * @ param : $_port ( integer )
 * @ param : $_timeout ( integer ) 
 */

private function set_error_log( $error = null )
{
	$this->_errors .= date('Y-m-d H:i:s') .'  '. $error . $this->_crlf;
	$this->_last_error = date('Y-m-d H:i:s') .'  '. $error . $this->_crlf;
}
	
// --------------------------------------------------------------------


/**
 * @ def : constructor class 
 * --------------------------------------------------------------------
 * @ param : $_host ( string )
 * @ param : $_user ( string )
 * @ param : $_pwd ( string )
 * @ param : $_port ( integer )
 * @ param : $_timeout ( integer ) 
 */
 
public function  __construct( $config = array() ) 
{
	if(count($config)> 0 )
	{
		$this->initialize($config);
	}
	
	log_message('debug', "FTP Class Initialized");
}


// --------------------------------------------------------------------
/**
 * @ def : Auto close connection
 * --------------------------------------------------------------------
 * 
 */	
public function initialize( $config = array() )
{
	foreach ($config as $key => $val){
		$this->$key = $val;
	}
	$this->_host = preg_replace('|.+?://|', '', $this->_host);
}
	
// --------------------------------------------------------------------	
/**
 * @ def : Auto close connection
 * --------------------------------------------------------------------
 * 
 */
public function  __destruct() 
{
	$this->close();
}

// --------------------------------------------------------------------

/**
 * Change currect directory on FTP server
 *
 * @param string $directory
 * @return bool
 */
 
public function changedir($directory = null) 
{
 // attempt to change directory
 if(ftp_chdir($this->_conn_id, $directory)) 
	return true; // fail
  else 
  {
	$this->set_error_log("Failed to change directory to \"$directory\"");
	return false;
  }
}

// --------------------------------------------------------------------

/**
 * Set file permissions
 * --------------------------------------------------------------------
 
 * @param int $permissions (ex: 0644)
 * @param string $remote_file
 * @return false
 */
 
 public function chmod($permissions = 0, $remote_file = null) 
 {
	// attempt chmod
	if(ftp_chmod($this->_conn_id, $permissions, $remote_file)){
		return true;
		// failed
	} else 
	{
		$this->set_error_log("Failed to set file permissions for \"{$remote_file}\"");
		return false;
	}
}

/**
 * Close FTP connection
 */
 
public function close() 
{
  if($this->_conn_id) 
  {
	ftp_close($this->_conn_id);
	$this->_conn_id = FALSE;
  }
}


// --------------------------------------------------------------------

/**
 * this function get connection ID 
 * --------------------------------------------------------------------
 
 * @param int $permissions (ex: 0644)
 * @param string $remote_file
 * @return false
 */
 
public function _is_conn()
{
 if(!is_resource($this->conn_id)) {
	$this->set_error_log("sftp_no_connection.");
	return FALSE;
}

 return TRUE;
}


// --------------------------------------------------------------------

/**
 * Connect to FTP server
 *
 * @return bool
 */
 
public function connect( $config = array() ) 
{
	if(count($config)> 0 ) {
		$this->initialize($config);
	}
	
// cek type connection **/
	
	if( !$this->_ssl ) 
	{
		// attempt connection
		if(!$this->_conn_id = ftp_connect($this->_host, $this->_port, $this->_timeout)) {
			$this->set_error_log("Failed to connect to {$this->_host}");
			return FALSE;
		}
		// SSL connection
	} 
	elseif(function_exists("ftp_ssl_connect")) 
	{
		// attempt SSL connection
		if(!$this->_conn_id = ftp_ssl_connect($this->_host, $this->_port, $this->_timeout)) {
			$this->set_error_log("Failed to connect to {$this->_host} (SSL connection)");
			return FALSE;
		}
		// invalid connection type
	}
	else
	{
		$this->set_error_log("Failed to connect to {$this->_host} (invalid connection type)");
		return FALSE;
	}

	// attempt login
	if(ftp_login($this->_conn_id, $this->_user, $this->_pwd)) 
	{
		// set passive mode
		ftp_pasv($this->_conn_id, (bool)$this->passive); // set system type
		$this->system_type = ftp_systype($this->_conn_id); // connection successful
		return TRUE; // login failed
	} 
	else 
	{
		$this->set_error_log("Failed to connect to {$this->_host} (login failed)");
		return FALSE;
	}
}

/**
 * Delete file on FTP server
 *
 * @param string $remote_file
 * @return bool
 */
 
public function delete($remote_file = null) 
{
	if ( ! $this->_is_conn()) { 
		return FALSE;
	}
	
	if(ftp_delete($this->_conn_id, $remote_file)) {
		return true;
	} else {
		$this->set_error_log("Failed to delete file \"{$remote_file}\"");
		return false;
	}
}

/**
 * Download file from server
 *
 * @param string $remote_file
 * @param string $local_file
 * @param int $mode
 * @return bool
 */
 
public function download($remote_file = null, $local_file = null, $mode = FTP_ASCII) 
{

  if ( ! $this->_is_conn())  {
	return FALSE;
 }
  
   if(ftp_get($this->_conn_id, $local_file,$remote_file, $mode)) {
		return true;
	} else 
	{
		$this->set_error_log("Failed to download file \"{$remote_file}\"");
		return false;
	}
}

/**
 * Get list of files/directories in directory
 *
 * @param string $directory
 * @return array
  */
public function ls($path = null) 
{
	$list = array();
	// attempt to get list
		if($list = ftp_nlist($this->_conn_id, $path)) {
			// success
			return $list;
		// fail
		} 
		else {
			$this->set_error_log("Failed to get directory list");
			return array();
		}
}

/**
 * Create directory on FTP server
 *
 * @param string $directory
 * @return bool
 */

 public function mkdir($path = null) 
 {
	// attempt to create dir
	if(ftp_mkdir($this->_conn_id, $path)) {
		return true;
	} 
	else {
		$this->set_error_log("Failed to create directory \"{$path}\"");
		return false;
	}
}
	
// -------------------------------------------------------------------- 
/**
 * Upload file to server
 * ---------------------------------------------------------------------
 * @param string $local_path
 * @param string $remote_file_path
 * @param int $mode
 * @return bool
 */
	 
public function upload($local_file = null, $remote_file = null, $mode = FTP_ASCII) 
{
	// attempt to upload file
	if(ftp_put($this->_conn_id, $remote_file, $local_file, $mode)) 
		{
			// success
			return true;
		// upload failed
		} else 
		{
			$this->set_error_log("Failed to upload file \"{$local_file}\"");
			return false;
		}
}

// -------------------------------------------------------------------- 
/**
 * Get current directory
 *
 * @return string
 */
 
public function pwd() 
{
	return ftp_pwd($this->_conn_id);
}


// -------------------------------------------------------------------- 

/**
 * Rename file on FTP server
 *
 * @param string $old_name
 * @param string $new_name
 * @return bool
 */
	 
public function rename($old_name = null, $new_name = null) 
{
		// attempt rename
		if(ftp_rename($this->_conn_id, $old_name, $new_name)) {
			// success
			return true;
		// fail
		} else {
			$this->set_error_log("Failed to rename file \"{$old_name}\"");
			return false;
		}
	}
	
// --------------------------------------------------------------------    
/**
 * @ def : get basename of file to execute 
 * ----------------------------------------------------------------------
 
 * @param string $old_name
 * @param string $new_name
 * @return bool
 */
	 
public function get_sftp_basename( $ftp_path = null )
{

 $result = null;
 if(!is_null($ftp_path))
 {
	if(!function_exists('basename') ) {
		log_message('debug', "FTP Class Initialized");
	}
	else{
		$result = basename($ftp_path);	
	}
 }	

 return result;
}


// --------------------------------------------------------------------   

/**
 * Rename file on FTP server
 *
 * @param string $old_name
 * @param string $new_name
 * @return bool
 */
	 
public function FTPdownload($filename) 
{
	if($filename){ 
		header("location:".$filename);
		return true;
	  }
 }
 
// --------------------------------------------------------------------    
/**
 * Remove directory on FTP server
 *
 * @param string $directory
 * @return bool
 */
 
public function rmdir($directory = null) 
{
	// attempt remove dir
	if(ftp_rmdir($this->_conn_id, $directory)) 
	{
		return true;
	} else 
	{
		$this->set_error_log("Failed to remove directory \"{$directory}\"");
		return false;
	}
}


// ----------------------------------------------------------
/**
 * get all error log every session login activity 
 *
 * @param string $directory
 * @return bool
 */
 
 public function get_error_log() {
	return $this->_errors;
 }
 
 
 
 // ----------------------------------------------------------
/**
 * get all error log every session login activity 
 *
 * @param string $directory
 * @return bool
 */
 
 public function get_last_error()
 {
	return $this->_last_error;
 }


}
?>