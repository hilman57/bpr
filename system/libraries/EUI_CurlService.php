<?php

/*
 * @ pack : EUI_CurlService 
 */
 
class EUI_CurlService 
{

var $argvs_string = null;
var $argc_string = null;
var $argc_webs = null;
var $argc_host = null;

/*
 * @ pack : EUI_CurlService 
 */
 
private static $Instance = NULL;

/*
 * @ pack : EUI_CurlService 
 */
 
 public function __construct()
{
 $this->argc_webs = "sms/?";
 $this->argc_host = "localhost";
} 
/*
 * @ pack : EUI_CurlService 
 */

 public static function  &Instance()
{
  if( is_null(self::$Instance) )
 {
		self::$Instance = new self();
	}
	
 return self::$Instance;
 
}
/*
 * @ pack : EUI_CurlService 
 */
 
 public function get_curl_server( )
{
 $UI =& get_instance();
 $UI->db->reset_select();
 $UI->db->select("a.RegServer");
 $UI->db->from("serv_sms_setting a");
 $UI->db->where("a.RegStatus", 1);
 
 $qry = $UI->db->get();
 
 if( $qry->num_rows()>0 )
 {
	if( $rows = $qry->result_first_assoc() )
	{
		$this->argc_host = (string)$rows['RegServer'];
	}
 }
 
 return $this->argc_host;
		
 }
 
/*
 * @ pack : reset curl 
 */
  
 public function reset_curl()
{
 if( is_array($this->argvs_string) )
 {
	$this->argvs_string = null;
 }
} 

/*
 * @ pack : EUI_CurlService 
 */
 
 public function set_curl( $keys =null, $values )
{
 if( !is_null($keys) )
 {
	$this->argvs_string[$keys] = $values;
 }
 
} 

/*
 * @ pack : EUI_CurlService 
 */
 
 public function get_curl()
{
 $this->argc_string = null;
 if( is_array($this->argvs_string) )
 {
	foreach( $this->argvs_string 
		AS $key => $values )
	{
		$this->argc_string .= "&{$key}=". rawurlencode($values);
	}	
	
	$this->argc_string = (string)substr($this->argc_string, 1, strlen($this->argc_string));
 }
 
 return $this->argc_string;
 
} 

/*
 * @ pack : compile_socket sen via socket ()
 */
 
 public function compile_socket()
{
 $UI =& get_instance();
 
 $UI->load->helper('EUI_Socket');
// @ pack : set command return  .... 
 
 $conds = 0;
 
// @ pack :  get server sms .... 
 
 Socket()->set_fp_server( $this->get_curl_server(), 1800 );
// @ pack : create command  .... 
 Socket()->set_fp_command("
	Action:send_sms\r\n".
	"MasterId:{$this->argvs_string['MasterId']}\r\n".
	"phone_number:{$this->argvs_string['phone_number']}\r\n".
	"templateId:{$this->argvs_string['templateId']}\r\n".
	"userid:{$this->argvs_string['userid']}\r\n".
	"location:{$this->argvs_string['location']}\r\n".
	"text_messages:{$this->argvs_string['text_messages']}\r\n"
 );

// @ send on to server  .... 
 if( Socket()->send_fp_comand() )
 {
	$conds++;
 }

 return $conds;
 
} 

/*
 * @ pack : compile_curl()
 */
 
 public function compile_curl()
{
 
 $conds = 0;
 $this->curl_data ="http://{$this->get_curl_server()}/{$this->argc_webs}{$this->get_curl()}";
 
 if( $this->curl_data )
 {
	if( function_exists('curl_init') )
	{
		$this->CURL_REQUEST = curl_init( $this->curl_data );
		
	 // @ pack : user curl its 
	 
		curl_setopt($this->CURL_REQUEST, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->CURL_REQUEST, CURLOPT_BINARYTRANSFER, TRUE);
		
	// @ get ouput 
	
		$this->CURL_OUPUT = curl_exec($this->CURL_REQUEST);
		
		if( (trim($this->CURL_OUPUT)!=''))
		{
			$this->reset_curl();
			$conds++;
		}
	} 
 }
 
return $conds;
 
} 

// END CLASS 

 
 

}

?>