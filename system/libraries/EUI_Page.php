<?php

/**
 * @ package 	: EUI_Page 
 * @ subpackage : libraries 
  --------------------------------
 * @ notes 		: compile and generate on grid attribute data 
 * 				  with simple data suport order only 
 * @ params		: - 
 * ------------------------------------------------------------------------------------------
 * # change of date :  2014-11-28 @ omens // hnadle cache , and handle where in 
 *
 */
 
 
class EUI_Page 
{
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 var $_pages    = 0;
 var $_start    = 0;
 var $_post     = 0;
 var $_where    = array();
 var $_and 	    = array();
 var $_from     = array();
 var $_join     = array();
 var $_or_like  = array();
 var $_or 	    = array();
 var $_primary  = array();
 var $_hidden	= array();
 var $_selected = array();
 var $_or_cache = array();
 var $_arr_func = array();
 
 
/** set null default properies **/ 

 var $_align	= NULL;
 var $_query    = NULL;
 var $_pref     = FALSE;
 var $_conds    = NULL;
 var $_compile  = NULL;
 var $_labels   = NULL;
 var $_styles   = NULL;
 var $_ascii    = NULL;
 
/** css styles on grid view ***/
 
 var $_font_color  		= NULL;
 var $_background_color = NULL;
 var $_font_size 		= NULL;
 var $_font_family 		= NULL;
 var $_rows_styles 		= array();
 var $_CRLF 			= ':';
 var $_EOFL 			= ';';
	
/* 
 * @ pack :  instance  
 * ---------------------------------------------------------------
 */
 
 private static $Instance = NULL;

 /* 
 * @ pack :  instance  
 * ---------------------------------------------------------------
 */
 
 public static function &Instance()
{
	if(is_null(self::$Instance) ) {
		self::$Instance = new self();
	}
	
	return self::$Instance;
 }
  
/* 
 * @ pack :  instance  
 * ---------------------------------------------------------------
 */
 
function EUI_Page()
{ 
	$this->_post  = 0;
	$this->_start = 0;
	$this->_pages = 0;
	$this->_query = NULL;
	$this->_where = NULL;
	$this->_conds = "WHERE 1=1";
	$this->_pref  = FALSE;
	$this->_labels = NULL;
	$this->_ascii = "*";
}	


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setAlign( $field, $style)
{
	if( !is_null($field) 
		AND !is_null($style) 
		AND in_array($field, array_keys($this->_labels) ))
	{
		$this->_align[$field] = $style;
	}	
}
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function set_call_function($field, $function )
{
	if(!is_null($function) AND !empty($function)) 
	{
		$this->_arr_func[$field]= $function; 	
	}
}
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _getAlign()
 {
	if( count($this->_align) > 0  )
	{
		return $this->_align;
	}
	else
		return null;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _setPage( $_int )
{
	$this -> _pages = $_int;
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: array('fieldname' => 'labelname ')
 * @ return 	: void(0)
 */

 public function _setLabels($labels = NULL)
 {
	if(!is_null($labels) AND is_array($labels) ) {
		$this->_labels = $labels;
	}
 }
 
 
 
 /*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: array('fieldname' => 'labelname ')
 * @ return 	: void(0)
 */

 public function _getLabels()
 {
	if(!is_null($this->_labels)) {
		return $this->_labels;
	}
	else
		return FALSE;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _postPage( $_post= '' )
 {
	if($_post!='') 
		$this->_post = $_post;
	else
		$this->_post = 0;
 }

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _setQuery( $_sql = '' )
{
	$this->_query .= $_sql." ". $this->_conds;
	$this->_setCompiler($this->_query);
	
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
private function _setCompiler( $_arr_compile = null ) 
{
	if( !is_null($_arr_compile) ) 
	{
		$this-> _compile .= $_arr_compile ; 
	}	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: 2 dimensi // array( 'UserId' => 'User Name')
 * @ return 	: void(0)
 */

protected function _string_covert_array( $string = '' )
{
	$labels = FALSE;
	if( $explode = explode(",", $string ) )
	{
		foreach( $explode as $is => $fieldname ) 
		{
			$space = explode(" ", $fieldname);
			if( count($space) > 1  )
			{
				$labels[$space[count($space)-1]] = $space[count($space)-1]; 
			}
			else
			{
				$labels[trim($fieldname)] = trim($fieldname);
			}
		}
	}

	return $labels;	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: 2 dimensi // array( 'UserId' => 'User Name')
 * @ return 	: void(0)
 */

protected function _setPrimary( $keys = null )
{
	if( !is_array($keys) ) 
	{
		$this->_primary[trim($keys)] = trim($keys);
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: 2 dimensi // array( 'UserId' => 'User Name')
 * @ return 	: void(0)
 */
 
protected function _setHidden( $keys )
{
	if( !is_array($keys) ) 
	{
		$this->_hidden[trim($keys)] = trim($keys);
	}
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: 2 dimensi // array( 'UserId' => 'User Name')
 * @ return 	: void(0)
 */

 
public function _getHidden() 
{
	if( is_array($this->_hidden) ) {
		return $this->_hidden;
	}
	else{
		return FALSE;
	}
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: 2 dimensi // array( 'UserId' => 'User Name')
 * @ return 	: void(0)
 */

public function _getPrimary() 
{
	if( is_array($this->_primary) )
	{
		return reset($this->_primary);
	}
	else{
		return FALSE;
	}
}



/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: 2 dimensi // array( 'UserId' => 'User Name')
 * @ return 	: void(0)
 */

public function _setArraySelect( $fieldname = NULL )
{
	if( !is_array($fieldname) )	
	{
		// look not "/*/" 
		
		if( $fieldname!=$this->_ascii) 
		{
			$this->_setSelect($fieldname);
			// look is string or NOT 
			
			if( $label = $this->_string_covert_array($fieldname) AND is_bool($label)==FALSE )
			{
				$this->_setLabels($label);
			}
		}
		
		// look yes "/*/"
		
		
		if( $fieldname == $this->_ascii )
		{
			$this->_setSelect('*');
		}	
		
		return TRUE;	
	}	
	
	/** if is array ****/
	
	if( is_array($fieldname)) 
	{
		$labels = null;
		foreach( $fieldname as $key => $rows )
		{
			if( is_array($rows) )
			{
				 // set primary key to available 
				
				if( isset($rows[2]) AND ( strtolower($rows[2])=='primary' ) ) 
				{
					$this->_setPrimary($rows[0]);	
				}
				
				// set hidden field 
				
				if( isset($rows[2]) AND ( strtolower($rows[2])=='hidden' ) ) 
				{
					$this->_setHidden($rows[0]);	
				}
				
				// set label field 
				
				if( !isset( $rows[2] ) OR ( $rows[2]==FALSE ) OR ( $rows[2]=='' ) )
				{  
					$labels[trim($rows[0])] = $rows[1];
				}		
			}
			else
			{
				$labels[trim($rows)] = $rows; 	
			}
		}
		
		$this->_setLabels($labels); 
		$this->_setSelect( implode(",",array_keys($fieldname)), FALSE );
		return TRUE;
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _setSelect($_sql=NULL, $pref = FALSE )
{
	$this->_pref = $pref; 
	if(!is_null($_sql))
	{
		$this->_query.= "SELECT $_sql"; 
		if( !empty($this->_query) ) 
		{
			$this->_setCompiler($this->_query);
		}	
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setAndOr( $field = NULL, $values = NULL, $pref = 'LIKE ' )
{
	$_pref = array('LIKE','IN', 'NOT IN');
	
	if( !is_array($field)) 
	{
		if( in_array($pref, $_pref ) AND strtolower($pref)=='like' ) 
		{
			$this->_or_like[$field] = $pref ." '%$values%' ";
		}
		
		if( in_array($pref, $_pref ) 
			AND ( strtolower($pref)=='in' OR strtolower($pref)=='not in') ) {
			$this->_or_like[$field] = $pref ." ('".implode("','",$values )."') ";
		}
		else if( is_null($pref)==FALSE ) {
			$this->_or_like[$field] = " $pref $values";
		}
		else
		{
			$this->_or_like[$field] = " $values";
		}
	}
	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setFrom($from = NULL, $pref = FALSE )
{
	$this->_pref = $pref;
	$EUI = & get_instance();
	
	if(!is_null($from))
	{
		if( is_bool($this->_pref) AND ($this->_pref==TRUE) )
		{
			$this->_query .= " FROM ( $from ) ". $this->_conds; 
			$this->_setCompiler("FROM ( $from ) ". $this->_conds);
		}
		else
		{
			$this->_query .= " FROM ( $from ) "; 
			$this->_setCompiler(" FROM ( $from ) ");
		}
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setJoin($sql = null, $on, $join = 'LEFT', $pref = FALSE )
{
	$this->_pref = $pref;
	if(!is_null($sql)) 
	{
		if( is_bool($this->_pref) AND ($this->_pref==TRUE) ) 
		{
			$this->_query .= " $join JOIN  $sql ON $on ". $this->_conds; 
			$this->_setCompiler(" $join JOIN  $sql ON $on ". $this->_conds);
		}	
		else
		{
			$this->_query .= " $join JOIN  $sql ON $on "; 
			$this->_setCompiler(" $join JOIN  $sql ON $on ");
		}
	}
}


/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _getNo()
{
	if( ($this -> _query!='') )
		$_page_number = (($this -> _getStart())+1);
	else
		$_page_number = 0;
	
	return $_page_number;	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _setWhere( $_where='' )
{ 
	if( $_where!='' )
	{
		$this -> _query.= $_where;
		$this->_setCompiler(" $_where ");
	}	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _setAnd( $key = null , $_where=false ) 
{ 
  if( !is_null($key) ) 
  {
	if( is_bool($_where) AND ($_where==FALSE) )
	{
		$this->_query.= " AND  $key $_where ";
		$this->_setCompiler(" AND  $key $_where ");
	}
	else{
		$this->_query.= " AND  $key='$_where' ";
		$this->_setCompiler(" AND  $key='$_where' ");
	}
}
	
}
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
public function _setLike( $key = null , $_where =false ) 
{ 
  if( !is_null($key))
  {
	 if(is_bool($_where) AND ($_where==false)) 
	 {
		$this->_query.= " AND  $key LIKE '%$_where' ";
		$this->_setCompiler(" AND  $key LIKE '%$_where' ");
	 }	
	 else
	 {
		$this->_query.= " AND  $key LIKE '%$_where%' ";
		$this->_setCompiler("AND  $key LIKE '%$_where%'");
	}
  }	
  
}
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setWherein( $key = null , $_where= null  ) 
{ 
	if( is_array($_where) AND !is_null($key) ) 
	{
		$this->_query.= " AND  $key IN('" . implode("','", $_where). "') ";
		$this->_setCompiler(" AND  $key IN('" . implode("','", $_where). "') ");
		
	}	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
public function _setWhereNotin( $key = null , $_where= null  ) 
{ 
	if( is_array($_where) AND !is_null($key) ) 
	{
		$this->_query.= " AND  $key NOT IN('" . implode("','", $_where). "') ";
		$this->_setCompiler(" AND  $key NOT IN('" . implode("','", $_where). "') ");
		
	}	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function _getStart()
 {
	$_pos  = 0;
	if( !empty( $this -> _post ) && ( $this -> _post >0 ) )
	{
		$_pos  = ((( $this -> _post )-1) * ((INT)$this -> _pages));
	}
	
	return $_pos;		
 }

 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _setOrderBy( $_cols=NULL , $_type= NULL )
{ 
  if( $_cols!=NULL )
	{
		$this->_query.= " ORDER BY $_cols $_type ";
		$this->_setCompiler(" ORDER BY $_cols $_type ");
		
		if(!is_null($_type))
		{
			$this->_styles[$_cols] = array 
			( 
				'order' => strtolower($_type),
				'style'=> $this->_get_styles() 
			); 
		}
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public function _get_order_style()
 {
	if( is_null($this->_styles) ) return null;
		
	return $this->_styles;
	
 }
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _setGroupBy( $_group = null )
{
	if( $_group != null )
	{
		$this->_query.= " Group BY $_group ";
		$this->_setCompiler(" Group BY $_group ");
		
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _setHaving( $_having = null)
{
	if( !is_null($_having))
	{
		$this -> _query.= " HAVING $_having ";
		$this->_setCompiler(" HAVING $_having ");
	}
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
function _setLimit()
{
	$this -> _start = $this -> _getStart();
	if( $this -> _start > 0 )
	{
		$this->_query.= " LIMIT {$this -> _start}, {$this -> _pages} ";
		$this->_setCompiler(" LIMIT {$this -> _start}, {$this -> _pages} ");
	}	
	else
	{
		$this->_query.= " LIMIT {$this -> _start}, {$this -> _pages} ";
		$this->_setCompiler(" LIMIT {$this -> _start}, {$this -> _pages} ");
		
	}	
}

/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 function _get_total_record()
 {
	$_EUI =& get_instance();
	$_int = 0;
	if( ( $this -> _query !='') && ( $this -> _query!=null ))
	{
		$qry = $_EUI -> db -> query( $this ->_query );
		if( is_object($qry) )
		{
			$_int = $qry -> result_num_rows();	
		}
	}
	
	return $_int;
 }
 
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _get_total_page()
 {
	$_total_pages = 1; // default page render to jquery
	
	$_totals_record = self::_get_total_record();
	if( $_totals_record > 0 )
	{
		$_total_pages = ceil( ($_totals_record)/($this -> _pages ) );
	}
	
	return $_total_pages;
 }
 
/*
 * @ def 		: _get_page_number // constructor class 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _result()
 {
	$_conds = false;
	$_EUI =& get_instance();
	$_RES =$_EUI->db->query( $this -> _query );
	if( is_object($_RES) ) { 
		$_conds = $_RES;
	}	
	else {
		exit(self::Exception());
	}	
	
	return $_RES;
 }
 

 /*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
 
 public function _set_style($key, $value )
 {
	if(!is_null($key)) 
	{
		$this->_rows_styles[$key] = $value;
	}
 }
 
 /*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
 public function _set_font_color($color= null )
 {
	if(!is_null($color))  {
		$this->_set_style('font-color',$color);
	}
 }
 
/*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
 
 public function _set_background_color($color= null)
 {
	if( !is_null($color) )
	{
		$this->_set_style('background-color', $color);
	}
 }
 
/*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
  
 public function _set_font_size( $size = null  )
 {
	if( !is_null($size) )
	{
		$this->_set_style('font-size', $size);
	}	
 }
 
/*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
 
 public function _set_font_family($font = null )
 {
	if( !is_null($font) ) 
	{
		$this->_set_style('font-family', $font);
	}
 }

/*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */ 
public function _selected_header( $field = null )
{
	$_bools = NULL;
	$post_styles = $this->_get_order_style();
	if( is_string($field) AND is_array($post_styles) )
	{
		if(in_array($field, array_keys($post_styles)))
		{
			$_bools = $post_styles[$field]['order'];
		}	
	}
	
	return $_bools;
}

/*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
public function _selected_columns( $field = NULL )
{
	$_bools = '';
	
	$post_styles = $this->_get_order_style();
	if( is_string($field) AND is_array($post_styles) )
	{
		if(in_array($field, array_keys($post_styles)))
		{
			$this->_styles[$field]['style'] = $this->_get_styles();
			$_bools = $this->_get_styles();
		}	
	}
	
	//var_dump($_bools);
	
	return $_bools;
}
 
/*
 * @ def 		:  set font family type extends CSS 
 * -----------------------------------------
 *
 * @ params  	: font
 */
 
 public function _get_styles()
 {
	$_bools = FALSE;
	
// @ pack : set default  style 	

	if( !is_array($this->_rows_styles) 
		AND  count($this->_rows_styles)==0 )
	{
		$this->_set_style("font-size", "11px");
		$this->_set_style("font-color", "#8A1B08");
		$this->_set_style("background-color", "#FFFCCC");
	}
	
// @ pack : compile attribute css 
	
	$style = 'style="';
	if(is_array($this->_rows_styles) )
	  foreach( $this->_rows_styles as $key => $value )
	{
		$style .= $key . $this->_CRLF . $value . $this->_EOFL;
	} 
	
	$style .='"';
	
	return $style;
 }
 
/*
 * @ def 		:  _setLikeCache
 * -----------------------------------------
 *
 * @ params  	: field name 
 * @ params		: cache name 
 */
 
public function _setLikeCache($_field = NULL, $_cache = NULL,  $on = FALSE )
{
 $UI =& get_instance();
 
 if( !is_null($_field) AND !is_null($_cache) )
 {	
	if( $UI->URI->_get_have_post($_cache) 
		AND $UI->URI->_get_post($_cache)!='')
	{ 
		$this->_setLike($_field, $UI->URI->_get_post($_cache));
		if( ($on != FALSE ) ) 
		{
			$UI->EUI_Session->replace_session( $_cache, $UI->URI->_get_post($_cache) );
		}	
	}
	else
	{
		if( $UI->EUI_Session->_have_get_session($_cache) ) 
		{
			if( isset($_REQUEST[$_cache]) AND ( $_REQUEST[$_cache]=='' )  )
			{
				if(($on != FALSE )) 
				{
					$UI->EUI_Session->deleted_session($_cache);		
				}	
			}
			else
			{
				if(($on != FALSE )) 
				{
					$this->_setLike($_field, $UI->EUI_Session->_get_session($_cache));
				}	
			}	
		}
	}
 }
}
 
/*
 * @ def 		:  _setAndCache
 * -----------------------------------------
 *
 * @ params  	: field name 
 * @ params		: cache name 
 */
 
public function _setAndCache( $_field = NULL, $_cache = NULL, $on = FALSE  )
{
 $UI =& get_instance();
 if( !is_null($_field) AND !is_null($_cache) )
 {	
	if( $UI->URI->_get_have_post($_cache) 
		AND $UI->URI->_get_post($_cache)!='')
	{ 
		/**  handle format array(); **/
		
		$this->_setAnd($_field, $UI->URI->_get_post($_cache) );
		
		/** if on  ==true then set to session **/
		
		if(($on != FALSE )) { 
			$UI->EUI_Session->replace_session( $_cache, $UI->URI->_get_post($_cache) );
		}
	}
	else
	{
		if( $UI->EUI_Session->_have_get_session($_cache) ) 
		{
			if( isset($_REQUEST[$_cache]) AND ( $_REQUEST[$_cache]=='' )  )
			{
				if(($on != FALSE )) 
				{
					$UI->EUI_Session->deleted_session($_cache);		
				}	
			}
			else
			{
				if(($on != FALSE ))  {
					$this->_setAnd($_field, $UI->EUI_Session->_get_session($_cache));
				}	
			}	
		}
	}
 }
 
} 


 
/*
 * @ def 		:  _setAndOrCache
 * -----------------------------------------
 *
 * @ params  	: field name 
 * @ params		: cache name 
 */
 
public function _setAndOrCache( $_field = NULL, $_cache = NULL, $on = FALSE  )
{

//unset($_SESSION['eui_coll_or_cache']);
	
 $UI =& get_instance();
 if( !is_null($_field) AND !is_null($_cache) )
 {	
	if( $UI->URI->_get_have_post($_cache) 
		AND $UI->URI->_get_post($_cache)!='')
	{ 
		$this->_setAnd($_field, FALSE);
		
		if(($on != FALSE ))
		{ 
			$UI->EUI_Session->replace_session( $_cache, $UI->URI->_get_post($_cache) );
			if( is_array($this->_or_cache))
			{
				$this->_or_cache[$_cache] = $_field;
			}
			
			$UI->EUI_Session->replace_session('or_cache',$this->_or_cache);
		}
	}
	else
	{
		if( $UI->EUI_Session->_have_get_session($_cache) ) 
		{
			if( isset($_REQUEST[$_cache]) AND ( $_REQUEST[$_cache]=='' )  )
			{
				if(($on != FALSE )) {
					$UI->EUI_Session->deleted_session($_cache);			
				}	
			}
			else
			{
				if(($on != FALSE )) 
				{
					
					$_or_cache = $UI->EUI_Session->_get_session('or_cache');
					
					if(is_array($_or_cache) )
					{
						if( in_array($_cache, array_keys($_or_cache) ) )
						{
							$this->_setAnd($_or_cache[$_cache], FALSE);
						}
					}
				}	
			}	
		}
	}
 }
} 

/*
 * @ def 		:  _setWhereinCache
 * -----------------------------------------
 *
 * @ params  	: field name 
 * @ params		: cache name 
 */
 
public function _setWhereinCache( $_field = NULL, $_cache = NULL, $on = FALSE  )
{
 $UI =& get_instance();
 if( !is_null($_field) AND !is_null($_cache) )
 {	
	if( $UI->URI->_get_have_post($_cache) 
		AND $UI->URI->_get_post($_cache)!='')
	{ 
		$this->_setWherein($_field, $UI->URI->_get_post($_cache));
		if(($on != FALSE )) 
		{ 
			$UI->EUI_Session->replace_session( $_cache, $UI->URI->_get_post($_cache) );
		}	
	}
	else
	{
		if( $UI->EUI_Session->_have_get_session($_cache) ) 
		{
			if( isset($_REQUEST[$_cache]) AND ( $_REQUEST[$_cache]=='' )  )
			{
				if( ($on != FALSE )) 
				{ 
					$UI->EUI_Session->deleted_session($_cache);		
				}	
			}
			else
			{
				if(($on != FALSE )) 
				{
					$this->_setWherein($_field, $UI->EUI_Session->_get_session($_cache));
				}	
			}	
		}
	}
 }
 
}

/*
 * @ def 		:  _setWhereinCache
 * -----------------------------------------
 *
 * @ params  	: field name 
 * @ params		: cache name 
 */
 
protected function _get_select_where_and_or()
{
   $_key = NULL;
   if( count($this->_or_like)> 0 ) 
   {
		$_key = "( ";
		$num = 0;
		foreach($this->_or_like as $fld => $value ) 
		{
			if( $num ==0 )  
				$_key .= " $fld  $value ";
			else 
				$_key .= " OR $fld $value ";	
			
			$num++;
		}
	
	$_key.= " ) ";
	
   }
   
 // cek if null string 
 
   if( is_null($_key)==FALSE )
   {
	  $this->_setAnd($_key);
   }
   
   $this->_or_like = array(); // reset to default 
}
 
 /**
  ** get sql string if neded to show bugs
  ** return string
  **/
  
public function _getCompiler()
{
	$debugs = NULL;
	$this->_get_select_where_and_or();
	if( !is_null( $this->_compile)) { 
		$debugs = " <div style=\"width:75%;padding:3px;line-height:22px;border:1px solid #dddddd;font-family:consolas;color:#4a6d8e;background-color:#f3f8fc;\"> <b><u>SQL : </u></b> <br> {$this->_compile}</div>";


    }
  
	return $debugs;
		
 }
 
 /**
  ** get sql string if neded to show bugs
  ** return string
  **/
  
 function _get_query()
 {
	return $this -> _query;
 } 
 
 // handle Error 
 
 private function Exception()
 {
	return "<div style=\"font-family:Arial;text-align:left;\" >
			<table cellspacing=\"0\" cellpadding=\"0\" 
				style=\"border-right:1px solid #eeefff;border-top:1px solid #eeefff;\"> 
				<tr>
					<td style=\"background-color:#fdfdfe;color:#5258a7;font-weight:bold;border-left:1px solid #eeefff;border-bottom:1px solid #eeefff;\" nowrap> Err No.</td>
					<td style=\"background-color:#fdfdfe;color:#5258a7;font-weight:normal;border-left:1px solid #eeefff;border-bottom:1px solid #eeefff;\">" . get_class($this) . " - ". mysql_errno()."</td>
				</tr>
				<tr>
					<td style=\"background-color:#fdfdfe;color:#5258a7;font-weight:bold;border-left:1px solid #eeefff;border-bottom:1px solid #eeefff;\" nowrap> MySQL Err.</td>
					<td style=\"background-color:#fdfdfe;color:#5258a7;font-weight:normal;border-left:1px solid #eeefff;border-bottom:1px solid #eeefff;\">".mysql_error()."</td>
				</tr>
				<tr>
					<td style=\"background-color:#fdfdfe;color:#74768d;font-weight:bold;border-left:1px solid #eeefff;border-bottom:1px solid #eeefff;\" nowrap> Query .</td>
					<td style=\"background-color:#f0f1ff;color:#5258a7;font-weight:normal;border-left:1px solid #eeefff;border-bottom:1px solid #eeefff;text-align:justify;\" valign=\"top\"><pre>".self:: _get_syntax()."</pre></td>
				</tr>
			</table>	
		</div>";
 }
 
 /**
  ** get sql string if neded to show bugs
  ** return string
  **/
  
 function _get_syntax() {
	return $this -> _query;
 } 
 
  
}
?>