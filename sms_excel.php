<!DOCTYPE html>
<html>
<head>
	<title>Export SMS</title>
</head>
<body>
	<?php
	$con = mysqli_connect("192.168.0.11","enigma","enigma","enigmahsbcdb");
	$tglReport = date("d-m-Y");
	// Check connection
	if (mysqli_connect_errno()){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

	header("Content-Type: application/vnd.ms-excel");
	$name		= "ReportSMS";
	$file		= ".xls";
	$sdate		= $start_date;
	$filename 	= $name.$tglReport;
	
	header("Pragma: no-cache");
	header("Expires: 0");
	header("Content-Disposition: attachment; filename=".($filename));
	?>
	<div style="background: whitesmoke;padding: 10px;">
      <h1 style="margin-top: 0;">Export SMS</h1>
      <h1 style="margin-top: 0;"><?php echo $tglReport; ?></h1>
    </div>
    <table border="1" cellpadding="5">
    	<tr>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">Customerid</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">DEBITUR NAME</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">SEGMENT</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">RECEPIENT</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">SEND DATE</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">UPLOAD DATE</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">WO DATE</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">SMS STATUS</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">MESSAGE</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">DC</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">TL</th>
	        <th bgcolor="#3366FF"  style="color:#FFFFFF;text-align:center;">Template</th>
      </tr>
      <?php
      	$date = date("Y-m-d");
      	$sql ="select d.deb_id AS cust_id,d.deb_name,DATE_FORMAT(d.deb_wo_date,'%d-%m-%Y') AS wo_date,
				d.deb_perm_msg_coll,s.Recipient,DATE_FORMAT(s.SendDate,'%d-%m-%Y %H:%i:%s') AS senddate,ss.StatusCode,
				s.TemplateId,s.Message,a.id AS agent,b.id AS tl,DATE_FORMAT(d.deb_created_ts, '%d-%m-%Y') as tglupload,
					case
						WHEN s.TemplateId = 13 THEN 'Fresh WO (+5)'
						WHEN s.TemplateId = 15 THEN 'Fresh WO (+20)'
						WHEN s.TemplateId = 14 THEN 'Fresh WO (+30)'
						WHEN s.TemplateId = 17 THEN 'Fresh WO (+53)'
						WHEN s.TemplateId = 18 THEN 'Fresh WO (+75)'
						WHEN s.TemplateId = 23 THEN 'Fresh WO (+100)'
						WHEN s.TemplateId = 19 THEN 'Fresh WO (+150)'
						WHEN s.TemplateId = 20 THEN 'Fresh WO (+175)'						
					end AS SMStype						
				FROM t_gn_debitur d
				INNER JOIN serv_sms_outbox s ON d.deb_id=s.MasterId
				INNER JOIN t_tx_agent a ON d.deb_update_by_user=a.UserId
				INNER JOIN t_tx_agent b ON a.tl_id=b.UserId
				INNER JOIN serv_sms_status ss ON s.SmsStatus=ss.StatusId
				WHERE 1=1
				AND s.TemplateId IN (13,15,14,17,18,23,19,20)
				AND s.SendDate >= '".$date." 00:00:00'
				AND s.SendDate <= '".$date." 23:00:00'
				GROUP BY d.deb_id;";
		$result = mysqli_query($con, $sql);
		if (!$result) {
			echo "Could not successfully run query ($sql) from DB: " . mysqli_connect_error();
    		exit;
		}
		while ($row = mysqli_fetch_assoc($result)) {			
			echo "
			<tr>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['cust_id']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['deb_name']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['deb_perm_msg_coll']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['Recipient']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['senddate']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['tglupload']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['wo_date']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['StatusCode']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['Message']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['agent']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['tl']."</td>
				<td nowrap style=\"text-align: left\" class=\"content-middle\">".$row['SMStype']."</td>
			</tr>";
		}
      ?>
    </table>
</body>
</html>	