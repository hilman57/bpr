/**
	 ** check on enter keyword
	 **/
		Ext.DOM.submit = function( elem ){
			Ext.Css(elem.name).style({ borderColor : "green", fontSize :"12px",color:'red' });
		}
	
	/**
	 ** check on enter keyword
	 **/
		Ext.DOM.Install = function() {
			if( Ext.Cmp('server_database').empty()){
				Ext.Css('server_database').style({ borderColor : "red", fontSize :"13px",color:'red' });
				Ext.Cmp('server_database').setFocus();
				return false;
			}	
			else if( Ext.Cmp('user_database').empty()){
				Ext.Css('user_database').style({ borderColor : "red", fontSize :"13px", color:'red' });
				Ext.Cmp('user_database').setFocus();
				return false;
			}	
			else if( Ext.Cmp('user_login').empty()){
				Ext.Css('user_login').style({ borderColor : "red", fontSize :"13px", color:'red' });
				Ext.Cmp('user_login').setFocus();
				return false;
			}
			else if( Ext.Cmp('password_login').empty()){
				Ext.Css('password_login').style({ borderColor : "red", fontSize :"13px", color:'red' });
				Ext.Cmp('password_login').setFocus();
				return false;
			}
			else{
				var Error = Ext.Ajax({
					url		: Ext.System.view_sytem_url()+'/install/start_install.php',
					method	:'POST',
					param 	: {
						action 			 : 'INSTALL',
						server_database  : Ext.Cmp('server_database').Encrypt(),
						user_database 	 : Ext.Cmp('user_database').Encrypt(),
						passwod_database : (Ext.Cmp('passwod_database').Encrypt()==undefined?'':Ext.Cmp('passwod_database').Encrypt()),
						user_login 		 : Ext.Cmp('user_login').Encrypt(),
						name_database 	 : Ext.Cmp('name_database').Encrypt(),
						password_login   : Ext.Cmp('password_login').Encrypt()
					}
				}).json();
				
				if( Error.success==1){
					Ext.Cmp("error").setText("Success Instalation. <br>then we will import your database.<br> Please wait..!");
					Ext.Css("error").style({color:'white',fontSize:'14px'});
					setTimeout(function(){
						
						Ext.Cmp("error").setText("Process import is running. <br>Please wait..!");
						setTimeout(function(){
							var Error = Ext.Ajax({
								url		: Ext.System.view_sytem_url()+'/install/start_install.php',
								method	:'POST',
								param 	: {
									action 			 :'IMPORT',
									server_database  : Ext.Cmp('server_database').Encrypt(),
									user_database 	 : Ext.Cmp('user_database').Encrypt(),
									passwod_database : (Ext.Cmp('passwod_database').Encrypt()==undefined?'':Ext.Cmp('passwod_database').Encrypt()),
									user_login 		 : Ext.Cmp('user_login').Encrypt(),
									name_database 	 : Ext.Cmp('name_database').Encrypt(),
									password_login 	 : Ext.Cmp('password_login').Encrypt()
								}
							}).responseText();
							
							if( Error !=''){
								Ext.Cmp("error").setText(Error);
								Ext.Css("error").style({color:'white',fontSize:'14px'});
							}
						},10000)
						
					},10000);
				}
				else if( Error.success==2 ){
					Ext.Cmp("error").setText("Failed Instalation. Please look access privileges");
					Ext.Css("error").style({color:'white',fontSize:'14px'});
				}
				
			}
		}