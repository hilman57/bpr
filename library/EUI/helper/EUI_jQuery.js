/*
 * E.U.I vesion 0.0.1
 * ---------------------------------------------------------------------
 * 
 * @ packege 	helper 
 * @ def	    Controller pagging combine Jquery && EUI framework
 * @ author  	Razaki Team
 * @ link		http://www.razakitechnology.com/eui/js/eui_framework 
 */
Ext.EQuery={TotalRecord:0,TotalPage:0,ShowRecord:!0,Navigate:"",Content:"",Fields:{},Summary:"",Totals:"",Main:"dta_contact_detail.php",construct:function(a,b){this.Navigate=""!=a.custnav?a.custnav:"";this.Content=""!=a.custlist?a.custlist:"";this.Fields=""!=b?b:""},postText:function(){var a="",b;for(b in this.Fields)a=a+"&"+b+"="+this.Fields[b];return a=a.substring(1,a.length)},postContentList:function(){0!=parseInt(Ext.EQuery.TotalPage)?Ext.query(function(){Ext.query("#pager").aqPaging({current:1,
pages:Ext.EQuery.TotalPage,records:""==Ext.EQuery.TotalRecord?0:Ext.EQuery.TotalRecord,summary:""==Ext.EQuery.Summary?"":Ext.EQuery.Summary,callfunc:Ext.EQuery,rec:Ext.EQuery.ShowRecord,flip:!0,cb:function(a){Ext.query(".content_table").load((Ext.EQuery.Content?Ext.EQuery.Content:"")+"?"+(Ext.EQuery.postText()?Ext.EQuery.postText().replace(/\s+/g,"%20"):"")+"&v_page="+a)}})}):Ext.query(function(){Ext.query("#pager").aqPaging({current:1,pages:1,records:Ext.EQuery.TotalRecord,summary:""==Ext.EQuery.Summary?
"":Ext.EQuery.Summary,callfunc:Ext.EQuery,flip:!0,cb:function(a){Ext.query(".content_table").load((Ext.EQuery.Content?Ext.EQuery.Content:"")+"?"+(Ext.EQuery.postText()?Ext.EQuery.postText().replace(/\s+/g,"%20"):""))}})})},pageContent:function(a){Ext.query(function(){Ext.query("#pager").aqPaging.flip("aqPaging_1",a,this.TotalPage)})},postContent:function(){Ext.Ajax({url:this.Navigate,method:"GET",param:this.Fields}).load("main_content")},orderBy:function(a,b){var c=""!=this.Fields.type?"ASC"==this.Fields.type?
"DESC":"ASC":"ASC";if(a=void 0!==a?a:"")Ext.EQuery.Fields.order_by=a,Ext.EQuery.Fields.type=c;Ext.EQuery.postContent()},contactDetail:function(a,b){try{0<a.length&&0<b.length&&Ext.query("#main_content").load(this.Main+"?customerid="+a+"&campaignid="+b)}catch(c){alert(c)}},verifiedContent:function(a,b,c){if(""!=a&&""!=b)Ext.query("#main_content").load(this.Main+"?customerid="+a+"&campaignid="+b+"&VerifiedStatus="+c);else return alert("No Customer ID. Please try again..!"),!1},Ajax:function(a){"object"==
typeof a&&Ext.query("#main_content").load(Ext.Ajax({url:a.url,method:a.method,param:a.param})._ajaxSetup)}};