/*
 * E.U.I vesion 0.0.1
 *
 
 * @ packege 	helper 
 * @ def	    Controller pagging combine Jquery && EUI framework
 * @ author  	Razaki Team
 * @ link		http://www.razakitechnology.com/eui/js/eui_framework 
 */
 
Ext.EQuery = 
{
	TotalRecord		: 0,
	TotalPage		: 0,
	ShowRecord		: true,
	Navigate		: '',
	Content			: '',
	Fields			: {},
	Summary			: '',
	Totals			: '',
	Main			: 'dta_contact_detail.php',
	
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 
 construct:function( p, v )
	{
		this.Navigate = (p.custnav!=''?p.custnav:'')
		this.Content = (p.custlist!=''?p.custlist:'')
		this.Fields  = (v!=''?v:'')
	},
	
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 	
 postText:function()
 {
	var p ='';
	for( var i in this.Fields){
		p = p+'&'+i+'='+this.Fields[i]
	}
	
	p = p.substring(1,p.length);
	
	return p;
 },
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 	
 postContentList:function()
	{
		if( parseInt(Ext.EQuery.TotalPage) !=0 )
		{
			Ext.query(function()
			{
				Ext.query('#pager').aqPaging({
					current	: 1, 
					pages	: Ext.EQuery.TotalPage, 
					records	: (Ext.EQuery.TotalRecord=='' ? 0 : Ext.EQuery.TotalRecord),
					summary : (Ext.EQuery.Summary==''? '' : Ext.EQuery.Summary),
					callfunc: Ext.EQuery,
					rec		: Ext.EQuery.ShowRecord,
					flip	: true, 
					cb		: function(p) {
						Ext.query('.content_table').load((Ext.EQuery.Content?Ext.EQuery.Content:'')+'?'+(Ext.EQuery.postText()?Ext.EQuery.postText().replace(/\s+/g, '%20'):'')+'&v_page='+p);
					} 
				});
			});	
		}
		else
		{
			Ext.query(function() {
				Ext.query('#pager').aqPaging
				({
					current	: 1, 
					pages	: 1, 
					records : Ext.EQuery.TotalRecord,
					summary : ( Ext.EQuery.Summary=='' ? '': Ext.EQuery.Summary ),
					callfunc: Ext.EQuery,
					flip	: true, 
					cb      : function(p) 
					{
						Ext.query('.content_table').load(
							( Ext.EQuery.Content?Ext.EQuery.Content:'' )+'?'+( Ext.EQuery.postText()?Ext.EQuery.postText().replace(/\s+/g, '%20'):'' )
						);
					} 
				});
			})	
		}
	},
	
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 
 pageContent:function(p)
	{
		var p = p; 
		Ext.query(function(){
			Ext.query('#pager').aqPaging.flip('aqPaging_1',p,this.TotalPage);
		});		
	},

/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 	
 postContent:function() 
 {
	Ext.Ajax({ url:this.Navigate, method : 'GET', param: this.Fields }).load('main_content');
 },
	
	
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 	
 orderBy:function(init_columns,order)
	{
		var order_init_type = (this.Fields.type!=''?(this.Fields.type=='ASC'?'DESC':'ASC'):'ASC');
		var init_columns= (init_columns!==undefined?init_columns:'');
		if(init_columns) {
			Ext.EQuery.Fields.order_by = init_columns;
			Ext.EQuery.Fields.type = order_init_type;
		}
		
		Ext.EQuery.postContent();
	},
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 	
 contactDetail:function(customerid,campaignid) 
 {
	try 
	{
		if( customerid.length >0  && campaignid.length >0 ) 
		{
			Ext.query('#main_content').load(this.Main+'?customerid='+customerid+'&campaignid='+campaignid);
			return;
		}
	}
	catch(e){
		alert(e);
	}
 },

/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
 
 verifiedContent:function(CustomerId, CampaignId, VerifiedStatus)
 {
		if( (CustomerId!='') && (CampaignId!='') ){
			Ext.query('#main_content').load(this.Main+'?customerid='+CustomerId+'&campaignid='+CampaignId+'&VerifiedStatus='+VerifiedStatus);
			return;
		}
		else{
			alert('No Customer ID. Please try again..!');
			return false;
		}
	},
/*
 * E.U.I
 *
 * @ method  : Ajax render to JQUERY FUCK 
 * @ def  	 : default content render class
 *
 */
 	
 Ajax : function( data )
 {
	if( typeof(data) =='object' )
	{
		Ext.query('#main_content').load( Ext.Ajax ({
			url 	: data.url,
			method 	: data.method,
			param 	: data.param
		})._ajaxSetup);
	}
 }
 
/*
 * E.U.I
 *
 * @ method  : construct
 * @ def  	 : default content render class
 *
 */
}

