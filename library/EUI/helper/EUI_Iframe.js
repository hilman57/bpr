(function(UI, $ ){

/*
  Ext.Iframe(target, {
	url  : ,
	data : {
	
	}
  }).post()

*/
	UI.prototype.Iframe = function( target, fn ){
		var iframe = ( typeof(fn)=='object' ? fn : null ),target = ( target ? target :'' ), attr = {
			post : function() 
			{
				if( target!='' )
				{
					var _target = Ext.Cmp(target).getElementId();
					if( _target )
					{
						var url = iframe.url+'?';
						for( var n in fn.data ) 
						{
							url +='&'+ n +'='+ fn.data[n]; 
						}
						 _target.src = url; 
						( typeof(fn.start) =='function' ? fn.start($(_target)) : null ); 
						_target.onload = ( typeof(fn.load) =='function' ? fn.load(_target) : null);
						return true;
					}
				
				}
			},
			close : function()
			{
				var _target = Ext.Cmp(target).getElementId();
					_target.src  = null;
					_target.onload = ( typeof(fn.load) =='function' ? fn.load(_target) : null);
			}
			
		}
		
		return attr;
	}
})(E_ui, jQuery);



/**
 * look reportong detail by Action 
 * ----------------------------------------------------
  
 * @ param : this module diffrent aonly and attrubute 
 * @ param : like this ?
 *
 **/
 
(function(core, $){

 core.prototype.DetailByParents = function( config ){
	var status_code = ( ( typeof(config)!='object' && config.iD==0) ? '' : config.iD ) , grider = 
	{
		iniated_width  : $(window).width(),
		iniated_height : $(window).height(),
		interval	   : config.tgl,	
		CallCode	   : status_code,	
		
	 /* totalPolicyNumber  */
	 
		totalPolicyNumber : function() {
			Ext.Window
			({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'PolicyNumber',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 :  { 
					action 		 : 'Policy Number',
					interval 	 : this.interval,
					status_code  : ''
				}
			}).popup();
		},
		
	/* totalPolicyNumber  */
	 	
		totalUntouched : function(){
			Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'Untouced',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval 	 : this.interval,
					action 		 : 'Untouced',
					status_code  : 'IS_NULL' 	
				}
			}).popup();
		},

	/* totalPolicyNumber  */
	 	
		totalTouched : function(){
			Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'Touche',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval	: this.interval,
					action 		: 'Touche',
					status_code : 'IS_NOT_NULL' 	
				}
			}).popup();
		},

		ParentDetailByCode : function(){
			Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailByCode',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval	: this.interval,
					action 		: 'Detail By Code '+ this.CallCode ,
					status_code : this.CallCode
				}
			}).popup();
		},

		ParentDetailContacted : function(){
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayContactDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailByCode',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					action   : 'Contacted',
					interval : this.interval
				}
			}).popup();
		},
		ParentDetailPTP : function(){
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayPtpDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailNotPTP',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval     : this.interval,
					action 		 : 'Cust Promise to Pay',
					CategoryCode : 'B002',
				}
			}).popup();
		},
		
		ParentDetailNotPTP : function(){
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayNotPtpDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailNotPTP',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval	 : this.interval,
					action 		 : 'Cust Not Promise to Pay',
					CategoryCode : 'B003',
				}
			}).popup();
		},
		
		ParentDetailReturnList : function(){
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayDetailReturnList/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailReturnList',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval	 : this.interval,
					action 		 : 'Return List',
					CategoryCode : 'B005'
				}
			}).popup();
		},
		
		ParentDetailByAtempt : function(){
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayCallAtempDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailNotPTP',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval	: this.interval,
					action 		: 'Call Atempt '
				}
			}).popup();
		},
		
		ParentDetailByAtemptContact : function() {
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayDetailAtemptAndContact/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailByAtemptContact',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					interval	: this.interval,
					action 		: 'Call Attempt to Contacted'
				}
			}).popup();
		},
		
		ParentDetailOptionCompleted : function() {
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DetailOptionCompleted/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ParentDetailByAtemptContact',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : {
					interval	: this.interval,
					action 	 	: 'Call Attempt to Contacted',
					complete 	: ( typeof(config.config.iD )=='object' ? config.config.iD : '')
				}
			}).popup();
		},
		
		ParentDetailDeadTone : function() {
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DetailDeadTone/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'DetailDetailDeadTone',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 :  { 
					interval	: this.interval,
					action 		: 'Dead Tone '
				}
			}).popup();
		},
		
		ParentDetailUnconnected : function() {
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DetailUnconnected/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'Unconnected',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 :  { 
					interval	: this.interval,
					action 	 	: 'Unconnected '
				}
			}).popup();
		},
		
		ParentDetailConnected : function() {
			Ext.Window({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DetailConnected/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'Connected',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 :  { 
					interval	: this.interval,
					action 	 	: 'Connected'
				}
			}).popup();
		},
	}
	
	return grider;
 }
 
// onchild handler  
 core.prototype.DetailByChild = function( JSON ){
	console.log(JSON);
	var status_code = ( ( typeof(JSON.config)!='object' && JSON.config.iD==0) ? '' : JSON.config.iD ) , grider = 
	{
		
		iniated_width  : $(window).width(),
		iniated_height : $(window).height(),
		interval	   : JSON.tgl,	
		CallCode	   : status_code,	
		
		ChildDetailByCode : function(){
				WindowPopup = new Ext.Window 
				({
					url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
					height  	 : this.iniated_height,
					height  	 : this.iniated_width,
					name		 : 'ChildDetailByCode',  
					scrollbars 	 : 1, 
					scrolling	 : 1, 
					param 		 : { 
						action 		 : 'Detail By Code '+ this.CallCode,
						status_code  : this.CallCode,
						interval	 : this.interval,
					}
				});
				WindowPopup.popup();
				
		},
		
		ChildDetailByCodeIsComplete : function(){
			WindowPopup = new Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ChildDetailByCode',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					action 		: 'Detail By Code And Complete '+ this.CallCode ,
					complete 	: '1',
					status_code : this.CallCode,
					interval	: this.interval
				}
			});
			
			WindowPopup.popup();
		},
		
		ChildDetailByCodeNotComplete : function(){
			WindowPopup = new Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayParentDetail/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ChildDetailByCode',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					action 		: 'Detail By Code And Not Complete  '+ this.CallCode ,
					complete 	: 0,
					status_code : this.CallCode,
					interval	: this.interval
				}
			});
			
			WindowPopup.popup();
		},
		
		
		ChildDetailByAtempt : function(){
			WindowPopup = new Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayDetailByAtempt/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ChildDetailByAtempt',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					action 		: 'Call Detail By Atempt  = '+ this.CallCode,
					CallAtempt 	: this.CallCode,
					interval   	: this.interval
				}
			});
			
			WindowPopup.popup();
		
		},
		
		ChildDetailByAtemptContact : function()
		{
			WindowPopup = new Ext.Window ({
				url  		 : window.opener.Ext.DOM.INDEX +"/CallOutboundReport/DisplayDetailByAtemptAndContact/",
				height  	 : this.iniated_height,
				height  	 : this.iniated_width,
				name		 : 'ChildDetailByAtemptContact',  
				scrollbars 	 : 1, 
				scrolling	 : 1, 
				param 		 : { 
					action 		 : 'Call Attempt to Contacted with atempt ='+ this.CallCode,
					CallAtempt 	 : this.CallCode,
					interval   	 : this.interval
				}
			});
			
			WindowPopup.popup();
		
		}
		
	}
	
	return grider;
 }
 
 
})(E_ui, jQuery);


