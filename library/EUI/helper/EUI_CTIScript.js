/* prepare function Window CTI Default 
 * fisrt my test configure CTI Wit prototype
 * extend < enigma user interface >>
 */
 
var CBTNALL = 0xFFFF,
    CBTNREADY = 0x0001,
    CBTNAUX = 0x0002,
    CBTNACW = 0x0004,
    CBTNPREDICTIVE = 0x0010,
    CBTNOUTBOUND = 0x0008,
    CBTNDIAL = 0x0100,
    CBTNHOLD	= 0x0200,
    CBTNHANGUP = 0x0400,
    CBTNTRANSFER = 0x0800,
    CBTNCONFERENCE =  0x1000,
    AGENT_NULL = 0,
    AGENT_LOGIN = 1,
    AGENT_READY= 2,  
    AGENT_NOTREADY = 3,
    AGENT_ACW = 4,  
    AGENT_OUTBOUND = 5,
    AGENT_PREDICTIVE	= 6,
    AGENT_BUSY = 7,
  
  //call constant  
    CALLSTATUS_IDLE = 0,
    CALLSTATUS_ALERTING = 1,
    CALLSTATUS_CONNECTED = 2,
    CALLSTATUS_SERVICEINITIATED = 3,
    CALLSTATUS_ANSWERED = 4,
    CALLSTATUS_HELD = 5,
    CALLSTATUS_ORIGINATING = 6,
    CALLSTATUS_TRUNKSEIZED = 7,
	INBOUND_CALL = 1,
	OUTBOUND_CALL = 2,	
	
//email constant
 EMAIL_MEDIA = 1,
//global variable
 onHold = false,
 // destNo of call to follow up	
 onCall = false,
 // destNo of call to follow up	
 connectedStatus = false,
 // destNo of call to follow up	
 agentStatus = AGENT_NULL,
 // destNo of call to follow up	
 agentBtnState = 0x0000,
 // destNo of call to follow up	
 callBtnState = 0x0000,
 // destNo of call to follow up	
 callStatus = 0,
 // destNo of call to follow up	
 warned = 0,
 
// destNo of call to follow up	
 window = this,
// destNo of call to follow up
 destNo,
// destNo of call to follow up
callPasswd,
// destNo of call to follow up
 callTAC,
// destNo of call to follow up
 isMSIE = ( Ext.Browser().getName()=='Microsoft Internet Explorer'),
// destNo of call to follow up
 promptDestWin = undefined;
 
 
 /* render to object with autoload
  * is body onload to refere cti client
  * then handle .
  * return < object >
  */
 
CTI = Ext.extend( function(){
return ({
	
	call_tac 	: 0,
	user_level 	: 0, 
	
	init:function( call_tac, user_level ) {
		this.call_tac = call_tac;
		this.user_level = user_level;
	},
		
	prepareCTIClient : function() {
		try 
		{
			if( document.ctiapplet.getAgentStatus() == Ext.DOM.AGENT_NULL )
			{
				document.ctiapplet.setAgentSkill(1);
				document.ctiapplet.ctiConnect();
				document.ctiapplet.setAgentEventHandler('CTI.OnAgentEventHandler');
				document.ctiapplet.setCallEventHandler ('CTI.OnCallEventHandler');			
				document.ctiapplet.setOtherMediaEventHandler('CTI.OnOtherMediaEventHandler');
			}	
		
			if(isMSIE)
				document.ctiapplet.style.display = 'none';
	   }
	   catch(e)
	   {
			Ext.Error
			({
				log	 : e, 
				name : 'CTI.prepareCTIClient()', 
				lineNumber: e.lineNumber
			}); 
	   }
	},
			
	prepareDisconnect:function(){
		document.ctiapplet.ctiDisconnect();
	},
		
	dialOut : function(destNo){	
		callTAC = this.call_tac;
		if (destNo.indexOf(callTAC) == 0)		
				destNo = destNo.substring(callTAC.length);
				document.ctiapplet.callDial(callTAC, destNo, "1234");
	},
		
	timeStamp : function (){
		var now = new Date();		
		var stamp = now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate()+ ' '+ now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
			return stamp;
	},
		
	disableAgentButton : function(val) {
		document.getElementById("idFrmAgent").btnReady.disabled = (val & CBTNREADY);
		document.frmAgent.btnReady.disabled 	= (val & CBTNREADY);
		document.frmAgent.btnAUX.disabled 		= (val & CBTNAUX);		
	},
	
	disableCallButton:function(val){		
		callBtnState |= val;
		document.frmAgent.btnHold.disabled 		= (callBtnState & CBTNHOLD);		
		document.frmAgent.btnHangup.disabled 	= (callBtnState & CBTNHANGUP);		
	},
	
	enableCallButton:function(val){		
		callBtnState &= ~val;				
		if(!onCall)
			document.frmAgent.btnDial.disabled 	 = (callBtnState & CBTNDIAL);		
			document.frmAgent.btnHold.disabled 	 = (callBtnState & CBTNHOLD);		
			document.frmAgent.btnHangup.disabled = (callBtnState & CBTNHANGUP);		
	},
	
	disableAllButton : function(){	
	  //disableCallButton(CBTNALL);  
	},	
	
	OnLeaveInbound : function(){ 
		return ( Ext.Ajax
		({
			url 	: Ext.DOM.INDEX+'/CallGroupTeam/index/',
			method  : 'POST',
			param 	: { 
				time : Ext.Date().getDuration()
			}
		}).json() );
	},
	
	OnAgentEventHandler : function( agentstatus ) {
		switch(agentstatus)
		{
			case AGENT_LOGIN : 
				Ext.Cmp("AgentStatus").setText('" Login'); 
			break;
			
			// ready check status if inbound status
			
			case AGENT_READY : 
				//if( this.OnLeaveInbound().INBOUND == INBOUND_CALL ) // default is outbound
					Ext.Cmp("AgentStatus").setText('" Ready');
				// else {
					// document.ctiapplet.agentSetNotReady(5);
					// Ext.Cmp("AgentStatus").setText('" Ready Outbound');
				// }
			break;
			
			case AGENT_NOTREADY:	
				var selAuxreason = Ext.Cmp('auxReason').getElementId();
				if( (Ext.Cmp('auxReason').getText() !='') ){
						if( Ext.Cmp('auxReason').getValue()!=''){
							Ext.Cmp("AgentStatus").setText('Not Ready [ '+ Ext.Cmp('auxReason').getText() +' ]');
						}
						else{
							Ext.Cmp("AgentStatus").setText('Not Ready');
						}
					}	
					else
						Ext.Cmp("AgentStatus").setText('" Not Ready');
			break;
					
			case AGENT_ACW : 
				Ext.Cmp("AgentStatus").setText('" Acw'); 
			break;
			
			case AGENT_OUTBOUND : 
				Ext.Cmp("AgentStatus").setText('" Busy');
			break;
			
			case AGENT_PREDICTIVE : break;
			
			case AGENT_BUSY : 
				Ext.Cmp("AgentStatus").setText('" Busy'); 
			break;
			
			default:				
				if(warned==0) {
					Ext.Msg('Login Telephony anda ditolak karena kemungkinanan anda sudah login ditempat lain\natau sudah ada yang login di PC ini').Info();
					warned=1;
					Ext.Cmp("AgentStatus").setText('" Reject');
				}
			break;
		}
	},
	
	OnCallEventHandler : function ( callstatus ){
	//	alert(callstatus);
		switch(callstatus) 
		{
			case CALLSTATUS_IDLE:
					onHold = false;
					onCall = false;
					Ext.Cmp("idCallStatus").setText("Idle");
			break;
					
			case CALLSTATUS_ALERTING:
					onCall = true;
					// inbound call type 
					console.log('CALLSTATUS_ALERTING='+CALLSTATUS_ALERTING);
					if(document.ctiapplet.getCallDirection() == INBOUND_CALL ) 
					{	
						Ext.Cmp("idCallStatus").setText("Call Inbound from "+document.ctiapplet.getCallerId());
						
						Ext.System.view_name_url('Form Inbound');
						Ext.ActiveMenu().NotActive();
						Ext.EQuery.Ajax
						({
							url 	: Ext.DOM.INDEX +'/MoFormInbound/index/',
							method 	: 'GET',
							param 	:  { 
								CallerId 		: document.ctiapplet.getCallerId(),
								CallSessionId 	: document.ctiapplet.getCallSessionKey(),
								IvrData			: document.ctiapplet.getIVRData(),
								ControllerId 	: Ext.DOM.INDEX +'/ModDashboard/index/', 
							}
						});
					}
				//console.log(document.ctiapplet.getCallDirection());
					if(document.ctiapplet.getCallDirection() == OUTBOUND_CALL ) {
						try{ Ext.Cmp("idCallStatus").setText("Call to "+document.ctiapplet.getCallerId()); }
						catch(e){ 
							Ext.Error({ log	 : e, name : 'CTI.Call Status=> '+ CALLSTATUS_ALERTING,  lineNumber: e.lineNumber }); 
						}
					}
			break;
				
			case CALLSTATUS_ANSWERED:
				console.log('CALLSTATUS_ANSWERED '+CALLSTATUS_ANSWERED);
					onCall = true;	
					if(document.ctiapplet.getCallDirection() == INBOUND_CALL)
						Ext.Cmp("idCallStatus").setText("Call to "+ document.ctiapplet.getCallerId() );
					else if(document.ctiapplet.getCallDirection() == OUTBOUND_CALL){
						try{ Ext.Cmp("idCallStatus").setText("Call to "+ ExtApplet.getPhoneNumber() );  }
						catch(e){
							Ext.Error({ log	 : e, name : 'CTI.Call Status=> '+ CALLSTATUS_ANSWERED,  lineNumber: e.lineNumber }); 
						}
					}	
			break;
					
			case CALLSTATUS_SERVICEINITIATED:
					onCall = true;
					Ext.Cmp("idCallStatus").setText("Phone offhook");
			break;
				
			case CALLSTATUS_ORIGINATING:
			console.log('CALLSTATUS_ORIGINATING '+CALLSTATUS_ORIGINATING);
				onCall = true;
				if (document.ctiapplet.getCallDirection()== INBOUND_CALL){ }
				if (document.ctiapplet.getCallDirection() == OUTBOUND_CALL){ 	
					try{ Ext.Cmp("idCallStatus").setText("Connected to - "+ ExtApplet.getPhoneNumber() ); }
					catch(e){
						Ext.Error({ log: e,name : 'CTI.Call Status=> '+ CALLSTATUS_ORIGINATING,  lineNumber: e.lineNumber }); 
					}
				}
			break;
				
			case CALLSTATUS_CONNECTED:
				console.log('CALLSTATUS_CONNECTED '+CALLSTATUS_CONNECTED);
					onCall = true;
					onHold = false;
					
					if (document.ctiapplet.getCallDirection() == INBOUND_CALL ){
						Ext.Cmp("idCallStatus").setText("Call from "+ document.ctiapplet.getCallerId() + " connected");
					}
					
					if (document.ctiapplet.getCallDirection()== OUTBOUND_CALL){ 
						try{ Ext.Cmp("idCallStatus").setText("Connected to - "+ ExtApplet.getPhoneNumber() ); }
						catch(e){
							Ext.Error({ log	 : e, name : 'CTI.Call Status=> '+ CALLSTATUS_CONNECTED,  lineNumber: e.lineNumber }); 
						}
					}	
			break;
				
			case CALLSTATUS_HELD:
					onHold = true;
					Ext.Cmp("idCallStatus").setText("Call with "+ ExtApplet.getPhoneNumber() + " on hold");
			break;
		}
	},
		
	OnOtherMediaEventHandler:function (media, eventid)
	{
		switch(media)
		{
			case EMAIL_MEDIA:
				( parent.frames[0] != 'undefined' ? parent.frames[0].cti_notification(1, document.ctiapplet.getMediaId()) : '' );	  	
			break;
		}
	},
		
	onButtonHoldClick:function (){
			if (onHold){
				document.ctiapplet.callRetrieve();
			}else{
				document.ctiapplet.callHold();
			}
		},
		
	setLabelStatus:	function (v_status_agent){
			if( v_status_agent!='') {
				Ext.Cmp('idCallStatus').setText( Ext.Cmp('auxReason').getText() )
				if( Ext.Cmp('auxReason').empty()!=true ){
					try{ document.ctiapplet.agentSetNotReady(v_status_agent); }
					catch(e){ Ext.Error({log: e, name : 'CTI.agentSetNotReady()', lineNumber : e.lineNumber }); }	
				}
				else{
					try{ document.ctiapplet.agentSetNotReady(0); }
					catch(e){ Ext.Error({log: e, name : 'CTI.agentSetNotReady()', lineNumber : e.lineNumber });}	
				}
				return false;
			}
			else
				return;
		},
	
	setLabelReady:function(){
			Ext.Cmp('idCallStatus').setText("Idle");
			try {
				if((document.ctiapplet.getAgentStatus() != AGENT_READY)){
					document.ctiapplet.agentSetReady(); return false; 
				}
			}
			catch(e) { Ext.Error({log: e, name : 'CTI.setLabelReady()', lineNumber : e.lineNumber }); }
		},
		
	ctiSetTicketNumber:	function (n){
			document.ctiapplet.setAssignmentId(n);
		}
	});
	
},Ext)(Ext);

