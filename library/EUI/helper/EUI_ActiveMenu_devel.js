/*
 * @ def  	 	: class active && not active && add methode 
				  to E_ui prototype JS Libs 	
 * 
 * @ Package 	: E.U.I::helper JS Libs   
 * @ Author  	: omens < jombi_par@yahoo.com > 
 * @ Method		: public 
 * @ Licensed	: under the MIT ( http://razakitechnology.com/siteraztech/product/web-application ) license.
 */
 
var _Cookies = [];
var _SetActiveMenu = [];
  
;(function(Core, $) {

Core.prototype.ActiveMenu = function() { 

  
  var ActiveMenu =  
  {
	menu : _SetActiveMenu, aksess  : false,
	messages :  function(){ 
		Ext.Msg('Please save call activity!').Info(); 
	},
		
	NotActive : function(){
		for( var a in this.menu ) {
			Ext.Cmp(this.menu[a].id).setAttribute('href', "JavaScript:void(0);");
			Ext.Cmp(this.menu[a].id).setAttribute('onClick',"Ext.ActiveMenu().messages();");
		}
		this.aksess = true;
	},
	
	Active : function(){
		var menu = this.menu;
		for( var a in menu ) {
			Ext.Cmp(menu[a].id).setAttribute('href', "JavaScript:void(0);");
			Ext.Cmp(this.menu[a].id).setAttribute('onClick',"Ext.ShowMenu('"+menu[a].id+"', '"+menu[a].title+"');");
		}
		this.aksess = false;
	},
	
	Home : function(){
		if( !this.aksess){ 
			$('#main_content').load( Ext.DOM.INDEX+'/Welcome');
		}
		else{
			this.messages();
		}
	},
	
	setup: function(array_id){
		_SetActiveMenu = array_id;
		if( array_id!='' ){ 
			Ext.ActiveMenu().menu = array_id; 
		}	
		this.aksess = false;
	}
	 
   }
   
   return ( ActiveMenu ? ActiveMenu : null );
 }
 
/*
 * @ def 		:  Session
 * ***********************************************
 * @ param  	: get handle session by JAVASCRIPT 
 * @ Author  	: omens < jombi_par@yahoo.com > 
 * @ Method		: public 
 */
 
 Core.prototype.Session = function(fn){
 var Session = {
 
    // constructor 
	
	Cokie    : {}, Cokies : [],
	
	// get store by json 
	
	getStore : function(){
		if( _Cookies.length < 1 )
			this.Cokie = ( Ext.Ajax ({url: Ext.DOM.INDEX +'/SysUser/UserSession/', method: 'GET', param:{}}).json() );
			
		for( var i in this.Cokie ) 
			_Cookies[i] = this.Cokie[i]; 
	},
		
	// get spesific data 
	
	getSession:function(){
		if( fn ) {
			return (_Cookies[Ext.BASE64.encode(fn)] ? Ext.BASE64.decode(_Cookies[Ext.BASE64.encode(fn)]) : '' );
		}
		else
		return null;
	}
 }
	return ( Session ? Session : null );
	
 }
 
 /*
 * @ def 		:  detect Browser type 
 * 
 * @ param  	: get handle ActiveBars by JAVASCRIPT 
 * @ Author  	: omens < jombi_par@yahoo.com > 
 * @ Method		: public 
 */
 
Core.prototype.Browser = function( window ) {
var window = ( window ? window : Ext.DOM ), a  = 
  {
	getName :function(){
	    try { return  ( window.navigator.appName ? window.navigator.appName  : null ); }
		catch(e)
		{
			Ext.Error
			({
				log  : e,
				name : window.navigator,
				lineNumber: e.lineNumber
			});  
		}
	},
		
	getCode :function(){
		try { return  ( window.navigator.appCodeName ? window.navigator.appCodeName  : null ); }
		catch(e)
		{
			Ext.Error
			({
				log  : e, name : window.navigator, lineNumber: e.lineNumber
			});  
		}
    },
		
	getVersion:function(){	
		try { return  ( window.navigator.appVersion ? window.navigator.appVersion  : null ); }
		catch(e)
		{
			Ext.Error
			({
				log  : e, name : window.navigator, lineNumber: e.lineNumber
			});  
		}
	},
		
	getBuildID:function()
	{
		try { return  ( window.navigator.buildID ? window.navigator.buildID  : null ); }
		catch(e)
		{
			Ext.Error
			({
				log  : e, name : window.navigator, lineNumber: e.lineNumber
			});  
		}
			
	},
	
	getPlatform : function(){
		try { return  ( window.navigator.platform ? window.navigator.platform  : null ); }
		catch(e)
		{
			Ext.Error
			({
				log  : e, name : window.navigator, lineNumber: e.lineNumber
			});  
		}
	}
	
 }; return ( a ? a: Ext.Error({log : 'Not found of property', name :'Ext.Browser() '})); }
 

Core.prototype.FindingByKeyword = function(){
	Ext.Window ({
		url 		: Ext.DOM.INDEX+'/FindCustomerDetail/index/',	
		method 		: 'POST',
		width  		: ($(window).width()-($(window).width()/4)), 
		height 		: ($(window).height() - ($(window).height()/3.5)),
		left  		: (($(window).width()/2) - ( $(window).width()/2)),
		top			: (($(window).height()/2) - ($(window).height()/2) ),
		scrollbars 	: 1,
		sccrolling	: 1,
		param  		:  { 
			time : Ext.Date().getDuration()
		}
	}).popup();
}
 
 
 /*
 * @ def 		:  Active Bars 
 * 
 * @ param  	: get handle ActiveBars by JAVASCRIPT 
 * @ Author  	: omens < jombi_par@yahoo.com > 
 * @ Method		: public 
 */
 
Core.prototype.ActiveBars = function( level ){
  var Bars = 
  {
	title:[] , menu:[], icon:[], option:[], USER_LEVEL : parseInt(level), 
	Title :function(){
		this.title[Ext.DOM.USER_SYSTEM_LEVEL] = [[],[],[],[],['Set Ready'],['Chat With'],['Discount'],[],['SMS'],[], ['All Debitur']];
		this.title[Ext.DOM.USER_LEVEL_ADMIN] = [[],[],[],[],['Set Ready'],['Chat With'] ,['Discount'],[],['SMS'],[],['All Debitur']];
		this.title[Ext.DOM.USER_LEVEL_MANAGER] 	= [[],[],[],[],['Set Ready'],['Chat With'],['Discount'],[],['SMS'],[],['All Debitur']];
		this.title[Ext.DOM.USER_LEVEL_ACT_MANAGER] 	= [[],[],[],[],['Set Ready'],['Chat With'],['Discount'],[],['SMS'],[],['All Debitur']];
		this.title[Ext.DOM.USER_LEVEL_SPV] = [[],[],[],[],['Set Ready'],['Chat With'],['Discount'],[],['SMS'],[],['All Debitur']];
		this.title[Ext.DOM.USER_LEVEL_LEADER] = [[],[],[],[],['Set Ready'],['Chat With'] ,['All Debitur']];
	//-------------------------------------------------------------------------------------------------------------------------	
		this.title[Ext.DOM.USER_LEVEL_AGENT] = [[],[],[],[],[],['Set Ready'],['Chat With']];
		this.title[Ext.DOM.USER_LEVEL_INBOUND] = [[],[],[],[],[],['Set Ready'],['Chat With']];
		this.title[Ext.DOM.USER_LEVEL_QUALITY_HEAD] = [[],[],[],[],['Set Ready'],['Chat With']];
		this.title[Ext.DOM.USER_LEVEL_QUALITY_STAFF] = [[],[],[],[],['Set Ready'],['Chat With']];
		return ( this.title[this.USER_LEVEL] ? this.title[this.USER_LEVEL] : []); 
	},
		
	Menu : function(){
		this.menu[Ext.DOM.USER_SYSTEM_LEVEL] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith'],['RequestDis'],[],['SMS'],[],['AllDebitur']];
		this.menu[Ext.DOM.USER_LEVEL_ADMIN] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith'],['RequestDis'],[],['SMS'],[],['AllDebitur']];
		this.menu[Ext.DOM.USER_LEVEL_MANAGER] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith'],['RequestDis'],[],['SMS'],[],['AllDebitur']];
		this.menu[Ext.DOM.USER_LEVEL_ACT_MANAGER] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith'],['RequestDis'],[],['SMS'],[],['AllDebitur']];
		this.menu[Ext.DOM.USER_LEVEL_SPV] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith'],['RequestDis'],[],['SMS'],[],['AllDebitur']];
	//-------------------------------------------------------------------------------------------------------------------------
		this.menu[Ext.DOM.USER_LEVEL_LEADER] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith'],['AllDebitur']];
		this.menu[Ext.DOM.USER_LEVEL_AGENT] = [[],[],[],[],[],['CTI.setLabelReady'], ['ChatWith']];
		this.menu[Ext.DOM.USER_LEVEL_INBOUND] = [[],[],[],[],[],['CTI.setLabelReady'],['ChatWith']];
		this.menu[Ext.DOM.USER_LEVEL_QUALITY_HEAD] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith']];	
		this.menu[Ext.DOM.USER_LEVEL_QUALITY_STAFF] = [[],[],[],[],['CTI.setLabelReady'],['ChatWith']];	
		return ( this.menu[this.USER_LEVEL] ? this.menu[this.USER_LEVEL] : new Array() ); 
	},
		
	Icon : function(){
		this.icon[Ext.DOM.USER_SYSTEM_LEVEL] = [[],[],[],[],['group_go.png'],['group_add.png'],['page_white_acrobat.png'],[],['email_edit.png'],[],['zoom.png']];
		this.icon[Ext.DOM.USER_LEVEL_ADMIN] = [[],[],[],[],['group_go.png'],['group_add.png'],['page_white_acrobat.png'],[],['email_edit.png'],[],['zoom.png']];
		this.icon[Ext.DOM.USER_LEVEL_MANAGER] = [[],[],[],[],['group_go.png'],['group_add.png'],['page_white_acrobat.png'],[],['email_edit.png'],[],['zoom.png']];
		this.icon[Ext.DOM.USER_LEVEL_ACT_MANAGER] = [[],[],[],[],['group_go.png'],['group_add.png'],['page_white_acrobat.png'],[],['email_edit.png'],[],['zoom.png']];
		this.icon[Ext.DOM.USER_LEVEL_SPV] = [[],[],[],[],['group_go.png'],['group_add.png'],['page_white_acrobat.png'],[],['email_edit.png'],[],['zoom.png']];
	//-------------------------------------------------------------------------------------------------------------------------
		this.icon[Ext.DOM.USER_LEVEL_LEADER] = [[],[],[],[],['group_go.png'],['group_add.png'],['zoom.png']];
		this.icon[Ext.DOM.USER_LEVEL_AGENT] = [[],[],[],[],[],['group_go.png'],['group_go.png']];
		this.icon[Ext.DOM.USER_LEVEL_INBOUND] = [[],[],[],[],[],['group_go.png'],['group_go.png']];
		this.icon[Ext.DOM.USER_LEVEL_QUALITY_HEAD] = [[],[],[],[],['group_go.png'],['group_add.png']];
		this.icon[Ext.DOM.USER_LEVEL_QUALITY_STAFF] = [[],[],[],[],['group_go.png'],['group_add.png']];
		return ( this.icon[this.USER_LEVEL] ? this.icon[this.USER_LEVEL] : []); 
	},
		
	Option : function(){
		this.option[Ext.DOM.USER_SYSTEM_LEVEL] = [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200},
	//-------------------------------------------------------------------------------------------------------------------------
			{render:7,type:'label', id:'request_discount', name:'request_discount', label:'-', width:200},
			{render:9,type:'label', id:'request_sms', name:'request_sms', label:'-', width:200}
			
		];
		
		this.option[Ext.DOM.USER_LEVEL_ADMIN] = [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200},
	//-------------------------------------------------------------------------------------------------------------------------
			{render:7,type:'label', id:'request_discount', name:'request_discount', label:'-', width:200},
			{render:9,type:'label', id:'request_sms', name:'request_sms', label:'-', width:200}
		];
							   
		this.option[Ext.DOM.USER_LEVEL_MANAGER]= [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200},
	//-------------------------------------------------------------------------------------------------------------------------
			{render:7,type:'label', id:'request_discount', name:'request_discount', label:'-', width:200},
			{render:9,type:'label', id:'request_sms', name:'request_sms', label:'-', width:200}
		];
		
		this.option[Ext.DOM.USER_LEVEL_ACT_MANAGER]= [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200},
	//-------------------------------------------------------------------------------------------------------------------------
			{render:7,type:'label', id:'request_discount', name:'request_discount', label:'-', width:200},
			{render:9,type:'label', id:'request_sms', name:'request_sms', label:'-', width:200}
		];
							   
		this.option[Ext.DOM.USER_LEVEL_SPV]= [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200},
	//-------------------------------------------------------------------------------------------------------------------------
			{render:7,type:'label', id:'request_discount', name:'request_discount', label:'-', width:200},
			{render:9,type:'label', id:'request_sms', name:'request_sms', label:'-', width:200}
		];	
		
		this.option[Ext.DOM.USER_LEVEL_LEADER]= [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200}
		];	
							   
		this.option[Ext.DOM.USER_LEVEL_AGENT]= [
			{render:4, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'-"', width:200},
			{render:3,type:'label', id:'time_counter', name:'time_counter', label:'-"', width:200},
			{render:2,type:'label', id:'lebel_counter', name:'lebel_counter', label:'-"', width:200}
		];	
		
		this.option[Ext.DOM.USER_LEVEL_INBOUND]= [
			{render:4, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'-"', width:200},
			{render:3,type:'label', id:'time_counter', name:'time_counter', label:'-"', width:200},
			{render:2,type:'label', id:'lebel_counter', name:'lebel_counter', label:'-"', width:200}
		];
							   
		this.option[Ext.DOM.USER_LEVEL_QUALITY_HEAD]= [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200}
		];

		this.option[Ext.DOM.USER_LEVEL_QUALITY_STAFF]= [
			{render:3, type:'combo', triger:'CTI.setLabelStatus', store:[V_STATUS_STORE], header:'', name:'auxReason', id:'auxReason', width:120, value:''},
			{render:0, type:'label', id:'AgentStatus', name:'AgentStatus', label:'Caller Status', width:50 },
			{render:1, type:'label', id:'idCallStatus', name:'idCallStatus', label:'', width:200 },
			{render:2,type:'label', id:'time_counter', name:'time_counter', label:'-', width:200}
		];				   
			
		
		return ( this.option[this.USER_LEVEL] ? this.option[this.USER_LEVEL] : []); 
	}
  }
  return ( Bars ? Bars : {} );
  
} 
})(E_ui, jQuery);

// END OF FILE 


//  ========================= ADD ATTRIBUTE FUNCTION ========================

if( typeof( window.AllDebitur ) != 'function' ){
	function AllDebitur()
	{
		var UserId = Ext.Session('UserId').getSession().toUpperCase();
		var UserData = Ext.Session('Username').getSession().toUpperCase();
		var UserLevel = Ext.Session('HandlingType').getSession().toUpperCase();
		
		Ext.Window
		({
			url    		: Ext.EventUrl( new Array("SysAllDebitur", "index", UserData, UserId, UserLevel ) ).Apply(),
			top    		: 0,
			left   		: 0,
			width  		: ($(window).width()-10),
			height 		: ($(window).height()-50),
			resizeable	: 1,
			scrollbars  : 1,
			param  : {
				time : Ext.Date().getDuration()
			}	
		}).popup();
	}
}
 