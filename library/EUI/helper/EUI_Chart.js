/**! 
	Thank to god 
	This class modified hightchart
	you can add function or properties
	for fleksibelity your realtime chart
	create@Omens<rahmattullah>
**/

var Chart;
var JP_Chart = function()
{
	this.fileJson = "../class/class.monlive.data.php";	
	this.source =[] 
	this.CategoryId = 0; 
}


/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.getCategoryId=function(){
	return this.CategoryId;
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.setCategoryId=function(CategoryId){
	this.CategoryId = CategoryId;
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.setSource=function(){
	this.source = this.JsonSource();
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.getSource=function(){
	var datas = []
		datas =  this.JsonSource();
	return datas;
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/
	
JP_Chart.prototype.JsonSource = function(){
var CategoryId = this.getCategoryId();
	
	doJava.File = this.fileJson;
	doJava.Params = {
		action:'get_chart_data',
		CategoryId : CategoryId
	}
	var chars = doJava.eJson();
	return chars;
}	

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.Axis = {'min':0,'max':0};

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/
JP_Chart.prototype.arrayInt= function(source){
	var cars = []
	for(var i in source )
	{
		cars[i] = parseInt(source[i])
	}
	
	return cars;
}
/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/
JP_Chart.prototype.arrayStr= function(source){
	var cars = []
	for(var i in source )
	{
		cars[i] = source[i]
	}
	
	return cars;
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.startWait = function(image,label){
	doJava.dom(label).innerHTML = "<span><img src=\"../gambar/"+image+"\" height=\"12\"></span>";
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.stopWait = function(label){
	doJava.dom(label).innerHTML = "<span style=\"color:#dddddd;\">-</span>";
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/
JP_Chart.prototype.getInterval= function(){
	var cars = new Date();
	return cars.getDate()+'/'+(cars.getMonth()+1)+'/'+cars.getFullYear();
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.Data = function(num) {
    var num = parseInt(num);
	return(__num = {
		Currency:function(){
			return parseFloat(num).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
		}
	});
}

/* 
 * class chrat js for relaod js Data 
 * settup begin created 
 * author<omens> 
 * source : hightcharts
*/

JP_Chart.prototype.Create = function(option){
	var __Chart = new JP_Chart();
	__Chart.setCategoryId(option);
	__Chart.startWait('loading.gif','loading_jpg');
	new(function(){
		source = __Chart.JsonSource();
		 var HorizontalSource = __Chart.arrayInt(source.source_size);
		 var VerticalSource = __Chart.arrayStr(source.source_agent);
		 var TotalsPremi = source.source_premi;
		 var colors = Highcharts.getOptions().colors
			 chart = new Highcharts.Chart({
					chart: {
						renderTo: 'monitoring_dashboard',
						defaultSeriesType: 'column'
					},
					title: {
						text: '<p style="color:#BBBCCC;font-family:Arial;font-size:12px;font-weight:bold;">Live Size Closing , Date : '+__Chart.getInterval()+'</p>',
						enable:false
					},
					
					xAxis: {
						categories: VerticalSource
						
					},
					yAxis: {min:0,max:60, title: {text: 'Summary '}},
					tooltip: {
						 enabled: true,
						formatter: function(){
							return ' <p style="font-size:11px;">'+this.x +': '+ this.y+', <br> Total Premi : '+__Chart.Data(TotalsPremi[this.x]).Currency()+'</p>';
						}
					},
					plotOptions: {
						// add event chat 
						series: {
								cursor: 'pointer',
								point: {
									events:{
										click:function(){
											var Users = this.category;
												window.chatWith(Users);
												$("#chatbox_"+Users+" .chatboxtextarea").val("Jumlah Closing : "+this.y);
										}
									}
								}
						},
						// stop: event chat 
						column: {pointPadding: 0.1, borderWidth:1,borderColor:"#dddddd",
							dataLabels: { 
									enabled: true 
								}
						}
						
					},
				    series: [{showInLegend:true,name: '( '+source.source_summary+' ) Summary Closing',data:HorizontalSource}]
			})
	})();
	// stop loading
	__Chart.stopWait('loading_jpg');
}
