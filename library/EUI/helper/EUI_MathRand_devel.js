/* @@
 * 
 *
 */
 
(function( core ){
	core.prototype.MathRand = function( fn ){
		var form = fn.form;
		var _rand  =  {
		setRandForm : function( object ){
			var curr_index = 0, leng_index = [], next_index = 0, prev_next = 0;
			
			if( object.value !='' ) {
				curr_index = this.position(object);
				leng_index = this.posLength();
				next_index = curr_index+1;
				prev_next  = next_index+1;
				
				for( var y = 0; y<=next_index; y++ ){
					Ext.Cmp(leng_index[y]).disabled(false);
				 }
				 
				for( var i = prev_next; i<leng_index.length; i++ ){
					Ext.Cmp(leng_index[i]).setValue('');
					Ext.Cmp(leng_index[i]).disabled(false);
				}
			}
			else
			{
				curr_index = this.position(object);
				leng_index = this.posLength();
				next_index = curr_index+1;
				prev_next  = next_index
				
				for( var y = 0; y<=next_index; y++ ){
					Ext.Cmp(leng_index[y]).disabled(false);
				 }
				 
				for( var i = prev_next; i<leng_index.length; i++ ){
					Ext.Cmp(leng_index[i]).disabled(false);
				}
			}
			
		},
		
		init : function(){
			Ext.Serialize(form).procedure(function(item){
				var empty_tots = 0; var no_empty_tots = 0;
				 
				 /* hitung kosong dan gak kosong **/
				 
				 for( var elname in item ){
					if( elname!='ProductForm' && elname!='ScoringForm' && elname!='ScoringAttempt') {
						if( Ext.Cmp(elname).empty() ) empty_tots+=1;
						else
							no_empty_tots+=1;
					}
				 }	
						
				/* compare  process data **/	
					
					if( no_empty_tots ==0 ){
						var num = 1;
						for( var elname in item ){
							if( elname!='ProductForm' && elname!='ScoringForm' )  {
								if( Ext.Cmp(elname).empty() && num!=1) Ext.Cmp(elname).disabled(false);
								else
									Ext.Cmp(elname).disabled(false);
								num++;	
							
							}
						}	
					}
					else
					{
						for( var elname in item ){
							if( elname!='ProductForm' && elname!='ScoringForm' )  { 
								if( Ext.Cmp(elname).empty()) Ext.Cmp(elname).disabled(false);
								else
									Ext.Cmp(elname).disabled(false);
							}		
						}	
					}
				});	
			},
			
			position : function( object ){
				var ret = 0; 
				Ext.Serialize(form).procedure(function(item){
					var pos = 0; 
					for( var elname in item ){
						if( object.id == elname ){
							ret = pos;
						}
						pos++;
					}
				});
				
				return ret;
			},
			
			init_argc :function( bool )
			{
				Ext.Serialize(form).procedure(function(item){
					 for( var elname in item ){
						if( elname!='ProductForm' && elname!='ScoringForm' ) { 
							Ext.Cmp(elname).disabled(bool); 
						}	
					}	
				})
			},
			
			posLength : function(){
				var d = [];
				var x = Ext.Serialize(form).getElement();
				var n = 0;
				for( var i in x ){
					if( i!='ProductForm' && i!='ScoringForm' ){
						d[n] = i;
						n++;
					}
				}
				return d;
			}
		}	
		
		return _rand; 
	}
})(E_ui);

