/*
 * @ pack : ref applikasi hsbc yang lama 
  ------------------------------------------------------------
 * @ auth : E'ndang ..
 *   
 */

var windowIntervalId = null, 
    Example2, 
	mycookie_time, 
	$mycookie_uid, 
	default_ceck_lock = 30000, 
	default_time = 180;

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */
 
 
var SessionHandle = SessionHandle = Ext.Session('HandlingType').getSession();

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */

function SetLockMonitoring(){
 
var User = [
	Ext.DOM.USER_LEVEL_ADMIN, 
	Ext.DOM.USER_LEVEL_MANAGER,
	Ext.DOM.USER_LEVEL_SPV,
	Ext.DOM.USER_LEVEL_QUALITY,
	Ext.DOM.USER_SYSTEM_LEVEL,
	Ext.DOM.USER_LEVEL_LEADER
 ];
// return User ================================> 

if( SessionHandle=='' || SessionHandle==0 ){ 
	SessionHandle = Ext.Session('HandlingType').getSession();
}
// return User ================================> 

if ( Ext.Array(User).in_array(SessionHandle) ) { 
	return false 
}

// return User ================================> 

  Ext.Ajax 
	({ 
		url    : Ext.DOM.INDEX+"/SetLockMonitoring/SelectTime/",
		method : 'POST',
		param  : { timer : Ext.Date().getDuration() },
		ERROR  : function(e){
		 Ext.Util(e).proc(function(response){
			if( response.isLocked ) {
			
			  // start :: set lock of data #=========================================>  
			   
			   Ext.Msg('Sorry you are locked, contact spv for unlocking').Info();
			    Ext.Ajax
			   ({
					url    : Ext.DOM.INDEX+"/SetLockMonitoring/UserUlock/",
					method : 'POST',
					param  : { timer : Ext.Date().getDuration() },
					ERROR  : function(e){
						Ext.Util(e).proc(function(response){
							if(response.success){ 
							  window.document.location = Ext.DOM.INDEX+'/Auth/logout/?login=(false)';
							}
						});
					}
				}).post();
				
			// end :: set lock of data #=========================================>  
				
			} else {
				return false;
			}
		 });
	   }
	}).post();
 }
 

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
 
function getCookie() {
var temp  = parseInt($.cookie('the_cookie_time'));
	if(isNaN(temp)) {
		mycookie_time = default_time;
        //storeTime(default_time);
    } else if(temp == 0) {
		mycookie_time = default_time;
        //storeTime(default_time);
    } else {
		mycookie_time = temp;
    }
    
	$mycookie_uid = $.cookie('the_cookie_uid')
    return false;
}

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
 
function storeTime(tm){
 $.cookie('the_cookie_time', tm, { expires: 1, path: '/' });
 return false;
}

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
 
function deleteTime() {
  $.removeCookie('the_cookie_time', { path: '/' });
  return false;
}
 
/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
 
function storeCookie(){
 $.cookie('the_cookie_uid', Ext.Session('UserId').getSession(), { expires: 1, path: '/' });
 $.cookie('the_cookie_time', 100, { expires: 1, path: '/' });
 return false;	
}
		
/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
		
function deleteCookie() {
	$.removeCookie('the_cookie_uid', { path: '/' });
	$.removeCookie('the_cookie_time', { path: '/' });
	return false;
}
/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
 
function logout() {
	deleteTime();
    window.location = "../index.php?action=logout";
}

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
function msToTime(s)  {

  function addZ(n) { return (n<10? '0':'') + n; }
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;

  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs)
} 
 
/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */ 
 
function formatTime( seconds )
{
	var s = "";
	var days = Math.floor( ( seconds / 3600 ) / 24 );
	if ( days >= 1 ) {
		s += days.toString() + " Minute" + ( ( days == 1 ) ? "" : "s" ) + " + ";
		seconds -= days * 24 * 3600;
	}

	var hours = Math.floor( seconds / 3600 );
		s += GetPaddedIntString( hours.toString(), 2 ) + ":";
		seconds -= hours * 3600;

		var minutes = Math.floor( seconds / 60 );
		s += GetPaddedIntString( minutes.toString(), 2 ) + ":";
		seconds -= minutes * 60;

		s += GetPaddedIntString( Math.floor( seconds ).toString(), 2 );

		return s;
}

/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
 
 
function GetPaddedIntString( n, numDigits ) {

	var nPadded = n;
	for ( ; nPadded.length < numDigits ; ) {
		nPadded = "0" + nPadded;
	}
	return nPadded;
}



/*
 * pack : lines ---------------------------------------------------------
 */   

function pause_timer() {
	Example2.Timer.stop();
	$('#time_counter').html(formatTime(0));
	return false;
}



/*
 * @ pack : set locked data if OK 
 * ---------------------------------------------------------
 */  
var Example2 = new (function() {

    var $countdown;
    var $form;
    var incrementTime = 60;
    var currentTime = 180000; // 5 minutes (in milliseconds)
    
    $(function() {

        // Setup the timer
        //$countdown = $('#time_counter');
        //Example2.Timer = $.timer(updateTimer, incrementTime, true);

        // Setup form
        // $form = $('#example2form');
        // $form.bind('submit', function() {
            // Example2.resetCountdown();
            // return false;
        // });

    });

    function updateTimer() {

        // Output timer position
		console.log(currentTime);
        var timeString = formatTime(currentTime);
        //$countdown.html(timeString);
		
		$('#time_counter').html(timeString);

        // If timer is complete, trigger alert
        if (currentTime == 0) {
            Example2.Timer.stop();
            Example2.resetCountdown();
            return;
        }

        // Increment timer position
        currentTime -= incrementTime;
        if (currentTime < 0) currentTime = 0;

    }

    this.resetCountdown = function() {

        // Get time from form
        var newTime = default_time;
        if (newTime > 0) {currentTime = newTime;}

        // Stop and reset timer
        Example2.Timer.stop().once();

    };

}); 
 