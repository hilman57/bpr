/**
 * jquery.purr.js
 * Copyright (c) 2008 Net Perspective (net-perspective.com)
 * Licensed under the MIT License (http://www.opensource.org/licenses/mit-license.php)
 * 
 * @author R.A. Ray
 * @projectDescription	jQuery plugin for dynamically displaying unobtrusive messages in the browser. Mimics the behavior of the MacOS program "Growl."
 * @version 0.1.0
 * 
 * @requires jquery.js (tested with 1.2.6)
 * 
 * @param fadeInSpeed 					int - Duration of fade in animation in miliseconds
 * 													default: 500
 *	@param fadeOutSpeed  				int - Duration of fade out animationin miliseconds
 														default: 500
 *	@param removeTimer  				int - Timeout, in miliseconds, before notice is removed once it is the top non-sticky notice in the list
 														default: 4000
 *	@param isSticky 						bool - Whether the notice should fade out on its own or wait to be manually closed
 														default: false
 *	@param usingTransparentPNG 	bool - Whether or not the notice is using transparent .png images in its styling
 														default: false
 */

( function( $ ) {
	$.purr = function ( notice, options )
	{ 
		// Convert notice to a jQuery object
		notice = $( notice );
		
		// Add a class to denote the notice as not sticky
		if ( !options.isSticky )
		{
			notice.addClass( 'not-sticky' );
		};
		
		// Get the container element from the page
		var cont = document.getElementById( 'purr-container' );
		
		// If the container doesn't yet exist, we need to create it
		if ( !cont )
		{
			cont = '<div id="purr-container"></div>';
		}
		
		// Convert cont to a jQuery object
		cont = $( cont );
		
		// Add the container to the page
		
		if( Ext.Cmp('notification').IsNull() ==true ){
			$(" <div />" ).attr("id","notification").appendTo($( "body" ));
		}
		
		$( '#notification' ).append( cont );
		
		notify();

		function notify ()
		{	
			// Set up the close button
			var close = document.createElement( 'a' );
			$( close ).attr(	
				{
					'class': 'close',
					'href': '#close',
					innerHTML: 'Close'
				}
			)
				.appendTo( notice )
					.click( function ()
						{
							console.log(this);
							removeNotice();
							
							return false;
						}
					);
			
			// Add the notice to the page and keep it hidden initially
			notice.appendTo( cont )
				.hide();
				
			if ( jQuery.browser.msie && options.usingTransparentPNG )
			{
				// IE7 and earlier can't handle the combination of opacity and transparent pngs, so if we're using transparent pngs in our
				// notice style, we'll just skip the fading in.
				notice.show();
			}
			else
			{
				//Fade in the notice we just added
				notice.fadeIn( options.fadeInSpeed );
			}
			
			// Set up the removal interval for the added notice if that notice is not a sticky
			if ( !options.isSticky )
			{
				var topSpotInt = setInterval( function ()
				{
					// Check to see if our notice is the first non-sticky notice in the list
					if ( notice.prevAll( '.not-sticky' ).length == 0 )
					{ 
						// Stop checking once the condition is met
						clearInterval( topSpotInt );
						
						// Call the close action after the timeout set in options
						setTimeout( function ()
							{
								removeNotice();
							}, options.removeTimer
						);
					}
				}, 200 );	
			}
			
			if( options.removeMe){
				removeNotice();
			}
		}

		function removeNotice ()
		{
			// IE7 and earlier can't handle the combination of opacity and transparent pngs, so if we're using transparent pngs in our
			// notice style, we'll just skip the fading out.
			if ( jQuery.browser.msie && options.usingTransparentPNG )
			{
				notice.css( { opacity: 0	} )
					.animate( 
						{ 
							height: '0px' 
						}, 
						{ 
							duration: options.fadeOutSpeed, 
							complete:  function ()
								{
									notice.remove();
								} 
							} 
					);
			}
			else
			{
				// Fade the object out before reducing its height to produce the sliding effect
				notice.animate( 
					{ 
						opacity: '0'
					}, 
					{ 
						duration: options.fadeOutSpeed, 
						complete: function () 
							{
								notice.animate(
									{
										height: '0px'
									},
									{
										duration: options.fadeOutSpeed,
										complete: function ()
											{
												notice.remove();
											}
									}
								);
							}
					} 
				);
			}
		};
	};
	
	$.fn.purr = function ( options )
	{
		options = options || {};
		options.fadeInSpeed = options.fadeInSpeed || 500;
		options.fadeOutSpeed = options.fadeOutSpeed || 500;
		options.removeTimer = options.removeTimer || 4000;
		options.isSticky = options.isSticky || false;
		//options.removeMe = options.removeMe || false;
		options.usingTransparentPNG = options.usingTransparentPNG || false;
		
		this.each( function() 
			{
				new $.purr( this, options );
			}
		);
		
		return this;
	};
})( jQuery );

/* --- create reminder function checking **/
/* --- create reminder function checking **/
/* --- create reminder function checking **/

;(function(core, $){
  core.prototype.getPurr = function(container, properties ){
	var _purr = 
		{
			compile : function(){
				var rows = Ext.Ajax({url:properties.read.url, param :{ time : Ext.Date().getDuration() }}).json();
				if( rows.counter > 0 ) {
					if( typeof( properties.EVENT )=='function' ){
						this.html( rows, properties );
					}
				}
			},
			
			html : function( rows , properties ){ 
				$(document).ready( function() {
					var link = '<a href="javascript:void(0);" id="purr_'+rows.PrimaryID+'_'+rows.CustomerId+'">'+ rows.CustomerName +'</a>',
						notice = '<div class="notice croper_'+rows.PrimaryID+'" id="notice_'+rows.PrimaryID+'_'+rows.CustomerId+'">'
							+ '<div class="notice-body">' 
								+ '<img src="'+ Ext.DOM.LIBRARY +'/gambar/info.png" alt="" />'
								+ '<p><span>'+properties.title+'</span></p>'
								+ '<h3>'+ link +'</h3>'
								+ '<p><span> Try Call : '+rows.TryCallAgain+'</span></p>'
							+ '</div>'
							+ '<div class="notice-bottom"> </div>'
							+ '</div>';
					$( notice ).purr({ 
						usingTransparentPNG : true, 
						isSticky: true 
					}); 
					Ext.Cmp('notice_'+ rows.PrimaryID+'_'+rows.CustomerId).getElementId().addEventListener("click", properties.EVENT, false);
					$.get( properties.close.url,{ PrimaryID : rows.PrimaryID });
					return false;
				});
			}
		}
		
	 return ( typeof( _purr)=='object' ? _purr : null ); 	
  }
  
})(E_ui, $);

//===================================================
// Call Reminder 
//===================================================

window.setInterval(function()
{
	Ext.getPurr('container', 
	{
		title : '" Appoinment Call With " :',
		read  : { url : Ext.DOM.INDEX+"/CallReminder/Appoinment/" },
		close : { url : Ext.DOM.INDEX+"/CallReminder/UpdateAppoinment/" },
		
		EVENT : function(e){
			Ext.Util(e).proc(function( notice ) 
			{	
				var notes = notice.id.split('_');
					Ext.ActiveMenu().NotActive();
					$(".croper_"+notes[1]).remove();
				Ext.EQuery.Ajax
				({
					url 	: Ext.DOM.INDEX +'/SrcCustomerList/ContactDetail/',
					method  : 'GET',
					param 	: {
					  CustomerId 	: notes[2],
					  ControllerId  : Ext.DOM.INDEX +'/SrcCustomerList/index/'
					}
				});
			});
		}
	}).compile();
	
}, 8000);


//===================================================
// sms request  
//===================================================

window.setInterval(function(){
	var SMS = ( Ext.Ajax({
		url : Ext.DOM.INDEX+"/CallReminder/getSmsCounter/",
		param :{
			time : Ext.Date().getDuration()
		}
	}).json() );
	
	if(Ext.Cmp("request_sms").IsNull() == false ){
	    if( parseInt(SMS.sms_counter) > 0 ){
			Ext.Cmp("request_sms").setText("You have New Request ( "+ SMS.sms_counter +" )");
		} else{
			Ext.Cmp("request_sms").setText('( 0 )');
		}
	}
	
}, 5000);



//===================================================
// discount request  
//===================================================

window.setInterval(function(){
	var Discount = ( Ext.Ajax({
		url : Ext.DOM.INDEX+"/CallReminder/getDiscountCounter/",
		param :{
			time : Ext.Date().getDuration()
		}
	}).json() );
	
	if(Ext.Cmp("request_discount").IsNull() == false ){
	    if( parseInt(Discount.discount_counter) > 0 ){
			Ext.Cmp("request_discount").setText("New Discount ( "+ Discount.discount_counter +" )");
		} else{
			Ext.Cmp("request_discount").setText('( 0 )');
		}
	}
	
}, 6000);

