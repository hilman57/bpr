// sample on EUI JS LIBS 
// Overide class 
// sample on EUI JS LIBS 
// author : omens 

var NewExt = new function( Core ){
	var p = new Core; 
	Core.prototype.test = function(){
		return ({ 
			b : function(){
				p.Msg("ss").Error();
			}
		});
	};
	
	Core.prototype.OK = function( a ){
		return ({ 
			pesan : function(){
				p.Msg(a).Error();
			}
		});
	};
	return new Core;
}(E_ui);

// create new function on EUI 
// sample on EUI JS LIBS 

;(function(Core){
	Core.prototype.Mahluk = function(){
		var x = { 
			kaki : 2,
			nama : 'Manusia', 
			info: function(){
				alert(this.nama +" Punya Kaki "+ this.kaki);
			}
		}
		return x;	
	}
	
	Core.prototype.column = {
		cols1 :'1',
		cols2 :'2',
		cols3 :'3',
	}
	
})(E_ui);

Ext.Mahluk().nama = 'Kambing';
Ext.Mahluk().kaki = 4;
Ext.Mahluk().info();


// sample create 
// new class extends class E_UI;

var EUI = new function( parent )
{
	this.cls = function() { return new parent(); }; 
	this.__proto__ = this.parent(); // constructor;
	
 // test func1
 
	this.func1 = function(){ // you can create object or other 
		this.Msg("t1 IM From Extends EUI Cores ").Error();
		
		this.Ajax
		({
			url		: 'test.php',
			method 	: 'POST',
			param 	: {
				act : 'test'
			},
			ERROR : function(e){
				this.cls.Msg(e.target.responseText).Error();
			}
		}).post();
	}
	
 // test func1
	
	this.func2 = function(){
		this.Msg("t2 IM From Extends EUI Cores ").Error();
	}
	return (this);
	
}(E_ui);
	
EUI.func1();
EUI.Error({log:'test', name:'test'});




