/**  
	@Brodacast Message Plugin extends Jquery 
	@author: omens 
	@version : trial;	
**/ 


(function($){
	$.showMessage=function(b,a){
		settings = $.extend ({
			id:"sliding_message_box",
			position:"bottom",
			size:"90",
			backgroundColor:"rgb(143, 177, 240)",
			delay:1500,
			speed:500,
			fontSize:"14px",
			htmlBody:{
					imgUrl :Ext.DOM.INDEX +'/gambar/icon',
					
					title :{
						icon :'information.png',
						text :'Message Box ',
						id 	 :'message-title'
					},
					
					content	:{
						text	 : '',
						cssBody	 : 'box-shadow',
						id		 : 'message-body',
						hiddenid : ''
					},
					
					close:{
						icon : 'cancel.png',
						css  : 'test'
					}
				}
		},a);
		
	
		
		var htmlBody = " <a href='javascript:void(0);' id='clear-text' style='float:right;margin-top:10px;margin-right:10px;' title='close'> <span id='"+(settings.htmlBody.close.id?settings.htmlBody.close.id:'')+"'>"+
						" <img src='"+(settings.htmlBody.imgUrl?settings.htmlBody.imgUrl:'')+"/"+(settings.htmlBody.close.icon?settings.htmlBody.close.icon:'')+"'></span></a>"+
							
						"<div style=\"border:0px solid #000;\" class='"+(settings.htmlBody.content.cssBody?settings.htmlBody.content.cssBody:'')+"' id='"+(settings.htmlBody.content.id?settings.htmlBody.content.id:'body-text')+"'>"+
							" <div id='"+(settings.htmlBody.title.id?settings.htmlBody.title.id:'')+"'>"+
								" <span style='border:0px solid #000;padding-left:2px;padding-top:10px;'>"+
								" <img src='"+(settings.htmlBody.imgUrl?settings.htmlBody.imgUrl:'')+"/"+(settings.htmlBody.title.icon?settings.htmlBody.title.icon:'')+"'></span>"+
								" <span id='text-title'>"+settings.htmlBody.title.text+"</span></div>"+
							" <div id='"+settings.htmlBody.content.html+"' style='width:310px;height:300px;overflow:auto;'>"+(settings.htmlBody.content.text?settings.htmlBody.content.text:'')+"</div>"+
						"</div>";
					
		b = htmlBody;
		
		a=$("#"+settings.id);
		
		if(a.length==0)
			{
				a=$("<div></div>").attr("id",settings.id);
				a.css({	
						"z-index":"999",
						"background-color":settings.backgroundColor,
						"text-align":"center",
						"position":"absolute",
						"left" :0,
						 right:"0",
						 width:"25%",
						 "line-height":settings.size+"px",
						 "font-size":settings.fontSize
				});
				$("body").append(a);
			}
		a.html(b);
		
	
		if(settings.position=="bottom")
		{
			a.css("bottom","-"+settings.size+"px");
			a.animate({bottom:"0"},settings.speed);
			b='$("#'+settings.id+'").animate({bottom:"-'+settings.size+'px"}, '+settings.speed+");";
			setTimeout(b,settings.delay)
		}
		else if(settings.position=="top")
		{
				a.css("top","-"+settings.size+"px");
				a.animate({top:"0"},settings.speed);
				b='$("#'+settings.id+'").animate({top:"-'+settings.size+'px"}, '+settings.speed+");";
		}
		
		$('#clear-text').click(function() {
			a.html('');
			Ext.Ajax({ url : Ext.DOM.INDEX +"/ModBroadcastMsg/UpdateAll/", 
				param : { 
					UserId : settings.htmlBody.content.hiddenid
				}
			}).json();
		});
}})(jQuery);
	
	
	
// 

/* clear data if call later *****/
	
Ext.DOM.ClearData = function (messageid) {
	Ext.Ajax({ url : Ext.DOM.INDEX +"/ModBroadcastMsg/UpdateMessage/", param : { 
		messageid : messageid }
	}).json();
	
	getMessage();
}
	
/*  broadcast Messages  **/
/////////////////////////////////////////
	
Ext.DOM.getMessage = function() {	
	var options = {
		id				: 'message_from_top',
		position		: 'top',
		size			: 11,
		backgroundColor	: '#ffffff',
		delay			: 3000,
		speed			: 500,
		fontSize		: '12px',
		htmlBody		: {
			imgUrl	: Ext.DOM.LIBRARY+'/gambar/icon',
			title	: {
				icon	:'information.png',
				text	:'Message Box ',
				id		:'message-title'
			},
			
			content : {
				text	 : 'Hello world',
				cssBody	 : 'box-shadow',
				id		 : 'message-body',
				html	 : 'content-msg',	
				hiddenid : ''
			},
					
			close:{
				icon:'cancel.png',
				css:'test'
			}
		}
	};
			
		
		new ( function(){
			var HTML = '', Json = [];
			var divContent = Ext.Cmp(options.htmlBody.content.html).getElementId();
			
			Ext.Ajax
			({
				url 	: Ext.DOM.INDEX +"/ModBroadcastMsg/PoolMessage/",
				param 	: { time : Ext.Date().getDuration() },
				ERROR 	: function(e)
				{
					Ext.Util(e).proc(function(JSON){
						if( JSON.pesan.result==1 ) {
						
							var i = 0;
							for(var a in JSON.pesan) {
								if(i==0) 
									HTML = "<span style='color:blue;'><b style='font-size:12px;color:red;'>"+JSON.pesan[a].from+",</b> says : </span>";
								if( JSON.pesan[a].datetime!=undefined )
									HTML += " <br><span style='color:#030c7b;'>[ "+JSON.pesan[a].datetime+" ] </span><br>"+JSON.pesan[a].message+"&nbsp; ( <a href='javascript:void(0);' onclick='javascript:ClearData("+JSON.pesan[a].msgid+");'>Clear</a> )<br>";
								
							  i++;
							}
									
							options.htmlBody.content.text=HTML;
							options.htmlBody.content.hiddenid =	0;
							if( i > 0 ) 
								$.showMessage('', options);	
						}
						else{
							if( divContent!=null ) divContent.innerHTML='';
						}
					});
			 }}).post();
		});	
		
		return false;
	};	
	
	
// ControlWind 	
var ControlWind = window.setInterval("Ext.DOM.getMessage()",8000);	
