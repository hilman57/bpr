/*
 * @ def : Panel plugin 
 * -------------------------------------------
 *
 * @ param  : onject data { } 
 * @ type   : helpers 
 * @ author : razaki team 
 * @ link 	: http://razakitechnology.com/siteraztech/product/web-application/eui-framework
 */
 
(function(g,b){g.prototype.Panel=function(e){return{load:function(f){var a=b.Cmp("dialog-container").getElementId();if(null==a){var c=new b.Css(e),a=b.Create("div").element();a.setAttribute("id","dialog-container");a.setAttribute("class","ExtDialog box-shadow");a.setAttribute("style",c.curCSS(f.style));c=b.Create("div").element();c.setAttribute("class","dialog-content-close");c.setAttribute("id","dialog-content-close");c.innerHTML="<a href='javascript:void(0);' id='panel-close-header' title='close'><span class='close' style='padding-bottom:4px;padding-right:6px;float:center;margin-right:6px;'></span><a>"; a.appendChild(c);var d=b.Create("div").element();d.setAttribute("class","dialog-content-html");d.setAttribute("id","dialog-content-html");a.appendChild(d);b.Cmp(e).getElementId().appendChild(a);b.Cmp("panel-close-header").getElementId().onclick=function(){var a=b.Cmp("dialog-container").getElementId();a.parentNode.removeChild(a)};f.content({dialog:a.id,header:c.id,content:d.id})}}}}})(E_ui,Ext);