(function(Cores){
 Cores.prototype.ViewPort = function( CTI_doc ){
	var contact = {
		ctiCallSessionId : '',
		ctiParam  : {},
		ctiApplet : null,
		setApplet : function(){
			try {
				if( typeof(CTI_doc)=='object'){
					ExtApplet.ctiApplet = CTI_doc;	
				}
			}
			catch(e){
				Ext.Error({log : e , name : 'setApplet'});
			}
		},
		
		getApplet : function(){
			if( ExtApplet.ctiApplet !=null ){
				return ExtApplet.ctiApplet; 
			}
		},
		
		setData : function(param){
			if( typeof(param)=='object'){
				ExtApplet.ctiParam = param; 
				ExtApplet.ctiCallSessionId = ''; 
				var applet = ExtApplet.getApplet();
				var _a = {
					Call : function(){
						try{
							applet.callDialCustomer('', 
								( param.Phone ? param.Phone :'' ), 
								( param.CustomerId ? param.CustomerId :'' ), 
								( param.CustomerId ? param.CustomerId :''));
							return;
						}
						catch(e){
							console.log("Error\n\r"+ param)
						}
					}
				}
			}
			return _a;
		},
		
		setHangup : function() {
			if( typeof( ExtApplet.getApplet() ) =='object' ) {
				try{
					ExtApplet.ctiCallSessionId = document.ctiapplet.getCallSessionKey();
					document.ctiapplet.callHangup();
					return false;
				} catch(e){
					console.log(e);
					return false;
				}	
			}
		},
		
		getCtiParam : function(){
			if( typeof(ExtApplet.ctiParam) =='object'){
				return ExtApplet.ctiParam;
			}
		},
		
		getPhoneNumber : function(){
			if( typeof(ExtApplet.ctiParam) =='object'){
				return ExtApplet.ctiParam.Phone;
			}
		},
		
		getCustomerId : function(){
			if( typeof(ExtApplet.ctiParam) =='object'){
				return ExtApplet.ctiParam.CustomerId;
			}
			else
				return false;
		},
		
		getCallerId : function(){
			if( typeof(ExtApplet.getApplet()) =='object' ){
				return document.ctiapplet.getCallerId();
			}
			else
				return false;
		},
		
		getDirection: function(){
			if( typeof(ExtApplet.getApplet()) =='object' ){
				return document.ctiapplet.getCallDirection()
			}
			else
				return false;
		},
		
		getCallSessionId : function(){
			if( typeof(ExtApplet.getApplet()) =='object' ){
				return ExtApplet.ctiCallSessionId;
			}
			else
				return false;
		},
		
		getOnSessionId : function(){
			if( typeof(ExtApplet.getApplet()) =='object' ){
				return ExtApplet.ctiCallSessionId = document.ctiapplet.getCallSessionKey();
			}
			else
				return false;
		},
		
		getIvrData : function(){
			if( typeof(ExtApplet.getApplet()) =='object' ){
				document.ctiapplet.getCallerId();
			}
			else
				return false;
		}
	}
	
	return contact;
 }
})(E_ui); 