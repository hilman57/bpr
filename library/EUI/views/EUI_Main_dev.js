/*
 * @ pack  : its will ask confirmation to user if 
 *           User reload of the page its. will write to log 
 *			 for handle condition 
 * -----------------------------------------------------------------
 * 
 * @ pack  : EUI_Main View Controller JS 
 * @ param : hanle main content page of application 
 * @ param : - 
 */


/*
 * @ pack : Main View Index Controller JS #--------------------------------------------------------------------------------------------------------
 */
 
Ext.DOM.INDEX = Ext.System.view_page_index();
Ext.DOM.setTimeOutId = 0;
/*
 * @ pack : is ready of jquery then required all  
 */
 
function DefaultAksessAll()
{
	Ext.Ajax
	({
		url 	: Ext.EventUrl(new Array("SrcAksesAll", "DefaultRemoveKeys") ).Apply(),
		method  : 'POST',
		param 	: {
			time : Ext.Date().getDuration()
		}	
	}).post();
}

/*
 * @ pack : Main View Index Controller JS #--------------------------------------------------------------------------------------------------------
 */
function blocked() 
{
	Ext.Ajax
	({
		url 	: Ext.EventUrl(new Array("Auth", "Blockeds") ).Apply(),
		method  : 'POST'
	}).post();
}
/*
 * @ pack : Main View Index Controller JS #--------------------------------------------------------------------------------------------------------
 */
function Logout()
{
	Ext.Ajax
	({
		url 	: Ext.EventUrl(new Array("Auth", "Logout") ).Apply(),
		method  : 'POST'
	}).post();
}
/*
 * @ pack : Main View Index Controller JS #--------------------------------------------------------------------------------------------------------
 */
var getHandlingType = function(){
	return ( 
	Ext.Ajax({
			url : Ext.DOM.INDEX +'/Auth/getHandlingType/',
			method :'GET'
			// param :{
				// action:'getATMName'
			// }
		}).json()
	)
}
Ext.DOM.CekJam =function()
{
	Ext.DOM.AutoLogout = setInterval(
	function() {
		var date = new Date();
		var currentTime = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
		var currentTime12 = date.getHours() + ':' + date.getMinutes();
		var currentHours = date.getHours();
		var currentMinutes = date.getMinutes();
		var currentSeconds = date.getSeconds();
		var Hari = date.getDay();

		// alert(currentTime);
		if(parseInt(Hari) === 0) {
			// alert(typeof(currentHours)+currentHours);
			if(parseInt(currentHours)>5 && parseInt(currentHours)<23){
				Logout();
				location.reload();
			}
		}
		else if(Hari != 6) {
			// if(currentTime >= "15:20:10" && currentTime <= "15:20:13") {
			// if((currentHours >= '11' && currentMinutes >= '55') && (currentHours <= '11' && currentMinutes <= '55' && currentSeconds <= '05')) {
				// alert('Tepat Pukul 12:00 akan dilakukan logout otomatis. Yang sedang ada aktifitas silahkan dilogout sebelum pukul 12:00 untuk menghindari data yang hilang. Terima kasih');
			// }
			
			if((currentHours >= '19' && currentMinutes >= '55') && (currentHours <= '19' && currentMinutes <= '55' && currentSeconds <= '20')) {
				alert('Tepat Pukul 20:00 akan dilakukan logout otomatis. Yang sedang ada aktifitas silahkan dilogout sebelum pukul 20:00 untuk menghindari data yang hilang. Terima kasih');
			}
			
			// if(currentTime >= "12:00:15" && currentTime <= "12:59:59") {
			// if((parseInt(currentHours) >= '12' && parseInt(currentMinutes) >= '00' && parseInt(currentSeconds) >= '15') &&
				// (parseInt(currentHours) <= '12' && parseInt(currentMinutes) <= '59' && parseInt(currentSeconds) <= '59')) {
					// var HandlingType = getHandlingType();
					// if(parseInt(HandlingType.handling_type) != '1' && parseInt(HandlingType.handling_type) != '8' && parseInt(HandlingType.handling_type) != '3'){
						// Logout();
						// location.reload();
					// }else{
						// console.log("No logout");
					// }
			// }
			if((parseInt(currentHours) >= '20' && parseInt(currentMinutes) >= '00' && parseInt(currentSeconds) >= '10') &&
				(parseInt(currentHours) <= '20' && parseInt(currentMinutes) <= '00' && parseInt(currentSeconds) <= '20')) {
				Logout();
				location.reload();
			}
		}
		else if(Hari == 6){
			// alert(typeof(currentMinutes)+currentMinutes);
			if((parseInt(currentHours) >= 16) && (parseInt(currentHours) <= 23)) {
				var HandlingType = getHandlingType();
				if(parseInt(HandlingType.handling_type) != '1' && parseInt(HandlingType.handling_type) != '8' && parseInt(HandlingType.handling_type) != '3'){
					Logout();
					location.reload();
				}else{
					console.log("No logout");
				}
			}
		}
	}, 10000);
}
/*
 * @ pack : is ready of jquery then required all  
 */
 
$(document).ready(function()
{
	Ext.DOM.CekJam();
	
   $.ajaxSetup({ cache: false }); 
   $("#accordion").accordion({
		icons: {
		header: "ui-icon-circle-arrow-e",
			headerSelected: "ui-icon-circle-arrow-s"
		},autoHeight : true
	});
	
	$("#accordions").accordion({
		icons: {
		header: "ui-icon-circle-arrow-e",
			headerSelected: "ui-icon-circle-arrow-s"
		},autoHeight : true
	});
	
	Ext.ShowMenu(new Array("Welcome", "index"), "Welcome", {
		time : Ext.Date().getDuration()
	});
	
	//$('#main_content').load( Ext.DOM.INDEX+'/Welcome');	
	
	
	$('.formPhoneDialog').dialog({
		title		:'Form Additional Phone',
		bgiframe	:true,
		width		:400,
		height		:270,
		autoOpen	:false,
		modal		:true,
		closeOnEscape: false,
   		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	
	$('.changedRequest').dialog({
		title		:'Form Request to change',
		bgiframe	:true,
		width		:400,
		height		:270,
		autoOpen	:false,
		modal		:true,
		draggable	:true,
		closeOnEscape: false,
   		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});	  
	
	var options = {  target:  '#password_confirm', success: showResponse }; 
	
});

/*
 * @ pack : call (this) EUI_libs Cores  #--------------------------------------------------------------------------------------------------------
 */
 
;(function(Cores,$){ 
 Cores.prototype.ShowMenu = function(controller, title, object) 
 {
	$.cookie('selected',0);
	
	Ext.System.view_name_url(title);
	try 
	{	
		try 
		{ 
			if(controller=='Logout'){ UserLogOut(); return false; }
			else if(controller=='Password') { Password(); return false; }
			else
			{
				if( Ext.DOM.setTimeOutId!=0 ) {
					clearTimeout(Ext.DOM.setTimeOutId)
				}
				
				// ------------ for Akksess all ---------------
				
				if( controller == 'SrcAksesAll' ){
					new DefaultAksessAll();
				}
				
				// ---------- load page image loading content -------------------------	
				$('#main_content').html("<div class='ui-widget-ajax-spiner'></div>");
				$('#main_content').load( Ext.EventUrl(controller).Apply(), object, 
				  function( response, status, xhr ) {
					if( status == 'error') { 
						$('#main_content').html(response);	 
					}	
				}); 
			}	
		} 
		catch(e){
			Ext.Error({log :e, name:'Ext.getWebContent();', lineNumber : e.lineNumber });
		}
	}
	catch(e){
		Ext.Error({log: e, name:'Ext.getWebContent();', lineNumber : e.lineNumber });
	}
 }	 
 
})(E_ui,jQuery);


/*
 * @ pack : showResponse #--------------------------------------------------------------------------------------------------------
 */
 
 function showResponse(responseText)
 {
      $("#password_confirm").dialog({
  			bgiframe: true,
  			modal: true,
  			buttons: {
  				Ok: function() {
  					$(this).dialog('close');
  				}
  			}
  		});
 } 	
	
/* 
 * @ pack : jQuery Bugs( jQuery-ui.js - Line 7287 )
 * 			Untuk menghindari dialog yang di close lewat icon (X)
 * 			yang terkadang tidak di destroy dengan benar maka untuk
 * 			membuat dialog sebaik-nya di simpan dalam function 
 * -------------------------------------------------------------------------
 */
  
 Ext.DOM.AdditionalPhone = function( CustomerId )
{
	Ext.Window
	({
		name 		: "win-multiline-add",
		url  		: Ext.EventUrl(new Array('ModApprovePhone','ViewMultipleAdd')).Apply(),
		left		: (Ext.Layout(window).Width()/2),
		top			: (Ext.Layout(window).Height()/2),	
		width 		: 660,
		height 		: 450,
		scrollbars	: 1,
		resizable	: 1,
		param		: {
			CustomerId : CustomerId,
			UserId : Ext.Session('Username').getSession(),
			Level  : Ext.Session('HandlingType').getSession()
		}	
	}).popup();
}  

/*
 * @ pack : showResponse #--------------------------------------------------------------------------------------------------------
 */  
 
var Password = function()
{
  $("#pass").dialog
  ({
	 bgiframe: true,
  	 autoOpen: false,
  	 height: 210,
	 width:350,
  	 modal: true,
  	 buttons: {
  				'Update': function()
				{
					Ext.Ajax({
						url : Ext.DOM.INDEX+'/Auth/UpdatePassword',
						method :'POST',
						param : {
							curr_password   : Ext.Cmp('curr_password').getValue(),
							new_password    : Ext.Cmp('new_password').getValue(),
							re_new_password : Ext.Cmp('re_new_password').getValue()
						},
						ERROR : function( e ){
							Ext.Util(e).proc(function(update){
								if( update.success==1 ){ 
									Ext.Msg("Update Yours Password").Success(); 
									$(this).dialog('close'); } 
									
								else if( update.success==2 ){
									Ext.Msg("Please input diffrent password").Info();
									return false; } 
									
								else if( update.success==3 ){
									Ext.Msg("Password has been used . \n\rPlease use diffrent password ").Info();
									return false; }
									
								else{
									Ext.Msg("Failed, Update Yours Password").Failed();
									return false;
								}
							});
						}
					}).post()
  				},
  				Cancel: function() {
  					$(this).dialog('close');
  				}
  			}
  	}).dialog('open');
 } 
 
/*
 * @ pack : UserLogOut #--------------------------------------------------------------------------------------------------------
 */  
 
var UserLogOut = function()
{	
	$("#logout").dialog
	({
			 bgiframe	: true,
			 autoOpen	: false,
			 resizable	: false,
			 height		: 140,
			 modal		: true,
			 overlay: {
					backgroundColor: '#000',
					opacity: 0.5
			 },
			 buttons: {
				'Logout': function() {
					$.cookie('limit_timer',0);
					document.location= Ext.DOM.INDEX+'/Auth/logout/?login=(false)';
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			 }
			 
		}).dialog('open');
	}

 
/*
 * @ pack : UserLogOut #--------------------------------------------------------------------------------------------------------
 */ 
 
 var ChatWith = function(){
	var dialog = new Ext.Window
		({
			url     : Ext.DOM.INDEX+'/ChatWith/UserReady/',
			height	: (Ext.Layout(window).Height()/2),
			width	: (Ext.Layout(window).Width()/2),
			left	: (Ext.Layout(window).Width()/2),
			top		: (Ext.Layout(window).Height()/2),
			name 	: 'WinChat',
			param 	: {
				time :  Ext.Date().getDuration()
			}
		});
	dialog.popup();
 }

var AddNote = function(){
 	var dialog = new Ext.Window
 	({
 		url 	: Ext.DOM.INDEX+'/SaveMemo/Add/',
 		height 	: (Ext.Layout(window).Height()/2),
 		width	: (Ext.Layout(window).Width()/2),
			left	: (Ext.Layout(window).Width()/2),
			top		: (Ext.Layout(window).Height()/2),
			name 	: 'WinChat',
			param 	: {
				time :  Ext.Date().getDuration()
			}
	});
	dialog.popup();
 }

/*
 * @ pack : UserLogOut #--------------------------------------------------------------------------------------------------------
 */ 
 
var RequestDis = function(){ 
 if( typeof(Ext) == 'object' ){
  Ext.ShowMenu("ModApprovalDiscount","Request Discount"); 
 }
}
 
/*
 * @ pack : UserLogOut #--------------------------------------------------------------------------------------------------------
 */ 
 
var SMS = function(){ 
 if( typeof(Ext) == 'object' ){
  Ext.ShowMenu("SMSApproval","SMS Approval"); 
 } 
} 

/*
 * @ pack : UserLogOut #--------------------------------------------------------------------------------------------------------
 */ 
 
 var FindBank = function() 
 {
	$(this).dialog('destroy');	
	$("#finder").dialog ({
  			bgiframe: true,
  			autoOpen: false,
  			height: ($(window).height()-150),
			width: ($(window).width()-35),
  			modal: true,
  			buttons: {
  				Cancel: function() {
  					$(this).dialog('close');
					$(this).dialog('destroy');
  				}
  			}
  	}).dialog('open');
 }

/* write log to data **/
 
;(function( $ ){ 
 var KEY_USER_OPTION = false;
 $.onload = ( function(event){
	Ext.Ajax({ 
		url 	: Ext.DOM.INDEX+"/Auth/Refresh/",
		method 	:'GET',
		param   :{
			time : Ext.Date().getDuration()
		},
		ERROR : function( e ){
			Ext.Util(e).proc(function(response){
				if( response.success ){ 
					console.log('refresh load ok');
				}
			});
		}
		
	}).post();
 })( $ );
 
/* handle key on keydow by User **/

 $.document.onkeydown = function( e ){
 e = e || $.event;
 if( KEY_USER_OPTION ) return; 
  if (e.keyCode == 116) {  return false; }
};

/* handle key on keypress by User **/

 $.document.onkeypress = function( e ){
  e = e || $.event;
  if( KEY_USER_OPTION ) return; 
   if (e.keyCode == 116) {  return false; }
 };

/* handle key on onkeyup by User **/

$.document.onkeyup = function( e ){
 e = e || $.event;
 if( KEY_USER_OPTION ) return; 
  if (e.keyCode == 116) {  return false; }
};

})( window ); 
