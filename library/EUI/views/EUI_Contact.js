/*
 * @ def : Ext.Applet --> encapsule
 * -------------------------------------------
 *
 * @ param  : object data { } 
 * @ type   : helpers 
 * @ author : razaki team 
 * @ link 	: http://razakitechnology.com/siteraztech/product/web-application/eui-framework
 */
 
(function(b){b.prototype.ViewPort=function(c){return{ctiCallSessionId:"",ctiParam:{},ctiApplet:null,setApplet:function(){try{"object"==typeof c&&(ExtApplet.ctiApplet=c)}catch(a){Ext.Error({log:a,name:"setApplet"})}},getApplet:function(){if(null!=ExtApplet.ctiApplet)return ExtApplet.ctiApplet},setData:function(a){if("object"==typeof a){ExtApplet.ctiParam=a;ExtApplet.ctiCallSessionId="";var c=ExtApplet.getApplet(),b={Call:function(){try{c.callDialCustomer("",a.Phone?a.Phone:"",a.CustomerId?a.CustomerId:"",a.CustomerId?a.CustomerId:"")}catch(b){console.log("Error\n\r"+a)}}}}return b},setHangup:function(){if("object"==typeof ExtApplet.getApplet())try{return ExtApplet.ctiCallSessionId=document.ctiapplet.getCallSessionKey(),document.ctiapplet.callHangup(),!1}catch(a){return console.log(a),!1}},getCtiParam:function(){if("object"==typeof ExtApplet.ctiParam)return ExtApplet.ctiParam},getPhoneNumber:function(){if("object"==typeof ExtApplet.ctiParam)return ExtApplet.ctiParam.Phone},getCustomerId:function(){return"object"==typeof ExtApplet.ctiParam?ExtApplet.ctiParam.CustomerId:!1},getCallerId:function(){return"object"==typeof ExtApplet.getApplet()?document.ctiapplet.getCallerId():!1},getDirection:function(){return"object"==typeof ExtApplet.getApplet()?document.ctiapplet.getCallDirection():!1},getCallSessionId:function(){return"object"==typeof ExtApplet.getApplet()?ExtApplet.ctiCallSessionId:!1},getOnSessionId:function(){return"object"==typeof ExtApplet.getApplet()?ExtApplet.ctiCallSessionId=document.ctiapplet.getCallSessionKey():!1},getIvrData:function(){if("object"==typeof ExtApplet.getApplet())document.ctiapplet.getCallerId();else return!1}}}})(E_ui);