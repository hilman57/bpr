/*
 * @ def : Ext.Applet --> Timer every User
 * -------------------------------------------
 *
 * @ param  : object data { } 
 * @ type   : helpers 
 * @ author : razaki team 
 * @ link 	: http://razakitechnology.com/siteraztech/product/web-application/eui-framework
 */
 
// ----------------------------------------------------- 
/*
 * @ pack :  status agent 
 * @ return : void (0) 
 */
// -----------------------------------------------------

var TimerActivity = { TimerAgentStatus :0, TimerCallStatus : 0, TimerLimit : 18000, TimerCookies: 0};

$(document).ready(function(){
  try { 
	if( typeof( document.ctiapplet)=='object' ){
		TimerActivity.TimerAgentStatus = document.ctiapplet.getAgentStatus(); 
	}
  } 
  catch( e ){ console.log(e); }
});

// ----------------------------------------------------- 
/*
 * @ pack :  status agent 
 * @ return : void (0) 
 */
// -----------------------------------------------------

var IntervalTime = null;

// ----------------------------------------------------- 
/*
 * @ pack :  status agent 
 * @ return : void (0) 
 */
// -----------------------------------------------------

(function(UI,Ext){
UI.prototype.Timer = function( offString )
{
var objHandle = ( typeof ( offString ) =='string' ? offString : null ),
time =  {
 
// ----------------------------------------------------- 
/*
 * @ pack :  status agent 
 * @ return : void (0) 
 */
// -----------------------------------------------------

 UserPrivilege : [
	Ext.DOM.USER_LEVEL_ADMIN, 
	Ext.DOM.USER_LEVEL_MANAGER,
	Ext.DOM.USER_LEVEL_SPV,
	Ext.DOM.USER_LEVEL_QUALITY,
	Ext.DOM.USER_SYSTEM_LEVEL,
	Ext.DOM.USER_LEVEL_LEADER
 ], 
 
// ----------------------------------------------------- 
/*
 * @ pack :  status agent 
 * @ return : void (0) 
 */
// -----------------------------------------------------
 AvailCallStatus : [ 
   INBOUND_CALL,
   OUTBOUND_CALL,
   CALLSTATUS_IDLE,
   CALLSTATUS_HELD,
   CALLSTATUS_ALERTING,
   CALLSTATUS_CONNECTED,
   CALLSTATUS_ANSWERED,
   CALLSTATUS_ORIGINATING,
   CALLSTATUS_TRUNKSEIZED,
   CALLSTATUS_SERVICEINITIATED 
 ],	
 
// ----------------------------------------------------- 
/*
 * @ pack :  status agent 
 * @ return : void (0) 
 */
// -----------------------------------------------------
 AvailStatus : [ 
	AGENT_NULL, 
	AGENT_LOGIN, 
	AGENT_READY, 
	AGENT_NOTREADY 
],
  
// ----------------------------------------------------- 
/*
 * @ pack : get activer timer process 
 * @ return : void (0) 
 */
// -----------------------------------------------------
  
  Active : function(timer) 
  {
	if( typeof (document.ctiapplet)!='undefined' ) {
	
		IntervalTime = window.setInterval(function(){
			Ext.Ajax ({ 
				url   : Ext.DOM.INDEX+"/MonAgentActivity/UserActivity/", 
				param : {},
				ERROR : function(e){
					try { Ext.Util(e).proc(function(UserActivity){
					$(function(){
					
						var $cookie_start = 0;
						
						if( AGENT_BUSY == UserActivity.ExtStatus){
							$.cookie('limit_timer', 0 );
						}
						
						if( CALLSTATUS_ORIGINATING == UserActivity.ExtStatus){
							$.cookie('limit_timer', 0 );
						}
						
						$cookie_start = parseInt($.cookie('limit_timer')); // get data cookie 
						
					/* 
					 *	if cookie is start = 0 then insert timer db 
					 *	if cookie is start > 0 then cookie+start time 
					 */
						TimerActivity.TimerCookies = ( $cookie_start ? ( $cookie_start + UserActivity.millisecond ) : UserActivity.millisecond);	
							
					/*
					 * then set to cookie 
					 */
						$.cookie('limit_timer',TimerActivity.TimerCookies);	
					/*
					 * send message to function to execute by condition  
					 */
					 // $.cookie('limit_timer')
						Ext.Timer(offString).TimerExpired($.cookie('limit_timer'));
							
					/*
					 * then set to cookie 
					 */
						Ext.Cmp(offString).setText(UserActivity.time);
						
					 });
					 
					});
				} catch(e){
					console.log(e);
				}
			}
		}).post();
	  },timer);
	  
	  
   }
  },
  
// ----------------------------------------------------- 
/*
 * @ pack : get timer of agent by status in here 
 * @ return : void (0) 
 */
// -----------------------------------------------------
 
 TimerExpired : function( timer )
 {
	if( parseInt(timer) >= TimerActivity.TimerLimit )
	{ 
		var IsAgentStatus = Ext.Timer().AvailStatus;
		if( Ext.Array(IsAgentStatus).in_array(TimerActivity.TimerAgentStatus) ) {
			$.cookie('limit_timer',0);
			SetLockMonitoring(); 
			return true;
		}
		
	}						
 },

// ----------------------------------------------------- 
/*
 * @ pack :  clear of interval ID 
 * @ return : void (0) 
 */
// -----------------------------------------------------
  
 IntervalID : function() 
 {
	if( (typeof(IntervalTime)!=null) ) 
	{
		return IntervalTime;
	} 
	
	return null;
  }
};


 return ( typeof(time)=='object' ? time : false );			
		
}
})(E_ui,Ext);

