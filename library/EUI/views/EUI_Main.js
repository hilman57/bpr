/* 
 * @ package 	: views 
 * @ namesapce  : EUI_Main.js
 * -----------------------------
 *
 * @ under libs : Enigma User Inter face frame work < EUI >  
 * @ globals    : Ext
 *
 */
Ext.DOM.INDEX = Ext.System.view_page_index();
Ext.DOM.setTimeOutId = 0;

function Logout()
{
	Ext.Ajax
	({
		url 	: Ext.EventUrl(new Array("Auth", "Logout") ).Apply(),
		method  : 'POST'
	}).post();
}

function CekJam()
{
	setInterval(
	function() {
		var date = new Date();
		var currentTime = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
		var currentTime12 = date.getHours() + ':' + date.getMinutes();
		// alert(currentTime);
		
		if(currentTime >= "11:55:10" && currentTime <= "11:55:20") {
			alert('Tepat Pukul 12:00 akan dilakukan logout otomatis. Yang sedang ada aktifitas silahkan dilogout sebelum pukul 12:00 untuk menghindari data yang hilang. Terima kasih');
		}
		
		if(currentTime >= "19:55:10" && currentTime <= "19:55:20") {
			alert('Tepat Pukul 20:00 akan dilakukan logout otomatis. Yang sedang ada aktifitas silahkan dilogout sebelum pukul 20:00 untuk menghindari data yang hilang. Terima kasih');
		}
		
		if(currentTime >= "12:00:10" && currentTime <= "12:00:20") {
			alert('Sistem akan logout otomatis. Terima kasih');
			Logout();
			location.reload();
		}
		
		if(currentTime >= "20:00:10" && currentTime <= "20:00:20") {
			alert('Sistem akan logout otomatis. Terima kasih');
			Logout();
			location.reload();
		}
	}, 10000);
}

$(document).ready(function() {
	CekJam();
    $.ajaxSetup({
        cache: !1
    });
    $("#accordion").accordion({
        icons: {
            header: "ui-icon-circle-arrow-e",
            headerSelected: "ui-icon-circle-arrow-s"
        },
        autoHeight: !0
    });
    $("#accordions").accordion({
        icons: {
            header: "ui-icon-circle-arrow-e",
            headerSelected: "ui-icon-circle-arrow-s"
        },
        autoHeight: !0
    });
    $("#main_content").load(Ext.DOM.INDEX + "/Welcome");
    $(".formPhoneDialog").dialog({
        title: "Form Additional Phone",
        bgiframe: !0,
        width: 400,
        height: 270,
        autoOpen: !1,
        modal: !0,
        closeOnEscape: !1,
        open: function(b, a) {
            $(".ui-dialog-titlebar-close").hide()
        }
    });
    $(".changedRequest").dialog({
        title: "Form Request to change",
        bgiframe: !0,
        width: 400,
        height: 270,
        autoOpen: !1,
        modal: !0,
        draggable: !0,
        closeOnEscape: !1,
        open: function(b, a) {
            $(".ui-dialog-titlebar-close").hide()
        }
    })
});
(function(b, a) {
    b.prototype.ShowMenu = function(b, e) {
        a.cookie("selected", 0);
        Ext.System.view_name_url(e);
        try {
            try {
                if ("Logout" == b) return UserLogOut(), !1;
                if ("Password" == b) return Password(), !1;
                0 != Ext.DOM.setTimeOutId && clearTimeout(Ext.DOM.setTimeOutId);
                a("#main_content").load(Ext.DOM.INDEX + "/" + b)
            } catch (c) {
                Ext.Error({
                    log: c,
                    name: "Ext.getWebContent();",
                    lineNumber: c.lineNumber
                })
            }
        } catch (d) {
            Ext.Error({
                log: d,
                name: "Ext.getWebContent();",
                lineNumber: d.lineNumber
            })
        }
    }
})(E_ui, jQuery);

function showResponse(b) {
    $("#password_confirm").dialog({
        bgiframe: !0,
        modal: !0,
        buttons: {
            Ok: function() {
                $(this).dialog("close")
            }
        }
    })
}
Ext.DOM.AdditionalPhone = function(b) {
    Ext.query("#WindowUserDialog").dialog({
        title: "Submit Phone Number",
        bgiframe: !0,
        autoOpen: !1,
        cache: !1,
        height: 210,
        width: 350,
        close: function(a, b) {
            $(this).empty();
            $(this).remove()
        },
        modal: !0,
        buttons: {
            Submit: function() {
                Ext.Ajax({
                    url: Ext.DOM.INDEX + "/ModApprovePhone/SavePhone/",
                    method: "POST",
                    param: Ext.Join([Ext.Serialize("frmAddPhone").getElement()]).object(),
                    ERROR: function(a) {
                        Ext.Util(a).proc(function(a) {
                            a.success ? (Ext.Msg("Request Submit Phone ").Success(), $(this).dialog("close"), $(this).remove()) : Ext.Msg("Request Submit Phone ").Failed()
                        })
                    }
                }).post()
            }
        }
    }).load(Ext.DOM.INDEX + "/ModApprovePhone/ViewAddPhone/?CustomerId=" + b + "&time=" + Ext.Date().getDuration()).dialog("open")
};
var Password = function() {
        $("#pass").dialog({
            bgiframe: !0,
            autoOpen: !1,
            height: 210,
            width: 350,
            modal: !0,
            buttons: {
                Update: function() {
                    Ext.Ajax({
                        url: Ext.DOM.INDEX + "/Auth/UpdatePassword",
                        method: "POST",
                        param: {
                            curr_password: Ext.Cmp("curr_password").getValue(),
                            new_password: Ext.Cmp("new_password").getValue(),
                            re_new_password: Ext.Cmp("re_new_password").getValue()
                        },
                        ERROR: function(b) {
                            Ext.Util(b).proc(function(a) {
                                if (1 == a.success) Ext.Msg("Update Yours Password").Success(), $(this).dialog("close");
                                else return 2 == a.success ? Ext.Msg("Please input diffrent password").Info() : 3 == a.success ? Ext.Msg("Password has been used . \n\rPlease use diffrent password ").Info() : Ext.Msg("Failed, Update Yours Password").Failed(), !1
                            })
                        }
                    }).post()
                },
                Cancel: function() {
                    $(this).dialog("close")
                }
            }
        }).dialog("open")
    },
    UserLogOut = function() {
        $("#logout").dialog({
            bgiframe: !0,
            autoOpen: !1,
            resizable: !1,
            height: 140,
            modal: !0,
            overlay: {
                backgroundColor: "#000",
                opacity: .5
            },
            buttons: {
                Logout: function() {
                    document.location = Ext.DOM.INDEX + "/Auth/logout/?login=(false)"
                },
                Cancel: function() {
                    $(this).dialog("close")
                }
            }
        }).dialog("open")
    },
    ChatWith = function() {
        (new Ext.Window({
            url: Ext.DOM.INDEX + "/ChatWith/UserReady/",
            height: Ext.Layout(window).Height() / 2,
            width: Ext.Layout(window).Width() / 2,
            left: Ext.Layout(window).Width() / 2,
            top: Ext.Layout(window).Height() / 2,
            name: "WinChat",
            param: {
                time: Ext.Date().getDuration()
            }
        })).popup()
    },
    FindBank = function() {
        $(this).dialog("destroy");
        $("#finder").dialog({
            bgiframe: !0,
            autoOpen: !1,
            height: $(window).height() - 150,
            width: $(window).width() - 35,
            modal: !0,
            buttons: {
                Cancel: function() {
                    $(this).dialog("close");
                    $(this).dialog("destroy")
                }
            }
        }).dialog("open")
    };
(function(b) {
    b.onload = function(a) {
        Ext.Ajax({
            url: Ext.DOM.INDEX + "/Auth/Refresh/",
            method: "GET",
            param: {
                time: Ext.Date().getDuration()
            },
            ERROR: function(a) {
                Ext.Util(a).proc(function(a) {
                    a.success && console.log("refresh load ok")
                })
            }
        }).post()
    }(b);
    b.document.onkeydown = function(a) {
        a = a || b.event;
        if (116 == a.keyCode) return !1
    };
    b.document.onkeypress = function(a) {
        a = a || b.event;
        if (116 == a.keyCode) return !1
    };
    b.document.onkeyup = function(a) {
        a = a || b.event;
        if (116 == a.keyCode) return !1
    }
})(window);