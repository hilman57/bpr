/*
 * $.plugin page jqueery modiofied by 
 * $.author::razakiteam && add cookies 
 * 
 */
var cookie_page = ( $.cookie('selected') ? $.cookie('selected') : 0 );
var total_record =  ( $.cookie('record') ? $.cookie('record') : 0 );
var onclick_flags = false;

// ------------------------------------------------------------
/*
 * @ pack : reset all data summary 
 */
 // --------------------------------------------------------------
$(function(){ Ext.EQuery.Summary =''; });
(function($){
 $.fn.aqPaging = function( options ) {
	var opts = $.extend({ },$.fn.aqPaging.defaults,options);
	return this.each( function() {
		if (!$( '.aqPaging',this ).length ) {
			$.fn.aqPaging.defaults.uniqID++;
			$('<div class="aqPaging" id="aqPaging_'
				+$.fn.aqPaging.defaults.uniqID+'"><\/div> &nbsp;'+( opts.rec ?''+
					'<div class="page-bottom-custom-nav">'+
					'<li class="first total-record page-bottom-custom-grid">Total Records : ( '+opts.records+' ) </li>'+
					''+( (typeof(opts.summary)=='undefined' || opts.summary=='') ? '' : '<li class="middle summary page-bottom-custom-grid">'+opts.summary+'</li>') +''+
					'<li class="lasted refresh page-bottom-custom-grid" '+ (typeof(opts.callfunc)=='object' ? 'onclick="Ext.EQuery.postContent();"' : '' ) +'>&nbsp;Refresh</li>'+
					+'</div>' : '') +'').appendTo(this);
				
			$.fn.aqPaging.defaults.cbs[$.fn.aqPaging.defaults.uniqID] = opts.cb;
			$('.aqPaging',this).css(opts.css);
		}

		var $pager = $('.aqPaging',this);
		var pid = $pager.attr('id');

		var s = 1, e = opts.pages;
		var html = '';
		var rows = '';
		var offset = (opts.current > opts.max) ? 1 : 0;

		if (opts.pages > opts.max) {
			if (opts.current > opts.max)
				s = opts.max*parseInt((opts.current-offset)/opts.max);

			if (opts.current-offset+opts.max < opts.pages) 
				e = s + opts.max + offset;
		}

		for (var p=s; p<=e; p++)
			html += '<a href="javascript:void(0)" onclick="$.fn.aqPaging.flip(\''+pid+'\'' + ','+p+','+opts.pages+');">' + p + '<\/a> ';
				
			
		$pager.html(html);
		
		if (opts.current >= s && opts.current-opts.max > 0) {
			$pager.prepend('<a href="javascript:void(0)" onclick="$.fn.aqPaging.flip(\''+pid+'\''+',1,'+opts.pages +')">1<\/a> <i>&hellip;<\/i> ');
		}
		
		if ((opts.current-offset+opts.max) <= opts.pages && e != opts.pages) {
			$pager.append('<i>&hellip;<\/i>' + ' <a href="javascript:void(0)" onclick="$.fn.aqPaging.flip(\''+pid+'\''+','+opts.pages+','+opts.pages +')">'+opts.pages+'<\/a> ');
		}

		var hi = ((opts.current-1)%opts.max) + ((offset+1)*offset);
		if (opts.css) {
			var _bg = $pager.css('backgroundColor');
			var _fg = $pager.css('color');
			var _bg2 = (_bg == 'transparent') ? '#fff' : _bg;
			
			$pager.find( 'a' ).css( opts.blockCss ).css({
				margin : '2px', 
				padding : '2px 5px', 
				color : _fg,
				borderWidth : '1px', 
				borderStyle : 'solid', 
				borderColor : _fg 
			}).not(':eq('+hi+')').hover(
				function() { $(this).css({ backgroundColor: _fg, borderColor: opts.borderColor, color: _bg2 }) },
				function() { $(this).css({ backgroundColor: _bg, borderColor: _fg, color: _fg }) }
			);
			
			$pager.find('a').eq(hi).css({ backgroundColor: _fg, borderColor: opts.borderColor, color: _bg2 });
			$pager.find('i').css(opts.blockCss);
			
		} else
			$pager.find('a').removeClass('aqPagingHi').eq(hi).addClass('aqPagingHi');

		if (opts.flip)
			$.fn.aqPaging.flip(pid,opts.current,opts.pages);
	});
};

/* 
 * @ set page currpage by cookie setup 
 * @ if user click page 
 * @ auth : omens 
 */
 
$.fn.aqPaging.flip = function(id,p,total) {
$("body").click(function( event ) {  
	onclick_flags = true; 
 });

if(cookie_page!=0 ) {

  if( (cookie_page > 1) && ( p==1 ) && ( total_record == total )) {
	 if( onclick_flags==false ){
		var idx = id.replace(/aqPaging_/,'');
		var func = $.fn.aqPaging.defaults.cbs[idx];
		if (func) func(cookie_page);
		$('#'+id).parent().aqPaging({current: cookie_page, pages: total});
		$.cookie('selected',cookie_page);
	 } else {
		var idx = id.replace(/aqPaging_/,'');
		var func = $.fn.aqPaging.defaults.cbs[idx];
		if (func) func(p);
		$('#'+id).parent().aqPaging({current: p, pages: total});
		$.cookie('selected',p);
	}
		
  } else {
	 var idx = id.replace(/aqPaging_/,'');
	 var func = $.fn.aqPaging.defaults.cbs[idx];
	 if (func) func(p);
	 $('#'+id).parent().aqPaging({current: p, pages: total});
	 $.cookie('selected',p);		
  }
	 
} else {
	 var idx = id.replace(/aqPaging_/,'');
	 var func = $.fn.aqPaging.defaults.cbs[idx];
	 if (func) func(p);
	 $('#'+id).parent().aqPaging({current: p, pages: total});
	 $.cookie('selected',p);
}

$.cookie('record', total); 

return false;

};

$.fn.aqPaging.defaults = {
	cbs: [], pages: 0, current: 0, max: 10, uniqID: 0, flip: false,
	css: { fontFamily: 'arial', padding: '0px' },
	blockCss: { display:'block', float:'left' },
	borderColor: '#444'
};
})(jQuery);

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
